---
title: 🕹 Arcade
description: 
published: true
date: 2021-06-22T13:11:12.351Z
tags: 
editor: markdown
dateCreated: 2021-06-17T16:43:29.747Z
---

Vous trouverez ici des tutoriels qui concerne l'arcade tel que le matériel utilisable, les branchements à faire, etc.



Dans cette suite de tutoriaux nous allons présenter les différents moyens qui existent pour intéragir, "matériellement" parlant, avec RecalBox au travers des connexions exposées par le Raspberry PI.
Nous viserons donc principalement ceux disposant d'un GPIO.
Le but est de s'amuser, dans le cadre de la réalisation d'une borne à :

- Controller le volume au travers d'un bouton.
- Exposer un bouton d'allumage total
- Exposer des boutons d'accès à des raccourcis aux fonctionnalités de RecalBox (screehshot, save, ...)


Ces tutoriaux ne s'adressent pas à des experts en electronique et/ou en développement, on va essayer d'être le plus didactif possible et d'expliquer posément chaque étape que nous allons emprunter dans nos réalisations. Les composants electroniques utilisés dans ces tutoriaux seront décris dans le prochain chapitre.

