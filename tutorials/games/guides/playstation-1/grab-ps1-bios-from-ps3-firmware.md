---
title: Récupérer le bios ps1_rom.bin depuis le firmware PS3
description: 
published: true
date: 2021-10-14T18:58:44.476Z
tags: bios, ps3, ps1
editor: markdown
dateCreated: 2021-10-14T18:56:54.849Z
---

## Qu'est-ce que le bios nommé ps1_rom.bin ?

Ce bios est un fichier qui est fourni dans les mises à jour de la PS3.

Ses avantages :

* Universel, convient pour les jeux de tout origine.
* C'est le seul bios que vous pouvez obtenir légalement sans extraire le bios de votre console originale.

Ce tutoriel va vous indiquer comment obtenir ce bios.

## Comment récupérer ce bios ?

* Vous devez en premier lieu télécharger et décompresser l'émulateur [RCPS3](https://rpcs3.net/), un émulateur de jeux PS3. Celui-ci existe pour Windows et Linux.

* Une fois décompressé, vous devez vous rendre [sur cette page](https://www.playstation.com/fr-fr/support/hardware/ps3/system-software/) et télécharger le firmware PS3.

* Quand vous aurez téléchargé le fichier PS3UPDAT.PUP, ouvrez l'émulateur RCPS3.

* Ouvrez le menu `File` et sélectionnez `Install Firmware`.

![](/tutorials/games/guides/playstation-1/ps1-bios-from-ps3/ps1biosps3-1.png){.full-width}

* Dans la nouvelle fenêtre, cherchez le fichier PS3UPDAT.PUP et ouvrez-le.

![](/tutorials/games/guides/playstation-1/ps1-bios-from-ps3/ps1biosps3-2.png){.full-width}

* Vous devez laisser RCPS3 travailler quelques minutes.

![](/tutorials/games/guides/playstation-1/ps1-bios-from-ps3/ps1biosps3-3.png){.full-width}

* Une fois terminé, allez dans le répertoire où vous avez décompressé RCPS3 et allez dans le dossier `/dev_flash/ps1_emu/` et vous trouverez le fameux fichier BIOS devant vous.

![](/tutorials/games/guides/playstation-1/ps1-bios-from-ps3/ps1biosps3-4.png){.full-width}