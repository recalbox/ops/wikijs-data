---
title: Unable to access to Recalbox from network share
description: 
published: true
date: 2021-08-06T14:55:44.942Z
tags: share, network, access
editor: markdown
dateCreated: 2021-08-06T08:46:04.112Z
---

Sometimes, Windows 10 may not want to display Recalbox on your home network.

## No computers are visible

If you don't see any computers that include Recalbox from the Windows 10 network, several things could be the cause:

* Network discovery is disabled by default and the network configuration is set to "public" 
* The "Make this PC discoverable" feature is set to "Disabled".

You need to go into the network settings and set your network to "private", or set "Make this PC discoverable" to "enabled".

## Recalbox is no longer available since Windows 10 Update #1709

Since Windows 10 Update 1709, you can see your Recalbox in your network, but you can no longer access it.

Windows prevents you from accessing network shares with guest access enabled. And there is no reference to the SMBv1 protocol that has been disabled.

To fix this, you can download and apply [this patch](https://mega.nz/#!rUA3xDgI!a14w9TqQWinLriLANpk7BF_WkoNg8mw6fHloyPEZMPg). This patch will disable security on anonymous network access in Windows, to restore access to your Recalbox.

* [Source 1](https://tech.nicolonsky.ch/windows-10-1709-cannot-access-smb2-share-guest-access/)
* [Source 2](https://support.microsoft.com/fr-fr/help/4046019/guest-access-in-smb2-disabled-by-default-in-windows-10-and-windows-server-2016)

## Other possibilities

* Open the Start menu and go to `Settings` > `Applications` > `Applications and Features` > `Enable or disable Windows features`
* Enable by checking the option `SMB 1.0/CIFS file sharing support/SMB1.0/CIFS client...`.
* Restart Windows.