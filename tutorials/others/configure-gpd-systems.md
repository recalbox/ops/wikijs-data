---
title: Configure GPD systems
description: How to apply certain configurations to make them work
published: true
date: 2022-12-18T10:20:50.523Z
tags: systems, gpd
editor: markdown
dateCreated: 2022-12-04T15:37:20.813Z
---

## Introduction

GPD hardware may not be correctly recognised (e.g. screen not rotated properly, etc.). Nevertheless, there is a procedure to make it compatible depending on the reference of your hardware.

This tutorial applies to the following references:

- GPD Pocket
- GPD Pocket 2
- GPD Pocket 3
- GPD P2 Max
- GPD MicroPC
- GPD Win 2
- GPD Win Max
- Topjoy Falcon

>This procedure is to be applied after **EVERY** update as well as after an installation !
{.is-warning}

## Procedure

### Prerequisites

- Have installed Recalbox on one of the references listed above.
- Be comfortable with SSH commands and file transfer.

### First time installation

1. You need to get a specific script in order to install all the requirements to make your hardware work properly. Go to https://github.com/wimpysworld/umpc-ubuntu, click on the green button and click on `Download ZIP`.

   Unzip the contents, open the `umpc-ubuntu.sh` file and modify the 4th line according to the model you own:

| Model | Value |
| :---: | :---: |
| GPD Pocket | `gpd-pocket` |
| GPD Pocket 2 | `gpd-pocket2` |
| GPD Pocket 3 | `gpd-pocket3` |
| GPD P2 Max | `gpd-p2-max` |
| GPD MicroPC | `gpd-micropc` |
| GPD Win 2 | `gpd-win2` |
| GPD Win Max | `gpd-win-max` |
| Topjoy Falcon | `topjoy-falcon` |

UMPC="value-of-your-model".

   Save the file after editing the corresponding line.

2. Put the unzipped directory (it should be named `umpc-ubuntu-master`) in `/recalbox/share/system/`. You should have `/recalbox/share/system/umpc-ubuntu-master/` with the unpacked files in it.

3. Now you need to connect [via SSH](../system/access/root-access-terminal-cli) to your Recalbox and run the following commands in succession:

```ssh
mount -o remount,rw /
mount -o remount,rw /boot
cd umpc-ubuntu-master
bash umpc-ubuntu.sh enable
```

>There may be an error concerning update-grub. This one is quite normal.
{.is-info}

4. Open the `/boot/EFI/BOOT/grub.cfg` file and on line 9, add this to the end:

```ini
fbcon=rotate:1 video=eDP-1:800x1280 drm.edid_firmware=eDP-1:edid/gpd-win-max-edid.bin
```

>Put a space between `loglevel=0` and `fbcon=rotate:1` !
{.is-warning}

>You can do the same thing on line 15, it will be useful if you select the corresponding option at startup.
{.is-info}

Now you just have to reboot and enjoy your hardware !

### Updating

For each update, you must execute steps 3 and 4 above.