---
title: Libretro uzem
description: 
published: true
date: 2021-10-04T22:41:07.266Z
tags: libretro, uzebox, uzem
editor: markdown
dateCreated: 2021-07-04T18:25:15.428Z
---

**Libretro Uzem** is an official emulator of the Uzebox (an open source 8-bit retro-minimalist game console). The Uzebox is a minimal system based on an AVR ATmega644 microcontroller.

## ![](/emulators/license.svg) License

This core is under [**GPLv3**](https://github.com/Uzebox/uzebox/blob/master/gpl-3.0.txt) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Features

| Feature | Supported |
| :---: | :---: |
| States | ✔ |
| Rewind | ✔ |

## ![](/emulators/bios.svg) BIOS

>**No bios is required.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Supported extensions

Roms must have the extension:

* .uze
* .zip
* .7z

This system supports compressed roms in .zip/.7z format. However, be careful, it is only an archive.

The files contained in the .zip/.7z must correspond to the extensions mentioned above.
Moreover, each .zip/.7z file must contain only one compressed rom.

### Location

Put your roms like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 uzebox
┃ ┃ ┃ ┃ ┣ 🗒 **game.zip**

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>**This core have no option.**
{.is-success}

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/libretro/libretro-uzem/](https://github.com/libretro/libretro-uzem)
* **Official source code**: [https://github.com/Uzebox/uzebox/](https://github.com/Uzebox/uzebox)
* **Official website**: [http://belogic.com/uzebox/index.asp](http://belogic.com/uzebox/index.asp)
* **Official forum**: [http://uzebox.org/forums/](http://uzebox.org/forums/)
* **Official wiki**: [http://uzebox.org/wiki/index.php?title=Hello_World](http://uzebox.org/wiki/index.php?title=Hello_World)