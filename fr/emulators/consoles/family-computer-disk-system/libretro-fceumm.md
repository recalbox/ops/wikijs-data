---
title: Libretro FCEUmm
description: 
published: true
date: 2024-07-24T12:33:50.151Z
tags: libretro, famicom, family computer disk system, fceumm
editor: markdown
dateCreated: 2021-05-21T08:20:29.890Z
---



## ![](/emulators/license.svg) Licence

Ce core est sous licence [**GPLv2**](https://github.com/libretro/libretro-fceumm/blob/master/Copying).

## ![](/emulators/compatibility.png) Compatibilité

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Fonctionnalités

| Fonctionnalité | Supporté |
| :---: | :---: |
| Redémarrage | ✔ |
| Captures d'écran | ✔ |
| Sauvegardes | ✔ |
| Sauvegardes instantanées | ✔ |
| Rembobinage | ✔ |
| Netplay | ✔ |
| Options du core | ✔ |
| RetroAchievements | ✔ |
| Cheats RetroArch | ✔ |
| Cheats natifs | ✔ |
| Controls | ✔ |
| Remapping | ✔ |
| Softpatching | ✔ |

## ![](/emulators/bios.svg) BIOS

### Liste des bios obligatoires

| Nom de fichier | Description | MD5 | Fourni |
| :---: | :---: | :---: | :---: |
| disksys.rom | BIOS Family Computer Disk System | ca30b50f880eb660a320674ed365ef7a | ❌  |

### Emplacement

Placez les bios comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 fds
┃ ┃ ┃ ┃ ┣ 🗒 **disksys.rom**

## ![](/emulators/roms.png) Roms

### Extensions supportées

Les roms doivent avoir les extensions suivantes :

* .fds
* .zip
* .7z

Ce système supporte les roms compressées au format .zip/.7z. Attention toutefois, il ne s'agit que d'une archive.

Les fichiers contenus dans les .zip/.7z doivent correspondre aux extensions citées précédemment.
De plus, chaque fichier .zip/.7z ne doit contenir qu'une seule rom compressée.

### Emplacement

Placez les roms comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 fds
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.zip**

>Les roms au format **No-Intro** sont vivement conseillées.
{.is-success}

>Pour plus d'information sur les roms, rendez-vous sur [ce tutoriel](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuration avancée de l'émulateur

>Pour pouvoir conserver vos configurations personnalisées lors d'une mise à jour, nous vous conseillons d'utiliser notre fonctionnalité [Surcharges de configuration](./../../../advanced-usage/configuration-overload).
{.is-info}

### Accéder aux options

Vous pouvez configurer diverses options de deux façons différentes.

* Via le menu RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via le fichier `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Options du core

| Options | Valeur des options | Variable (pour surcharge) | Valeurs des variables |
| --- | --- | --- | --- |
| Région | `Auto` ✅ / `NTSC` / `PAL` / `Dendy` | `fceumm_region` | `Auto` / `NTSC` / `PAL` / `Dendy` |
| Module Game Genie (Redémarrage requis) | `Désactivé` ✅ / `Activé` | `fceumm_game_genie` | `disabled` / `enabled` |
| Rapport d'aspect | `Rapport d'aspect des pixels 8:7 (PAR)` ✅ / `4:3` / `Pixels parfaits à l'échelle` | `fceumm_aspect` | `8:7 PAR` / `4:3` / `PP` |
| Crop Horizontal Left Overscan | `Désactivé` ✅ / `Activé` | `fceumm_overscan_h_left` | `disabled` / `enabled` |
| Crop Horizontal Right Overscan | `Désactivé` ✅ / `Activé` | `fceumm_overscan_h_right` | `disabled` / `enabled` |
| Crop Vertical Top Overscan | `Désactivé` ✅ / `Activé` | `fceumm_overscan_v_top` | `disabled` / `enabled` |
| Crop Vertical Bottom Overscan | `Désactivé` ✅ / `Activé` | `fceumm_overscan_v_bottom` | `disabled` / `enabled` |
| Palette de couleurs | `Par défaut` ✅ / `Réelle par AspiringSquire` / `Console virtuelle Nintendo` / `PPU RVB Nintendo` / `YUV-V3 de FBX` / `Finale non saturée de FBX` / `Sony CXA2025AS US` / `PAL` / `Finale 2 de BMF` / `Finale 3 de BMF` / `Lisse de FBX` / `Composite directe de FBX` / `Style PVM D93 de FBX` / `Matériel NTSC de FBX` / `FBX's NES Classic (fixed)` / `NESCAP de RGBSource` / `Wavebeam de nakedarthur` / `FBX's Digital Prime` / `FBX's Magnum` / `FBX's Smooth V2` / `FBX's NES Classic` / `Données brutes` / `Personnalisée` | `fceumm_palette` | `default` / `asqrealc` / `nintendo-vc` / `rgb` / `yuv-v3` / `unsaturated-final` / `sony-cxa2025as-us` / `pal` / `bmf-final2` / `bmf-final3` / `smooth-fbx` / `composite-direct-fbx` / `pvm-style-d93-fbx` / `ntsc-hardware-fbx` / `nes-classic-fbx-fs` / `nescap` / `wavebeam` / `dogotal-prime-fbx` / `magnum-fbx` / `smooth-v2-fbx` / `nes-classic-fbx` / `raw` / `custom` |
| Filtre NTSC | `Désactivé` ✅ / `Composite` / `S-Video` / `RGB` / `Monochrome` | `fceumm_ntsc_filter` | `disabled` / `composite` / `svideo` / `rgb` / `monochrome` |
| Qualité audio | `Basse` ✅ / `Élevée` / `Très élevée` | `fceumm_sndquality` | `Low` / `High` / `Very High` |
| Audio RF Filter | `Désactivé` ✅ / `Activé` | `fceumm_sndlowpass` | `disabled` / `enabled` |
| Stereo Sound Effect | `Désactivé` ✅ / `1ms Delay` / `2ms Delay` / `3ms Delay` / `4ms Delay` / `5ms Delay` / `6ms Delay` / `7ms Delay` / `8ms Delay` / `9ms Delay` / `10ms Delay` / `11ms Delay` / `12ms Delay` / `13ms Delay` / `14ms Delay` / `15ms Delay` / `16ms Delay` / `17ms Delay` / `18ms Delay` / `19ms Delay` / `20ms Delay` / `21ms Delay` / `22ms Delay` / `23ms Delay` / `24ms Delay` / `25ms Delay` / `26ms Delay` / `27ms Delay` / `28ms Delay` / `29ms Delay` / `30ms Delay` / `31ms Delay` / `32ms Delay` | `fceumm_sndstereodelay` | `disabled` / `01_ms_delay` / `02_ms_delay` / `03_ms_delay` / `04_ms_delay` / `05_ms_delay` / `06_ms_delay` / `07_ms_delay` / `08_ms_delay` / `09_ms_delay` / `10_ms_delay` / `11_ms_delay` / `12_ms_delay` / `13_ms_delay` / `14_ms_delay` / `15_ms_delay` / `16_ms_delay` / `17_ms_delay` / `18_ms_delay` / `19_ms_delay` / `20_ms_delay` / `21_ms_delay` / `22_ms_delay` / `23_ms_delay` / `24_ms_delay` / `25_ms_delay` / `26_ms_delay` / `27_ms_delay` / `28_ms_delay` / `29_ms_delay` / `30_ms_delay` / `31_ms_delay` / `32_ms_delay` |
| Permuter les cycles de service | `Désactivé` ✅ / `Activé` | `fceumm_swapduty` | `disabled` / `enabled` |
| Volume principal | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` ✅ / `8` / `9` / `10` | `fceumm_sndvolume` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` |
| Canal 1 (carré 1) | `Activé` ✅ / `Désactivé` | `fceumm_apu_1` | `enabled` / `disabled` |
| Canal 2 (carré 2) | `Activé` ✅ / `Désactivé` | `fceumm_apu_2` | `enabled` / `disabled` |
| Canal 3 (triangle) | `Activé` ✅ / `Désactivé` | `fceumm_apu_3` | `enabled` / `disabled` |
| Canal 4 (bruit) | `Activé` ✅ / `Désactivé` | `fceumm_apu_4` | `enabled` / `disabled` |
| Canal 5 (PCM) | `Activé` ✅ / `Désactivé` | `fceumm_apu_5` | `enabled` / `disabled` |
| Activer le turbo | `Aucun` ✅ / `Joueur 1` / `Joueur 2` / `Les deux` | `fceumm_turbo_enable` | `None` / `Player 1` / `Player 2` / `Both` |
| Délai du turbo (en images) | `1` / `2` / `3` ✅ / `5` / `10` / `15` / `30` / `60` | `fceumm_turbo_delay` | `1` / `2` / `3` / `5` / `10` / `15` / `30` / `60` |
| Mode Zapper | `Crosshair light gun` ✅ / `Sequential Targets light gun` / `Écran tactile` / `Souris` | `fceumm_zapper_mode` | `clightgun` / `stlightgun` / `touchscreen` / `mouse` |
| Afficher le viseur du Zapper | `Activé` ✅ / `Désactivé` | `fceumm_show_crosshair` | `enabled` / `disabled` |
| Tolérance du Zapper | `0` / `1` / `2` / `3` / `4` / `5` / `6` ✅ / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` | `fceumm_zapper_tolerance` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` |
| Invert Zapper Trigger Signal | `Activé` ✅ / `Désactivé` | `fceumm_zapper_trigger` | `enabled` / `disabled` |
| Invert Zapper Sensor Signal | `Activé` ✅ / `Désactivé` | `fceumm_zapper_sensor` | `enabled` / `disabled` |
| Arkanoid Mode | `Absolute mouse` / `Mouse` ✅ / `Stelladaptor` / `Touchscreen` | `fceumm_arkanoid_mode` | `abs_mouse` / `mouse` / `stelladaptor` / `touchscreen` |
| Mouse Sensitivity | `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ / `110` / `120` / `130` / `140` / `150` / `160` / `170` / `180` / `190` / `200` | `fceumm_mouse_sensitivity` | `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` / `110` / `120` / `130` / `140` / `150` / `160` / `170` / `180` / `190` / `200` |
| Autoriser les directions opposées | `Désactivé` ✅ / `Activé` | `fceumm_up_down_allowed` | `disabled` / `enabled` |
| Aucune limite de sprites | `Désactivé` ✅ / `Activé` | `fceumm_nospritelimit` | `disabled` / `enabled` |
| Overclock | `Désactivé` ✅ / `Post-rendu x2` / `Rafraîchissement vertical x2` | `fceumm_overclocking` | `disabled` / `2x-Postrender` / `2x-VBlank` |
| Remplir la RAM lors de la mise sous tension (Redémarrage requis) | `$FF` ✅ / `$00` / `Aléatoire` | `fceumm_ramstate` | `fill $ff` / `fill $00` / `random` |

## ![](/emulators/external-links.png) Liens externes

* **Code source utilisé** : [https://github.com/libretro/libretro-fceumm/](https://github.com/libretro/libretro-fceumm/)
* **Documentation Libretro** : [https://docs.libretro.com/library/fceumm/](https://docs.libretro.com/library/fceumm/)