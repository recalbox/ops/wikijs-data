---
title: Glossar
description: Liste der Begriffe, die in Verbindung mit Recalbox stehen.
published: true
date: 2021-06-26T10:18:50.201Z
tags: 
editor: markdown
dateCreated: 2021-06-22T09:17:32.949Z
---

In diesem Abschnitt findest du Definitionen für Fachbegriffe im Zusammenhang mit Recalbox. Wenn es ein Wort gibt, das du nicht verstehst, sollte diese Seite dir weiterhelfen.

## A

* **Arcade**: Als Arcade werden im Allgemeinen Videospielmaschinen bezeichnet, bei denen man Münzen einwirft, um Credits zu erhalten. Aufgrund ihrer Beschaffenheit (eine große Anzahl verschiedener Maschinen, wohingegen Konsolen in der Regel eine begrenzte Hardware haben, auf der eine große Anzahl von Spielen läuft), ist es wichtig zu verstehen, wie man die zugehörigen Emulatoren zum Laufen bringt, da das Einfügen von Dateien, ohne genau zu wissen was man tut, normalerweise nicht funktioniert. Außerdem werden Neogeo-Spiele im Kontext von Recalbox auf die gleiche Weise emuliert wie Arcade-Titel.
* **API**: Schnittstelle zur Anwendungsprogrammierung.

## B

* **Betriebssystem**: Auch **OS** genannt, ist es ein Satz von Programmen, der die Nutzung der Ressourcen eines Computers durch Anwendungssoftware steuert.
* **Big-Endian**: ist die gängigste Art, [binäre Dateien](https://de.wikipedia.org/wiki/Bin%C3%A4rdatei) zu speichern . Er setzt den höchstwertigen (oder größten) Wert an die erste Stelle, gefolgt von weniger bedeutenden Werten. Beispielsweise wird bei der Big-Endian-Darstellung der [Ganzzahl](https://de.wikipedia.org/wiki/Integer_(Datentyp)) 123 zuerst der Hunderterwert (1), dann der Zehnerwert (2) und schließlich der Einserwert (3) oder eben [123] dargestellt.
* **BIOS**: Dateien, die vom Originalsystem gelesene Programme darstellen und von einigen Emulatoren benötigt werden, um zu funktionieren.
* **Buildroot**: Ein Satz von Makefiles und Patches, der den Prozess der Erstellung einer vollständigen, bootfähigen Linux-Umgebung für ein Embedded-System vereinfacht und automatisiert. Gleichzeitig wird Cross-Compiling eingesetzt, um die Erstellung mehrerer Zielplattformen auf einem einzigen Linux-basierten Entwicklungssystem zu ermöglichen. Buildroot kann automatisch die erforderliche Cross-Compile-Toolchain erstellen, ein Root-Dateisystem erstellen, ein Linux-Kernel-Image kompilieren und einen Bootloader für das eingebettete Zielsystem generieren oder eine beliebige unabhängige Kombination dieser Schritte durchführen. Zum Beispiel kann eine bereits installierte Cross-Compiler-Toolchain unabhängig verwendet werden, während Buildroot nur das Root-Dateisystem erstellt.

## C

* **Changelog**: Textdatei, die die Änderungen für jede Version einer Software enthält. Die Dateien [changelog](https://gitlab.com/recalbox/recalbox/raw/master/CHANGELOG.md) und [release-notes](https://gitlab.com/recalbox/recalbox/raw/master/RELEASE-NOTES.md) sind für jeden zugänglich.
* **CHD**: "Compressed Hunks of Data". Disk-Image von Arcade-Spielen, das von einigen MAME-Spielen verwendet wird und eine komprimierte Version des gesamten Inhalts der Festplatte einer Arcade-Maschine beinhaltet.
* **Cheats**: Cheats ermöglichen es, das Verhalten einiger Cheating-Geräte der Originalsysteme zu reproduzieren und erlauben es, vorher eingegebene Codes zu aktivieren oder zu deaktivieren, die das Spielverhalten verändern.
* **Checksum** _(oder Prüfsumme)_: UEine Zeichenkette, die aus dem Inhalt einer Datei berechnet wird. Wenn zwei Dateien der gleichen Größe die gleiche Prüfsumme haben, sind sie wahrscheinlich identisch. Wird hauptsächlich für Netplay verwendet.
* **Core**: Ein Programm, das von einem Emulator (normalerweise RetroArch) geladen wird.
* **CRC**: Eine Art von Prüfsumme, die für Netplay verwendet wird.
* **Cue** _(oder .cue)_ : Dateien, die beschreiben, wie Rohdaten auf einem Datenträger angeordnet sind. Hauptsächlich für CD-basierte Konsolen und nur hilfreich im Zusammenspiel mit bin-Dateien.

## D

* **Dat** _(oder .dat)_: Dateien, die verwendet werden, um mehrere ROMs zu prüfen, um sicherzustellen, dass sie zu einem bestimmten Satz gehören. Hauptsächlich verwendet für Arcade und Netplay. 
* **Debugging**: Fixing a computer problem caused by a bug, i.e. a malfunction of a computer program, which prevents the proper functioning of an application, software, etc.
* **Demo Mode**: Eine Recalbox-Funktion, die es EmulationStation ermöglicht, Spiele nach einer bestimmten Zeit der Inaktivität zufällig zu starten und diese als eine Art Bildchirmschoner zu verwenden.
* **DragonBlaze**: Name der Recalbox-Version 6.0.

## E

* **Endianness** _(oder Byte-Reihenfolge)_: In der Computertechnik ein Begriff, der die Speicherorganisation für einfache Zahlenwerte, in erster Linie die Ablage ganzzahliger Werte (Integer) im Arbeitsspeicher, beschreibt.
* **EmulationStation** _(oder ES)_: Das in Recalbox verwendete Front-End (oder die Schnittstelle zur Verwaltung und zum Zugriff auf Spiele).
* **Emulator**: Programm, das das Verhalten einer bestimmten Hardware imitiert.

## F

* **Front-End**:

## G

* **Gamelist** _(oder gamelist.xml)_: Datei, die für jedes emulierte System spezifisch ist und Informationen für die einzelnen Spiele beinhaltet..
* **Gitlab**: 
* **GPI**:
* **GPIO** _(oder General Purpose Input/Output)_ : Kontakte, die auf einem Raspberry Pi zur Verfügung stehen und für verschiedene Zwecke verwendet werden können (im Falle von Recalbox hauptsächlich zum Anbringen von Tasten und anderen Bedienelementen).

## H

* **Hash**: Funktionalität zur Berechnung der Prüfsummen der ROMs für Netplay.
* **Hotkey**: Taste für Tastenkombinationen, die von den Emulatoren als Shortcuts für Funktionen übersetzt werden.

## I

* **Interface**:
* **ISO** _(oder .iso)_: Image-Dateien von einem physischen Datenträger.
* **Issue**: Im Fall von Gitlab stellt ein Issue einen Fehlerbericht oder einen Feature Request dar. Es ist möglich, alle Issues einzusehen oder eines zu erstellen, indem Du [diesem Link](https://gitlab.com/recalbox/recalbox/-/issues) folgst. 

## K

* **Kodi**: In Recalbox enthaltener Media Player.

## L

* **Libretro**: Von Retroarch verwendete plattformübergreifende API.
* **Lizenz**: Eine Lizenz definiert, was mit einer Software gemacht werden darf und was nicht. Es ist möglich, die Lizenz von Recalbox [hier](https://gitlab.com/recalbox/recalbox/-/blob/master/LICENSE.md) einzusehen. Es sollte auch beachtet werden, dass Recalbox mit anderer Software geliefert wird, die ebenfalls ihrer eigenen Lizenz unterliegt.
* **Link-Kabel**: Ein Kabel, mit dem zwei tragbare Systeme miteinander verbunden werden können, um die Multiplayer-Funktionen für unterstützte Spiele zu nutzen; einige Emulatoren können dieses Verhalten imitieren.
* **Little Endian**: speichert den niedrigstwertigen Wert zuerst, gefolgt von immer höherwertigen Werten. Zum Beispiel ist die Zahl 123 in Little-Endian-Darstellung [321]. Der String "ABC" wird also als [CBA] dargestellt.

## M

* **MAME**: Multiple Arcade Machine Emulator ist ein kostenloser Open-Source-Emulator, der entwickelt wurde, um die Hardware von Arcade-Spielsystemen als Software auf modernen Computern und anderen Plattformen nachzubilden.
* **MD5**: eine Art von Prüfsumme, die für BIOS-Dateien verwendet wird.
* **MITM**: _(oder Man In The Middle)_: Einstellung, die für Netplay verwendet wird, wobei ein Remote-Server zur Synchronisierung der Spiele anstelle einer direkten Verbindung verwendet wird, was allerdings mit einem Verlust an Zuverlässigkeit und Geschwindigkeit einhergeht.
* **Multitap**: Gerät, an das mehr Controller angeschlossen werden können, als es Controller-Ports an einer Konsole gibt, wodurch ein 4-Spieler-Modus auf Systemen mit nur zwei Original-Controller-Ports für unterstützte Spiele möglich ist; einige Emulatoren können dieses Verhalten imitieren.

## N

* **Netplay**: Eine Technologie, die es erlaubt, im Netzwerk zu spielen. Im Fall von Recalbox ermöglicht es dir, Retro-Spiele online zu spielen.
* **No-intro**: Name einer Gruppe, die Roms referenziert, sich auf Cartridge-Spiele spezialisiert und die Vision hat, nur die den Originalen am nächsten kommenden Dumps zu behalten. **Referenz-Romsets** für Systeme mit **Cartridges**, hauptsächlich **für Netplay** verwendet.
* **NOOBS **: (New Out Of the Box Software) hat einen Namen, der abwertend klingen mag, aber dieses Dienstprogramm ermöglicht es dir, ein Betriebssystem zuverlässig und schnell aus einer großen Auswahl zu installieren. NOOBS ist eigentlich nur ein einfacher Betriebssystem-Installationsmanager für den Raspberry Pi. Er wurde entwickelt, um diese Aufgabe so weit wie möglich zu vereinfachen, indem er es ermöglicht, ein Betriebssystem mit nur ein paar Klicks zu installieren.

## O

* **OCR**: Die **optische Zeichenerkennung** (OCR) bezieht sich auf computergestützte Verfahren zur Übersetzung von Bildern von gedrucktem oder getipptem Text in Textdateien.
* Open-Source**: Die Bezeichnung **Open-Source** oder "Open-Source-Code**" gilt für Software, deren [Lizenz](https://en.wikipedia.org/wiki/Software_license) die von der [Open-Source-Initiative](https://de.wikipedia.org/wiki/Open_Source_Initiative) genau festgelegten Kriterien respektiert, d.h. die Möglichkeiten der freien Weitergabe, des Zugriffs auf den [Quellcode](https://de.wikipedia.org/wiki/Quelltext) und der Erstellung von abgeleiteten Werken. Dieser Quellcode wird der Allgemeinheit zur Verfügung gestellt und ist in der Regel das Ergebnis der Zusammenarbeit von Programmierern.
* **OS**: Siehe **Betriebssystem**.
* **Overlays**: Bild, das über dem Spiel angezeigt wird und hauptsächlich aus kosmetischen Gründen verwendet wird.
* **Override**: Dateien, die das Überschreiben der Standardkonfiguration für bestimmte Datensätze oder Ordner ermöglichen, siehe [Override](./advanced-usage/configuration-override/).

## P

* **PBP** _(oder .pbp)_ : Image-Dateien, die physischen UMDs der Playstation Portable oder Multi-Disc-Spielen der Playstation entsprechen.
* **Pi**: Siehe **Raspberry Pi**.
* **Plug and Play**: Auch bekannt unter dem Akronym **PnP**, steht es für " Verbinden und Spielen" oder " Einstecken und Benutzen". Dabei handelt es sich um eine Technologie, die es Betriebssystemen ermöglicht, kompatible Geräte schnell und automatisch zu erkennen, sobald sie eingesteckt werden, ohne den Computer neu zu starten. Plug and Play ermöglicht die Implementierung mit minimalen Benutzereingriffen, ohne Installation von dedizierter Software, und minimiert somit Handhabungs- und Einrichtungsfehler.

## R

* **Raspberry Pi**: 
* **Rebuild**: 
* **Recalbox**: Das Betriebssystem selbst, basierend auf Buildroot System.
* **Recalbox.conf**: Datei, die die meisten Einstellungen von Recalbox und den Emulatoren enthält, siehe dazu["recalbox.conf"](./basic-usage/getting-started/recalboxconf-file).
* **Redump** _(auch Redump.org)_: Redump.org ist eine Disc-Konservierungsdatenbank und Internet-Community, die sich dem Sammeln präziser und genauer Informationen über jedes Videospiel widmet, das jemals auf optischen Medien eines beliebigen Systems veröffentlicht wurde..
* **Reloaded**: Name der Version 7 von Recalbox.
* **RetroAchievements**: Ein Dienst zur Aktivierung eines Trophäen-/Erfolgssystems für unterstützte Spiele und Emulatoren.
* **RetroArch**: Das Libretro-Frontend, das von einigen Emulatoren in Recalbox verwendet wird, siehe [RetroArch](./advanced-usage/retroarch).
* **Rewind**: Eine Einstellung, die es erlaubt, in einigen Emulatoren zurückzuspulen, im Tausch gegen einen Verlust an Leistung.
* **ROMs**: Image-Dateien physischer Spiele.
* **ROMset**: Ein spezieller Satz von ROMs, der hauptsächlich für Netplay und Arcade verwendet wird.

## S

* **Savestates**: Eine Funktion, die es ermöglicht, den exakten Zustand des emulierten Systems zu speichern, so dass ein komplettes Speichern eines Spiels jederzeit möglich ist, auch wenn das Spiel ursprünglich keine Spielstände unterstützt. Es ist wichtig zu beachten, dass traditionelle Spielstände und Savestates in Konflikt geraten können.
* **Samba**: SMB (Server Message Block), die Freigabe von Ordnern und Druckern in einem lokalen Netzwerk ist eine Funktion moderner Betriebssysteme, die den Zugriff auf Ressourcen auf einem Computer (Datenordner und Drucker) von einem anderen Computer aus ermöglicht, der sich im selben lokalen Netzwerk (Heim- oder Firmennetzwerk) befindet.
* **Scraper**: Programm, das in der Lage ist, Spielinformationen aus dem Internet zu holen und sie in einem Format zu speichern, das von der EmulationStation interpretiert werden kann.
* **Shaders**: Filter, die das visuelle Rendering eines Spiels auf Kosten einer verringerten Leistung verändern können und hauptsächlich aus kosmetischen Gründen verwendet werden.
* **Share**: Recalbox-Partition, die über das Netzwerk erreichbar ist.
* **Prüfsumme**: Siehe _Checksum_
* **SSH**:
* **Support-Archiv**: Webmanager-Option, die es ermöglicht, einen Debugging-Link zu generieren, der für Entwickler im Falle von Problemen nützlich ist.


## T

* **Themes**: Dateien, die das gesamte Erscheinungsbild von EmulationStation definieren.

## U

* **UMD**:

## V

* **Verbose**: Für Recalbox ist dies ein Boot-Modus, der bei einigen Architekturen Debugging-Informationen anzeigt, die nützlich sind, um zu verstehen, warum das System nicht korrekt bootet.

## W

* **Webmanager**: Eine Recalbox-fähige Netzwerkschnittstelle für einfache Einrichtung, Dateiverwaltung, BIOS-Überprüfung und mehr.

## X

* **Xin-Mo**:

## Y

* 

## Z

* 