---
title: 5. 🛠️ EMULADORES
description: 
published: true
date: 2024-07-26T07:35:05.030Z
tags: emuladores
editor: markdown
dateCreated: 2023-08-31T12:52:16.382Z
---

Esta sección es la sección más sofisticada de toda la documentación y trata de la descripción e todos los sistemas de juegos incluidos en Recalbox:

* [Arcade](arcade)
* [Consolas](consoles)
* [Fantasy](fantasy)
* [Motores de juegos](engines)
* [Ordenadores](computers)
* [Ports](ports)
* [Portátiles](handhelds)

Aquí tenéis la [lista de juegos incluídos](included-games) en la última versión de Recalbox.