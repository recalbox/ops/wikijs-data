---
title: Mandos compatibles
description: ¿Cuales son los mandos que han sido probados y son compatibles con Recalbox?
published: true
date: 2025-02-07T10:30:31.102Z
tags: compatibilidad, controladores, mandos
editor: markdown
dateCreated: 2023-09-19T07:04:37.058Z
---

Aquí tenéis una lista de mandos que han sido probados y aprobados por la comunidad Recalbox.

>Es **muy poco probable** que los mandos que no sean 8bitdo o los mandos no oficiales de los fabricantes de videoconsolas sean compatibles, si es que lo son. Es preferible utilizar mandos oficiales o de 8bitdo.
{.is-warning}

## Mandos oficiales de consolas

| Periférico | Con cable | Sin cable | Última versión probada | Comentarios |
| :---: | :---: | :---: | :---: | :---: |
| **MICROSOFT** |  |  |  |  |
| Mando Xbox de la Xbox originale | ✅ |  |  | ✅ Con modificación USB |
| Mando Xbox 360 | ✅ | ☑️ | 8.0.0 | ☑️ Con el dongle Microsoft Xbox 360 |
| Mando Xbox One | ✅ | ☑️ | 8.0.0 | ☑️ Con un dongle : [adaptador sin cables Xbox](https://www.xbox.com/fr-FR/accessories/adapters/wireless-adapter-windows), 8BitDo (esperar 5s), Janswall,... |
| Mando Xbox One Elite | ✅ |  | 7.1.1 |  |
| Mando Xbox One S | ✅ | ☑️ | 9.1.0 | ☑️ No en BT, sólo con dongle : [adaptador sin cables Xbox](https://www.xbox.com/fr-FR/accessories/adapters/wireless-adapter-windows) |
| Mando Xbox One Series X\|S | ✅ | ☑️ | 9.1.0 | ☑️ No en BT, sólo con dongle : [adaptador sin cables Xbox](https://www.xbox.com/fr-FR/accessories/adapters/wireless-adapter-windows) | 
| **NINTENDO** |  |  |  |  |
| Mando GameCube | ✅ | ☑️ |  | ✅ Adaptador Mayflash Mayflash GameCube para PC USB ☑️ Con un dongle: 8BitDo GBros Adaptator |
| Mando GameCube WaveBird | ❌ | ☑️ | 8.0.0 | ☑️ Con un dongle: 8BitDo GBros Adaptator (con un cable de extensión GameCube) o [un adaptador de mandos GameCube a USB](https://www.raphnet-tech.com/products/gc_usb_adapter_gen3/index_fr.php) |
| Mando NES Mini | ✅ | ☑️ |  | ✅ Con el convertidor Mayflash USB ☑️ Con un dongle : 8BitDo GBros Adaptator |
| SNES Mini Controller | ✅ | ☑️ |  | ✅ Con el convertidor Mayflash USB ☑️ Con un dongle : 8BitDo GBros Adaptator |
| JoyCon |  | ☑️ | 8.0.0 |  |
| Mando Switch Pro Controller | ✅ | ☑️ | 8.0.0 | ✅ Pulsad ZL + ZR depués de la detección |
| Mando Wii Classic Controller | ✅ | ☑️ | v4.1 | ✅ Con el convertidor Mayflash USB ☑️ Nativamente & con el dongle 8BitDo GBros Adaptator |
| Mando Wii Classic Pro | ✅ | ☑️ |  | ✅ Con el convertidor Mayflash USB ☑️ Con un dongle : 8BitDo GBros Adaptator |
| Wiimote | ✅ | ☑️ | 9.1.0 | ✅ Con la barra Mayflash Dolphin Bar |
| Wii U Pro Controller | ❌ | ☑️ | 4.1 | Nativamente & con un dongle : 8BitDo, Janswall,...|
| Mando Super Nintendo Entertainment System for Nintendo Switch | ❌ | ☑️ | 8.0.0 |  |
| Mando Nintendo 64 for Nintendo Switch | ❌ | ☑️ | 9.2 | No soportado por los drivers Linux |
| Mando Sega MegaDrive for Nintendo Switch | ❌ | ❌ | 8.0.0 | No soportado por los drivers Linux |
| PDP Mando Afterglow sin cable Nintendo Switch |  | ☑️ | 8.0.0 |  |
| **SNK** |  |  |&nbsp; |&nbsp; |
| Mando NeoGeo Mini | ❌ |  |  | Algunos problemas con un adaptador USB-C |
| **SONY** |  |  |  |  |
| Mando PlayStation 1 | ✅ |  | v 3.3beta 17 & v4.1.0-dev | Con adaptador genérico USB para 2 PS1/PS2 PAD controllers |
| Mando PlayStation 1 Dualshock | ✅ |  | v 3.3beta 17 & v4.1.0-dev | Con adaptador genérico USB para 2 PS1/PS2 PAD controllers |
| Mando PlayStation 2 Dualshock 2 | ✅ |  | v 3.3beta 17 & v4.1.0-dev | Con adaptador genérico USB para 2 PS1/PS2 PAD controllers |
| PlayStation 3 DualShock 3 | ✅ | ☑️ | 7.0 | ☑️ Con un dongle: 8BitDo, Janswall,... o cualquier otro dongle Bluetooth |
| PlayStation 4 DualShock 4 | ✅ | ☑️ | 7.0.1 | ☑️ Con el dongle oficial Sony PS4 & otros dongles : 8BitDo, Janswall,... |
| PlayStation 5 DualSense | ✅ |  | 7.0.1 | ✅ Con [esto](https://forum.recalbox.com/topic/8920/a-lire-manettes-ps4). |
| Playstation Classic Mini Controller | ✅ |  |  |  |

## Mandos

| Periférico | Estado | Última versión probada | Comentarios |
| :---: | :---: | :---: | :---: | :---: |
| Colleciones de mandos de 8bitdo | ✅ | 8.0.0 | Consultad la página [8bitdo en Recalbox](./../../hardware-compatibility/compatible-devices/8bitdo-on-recalbox) |
|  |  |  |  |
| **Bigben Interactive** |  |  |  |
| Pad Luxe PS3 | ✅ |  |  |
| PlayStation 3 BB4259 | ✅ |  |  |
| PlayStation 3 BB4401 | ✅ |  |  |
| Big Ben BB5033 (avec dongle USB 2.4GHz RF) | ✅ | v17.11.10.2 |  |
| **Bowick** |  |  |  |
| Wireless Bluetooth Controllers for PS3 | ✅ | v4.0.2 | Seleccionad `shawan` en la configuración |
| **BUFFALO** |  |  |  |
| iBuffalo SNES Classic USB Gamepad | ✅ |  | Los dos botones especiales `Turbo` y `Clear` funcionan |
| **CSL-Computer** |  |  |  |
| Gamepad USB SNESw | ✅ |  |  |
| USB Wireless Gamepad (Model 2016) | ✅ |  |  |
| **EasySMX** |  |  |  |
| EMS-9100 Gamepad USB Wired | ✅ |  | Tiene un botón Home para la configuración. Soporta Android 4.0 o superior. |
| **Free** |  |  |  |
| Freebox Controller | ✅ |  | Modo analógico |
 | **Funlab / Palpow** |  |  |  |
 | Firefly | ✅ |  |  Con cable y BT bien. Los 2 botones especiales `Turbo` y `Macro` funcionan |
| **Gamesir** |  |  |  |
| Gamesir G3s | ✅ | v4.0.2 | El mando se configura desde los menus de EmulationStation pero no funciona bien al jugar. |
| **HORI** |  |  |  |
| Fighting Commander | ✅ | v4.0.2 |  |
| Pokken Tournament for Wii U | ✅ | v4.0.2 |  |
| **Kreema** |  |  |  |
| Gen Game S3 |  | v18.04.20 | No funciona bien con la conexión Bluetooth (Wireless Bluetooth 3.0) |
| Gen Game S5 | ❌ | 17.13.02 | Se puede emparejar pero actúa de forma extraña durante la configuración. Se reconoce como teclado si se pulsan los 2 sticks analógicos. En configuración, cada botón pulsado se duplica. No funciona en el juego, sólo en el menú principal. |
| **iNNEXT** |  |  |  |
| IN-Y-D002-024\*2 | ✅ | v4.0.2 |  |
| **IPEGA** |  |  |  |
| PG-9228 | ✅ | 8.1.1 | Emparejamiento con Home + B |
| **Konix** |  |  |  |
| Konix PS3/PC | ✅ |  |  |
| KONIX KX-CT-PC | ✅ | v4.0.2 |  |
| **KROM** |  |  |  |
| Kay Pro Gaming Wired Gamepad (NXROMKEY) | ✅ |  | Detectado como un mando Xbox 360, pero imposible configurarlo. |
| **Logitech** |  |  |  |
| Cordless Rumbleepad 2 (con dongle USB 2.4GHz RF) | ✅ |  |  |
| Dual Action Gamepad (USB) | ✅ | v4.0.0-beta4 |  |
| Precision Gamepad (USB) | ✅ | v4.0.0-beta4 |  |
| Rumblepad 2 (USB) | ✅ |  |  |
| **Logitech G** |  |  |  |
| F310 Gamepad (USB) | ✅ |  |  |
| F710 Wirelesss Gamepad (con un dongle USB 2.4GHz RF) | ✅ | v4.0.0-beta4 | Seleccionad el modo de entrada D, no el X. |
| **MadCatz** |  |  |  |
| MadCatz Wireless FightPad for PlayStation 3 (con un dongle USB 2.4GHz RF) | ✅ |  |  |
| **Nacon** |  |  |  |
| GC-400ES Wired Controller | ✅ | 6.1.1 | [Vendeur](https://www.nacongaming.com/controllers/) (testeado con las versiones 1, 2 y 3 ; reconozido como un mando Xbox genérico) |
| Revolution Wired | ✅ | v6.1.1 | [Vendeur](https://nacongaming.com/controllers/) (testeado con las versiones 1, 2 y 3 ; reconozido como un mando Xbox genérico) |
| Nacon esport controllers/revolution wired et GC-400ES | ✅ |  | [Vendeur](https://www.nacongaming.com/controllers/) (ttesteado con las versiones 1, 2 y 3 ; reconozido como un mando Xbox genérico) |
| **NGS** |  |  |  |
| Maverick (PS3/PC) | ✅ |  |  |
| Nvidia | ✅ | 9.2.2 | V01.04 |
| Shield 2017 (USB) | ❌ |  | Funciona en EmulationStation pero los juegos tardan en responder. |
| **PDP** |  |  |  |
| PlayStation 3 Rock Candy (con el dongle Bluetooth) | ✅ |  |  |
| Xbox One & Windows 10 | ❌ | v18.04.20 | Detectado pero no puede configurarse. |
| PDP Afterglow sans fil Nintendo Switch | ✅ | 8.0.2 |  |
| PDP Afterglow Xbox 360 | ✅ | 9.1.0 | con cable vía un adaptador USB, mando translucido con leds verdes o azules |
| **PowerA** |  |  |  |
| MOGA PRO Power | ✅ | v17.11.02 |  |
| **Razer** |  |  |  |
| Razer Sabertooth | ✅ | v4.0.0-beta2 |  |
| **Retro Games** |  |  |  |
| The Amiga 500 Mini Gamepad | ✅ | 8.1.0 |  |
| **RetroUSB** |  |  |  |
| USB Super RetroPad (SNES) | ✅ |  |  |
| **Saitek** |  |  |  |
| P380 | ✅ |  |  |
| P480 | ✅ |  |  |
| P880 | ✅ |  |  |
| **SlickBlue** |  |  |  |
| SlickBlue Wireless Bluetooth Game Pad Controller para la Sony PlayStation 3 | ❌ |  |  |
| **Snakebyte** |  |  |  |
| idroid:con (avec le dongle Bluetooth) | ✅ | v3?3beta17 |  |
| blu:con (avec le dongle Bluetooth) | ❌ | v4.0-rpi2-build15 |  |
| **Speedlink** |  |  |  |
| Speedlink Strike FX - PS3 (câble USB) | ❌ | v4.0.0-rpi2B |  |
| Speedlink Torid Wireless Gamepad | ✅ | v4.0.0-rpi3 |  |
| **SteelSeries** |  |  |  |
| SleetSeries Free Mobile Wireless | ✅ | v4.0.0-rpi3 | Dongle Bluetootth obligatorio |
| SteelSeries Stratus Wireless Gaming Controller | ❌ | v4.0.0-rpi3B | Se puede emparejar por Bluetooth pero a continuación no es detectado. |
| **Subsonic** |  |  |  |
| Subsonic Bluetooth Controller PS3 | ✅ |  | (driver gasia) |
| **ThrustMaster** |  |  |  |
| T-Wireless | ✅ | v17.11.10.2 |  |
| **VZTEC** |  |  |  |
| VZTEC USB Double Shock Controller Game Pad Joystick(VZ-GA6002) | ✅ | v4.0.0-beta3 |  | **Xiaomi** |  |  |  |
| Xiaomi Controller | ✅ |  | Necesita emparejamiento en cada desconexión. |
| **Marque non trouvé** |  |  |  |
| PlayStation 3 Afterglow Wireless Controller (con dongle USB 2.4GHz RF) | ✅ |  |  |
| PlayStation 3 GASIA Clone | ✅ |  |  |
| PlayStation 3 ShawnWan Clone | ✅ |  |  |
| Sony PlayStation Brandery compatible DualShowk Pad Controller | ✅ | v3.3beta 17 & v4.1.0-dev | Con un adaptador genérico USB para 2 PS1/PS2 pad controllers |

## Adaptadores de mandos

| Periférico | Estado | Última versión probada | Comentarios |
| :---: | :---: | :---: | :---: | :---: |
| 4nes4snes | ✅ | 3.3.0 |  |
| **Cronusmax** |  |  |  |
| CronusMAX Adapter (Xbox 360 Out) | ✅ | v3.3beta17 |  |
| **Mayflash** |  |  |  |
| Mayflash Dolphin Bar | ✅ | 8.0.0 |  |
| Mayflash GameCube Controller Adapter |  | 6.1 | Sólo funciona con Dolphin. |
| Mayflash N64 Controller Adapter | ✅ | 4.0.0-dev |  |
| Mayflash NES/SNES Controller Adapter | ✅ | 4.0.0 | Necesita Usbhid |
| Mayflash PC052 Wii Classic Controller | ✅ | 4.1.0 | Para los mandos Wii clásicos, NES Mini y SNES Mini |
| Mayflash Sega Saturn Controller Adapter | ✅ | 4.0.0 | Necesita Usbhid |
| **OriGlam** |  |  |  |
| Wii Sensor Bar USB | ❌ | 2018.x.x |  |
| **Autres** |  |  |  |
| PSP 1000 - 3000 avec FuSa Gamepad et câble MiniUSB | ✅ | 4.0.1 | El Pi puede no cargar vuestro PSP |
| Retrode |  | 3.2.11 | Necesita Usbhid |
| SFC/USB | ✅ | 4.0.0 |  |
| Manette Sony PlayStation 3 | ✅ | 4.0.0 |  |
| Trenro Dual PlayStation / PlayStation 2 to PC USB Controller Adapter | ✅ | v3.3beta17 & v4.1.0-dev | Con adaptador genérico USB para 2 manettes PS1 / PS2 |

## Sticks Arcade

| Periférico | Estado | Última versión probada | Comentarios |
| :---: | :---: | :---: | :---: | :---: |
| **8bitdo** |  |  |  |
| FC30 Arcade Joystick (uniquement USB) | ✅ |  |  |
| NES30 Arcade Joystick (uniquement USB) | ✅ |  |  |
| **Datel** |  |  |  |
| Arcade Pro Joystick inc Rapid Fire for PS3 / Xbox 360 / PC | ✅ |  |  |
| **HORI** |  |  |  |
| Fighting Stick EX2 USB (Xbox 360 / PC) | ✅ |  |  |
| Fighting Stick MINI 3 pour PS3 | ✅ |  |  |
| Fighting Stick Mini 4 pour PS3 / PS4 | ✅ |  |  |
| Real Arcade Pro. 3 Fighting Stick | ✅ |  |  |
| Real Arcade Pro. 4 Kai (PS3 / PS4 / PC) | ✅ |  |  |
| HORI Real Arcade Pro. V (PS4) |  |  |  |
| Tekken 6 Wireless Arcade Stick PS3 | ✅ |  |  |
| **LIONCAST** |  |  |  |
| LIONCAST Arcade Fighting Stick (PS2 / PS3 / PC) | ✅ |  |  |
| **MadCatz** |  |  |  |
| Arcade Fightstick Street Fighter V Tes+ (PS3 / PS4) | ✅ | 4.0.0-beta1 |  |
| Arcade Stick Fightstick Alpha (PS3 / PS4) | ✅ | 4.0.0-beta3 |  |
| Arcade Fightsitck Street Fighter IV Standard Edition for Xbox 360 | ✅ |  |  |
| Street Fighter IV Fightstick Round 1 Tournament Edition | ✅ |  |  |
| Street Fighter IV Round 2 Arcade Fightstick Tournament Editin | ✅ |  |  |
| Arcade Fightstick Tournament Edition 2 (For PS3 / PS4) | ✅ | 4.0.0-beta6 |  |
| **Mayflash** |  |  |  |
| Mayflash XBOX 360 / PS3 / PC Arcade Fighting Stick V2 | ✅ |  |  |
| Mayflash PS2 PS3 PC USB Universal Arcade Fighting Stick | ✅ |  |  |
| **PXN** |  |  |  |
| PXN - 0082 Arcade Joystick | ✅ |  |  |
| **QanBa** |  |  |  |
| QamBa Q4 RAF Joystick Pro Fightstick Arcade 3in1 (Xbox 360 / PS3 / PC) | ✅ |  |  |
| **Sanwa** |  |  |  |
| JLF-TP-8YT (Sanwa) Arcade Joystick | ✅ |  |  |
| **Time Warner** |  |  |  |
| Mortal Kombat - Edition Ultimate (PS3) | ❌ |  |  |
| **Venom** |  |  |  |
| Venom - Arcade Stick (PS3 / PS4) | ✅ |  |  |
| **X-Arcade** |  |  |  |
| X-Arcade Tankstick (Xinput and DirectInput) | ✅ | 8.0.2-Electron | [Detalles aquí](https://support.xgaming.com/support/solutions/articles/5000554993-how-to-use-x-arcade-with-raspberry-pi) Version avec PCB Tri-Mode |

## Joystick

| Periférico | Estado | Última versión probada | Comentarios |
| :---: | :---: | :---: | :---: |
| **Madcatz :** |  |  |  |
| Cyborg F.L.Y 5 | ✅ | v18.04.20 |  |

## LightGun

| Periférico | Estado | Última versión probada | Comentarios |
| :---: | :---: | :---: | :---: | :---: |
| **Nintendo** |  |  |  |
| Wiimote | ✅ | 7.2.1 | Con el convertidor USB Mayflash (puede ser utilizado como Lightgun) |
| **Ultimarc** |  |  |  |
| AimTrack Lightgun | ✅ | 2018.x.x |  |

## Interface USB

| Periférico | Estado | Última versión probada | Comentarios |
| :---: | :---: | :---: | :---: |
| **Akishop :** |  |  |  |
| PS360+ | ✅ |  |  |
|  |  |  |  |
| Zero Delay USB Encoder PC/PS3 controller |  | v 3.3beta 17 & v4.0.0-dev |  |
| Zero Delay USB Enconder Remix PC/PS3 controller | ✅ | v 3.3beta 17 & v4.0.0-dev |  |

## Arcade Push Buttons

| Periférico | Estado | Última versión probada | Comentarios |
| :---: | :---: | :---: | :---: |
| **Halo :** |  |  |  |
| Halo Button | ✅ |  |  |
|  |  |  |  |
| **Pushbutton :** |  |  |  |
| Pushbutton SL-CV | ✅ |  |  |
| EG Starts Arcade Classic Kit | ✅ | 8.0.0 | ID 0079:0006 DragonRise Inc. PC TWIN SHOCK Gamepad |
