---
title: TI-99/Sim
description: 
published: true
date: 2024-07-18T18:48:47.998Z
tags: ti-99/4a, ti-99/sim, 8.0+
editor: markdown
dateCreated: 2023-09-11T11:50:59.434Z
---



## ![](/emulators/license.svg) Licencia

El core posee la siguiente licencia [**GPL-2.0**](https://gitlab.com/recalbox/packages/standalone/ti99sim/-/blob/master/doc/COPYING).

## ![](/emulators/compatibility.png) Compatibilidad

 | RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | 
 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Funcionalidades

| Fonctionnalité | Supporté |
| :---: | :---: |


## ![](/emulators/bios.svg) BIOS

### Lista de BIOS obligatorias

| Nombre del fichero | Descripción | MD5 | ¿Preinstalada? |
| :---: | :---: | :---: | :---: |
| TI-994A.ctg | System cartridge | 412ecbf991edcb68edd0e76c2caa4a59 | ❌ |

### Emplazamiento

Disponed vuestras bios como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 ti994a
┃ ┃ ┃ ┃ ┣ 🗒  **TI-994A.ctg**

### Lista de BIOS opcionales

| Nombre del fichero | Descripción | MD5 | ¿Preinstalada? |
| :---: | :---: | :---: | :---: |
| spchrom.bin | Speech synthesis | 7adcaf64272248f7a7161cfc02fd5b3f | ❌ |
| ti-disk.ctg | Disk Operating System | 04714f43347cefb2a051a77116344b3f | ❌ |

### Emplazamiento

Disponed vuestras bios como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 ti994a
┃ ┃ ┃ ┃ ┣ 🗒  **spchrom.bin**
┃ ┃ ┃ ┃ ┣ 🗒  **ti-disk.ctg**

## ![](/emulators/roms.png) Roms

### Extensions supportées

La rom doit avoir l'extension :

* .ctg

### Extensiones de roms soportadas

Las roms deben tener las extensiones siguientes:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 ti994a
┃ ┃ ┃ ┃ ┣ 🗒  **juego.ctg**

>Para más información sobre las roms, consultad [este tutorial](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

>Para conservar las configuraciones personalizadas cuando se produce una actualización del sistema Recalbox, os aconsejamos utilizar las [Sobrecargas de configuración](./../../../advanced-usage/configuration-override).
{.is-info}

## ![](/emulators/external-links.png) Enlaces externos

* **Código fuente utilizado** : [https://gitlab.com/recalbox/packages/standalone/ti99sim/](https://gitlab.com/recalbox/packages/standalone/ti99sim/)
* **Página web oficial** : [https://www.mrousseau.org/programs/ti99sim/](https://www.mrousseau.org/programs/ti99sim/)