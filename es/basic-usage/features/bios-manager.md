---
title: Verificación de Bios
description: El nuevo analizador de bios desde vuestra Recalbox
published: true
date: 2023-08-27T18:27:16.658Z
tags: bios, gestor, analizador, scanner
editor: markdown
dateCreated: 2023-08-27T18:27:16.658Z
---

## Presentación

Sabemos que para muchos usuarios las **bios** son un rompecabezas. Hasta ahora, la única forma de comprobar las bios era a través del WebManager. Aunque práctico, para nosotros no es suficiente.

Mientras esperamos la aparición de la nueva intefaz de gestión web de Recalbox, hemos incluido un **verificador de Bios** dentro de EmulationStation. Y para decirlo sin tapujos: _**es como el día y la noche**_

Este nuevo scaner os indica si los ficheros son obligatorios u opcionales, si existe una correspondencia MD5 con las posibles firmas aceptadas y os indicará además para qué sirven ciertos ficheros de bios.

Y esto no es todo! El verificador os muestra todas firmas MD5 válidas que podéis utilizar para buscar las bios en internet y sobre todo _**dispondréis de una interfaz completa para comprobar el estado de vuestras bios de forma sencilla y precisa**_

![](/basic-usage/features/biosmanager1.png)

## Utilización

* En la lista de todos los sitemas, pulsad `START`.
* Id a la entrada `VERIFICACIÓN DE BIOS`.

Os encontraréis con la ventana donde se muestra el estado de todas las bios de Recalbox.

## Informaciones que muestra el verificador

### En la parte izquierda

En la parte izquierda, encontraréis las informaciones siguientes:

* **Sistema**: el sistema junto con sus bios.
* **Nombre de la bios**: cada fichero se muestra dentro del sistema que lo necesita.

### En la parte de la derecha

* `SISTEMA`: el sistema asociado a la bios seleccionada.
* `CORE`: el/los core(s) que lo utilizan.
* `RUTA`: la ruta de directorios donde se encuentra el fichero de bios, incluyendo su nombre completo y extensión de fichero.
* `OBLIGATORIO`: indica si el fichero es obligatorio u opcional para utilizar el core que tiene asociado.
* `MD5 DE CONTROL`: Las posibles firmas MD5 aceptadas por el sistema.
* `¿BIOS ENCONTRADA?`: indica si se ha encontrado el fichero
* `¿MD5 OK?`: la verificación de match de la firma del fichero con la lista de firmas aceptadas por el sistema en cuestión.

### Resultado global

Podéis observar fácilmente el resultado global del verificador para cada bios simplemente echando un ojo al color del icono del pulgar:

* **Verde**: verificación válida, todo va bien.
* **Amarillo**: el fichero de bios está presente pero su firma MD5 no coincide con las aceptadas, o falta el fichero de bios pero se trata de un fichero opcional no obligatorio para el funcionamiento del sistema.
* **Rojo**: falta el fichero de bios y es un fichero obligatorio para el correcto funcionamiento del sistema.

## Búsqueda de bios

Para facilitar la búsqueda en internet de vuestras bios, en la carpeta `/recalbox/share/bios/` existe un fichero llamado `missing_bios_report.txt` que lista los nombres de los ficheros de bios que faltan junto con sus firmas MD5 aceptadas.