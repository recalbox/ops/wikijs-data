---
title: Mega CD
description: 
published: true
date: 2024-06-02T20:59:51.247Z
tags: sega, mega cd
editor: markdown
dateCreated: 2021-10-02T10:45:52.091Z
---

![](/emulators/consoles/megacd.svg){.align-center}

## Technische Daten

* **Hersteller**: Sega
* **Jahr der Veröffentlichung**: 1991
* **Verkaufte Einheiten**: 30 Millionen
* **Meistverkauftes Spiel**: Sewer Shark
* **CPU**: 16-bit Motorola 68000 @ 12.5 MHz + Megadrive CPUs
* **RAM**: 512kB
* **VRAM**: 256kB
* **Sound RAM**: 16kB
* **Video**: Custom ASIC
* **Auflösung**: 320×224 pixels, 64 Farben (512 colors palette)
* **Sound**: Ricoh RF5C164 + Megadrive chips
* **Datenträger**: 500MB CD-ROM discs

## Beschreibung

Das **Sega CD**, auch bekannt als **Mega-CD** oder **Mega CD**, ist ein Peripheriegerät für Segas Mega Drive Videospielkonsole. Es ist ein CD-ROM-Laufwerk, das Spiele im CD-ROM-Format, Audio-CDs und CD-Gs lesen kann. Es wurde Ende 1991 in Japan, 1992 in den Vereinigten Staaten und 1993 in Europa veröffentlicht.

## Emulatoren

[Libretro PicoDrive](libretro-picodrive)
[Libretro GenesisPlusGX](libretro-genesisplusgx)