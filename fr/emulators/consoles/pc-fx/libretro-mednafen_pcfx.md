---
title: Libretro Mednafen_PCFX
description: 
published: true
date: 2024-06-22T11:21:13.468Z
tags: 
editor: markdown
dateCreated: 2021-05-21T08:25:17.460Z
---

**Libretro Mednafen_PCFX** est un port de l'émulateur Mednafen PC-FX pour le NEC PC-FX.

## ![](/emulators/license.svg) Licence

Ce core est sous licence [**GPLv2**](https://github.com/libretro/beetle-pcfx-libretro/blob/master/COPYING).

## ![](/emulators/compatibility.png) Compatibilité

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ❌ | ❌ | ✅ | ✅ | ✅ | ❌ | ✅ | ✅ |

## ![](/emulators/features.png) Fonctionnalités

| Fonctionnalité | Supporté |
| :---: | :---: |
| Redémarrage | ✔ |
| Captures d'écran | ✔ |
| Sauvegardes | ✔ |
| Sauvegardes instantanées | ✔ |
| Rembobinage | ✔ |
| Netplay | ✔ |
| Options du core | ✔ |
| RetroAchievements | ✔ |
| Controls | ✔ |
| Remapping | ✔ |
| Crop Overscan | ✔ |

## ![](/emulators/bios.svg) BIOS

### Liste des bios obligatoires

| Nom de fichier | Description | MD5 | Fourni |
| :---: | :---: | :---: | :---: |
| pcfx.rom | BIOS PC-FX v1.00 - 2 Sep 1994 | 08e36edbea28a017f79f8d4f7ff9b6d7 | ❌  |

### Emplacement

Placez le BIOS comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 pcfx
┃ ┃ ┃ ┃ ┣ 🗒 **pcfx.rom**

## ![](/emulators/isos.svg) Isos

### Extensions supportées

Les isos doivent avoir l'extension suivante :

* .cue
* .ccd
* .toc
* .chd

### Emplacement

Placez les isos comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 pcfx
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.cue**

>Les isos au format **Redump** sont vivement conseillés.
{.is-success}

>Pour plus d'information sur les isos, rendez-vous sur [ce tutoriel](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuration avancée de l'émulateur

>Pour pouvoir conserver vos configurations personnalisées lors d'une mise à jour, nous vous conseillons d'utiliser notre fonctionnalité [Surcharges de configuration](./../../../advanced-usage/configuration-override).
{.is-info}

### Accéder aux options

Vous pouvez configurer diverses options de deux façons différentes.

* Via le menu RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via le fichier `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Options du core

| Options | Valeur des options | Variable (pour surcharge) | Valeurs des variables |
| --- | --- | --- | --- |
| Mise en cache des images CD (Redémarrage requis) | `Désactivé` ✅ / `Activé` | `pcfx_cdimagecache` | `disabled` / `enabled` |
| Grande largeur d'horloge pour les pixels (Redémarrage requis) | `256` / `341` / `1024` ✅ | `pcfx_high_dotclock_width` | `256` / `341` / `1024` |
| Supprimer les clics de réinitialisation du canal (Redémarrage requis) | `Activé` ✅ / `Désactivé` | `pcfx_suppress_channel_reset_clicks` | `enabled` / `disabled` |
| Émuler le Codec buggé (Redémarrage requis) | `Désactivé` ✅ / `Activé` | `pcfx_emulate_buggy_codec` | `disabled` / `enabled` |
| Qualité du son (Redémarrage requis) | `0` / `1` / `2` / `3` ✅ / `4` / `5` | `pcfx_resamp_quality` | `0` / `1` / `2` / `3` / `4` / `5` |
| Interpolation bilinéaire du canal chromatique (Redémarrage requis) | `Désactivé` ✅ / `Activé` | `pcfx_rainbow_chromaip` | `disabled` / `enabled` |
| Aucune limite de sprites (Redémarrage requis) | `Désactivé` ✅ / `Activé` | `pcfx_nospritelimit` | `disabled` / `enabled` |
| Ligne de balayage initiale | `0` / `1` / `2` / `3` / `4` ✅ / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `21` / `22` / `23` / `24` / `25` / `26` / `27` / `28` / `29` / `30` / `31` / `32` / `33` / `34` / `35` / `36` / `37` / `38` / `39` / `40` | `pcfx_initial_scanline` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `21` / `22` / `23` / `24` / `25` / `26` / `27` / `28` / `29` / `30` / `31` / `32` / `33` / `34` / `35` / `36` / `37` / `38` / `39` / `40` |
| Dernière ligne de balayage | `208` / `209` / `210` / `211` / `212` / `213` / `214` / `215` / `216` / `217` / `218` / `219` / `220` / `221` / `222` / `223` / `224` / `225` / `226` / `227` / `228` / `229` / `230` / `231` / `232` / `233` / `234` / `235` ✅ / `236` / `237` / `238` / `239` | `pcfx_last_scanline` | `208` / `209` / `210` / `211` / `212` / `213` / `214` / `215` / `216` / `217` / `218` / `219` / `220` / `221` / `222` / `223` / `224` / `225` / `226` / `227` / `228` / `229` / `230` / `231` / `232` / `233` / `234` / `235` / `236` / `237` / `238` / `239` |
| Sensibilité de la souris | `0.25` / `0.50` / `0.75` / `1.00` / `1.25` ✅ / `1.50` / `1.75` / `2.00` / `2.25` / `2.50` / `2.75` / `3.00` / `3.25` / `3.50` / `3.75` / `4.00` / `4.25` / `4.50` / `4.75` / `5.00` | `pcfx_mouse_sensitivity` | `0.25` / `0.50` / `0.75` / `1.00` / `1.25` / `1.50` / `1.75` / `2.00` / `2.25` / `2.50` / `2.75` / `3.00` / `3.25` / `3.50` / `3.75` / `4.00` / `4.25` / `4.50` / `4.75` / `5.00` |

## ![](/emulators/external-links.png) Liens externes

* **Code source utilisé** : [https://github.com/libretro/beetle-pcfx-libretro/](https://github.com/libretro/beetle-pcfx-libretro/)
* **Documentation Libretro** : [https://docs.libretro.com/library/beetle_pc_fx/](https://docs.libretro.com/library/beetle_pc_fx/)