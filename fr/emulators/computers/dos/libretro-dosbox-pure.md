---
title: Libretro DOSBox Pure
description: 
published: true
date: 2024-06-21T08:28:30.826Z
tags: libretro, dos, msdos, dosbox, pure, 7.2+
editor: markdown
dateCreated: 2021-05-21T08:16:42.176Z
---



## ![](/emulators/license.svg) Licence

Ce core est sous licence [**GPLv2**](https://github.com/schellingb/dosbox-pure/blob/main/LICENSE).

## ![](/emulators/compatibility.png) Compatibilité

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ❌ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Fonctionnalités

| Fonctionnalité | Supporté |
| :---: | :---: |
| Redémarrage  | ✔ |
| Captures d'écran | ✔ |
| Sauvegardes | ✔ |
| Sauvegardes instantanées | ✔ |
| Rembobinage | ✔ |
| Options du core | ✔ |
| Cheats RetroArch | ✔ |
| Cheats natifs | ✔ |
| Contrôles | ✔ |
| Remapping | ✔ |
| Contrôle du disque | ✔ |

## ![](/emulators/bios.svg) BIOS

>**Aucun bios n'est requis.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Extensions supportées

Les roms doivent avoir les extensions suivantes :

* .zip

### Emplacement

Placez les roms comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 dos
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.zip**

## ![](/emulators/advanced-configuration.png) Configuration avancée de l'émulateur

>Pour pouvoir conserver vos configurations personnalisées lors d'une mise à jour, nous vous conseillons d'utiliser notre fonctionnalité [Surcharges de configuration](/fr/advanced-usage/configuration-override).
{.is-info}

### Accéder aux options

Vous pouvez configurer diverses options de deux façons différentes.

* Via le menu RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via le fichier `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Options du core

| Options | Valeur des options | Variable (pour surcharge) | Valeurs des variables |
| --- | --- | --- | --- |
| Force 60 FPS Output | `Désactivé` ✅ / `Activé` | `dosbox_pure_force60fps` | `false` / `true` |
| Show Performance Statistics | `Désactivé` ✅ / `Simple` / `Detailed information` | `dosbox_pure_perfstats` | `none` / `simple` / `detailed` |
| Save States Support | `Enable save states` ✅ / `Enable save states with rewind` / `Désactivé` | `dosbox_pure_savestate` | `on` / `rewind` / `disabled` |
| Loading of dosbox.conf | `Disabled conf support (default)` ✅ / `Try 'dosbox.conf' in the loaded content (ZIP or folder)` / `Try '.conf' with same name as loaded content (next to ZIP or folder)` | `dosbox_pure_conf` | `false` / `inside` / `outside` |
| Use Strict Mode | `Désactive` ✅ / `Activé` | `dosbox_pure_strict_mode` | `false` / `true` |
| Advanced > Start Menu | `Show at start, show again after game exit (default)` / `Show at start, shut down core 5 seconds after auto started game exit` ✅ / `Show at start, shut down core 3 seconds after auto started game exit` / `Show at start, shut down core immediately after auto started game exit` / `Always show menu on startup and after game exit, ignore auto start setting` | `dosbox_pure_menu_time` | `99` / `5` / `3` / `0` / `-1` |
| Advanced > Input Latency | `Default` ✅ / `Lowest latency - See CPU usage setting below!` / `Irregular latency - Might improve performance on low-end devices` | `dosbox_pure_latency` | `default` / `low` / `variable` |
| Advanced > Low latency CPU usage | `50%` / `51%` / `52%` / `53%` / `54%` / `55%` / `56%` / `57%` / `58%` / `59%` / `60%` / `61%` / `62%` / `63%` / `64%` / `65%` / `66%` / `67%` / `68%` / `69%` / `70%` / `71%` / `72%` / `73%` / `74%` / `75%` / `76%` / `77%` / `78%` / `79%` / `80%` / `81%` / `82%` / `83%` / `84%` / `85%` / `86%` / `87%` / `88%` / `89%` / `90%` ✅ / `91%` / `92%` / `93%` / `94%` / `95%` / `96%` / `97%` / `98%` / `99%` / `100%` | `dosbox_pure_auto_target` | `0.5` / `0.51` / `0.52` / `0.53` / `0.54` / `0.55` / `0.56` / `0.57` / `0.58` / `0.59` / `0.6` / `0.61` / `0.62` / `0.63` / `0.64` / `0.65` / `0.66` / `0.67` / `0.68` / `0.69` / `0.7` / `0.71` / `0.72` / `0.73` / `0.74` / `0.75` / `0.76` / `0.77` / `0.78` / `0.79` / `0.8` / `0.81` / `0.82` / `0.83` / `0.84` / `0.85` / `0.86` / `0.87` / `0.88` / `0.89` / `0.9` / `0.91` / `0.92` / `0.93` / `0.94` / `0.95` / `0.96` / `0.97` / `0.98` / `0.99` / `1.0` |
| Enable On Screen Keyboard | `Activé` ✅ / `Désactivé` | `dosbox_pure_on_screen_keyboard` | `true` / `false` |
| Mouse Input Mode | `Virtual mouse (default)` ✅ / `Direct controlled mouse (not supported by all games)` / `Touchpad mode (see descriptio, best for touch screens)` / `Off (ignore mouse inputs)` | `dosbox_pure_mouse_input` | `true` / `direct` / `pad` / `false` |
| Bind Mouse Wheel To Key | `Left-Bracket/Right-Bracket` ✅ / `Comma/Period` / `Page-Up/Page-Down` / `Home/End` / `Delete/Page-Down` / `Minus/Equals` / `Semicolon/Quote` / `Numpad Minus/Plus` / `Numpad Divide/Multiply` / `Up/Down` / `Left/Right` / `Q/E` / `Désactivé` | `dosbox_pure_mouse_wheel` | `67/68` / `72/71` / `79/82` / `78/81` / `80/82` / `64/65"` / `69/70` / `99/100` / `97/98` / `84/85` / `83/86` / `11/13` / `none` |
| Mouse Sensitivity | `20%` / `25%` / `30%` / `35%` / `40%` / `45%` / `50%` / `55%` / `60%` / `65%` / `70%` / `75%` / `80%` / `85%` / `90%` / `95%` / `100%` ✅ / `110%` / `120%` / `130%` / `140%` / `150%` / `160%` / `170%` / `180%` / `190%` / `200%` / `220%` / `240%` / `260%` / `280%` / `300%` / `320%` / `340%` / `360%` / `380%` / `400%` / `420%` / `440%` / `460%` / `480%` / `500%` | `dosbox_pure_mouse_speed_factor` | `0.2` / `0.25` / `0.3` / `0.35` / `0.4` / `0.45` / `0.5` / `0.55` / `0.6` / `0.65` / `0.7` / `0.75` / `0.8` / `0.85` / `0.9` / `0.95` / `1.0` / `1.1` / `1.2` / `1.3` / `1.4` / `1.5` / `1.6` / `1.7` / `1.8` / `1.9` / `2.0` / `2.2` / `2.4` / `2.6` / `2.8` / `3.0` / `3.2` / `3.4` / `3.6` / `3.8` / `4.0` / `4.2` / `4.4` / `4.6` / `4.8` / `5.0` |
| Advanced > Horizontal Mouse Sensitivity | `20%` / `25%` / `30%` / `35%` / `40%` / `45%` / `50%` / `55%` / `60%` / `65%` / `70%` / `75%` / `80%` / `85%` / `90%` / `95%` / `100%` ✅ / `110%` / `120%` / `130%` / `140%` / `150%` / `160%` / `170%` / `180%` / `190%` / `200%` / `220%` / `240%` / `260%` / `280%` / `300%` / `320%` / `340%` / `360%` / `380%` / `400%` / `420%` / `440%` / `460%` / `480%` / `500%` | `dosbox_pure_mouse_speed_factor_x` | `0.2` / `0.25` / `0.3` / `0.35` / `0.4` / `0.45` / `0.5` / `0.55` / `0.6` / `0.65` / `0.7` / `0.75` / `0.8` / `0.85` / `0.9` / `0.95` / `1.0` / `1.1` / `1.2` / `1.3` / `1.4` / `1.5` / `1.6` / `1.7` / `1.8` / `1.9` / `2.0` / `2.2` / `2.4` / `2.6` / `2.8` / `3.0` / `3.2` / `3.4` / `3.6` / `3.8` / `4.0` / `4.2` / `4.4` / `4.6` / `4.8` / `5.0` |
| Advanced > Action Wheel Inputs | `Right Stick, D-Pad, Mouse (Default)` / `Right Stick, D-Pad` / `Right Stick, Mouse` / `Right Stick` / `Both Stick, D-Pad, Mouse` / `Both Stick, D-Pad` / `Both Stick, Mouse` / `Both Stick` / `Left Stick, D-Pad, Mouse` / `Left Stick, D-Pad` / `Left Stick, Mouse` / `Left Stick` / `D-Pad, Mouse` / `D-Pad` / `Mouse` | `dosbox_pure_actionwheel_inputs` | `14` / `6` / `10` / `2` / `15` / `7` / `11` / `3` / `13` / `5` / `9` / `1` / `12` / `4` / `8` |
| Advanced > Automatic Game Pad Mappings | `On (default)` ✅ / `Enable with notification on game detection` / `Désactivé` | `dosbox_pure_auto_mapping` | `true` / `notify` / `false` |
| Advanced > Keyboard Layout | `US (default)` ✅ / `UK` / `Belgium` / `Brazil` / `Croatia` / `Czech Republic` / `Denmark` / `Finland` / `France` / `Germany` / `Greece` / `Hungary` / `Iceland` / `Italy` / `Netherlands` / `Norway` / `Poland` / `Portugal` / `Russia` / `Slovakia` / `Slovenia` / `Spain` / `Sweden` / `Switzerland (German)` / `Switzerland (French)` / `Turkey` | `dosbox_pure_keyboard_layout` | `us` / `uk` / `be` / `br` / `hr` / `cz243` / `dk` / `su` / `fr` / `gr` / `gk` / `hu` / `is161` / `it` / `nl` / `no` / `pl` / `po` / `ru` / `sk` / `si` / `sp` / `sv` / `sg` / `sf` / `tr` |
| Advanced > Menu Transparency | `10%` / `20%` / `30%` / `40%` / `50%` / `60%` / `70%` ✅ / `80%` / `90%` / `100%` | `dosbox_pure_menu_transparency` | `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
| Advanced > Joystick Analog Deadzone | `0%` / `5%` / `10%` / `15%` ✅ / `20%` / `25%` / `30%` / `35%` / `40%` | `dosbox_pure_joystick_analog_deadzone` | `0` / `5` / `10` / `15` / `20` / `25` / `30` / `35` / `40` |
| Advanced > Enable Joystick Timed Intervals | `On (default)` ✅ / `Désactivé` | `dosbox_pure_joystick_timed` | `true` / `false` |
| Emulated Performance | `AUTO - DOSBox will try to detect performance needs (default)` ✅ / `MAX - Emulate as many instructions as possible` / `8086/8088, 4.77 MHz from 1980 (315 cps)` / `286, 6 MHz from 1982 (1320 cps)` / `286, 12.5 MHz from 1985 (2750 cps)` / `386, 20 MHz from 1987 (4720 cps)` / `386DX, 33 MHz from 1989 (7800 cps)` / `486DX, 33 MHz from 1990 (13400 cps)` / `486DX2, 66 MHz from 1992 (26800 cps)` / `Pentium, 100 MHz from 1995 (77000 cps)` / `Pentium II, 300 MHz from 1997 (200000 cps)` / `Pentium III, 600 MHz from 1999 (500000 cps)` / `AMD Athlon, 1.2 GHz from 2000 (1000000 cps)` | `dosbox_pure_cycles` | `auto` / `max` / `315` / `1320` / `2750` / `4720` / `7800` / `13400` / `26800` / `77000` / `200000` / `500000` / `1000000` |
| Detailed > Performance Scale | `20%` / `25%` / `30%` / `35%` / `40%` / `45%` / `50%` / `55%` / `60%` / `65%` / `70%` / `75%` / `80%` / `85%` / `90%` / `95%` / `100%` ✅ / `105%` / `110%` / `115%` / `120%` / `125%` / `130%` / `135%` / `140%` / `145%` / `150%` / `155%` / `160%` / `165%` / `170%` / `175%` / `180%` / `185%` / `190%` / `195%` / `200%` | `dosbox_pure_cycles_scale` | `0.20` / `0.25` / `0.30` / `0.35` / `0.40` / `0.45` / `0.50` / `0.55` / `0.60` / `0.65` / `0.70` / `0.75` / `0.80` / `0.85` / `0.90` / `0.95` / `1.00` / `1.05` / `1.10` / `1.15` / `1.20` / `1.25` / `1.30` / `1.35` / `1.40` / `1.45` / `1.50` / `1.55` / `1.60` / `1.65` / `1.70` / `1.75` / `1.80` / `1.85` / `1.90` / `1.95` / `2.00` |
| Detailed > Limit CPU Usage | `50%` / `51%` / `52%` / `53%` / `54%` / `55%` / `56%` / `57%` / `58%` / `59%` / `60%` / `61%` / `62%` / `63%` / `64%` / `65%` / `66%` / `67%` / `68%` / `69%` / `70%` / `71%` / `72%` / `73%` / `74%` / `75%` / `76%` / `77%` / `78%` / `79%` / `80%` / `81%` / `82%` / `83%` / `84%` / `85%` / `86%` / `87%` / `88%` / `89%` / `90%` / `91%` / `92%` / `93%` / `94%` / `95%` / `96%` / `97%` / `98%` / `99%` / `100%` ✅ | `dosbox_pure_cycle_limit` | `0.5` / `0.51` / `0.52` / `0.53` / `0.54` / `0.55` / `0.56` / `0.57` / `0.58` / `0.59` / `0.6` / `0.61` / `0.62` / `0.63` / `0.64` / `0.65` / `0.66` / `0.67` / `0.68` / `0.69` / `0.7` / `0.71` / `0.72` / `0.73` / `0.74` / `0.75` / `0.76` / `0.77` / `0.78` / `0.79` / `0.8` / `0.81` / `0.82` / `0.83` / `0.84` / `0.85` / `0.86` / `0.87` / `0.88` / `0.89` / `0.9` / `0.91` / `0.92` / `0.93` / `0.94` / `0.95` / `0.96` / `0.97` / `0.98` / `0.99` / `1.0` |
| Emulated Graphics Chip (restart required) | `SVGA (Super Video Graphics Array) (default)` ✅ / `VGA (Video Graphics Array)` / `EGA (Enhanced Graphics Adapter)` / `CGA (Color Graphics Adapter)` / `Tandy (Tandy Graphics Adapter)` / `Hercules (Hercules Graphics Card)` / `PCjr` | `dosbox_pure_machine` | `svga` / `vga` / `ega` / `cga` / `tandy` / `hercules` / `pcjr` |
| CGA Mode | `Early model, composite mode auto (default)` ✅ / `Early model, composite mode on` / `Early model, composite mode off` / `Late model, composite mode auto` / `Late model, composite mode on` / `Late model, composite mode off` | `dosbox_pure_cga` | `early_auto` / `early_on` / `early_off` / `late_auto` / `late_on` / `late_off` |
| Hercules Color Mode | `Black & white (default)` ✅ / `Black & amber` / `Black & green` | `dosbox_pure_hercules` | `white` / `amber` / `green` |
| SVGA Mode (restart required) | `S3 Trio64 (default)` ✅ / `S3 Trio64 no-line buffer hack (reduces flickering in some games)` / `S3 Trio64 VESA 1.3` / `Tseng Labs ET3000` / `Tseng Labs ET4000` / `Paradise PVGA1A` | `dosbox_pure_svga` | `svga_s3` / `svga_nolfb` / `svga_oldvbe` / `svga_et3000` / `svga_et4000` / `svga_paradise` |
| SVGA Memory (restart required) | `512KB` / `1MB` / `2MB (default)` / `3MB` / `4MB` / `8MB (not always recognized)` | `dosbox_pure_svgamem` | `0` / `1` / `2` / `3` / `4` / `8` |
| 3dfx Voodoo Emulation | `Enabled - 12MB memory (default)` ✅ / `Enabled - 4MB memory` / `Désactivé` | `dosbox_pure_voodoo` | `12mb` / `4mb` / `off` |
| 3dfx Voodoo Performance Settings | `Multi-threading (default)` ✅ / `Multi-threading, low quality` / `Low quality` / `Désactivé` | `dosbox_pure_voodoo_perf` | `1` / `3` / `2`/ `0` |
| Aspect Ratio Correction | `Off (default)` ✅ / `Activé` | `dosbox_pure_aspect_correction` | `false` / `true` |
| Overscan Border Size | `Off (default)` / `Small` / `Medium` / `Large` | `dosbox_pure_overscan` | `0` / `1` / `2` / `3` |
| Memory Size (restart required) | `Disable extended memory (no EMS/XMS)` / `4 MB` / `8 MB` / `16 MB (default)` ✅ / `24 MB` / `32 MB` / `48 MB` / `64 MB ` / `96 MB` / `128 MB` / `224 MB` / `256 MB` / `512 MB` / `1024 MB` | `dosbox_pure_memory_size` | `none` / `4` / `8` / `16` / `24` / `32` / `48` / `64` / `96` / `128` / `224` / `256` / `512` / `1024` |
| CPU Type (restart required) | `Auto - Mixed feature set with maximum performance and compatibility` ✅ / `386 - 386 instruction with fast memory access` / `386 (slow) - 386 instruction set with memory privilege checks` / `386 (prefetch) - With prefetch queue emulation (only on 'auto' and 'normal' core)` / `486 (slow) - 486 instruction set with memory privilege checks` / `Pentium (slow) - 586 instruction set with memory privilege checks` | `dosbox_pure_cpu_type` | `auto` / `386` / `386_slow` / `386_prefetch` / `486_slow` / `pentium_slow` |
| Advanced > CPU Core (restart required) | `Auto - Real-mode games use normal, protected-mode games use dynamic` ✅ / `Dynamic - Dynamic recompilation (fast, using dynrec / dynamic_x86 implementation)` / `Normal (interpreter)` / `Simple (interpreter optimized for old real-mode games)` | `dosbox_pure_cpu_core` | `auto` / `dynamic` / `normal` / `simple` |
| Advanced > OS Disk Modifications (restart required) | `Keep (default)` ✅ / `Discard` / `Save Difference Per Content` | `dosbox_pure_bootos_ramdisk` | `false` / `true` / `diff` |
| Advanced > Free Space on D: in OS (restart required) | `1GB (Default)` ✅ / `2BG` / `4GB` / `8GB` / `Discard Changes on D/` | `dosbox_pure_bootos_dfreespace` | `1024` / `2048` / `4192` / `8196` / `discard` |
| Advanced > Force Normal Core in OS | `Off (default)` ✅ / `Activé` | `dosbox_pure_bootos_forcenormal` | `false` / `true` |
| Audio Sample Rate (restart required) | `8000` / `11025` / `16000` / `22050` / `32000` / `44100` / `48000` ✅ / `49716` | `dosbox_pure_audiorate` | `8000` / `11025` / `16000` / `22050` / `32000` / `44100` / `48000` / `49716` |
| SoundBlaster Settings | `Port 0x220, IRQ 7, 8-Bit DMA 1, 16-bit DMA 5` ✅ / `Port 0x220, IRQ 5, 8-Bit DMA 1, 16-bit DMA 5` / `Port 0x240, IRQ 7, 8-Bit DMA 1, 16-bit DMA 5` / `Port 0x240, IRQ 7, 8-Bit DMA 3, 16-bit DMA 7` / `Port 0x240, IRQ 2, 8-Bit DMA 3, 16-bit DMA 7` / `Port 0x240, IRQ 5, 8-Bit DMA 3, 16-bit DMA 5` / `Port 0x240, IRQ 5, 8-Bit DMA 1, 16-bit DMA 5` / `Port 0x240, IRQ 10, 8-Bit DMA 3, 16-bit DMA 7` / `Port 0x280, IRQ 10, 8-Bit DMA 0, 16-bit DMA 6` / `Port 0x210, IRQ 5, 8-Bit DMA 1, 16-bit DMA 5` | `dosbox_pure_sblaster_conf` | `A220 I7 D1 H5` / `A220 I5 D1 H5` / `A240 I7 D1 H5` / `A240 I7 D3 H7` / `A240 I2 D3 H7` / `A240 I5 D3 H5` / `A240 I5 D1 H5` / `A240 I10 D3 H7` / `A280 I10 D0 H6` / `A210 I5 D1 H5` |
| MIDI Output | `Désactivé` ✅ / `Frontend MIDI driver` / `Scan System directory for soundfonts (open this menu again after)` | `dosbox_pure_midi` | `disabled` / `frontend` / `scan` |
| Advanced > SoundBlaster Type | `SoundBlaster 16 (default)` ✅ / `SoundBlaster Pro 2` / `SoundBlaster Pro` / `SoundBlaster 2.0` / `SoundBlaster 1.0` / `GameBlaster` / `none` | `dosbox_pure_sblaster_type` | `sb16 (default)` / `sbpro2` / `sbpro1` / `sb2` / `sb1` / `gb` / `none` |
| Advanced > SoundBlaster Adlib/FM Mode | `Auto (select based on the SoundBlaster type) (default)` ✅ / `CMS (Creative Music System / GameBlaster)` / `OPL-2 (AdLib / OPL-2 / Yamaha 3812)` / `Dual OPL-2 (Dual OPL-2 used by SoundBlaster Pro 1.0 for stereo sound)` / `OPL-3 (AdLib / OPL-3 / Yamaha YMF262)` / `OPL-3 Gold (AdLib Gold / OPL-3 / Yamaha YMF262)` / `Désactivé` | `dosbox_pure_sblaster_adlib_mode` | `auto` / `cms` / `opl2` / `dualopl2` / `opl3` / `opl3gold` / `none` |
| Advanced > SoundBlaster Adlib Provider | `Default` ✅ / `High quality Nuked OPL3` | `dosbox_pure_sblaster_adlib_emu` | `default` / `nuked` |
| Advanced > Enable Gravis Ultrasound (restart required) | `Off (default)` ✅ / `Activé` | `dosbox_pure_gus` | `false` / `true` |
| Advanced > Swap Stereo Channels | `Off (default)` ✅ / `Activé` | `dosbox_pure_swapstereo` | `false` / `true` |

## ![](/emulators/external-links.png) Liens externes

* **Code source utilisé** : [https://github.com/schellingb/dosbox-pure/](https://github.com/schellingb/dosbox-pure/)
* **Documentation Libretro** : [https://docs.libretro.com/library/dosbox_pure/](https://docs.libretro.com/library/dosbox_pure/)