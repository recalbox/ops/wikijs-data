---
title: Add custom music into EmulationStation
description: 
published: true
date: 2021-09-04T18:06:15.590Z
tags: custom, music, emulationstation
editor: markdown
dateCreated: 2021-08-06T12:55:34.912Z
---

You can add your own music to be played in EmulationStation.

## Procedure

* You must have access to your system:
  * Via [WinSCP](./../system/access/network-access-winscp)
  * Via [Cyberduck](./../system/access/network-access-cyberduck)
  * Via [MobaXTerm](./../system/access/network-access-mobaxterm)
* Once connected, you can put your music in the `/recalbox/share/music` directory
* Once set up, your music will play randomly in EmulationStation without needing to reboot.

## See also

Put your favorite music on EmulationStation [following Fabrice's step by step](https://www.youtube.com/watch?v=uu40MDzBdJM).

![https://www.youtube.com/watch?v=uu40MDzBdJM](/tutorials/frontend-customization/add-music-into-emulationstation/changer-la-musique-sur-es.png)

![See 2:42 for explanations](/tutorials/frontend-customization/add-music-into-emulationstation/ajouter-des-musiques-sur-emulationstation.png)