---
title: Dreamcast
description: 
published: true
date: 2024-07-29T13:20:23.004Z
tags: sega, dreamcast, consolas
editor: markdown
dateCreated: 2023-09-03T15:54:03.432Z
---

![](/emulators/consoles/dreamcast.svg){.align-center}

## Detalles técnicos

* **Fabricante :** SEGA
* **Año de lanzamiento :**
  * 27 de noviembre de 1998 (Japón)
  * 9 de septiembre de 1999 (EE.UU./Canadá)
  * 14 de octubre de 1999 (Europa)
* **Procesador:** Hitachi SH4 RISC (128 bits)
* **Procesador gráfico:** NEC Power VR II 
* **Chip de sonido:** Yamaha 64 canales (200 MHz)
* **RAM:** 16 MB
* **Soporte:** GD-Rom (1 GB de capacidad)

## Presentación

La última consola de **SEGA** perdurará en el recuerdo.

A pesar de su impresionante catálogo de juegos, esta consola, que ofrece de serie el juego en línea, no consiguió conquistar a un público suficiente, quizá porque se adelantó demasiado a su tiempo.

A pesar de su buena relación calidad-precio, la consola fue un fracaso comercial.

La **Dreamcast** no comete el mismo error que la [**Saturn**](./saturn) y tiene un único procesador principal.

En cuanto a los gráficos, el trabajo lo realiza un Power VR II directamente sacado del mundo del PC, lo que, combinado con un SO derivado de Windows CE, facilita la programación (sobre todo a la hora de portar).

El formato propietario **GD-Rom** hace que los juegos no puedan copiarse con demasiada facilidad, pero es la capacidad de 1 GB lo que realmente hace que el soporte merezca la pena.

Ahora llegamos a los puertos de joystick: la **Dreamcast** tiene 4, para que puedas sacar el máximo partido a juegos como **Virtua Tennis** sin tener que invertir en un multipad.

Sin embargo, no tenemos puertos para **VMS**. Estos se insertan en los controladores, por lo que puedes tener hasta 8 de ellos insertados en la consola al mismo tiempo.

Ahora lo más interesante: el **modem**. Este se conecta en el lateral de la consola, permitiéndote cambiarlo por uno más rápido si te apetece. 

Por último, el **Dreamcast** es la base del sistema arcade [**NAOMI**](../arcade/naomi). Este sistema se compone principalmente de una tarjeta con especificaciones técnicas casi idénticas a las del **Dreamcast** (pero con el doble de memoria). La ventaja del sistema [**NAOMI**](../arcade/naomi) es que se pueden conectar hasta 16 de estas tarjetas y trabajar en paralelo. No obstante, el sistema [**NAOMI**](../arcade/naomi) seguirá su propia evolución al margen del **Dreamcast**.

## Emuladores

[Libretro Flycast](libretro-flycast)
[Libretro Flycast-Next](libretro-flycast-next)
[Reicast](reicast)

