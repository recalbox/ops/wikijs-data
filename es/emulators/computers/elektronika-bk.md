---
title: Elektronika BK
description: 
published: true
date: 2024-07-26T12:47:53.821Z
tags: elektronika, bk, 7.2+, ordenadores
editor: markdown
dateCreated: 2023-09-02T18:48:12.818Z
---

![](/emulators/computers/elektronikabk.svg){.align-center}

## Datos técnicos

* **Fabricante**: Elektronika
* **Año de lanzamiento**: 1984
* **Sistema operativo**: Focal
* **Procesador**: K1801VM1 @ 3MHz
* **RAM**: 32 KB
* **ROM**: 8 KB
* **Procesador gráfico**: K1801VP1-037
* **Sonido**: DAC Covox
* **Resolución**: 512×256 (monocromo) / 256×256 (4 colores)

## Presentación

El Elektronika BK es una serie de ordenadores domésticos sin ventilador compatibles con el PFP-11 de 16 bits desarrollados bajo la marca Elektronika por el Centro Científico NPO, el equipo soviético más importante de la época. También fue el predecesor de los ordenadores más potentes UKNC y DVK.

Lanzado en 1984 (desarrollado en 1983), el ordenador se basaba en el K1801BM1 (el procesador soviético compatible con LSI-11) y fue el único ordenador "oficial" que entró en producción en serie.

Se vendía por entre 600 y 650 rublos. Era caro pero asequible, lo que lo convirtió en uno de los ordenadores más populares de la Unión Soviética, a pesar de contener varios problemas. Más tarde, en los años 90, su potente procesador y su diseño sencillo y fácil de programar los hicieron populares como máquinas de demostración. BK (БК) significa en ruso "бытовой компьютер" - ordenador familiar. Las máquinas también se utilizaron durante un breve periodo de tiempo como cajas registradoras, por ejemplo en los grandes almacenes GUM.

Aunque el BK-0010 era uno de los ordenadores soviéticos más baratos y la velocidad (así como la memoria, los gráficos, etc.) difería poco de los modelos sencillos de 8 bits, fue uno de los primeros ordenadores domésticos totalmente de 16 bits del mundo (a diferencia del TI-99 / 4A, el BK tenía controladores con el mismo ancho de bus de datos). La Intellivision utilizaba un procesador muy similar de 16 bits, el General Instrument CP1600, y con las extensiones del Componente de Teclado o ECS, se transformó en un ordenador doméstico de 16 bits. El IBM PC y el PCjr eran ordenadores de 8/16 bits, porque su procesador 8088 tiene un bus de datos de 8 bits y un bus interno de 16 bits.

## Emuladores

[Libretro BK](libretro-bk)