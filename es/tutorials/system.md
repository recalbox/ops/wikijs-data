---
title: 🎛️ Sistema
description: 
published: true
date: 2024-07-26T06:29:16.965Z
tags: tutorial, sistema
editor: markdown
dateCreated: 2023-09-01T09:16:25.244Z
---

Aquí encontraréis algunos fundamentos de Recalbox, que constituyen la base: cómo acceder al sistema o cómo modificar algunas de sus opciones, por ejemplo.

Aquí tenéis la lista de categorías disponibles:

[Acceso](access)
[Backups / Duplicación de la tarjeta SD](backups-and-clone)
[Instalación](installation)
[Modificación](modification)