---
title: FAQ
description: 
published: true
date: 2021-10-16T11:40:04.550Z
tags: games, tic-80, faq, download
editor: markdown
dateCreated: 2021-08-08T09:36:47.056Z
---

## Games downloader

### Introduction

The Tic-80 games are free games. Recalbox includes a script that allows you to download the desired games directly from https://tic80.com/.

### Usage

Via [SSH](./../../../system/access/root-access-terminal-cli), in the folder `/recalbox/share/roms/tic80`, type `bash recalbox-tic80` to download TIC-80 games easily. You can choose a category of games (demos, others...) or the 5 categories proposed.