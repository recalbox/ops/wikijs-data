---
title: Libretro PX68k
description: 
published: true
date: 2021-11-06T23:43:16.150Z
tags: libretro, x68000, px68k
editor: markdown
dateCreated: 2021-07-29T17:38:28.195Z
---



## ![](/emulators/license.svg) License

This core is under [**GPLv2**](https://github.com/libretro/px68k-libretro/blob/master/COPYING) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Features



## ![](/emulators/bios.svg) BIOS

### List od mandatory bios

| Filename | Description | MD5 | Provided |
| :---: | :---: | :---: | :---: |
| iplrom.dat | BIOS X68000 | 7fd4caabac1d9169e289f0f7bbf71d8e | ✅ |
| cgrom.dat | Font file | cb0a5cfcf7247a7eab74bb2716260269 | ✅ |

### Location

Put the bios like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 keropi
┃ ┃ ┃ ┃ ┣ 🗒 **iplrom.dat**
┃ ┃ ┃ ┃ ┣ 🗒 **cgrom.dat**

### List of optional bios

| File | Description | MD5 | Provided |
| :---: | :---: | :---: | :---: |
| iplrom30.dat | BIOS X68000 #2 | f373003710ab4322642f527f567e020a | ❌ |
| iplromco.dat | BIOS X68000 #3 | cc78d4f4900f622bd6de1aed7f52592f | ❌ |
| iplromxv.dat | BIOS X68000 #4 | 0617321daa182c3f3d6f41fd02fb3275 | ❌ |

### Location

Put the bios like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 keropi
┃ ┃ ┃ ┃ ┣ 🗒 **iplrom30.dat**
┃ ┃ ┃ ┃ ┣ 🗒 **iplromco.dat**
┃ ┃ ┃ ┃ ┣ 🗒 **iplromxv.dat**

## ![](/emulators/roms.png) Roms

### Supported extensions

Roms must have the extension:

* .dim
* .img
* .d88
* .88d
* .hdm
* .dup
* .2hd
* .xdf
* .hdf
* .cmd
* .m3u
* .zip
* .7z

This system supports compressed roms in .zip/.7z format. However, be careful, it is only an archive.

The files contained in the .zip/.7z must correspond to the extensions mentioned above.
Moreover, each .zip/.7z file must contain only one compressed rom.

### Location

Put the roms like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 x68000
┃ ┃ ┃ ┃ ┣ 🗒 **game.zip**

>The roms using **TOSEC** format are highly recommended.
{.is-success}

>For more information about roms, go to [this tutorial](./../../../tutorials/games/generalities/isos-and-roms)!
{.is-info}

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Accessing to options

You can configure various options in two different ways.

* Via RetroArch menu:

┣ 📁 RetroArch Menu
┃ ┣ 📁 Core options
┃ ┃ ┣ 🧩 Name_option

* Via the file `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core options

| Options | Options values | Variables (for configuration override) | Variables values |
| --- | --- | --- | --- |
| Menu Font Size | `normal` ✅ / `large` | `px68k_menufontsize` | `normal` / `large` |
| CPU Speed | `10MHz` ✅ / `16MHz` / `25MHz` / `33MHz (OC)` / `66MHz (OC)` / `100MHz (OC)` / `150MHz (OC)` / `200MHz (OC)` | `px68k_cpuspeed` | `10MHz` / `16MHz` / `25MHz` / `33MHz (OC)` / `66MHz (OC)` / `100MHz (OC)` / `150MHz (OC)` / `200MHz (OC)` |
| RAM Size (Restart) | `1MB` / `2MB` / `3MB` / `4MB` ✅ / `5MB` / `6MB` / `7MB` / `8MB` / `9MB` / `10MB` / `11MB` / `12MB` | `px68k_ramsize` | `1MB` / `2MB` / `3MB` / `4MB` / `5MB` / `6MB` / `7MB` / `8MB` / `9MB` / `10MB` / `11MB` / `12MB` |
| Use Analog | `Disabled` ✅ / `Enabled` | `px68k_analog` | `disabled` / `enabled` |
| P1 Joypad Type | `Default` ✅ / `CPSF-MD (8 Buttons)` / `CPSF-SFC (8 Buttons)` | `px68k_joytype1` | `Default` / `CPSF-MD (8 Buttons)` / `CPSF-SFC (8 Buttons)` |
| P2 Joypad Type | `Default` ✅ / `CPSF-MD (8 Buttons)` / `CPSF-SFC (8 Buttons)` | `px68k_joytype2` | `Default` / `CPSF-MD (8 Buttons)` / `CPSF-SFC (8 Buttons)` |
| P1 Joystick Select Mapping | `Default` ✅ / `XF1` / `XF2` / `XF3` / `XF4` / `XF5` / `OPT1` / `OPT2` / `F1` / `F2` | `px68k_joy1_select` | `Default` / `XF1` / `XF2` / `XF3` / `XF4` / `XF5` / `OPT1` / `OPT2` / `F1` / `F2` |
| ADPCM Volume | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` ✅ | `px68k_adpcm_vol` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` |
| OPM Volume | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` ✅ / `13` / `14` / `15` | `px68k_opm_vol` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` |
| Mercury Volume | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` ✅ / `14` / `15` | `px68k_mercury_vol` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` |
| Swap Disks on Drive | `FDD1` ✅ / `FDD0` | `px68k_disk_drive` | `FDD1` / `FDD0` |
| Save FDD Paths | `Disabled` / `Enabled` ✅ | `px68k_save_fdd_path` | `disabled` / `enabled` |
| Save HDD Paths | `Disabled` / `Enabled` ✅ | `px68k_save_hdd_path` | `disabled` / `enabled` |
| Rumble on FDD Reads | `Disabled` ✅ / `Enabled` | `px68k_rumble_on_disk_read` | `disabled` / `enabled` |
| Joy/Mouse | `Mouse` ✅ / `Joystick` | `px68k_joy_mouse` | `Mouse` / `Joystick` |
| VBtn Swap | `TRIG1 TRIG2` ✅ / `TRIG2 TRIG1` | `px68k_vbtn_swap` | `TRIG1 TRIG2` / `TRIG2 TRIG1` |
| No Wait Mode | `Disabled` ✅ / `Enabled` | `px68k_no_wait_mode` | `disabled` / `enabled` |
| Frames Skip | `Full Frame` ✅ / `1/2 Frame` / `1/3 Frame` / `1/4 Frame` / `1/5 Frame` / `1/6 Frame` / `1/8 Frame` / `1/16 Frame` / `1/32 Frame` / `1/60 Frame` / `Auto Frame Skip` | `px68k_frameskip` | `Full Frame` / `1/2 Frame` / `1/3 Frame` / `1/4 Frame` / `1/5 Frame` / `1/6 Frame` / `1/8 Frame` / `1/16 Frame` / `1/32 Frame` / `1/60 Frame` / `Auto Frame Skip` |
| Push Video before Audio | `Disabled` ✅ / `Enabled` | `px68k_push_video_before_audio` | `disabled` / `enabled` |
| Adjust Frame Rates | `Disabled` / `Enabled` ✅ | `px68k_adjust_frame_rates` | `disabled` / `enabled` |
| Audio Desync Hack | `Disabled` ✅ / `Enabled` | `px68k_audio_desync_hack` | `disabled` / `enabled` |

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/libretro/px68k-libretro/](https://github.com/libretro/px68k-libretro/)
* **Libretro documentation**: [https://docs.libretro.com/library/px68k/](https://docs.libretro.com/library/px68k/)