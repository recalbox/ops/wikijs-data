---
title: 5. 🛠️ ÉMULATEURS
description: 
published: true
date: 2023-09-04T17:26:41.435Z
tags: émulateurs
editor: markdown
dateCreated: 2021-05-21T07:48:34.092Z
---

Vous arrivez dans la partie un peu plus sophistiqué de toute la documentation, concerenant tous les systèmes de jeux inclus dans Recalbox :

* [Arcade](arcade)
* [Consoles](consoles)
* [Fantasy](fantasy)
* [Moteurs de jeux](engines)
* [Ordinateurs](computers)
* [Portables](handhelds)
* [Ports](ports)

Voici la [liste de jeux inclus](included-games) dans la dernière version de Recalbox.