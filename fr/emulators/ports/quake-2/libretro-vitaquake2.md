---
title: Libretro VitaQuake 2
description: 
published: true
date: 2024-06-23T12:09:31.379Z
tags: libretro, quake 2, vitaquake, 8.1+
editor: markdown
dateCreated: 2022-02-06T18:29:09.325Z
---

## ![](/emulators/license.svg) Licence

Ce core est sous licence [**GPLv2**](https://github.com/libretro/vitaquake2/blob/libretro/LICENSE).

## ![](/emulators/compatibility.png) Compatibilité

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ❌ | ❌ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Fonctionnalités



## ![](/emulators/bios.svg) BIOS

>**Aucun bios n'est requis.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Extensions supportées

Les roms doivent avoir l'extension :

* .pak

### Emplacement

Placez les roms comme ceci : 

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 ports
┃ ┃ ┃ ┃ ┣ 📁 Quake
┃ ┃ ┃ ┃ ┃ ┣ 📁 baseq2
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 **pak0.pak**

## ![](/emulators/advanced-configuration.png) Configuration avancée de l'émulateur

>Pour pouvoir conserver vos configurations personnalisées lors d'une mise à jour, nous vous conseillons d'utiliser notre fonctionnalité [Surcharges de configuration](./../../../advanced-usage/configuration-override).
{.is-info}

### Accéder aux options

Vous pouvez configurer diverses options de deux façons différentes.

* Via le Menu RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via le fichier `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Options du core

| Options | Valeur des options | Variable (pour surcharge) | Valeurs des variables |
| --- | --- | --- | --- |
| Internal Resolution (Restart) | `320x240` / `400x240` / `480x272` / `512x384` / `640x368` / `640x480` / `720x408` / `800x600` / `960x544` ✅ / `1024x768` / `1280x720` / `1280x800` / `1280x1024` / `1360x768` / `1366x768` / `1440x900` / `1600x900` / `1680x1050` / `1920x1080` / `1920x1200` / `2560x1080 (OpenGL Only)` / `2560x1440 (OpenGL Only)` / `2560x1600 (OpenGL Only)` / `3440x1440 (OpenGL Only)` / `3840x2160 (OpenGL Only)` / `5120x2880 (OpenGL Only)` / `7680x4320 (OpenGL Only)` / `15360x8640 (OpenGL Only)` | `vitequakeii_resolution` | `320x240` / `400x240` / `480x272` / `512x384` / `640x368` / `640x480` / `720x408` / `800x600` / `960x544` / `1024x768` / `1280x720` / `1280x800` / `1280x1024` / `1360x768` / `1366x768` / `1440x900` / `1600x900` / `1680x1050` / `1920x1080` / `1920x1200` / `2560x1080` / `2560x1440` / `2560x1600` / `3440x1440` / `3840x2160` / `5120x2880` / `7680x4320` / `15360x8640` |
| Framerate (Restart) | `Auto` ✅ / `30 fps` / `50 fps` / `60 fps` / `72 fps` / `75 fps` / `90 fps` / `100 fps` / `119 fps` / `120 fps` / `144 fps` / `155 fps` / `160 fps` / `165 fps` / `180 fps` / `200 fps` / `240 fps` / `244 fps` / `300 fps` / `360 fps` | `vitequakeii_framerate` | `auto` / `30` / `50` / `60` / `72` / `75` / `90` / `100` / `119` / `120` / `144` / `155` / `160` / `165` / `180` / `200` / `240` / `244` / `300` / `360` |
| Gamma Level (Restart) | `0.20` / `0.22` / `0.24` / `0.26` / `0.28` / `0.30` / `0.32` / `0.34` / `0.36` / `0.38` / `0.40` / `0.42` / `0.44` / `0.46` / `0.48` / `0.50` ✅ / `0.52` / `0.54` / `0.56` / `0.58` / `0.60` / `0.62` / `0.64` / `0.66` / `0.68` / `0.70` / `0.72` / `0.74` / `0.76` / `0.78` / `0.80` / `0.82` / `0.84` / `0.86` / `0.88` / `0.90` / `0.92` / `0.94` / `0.96` / `0.98` / `1.00` | `vitequakeii_gamma` | `0.20` / `0.22` / `0.24` / `0.26` / `0.28` / `0.30` / `0.32` / `0.34` / `0.36` / `0.38` / `0.40` / `0.42` / `0.44` / `0.46` / `0.48` / `0.50` / `0.52` / `0.54` / `0.56` / `0.58` / `0.60` / `0.62` / `0.64` / `0.66` / `0.68` / `0.70` / `0.72` / `0.74` / `0.76` / `0.78` / `0.80` / `0.82` / `0.84` / `0.86` / `0.88` / `0.90` / `0.92` / `0.94` / `0.96` / `0.98` / `1.00` |
| Renderer (Restart) | `OpenGL` ✅ / `Software` | `vitequakeii_renderer` | `opengl` / `software` |
| \[GL] Brightness (Restart) | ``1.0` / `1.2` / `1.4` / `1.6` / `1.8` / `2.0` ✅ / `2.2` / `2.4` / `2.6` / `2.8` / `3.0` / `3.2` / `3.4` / `3.6` / `3.8` / `4.0` / `4.2` / `4.4` / `4.6` / `4.8` / `5.0`` | `vitequakeii_gl_modulate` | `1.0` / `1.2` / `1.4` / `1.6` / `1.8` / `2.0` / `2.2` / `2.4` / `2.6` / `2.8` / `3.0` / `3.2` / `3.4` / `3.6` / `3.8` / `4.0` / `4.2` / `4.4` / `4.6` / `4.8` / `5.0` |
| \[GL] Texture Filtering | `Nearest` / `Linear` / `Nearest (HQ)` ✅ / `Linear (HQ)` | `vitequakeii_gl_texture_filtering` | `nearest` / `linear` / `nearest_hq` / `linear_hq` |
| \[GL] Dynamic Shadows | `Désactivé` ✅ / `Activé` | `vitequakeii_gl_shadows` | `disabled` / `enabled` |
| \[GL] Mirror Mode | `Désactivé` ✅ / `Activé` | `vitequakeii_gl_xflip` | `disabled` / `enabled` |
| \[GL] HUD Scale Factor | `0.00` / `0.02` / `0.04` / `0.06` / `0.08` / `0.10` / `0.12` / `0.14` / `0.16` / `0.18` / `0.20` / `0.22` / `0.24` / `0.26` / `0.28` / `0.30` / `0.32` / `0.34` / `0.36` / `0.38` / `0.40` / `0.42` / `0.44` / `0.46` / `0.48` / `0.50` ✅ / `0.52` / `0.54` / `0.56` / `0.58` / `0.60` / `0.62` / `0.64` / `0.66` / `0.68` / `0.70` / `0.72` / `0.74` / `0.76` / `0.78` / `0.80` / `0.82` / `0.84` / `0.86` / `0.88` / `0.90` / `0.92` / `0.94` / `0.96` / `0.98` / `1.00` | `vitequakeii_gl_hud_scale` | `0.00` / `0.02` / `0.04` / `0.06` / `0.08` / `0.10` / `0.12` / `0.14` / `0.16` / `0.18` / `0.20` / `0.22` / `0.24` / `0.26` / `0.28` / `0.30` / `0.32` / `0.34` / `0.36` / `0.38` / `0.40` / `0.42` / `0.44` / `0.46` / `0.48` / `0.50` / `0.52` / `0.54` / `0.56` / `0.58` / `0.60` / `0.62` / `0.64` / `0.66` / `0.68` / `0.70` / `0.72` / `0.74` / `0.76` / `0.78` / `0.80` / `0.82` / `0.84` / `0.86` / `0.88` / `0.90` / `0.92` / `0.94` / `0.96` / `0.98` / `1.00` |
| \[SW] Dithered Filtering | `Désactivé` ✅ / `Activé` | `vitequakeii_sw_dithered_filtering` | `disabled` / `enabled` |
| Weapon Position | `Right` ✅ / `Left` / `Center` / `Hidden` | `vitequakeii_hand` | `right` / `left` / `center` / `hidden` |
| Show Crosshair | `Désactivé` / `White Cross` ✅ / `Red Dot` / `Red Angle` | `vitequakeii_xhair` | `disabled` / `cross` / `dot` / `angle` |
| Show FPS | `Désactivé` ✅ / `Activé` | `vitequakeii_fps` | `disabled` / `enabled` |
| Force 4:3 for Cinematics | `Activé` ✅ / `Désactivé` | `vitequakeii_cin_force43` | `enabled` / `disabled` |
| Play Music | `Activé` ✅ / `Désactivé` | `vitequakeii_cdaudio_enabled` | `enabled` / `disabled` |
| Music Volume | `5%` / `10%` / `15%` / `20%` / `25%` / `30%` / `35%` / `40%` / `45%` / `50%` ✅ / `55%` / `60%` / `65%` / `70%` / `75%` / `80%` / `85%` / `90%` / `95%` / `100%` / `105%` / `110%` / `115%` / `120%` / `125%` / `130%` | `vitequakeii_cdaudio_volume` | `5` / `10` / `15` / `20` / `25` / `30` / `35` / `40` / `45` / `50` / `55` / `60` / `65` / `70` / `75` / `80` / `85` / `90` / `95` / `100` / `105` / `110` / `115` / `120` / `125` / `130` |
| Auto Run | `Désactivé` ✅ / `Activé` | `vitequakeii_cl_run` | `disabled` / `enabled` |
| Accurate Aiming | `Désactivé` ✅ / `Activé` | `vitequakeii_aimfix` | `disabled` / `enabled` |
| Invert Y Axis | `Désactivé` / `Activé` ✅ | `vitequakeii_invert_y_axis` | `disabled` / `enabled` |
| Camera Sensitivity | `0.4` / `0.6` / `0.8` / `1.0` / `1.2` / `1.4` / `1.6` / `1.8` / `2.0` / `2.2` / `2.4` / `2.6` / `2.8` / `3.0` ✅ / `3.2` / `3.4` / `3.6` / `3.8` / `4.0` / `4.2` / `4.4` / `4.6` / `4.8` / `5.0` | `vitequakeii_mouse_sensitivity` | `0.4` / `0.6` / `0.8` / `1.0` / `1.2` / `1.4` / `1.6` / `1.8` / `2.0` / `2.2` / `2.4` / `2.6` / `2.8` / `3.0` / `3.2` / `3.4` / `3.6` / `3.8` / `4.0` / `4.2` / `4.4` / `4.6` / `4.8` / `5.0` |
| Analog Deadzone | `0` / `3` / `5` / `7` / `10` / `13` / `15` ✅ / `17` / `20` / `23` / `25` / `27` / `30` | `vitequakeii_analog_deadzone` | `0` / `3` / `5` / `7` / `10` / `13` / `15` / `17` / `20` / `23` / `25` / `27` / `30` |
| Rumble Effects | `Désactivé` ✅ / `Activé` | `vitequakeii_rumble` | `disabled` / `enabled` |
| Keyboard Mapping: Forwards/Menu Up | `` | `vitequakeii_kb_map_up` | `` |
| Keyboard Mapping: Backwards/Menu Down | `` | `vitequakeii_kb_map_down` | `` |
| Keyboard Mapping: Strafe Left | `` | `vitequakeii_kb_map_left` | `` |
| Keyboard Mapping: Strafe Right | `` | `vitequakeii_kb_map_right` | `` |
| Keyboard Mapping: Show Menu | `` | `vitequakeii_kb_map_menu_show` | `` |
| Keyboard Mapping: Menu Select | `` | `vitequakeii_kb_map_menu_select` | `` |
| Keyboard Mapping: Menu Cancel | `` | `vitequakeii_kb_map_menu_cancel` | `` |
| Keyboard Mapping: Show/Hide Help Computer | `` | `vitequakeii_kb_map_menu_help` | `` |
| Keyboard Mapping: Open/Close Inventory | `` | `vitequakeii_kb_map_inventory_show` | `` |
| Keyboard Mapping: Previous Inventory Item | `` | `vitequakeii_kb_map_inventory_prev` | `` |
| Keyboard Mapping: Next Inventory Item | `` | `vitequakeii_kb_map_inventory_next` | `` |
| Keyboard Mapping: Use Inventory Item | `` | `vitequakeii_kb_map_inventory_use` | `` |
| Keyboard Mapping: Drop Inventory Item | `` | `vitequakeii_kb_map_inventory_drop` | `` |
| Keyboard Mapping: Next Weapon | `` | `vitequakeii_kb_map_weapon_next` | `` |
| Keyboard Mapping: Run | `` | `vitequakeii_kb_map_map_run` | `` |
| Keyboard Mapping: Jump/Climb | `` | `vitequakeii_kb_map_jump` | `` |
| Keyboard Mapping: Crouch/Descend | `` | `vitequakeii_kb_map_crouch` | `` |

## ![](/emulators/external-links.png) Liens externes

* **Code source utilisé** : [https://github.com/libretro/vitaquake2/](https://github.com/libretro/vitaquake2/)
* **Documentation Libretro** : [https://docs.libretro.com/library/vitaquake2/](https://docs.libretro.com/library/vitaquake2/)