---
title: Libretro Flycast
description: 
published: true
date: 2024-07-29T12:37:52.713Z
tags: libretro, naomi, flycast, gd-rom, naomi-gd
editor: markdown
dateCreated: 2023-09-06T06:44:54.998Z
---

**Libretro Flycast** es un emulador **Sega Dreamcast multiplataforma** capaz de emular la **Naomi GD-ROM**.

## ![](/emulators/license.svg) Licencia

Este núcleo está bajo la licencia [**GPLv2**](https://github.com/libretro/flycast/blob/master/LICENSE).

## ![](/emulators/compatibility.png) Compatibilidad

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ❌ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Características

| Característica | Soportado |
| :---: | :---: |
| Reinicio | ✔ |
| Capturas de pantalla | ✔ |
| Guardados | ✔ |
| Opciones del núcleo | ✔ |
| Trucos RetroArch | ✔ |
| Controles | ✔ |
| Remapeo | ✔ |
| Vibración | ✔ |
| Control del disco | ✔ |

## ![](/emulators/bios.svg) BIOS

### Lista de BIOS obligatorios

| Nombre de archivo | Descripción | MD5 | Proveído |
| :---: | :---: | :---: | :---: |
| naomi.zip | Formato MAME a partir del Romset 0.154 | eb4099aeb42ef089cfe94f8fe95e51f6 | ❌ |

### Lista de BIOS opcionales

| Nombre de archivo | Descripción | MD5 | Proveído |
| :---: | :---: | :---: | :---: |
| airlbios.zip | Naomi Airline Pilots deluxe Bios from MAME | 3f348c88af99a40fbd11fa435f28c69d | ❌ |
| hod2bios.zip | Naomi The House of the Dead 2 Bios from MAME | 9c755171b222fb1f4e1439d5b709dbf1 | ❌ |

### Ubicación

Coloca los BIOS de la siguiente manera:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 dc
┃ ┃ ┃ ┃ ┣ 🗒 **naomi.zip**
┃ ┃ ┃ ┃ ┣ 🗒 **airlbios.zip**
┃ ┃ ┃ ┃ ┣ 🗒 **hod2bios.zip**

## ![](/emulators/roms.png) Roms

Flycast se basa en el romset de **Mame**.

### Romset Mame

¡Solo los roms Naomi **provenientes de un romset MAME 0.135 o superior** son compatibles!
Te recomendamos el último romset Mame 0.258 que traerá su lote de compatibilidad adicional.
Para más información sobre la versión del romset en curso: [MameDev](https://www.mamedev.org/release.html).

En Naomi GD-Rom, los roms requieren un archivo `.chd` también proveniente del romset MAME.

>El archivo dat para ordenar tus roms arcade está disponible en la carpeta `/recalbox/share/bios/dc/`
{.is-info}

>Los juegos **Naomi GD-ROM** están en formato `.zip + .chd`
{.is-info}

Ejemplo para el juego **`cfield.zip`**:
Coloca el archivo del juego en la raíz de la carpeta NaomiGD
`/recalbox/share/roms/naomigd/cfield.zip`

Coloca el(los) archivo(s) .chd necesario(s) en una carpeta con el mismo nombre que el juego zip

`/recalbox/share/roms/naomigd/cfield/gdl-0025.chd`

### Ubicación

Coloca los roms de la siguiente manera:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 naomigd
┃ ┃ ┃ ┃ ┣ 🗒 **juego.zip**
┃ ┃ ┃ ┃ ┣ 📁 **juego**
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **juego.chd**

## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

>Para poder conservar tus configuraciones personalizadas durante una actualización, te recomendamos usar nuestra funcionalidad [Sobrecargas de configuración](./../../../advanced-usage/configuration-override).
{.is-info}

### Acceder a las opciones

Puedes configurar diversas opciones de dos maneras diferentes.

* A través del Menú RetroArch:

┣ 📁 Menu RetroArch
┃ ┣ 📁 Opciones del núcleo
┃ ┃ ┣ 🧩 Name_option

* A través del archivo `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Opciones del núcleo

| Opciones | Valor de las opciones | Variable (para sobrecarga) | Valores de las variables |
| --- | --- | --- | --- |
| Tipo de sistema (Reinicio) | `Auto` ✅ / `Dreamcast` / `NAOMI` / `Atomiswave` | `reicast_system` | `auto` / `dreamcast` / `naomi` / `atomiswave` |
| Resolución interna (reinicio) | `320x240` ✅ / `640x480` / `800x600` / `960x720` / `1024x768` / `1280x960` / `1440x1080` / `1600x1200` / `1920x1440` / `2560x1920` / `2880x2160` / `3200x2400` / `3840x2880` / `4480x3360` / `5120x3840` / `5760x4320` / `6400x4800` / `7040x5280` / `7680x5760` / `8320x6240` / `8960x6720` / `9600x7200` / `10240x7680` / `10880x8160` / `11520x8640` / `12160x9120` / `12800x9600` | `reicast_internal_resolution` | `320x240` / `640x480` / `800x600` / `960x720` / `1024x768` / `1280x960` / `1440x1080` / `1600x1200` / `1920x1440` / `2560x1920` / `2880x2160` / `3200x2400` / `3840x2880` / `4480x3360` / `5120x3840` / `5760x4320` / `6400x4800` / `7040x5280` / `7680x5760` / `8320x6240` / `8960x6720` / `9600x7200` / `10240x7680` / `10880x8160` / `11520x8640` / `12160x9120` / `12800x9600` |
| Orientación de la pantalla | `Horizontal` ✅ / `Vertical` | `reicast_screen_rotation` | `horizontal` / `vertical` |
| Ordenación alfa | `Por tira (rápido, menos preciso)` / `Por triángulo (normal)` ✅ | `reicast_alpha_sorting` | `per-strip (fast, least accurate)` / `per-triangle (normal)` |
| Mipmapping | `Habilitado` ✅ / `Deshabilitado` | `reicast_mipmapping` | `enabled` / `disabled` |
| Efectos de niebla | `Habilitado` ✅ / `Deshabilitado` | `reicast_fog` | `enabled` / `disabled` |
| Modificador de volumen | `Habilitado` ✅ / `Deshabilitado` | `reicast_volume_modifier_enable` | `enabled` / `disabled` |
| Truco de pantalla ancha (Reinicio) | `Deshabilitado` ✅ / `Habilitado` | `reicast_widescreen_hack` | `disabled` / `enabled` |
| Trucos de pantalla ancha (Reinicio) | `Deshabilitado` ✅ / `Habilitado` | `reicast_widescreen_cheats` | `disabled` / `enabled` |
| Región | `Predeterminado` ✅ / `Japón` / `USA` / `Europa` | `reicast_region` | `Default` / `Japan` / `USA` / `Europe` |
| Coincidencia DIV | `Deshabilitado` / `Auto` ✅ | `reicast_div_matching` | `disabled` / `auto` |
| Zona muerta del stick analógico | `0%` / `5%` / `10%` / `15%` ✅ / `20%` / `25%` / `30%` | `reicast_analog_stick_deadzone` | `0%` / `5%` / `10%` / `15%` / `20%` / `25%` / `30%` |
| Zona muerta del gatillo | `0%` ✅ / `5%` / `10%` / `15%` / `20%` / `25%` / `30%` | `reicast_trigger_deadzone` | `0%` / `5%` / `10%` / `15%` / `20%` / `25%` / `30%` |
| Gatillos digitales | `Deshabilitado` ✅ / `Habilitado` | `reicast_digital_triggers` | `disabled` / `enabled` |
| Habilitar DSP | `Deshabilitado` / `Habilitado` ✅ | `reicast_enable_dsp` | `disabled` / `enabled` |
| Filtrado anisotrópico | `Off` / `2` / `4` ✅ / `8` / `16` | `reicast_anisotropic_filtering` | `disabled` / `2` / `4` / `8` / `16` |
| Filtro de post-procesamiento PowerVR2 | `Deshabilitado` ✅ / `Habilitado` | `reicast_pvr2_filtering` | `disabled` / `enabled` |
| Escalado de texturas (xBRZ) | `Off` ✅ / `2` / `4` / `6` | `reicast_texupscale` | `disabled` / `2` / `4` / `6` |
| Habilitar búfer RTT (Render To Texture) | `Deshabilitado` ✅ / `Habilitado` | `reicast_enable_rttb` | `disabled` / `enabled` |
| Escalado de Render To Texture | `1x` ✅ / `2x` / `3x` / `4x` / `8x` | `reicast_render_to_texture_upscaling` | `1x` / `2x` / `3x` / `4x` / `8x` |
| Renderizado hilado (Reinicio) | `Deshabilitado` / `Habilitado` ✅ | `reicast_threaded_rendering` | `disabled` / `enabled` |
| Renderizado sincrónico | `Deshabilitado` / `Habilitado` ✅ | `reicast_synchronous_rendering` | `disabled` / `enabled` |
| Retraso en el intercambio de fotogramas | `Deshabilitado` ✅ / `Habilitado` | `reicast_delay_frame_swapping` | `disabled` / `enabled` |
| Salto de fotogramas | `Deshabilitado` ✅ / `1` / `2` / `3` / `4` / `5` / `6` | `reicast_frame_skipping` | `off` / `1` / `2` / `3` / `4` / `5` / `6` |
| Permitir botones de servicio NAOMI | `Deshabilitado` ✅ / `Habilitado` | `reicast_allow_service_buttons` | `disabled` / `enabled` |
| Habilitar dipswitch NAOMI 15KHz | `Deshabilitado` ✅ / `Habilitado` | `reicast_enable_naomi_15khz_dipswitch` | `disabled` / `enabled` |
| Cargar texturas personalizadas | `Deshabilitado` ✅ / `Habilitado` | `reicast_custom_textures` | `disabled` / `enabled` |
| Volcar texturas | `Deshabilitado` ✅ / `Habilitado` | `reicast_dump_textures` | `disabled` / `enabled` |
| Pantalla de mira de pistola 1 | `Deshabilitado` ✅ / `Blanco` / `Rojo` / `Verde` / `Azul` | `reicast_lightgun1_crosshair` | `disabled` / `White` / `Red` / `Green` / `Blue` |
| Pantalla de mira de pistola 2 | `Deshabilitado` ✅ / `Blanco` / `Rojo` / `Verde` / `Azul` | `reicast_lightgun2_crosshair` | `disabled` / `White` / `Red` / `Green` / `Blue` |
| Pantalla de mira de pistola 3 | `Deshabilitado` ✅ / `Blanco` / `Rojo` / `Verde` / `Azul` | `reicast_lightgun3_crosshair` | `disabled` / `White` / `Red` / `Green` / `Blue` |
| Pantalla de mira de pistola 4 | `Deshabilitado` ✅ / `Blanco` / `Rojo` / `Verde` / `Azul` | `reicast_lightgun4_crosshair` | `disabled` / `White` / `Red` / `Green` / `Blue` |

## ![](/emulators/external-links.png) Enlaces externos

* **Código fuente utilizado**: [https://github.com/libretro/flycast](https://github.com/libretro/flycast)
* **Documentación Libretro**: [https://docs.libretro.com/library/flycast/](https://docs.libretro.com/library/flycast/)
* **Código fuente oficial**: [https://github.com/flyinghead/flycast](https://github.com/flyinghead/flycast)
