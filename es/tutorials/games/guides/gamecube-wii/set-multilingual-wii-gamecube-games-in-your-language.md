---
title: Poner los juegos Wii/GameCube multi-lenguajes con el idioma que queréis
description: 
published: true
date: 2023-09-20T09:19:40.617Z
tags: gamecube, wii, juegos, idioma, lengua, multi-lenguas, multi-idiomas
editor: markdown
dateCreated: 2023-09-20T09:19:40.617Z
---

## Introducción

En los juegos con varios idiomas, no se puede elegir el idioma al arranque del juego porque se encuentra preconfigurado en el emulador.

## Uso general

Para cambiar los juegos al idioma que queréis, seguid estos pasos:

* Lanzad un juego de GameCube o Wii.
* Con el teclado, pulsad `Alt + F4`.
* Con el ratón, id a "Configurar".

![](/tutorials/games/guides/gamecube-wii/change-games-language/dolphinlanguage1.png)

## {.tabset}

### Para los juegos Gamecube

* En la nueva ventana, pulsad `GameCube` arriba del todo.
* Modificad la opción `Idioma del sistema`.

![](/tutorials/games/guides/gamecube-wii/change-games-language/dolphinlanguage2.png){.full-width}

* Validad y quitad el emulador.

### Para los juegos Wii

* En la nueva ventana, pulsad `Wii` arriba del todo.
* Modificad la opción `Idioma del sistema`.

![](/tutorials/games/guides/gamecube-wii/change-games-language/dolphinlanguage3.png){.full-width}

* Validad y quitad el emulador.
