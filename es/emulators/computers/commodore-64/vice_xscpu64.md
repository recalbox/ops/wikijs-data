---
title: Vice_xscpu64
description: 
published: true
date: 2024-07-26T07:10:12.652Z
tags: commodore-64, c64, vice, xscpu64, 9.1+
editor: markdown
dateCreated: 2024-07-26T07:10:12.652Z
---

## ![](/emulators/license.svg) Licencia

Este núcleo está bajo la licencia [**GPLv2**](https://sourceforge.net/p/vice-emu/code/HEAD/tree/trunk/vice/COPYING).

## ![](/emulators/compatibility.png) Compatibilidad

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Funcionalidades

## ![](/emulators/bios.svg) BIOS

### Lista de BIOS opcionales

| Nombre de archivo | Descripción | MD5 | Proporcionado |
| :---: | :---: | :---: | :---: |
| scpu-dos-1.4.bin | CMD SuperCPU Kernal 1.4 | cda2fcd2e1f0412029383e51dd472095 | ❌ |
| scpu-dos-2.04.bin | CMD SuperCPU Kernal 2.04 | b2869f8678b8b274227f35aad26ba509 | ❌ |

### Ubicación

Coloque los BIOS de esta manera:

┣ 📁 recalbox  
┃ ┣ 📁 share  
┃ ┃ ┣ 📁 bios  
┃ ┃ ┃ ┣ 📁 vice  
┃ ┃ ┃ ┃ ┣ 📁 SCPU64  
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **scpu-dos-1.4.bin**  
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **scpu-dos-2.04.bin**  

## ![](/emulators/roms.png) Roms

### Extensiones soportadas

Las roms deben tener las siguientes extensiones:

* .bin
* .cmd
* .crt
* .d1m
* .d2m
* .d4m
* .d64
* .d6z
* .d67
* .d71
* .d7z
* .d80
* .d81
* .d82
* .d8z
* .d90
* .dhd
* .g41
* .g4z
* .g64
* .g6z
* .m3u
* .nbz
* .nib
* .p00
* .p64
* .prg
* .t64
* .tap
* .tcrt
* .vfl
* .vsf
* .x64
* .x6z
* .zip
* .7z
* .gz

Este sistema soporta roms comprimidas en formato .zip/.7z. Sin embargo, esto solo es un archivo.

Los archivos contenidos en los .zip/.7z deben corresponder a las extensiones mencionadas anteriormente.  
Además, cada archivo .zip/.7z debe contener solo una rom comprimida.

### Ubicación

Coloque las roms de esta manera:

┣ 📁 recalbox  
┃ ┣ 📁 share  
┃ ┃ ┣ 📁 roms  
┃ ┃ ┃ ┣ 📁 c64  
┃ ┃ ┃ ┃ ┣ 🗒 **juego.zip**  

>Las roms en formato **No-Intro** son altamente recomendadas.
{.is-success}

>Para más información sobre las roms, consulte [este tutorial](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/external-links.png) Enlaces externos

* **Código fuente utilizado**: [https://sourceforge.net/projects/vice-emu/](https://sourceforge.net/projects/vice-emu/)
