---
title: Quake 1
description: 
published: true
date: 2021-08-08T23:46:24.852Z
tags: ports, quake 1
editor: markdown
dateCreated: 2021-06-29T18:50:21.926Z
---

![](/emulators/ports/quake.gif){.align-center}

## Technical data

* **Created by**: id Software
* **Year of release**: 1996
* **Genre**: First person shooter
* **Engine**: Quake Engine
* **Latest version**: v1.09
* **Operating system**: MS-DOS 5.0 (or higher), Win95/98
* **CPU**: Pentium minimum
* **Storage**: 16 MB on MS-DOS, 24MB on Win95/98 + CD-Rom player 2x (no player = no sound track)
* **RAM**: 8 MB MS-DOS or 16MB Win95/98 minimum
* **Resolution**: 320x200 pixels minimum
* **Sound**: Sound Blaster and AdLib or 100% compatible sound card
* **Developer**: id Software
* **Editor**: GT Interactive
* **Designers**: John Romero, American McGee, Sandy Petersen, Tim Willits
* **Programmers**: John Carmack, Michael Abrash, John Cash
* **Artists**: Adrian Carmack, Kevin Cloud, Paul Steed
* **Compositors**: Trent Reznor, Nine Inch Nails
* **PEGI**: 16

## Presentation

Quake takes place in a world where the government, after several years of research and development, develops a prototype that makes teleportation possible. However, this prototype is corrupted by an enemy, known as Quake, who uses the prototype to send his nightmarish creatures into the human dimension, preparing a possible invasion of Earth. The government sends a team of fighters through the portal to eliminate this threat. The player takes on the role of an anonymous soldier, the only survivor of the operation. From a still secure base, he can access, via portals, four dimensions controlled by the enemy. Each dimension allows the player to acquire one of the four magical runes of Shub-Niggurath, Quake's lieutenant in charge of invading the Earth. Collecting all four runes allows access to the dimension where the demon is hiding.

## Emulators

[Libretro TyrQuake](libretro-tyrquake)