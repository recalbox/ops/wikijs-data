---
title: Naomi
description: 
published: true
date: 2024-07-29T12:08:33.621Z
tags: arcade, naomi
editor: markdown
dateCreated: 2023-09-01T21:12:46.248Z
---

![](/emulators/arcade/naomi.svg){.align-center}

## Ficha técnica

* **Procesador** : Hitachi SH-4 (128 bits) RISC 360 MIPS
* **Vídeo** : Procesador PowerVR 2 a 100 MHz fabricado por Nec
* **Audio** : Procesador ARM7 Yamaha XG AICA RISC 32 bits
* **RAM** : 32 Mo1,2
* **RAM de vídeo** : 16 Mo1,2
* **RAM de audio** : 8 Mo1,2
* **Soporte** : Cartucho (PCB de ROMS)

## Presentación

El sistema Naomi se lanzó bajo el nombre de **Naomi Multi System** (también llamado **Naomi Multiboard**), acrónimo de "_New Arcade Operation Machine Idea"_, literalmente "nueva idea de máquina de arcade"; Naomi también se traduce como "belleza" en japonés.

El sistema cuenta con una arquitectura **muy cercana a la de la consola Dreamcast**: un microprocesador Hitachi SH-4 a 200 MHz, un procesador gráfico PowerVR Series 2 GPU (PVR2DC) y un procesador de sonido Yamaha AICA; pero con **más memoria RAM y memoria de vídeo** (dos veces más que la Dreamcast). Los juegos se almacenan en **un cartucho** (Naomi) **o un GD-ROM** (NaomiGD)**.**

Producido por SEGA, líder del arcade en esa época, para competir con el System 23 de NAMCO, su comercialización comenzó en 1998 con **The House of the Dead 2** como primer juego hasta el año 2006. Más de 160 juegos saldrán en este hardware (el récord mundial del número de juegos en un hardware de arcade).

Sega anuncia que su **Naomi 2** sigue siendo compatible con los **cartuchos y GD-Rom Naomi 1.**

## Emuladores

[Libretro Flycast](libretro-flycast)
[Libretro Flycast-Next](libretro-flycast-next)
