---
title: Lutro
description: Lua engine
published: true
date: 2024-07-26T08:22:21.550Z
tags: fantasy, lutro, consolas, lua
editor: markdown
dateCreated: 2023-09-04T06:59:26.854Z
---

![](/emulators/fantasy/lutro.svg){.align-center}

## Datos técnicos

* **Fabricante**: -
* **Año de lanzamiento**: -

## Presentación

Lutro es un framework experimental de juegos en Lua para Libretro que sigue la API de LÖVE.

Lutro es un renderizador e implementa sólo un subconjunto de la API de LÖVE. Su objetivo es la portabilidad a través de la API de Libretro y se basa en dependencias.

## Emuladores

[Libretro Lutro](libretro-lutro)