---
title: Prince of Persia
description: 
published: true
date: 2024-07-29T14:51:09.364Z
tags: ports, prince of persia
editor: markdown
dateCreated: 2024-07-13T09:47:23.587Z
---

![](/emulators/ports/prince-of-persia.svg){.align-center}

## Ficha técnica

* **Creado por:** Brøderbund
* **Año de lanzamiento:** 1989
* **Género:** Plataformas, Die and retry
* **Sistema operativo:** Amiga 500, Amstrad CPC, Atari ST, Commodore 64, DOS y ZX Spectrum.
* **Desarrollador:** Jordan Mechner
* **Editor:** Brøderbund
* **PEGI:** 3

## Presentación

Prince of Persia es una serie de videojuegos de acción/plataformas que empezo en 1989 con Prince of Persia, desarrollado por Jordan Mechner para Brøderbund. Con el éxito del juego, Brøderbund produjo una secuela que apareció en 1993, luego Red Orb Entertainment desarrolló la primera entrega en 3D que salió en 1999, pero recibió malas críticas. En 2003, cuando la licencia estaba destinada a ser abandonada, Ubisoft y su estudio en Montreal lanzaron Prince of Persia: Las Arenas del Tiempo, que tuvo un gran éxito, especialmente en consolas de salón. Este juego abrió el camino a la trilogía llamada "Las Arenas del Tiempo" desarrollada bajo la dirección de Yannis Mallat. Otros juegos siguieron posteriormente, siendo el último juego importante Prince of Persia: Las Arenas Olvidadas (2010).

En abril de 2008, Ubisoft afirmó que la serie había vendido más de 20 millones de copias. En 2010, una superproducción de Hollywood titulada Prince of Persia: Las Arenas del Tiempo de Walt Disney Pictures, dirigida por Mike Newell, se estrenó en cines.

## Emuladores

[SDLPoP](sdlpop)
