---
title: Libretro SameBoy
description: 
published: true
date: 2024-07-24T23:46:58.025Z
tags: libretro, color, gameboy, sameboy, gbc
editor: markdown
dateCreated: 2021-07-14T06:40:20.421Z
---

**Libretro SameBoy** is an extremely accurate open source Gameboy (DMG) and Gameboy Color (CGB) emulator written in C.

* Supports Game Boy (DMG); Game Boy Color (CGB) and GBC-Mode emulation; Game Boy Advance (AGB)
* Supports accurate high-level emulation of Super Game Boy (SGB; NTSC and PAL) and Super Game Boy 2 (SGB2)
* Allows you to choose the model you want to emulate regardless of ROM
* High quality 96 kHz audio
* Support for battery saving
* Save states
* Includes open source boot ROMs for all emulated models:

  * Full support for (and documentation of) all game-specific palettes in the CGB / AGB boot ROM, for accurate emulation of Game Boy games on a Game Boy Color
  * Supports manual palette selection with key combinations, with 4 additional new palettes (direction + A or B)
  * Supports palette selection in a CGB game, forcing it to run in "palletized" DMG mode, if the ROM allows it
  * Support for games with a non-Nintendo logo in the header
  * No long animation in the DMG boot ROM

* Four color correction settings
* Three high-pass audio filter settings
* Real-time clock emulation
* Turbo, rewind and slow motion modes
* Extremely high accuracy
* Link cable emulation

## ![](/emulators/license.svg) License

This core is under [**MIT**](https://github.com/libretro/SameBoy/blob/master/LICENSE) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Features

| Feature | Supported |
| :---: | :---: |
| Restart | ✔ |
| Screenshots | ✔ |
| Saves | ✔ |
| States | ✔ |
| Rewind | ✔ |
| Netplay | ✔ |
| Core Options | ✔ |
| RetroAchievements | ✔ |
| Controls | ✔ |
| Remapping | ✔ |
| Rumble | ✔ |
| Subsystem | ✔ |

## ![](/emulators/bios.svg) BIOS

### List of optional bios

| Filename | Description | MD5 | Provided |
| :---: | :---: | :---: | :---: |
| cgb_boot.bin | Game Boy Color BIOS | b560efd759d87634a03b243f22bba27a | ✅ |

### Location

Put your bios like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 gbc
┃ ┃ ┃ ┃ ┣ 🗒 **cgb_boot.bin**

## ![](/emulators/roms.png) Roms

### Supported extensions

Roms must have the extension:

* .bin
* .gbc
* .zip
* .7z

This system supports compressed roms in .zip/.7z format. However, be careful, it is only an archive.

The files contained in the .zip/.7z must correspond to the extensions mentioned above.
Moreover, each .zip/.7z file must contain only one compressed rom.

### Location

Put your roms like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 gbc
┃ ┃ ┃ ┃ ┣ 🗒 **game.zip**

>The roms using **No-Intro** format are highly recommended.
{.is-success}

>For more information about roms, go to [this tutorial](./../../../tutorials/games/generalities/isos-and-roms)!
{.is-info}

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Accessing to options

You can configure various options in two different ways.

* Via RetroArch menu:

┣ 📁 RetroArch Menu
┃ ┣ 📁 Core options
┃ ┃ ┣ 🧩 Name_option

* Via the file `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core options

Use this table for single player:

| Options | Options values | Variables (for configuration override) | Variables values |
| --- | --- | --- | --- |
| Emulated Model (Requires Restart) | `Auto Detect DMG/CGB` ✅ / `Auto Detect DMG/SGB/CGB` / `Game Boy (DMG-CPU B)` / `Game Boy Color (CPU-CGB C) (Experimental)` / `Game Boy Color (CPU-CGB E)` / `Game Boy Advance` / `Super Game Boy NTSC` / `Super Game Boy PAL` /`Super Game Boy 2` | `sameboy_model` | `Auto` / `Auto (SGB)` / `Game Boy` / `Game Boy Color C` / `Game Boy Color` / `Game Boy Advance` / `Super Game Boy` / `Super Game Boy PAL` / `Super Game Boy 2` |
| Auto Detected SGB Model (Requires Restart) | `Super Game Boy NTSC` ✅ / `Super Game Boy PAL` / `Super Game Boy 2` | `sameboy_auto_sgb_model` | `Super Game Boy` / `Super Game Boy PAL` / `Super Game Boy 2` |
| Real Time Clock Emulation | `Sync to System Clock` ✅ / `Accurate` | `sameboy_rtc` | `sync to system clock` / `accurate` |
| GB Mono Palette | `Greyscale` ✅ / `Lime (Game Boy)` / `Olive (Game Boy Pocket)` / `Teal (Game Boy Light)` | `sameboy_mono_palette` | `greyscale` / `lime` / `olive` / `teal` |
| GBC Color Correction | `Modern - Balanced` ✅ / `Modern - Accurate` / `Modern - Boost Contrast` / `Reduce Contrast` / `Correct Color Curves` / `Harsh Reality (Low Contrast)` / `Disabled` | `sameboy_color_correction_mode` | `emulate hardware` / `accurate` / `preserve brightness` / `reduce contrast` / `correct curves` / `harsh reality` / `off` |
| Ambient Light Temperature | `1000K (Warmest)` / `1550K` / `2100K` / `2650K` / `3200K` / `3750K` / `4300K` / `4850K` / `5400K` / `5950K` / `6500K (Neutral White)` ✅ / `7050K` / `7600K` / `8150K` / `8700K` / `9250K` / `9800K` / `10350K` / `10900K` / `11450K` / `12000K (Coolest)` | `sameboy_light_temperature` | `1.0` / `0.9` / `0.8` / `0.7` / `0.6` / `0.5` / `0.4` / `0.3` / `0.2` / `0.1` / `0` / `-0.1` / `-0.2` / `-0.3` / `-0.4` / `-0.5` / `-0.6` / `-0.7` / `-0.8` / `-0.9` / `-1.0` |
| Display border | `Always` / `Only for Super Game Boy` ✅ / `Disabled` | `sameboy_border` | `always` / `Super Game Boy only` / `never` |
| Highpass Filter | `Accurate` ✅ / `Preserve Waveform` / `Disabled` | `sameboy_high_pass_filter_mode` | `accurate` / `remove dc offset` / `off` |
| Interference Volume | `0%` ✅ / `5%` / `10%` / `15%` / `20%` / `25%` / `30%` / `35%` / `40%` / `45%` / `50%` / `55%` / `60%` / `65%` / `70%` / `75%` / `80%` / `85%` / `90%` / `95%` / `100%` | `sameboy_audio_interference` | `0` / `5` / `10` / `15` / `20` / `25` / `30` / `35` / `40` / `45` / `50` / `55` / `60` / `65` / `70` / `75` / `80` / `85` / `90` / `95` / `100` |
| Rumble Mode | `Always` / `Only for rumble-enabled games` ✅ / `Never` | `sameboy_rumble` | `all games` / `rumble-enabled games` / `never` |

Use this table for linked players:

| Options | Options values | Variables (for configuration override) | Variables values |
| --- | --- | --- | --- |
| Link cable emulation | `Activé` ✅ / `Disabled` | `sameboy_link` | `enabled` / `disabled` |
| Screen Layout | `Top-Down` ✅ / `Left-Right` | `sameboy_screen_layout` | `top-down` / `left-right` |
| Audio Output | `Game Boy #1` ✅ / `Game Boy #2` | `sameboy_audio_output` | `GameBoy #1` / `GameBoy #2` |
| Emulated model for Game Boy #1 | `Auto Detect DMG/CGB` ✅ / `Auto Detect DMG/SGB/CGB` / `Game Boy (DMG-CPU B)` / `Game Boy Color (CPU-CGB C) (Experimental)` / `Game Boy Color (CPU-CGB E)` / `Game Boy Advance` / `Super Game Boy NTSC` / `Super Game Boy PAL` /`Super Game Boy 2` | `sameboy_model_1` | `Auto` / `Auto (SGB)` / `Game Boy` / `Game Boy Color C` / `Game Boy Color` / `Game Boy Advance` / `Super Game Boy` / `Super Game Boy PAL` / `Super Game Boy 2` |
| Auto Detected SGB Model for Game Boy #1 (Requires Restart) | `Super Game Boy NTSC` ✅ / `Super Game Boy PAL` / `Super Game Boy 2` | `sameboy_auto_sgb_model_1` | `Super Game Boy` / `Super Game Boy PAL` / `Super Game Boy 2` |
| Emulated model for Game Boy #2 | `Auto Detect DMG/CGB` ✅ / `Auto Detect DMG/SGB/CGB` / `Game Boy (DMG-CPU B)` / `Game Boy Color (CPU-CGB C) (Experimental)` / `Game Boy Color (CPU-CGB E)` / `Game Boy Advance` / `Super Game Boy NTSC` / `Super Game Boy PAL` /`Super Game Boy 2` | `sameboy_model_2` | `Auto` / `Auto (SGB)` / `Game Boy` / `Game Boy Color C` / `Game Boy Color` / `Game Boy Advance` / `Super Game Boy` / `Super Game Boy PAL` / `Super Game Boy 2` |
| Auto Detected SGB Model for Game Boy #2 (Requires Restart) | `Super Game Boy NTSC` ✅ / `Super Game Boy PAL` / `Super Game Boy 2` | `sameboy_auto_sgb_model_2` | `Super Game Boy` / `Super Game Boy PAL` / `Super Game Boy 2` |
| GB Mono Palette for Game Boy #1 | `Greyscale` ✅ / `Lime (Game Boy)` / `Olive (Game Boy Pocket)` / `Teal (Game Boy Light)` | `sameboy_mono_palette_1` | `greyscale` / `lime` / `olive` / `teal` |
| GB Mono Palette for Game Boy #2 | `Greyscale` ✅ / `Lime (Game Boy)` / `Olive (Game Boy Pocket)` / `Teal (Game Boy Light)` | `sameboy_mono_palette_2` | `greyscale` / `lime` / `olive` / `teal` |
| GBC Color Correction for Game Boy #1 | `Modern - Balanced` ✅ / `Modern - Accurate` / `Modern - Boost Contrast` / `Reduce Contrast` / `Correct Color Curves` / `Harsh Reality (Low Contrast)` / `Disabled` | `sameboy_color_correction_mode_1` | `emulate hardware` / `accurate` / `preserve brightness` / `reduce contrast` / `correct curves` / `harsh reality` / `off` |
| GBC Color Correction for Game Boy #2 | `Modern - Balanced` ✅ / `Modern - Accurate` / `Modern - Boost Contrast` / `Reduce Contrast` / `Correct Color Curves` / `Harsh Reality (Low Contrast)` / `Disabled` | `sameboy_color_correction_mode_2` | `emulate hardware` / `accurate` / `preserve brightness` / `reduce contrast` / `correct curves` / `harsh reality` / `off` |
| Ambient Light Temperature for Game Boy #1 | `1000K (Warmest)` / `1550K` / `2100K` / `2650K` / `3200K` / `3750K` / `4300K` / `4850K` / `5400K` / `5950K` / `6500K (Neutral White)` ✅ / `7050K` / `7600K` / `8150K` / `8700K` / `9250K` / `9800K` / `10350K` / `10900K` / `11450K` / `12000K (Coolest)` | `sameboy_light_temperature_1` | `1.0` / `0.9` / `0.8` / `0.7` / `0.6` / `0.5` / `0.4` / `0.3` / `0.2` / `0.1` / `0` / `-0.1` / `-0.2` / `-0.3` / `-0.4` / `-0.5` / `-0.6` / `-0.7` / `-0.8` / `-0.9` / `-1.0` |
| Ambient Light Temperature for Game Boy #2 | `1000K (Warmest)` / `1550K` / `2100K` / `2650K` / `3200K` / `3750K` / `4300K` / `4850K` / `5400K` / `5950K` / `6500K (Neutral White)` ✅ / `7050K` / `7600K` / `8150K` / `8700K` / `9250K` / `9800K` / `10350K` / `10900K` / `11450K` / `12000K (Coolest)` | `sameboy_light_temperature_2` | `1.0` / `0.9` / `0.8` / `0.7` / `0.6` / `0.5` / `0.4` / `0.3` / `0.2` / `0.1` / `0` / `-0.1` / `-0.2` / `-0.3` / `-0.4` / `-0.5` / `-0.6` / `-0.7` / `-0.8` / `-0.9` / `-1.0` |
| Highpass Filter for Game Boy #1 | `Accurate` ✅ / `Preserve Waveform` / `Disabled` | `sameboy_high_pass_filter_mode_1` | `accurate` / `remove dc offset` / `off` |
| Highpass Filter for Game Boy #2 | `Accurate` ✅ / `Preserve Waveform` / `Disabled` | `sameboy_high_pass_filter_mode_2` | `accurate` / `remove dc offset` / `off` |
| Interference Volume for Game Boy #1 | `0%` ✅ / `5%` / `10%` / `15%` / `20%` / `25%` / `30%` / `35%` / `40%` / `45%` / `50%` / `55%` / `60%` / `65%` / `70%` / `75%` / `80%` / `85%` / `90%` / `95%` / `100%` | `sameboy_audio_interference_1` | `0` / `5` / `10` / `15` / `20` / `25` / `30` / `35` / `40` / `45` / `50` / `55` / `60` / `65` / `70` / `75` / `80` / `85` / `90` / `95` / `100` |
| Interference Volume for Game Boy #2 | `0%` ✅ / `5%` / `10%` / `15%` / `20%` / `25%` / `30%` / `35%` / `40%` / `45%` / `50%` / `55%` / `60%` / `65%` / `70%` / `75%` / `80%` / `85%` / `90%` / `95%` / `100%` | `sameboy_audio_interference_2` | `0` / `5` / `10` / `15` / `20` / `25` / `30` / `35` / `40` / `45` / `50` / `55` / `60` / `65` / `70` / `75` / `80` / `85` / `90` / `95` / `100` |
| Rumble Mode for Game Boy #1 | `Always` / `Only for rumble-enabled games` ✅ / `Never` | `sameboy_rumble_1` | `rumble-enabled games` / `all games` / `never` |
| Rumble Mode for Game Boy #2 | `Always` / `Only for rumble-enabled games` ✅ / `Never` | `sameboy_rumble_2` | `rumble-enabled games` / `all games` / `never` |

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/libretro/SameBoy/](https://github.com/libretro/SameBoy/)
* **Libretro documentation**: [https://docs.libretro.com/library/sameboy/](https://docs.libretro.com/library/sameboy/)
* **Official source code**: [https://github.com/LIJI32/SameBoy](https://github.com/LIJI32/SameBoy/)
* **Official website**: [https://sameboy.github.io](https://sameboy.github.io)