---
title: Prince of Persia
description: 
published: true
date: 2024-06-23T12:19:45.140Z
tags: ports, prince of persia
editor: markdown
dateCreated: 2024-06-23T12:19:45.140Z
---

![](/emulators/ports/prince-of-persia.svg){.align-center}

## Fiche technique

* **Crée par :** Brøderbund
* **Année de sortie :** 1989
* **Genre :** Plates-formes, Die and retry
* **Système d'exploitation :** Amiga 500, Amstrad CPC, Atari ST, Commodore 64, DOS et ZX Spectrum.
* **Développeur** : Jordan Mechner
* **Éditeur** : Brøderbund
* **PEGI** : 3

## Présentation

Prince of Persia est une série de jeux vidéo d'action/plates-formes commencée en 1989 avec Prince of Persia, développé par Jordan Mechner pour Brøderbund. Avec le succès du jeu, Brøderbund produit une suite qui paraît en 1993, puis Red Orb Entertainment développe le premier opus en 3D qui sort en 1999 mais qui reçoit de mauvaises critiques. En 2003, alors que la licence est destinée à être abandonnée, Ubisoft et son studio de Montréal sortent Prince of Persia : Les Sables du Temps qui connaît un fort succès, notamment sur consoles de salon. Ce jeu ouvre la voie à la trilogie dite des « Sables du temps » développée sous la direction de Yannis Mallat. D'autres jeux suivront par la suite, le dernier jeu majeur étant Prince of Persia : Les Sables oubliés (2010).

En avril 2008, Ubisoft affirme que la série s'est vendue à plus de 20 millions d'exemplaires. En 2010, une superproduction hollywoodienne intitulée Prince of Persia : Les Sables du Temps de Walt Disney Pictures, réalisée par Mike Newell, sort au cinéma.

## Émulateurs

[SDLPoP](sdlpop)