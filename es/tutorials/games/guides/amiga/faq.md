---
title: FAQ
description: 
published: true
date: 2024-07-27T22:29:58.475Z
tags: faq
editor: markdown
dateCreated: 2023-09-19T12:49:11.796Z
---

 ## Systèmes
 
 Las máquinas Amiga se han dividido en 4 sistemas:
 
 * Amiga 600 : todas las máquinas `OCS`/`ECS` del Amiga 1000 hasta el último Amiga 600.
 * Amiga 1200 : todas las máquinas `AGA`, a partir del Amiga 1200.
 * Amiga CD32 : la consola Amiga CD32.
 * Amiga CDTV : la consola Amiga CDTV.
 
## Extensiones

He aquí una lista con varias explicaciones sobre los diferentes formatos de fichero en los sistemas Amiga:

* Amiga 600 y Amiga 1200 :

  * Discos **AFD** (`*.adf`), el formato de disco **más popular**. **No incorporan** protección de disco (**pueden comprimirse o 7-zippearse**). 
  * Discos **IPF** (`*.ipf`), más utilizados **por copias de juegos sin intro** (_pueden incorporar protección de disco). **Pueden comprimirse. 
  * **WHD** (carpetas, `*.lha/lzh/lzx`, `*.zip`), el conocido formato para juegos en disco duro. Las carpetas WHD se reconocen utilizando el archivo *.slave dentro de la carpeta, pero es _fuertemente_ recomendable mantenerlas empaquetadas como lha o zip. Los WHDs sin empaquetar no son más rápidos y lo que es peor, pueden hacer que EmulationStation sea superlenta. 
  * **HDD FS**, "Disco duro en sistema de archivos". Debe ser una carpeta que termine en `.hd`. Raramente utilizado. Para verdaderos fans de Amiga que han hecho una copia de seguridad del disco duro de su máquina. Se puede comprimir, pero no se recomienda. 
  * **HDF** (`*.hdf`), Imagen de disco en un único archivo. Puede comprimirse (¡Sólo lectura!) 
  * **RP9** (`*.rp9`), Paquetes todo en uno de Amiga Forever.
 
Cuando jugáis a partir de ficheros comprimidos/7-zip, el sistema intenta identificar el tipo de rom buscando extensiones específicas dentro del mismo.

* Las extensiones `ADF`/`IPF`/`BIN`/`ISO` se identifican rápidamente. 
* La extensión `WHD` es rápido, pero LHA debería ser la opción preferida. 
* La extensión `HDDFS` puede tardar más en identificarse y puede dar lugar a interpretaciones erróneas. Si utilizáis `HDDFS`, dejadlos como ficheros y carpetas normales sin comprimir y terminad el nombre de la carpeta raíz con `.hd` para facilitar la identificación.

## Comandos especiales con Amiberry

### Al teclado

* Entrar en la interfaz : `F12`.
* Salir : `F12`, bajar hasta `Quit` y aceptar con `ENTER`.

### Con el mando

* Entrar en la interfaz : `HK` + `B`.
* Salir : `HK` + `Start`.

## Sobrecargas de configuración

>Las sobrecargas de configuración en los núcleos Libretro son [configuration overloads](../../../../advanced-usage/configuration-override). Aquí sólo se tratará Amiberry.
{.is-info}

Como se mencionó anteriormente, se ha hecho una gran cantidad de trabajo para configurar automáticamente los emuladores de acuerdo a lo que desea iniciar.

Recalbox genera una configuración por sistema. Sin embargo, en ciertas situaciones, puedes sobrecargar estas configuraciones.

Los ficheros de configuración se generan en `/tmp/amiga/conf`. El más importante de ellos es el fichero `uaeconfig.uae`, en el que encontrarás todas las claves que definen la máquina, los discos duros y demás.

Si sois curiosos, podéis echar un vistazo a la documentación de Amiberry.

Tenéis 2 formas diferentes de sobrecargar las configuraciones, que se muestran a continuación con un ejemplo para cada una de ellas:

### Por sistema

Para hacer esto, hay que crear un fichero en la carpeta `/recalbox/share/system/configs/amiberry/`. Este fichero se llamará según el sistema:

* `amiga600.uae` para el Amiga 600
* `amiga1200.uae` para el Amiga 1200
* `amigacd32.uae` para el Amiga CD32
* `amigacdtv.uae` para ignorar el Amiga CDTV

Por ejemplo, para desactivar los LED de la esquina inferior derecha, podéis utilizar esta línea:

```txt
show_leds=false
```

### Por juego

Para ello, hay crear un fichero en la carpeta roms donde se encuentre el juego a sobrecargar, que termine con la extensión `.uae`.

Por ejemplo, dentro de vuestra carpeta `/recalbox/share/roms/amiga1200`, para el juego `Aladdin (AGA).ipf`, podéis crear el fichero `Aladdin (AGA).uae` y colocar las sobrecargas dentro del mismo.

Las sobrecargas se gestionan del siguiente modo:

* Primero, se genera la configuración base.
* Luego, si existe una sobrecarga para el sistema, las entradas encontradas en este fichero de sobrecarga sobrescribirán las entradas de la configuración de base.
* Por último, si existe una sobrecarga para el juego que se ejecuta, las entradas que se encuentran en dicho fichero de sobrecarga sobrescribirán las entradas de la configuración de base **y también** las entradasd de la sobrecarga del sistema.

## Funcionamiento de los juegos ADF

Copiadlos dentro de la carpeta de roms Amiga que corresponde al sistema.

Los discos múltiples se carga automáticamente, hasta un máximo de 4 discos para todos aquellos juegos que utilicen una nomenclatura correcta como `(Disk 1 of X)`.

## Funcionamiento de los juegos WHD

**Los juegos WHD** son **un poco más difíciles** pero nada demasiado complicado.

### Instalación

Descomprime tu juego WHDLoad, borra el archivo `.info` al mismo nivel que la carpeta, y copia sólo esta carpeta a tu carpeta de roms (puedes usar subcarpetas si lo deseas; por ejemplo `/recalbox/share/roms/amiga1200/ShootEmUp`).

### Utilización

Necesitará un archivo `.uae` al mismo nivel que la carpeta del mismo nombre, por lo que hay dos soluciones:

* Cread uno vacío. 
* Usad un fichero personalizado que os permitirá modificar la configuración del emulador.

Para la segunda opción, existe un pequeño programa llamado [UCG](https://github.com/solarmon/UCG). Este programa os permite generar una configuración UAE.

### Modificación de juegos WHDL

#### Edición de ficheros .UAE

En el fichero uae, podéis definir bloques `;hardware`, `;controls` y `;graphics` para reemplazar la configuración estándar del emulador.

**Las partes personalizadas de la configuración en el fichero .uae sólo se utilizarán si empiezan con el nombre de bloque correcto:** `;hardware`**,** `;controls` **o** `;graphics`

Esto os permite utilizar dos joysticks, tener una configuración de hardware muy específica para ciertos juegos que son un poco especiales, o definir una resolución personalizada.

Esto os permite, por ejemplo, utilizar dos joysticks, tener una configuración de hardware muy específica para ciertos juegos que son un poco especiales/caprichosos o definir una resolución personalizada.

También podéis borrar todo lo que haya en el fichero uae y en este caso se utilizará la configuración básica del emulador.

Copiad este fichero (correspondiente al modelo de Amiga que queréis utilizar) junto a la carpeta del juego de la BWD y renombradlo exactamente como la carpeta del juego (manteniendo la extensión `.uae`) p.e. si el nombre de la carpeta del juego es `Rick Dangerous`, el fichero uae debería llamarse `Rick Dangerous.uae` y estar al mismo nivel que la carpeta.

### Cambiar la secuencia de inicio (sólo para expertos)

La secuencia de inicio estándar generada para los juegos WHDL es `WHDload game.slave Preload` (en `WHDL/S/Startup-Sequence`). Pero algunos juegos WHDL (como `History Line: 1914-1918`) pueden requerir parámetros adicionales en el arranque (en caso de ignorar estos parámetros adicionales, se bloquearán).

Podéis añadir un segundo fichero (completamente opcional) con estos parámetros adicionales. Tomando History Line como ejemplo, tendríais :

* HistoryLine (carpeta del juego WHDL) 
* HistoryLine.uae (que puede estar vacío o personalizado) 
* HistoryLine.whdl (opcional, contiene parámetros adicionales `CUSTOM1=1`)

A continuación, se podrá lanzar el juego.

Aquí hay una pequeña lista de juegos que requieren parámetros adicionales.

| Juego | Parámetro(s) extra(s) / Contenido del fichero |
| :--- | :--- |
| HistoryLine 1914-1918 | `CUSTOM1=1` para la intro, `CUSTOM1=2` para el juego |
| The Settlers / Die Siedlers | `CUSTOM1=1` saltar la intro |