---
title: MSX Turbo R
description: 
published: true
date: 2024-07-26T12:48:36.738Z
tags: msx-turbo-r, ordenadores
editor: markdown
dateCreated: 2023-09-02T18:52:39.785Z
---

![](/emulators/computers/msxturbor.svg){.align-center}

## Datos técnicos

* **Fabricante**: Panasonic
* **Año de lanzamiento**: 1990
* **Sistema operativo**: MSX BASIC V4.0 (32 kB)
* **Procesador**: R800 (DAR800-X0G) @ 28.64 MHz y Z80A @ 3.58
* **RAM**: 256 KB (FS-A1ST) o 512 KB (FS-A1GT)
* **VRAM** : 128 KB
* **RROM**: 96 KB
* **Procesador gráfico**: Yamaha V9958
* **Chip de sonido**: PSG (compatible con AY-3-8910), MSX-MUSIC, PCM, MSX-MIDI
* **Resolución** : 512 x 212 (4 ó 16 colores) y 256 x 212 (16 a 19268 colores)

## Presentación

MSX** es un estándar japonés para microordenadores domésticos (de consumo) que se remonta a la década de 1980. A diferencia de la mayoría de los ordenadores de la época, los MSX eran producidos por varios fabricantes. Eran compatibles entre sí, tanto en términos de hardware como de software. Hubo varias versiones del estándar.

A menudo interpretado como **M**icro**S**oft e**X**tension, el acrónimo MSX significa **Máquinas con **S**software e**X**changeability, según Kazuhiko Nishi, iniciador del proyecto. El estándar fue creado en 1983 y producido por varias empresas japonesas, entre ellas Canon, Casio, Panasonic, Sanyo, Sony, Toshiba y Yashica. La empresa japonesa Yamaha ha producido MSX para uso musical, incluida una versión MSX1 con un procesador de sonido de ocho voces y conectores DIN con estándar MIDI. En Europa, Philips y Schneider han estado presentes en la escena MSX.

El estándar MSX se inventó a raíz de una licitación del METI, que quería que los ordenadores fueran compatibles entre sí (en aquella época, cada marca/modelo de ordenador tenía su propio lenguaje y sistema operativo). Microsoft respondió a la llamada para desarrollar las capas de software: lenguaje de programación interpretado MSX-Basic integrado de serie en una ROM y sistema operativo MSX-DOS 1. Más tarde, MSX-DOS 2 fue desarrollado por ASCII, añadiendo, entre otras cosas, las nociones de directorios (y subdirectorios) y particiones de disco duro SCSI.

Los MSX tenían peculiaridades "nacionales": la versión comercializada en Japón estaba equipada con un teclado QWERTY/Kanji. En Francia, la mayoría tenía un teclado AZERTY. Al Alamia comercializó una versión compatible con el árabe. Esta versión se llamaba Sakhr ("roca" en árabe).

El MSX2 de Sony ya contaba con un ratón y un escritorio gráfico (interfaz).

Ha habido cuatro generaciones de MSX: MSX (o MSX1), MSX2, MSX2+ y MSX turbo R. Fueron muy populares en Japón y Europa, especialmente durante la época del MSX1. El MSX2+ se vende en Europa a través de un puñado de importadores en Francia, España y los Países Bajos. El MSX turbo R es aún más raro en Europa, ya que está producido exclusivamente por Panasonic y reservado exclusivamente para el mercado japonés. Sin embargo, el NEOS MA-20(v), una extensión para el MSX1, permite utilizarlo como un MSX2.

## Emuladores

[ibretro blueMSX](libretro-bluemsx)