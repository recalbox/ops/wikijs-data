---
title: Hypseus
description: 
published: true
date: 2024-07-23T23:13:00.488Z
tags: daphne, singe, hypseus
editor: markdown
dateCreated: 2021-05-21T08:13:17.096Z
---

**Hypseus** pour l'émulation **des jeux d'arcade sur laserdisc**, est un fork de Daphne écrit par Jeffrey Clark pour lui apporter quelques améliorations :

* Mise à jour du décodeur MPEG2
* Supporte SDL2
* Etc...

## ![](/emulators/license.svg) Licence

Ce core est sous licence [**GPLv3**](https://github.com/DirtBagXon/hypseus-singe/blob/master/LICENSE).

**Hypseus Singe**, Being Retro Orientated This Humble Emulator Regenerates
Copyright (C) 2021-2024 [DirtBagXon](https://github.com/DirtBagXon/hypseus-singe)

**Hypseus**, Multiple Arcade Laserdisc Emulator
Copyright (C) 2016 [Jeffrey Clark](https://github.com/h0tw1r3)

**Daphne**, the First Ever Multiple Arcade Laserdisc Emulator
 Copyright (C) 1999-2013 [Matt Ownby](http://www.daphne-emu.com/site3/statement.php)

## ![](/emulators/compatibility.png) Compatibilité

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Fonctionnalités

| Fonctionnalité | Supporté |
| :---: | :---: |

## ![](/emulators/bios.svg) BIOS

>**Aucun bios n'est requis.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Comment jouer à des jeux laserdisc sur Recalbox ?

* Pour ajouter un jeu sur laserdisc, vous avez besoin des deux composants d'un tel jeu :

  * L'image du laserdisc original, qui contient les contenus vidéo et audio.
  * Le fichier ROM, qui est le programme du jeu.

**La ROM** est un simple fichier ZIP qui doit être copié sous le sous-répertoire `roms/daphne/`. Celui-ci contient des fichiers en `.bin`.

* L'image du disque-laser est constitué de plusieurs fichiers :

  * **Un ou plusieurs fichiers vidéo** (`*.m2v`)
  * **Un ou plusieurs fichiers audio** (`*.ogg`)
  * **Un ou plusieurs fichiers d'index** (`*.dat`) : peu(ven)t être créé(s) par l'émulateur au premier lancement (opération lente)
  * **Un fichier frame** (`.txt`) : pour associer les numéros de frame aux fichiers vidéo.

* Tous ces fichiers doivent être copiés dans un répertoire avec une extension `.daphne`, sous le répertoire `.daphne`.
* La première ligne de votre ficher `.txt` est un chemin et doit être . (juste le caractère point), modifiez-là si ce n'est pas le cas.
* Vous pouvez aussi créer un fichier avec l'extension `.commands` pour passer à l'émulateur des paramètres spécifiques à un jeu. Voir ci-dessous pour plus de détails.
* Le répertoire, le fichier frame et le fichier commands doivent porter le même nom que le fichier ROM ! (seules les extensions sont diffèrentes).

### Emplacement

Placez les roms comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 **jeu.daphne**
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **jeu.command** (optionnel)
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **jeu.txt**
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **jeu.dat**
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **jeu.m2v**
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **jeu.ogg**
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **...**
┃ ┃ ┃ ┃ ┣ 📁 **roms**
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **jeu.zip**

## ![](/emulators/advanced-configuration.png) Configuration avancée de l'émulateur

>**Ce core n'a pas d'option.**
{.is-success}

## ![](/emulators/external-links.png) Liens externes

* **Code source utilisé** : [https://github.com/DirtBagXon/hypseus-singe](https://github.com/DirtBagXon/hypseus-singe)
* **Site officiel** : [http://www.daphne-emu.com/site3/index_hi.php](http://www.daphne-emu.com/site3/index_hi.php)
* **FAQ** : [http://www.daphne-emu.com/site3/FAQ_USA.php](http://www.daphne-emu.com/site3/FAQ_USA.php)
* **Wiki du core** : [https://www.daphne-emu.com:9443/mediawiki/index.php/Main_Page](https://www.daphne-emu.com:9443/mediawiki/index.php/Main_Page)