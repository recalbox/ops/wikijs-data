---
title: Dateiverwaltung
description: 
published: true
date: 2024-06-02T19:30:54.407Z
tags: 
editor: markdown
dateCreated: 2021-06-21T15:27:07.044Z
---

**Recalbox** ermöglicht es dir, deine Dateien über den "share"-Ordner zu verwalten.

Du hast zwei Möglichkeiten:

* Direkter Zugriff
* Über das Netzwerk

## Der "share" Ordner

>Der Ordner **"share"** ist derjenige, über den du den Inhalt deiner Recalbox verwalten kannst. Er ist **über das Netzwerk** verfügbar.
{.is-info}

![](/basic-usage/file-management/share1.png){.full-width}

Er beinhaltet mehrere Unterverzeichnisse:

![](/basic-usage/file-management/share2.png){.full-width}

* **bios**

Kopiere hier die BIOS Dateien hin, die Recalbox benötigt, um bestimmte Emulatoren lauffähig zu machen. Falls notwendig, kannst du dir eine Liste benötigter BIOS Dateien anzeigen lassen (EmulationStation → `START` → `BIOS-ÜBERPRÜFUNG`).

>Einige Emulatoren benötigen das BIOS in einem Unterordner oder eine extra Kopie im jeweiligen ROMs-Verzeichnis. Schaue dazu in der Dokumentation des jeweiligen Emulators nach.
{.is-warning}

* **bootvideos**

Kopiere hier die Videos hin, die angezeigt werden, wenn du deine Recalbox einschaltest oder neu startest.

* **cheats**

Inhalte der Cheats, die über das RetroArch-Menü heruntergeladen werden können.

* **extractions**

Verzeichnis, in das die gezippten ROMs entpackt werden, bevor ein Spiel gestartet wird.

* **kodi**

Kopiere hier die Medien, die in Kodi verfügbar sein sollen, in die entsprechenden Unterordner:

  * music
  * movie
  * picture
  
* **music**

Kopiere hier die Musik hin, die in EmulationStation als Hintergrundmusik abgespielt werden soll.

>Die Dateien müssen im Format `.MP3` oder `.OGG` vorliegen und eine Abtastrate von 44100Hz und eine Bitrate größer als 256 kb/s haben.
{.is-info}

* **overlays**

Kopiere hier die Overlay-Dateien hin, die während des Spielens angezeigt werden sollen.

* **roms**

Kopiere hier deine ROMs in die Unterordner des enstprechenden Systems.

* **saves**

Das Verzeichnis, in dem deine Spielstände hinterlegt werden.

* **screenshots**

Das Verzeichnis, in dem deine Screenshots hinterlegt werden.

* **shaders**

Das Verzeichnis, in dem sich die System-Shader befinden.

* **system**

Das Verzeichnis, in dem sich die Konfigurationsdateien der Recalbox/des Systems befinden.

* **themes**

Kopiere hier die Themes hin, die in EmulationStation verfügbar gemacht werden sollen.

* **userscripts**

Das Verzeichnis, in dem User-Skripte hinterlegt werden können.

## Dateiübertragung

Um deine Datein (ROMs, Saves, BIOS, Scrape Daten, etc.) auf deine Recalbox zu bringen, hast du zwei Möglichkeiten:

### Direkter Zugriff

* Schließe dein Speichermedium an deinen Computer an.
* Öffne den Datei-Explorer deines Betriebssystems.
* Öffne das Fenster, das deine Geräte auflistet.
* Wähle dann dein Speichermedium, öffne dann das Verzeichnis `recalbox` und schließlich das Unterverzeichnis `share`.
* Kopiere deine Dateien in das gewünschte Unterverzeichnis.
* Sobald die Übertragung abgeschlossen ist, musst Du dein Speichermedium nur wieder an deine Recalbox anschließen, diese dann einschalten und spielen.

### Über das Netzwerk

>Wenn du WLAN für die Recalbox einrichtest oder ein Netzwerkkabel anschließt, gibt die Recalbox automatisch Ordner im lokalen Netzwerk frei.
{.is-info}

* Sobald dein Gerät über WLAN oder Ethernet-Kabel verbunden ist, öffne den Datei-Explorer deines Betriebssystems.
* Öffne dann das Fenster Netzwerke, dann `recalbox` und schließlich `share`.
* Kopieren deine Dateien in das gewünschte Unterverzeichnis.

>Wenn du deine Recalbox nicht im Netzwerk siehst, versuche, ``\\RECALBOX`` in die Adressleiste des Datei-Explorers einzugeben. 
>
>Wenn dies immer noch nicht funktioniert, ermittle deine [IP-Adresse](./tutorials/network/ip/discover-recalbox-ip). Gib dann deine IP-Adresse in die Adresszeile des Datei-Explorers ein. (Zum Beispiel: \\192.168.1.30\).
>
>Wenn dies immer noch nicht funktioniert und du mit Windows 10 arbeitest, kannst du auch [diesem Tutorial](./../tutorials/network/share/access-network-share-with-windows-10) folgen.
{.is-info}

### Damit neue Dateien auch erscheinen:

* Aktualisiere die Spieleliste :

  * Öffne das Menü in EmulationStation mit `START`.
  * → `UI EINSTELLUNGEN`.
  * → `SPIELELISTE AKTUALISIEREN`.

* Starte deine Recalbox neu:

  * Öffne das Menü in EmulationStation mit `SELECT`.
  * → `SYSTEM NEUSTARTEN`.

## Inhalte hinzufügen

### Spiele hinzufügen

Du musst nur die ROMs des gewünschten Systems in das entsprechende Verzeichnis auf der Recalbox kopieren.

>**Beispiel für Super Nintendo:**
>
>`/recalbox/share/roms/snes/`
{.is-success}

Du kannst je nach Emulator komprimierte ROMs (.zip, .7z) oder unkomprimierte ROMs verwenden.

>Weitere Informationen zu den ROM-Formaten, die von einem Emulator unterstützt werden, findest du in der Datei `_liesmich.txt` im jeweiligen ROM-Ordner.
{.is-info}

### BIOS hinzufügen

Einige EMulatoren benötigen ein BIOS, um Spiele korrekt emulieren zu können.

* Wenn du ein BIOS deinem System hinzufügen möchtest, öffne den gemeinsamen BIOS-Ordner über Samba oder gehe direkt zu `/recalbox/share/bios/`.
* Die BIOS-Namen und deren MD5-Signatur müssen mit der Liste im [Bios Manager](./basic-usage/features/bios-manager) übereinstimmen.

Wie man die MD5-Signatur eines BIOS überprüft, erfährst du auf [dieser Seite](./../tutorials/utilities/rom-management/check-md5sum-of-rom-or-bios).

>Für Neo-Geo musst du das BIOS `neogeo.zip` in den BIOS-Ordner und in den ROMs-Ordner des Emulators mit deinen Neo-Geo Spielen (`/recalbox/share/roms/neogeo` oder `/recalbox/share/roms/fbneo`) kopieren
>Für Neo-Geo CD müssen beide BIOS (`neogeo.zip` und `neocdz.zip`) in den Ordner `/recalbox/share/bios/` kopiert werden
{.is-warning}