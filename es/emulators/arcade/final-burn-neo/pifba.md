---
title: PiFBA
description: 
published: true
date: 2024-06-23T13:10:22.243Z
tags: finalburn, neo, pifba
editor: markdown
dateCreated: 2023-09-23T14:34:03.452Z
---

**PiFBA** es el Final Burn Alpha 2x para los Raspberry Pis portado por Squid. 
Emula viejos juegos de arcade usando roms basadas en MAME para CPS1, CPS2, Neogeo, Toaplan y muchos otros juegos.

Debería soportar las versiones más recientes de las versiones MAME de estas ROMs, pero desafortunadamente no hay una versión definitiva para usar. Una lista completa de los juegos soportados se puede encontrar en el frontend.

Algunos juegos no se ejecutarán correctamente, algunos de ellos son tan grandes que los Raspberry Pis no tienen suficiente memoria.

>Utilizadlo sólo si tenéis un Raspberry Pi0/1 o si queréis mejorar el rendimiento de un juego en particular.
>piFBA es el emulador FBA que está más optimizado para los Raspberry Pi0/1 pero es compatible con muchos menos juegos que el emulador FBNeo.
{.is-warning}

## ![](/emulators/license.svg) Licencia

Este core está bajo licencia **GPLv2**.

## ![](/emulators/compatibility.png) Compatibilidad

 | RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
 | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
 | ✅ | ✅ | ❌ | ❌ | ❌ | ❌ | ❌ | ❌ |

## ![](/emulators/features.png) Funcionalidades

| Funcionalidad | ¿Soportada? |
| :---: | :---: |
| Grabaciones | ✔ |
| Netplay | ✔ |
| RetroAchievements | ✔ |

## ![](/emulators/bios.svg) Bios

### Lista de bios

Dependiendo de los juegos, algunas bios son necesarias y deben colocarse en el mismo directorio que los juegos.

### Emplazamiento

Disponed vuestras **bios** como en la siguiente imagen:

┣ 📁 recalbox  
┃ ┣ 📁 share  
┃ ┃ ┣ 📁 roms  
┃ ┃ ┃ ┣ 📁 fba  
┃ ┃ ┃ ┃ ┣ 🗒 bios.zip  

## ![](/emulators/roms.png) Roms

* Basado en el romset MAME : **FBA 0.2.96.71** (Abril de 2007) él mismo basado en el set MAME 0.114
* Peso : 3.62GB
* Romsets emuladas : 684 (Ningún clone en este set)
* Lista de compatibilidades : [/recalbox/share/roms/fba/clrmamepro/piFBA_gamelist.txt](https://gitlab.com/recalbox/recalbox/-/raw/master/board/recalbox/fsoverlay/recalbox/share_init/bios/fba/piFBA_gamelist.txt)
* Fichero Dat : [/recalbox/share/roms/fba/clrmamepro/fba_029671_od_release_10_working_roms.dat](https://gitlab.com/recalbox/recalbox/-/raw/master/board/recalbox/fsoverlay/recalbox/share_init/bios/fba/fba_029671_od_release_10_working_roms.dat)

Algunas roms importantes necesitan ser convertidas al formato '.fba' antes de funcionar.
Para realizar esto se incluye un herramienta FBACache_windows.zip, que sólo funciona bajo Windows.

Se incluye también un fichero DAT para clrmamepro / romcenter llamado 'fba_029671_clrmame_dat.zip' que soporta la mayoría de las ROMs que funcionan con este emulador y puede generar ROMs compatibles a partir de versiones recientes de MAME.

## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

>Para conservar las configuraciones personalizadas cuando se produce una actualización del sistema Recalbox, os aconsejamos utilizar las [Sobrecargas de configuración](./../../../advanced-usage/configuration-override).
{.is-info}

## ![](/emulators/external-links.png) Enlaces externos

* **Código fuente utilizado** : [https://github.com/recalbox/pifba/](https://github.com/recalbox/pifba)
* **Código fuente original**: [https://github.com/squidrpi/pifba/](https://github.com/squidrpi/pifba)