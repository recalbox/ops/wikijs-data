---
title: Libretro MAME 2000
description: 
published: true
date: 2024-06-25T16:27:12.681Z
tags: libretro, mame, mame2000
editor: markdown
dateCreated: 2021-05-21T08:14:04.868Z
---



## ![](/emulators/license.svg) Licence

Ce core est sous licence **MAME non-commerciale**.

## ![](/emulators/compatibility.png) Compatibilité

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ❌ | ❌ | ❌ | ❌ | ❌ | ❌ |

## ![](/emulators/features.png) Fonctionnalités

| Fonctionnalité | Supporté |
| :---: | :---: |
| Sauvegardes | ✔ |
| Sauvegardes instantanées | Selon les jeux |
| Rembobinage | ✔ |
| Netplay | ✔ |
| Native Cheats | ✔ |
| Controllers | ✔ |
| Multi-Mouse | ✔ |

## ![](/emulators/bios.svg) BIOS

Les romsets BIOS ne sont pas nécessaires lorsque vous utilisez des romsets d'arcade "Full Non-Merged". Pour les roms "Split" et "Non-Merged", placez les BIOS nécessaires dans le même répertoire que la Romset du jeu.

>**Remarque** :
>Placez les bios dans le répertoire : `/recalbox/share/roms/mame/`   
>ou dans son sous-répertoire si vous utilisez plusieurs cores de Mame, par exemple : `/recalbox/share/roms/mame/Mame2000/`
Les jeux et leurs bios doivent être dans le même répertoire ou sous-répertoire.
{.is-warning}

## ![](/emulators/roms.png) Roms

* Basé sur le romset : MAME 0.37b5 (Juillet 2000)
* Fichier dat : `/recalbox/share/arcade/libretro/mame2000.dat`.

### Emplacement

Placez les roms comme ceci : 

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 mame
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.zip**
┃ ┃ ┃ ┃ ┣ 📁 **jeu**
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **jeu.chd**

Vous pouvez aussi opter pour le sous-répertoire déjà présent `mame2000` (utile si vous voulez avoir un set mame pour un autre core). Ceci permettra de lancer le bon émulateur automatiquement.

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 mame
┃ ┃ ┃ ┃ ┣ 📁 mame2000
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **jeu.zip**
┃ ┃ ┃ ┃ ┃ ┣ 📁 **jeu**
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 **jeu.chd**

## ![](/emulators/advanced-configuration.png) Configuration avancée de l'émulateur

>Pour pouvoir conserver vos configurations personnalisées lors d'une mise à jour, nous vous conseillons d'utiliser notre fonctionnalité [Surcharges de configuration](./../../../advanced-usage/configuration-override).
{.is-info}

### Accéder aux options

Vous pouvez configurer diverses options de deux façons différentes.

* Via le Menu RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via le fichier `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Options du core

| Options | Valeur des options | Variable (pour surcharge) | Valeurs des variables |
| --- | --- | --- | --- |
| Frameskip | `Désactivé` ✅ / `auto` / `threshold` | `mame2000-frameskip` | `disabled` / `auto` / `threshold` |
| Frameskip Threshold (%) | `30` ✅ / `40` / `50` / `60` | `mame2000-frameskip_threshold` | `30` / `40` / `50` / `60` |
| Frameskip Interval | `1` ✅ / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` | `mame2000-frameskip_interval` | `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` |
| Skip Disclaimer | `Activé` ✅ / `Désactivé` | `mame2000-skip_disclaimer` | `enabled` / `disabled` |
| Show Game Information | `Désactivé` ✅ / `Activé` | `mame2000-show_gameinfo` | `disabled` / `enabled` |
| Audio Rate (Restart) | `11025` / `22050` ✅ / `32000` / `44100` | `mame2000-sample_rate` | `11025` / `22050` / `32000` / `44100` |
| Stereo (Restart) | `Activé` ✅ / `Désactivé` | `mame2000-stereo` | `enabled` / `disabled` |

## ![](/emulators/external-links.png) Liens externes

* **Progettosnaps** : [https://www.progettosnaps.net/](https://www.progettosnaps.net/)
* **Code source utilisé** : [https://github.com/libretro/mame2000-libretro/](https://github.com/libretro/mame2000-libretro/)