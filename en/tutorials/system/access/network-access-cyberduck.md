---
title: Network access via Cyberduck
description: 
published: true
date: 2021-08-06T15:05:29.721Z
tags: cyberduck, network, access
editor: markdown
dateCreated: 2021-08-06T08:16:00.702Z
---

## Network connection with Cyberduck

You are going to discover how to connect on Windows and macOS in network to your Recalbox in SFTP with the Cyberduck software.

You will be able to easily access the different configuration files such as `/boot/recalbox-user-config.txt`.
If you are used to shell tools, this tutorial is not for you, it is for Windows and macOS users who need a graphical interface.

>**Cyberduck** is a free file transfer program for Windows and macOS. 
>It supports many protocols: FTP, FTP-SSL, SFTP, WebDAV, Swift, S3, Google Cloud Storage, Windows Azure Storage, Dropbox, Google Drive, Backblaze B2 Cloud Storage, Backspace Cloud Files.
{.is-info}

* To get started, download [Cyberduck](https://cyberduck.io/).
* Your Recalbox must be connected to your local network. You can find the IP address of your Recalbox by following [this tutorial](./../../../tutorials/network/ip/discover-recalbox-ip).

![](/tutorials/system/access/cyberduck1.png){.full-width}

* Open Cyberduck and click on "Open a connection" and fill in the following information:
  * Protocol: `SFTP (SSH File Transfer Protocol)`
  * Port: `22`
  * Server: enter the IP address of your Recalbox
  * User name: `root`
  * Password: `recalboxroot`
* Click the `Connect` button and allow unknown fingerprints.

![](/tutorials/system/access/cyberduck2.png){.full-width}

* To save the configuration to a bookmark, click on the bookmark icon at the top left, then on the `+` button at the bottom left.

![](/tutorials/system/access/cyberduck3.png){.full-width}

* Take the opportunity to rename the connection before closing the window.

![](/tutorials/system/access/cyberduck4.png){.full-width}

* Here is the result:

![](/tutorials/system/access/cyberduck5.png){.full-width}

That's it, your bookmark is saved.

The `recalbox-user-config.txt` file is in `/boot/`.
The Recalbox `share` folder is in `/recalbox/share/`.

## Set write permissions on the SD card

For the advanced hobbyist, you need to write the SD card, to modify the `recalbox-user-config.txt` file. 

* In the Cyberduck menu, use the `Send command` option

![](/tutorials/system/access/cyberduck6.png){.full-width}

* And insert the following command:

`mount -o remount,rw /boot`

* Now you can modify the `recalbox-user-config.txt` file. You are advised to make a backup of the original file on this computer

[Source](https://forum.recalbox.com/topic/7341/tutoriel-se-connecter-en-ssh-pour-les-nuls-avec-cyberduck-pour-config-txt-osx-et-win)