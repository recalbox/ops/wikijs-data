---
title: Libretro gpSP
description: 
published: true
date: 2024-07-24T23:14:23.881Z
tags: libretro, gameboy, advance, gba, gpsp
editor: markdown
dateCreated: 2021-07-14T06:53:49.071Z
---

**Libretro gpSP** is an emulator based on the notaz fork of gpSP for the Game Boy Advance with additional improvements to the code base.

## ![](/emulators/license.svg) License

This core is under [**GPLv2**](https://github.com/libretro/gpsp/blob/master/COPYING) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Features

| Feature | Supported |
| :---: | :---: |
| Restart | ✔ |
| Screenshots | ✔ |
| Saves | ✔ |
| States | ✔ |
| Rewind | ✔ |
| Core Options | ✔ |
| RetroAchievements | ✔ |
| RetroArch Cheats | ✔ |
| Controls | ✔ |
| Remapping | ✔ |

## ![](/emulators/bios.svg) BIOS

### Liste of optional bios

| Filename | Description | MD5 | Provided |
| :---: | :---: | :---: | :---: |
| gba_bios.bin | Game Boy Advance BIOS | a860e8c0b6d573d191e4ec7db1b1e4f6 | ❌ |
| open_gba_bios.bin | Open Source Game Boy Advance BIOS | 1876f71b0d8c65eef3454547896ffb11 | ✅ |

### Location

Put your bios like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 gba
┃ ┃ ┃ ┃ ┣ 🗒 **gba_bios.bin**
┃ ┃ ┃ ┃ ┣ 🗒 **open_gba_bios.bin**

## ![](/emulators/roms.png) Roms

### supported extensions

Roms must have the extension:

* .bin
* .gba
* .zip
* .7z

This system supports compressed roms in .zip/.7z format. However, be careful, it is only an archive.

The files contained in the .zip/.7z must correspond to the extensions mentioned above.
Moreover, each .zip/.7z file must contain only one compressed rom.

### Location

Put your roms like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 gba
┃ ┃ ┃ ┃ ┣ 🗒 **game.zip**

>The roms using **No-Intro** format are highly recommended.
{.is-success}

>For more information about roms, go to [this tutorial](./../../../tutorials/games/generalities/isos-and-roms)!
{.is-info}

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Accessing to options

You can configure various options in two different ways.

* Via RetroArch menu:

┣ 📁 RetroArch Menu
┃ ┣ 📁 Core options
┃ ┃ ┣ 🧩 Name_option

* Via the file `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core options

| Options | Options values | Variables (for configuration override) | Variables values |
| --- | --- | --- | --- |
| BIOS | `Auto select` ✅ / `Builtin BIOS` / `Original BIOS` | `gpsp_bios` | `auto` / `builtin` / `official` |
| Boot mode | `Boot to game` ✅ / `Boot to BIOS` | `gpsp_boot_mode` | `game` / `bios` |
| Dynamic Recompiler | `Activé` ✅ / `Désactivé` | `gpsp_drc` | `enabled` / `disabled` |
| No Sprite Limit | `Désactivé` ✅ / `Activé` | `gpsp_sprlim` | `disabled` / `enabled` |
| RTC support | `Auto` ✅ / `Activé` / `Désactivé` | `gpsp_rtc` | `auto` / `enabled` / `disabled` |
| Rumble support | `Auto` ✅ / `Activé` / `Désactivé` | `gpsp_rumble` | `auto` / `enabled` / `disabled` |
| Frameskip | `Disabled` ✅ / `Auto` / `Auto (Threshold)` / `Fixed Interval` | `gpsp_frameskip` | `disabled` / `auto` / `auto_threshold` / `fixed_interval` |
| Frameskip Threshold (%) | `15` / `18` / `21` / `24` / `27` / `30` / `33` ✅ / `36` / `39` / `42` / `45` / `48` / `51` / `54` / `57` / `60` | `gpsp_frameskip_threshold` | `15` / `18` / `21` / `24` / `27` / `30` / `33` / `36` / `39` / `42` / `45` / `48` / `51` / `54` / `57` / `60` |
| Frameskip Interval | `0` / `1` ✅ / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` | `gpsp_frameskip_interval` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` |
| Color Correction | `Enabled` / `Disabled` ✅ | `gpsp_color_correction` | `enabled` / `disabled` |
| Interframe Blending | `Enabled` / `Disabled` ✅ | `gpsp_frame_mixing` | `enabled` / `disabled` |
| Turbo Button Period | `4` ✅ / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `21` / `22` / `23` / `24` / `25` / `26` / `27` / `28` / `29` / `30` / `31` / `32` / `33` / `34` / `35` / `36` / `37` / `38` / `39` / `40` / `41` / `42` / `43` / `44` / `45` / `46` / `47` / `48` / `49` / `50` / `51` / `52` / `53` / `54` / `55` / `56` / `57` / `58` / `59` / `60` / `61` / `62` / `63` / `64` / `65` / `66` / `67` / `68` / `69` / `70` / `71` / `72` / `73` / `74` / `75` / `76` / `77` / `78` / `79` / `80` / `81` / `82` / `83` / `84` / `85` / `86` / `87` / `88` / `89` / `90` / `91` / `92` / `93` / `94` / `95` / `96` / `97` / `98` / `99` / `100` / `101` / `102` / `103` / `104` / `105` / `106` / `107` / `108` / `109` / `110` / `111` / `112` / `113` / `114` / `115` / `116` / `117` / `118` / `119` / `120` | `gpsp_turbo_period` | `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `21` / `22` / `23` / `24` / `25` / `26` / `27` / `28` / `29` / `30` / `31` / `32` / `33` / `34` / `35` / `36` / `37` / `38` / `39` / `40` / `41` / `42` / `43` / `44` / `45` / `46` / `47` / `48` / `49` / `50` / `51` / `52` / `53` / `54` / `55` / `56` / `57` / `58` / `59` / `60` / `61` / `62` / `63` / `64` / `65` / `66` / `67` / `68` / `69` / `70` / `71` / `72` / `73` / `74` / `75` / `76` / `77` / `78` / `79` / `80` / `81` / `82` / `83` / `84` / `85` / `86` / `87` / `88` / `89` / `90` / `91` / `92` / `93` / `94` / `95` / `96` / `97` / `98` / `99` / `100` / `101` / `102` / `103` / `104` / `105` / `106` / `107` / `108` / `109` / `110` / `111` / `112` / `113` / `114` / `115` / `116` / `117` / `118` / `119` / `120` |

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/libretro/gpsp/](https://github.com/libretro/gpsp/)
* **Libretro documentation**: [https://docs.libretro.com/library/gpsp/](https://docs.libretro.com/library/gpsp/)
* **Official source code**: [https://github.com/notaz/gpsp](https://github.com/notaz/gpsp)
* **Official website**: [https://notaz.gp2x.de/other.php](https://notaz.gp2x.de/other.php)