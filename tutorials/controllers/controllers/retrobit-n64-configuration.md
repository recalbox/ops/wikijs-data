---
title: N64 Retrobit controller configuration
description: 
published: true
date: 2023-09-27T06:16:40.400Z
tags: n64, controller, retrobit
editor: markdown
dateCreated: 2023-09-27T06:16:40.400Z
---

## Introduction

There are several N64 controllers on the market, and RetroArch doesn't know how to use the C buttons correctly. This tutorial will show you how to use retrobit's Tribute64 controller.

![](/tutorials/controllers/controllers/retrobit-tribute64.png)

## Usage

Unfortunately, RetroArch doesn't know the original 4 C buttons and is limited to A / B / X / Y. Let's see how we can remedy this to the full.

### Ignition / configuration

The controller has its own inseparable USB dongle. You need to plug it into your recalbox and use the `START` + `HIGH` key combination to switch it on.

The basic configuration is provided in recalbox, so there's no need to remap it. However, if this should happen, please follow these steps:

- Button A in EmulationStation must correspond to button C-Lower on the controller
- Button B in EmulationStation must correspond to button A on the controller
- Button X in EmulationStation must correspond to button C-Left on the controller
- The Y button in EmulationStation must correspond to the B button on the controller
- The Select button in EmulationStation must correspond to the left gray button above Start on the joystick
- The Hotkey button in EmulationStation must correspond to the right-hand gray button above Start on the controller.

> Use the controller only with N64 and 64DD games. It is not designed for any other system.
> {.is-info}

Once this has been done, you need to create 1 configuration overload file. This will ensure that all buttons are functional.

### Configuration overloads

>This only works for Libretro cores.
{.is-info}

You need to create an empty text file named `.retroarch.cfg` in `/recalbox/share/roms/n64` and insert the following content:

```ini
input_player1_b_btn = "2" # A
input_player1_a_btn = "15" # No affectation
input_player1_y_btn = "15" # No affectation
input_player1_y_btn = "1" # B
input_player1_start_btn = "9" # Start
input_player1_select_btn = "8" # Select
input_player1_up_btn = "h0up" # D-pad up
input_player1_down_btn = "h0down" # D-pad down
input_player1_left_btn = "h0left" # D-pad left
input_player1_right_btn = "h0right" # D-pad right
input_player1_l_btn = "4" # L
input_player1_r_btn = "5" # R
input_player1_l2_btn = "6" # Z
input_player1_r_x_plus_btn = "10" # C-right
input_player1_r_x_minus_btn = "0" # C-left
input_player1_r_y_plus_btn = "3" # C-down
input_player1_r_y_minus_btn = "11" # C-up
input_player1_l_x_plus_axis = "+0" # Stick right
input_player1_l_x_minus_axis = "-0" # Stick left
input_player1_l_y_plus_axis = "+1" # Stick down
input_player1_l_y_minus_axis = "-1" # Stick up
input_player1_menu_toggle_btn = "12" # Unknown
```

Enjoy the game!