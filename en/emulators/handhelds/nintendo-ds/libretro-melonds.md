---
title: Libretro melonDS
description: 
published: true
date: 2021-11-04T12:55:26.282Z
tags: libretro, ds, melonds
editor: markdown
dateCreated: 2021-07-27T10:52:14.619Z
---

**Libretro melonDS** is a promising Nintendo DS/DSI emulator developed in C and C++.

The **melonDS** emulator aims to provide a fast and accurate Nintendo DS emulation. Although it is still a work in progress, it has a pretty solid set of features:

* Almost complete core (CPU, video, audio, ...)
* JIT recompiler for fast emulation
* OpenGL rendering, 3D upconversion
* RTC, microphone, closed / open cover
* Joystick support
* Savestates
* Different display modes / sizing / rotation
* (WIP) Wifi: local multiplayer, online connectivity
* DSi emulation (WIP)
* DLDI
* (WIP) GBA slot add-ons
* and more are planned!

It was written by **Arisotura** (known as StapleButter), a former contributor to DeSmuME.

## ![](/emulators/license.svg) License

This core is under [**GPLv3**](https://github.com/libretro/melonDS/blob/master/LICENSE) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ❌ | ❌ | ❌ | ❌ | ❌ | ❌ | ✅ | ✅ |

## ![](/emulators/features.png) Features

| Feature | Supported |
| :---: | :---: |
| Restart | ✔ |
| Screenshots | ✔ |
| Saves | ✔ |
| RetroAchievements | ✔ |
| RetroArch Cheats | ✔ |
| Controls | ✔ |
| Remapping | ✔ |

## ![](/emulators/bios.svg) BIOS

### List od mandatory bios

| Filename | Description | MD5 | Provided |
| :---: | :---: | :---: | :---: |
| bios7.bin | ARM7 BIOS | df692a80a5b1bc90728bc3dfc76cd948 | ❌ |
| bios9.bin | ARM9 BIOS | a392174eb3e572fed6447e956bde4b25 | ❌ |
| firmware.bin | NDS Firmware | 145eaef5bd3037cbc247c213bb3da1b3 e45033d9b0fa6b0de071292bba7c9d13 3ad72b2c9a736b24953f2d391da4bfcc | ❌ |

### Location

Put the bios like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 🗒 **bios7.bin**
┃ ┃ ┃ ┣ 🗒 **bios9.bin**
┃ ┃ ┃ ┣ 🗒 **firmware.bin**

## ![](/emulators/roms.png) Roms

### Supported extensions

Roms must have the extension:

* .nds
* .zip
* .7z

This system supports compressed roms in .zip/.7z format. However, be careful, it is only an archive.

The files contained in the .zip/.7z must correspond to the extensions mentioned above.
Moreover, each .zip/.7z file must contain only one compressed rom.

### Location

Put the roms like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 ds
┃ ┃ ┃ ┃ ┣ 🗒 **game.zip**

>The roms using **No-Intro** format are highly recommended.
{.is-success}

>For more information about roms, go to [this tutorial](./../../../tutorials/games/generalities/isos-and-roms)!
{.is-info}

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Accessing to options

You can configure various options in two different ways.

* Via RetroArch menu:

┣ 📁 RetroArch Menu
┃ ┣ 📁 Core options
┃ ┃ ┣ 🧩 Name_option

* Via the file `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core options

| Options | Options values | Variables (for configuration override) | Variables values |
| --- | --- | --- | --- |
| Boot game directly | `Enabled` ✅ / `Disabled` | `melonds_boot_directly` | `enabled` / `disabled` |
| Screen Layout | `Top/Bottom` ✅ / `Bottom/Top` / `Left/Right` / `Right/Left` / `Top Only` / `Bottom Only` / `Hybrid Top` / `Hybrid Bottom` | `melonds_screen_layout` | `Top/Bottom` / `Bottom/Top` / `Left/Right` / `Right/Left` / `Top Only` / `Bottom Only` / `Hybrid Top` / `Hybrid Bottom` |
| Hybrid ratio | `2` ✅ / `3` | `melonds_hybrid_ratio` | `2` / `3` |
| Swap Screen mode | `Toggle` ✅ / `Hold` | `melonds_swapscreen_mode` | `Toggle` / `Hold` |
| Threaded software renderer | `Disabled` ✅ / `Enabled` | `melonds_threaded_renderer` | `disabled` / `enabled` |
| Touch mode | `Disabled` ✅ / `Mouse` / `Touch` / `Joystick` | `melonds_touch_mode` | `disabled` / `Mouse` / `Touch` / `Joystick` |
| OpenGL Renderer (Restart) | `Disabled` ✅ / `Enabled` | `melonds_opengl_renderer` | `disabled` / `enabled` |
| OpenGL Internal Resolution | `1x native (256x192)` ✅ / `2x native (512x384)` / `3x native (768x576)` / `4x native (1024x768)` / `5x native (1280x960)` / `6x native (1536x1152)` / `7x native (1792x1344)` / `8x native (2048x1536)` | `melonds_opengl_resolution` | `1x native (256x192)` / `2x native (512x384)` / `3x native (768x576)` / `4x native (1024x768)` / `5x native (1280x960)` / `6x native (1536x1152)` / `7x native (1792x1344)` / `8x native (2048x1536)` |
| OpenGL Improved polygon splitting | `Disabled` ✅ / `Enabled` | `melonds_opengl_better_polygons` | `disabled` / `enabled` |
| JIT Enable (Restart) | `Enabled` ✅ / `Disabled` | `melonds_jit_enable` | `enabled` / `disabled` |
| JIT block size | `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `21` / `22` / `23` / `24` / `25` / `26` / `27` / `28` / `29` / `30` / `31` / `32` ✅ / `33` / `34` / `35` / `36` / `37` / `38` / `39` / `40` / `41` / `42` / `43` / `44` / `45` / `46` / `47` / `48` / `49` / `50` / `51` / `52` / `53` / `54` / `55` / `56` / `57` / `58` / `59` / `60` / `61` / `62` / `63` / `64` / `65` / `66` / `67` / `68` / `69` / `70` / `71` / `72` / `73` / `74` / `75` / `76` / `77` / `78` / `79` / `80` / `81` / `82` / `83` / `84` / `85` / `86` / `87` / `88` / `89` / `90` / `91` / `92` / `93` / `94` / `95` / `96` / `97` / `98` / `99` / `100` | `melonds_jit_block_size` | `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `21` / `22` / `23` / `24` / `25` / `26` / `27` / `28` / `29` / `30` / `31` / `32` / `33` / `34` / `35` / `36` / `37` / `38` / `39` / `40` / `41` / `42` / `43` / `44` / `45` / `46` / `47` / `48` / `49` / `50` / `51` / `52` / `53` / `54` / `55` / `56` / `57` / `58` / `59` / `60` / `61` / `62` / `63` / `64` / `65` / `66` / `67` / `68` / `69` / `70` / `71` / `72` / `73` / `74` / `75` / `76` / `77` / `78` / `79` / `80` / `81` / `82` / `83` / `84` / `85` / `86` / `87` / `88` / `89` / `90` / `91` / `92` / `93` / `94` / `95` / `96` / `97` / `98` / `99` / `100` |
| JIT Branch optimisations | `Enabled` ✅ / `Disabled` | `melonds_jit_branch_optimisations` | `enabled` / `disabled` |
| JIT Literal optimisations | `Enabled` ✅ / `Disabled` | `melonds_jit_literal_optimisations` | `enabled` / `disabled` |
| JIT Fast memory | `Enabled` ✅ / `Disabled` | `melonds_jit_fast_memory` | `enabled` / `disabled` |

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/libretro/melonds/](https://github.com/libretro/melonds/)
* **Libretro documentation**: [https://docs.libretro.com/library/melonds/](https://docs.libretro.com/library/melonds/)
* **Official source code**: [https://github.com/Arisotura/melonDS](https://github.com/Arisotura/melonDS/)
* **Official website**: [http://melonds.kuribo64.net/](http://melonds.kuribo64.net/)
* **Official forum**: [http://melonds.kuribo64.net/board/](http://melonds.kuribo64.net/board/)