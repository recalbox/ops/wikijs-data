---
title: FAQ
description: 
published: true
date: 2021-09-15T07:46:24.757Z
tags: daphne, faq
editor: markdown
dateCreated: 2021-09-15T07:46:24.757Z
---

### Where can I find the games?

* Some games can be directly and legally downloaded through the DaphneLoader tool of the [Daphne Windows distribution](http://www.daphne-emu.com/site3/index_hi.php).
* For others (Dragon's Lair, Dragon's Lair II, Space Ace, ...), you must prove that you have a valid license of the game (DVD version for example).

Once downloaded, copy the necessary files (ROM and laserdisc image files) to your Recalbox, as explained in the 2 dedicated pages:

* [Play Daphne games](./play-daphne-games)
* [Play Singe games](./play-singe-games)

### How do I quit a game?

To quit a game, only the `Hotkey` button is enough.

However, as it's an arcade system, you need to use the `Select` button to put credits. And if the `Hotkey` button is set to `Select`, how it works?

With controllers without any dedicated `Hotkey` button, you can use the `R1` button to quit your game and continue to use `Select` to add credits.

## Basic Requirements

Basic configuration required for the games to work.

### What is the `.commands` file?

As explained before, this file must be created in the game directory, with the same name as the ROM file but with the extension `.commands`. It allows you to pass additional parameters to the emulator for a specific game.

Most of the time it contains the `-bank` option, which defines the game configuration (also known as DIP switch):

* Number of credits for a game
* Number of lives for one credit
* Level of difficulty
* Etc...

>Recommended values for the most common games are described below, but you can check the [LaserDisc Game Tech Center](http://www.dragons-lair-project.com/tech/) site for complete information.
>
>Please also note that, for the same game, the DIP switches may vary from one ROM version to another. Other settings are also possible. Check [Daphne's wiki](http://www.daphne-emu.com/mediawiki/index.php/CmdLine) for more details.
{.is-info}

Example for Dragon's Lair (DLE 2.x):
  
`-bank 1 00110111 -bank 0 10011000`   
  
The [following page](http://www.dragons-lair-project.com/tech/dips/dle20.asp) tells us that:

* The sound is always present, with the narrator's voice (even outside a game)
* 1 coin = 1 credit
* 1 credit = 5 lives
* No test or diagnostic
* Standard game mode

### Supported games and recommended configuration

For each game, the following table describes :

* The emulator compatibility level. 
* The recommended ROM file for a better experience. 
* The recommended content of the `.commands` file: for normal difficulty, 5 lives, minimum credit, etc...

| Game Name | Compatibility | Recommended ROM | commands |
| :--- | :--- | :--- | :---: |
| Astron Belt | ⭐️⭐️ | astron.zip | `-bank 0 00000000 -bank 1 01000000` |
| Badlands | ⭐️ | badlands.zip | `-bank 0 00000000 -bank 1 10000010` |
| Bega's Battle | ⭐️ | bega.zip | `-bank 0 00000000 -bank 1 00000001` |
| Cliff Hanger |  | cliff.zip | `-bank 0 00000000 -bank 1 01000000 -bank 2 00000000 -bank 3 01111000` |
| Cobra Command | ⭐️⭐️ | cobraconv.zip | `-bank 0 11010000 -bank 1 00010001` |
| Dragon's Lair | ⭐️ | dle21.zip | `-bank 1 00110111 -bank 0 10011000` |
| Dragon's Lair II | ⭐️⭐️ | lair2.zip |  |
| Esh's Aurunmilla | ⭐️⭐️ | esh.zip |  |
| Galaxy Ranger | ⭐️ | galaxy.zip | `-bank 0 00000000 -bank 1 00000000` |
| GP World | ⭐️ | gpworld.zip |  |
| Interstellar Laser Fantasy | ⭐️ | interstellar.zip | `-bank 0 00100001 -bank 1 00000000` |
| M.A.C.H. 3 | ⭐️ | mach3.zip | `-bank 0 01000100` |
| Road Blaster | ⭐️ | roadblaster.zip | `-bank 0 00000000 -bank 1 00000000` |
| Space Ace | ⭐️⭐️ | sae.zip | `-bank 1 01100111 -bank 0 10011000` |
| Super Don Quix-Ote | ⭐️ | sdq.zip | `-bank 0 00100001 -bank 1 00000000` |
| Thayer's Quest |  | tq.zip | `-bank 0 00010000` |
| Us Vs Them | ⭐️ | uvt.zip | `-bank 0 00000000` |

### Joystick buttons

>Only one joystick is supported, only player 1.
{.is-info}

* Use the left stick for movement and the standard buttons for actions.

>For most games, only the B button is used (A for Xbox controllers, X for Playstation).
{.is-info}