---
title: Dolphin
description: 
published: true
date: 2025-02-06T23:48:48.480Z
tags: gamecube, gc, dolphin
editor: markdown
dateCreated: 2023-09-06T13:00:13.436Z
---

**Dolphin** es un emulador para dos recientes consolas de juegos de Nintendo: la GameCube y la Wii. Permite a los jugadores en PC disfrutar de los juegos realizados para estas dos consolas en **full HD** (1080p) con diferentes mejoras: compatibilidad con todos los controladores de PC, velocidad turbo, juego en línea en red, y mucho más.

Más de 200 personas han trabajado duro durante años para crear Dolphin. La lista de contribuyentes se puede encontrar [en GitHub](https://github.com/dolphin-emu/dolphin/graphs/contributors).

## ![](/emulators/license.svg) Licencia

Este núcleo está bajo la licencia [**GPLv2**](https://github.com/dolphin-emu/dolphin/blob/master/license.txt).

## ![](/emulators/compatibility.png) Compatibilidad

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ❌ | ❌ | ❌ | ❌ | ❌ | ❌ | ✅ | ❌ |

## ![](/emulators/features.png) Funcionalidades

| Funcionalidad | Soportado |
| :---: | :---: |
| Netplay | ✔ |

## ![](/emulators/bios.svg) BIOS

Aunque son puramente opcionales, existen BIOS de GameCube por zona geográfica, a saber Europa (EUR), Estados Unidos (USA) y Japón (JAP).

### Lista de BIOS opcionales

| Nombre de archivo | Descripción | MD5 | Proporcionado |
| :---: | :---: | :---: | :---: |
| IPL.bin | BIOS GameCube EUR | 0cdda509e2da83c85bfe423dd87346cc | ❌ |
| IPL.bin | BIOS GameCube JAP | fc924a7c879b661abc37cec4f018fdf3 | ❌ |
| IPL.bin | BIOS GameCube USA | 339848a0b7c2124cf155276c1e79cbd0 | ❌ |

### Ubicación

Coloque los BIOS de la siguiente manera:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 gamecube
┃ ┃ ┃ ┃ ┣ 📁 EUR
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **IPL.bin**
┃ ┃ ┃ ┃ ┣ 📁 JAP
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **IPL.bin**
┃ ┃ ┃ ┃ ┣ 📁 USA
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **IPL.bin**

## ![](/emulators/isos.svg) Isos

### Extensiones soportadas

Los isos deben tener las siguientes extensiones:

* .ciso
* .gc
* .gcm
* .gcz
* .iso
* .m3u
* .rvz

### Ubicación

Coloque los isos de la siguiente manera:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 gamecube
┃ ┃ ┃ ┃ ┣ 🗒 **juego.rvz**

>Se recomienda encarecidamente el uso de isos en formato **Redump**.
{.is-success}

>Para más información sobre los isos, visite [este tutorial](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/external-links.png) Enlaces externos

* **Código fuente utilizado** : [https://github.com/dolphin-emu/dolphin/](https://github.com/dolphin-emu/dolphin/)
* **Página oficial** : [https://www.dolphin-emu.org/](https://www.dolphin-emu.org/)
