---
title: Rufus
description: 
published: true
date: 2023-09-17T07:25:46.885Z
tags: rufus
editor: markdown
dateCreated: 2023-09-17T07:25:46.885Z
---

Rufus es un programa para formatear y crear medios USB de arranque, como memorias USB, memorias flash, etc.

Es particularmente útil cuando:

* Quieres crear un medio de instalación USB a partir de una imagen ISO de arranque (Windows, Linux, UEFI, etc.).
* Quieres trabajar en una máquina que no tiene un sistema operativo instalado.
* Quieres programar una BIOS u otro tipo de firmware desde DOS.
* Quieres ejecutar una utilidad de bajo nivel

A pesar de su pequeño tamaño, ¡Rufus proporciona todo lo que necesitas!

Ah, y Rufus es rápido. Por ejemplo, es aproximadamente el doble de rápido que [UNetbootin](http://unetbootin.sourceforge.net/), [Universal USB Installer](http://www.pendrivelinux.com/universal-usb-installer-easy-as-1-2-3) o [Windows 7 USB Utility](https://www.microsoft.com/en-us/download/windows-usb-dvd-download-tool), para la creación medios de instalación USB a partir de una imagen ISO de Windows 7.  
También es un poquito más rápido que otros programas para la creación de medios USB `bootables` bajo Linux.

* [Descargad el fichero ejecutable](https://rufus.akeo.ie/) y ejecutadlo (no necesita ser instalado).

El fichero ejecutable está firmado digitalmente, y la firma debe indicar:

* Akeo Consulting" (v1.3.0 o superior)
* Pete Batard - Open Source Developer" (v1.2.0 o anterior)

## Notas de soporte DOS

Si creáis un disco USB de arranque con un teclado no estadounidense, Rufus intentará seleccionar una distribución de teclado que coincida con el idioma de vuestro sistema.  
En este caso, se aconseja utilizar [FreeDOS](http://www.freedos.org) en vez de MS-DOS, que es la selección por defecto, ya que soporta más distribuciones de teclado internacionales.

## Notas sobre el soporte de imagenes ISO

Todas las versiones de Rufus, desde la 1.1.0 en adelante, soportan la creación de medios USB de arranque desde una [imagen ISO](http://en.wikipedia.org/wiki/ISO_image) (.iso).

Si no tenéis ningún fichero ISO, podéis crearlo, desde un disco óptico o desde un conjunto de fichero, la operación no es complicada. Basta con utilizar una de las muchas aplicaciones gratuitas de grabación de CD o DVD, como [CDBurnerXP](http://cdburnerxp.se/) o [ImgBurn](http://www.imgburn.com/).

## Licencia

[Licencia Pública General de GNU (GPL) versión 3](http://www.gnu.org/licenses/gpl.html) o posterior.
Eres libre de distribuir, modificar y vender este software, siempre que cumplas con la licencia GPLv3.

Rufus es desarrollado de forma 100% transparente, a partir de su [código fuente público](https://github.com/pbatard/rufus), utilizando un entorno [MinGW32](http://www.mingw.org).