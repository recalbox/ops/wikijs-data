---
title: Accès réseau via Cyberduck
description: 
published: true
date: 2021-09-04T21:47:40.833Z
tags: cyberduck, accès, réseau
editor: markdown
dateCreated: 2021-05-21T08:40:27.175Z
---

## Connexion au réseau avec Cyberduck

Vous allez découvrir comment vous connecter sur Windows et macOS en réseau à votre Recalbox en SFTP avec le logiciel Cyberduck.

Vous pourrez ainsi avoir accès facilement aux différents fichiers de configuration comme par exemple `/boot/recalbox-user-config.txt`.
Si vous êtes un habitué des outils du shell, ce tutoriel n'est pas fait pour vous, il s'adresse aux utilisateurs de Windows et macOS qui ont besoin d'une interface graphique.

>**Cyberduck** est un logiciel gratuit de transfert de fichiers sur Windows et macOS. 
>Il gère de nombreux protocoles : FTP, FTP-SSL, SFTP, WebDAV, Swift, S3, Google Cloud Storage, Windows Azure Storage, Dropbox, Google Drive, Backblaze B2 Cloud Storage, Backspace Cloud Files.
{.is-info}

* Pour commencer, téléchargez [Cyberduck](https://cyberduck.io/).
* Il faut que votre Recalbox soit connectée à votre réseau local. Vous pouvez trouver l’adresse IP de votre Recalbox en suivant [ce tutoriel](./../../../tutorials/network/ip/discover-recalbox-ip).

![](/tutorials/system/access/cyberduck1.png){.full-width}

* Ouvrez Cyberduck et cliquez sur « Ouvrir une connexion » et renseignez les informations suivantes :
  * Protocole : `SFTP (SSH File Transfert Protocol)`
  * Port : `22`
  * Serveur : renseignez l’adresse IP de votre Recalbox
  * Nom d’utilisateur : `root`
  * Mot de passe : `recalboxroot`
* Cliquez sur le bouton `Connecter` et autorisez les empreintes inconnues.

![](/tutorials/system/access/cyberduck2.png){.full-width}

* Pour enregistrer la configuration dans un signet, cliquez sur l'icône de signet en haut à gauche, puis sur le bouton `+` en bas à gauche.

![](/tutorials/system/access/cyberduck3.png){.full-width}

* Profitez-en pour renommer la connexion avant de fermer la fenêtre.

![](/tutorials/system/access/cyberduck4.png){.full-width}

* Voilà le résultat :

![](/tutorials/system/access/cyberduck5.png){.full-width}

Voilà, votre signet est sauvegardé.

Le fichier `recalbox-user-config.txt` se trouve dans `/boot/`.
Le dossier `share` de Recalbox dans `/recalbox/share/`.

## Mettre les droits en écriture sur la carte SD

Pour les bidouilleurs avertis, vous devez passer les droits de la carte SD en écriture, pour modifier le fichier `recalbox-user-config.txt`. 

* Dans le menu de Cyberduck, utilisez l’option `Envoyer une commande`

![](/tutorials/system/access/cyberduck6.png){.full-width}

* Et insérez la commande suivante :

`mount -o remount,rw /boot`

* Voilà, vous pouvez modifier le fichier `recalbox-user-config.txt`. Il vous est conseillé de faire une sauvegarde du fichier d’origine sur votre ordinateur, puis de remplacer celui de la carte SD par le fichier modifié.

[Source](https://forum.recalbox.com/topic/7341/tutoriel-se-connecter-en-ssh-pour-les-nuls-avec-cyberduck-pour-config-txt-osx-et-win)