---
title: Retroarch AI (Traduction en jeu)
description: Traduisez vos jeux en direct !
published: true
date: 2024-11-11T05:33:55.964Z
tags: traduction, retroarch, jeu
editor: markdown
dateCreated: 2021-05-21T07:53:55.915Z
---

## Retroarch AI, c'est quoi ?

Bienvenue dans le futur ! 

Depuis quelques temps, RetroArch permet l'utilisation d'un service de traduction dit "[**OCR**](./../../basic-usage/glossary)**"** (Reconnaissance Optique de Caractères) et de la **synthèse vocale,** ce qui va vous permettre de traduire vos jeux étrangers quasiment instantanément !

>Cette fonction nécessite une connexion internet et au minimum un joystick analogique gauche !!!
{.is-info}

![Avantg](/basic-usage/features/ocr1.png)

![Après](/basic-usage/features/ocr2.png)

## Configuration

Vous le savez bien, tout est mis en œuvre pour que la configuration de ce genre de service soit le plus simple possible. Vous avez la possibilité d'utiliser la clé fourni par défaut ou d'utiliser votre propre compte.

### {.tabset}
#### Compte par défaut

L'utilisation du compte par défaut est très simple :

* Ouvrir votre fichier `recalbox.conf` situé dans `/recalbox/share/system/`
* Chercher la ligne `;global.translate.apikey=RECALBOX`
* Supprimer le `;` en début de ligne
* Profitez pour indiquer au système en quelle langue vous souhaitez avoir la traduction en modifiant le paramètre `global.translate.to`. Vous pouvez toujours laisser le paramètre sur `auto`, RetroArch tentera dans ce cas de deviner la langue cible.

#### Votre compte

La première consiste à créer un compte sur [Ztranslate.net](https://ztranslate.net/) un service de traduction tier appelé API, mais d'autres API peuvent être configurées !

![](/basic-usage/features/ztranslate_signup.png)

* Une fois inscrit et connecté au site, rendez vous dans la section settings pour récupérer votre `API KEY`

![](/basic-usage/features/ztranslate_settings.png)

* Ouvrir votre fichier `recalbox.conf` situé dans `/recalbox/share/system/`
* Chercher la ligne `;global.translate.apikey=RECALBOX`
* Supprimer le `;` en début de ligne puis coller votre `API KEY` à la place de `RECALBOX`
* Profitez pour indiquer au système en quelle langue vous souhaitez avoir la traduction en modifiant le paramètre `global.translate.to`. Vous pouvez toujours laisser le paramètre sur `auto`, RetroArch tentera dans ce cas de deviner la langue cible.

![](/basic-usage/features/recalbox.conf_fr.png)

## C'est parti !

Faites-vous plaisir et oubliez la frustration, lancez un jeu 100 % japonais !

* Pour activer la traduction, rien de plus simple : avec votre manette, appuyez sur les boutons `HOTKEY + JOYSTICK-GAUCHE VERS LE BAS`, le jeu est mis en pause et vous montre une image avec le texte de remplacement.
* Appuyez de nouveau sur la même combinaison de touches pour reprendre le jeu.

Par défaut, Recalbox utilise le mode image plutôt que la synthèse vocale. Pour utiliser cette dernière, une simple [surcharge de configuration](./../../advanced-usage/configuration-override/retroarch-overrides) suffit.