---
title: FAQ
description: 
published: true
date: 2023-12-19T01:00:25.368Z
tags: ps1, faq
editor: markdown
dateCreated: 2021-08-08T14:28:21.609Z
---

## Multi-disk

First of all, we will see the different possibilities offered by Recalbox to change the disc.

### PSP mode

Put all disks in one `.PBP` file. You can change the disk in the Retroarch menu.

#### Advantages:

* Special comands can be used to change disks.
* There is a save file for all disks.

#### Disadvantages:

* `.SBI` files cannot be used in multi-disk games.
* Does not allow to switch to a patched disk (with LibCrypt protection fix).
* It takes some time to change all multi-disk sets to `.PBP` format.

### PCSX-reARMed mode

The following disk is searched in the Retroarch menu by the file system.

#### Advantages:

* Your games are in `.BIN` / `.CUE` format.

#### Disadvantages:

* It doesn't work now.
* It is ugly and slow.
* Saved files are not shared between disks.
* Special commands cannot be used to switch disks.

Therefore, games with LibCrypt and multi-disk protection are a real pain in these two modes.

### Final mode

Just create a `.M3U` file for each multi-disk game. This file will contain the names some files. For example:

![M3U](/tutorials/games/guides/playstation-1/m3u1.png)

The last aspect should be as follows:

![PSX folder](/tutorials/games/guides/playstation-1/m3u2.png)

As you can see in this example, there are `.CUE` / `.BIN` / `.SBI` per disk and one `.M3U` per multi-disk game.

#### Advantages :

* Your games stays in their original format.
* The special commands in game can be used to change discs.
* The `.SBI` files can be used.
* There is a save file for all disks.

There is only advantages.

## Multi-track games

Some games have multiple tracks per disk. This means that the `.CUE` file requires multiple entries (one per track).

Example of a multi-track `.CUE`:

![Multi-track Cue](/tutorials/games/guides/playstation-1/cue1.png)

The last aspect should be the following:

![Multi-track game](/tutorials/games/guides/playstation-1/cue2.png)

## Extensions

### `.BIN` extension: special case

Rom files with the `.BIN` extension are also supported but require a `.CUE` file to be started.

### `.CUE` extension: how does it work?

A `.CUE` file is, quite simply, a text file whose extension has been changed from `.TXT` to `.CUE` . It must have the same name as the `.BIN` file it accompanies and it contains the information necessary to launch the `.BIN` file. This is also working if you have multiple `.BIN` files with only one `.CUE` (this is depending of the games).

### `.M3U` extension: manage multi-CD games

Multiple CDs of the same game can be loaded simultaneously into RetroArch by creating a `.M3U` playlist file (plain text `.TXT` with a `.M3U` extension).

>A .m3u playlist can be used with any extension which have a game into multiple supports, it can be cartridge or CD.
{.is-warning}

## Steps to manage a multi-CD game

You need to create a new file named `<game_name>.m3u`.

>You have 2 examples below with `.BIN` / `.CUE` and `.CHD` extensions.
{.is-info}

### Example with Final Fantasy VII with `.CUE` files

We create the file `Final Fantasy (France).m3u` with the following content:

```
Final Fantasy VII (France) (Disc 1).cue
Final Fantasy VII (France) (Disc 2).cue
Final Fantasy VII (France) (Disc 3).cue
```

### Example with Final Fantasy VII with `.CHD` files

We create the file `Final Fantasy (France).m3u` with the following content:

```
Final Fantasy VII (France) (Disc 1).chd
Final Fantasy VII (France) (Disc 2).chd
Final Fantasy VII (France) (Disc 3).chd
```

## Changing discs during the game

If you need to change the disk while the game is running, you should do the following:

1. Open the "virtual" CD tray: `HK key + Left Joystick (Up direction)`
2. Change the disc: `HK key + Left Joystick (Right or Left direction)`
3. Close the "virtual" CD tray: `HK key + Left Joystick (Up)`

This works if you have your milti-disc set in a `.M3U` file.

## Miscellaneous

### What do I do with the `.ECM` and `.APE` files?

You have to unzip them. See this [guide](https://www.epforums.org/showthread.php?57757-ECM-And-APE-Guide) to do so.

### How do I activate the Dualshock (analogs)?

* In the game, go to the RetroArch menu by pressing `Hotkey` + `B`.
* Select `Controls` > `Port 1 Controls`.

![](/tutorials/games/guides/playstation-1/enable-analog/psxanalog1-en.png){.full-width}

* Select `analog` in the `Device Type` option (same for all the pads you want).

![](/tutorials/games/guides/playstation-1/enable-analog/psxanalog2-en.png){.full-width}

### I don't have the `.CUE` file, how do I do it?

 If you only have a `.BIN` file and no `.CUE` file, you can generate it in several ways:

* Online: [http://nielsbuus.dk/pg/psx_cue_maker/](http://nielsbuus.dk/pg/psx_cue_maker/)
* With this freeware : [CueMaker](https://github.com/thorst/CueMaker/releases/)

You can also get it [from Redump](http://redump.org/cues/psx/) by going to the corresponding game page.

## Glossary

* .ape: compressed file for the .wav file.
* .bin: game data and music tracks.
* .ccd/.img/.sub: cloning of CD files.
* .cue: File in which the tracks on the disc are defined. One per .bin data.
* .ecm: compressed file for the .bin file.
* .pbp : PSP file for PSX games.
* .ppf: patch file for games with LibCrypt protection.
* .sbi: file that contains protection information and that is needed to run protected games in emulators. They must have the same name as the .cue file.
* .wav: music track file. You must rename it to .bin.

## Useful links

* [ECM-APE Guide](https://www.epforums.org/showthread.php?57757-ECM-And-APE-Guide)
* [PSX games list](https://psxdatacenter.com/pal_list.html)
* [.sbi files](http://psxdatacenter.com/sbifiles.html)
* [Cue File Generator](http://nielsbuus.dk/pg/psx_cue_maker/)