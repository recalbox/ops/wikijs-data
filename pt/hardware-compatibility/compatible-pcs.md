---
title: PCs Compatíveis
description: 
published: true
date: 2024-08-14T19:52:58.200Z
tags: pc, compatibilidade
editor: markdown
dateCreated: 2022-02-27T17:34:29.675Z
---

>**Informações**:
>
>Se você **não encontrar seu PC** nas tabelas abaixo, ele **ainda não foi testado**.
>Você também pode testar o hardware com a versão mais recente do Recalbox, caso ainda não tenha sido feito, as informações podem ser atualizadas (informe-nos se o hardware testado é compatível).
>
>* Especifique o **Fabricante** e o **Modelo**.
>* Se for um PC montado, especifique a CPU e a GPU.
>* Sinta-se à vontade para **adicionar detalhes sobre o desempenho** de sua configuração.
>* Se você tiver algum problema, por favor nos avise.
{.is-info}

## Modelos de PC

### Dell

| Nome | Status | Versão Recalbox | Notas |
| :---: | :---: | :---: | :---: |
| Dell Optiplex 390 i5-2400 3,10 GHz | ✅ | 6.0 Rc3 | 100% Compatível |
| Dell Optiplex 990 i3 2100 | ✅ | 13.07.18 | 100% Compatível |
| Dell Optiplex 990 USFF i5 2400S 2,5 GHz | ✅ | 6.0.1 | 100% Compatível |
| Dell Optiplex 3020 micro i3 4150T | ✅ | 6.0 Rc3 | 100% Compatível |

### Intel

| Nome | Status | Versão Recalbox | Notas |
| :---: | :---: | :---: | :---: |
| Intel NUC7i3BNK | ✅ | 4.1 | i3-7100U / HD Graphics 620 / BIOS 0036:OK / BIOS 0052:Sem sonda de temperatura |
| Intel NUC7I7BNH | ✅ | 9.1 | 100% compatível, cerca de 60 FPS para todos os sistemas, CPU i7-7567U Dual-Core, GPU Intel HD Graphics 650 |
| Intel NUC7i5BNH | ✅ | 6.1 | 100% compatível, cerca de 60 FPS para todos os sistemas, CPU Core i5-7260U, GPU Intel IRIS Plus 640 |
| Intel NUC6I7KYK | ✅ | 4.1 | Totalmente compatível, Cerca de 60 FPS para todos os consoles, BIOS mais recente 0050, todos os hardwares estão ok: CPU Core i7-6770HQ, gráficos Intel IRIS PRO 580 totalmente suportados, wifi, bluetooth, som, usbs, SSD SATA Samsung 1TB |
| Intel NUC8i7BEH | ✅ | 6.1 | 100% compatível, cerca de 60 FPS para todos os sistemas, CPU Core i7-8559U, GPU Intel IRIS Plus 655 |
| Intel N95 | ✅ | 9.2 |  |
| Intel N100 | ✅ | 9.2 | 100% compatível. GPU Intel UHD |

### Diversos

| Nome | Status | Versão Recalbox | Notas |
| :---: | :---: | :---: | :---: |
| Desktop HP Slim 270-P026 i3-7100T | ✅ | 18.07.13 | 100% Compatível |
| Laptop 17,3" MSI GP72M 7RDX-871XFR | ✅ | 4.1 | Cerca de 60 FPS para todos os consoles na versão x64 com parâmetros padrão, BIOS mais recente, todos os hardwares estão ok: CPU Core i7-7700HQ, gráficos Intel HD Graphics 630 totalmente suportado, wifi , bluetooth, som (nos alto falantes do notebook+fones+HDMI), usbs, SSD SATA Samsung 1TB, tudo ok! Por ter bateria, os jogos não são mais interrompidos! (único inconveniente é que não suporta a GPU Nvidia GTX 1050 ainda devido ao kernel linux usado atualmente) |
| Taichi Z270 + I5 7500 | ✅ | 4.1 | Cerca de 60 FPS para todos os consoles em x64 |
| ZOTAC ZBOX Nano AD10 | ✅ | 4.1 | Wii/NGC ok mas não jogável |

### AppleMac

| Nome | Situação | Versão Recalbox | Notas |
| :---: | :---: | :---: | :---: |
| MacBook Pro (13 unidades, estreia em 2011) | ✅ | 4.1 | 100% compatível |
| iMac (21,5 pouces, meados de 2011) | ✅ | 4.1 | 100% compatível, alguns pequenos bugs gráficos no menu |
| Mac Pro (final de 2013) | ❌ | 4.1 | Não está funcionando. Não é possível nem inicializar |
| MacBook (13 pouces, final de 2009) | ✅ | 7.2.1 | 100% compatível |
| MacBook Pro (15 unidades, 2018) | ✅ | 7.3 | Sem som, teclado não funciona. [Desative o chip seguro T2 para inicializar](./../tutorials/others/disable-mac-t2-chip) |

## CPU

### Intel

| Nome | Status | Versão Recalbox | Notas |
| :---: | :---: | :---: | :---: |
| Intel i3-4150T 3,00 GHz | ✅ | 6.0 Rc3 | Cerca de 60 FPS para todos os consoles na versão x64 com parâmetros padrão, dolphin funciona muito bem com parâmetros padrão |
| Intel Core i3-7100T | ✅ | 18.07.13 | Cerca de 60 FPS para todos os consoles na versão x64 com parâmetros padrão |
| Intel Core i3-7100U | ✅ | 4.1 | |
| Intel Core i5-2400 3,10 GHz | ✅ | 4.1 | Funcionando bem para todos os emuladores, exceto para emuladores de gamecube e wii (melhor desempenho com placa gráfica) |
| Intel Core i5-2400S 2,5 GHz | ✅ | 7.0.1 | |
| Intel Core i5-3570K 3,40 GHz | ✅ | 6.0 Rc3 | Cerca de 60 FPS para todos os consoles na versão x64 com parâmetros padrão |
| Intel Core i5-4570S | ✅ | 4.1 | |
| Intel Core i5-6500 | ✅ | 4.1 | |
| Intel Core i5-7500 | ✅ | 4.1 | |
| Intel Core i7-2600K | ✅ | 4.1 | Cerca de 60 FPS para todos os consoles na versão x64 com parâmetros padrão |
| Intel Core i7-6770HQ | ✅ | 4.1 | Cerca de 60 FPS para todos os consoles na versão x64 com parâmetros padrão |
| Intel Core i7-7700HQ | ✅ | 4.1 | Cerca de 60 FPS para todos os consoles na versão x64 com parâmetros padrão |
| Intel Pentium G2130 3.20GHZ | ✅ | 4.1 | |
| Intel G4600 | ✅ | 18.04.20 | Cerca de 60 FPS para todos os consoles na versão x64 com parâmetros padrão |

### AMD

| Nome | Status | Versão Recalbox | Notas |
| :---: | :---: | :---: | :---: |
| AMD X2 4850B | ✅ | 4.1 | |
| AMD Ryzen 9 5900HX | ✅ | 8.0 | |
| AMD Ryzen 5 1600  | ✅ | 8.0 | |
| AMD Ryzen 5 5600X | ✅ | 8.0 | |

## GPU

>**Atenção**:
>Por favor, tenha em mente que a placa gráfica não é tudo, o kernel Linux contido no Recalbox também precisa ser capaz de lidar com o dispositivo.
{.is-info}

### Nouveau

Nouveau é um projeto da fundação X.Org e Freedesktop.org para fornecer drivers gráficos Nvidia isentos de royalties por engenharia reversa.

O Nouveau é baseado nos drivers nv gratuitos (mas obscuros) mantidos pela Nvidia para suporte 2D.

Para dar suporte ao OpenGL, o projeto usa o Mesa 3D, mas foi alterado em fevereiro de 2018 por seu sucessor Gallium3D4.5. O suporte a OpenCL também é feito através do Gallium3D.

#### Processo utilizado

O projeto utiliza a técnica conhecida como engenharia reversa em placas gráficas Nvidia estudando como os drivers 3D para Linux, fornecidos pelo fabricante (sob licença proprietária), funcionam sem tocar nos drivers. Essa maneira de fazer as coisas permite que o projeto evite conflitos com a licença da Nvidia.

* De acordo com a [página de recursos do noveau](https://nouveau.freedesktop.org/wiki/FeatureMatrix/)
* De acordo com a compatibilidade com NVIDIA ([Codinomes da NVidia](https://nouveau.freedesktop.org/wiki/CodeNames/))
* O novo driver no Recalbox (versão 1.0.15)

>**Observação:**
>Os drivers Noveau são usados por padrão quando uma placa gráfica Nvidia é detectada, mas não é compatível com os drivers oficiais.
{.is-warning}

### NVIDIA

As placas gráficas Nvidia são compatíveis com o **driver oficial** em sua versão 460.67 que você pode encontrar a lista de placas gráficas compatíveis [aqui](https://gitlab.com/recalbox/recalbox-hardware/blob/master/videocard/nvidiacheckcompatibility-460.txt).

### Intel

Lista de placas gráficas integradas ao processador.

Seu [código-fonte](https://cgit.freedesktop.org/xorg/driver/xf86-video-intel/tree/?id=b57abe20e81f4b8e4dd203b6a9eda7ff441bc8ce) não é muito explícito sobre as placas gráficas suportadas.

| Nome | Status | Versão Recalbox | Notas |
| :---: | :---: | :---: | :---: |
| Intel® HD Graphics 630 | ✅ | 4.1 | Cerca de 60 FPS para todos os consoles na versão x64 com parâmetros padrão |
| Intel® HD Graphics 3000 | ✅ | 4.1 | Cerca de 60 FPS para todos os consoles na versão x64 com parâmetros padrão |
| Intel® Iris™ Plus Graphics 620 | ✅ | 4.1 | |
| Intel® Iris™ Pro Graphics 580 | ✅ | 4.1 | Cerca de 60 FPS para todos os consoles na versão x64 com parâmetros padrão |
| Intel® HD Graphics 4600 | ✅ | 4.1 | Cerca de 60 FPS para todos os consoles na versão x64 com parâmetros padrão |
| Intel® HD Graphics 4400 | ✅ | 6.0 rc3 | Cerca de 60 FPS para todos os consoles na versão x64 com parâmetros padrão |
| Intel® HD Graphics 4850 | ✅ | 6.0 DragonBlaze | Cerca de 60 FPS para todos os consoles na versão x64 com parâmetros padrão |

### AMD

Seu [código-fonte](https://cgit.freedesktop.org/xorg/driver/xf86-video-amdgpu/tree/?h=xf86-video-amdgpu-1.4.0) não é muito explícito sobre as placas gráficas suportadas .

| Nome | Status | Versão Recalbox | Notas |
| :--- | :---: | :---: | :---: |
| AMD Radeon HD 4850 512mo | ✅ | 6,0 Rc3 | funciona com CPU Intel i5-3570K |
| AMD Radeon HD 4890 1Go | ✅ | 4.1 | |
| AMD Radeon HD 6750M | ✅ | 4.1 | alguns pequenos bugs gráficos no menu |
| AMD Radeon HD 7970 3G | ❌ | 18.04.20 | Não funciona (menu é muito lento e não pode ser reproduzido) |
| AMD Radeon HD 8280E | ❌ | 18.07.13 | Não funciona (menu é muito lento e não pode ser reproduzido) |
| AMD Radeon RX 480 8GB | ❌ | 6.0 RC2 | Não funciona (menu é muito lento e não pode ser reproduzido) |