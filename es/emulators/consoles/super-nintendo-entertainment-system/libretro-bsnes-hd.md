---
title: Libretro bsnes HD
description: 
published: true
date: 2024-07-29T18:19:17.316Z
tags: libretro, snes, bsnes, 8.1+
editor: markdown
dateCreated: 2023-09-08T09:19:13.449Z
---

bsnes-hd (llamado "Modo HD 7, para bsnes" en las primeras betas) es un fork de bsnes (gran emulador SNES por byuu) que añade funcionalidades de video HD, como "true color", "modo de juego en pantalla ancha" y otras.

## ![](/emulators/license.svg) Licencia

Este núcleo está bajo la licencia [**GPLv3**](https://github.com/libretro/bsnes-hd/blob/master/LICENSE).

## ![](/emulators/compatibility.png) Compatibilidad

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ❌ | ❌ | ✅🐌 | ✅ | ❌ | ❌ | ✅ | ✅ |

🐌 Bajo rendimiento pero jugable

## ![](/emulators/features.png) Características

## ![](/emulators/bios.svg) BIOS

>**No se requiere BIOS.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Extensiones soportadas

Las roms deben tener la siguiente extensión:

* .fig
* .gd3
* .sfc
* .smc
* .swc
* .zip
* .7z

Este sistema soporta roms comprimidas en formato .zip/.7z. Sin embargo, se trata solo de un archivo.

Los archivos contenidos en los .zip/.7z deben corresponder a las extensiones mencionadas anteriormente.
Además, cada archivo .zip/.7z debe contener solo una rom comprimida.

### Ubicación

Coloca las roms de la siguiente manera:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 snes
┃ ┃ ┃ ┃ ┣ 🗒 **juego.zip**

>Se recomiendan encarecidamente las roms en formato **No-Intro**.
{.is-success}

>Para más información sobre las roms, visita [este tutorial](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

>Para poder conservar tus configuraciones personalizadas durante una actualización, te recomendamos utilizar nuestra funcionalidad [Sobrecargas de configuración](./../../../advanced-usage/configuration-override).
{.is-info}

### Acceder a las opciones

Puedes configurar diversas opciones de dos maneras diferentes.

* A través del menú RetroArch:

┣ 📁 Menu RetroArch
┃ ┣ 📁 Opciones del núcleo
┃ ┃ ┣ 🧩 Name_option

* A través del archivo `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Opciones del núcleo

| Opciones | Valores de las opciones | Variable (para sobrecarga) | Valores de las variables |
| --- | --- | --- | --- |
| Color Adjustement - Luminance | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `bsnes_video_luminance` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
| Color Adjustement - Saturation | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ / `110` / `120` / `130` / `140` / `150` / `160` / `170` / `180` / `190` / `200` | `bsnes_video_saturation` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` / `110` / `120` / `130` / `140` / `150` / `160` / `170` / `180` / `190` / `200` |
| Color Adjustement - Gamma | `100` ✅ / `110` / `120` / `130` / `140` / `150` / `160` / `170` / `180` / `190` / `200` | `bsnes_video_gamma` | `100` / `110` / `120` / `130` / `140` / `150` / `160` / `170` / `180` / `190` / `200` |
| Pixel Aspect Correction | `Activado` / `Desactivado` ✅ | `bsnes_video_aspectcorrection` | `ON` / `OFF` |
| Crop Overscan | `12 píxeles` ✅ / `8 píxeles` | `bsnes_ppu_show_overscan` | `OFF` / `ON` |
| Blur Emulation | `Activado` / `Desactivado` ✅ | `bsnes_blur_emulation` | `ON` / `OFF` |
| Hotfixes | `Activado` / `Desactivado` ✅ | `bsnes_hotfixes` | `ON` / `OFF` |
| Entropy (randomization) | `Baja` ✅ / `Alta` / `Ninguna` | `bsnes_entropy` | `Low` / `High` / `None` |
| Overclocking - CPU | `10%` / `20%` / `30%` / `40%` / `50%` / `60%` / `70%` / `80%` / `90%` / `100%` ✅ / `110%` / `120%` / `130%` / `140%` / `150%` / `160%` / `170%` / `180%` / `190%` / `200%` / `210%` / `220%` / `230%` / `240%` / `250%` / `260%` / `270%` / `280%` / `290%` / `300%` / `310%` / `320%` / `330%` / `340%` / `350%` / `360%` / `370%` / `380%` / `390%` / `400%` | `bsnes_cpu_overclock` | `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` / `110` / `120` / `130` / `140` / `150` / `160` / `170` / `180` / `190` / `200` / `210` / `220` / `230` / `240` / `250%` / `260%` / `270%` / `280%` / `290%` / `300%` / `310%` / `320%` / `330%` / `340%` / `350%` / `360%` / `370%` / `380%` / `390%` / `400%` |
| CPU Fast Math | `Activado` / `Desactivado` ✅ | `bsnes_cpu_fastmath` | `ON` / `OFF` |
| Overclocking - Coprocesador SA1 | `10%` / `20%` / `30%` / `40%` / `50%` / `60%` / `70%` / `80%` / `90%` / `100%` ✅ / `110%` / `120%` / `130%` / `140%` / `150%` / `160%` / `170%` / `180%` / `190%` / `200%` / `210%` / `220%` / `230%` / `240%` / `250%` / `260%` / `270%` / `280%` / `290%` / `300%` / `310%` / `320%` / `330%` / `340%` / `350%` / `360%` / `370%` / `380%` / `390%` / `400%` | `bsnes_sa1_overclock` | `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` / `110` / `120` / `130` / `140` / `150%` / `160%` / `170%` / `180%` / `190%` / `200%` / `210%` / `220%` / `230%` / `240%` / `250%` / `260%` / `270%` / `280%` / `290%` / `300%` / `310%` / `320%` / `330%` / `340%` / `350%` / `360%` / `370%` / `380%` / `390%` / `400%` |
| Overclocking - Coprocesador SuperFX | `10%` / `20%` / `30%` / `40%` / `50%` / `60%` / `70%` / `80%` / `90%` / `100%` ✅ / `110%` / `120%` / `130%` / `140%` / `150%` / `160%` / `170%` / `180%` / `190%` / `200%` / `210%` / `220%` / `230%` / `240%` / `250%` / `260%` / `270%` / `280%` / `290%` / `300%` / `310%` / `320%` / `330%` / `340%` / `350%` / `360%` / `370%` / `380%` / `390%` / `400%` / `410%` / `420%` / `430%` / `440%` / `450%` / `460%` / `470%` / `480%` / `490%` / `500%` / `510%` / `520%` / `530%` / `540%` / `550%` / `560%` / `570%` / `580%` / `590%` / `600%` / `610%` / `620%` / `630%` / `640%` / `650%` / `660%` / `670%` / `680%` / `690%` / `700%` / `710%` / `720%` / `730%` / `740%` / `750%` / `760%` / `770%` / `780%` / `790%` / `800%` | `bsnes_sfx_overclock` | `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` / `110` / `120` / `130` / `140%` / `150%` / `160%` / `170%` / `180%` / `190%` / `200%` / `210%` / `220%` / `230%` / `240%` / `250%` / `260%` / `270%` / `280%` / `290%` / `300%` / `310%` / `320%` / `330%` / `340%` / `350%` / `360%` / `370%` / `380%` / `390%` / `400%` / `410%` / `420%` / `430%` / `440%` / `450%` / `460%` / `470%` / `480%` / `490%` / `500%` / `510%` / `520%` / `530%` / `540%` / `550%` / `560%` / `570%` / `580%` / `590%` / `600%` / `610%` / `620%` / `630%` / `640%` / `650%` / `660%` / `670%` / `680%` / `690%` / `700%` / `710%` / `720%` / `730%` / `740%` / `750%` / `760%` / `770%` / `780%` / `790%` / `800%` |
| PPU (Video) - Modo rápido | `Activado` ✅ / `Desactivado` | `bsnes_ppu_fast` | `ON` / `OFF` |
| PPU (Video) - Desentrelazar | `Activado` ✅ / `Desactivado` | `bsnes_ppu_deinterlace` | `ON` / `OFF` |
| PPU (Video) - Sin límite de sprites | `Activado` ✅ / `Desactivado` | `bsnes_ppu_no_sprite_limit` | `ON` / `OFF` |
| PPU (Video) - Sin bloqueo VRAM | `Activado` / `Desactivado` ✅ | `bsnes_ppu_no_vram_blocking` | `ON` / `OFF` |
| DSP (Audio) - Modo rápido | `Activado` ✅ / `Desactivado` | `bsnes_dsp_fast` | `ON` / `OFF` |
| DSP (Audio) - Interpolación cúbica | `Activado` / `Desactivado` ✅ | `bsnes_dsp_cubic` | `ON` / `OFF` |
| DSP (Audio) - RAM de Echo Shadow | `Activado` / `Desactivado` ✅ | `bsnes_dsp_echo_shadow` | `ON` / `OFF` |
| Coprocesador - Modo rápido | `Activado` ✅ / `Desactivado` | `bsnes_coprocessor_delayed_sync` | `ON` / `OFF` |
| Coprocesador - Preferir HLE | `Activado` ✅ / `Desactivado` | `bsnes_coprocessor_prefer_hle` | `ON` / `OFF` |
| BIOS preferido del Super Game Boy (Requiere reinicio) | `Super Game Boy (SGB1.sfc)` ✅ / `Super Game Boy 2 (SGB2.sfc)` | `bsnes_sgb_bios` | `SGB1.sfc` / `SGB2.sfc` |
| Ejecución adelantada interna | `Desactivado` ✅ / `1 frame` / `2 frames` / `3 frames` / `4 frames` | `bsnes_run_ahead_frames` | `OFF` / `1` / `2` / `3` / `4` |
| Esperar roms con cabecera para parches IPS | `Activado` / `Desactivado` ✅ | `bsnes_ips_headered` | `ON` / `OFF` |
| HD Mode 7 - Escala | `Desactivado` / `240p (1x)` / `480p (2x)` ✅ / `720p (3x)` / `960p (4x)` / `1200p (5x)` / `1440p (6x)` / `1680p (7x)` / `1920p (8x)` / `2160p (9x)` / `2400p (10x)` | `bsnes_mode7_scale` | `disable` / `1x` / `2x` / `3x` / `4x` / `5x` / `6x` / `7x` / `8x` / `9x` / `10x` |
| HD Mode 7 - Corrección de perspectiva | `Auto (Amplio)` ✅ / `Auto (Medio)` / `Auto (Estrecho)` / `On (Amplio)` / `On (Medio)` / `On (Estrecho)` / `Desactivado` | `bsnes_mode7_perspective` | `auto (wide)` / `auto (medium)` / `auto (narrow)` / `on (wide)` / `on (medium)` / `on (narrow)` / `off` |
| HD Mode 7 - Sobremuestreo | `Desactivado` ✅ / `2x` / `3x` / `4x` / `5x` / `6x` / `7x` / `8x` / `9x` / `10x` | `bsnes_mode7_supersample` | `none` / `2x` / `3x` / `4x` / `5x` / `6x` / `7x` / `8x` / `9x` / `10x` |
| HD Mode 7 - Mosaico HD->SD | `no-HD` / `ignore` / `1x scale` ✅ | `bsnes_mode7_mosaic` | `non-HD` / `ignore` / `1x scale` |
| HD Background Color Radius | `1` / `2` / `3` / `4` ✅ / `5` / `6` / `7` / `8` / `Desactivado` | `bsnes_mode7_bgGrad` | `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `0` |
| HD Windowing (Experimental) | `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `Desactivado` ✅ | `bsnes_mode7_windRad` | `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `0` |
| Modo pantalla ancha | `Solo para escenas del Modo 7` ✅ / `Activar para todas las escenas` / `Desactivado` | `bsnes_mode7_wsMode` | `Mode 7` / `all` / `none` |
| Modo pantalla ancha - Relación de aspecto | `16:9` ✅ / `16:10` / `21:9` / `2:1` / `4:3` / `Desactivado` | `bsnes_mode7_widescreen` | `16:9` / `16:10` / `21:9` / `2:1` / `4:3` / `none` |
| Modo pantalla ancha - Fondo 1 | `Activado` / `Activar si es de ancho de pantalla y la posición X es 0` / `Activar si es de ancho de pantalla y la posición XY es 0` ✅ / `Activar sobre la línea 40` / `Activar bajo la línea 40` / `Activar sobre la línea 80` / `Activar bajo la línea 80` / `Activar sobre la línea 120` / `Activar bajo la línea 120` / `Activar sobre la línea 160` / `Activar bajo la línea 160` / `Activar sobre la línea 200` / `Activar bajo la línea 200` / `Ocultar 8 píxeles laterales` / `Ocultar 8 píxeles negros laterales` / `Desactivar pantalla ancha para esta capa` / `Ocultar capa completamente` | `bsnes_mode7_wsbg1` | `on` / `auto horizontal` / `auto horz and vert` / `above line 40` / `below line 40` / `above line 80` / `below line 80` / `above line 120` / `below line 120` / `above line 160` / `below line 160` / `above line 200` / `below line 200` / `crop edges` / `auto crop edges` / `off` / `disable entirely` |
| Modo pantalla ancha - Fondo 2 | `Activado` / `Activar si es de ancho de pantalla y la posición X es 0` / `Activar si es de ancho de pantalla y la posición XY es 0` ✅ / `Activar sobre la línea 40` / `Activar bajo la línea 40` / `Activar sobre la línea 80` / `Activar bajo la línea 80` / `Activar sobre la línea 120` / `Activar bajo la línea 120` / `Activar sobre la línea 160` / `Activar bajo la línea 160` / `Activar sobre la línea 200` / `Activar bajo la línea 200` / `Ocultar 8 píxeles laterales` / `Ocultar 8 píxeles negros laterales` / `Desactivar pantalla ancha para esta capa` / `Ocultar capa completamente` | `bsnes_mode7_wsbg2` | `on` / `auto horizontal` / `auto horz and vert` / `above line 40` / `below line 40` / `above line 80` / `below line 80` / `above line 120` / `below line 120` / `above line 160` / `below line 160` / `above line 200` / `below line 200` / `crop edges` / `auto crop edges` / `off` / `disable entirely` |
| Modo pantalla ancha - Fondo 3 | `Activado` / `Activar si es de ancho de pantalla y la posición X es 0` / `Activar si es de ancho de pantalla y la posición XY es 0` ✅ / `Activar sobre la línea 40` / `Activar bajo la línea 40` / `Activar sobre la línea 80` / `Activar bajo la línea 80` / `Activar sobre la línea 120` / `Activar bajo la línea 120` / `Activar sobre la línea 160` / `Activar bajo la línea 160` / `Activar sobre la línea 200` / `Activar bajo la línea 200` / `Ocultar 8 píxeles laterales` / `Ocultar 8 píxeles negros laterales` / `Desactivar pantalla ancha para esta capa` / `Ocultar capa completamente` | `bsnes_mode7_wsbg3` | `on` / `auto horizontal` / `auto horz and vert` / `above line 40` / `below line 40` / `above line 80` / `below line 80` / `above line 120` / `below line 120` / `above line 160` / `below line 160` / `above line 200` / `below line 200` / `crop edges` / `auto crop edges` / `off` / `disable entirely` |
| Modo pantalla ancha - Fondo 4 | `Activado` / `Activar si es de ancho de pantalla y la posición X es 0` / `Activar si es de ancho de pantalla y la posición XY es 0` ✅ / `Activar sobre la línea 40` / `Activar bajo la línea 40` / `Activar sobre la línea 80` / `Activar bajo la línea 80` / `Activar sobre la línea 120` / `Activar bajo la línea 120` / `Activar sobre la línea 160` / `Activar bajo la línea 160` / `Activar sobre la línea 200` / `Activar bajo la línea 200` / `Ocultar 8 píxeles laterales` / `Ocultar 8 píxeles negros laterales` / `Desactivar pantalla ancha para esta capa` / `Ocultar capa completamente` | `bsnes_mode7_wsbg4` | `on` / `auto horizontal` / `auto horz and vert` / `above line 40` / `below line 40` / `above line 80` / `below line 80` / `above line 120` / `below line 120` / `above line 160` / `below line 160` / `above line 200` / `below line 200` / `crop edges` / `auto crop edges` / `off` / `disable entirely` |
| Modo pantalla ancha - Sprites | `Renderizar cuando en área segura` ✅ / `Renderizar en cualquier lugar (inseguro)` / `Recortar en los bordes de pantalla ancha` / `Ocultar sprites completamente` | `bsnes_mode7_wsobj` | `safe` / `unsafe` / `clip` / `disable entirely` |
| Modo pantalla ancha - Color de fondo del área | `Rellenar si la escena es de pantalla ancha` ✅ / `Siempre rellenar` / `Usar fondo negro` | `bsnes_mode7_wsBgCol` | `auto` / `color` / `black` |
| Modo pantalla ancha - Ignorar efectos de ventana | `Solo efectos 'exteriores'` ✅ / `Solo efectos 'exteriores' y 'siempre'` / `Todos los efectos de ventana` / `Desactivado` | `bsnes_mode7_igwin` | `outside` / `outside and always` / `all` / `none` |
| Modo pantalla ancha - Coordenada X de fallback | `40` / `88` / `128 (Centro)` ✅ / `168` / `216` | `bsnes_mode7_igwinx` | `40` / `88` / `128` / `168` / `216` |
| Modo pantalla ancha - Marcador | `Mostrar líneas en los bordes` / `Oscurecer áreas` / `Desactivado` ✅ | `bsnes_mode7_wsMarker` | `lines` / `darken` / `none` |
| Modo pantalla ancha - Alfa del marcador | `1/1` ✅ / `1/2` / `1/3` / `1/4` / `1/5` / `1/6` / `1/7` / `1/8` / `1/9` / `1/10` | `bsnes_mode7_wsMarkerAlpha` | `1/1` / `1/2` / `1/3` / `1/4` / `1/5` / `1/6` / `1/7` / `1/8` / `1/9` / `1/10` |
| Modo pantalla ancha - Estirar ventana | `Activado` / `Desactivado` ✅ | `bsnes_mode7_strWin` | `ON` / `OFF` |

## ![](/emulators/external-links.png) Enlaces externos

* **Código fuente utilizado** : [https://github.com/libretro/bsnes-hd/](https://github.com/libretro/bsnes-hd/)
