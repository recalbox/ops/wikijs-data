---
title: Preparation
description: 
published: true
date: 2021-08-08T19:07:03.092Z
tags: preparation, netplay
editor: markdown
dateCreated: 2021-08-05T17:38:29.229Z
---

## I - Preparing your roms

You must follow these steps to get your roms ready for Netplay.

### A - Netplay Compatible Core/Romsets Libretro

#### 1. Arcades

| Systems | Core | Romset |
| :---: | :---: | :---: |
| **FinalBurn Neo** | Libretro FbNeo | 1.0.0.01 |
| **Mame** | |
| Libretro Mame 2003 | Mame 0.78 |
| Libretro Mame 2003Plus | Mame 0.78 to 0.188 |
| Libretro Mame 2010 | Mame 0.139 |
| **Neo-geo** | Libretro FbNeo | 1.0.0.01 |

#### 2. Fantasy

| Systems | Core | Romset |
| :---: | :---: | :---: |
| **TIC80** | Libretro Tic80 |  |

#### 3. Consoles

| Systems | Core | Romset |
| :---: | :---: | :---: |
| **Atari 2600** | Libetro Stella | No Intro |
| **Famicom Disk System** |  | No-Intro |
|  | Libretro FCEUmm |  |
|  | Libretro Nestopia |  |
|  | Libretro Mesen |  |
| **Master System** |  | No-Intro |
|  | Libretro PicoDrive |  |
|  | Libretro Genesisplusgx |  |
| **Megadrive** |  | No-Intro |
|  | Libretro PicoDrive |  |
|  | Libretro Genesisplusgx |  |
| **Nintendo Entertainment System** |  | No-Intro |
|  | Libretro FCEUmm |  |
|  | Libretro FCEUNext |  |
|  | Libretro Nestopia |  |
|  | Libretro Quicknes |  |
|  | Libretro Mesen |  |
| **Super Nintendo** |  | No-Intro |
|  | Libretro Snes9x (enable « multitap » option) |  |
|  | Libretro Snes9x_2002 |  |
|  | Libretro Snes9x_2005 |  |
|  | Libretro Snes9x_2010 |  |
| **Pc-Engine** |  | No-Intro |
|  | Libretro Mednafen_supergrafx |  |
|  | Libretro Mednafen_PCE_FAST |  |
| **Pc-Engine CD** |  | Redump |
|  | Libretro Mednafen_PCE_FAST |  |
|  | Libretro Mednafen_supergrafx |  |
| **PC-FX** | Libretro Mednafen_PCFX | Redump |
| **Sega 32x** | Libretro PicoDrive | No-Intro |
| **Sega CD** |  | Redump |
|  | Libretro PicoDrive |  |
|  | Libretro Genesisplusgx |  |
| **SG-1000** | Libretro blueMSX | No-Intro |
| **Supergrafx** | Libretro Mednafen_supergrafx | No-Intro |

#### 4. Ports

| Systems | Core | Romset |
| :---: | :---: | :---: |
| **MR BOOM** | Libretro MrBoom | ? |

### B - Compatibles Core/Romsets Netplay Standalone

#### Consoles

| Systems | Core | Romset |
| :---: | :---: | :---: |
| **GameCube** | Dolphin-emu | Redump |
| **Wii** | Dolphin-emu | Redump |

### C - Sort and check your romsets

The different sorting preferences. What to choose ?

#### 1 - National or international ?

* **National**: If you want to play only with people from the same country as you : keep only the roms from your country **then choose the USA and Japan roms for the missing ones ** **International :** If you want to play with people from other countries : keep a rom from your country.
* **International**: If you want to play with people from other countries : keep one rom from each country.

#### 2 - Obtaining a dat file for the sorting software

Download the dat files of your consoles to sort (Link to come soon... ?).

#### 3 - The sorting software

* Clrmamepro (**Best software** but requires practice before mastering, **indispensable for the arcade**)

[https://mamedev.emulab.it/clrmamepro/](https://mamedev.emulab.it/clrmamepro/)

* Romulus (**much easier to use** but does not handle the arcade well enough)

[http://romulus.net63.net/#downloads](https://romulus.cc/#downloads)

To find out what the codes and tags in the roms correpond to, [click here](./../../../tutorials/games/generalities/tags-used-in-rom-names).

### D - Scrape your romsets

There are several ways to scrape your romsets:

#### 1. Scrape with Recalbox's built-in tool

* MENU EMULATIONSTATION (`START`)
* Scraper

#### 2. Scraper your roms with a dedicated software for a better customization (Metadata example : Hack Region translation)

* **Skraper**

[https://www.skraper.net/](https://www.skraper.net/) (Windows/Linux compatible and soon macOS).

**Video tutorial of Skraper :**

[https://www.youtube.com/watch?v=vV8DxtdOCaU&index=15&list=PL2oNQ0AT7fx2ExiSNrfHUzga5GnogI4sh](https://www.youtube.com/watch?v=vV8DxtdOCaU&index=15&list=PL2oNQ0AT7fx2ExiSNrfHUzga5GnogI4sh)

### E - Hash your roms

The **HASH** is a unique digital fingerprint for a **better recognition** when choosing a game (it is a CRC32 checksum of the rom).

**The Hash is therefore mandatory for the proper functioning of Netplay.

* EmulationStation menu
* Game options
* Netplay options
* Hash
* Launch

However, some roms will not have a Hash, this is the case of the Arcade systems:

* Fba Libretro
* Mame
* Neo-Geo

>Some programs can hash and scrape at the same time, for example Skraper!
{.is-info}

## II- Netplay system setup

### A - Activation and configuration of Netplay

* From your Recalbox :
  * EmulationStation menu (button `START`)
  * Game settings
  * Netplay settings

![](/basic-usage/features/netplay/netplaypreparation-en.png)

* NETPLAY
  * **ON/OFF**
* NICKNAME
  * Select to enter your nickname

>Be careful not to put any special character in the nickname, it will prevent the Netplay from working.
{.is-warning}

* PORT
  * 55435 (Corresponds to RetroArch server)
* NETPLAY MITM(see below for more explanation)
  * None
  * New-York
  * Madrid
* PREDEFINED PASSWORDS
  * Select to modify the predefined passwords.
* HASH ROMS
  * FILTER
    * Only missing hashes
    * All games
  * SYSTEMS
    * List of systems to hash

### B - Configure your port (UPNP/Open port/Netplay MITM)

>Some modems have UPNP which allows automatic opening/closing of ports.
{.is-info}

#### 1. Check that your port supports UPnP

We strongly recommend that you enable the UPNP option.

* Start hosting a game from your recalbox.
* On your PC, go to : [https://www.yougetsignal.com/tools/open-ports/](https://www.yougetsignal.com/tools/open-ports/)
* Enter the port 55435 and press Check.
  * **a. The answer is "positive":** You don't need to open the port, go to step B.
  * **b. The answer is "negative":** Either your box does not support UPNP or you need to enable this option. Open **a port manually** (recommended) or use **Netplay MITM**.

#### 2. Open your port (Direct connection)

It is necessary to open port 55435 for the Libretro lobby and port 2626 for Dolphin on your internet modem.

* Depending on your ISP the operation to do is different. Check your ISP's website or forums for more information.
  * The name can be whatever you want, but it helps if you name it exactly like the software whose ports you are transferring. Remember that you can't use a name that is already used by another exception rule.
  * Next, you will choose the Protocol, TCP or UDP. Some routers allow you to choose one or the other, while others give you a third option to choose both simultaneously.
  * You will need one or the other, or both. If not specified, it is best to open the ports using both protocols.
  * Then you have a range of ports. In this section we will note the port or range of ports we are going to open.
  * If you are opening only one port, note it only in the first box and leave the other blank.
  * If you are opening, for example, 100 ports between ports 1500 and 1600, you write 1500 in the first box and 1600 in the second.
  * Everything else can be safely ignored. You can add a definition or an application type if you wish, but it is not necessary.
  * If the router does not allow you to choose both UDP and TCP at the same time, you will have to make 2 separate entries for the same port.
  * Once you have finished adding your port, click on Add Definition or Accept or whatever allows you to save your changes.
  * Some routers allow you to create exception rules, but do not automatically assign them to your computer/device. There should be an option to do this.

#### 3. The Netplay MITM option

The Netplay MITM option, Relay Server, allows you to override the port opening of your modem.  
This allows both sides of the connection to be routed through an intermediary server.

However, this will have an impact on the speed at which data is synchronized and therefore on the responsiveness of the game.

* When to use it

  * Your router does not support the UPnP protocol (automatic opening and closing of ports).
  * You don't want to open your ports.
  * You have a doubt:

  Enable the Use Netplay MITM option.

>It is recommended to open your port manually or to have a box that supports UPnP, because the relay server will add extra latency.
>
>RetroArch does not check if your ports are open, nor does the Libretro lobby server.  
>So make sure you open your port correctly or enable the relay server, otherwise users will not be able to connect to your session.
{.is-warning}