---
title: Libretro Mednafen_PSX
description: 
published: true
date: 2024-07-24T17:11:28.877Z
tags: libretro, ps1, mednafen, playstation 1, psx
editor: markdown
dateCreated: 2021-05-21T08:25:30.676Z
---

[Beetle PSX](https://github.com/libretro/beetle-psx-libretro)  est un fork du module PSX de [Mednafen](https://mednafen.github.io/) pour Libretro, il fonctionne actuellement sous Linux, macOS et Windows.

## ![](/emulators/license.svg) Licence

Ce core est sous licence [**GPLv2**](https://github.com/libretro/beetle-psx-libretro/blob/master/COPYING).

## ![](/emulators/compatibility.png) Compatibilité

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ❌ | ❌ | ✅ | ✅ | ❌ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Fonctionnalités

| Fonctionnalité | Supporté |
| :---: | :---: |
| Redémarrage | ✔ |
| Capture d'écran | ✔ |
| Sauvegardes | ✔ |
| Sauvegardes instantanées | ✔ |
| Rembobinage | ✔ |
| Options du core | ✔ |
| RetroAchievements | ✔ |
| Cheats RetroArch | ✔ |
| Contrôles | ✔ |
| Remapping | ✔ |
| Multi-Mouse | ✔ |
| Vibration | ✔ |
| Contrôle de disque | ✔ |

## ![](/emulators/bios.svg) BIOS

### Liste des bios obligatoires

| Nom de fichier | Description | MD5 | Fourni |
| :---: | :---: | :---: | :---: |
| scph5500.bin | BIOS PS1 japonais - requis pour les jeux japonais | 8dd7d5296a650fac7319bce665a6a53c | ❌ |
| scph5501.bin | BIOS PS1 américain - requis pour les jeux américains | 490f666e1afb15b7362b406ed1cea246 | ❌ |
| scph5502.bin | BIOS PS1 européen - requis pour les jeux européens | 32736f17079d0b2b7024407c39bd3050 | ❌ |

### Emplacement

Placez les BIOS comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 🗒 **scph5500.bin**
┃ ┃ ┃ ┣ 🗒 **scph5501.bin**
┃ ┃ ┃ ┣ 🗒 **scph5502.bin**

## ![](/emulators/isos.svg) Isos

### Extensions supportées

Les isos doivent avoir les extensions suivantes :

* .bin/.cue
* .bin/.toc
* .chd
* .exe
* .img/.ccd/.sub
* .m3u
* .pbp

### Emplacement

Placez les isos comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 psx
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.bin**
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.cue**

>Les isos au format **Redump** sont vivement conseillés.
{.is-success}

>Pour plus d'information sur les isos, rendez-vous sur [ce tutoriel](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuration avancée de l'émulateur

>Pour pouvoir conserver vos configurations personnalisées lors d'une mise à jour, nous vous conseillons d'utiliser notre fonctionnalité [Surcharges de configuration](./../../../advanced-usage/configuration-override).
{.is-info}

### Accéder aux options

Vous pouvez configurer diverses options de deux façons différentes.

* Via le menu RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via le fichier `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Options du core

| Options | Valeurs des options | Variable (pour surcharge) | Valeurs des variables |
| --- | --- | --- | --- |
| Résolution du processeur graphique interne | `1x (native)` ✅ / `2x` / `4x` / `8x` / `16x` | `beetle_psx_internal_resolution` | `1x(native)` / `2x` / `4x` / `8x` / `16x` |
| Profondeur de couleurs interne | `16 bpp (native)` ✅ / `32 bpp` | `beetle_psx_depth` | `16bpp(native)` / `32 bpp` |
| Motif de tramage (dithering) | `1x (native)` ✅ / `Résolution interne` / `Désactivé` | `beetle_psx_dither_mode` | `1x(native)` / `internal resolution` / `disabled` |
| Décalage UV de la texture | `Activé` ✅ / `Désactivé` | `beetle_psx_scaled_uv_offset` | `enabled` / `disabled` |
| Filtrage des textures | `Au plus proche` ✅ / `SABR` / `xBR` / `Bilinéaire` / `À 3 points` / `JINC2` | `beetle_psx_filter` | `nearest` / `SABR` / `xBR` / `bilinear` / `3-point` / `JINC2` |
| Exclure les sprites du filtrage | `Désactivé` ✅ / `Opaque uniquement` / `Opaque et semi-transparent` | `beetle_psx_filter_exclude_sprite` | `disable` / `opaque` / `all` |
| Exclure les polygones 2D du filtrage | `Désactivé` ✅ / `Opaque uniquement` | `beetle_psx_filter_exclude_2d_polygon` | `disable` / `opaque` / `all` |
| Lissage adaptatif | `Désactivé` ✅ / `Activé` | `psx_adaptive_smoothing` | `disabled` / `enabled` |
| Super échantillonage (sous-échantillonage à la résolution native) | `Désactivé` ✅ / `Activé` | `psx_super_sampling` | `disabled` / `enabled` |
| Anti-crénelage multi-échantillonné | `1x (Default)` ✅ / `2x` / `4x` / `8x` / `16x` | `psx_msaa` | `1x` / `2x` / `4x` / `8x` / `16x` |
| Filtre MDEC YUV chroma | `Désactivé` ✅ / `Activé` | `psx_mdec_yuv` | `disabled` / `enabled` |
| Suivre les textures | `Désactivé` ✅ / `Activé` | `beetle_psx_track_textures` | `disabled` / `enabled` |
| Importer les textures | `Désactivé` ✅ / `Activé` | `beetle_psx_dump_textures` | `disabled` / `enabled` |
| Remplacer les textures | `Désactivé` ✅ / `Activé` | `beetle_psx_replace_textures` | `disabled` / `enabled` |
| Mode fil de fer (débuggage) | `Désactivé` ✅ / `Activé` | `beetle_psx_wireframe` | `disabled` / `enabled` |
| Duplication d'images (accélération) | `Désactivé` ✅ / `Activé` | `psx_frame_duping` | `disabled` / `enabled` |
| Afficher les i/s internes | `Désactivé` ✅ / `Activé` | `beetle_psx_display_internal_fps` | `disabled` / `enabled` |
| Afficher la VRAM complète (débuggage) | `Désactivé` ✅ / `Activé` | `beetle_psx_dispaly_vram` | `disabled` / `enabled` |
| Auto-étalonnage analogique | `Désactivé` ✅ / `Activé` | `beetle_psx_analog_calibration` | `disabled` / `enabled` |
| Activer le mode analogique DualShock | `Désactivé` ✅ / `Activé` | `beetle_psx_analog_toggle` | `disabled` / `enabled` |
| Combinaison de touches du mode analogique DualShock | `L1 + L2 + R1 + R2 + Start + Select` / `L1 + R1 + Select` ✅ / `L1 + R1 + Start` / `L1 + R1 + L3` / `L1 + R1 + R3` / `L2 + R2 + Select` / `L2 + R2 + Start` / `L2 + R2 + L3` / `L2 + R2 + R3` / `L3 + R3` | `beetle_psx_analog_toggle_combo` | `l1+l2+r1+r2+start+select` / `l1+r1+select` / `l1+r1+start` / `l1+r1+l3` / `l1+r1+r3` / `l2+r2+select` / `l2+r2+start` / `l2+r2+l3` / `l2+r2+r3` / `l3+r3` |
| Délai de maintien de la combinaison de touches du mode analogique DualShock | `0 seconde` / `1 seconde` ✅ / `2 secondes` / `3 secondes` / `4 secondes` / `5 secondes` | `beetle_psx_analog_toggle_hold` | `0` / `1` / `2` / `3` / `4` / `5` |
| Port 1 : Activer le multitap | `Désactivé` ✅ / `Activé` | `beetle_psx_enable_multitap_port1` | `disabled` / `enabled` |
| Port 2 : Activer le multitap | `Désactivé` ✅ / `Activé` | `beetle_psx_enable_multitap_port2` | `disabled` / `enabled` |
| Mode d'entrée du pistolet | `Pistolet` ✅ / `Écran tactile` | `beetle_psx_gun_input_mode` | `lightgun` / `touchscreen` |
| Viseur du pistolet | `Croix` ✅ / `Point` / `Désactivé` | `beetle_psx_gun_cursor` | `cross` / `dot` / `off` |
| Port 1 : Couleur de viseur du pistolet | `Rouge` ✅ / `Bleue` / `Verte` / `Orange` / `Jaune` / `Cyan` / `Rose` / `Violet` / `Noire` / `Blanche` | `beetle_psx_crosshair_color_p1` | `red` / `blue` / `green` / `orange` / `yellow` / `cyan` / `pink` / `purple` / `black` / `white` |
| Port 2 : Couleur de viseur du pistolet | `Bleue` ✅ / `Rouge` / `Verte` / `Orange` / `Jaune` / `Cyan` / `Rose` / `Violet` / `Noire` / `Blanche` | `beetle_psx_crosshair_color_p2` | `blue` / `red` / `green` / `orange` / `yellow` / `cyan` / `pink` / `purple` / `black` / `white` |
| Sensibilité de la souris | `5%` / `10%` / `15%` / `20%` / `25%` / `30%` / `35%` / `40%` / `45%` / `50%` / `55%` / `60%` / `65%` / `70%` / `75%` / `80%` / `85%` / `90%` / `95%` / `100%` ✅ / `105%` / `110%` / `115%` / `120%` / `125%` / `130%` / `135%` / `140%` / `145%` / `150%` / `155%` / `160%` / `165%` / `170%` / `175%` / `180%` / `185%` / `190%` / `195%` / `200%` | `beetle_psx_mouse_sensitivity` | `5%` / `10%` / `15%` / `20%` / `25%` / `30%` / `35%` / `40%` / `45%` / `50%` / `55%` / `60%` / `65%` / `70%` / `75%` / `80%` / `85%` / `90%` / `95%` / `100%` / `105%` / `110%` / `115%` / `120%` / `125%` / `130%` / `135%` / `140%` / `145%` / `150%` / `155%` / `160%` / `165%` / `170%` / `175%` / `180%` / `185%` / `190%` / `195%` / `200%` |
| Réponse de torsion du neGcon | `Linéaire` ✅ / `Quadratique` / `Cubique` | `beetle_psx_negcon_response` | `linear` / `quadratic` / `cubic` |
| Deadzone de torsion du neGcon | `0%` ✅ / `5%` / `10%` / `15%` / `20%` / `25%` / `30%` | `beetle_psx_negcon_deadzone` | `5%` / `10%` / `15%` / `20%` / `25%` / `30%` |
| Méthode de la Memory Card 0 (Redémarrage requis) | `Libretro` ✅ / `Mednafen` | `beetle_psx_use_mednafen_memcard0_method` | `libretro` / `mednafen` |
| Activer la Memory Card 1 (Redémarrage requis) | `Activé` ✅ / `Désactivé` | `beetle_psx_enable_memcard1` | `enabled` / `disabled` |
| Memory Cards partagées (Redémarrage requis) | `Activé` ✅ / `Désactivé` | `beetle_psx_shared_memory_cards` | `enabled` / `disabled` |
| Index de la Memory Card de gauche | `0` ✅ / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `21` / `22` / `23` / `24` / `25` / `26` / `27` / `28` / `29` / `30` / `31` / `32` / `33` / `34` / `35` / `36` / `37` / `38` / `39` / `40` / `41` / `42` / `43` / `44` / `45` / `46` / `47` / `48` / `49` / `50` / `51` / `52` / `53` / `54` / `55` / `56` / `57` / `58` / `59` / `60` / `61` / `62` / `63` | `beetle_psx_memcard_left_index` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `21` / `22` / `23` / `24` / `25` / `26` / `27` / `28` / `29` / `30` / `31` / `32` / `33` / `34` / `35` / `36` / `37` / `38` / `39` / `40` / `41` / `42` / `43` / `44` / `45` / `46` / `47` / `48` / `49` / `50` / `51` / `52` / `53` / `54` / `55` / `56` / `57` / `58` / `59` / `60` / `61` / `62` / `63` |
| Index de la Memory Card de droite | `0` / `1 (par défaut)` ✅ / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `21` / `22` / `23` / `24` / `25` / `26` / `27` / `28` / `29` / `30` / `31` / `32` / `33` / `34` / `35` / `36` / `37` / `38` / `39` / `40` / `41` / `42` / `43` / `44` / `45` / `46` / `47` / `48` / `49` / `50` / `51` / `52` / `53` / `54` / `55` / `56` / `57` / `58` / `59` / `60` / `61` / `62` / `63` | `beetle_psx_memcard_right_index` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `21` / `22` / `23` / `24` / `25` / `26` / `27` / `28` / `29` / `30` / `31` / `32` / `33` / `34` / `35` / `36` / `37` / `38` / `39` / `40` / `41` / `42` / `43` / `44` / `45` / `46` / `47` / `48` / `49` / `50` / `51` / `52` / `53` / `54` / `55` / `56` / `57` / `58` / `59` / `60` / `61` / `62` / `63` |
| Mode d'opération de PGXP | `Désactivé` ✅ / `Mémoire uniquement` / `Memoire + processeur (buggé)` |`beetle_psx_pgxp_mode` | `disabled` / `memory only` / `memory + CPU` |
| Tolérance à la géométrie 2D de PGXP | `Désactivé` ✅ / `1px` / `2px` / `3px` / `4px` / `5px` / `6px` / `7px` / `8px` | `beetle_psx_pgxp_2d_tol` | `disabled` / `1px` / `2px` / `3px` / `4px` / `5px` / `6px` / `7px` / `8px` |
| Élimination primitive des faces cachées PGXP | `Désactivé` ✅ / `Activé` | `beetle_psx_pgxp_nclip` | `disabled` / `enabled` |
| Cache des vertex de PGXP | `Désactivé` ✅ / `Activé` | `beetle_psx_pgxp_vertex` | `disabled` / `enabled` |
| Textures correctes en perspective de PGXP | `Désactivé` ✅ / `Activé` | `beetle_psx_pgxp_texture` | `disabled` / `enabled` |
| Hack des lignes en quadrilatères | `Par défaut` ✅ / `Agressif` / `Désactivé` | `beetle_psx_line_render` | `default` / `aggressive` / `disabled` |
| Hack de mode écran large | `Désactivé` ✅ / `Activé` | `beetle_psx_widescreen_hack` | `disabled` / `enabled` |
| Rapport d'aspect du hack de mode écran large | `16:10` / `16:9` ✅ / `18:9` / `19:9` / `20:9` / `21:9` / `32:9` | `beetle_psx_widescreen_hack_aspect_ratio` | `16:10` / `16:9` / `18:9` / `19:9` / `20:9` / `21:9` / `32:9` |
| Échelle de la fréquence du processeur (overclock) | `50%` / `60%` / `70%` / `80%` / `90%` / `100% (Native) ✅` / `110%` / `120%` / `130%` / `140%` / `150%` / `160%` / `170%` / `180%` / `190%` / `200%` / `210%` / `220%` / `230%` / `240%` / `250%` / `260%` / `270%` / `280%` / `290%` / `300%` / `310%` / `320%` / `330%` / `340%` / `350%` / `360%` / `370%` / `380%` / `390%` / `400%` / `410%` / `420%` / `430%` / `440%` / `450%` / `460%` / `470%` / `480%` / `490%` / `500%` / `510%` / `520%` / `530%` / `540%` / `550%` / `560%` / `570%` / `580%` / `590%` / `600%` / `610%` / `620%` / `630%` / `640%` / `650%` / `660%` / `670%` / `680%` / `690%` / `700%` / `710%` / `720%` / `730%` / `740%` / `750%` | `beetle_psx_cpu_freq_scale` | `50%` / `60%` / `70%` / `80%` / `90%` / `100%(native)` / `110%` / `120%` / `130%` / `140%` / `150%` / `160%` / `170%` / `180%` / `190%` / `200%` / `210%` / `220%` / `230%` / `240%` / `250%` / `260%` / `270%` / `280%` / `290%` / `300%` / `310%` / `320%` / `330%` / `340%` / `350%` / `360%` / `370%` / `380%` / `390%` / `400%` / `410%` / `420%` / `430%` / `440%` / `450%` / `460%` / `470%` / `480%` / `490%` / `500%` / `510%` / `520%` / `530%` / `540%` / `550%` / `560%` / `570%` / `580%` / `590%` / `600%` / `610%` / `620%` / `630%` / `640%` / `650%` / `660%` / `670%` / `680%` / `690%` / `700%` / `710%` / `720%` / `730%` / `740%` / `750%` |
| Overclocker le GTE | `Désactivé` ✅ / `Activé` | `beetle_psx_gte_overclock` | `disabled` / `enabled` |
| Ignorer le BIOS | `Désactivé` ✅ / `Activé` | `beetle_psx_skip_bios` | `disabled` / `enabled` |
| Remplacer le BIOS (Redémarrage requis) | `Désactivé` ✅ / `BIOS PS1 de la PSP` / `Bios PS1 de la PS3` | `beetle_psx_override_bios` | `disabled` / `psxonpsp` / `ps1_rom` |
| Moteur de rendu (Redémarrage requis) | `Matériel (auto)` ✅ / `Matériel (OpenGL)` / `Matériel (Vulkan)` / `Logiciel` | `beetle_psx_renderer` | `hardware` / `hardware_gl` / `hardware_vk` / `software` |
| Tampon d'images logiciel | `Activé` ✅ / `Désactivé` | `beetle_psx_renderer` | `enabled` / `disabled` |
| Dynarec du processeur | `Désactivé (interpréteur Beetle)` ✅ / `Performances maximales` / `Contrôle du temps de cycle` / `Interpréteur Lightrec` | `beetle_psx_cpu_dynarec` | `disabled` / `execute` / `execute_once` / `run_interpreter` |
| Invalidation du code Dynarec | `Complète` ✅ / `DMA uniquement (légèrement plus rapide)` | `beetle_psx_dynarec_invalidate` | `full` / `dma` |
| Cycles d'événements Dynarec DMA/processeur graphique/MDEC/Timer | `128 (par défaut)` ✅ / `256` / `384` / `512` / `640` / `768` / `896` / `1024` / `1152` / `1280` / `1408` / `1536` / `1664` / `1792` / `1920` / `2048` | `beetle_psx_dynarec_eventcycles` | `128` / `256` / `384` / `512` / `640` / `768` / `896` / `1024` / `1152` / `1280` / `1408` / `1536` / `1664` / `1792` / `1920` / `2048` |
| Échantillons SPU Dynarec | `1 (par défaut)` ✅ / `4` / `16` | `beetle_psx_dynarec_spu_samples` | `1` / `4` / `16` |
| Cadence des i/s communiquée par le cœur | `Débit progressif` ✅ / `Forcer le débit entrelacé` / `Autoriser le basculement automatique` | `beetle_psx_core_timing_fps` | `force_progressive` / `force_interlaced` / `auto_toggle` |
| Substitution de la cadence vidéo PAL (européenne) | `Désactivé` ✅ / `Activé` | `beetle_psx_pal_video_timing_override` | `disabled` / `enabled` |
| Recadrage du surbalayage | `Activé` ✅ / `Désactivé` | `beetle_psx_crop_overscan` | `enabled` / `disabled` |
| Recadrage supplémentaire | `Désactivé` ✅ / `1px` / `2px` / `3px` / `4px` / `5px` / `6px` / `7px` / `8px` / `9px` / `10px` / `11px` / `12px` / `13px` / `14px` / `15px` / `16px` / `17px` / `18px` / `19px` / `20px` | `psx_image_crop` | `disabled` / `1px` / `2px` / `3px` / `4px` / `5px` / `6px` / `7px` / `8px` / `9px` / `10px` / `11px` / `12px` / `13px` / `14px` / `15px` / `16px` / `17px` / `18px` / `19px` / `20px` |
| Décalage de l'image recadrée | `-12px` / `-11px` / `-10px` / `-9px` / `-8px` / `-7px` / `-6px` / `-5px` / `-4px` / `-3px` / `-2px` / `-1px` / `0 (par défaut)` ✅ / `+1px` / `+2px` / `+3px` / `+4px` / `+5px` / `+6px` / `+7px` / `+8px` / `+9px` / `+10px` / `+11px` / `+12px` | `psx_image_offset` | `-12px` / `-11px` / `-10px` / `-9px` / `-8px` / `-7px` / `-6px` / `-5px` / `-4px` / `-3px` / `-2px` / `-1px` / `disabled` / `+1px` / `+2px` / `+3px` / `+4px` / `+5px` / `+6px` / `+7px` / `+8px` / `+9px` / `+10px` / `+11px` / `+12px` |
| Décalage horizontal de l'image (cycles du processeur graphique) | `-40` / `-39` / `-38` / `-37` / `-36` / `-35` / `-34` / `-33` / `-32` / `-31` / `-30` / `-29` / `-28` / `-27` / `-26` / `-25` / `-24` / `-23` / `-22` / `-21` / `-20` / `-19` / `-18` / `-17` / `-16` / `-15` / `-14` / `-13` / `-12` / `-11` / `-10` / `-9` / `-8` / `-7` / `-6` / `-5` / `-4` / `-3` / `-2` / `-1` / `0` ✅ / `+1` / `+2` / `+3` / `+4` / `+5` / `+6` / `+7` / `+8` / `+9` / `+10` / `+11` / `+12` / `+13` / `+14` / `+15` / `+16` / `+17` / `+18` / `+19` / `+20` / `+21` / `+22` / `+23` / `+24` / `+25` / `+26` / `+27` / `+28` / `+29` / `+30` / `+31` / `+32` / `+33` / `+34` / `+35` / `+36` / `+37` / `+38` / `+39` / `+40` | `beetle_psx_image_offset_cycles` | `-40` / `-39` / `-38` / `-37` / `-36` / `-35` / `-34` / `-33` / `-32` / `-31` / `-30` / `-29` / `-28` / `-27` / `-26` / `-25` / `-24` / `-23` / `-22` / `-21` / `-20` / `-19` / `-18` / `-17` / `-16` / `-15` / `-14` / `-13` / `-12` / `-11` / `-10` / `-9` / `-8` / `-7` / `-6` / `-5` / `-4` / `-3` / `-2` / `-1` / `0` / `+1` / `+2` / `+3` / `+4` / `+5` / `+6` / `+7` / `+8` / `+9` / `+10` / `+11` / `+12` / `+13` / `+14` / `+15` / `+16` / `+17` / `+18` / `+19` / `+20` / `+21` / `+22` / `+23` / `+24` / `+25` / `+26` / `+27` / `+28` / `+29` / `+30` / `+31` / `+32` / `+33` / `+34` / `+35` / `+36` / `+37` / `+38` / `+39` / `+40` |
| Overclocking du rastériseur processeur graphique | `1x (native)` ✅ / `2x` / `4x` / `8x` / `16x` / `32x` | `beetle_psx_gpu_overclock` | `1x(native)` / `2x` / `4x` / `8x` / `16x` / `32x` |
| Rapport d'aspect du cœur | `Corrigé` ✅ / `Non corrigé` / `Forcer 4:3` / `Forcer NTSC` | `beetle_psx_aspect_ratio` | `corrected` / `uncorrected` / `4:3` / `ntsc` |
| Ligne de balayage initiale - NTSC | `0` ✅ / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `21` / `22` / `23` / `24` / `25` / `26` / `27` / `28` / `29` / `30` / `31` / `32` / `33` / `34` / `35` / `36` / `37` / `38` / `39` / `40` | `psx_initial_scanlines` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `21` / `22` / `23` / `24` / `25` / `26` / `27` / `28` / `29` / `30` / `31` / `32` / `33` / `34` / `35` / `36` / `37` / `38` / `39` / `40` |
| Dernière ligne de balayage - NTSC | `210` / `211` / `212` / `213` / `214` / `215` / `216` / `217` / `218` / `219` / `220` / `221` / `222` / `223` / `224` / `225` / `226` / `227` / `228` / `229` / `230` / `231` / `232` / `233` / `234` / `235` / `236` / `237` / `238` / `239 (par défaut)` ✅ | `psx_last_scanlines` | `210` / `211` / `212` / `213` / `214` / `215` / `216` / `217` / `218` / `219` / `220` / `221` / `222` / `223` / `224` / `225` / `226` / `227` / `228` / `229` / `230` / `231` / `232` / `233` / `234` / `235` / `236` / `237` / `238` / `239` |
| Ligne de balayage initiale - PAL | `0` ✅ / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `21` / `22` / `23` / `24` / `25` / `26` / `27` / `28` / `29` / `30` / `31` / `32` / `33` / `34` / `35` / `36` / `37` / `38` / `39` / `40` | `beetle_psx_initial_scanline_pal` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `21` / `22` / `23` / `24` / `25` / `26` / `27` / `28` / `29` / `30` / `31` / `32` / `33` / `34` / `35` / `36` / `37` / `38` / `39` / `40` |
| Dernière ligne de balayage - PAL | `230` / `231` / `232` / `233` / `234` / `235` / `236` / `237` / `238` / `239` / `240` / `241` / `242` / `243` / `244` / `245` / `246` / `247` / `248` / `249` / `250` / `251` / `252` / `253` / `254` / `255` / `256` / `257` / `258` / `259` / `260` / `261` / `262` / `263` / `264` / `265` / `266` / `267` / `268` / `269` / `270` / `271` / `272` / `273` / `274` / `275` / `276` / `277` / `278` / `279` / `280` / `281` / `282` / `283` / `284` / `285` / `286` / `287 (par défaut)` ✅ | `beetle_psx_last_scanline_pal` | `230` / `231` / `232` / `233` / `234` / `235` / `236` / `237` / `238` / `239` / `240` / `241` / `242` / `243` / `244` / `245` / `246` / `247` / `248` / `249` / `250` / `251` / `252` / `253` / `254` / `255` / `256` / `257` / `258` / `259` / `260` / `261` / `262` / `263` / `264` / `265` / `266` / `267` / `268` / `269` / `270` / `271` / `272` / `273` / `274` / `275` / `276` / `277` / `278` / `279` / `280` / `281` / `282` / `283` / `284` / `285` / `286` / `287` |
| Méthode d'accès au CD (Redémarrage requis) | `Synchrone` ✅ / `Asynchrone` / `Pré-cachée` | `beetle_psx_cd_access_method` | `sync` / `async` / `precache` |
| Vitesse de chargement du CD | `2x (native)` ✅ / `4x` / `6x` / `8x` / `10x` / `12x` / `14x` | `beetle_psx_cd_fastload` | `2x(native)` / `4x` / `6x` / `8x` / `10x` / `12x` / `14x` |

## ![](/emulators/external-links.png) Liens externes

* **Code source utilisé** : [https://github.com/libretro/beetle-psx-libretro/](https://github.com/libretro/beetle-psx-libretro/)
* **Documentation Libretro** : [https://docs.libretro.com/library/beetle_psx/](https://docs.libretro.com/library/beetle_psx/)