---
title: SDLPoP
description: 
published: true
date: 2024-07-29T12:00:28.591Z
tags: 9.2+, prince of persia, sdlpop
editor: markdown
dateCreated: 2024-07-29T12:00:28.591Z
---

## ![](/emulators/license.svg) Licencia

Este núcleo está bajo la licencia [**GPL-3.0**](https://github.com/NagyD/SDLPoP/blob/master/COPYING).

## ![](/emulators/compatibility.png) Compatibilidad

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Funcionalidades

| Funcionalidad | Soportado |
| :---: | :---: |

## ![](/emulators/bios.svg) BIOS

>**No se requiere ningún bios.**
{.is-success}

## ![](/emulators/roms.png) Roms

El juego desde la versión descompilada DOS ya está proporcionado.

### Ubicación

Coloque los archivos en la siguiente carpeta:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 ports
┃ ┃ ┃ ┃ ┣ 📁 Prince of Persia
┃ ┃ ┃ ┃ ┃ ┣ 📁 data

## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

> **¡Este núcleo no tiene opciones!**
{.is-success}

## ![](/emulators/external-links.png) Enlaces externos

* **Código fuente utilizado** : [https://github.com/NagyD/SDLPoP/](https://github.com/NagyD/SDLPoP/)
