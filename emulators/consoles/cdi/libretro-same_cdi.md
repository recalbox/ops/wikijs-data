---
title: Libretro Same_cdi
description: 
published: true
date: 2024-07-24T10:03:43.447Z
tags: libretro, cdi, 9.1+, same_cdi
editor: markdown
dateCreated: 2024-07-24T10:03:43.447Z
---

**Libretro-same_cdi** is a core derivative of MAME. The same part stands for S(ingle) A(rcade) M(achine) E(mulator).

## ![](/emulators/license.svg) License

This core is under **[GPLv2](https://github.com/libretro/same_cdi/blob/master/COPYING)** license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ❌ | ❌ | ❌ | ❌ | ❌ | ❌ | ✅ | ❌ |

## ![](/emulators/features.png) Features

| Feature | Supported |
| :---: | :---: |
| Restart | ✔ |
| Screenshots | ✔ |
| Saves | ✔ |
| Core Options | ✔ |
| Controls | ✔ |
| Remapping | ✔ |

## ![](/emulators/bios.svg) BIOS

### List of mandatory bios

| Filename | Description | MD5 | Provided |
| :---: | :---: | :---: | :---: |
| cdibios.zip |  | 9fd6b277cd877d51426427a33d1d19bb | ❌ |
| cdimono1.zip | CD-i (Mono-I) (PAL) | ce590c4787460161359f2b680136cb60 bbe6636ac11d3744f0710e8e820187e7 | ❌ |
| cdimono2.zip |  | 2ac8cd5bb23287e022ca17d616949255 | ❌ |

### Location

Put your bios like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 cdi
┃ ┃ ┃ ┃ ┣ 🗒 **cdibios.zip**
┃ ┃ ┃ ┃ ┣ 🗒 **cdimono1.zip**
┃ ┃ ┃ ┃ ┣ 🗒 **cdimono2.zip**

## ![](/emulators/isos.svg) Isos

### Supported extensions

Isos must have the extensions:

* .chd
* .cue
* .iso

### Location

Put the isos like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 cdi
┃ ┃ ┃ ┃ ┣ 🗒 **game.chd**

>The isos using **Redump** format are highly recommended.
{.is-success}

>For more information about isos, go to [this tutorial](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Accessing to options

You can configure various options in two different ways.

* Via RetroArch menu:

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via the file `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core options

| Options | Options values | Variables (for configuration override) | Variables values |
| --- | --- | --- | --- |
| Read configuration | `Disabled` ✅ / `Enabled` | `same_cdi_read_config` | `disabled` / `enabled` |
| Write configuration | `Disabled` ✅ / `Enabled` | `same_cdi_write_config` | `disabled` / `enabled` |
| Save state naming | `game` ✅ / `system` | `same_cdi_saves` | `game` / `system` |
| NVRAM Saves per game | `Enabled` ✅ / `Disabled` | `same_cdi_nvram_saves` | `enabled` / `disabled` |
| Auto save/load states | `Disabled` ✅ / `Enabled` | `same_cdi_auto_save` | `disabled` / `enabled` |
| Enable in-game mouse | `Disabled` ✅ / `Enabled` | `same_cdi_mouse_enable` | `disabled` / `enabled` |
| Lightgun mode | `none` ✅ / `touchscreen` / `lightgun` | `same_cdi_lightgun_mode` | `none` / `touchscreen` / `lightgun` |
| Profile Buttons according to games (Restart) | `Enabled` ✅ / `Disabled` | `same_cdi_buttons_profiles` | `enabled` / `disabled` |
| Enable throttle | `Disabled` ✅ / `Enabled` | `same_cdi_throttle` | `disabled` / `enabled` |
| Enable cheats | `Disabled` ✅ / `Enabled` | `same_cdi_cheats_enable` | `disabled` / `enabled` |
| Main CPU Overclock | `default` ✅ / `30` / `31` / `32` / `33` / `34` / `35` / `36` / `37` / `38` / `39` / `40` / `41` / `42` / `43` / `44` / `45` / `46` / `47` / `48` / `49` / `50` / `51` / `52` / `53` / `54` / `55` / `60` / `65` / `70` / `75` / `80` / `85` / `90` / `95` / `100` / `105` / `110` / `115` / `120` / `125` / `130` / `135` / `140` / `145` / `150` | `same_cdi_cpu_overclock` | `default` / `30` / `31` / `32` / `33` / `34` / `35` / `36` / `37` / `38` / `39` / `40` / `41` / `42` / `43` / `44` / `45` / `46` / `47` / `48` / `49` / `50` / `51` / `52` / `53` / `54` / `55` / `60` / `65` / `70` / `75` / `80` / `85` / `90` / `95` / `100` / `105` / `110` / `115` / `120` / `125` / `130` / `135` / `140` / `145` / `150` |
| Alternate render method | `Disabled` ✅ / `Enabled` | `same_cdi_alternate_render` | `disabled` / `enabled` |
| Boot from CLI | `Disabled` ✅ / `Enabled` | `same_cdi_boot_from_cli` | `disabled` / `enabled` |
| Resolution | `640x480` ✅ / `640x360` / `800x600` / `800x450` / `960x720` / `960x540` / `1024x768` / `1024x576` / `1280x960` / `1280x720` / `1600x1200` / `1600x900` / `1440x1080` / `1920x1080` / `1920x1440` / `2560x1440` / `2880x2160` / `3840x2160` | `same_cdi_altres` | `640x480` / `640x360` / `800x600` / `800x450` / `960x720` / `960x540` / `1024x768` / `1024x576` / `1280x960` / `1280x720` / `1600x1200` / `1600x900` / `1440x1080` / `1920x1080` / `1920x1440` / `2560x1440` / `2880x2160` / `3840x2160` |
| MAME INI Paths | `Disabled` ✅ / `Enabled` | `same_cdi_mame_paths_enable` | `disabled` / `enabled` |
| MAME Joystick 4-way simulation | `disabled` ✅ / `4way` / `strict` / `qbert` | `same_cdi_mame_4way_enable` | `disabled` / `4way` / `strict` / `qbert` |

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/libretro/same_cdi/](https://github.com/libretro/same_cdi/)
* **Libretro documentation**: [https://docs.libretro.com/library/same_cdi/](https://docs.libretro.com/library/same_cdi/)