---
title: Folders and SSH commands
description: Some useful commands...
published: true
date: 2021-08-06T15:04:57.400Z
tags: commands, folder, ssh
editor: markdown
dateCreated: 2021-08-06T09:04:30.110Z
---

Below are some commands that allow you to perform some specific actions if you need to.

## SSH commands

Connect to your Recalbox:

```shell
ssh root@[IP address of your Recalbox]
```

Mount the system partition in read/write mode:

```shell
mount -o remount,rw / 
```

Mount the boot partition in read/write mode:

```shell
mount -o remount,rw /boot
```

Stop EmulationStation:

```shell
es stop
```

Start EmulationStation:

```shell
es start
```

Restart EmulationStation:

```shell
es restart
```

Change user password:

```shell
passwd
```

Restart your Recalbox:

```shell
reboot
```

Turn off your Recalbo:

```shell
poweroff
```

Take a screenshot:

```shell
raspi2png
```

It will take a screenshot and create a file under `/recalbox/share/screenshots/` with the name `screenshot-date-du-screenTheure-du-screen.png`.

You can also change your screenshot to a custom name with `-p filename.png`

Network information:

```shell
ifconfig
```

Check the CPU temperature:

```shell
cat /sys/class/thermal/thermal_zone0/temp  
```

Then divide the result by 1000 to get the temperature in Celsius (for example: 54034 becomes 54°C).

## Files

Raspberry Pi specific configuration, like overscan settings, overclocking, default video mode, etc... :

```shell
nano /boot/recalbox-user-config.txt  
```

Recalbox configuration file:

```shell
nano /recalbox/share/system/recalbox.conf
```