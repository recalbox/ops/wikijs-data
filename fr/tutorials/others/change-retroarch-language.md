---
title: Modifier la langue de RetroArch
description: L'anglais, c'est bien, mais pas pour tout le monde !
published: true
date: 2021-11-17T16:28:52.631Z
tags: retroarch, langue
editor: markdown
dateCreated: 2021-05-29T17:59:00.391Z
---

## Explications

Saviez-vous que vous pouviez modifier la langue affichée dans les options de RetroArch ? Vous avez le français, bien évidemment, mais aussi d'autres langues. Cela vous sera très utile, les autres pages se basant sur RetroArch en français pour vous guider.

## Mise en place

* Lancez un jeu utilisant un core Libretro.
* Une fois le jeu lancé, allez dans les options de RetroArch avec les boutons `HOTKEY` + `B` (voir [cette page](./../../basic-usage/getting-started/special-commands/in-game) pour savoir la correspondance pour votre manette).

![](/tutorials/others/change-retroarch-language/ralanguage1.png){.full-width}

* Quand vous avez ouvert les options de RetroArch, en suivant les boutons affichés en bas à droite, faites « Back » et allez une fois à gauche.

![](/tutorials/others/change-retroarch-language/ralanguage2.png){.full-width}

* Quand le curseur est sur « Main Menu », descendez sur « Settings » et validez.

![](/tutorials/others/change-retroarch-language/ralanguage3.png){.full-width}

* Dans le menu de droite, descendez sur « User » tout en bas et validez.

![](/tutorials/others/change-retroarch-language/ralanguage4.png){.full-width}

* Dans les options de l'utilisateur, sélectionnez « Language » et modifiez-le pour la langue de votre choix.

![](/tutorials/others/change-retroarch-language/ralanguage5.png){.full-width}

* RetroArch est maintenant dans la langue de votre choix.

![](/tutorials/others/change-retroarch-language/ralanguage6.png){.full-width}

* Faites une fois « Retour », remontez le menu et allez dans « Configuration ».

![](/tutorials/others/change-retroarch-language/ralanguage7.png){.full-width}

* Sur l'option « Sauvegarder la configuration en quittant », choisissez « Activé ».

![](/tutorials/others/change-retroarch-language/ralanguage8.png){.full-width}

* Quittez complètement le jeu et la langue enregistrée sera sélectionnée à chaque démarrage.