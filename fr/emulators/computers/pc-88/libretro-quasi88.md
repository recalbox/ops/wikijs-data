---
title: Libretro QUASI88
description: 
published: true
date: 2024-06-22T23:25:28.379Z
tags: libretro, pc-88, quasi88
editor: markdown
dateCreated: 2021-05-21T08:17:37.128Z
---



## ![](/emulators/license.svg) Licence

Ce core est sous licence [**BSD 3-Clause**](https://github.com/libretro/quasi88-libretro/blob/master/LICENSE).

## ![](/emulators/compatibility.png) Compatibilité

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Fonctionnalités

| Fonctionnalité | Supporté |
| :---: | :---: |
| Redémarrage  | ✔ |
| Captures d'écran | ✔ |
| Sauvegardes | ✔ |
| Sauvegardes instantanées | ✔ |
| Rembobinage | ✔ |
| Netplay | ✔ |
| Options du core | ✔ |
| RetroAchievements | ✔ |
| Cheats RetroArch | ✔ |
| Contrôles | ✔ |
| Remapping | ✔ |
| Vibration | ✔ |
| Sous-systèmes | ✔ |

## ![](/emulators/bios.svg) BIOS

### Liste des bios obligatoires

| Nom de fichier | Description | MD5 | Fourni |
| :---: | :---: | :---: | :---: |
| n88.rom | BASIC rom | 22be239bc0c4298bc0561252eed98633 f7cba6a308c2718dbe97e60e46ddd66a 4f984e04a99d56c4cfe36115415d6eb8 | ❌ |
| n88_0.rom | BASIC rom #1 | e28fe3f520bea594350ea8fb00395370 c254685f00ca9c31b97203d6ef19f5e2 d675a2ca186c6efcd6277b835de4c7e5 | ❌ |
| n88_1.rom | BASIC rom #2 | e844534dfe5744b381444dbe61ef1b66 a8e298da7ac947669bcb1ff25cee0a83 | ❌ |
| n88_2.rom | BASIC rom #3 | 6548fa45061274dee1ea8ae1e9e93910 9d03154fd9abfc28c4e6d4dc705e6e23 | ❌ |
| n88_3.rom | BASIC rom #4 | fc4b76a402ba501e6ba6de4b3e8b4273 e1791f8154f1cdf22b576a1a365b6e1f | ❌ |
| n88n.rom | Emulation de la série PC-8000 | 5d6854624dd01cd791f58727fc43a525 93cd1d78b7b9c50b80041ed330332ece 2ff07b8769367321128e03924af668a0 | ❌ |
| disk.rom | Chargement des images disque | 793f86784e5608352a5d7f03f03e0858 01b1af474fcabe93c40d779b234a3825 | ❌ |

### Emplacement

Placez les BIOS comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 quasi88
┃ ┃ ┃ ┃ ┣ 🗒 **n88.rom**
┃ ┃ ┃ ┃ ┣ 🗒 **n88_0.rom**
┃ ┃ ┃ ┃ ┣ 🗒 **n88_1.rom**
┃ ┃ ┃ ┃ ┣ 🗒 **n88_2.rom**
┃ ┃ ┃ ┃ ┣ 🗒 **n88_3.rom**
┃ ┃ ┃ ┃ ┣ 🗒 **n88n.rom**
┃ ┃ ┃ ┃ ┣ 🗒 **disk.rom**

### Liste des bios optionnels

| Nom de fichier | Description | MD5 | Fourni |
| :---: | :---: | :---: | :---: |
| n88knj1.rom | Affichage des kanjis. | d81c6d5d7ad1a4bbbd6ae22a01257603 | ❌ |

### Emplacement

Placez les bios comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 quasi88
┃ ┃ ┃ ┃ ┣ 🗒 **n88knj1.rom**

## ![](/emulators/roms.png) Roms

### Extensions supportées

Les roms doivent avoir les extensions suivantes :

* .d88
* .t88
* .cmt
* .m3u
* .zip
* .7z

Ce système supporte les roms compressées au format .zip/.7z. Attention toutefois, il ne s'agit que d'une archive.

Les fichiers contenus dans les .zip/.7z doivent correspondre aux extensions citées précédemment.
De plus, chaque fichier .zip/.7z ne doit contenir qu'une seule rom compressée.

### Emplacement

Placez les roms comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 pc88
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.zip**

>Les roms au format **TOSEC** sont vivement conseillées.
{.is-success}

>Pour plus d'information sur les roms, rendez-vous sur [ce tutoriel](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuration avancée de l'émulateur

>Pour pouvoir conserver vos configurations personnalisées lors d'une mise à jour, nous vous conseillons d'utiliser notre fonctionnalité [Surcharges de configuration](./../../../advanced-usage/configuration-override).
{.is-info}

### Accéder aux options

Vous pouvez configurer diverses options de deux façons différentes.

* Via le menu RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via le fichier `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Options du core

| Options | Valeur des options | Variable (pour surcharge) | Valeurs des variables |
| --- | --- | --- | --- |
| Mode BASIC | `N88-BASIC V2` ✅ / `N88-BASIC V1H` / `N88-BASIC V1S` / `N-BASIC` | `q88_basic_mode` | `N88 V2` / `N88 V1H` / `N88 V1S` / `N` |
| Mode du sous-processeur | `Faire tourner le sous-processeur uniquement lors de l'accès au disque` ✅ / `Faire tourner les deux processeurs lors de l'accès au disque` / `Toujours utiliser les deux processeurs` | `q88_sub_cpu_mode` | `0` / `1` / `2` |
| Horloge processeur | `1 MHz (underclock)` / `2 MHz (underclock)` / `4 MHz (NEC µPD780)` ✅ / `8 MHz (NEC µPD70008)` / `16 MHz (overclock)` / `32 MHz (overclock)` / `64 MHz (overclock)` | `q88_cpu_clock` | `1` / `2` / `4` / `8` / `16` / `32` / `64` |
| Carte son | `OPN (Yamaha YM2203)` / `OPNA (Yamaha YM2608)` ✅ | `q88_sound_board` | `OPN` / `OPNA` |
| Attente du FDC | `Activé` ✅ / `Désactivé` | `q88_use_fdc_wait` | `enabled` / `disabled` |
| Utiliser PCG-8100 | `Désactivé` ✅ / `Activé` | `q88_use_pcg-8100` | `disabled` / `enabled` |
| Sauvegarder vers une image disque | `Désactivé` ✅ / `Activé` | `q88_save_to_disk_image` | `disabled` / `enabled` |
| Vibration lors de l'accès au disque | `Activé` ✅ / `Désactivé` | `q88_rumble` | `enabled` / `disabled` |
| Taille de l'écran | `Pleine résolution (640x400)` ✅ / `Demi résolution (320x200)` | `q88_screen_size` | `full` / `half` |

## ![](/emulators/external-links.png) Liens externes

* **Code source utilisé** : [https://github.com/libretro/quasi88-libretro/](https://github.com/libretro/quasi88-libretro/)
* **Documentation Libretro** : [https://docs.libretro.com/library/quasi88/](https://docs.libretro.com/library/quasi88/)