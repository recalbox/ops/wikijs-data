---
title: Libretro FCEUmm
description: 
published: true
date: 2021-10-08T06:50:57.233Z
tags: libretro, famicom, family computer disk system, fceumm
editor: markdown
dateCreated: 2021-07-30T18:28:14.206Z
---



## ![](/emulators/license.svg) License

This core is under [**GPLv2**](https://github.com/libretro/libretro-fceumm/blob/master/Copying) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Features

| Feature | Supported |
| :---: | :---: |
| Restart | ✔ |
| Screenshots | ✔ |
| Saves | ✔ |
| States | ✔ |
| Rewind | ✔ |
| Netplay | ✔ |
| Core Options | ✔ |
| RetroAchievements | ✔ |
| Cheats RetroArch | ✔ |
| Cheats natifs | ✔ |
| Controls | ✔ |
| Remapping | ✔ |
| Softpatching | ✔ |

## ![](/emulators/bios.svg) BIOS

### List of mandatory bios

| Filename | Description | MD5 | Provided |
| :---: | :---: | :---: | :---: |
| disksys.rom | Family Computer Disk System BIOS | ca30b50f880eb660a320674ed365ef7a | ❌  |

### Location

Put the bios like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 🗒 **disksys.rom**

## ![](/emulators/roms.png) Roms

### Supported extensions

Roms must have the extension:

* .fds
* .zip
* .7z

This system supports compressed roms in .zip/.7z format. However, be careful, it is only an archive.

The files contained in the .zip/.7z must correspond to the extensions mentioned above.
Moreover, each .zip/.7z file must contain only one compressed rom.

### Location

Put the roms like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 fds
┃ ┃ ┃ ┃ ┣ 🗒 **game.zip**

>The roms using **No-Intro** format are highly recommended.
{.is-success}

>For more information about roms, go to [this tutorial](./../../../tutorials/games/generalities/isos-and-roms)!
{.is-info}

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Accessing to options

You can configure various options in two different ways.

* Via RetroArch menu:

┣ 📁 RetroArch Menu
┃ ┣ 📁 Core options
┃ ┃ ┣ 🧩 Name_option

* Via the file `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core options

| Options | Options values | Variables (for configuration override) | Variables values |
| --- | --- | --- | --- |
| Machine Type (Restart) | `Auto` ✅ / `NTSC` / `PAL` / `Dendy` | `fceumm_region` | `Auto` / `NTSC` / `PAL` / `Dendy` |
| Overclock | `Disabled` ✅ / `2x-Postrender` / `2x-VBlank` | `fceumm_overclocking` | `disabled` / `2x-Postrender` / `2x-VBlank` |
| RAM Power-On Fill (Needs Restart) | `fill $ff` ✅ / `fill $00` | `fceumm_ramstate` | `fill $ff` / `fill $00` |
| No Sprite Limit | `Disabled` ✅ / `Enabled` | `fceumm_nospritelimit` | `disabled` / `enabled` |
| NTSC Filter | `Disabled` ✅ / `composite` / `svideo` / `rgb` / `monochrome` | `fceumm_ntsc_filter` | `disabled` / `composite` / `svideo` / `rgb` / `monochrome` |
| Color Palette | `default` ✅ / `asqrealc` / `nintendo-vc` / `rgb` / `yuv-v3` / `unsaturated-final` / `sony-cxa2025as-us` / `pal` / `bmf-final2` / `bmf-final3` / `smooth-fbx` / `composite-direct-fbx` / `pvm-style-d93-fbx` / `ntsc-hardware-fbx` / `nes-classic-fbx-fs` / `nescap` / `wavebeam` / `raw` / `custom` | `fceumm_palette` | `default` / `asqrealc` / `nintendo-vc` / `rgb` / `yuv-v3` / `unsaturated-final` / `sony-cxa2025as-us` / `pal` / `bmf-final2` / `bmf-final3` / `smooth-fbx` / `composite-direct-fbx` / `pvm-style-d93-fbx` / `ntsc-hardware-fbx` / `nes-classic-fbx-fs` / `nescap` / `wavebeam` / `raw` / `custom` |
| Aspect Ratio | `8:7 PAR` ✅ / `4:3` | `fceumm_aspect` | `8:7 PAR` / `4:3` |
| Crop Horizontal Overscan | `Disabled` ✅ / `Enabled` | `fceumm_overscan_h` | `disabled` / `enabled` |
| Crop Vertical Overscan | `Disabled` ✅ / `Enabled` | `fceumm_overscan_v` | `disabled` / `enabled` |
| Allow Opposing Directions | `Disabled` ✅ / `Enabled` | `fceumm_up_down_allowed` | `disabled` / `enabled` |
| Turbo Enable | `None` ✅ / `Player 1` / `Player 2` / `Both` | `fceumm_turbo_enable` | `None` / `Player 1` / `Player 2` / `Both` |
| Turbo Delay (in frames) | `1` / `2` / `3` ✅ / `5` / `10` / `15` / `30` / `60` | `fceumm_turbo_delay` | `1` / `2` / `3` / `5` / `10` / `15` / `30` / `60` |
| Zapper Mode | `lightgun` ✅ / `touchscreen` / `mouse` | `fceumm_zapper_mode` | `lightgun` / `touchscreen` / `mouse` |
| Show Crosshair | `Enabled` ✅ / `Disabled` | `fceumm_show_crosshair` | `enabled` / `disabled` |
| Zapper Tolerance | `0` / `1` / `2` / `3` / `4` / `5` / `6` ✅ / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` | `fceumm_zapper_tolerance` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` |
| Sound Quality | `Low` ✅ / `High` / `Very High` | `fceumm_sndquality` | `Low` / `High` / `Very High` |
| Swap Duty Cycles | `Disabled` ✅ / `Enabled` | `fceumm_swapduty` | `disabled` / `enabled` |
| Master Volume | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` ✅ / `8` / `9` / `10` | `fceumm_sndvolume` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` |
| Channel 1 (Square 1) | `Enabled` ✅ / `Disabled` | `fceumm_apu_1` | `enabled` / `disabled` |
| Channel 2 (Square 2) | `Enabled` ✅ / `Disabled` | `fceumm_apu_2` | `enabled` / `disabled` |
| Channel 3 (Triangle) | `Enabled` ✅ / `Disabled` | `fceumm_apu_3` | `enabled` / `disabled` |
| Channel 4 (Noise) | `Enabled` ✅ / `Disabled` | `fceumm_apu_4` | `enabled` / `disabled` |
| Channel 5 (PCM) | `Enabled` ✅ / `Disabled` | `fceumm_apu_5` | `enabled` / `disabled` |
| Show Advanced System Options | `Disabled` ✅ / `Enabled` | `fceumm_show_adv_system_options` | `disabled` / `enabled` |
| Show Advanced Sound Options | `Disabled` ✅ / `Enabled` | `fceumm_show_adv_sound_options` | `disabled` / `enabled` |

## ![](/emulators/external-links.png) External links

* **Code source utilisé** : [https://github.com/libretro/libretro-fceumm/](https://github.com/libretro/libretro-fceumm/)
* **Documentation Libretro** : [https://docs.libretro.com/library/fceumm/](https://docs.libretro.com/library/fceumm/)