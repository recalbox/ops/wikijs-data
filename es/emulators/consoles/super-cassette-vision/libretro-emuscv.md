---
title: Libretro EmuSCV
description: 
published: true
date: 2024-07-29T18:10:41.776Z
tags: libretro, 7.2+, super cassette vision, emuscv
editor: markdown
dateCreated: 2023-09-07T20:57:26.958Z
---

## ![](/emulators/license.svg) Licencia

Este núcleo está bajo la licencia [**GPLv3**](https://gitlab.com/MaaaX-EmuSCV/libretro-emuscv/-/blob/master/licence.txt).

## ![](/emulators/compatibility.png) Compatibilidad

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Características

| Característica | Soportado |
| :---: | :---: |
| Reinicio | ✔ |
| Capturas de pantalla | ✔ |
| Guardado | - |
| Guardado instantáneo | - |
| Rebobinado | - |
| Controles | ✔ |

## ![](/emulators/bios.svg) BIOS

### Lista de BIOS obligatorios

| Nombre de archivo | Descripción | MD5 | Proporcionado |
| :---: | :---: | :---: | :---: |
| upd7801g.s01 | BIOS - Requerido | 635a978fd40db9a18ee44eff449fc126 | ❌ |

### Ubicación

Coloca los BIOS de la siguiente manera:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 scv
┃ ┃ ┃ ┃ ┣ 🗒 **upd7801g.s01**

## ![](/emulators/roms.png) Roms

### Extensiones soportadas

Las roms deben tener las siguientes extensiones:

* .0
* .bin
* .cart
* .rom
* .zip

Este sistema soporta roms comprimidas en formato .zip. Sin embargo, se trata solo de un archivo.

Los archivos contenidos en los .zip deben corresponder a las extensiones mencionadas anteriormente.
Además, cada archivo .zip debe contener solo una rom comprimida.

### Ubicación

Coloca las roms de la siguiente manera:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 scv
┃ ┃ ┃ ┃ ┣ 🗒 **juego.zip**

La particularidad es que si se carga una ROM en `.bin`, `.rom` o `.0` (`.1`, `.2`, etc.) conocida, se crea un archivo ROM en formato `.CART` que indica al emulador el mapeo correcto de los datos (la cartucho puede contener varias ROMs, eventualmente RAM, o incluso SRAM guardada por una pila). Para los juegos donde se podía guardar, también se crea un archivo `.SAVE` que contiene los datos de la SRAM (guardado al salir).

El `.CART` se crea en el directorio `roms`.

Si no se puede crear el `.CART`, no se carga el juego, se inicia en la pantalla de prueba de la consola como cuando la consola no puede leer un cartucho.
Voy a modificar el código para que se cargue de todos modos y ver cómo puedo ponerlo en el directorio de guardado.

>Se recomiendan encarecidamente las roms en formato **No-Intro**.
{.is-success}

>Para más información sobre las roms, visita [este tutorial](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

>Para poder conservar tus configuraciones personalizadas durante una actualización, te recomendamos utilizar nuestra funcionalidad [Sobrecargas de configuración](./../../../advanced-usage/configuration-override).
{.is-info}

### Acceder a las opciones

Puedes configurar diversas opciones de dos maneras diferentes.

* A través del menú RetroArch:

┣ 📁 Menu RetroArch
┃ ┣ 📁 Opciones del núcleo
┃ ┃ ┣ 🧩 Name_option

* A través del archivo `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Opciones del núcleo

| Opciones | Valores de las opciones | Variable (para sobrecarga) | Valores de las variables |
| --- | --- | --- | --- |
| CONSOLE | `AUTO` ✅ / `EPOCH` / `YENO` / `EPOCHLADY` | `emuscv_console` | `AUTO` / `EPOCH` / `YENO` / `EPOCHLADY` |
| DISPLAY | `AUTO` ✅ / `EMUSCV` / `EPOCH` / `YENO` | `emuscv_display` | `AUTO` / `EMUSCV` / `EPOCH` / `YENO` |
| PIXELASPECT | `AUTO` ✅ / `RECTANGULAR` / `SQUARE` | `emuscv_pixelaspect` | `AUTO` / `RECTANGULAR` / `SQUARE` |
| RESOLUTION | `AUTO` ✅ / `LOW` / `MEDIUM` / `HIGH` | `emuscv_resolution` | `AUTO` / `LOW` / `MEDIUM` / `HIGH` |
| PALETTE | `AUTO` ✅ / `STANDARD` / `OLDNTSC` | `emuscv_palette` | `AUTO` / `STANDARD` / `OLDNTSC` |
| FPS | `AUTO` ✅ / `EPOCH60` / `YENO50` | `emuscv_fps` | `AUTO` / `EPOCH60` / `YENO50` |
| DISPLAYFULLMEMORY | `AUTO` ✅ / `YES` / `NO` | `emuscv_displayfullmemory` | `AUTO` / `YES` / `NO` |
| DISPLAYINPUTS | `AUTO` ✅ / `YES` / `NO` | `emuscv_displayinputs` | `AUTO` / `YES` / `NO` |
| LANGAGE | `AUTO` ✅ / `JP` / `FR` / `EN` | `emuscv_langage` | `AUTO` / `JP` / `FR` / `EN` |
| BIOS | `AUTO` ✅ / `YES` / `NO` | `emuscv_checkbios` | `AUTO` / `YES` / `NO` |

## ![](/emulators/external-links.png) Enlaces externos

* **Código fuente utilizado** : [https://gitlab.com/MaaaX-EmuSCV/libretro-emuscv/](https://gitlab.com/MaaaX-EmuSCV/libretro-emuscv/)
* **Documentación Libretro** : [https://docs.libretro.com/library/emuscv/](https://docs.libretro.com/library/emuscv/)
