---
title: Supermodel
description: 
published: true
date: 2021-11-03T12:14:10.754Z
tags: model-3, supermodel
editor: markdown
dateCreated: 2021-07-05T22:53:35.480Z
---

Supermodel emulates the Sega Model 3 arcade platform.  
It uses **OpenGL 2.1** and **SDL**, and can run on Windows, Linux and macOS.

This is an early public release of Supermodel. This is a very preliminary document, the alpha version of the software.
Development began in January 2011 and focused on reverse engineering aspects of the still unknown Model 3.
Therefore, many important features, such as a proper user interface, are not yet implemented and game compatibility is still low.

## ![](/emulators/license.svg) License

This core is under [**GPLV3**](https://www.supermodel3.com/About.html) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ❌ | ❌ | ❌ | ❌ | ❌ | ❌ | ✅ | ✅ |

## ![](/emulators/features.png) Features

| Feature | Supported |
| :---: | :---: |
| Pause | ✔︎ |
| States | ✔︎ |
| Screenshots | ✔︎ |

## ![](/emulators/bios.svg) BIOS

>**No bios is required.**
{.is-success}

## ![](/emulators/roms.png) Roms

Model3 is based on the Mame 0.235 romset.

>You must pass your MAME romset in ClrMamePro to have a working Model3 romset.
{.is-info}

>In order to sort your arcade roms, the dat file is available in the folder `/recalbox/share/bios/supermodel/`.
{.is-success}

### Location

Put your roms like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 model3
┃ ┃ ┃ ┃ ┣ 🗒 **game.zip**

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Supermodel configuration file:

```text
#
#  ####                                                      ###           ###
# ##  ##                                                      ##            ##
# ###     ##  ##  ## ###   ####   ## ###  ##  ##   ####       ##   ####     ##
#  ###    ##  ##   ##  ## ##  ##   ### ## ####### ##  ##   #####  ##  ##    ##
#    ###  ##  ##   ##  ## ######   ##  ## ####### ##  ##  ##  ##  ######    ##
# ##  ##  ##  ##   #####  ##       ##     ## # ## ##  ##  ##  ##  ##        ##
#  ####    ### ##  ##      ####   ####    ##   ##  ####    ### ##  ####    ####
#                 ####                                                            
#
#                       A Sega Model 3 Arcade Emulator.
#                Copyright 2011 Bart Trzynadlowski, Nik Henson


# ----------------------------- CONTROLLERS -------------------------------- #
# The rate at which analog control values increase/decrease
# when controlled by a key.  Valid range is 1-100, with 1
# being the least sensitive (slowest response) on default [25]
sensitivity=25

# Specifies the saturation, the position at which the joystick
# is interpreted as being in its most extreme position,
# as a % of the total range of the axis, from 0-200.
# A value of 200 means that the range will be halved, on default [100].
saturation=100

# Specifies the dead zone as a percentage,
# 0-99, of the total range of the axis.
# Within the dead zone, the joystick is inactive on default [2].
deadzone=2

# ------------------------------- AUDIO ------------------------------------- #

# Master Volume in %
sound-volume=100
# Set Music volume in %
music-volume=100

# Swap left and right audio channels
flip-stereo=0
# Relative front/rear balance in %
balance=0

# Disable sound board emulation sound effects
no-sound=0
# Disable Digital Sound Board MPEG music
no-dsb=0

# Select your sound engine:
# New SCSP engine based on MAME [0]
# Legacy SCSP engine [1]
sound-engine=0

# ------------------------------- VIDEO ------------------------------------- #

# set your graphics resolution automaticly with [auto]
# or set on this format [1280,1024] or [none] if needed
resolution=auto

# set on default ratio with [0]
# Expand 3D field of view to screen width [1]
# ignoring aspect ratio with [2]
screen-ratio=0
# Disable 60 Hz frame rate lock
no-throttle=0

# New 3D engine on default [0] enable 
# Legacy 3D engine only if no image only sound set [1] to enable 
3d-engine=0
# Use 8 texture maps for decoding
multi-texture=0
# Enable proper quad rendering
quad-rendering=0

# Crosshairs configuration for gun games
crosshairs=1

# -------------------------------- CORE ------------------------------------- #

# Run graphics rendering in main thread
no-gpu-thread=1
# Disable [0], Enable [1] multi-threading.
no-threads=0

# The PowerPC frequency in MHz.
# The default is [50].
ppc-frequency=100

# Enable [1] or Disable [0] menu services [L3=test, R3=service]
service-button=1

# Log level information in Supermodel.log
# Default value : info
# add value : error
log-level=info
```

**File location**: `/recalbox/share/system/configs/model3/ConfigModel3.ini`

### Core options

## ![](/emulators/external-links.png) External links

* **Official forum** : [https://www.supermodel3.com/](https://www.supermodel3.com/)
* **Official Sourceforge** : [https://sourceforge.net/projects/model3emu/](https://sourceforge.net/projects/model3emu/)