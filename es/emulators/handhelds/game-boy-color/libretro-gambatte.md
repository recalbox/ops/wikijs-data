---
title: Libretro Gambatte
description: 
published: true
date: 2024-07-25T22:16:18.407Z
tags: libretro, color, gameboy, gambatte, gbc
editor: markdown
dateCreated: 2023-09-11T13:09:48.629Z
---

**Libretro Gambatte** es un emulador de Game Boy Color multiplataforma, de código abierto y centrado en la precisión, escrito en C++.  
Se basa en cientos de pruebas de casos prácticos de hardware, así como en documentación previa y esfuerzos de ingeniería inversa.

## ![](/emulators/license.svg) Licencia

El core posee la siguiente licencia [**GPLv2**](https://github.com/libretro/gambatte-libretro/blob/master/COPYING).

## ![](/emulators/compatibility.png) Compatibilidad

 | RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | 
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Funcionalidades

| Funcionalidad | ¿Soportada? |
| :---: | :---: |
| Reinicio | ✔ |
| Grabaciones | ✔ |
| Grabaciones instantáneas | ✔ |
| Rebobinado | ✔ |
| Opciones del core | ✔ |
| RetroAchievements | ✔ |
| Cheats RetroArch | ✔ |
| Controles | ✔ |
| Remapping | ✔ |
| Softpatching | ✔ |

## ![](/emulators/bios.svg) BIOS

### Lista de BIOS opcionales

| Nombre del fichero | Descripción | MD5 | ¿Preinstalada? |
| :---: | :---: | :---: | :---: |
| gbc_bios.bin | BIOS Game Boy Color | dbfce9db9deaa2567f6a84fde55f9680 | ❌ |

### Emplazamiento

Disponed vuestras bios como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 gbc
┃ ┃ ┃ ┃ ┣ 🗒 **gbc_bios.bin**

## ![](/emulators/roms.png) Roms

### Extensiones de roms soportadas

Las roms deben tener las extensiones siguientes:

* .bin
* .gbc
* .zip
* .7z

Este sistema soporta las roms comprimidas en formato .zip/.7z. No obstante, se trata sólo de un archivo.

Los ficheros contenidos en los .zip/.7z deben tener las extensiones citadas previamente.
Además, cada fichero .zip/.7z sólo debe contener un fichero rom al interior.

### Emplazamiento

Disponed vuestras roms como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 gbc
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.zip**

>Se aconsejan las roms del proyecto **No-Intro**.
{.is-success}

>Para más información sobre las roms, consultad [este tutorial](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

>Para conservar las configuraciones personalizadas cuando se produce una actualización del sistema Recalbox, os aconsejamos utilizar las [Sobrecargas de configuración](./../../../advanced-usage/configuration-override).
{.is-info}

### Acceder a las opciones

Podéis configurar las opciones del core de dos formas diferentes.

* Via el Menú RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via el fichero `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Opciones del core

| Opción | Valor de la opción | Variable (para la sobrecarga) | Valores posibles |
| --- | --- | --- | --- |
| Colorisation GB | `Désactivé` ✅ / `Auto` / `GBC` / `SGB` / `Interne` / `Personnalisé` | `gambatte_gb_colorization` | `disabled` / `auto` / `GBC` / `SGB` / `internal` / `custom` |
| Palette interne | `GB - DMG` ✅ / `GB - Pocket` / `GB - Light` / `GBC - Bleue` / `GBC - Marron` / `GBC - Bleu foncé` / `GBC - Marron foncé` / `GBC - Vert foncé` / `GBC - Niveaux de gris` / `GBC - Verte` / `GBC - Inversée` / `GBC - Orange` / `GBC - Mix pastel` / `GBC - Rouge` / `GBC - Jaune` / `SGB - 1A` / `SGB - 1B` / `SGB - 1C` / `SGB - 1D` / `SGB - 1E` / `SGB - 1F` / `SGB - 1G` / `SGB - 1H` / `SGB - 2A` / `SGB - 2B` / `SGB - 2C` / `SGB - 2D` / `SGB - 2E` / `SGB - 2F` / `SGB - 2G` / `SGB - 2H` / `SGB - 3A` / `SGB - 3B` / `SGB - 3C` / `SGB - 3D` / `SGB - 3E` / `SGB - 3F` / `SGB - 3G` / `SGB - 3H` / `SGB - 4A` / `SGB - 4B` / `SGB - 4C` / `SGB - 4D` / `SGB - 4E` / `SGB - 4F` / `SGB - 4G` / `SGB - 4H` / `Spéciale 1` / `Spéciale 2` / `Spéciale 3` / `Spéciale 4 (Héritée de la TI-83)` / `TWB64 - Pack 1` / `TWB64 - Pack 2` / `PixelShift - Pack 1` | `gambatte_gb_internal_palette` | `GB - DMG` / `GB - Pocket` / `GB - Light` / `GBC - Blue` / `GBC - Brown` / `GBC - Dark Blue` / `GBC - Dark Brown` / `GBC - Dark Green` / `GBC - Grayscale` / `GBC - Green` / `GBC - Inverted` / `GBC - Orange` / `GBC - Pastel Mix` / `GBC - Red` / `GBC - Yellow` / `SGB - 1A` / `SGB - 1B` / `SGB - 1C` / `SGB - 1D` / `SGB - 1E` / `SGB - 1F` / `SGB - 1G` / `SGB - 1H` / `SGB - 2A` / `SGB - 2B` / `SGB - 2C` / `SGB - 2D` / `SGB - 2E` / `SGB - 2F` / `SGB - 2G` / `SGB - 2H` / `SGB - 3A` / `SGB - 3B` / `SGB - 3C` / `SGB - 3D` / `SGB - 3E` / `SGB - 3F` / `SGB - 3G` / `SGB - 3H` / `SGB - 4A` / `SGB - 4B` / `SGB - 4C` / `SGB - 4D` / `SGB - 4E` / `SGB - 4F` / `SGB - 4G` / `SGB - 4H` / `Special 1` / `Special 2` / `Special 3` / `Special 4 (TI-83 Legacy)` / `TWB64 - Pack 1` / `TWB64 - Pack 2` / `PixelShift - Pack 1` |
| > TWB64 - Palette du pack 1 | `TWB64 001 - Bleu Aqours` ✅ / `TWB64 002 - Version Anime Expo` / `TWB64 003 - Jaune Bob l'éponge` / `TWB64 004 - Rose Patrick Étoile` / `TWB64 005 - Rouge néon` / `TWB64 006 - Bleu néon` / `TWB64 007 - Jaune néon` / `TWB64 008 - Vert néon` / `TWB64 009 - Rose néon` / `TWB64 010 - Rouge Mario` / `TWB64 011 - Orange Nick` / `TWB64 012 - Version Virtual Boy` / `TWB64 013 - Doré Wild` / `TWB64 014 - Jaune constructeur` / `TWB64 015 - Bleuet classique` / `TWB64 016 - Version 765 Production` / `TWB64 017 - Ivoire Superball` / `TWB64 018 - Orange Crunchyroll` / `TWB64 019 - Rose Muse` / `TWB64 020 - Jaune Nijigasaki` / `TWB64 021 - Version Gamate` / `TWB64 022 - Version niveaux de vert` / `TWB64 023 - Or Odyssey` / `TWB64 024 - Dieu Super Saiyan` / `TWB64 025 - Bleu Super Saiyan` / `TWB64 026 - Rose Bizarre` / `TWB64 027 - Version Nintendo Switch Lite` / `TWB64 028 - Version Game.com` / `TWB64 029 - Rose Sanrio` / `TWB64 030 - Version BANDAI NAMCO` / `TWB64 031 - Vert Cosmo` / `TWB64 032 - Rose Wanda` / `TWB64 033 - Version Link's Awakening DX` / `TWB64 034 - Bois des Voyages` / `TWB64 035 - Version Pokémon` / `TWB64 036 - Orange Game Grump` / `TWB64 037 - Version Scooby-Doo Mystères` / `TWB64 038 - Version Pokémon mini` / `TWB64 039 - Version Supervision` / `TWB64 040 - Version DMG` / `TWB64 041 - Version Pocket` / `TWB64 042 - Version Light` / `TWB64 043 - Bleu Miraitowa` / `TWB64 044 - Rose Someity` / `TWB64 045 - Jaune Pikachu` / `TWB64 046 - Marron Évoli` / `TWB64 047 - Version Microvosion` / `TWB64 048 - Verison TI-83` / `TWB64 049 - Cerise Aegis` / `TWB64 050 - Faon Labo` / `TWB64 051 - OR MILLION LIVE!` / `TWB64 052 - Version Tokyo Midtown` / `TWB64 053 - Version VMU` / `TWB64 054 - Version Game Master` / `TWB64 055 - Vert Android` / `TWB64 056 - Azur Ticketmaster` / `TWB64 057 - Rouge Google` / `TWB64 058 - Bleu Google` / `TWB64 059 - Jaune Google` / `TWB64 060 - Vert Google` / `TWB64 061 - Version WonderSwan` / `TWB64 062 - Version Neo Geo Pocket` / `TWB64 063 - Vert Dew` / `TWB64 064 - Rouge Coca-Cola` / `TWB64 065 - Version GameKing` / `TWB64 066 - Version Do The Dew` / `TWB64 067 - Version Digivice` / `TWB64 068 - Version Bikini Bottom` / `TWB64 069 - Rose Belle` / `TWB64 070 - Bleu Bulle` / `TWB64 071 - Vert Rebelle` / `TWB64 072 - Version NASCAR` / `TWB64 073 - Citron-citron vert` / `TWB64 074 - Version Mega Man V` / `TWB64 075 - Version Tamagotchi` / `TWB64 076 - Rouge Fantômes` / `TWB64 077 - Version Halloween` / `TWB64 078 - Version Noël` / `TWB64 079 - Rose Cardcaptor` / `TWB64 080 - Or Pretty Guardian` / `TWB64 081 - Version camouflage` / `TWB64 082 - Super saiyan légendaire` / `TWB64 083 - Rosé Super Saiyan` / `TWB64 084 - Super Saiyan` / `TWB64 085 - Ultra Instinct maîtrisé` / `TWB64 086 - Rouge Saint Snow` / `TWB64 087 - Banane jaune` / `TWB64 088 - Banane verte` / `TWB64 089 - Super Saiyan 3` / `TWB64 090 - Super Saiyan Blue Évolué` / `TWB64 091 - Version Pocket Tales` / `TWB64 092 - Jaune Investigation` / `TWB64 093 - Bleu S.E.E.S.` / `TWB64 094 - Cyan Game Awards` / `TWB64 095 - Orange Hokage` / `TWB64 096 - Rouge Chapeau de paille` / `TWB64 097 - Cyan Sword Art` / `TWB64 098 - Émeraude Deku Alpha` / `TWB64 099 - Version Blue Stripes` / `TWB64 100 - Orange Stone` | gambatte_gb_palette_twb64_1 | `TWB64 001 - Aqours Blue` / `TWB64 002 - Anime Expo Ver.` / `TWB64 003 - SpongeBob Yellow` / `TWB64 004 - Patrick Star Pink` / `TWB64 005 - Neon Red` / `TWB64 006 - Neon Blue` / `TWB64 007 - Neon Yellow` / `TWB64 008 - Neon Green` / `TWB64 009 - Neon Pink` / `TWB64 010 - Mario Red` / `TWB64 011 - Nick Orange` / `TWB64 012 - Virtual Boy Ver.` / `TWB64 013 - Golden Wild` / `TWB64 014 - Builder Yellow` / `TWB64 015 - Classic Blurple` / `TWB64 016 - 765 Production Ver.` / `TWB64 017 - Superball Ivory` / `TWB64 018 - Crunchyroll Orange` / `TWB64 019 - Muse Pink` / `TWB64 020 - Nijigasaki Yellow` / `TWB64 021 - Gamate Ver.` / `TWB64 022 - Greenscale Ver.` / `TWB64 023 - Odyssey Gold` / `TWB64 024 - Super Saiyan God` / `TWB64 025 - Super Sayian Blue` / `TWB64 026 - Bizarre Pink` / `TWB64 027 - Nintendo Switch Lite Ver.` / `TWB64 028 - Game.com Ver.` / `TWB64 029 - Sanrio Pink` / `TWB64 030 - BANDAI NAMCO Ver.` / `TWB64 031 - Cosmo Green` / `TWB64 032 - Wanda Pink` / `TWB64 033 - Link's Awakening DX Ver.` / `TWB64 034 - Travel Wood` / `TWB64 035 - Pokemon Ver.` / `TWB64 036 - Game Grump Orange` / `TWB64 037 - Scooby-Doo Mystery Ver.` / `TWB64 038 - Pokemon mini Ver.` / `TWB64 039 - Supervision Ver.` / `TWB64 040 - DMG Ver.` / `TWB64 041 - Pocket Ver.` / `TWB64 042 - Light Ver.` / `TWB64 043 - Miraitowa Blue` / `TWB64 044 - Someity Pink` / `TWB64 045 - Pikachu Yellow` / `TWB64 046 - Eevee Brown` / `TWB64 047 - Microvision Ver.` / `TWB64 048 - TI-83 Ver.` / `TWB64 049 - Aegis Cherry` / `TWB64 050 - Labo Fawn` / `TWB64 051 - MILLION LIVE GOLD!` / `TWB64 052 - Tokyo Midtown Ver.` / `TWB64 053 - VMU Ver.` / `TWB64 054 - Game Master Ver.` / `TWB64 055 - Android Green` / `TWB64 056 - Ticketmaster Azure` / `TWB64 057 - Google Red` / `TWB64 058 - Google Blue` / `TWB64 059 - Google Yellow` / `TWB64 060 - Google Green` / `TWB64 061 - WonderSwan Ver.` / `TWB64 062 - Neo Geo Pocket Ver.` / `TWB64 063 - Dew Green` / `TWB64 064 - Coca-Cola Red` / `TWB64 065 - GameKing Ver.` / `TWB64 066 - Do The Dew Ver.` / `TWB64 067 - Digivice Ver.` / `TWB64 068 - Bikini Bottom Ver.` / `TWB64 069 - Blossom Pink` / `TWB64 070 - Bubbles Blue` / `TWB64 071 - Buttercup Green` / `TWB64 072 - NASCAR Ver.` / `TWB64 073 - Lemon-Lime Green` / `TWB64 074 - Mega Man V Ver.` / `TWB64 075 - Tamagotchi Ver.` / `TWB64 076 - Phantom Red` / `TWB64 077 - Halloween Ver.` / `TWB64 078 - Christmas Ver.` / `TWB64 079 - Cardcapter Pink` / `TWB64 080 - Pretty Guardian Gold` / `TWB64 081 - Camouflage Ver.` / `TWB64 082 - Legendary Super Saiyan` / `TWB64 083 - Super Saiyan Rose` / `TWB64 084 - Super Saiyan` / `TWB64 085 - Perfected Ultra Instinct` / `TWB64 086 - Saint Snow Red` / `TWB64 087 - Yellow Banana` / `TWB64 088 - Green Banana` / `TWB64 089 - Super Saiyan 3` / `TWB64 090 - Super Saiyan Blue Evolved` / `TWB64 091 - Pocket Tales Ver.` / `TWB64 092 - Investigation Yellow` / `TWB64 093 - S.E.E.S. Blue` / `TWB64 094 - Game Awards Cyan` / `TWB64 095 - Hokage Orange` / `TWB64 096 - Straw Hat Red` / `TWB64 097 - Sword Art Cyan` / `TWB64 098 - Deku Alpha Emerald` / `TWB64 099 - Blue Stripes Ver.` / `TWB64 100 - Stone Orange` |
| > TWB64 - Palette du pack 2 | `TWB64 101 - Rose 765PRO` ✅ / `TWB64 102 - Bleu CINDERELLA` / `TWB64 103 - Jaune MILLION !` / `TWB64 104 - Vert SideM` / `TWB64 105 - Bleu ciel SHINY` / `TWB64 106 - Version Angry Volcano` / `TWB64 107 - Rose Yo-kai` / `TWB64 108 - Vert Yo-kai` / `TWB64 109 - Bleu Yo-kai` / `TWB64 110 - Violet Yo-kai` / `TWB64 111 - Iro aquatique` / `TWB64 112 - Thé midori` / `TWB64 113 - Rose sakura` / `TWB64 114 - Wisteria Murasaki` / `TWB64 115 - Oni Aka` / `TWB64 116 - Kiiro doré` / `TWB64 117 - Shiro argenté` / `TWB64 118 - Orange fruité` / `TWB64 119 - Rose AKB48` / `TWB64 120 - Bleu Miku` / `TWB64 121 - Rouge Fairy Tail` / `TWB64 122 - Marron Survey Corps` / `TWB64 123 - Vert insulaire` / `TWB64 124 - Vert Mania Plus` / `TWB64 125 - Vert Tortues Ninja` / `TWB64 126 - Bleu slime` / `TWB64 127 - Citron vert Midori` / `TWB64 128 - Aoi fantomatique` / `TWB64 129 - Bogeda rétro` / `TWB64 130 - Bleu royal` / `TWB64 131 - Violet néon` / `TWB64 132 - Orange néon` / `TWB64 133 - Vision Moonlight` / `TWB64 134 - Rouge Tokyo` / `TWB64 135 - Or Paris` / `TWB64 136 - Bleu Beijing` / `TWB64 137 - Jaune Pac-Man` / `TWB64 138 - Vert irlandais` / `TWB64 139 - Orange Kakarot` / `TWB64 140 - Orange Dragon Ball` / `TWB64 141 - Or de Noël` / `TWB64 142 - Bleu Pepsi` / `TWB64 143 - Vert bub` / `TWB64 144 - Bleu Bob` / `TWB64 145 - Tempête Baja Blast` / `TWB64 146 - Or olympique` / `TWB64 147 - Orange valeur` / `TWB64 148 - Violet Liella !` / `TWB64 149 - Argent olympique` / `TWB64 150 - Bronze olympique` / `TWB64 151 - Bleu ciel ANA` / `TWB64 152 - Orange Nijigasaki` / `TWB64 153 - HoloBleu` / `TWB64 154 - Rouge catch` / `TWB64 155 - Vert œuf de Yoshi` / `TWB64 156 - Rouge Pokédex` / `TWB64 157 - Rêve bleu Disney` / `TWB64 158 - Vert Xbox` / `TWB64 159 - Mega Bleu Sonic` / `TWB64 160 - Orange G4` / `TWB64 161 - Vert Scarlett` / `TWB64 162 - Bleu Glitchy` / `TWB64 163 - LCD classique` / `TWB64 164 - Version Console virtuelle 3DS` / `TWB64 165 - Version PocketStation` / `TWB64 166 - Game and Or` / `TWB64 167 - Schtroumpfement bleu` / `TWB64 168 - Vert ogre marécageux` / `TWB64 169 - Vert épinards de marin` / `TWB64 170 - Vert Shenron` / `TWB64 171 - Sang Berserk` / `TWB64 172 - Rose Fun Pak` / `TWB64 173 - Version Gamebuino Classic` / `TWB64 174 - Rose Barbie` / `TWB64 175 - Vert Star Command` / `TWB64 176 - Version Nokia 3310` / `TWB64 177 - Vert trèfle` / `TWB64 178 - Orange Crash` / `TWB64 179 - Jaune disquette Famicom` / `TWB64 180 - Rouge Team Rocket` / `TWB64 181 - Jaune minuteur SEIKO` / `TWB64 182 - PINK109` / `TWB64 183 - Bleu Doraemon` / `TWB64 184 - Bleu Fury` / `TWB64 185 - Orage Rockstar` / `TWB64 186 - Vert Puyo Puyo` / `TWB64 187 - Rose Susan G.` / `TWB64 188 - Rouge Pizza Hut` / `TWB64 189 - Vert plumbob` / `TWB64 190 - Grand Ivoire` / `TWB64 191 - Or du démon` / `TWB64 192 - Bleu Tokyo SEGA` / `TWB64 193 - Bleu Champion` / `TWB64 194 - Marron baril DK` / `TWB64 195 - Vert Evangelion` / `TWB64 196 - Violet équestre` / `TWB64 197 - Rouge Autobot` / `TWB64 198 - Vert océan Niconico` / `TWB64 199 - Cuivre Duracell` / `TWB64 200 - BLEU NUAGEUX TOKYO SKYTREE` | `gambatte_gb_palette_twb64_2` | `TWB64 101 - 765PRO Pink` / `TWB64 102 - CINDERELLA Blue` / `TWB64 103 - MILLION Yellow!` / `TWB64 104 - SideM Green` / `TWB64 105 - SHINY Sky Blue` / `TWB64 106 - Angry Volcano Ver.` / `TWB64 107 - Yo-kai Pink` / `TWB64 108 - Yo-kai Green` / `TWB64 109 - Yo-kai Blue` / `TWB64 110 - Yo-kai Purple` / `TWB64 111 - Aquatic Iro` / `TWB64 112 - Tea Midori` / `TWB64 113 - Sakura Pink` / `TWB64 114 - Wisteria Murasaki` / `TWB64 115 - Oni Aka` / `TWB64 116 - Golden Kiiro` / `TWB64 117 - Silver Shiro` / `TWB64 118 - Fruity Orange` / `TWB64 119 - AKB48 Pink` / `TWB64 120 - Miku Blue` / `TWB64 121 - Fairy Tail Red` / `TWB64 122 - Survey Corps Brown` / `TWB64 123 - Island Green` / `TWB64 124 - Mania Plus Green` / `TWB64 125 - Ninja Turtle Green` / `TWB64 126 - Slime Blue` / `TWB64 127 - Lime Midori` / `TWB64 128 - Ghostly Aoi` / `TWB64 129 - Retro Bodega` / `TWB64 130 - Royal Blue` / `TWB64 131 - Neon Purple` / `TWB64 132 - Neon Orange` / `TWB64 133 - Moonlight Vision` / `TWB64 134 - Tokyo Red` / `TWB64 135 - Paris Gold` / `TWB64 136 - Beijing Blue` / `TWB64 137 - Pac-Man Yellow` / `TWB64 138 - Irish Green` / `TWB64 139 - Kakarot Orange` / `TWB64 140 - Dragon Ball Orange` / `TWB64 141 - Christmas Gold` / `TWB64 142 - Pepsi-Cola Blue` / `TWB64 143 - Bubblun Green` / `TWB64 144 - Bubblun Blue` / `TWB64 145 - Baja Blast Storm` / `TWB64 146 - Olympic Gold` / `TWB64 147 - Value Orange` / `TWB64 148 - Liella Purple!` / `TWB64 149 - Olympic Silver` / `TWB64 150 - Olymbic Bronze` / `TWB64 151 - ANA Sky Blue` / `TWB64 152 - Nijigasaki Orange` / `TWB64 153 - HoloBlue` / `TWB64 154 - Wrestling Red` / `TWB64 155 - Yoshi Egg Green` / `TWB64 156 - Pokedex Red` / `TWB64 157 - Disney Dream Blue` / `TWB64 158 - Xbox Green` / `TWB64 159 - Sonic Mega Blue` / `TWB64 160 - G4 Orange` / `TWB64 161 - Scarlett Green` / `TWB64 162 - Glitchy Blue` / `TWB64 163 - Classic LCD` / `TWB64 164 - 3DS Virtual Console Ver.` / `TWB64 165 - PocketStation Ver.` / `TWB64 166 - Game and Gold` / `TWB64 167 - Smurfy Blue` / `TWB64 168 - Swampy Ogre Green` / `TWB64 169 - Sailor Spinach Green` / `TWB64 170 - Shenron Green` / `TWB64 171 - Berserk Blood` / `TWB64 172 - Super Star Pink` / `TWB64 173 - Gamebuino Classic Ver.` / `TWB64 174 - Barbie Pink` / `TWB64 175 - Star Command Green` / `TWB64 176 - Nokia 3310 Ver.` / `TWB64 177 - Clover Green` / `TWB64 178 - Crash Orange` / `TWB64 179 - Famicom Disk Yellow` / `TWB64 180 - Team Rocket Red` / `TWB64 181 - SEIKO Timer Yellow` / `TWB64 182 - PINK109` / `TWB64 183 - Doraemon Blue` / `TWB64 184 - Fury Blue` / `TWB64 185 - Rockstar Orange` / `TWB64 186 - Puyo Puyo Green` / `TWB64 187 - Susan G. Pink` / `TWB64 188 - Pizza Hut Red` / `TWB64 189 - Plumbob Green` / `TWB64 190 - Grand Ivory` / `TWB64 191 - Demon's Gold` / `TWB64 192 - SEGA Tokyo Blue` / `TWB64 193 - Champion Blue` / `TWB64 194 - DK Barrel Brown` / `TWB64 195 - Evangelion Green` / `TWB64 196 - Equestrian Purple` / `TWB64 197 - Autobot Red` / `TWB64 198 - Niconico Sea Green` / `TWB64 199 - Duracell Copper` / `TWB64 200 - TOKYO SKYTREE CLOUDY BLUE` |
| > PixelShift - Palette Pack 1 | `PixelShift 01 - Vert arctique` ✅ / `PixelShift 02 - Arduboy` / `PixelShift 03 - Émulateur BGB 0.3` / `PixelShift 04 - Camouflage` / `PixelShift 05 - Barre de chocolat` / `PixelShift 06 - CMJN` / `PixelShift 07 - Barbe à papa` / `PixelShift 08 - Verts faciles` / `PixelShift 09 - Gamate` / `PixelShift 10 - Game Boy Light` / `PixelShift 11 - Game Boy Pocket` / `PixelShift 12 - Game Boy Pocket alt.` / `PixelShift 13 - Game Pocket Computer` / `PixelShift 14 - Game & Watch Ball` / `PixelShift 15 - GB rétro-éclairage bleu` / `PixelShift 16 - GB rétro-éclairage décoloré` / `PixelShift 17 - GB rétro-éclairage orange` / `PixelShift 18 - GB rétro-éclairage blanc` / `PixelShift 19 - GB rétro-éclairage jaune foncé` / `PixelShift 20 - GB de contrebande` / `PixelShift 21 - GB Hunter` / `PixelShift 22 - Kiosque GB` / `PixelShift 23 - Kiosque GB 2` / `PixelShift 24 - Nouvelle GB` / `PixelShift 25 - GB détruite` / `PixelShift 26 - Vieille GB` / `PixelShift 27 - GBP Bivert` / `PixelShift 28 - GB rétroéclairage jaune délavé` / `PixelShift 29 - Fantôme` / `PixelShift 30 - Brille dans le noir` / `PixelShift 31 - Lingot d'or` / `PixelShift 32 - Pamplemousse` / `PixelShift 33 - Mélange vert gris` / `PixelShift 34 - Missingno` / `PixelShift 35 - MS-Dos` / `PixelShift 36 - Journal` / `PixelShift 37 - Pip-Boy` / `PixelShift 38 - Pocket Girl` / `PixelShift 39 - Silhouette` / `PixelShift 40 - Coup de soleil` / `PixelShift 41 - Technicolor` / `PixelShift 42 - Tron` / `PixelShift 43 - Vaporware` / `PixelShift 44 - Virtual Boy` / `PixelShift 45 - Wish` | `gambatte_gb_palette_pixelshift_1` | `PixelShift 01 - Arctic Green` / `PixelShift 02 - Arduboy` / `PixelShift 03 - BGB 0.3 Emulator` / `PixelShift 04 - Camouflage` / `PixelShift 05 - Chocolate Bar` / `PixelShift 06 - CMYK` / `PixelShift 07 - Cotton Candy` / `PixelShift 08 - Easy Greens` / `PixelShift 09 - Gamate` / `PixelShift 10 - Game Boy Light` / `PixelShift 11 - Game Boy Pocket` / `PixelShift 12 - Game Boy Pocket Alt` / `PixelShift 13 - Game Pocket Computer` / `PixelShift 14 - Game & Watch Ball` / `PixelShift 15 - GB Backlight Blue` / `PixelShift 16 - GB Backlight Faded` / `PixelShift 17 - GB Backlight Orange` / `PixelShift 18 - GB Backlight White` / `PixelShift 19 - GB Backlight Yellow Dark` / `PixelShift 20 - GB Bootleg` / `PixelShift 21 - GB Hunter` / `PixelShift 22 - GB Kiosk` / `PixelShift 23 - GB Kiosk 2` / `PixelShift 24 - GB New` / `PixelShift 25 - GB Nuked` / `PixelShift 26 - GB Old` / `PixelShift 27 - GBP Bivert` / `PixelShift 28 - GB Washed Yellow Backlight` / `PixelShift 29 - Ghost` / `PixelShift 30 - Glow In The Dark` / `PixelShift 31 - Gold Bar` / `PixelShift 32 - Grapefruit` / `PixelShift 33 - Gray Green Mix` / `PixelShift 34 - Missingno` / `PixelShift 35 - MS-Dos` / `PixelShift 36 - Newspaper` / `PixelShift 37 - Pip-Boy` / `PixelShift 38 - Pocket Girl` / `PixelShift 39 - Silhouette` / `PixelShift 40 - Sunburst` / `PixelShift 41 - Technicolor` / `PixelShift 42 - Tron` / `PixelShift 43 - Vaporware` / `PixelShift 44 - Virtual Boy` / `PixelShift 45 - Wish` |
| Correction colorimétrique | `GBC uniquement` ✅ / `Toujours` / `Désactivé` | `gambatte_gbc_color_correction` | `GBC only` / `always` / `disabled` |
| Mode de correction colorimétrique | `Précis` ✅ / `Rapide` | `gambatte_gbc_color_correction_mode` | `accurate` / `fast` |
| Correction colorimétrique - Position de l'éclairage avant | `Centrale` ✅ / `Au-dessus de l'écran` / `En dessous de l'écran` | `gambatte_gbc_frontlight_position` | `central` / `above screen` / `below screen` |
| Niveau de filtre d'assombrissement (%) | `0` ✅ / `5` / `10` / `15` / `20` / `25` / `30` / `35` / `40` / `45` / `50` | `gambatte_dark_filter_level` | `0` / `5` / `10` / `15` / `20` / `25` / `30` / `35` / `40` / `45` / `50` |
| Interframe Blending | `Désactivé` ✅ / `Simple` / `Rémanence LCD (réaliste)` / `Rémanence LCD (rapide)` | `gambatte_mix_frames` | `disabled` / `mix` / `lcd_ghosting` / `lcd_ghosting_fast` |
| Audio Resampler | `Sinc` ✅ / `Cosinus` | `gambatte_audio_resampler` | `sinc` / `cc` |
| Matériel émulé (Redémarrage requis) | `Auto` ✅ / `GB` / `GBC` / `GBA` | `gambatte_gb_hwmode` | `Auto` / `GB` / `GBC` / `GBA` |
| Utiliser le bootloader officiel (Redémarrage requis) | `Activé` ✅ / `Désactivé` | `gambatte_gb_bootloader` | `enabled` / `disabled` |
| Autoriser les directions opposées | `Désactivé` ✅ / `Activé` | `gambatte_up_down_allowed` | `disabled` / `enabled` |
| Délai d'activation du bouton en mode turbo | `4` ✅ / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `21` / `22` / `23` / `24` / `25` / `26` / `27` / `28` / `29` / `30` / `31` / `32` / `33` / `34` / `35` / `36` / `37` / `38` / `39` / `40` / `41` / `42` / `43` / `44` / `45` / `46` / `47` / `48` / `49` / `50` / `51` / `52` / `53` / `54` / `55` / `56` / `57` / `58` / `59` / `60` / `61` / `62` / `63` / `64` / `65` / `66` / `67` / `68` / `69` / `70` / `71` / `72` / `73` / `74` / `75` / `76` / `77` / `78` / `79` / `80` / `81` / `82` / `83` / `84` / `85` / `86` / `87` / `88` / `89` / `90` / `91` / `92` / `93` / `94` / `95` / `96` / `97` / `98` / `99` / `100` / `101` / `102` / `103` / `104` / `105` / `106` / `107` / `108` / `109` / `110` / `111` / `112` / `113` / `114` / `115` / `116` / `117` / `118` / `119` / `120` | `gambatte_turbo_period` | `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `21` / `22` / `23` / `24` / `25` / `26` / `27` / `28` / `29` / `30` / `31` / `32` / `33` / `34` / `35` / `36` / `37` / `38` / `39` / `40` / `41` / `42` / `43` / `44` / `45` / `46` / `47` / `48` / `49` / `50` / `51` / `52` / `53` / `54` / `55` / `56` / `57` / `58` / `59` / `60` / `61` / `62` / `63` / `64` / `65` / `66` / `67` / `68` / `69` / `70` / `71` / `72` / `73` / `74` / `75` / `76` / `77` / `78` / `79` / `80` / `81` / `82` / `83` / `84` / `85` / `86` / `87` / `88` / `89` / `90` / `91` / `92` / `93` / `94` / `95` / `96` / `97` / `98` / `99` / `100` / `101` / `102` / `103` / `104` / `105` / `106` / `107` / `108` / `109` / `110` / `111` / `112` / `113` / `114` / `115` / `116` / `117` / `118` / `119` / `120` |
| Intensité de la vibration manette | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` ✅ | `gambatte_rumble_level` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` |
| Mode liaison | `Non connecté` ✅ / `Serveur réseau` / `Client réseau` | `gambatte_gb_link_mode` | `Not Connected` / `Network Server` / `Network Client` |
| Network Link Port | `56400` ✅ / `56401` / `56402` / `56403` / `56404` / `56405` / `56406` / `56407` / `56408` / `56409` / `56410` / `56411` / `56412` / `56413` / `56414` / `56415` / `56416` / `56417` / `56418` / `56419` / `56420` | `gambatte_gb_link_network_port` | `56400` / `56401` / `56402` / `56403` / `56404` / `56405` / `56406` / `56407` / `56408` / `56409` / `56410` / `56411` / `56412` / `56413` / `56414` / `56415` / `56416` / `56417` / `56418` / `56419` / `56420` |
| Network Link Server Address Pt. 01: \_\_\_.\_\_\_.\_\_\_.\_\_x | `0` ✅ / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` | `gambatte_gb_link_network_server_ip_1` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` |
| Network Link Server Address Pt. 02: \_\_\_.\_\_\_.\_\_\_.\_\_x | `0` ✅ / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` | `gambatte_gb_link_network_server_ip_2` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` |
| Network Link Server Address Pt. 03: \_\_\_.\_\_\_.\_\_\_.\_\_x | `0` ✅ / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` | `gambatte_gb_link_network_server_ip_3` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` |
| Network Link Server Address Pt. 04: \_\_\_.\_\_\_.\_\_\_.\_\_x | `0` ✅ / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` | `gambatte_gb_link_network_server_ip_4` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` |
| Network Link Server Address Pt. 05: \_\_\_.\_\_\_.\_\_\_.\_\_x | `0` ✅ / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` | `gambatte_gb_link_network_server_ip_5` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` |
| Network Link Server Address Pt. 06: \_\_\_.\_\_\_.\_\_\_.\_\_x | `0` ✅ / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` | `gambatte_gb_link_network_server_ip_6` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` |
| Network Link Server Address Pt. 07: \_\_\_.\_\_\_.\_\_\_.\_\_x | `0` ✅ / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` | `gambatte_gb_link_network_server_ip_7` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` |
| Network Link Server Address Pt. 08: \_\_\_.\_\_\_.\_\_\_.\_\_x | `0` ✅ / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` | `gambatte_gb_link_network_server_ip_8` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` |
| Network Link Server Address Pt. 09: \_\_\_.\_\_\_.\_\_\_.\_\_x | `0` ✅ / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` | `gambatte_gb_link_network_server_ip_9` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` |
| Network Link Server Address Pt. 10: \_\_\_.\_\_\_.\_\_\_.\_\_x | `0` ✅ / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` | `gambatte_gb_link_network_server_ip_10` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` |
| Network Link Server Address Pt. 11: \_\_\_.\_\_\_.\_\_\_.\_\_x | `0` ✅ / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` | `gambatte_gb_link_network_server_ip_11` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` |
| Network Link Server Address Pt. 12: \_\_\_.\_\_\_.\_\_\_.\_\_x | `0` ✅ / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` | `gambatte_gb_link_network_server_ip_12` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` |

## ![](/emulators/external-links.png) Enlaces externos

* **Código fuente utilizado:** [https://github.com/libretro/gambatte-libretro/](https://github.com/libretro/gambatte-libretro)
* **Documentación Libretro** : [https://docs.libretro.com/library/gambatte/](https://docs.libretro.com/library/gambatte/)
* **Código fuente oficial:** [https://github.com/sinamas/gambatte](https://github.com/sinamas/gambatte)
* **Antiguo código fuente standalone :** [https://sourceforge.net/projects/gambatte/files/gambatte/](https://sourceforge.net/projects/gambatte/files/gambatte/)