---
title: Libretro Same_cdi
description: 
published: true
date: 2024-07-24T09:57:14.281Z
tags: libretro, cdi, 9.1+, same_cdi
editor: markdown
dateCreated: 2023-06-27T18:30:33.929Z
---

**Libretro-same_cdi** est un core dérivé de MAME. La partie same signifie S(ingle) A(rcade) M(achine) E(mulator) (émulateur simple de machine arcade).

## ![](/emulators/license.svg) Licence

Ce core est sous licence **[GPLv2](https://github.com/libretro/same_cdi/blob/master/COPYING)**.

## ![](/emulators/compatibility.png) Compatibilité

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ❌ | ❌ | ❌ | ❌ | ❌ | ❌ | ✅ | ❌ |

## ![](/emulators/features.png) Fonctionnalités

| Fonctionnalité | Supporté |
| :---: | :---: |
| Redémarrage | ✔ |
| Captures d'écran | ✔ |
| Sauvegardes | ✔ |
| Options du core | ✔ |
| Contrôles | ✔ |
| Remappage | ✔ |

## ![](/emulators/bios.svg) BIOS

### Liste des bios obligatoires

| Nom de fichier | Description | MD5 | Fourni |
| :---: | :---: | :---: | :---: |
| cdibios.zip |  | 9fd6b277cd877d51426427a33d1d19bb | ❌ |
| cdimono1.zip | CD-i (Mono-I) (PAL) | ce590c4787460161359f2b680136cb60 bbe6636ac11d3744f0710e8e820187e7 | ❌ |
| cdimono2.zip |  | 2ac8cd5bb23287e022ca17d616949255 | ❌ |

### Emplacement

Placez les bios comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 cdi
┃ ┃ ┃ ┃ ┣ 🗒 **cdibios.zip**
┃ ┃ ┃ ┃ ┣ 🗒 **cdimono1.zip**
┃ ┃ ┃ ┃ ┣ 🗒 **cdimono2.zip**

## ![](/emulators/isos.svg) Isos

### Extensions supportées

Les isos doivent avoir les extensions suivantes :

* .chd
* .cue
* .iso

### Emplacement

Placez les isos comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 cdi
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.chd**

>Les isos au format **Redump** sont vivement conseillées.
{.is-success}

>Pour plus d'information sur les roms, rendez-vous sur [ce tutoriel](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuration avancée de l'émulateur

>Pour pouvoir conserver vos configurations personnalisées lors d'une mise à jour, nous vous conseillons d'utiliser notre fonctionnalité [Surcharges de configuration](./../../../advanced-usage/configuration-override).
{.is-info}

### Accéder aux options

Vous pouvez configurer diverses options de deux façons différentes.

* Via le menu RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via le fichier `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Options du core

| Options | Valeurs des options | Variable (pour surcharge) | Valeurs des variables |
| --- | --- | --- | --- |
| Read configuration | `Désactivé` ✅ / `Activé` | `same_cdi_read_config` | `disabled` / `enabled` |
| Write configuration | `Désactivé` ✅ / `Activé` | `same_cdi_write_config` | `disabled` / `enabled` |
| Save state naming | `game` ✅ / `system` | `same_cdi_saves` | `game` / `system` |
| NVRAM Saves per game | `Activé` ✅ / `Désactivé` | `same_cdi_nvram_saves` | `enabled` / `disabled` |
| Auto save/load states | `Désactivé` ✅ / `Activé` | `same_cdi_auto_save` | `disabled` / `enabled` |
| Enable in-game mouse | `Désactivé` ✅ / `Activé` | `same_cdi_mouse_enable` | `disabled` / `enabled` |
| Lightgun mode | `none` ✅ / `touchscreen` / `lightgun` | `same_cdi_lightgun_mode` | `none` / `touchscreen` / `lightgun` |
| Profile Buttons according to games (Restart) | `Activé` ✅ / `Désactivé` | `same_cdi_buttons_profiles` | `enabled` / `disabled` |
| Enable throttle | `Désactivé` ✅ / `Activé` | `same_cdi_throttle` | `disabled` / `enabled` |
| Enable cheats | `Désactivé` ✅ / `Activé` | `same_cdi_cheats_enable` | `disabled` / `enabled` |
| Main CPU Overclock | `default` ✅ / `30` / `31` / `32` / `33` / `34` / `35` / `36` / `37` / `38` / `39` / `40` / `41` / `42` / `43` / `44` / `45` / `46` / `47` / `48` / `49` / `50` / `51` / `52` / `53` / `54` / `55` / `60` / `65` / `70` / `75` / `80` / `85` / `90` / `95` / `100` / `105` / `110` / `115` / `120` / `125` / `130` / `135` / `140` / `145` / `150` | `same_cdi_cpu_overclock` | `default` / `30` / `31` / `32` / `33` / `34` / `35` / `36` / `37` / `38` / `39` / `40` / `41` / `42` / `43` / `44` / `45` / `46` / `47` / `48` / `49` / `50` / `51` / `52` / `53` / `54` / `55` / `60` / `65` / `70` / `75` / `80` / `85` / `90` / `95` / `100` / `105` / `110` / `115` / `120` / `125` / `130` / `135` / `140` / `145` / `150` |
| Alternate render method | `Désactivé` ✅ / `Activé` | `same_cdi_alternate_render` | `disabled` / `enabled` |
| Boot from CLI | `Désactivé` ✅ / `Activé` | `same_cdi_boot_from_cli` | `disabled` / `enabled` |
| Resolution | `640x480` ✅ / `640x360` / `800x600` / `800x450` / `960x720` / `960x540` / `1024x768` / `1024x576` / `1280x960` / `1280x720` / `1600x1200` / `1600x900` / `1440x1080` / `1920x1080` / `1920x1440` / `2560x1440` / `2880x2160` / `3840x2160` | `same_cdi_altres` | `640x480` / `640x360` / `800x600` / `800x450` / `960x720` / `960x540` / `1024x768` / `1024x576` / `1280x960` / `1280x720` / `1600x1200` / `1600x900` / `1440x1080` / `1920x1080` / `1920x1440` / `2560x1440` / `2880x2160` / `3840x2160` |
| MAME INI Paths | `Désactivé` ✅ / `Activé` | `same_cdi_mame_paths_enable` | `disabled` / `enabled` |
| MAME Joystick 4-way simulation | `disabled` ✅ / `4way` / `strict` / `qbert` | `same_cdi_mame_4way_enable` | `disabled` / `4way` / `strict` / `qbert` |

## ![](/emulators/external-links.png) Liens externes

* **Code source utilisé** : [https://github.com/libretro/same_cdi/](https://github.com/libretro/same_cdi/)
* **Documentation Libretro** : [https://docs.libretro.com/library/same_cdi/](https://docs.libretro.com/library/same_cdi/)