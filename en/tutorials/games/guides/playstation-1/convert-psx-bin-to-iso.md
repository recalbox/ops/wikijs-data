---
title: Convert .BIN image discs to .ISO
description: 
published: true
date: 2021-08-08T14:20:13.460Z
tags: ps1, convert, bin, iso
editor: markdown
dateCreated: 2021-08-08T14:05:23.188Z
---

## Introduction

Here we will develop a solution to get only one .ISO file per game. All games work on this model provided that the downloaded .BIN files also work.

>A recurrent problem with roms for PSX is that the files are more often in .BIN and .CUE formats than in .ISO, and sometimes roms in .BIN.ECM format are still available.
>
>One of the disadvantages of these files is that they are often several, 2 or 3 files or even sometimes dozens. This can distort the quality of the presentation in the EmulationStation interface (you can end up with several files per game, and the total number of games for this system is also distorted).  
>  
>This display problem under EmulationStation occurs either :
>
>* If you don't scrape your roms.
>* If you have misconfigured your scraping software.
>
>However, .BIN and .CUE roms are functional too.
{.is-info}

First of all, the ISO file is a binary disk image, which is the format we are interested in because the whole game is in one file.

### The BIN file

The .BIN file is also a disk image, it is a format very close to the ISO. The disadvantage is that the BIN files can be several, sometimes very numerous, and are generally accompanied by another file in CUE format.

### The CUE file

The .CUE file is a small text file that accompanies .BIN files. It can be opened with any text editor, its only purpose is to help index the BIN files it accompanies. To tell the emulator that will read it, where the BIN files are, what their names are, their order, and sometimes the different audio tracks contained.

### The .BIN.ECM file

The .BIN.ECM file is rarer, it is the result of a compressed BIN file.   
We will see in the last part how to decompress it.

---

The first remark is that often, a game is composed of a BIN file accompanied by a CUE file. In this case, the CUE file (text), which has the role of indexing the various BIN files that accompany it, is no longer useful. Users have understood this, in fact, in this case, simply delete the CUE file, and then the BIN file is autonomous (like an ISO), you can leave it as is, it works.

You can learn to tell the difference by looking at them systematically, you also have the possibility to convert all your roms to have no problem.

The second remark is to never rename the BIN files that we have just seen.
Indeed, if you open a CUE file with a text editor, you will see that it lists all the .BIN files that it accompanies. If you rename the BIN files, then it doesn't match anymore.

In this case you can rename all the BINs in the CUE file, but this is a step that is easier to avoid by not renaming the files. Once you have obtained an ISO file you will have the freedom to rename it as you wish.

## Converting a BIN file to ISO

We will now approach the conversion of `.BIN` files to `.ISO`. To do this we will use the software [**IsoBuster**](http://www.isobuster.com/fr/), a free demo version.

* Open IsoBuster.
* Go to the `File` menu and click on `Open Image File`.
* Go to the location of the game you want to convert to BIN/CUE format.
* Since you can't select multiple `.BIN` files to incorporate into IsoBuster, the trick is to use the `.CUE` file, which in turn indexes all `.BIN` files that make up the set. This example is not interesting because there is only one `.BIN` file, but the principle would be the same with 3 or 30 `.BIN` files: you use the `.CUE` file.

If your rom is in 1 BIN file and you have deleted the .CUE file, no worries, it works by selecting the .BIN file directly.

>Special cases can occur as with Rayman, composed of _Rayman.bin_ and _Rayman.cue_.
>
>The `.CUE` file is much more complete and also indexes the audio tracks of the game. Not all `.CUE` files are as complete, but it is a good habit to always work with the `.CUE` file.
{.is-warning}

>Here is [an archive](https://www.google.fr/search?q=cue_and_sbis) containing the CUE files for all PSX roms.
{.is-info}

* In the left-hand column, right-click on the root [CD], then, in the menus, click on **Extract CD** and click on **Raw Data (`.BIN`, `.ISO`)**.
* You can give your export file a name, the name of the desired game.  Usually at this stage in programs, if you don't put a file extension (`.ISO`) but check that it is selected `.ISO` underneath, your file will come up as `.ISO` With IsoBuster this is not the case, your file will have no file extension if you don't write it down. You can add it later but you might also get lost, it is easier to put the `.ISO` extension in the file name.
* The conversion takes a few seconds.
* If you have selected a `.CUE`, click `No`. Anyway, being used in the software, it can't be overwritten.

You have your game in ISO format, which will have been extracted alongside the original `.BIN` and `.CUE` files.

## Compressed BIN (BIN.ECM)

And we'll finish with the rarer case of the compressed BIN, in `.BIN.ECM` format.
To remove the `.ECM` extension to obtain a `.BIN`, you have to use a decompression program. For this, there are several methods:

## {.tabset}
## Windows
* Download the program `Un-ECM.exe` [on this page](https://archive.org/details/ECMToolsV1.0).
* Extract the Un-ECM.exe program into a directory of the same name among your programs, and remember its installation path.
* Right click on your `.BIN.ECM` file and choose `Open with`.
* In the list of programs offered, scroll down, and click on `Find another application on this PC` and open it with the `Un-ECM.exe` program.
* You can check the `Always open with this application` box, so that in future, double-clicking on a .BIN.ECM file will automatically unzip the BIN file into that same directory.

### macOS
* With Homebrew, you can install `ecm` and `unecm` via [SSH](./../../../system/access/root-access-terminal-cli):

`brew install ecm`

* To compress :

`ecm game.bin`

* To decompress:

`unecm game.bin.ecm`

### Linux
* There are command line methods, where the method is to drag the files into the terminal. You can find appropriate tutorials by searching for the terms "BIN.ECM" and "Un-ECM" + the name of your operating system.

The resulting BIN file is then usable as is or you can also convert it to ISO as discussed above.