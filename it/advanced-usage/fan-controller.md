---
title: Controllo della ventola
description: gestione dei ventilatori sui RPIs
published: true
date: 2024-06-22T06:20:27.774Z
tags: 
editor: markdown
dateCreated: 2024-06-22T06:20:27.774Z
---

# Descrizione

L'utilità **recalbox-wpaf** permette di controllare alcune schede (*hat*) dotate di una ventola.
L'elenco delle schede supportate da recalbox-wpaf è disponibile [qui](./../hardware-compatibility/compatible-devices/rpi-hats).

Controllo della ventola
=======================

L'utilità **recalbox-wpaf** permette di controllare alcune schede (*hat*) dotate di una ventola.
L'elenco delle schede supportate da recalbox-wpaf è disponibile [qui](./../hardware-compatibility/compatible-devices/rpi-hats).

Configurazione
--------------

La configurazione viene effettuata nel file `recalbox.conf` sotto le chiavi `hat.wpaf.enabled` e `hat.wpaf.board`.

`hat.wpaf.enabled`
------------------

Imposta questa chiave su `1` per abilitare l'utilità recalbox-wpaf.

`hat.wpaf.board`
----------------

Questa chiave permette di selezionare la scheda che equipaggia il tuo Raspberry PI. I valori possibili sono:

* `wspoehatb` per la scheda Waveshare PoE hat (b),
* `argonforty` per la scheda che equipaggia il case Argon One,
* `piboy` per il case di Experimental PI,
* `rpipoeplus` per la scheda Raspberry PI PoE+,
* `fanshim` per la scheda Pimoroni fan SHIM.

Esempio:

```ini
hat.wpaf.enabled=1
hat.wpaf.board=rpipoeplus
```

Riavvia per rendere effettive le impostazioni.
