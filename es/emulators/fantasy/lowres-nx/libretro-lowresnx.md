---
title: Libretro LowResNX
description: 
published: true
date: 2024-07-14T19:16:30.822Z
tags: libretro, lowresnx, 8.0+
editor: markdown
dateCreated: 2023-09-11T06:13:19.311Z
---

**Libretro LowResNX** es una adaptación Libretro del emulador **LowRes NX**.

## ![](/emulators/license.svg) Licencia

El core posee la siguiente licencia  [**Zlib**](https://github.com/timoinutilis/lowres-nx/blob/master/LICENSE).

## ![](/emulators/compatibility.png) Compatibilidad

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | 
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Funcionalidades

| Funcionalidad | ¿Soportada? |
| :---: | :---: |

## ![](/emulators/bios.svg) BIOS

>**No necesita bios.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Extensiones de roms soportadas

Las roms deben tener las extensiones siguientes:

* .nx
* .zip
* .7z

Este sistema soporta las roms comprimidas en formato .zip/.7z. No obstante, se trata sólo de un archivo.

Los ficheros contenidos en los .zip/.7z deben tener las extensiones citadas previamente.
Además, cada fichero .zip/.7z sólo debe contener un fichero rom al interior.

### Emplazamiento

Disponed vuestras roms como en la siguiente imagen: 

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 lowresnx
┃ ┃ ┃ ┃ ┣ 🗒  **jeu.nx**

## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

>**Este core no tiene opciones.**
{.is-success}

## ![](/emulators/external-links.png) Enlaces externos

* **Código fuente utilizado** : [https://github.com/timoinutilis/lowres-nx/](https://github.com/timoinutilis/lowres-nx/)
* **Página oficial** : [https://lowresnx.inutilis.com/](https://lowresnx.inutilis.com/)
* **Documentación oficial** : [https://lowresnx.inutilis.com/help.php](https://lowresnx.inutilis.com/help.php)