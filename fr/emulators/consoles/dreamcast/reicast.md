---
title: Reicast
description: 
published: true
date: 2024-06-22T23:47:09.073Z
tags: dreamcast, reicast
editor: markdown
dateCreated: 2021-05-21T08:20:17.642Z
---

**Reicast** est un émulateur capable d'émuler la **Dreamcast**.

## ![](/emulators/license.svg) Licence

Ce core est sous licence [**BSD-3**](https://github.com/reicast/reicast-emulator/blob/alpha/LICENSE).

## ![](/emulators/compatibility.png) Compatibilité

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ❌ | ✅ | ❌ | ❌ | ❌ | ❌ | ❌ | ❌ |

## ![](/emulators/features.png) Fonctionnalités

| Fonctionnalité | Supporté |
| :---: | :---: |
|  |  |

## ![](/emulators/bios.svg) BIOS

### Liste des bios obligatoires

| Nom de fichier | Description | MD5 | Fourni |
| :---: | :---: | :---: | :---: |
| dc_boot.bin | BIOS Dreamcast | e10c53c2f8b90bab96ead2d368858623 d552d8b577faa079e580659cd3517f86 d407fcf70b56acb84b8c77c93b0e5327 | ❌  |
| dc_flash.bin | Date / Heure / Langue | d6e11a23f1fa01cddb5dfccf7e4cc8d7 93a9766f14159b403178ac77417c6b68 0a93f7940c455905bea6e392dfde92a4 23df18aa53c8b30784cd9a84e061d008 69c036adfca4ebea0b0c6fa4acfc8538 74e3f69c2bb92bc1fc5d9a53dcf6ffe2 2f818338f47701c606ade664a3e16a8a | ❌  |

### Emplacement

Placez les bios comme ceci :

┣ 📁 recalbox  
┃ ┣ 📁 share  
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 dc
┃ ┃ ┃ ┃ ┣ 🗒 **dc_boot.bin**  
┃ ┃ ┃ ┃ ┣ 🗒 **dc_flash.bin**  

## ![](/emulators/isos.svg) Isos

### Extensions supportées

Les isos doivent avoir les extensions suivantes :

* .bin/.cue
* .bin/.gdi
* .cdi
* .chd (format v4 !)

### Emplacement

Placez les isos comme ceci : 

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 dreamcast
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.gdi**
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.bin**

>Les isos au format **TOSEC** sont vivement conseillés.
{.is-success}

>Pour plus d'information sur les isos, rendez-vous sur [ce tutoriel](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuration avancée des émulateurs

>Pour pouvoir conserver vos configurations personnalisées lors d'une mise à jour, nous vous conseillons d'utiliser notre fonctionnalité [Surcharges de configuration](./../../../advanced-usage/configuration-override).
{.is-info}

## ![](/emulators/external-links.png) Liens externes

* **Code source utilisé :** [https://github.com/reicast/reicast-emulator/](https://github.com/reicast/reicast-emulator/)
* **Documentation** : [https://reicast.com/guide/](https://reicast.com/guide/)