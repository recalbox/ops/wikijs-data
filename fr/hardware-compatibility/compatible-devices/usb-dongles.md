---
title: Dongles USB
description: 
published: true
date: 2024-09-06T10:07:59.225Z
tags: usb, dongles, compatibilité
editor: markdown
dateCreated: 2021-05-21T15:36:32.696Z
---

| Périphérique | État | Chipset | Kernel Linux | Dernière version testée | Commentaires |
| :---: | :---: | :---: | :---: | :---: | :---: |
| Asus USB-BT400 Bluetooth 4.0 | ✅ |  |  |  | Fonctionne après avoir apporté beaucoup de personnalissations au firmware Broadcom - ne fonctionne pas par défaut. |
| AZiO BTD-V401 Bluetooth 4.0 | ✅ |  |  |  |  |
| AZiO BTD-V201 USB 2.0 Micro Bluetooth Adapter | ✅ |  |  |  |  |
| BCM2046B1 | ✅ |  |  |  |  |
| Belkin Mini Bluetooth v4.0 Adapter | ✅ |  | 4.0+ |  | Ne fonctionne pas dans Recalbox sur XU4 à cause de la version 3.10 du kernel. |
| BigTec BIG121 | ❌ |  |  |  |  |
| Billionton USBBT02-B-DX | ✅ |  |  |  |  |
| Bluetooth Adapter USB 5.0 + EDR (Amazon) | ✅ | Realtek 8761B | 8.0.+ | v8.0.1 - rpi 2B | Dongle BT 5.0 avec une portée pouvant aller jusqu'à 20m. |
| Cambridge Silicon Radio ‪Bluetooth | ✅ |  | 3.0+ | v4.0.0 - rpi2B |  |
| CLiPtec USB Bluetooth Dongle Ver. 4.0 + EDR RZB939 | ✅ |  |  |  |  |
| CSL - V4.0 USB nano Adaptateur Bluetooth avec DEL Class 4.0 | ✅ |  |  |  |  |
| CSR Ltd Bluetooth Dongle (HCI mode) | ❌ |  |  |  |  |
| D-Link DBT-122 Dongle Bluetooth 1.2 | ✅ |  |  |  |  |
| Digitus Adapter tiny USB Bluetooth V 4.0 | ✅ |  |  |  |  |
| Dongle Bluegiga Technologies BLED112-V1 4.0 0dBm | ❌ |  |  |  |  |
| EMTEC USB Bluetooth dongle 100m | ✅ |  |  |  |  |
| Energy Sistem 1550T | ✅ |  |  |  |  |
| Essentiel B Micro dongle bluetooth 2.0 | ❌ |  |  |  |  |
| ESSENTIEL B Bluetooth  USB Bluetooth 4.0 | ✅ |  |  |  |  |
| Generic Bluetooth 4.0 dongle / adapter. USB 2.0 | ✅ |  |  |  | Ne fonctionne pas dans Recalbox sur XU4 à cause de la version 3.10 du kernel. |
| ieGeek® Mini USB Bluetooth CSR V4.0 Dongle Dual Mode | ✅ |  |  |  |  |
| IOGEAR Bluetooth 4.0 USB Micro Adapter (GBU521) | ✅ |  |  |  |  |
| Inateck (BTA-BC4B6) Bluetooth 4.0+EDR | ✅ |  |  |  |  |
| Inateck USB Bluetooth 4.0 (B00N2E3ZHG) | ✅ |  |  |  |  |
| Insignia USB Bluetooth Adapter NS-PCY5BMA2 | ✅ |  |  |  | Fonctionne sur la version 4.1 avec une manette PS3.  |
| IT Works (DONGLE BT-150) USB Bluetooth 4.0 | ✅ |  |  |  |  |
| Lindy 52213 Dongle USB 3.0 WiFi Bluetooth | ✅ |  |  |  |  |
| LogiLink BT0006A | ❌ |  |  |  |  |
| Logilink BT0007A | ✅ |  |  |  | Avec la manette officielle PS3 DualShock 3, la capture du signal Bluetooth sembre être trop courte |
| Maxxtro Bluetooth 4.0 USB adapter micro | ✅ |  |  |  |  |
| Nano USB to Bluetooth Dongle V2.0 (dynamode) | ❌ |  |  |  |  |
| ORICO BTA-402 USB Bluetooth 4.0 Micro Adapter (Model X) | ✅ |  |  |  |  |
| ORICO BTA-403 USB Bluetooth 4.0 | ✅ |  |  | Raspberry pi 3 | Testé avec la manette PS3 cechzc2ua1 |
| PANRICH PBT06H | ✅ |  |  |  |  |
| Plugable Technologies USB-BT4LE | ✅ |  |  |  |  |
| Sitecom CN-512 v1 001 | ✅ |  |  |  |  |
| Sitecom CN-516 | ✅ |  |  | raspberry pi 3. V 4.0.2 | Testé avec la manette 8bitdo zero |
| SoundBot SB340 | ✅ |  |  |  |  |
| Speedlink SL-7409-BK | ✅ |  | 4.0+ - 8.0+ | 8.0.1 - rpi 2B | BT 2.1, le dongle fonctionne mais a une faible portée |
| TP-Link UB400 | ✅ |  |  | 9.1 | Testé avec un pc x86-64 |
| TP-Link UB500 | ✅ | Realtek 8761B |  | 9.2.3 | Testé avec un pi5 |
| TP-Link UE300(UN) USB3.0->Gigabit| ✅ |  |  | 9.2-Patron 3 | Testé avec un pc/mac x86-64 |
| Trendnet TBW-102UB | ✅ |  |  |  |  |
| Trendnet TBW-107UB | ✅ |  |  | 4.0.0-beta4 | Problèmes de latence |
| Trust Ultra Small Bluetooth 2 USB Adapter 10m BT-2400p (15542) | ✅ |  |  | 4.0.0-beta |  |
| Trust nano Bluetooth universal 4.0 Adapter | ✅ |  |  |  |  |
| VIVANCO USB Bluetooth Dongle v4.0 (30447) | ✅ |  |  |  |  |