---
title: Utilización de vuestro periférico XArcade
description: 
published: true
date: 2023-09-19T09:32:54.997Z
tags: xarcade
editor: markdown
dateCreated: 2023-09-19T09:32:54.997Z
---

## Dispositivos compatibles y requisitos

Los siguientes sticks son compatibles:

* X-Arcade Model Tankstick
* X-Arcade Dual Stick

## Utilización del stick XArcade en los Raspberry Pi 4

En los Raspberry Pi 4, el binario del stick XArcade no es correcto. Para utilizar el stick XArcade en Raspberry Pi 4, es necesario ejecutar algunos comandos a través de [SSH](./../../system/access/root-access-terminal-cli) y montar la [partición del sistema](./../../system/access/remount-partition-with-write-access) en modo lectura/escritura. Los comandos son los siguientes:

```texto
cp /usr/bin/xarcade2jstick /usr/bin/xarcade2jstick-viejo
cp xarcade2jstick-rpi4 /usr/bin/xarcade2jstick
chmod 755 /usr/bin/xarcade2jstick
```