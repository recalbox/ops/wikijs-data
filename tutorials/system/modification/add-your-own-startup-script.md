---
title: Add your own startup script
description: 
published: true
date: 2023-04-12T17:10:48.499Z
tags: startup, script, add
editor: markdown
dateCreated: 2021-08-06T09:26:46.813Z
---

## Introduction

Follow these instructions if you want to create your own startup script that will be executed automatically when Recalbox is started.

The startup procedure of a script will be executed at startup and the shutdown procedure at shutdown of Recalbox.

## Usage

You must create a file named `custom.sh` which must be located in `/recalbox/share/system`.

This file can contain:

* Shell code
* Can call other files such as Python files for example

## Test

To test if everything works, run the command `/etc/init.d/S99custom start`.