---
title: Jugar a varios en la Wii
description: 
published: true
date: 2023-09-20T09:14:40.974Z
tags: wii
editor: markdown
dateCreated: 2023-09-20T09:14:40.974Z
---

>Lo que sigue se aplica a todos los mandos exceptos los Wiimotes que funcionan por defecto.
{.is-info}

* Abrid el fichero [recalbox.conf](./../../../../basic-usage/getting-started/recalboxconf-file).
* Buscad la siguiente sección dentro del fichero:

```ini
## Wiimotes
## Real wiimotes must not be paired with recalbox system so that they can work with the wii emulator
## set emulatedwiimotes to 1 to emulate wiimotes with standard pads
wii.emulatedwiimotes=0
```

* Añadid a continuación la siguiente línea:

```ini
wii.configfile=dummy
```

A partir de este momento, podréis jugar a varios con el emulador de la Wii utilizando vuestros mandos.