---
title: Amiga CD32
description: 
published: true
date: 2025-01-05T23:47:17.933Z
tags: amiga-cd32, consoles, amiga, commodore, cd32
editor: markdown
dateCreated: 2021-07-27T18:42:28.876Z
---

![](/emulators/consoles/amigacd32.svg){.align-center}

## Technical data

* **Manufacturer**: Commodore International
* **Year of release**: 1993
* **OS**: AmigaOS 3.1
* **CPU**: Motorola 68EC020 @ 14.32 MHz (NTSC), 14.18 MHz (PAL)
* **RAM**: 2 MB
* **ROM**: 1 MB Kickstart with CD32 firmware, 1 KB EEPROM for save games
* **GPU**: Advanced Graphics Architecture
* **Sound chip**: 4 x 8-bit PCM channels
* **Resolution**: 320x200 to 1280x400i (NTSC), 320x256 to 1280x512i (PAL)

## Presentation

The **Amiga CD32** is a fifth generation video game console developed by Commodore and released in September 1993. The CD32 is a 32-bit console whose architecture is based on the **Amiga 1200** computer.

The **Amiga CD32** is the first 32-bit to be released in the West (the FM Towns Marty preceded it in Japan). Like the **3DO** or the **Atari Jaguar**, released in the same period, the CD32 did not manage to break through on the market despite the support of the fans of the **Amiga** range. Its technical limitations, its bitplan display mode greatly limiting it in the display of textured 3D games like **Doom** in vogue at the time, the lack of involvement of the developers and the financial difficulties of Commodore are all explanations put forward for its failure.

## Emulators

[Amiberry](amiberry)
[Libretro PUAE](libretro-puae)
[Libretro UAE4ARM](libretro-uae4arm)