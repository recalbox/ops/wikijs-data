---
title: ¡Bienvenido a la wiki de Recalbox!
description: ¿Cómo empiezo a usar Recalbox? ¿De qué va todo esto?
published: true
date: 2023-09-26T07:21:50.944Z
tags: home, recalbox, primer arranque, como hacer, bienvenido, inicio
editor: markdown
dateCreated: 2021-07-28T12:18:47.035Z
---

![welcome.png](/home/welcome.png)

## ¿Qué es Recalbox?

[Recalbox](https://www.recalbox.com) es **LA consola de juegos retro definitiva** que os permite volver a jugar a todos los juegos, consolas y ordenadores de vuestra infancia.

Con [Recalbox](https://www.recalbox.com), (re)descubrid y volved a jugar a los títulos que han marcado la historia de los videojuegos, ¡de la forma más sencilla posible!

[Recalbox](https://www.recalbox.com) puede instalarse fácilmente en nanoordenadores muy baratos como el Raspberry Pi 4, en consolas portátiles como el Odroid Go Super, o en cualquier ordenador tipo PC (reciente o antiguo).

## Quiero instalar Recalbox, ¿cómo lo hago?

Para instalar Recalbox en vuestro mini-ordenador/pc, consultad la página [preparación e instalación ](./basic-usage/preparation-and-installation)

Para obtener más información sobre los sistemas y emuladores compatibles, consultad la sección [compatibilidad de hardware](./hardware-compatibility)

## ¿Cómo buscar una página en particular?

Utilizad la función de **búsqueda** en la parte superior de esta misma página.

Sólo tenéis que introducir la búsqueda en el cuadro de texto:
![search-text.png](/home/search-text.png)

O buscad las páginas usando etiquetas:
![search-tags.png](/home/search-tags.png)

## Soy principiante, ¿por dónde empiezo?

* Os aconsejamos consultar la siguiente documentación:
  * [Uso básico](./basic-usage), para entender **los fundamentos del funcionamiento de** [Recalbox](https://www.recalbox.com).
 * [Las F.A.Q](./faq).
  * [Compatibilidad de los emuladores](./hardware-compatibility/emulators-compatibility)
  * [Compatibilidad de dispositivos](./hardware-compatibility/compatible-devices).
  * También podéis consultar la sección [emuladores](./emulators) para obtener más información sobre cada uno de ellos.
  * Si algunos términos os parecen demasiado técnicos, el [glosario](/basic-usage/glossary) puede ser un valioso aliado.
* Para un uso más avanzado de Recalbox:
  * Consultad la sección [tutoriales](./tutorials).
  * Visitad la sección [uso-avanzado](./advanced-usage).
* Para obtener información adicional, consultad tambien:
  * La pagina oficial de [Recalbox](https://www.recalbox.com/fr/).
  * El [foro](https://forum.recalbox.com/).
  * Entrad en nuestro [Discord](https://discord.gg/NbQFbGM).

## ¿Tenéis algún consejo para los nuevos usuarios?

[Recalbox](https://www.recalbox.com) es un proyecto de **código abierto** y, por tanto,  **gratuito**.

Como tal, las personas que trabajan en él lo hacen en su tiempo libre, y el proyecto y su documentación evolucionan a medida que la gente contribuye. 

**El mejor consejo** que podemos daros es:

* Sed curiosos.
* Leed la documentación.
* Leed los [tutoriales](./tutorials).
* Participad en la redacción y traducción de esta wiki (consultad [cómo contribuir](./contribute))
* Buscad en el [foro](https://forum.recalbox.com/).
* Haced siempre una copia de seguridad de vuestra carpeta **SHARE** antes de hacer cualquier cosa de la que no estéis seguros.