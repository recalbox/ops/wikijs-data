---
title: Restauration des paramètres d'usine
description: 
published: true
date: 2024-11-11T05:29:18.223Z
tags: 7.2+, restaurer, paramètres, usine
editor: markdown
dateCreated: 2021-05-21T07:53:43.100Z
---

## Présentation

Une option nommée `Retour aux paramètres d'usine` permet de remettre l'installation à neuf. Cette fonctionnalité évite le travail manuel quelque peu fastidieux d'une réinstallation. 
>Cette option **ne touche pas** aux bios, jeux et sauvegardes !
{.is-success}

## Mise en œuvre 

Cette option se trouve dans EmulationStation. Pour cela, appuyer sur `START`, puis aller dans `PARAMÈTRES AVANCÉS` et sélectionner `RESTAURER LES PARAMÈTRES D'USINE`.

![](/basic-usage/features/resetfactory1.png){.full-width}

Une fois l'option sélectionnée, deux fenêtres de mise en garde s'affichent.

![](/basic-usage/features/resetfactory2.png){.full-width}

![](/basic-usage/features/resetfactory3.png){.full-width}

>Attention une fois ces deux fenêtres validées, **vous ne pourrez plus revenir en arrière**.
{.is-danger}

Un fois le processus de restauration des paramètres d'usine terminé, vous disposez alors d'un installation toute propre !