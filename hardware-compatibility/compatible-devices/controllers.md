---
title: Supported controllers
description: 
published: true
date: 2025-02-07T10:30:14.739Z
tags: compatibility, controllers
editor: markdown
dateCreated: 2021-08-05T21:42:23.699Z
---

Here you'll find a list of controllers that have been tested and approved by the community.

>Controllers that are not 8bitdo or unofficial from video game console manufacturers are **very unlikely** to be compatible, if at all. It's best to use official or 8bitdo controllers.
{.is-warning}

## Official Console Controllers

| Device | Wired | Wireless | Latest version tested | Comments |
| :---: | :---: | :---: | :---: | :---: |
| **MICROSOFT** |  |  |  |  |
| Xbox Controller from original Xbox | ✅ |  |  | ✅ With USB modification |
| Xbox 360 Controller | ✅ | ☑️ | 8.0.0 | ☑️ With Xbox 360 dongle |
| Xbox One Controller | ✅ | ☑️ | 8.0.0 | ☑️ With dongle like: [Xbox Wireless Adapter](https://www.xbox.com/en-US/accessories/adapters/wireless-adapter-windows), 8BitDo (wait 5s), Janswall,... |
| Xbox One Elite Controller | ✅ |  | 7.1.1 |  |
| Xbox One S Controller | ✅ |  ☑️ | 9.1.0 | ☑️ Not in BT, only with dongle: [Xbox Wireless Adapter](https://www.xbox.com/en-US/accessories/adapters/wireless-adapter-windows) |
| Xbox One Series X\|S Controller | ✅ | ☑️ | 9.1.0 | ☑️ Not in BT, only with dongle: [Xbox Wireless Adapter](https://www.xbox.com/en-US/accessories/adapters/wireless-adapter-windows) |
| **NINTENDO** |  |  |  |  |
| GameCube Controller | ✅ | ☑️ |  | ✅ Mayflash GameCube Controller Adapter for PC USB ☑️ With dongle: 8BitDo GBros Adaptator |
| GameCube WaveBird Controller | ❌ | ☑️ | 8.0.0 | ☑️ With dongle: 8BitDo GBros Adaptator (with a GameCube extension cable) or [GameCube to USB adapter](https://www.raphnet-tech.com/products/gc_usb_adapter_gen3/index.php) |
| NES Mini Controller | ✅ | ☑️ |  | ✅ With Mayflash USB Converter ☑️ With dongle: 8BitDo GBros Adaptator |
| SNES Mini Controller | ✅ | ☑️ |  | ✅ With Mayflash USB Converter ☑️ With dongle: 8BitDo GBros Adaptator |
| Switch JoyCon |  | ☑️ | 8.0.0 |  |
| Switch Pro Controller | ✅ | ☑️ | 8.0.0 |  ✅ Press ZL + ZR after detection |
| Wii Classic Controller | ✅ | ☑️ | v4.1 | ✅ With Mayflash USB Converter ☑️ Natively & With dongle 8BitDo GBros Adaptator |
| Wii Classic Pro Controller | ✅ | ☑️ |  | ✅ With Mayflash USB Converter ☑️ With dongle: 8BitDo GBros Adaptator |
| Wiimote | ✅ | ☑️ | 9.1.0 | ✅ With Mayflash Dolphin Bar |
| Wii U Pro Controller | ❌ | ☑️ | 4.1 | Natively & with dongle: 8BitDo, Janswall,...|
| SNES Controller for Nintendo Switch | ❌ | ☑️ | 8.0.0 |  |
| NES Controller for Nintendo Switch | ❌ | ☑️ | 9.2 | Drivers don't recognize them yet |
| Nintendo 64 Controller for Nintendo Switch | ❌ | ❌ | 8.0.0 | Drivers don't recognize them yet |
| Sega MegaDrive Controller for Nintendo Switch | ❌ | ❌ | 8.0.0 | Drivers don't recognize them yet |
| **SNK** |  |  |  |  |
| NeoGeo Mini Controller | ❌ |  |  | Some issues while using an USB-C adapter |
| **SONY** |  |  |  |  |
| PlayStation 1 Controller | ✅ |  | v 3.3beta 17 & v4.1.0-dev | With Generic USB adapter for 2 PS1/PS2 PAD controllers |
| PlayStation 1 DualShock Controller | ✅ |  | v 3.3beta 17 & v4.1.0-dev | With Generic USB adapter for 2 PS1/PS2 PAD controllers |
| PlayStation 2 DualShock 2 Controller | ✅ |  | v 3.3beta 17 & v4.1.0-dev | With Generic USB adapter for 2 PS1/PS2 PAD controllers |
| PlayStation 3 DualShock 3 | ✅ | ☑️ | 7.0 | ☑️ With dongle too, like: 8BitDo, Janswall,... or any bluetooth dongle |
| PlayStation 4 DualShock 4 | ✅ | ☑️ | 7.0.1 | ☑️ With Official Sony PS4 dongle & With dongle like: 8BitDo, Janswall,... |
| PlayStation 5 DualSense | ✅ |  | 7.0.1 | ✅ With [this](https://forum.recalbox.com/topic/8920/a-lire-manettes-ps4) |
| Playstation Classic Mini Controller | ✅ |  |  |  |

## Others controllers

| Device | Status | Latest version tested | Comments |
| :---: | :---: | :---: | :---: |
| **8BITDO** |  |  |  |
| 8bitdo controllers | ✅ | 8.0.0 | See the page [8bitdo on Recalbox](./../../hardware-compatibility/compatible-devices/8bitdo-on-recalbox) |
| **BIGBEN INTERACTIVE** |  |  |  |
| Pad Luxe PS3 | ✅ |  |  |
| PlayStation 3 BB4259 | ✅ |  |  |
| PlayStation 3 BB4401 | ✅ |  |  |
| Big Ben BB5033 (with 2.4GHz RF USB dongle) | ✅ | v17.11.10.2 |  |
| **BOWICK** |  |  |  |
| Wireless Bluetooth Controllers for PS3 | ✅ | v4.0.2 | Select `shawan` in the configuration |
| **BUFFALO** |  |  |  |
| iBuffalo SNES Classic USB Gamepad | ✅ |  | The buttons `Turbo` and `Clear` work |
| **CSL-COMPUTER** |  |  |  |
| Gamepad USB SNESw | ✅ |  |  |
| USB Wireless Gamepad (Model 2016) | ✅ |  |  |
| **EASYSMX** |  |  |  |
| EMS-9100 Gamepad USB Wired | ✅ |  | Has a home button for configuration (see attached manual) Requires Android 4.0 or higher |
| **FREE** |  |  |  |
| Freebox Controller | ✅ |  | Analog mode |
| **FUNLAB / PALPOW** |  |  |  |
| Firefly | ✅ |  | Wired and BT ok. The 2 special buttons `Turbo` and `Macro` works |
| **GAMESIR** |  |  |  |
| Gamesir G3s | ✅ | v4.0.2 | Pad can be configured in menu but behaves awkwardly in game |
| **HORI** |  |  |  |
| Fighting Commander | ✅ | v4.0.2 |  |
| Pokken Tournament for Wii U | ✅ | v4.0.2 |  |
| **KREEMA** |  |  |  |
| Gen Game S3 |  | v18.04.20 | Does not work properly with bluetooth connection (Wireless Bluetooth 3.0) |
| Gen Game S5 | ❌ | 17.13.02 | Can be paired, but is acting weird when configuring. Recognized as a keyboard, if both analog stick are pressed. In configuration, it doubles the pressed button for every entry. Does not work in a game, just in the main menu |
| **INNEXT** |  |  |  |
| IN-Y-D002-024\*2 | ✅ | v4.0.2 |  |
| **KONIX** |  |  |  |
| Konix PS3/PC | ✅ |  |  |
| KONIX KX-CT-PC | ✅ | v4.0.2 |  |
| **KROM** |  |  |  |
| Kay Pro Gaming Wired Gamepad (NXROMKEY) | ✅ |  | Detected as Xbox 360 Joypad, but impossible to map keys. |
| **LOGITECH** |  |  |  |
| Cordless Rumbleepad 2 (with 2.4GHz RF USB dongle) | ✅ |  |  |
| Dual Action Gamepad (USB) | ✅ | v4.0.0-beta4 |  |
| Precision Gamepad (USB) | ✅ | v4.0.0-beta4 |  |
| Rumblepad 2 (USB) | ✅ |  |  |
| **LOGITECH G** |  |  |  |
| F310 Gamepad (USB) | ✅ |  |  |
| F710 Wirelesss Gamepad (with 2.4GHz RF USB dongle) | ✅ | v4.0.0-beta4 | On top, turn the switch to D, not X. |
| **MADCATZ** |  |  |  |
| MadCatz Wireless FightPad for PlayStation 3 (with 2.4GHz RF USB dongle) | ✅ |  |  |
| **NACON** |  |  |  |
| GC-400ES Wired Controller | ✅ | 6.1.1 | [Seller](https://www.nacongaming.com/controllers/) (tested on v1, v2 and v3 ; recognized as a generic Xbox controller) |
| Revolution Wired | ✅ | v6.1.1 | [Seller](https://nacongaming.com/controllers/) (tested on v1, v2 and v3 ; recognized as a generic Xbox controller) |
| Nacon esport controllers/revolution wired et GC-400ES | ✅ |  | [Seller](https://www.nacongaming.com/controllers/) (tested on v1, v2 and v3 ; recognized as a generic Xbox controller) |
| **NGS** |  |  |  |
| Maverick (PS3/PC) | ✅ |  |  |
| Nvidia | ✅ | 9.2.2 | V01.04 |
| Shield 2017 (USB) | ❌ |  | Works fine in ES but take some times after launching a game. |
| **PDP** |  |  |  |
| PlayStation 3 Rock Candy (With bluetooth dongle) | ✅ |  |  |
| Xbox One & Windows 10 | ❌ | v18.04.20 | Detected but cannot be configured |
| PDP Afterglow Nintendo Switch wireless | ✅ | 8.0.2 |  |
| PDP Afterglow Xbox 360 | ✅ | 9.1.0 | wired with usb-adapter, translucid controller with blue or green leds |
| **POWERA** |  |  |  |
| MOGA PRO Power | ✅ | v17.11.02 |  |
| **RAZER** |  |  |  |
| Razer Sabertooth | ✅ | v4.0.0-beta2 |  |
| **RETRO GAMES** |  |  |  |
| The Amiga 500 Mini Gamepad | ✅ | 8.1.0 |  |
| **RETROUSB** |  |  |  |
| USB Super RetroPad (SNES) | ✅ |  |  |
| **SAITEK** |  |  |  |
| P380 | ✅ |  |  |
| P480 | ✅ |  |  |
| P880 | ✅ |  |  |
| **SLICKBLUE** |  |  |  |
| SlickBlue Wireless Bluetooth Game Pad Controller for Sony PlayStation 3 | ❌ |  |  |
| **SNAKEBYTE** |  |  |  |
| idroid:con (With bluetooth dongle) | ✅ | v3?3beta17 |  |
| blu:con (With bluetooth dongle) | ❌ | v4.0-rpi2-build15 |  |
| **SPEEDLINK** |  |  |  |
| Speedlink Strike FX - PS3 (câble USB) | ❌ | v4.0.0-rpi2B |  |
| Speedlink Torid Wireless Gamepad | ✅ | v4.0.0-rpi3 |  |
| **STEELSERIES** |  |  |  |
| SleetSeries Free Mobile Wireless | ✅ | v4.0.0-rpi3 | Require a bluetooth dongle |
| SteelSeries Stratus Wireless Gaming Controller | ❌ | v4.0.0-rpi3B | Bluetooth can be "paired" but is not detected afterwards |
| **SUBSONIC** |  |  |  |
| Subsonic Bluetooth Controller PS3 | ✅ |  | (gasia driver) |
| **THRUSTMASTER** |  |  |  |
| T-Wireless | ✅ | v17.11.10.2 |  |
| **VZTEC** |  |  |  |
| VZTEC USB Double Shock Controller Game Pad Joystick (VZ-GA6002) | ✅ | v4.0.0-beta3 |  |
| **XIAOMI** |  |  |  |
| Xiaomi Controller | ✅ |  | Need to pair after every disconnect |
| **UNKNOWN BRAND** |  |  |  |
| PlayStation 3 Afterglow Wireless Controller (with 2.4GHz RF USB dongle) | ✅ |  |  |
| PlayStation 3 GASIA Clone | ✅ |  |  |
| PlayStation 3 ShawnWan Clone | ✅ |  |  |
| Sony PlayStation Brand compatible DualShowk Pad Controller | ✅ | v3.3beta 17 & v4.1.0-dev | With Generic USB adapter for 2 PS1/PS2 PAD controllers |

## Controller adapters

| Device | Status | Latest version tested | Comments |
| :---: | :---: | :---: | :---: |
| **RALPHNET** |  |  |  |
| 4nes4snes | ✅ | 3.3.0 |  |
| **CRONUSMAX** |  |  |  |
| CronusMAX Adapter (Xbox 360 Out) | ✅ | v3.3beta17 |  |
| **MAYFLASH** |  |  |  |
| Mayflash Dolphin Bar | ✅ | 8.0.0 |  |
| Mayflash GameCube Controller Adapter |  | 6.1 | Only work on dolphin emulator for GameCube |
| Mayflash N64 Controller Adapter | ✅ | 4.0.0-dev |  |
| Mayflash NES/SNES Controller Adapter | ✅ | 4.0.0 | Need Usbhid |
| Mayflash PC052 Wii Classic Controller | ✅ | 4.1.0 | For wii classic, nes mini & snes mini controller |
| Mayflash Sega Saturn Controller Adapter | ✅ | 4.0.0 | Need Usbhid |
| **ORIGLAM** |  |  |  |
| Wii Sensor Bar USB | ❌ | 2018.x.x |  |
| **AUTRES** |  |  |  |
| PSP 1000 - 3000 with FuSa Gamepad and MiniUSB cable | ✅ | 4.0.1 | The Pi may not charge your PSP |
| Retrode |  | 3.2.11 | Need Usbhid |
| SFC/USB | ✅ | 4.0.0 |  |
| Manette Sony PlayStation 3 | ✅ | 4.0.0 |  |
| Trenro Dual PlayStation / PlayStation 2 to PC USB Controller Adapter | ✅ | v3.3beta17 & v4.1.0-dev | With Generic USB adapter for 2 PS1/PS2 PAD controllers |

## Arcade Stick

| Device | Status | Latest version tested | Comments |
| :---: | :---: | :---: | :---: |
| **8BITDO** |  |  |  |
| FC30 Arcade Joystick (USB only) | ✅ |  |  |
| NES30 Arcade Joystick (USB only) | ✅ |  |  |
| **DATEL** |  |  |  |
| Arcade Pro Joystick inc Rapid Fire for PS3 / Xbox 360 / PC | ✅ |  |  |
| **HORI** |  |  |  |
| Fighting Stick EX2 USB (Xbox 360 / PC) | ✅ |  |  |
| Fighting Stick MINI 3 pour PS3 | ✅ |  |  |
| Fighting Stick Mini 4 pour PS3 / PS4 | ✅ |  |  |
| Real Arcade Pro. 3 Fighting Stick | ✅ |  |  |
| Real Arcade Pro. 4 Kai (PS3 / PS4 / PC) | ✅ |  |  |
| HORI Real Arcade Pro. V (PS4) |  |  |  |
| Tekken 6 Wireless Arcade Stick PS3 | ✅ |  |  |
| **LIONCAST** |  |  |  |
| LIONCAST Arcade Fighting Stick (PS2 / PS3 / PC) | ✅ |  |  |
| **MADCATZ** |  |  |  |
| Arcade Fightstick Street Fighter V Tes+ (PS3 / PS4) | ✅ | 4.0.0-beta1 |  |
| Arcade Stick Fightstick Alpha (PS3 / PS4) | ✅ | 4.0.0-beta3 |  |
| Arcade Fightsitck Street Fighter IV Standard Edition for Xbox 360 | ✅ |  |  |
| Street Fighter IV Fightstick Round 1 Tournament Edition | ✅ |  |  |
| Street Fighter IV Round 2 Arcade Fightstick Tournament Editin | ✅ |  |  |
| Arcade Fightstick Tournament Edition 2 (For PS3 / PS4) | ✅ | 4.0.0-beta6 |  |
| **MAYFLASH** |  |  |  |
| Mayflash XBOX 360 / PS3 / PC Arcade Fighting Stick V2 | ✅ |  |  |
| Mayflash PS2 PS3 PC USB Universal Arcade Fighting Stick | ✅ |  |  |
| **PXN** |  |  |  |
| PXN - 0082 Arcade Joystick | ✅ |  |  |
| **QANBA** |  |  |  | 
| QamBa Q4 RAF Joystick Pro Fightstick Arcade 3in1 (Xbox 360 / PS3 / PC) | ✅ |  |  |
| **SANWA** |  |  |  | 
| JLF-TP-8YT (Sanwa) Arcade Joystick | ✅ |  |  |
| **TIME WARNER** |  |  |  |
| Mortal Kombat - Edition Ultimate (PS3) | ❌ |  |  |
| **VENOM** |  |  |  |
| Venom - Arcade Stick (PS3 / PS4) | ✅ |  |  |

## Joystick

| Device | Status | Latest version tested | Comments |
| :---: | :---: | :---: | :---: |
| **MADCATZ** |  |  |  |
| Cyborg F.L.Y 5 | ✅ | v18.04.20 |  |

## LightGun

| Device | Status | Latest version tested | Comments |
| :---: | :---: | :---: | :---: |
| **NINTENDO** |  |  |  |
| Wiimote | ✅ | 8.0.0 | With Mayflash USB Converter (Can be used as a Lightgun) |
| **ULTIMARC** |  |  |  |
| AimTrack Lightgun | ✅ | 2018.x.x |  |

## Interface USB

| Device | Status | Latest version tested | Comments |
| :---: | :---: | :---: | :---: |
| **AKISHOP** |  |  |  |
| PS360+ | ✅ |  |  |
| Zero Delay USB Encoder PC/PS3 controller |  | v 3.3beta 17 & v4.0.0-dev |  |
| Zero Delay USB Enconder Remix PC/PS3 controller | ✅ | v 3.3beta 17 & v4.0.0-dev |  |

## Arcade Push Buttons

| Device | Status | Latest version tested | Comments |
| :---: | :---: | :---: | :---: |
| **HALO** |  |  |  |
| Halo Button | ✅ |  |  |
|  |  |  |  |
| **PUSHBUTTON** |  |  |  |
| Pushbutton SL-CV | ✅ |  |  |
| **EG STARTS** |  |  |  |
| EG Starts Arcade Classic Kit | ✅ | 8.0.0 | ID 0079:0006 DragonRise Inc. PC TWIN SHOCK Gamepad |
