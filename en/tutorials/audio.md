---
title: 🔊 Audio
description: 
published: true
date: 2021-08-06T15:12:53.152Z
tags: tutorial, audio
editor: markdown
dateCreated: 2021-08-06T08:01:48.364Z
---

You will find here some tutorials about the audio outputs of your boards.

Here are the available tutorials:

[Analog audio output on Pi Zero](analog-audio-output-on-pi-zero)
[HifiBerry audio](hifiberry-audio)
[Rotary encoder via GPIO (digital volume control)](rotary-encoder-via-gpio-digital-volume-control)