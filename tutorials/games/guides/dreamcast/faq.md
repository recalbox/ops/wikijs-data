---
title: FAQ
description: 
published: true
date: 2021-10-16T11:13:14.909Z
tags: carte, dreamcast, faq, vmu, mémoire, multi-disques
editor: markdown
dateCreated: 2021-09-28T11:11:09.119Z
---

## Conversion des jeux

### Quelles sont les extensions de jeux utilisables avec la Dreamcast ?

Vous avez plusieurs extensions mais on va s'attarder sur les extensions suivantes :

* `.BIN`/`.CUE`
* `.CHD`
* `.GDI`

### Je souhaite convertir mes isos du format `.BIN`/`.CUE` vers `.CHD`, est-ce possible ?

Oui mais vous pourrez rencontrer des problèmes avec les émulateurs par la suite. En effet, le format Redump (`.BIN`/`.CUE`) est un format qui stocke plus d'informations que les données du jeu tel que les données ajoutées au moment de la création du Master Glass en usine, puis au pressage. Ces informations ne sont pas lues par les émulateurs et ne leur serviraient à rien.

Toutefois, le format Redump est taillé pour de la préservation pure et archivage, mais pas pour jouer.

Pour jouer, il est vivement recommandé d'utiliser le format TOSEC (`.GDI`). Ce format comprend les pistes et données lisibles par les lecteurs Dreamcast (Yamaha GD-ROM) et est entièrement compris par les émulateurs.

Le format `.CHD` (v4 et v5) a été conçu pour convertir du `.GDI` au format TOSEC uniquement.

Lui faire prendre le format Redump pose souci, car il n'est pas possible de 'recaler' les pistes simplement (sans repasser par des logiciels tierces, en virant des données et en décalant les positions inscrites dans le fichier `.CUE` vers un fichier `.GDI` + reformer les fichiers RAW) comme chez TOSEC.

Quand vous allez présenter un `.BIN`/`.CUE` converti vers du `.CHD`, l'émulateur ne saura pas comprendre l'origine du set, et attendra des pistes calées comme chez TOSEC.

Il n'y a pas de meilleur format entre les deux.

### Le mieux reste dont de convertir mes isos du format `.GDI` vers `.CHD` ?

Oui, c'est ce qui est le mieux à faire si vous souhaiter convertir vos isos au format `.CHD`.

## Jeux multi-disques

Sur **Dreamcast**, il existe **quelques jeux multi-disques**.

**Exemple** :

* _Resident Evil Code Veronica_
* _Skies Of Arcadia_
* _Grandia 2_
* _Etc..._

**Voici comment changer de disque** :

A la fin du disque 1 de votre jeu, le jeu propose une _sauvegarde_. On revient au menu avec **Hotkey** (sur Reicast ça suffit), puis on lance le deuxième disque **en récupérant sa sauvegarde**.

Rien de plus simple !

## Accéder au menu des cartes mémoires

### Objectif

L'objectif est de pouvoir modifier et manipuler le contenu des cartes mémoires. Il existe 2 façons de procéder.

### {.tabset}
#### Ejection du disque

* Lancez un jeu Dreamcast.
* Quand le jeu se joue, éjectez le disque avec `Hotkey` + `Stick gauche vers le haut`.
* La console va automatiquement arrêter le jeu et arriver sur le système. D'ici, vous pouvez accéder aux cartes mémoires.
* Pour reprendre le jeu, faites de nouveau `Hotkey` + `Stick gauche vers le haut`.

#### Disque vide

* Créez un fichier texte vide dans le dossier `/share/roms/dreamcast` que vous renommerez à votre convenance avec une extension `.cdi` (Dreamcast BIOS.cdi par exemple).
* Actualisez la liste de jeux dans Recalbox.
* Lancez le "jeu" et voilà !