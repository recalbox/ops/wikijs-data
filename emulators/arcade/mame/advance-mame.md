---
title: ​AdvanceMAME
description: 
published: true
date: 2024-07-24T08:00:51.526Z
tags: mame, advancemame
editor: markdown
dateCreated: 2021-07-05T23:34:50.261Z
---

**AdvanceMAME** is a standalone emulator, initially added for CRT configuration. It is based on the MAME 0.106 romset.

## ![](/emulators/license.svg) License

This core is under [**GPLv2**](https://github.com/amadvance/advancemame/blob/master/COPYING) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Features

| Feature | Supported |
| :---: | :---: |
| Saves |  |
| States |  |
| Rewind |  |
| Netplay |  |
| RetroAchievements |  |
| RetroArch Cheats |  |
| Native Cheats |  |
| Controllers |  |
| Multi-Mouse |  |
| Rumble |  |
| Sensors |  |
| Camera |  |
| Location |  |
| Subsystem |  |

## ![](/emulators/bios.svg) BIOS

BIOS Romsets are not required when using "Full Non-Merged" arcade Romsets. For "Split" and "Non-Merged" Romsets, place the BIOS in the same directory as the game Romset.

>**Note**:  
>
>Place the bios in the directory: `/recalbox/share/roms/mame/`   
>or in its subdirectory if you use several Mame cores: `/recalbox/share/roms/mame/AdvanceMame/`
>Games and their bios must be in the same directory or sub-directory.
{.is-warning}

## ![](/emulators/roms.png) Roms

* Based on romset: MAME 0.106 (May 2006)
* DatFile: You can find the dat file in your recalbox at location `/recalbox/share/arcade/advancemame/advancemame.dat`.

### Location

Put your roms like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 mame
┃ ┃ ┃ ┃ ┣ 🗒 **game.zip**

You can also opt for a subdirectory (useful if you want to have a mame set for another core)

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 mame
┃ ┃ ┃ ┃ ┣ 📁 Advance Mame
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **game.zip**

>For more information about roms, go to [this tutorial](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Core options

## ![](/emulators/external-links.png) External links

* **Progettosnaps**: [https://www.progettosnaps.net/](https://www.progettosnaps.net/)
* **Source code used**: [https://github.com/amadvance/advancemame/](https://github.com/amadvance/advancemame)
* **Official website**: [http://www.advancemame.it/](http://www.advancemame.it/)