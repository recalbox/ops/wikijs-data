---
title: RetroArch
description: 
published: true
date: 2023-09-16T12:29:45.652Z
tags: retroar
editor: markdown
dateCreated: 2023-08-31T08:08:50.772Z
---

Recalbox utiliza [**RetroArch**](https://github.com/libretro/RetroArch), sistema creado por Twinaphex, como interfaz con los emuladores.

RetroArch es un projecto de software libre que regroupa varios emuladores bajo una misma interfaz de gestión. La mayoría de los emuladores de Recalbox funcionan bajo la arquitectura de RetroArch (son emuladores de RetroArch). RetroArch es el sistema que permite hacerlos funcionar.

## ¿Qué es?

Técnicamente hablando, RetroArch es un frontend Libretro, es decir una interfaz que permite lanzar los cores Libretro, unificando la forma de funcionar de todos estos cores.

Un core Libretro es un programa adaptado para funcionar con el frontend Libretro, en la mayoría de los casos, un core Libretro es igual que un emulador.

## Funcionalidades

RetroArch, por su propia naturaleza, es un programa que ejecuta otros programas más pequeños. También les indica cómo se configuran ciertos comandos (por lo que sólo necesitarás configurar tu mando una vez para todos los core, por ejemplo). Cuando RetroArch añade nuevas funcionalidades, a menudo se encuentran disponibles a través de varios core a la vez y se puede acceder a ellas de la misma forma.

En resumen, RetroArch establece un estándar que todos los core seguirán.

La interfaz de RetroArch permite modificar muchas configuraciones, pero en la mayoría de los casos, los valores predeterminados deberían ser correctos. Sin embargo, las opciones del core, por ejemplo, pueden permitirte cambiar la región del sistema en la que estás jugando, si es necesario. Como de costumbre, estos ajustes no deben modificarse sin una consideración previa.

También incluye características básicas ofrecidas por un emulador (como los estados de guardado, las capturas de pantalla, el disc-wrapping para sistemas basados en CD, ...) así como configuraciones más avanzadas (shaders, rebobinado y avance rápido, juego en red, ...) algunas de las cuales son accesibles mediante **combinaciones del botón Hotkey**.

> No se recomienda modificar las opciones por defecto de RetroArch directamente a través de su interfaz, porque estas modificaciones no sobreviven a una "restauración de los parámetros de fábrica" de Recalbox y pueden generar problemas. Si necesitáis modificar las opciones de RetroArch, se recomienda utilizar las [sobrecargas de configuracion](/advanced-usage/configuration-override).
{.is-warning}


## Exigencias

Recalbox preconfigura RetroArch para que el usuario final no tenga nada que hacer, pero algunos cores necesitan determinados ficheros que no podemos distribuir, como las BIOS o formatos específicos de ROMS para determinados juegos; el webmanager os puede indicar las differentes BIOS que necésitáis para cada core, y los ficheros `leeme.txt` dentro de los directorios de cada sistema os indican los formatos de las roms aceptadas por el emulador.

Os aconsejamos vivamente hacer el esfuerzo de comprender el funcionamiento de ciertos sistemas (por ejemplo el Arcade), porque no podéis esperar copiar juegos encontrados al azar en internet y pensar que van a funcionar correctamente. Sólamente aprendiendo el funcionamiento y los requisitos de los core, podréis construir vuestros fullsets correctamente.