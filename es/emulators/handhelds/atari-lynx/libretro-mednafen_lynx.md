---
title: Libretro Mednafen_Lynx
description: 
published: true
date: 2024-07-19T15:10:53.992Z
tags: libretro, atari, lynx, mednafen
editor: markdown
dateCreated: 2023-09-11T12:18:21.543Z
---

**Libretro Mednafen_lynx** es un emulador del sistema de videojuegos Atari Lynx.
Más concretamente, es una adaptación de Mednafen Lynx que es un fork de Handy.

## ![](/emulators/license.svg) Licencia

El core posee la siguiente licencia [**zlib**](https://github.com/libretro/beetle-lynx-libretro/blob/master/mednafen/lynx/license.txt) y [**GPLv2**](https://github.com/libretro/beetle-lynx-libretro/blob/master/COPYING).

## ![](/emulators/compatibility.png) Compatibilidad


| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | 
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | 

## ![](/emulators/features.png) Funcionalidades

| Funcionalidad | ¿Soportada? |
| :---: | :---: |
| Reinicio | ✔ |
| Capturas de pantalla | ✔ |
| Grabaciones instantáneas | ✔ |
| Rebobinado | ✔ |
| Netplay | ✔ (no hay emulación de câble link) |
| Opciones del core | ✔ |
| RetroAchievements | ✔ |
| Cheats (Menú de cheats) | ✔ |
| Controles | ✔ |
| Remapping | ✔ |
| Softpatching | ✔ |
| Crop Overscan | ✔ |

## ![](/emulators/bios.svg) BIOS

### Lista de BIOS obligatorias

| Nombre del fichero | Descripción | MD5 | ¿Preinstalada? |
| :---: | :---: | :---: | :---: |
| lynxboot.img | Image Boot Lynx | fcd403db69f54290b51035d82f835e7b | ❌  |

### Emplazamiento

Disponed vuestras bios como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 lynx
┃ ┃ ┃ ┃ ┣ 🗒 **lynxboot.img**

## ![](/emulators/roms.png) Roms

### Extensiones de roms soportadas

Las roms deben tener las extensiones siguientes:

* .lnx
* .o
* .zip
* .7z

Este sistema soporta las roms comprimidas en formato .zip/.7z. No obstante, se trata sólo de un archivo.

Los ficheros contenidos en los .zip/.7z deben tener las extensiones citadas previamente.
Además, cada fichero .zip/.7z sólo debe contener un fichero rom al interior.

### Emplazamiento

Disponed vuestras roms como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 lynx
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.zip**

>Se aconsejan las roms del proyecto **No-Intro**.
{.is-success}

>Para más información sobre las roms, consultad [este tutorial](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

>Para conservar las configuraciones personalizadas cuando se produce una actualización del sistema Recalbox, os aconsejamos utilizar las [Sobrecargas de configuración](./../../../advanced-usage/configuration-override).
{.is-info}

### Acceder a las opciones

Podéis configurar las opciones del core de dos formas diferentes.

* Via el Menú RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via el fichero `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Opciones del core

| Opción | Valor de la opción | Variable (para la sobrecarga) | Valores posibles |
| --- | --- | --- | --- |
| Display rotation | `Activé` ✅ / `Désactivé` | `lynx_rot_screen` | `enabled` / `disabled` |
| Color Format (Restart Required) | `16-Bit (RGB565)` ✅ / `32-Bit (RGB8888)` | `lynx_pix_format` | `16` / `32` |
| Force 60Hz | `Désactivé` ✅ / `Activé` | `lynx_force_60hz` | `disabled` / `enabled` |

## ![](/emulators/external-links.png) Enlaces externos

* **Código fuente utilizado** : [https://github.com/libretro/beetle-lynx-libretro/](https://github.com/libretro/beetle-lynx-libretro/)
* **Documentación Libretro** : [https://docs.libretro.com/library/beetle\_lynx/](https://docs.libretro.com/library/beetle_lynx/)
* **Código fuente oficial** : [https://mednafen.github.io/releases/](https://mednafen.github.io/releases/)
* **Página web oficial** : [https://mednafen.github.io/](https://mednafen.github.io/)