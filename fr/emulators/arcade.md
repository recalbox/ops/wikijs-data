---
title: Arcade
description: Systèmes Arcade
published: true
date: 2024-10-18T16:31:44.451Z
tags: arcade
editor: markdown
dateCreated: 2021-05-21T07:50:29.971Z
---

![](/emulators/arcade/arcade-logo.png)

![](/emulators/arcade/arcade-bartop.svg)

## ATTENTION.

Nous entrons ici dans une partie un peu technique de Recalbox. **ON VA FAIRE SIMPLE.**

Comme pour les consoles, l'arcade dans Recalbox se sépare en 2 éléments : 
- les émulateurs *(inclus et mis à jour dans votre Recalbox)*,
- les "jeux", à ajouter.

...SAUF QUE...
Contrairement aux consoles, l'arcade ne lance pas des "jeux", mais des "systèmes complets" 
*(= on émule l'ensemble de la borne d'arcade, pas que les données du jeu)*

![](/advanced-usage/arcade/board-to-zip.png)

L'émulation arcade est en perpétuelle évolution, encore aujourd'hui.
Les émulateurs s'améliorent, et les "dumps" des systèmes arcade aussi.

A chaque nouvelle version de Recalbox, on vous propose les dernières versions des émulateurs. Attention donc de bien ajouter LES BONNES VERSIONS des systèmes arcades. *(appelés "ROMSET")*

[**Plus d'infos sur l'émulation Arcade dans Recalbox ICI**](./../advanced-usage/arcade-in-recalbox)

## ARCADE "PAR DEFAUT"

| Émulateur | Romset |
| :---: | :---: |
| PiFBA | ROMSET FBA 0.2.96.71 |
| MAME | ROMSET MAME 0.258 (PC et PI5) sinon MAME 0.78-0.188 |
| FBNEO | ROMSET FBNeo 1.0.0.03 |
| NEO-GEO | ROMSET FBNeo 1.0.0.03 |
| NAOMI | ROMSET MAME 0.258 |
| NAOMI 2 | ROMSET MAME 0.258 |
| NAOMI GD | ROMSET MAME 0.258 |
| ATOMISWAVE | ROMSET MAME 0.258 |
| SEGA MODEL 3 | ROMSET MAME 0.258 |

✅ *Attention : tous les systèmes Arcade ne sont pas forcément disponibles sur votre matériel.*
*[**Vérifiez la compatibilité de votre matériel ICI.**](./../hardware-compatibility)*

✅ *Quel romset choisir vis à vis de votre matériel ?*
*[**Vériviez la compatibilité des Romsets dans Recalbox**](./../tutorials/games/generalities/isos-and-roms/romsets-for-recalbox)*

✅ *Pour les utilisateurs avancés, il est possible de changer l'émulateur MAME par défaut, pour essayer de lancer des jeux plus récents. Il vous faudra alors un ROMSET PLUS RÉCENT*
*[**TUTO pour changer d'émulateur par défaut ICI.**](./../tutorials/games/guides/nintendo-64/game-core-compatibility)*

## ARCADE "UTILISATEUR AVANCÉ"

*Emulateurs MAME alternatifs :*

| Émulateur | Romset |
| :---: | :---: |
| LIBRETRO-MAME 2000 | ROMSET MAME 0.35B7 |
| LIBRETRO-MAME 2003 | ROMSET MAME 0.78 |
| LIBRETRO-MAME 2003-Plus | ROMSET MAME 0.78-0.188 |
| LIBRETRO-MAME 2010 | ROMSET MAME 0.139 |
| LIBRETRO-MAME 2015 | ROMSET MAME 0.160 |
| LIBRETRO-MAME | ROMSET MAME 0.258 |

[**Plus d'infos sur l'émulation Arcade avancée ICI**](./../advanced-usage/arcade-in-recalbox)

### LES PROBLÈMES FRÉQUENTS

| Problème | MAUVAISE ROM | MAUVAIS EMULATEUR | CHD MANQUANT | MAUVAIS DUMP | MAUVAIS MAPPING |
| :--- | :---: | :---: | :---: | :---: | :---: |
| **Mon jeu ne se lance pas**  | ✅ | ✅ | ✅ | ✅ |  |
| **Problème de son du jeu** |  | ✅ |  | ✅ |  |
| **Le jeu que j’utilise est buggé, glitché**  |  | ✅ |  | ✅ |  |
| **Le jeu se lance mais les touches ne sont pas les bonnes** |  |  |  |  | ✅ |

[**Plus d'infos sur l'émulation Arcade avancée ICI**](./../advanced-usage/arcade-in-recalbox)