---
title: Libretro Retro8
description: 
published: true
date: 2024-07-14T19:17:49.236Z
tags: libretro, pico-8, retro8, 7.2+
editor: markdown
dateCreated: 2023-09-11T06:17:36.126Z
---

Libretro Retro8 es un core desarrollado para ejecutar juegos creados con este emulador.

## ![](/emulators/license.svg) Licencia

El core posee la siguiente licencia [**GPLv3**](https://github.com/libretro/retro8/blob/master/LICENSE).

## ![](/emulators/compatibility.png) Compatibilidad

 | RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | 
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Funcionalidades

| Funcionalidad | ¿Soportada? |
| :---: | :---: |

## ![](/emulators/bios.svg) BIOS

>**No necesita bios.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Extensiones de roms soportadas

Las roms deben tener las extensiones siguientes:

* .p8
* .png
* .zip
* .7z

### Emplazamiento

Disponed vuestras roms como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 pico8
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.png**

## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

>**Este core no tiene opciones.**
{.is-success}

## ![](/emulators/external-links.png) Enlaces externos

* **Código fuente utilizado** : [https://gitlab.com/recalbox/packages/libretro/libretro-retro8/](https://gitlab.com/recalbox/packages/libretro/libretro-retro8/)
* **Código fuente oficial** : [https://github.com/Jakz/retro8/](https://github.com/Jakz/retro8/)
* **Foro Pico-8** : [https://www.lexaloffle.com/bbs/?cat=7](https://www.lexaloffle.com/bbs/?cat=7)