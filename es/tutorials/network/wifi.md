---
title: Wi-Fi
description: 
published: true
date: 2024-07-26T06:28:11.834Z
tags: wifi, red, administración
editor: markdown
dateCreated: 2023-09-13T14:46:41.019Z
---

Si quieres activar el Wi-Fi en vuestra Recalbox, o si tenéis problemas con vuestra conexión, aquí encontraréis lo que buscáis.

Lista de tutoriales disponibles:

- [Activar la Wifi](enable-wifi)
- [Indicativo wifi de país](wifi-country-code)