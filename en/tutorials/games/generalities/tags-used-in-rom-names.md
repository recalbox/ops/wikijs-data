---
title: Tags used in rom names
description: Some explanations about codes and tags found in rom file names.
published: true
date: 2021-09-04T22:56:55.807Z
tags: rom, tags, codes, file, name
editor: markdown
dateCreated: 2021-08-07T16:12:58.231Z
---

>These tags are not valid for Arcade roms.
{.is-info}

## Geographic tags

**(1)** Japan and Korea
**(4)** USA and Brazil NTSC
**(A)** Australia
**(A)** Asia
**(B)** not USA (Genesis)
**(C)** or **(Ch)** China
**(D)** Netherlands
**(E)** Europe
**(F)** France
**(G)** Germany
**(H)** Holland
**(I)** Italy
**(J)** Japan and South Korea  
**(K)** Korea  
**(R)** Russia  
**(S)** Spain  
**(U)** USA and United States
**(FC)** French Canada
**(FN)** Finland
**(GR)** Greece
**(HK)** Hong Kong
**(NL)** The Netherlands
**(No)** Norway
**(PD)** **Public Domain** : Free software
**(UK)** United Kingdom
**(SW)** Sweden and Switzerland
**(UK)** England
**(Unk)** Country unknown
**(Bra)** Brazilian / Portuguese
**(Kor)** Korea
**(Eng)** English
**(Nor)** Norwegian
**(Dut)** Dutch
**(Pol)** Polish
**(Fin)** Finnish
**(Por)** Portuguese
**(Fre)** French
**(Rus)** Russian
**(Ger)** Germany
**(Ser)** Serbian
**(Gre)** Greek
**(Spa)** Spanish
**(Ita)** Italian
**(Swe)** Swedish

## General version tags

**\[a]** Alternative: Alternative version, usually they fix bugs
**\[b]** Bad Dump: The dump was not done correctly, it's a corrupted version
**\[c]** Cracked: Cracked version
**\[f]** Fixed: Optimized to work better on the emulator
**\[h]** Hacked: Modified version, can be anything from a cheat mod, new game to a wacky hack and/or advertisement
**(M5)** Multilingual: 5 languages in this case
**\[o]** Overdump: size larger than a standard cartridge, does not change anything in the emulator
**\[p]** Pirate: non-original rom
**\[t]** Formed: Usually opens a cheat menu when launching the rom
**\[T]** Translation: Translation by fans
**\[T +]** Newer translation: Rom translation, use current rom
**\[T-]** Older translation: Translation has outgrown the rom, new one exists
**(Unl)** Unlicensed: Quite rare, this is a finished or unfinished game that has never been edited
**\[x]** incorrect checksum
**(ZZZ)** not classified
**(ZZZ)** Unclassified
**(-)** Year unknown
**\[!]** Verified Good Dump: Verified and perfectly playable version, a must-have.
**\[! p]** Pending Dump: This is the closest copy of the original game to date, but still waiting for a true copy of the ROM.
**\[###]** Checksum
**(?? k)** ROM size
**(Bootleg)** This is a 'pirate' version of the game. Sometimes modified versions as well, with gameplay changes (Street Fighter II Rainbow is one of the most famous) or removal of protections/changing the type of electronic card. In some parts of the world, these were more famous than the official versions
**(Taikenban)** Trial version, playable version, limited version demo, is originally a webzine created via the [Limited Edition](http://www.edition-limited.net/) forum.
**Collector's Edition, revised reissue of a version running on a previous hardware.
**(Genteiban)** Limited Edition
**(Shokai Genteiban)** Limited version
**(Midway Classics)** Games re-released after Midway, an American company developing and publishing arcade and console games, bought the rights.

## Advanced tags

**(NG-Dump Known)** No good backup found
**(REVXX)** Revision number (00 is the oldest)
**(VX.X)** Version number (1.0 is the oldest)
**(M #)** Multilingual; # of languages (selectable by a menu)
**(??? - in-1)** Pirate multigame cartridge
**(Vol #)** Official multigame cartridge
**(GCN)** Nintendo GameCube rip
**\[h # C]** Information about the pirated internal cartridge
**\[h # + # C]** Information about the pirated internal cartridge; # th variant
**\[hI]** Intro of the hacked dumping group
**\[hIR]** Intro of hacked dumping group deleted
**\[M]** Single color game
**\[Alpha]** Alpha version
**\[Beta]** Beta version
**(Prototype)** Prototype version
**(Pre-version)** Pre-release version
**(Demo)** Demo version available in kiosks
**(Hack)** Hacked ROM
**(Menu)** Multi-card menu, options not selectable
**(ZZZ_UNK)** Unclassified ROM
**(BIOS)** Copy of basic console input/output system
**(N64DD)** Nintendo 64DD ROM
**(## MBit)** ROM size in megabits
**(## k)** ROM size in kilobits
**(19XX)** Year of release (20th century)
**(20XX)** Release year (21st century)
**(Atmos)** Atmos ROM
**(Telestrat)** Telestrat ROM
**\[R]** RSID format
**(old)** Old version
**\[R-XXX]** Language
**(SC-3000)** SC-3000 Cartridge
**(SG-1000)** SG-1000 Cartridge
**(SF-7000)** SF-7000 Cartridge
**(GG2SMS)** Sega Master System Mode
**(Cart)** Cartridge format
**(Adam)** ADAM version
**(Compilation)** ROM is a dump of a compilation
**(Ch-Trad)** Traditional Chinese pirated game
**(Ch-Simple)** Pirated standard Chinese game
**\[f1C]** Hack, only the name of the cartridge is changed
**(PAL)** European
**(NTSC)** American

## Special tags

### Atari

**(PAL)** European version

### Coleco

**(Adam)** ADAM version

### Game Boy Advance

**\[hIR00]** Intro deleted; replaced by 00 values
**\[hIRff]** Intro deleted; replaced by ff values
**\[f_?]** EEPROM_V124 patch deleted
**\[v]** Game Boy Advance video
**\[eb]** E-book ROM
**\[eba]** E-Book Advance ROM
**\[ebb]** Advance ROM book reader
**\[f125]** EEPROM_V125 deleted
**\[f126]** EEPROM_V126 deleted
**(IQue)** Chinese Game Boy Advance ROM for IQue console
**(Coleco on GBA emulation)** ColecoVision ROM via Coleco on GBA emulator
**(Cologne emulation)** ColecoVision ROM via Cologne emulator
**(DrSMS emulation)** Sega Master System ROM via DrSMS emulator
**(FooN emulation)** ZX Spectrum ROM via FooN emulator
**(PocketNES emulation)** NES ROM via PocketNES emulator
**(FC2GBA emulation)** NES ROM via FC2GBA emulator
**(Goomba emulation)** Game Boy ROM via Goomba emulator
**(GBAGI emulation)** AGI ROM (Adventure Game Interpreter) via GBAGI emulator
**(Swanadvance emulation)** WonderSwan ROM via Swanadvance emulator
**(PCEAdvance emulation)** TurboGrafx-16 ROM via PCEAdvance emulator
**(PocketSMS emulation)** Sega Master System ROM via PocketSMS emulator
**(PocketSNES emulation)** SNES ROM via PocketSNES emulator
**(SNESAdvance emulation)** SNES ROM via SNESAdvance emulator
**(Sneezyboy emulation)** SNES ROM via Snezzyboy emulator
**(VGB emulation)** Game Boy ROM via VGB emulator
**(Pogoshell)** Program for Pogoshell tool
**(s ## e ##)** Serial number and episode number for videos
**(MB)** Multi-boot ROM
**(MB2GBA)** Multi-boot game converted to GBA format
**(-e)** ROM reader
**(+ ## NDA)** Unlicensed multi-boot with ## NES games
**(NDS)** Dump of Nintendo DS 2 cartridge slot
**(GoodBook)** E-Book using GoodBook converter
**(TextReader)** E-Book using TextReader converter
**(PictureBoy)** E-Book using PictureBoy Converter
**(ReadBoy)** E-Book using ReadBoy Converter

### Gameboy

**\[C]** **Color**
**\[S] Super**
**\[BF] Bug Fix** : For modified physical cartridge (flash card like on DS)

### Super Nintendo

**(BS)** **BS ROM** : Rom sent by satellite to TV sets during a Japanese TV show
**(ST)** **Sufami Turbo** : To play with two GB cartridges at the same time on the SNES
**(NP) Nintendo Power** : Games distributed privately (to magazine subscribers in Japan)

### Megadrive / Genesis

**(1)** Japan
**(4)** USA
**(5)** NTSC only
**(8)** PAL only
**(B)** **non USA** : Runs on all MG except USA
**\[c]** **Checksum**: Has some footprint problems
**(F)** World (Megadrive/Genesis)
**(W)** World (Genesis)
**\[x] Checksum incorrect**: Incorrect footprint, risk of major bugs
**\[R-]** **Country**: For example \[RF] for France
**(J-Cart)** The original cartridge has two controller ports
**(SN)** Sega-Net game
**(REVXB)** Sonic the Hedgehog, Japanese version 2
**(REVSC02)** Sonic Compilation, Sonic 2 version
**(MP)** MegaPlay version
**(MD Bundle)** The ROM is from the Pack-in Sega Genesis game
**(Alt Music)** Alternative music version of Sonic Spinball

### NeoGeo Pocket

**\[M]** **Mono only** : Mono audio

### NES / FC

**(PC10) PlayChoice 10** : Arcade version based on NES games
**(VS) Versus** : Same as (PC10) but not playable in solo
**\[hFEE]** Copied from Far East copiers
**\[hMxx]** Hacked to run on the xx card
**(No SMB)** Hack Super Mario Bros. not specified
**(Aladdin)** Works with an Aladdin cartridge that locks the 10NES lock chip
**(Sachen)** Game without Sachen license
**(KC)** Konami Classic
**(PRG0)** Program review 0
**(PRG1)** Program review 1
**\[U]** Universal NES image file format
**(Mapper ##)** Mapper number
**(FDS Hack)** Hacked from Famicom Disk System to NES
**(GBA E-reader)** Hacked from the e-Reader card
**(E-GC)** NES ROM extracted from a European Nintendo GameCube
**(J-GC)** NES ROM extracted from a Japanese Nintendo GameCube

### SNES

**(BS)** **Broadcast Satellite (Satellaview) ROMs** : ROMs sent by satellite to TV sets, but that have never been physically supported.
**(NP) Nintendo Power ROM**: Games distributed privately (to newspaper subscribers in Japan)
**(NSS)** SNES Arcade ROM
**(ST) Sufami Turbo ROM** : Special cartridge to play with two GB cartridges at the same time on the SNES.