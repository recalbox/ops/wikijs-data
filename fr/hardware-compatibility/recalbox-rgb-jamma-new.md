---
title: Recalbox RGB JAMMA - Manual 
description: Recalbox RGB JAMMA Manual and F.A.Q.
published: true
date: 2024-09-05T14:38:15.476Z
tags: 
editor: markdown
dateCreated: 2024-01-16T22:48:31.481Z
---


> English version available [here](../../en/hardware-compatibility/recalbox-rgb-jamma-new).
{.is-info}


# Présentation


_RECALBOX RGB JAMMA_: Profitez de tout Recalbox sur votre borne JAMMA !

![rgb_jamma_v5_main_01.jpg](/compatibility/jamma/rgb_jamma_v5_main_01.jpg){.center}{.decorate}

Avant d'aller plus loin, qu'est-ce que le JAMMA ?
Apparu dans les années 80, le JAMMA a permis de standardiser la connexion des différents signaux dans les machines d'arcade. Changer de jeu sur une machine d'arcade est soudain devenu un jeu d'enfant.

Pour plus d'informations, vous pouvez consulter la vidéo de l'association HFS qui retrace l'histoire et le fonctionnement du jamma:

[![jamma_cqlb.jpeg](/compatibility/jamma/jamma_cqlb.jpeg)](https://www.youtube.com/watch?v=jOBZ0h9bA0Y)


Ce format a perduré jusqu'à aujourd'hui, et est encore utilisé dans un grand nombre de machines d'arcade "à l'ancienne" à tube cathodique.
Si vous n'avez pas de machine JAMMA, ou si vous n'envisagez pas d'en acheter une dans un futur proche, le Recalbox RGB JAMMA n'est pas fait pour vous...

En revanche, son grand frère, Recalbox RGB DUAL, qui vous permet de vivre le frisson du jeu sur votre TV CRT, est également compatible avec le Rasbperry Pi 4/3/Zero2 et est disponible sur notre boutique.



# Préparation

Le pinout du Recalbox RGB Jamma est compatible JAMMA, JAMMA+ et CHAMMA.
![jamma_pinout_(10).png](/compatibility/jamma/jamma_pinout_(10).png){.center}

_Les boutons sont nommés selon le standard JAMMA:_

![jamma-layout.png](/compatibility/jamma/jamma-layout.png)

Les boutons TEST et SERVICE sont situés dans votre borne, souvent directement sur l'alimentation.

## Installation dans la borne

### Assemblage

#### Pi + Recalbox RGB JAMMA

Clipsez le Raspberry Pi dans le Recalbox RGB JAMMA, et clipsez les entretoises en plastique fournies sur les deux PCB.

![assembly1.jpg](/compatibility/jamma/assembly1.jpg){.decorate}

![assembly2.jpg](/compatibility/jamma/assembly2.jpg){.decorate}

#### Ventilateur

Le ventilateur du Recalbox RGB JAMMA est utilisé pour refroidir le Raspberry Pi. Il a été testé pour refroidir le plus efficacement possible le Raspberry, hors boitier ou lorsque vous utilisez le boitier officiel. **Il remplace le ventilateur de votre Raspberry Pi.**

Utilisez les Pin +5V et GND pour une puissance maximale, ou +3V et GND pour limiter le bruit du ventilateur.

![fan_(5).png](/compatibility/jamma/fan_(5).png){.center}

_Ventilateurs compatibles :_
- Taille 40x40x20mm ou 40x40x10mm
- 5V
- 1W max (0.3W suffisent)
- Vis et écrous M3

> Si vous installez votre propre ventilateur, positionnez le pour qu'il souffle l'air vers le bas.
> 
{.is-info}

> N'ajouter pas de ventilateur directement sur le Rasbperry, la ventilation provenant du Recalbox RGB JAMMA refroidi directement le systeme complet.
{.is-warning}


#### Boitier (en option)

Une fois le Raspberry Pi clipsé sur le Recalbox RGB JAMMA, il vous reste à positionner le tout dans le boitier et à refermer ce dernier avec les vis M2.5x15mm fournies avec le boitier.

![case-assembly1.jpg](/compatibility/jamma/case-assembly1.jpg){.decorate}

![case-assembly1-1.jpg](/compatibility/jamma/case-assembly1-1.jpg){.decorate}

## Tension de l'alimentation
La tension de l'alimentation du Recalbox RGB JAMMA + Raspberry Pi 5 doit se situer entre 5.05V et 5.20V.
Si votre borne est déjà réglée pour être compatible avec la plupart des PCB / Systèmes multi, aucun problème, vous pouvez lancer Recalbox RGB JAMMA tel quel.

Un popup vous avertira lors de l'utilisation si la tension devient trop basse lorsque le Raspberry Pi monte en charge.

Si ce popup apparait, il vous faudra remonter votre tension de quelques millivolts (entre 5.05V et 5.2V, **nous conseillons 5.1V**).
Cette opération se fait à l'aide d'un potentiomètre qui se situe sur votre alimentation. 
Prenez soin de bien vous renseigner sur votre alimentation, de bien vous équiper (tourne vis en plastique ou céramique), ou de vous faire accompagner si vous êtes débutant et que vous n'êtes pas sûrs de vous.

## Installation de l'image

- Téléchargez l'image de Recalbox disponible sur la section Jamma du site [recalbox.com/recalbox-rgb-jamma](https://www.recalbox.com/fr/recalbox-rgb-jamma/).
- Sans la décompresser, inscrivez l'image sur votre carte SD à l'aide du [logiciel Raspberry Pi Imager](https://www.raspberrypi.com/software/), en sélectionnant votre image comme source et votre lecteur de carte SD comme destination (voir [cette vidéo](https://www.recalbox.com/static/newrpiimager-image.mp4-processed-ffa17b13a7bbc9a682f419b3c9d9a451.mp4)).
- Insérez la carte SD dans votre Raspberry Pi, branchez votre Recabox RGB JAMMA sur votre socket JAMMA et démarrez votre borne !
L'installation et la détection du Recalbox RGB JAMMA se lancent automatiquement.

# VIDEO

## Pixel Perfect 15Khz
Tous les jeux, qu'ils soient sur arcade ou console, sont lancés dans leur résolution et leur fréquence d'origine. Certains jeux possedent des résolutions "exotiques", comme Mortal Kombat (`400x254@54.815170Hz`) ou R-Type (`384x256@55.017606Hz`).
Les modes vidéos générés par le Recalbox RGB JAMMA s'approchent au plus près de ces résolutions d'origine, et certains jeux pourront donc nécessiter un réglage de votre moniteur arcade.

### Super Resolution
Pour produire un signal vidéo de la meilleure qualité qui soit, il est parfois nécessaire de passer par les super résolutions, qui multiplient la résolution horizontale par un nombre entier. L'écran du Recalbox RGB JAMMA vous indique le facteur de multiplication horizontal lorsqu'une super résolution est activée.

![metal-slug-screen.png](/compatibility/jamma/metal-slug-screen.png){.decorate}{.center}


## 31kHz support / JVS / Naomi
Si vous avez un écran 31kHz (une NUC + jammaifier par exemple), vous devez activer le mode 31kHz pour que le signal vidéo passe en 31kHz pour tous les menus et jeux.

Cependant, Recalbox se lance par défaut en 15kHz, donc deux solutions s'offrent à vous :
* Brancher le recalbox rgb jamma sur une borne 15kHz une première fois et changer la fréquence dans le menu :

![option-31khz.png](/compatibility/jamma/option-31khz.png){.decorate}

* Brancher la carte SD sur votre PC et ajouter la ligne `options.screen.31kHz = 1` directement dans le fichier `RECALBOX/crt/recalbox-crt-options.cfg`.

Lorsque vous lancez un jeu 15kHz en 480p, les lignes du jeu sont simplement doublées. Vous pouvez activer les scanlines pour simuler les scanlines d'un affichage 240p. 3 options s'offrent à vous pour activer des scanlines plus ou moins prononcées :

![scanlines.png](/compatibility/jamma/scanlines.png){.decorate}

## Multisync support

Le mode multisync est approprié pour les platines qui détectent la fréquence du signal vidéo et changent automatiquement de mode en fonction. Les jeux 240p sont lancés en 15kHz et les jeux 480p en 31kHz. Vous retrouverez donc TOUS les jeux dans leurs résolutions d'origine !

Le multisync s'active directement depuis le menu Recalbox RGB JAMMA:

![multisync.png](/compatibility/jamma/multisync.png){.decorate}

Une fois le mode multisync sélectionné, vous pourrez choisir la résolution des menus, en fonction de vos habitudes, et pour limiter les switchs de votre borne:
— Choisssiez plutôt le 240p si vous jouez plus souvent aux jeux 15kHz qu'aux jeux 31kHz.
— Choisssiez plutôt le 480p si vous jouez plus souvent aux jeux 31kHz qu'aux jeux 15kHz.

![menu-resolution.png](/compatibility/jamma/menu-resolution.png)

> Verifiez bien que votre platine est compatible avec le mode MultiSync avant d'activer ce mode.
{.is-warning}

Par défaut, les jeux Atomiswave, Naomi, et Dreamcast seront lancés en 480p. Mais certains jeux de l'époque étaient upscalés sur ces plateformes, et certains d'entre vous pourraient avoir envie de les découvrir en 240p :) Pour cela rien de plus simple, activez l'option de sélection de la résolution dans le menu, et choisissez la résolution de chaque jeu avant de le lancer !

![select-resolution.png](/compatibility/jamma/select-resolution.png){.decorate}

À chaque lancement, la résolution pourra être sélectionnée :

![select-game-rez.png](/compatibility/jamma/select-game-rez.png){.decorate}


### Mode TATE (vertical)

Pour activer le mode TATE, rien de plus simple, tout se trouve dans le menu TATE (START -> TATE).

![tate-settings.png](/compatibility/jamma/tate-settings.png){.decorate}

Sélectionnez le sens de rotation (RIGHT = 90° dans le sens des aiguilles d'une montre). Une fois la rotation sélectionnée, le frontend se recharge et l'écran est pivoté !

![tate-rotation.png](/compatibility/jamma/tate-rotation.png){.decorate}

Il est possible de n'afficher que les jeux verticaux a l'aide de l'option "DISPLAY ONLY TATE GAMES IN GAMELIST"

> Pensez a scraper vos jeux pour que Recalbox puisse detecter les jeux verticaux dans les menus !
{.is-success}

Si votre thème est compatible, il s'adaptera automatiquement à la disposition verticale de l'écran !

![tate-theme.png](/compatibility/jamma/tate-theme.png){.decorate}


# CONTROLS
## Jamma + Jamma+ + Chamma

À chaque borne sa configuration. Le Recalbox RGB JAMMA supporte automatiquement les différentes combinaisons de boutons sur le jamma ou sur les kick harness.
Les KickHarness CPS1 et CPS2 sont interchangeables sans aucune configuration.

* Configuration JAMMA et Kick Harness

| Boutons sur le JAMMA | Boutons sur kick harness |
|----------------------|--------------------------|
| 1 2                  |                          |
| 1 2 3                |                          |
| 1 2 3                | 4                        |
| 1 2 3                | 4 5                      |
| 1 2 3                | 4 5 6                    |
| 1 2 3 4              | 5 6                      |

* Configuration JAMMA+ et Kick Harness

| Boutons sur le JAMMA | Boutons sur kick harness |
|----------------------|--------------------------|
| 1 2 3 4 5            | 6                        |

* Configuration CHAMMA et Kick Harness

| Boutons sur le JAMMA | Boutons sur kick harness |
|----------------------|--------------------------|
| 1 2 3 4 5 6          |                          |

### Ground on pin 27/E
Si vous avez la masse des J1 et J2 sur les pins 27/E du JAMMA, il vous faudra activer la masse commune dans la configuration.

![27asground.png](/compatibility/jamma/27asground.png){.decorate}

## CPS1 and CPS2 Kick Harness

![cps1and2.png](/compatibility/jamma/cps1and2.png){.center}

## Input Lag

L'input lag du Recalbox RGB JAMMA est extrêmement bas, moins d'une milli seconde.

Cependant, la latence globale des jeux et systèmes est impacté par d'autres éléments que l'input lag.

Deux options sont disponibles dans Recalbox RGB JAMMA pour diminuer au maximum la latence :
![reducelagoptions.png](/compatibility/jamma/reducelagoptions.png){.decorate}

* `reduce latency` va activer de nombreuses options dans retroarch et autres émulateurs pour réduire la latence (auto frame delay, late polling etc).
* `run ahead` permet de précalculer des frames à l'avance pour gagner une frame de latence.

Ces deux options activées conjointement, combinées à l'input lag très bas des composants du Recalbox RGB JAMMA, permettent de réduire drastiquement la latence et de côtoyer de près la latence du mister ou des jeux originaux.
Cependant, ces options pourraient avoir un impact négatif sur une poignée de jeux, pensez-y si certains jeux ralentissent.

Plus d'info sur [notre dossier complet sur l'input lag sur le JAMMA](https://www.recalbox.com/fr/blog/2023-07-19-input-lag-and-latency/).



## NeoGeo layout selection

Vous pouvez configurer le mapping qui sera automatiquement appliqué lorsque vous lancez un jeu NeoGeo.

Si vous possédez un panel 4 boutons NeoGeo, renseignez le nombre de boutons dans les options du menu Recalbox RGB JAMMA et profitez du mapping standard NeoGeo:

![panel_4btn.png](/compatibility/jamma/panel_4btn.png){.decorate}

![4buttons_(1).png](/compatibility/jamma/4buttons_(1).png)

Pour les panels 6 boutons, vous avez le choix entre deux mappings. Le line qui reprend le mapping en ligne des panels NeoGeo et le Square qui reprend le mapping des contrôleurs Neogeo (réglable dans le menu Recalbox RGB JAMMA)

![neogeolayout.png](/compatibility/jamma/neogeolayout.png){.decorate}

![neogeo-layout_(1).png](/compatibility/jamma/neogeo-layout_(1).png)

## 4 players mode

Le mode 4 joueurs s'active directement depuis le menu Recalbox RGB JAMMA.

Les joueurs 3 et 4 sont mappés sur le Kick Harness CPS2:

![4playerscps2.png](/compatibility/jamma/4playerscps2.png){.center}

Le système virtuel contenant les jeux 3 joueurs et plus s'active automatiquement lorsque le mode 4 joueurs est activé.

## 5/12V Voltage compatible Coin Support+

Recalbox RGB JAMMA est compatible avec les monnayeurs mécaniques ou électroniques (+3V, +5V, +12V) sur votre peigne JAMMA.
Recalbox détectera automatiquement le niveau logique du monnayeur (Normally Open, ou Normally Closed) pour détecter l'insertion de pièces.

## Test + Service buttons

Les boutons SERVICE et TEST, qui se trouvent à l'intérieur de la borne, sont supportés par Recalbox RGB JAMMA et ont deux utilités majeures :
- acceder au menu de service de vos jeux arcade (compatible avec fbneo, mame, naomi, atomiswave), reproduisant ainsi leur fonctionnalité d'origine.
- sortir d'un jeu avec la combinaison SERVICE + TEST. Pratique lorsque vous avez activé en mode Gamecenter mais que vous voulez changer de jeu.


## Start Button Patterns (Coin, HK, Turbo, Volume)

De nombreux raccourcis sont disponibles avec le bouton START. Ces raccourcis peuvent êtres activés et désactivés depuis le menu Recalbox RGB JAMMA.

![start-patterns.png](/compatibility/jamma/start-patterns.png){.decorate}

* `START + BTN1 = CREDIT`: active les crédits avec le raccourcir START + BTN1 pour chaque joueur.
* `START + BTN = HK`: active les raccourcis START + BTN = HOTKEY + BTN. Pratique pour ouvrir les menus de retroarch ou pour régler le son avec START + HAUT/BAS
* `START 3SEC = EXIT`: sortez des jeux avec un long appui sur START. Si désactivé, vous pouvez toujours sortir des jeux avec la combinaison TEST+SERVICE
* `START + BTN 5SEC = AUTO FIRE`: activez l'auto fire sur un bouton avec START + BTN. Désactivez de la même façon. Un reboot réinitialise les modes turbo.
* `START + UP/DOWN = VOLUME`: règle le volume sonore.
* `START + LEFT/RIGHT = AFFICHER/CACHER LES CLONES`: affiche ou cache les clones d'un jeu arcade.




## Les raccourcis 

![instruction_space_(2).png](/compatibility/jamma/instruction_space_(2).png)


# AUDIO
## Sound control with joystick

Utilisez le raccourci START + HAUT et BAS sur le joystick pour monter ou baisser le son.


## Outputs

* La prise JACK permet de sortir un son stéréo non amplifié. Pour une utilisation avec un ampli externe.
* L'ampli MONO permet de sortir le son sur les pins audio du JAMMA. Il peut être désactivé dans les options.
* La prise 4 pins audio permet aussi de sortir un signal stéréo non amplifié.

![4pin_audio.png](/compatibility/jamma/4pin_audio.png){.center}

## Mono Amp BOOST

Le son de l'ampli mono peut être réglé de plusieurs façons :
* depuis le bouton rotatif sur le Recalbox RGB JAMMA
* depuis l'interface (START + HAUT/BAS)
* il est aussi possible de booster le son en augmentant le gain de l'ampli depuis le menu. Augmentez le gain pas à pas pour éviter les mauvaises surprises :)

![monoboost.png](/compatibility/jamma/monoboost.png){.decorate}


# SYSTEM

## Automatic Systems by Editor

Lorsque vous ajoutez un set complet FBNeo ou Mame, Recalbox peut afficher automatiquement des systèmes qui correspondent a chaque éditeur.
Les systèmes sont automatiquement activés par Recalbox pour Recalbox RGB JAMMA.

Pour activer ou désactiver ces systèmes virtuels, rendez-vous dans le menu ARCADE.

![virtual-editors.png](/compatibility/jamma/virtual-editors.png){.decorate}

## Auto Boot on game

Pour booter directement sur un jeu lorsque vous allumez votre borne, activez le `boot on game` dans le menu ADVANCED SETTINGS -> BOOT SETTINGS.

Ensuite, naviguez sur le jeu que vous voulez lancer automatiquement, appuyez sur START et selectionnez BOOT ON THIS GAME.

À chaque démarrage, le jeu sera lancé automatiquement. Restez appuyé sur START pour en sortir et revenir dans le menu.

![bootongame.png](/compatibility/jamma/bootongame.png){.decorate}


## Gamecenter Mode

Le GameCenter mode permet de lancer la borne comme dans une salle d'arcade : un seul jeu est lancé, des crédits doivent être ajoutés avec le monnayeur, aucun raccourci n'est disponible, et la sortie du jeu ne peut se faire qu'avec la combinaison SERVICE + TEST (deux boutons qui se trouvent dans la borne, accessibles seulement en ouvrant la trappe à l'aide des clés de la borne).

Si vous ajoutez à cela l'auto boot on game, votre borne se comportera à 100% comme une borne originale.

Pour activer le mode GameCenter, il suffit de désactiver les Start Buttons Patterns (les raccourcis des boutons) dans le menu Recalbox RGB JAMMA:

![gamecenter.png](/compatibility/jamma/gamecenter.png){.decorate}


## Kiosk Mode

Le Kiosk Mode permet de profiter de la sélection des jeux Recalbox, mais en éliminant la possibilité d'accéder aux menus du frontend ou au menu des jeux. Il désactive aussi l'ajout et la suppression des favoris.

Pour l'activer, il faut passer par l'édition du fichier `recalbox.conf` ([tuto](../basic-usage/getting-started/recalboxconf-file)) et remplacer la ligne :
```
emulationstation.menu=default
```
par :
```
emulationstation.menu=none
```
Il vous faudra ensuite modifier ce fichier et remettre la valeur `default` pour retrouver toutes les fonctionnalités de recalbox.


## System Update

Les mises à jour du système Recalbox sont fréquentes, et on en est fier. Les bugs sont corrigés au plus vite, et cela vous permet de profiter des dernières fonctionnalités rapidement.

Recalbox vous proposera automatiquement la mise à jour si votre système est connecté à internet, par câble ou wifi.

## Disque dur USB

Il est conseillé d'utiliser une carte SD pour stocker le système et les jeux sur Recalbox RGB JAMMA.

Si vous décidez d'utiliser un disque dur externe en USB, il faudra activer une option dans le fichier `recalbox-user-config.txt` qui se situe dans la partition `RECALBOX` lorsque vous branchez la carte SD sur votre PC.

Ouvrez le fichier `RECALBOX/recalbox-user-config.txt` avec un éditeur de texte et ajoutez à la fin la ligne suivante :
```
usb_max_current_enable=1
```

# TIPS

### Alimentation grise Astro City

Les alimentations grises de Sega Astro City 400-5198-01**Y** (et uniquement ce modèle) posent un problème sur les systèmes qui ne consomment pas assez sur le 5V au démarrage.

![psu400-5198-01y.jpg](/compatibility/jamma/psu400-5198-01y.jpg){.decorate}

Si c'est le cas, vous pouvez connecter une résistance 8Ω (minimum 25W, conseillé 50W) sur les deux pads +5V et GND situés au niveau de l'alimentation sur le Recalbox RGB JAMMA. Cela permettra de tirer le courant nécessaire pour que l'alimentation ne se mette pas en sécurité au démarrage.

Préférez l'utilisation de pins et de câbles Dupont plutôt qu'une soudure des fils directement sur la board.
Veillez à bien isoler votre résistance pour éviter qu'elle ne cause des cours circuits (son contour en cuivre est conducteur).

![resistor.png](/compatibility/jamma/resistor.png){.decorate}

Ici la résistance a été isolée avec du plastique anti-statique :

![pxl_20240210_203627364~2.jpg](/compatibility/jamma/pxl_20240210_203627364~2.jpg){.decorate}

# Troubleshoting / Recherche de panne

* **Mon Raspberry Pi ne démarre pas, et je n'ai que la led en haut a droite allumée sur le Recalbox RGB JAMMA.**

![ouvlo.jpg](/compatibility/jamma/ouvlo.jpg){.decorate}{.center}
Si la LED d'alimentation s'allume, mais que la LED `+5V RPi` du système OUVLO reste éteinte, c'est que le système de protection empêche le courant de passer jusqu'au Raspberry Pi. 
L'alimentation de votre borne est soit trop élevée (au-dessus de 5.25V), soit trop basse.

Il vous faudra donc la régler à l'aide d'un tournevis adéquat, et atteindre entre 5.1V et 5.2V hors charge.

# F.A.Q


* **Faut-il ajouter un ventilateur ou un pad thermique sur le Raspberry Pi**
Non, le ventilateur situé sur le Recalbox RGB JAMMA est suffisant pour le refroidissement.
* **Faut-il ajouter une alimentation USB ?**
Non 
* **Est-ce compatible avec les bornes JVS ?**
Non pas directement. Il vous faudra un PCB permettant d'adapter le JAMMA sur le JVS de votre borne. Une fois adapté, Recalbox RGB JAMMA est tout à fait compatible avec les contrôles et écrans des bornes JVS (31kHz ou multisync).
* **Ou acheter un Recalbox RGB JAMMA ?**
Sur [la boutique officielle Recalbox](https://shop.recalbox.com) (suivant disponibilité et date de sortie)
* **Comment scraper ses jeux ?**
Vous pouvez scraper vos jeux pour récupérer les informations, images et vidéos directement depuis le menu SCRAPER de Recalbox. 
Pensez-y, si vous êtes [patron](https://www.patreon.com/recalbox) 
  - Patron niveau 1 : l'auto scrap vous permettra de récupérer les informations du jeu automatiquement lorsque vous naviguez dans vos listes !
  - Patron niveau 2 : en plus de l'auto scrap, vous pourrez utiliser les serveurs de Recalbox pour effectuer un scrap complet de tous vos jeux en un temps record !
- **Puis-je lancer les jeux en 480i ?**
Seuls les jeux compatibles en 480i peuvent être lancés en 480i. Il n'est pas possible de forcer un mode 480i pour un jeu avec une résolution native en 240p.
Sur Raspberry Pi 4/400 et 3, le 480i est automatique sur les jeux qui sont compatibles (Dreamcast, Atomiswave, etc...)
Sur Raspberry Pi 5, le mode 480i n'est pas supporté, et vos jeux se lanceront soit en 240p si vous êtes sur un écran 15kHz, ou en 480p si vous êtes sur un écran 31kHz ou multisync.
- **Puis-je overclocker mon Raspberry Pi ?**
Vous pouvez overclocker tous les Raspberry Pi depuis le menu de Recalbox -> Options avancées -> Overclock
Il est vivement conseillé de ventiler le Raspberry Pi si vous overclockez.
Pensez aussi que l'overclock implique une plus grande consommation, donc une plus forte baisse de tension. Désactivez l'overclock si des alertes d'undervoltage apparaissent.
- **Que se passe-t-il si je monte trop la tension sur l'alimentation ?**
Le Raspberry Pi et le Recalbox RGB JAMMA fonctionnent jusqu'à une tension de 5.25V, au-dessus de cette tension, le système OUVLO du Recalbox RGB JAMMA coupera l'alimentation pour protéger les circuits.
- **Comment fabriquer ses câbles 4 joueurs ?**
Vous pouvez commander vos éléments (connecteurs etc) chez smallcab, et suivre le pinout renseigné plus haut sur cette page.
- **Quels jeux et sets arcade conseillez-vous ?**
Les romset arcade compatibles avec les versions des émulateurs que Recalbox embarque sont disponibles sur la page https://wiki.recalbox.com/fr/emulators/arcade
Nous conseillons de partir d'un fullset FBNEO, qui contient la plupart des jeux 2D que vous recherchez, et ensuite de sélectionner les jeux que vous voulez dans le set MAME.
- **Puis-je brancher des manettes / joysticks supplémentaires ?**
Oui, si vous voulez brancher des manettes supplémentaires, il est recommandé de jouer avec des manettes USB, mais le bluetooth est aussi compatible et facilement activable depuis les options des manettes.
Pensez à reconfigurer les boutons lors du premier branchement d'une manette.
- **Puis-je faire tourner des jeux 240p (15kHz) sur un écran uniquement 31kHz ?**
Les jeux en résolution 240p, 224p et autres sont lancés dans un mode 31kHz sur votre écran, qui ne supporte pas les modes 15kHz. Cependant, le filtre Scanlines du Recalbox RGB JAMMA permet de se rapprocher au maximum du rendu sur un écran 15kHz.
- **Est-ce que les guns sont supportés (guncon, ir gun, sinden) ?**
Pas pour l'instant.
- **Est-ce risqué de changer de fréquence en multisync ?**
Non, à partir du moment où votre platine de change pas de mode des dizaines de fois par minute, il n'y a pas de risque. Cependant, certains s'accordent à dire que le relais qui émet ce bruit de claquement au moment du switch de résolution est le point faible des platines multifréquences automatiques. 
C'est au choix de chacun de l'utiliser ou pas avec Recalbox RGB JAMMA qui ne saura être tenu responsable si votre platine tombe en panne.
- **Est-ce que je peux enlever les vidéos d'intro ?**
Oui depuis le menu -> Options Avancées -> Options de Boot.
- **Est-ce que je peux ajouter une vidéo d'intro perso ?**
Oui, comme indiqué sur la page suivante : https://wiki.recalbox.com/fr/basic-usage/features/boot-videos
- **Comment faire les mises à jour ?**
En connectant tout simplement votre Recalbox à internet, avec un câble ethernet ou en wifi. Le système vous proposera d'effectuer la mise à jour si vous démarrez  votre borne et qu'une mise à jour est disponible.
- **Quelle taille de micro SD recommandez-vous ?**
Pour une expérience 100% arcade, 128Go suffisent (jeux et scrap compris).
Pour une expérience arcade et consoles 8/16/32bits sans les jeux CD, préférez une 256Go.
- **Comment utiliser un Recalbox RGB JAMMA dans une borne Naomi ?**
Pour brancher Recalbox RGB JAMMA dans une borne Naomi NUC, il vous faudra un adaptateur JAMMA vers JVS. Il en existe plusieurs disponibles sur le net.
- **Est-ce que je peux endommager mon Recalbox RGB JAMMA en le branchant à l'envers dans le peigne JAMMA ?**
Nous avons placé des protections pour éviter au maximum que le Recalbox RGB JAMMA ou le Raspberry Pi ne subissent de dégâts lorsque vous le branchez a l'envers. Soyez cependant vigilants lorsque vous le branchez pour éviter le moindre risque pour votre installation.
- **Est-ce que je dois augmenter la tension de mon alimentation si j'ai un message d'undervoltage ?**
Par nature, la taille des câbles du faisceau JAMMA implique une baisse de tension non négligeable sur votre installation. Il est donc normal d'augmenter votre tension, hors charge, entre 5.05V et 5.2V suivant les PCB que vous installez dans la borne.
- **Est-ce que les jetons sont compatibles avec les monnayeurs ?**
Les jetons ont une taille très proche des pièces de 100 yens, et sont compatibles avec beaucoup de monnayeurs mécaniques et électroniques. Ils restent cependant des goodies pour la collection et la compatibilité avec votre monnayeur n'est pas garantie.
- **Puis-je éteindre ma borne sans passer par le menu Recalbox ?**
Oui. Pensez tout de même à passer par le menu d'extinction lorsque vous changez des paramètres ou ajoutez/enlevez des favoris.
- **Est-ce que Recalbox RGB JAMMA est compatible avec mon panel 1, 2, 3, 4, 5 ou 6 boutons ?**
Oui, voir ci-dessus dans la section des contrôleurs, pour les différents câblages compatibles.
- **Est-ce que le Recalbox RGB JAMMA est compatible avec une borne avec écran HDMI ?**
Non.
- **Puis-je personnaliser ma disposition des boutons ?**
Vous pouvez choisir différentes options de layout pour la NeoGeo par exemple. Cependant, vous ne pourrez pas reconfigurer les boutons pour les différents systèmes et jeux. Il y a une raison à cela : nous voulons vous proposer une expérience plug and play. La disposition des boutons sur les panels étant standard, la configuration du mapping s'adapte déjà à chaque jeu et à chaque système que vous pourrez lancer.
- **Est-ce Recalbox RGB JAMMA s'adapte dans une borne Viewlix/chewlix?**
Non, pas si l'écran nécessite une entrée vidéo HDMI.
- **Est-ce Recalbox RGB JAMMA est compatible avec d'autres systèmes d'émulation que Recalbox ?**
Non.
- **Avec quels Raspberry est compatible le Recalbox RGB JAMMA?**
Avec les Raspberry Pi 3/4/400 et 5.
- **Est-ce compatible avec les clones de Raspberry Pi (Orange Pi etc...) ?**
Non.
- **Est-ce que je peux laisser le ventilateur du Raspberry Pi 5 ?**
Non, le ventilateur du Recalbox RGB JAMMA suffit largement au refroidissement du Raspberry Pi.
- **Puis-je régler la géométrie de l'image depuis Recalbox RGB JAMMA ?**
Il est possible de régler la position de l'image pour chaque mode d'affichage dans l'écran de calibrage du menu Recalbox RGB JAMMA. Cependant, la taille verticale et horizontale de l'image doit se régler directement sur votre écran.
- **Est-ce compatible avec toutes les bornes JAMMA ?**
Si votre borne est équipée d'un faisceau JAMMA, le Recalbox RGB JAMMA est compatible !
- **Est-ce que je peux augmenter le son depuis le Recalbox RGB JAMMA ?**
Vous avez de nombreuses possibilités pour régler le son :
  - depuis votre ampli de l'alimentation.
  - depuis le potard sur le Recalbox RGB JAMMA.
  - depuis le menu des options du son.
  - depuis le raccourci START + HAUT/BAS, si celui-ci est activé dans les options.
- **Le son sature**
  - Réglez le son à 90% maximum dans les options du son (START -> OPTIONS DU SON)
  - Poussez le potard du son Mono au maximum sur le Recalbox RGB JAMMA.
  - Réglez l'ampli de votre borne en fonction.
- **Puis-je brancher mon Recalbox RGB JAMMA sur un supergun ?**
Oui.
- **Mes favoris ne se sauvegardent pas.**
Pour sauvegarder vos favoris (après avoir ajouté ou retiré un jeu favori avec la touche 3), il faudra procéder à une extinction depuis le menu (START -> QUITTER -> ETEINDRE)
- **J'ai des parasites vidéos**
  - Les niveaux RGB du Recalbox RGB JAMMA sont proches des niveaux des PCB originaux, souvent plus élevés que certaines solutions multis, à base de Raspberry Pi. Pensez à régler vos niveaux sur les potards de votre platine.
  - Pensez aussi à vérifier la mise à la terre de vos bornes.

