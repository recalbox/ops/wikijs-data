---
title: Atari 5200
description: 
published: true
date: 2022-07-22T10:07:14.946Z
tags: consoles, atari, 5200, atari-5200
editor: markdown
dateCreated: 2021-05-21T07:58:47.889Z
---

![](/emulators/consoles/atari5200.svg){.align-center}

## Fiche technique

* **Fabricant :** Atari Corporation
* **Année de sortie :** 1982
* **Processeur :** 6502C cadencé à 1,79 MHz
* **RAM :** 16 Kbytes
* **Processeur Audio / Vidéo :** TIA
* **Résolution :** 320 x 192 pixels, 16 couleurs à l'écran
* **Puce sonore :** 4 voix
* **Taille des cartouches :** 2kB - 32kB

## Présentation

L'Atari 5200 est essentiellement un Atari 400 sans clavier. Ceci prouve que les conceptions d'Atari peuvent rapidement s'adapter au marché.

La console apporte un nouveau joystick révolutionnaire avec un manche analogique et des touches de fonction (start, pause, reset). Elle bénéficie d'autres innovations comme un tir automatique et quatre ports de joystick.

La conception sans centrage du manche s'avère sans avantage et peu fiable, énervant beaucoup d'acheteurs. Tandis que les 5200 recueillent de nombreux adeptes grâce à une bibliothèque de jeux de haute qualité, elle doit faire face à une nouvelle concurrence avec la [**Colecovision**](./colecovision) et à une économie qui devient de plus en plus vacillante.

La question de savoir quel système serait supérieur à l'autre est devenue inutile quand le marché du jeu s'est effondré en 1983-1984, tuant les deux consoles dans leur perfection.

## Émulateurs

[Libretro A5200](libretro-a5200)
[Libretro Atari800](libretro-atari800)