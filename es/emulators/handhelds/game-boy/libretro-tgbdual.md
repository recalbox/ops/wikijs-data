---
title: Libretro TGBDual
description: 
published: true
date: 2024-07-25T22:03:10.070Z
tags: libretro, game, boy, gameboy, tgbdual
editor: markdown
dateCreated: 2023-09-11T12:50:50.102Z
---

**Libretro Tgbdual** es un emulador que te permite ejecutar juegos de Game Boy con soporte para el cable game link.

## ![](/emulators/license.svg) Licencia

El core posee la siguiente licencia [**GPLv2**](https://github.com/libretro/tgbdual-libretro/blob/master/docs/COPYING-2.0.txt).

## ![](/emulators/compatibility.png) Compatibilidad

 | RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | 
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | 

## ![](/emulators/features.png) Funcionalidades

| Funcionalidad | ¿Soportada? |
| :---: | :---: |
| Reinicio | ✔ |
| Capturas de pantalla | ✔ |
| Grabaciones | ✔ |
| Grabaciones instantáneas | ✔ |
| Rebobinado | ✔ |
| Netplay | ✔ |
| Opciones del core | ✔ |
| Controles | ✔ |
| Remapping | ✔ |
| Softpatching | ✔ |

## ![](/emulators/bios.svg) BIOS

>**No necesita bios.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Extensiones de roms soportadas

Las roms deben tener las extensiones siguientes:

* .bin
* .gb
* .zip
* .7z

Este sistema soporta las roms comprimidas en formato .zip/.7z. No obstante, se trata sólo de un archivo.

Los ficheros contenidos en los .zip/.7z deben tener las extensiones citadas previamente.
Además, cada fichero .zip/.7z sólo debe contener un fichero rom al interior.

### Emplazamiento

Disponed vuestras roms como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 gb
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.zip**

>Se aconsejan las roms del proyecto **No-Intro**.
{.is-success}

>Para más información sobre las roms, consultad [este tutorial](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

>Para conservar las configuraciones personalizadas cuando se produce una actualización del sistema Recalbox, os aconsejamos utilizar las [Sobrecargas de configuración](./../../../advanced-usage/configuration-override).
{.is-info}

### Acceder a las opciones

Podéis configurar las opciones del core de dos formas diferentes.

* Via el Menú RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via el fichero `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Opciones del core

| Opción | Valor de la opción | Variable (para la sobrecarga) | Valores posibles |
| --- | --- | --- | --- |
| Link cable emulation (reload) | `Désactivé` ✅ / `Activé` | `tgbdual_gblink_enable` | `disabled` / `enabled` |
| Screen layout | `left-right` ✅ / `top-down` | `tgbdual_screen_placement` | `left-right` / `top-down` |
| Switch player screens | `normal` ✅ / `switched` | `tgbdual_switch_screens` | `normal` / `switched` |
| Show player screens | `both players` ✅ / `player 1 only` / `player 2 only` | `tgbdual_single_screen_mp` | `both players` / `player 1 only` / `player 2 only` |
| Audio output | `Game Boy #1` ✅ / `Game Boy #2` | `tgbdual_audoi_output` | `Game Boy #1` / `Game Boy #2` |

## ![](/emulators/external-links.png) Enlaces externos

* **Código fuente utilizado** : [https://github.com/libretro/tgbdual-libretro/](https://github.com/libretro/tgbdual-libretro)
* **Documentación Libretro** : [https://docs.libretro.com/library/tgb\_dual/](https://docs.libretro.com/library/tgb_dual/)
* **Código fuente oficial** : [http://shinh.skr.jp/tgbdualsdl/](http://shinh.skr.jp/tgbdualsdl/)
* **Página web oficial** : [http://gigo.retrogames.com/download.html\#tgb-dual](http://shinh.skr.jp/tgbdualsdl/)