---
title: Mupen64Plus RICE
description: 
published: true
date: 2024-07-05T14:50:08.130Z
tags: n64, mupen64plus, nintendo 64, rice
editor: markdown
dateCreated: 2023-09-07T06:26:55.818Z
---

## ![](/emulators/license.svg) Licencia

El core posee la siguiente licencia **MIT**.

## ![](/emulators/compatibility.png) Compatibilidad

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | 
| ❌ | ✅  🐌 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

🐌 &nbsp;Basses performances mais jouable

## ![](/emulators/features.png) Funcionalidades



## ![](/emulators/bios.svg) BIOS

>**No necesita bios.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Extensiones de roms soportadas

Las roms deben tener las extensiones siguientes:

* .n64
* .v64
* .z64

### Emplazamiento

Disponed vuestras roms como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 n64
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.n64**

>Se aconsejan las rom del proyecto **No-Intro**.
{.is-success}

>Para más información sobre las roms, consultad [este tutorial](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

>Para conservar las configuraciones personalizadas cuando se produce una actualización del sistema Recalbox, os aconsejamos utilizar las [Sobrecargas de configuración](./../../../advanced-usage/configuration-override).
{.is-info}

## ![](/emulators/external-links.png) Enlaces externos

* **Código fuente utilizado** : [https://github.com/mupen64plus/mupen64plus-video-rice/](https://github.com/mupen64plus/mupen64plus-video-rice/)
* **Documentación** : [http://www.mupen64plus.org/](http://www.mupen64plus.org/)