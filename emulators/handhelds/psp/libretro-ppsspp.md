---
title: Libretro PPSSPP
description: 
published: true
date: 2024-07-25T00:20:52.243Z
tags: libretro, psp, ppsspp, 8.1+
editor: markdown
dateCreated: 2022-07-22T10:20:31.111Z
---

**PPSSPP** is a free emulator for the PSP handheld console developed in C++.
It is available on many platforms, including Android, Windows, iOS and GNU/Linux.

It is written by **Henrik Hrydgard**.

## Requirements

* OpenGL / Open GL ES 2.0 or higher for the OpenGL renderer.
* Vulkan for the Vulkan renderer.
* Direct3D 11 for the Direct3D 11 renderer.

## ![](/emulators/license.svg) Licence

This core is under [**GPLv2**](https://github.com/libretro/ppsspp/blob/master/LICENSE.TXT) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ❌ | ❌ | ✅ | ✅ | ❌ | ❌ | ✅ | ✅ |

## ![](/emulators/features.png) Features

| Feature | Supported |
| :---: | :---: |

## ![](/emulators/bios.svg) BIOS

>**No bios is required.**
{.is-success}

## ![](/emulators/roms.png) Isos

### Supported extensions

Isos must have the extension:

* .cso
* .elf
* .iso
* .pbp
* .prx

### Location

Put your roms like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 psp
┃ ┃ ┃ ┃ ┣ 🗒 **game.iso**

>The roms using **Redump** format are highly recommended.
{.is-success}

>For more information about roms, go to [this tutorial](./../../../tutorials/games/generalities/isos-and-roms)!
{.is-info}

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Accessing to options

You can configure various options in two different ways.

* Via RetroArch menu:

┣ 📁 RetroArch Menu
┃ ┣ 📁 Core options
┃ ┃ ┣ 🧩 Name_option

* Via the file `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core options

| Options | Options values | Variables (for configuration override) | Variables values |
| --- | --- | --- | --- |
| Language | `Automatic` ✅ / `English` / `Japanese` / `French` / `Spanish` / `German` / `Italian` / `Dutch` / `Portuguese` / `Russian` / `Korean` / `Chinese Traditional` / `Chinese Simplified` | `ppsspp_language` | `-1` / `en_US` / `ja_JP` / `fr_FR` / `es_ES` / `de_DE` / `it_IT` / `nl_NL` / `pt_PT` / `ru_RU` / `koKR` / `zh_TW` / `zh_CN` |
| CPU Core | `JIT` ✅ / `IR JIT` / `Interpreter` | `ppsspp_cpu_core` | `1` / `2` / `0` |
| Fast Memory | `Disabled` / `Enabled` ✅ | `ppsspp_fast_memory` | `false` / `true` |
| Ignore bad memory accesses | `Disabled` / `Enabled` ✅ | `ppsspp_ignore_bad_memory_access` | `false` / `true` |
| IO Timing Method | `Fast` ✅ / `Host` / `Simulate UMD delays` | `ppsspp_io_timing_method` | `0` / `1` / `2` |
| Force real clock sync (Slower, less lag) | `Disabled` ✅ / `Enabled` | `ppsspp_force_lag_sync` | `false` / `true` |
| Locked CPU Speed | `Disabled` ✅ / `222MHz` / `266MHz` / `333MHz` | `ppsspp_locked_cpu_speed` | `0` / `222` / `266` / `333` |
| Cache Full ISO in RAM | `Disabled` ✅ / `Enabled` | `ppsspp_cache_iso` | `disabled` / `enabled` |
| Internal Chetas Support | `Disabled` ✅ / `Enabled` | `ppsspp_cheats` | `false` / `true` |
| PSP Model | `PSP-1000` / `PSP-2000/3000` ✅ | `ppsspp_psp_model` | `psp_1000` / `psp_2000_3000` |
| Confirmation Button | `Cross` ✅ / `Circle` | `ppsspp_button_preference` | `1` / `0` |
| Analog Circle vs Square Gate Compensation | `Désactivé` ✅ / `Activé` | `ppsspp_analog_is_circular` | `disabled` / `enabled` |
| Internal Resolution (Restart) | `480x272` ✅ / `960x544` / `1440x816` / `1920x1088` / `2400x1360` / `2880x1632` / `3360x1904` / `3840x2176` / `4320x2448` / `4800x2720` | `ppsspp_internal_resolution` | `480x272` / `960x544` / `1440x816` / `1920x1088` / `2400x1360` / `2880x1632` / `3360x1904` / `3840x2176` / `4320x2448` / `4800x2720` |
| Skip GPU Readbacks | `Disabled` ✅ / `Enabled` | `ppsspp_skip_gpu_readbacks` | `disabled` / `enabled` |
| Frameskip | `Disabled` ✅ / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` | `ppsspp_frameskip` | `Off` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` |
| Frameskip Type | `Number of frames` ✅ / `Percent of FPS` | `ppsspp_frameskiptype` | `Number of frames` / `Percent of FPS` |
| Auto Frameskip | `Disabled` ✅ / `Enabled` | `ppsspp_auto_frameskip` | `false` / `true` |
| Duplicate Frames in 30 Hz Games | `Disabled` ✅ / `Enabled` | `ppsspp_frame_duplication` | `disabled` / `enabled` |
| Detect Frame Rate Changes (Notify frontend) | `Disabled` ✅ / `Enabled` | `ppsspp_detect_vsync_swap_interval` | `disabled` / `enabled` |
| Buffered frames (Slower, less lag, restart) | `No buffer` / `Up to 1` / `Up to 2` ✅ | `ppsspp_inflight_frames` | `0` / `1` / `2` |
| Hardware Transform | `Disabled` / `Enabled` ✅ | `ppsspp_gpu_hardware_transform` | `disabled` / `enabled` |
| Software Skinning | `Disabled` / `Enabled` ✅ | `ppsspp_software_skinning` | `disabled` / `enabled` |
| Vertex Cache | `Disabled` ✅ / `Enabled` | `ppsspp_vertex_cache` | `disabled` / `enabled` |
| Lazy Texture Caching (Speedup) | `Disabled` ✅ / `Enabled` | `ppsspp_lazy_texture_caching` | `disabled` / `enabled` |
| Spline/Bezier curves quality | `Low` / `Medium` / `High` ✅ | `ppsspp_spline_quality` | `Low` / `Medium` / `High` |
| Hardware Tesselation | `Disabled` ✅ / `Enabled` | `ppsspp_hardware_tesselation` | `disabled` / `enabled` |
| Lower Resolution for Effects | `Disabled` ✅ / `Safe` / `Balanced` / `Aggressive` | `ppsspp_lower_resolution_for_effects` | `disabled` / `Safe` / `Balanced` / `Aggressive` |
| Texture Scaling Type | `xBRZ` ✅ / `Hybrid` / `Bicubic` / `Hybrid + Bicubic` | `ppsspp_texture_scaling_type` | `xbrz` / `hybrid` / `bicubic` / `hybrid_bicubic` |
| Texture Scaling Level | `Disabled` ✅ / `2x` / `3x` / `4x` / `5x` | `ppsspp_texture_scaling_level` | `disabled` / `2x` / `3x` / `4x` / `5x` |
| Texture Deposterize | `Disabled` ✅ / `Enabled` | `ppsspp_texture_deposterize` | `disabled` / `enabled` |
| Texture Shader (Vulkan only, overrides Texture Scaling Type) | `Disabled` ✅ / `2xBRZ` / `4xBRZ` / `MMPX` | `ppsspp_texture_shader` | `Off` / `Tex2xBRZ` / `Tex4xBRZ` / `TexMMPX` |
| Anisotropic Filtering | `Disabled` ✅ / `2x` / `4x` / `8x` / `16x` | `ppsspp_texture_anisotropic_filtering` | `off` / `2x` / `4x` / `8x` / `16x` |
| Texture Filtering | `Auto` ✅ / `Nearest` / `Linear` / `Auto max quality` | `ppsspp_texture_filtering` | `Auto` / `Nearest` / `Linear` / `Auto max quality` |
| Texture Replacement | `Disabled` ✅ / `Enabled` | `ppsspp_texture_replacement` | `disabled` / `enabled` |

## ![](/emulators/external-links.png) Liens externes

* **Code source utilisé** : [https://github.com/hrydgard/ppsspp](https://github.com/hrydgard/ppsspp)
* **Documentation Libretro** : [https://docs.libretro.com/library/ppsspp/](https://docs.libretro.com/library/ppsspp/)
* **Site officiel** : [http://www.ppsspp.org/](http://www.ppsspp.org/)
* **Forum officiel** : [https://forums.ppsspp.org/](https://forums.ppsspp.org/)
* **Documentation officielle** : [http://www.ppsspp.org/guides.html](http://www.ppsspp.org/guides.html)