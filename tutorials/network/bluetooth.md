---
title: Bluetooth
description: 
published: true
date: 2021-09-04T22:09:56.587Z
tags: bluetooth
editor: markdown
dateCreated: 2021-05-21T08:08:17.504Z
---

Vous pouvez avoir de petits soucis avec le Bluetooth qui peuvent être traités ici.

Voici les tutoriels disponibles :

[Changer l'adresse MAC du Bluetooth dans Raspberry Pi](change-bluetooth-mac-address-on-rpi)