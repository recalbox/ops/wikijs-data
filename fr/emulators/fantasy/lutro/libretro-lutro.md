---
title: Libretro Lutro
description: 
published: true
date: 2024-06-22T10:08:37.254Z
tags: libretro, lutro
editor: markdown
dateCreated: 2021-05-21T08:28:25.662Z
---

**Lutro** est un framework de jeu lua expérimental qui suit l'API LÖVE. Les jeux Lutro peuvent être joués avec LibRetro / RetroArch via le noyau Lutro.

## ![](/emulators/license.svg) Licence

Ce core est sous licence [**MIT**](https://github.com/libretro/libretro-lutro/blob/master/LICENSE).

## ![](/emulators/compatibility.png) Compatibilité

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Fonctionnalités

| Fonctionnalité | Supporté |
| :---: | :---: |
| Contrôles | ✔ |
| Remappage | ✔ |

## ![](/emulators/bios.svg) BIOS

>**Aucun bios n'est requis.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Extensions supportées

Les roms doivent avoir l'extension :

* .lutro
* .lua

### Emplacement

Placez les roms comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 lutro
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.lutro**

## ![](/emulators/advanced-configuration.png) Configuration avancée de l'émulateur

>**Ce core n'a pas d'option.**
{.is-success}

## ![](/emulators/external-links.png) Liens externes

* **Code source utilisé** : [https://github.com/libretro/libretro-lutro](https://github.com/libretro/libretro-lutro)
* **Documentation Libretro** : [https://docs.libretro.com/library/lutro/](https://docs.libretro.com/library/lutro/)