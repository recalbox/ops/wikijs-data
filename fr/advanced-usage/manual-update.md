---
title: Mise à jour manuelle et/ou hors connexion
description: Comment mettre à jour Recalbox manuellement, et sans connexion internet
published: true
date: 2024-09-14T15:03:02.427Z
tags: mise à jour, hors-ligne
editor: markdown
dateCreated: 2021-05-25T15:22:09.124Z
---

## Quand utiliser la mise à jour manuelle ?

Vous pouvez avoir besoin dans certains cas de déclencher une mise à jour manuelle de votre Recalbox :

- Pour mettre à jour une recalbox qui n'a pas de connexion à internet.
- Lorsque l'équipe vous demande de tester une image *de développement* pour vérifier la correction d'un bug.
- Lorsque vous avez un soucis système (très rare).

>Les exemples suivants seront utilisés avec l'image du Raspberry Pi 4, il faudra bien sur adapter ces exemples à votre board.
> {.is-info}

Pour cela, il faudra récuperer deux fichiers :

- Le **fichier image** de Recalbox, nommé `recalbox-rpi4_64.img.xz`
- Le fichier **checksum** qui contient la somme de contrôle de l'image permetant de vérifier automatiquement son intégrité. Il porte le même nom que l'image, mais avec une extension `.sha1` donc dans notre exemple `recalbox-rpi4_64.img.xz.sha1`.

## Télécharger l'image officielle

Pour mettre à jour votre Recalbox vers la dernière version disponible en mode hors ligne, rendez vous sur https://www.recalbox.com/download/stable/allimages/ :

Téléchargez ensuite l'image correspondante à votre board, et le checksum `sha1` correspondant :

![download.png](/advanced-usage/manual-update/download.png)

## Copier les fichiers sur la carte SD

Il existe 2 façons de procéder :

## {.tabset}
### Carte SD dans votre PC

Maintenant que vous avez les deux fichiers `recalbox-rpi4_64.img.xz` et `recalbox-rpi4_64.img.xz.sha1`, il vous suffit de les placer dans le répertoire `update` de la partition nommée `RECALBOX` de la carte SD sur laquelle est installée Recalbox :

![images-in-boot-partition.png](/advanced-usage/manual-update/images-in-boot-partition.png)

### Carte SD dans votre Recalbox allumée

Vous pouvez aussi placer les fichiers dans votre Recalbox en étant toujours allumée. Pour cela, il vous suffit de suivre ces étapes :

- Montez la [partition de démarrage en écriture](/tutorials/system/access/remount-partition-with-write-access).
- Placez les deux fichiers `recalbox-rpi4_64.img.xz` et `recalbox-rpi4_64.img.xz.sha1` dans le dossier `/boot/update`.
- Redémarrez.

## Lancer l'installation

L'installation se lancera automatiquement au prochain démarrage de votre Recalbox. Pour vérifier que l'installation s'est bien effectuée, vous pouvez afficher le menu avec le bouton `Start` et constater le changement de version.