---
title: No hay sonido en la salida jack con mupen64plus
description: 
published: true
date: 2023-09-20T20:45:10.526Z
tags: n64, nintendo, 64, jack, sonido, cascos
editor: markdown
dateCreated: 2023-09-20T20:45:10.526Z
---

Si estáis utilizando la toma de auriculares de vuestro Raspberry Pi como salida de sonido y el sonido no funciona con el emulador de N64 `mupen64plus`, seguid los pasos que se indican a continuación.

* Conectaros vía [SSH](./../../../system/access/root-access-terminal-cli) 
* Modificad el fichero de configuración `mupen64plus` con el siguiente comando:

`nano /recalbox/share/system/configs/mupen64/mupen64plus.cfg`

* Buscad esta línea:

```text
OUTPUT_PORT = 1
```

**Cambiadla por esta otra:**

```text
OUTPUT_PORT = 0
```

* Todo lo que tenéis que hacer ahora es guardar el fichero usando `CTRL + X` + `Y` para confirmar, y pulsad `Enter` para validar.
* Reiniciad vuestro Raspberry Pi con el comando `reboot`.

Una vez reiniciado, el sonido debería estar de nuevo disponible.

>Esta modificación también se puede relizar utilizando [WinSCP](./../../../system/access/network-access-winscp), [Cyberduck](./../../../system/access/network-access-cyberduck) o [MobaXTerm](./../../../system/access/network-access-mobaxterm) si sois alérgicos al uso del terminal.
{.is-info}