---
title: Frotz
description: 
published: true
date: 2024-06-14T14:49:30.596Z
tags: 9.0+, z-machine, frotz
editor: markdown
dateCreated: 2023-02-25T14:30:09.580Z
---

**Frotz** est un interpréteur des jeux Infocom et d'autres développeurs pour la **Z-machine**.

## ![](/emulators/license.svg) Licence

Ce core est sous licence [**GPL-2.0+**](https://gitlab.com/DavidGriffith/frotz/-/blob/master/COPYING).

## ![](/emulators/compatibility.png) Compatibilité

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Fonctionnalités

| Fonctionnalité | Supporté |
| :---: | :---: |
| Sauvegardes | ✔ |

## ![](/emulators/bios.svg) BIOS

>**Aucun bios n'est requis.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Extensions supportées

Les roms doivent avoir l'extension :

* .dat
* .zip
* .z1
* .z2
* .z3
* .z4
* .z5
* .z6
* .z7
* .z8

### Emplacement

Placez les roms comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 zmachine
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.zip**

## ![](/emulators/advanced-configuration.png) Configuration avancée de l'émulateur

> **Ce core n'a pas d'options !**
{.is-success}

## ![](/emulators/external-links.png) Liens externes

* **Code source utilisé** : [https://gitlab.com/DavidGriffith/frotz/](https://gitlab.com/DavidGriffith/frotz/)