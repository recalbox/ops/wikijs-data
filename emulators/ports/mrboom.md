---
title: MrBoom
description: 
published: true
date: 2021-09-16T04:49:09.570Z
tags: ports, mrboom
editor: markdown
dateCreated: 2021-05-21T08:04:54.518Z
---

![](/emulators/ports/mrboom.png){.align-center}

## Fiche technique

* **Développeur** : Javanaise

## Présentation

Mr.Boom est un clone de Bomberman jusqu'à 8 joueurs pour LibRetro / RetroArch

**Bomberman** est une série de jeux vidéo de Hudson Soft où le joueur incarne un poseur de bombes, le but étant de faire exploser les adversaires/ennemis pour gagner. Le jeu a connu un grand succès, surtout grâce à son mode multi-joueur qui, suivant les machines, permet de jouer jusqu'à une dizaine de personnes en même temps.

## Émulateurs

[Libretro MrBoom](libretro-mrboom)