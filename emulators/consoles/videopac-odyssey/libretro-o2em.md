---
title: Libretro O2EM
description: 
published: true
date: 2024-07-24T20:42:07.961Z
tags: libretro, o2em, odyssey2
editor: markdown
dateCreated: 2021-08-01T21:10:43.539Z
---

**Libretro O2EM** is a multi-platform open source Odyssey2 / Videopac + emulator.

## ![](/emulators/license.svg) License

This core is under [**artistic**](https://sourceforge.net/projects/o2em/) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Features

| Feature | Supported |
| :---: | :---: |
| Restart | ✔ |
| States | ✔ |
| Rewind | ✔ |
| Core Options | ✔ |
| RetroAchievements | ✔ |
| Controls | ✔ |
| Remapping | ✔ |

## ![](/emulators/bios.svg) BIOS

### List of mandatory bios

| Filename | Description | MD5 | Provided |
| :---: | :---: | :---: | :---: |
| o2rom.bin | BIOS Odyssey2 - G7000 model | 562d5ebf9e030a40d6fabfc2f33139fd | ❌ |
| c52.bin | BIOS French Videopac+ - G7000 model | f1071cdb0b6b10dde94d3bc8a6146387 | ❌ |

### Location

Put the bios like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 o2em
┃ ┃ ┃ ┃ ┣ 🗒 **o2rom.bin**
┃ ┃ ┃ ┃ ┣ 🗒 **c52.bin**

## ![](/emulators/roms.png) Roms

### supported extensions

Roms must have the extension:

* .bin
* .zip
* .7z

This system supports compressed roms in .zip/.7z format. However, be careful, it is only an archive.

The files contained in the .zip/.7z must correspond to the extensions mentioned above.
Moreover, each .zip/.7z file must contain only one compressed rom.

### Location

Put the roms like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 o2em
┃ ┃ ┃ ┃ ┣ 🗒 **game.zip**

>The roms using **No-Intro** format are highly recommended.
{.is-success}

>For more information about roms, go to [this tutorial](./../../../tutorials/games/generalities/isos-and-roms)!
{.is-info}

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Accessing to options

You can configure various options in two different ways.

* Via RetroArch menu:

┣ 📁 RetroArch Menu
┃ ┣ 📁 Core options
┃ ┃ ┣ 🧩 Name_option

* Via the file `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core options

| Options | Options values | Variables (for configuration override) | Variables values |
| --- | --- | --- | --- |
| Emulated Hardware (Restart) | `Odyssey 2 (NTSC)` ✅ / `Videopac G7000 (European)` / `Videopac+ G7400 (European)` / `Videopac+ G7400 (French)` | `o2em_bios` | `o2rom.bin` / `c52.bin` / `g7400` / `jopac.bin` |
| Console Region (Restart) | `Auto` ✅ / `NTSC` / `PAL` | `o2em_region` | `auto` / `NTSC` / `PAL` |
| Swap Gamepads | `Disabled` ✅ / `Enabled` | `o2em_swap_gamepads` | `disabled` / `enabled` |
| Virtual KBD Transparency | `0%` ✅ / `25%` / `50%` / `75%` | `o2em_vkbd_transparency` | `0` / `25` / `50` / `75` |
| Crop Overscan | `Disabled` ✅ / `Enabled` | `o2em_crop_overscan` | `disabled` / `enabled` |
| Interframe Blending | `Disabled` ✅ / `Simple` / `Ghosting (65%)` / `Ghosting (75%)` / `Ghosting (85%)` / `Ghosting (95%)` | `o2em_mix_frames` | `disabled` / `mix` / `ghost_65` / `ghost_75` / `ghost_85` / `ghost_95` |
| Audio Volume | `0%` / `10%` / `20%` / `30%` / `40%` / `50%` ✅ / `60%` / `70%` / `80%` / `90%` / `100%` | `o2em_audio_volume` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
| Voice Volume | `0%` / `5%` / `10%` / `15%` / `20%` / `25%` / `30%` / `35%` / `40%` / `45%` / `50%` / `55%` / `60%` / `65%` / `70%` ✅ / `75%` / `80%` / `85%` / `90%` / `95%` / `100%` | `o2em_voice_volume` | `0` / `5` / `10` / `15` / `20` / `25` / `30` / `35` / `40` / `45` / `50` / `55` / `60` / `65` / `70` / `75` / `80` / `85` / `90` / `95` / `100` |
| Audio Filter | `Disabled` ✅ / `Enabled` | `o2em_low_pass_filter` | `disabled` / `enabled` |
| Audio Filter Level | `5%` / `10%` / `15%` / `20%` / `25%` / `30%` / `35%` / `40%` / `45%` / `50%` / `55%` / `60%` ✅ / `65%` / `70%` / `75%` / `80%` / `85%` / `90%` / `95%` | `o2em_low_pass_range` | `5` / `10` / `15` / `20` / `25` / `30` / `35` / `40` / `45` / `50` / `55` / `60` / `65` / `70` / `75` / `80` / `85` / `90` / `95` |

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/libretro/libretro-o2em/](https://github.com/libretro/libretro-o2em/)
* **Libretro documentation**: [https://docs.libretro.com/library/o2em/](https://docs.libretro.com/library/o2em/)