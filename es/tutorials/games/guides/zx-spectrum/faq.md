---
title: FAQ
description: 
published: true
date: 2023-09-14T08:11:56.919Z
tags: zx, spectrum, faq, trdos
editor: markdown
dateCreated: 2023-09-14T08:11:56.919Z
---

## Diquetes TRDOS

* Para poder utilizar los disquetes TRDOS (_.scl ou_ .trd), hay que configurar el emulador como un "**Scorpion 256k**" dentro de las opciones de RetroArch:
  * Menú RetroArch
  * Opciones
  * Cambiar la máquina a "**Scorpion 256k**" 
  * Utilizar el teclado para validar "GLUK BOOT"
  * A continuación escribid el nombre del juego.