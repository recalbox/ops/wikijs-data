---
title: Wonderswan Color
description: 
published: true
date: 2021-09-16T06:50:17.026Z
tags: color, bandai, wonderswan, portables
editor: markdown
dateCreated: 2021-05-21T08:04:11.102Z
---

![](/emulators/handheld/wonderswancolor.svg){.align-center}

## Fiche technique

* **Fabricant** : Bandai
* **Année de sortie** : 2000
* **Quantités vendues** : 740 727
* **Processeur** : SSPGY-1002 @ 3.072 MHz 16-bit NEC V30MZ duplicate
* **Mémoire** : 64 kB partagé VRAM/WRAM
* **Résolution** : 224 x 144 pixels
* **Affichage** : FSTN reflective LCD, 2.8 in (71 mm), 241 couleurs
* **Son** : 1 haut-parleur mono, casque optionnel

## Présentation

Le **WonderSwan Color** est une console de jeux vidéo portable, créée par Bandai.

Elle est une amélioration de la WonderSwan originale. Elle sort le 30 décembre 2000 au Japon et rencontre un assez grand succès, prenant jusqu'à 8 % du marché japonais, dominé par Nintendo et ses différentes Game Boy. La WonderSwan Color est compatible avec les jeux de la WonderSwan originale.

En novembre 2002, Bandai sort une évolution de la WonderSwan Color, sous le nom de SwanCrystal, bénéficiant d'un meilleur écran.

## Émulateurs

[Libretro Mednafen_WSwan](libretro-mednafen-wswan)