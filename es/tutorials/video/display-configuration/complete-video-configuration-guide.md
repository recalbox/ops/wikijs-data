---
title: Guía completa sobre la configuración vídeo
description: Cómo modificar las resoluciones de pantalla
published: true
date: 2024-04-20T11:58:05.686Z
tags: vídeo, configuración, guía
editor: markdown
dateCreated: 2023-09-14T12:00:25.443Z
---

## Introducción

En Recalbox, podéis configurar diferentes resoluciones de pantalla en función de vuestras necesidades:

 - Resolución global (de todo el sistema)
 - Resolución de la pantalla de la interfaz
 - Resolución de la pantalla de cada sistema (incluyendo Kodi)

Antes, para definir una resolución de pantalla, había que utilizar los modos de video que sólo existen en los Raspberry PI. Esto ha cambiado, ahora es mucho más sencillo y accesible a todo el mundo y para todos los hardware que Recalbox soporta.

Para definir una resolución de pantalla, simplemente tenéis que indicar la resolución en pixels. Por ejemplo:

```ini
global.videomode=1280x1080
```

## Configuración manual

### Configuración de la resolución global

Para establecer la resolución global a utilizar, es necesario cambiar el valor de `global.videomode` en el fichero `recalbox.conf`.

```ini
global.videomode=1280x1080
```

También podéis utilizar el valor `default` para usar la resolución nativa de la pantalla.

>Si la línea `global.videomode` no existe, deberéis añadirla en una nueva línea.
{.is-info}

### Configuración de la resolución de la interfaz

Para establecer la resolución de la interfaz de Recalbox (EmulationStation), es necesario cambiar el valor de `es.videomode` en el fichero `recalbox.conf`.

```ini
es.videomode=1280x1080
```

También podéis utilizar el valor `default` para usar la resolución nativa de la pantalla.

>Si la línea `es.videomode` no existe, hay que añadirla en una nueva línea.
{.is-info}

### Configuración de la resolución por sistema

Para configurar la resolución de un sistema en particular, es necesario cambiar el valor que corresponde a dicho sistema. Ejemplos:

```ini
nes.videomode=640x480
snes.videomode=1280x1024
```

También podéis utilizar el valor `default` para user la resolución nativa de la pantalla.

>Si la línea system no existe, hay que añadirla en una nueva línea.
{.is-info}

## Configuración práctica

Con excepción de la versión PC de Recalbox, podéis configurar las resoluciones desde la interfaz de EmulationStation pulsando `START` y yendo a `Advanced settings` > `Resolutions`.
