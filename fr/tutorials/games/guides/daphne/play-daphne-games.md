---
title: Jouer aux jeux Daphné
description: 
published: true
date: 2024-09-24T06:17:14.848Z
tags: daphne, jeux
editor: markdown
dateCreated: 2021-08-08T15:07:15.471Z
---

## Liste des jeux Daphné

Les jeux Daphné sont les jeux qui n'utilisent pas d'extension tel que Singe.

Voici la liste des jeux Daphné :

* Astron Belt
* Badlands
* Bega's Battle
* Cliff Hanger
* Cobra Command
* Dragon's Lair
* Dragon's Lair II : Time Warp
* Esh's Aurunmilla
* Firefox
* Galaxy Ranger
* GP World
* Interstellar Laser Fantasy
* M.A.C.H. 3
* Road Blaster
* Space Ace
* Super Don Quix-Ote
* Thayer's Quest
* Us Vs Them

## Installation des jeux

Chaque jeu se sépare en 2 parties :

* Les données pour faire fonctionner le jeu (vidéos, musiques, etc.)
* Les données du jeu (fichier zip)

### Pour les données du jeu

Les données du jeu sont, pour chacun, contenu dans un fichier `.ZIP` avec des extensions spécifiques. Ces fichiers ne sont pas à ouvrir et doivent avoir le même nom que le dossier `nomdujeu.daphne`.

Vous devez les placer dans le répertoire suivant :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┃ ┃ ┣ 🗒 nomdujeu.zip

>La majorité des fichiers dans ce dossier font moins de 100ko à quelques rares exceptions. Il ne correspond pas au contenu dans un dossier `.daphne` compressé !
{.is-info}

### Pour les données pour faire fonctionner le jeu

Le contenu suivant est requis :

* 1 fichier `nomdujeu.commands` (facultatif)
* 1 fichier `nomdujeu.dat` (facultatif)
* 1 ou plusieurs fichiers `nomdujeu.m2v`
* 1 ou plusieurs fichiers `nomdujeu.m2v.bf` (facultatif)
* 1 ou plusieurs fichiers `nomdujeu.ogg`
* 1 ou plusieurs fichiers `nomdujeu.ogg.bf` (facultatif)
* 1 fichier `nomdujeu.txt`

Vous devez les placer dans le répertoire suivant :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 nomdujeu.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 nomdujeu.commands
┃ ┃ ┃ ┃ ┃ ┣ 🗒 nomdujeu.dat
┃ ┃ ┃ ┃ ┃ ┣ 🗒 nomdujeu.m2v
┃ ┃ ┃ ┃ ┃ ┣ 🗒 nomdujeu.ogg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 nomdujeu.txt
┃ ┃ ┃ ┃ ┃ ┣ 🗒 ...

>Certains noms de fichiers peuvent ne pas avoir les mêmes noms que le jeu comme les fichiers `.DAT`, `.M2V` et `.OGG`. Ceci n'est pas un problème en soi, du moment que les fichier `.M2V` sont correctement répertoriées dans le fichier `.TXT`, je jeu devrait fonctionner sans problème.
{.is-info}

## Emplacement des fichiers par jeu

Comme il est particulièrement difficile de placer les bons fichiers aux bons endroits, vous allez voir ci-dessous comment mettre les fichiers nécessaires de chaque jeu dans l'arborescence de Recalbox.

>Tout se qui se trouve en dessous n'est qu'un exemple d'un romset fonctionnel, le votre peut être fonctionnel et différent.
{.is-info}

## {.tabset}
### Astron Best

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 astron.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 astron.commands
┃ ┃ ┃ ┃ ┃ ┣ 🗒 astron.txt
┃ ┃ ┃ ┃ ┃ ┣ 🗒 astron512.m2v
┃ ┃ ┃ ┃ ┃ ┣ 🗒 astron512.ogg
┃ ┃ ┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┃ ┃ ┣ 🗒 astron.zip

### Badlands

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 badlands.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 badlands-pc.m2v
┃ ┃ ┃ ┃ ┃ ┣ 🗒 badlands-pc.ogg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 badlands.commands
┃ ┃ ┃ ┃ ┃ ┣ 🗒 badlands.txt
┃ ┃ ┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┃ ┃ ┣ 🗒 badlands.zip

### Bega's Battle

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 bega.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 bega.commands
┃ ┃ ┃ ┃ ┃ ┣ 🗒 bega.m2v
┃ ┃ ┃ ┃ ┃ ┣ 🗒 bega.ogg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 bega.txt
┃ ┃ ┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┃ ┃ ┣ 🗒 bega.zip

### Cliff Hanger

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 cliff.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 cliff.m2v
┃ ┃ ┃ ┃ ┃ ┣ 🗒 cliff.ogg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 cliff.txt
┃ ┃ ┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┃ ┃ ┣ 🗒 cliff.zip

### Cobra Command

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 cobra.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 cc-frame53930_to_end.m2v
┃ ┃ ┃ ┃ ┃ ┣ 🗒 cobra.commands
┃ ┃ ┃ ┃ ┃ ┣ 🗒 cobra.m2v
┃ ┃ ┃ ┃ ┃ ┣ 🗒 cobra.ogg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 cobra.txt
┃ ┃ ┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┃ ┃ ┣ 🗒 cobra.zip

### Dragon's Lair

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 lair.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 lair.commands
┃ ┃ ┃ ┃ ┃ ┣ 🗒 lair.m2v
┃ ┃ ┃ ┃ ┃ ┣ 🗒 lair.ogg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 lair.txt
┃ ┃ ┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┃ ┃ ┣ 🗒 lair.zip

### Dragon's Lair II : Time Warp

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 lair2.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 dl2-00001.m2v
┃ ┃ ┃ ┃ ┃ ┣ 🗒 dl2-00001.m2v.bf
┃ ┃ ┃ ┃ ┃ ┣ 🗒 dl2-00001.ogg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 dl2-00001.ogg.bf
┃ ┃ ┃ ┃ ┃ ┣ 🗒 ...
┃ ┃ ┃ ┃ ┃ ┣ 🗒 dl2-44550.m2v
┃ ┃ ┃ ┃ ┃ ┣ 🗒 dl2-44550.m2v.bf
┃ ┃ ┃ ┃ ┃ ┣ 🗒 dl2-44550.ogg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 dl2-44550.ogg.bf
┃ ┃ ┃ ┃ ┃ ┣ 🗒 lair2.commands
┃ ┃ ┃ ┃ ┃ ┣ 🗒 lair2.txt
┃ ┃ ┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┃ ┃ ┣ 🗒 lair2.zip

### Esh's Aurunmilla

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 esh.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 esh.m2v
┃ ┃ ┃ ┃ ┃ ┣ 🗒 esh.ogg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 esh.txt
┃ ┃ ┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┃ ┃ ┣ 🗒 esh.zip

### Firefox

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 firefox.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 firefox.m2v
┃ ┃ ┃ ┃ ┃ ┣ 🗒 firefox.ogg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 firefox.txt
┃ ┃ ┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┃ ┃ ┣ 🗒 firefox.zip

### Galaxy Ranger

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 galaxy.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 galaxy.m2v
┃ ┃ ┃ ┃ ┃ ┣ 🗒 galaxy.ogg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 galaxy.txt
┃ ┃ ┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┃ ┃ ┣ 🗒 galaxy.zip

### GP World

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 gpworld.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 gpdiags.m2v
┃ ┃ ┃ ┃ ┃ ┣ 🗒 gpdiags.ogg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 gpworld.m2v
┃ ┃ ┃ ┃ ┃ ┣ 🗒 gpworld.ogg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 gpworld.txt
┃ ┃ ┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┃ ┃ ┣ 🗒 gpworld.zip

### Interstellar Laser Fantasy

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 interstellar.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 interstellar.commands
┃ ┃ ┃ ┃ ┃ ┣ 🗒 interstellar.m2v
┃ ┃ ┃ ┃ ┃ ┣ 🗒 interstellar.ogg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 interstellar.txt
┃ ┃ ┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┃ ┃ ┣ 🗒 interstellar.zip

### M.A.C.H. 3

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 mach3.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 mach3.commands
┃ ┃ ┃ ┃ ┃ ┣ 🗒 mach3.m2v
┃ ┃ ┃ ┃ ┃ ┣ 🗒 mach3.ogg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 mach3.txt
┃ ┃ ┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┃ ┃ ┣ 🗒 mach3.zip

### Road Blaster

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 roadblaster.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 roadblaster.m2v
┃ ┃ ┃ ┃ ┃ ┣ 🗒 roadblaster.ogg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 roadblater.txt
┃ ┃ ┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┃ ┃ ┣ 🗒 roadblaster.zip

### Space Ace

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 ace.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 ace.commands
┃ ┃ ┃ ┃ ┃ ┣ 🗒 ace.m2v
┃ ┃ ┃ ┃ ┃ ┣ 🗒 ace.ogg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 ace.txt
┃ ┃ ┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┃ ┃ ┣ 🗒 ace.zip

### Super Don Quix-Ote

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 sdq.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 sdq-blank.m2v
┃ ┃ ┃ ┃ ┃ ┣ 🗒 sdq-colorbars.m2v
┃ ┃ ┃ ┃ ┃ ┣ 🗒 sdq-crosshairs.m2v
┃ ┃ ┃ ┃ ┃ ┣ 🗒 sdq.commands
┃ ┃ ┃ ┃ ┃ ┣ 🗒 sdq.m2v
┃ ┃ ┃ ┃ ┃ ┣ 🗒 sdq.ogg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 sdq.txt
┃ ┃ ┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┃ ┃ ┣ 🗒 sdq.zip

### Thayer's Quest

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 tq.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 tq.m2v
┃ ┃ ┃ ┃ ┃ ┣ 🗒 tq.ogg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 tq.txt
┃ ┃ ┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┃ ┃ ┣ 🗒 tq.zip

### Us Vs Them

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 uvt.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 uvt-170606.m2v
┃ ┃ ┃ ┃ ┃ ┣ 🗒 uvt-170606.ogg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 uvt.commands
┃ ┃ ┃ ┃ ┃ ┣ 🗒 uvt.txt
┃ ┃ ┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┃ ┃ ┣ 🗒 uvt.zip