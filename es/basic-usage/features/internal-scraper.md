---
title: Scrapper interno
description: 
published: true
date: 2025-02-08T12:08:05.633Z
tags: scrapper, interno
editor: markdown
dateCreated: 2023-08-24T15:27:47.399Z
---

## Introducción

Esta vez, no nos hemos andado con chiquitas y hemos reescrito completamente el scrapper interno partiendo desde cero. No queda nada del antiguo sistema, ni una migaja.

El resultado es un scrapper nuevo, limpio, rápido, con muchas opciones configurables y _**compatible con las opciones de vuestra cuenta ScreenScraper**_ (número de threads, cuotas, ...)

![](/basic-usage/features/internalscraper1.png)

## Utilización

* En la lista de sistemas de EmulationStation, pulsado el botón `START` y dirigiros dentro de `SCRAPPER`.
* Aquí, se os presentan varias opciones de scrapping:
	* `SCRAPER DESDE`: permite seleccionar la fuente de scrapping.
  * `OPCIONES DEL SCRAPPER`: permite configurar varias opciones:
  	* `TIPO DE IMAGEN`: permite elegir el tipo de image de presentación de los juegos.
    * `TIPO DE VIDEO`: selecciona si se recuperan los vidéos originales de ScreenScraper o los vídeos normalizados que ocupan menos espacio en disco.
    * `TIPO DE VIÑETA`: permite elegir entre diferentes formatos.
    * `REGION DEL JUEGO`: permite elegir entre la región preferida por el usuario o la región detectada.
    + `REGION PREFERIDA`: permite configurar vuestra región preferida.
    * `IDIOMA PREFERIDO`: permite configurar el lenguage que se utiliza en las descripciones de los juegos.
    * `DESCARGAR LOS MANUALES`: decide si se descargan o no los manuales de los juegos disponibles en ScreenScraper.
    * `DESCARGAR LOS MAPAS`: decide si se descargan los mapas de juegos disponibles en ScreenScraper.
    * `DESCARGAR LAS CONFIGURACIONES PAD - TECLADO`: permite descargar los ficheros de configuración p2k para poder jugar à los juegos que necesitan un teclado directamente con el mando. Muy útil para los juegos de ordenadores antiguos.
    * `NOMBRE DE USUARIO`: permite indicar vuestro login de ScreenScraper.
    * `CONTRASEÑA`: permite indicar la contraseña de ScreenScraper.
  * `OBTENER LOS NOMBRES A PARTIR DE`: permite elegir de dónde vienen los nombres de los juegos que aparecerán en la interfaz de EmulationStation.
  * `FILTRO`: permite elegir entre scrapper toda la colleción o sólamente los juegos que no disponen de medias.
  * `SISTEMAS`: permite seleccionar los sistemas que queréis scrapar.
* Cuando habéis terminado, pulsad el botón `SCRAPAR AHORA` para lanzar el scrapping.

El tiempo de scrapping puede variar entre unos minutos y varias horas, dependiendo de múltiples factores como las opciones elegidas anteriormente, la cantidad de juegos a scrappar, el numéro de threads disponibles en vuestra cuenta de ScreenScraper y vuestra quota diaria, el ancho de banda de vuestra conexión a internet, etc.

## Cuotas

Hablando de cuotas en la introducción, es importante entender cómo funcionan. Cuando desguaces un juego, vas a utilizar una cierta cantidad de tu cuota, no es 1 juego = 1 petición que afecta a la cuota.

¡Scrapear un juego = 1 petición de cuota para toda la información textual **Y** 1 petición de cuota **POR CADA IMAGEN/VIDEO** que exista!

Si tienes un juego con 1 imagen, 1 miniatura, 1 p2k, 1 vídeo, 1 mapa y 1 manual y todo está disponible, utilizarás 7 peticiones de tu cuota. De una cuota de 15.000 al día, para este juego, pasarás a una cuota de 14.993.