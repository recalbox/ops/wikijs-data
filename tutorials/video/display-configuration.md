---
title: Display configuration
description: 
published: true
date: 2024-07-25T19:12:12.131Z
tags: configuration, display
editor: markdown
dateCreated: 2021-08-06T17:07:53.278Z
---

Here you will find tutorials on how to configure the display.

Here are the available tutorials:

[Complete video configuration guide](complete-video-configuration-guide)
[Configure an external screen](external-screen-configuration)