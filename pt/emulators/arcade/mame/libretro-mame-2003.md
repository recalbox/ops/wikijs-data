---
title: Libretro MAME 2003
description: 
published: true
date: 2024-06-01T10:37:28.548Z
tags: libretro, mame, mame2003
editor: markdown
dateCreated: 2021-09-29T00:27:55.821Z
---

**Libretro MAME2003** é um emulador mais novo que o imame4all. Muitos outros jogos são funcionais.

## ![](/emulators/license.svg) Licença

Este núcleo é licenciado sob uma licença [**MAME não-comercial**](https://github.com/libretro/mame2003-libretro/blob/master/LICENSE.md).

## ![](/emulators/compatibility.png) Compatibilidade

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Recursos

| Recurso | Suportado |
| :---: | :---: |
| Saves | ✔ |
| Savestates | Depende do jogo |
| Rebobinar | ✔ |
| Netplay | ✔ |
| Trapaças Nativas | ✔ |
| Controles | ✔ |
| Multi-Mouse | ✔ |

## ![](/emulators/bios.svg) BIOS

Os Romsets BIOS não são necessários quando se utilizam romsets de arcade "Full Non-Merged" (Completamente Não-Mesclado). Para romsets "Split"(divididos) e "Non-Merged"(Não-mesclado), coloque a BIOS na mesma pasta que o romset do jogo.

>**Nota** :
>Coloque as BIOS na pasta: `/recalbox/share/roms/mame/`   
>ou sua subpasta se você estiver utilizando múltiplos núcleos Mame, por exemplo: `/recalbox/share/roms/mame 2003/`
{.is-warning}

## ![](/emulators/roms.png) Roms

* Baseado no romset: MAME 0.78 (dezembro de 2003)
* Tamanho: 28gb
* Romsets emulados: 4721 (incluindo clones, etc.)
* Conjuntos ativos: 4721
* Pais: 1042
* Clones: 2039
* Outros: 1624
* BIOS: 16
* CHDs: 30
* Versão Chds : 3
* Amostras: 2013
* Arquivo dat: [mame2003.xml](https://github.com/libretro/mame2003-libretro/blob/master/metadata/mame2003.xml)


### Local

Coloque as roms assim:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 mame
┃ ┃ ┃ ┃ ┣ 🗒 **jogo.zip**
┃ ┃ ┃ ┃ ┣ 📁 **jogo**
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **jogo.chd**

Você também pode optar por uma subpasta (útil se você quiser ter um Set MAME para outro núcleo).

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 mame
┃ ┃ ┃ ┃ ┣ 📁 Mame 2003
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **jogo.zip**
┃ ┃ ┃ ┃ ┃ ┣ 📁 **jogo**
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 **jogo.chd**

## ![](/emulators/advanced-configuration.png) Configuração avançada do emulador

> Para poder manter suas configurações personalizadas durante uma atualização, aconselhamos que você use nosso recurso de [Sobrecarga de configuração](./../../../advanced-usage/configuration-override).
{.is-info}

### Acessar as Opções

Você pode configurar várias opções de duas maneiras diferentes.

* Por meio do Menu RetroArch:

┣ 📁 Menu RetroArch
┃ ┣ 📁 Opções do núcleo
┃ ┃ ┣ 🧩 Nome_da_opção

* Por meio do arquivo `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Opções de núcleo

## ![external-links.png](/emulators/external-links.png) Links externos

* **Progettosnaps** : [https://www.progettosnaps.net/](https://www.progettosnaps.net/)
* **Código fonte utilizado** : [https://github.com/libretro/mame2003-libretro/](https://github.com/libretro/mame2003-libretro)
* **Documentação Libretro** : [https://docs.libretro.com/library/mame_2003/](https://docs.libretro.com/library/mame_2003/)
