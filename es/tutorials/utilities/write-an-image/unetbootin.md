---
title: UNetbootin
description: 
published: true
date: 2023-09-17T07:33:31.878Z
tags: unetbootin
editor: markdown
dateCreated: 2023-09-17T07:33:31.878Z
---

## Funcionalidades

[UNetbootin](https://unetbootin.github.io/) puede crear una llave USB Live de arranque.

Descarga los ficheros ISO (imagen de CD) para varias distribuciones automáticamente, o también podéis [usar un fichero ISO que ya hayáis descargado](https://unetbootin.github.io/#other).

![](/tutorials/utilities/image-write/unetbootin/unetbootin1.jpg){.full-width}

## Utilizar Unetbootin

Seleccionad un fichero ISO o una distribución para descargar, elejid una unidad de destino (llave USB o disco duro) y reiniciad cuando el programa haya terminado. Si no se reconoce vuestra llave USB, formateadla en formato FAT32.

![](/tutorials/utilities/image-write/unetbootin/unetbootin2.jpg){.full-width}

![](/tutorials/utilities/image-write/unetbootin/unetbootin3.jpg){.full-width}

![](/tutorials/utilities/image-write/unetbootin/unetbootin4.jpg){.full-width}

Si utilizáis el modo de instalación "llave USB": tras reiniciar, [arrancad desde llave USB](http://pcsupport.about.com/od/tipstricks/ht/bootusbflash.htm).

En los PC, suele bastar con pulsar la tecla Esc (o F12, o F9) inmediatamente después de reiniciar el ordenador, mientras que en los Mac es necesario mantener pulsada la tecla Opción (⌥) antes de iniciar macOS.

Si utilizáis el modo de instalación "Disco duro": tras reiniciar, seleccionad la entrada UNetbootin del menú de arranque.

## Instructions para su borrado (se aplica sólo en las instalaciones en disco duro)

Si utilizáis Windows, UNetbootin debería ofreceros eliminarlo la próxima vez que inicies Windows. También podéis eliminarlo mediante la opción "Agregar o quitar programas" del Panel de control.

Si utilizáis Linux, relanzad el ejecutable de UNetbootin (con privilegios de administrador) y pulsad Aceptar cuando os pida desinstalarlo.

La desinstalación sólo es necesaria si se ha utilizado el modo de instalación "Disco duro"; para eliminar el gestor de arranque de una llave USB, haced una copia de su contenido, y, a continuación, volved a formatearla.

Desinstalar UNetbootin simplemente elimina la entrada UNetbootin del menú de arranque; si habéis instalado un sistema operativo en una partición usando UNetbootin, borrar UNetbootin no borrará el SO.

Para eliminar el arranque manualmente de una instalación Linux, hay que restaurar el gestor de arranque de Windows usando "fixmbr" desde un CD de recuperación y usar Parted Magic para borrar la partición Linux y redimensionar la partición Windows.

## {.tabset}

### Licencia

UNetbootin fue creado y escrito por [Geza Kovacs](http://www.gkovacs.com/) (Github: [gkovacs](http://github.com/gkovacs), Launchpad: [gezakovacs](https://launchpad.net/~gezakovacs), [información de contacto](http://wiki.ubuntu.com/GezaKovacs)).

Los traductores aparecen en la [página de traducciones](https://github.com/unetbootin/unetbootin/wiki/translations).

UNetbootin se distribuye bajo [GNU General Public License (GPL) versión 2](http://www.gnu.org/licenses/old-licenses/gpl-2.0.html) o [superior](http://www.gnu.org/copyleft/gpl.html). El contenido del sitio (documentación, capturas de pantalla y logotipos) se distribuye bajo [Creative Commons - Share Alike 3.0 licence](http://creativecommons.org/licenses/by-sa/3.0/).
