---
title: RetroArch
description: 
published: true
date: 2023-09-16T12:30:46.141Z
tags: retroarch
editor: markdown
dateCreated: 2021-09-12T14:06:17.273Z
---

Recalbox usa o [**RetroArch**](https://github.com/libretro/RetroArch), criado por Twinaphex, como uma interface para emuladores.

O RetroArch é o programa que executa a maioria dos emuladores do Recalbox.

## O que é isso?

O Retroarch é um front-end (a interface utilizada para interagir) da Libretro, o que significa que ele pode executar o conteúdo dos núcleos Libretro, ele também unifica a maneira como esses núcleos trabalham com o sistema.

Um núcleo é um programa adaptado para trabalhar com front-ends Libretro, neste caso, a maioria são emuladores.

## Características

O RetroArch, em sua natureza, é um programa que inicia outros pequenos programas, no entanto, ele também lhes diz como os controles são configurados (assim você só precisa configurar seu controle uma vez para todos os núcleos, ao invés de uma vez por núcleo), os recursos adicionados estão geralmente disponíveis em vários núcleos, e podem ser acessados da mesma forma para cada um dos núcleos. 

Em resumo, ele estabelece um padrão que os núcleos seguirão.

A interface do RetroArch permite que muitos parâmetros sejam alterados, mas na maioria dos casos os padrões devem ser corretos. Entretanto, as opções centrais, por exemplo, podem permitir que você mude a região do sistema em que você está jogando atualmente, se necessário. Como de costume, estas configurações não devem ser alteradas sem alguma reflexão preliminar.

Também inclui as características básicas oferecidas por um emulador (tais como *savestates* (salvamentos rápidos), capturas de tela, a troca de discos para sistemas baseados em CD, ...), bem como configurações mais avançadas (shaders, rebobinar e avanço rápido, netplay, ...), algumas das quais podem ser acessadas usando **combinações com a Hotkey (tecla de atalho)**.

> Não é recomendado modificar as opções padrão do RetroArch diretamente através de sua interface, pois essas modificações não sobrevivem a uma "restauração dos parâmetros de fábrica" do Recalbox e podem gerar problemas. Se você precisa modificar as opções do RetroArch, é recomendado usar as [sobrecargas de configuração](/advanced-usage/configuration-override).
{.is-warning}

## Requisitos

Embora o RetroArch e oRecalbox tentem tornar as coisas o mais simples possíveis para o usuário final, alguns núcleos têm requisitos para funcionar corretamente, tais como BIOS ou o formato de arquivo correto para um jogo; o webmanager pode lhe dizer quais BIOS você tem ou precisa para os vários sistemas emulados, e os arquivos `readme.txt` dentro de cada sistema lhe dirão que formatos são suportados para cada sistema.

Também pedimos que você compreenda o que está fazendo para certos sistemas (como o arcade, por exemplo), pois adicionar arquivos aleatórios e esperar que funcione pode simplesmente não funcionar, dependendo da situação.