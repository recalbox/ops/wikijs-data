---
title: FAQ
description: 
published: true
date: 2023-08-17T08:59:07.057Z
tags: faq, ps2, 8.0+
editor: markdown
dateCreated: 2021-10-12T20:55:09.421Z
---

## Is there a set of key combinations for PCSX2?

Yes, here is the list of these combinations:

- `HK` + `Up`/`Down`: select slot
- `HK` + `X`/`Y`: load/save state
- `HK` + `Left`: toggle slow mode
- `HK` + `Right`: toggle fast mode
- `HK` + `L1`: cycle in ratio (4:3, 16:9, fullscreen stretch)
- `HK` + `R1`: screenshot
- `Select` + `Start`: quit PCSX2.

## Is it possible to display the FPS counter in-game?

Yes, you need to do the following:

- Start any game, you'll need to quit at some point
- When the game has started, press Escape on your keyboard, and you'll be taken to the PCSX2 graphical interface
- Click on `Config` > `Graphics settings`
- In the window, click on `OSD` > `Enable monitor` and click on `OK`
- Exit PCSX2
- Start the desired game