---
title: In game
description: When you are playing and if the emulator allows it, it is possible to use some special commands to improve your game experience.
published: true
date: 2024-11-11T07:53:22.070Z
tags: game, special commands
editor: markdown
dateCreated: 2021-06-28T22:51:14.174Z
---

>In Recalbox, the shortcuts are, for the most part, a combination of keys involving a key named **HOTKEY** (`HK`) which must first be defined in the controller's settings options.
>
>The names used for the buttons are those of the Super Nintendo controller (the `L1`, `R1`, `L2`, `R2`, `L3` and `R3` buttons are based on any recent controller).
>
>See [this page](./../../first-use-and-configuration#configuring-a-controller) for details.
{.is-info}

![](/basic-usage/getting-started/special-commands/controllers.png)

## Basic commands

### Back to Gamelist

Exit the current game to return to the game list:

* `HK` + `START`

### Add credits [Insert Coin].

Add credit for arcade and NeoGeo games:

* `SELECT`

### Restart the game [Reset]

Restart the current game:

* `HK` + `A`

### Rewind [Rewind]

Rewind the game:

* `HK` + `LEFT`

### Fast Forward

Fast forward the game:

* `HK` + `RIGHT`

## Save

### Save [Save State]

Create a save point in the current location:

* `HK` + `Y`

### Load a backup [Load State]

Load the last backup point in the current location:

* `HK` + `X`

### Change the backup slot [Select Slot].

Select a backup slot from the 10 available slots (per set, numbered 0 to 9):

* `HK` + `UP` or `DOWN`

## Change CD

### Eject CD [Disk Eject]

Allows you to simulate the ejection or insertion of a CD into the console (a function intended for games consisting of several CDs or disks):

* `HK` + `L. STICK` to the _UP_.

### Change CDs [Disk Swap]

>To be able to swap disks, all the images in your game must be in a `.M3U` file.
{.is-info}

Selects the previous or next disc (a function for games consisting of multiple CDs or disks):

* `HK` + `L. STICK` to the _LEFT_ or to the _RIGHT_

>To summarize simply how to change CDs in 3 steps:
>
>1. _**I press Eject and the drawer opens:**_
>* `HK` + `L. STICK` to the _UP_. 
>2. _**I change CDs (-1, -2, ... or +1, +2, ...):**_
>* `HK` + `L. STICK` to the _LEFT_ or to the _RIGHT_.
>3. _**I press Eject, the drawer closes and I continue the game:**_
>* `HK` + `L. STICK` to the _UP_. 
{.is-success}

## Display

### Screenshots [Screenshot]

Take a screenshot of the current screen (.png file saved in the `screenshots` folder):

* `HK` + `L1`

### Recording video [Recording]

Record a video sequence (file saved in the `screenshots` folder):

* Press `HK` + `R3` (Press `R. Stick`)

>The 1st press starts the recording and the 2nd stops the recording.
{.is-info}

### Change the shader [Shaders Swap]

Change the video filter (shader) displayed on the screen:

* `HK` + `L2` or `R2`

### Translate [Translate]

Translate "on the fly" and overlay the text displayed on the screen:

* `HK` + `L. STICK` to the _DOWN_

>Requires an Internet connection to operate. More information on [Retroarch AI (In-game translation)](./../../features/retroarch-ai-ingame-translation)
{.is-warning}

### Display FPS [FPS Display]

Display the number of frames per second (FPS) currently being rendered:

* `HK` + `R. STICK` to the _DOWN_

## Advanced user

### Access the RetroArch menu [RetroArch Menu].

Access the RetroArch menu:

* `HK` + `B`

>If you want to change and save the RetroArch configuration, consider enabling the "**Save settings on Exit**" option in the RetroArch menu.  
>Once this option is enabled, **all changes** made in this menu will be saved before you exit the menu.
{.is-info}

## Retroflag GPi Case

![](/basic-usage/getting-started/special-commands/gpicase.png)