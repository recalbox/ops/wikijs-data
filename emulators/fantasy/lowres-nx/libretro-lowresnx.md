---
title: Libretro LowResNX
description: 
published: true
date: 2024-07-24T20:50:31.730Z
tags: libretro, lowresnx, 8.0+
editor: markdown
dateCreated: 2021-09-12T08:50:09.929Z
---

**Libretro LowResNX** is a port of the emulator **LowRes NX**.

## ![](/emulators/license.svg) License

This core is under [**Zlib**](https://github.com/timoinutilis/lowres-nx/blob/master/LICENSE) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Features

| Feature | Supported |
| :---: | :---: |

## ![](/emulators/bios.svg) BIOS

>**No bios is required.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Supported extensions

Roms must have the extension:

* .nx
* .zip
* .7z

The files contained in the .zip/.7z must correspond to the extensions mentioned above.

### Location

Put the roms like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 lowresnx
┃ ┃ ┃ ┃ ┣ 🗒  **game.nx**

>For more information about roms, go to [this tutorial](./../../../tutorials/games/generalities/isos-and-roms)!
{.is-info}

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>**This core have no option.**
{.is-success}

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/timoinutilis/lowres-nx/](https://github.com/timoinutilis/lowres-nx/)
* **Official website**: [https://lowresnx.inutilis.com/](https://lowresnx.inutilis.com/)
* **Official documentation**: [https://lowresnx.inutilis.com/help.php](https://lowresnx.inutilis.com/help.php)