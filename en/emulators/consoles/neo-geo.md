---
title: Neo-Geo
description: 
published: true
date: 2021-08-09T20:13:04.046Z
tags: consoles, snk, neo-geo
editor: markdown
dateCreated: 2021-07-28T05:24:06.107Z
---

![](/emulators/consoles/neogeo.svg){.align-center}

## Technical data

* **Manufacturer**: SNK Corporation
* **Year of release**: 1990
* **Units sold**: 1 million
* **Best-selling game**: The King of Fighters '95
* **CPU**: Motorola 68000 @ 12MHz, Zilog Z80A @ 4MHz
* **RAM**: 64KB
* **Audio RAM**: 2KB
* **Video RAM**: 84KB
* **Display**: 320×224 resolution, 4096 colors out of 65536
* **Sound chip**: Yamaha YM2610

## Presentation

The **Neo-Geo**, also called **Neo-Geo Advanced Entertainment System** or **Neo-Geo AES**, is a video game console released in 1990.

Designed by the Japanese company SNK, it is technically identical to the Neo-Geo MVS arcade system, with which it shares a common game library. The console is known for its library of quality 2D arcade games, many of which are fighting games, as well as for its very high price and its very long life. Considering its price and its characteristics compared to the competing consoles of the same period, it is considered by some observers as the "Rolls Royce" of consoles.

The Neo-Geo is known for its quality games that continue to be enjoyed many years after their release. Among the hundred and fifty games released, several game series stood out:

* Art of Fighting (combat)
* Fatal Fury (combat)
* King of Fighters (combat)
* Magical Drop (jeu de puzzle)
* Metal Slug (run and gun)
* Samurai Shodown (combat au sabre)

### Neo-Geo MVS

The **Neo-Geo MVS** or **Neo-Geo Multi Video System** is a JAMMA-compatible arcade video game system created by the Japanese company SNK in parallel with the Neo-Geo AES home console, sharing the same hardware base. The quality of the games and the winning technical choices will make Neo-Geo a success, one of the most famous and best-selling arcade systems in the world.

## Emulators

[FBA2x](fba2x)
[Libretro FBNeo](libretro-fbneo)
[Libretro MAME2000](libretro-mame2000)
[Libretro MAME2003](libretro-mame2003_plus)
[Libretro MAME2003_Plus](libretro-mame2003_plus)
[Libretro MAME2010](libretro-mame2010)
[Libretro MAME2015](libretro-mame2015)

## External links

* [Wikipedia : Neo-Geo AES](https://fr.wikipedia.org/wiki/Neo-Geo_AES)
* [Wikipedia : Neo-Geo MVS](https://fr.wikipedia.org/wiki/Neo-Geo_MVS)