---
title: Mega CD
description: Sega Mega CD
published: true
date: 2021-10-03T18:48:03.937Z
tags: consoles, sega, mega cd
editor: markdown
dateCreated: 2021-05-21T07:59:54.630Z
---

![](/emulators/consoles/megacd.svg){.align-center}

## Fiche technique

* **Fabricant :** Sega
* **Année de sortie :** 1991
* **Quantités vendues :** 30 millions
* **Meilleur jeu vendu :** Sewer Shark
* **Processeur :** processeurs 16-bit Motorola 68000 @ 12.5 MHz + Megadrive
* **RAM :** 512kB
* **VRAM :** 256kB
* **RAM sonore :** 16kB
* **Vidéo :** Custom ASIC
* **Résolution :** 320×224 pixels, 64 couleurs (sur une palette de 512 couleurs)
* **Puce sonore :** puces Ricoh RF5C164 + Megadrive
* **Média :** disque CD-ROM 500MB

## Présentation

Le **Mega-CD** ou **Mega CD**, également connu sous le nom de **Sega CD** en Amérique du Nord, est un périphérique destiné à la console de jeu vidéo Mega Drive de Sega. Il s'agit d'un lecteur de CD-ROM permettant de lire des jeux au format CD-ROM, des CD Audio et des CD-G. Il fut commercialisé fin 1991 au Japon, en 1992 aux États-Unis et en 1993 en Europe.

## Émulateurs

[Libretro PicoDrive](libretro-picodrive)
[Libretro GenesisPlusGX](libretro-genesisplusgx)