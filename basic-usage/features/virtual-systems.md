---
title: Virtual systems
description: 
published: true
date: 2024-11-11T05:50:05.757Z
tags: virtual, systems
editor: markdown
dateCreated: 2021-06-29T11:39:06.226Z
---

## Presentation

Also long awaited, virtual systems (like the Favorites system) among the classics:

* All games.
* All multiplayer games (2 players and more).
* All last played games, automatically sorted by date.

Not forgetting the **Arcade** system, which gathers all the MAME, FBN, NEO-GEO systems, ... which will now be activated/deactivated in the menus.  
But that's not all.

For those who will rescrape their systems with the new internal scraper (Skraper not being updated yet), _**new virtual systems per game genre can be activated**_.  
Yes, you read that right:

* By genre.
* By Shoot'em up games ?
* By platform games?
* By fighting games?
* By puzzle games?

Activate the virtual systems of the genres you are interested in, and they will all be gathered in the same list!

## Configuration of virtual systems

To configure, in the interface, press `START` and go to ` ADVANCED SETTINGS` > `VIRTUAL SYSTEMS`. You will have the following options:

* Show all-games system (On/Off)
* Show multiplayer system (On/Off)
* Show last games system (On/Off)
* Show Lightgun system (On/Off)
* Show Ports system (On/Off)
* Virtual systems per genre - select the genres according to your needs.

![](/basic-usage/features/virtualsystem3.png){.full-width}

![](/basic-usage/features/virtualsystem4.png){.full-width}

## Configuration of arcade virtual systems

For arcade virtual systems, the configuration is different.

To configure, in the interface, press `START` and go to `ARCADE SETTINGS` > `ALL-IN-ONE ARCADE SYSTEM`. You will have the following options:

* Activate arcade virtual system (On/Off)
* Include Neo-Geo (On/Off)
* Hide original systems (On/Off)

![](/basic-usage/features/virtualsystem5.png){.full-width}

## Configuration of arcade virtual systems by manufacturer

For arcade virtual systems by manufacturer, the configuration is different again.

To configure, in the interface, press `START` and go to `ARCADE SETTINGS` > `MANUFACTURER VIRTUAL SYSTEMS`. You will be presented with a list of builders to enable or disable according to your needs.