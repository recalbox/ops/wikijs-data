---
title: Generalities
description: 
published: true
date: 2025-03-22T01:24:23.824Z
tags: generalities
editor: markdown
dateCreated: 2021-08-07T15:18:22.626Z
---

You will know here some generalities related to the games and the systems around.

Here are the available categories:

[Roms and isos](isos-and-roms)

Here are the available tutorials:

[Multi-disc management with .M3U](multidisc-management-with-m3u)
[Shaders configuration](shaders-configuration)
[Tags used in rom names](tags-used-in-rom-names)
[Where to find 100% legal roms](where-to-find-100-legal-roms)