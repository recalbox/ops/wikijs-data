---
title: Bios Checking
description: The new BIOS checker from your Recalbox.
published: true
date: 2021-08-08T19:00:31.943Z
tags: manager, bios, checking
editor: markdown
dateCreated: 2021-06-29T11:56:57.311Z
---

## Presentation

The **bios** are a problem for many users, and we know it. Currently, the only way to check them was the _checker_ available in the WebManager. Handy, but it is no longer sufficient.

While waiting for the new WebManager, we have included a new **Bios Checker** in EmulationStation. And let's say it frankly: It's night and day!

This new checker will tell you if bios are mandatory, if they are not, if MD5 signatures must match or not, it will also tell you what some bios are for.

And that's not all! It also supports multiple different MD5 signatures, and best of all: _**it will do 90% of the work for you and give you a complete and accurate status report.**_

![](/basic-usage/features/biosmanager1.png){.full-width}

## Usage

* In the system list, press the `START` button.
* Go to `BIOS CHECKING`.

You will be taken directly to the window with all the bios listed.

## Information in the bios checking

### On the left side

On the left side, you will find the following information:

* **System**: the system with bios.
* **Bios name**: each bios is listed in its system.

### On the right side

* `SYSTEM`: the system associated with the selected bios.
* `CORE: the core(s) to which the bios is requested.
* `FILE PATH`: the path where to place the bios with its exact filename, extension included.
* `MANDATORY`: the bios is mandatory to use the associated core ?
* `MUST MATCH MD5` : the MD5 signature could be checked ?
* `FILE EXISTS?: the bios has been found?
* `MATCHING MD5?`: the MD5 signature is correct ?

### Global result

You will see the global result by bios with the color of the thumb.

* **Green** : everything is OK.
* **Yellow** : the bios is present but its MD5 signature may not be good or the bios is missing but not required.
* **Red** : required bios are missing.

## Search for bios

For the bios you need, in the `/recalbox/share/bios/` directory, you will have a file named `missing_bios_report.txt` listing the names of the missing bios and their potential valid MD5 signatures.