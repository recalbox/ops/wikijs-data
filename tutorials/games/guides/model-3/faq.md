---
title: FAQ
description: 
published: true
date: 2024-07-25T22:47:56.839Z
tags: model3, faq
editor: markdown
dateCreated: 2024-07-25T22:46:57.695Z
---

## "Network board not present" error

To overcome this error when launching certain games such as Daytona, follow these steps:

Launch the game and press `L3` to access the test menu.

![](/tutorials/games/guides/model-3/networkerror1.png)

Use the `X` or `Y` buttons to move to `GAME ASSIGNMENTS` and press `A` or `B` to confirm.

![](/tutorials/games/guides/model-3/networkerror2.png)

Go to `LINK ID` and press `A` or `B` to switch from `MASTER` to `SINGLE`.

![](/tutorials/games/guides/model-3/networkerror3.png)

Return to the previous menus until you exit the test menu.