---
title: Libretro blueMSX
description: 
published: true
date: 2021-10-07T17:27:38.725Z
tags: libretro, bluemsx, colecovision
editor: markdown
dateCreated: 2021-07-30T17:01:00.535Z
---



## ![](/emulators/license.svg) License

This core is under [**GPLv2**](https://github.com/libretro/blueMSX-libretro/blob/master/license.txt) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Features

| Feature | Supported |
| :---: | :---: |
| Restart | ✔ |
| Screenshots | ✔ |
| Core Options | ✔ |
| RetroAchievements | ✔ |
| Controls | ✔ |
| Remapping | ✔ |
| Disk Control | ✔ |

## ![](/emulators/bios.svg) BIOS

### List of mandatory bios

| Filename | Description | MD5 | Provided |
| :---: | :---: | :---: | :---: |
| czz50-1.rom | - | 1de922acdd742d31349c2801e9768c35 | ✅  |
| czz50-2.rom | - | 72b089dc55b7fe7ffb5028f365e8c045 | ✅  |
| coleco.rom | - | 2c66f5911e5b42b8ebe113403548eee7 | ✅  |
| coleco.rom | - | 2c66f5911e5b42b8ebe113403548eee7 | ✅  |
| SVI603.ROM | - | c60a2e85572c0ccb69505a7646d5c1b6 | ✅  |

### Location

Put the bios like this:

┣ 📁 recalbox  
┃ ┣ 📁 share  
┃ ┃ ┣ 📁 bios  
┃ ┃ ┃ ┣ 📁 Machines  
┃ ┃ ┃ ┃ ┣ 📁 COL - Bit Corporation Dina
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **czz50-1.rom**
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **czz50-2.rom**
┃ ┃ ┃ ┃ ┣ 📁 COL - ColecoVision with Opcode Memory Extension
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **coleco.rom**
┃ ┃ ┃ ┃ ┣ 📁 COL - ColecoVision
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **coleco.rom**
┃ ┃ ┃ ┃ ┣ 📁 COL - Spectravideo SVI-603 Coleco
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **SVI603.ROM**

## ![](/emulators/roms.png) Roms

### Supported extensions

Roms must have the extension:

* .col

### Location

Put the roms like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 colecovision
┃ ┃ ┃ ┃ ┣ 🗒 **game.col**

>The roms using **No-Intro** format are highly recommended.
{.is-success}

>For more information about roms, go to [this tutorial](./../../../tutorials/games/generalities/isos-and-roms)!
{.is-info}

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Accessing to options

You can configure various options in two different ways.

* Via RetroArch menu:

┣ 📁 RetroArch Menu
┃ ┣ 📁 Core options
┃ ┃ ┣ 🧩 Name_option

* Via the file `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core options

| Options | Options values | Variables (for configuration override) | Variables values |
| --- | --- | --- | --- |
| Machine Type (Restart) | `Auto` ✅ / `MSX` / `MSXturboR` / `MSX2` / `MSX2+` / `SEGA - SG-1000` / `SEGA - SC-3000` / `SEGA - SF-7000` / `SVI - Spectravideo SVI-318` / `SVI - Spectravideo SVI-328` / `SVI - Spectravideo SVI-328 MK2` / `ColecoVision` / `Coleco (Spectravideo SVI-603)` | `bluemsx_msxtype` | `Auto` / `MSX` / `MSXturboR` / `MSX2` / `MSX2+` / `SEGA - SG-1000` / `SEGA - SC-3000` / `SEGA - SF-7000` / `SVI - Spectravideo SVI-318` / `SVI - Spectravideo SVI-328` / `SVI - Spectravideo SVI-328 MK2` / `ColecoVision` / `Coleco (Spectravideo SVI-603)` |
| Crop Overscan | `Disabled` ✅ / `Enabled` / `MSX2` | `bluemsx_overscan` | `disabled` / `enabled` / `MSX2` |
| VDP Sync Type (Restart) | `Auto` / `50Hz` / `60Hz` ✅ | `bluemsx_vdp_synctype` | `auto` / `50 Hz` / `60 Hz` |
| No Sprite Limit | `Disabled` ✅ / `Enabled` | `bluemsx_nospritelimits` | `disabled` / `enabled` |
| Sound YM2413 Enable (Restart) | `Enabled` ✅ / `Disabled` | `bluemsx_ym2413_enable` | `enabled` / `disabled` |
| Cart Mapper Type (Restart) | `Auto` ✅ / `Normal` / `mirrored` / `basic` / `0x4000` / `0xC000` / `ascii8` / `ascii8sram` / `ascii16` / `ascii16sram` / `ascii16nf` / `konami4` / `konami4nf` / `konami5` / `konamisynth` / `korean80` / `korean90` / `korean126` / `MegaFlashRomScc` / `MegaFlashRomSccPlus` / `msxdos2` / `scc` / `sccexpanded` / `sccmirrored` / `sccplus` / `snatcher` / `sdsnatcher` / `SegaBasic` / `SG1000` / `SG1000Castle` / `SG1000RamA` / `SG1000RamB` / `SC3000` | `bluemsx_cartmapper` | `Auto` / `Normal` / `mirrored` / `basic` / `0x4000` / `0xC000` / `ascii8` / `ascii8sram` / `ascii16` / `ascii16sram` / `ascii16nf` / `konami4` / `konami4nf` / `konami5` / `konamisynth` / `korean80` / `korean90` / `korean126` / `MegaFlashRomScc` / `MegaFlashRomSccPlus` / `msxdos2` / `scc` / `sccexpanded` / `sccmirrored` / `sccplus` / `snatcher` / `sdsnatcher` / `SegaBasic` / `SG1000` / `SG1000Castle` / `SG1000RamA` / `SG1000RamB` / `SC3000` |
| Auto Rewind Cassette | `Enabled` ✅ / `Disabled` | `bluemsx_auto_rewind_cas` | `enabled` / `disabled` |

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/libretro/blueMSX-libretro/](https://github.com/libretro/blueMSX-libretro/)
* **Libretro documentation**: [https://docs.libretro.com/library/bluemsx/](https://docs.libretro.com/library/bluemsx/)