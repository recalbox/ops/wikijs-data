---
title: Utilización de Joycons
description: Cómo sincronizar y utilizar los Joy-Cons de Nintendo con Recalbox
published: true
date: 2023-09-23T19:25:24.468Z
tags: joy-con, 7.2+
editor: markdown
dateCreated: 2023-09-23T19:25:24.468Z
---

Os vamos a explicar cómo sincronizar un par de Joycons y cómo utilizarlos separadamente o combinados.

## Modo "un Joycon por cada jugador"

En cada Joycon, existe un botoncito para encender y apagar. Este botón os permite también sincronizar el mando con vuestra Recalbox.

![Bouton de synchronisation sur chaque Joy-Con](/tutorials/controllers/controllers/switch-joycons/joyconconfig1.jpg)

En Recalbox, id a la lista de sistemas, pulsad el botón **Start** de vuestro mando o **Enter** en un teclado y dirigiros dentro de **CONFIGURACION DE MANDOS**.

![](/tutorials/controllers/controllers/switch-joycons/joyconconfig2.png){.full-width}

En este menú, seleccionad **ASOCIAR UN MANDO BLUETOOTH** y validad. El proceso de búsqueda bluetooth comienza.

![](/tutorials/controllers/controllers/switch-joycons/joyconconfig3.png){.full-width}

En el Joycon, pulsad el botón de sincronización durante unos segundos (normalemente tres segundos) para que pase en modo "asociación bluetooth". Las cuatro luces laterales comenzarán a desfilar.
Si las luces se apagan, recomenzad el proceso.

![](/tutorials/controllers/controllers/switch-joycons/joyconconfig4.png){.full-width}

Cuando terminará la búsqueda de dispositivos en Recalbox, deberíais poder ver vuestro Joycon en la lista.

![](/tutorials/controllers/controllers/switch-joycons/joyconconfig5.png){.full-width}

Seleccionad el Joycon y validad. Cuando obtendréis el mensaje de **MANDO ASOCIADO**, pulsad los botones **SL** y **SR**. Vuestro Joycon debe quedarse con una luz lateral encendida.
Sélectionnez le Joycon et validez. Lorsque vous obtenez le message **MANETTE ASSOCIÉE**, appuyez sur les boutons **SL** et **SR**. Votre Joycon doit avoir une lumière latérale allumée. El emparejamiento se habrá realizado con éxisto.

Aplicad estos mismos pasos con el segundo Joycon.

![](/tutorials/controllers/controllers/switch-joycons/joyconconfig6.png){.full-width}

Validad el segundo Joycon para terminar su asociación y pulsad en **SL** + **SR** como en la última étapa anterior. Una vez que tenéis asociados vuestros 2 Joycons, los podréis ver en la lista de controladores.

![](/tutorials/controllers/controllers/switch-joycons/joyconconfig7.png){.full-width}

## Joycons en modo combinado

Para usar los dos Joycons como un único mando, hay que combinar los dos Joycons y, cuando las luces parpadeen, pulsar **L** en el Joycon izquierdo y **R** en el Joycon derecho (L+R) al mismo tiempo. Ahora tienes un único mando a partir de dos Joycons.

Si los Joycons ya estaban conectados en modo individual, apagadlos, esperad unos 10 segundos y encendedlos de nuevo, las luces empezarán a parpadear, entonces pulsad **L** + **R** como antes.

![](/tutorials/controllers/controllers/switch-joycons/joyconconfig8.png){.full-width}

A vosotros de disfrutar del placer de jugar con vuestros Joy-Cons !