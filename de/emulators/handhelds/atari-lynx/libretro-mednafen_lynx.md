---
title: Libretro Mednafen_Lynx
description: 
published: true
date: 2021-07-12T09:40:18.785Z
tags: 
editor: markdown
dateCreated: 2021-07-12T09:40:18.785Z
---

**Libretro Mednafen\_lynx** ist ein Emulator des Atari Lynx Videospielsystems.  
Genauer gesagt ist er eine Portierung von Mednafen Lynx, der ein Fork von [Handy](./emulators/handhelds/atari-lynx/libretro-handy) ist.

## ![](/emulators/license.svg) Lizenz

Diese Core steht unter den Lizenzen [**zlib**](https://github.com/libretro/beetle-lynx-libretro/blob/master/mednafen/lynx/license.txt) und [**GPLv2**](https://github.com/libretro/beetle-lynx-libretro/blob/master/COPYING).

## ![](/emulators/compatibility.png) Kompatibilität

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulateurs/features.png) Eigenschaften

| Eigenschaft | Unterstützt |
| :---: | :---: |
| Neustart | ✔ |
| Screenshots | ✔ |
| Schnellspeichern | ✔ |
| Zurückspulen | ✔ |
| Netplay | ✔ (keine Link-Kabel-Emulation) |
| Core Optionen | ✔ |
| RetroAchievements | ✔ |
| Cheats (Cheat-Menü) | ✔ |
| Steuerung | ✔ |
| Remapping | ✔ |
| Softpatching | ✔ |
| Crop Overscan | ✔ |

## ![](/emulators/bios.svg) BIOS

### Liste der obligatorischen BIOS

| Dateiname | Beschreibung| MD5 | Bereitgestellt |
| :---: | :---: | :---: | :---: |
| lynxboot.img | Lynx Boot Image | fcd403db69f54290b51035d82f835e7b | ❌  |

### Speicherort BIOS

Lege das BIOS wie folgt ab:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 🗒 **lynxboot.img**

## ![](/emulators/roms.png) ROMs

### Unterstützte Erweiterungen

Roms müssen folgende Erweiterung haben:

* .lnx
* .o
* .zip
* .7z

Dieses System unterstützt komprimierte ROMs im .zip/.7z-Format. Sei jedoch vorsichtig, denn es handelt sich nur um Archive.

Die in der .zip/.7z enthaltenen Dateien müssen den oben genannten Erweiterungen entsprechen.
Außerdem darf jede .zip/.7z-Datei nur ein komprimiertes ROM enthalten.

### Speicherort ROMs

Lege deine Spiele wie folgt ab:

┣ 📁 recalbox  
┃ ┣ 📁 share  
┃ ┃ ┣ 📁 roms  
┃ ┃ ┃ ┣ 📁 lynx  
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.zip**  

>Die Roms im **No-Intro-Format** werden ausdrücklich empfohlen..
{.is-success}

>Weitere Informationen über Roms findest du in [dieser Anleitung](./../../tutorials/games/generalities/isos-and-roms)!
{.is-info}

## ![](/emulators/advanced-configuration.png) Erweiterte Konfiguration des Emulators

>Um eigene Konfigurationen nach einem Upgrades beibehalten zu können, empfehlen wir die Benutzung der Funktion [Konfiguration überschreiben](./../../../advanced-usage/configuration-override).
{.is-info}

### Zugriff auf Optionen

Du kannst verschiedene Optionen auf zwei verschiedene Arten konfigurieren.

* Über das RetroArch-Menü:

┣ 📁 RetroArch Menu
┃ ┣ 📁 Core options
┃ ┃ ┣ 🧩 Name_option

* Über die Datei `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core Optionen

## ![](/emulators/external-links.png) Externe Links

* **Verwendeter Quellcode**: [https://github.com/libretro/beetle-lynx-libretro/](https://github.com/libretro/beetle-lynx-libretro/)
* **Libretro Dokumentation**: [https://docs.libretro.com/library/beetle\_lynx/](https://docs.libretro.com/library/beetle_lynx/)
* **Offizieller Quellcode**: [https://mednafen.github.io/releases/](https://mednafen.github.io/releases/)
* **Offizielle Webseite**: [https://mednafen.github.io/](https://mednafen.github.io/)