---
title: Macintosh
description: 
published: true
date: 2024-07-26T12:48:46.610Z
tags: apple, macintosh, 9.0+, ordenadores
editor: markdown
dateCreated: 2023-09-02T18:54:13.958Z
---

![](/emulators/computers/macintosh.svg){.align-center}

## Datos técnicos

* **Fabricante**: Apple Computer, Inc.
* **Año de lanzamiento**: 1984
* **Sistema operativo**: System Software 1.0
* **Procesador**: Motorola 68000 a 7,8336 MHz
* **RAM**: 128 KB
* **ROM**: 64 KB
* **Sonido**: 8 chips TTL que implementan un controlador DMA de vídeo y sonido
* **Pantalla** : 9" Monocromo incorporada (512×342 píxeles)

## Presentación

Macintosh (/ma.kin.tɔʃ/, en inglés: /ˈmæk.ɪn.tɑʃ/) o Mac es una serie de diferentes familias de ordenadores personales diseñados, desarrollados y vendidos por Apple. El primer Macintosh se lanzó el 24 de enero de 1984 (pasó a llamarse Macintosh 128K cuando se lanzó el Macintosh 512K). Fue el primer ordenador de éxito comercial que utilizaba un ratón y una interfaz gráfica (en lugar de una interfaz de línea de comandos).

El Macintosh sustituyó al Apple II como principal producto de Apple. Sin embargo, la cuota de mercado de Apple cayó, antes de un resurgimiento del Macintosh en 1998, con el lanzamiento del ordenador de consumo todo en uno iMac G3, que permitió a Apple escapar de una probable quiebra y supuso un éxito para la empresa.

## Emuladores

[Libretro MinivMac](libretro-minivmac)