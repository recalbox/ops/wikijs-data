---
title: Hatari
description: 
published: true
date: 2024-07-29T18:45:56.691Z
tags: atari-st, hatari, 8.0+
editor: markdown
dateCreated: 2023-09-11T08:29:35.314Z
---

## ![](/emulators/license.svg) Licencia

Este núcleo está bajo la licencia [**GPLv2**](https://github.com/hatari/hatari#readme).

## ![](/emulators/compatibility.png) Compatibilidad

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Funcionalidades

El emulador soporta las siguientes series de máquinas:

- `Atari ST o STF`: los primeros Ataris de 32 bits lanzados en 1985, basados en CPU Motorola 68000.
- `Atari MegaST`: versión profesional del ST en formato pizza, ofreciendo de 1 a 4 MB de RAM.
- `Atari STE`: segunda serie de 1989 con mejor sonido, más colores y un chip gráfico que le faltaba: el blitter!
- `Atari MegaSTE`: versión profesional del STE con más RAM y una CPU más potente a 16 MHz.
- `Atari TT`: aparecido en 1990, el TT funciona con un 68030 a 32 MHz.
- `Falcon`: los últimos de la familia, los Falcon ofrecerán CPU desde el 68030 hasta el 68060. Verdaderas joyas tecnológicas para la época, su reinado terminó 1 año después de su lanzamiento.

La gran mayoría de los juegos funcionarán en un 520 ST básico, pero algunos requerirán un STE, o funcionarán mejor en un STE.
Encontrarás muy pocos juegos para el TT, que está más reservado para profesionales. Además, el TT es la serie menos retrocompatible.
Para el Falcon, muy pocos juegos fueron lanzados. Los que encuentras generalmente están en formato zip, en formato nativo. Para los conocedores e interesados, puedes copiar los zip en `share/roms/atarist` y convertirlos en imágenes de disco usando el script `/usr/share/hatari/zip2st.sh` (conexión SSH obligatoria).

Recalbox permite una auto-configuración del modelo cuando se lanza un juego, reconociendo ciertas palabras clave en el nombre del archivo del juego o en tu directorio:

| Modelo         | Palabras clave |
|----------------|----------|
| Atari ST       | `atari-st` |
| Atari STE      | `atariste`, `atari-ste` |
| Atari Mega-ST  | `atarimegast`, `atari-megast`, `atari-mega-st`, `megast`, `mega-st` |
| Atari Mega-STE | `atarimegaste`, `atari-megaste`, `atari-mega-ste`, `megaste`, `mega-ste` |
| Atari TT       | `ataritt`, `atari-tt` |
| Falcon         | `falcon`, `atari-falcon` |

Las palabras clave no son sensibles a mayúsculas y minúsculas, pueden contener minúsculas/mayúsculas indistintamente.

Por ejemplo:

- La rom `share/roms/atarist/Grand Prix 500cc (1987)(Microids)(fr)(atari-ste).zip` lanzará el emulador en modo STE.
- Y `share/roms/atarist/Grand Prix 500cc (1987)(Microids)(fr) [falcon].zip` lanzará el emulador en modo Falcon.

Estas palabras clave también pueden formar parte o ser un subdirectorio:

- `share/roms/atarist/Atari-STE/Rick Dangerous (1989)(Core - Firebird).zip` lanzará el juego en modo STE.
- `share/roms/atarist/Atari-TT/Rick Dangerous (1989)(Core - Firebird).zip` lo lanzará en modo Atari TT.

En el caso de los subdirectorios, se reconocen 3 directorios además de las palabras clave (`st`, `ste` y `tt`):

- `share/roms/atarist/st/Blues Brothers, The (Europe).zip` forzará el juego en un Atari STF.
- `share/roms/atarist/STe/Blues Brothers, The (Europe).zip` forzará un Atari STE.
- `share/roms/atarist/TT/Blues Brothers, The (Europe).zip` forzará un Atari TT.

## ![](/emulators/bios.svg) BIOS

### Lista de BIOS obligatorios

| Nombre de archivo | Descripción | MD5 | Proporcionado |
| :---: | :---: | :---: | :---: |
| emutos.img | Bios open-source EMUTOS. Compatible al 99% con todas las máquinas Atari. | `e2c861c588fca2d0cf6be3df3aaf05f2` | ✅ |
| st.img | Bios Atari ST/Mega-ST (¡no STE!) | `0087e2707c57efa2106a0aa7576655c0` `036c5ae4f885cbf62c9bed651c6c58a8` `0604dbb85928f0598d04144a8b554bbe` `091a4d95f6c600231a3f1391a309ac26` `143343f7b8e0b1162af206fe8f46b002` `25789a649faff0a1176dc7d9b98105c0` `332fe3803a7e20cd625b27a69f07ae69` `41b7dae4e24735f330b63ad923a0bfbc` `4a0d4f282c3f2a0196681adf88862dd4` `5491b3755992feaf743ceecaa3b18f34` `52248cc70ae48b3050e197e270917130` `696ba04b3bc963aee2ba769298e342de` `736adb2dc835df4d323191fdc8926cc9` `7c040857bdcfcd7d748ca82205463efa` `800d48825be45ec7f39ba7bb2c3f0b59` `886b9b6bb6afa287211a594e16f6ca67` `91ce7c1903006da75c823ff3c938e5d2` `91fb61080d27d0376e1a3da409727468` `a622cc35d8d78703905592dfaa4d2ccb` `a7dc40dc5c1086bce1a8f3d44fd29051` `b2873004a408b8db36321f98daafa251` `b2a8570de2e850c5acf81cb80512d9f6` `b9e37f0f0a700fc673049794870bdb85` `c1c57ce48e8ee4135885cee9e63a68a2` `c87a52c277f7952b41c639fc7bf0a43b` `c9093f27159e7d13ac0d1501a95e53d4` `d0f682ee6237497004339fb02172638b` `d6521785627d20c51edc566808a6bf28` `e0444d8c51a8093a3f09665d8099c0af` `e690bec90d902024beed549d22150755` `f6a8fa31d0d1bac1a08d47502244408d` | ❌ |
| ste.img | Bios Atari STE | `0604dbb85928f0598d04144a8b554bbe` `1cbc4f55295e469fc8cd72b7efdea1da` `30f69d70fe7c210300ed83f991b12de9` `332fe3803a7e20cd625b27a69f07ae69` `4759f2c7bace6c2f440d27acd2ab6ed4` `4a0d4f282c3f2a0196681adf88862dd4` `6033f2b9364edfed321c6931a8181fd2` `61168619b5109d314638c9409f44bca2` `6f9471098a521214fad1e2c6f2dd3d08` `886b9b6bb6afa287211a594e16f6ca67` `94a75c1c65408d9f974b0463e15a3b11` `992bac38e01633a529121a2a65f0779e` `a0982e760f9807d82667ff5a69e78f6b` `a4cfd3c7412002dd693f69a15d4d961c` `a7dc40dc5c1086bce1a8f3d44fd29051` `b2873004a408b8db36321f98daafa251` `bc7b224d0dc3f0cc14c8897db89c5787` `c9093f27159e7d13ac0d1501a95e53d4` `e690bec90d902024beed549d22150755` `ed5fbaabe0219092df74c6c14cea3f8e` `f3f92cd4e2c331aef8970a156be03b7c` `febb00ba8784798293a7ae709a1dafcb` | ❌ |

### Ubicación

Coloca los BIOS de la siguiente manera:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 atarist
┃ ┃ ┃ ┃ ┣ 🗒 **emutos.img**
┃ ┃ ┃ ┃ ┣ 🗒 **st.img**
┃ ┃ ┃ ┃ ┣ 🗒 **ste.img**

### Lista de BIOS opcionales

| Nombre de archivo | Descripción | MD5 | Proporcionado |
| :---: | :---: | :---: | :---: |
| falcon.img | Bios Atari Falcon 30/40 | `919fd0f045299f101cf7757e5e058c41` `941baac767269649f92f7918dd5740b8` `9e880168d0a004f7f5e852be758f39e4` `b2ecabc15b65afef96331be46f2a2cd4` `b338bacb2fc453bab61bcc1e2cf5076b` `e5ea0f216fb446f1c4a4f476bc5f03d4` `ed2647936ce4bd283c4d7dfd7ae09d1c` | ❌ |
| megaste.img | Bios Atari Mega-STE | `0604dbb85928f0598d04144a8b554bbe` `1c92855316a33faee602b8007f22d2cb` `332fe3803a7e20cd625b27a69f07ae69` `4689ef3694ac86e7289923190e6cd10d` `4a0d4f282c3f2a0196681adf88862dd4` `61b620ad951815a25cb37895c81a947c` `7449b131681f1dfe62ebed1392847057` `7aeabdc25f8134590e25643a405210ca` `7cdd45b6aac66a21bfb357d9334e46db` `7e87d8fe7e24e0b4c55ad1b7955beae3` `886b9b6bb6afa287211a594e16f6ca67` `a7dc40dc5c1086bce1a8f3d44fd29051` `b2873004a408b8db36321f98daafa251` `c9093f27159e7d13ac0d1501a95e53d4` `e690bec90d902024beed549d22150755` | ❌ |
| tos.img | Imagen de arranque TOS | `036c5ae4f885cbf62c9bed651c6c58a8` `0604dbb85928f0598d04144a8b554bbe` `066f39a7ea5789d5afd59dd7b3104fa6` `091a4d95f6c600231a3f1391a309ac26` `143343f7b8e0b1162af206fe8f46b002` `1c92855316a33faee602b8007f22d2cb` `25789a649faff0a1176dc7d9b98105c0` `27ac71b59d59f65571b972e0f894054a` `2a8e39c60317b921eabd7e25b5d74395` `30f69d70fe7c210300ed83f991b12de9` `332fe3803a7e20cd625b27a69f07ae69` `41b7dae4e24735f330b63ad923a0bfbc` `4a0d4f282c3f2a0196681adf88862dd4` `52248cc70ae48b3050e197e270917130` `5491b3755992feaf743ceecaa3b18f34` `5d65c6a384d847a0b8013d5e22ec0887` `61b620ad951815a25cb37895c81a947c` `696ba04b3bc963aee2ba769298e342de` `6f9471098a521214fad1e2c6f2dd3d08` `736adb2dc835df4d323191fdc8926cc9` `7449b131681f1dfe62ebed1392847057` `7c040857bdcfcd7d748ca82205463efa` `7cdd45b6aac66a21bfb357d9334e46db` `7e87d8fe7e24e0b4c55ad1b7955beae3` `800d48825be45ec7f39ba7bb2c3f0b59` `91ce7c1903006da75c823ff3c938e5d2` `941baac767269649f92f7918dd5740b8` `94a75c1c65408d9f974b0463e15a3b11` `992bac38e01633a529121a2a65f0779e` `9e880168d0a004f7f5e852be758f39e4` `a0982e760f9807d82667ff5a69e78f6b` `a4cfd3c7412002dd693f69a15d4d961c` `a7dc40dc5c1086bce1a8f3d44fd29051` `b2873004a408b8db36321f98daafa251` `b2a8570de2e850c5acf81cb80512d9f6` `b9e37f0f0a700fc673049794870bdb85` `bc7b224d0dc3f0cc14c8897db89c5787` `c1c57ce48e8ee4135885cee9e63a68a2` `d0f682ee6237497004339fb02172638b` `d41ae4efaa2c97f632dbe75d24d51bea` `d6521785627d20c51edc566808a6bf28` `e5ea0f216fb446f1c4a4f476bc5f03d4` `e690bec90d902024beed549d22150755` `ed2647936ce4bd283c4d7dfd7ae09d1c` `ed5fbaabe0219092df74c6c14cea3f8e` `f3f92cd4e2c331aef8970a156be03b7c` `f62bc9777f28bc6876422738758db365` `febb00ba8784798293a7ae709a1dafcb` | ❌ |
| tt.img | Bios Atari TT | `066f39a7ea5789d5afd59dd7b3104fa6` `19cd81e1c3049bf408ad426dc5c3b2e0` `2a8e39c60317b921eabd7e25b5d74395` `ac6ca0f0ed42a1603dfd409c4bf8eb89` `dd1010ec566efbd71047d6c4919feba5` | ❌ |

### Ubicación

Coloca los BIOS de la siguiente manera:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 atarist
┃ ┃ ┃ ┃ ┣ 🗒 **falcon.img**
┃ ┃ ┃ ┃ ┣ 🗒 **megaste.img**
┃ ┃ ┃ ┃ ┣ 🗒 **tos.img**
┃ ┃ ┃ ┃ ┣ 🗒 **tt.img**

## ![](/emulators/roms.png) Roms

### Extensiones soportadas

Las roms deben tener las siguientes extensiones:

* .ctr
* .dim
* .ipf
* .msa
* .raw
* .st
* .stx

### Ubicación

Coloca las roms de la siguiente manera:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 atarist
┃ ┃ ┃ ┃ ┣ 🗒 **juego.st**

>Se recomiendan encarecidamente las roms en formato **No-Intro**.
{.is-success}

>Para más información sobre las roms, visita [este tutorial](./../../../tutorials/games/generalities/isos-and-roms)!
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

>Para poder conservar tus configuraciones personalizadas durante una actualización, te recomendamos utilizar nuestra funcionalidad [Sobrecargas de configuración](./../../../advanced-usage/configuration-override).
{.is-info}

## ![](/emulators/external-links.png) Enlaces externos

* **Código fuente utilizado** : [https://github.com/hatari/hatari/](https://github.com/hatari/hatari/)
