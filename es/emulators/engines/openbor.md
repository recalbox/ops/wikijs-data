---
title: OpenBOR
description: 
published: true
date: 2024-07-26T08:24:12.027Z
tags: openbor, motor de juego
editor: markdown
dateCreated: 2023-09-04T07:13:40.528Z
---

![](/emulators/fantasy/openbor.svg){.align-center}

## Detalles técnicos

* **Fabricante:** Senile Team, el equipo OpenBOR y la comunidad
* **Año de lanzamiento:** 2004

## Presentación

OpenBOR es un motor de juego de desplazamiento lateral basado en sprites libres de derechos de autor.  
Desde sus modestos comienzos en 2004, se ha convertido en el motor más versátil, fácil de usar y potente de su clase.

OpenBOR está optimizado para juegos de desplazamiento lateral tipo beat em up (Double Dragon, Streets of Rage, Final Fight), pero se adapta fácilmente a cualquier tipo de estilo de juego y funcionalidad que puedas imaginar.

¿Quieres probar a crear juegos? Abre uno de los módulos existentes y juega: la comunidad está aquí para ayudarte.  
¿Estás listo para crear tu propio juego? Reúne unas cuantas imágenes y ¡listo! ¿Te sientes ambicioso? Sumérgete en el motor de scripting integrado y en la suite gráfica para crear una obra maestra que rivalice con las producciones AAA más alocadas. 

OperBOR es la suite de código abierto de Beats of Rage.

En 2004, Senile Team lanzó Beats of Rage, un motor de videojuegos personalizable, un beat-'em up gratuito para DOS inspirado en la serie Streets of Rage de SEGA y que utiliza sprites de la serie King of Fighters de SNK Playmore.  
Con Beats of Rage, los usuarios pueden crear sus propios juegos beat 'em up, ¡sin necesidad de experiencia en programación!  
Beats of Rage se lanzó por primera vez para DOS en noviembre de 2003. Sólo se anunció de boca en boca, pero rápidamente ganó en popularidad.  
Senile Team no tardó en lanzar un pack editor que permitía a cualquier interesado crear un módulo para el motor BOR.  
Pronto aparecieron versiones para muchos otros sistemas (Dreamcast, Linux, Windows, GP32, GP2X, GP2X Wiz, Mac OS X, OpenDingux Dingoo A320 y GCW-Zero).  
Beats of Rage ya no es mantenido, distribuido o soportado por Senile Team.

En 2005, Kirby2000 pidió a Senile Team que abriera el código fuente de BOR. Aceptaron y nació OpenBOR. El desarrollo del motor fue continuado por la comunidad, y sigue siéndolo a día de hoy.

Para saber más, visita la comunidad OpenBOR en ChronoCrash.com. También encontrarás docenas de módulos de juego completados para descargar y jugar.

## Emuladores

[OpenBOR](/emulators/engines/openbor/openbor)
