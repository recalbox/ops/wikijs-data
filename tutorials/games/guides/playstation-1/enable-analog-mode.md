---
title: Activer le mode analogue
description: 
published: true
date: 2021-09-08T13:00:18.790Z
tags: ps1, mode, analogue
editor: markdown
dateCreated: 2021-06-06T22:42:07.076Z
---

## Introduction

Sur certains jeux, vous aurez besoin d'avoir une manette PlayStation analogue.

## Utilisation

* Allez dans le menu de RetroArch en faisant `Hotkey + B`.
* Allez dans `Touches` > `Port 1 Touches`.

![](/tutorials/games/guides/playstation-1/enable-analog/psxanalog1.png){.full-width}

* Sur l'option `Type de périphérique`, faites défiler jusqu'à avoir le texte `analog` affiché.

![](/tutorials/games/guides/playstation-1/enable-analog/psxanalog2.png){.full-width}

* Faites « Retour » 2 fois et allez dans `Reprendre`.

Bon jeu !