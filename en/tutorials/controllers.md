---
title: 🎮 Controllers
description: 
published: true
date: 2021-08-07T13:26:33.721Z
tags: controllers, tutorial
editor: markdown
dateCreated: 2021-08-06T07:59:25.894Z
---

All this part is dedicated to your controllers and other sticks.

Here are the available categories:

[Configuration / Test](test-configuration)
[Configuration of customized buttons](configuration-of-customized-buttons)
[Controllers](controllers)
[GPIO](gpio)
[USB encoders](usb-encoders)