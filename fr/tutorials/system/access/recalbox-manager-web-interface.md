---
title: Recalbox Manager - Interface web
---




>_**Information :**_
>
>Recalbox Manager est une interface web pour accéder à votre Recalbox via un smartphone , une tablette ou votre PC.
{.is-info}

## **Pour vous connecter**

Il suffit de saisir dans votre navigateur web simplement son nom :

* Sous windows \(IE, CHROME, FIREFOX\) :

  *  [http://recalbox/](http://recalbox/)

* Sous Mac et linux, ou utilisateur de safari sous windows :

  * [http://recalbox.local/](http://recalbox.local/)

* Si cela ne fonctionne pas, vous pouvez utiliser l'adresse IP de votre Recalbox :
  * **192.168.1.x**


>**Informations :**
>
>La plupart **des routeurs** sont configurés de tel que vous devriez **trouver votre Recalbox** entre  **192.168.1.2** et **192.168.1.254** .
>
>Pour connaitre son adresse IP :  
>Depuis le menu EmulationStation, appuyez sur `Start`\)  &gt; `OPTIONS RÉSEAU` &gt; `Adresse IP`
{.is-info}

## **Interface**

Lorsque vous êtes sur **la page principale**, vous pouvez **naviguer entre les menus** suivants :

* Modifier le fichier recalbox.conf.
* Ajouter/Supprimer des roms \(pas les images CD\).
* Consulter les log.

**Il permet de :**

* Suivre \(monitoring\) votre Recalbox \(température, utilisation des cores du CPU, etc.\)
* Modifier le fichier recalbox.conf.
* Lancer le gamepad virtuel \(pratique pour configurer en 5min sa recalbox pour la 1ère utilisation\) via smartphone ou tablette uniquement.
* Consulter les logs de Recalbox.
* Redémarrer EmulationStation ou le système entier en cas de plantage.

**Il permet par glisser-déposer :**

* Uploader les roms au format `.zip` ou extension pris en charge par les émulateurs \(pas les images CD\).

Par défaut, le gestionnaire démarre automatiquement. Si vous voulez, vous pouvez le désactiver dans recalbox.conf en ajoutant la ligne suivante :

```text
## Recalbox Manager (gestionnaire de http)
system.manager.enabled = 1
```

Vous avez aussi la possibilité de le désactiver directement dans EmulationStation \(`Start` &gt; `Paramètres avancés` &gt; `GESTIONNAIRE WEB RECALBOX`\).

## **Gamepad Virtuel**

Pour y accéder directement, il suffit de saisir l'ip de votre recalbox avec le port 8080 `http://192.168.0.X:8080` ou `http://recalbox:8080`

Consulter le manuel d'utilisation du Gamepad Virtuel [ici](/fr/basic-usage/premiere-utilisation-et-configuration#ii-configuration-dun-controleur)_**.**_

Le Gitlab pour les retours : [https://gitlab.com/recalbox/recalbox-manager](https://gitlab.com/recalbox/recalbox-manager)

