---
title: Redefinir para a configuração de fábrica
description: 
published: true
date: 2021-07-31T22:29:12.418Z
tags: 
editor: markdown
dateCreated: 2021-07-31T15:02:20.277Z
---

## Apresentação

Desde o Recalbox 7.2, existe uma nova opção chamada `Redefinir para as configurações de fábrica` que permite que você reconfigure seu sistema para o mesmo estado depois de fazer uma instalação limpa e totalmente nova. Esta opção permite que você volte a um novo estado com mais facilidade do que reinstalar com frequência.

> Esta opção **não afeta** sua BIOS, jogos e savess!
{.is-success}

## Utilização da opção

Esta opção é encontrada no EmulationStation. Para fazer isso, basta pressionar `START`, ir para `CONFIGURAÇÕES AVANÇADAS` e selecionar `REDEFINIR PARA AS CONFIGURAÇÕES DE FÁBRICA`.

![](/basic-usage/features/resetfactory1-pt.png){.full-width}

Uma vez selecionada a opção, você terá duas janelas de advertência.

![](/basic-usage/features/resetfactory2-pt.png){.full-width}

![](/basic-usage/features/resetfactory3-pt.png){.full-width}

>Se estas duas janelas forem validadas, **você não poderá voltar atrás**.
{.is-danger}

Quando a redefinição para as configurações de fábrica estiver concluída, você encontrará sua instalação do Recalbox toda limpa!
