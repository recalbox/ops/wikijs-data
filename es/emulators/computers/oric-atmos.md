---
title: Oric Atmos
description: 
published: true
date: 2024-07-26T12:48:55.989Z
tags: tangerine, oric, oric-atmos, ordenadores
editor: markdown
dateCreated: 2023-09-02T18:55:41.012Z
---

![](/emulators/computers/oricatmos.svg){.align-center}

## Detalles técnicos

* **Desarrollador**: Tangerine Computer Systems
* **Sistema operativo**: Tangerine/Microsoft Extended Basic v1.0
* **Año de lanzamiento**: 1982
* **Cantidad vendida**: 210000
* **Procesador**: MOS 6502A a 1 MHz
* **Memoria**: 16 KB / 48 KB
* **Almacenamiento**: Grabadora de casete, 300 y 2400 baudios
* **Gráficos**: 40×28 caracteres de texto / 240×200 píxeles, 8 colores

## Presentación

El **Oric Atmos** es el segundo ordenador de Oric. Lanzado en 1984, se basaba en el Oric-1, pero sustituyó su teclado por un modelo que ofrecía un tacto más convencional para una escritura más rápida, y corrigió algunos errores en su ROM. Fue elegido Ordenador del Año en 1984.

El principal problema del Oric-1 era su teclado de "pivote semimecánico". Los teclados de los microordenadores de consumo estaban lejos de igualar a los de los modelos profesionales porque, por razones de coste, generalmente utilizaban una simple membrana de plástico (como en el ZX-81), posiblemente rematada por teclas de goma. El teclado Oric-1 se calificaba de semimecánico porque sus teclas eran de plástico rígido, lo que suponía una mejora innegable, pero seguía siendo menos práctico de usar que un teclado mecánico de verdad. El Oric-Atmos resuelve el problema nº 1 del Oric 1 con su teclado mecánico. También corrige algunos errores en su memoria de sólo lectura que afectan a la carga de casetes y al Basic incorporado (el Tangerine Basic producido por Microsoft pasa de la versión 1.0 a la 1.1), pero estos serán de doble filo, ya que algunos programas del Oric-1 dejarán de funcionar en el Atmos. Aparte del aspecto de la carcasa, el resto de la máquina es casi idéntico.

## Emuladores

[Oricutron](oricutron)