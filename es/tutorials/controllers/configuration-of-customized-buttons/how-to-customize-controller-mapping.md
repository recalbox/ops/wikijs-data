---
title: Cómo personalizar el mapping de un mando
description: En RetroArch, se puede redefir el mapping de los mandos hacia las teclas
published: true
date: 2023-09-18T11:12:06.840Z
tags: retroarch, mapping, mando, configurar, ajustar, personalizar
editor: markdown
dateCreated: 2023-09-18T11:11:28.208Z
---

## Explicaciones

En RetroArch, podéis definir acciones específicas a ejecutar a partir de los botones de vuestro mando.

## Utilización

* Lanzad un juego con un core Libretro.
* Abrid el menú de RetroArch con `Hotkey` + `B`.
* Id a `Teclas` > `Port 1 Teclas`.

![](/tutorials/controllers/buttons-customization/how-to/controllerconfig1.png)

* Aquí podéis reconfigurar las teclas. No está de más tener un teclado cerca, porque si en algún momento os bloqueáis y no podeis navegar por los menús de RetroArch, lo necestaréis (`W` para validar y `X` para volver atrás).

![](/tutorials/controllers/buttons-customization/how-to/controllerconfig2.png)

* Id hacia atrás 1 vez y os encontraréis con varias opciones para grabar vuestra configuración:
	
  * « Grabar el mapping para el core » : las modificaciones de configuración serán grabadas y se aplicarán a todos los juegos que se lancen con este core.
  
![](/tutorials/controllers/buttons-customization/how-to/controllerconfig3.png)
	
  * « Grabar el mapping para la carpeta »: las modificaciones de configuración se aplicarán a toda la carpeta que contiene el juego actual.

![](/tutorials/controllers/buttons-customization/how-to/controllerconfig4.png)

  * « Grabar el mapping para el juego » : Las modificaciones de configuración se aplicarán únicamente al juego en curso.

![](/tutorials/controllers/buttons-customization/how-to/controllerconfig5.png)

Todos los ficheros de (re)mapping se encuentran dentro de `/recalbox/share/system/.config/retroarch/config/remaps/` y poseen la extensión `*.rmp`.