---
title: Configuration / Test
description: 
published: true
date: 2021-08-07T13:33:31.506Z
tags: configuration, test
editor: markdown
dateCreated: 2021-08-07T13:22:08.673Z
---

Here you will find something to test your controller in case of problems with it.

Here are the available tutorials:

[Test your joystick with sdl2-jstest](test-your-joystick-with-sdl2-jstest)