---
title: Sobrecargas do RetroArch
description: Exemplos de chaves para sobrecarregar a configuração do RetroArch.
published: true
date: 2021-09-21T23:53:47.216Z
tags: retroarch, sobrecarga
editor: markdown
dateCreated: 2021-09-19T00:12:18.847Z
---

Aqui está uma lista não exaustiva de chaves que podem ser alteradas em um arquivo de sobrecarga `.retroarch.cfg`, estas alterações só são feitas se o emulador for um núcleo RetroArch.

>Um \* após o valor significa que o parâmetro também pode ser sobrecarregado através de um .recalbox.conf
{.is-info}

## Áudio

* `audio_enable = "true"` : ativar ou desativar o áudio.
* `audio_volume = "0.000000"` : define o aumento de volume, 0= volume padrão.

## Menu do RetroArch

* `quick_menu_show_save_content_dir_overrides = "false"` : mostrar ou ocultar a opção de Substituição da configuração da pasta.
* `quick_menu_show_save_core_overrides = "false"` : mostrar ou ocultar a opção de Substituição da Configuração para o núcleo.
* `quick_menu_show_save_game_overrides = "false"` : mostrar ou ocultar a opção de Substituição de Configuração para o jogo.

>As Substituições de Configuração são um recurso do RetroArch, semelhante às sobrecargas, mas retêm muitas informações, portanto, no contexto do Recalbox, é melhor usar as sobrecargas se possível.
{.is-info}

## Depuração

* `fps_show = "true"` : mostrar o FPS no jogo.
* `menu_driver = "ozone"` : escolha o menu retroarch, ozone é o padrão, exceto na GPi case, onde o rgui é utilizado.
* `menu_enable_widgets = "true"` : habilitar popups dentro do jogo, se definido como false, exibirá notificações como texto amarelo.

## Pastas

* `recording_output_directory = ""` : pasta para onde vão as gravações de vídeo.
* `savefile_directory = ""` : onde salvar/carregar os *saves* normais.
* `savestate_directory = ""` : onde salvar/carregar os *savestates*.
* `screenshot_directory = ""` : pasta para onde vão as capturas de tela.

## Serviço de tradução

* `ai_service_enable = "true"` : \* Ativar ou desativar o serviço de tradução.
* `ai_service_mode = "0"` : Modo de serviço de tradução, 0: imagem, 1: voz.
* `ai_service_source_lang = "0"` : \* Idioma a ser lido pelo serviço de tradução, 0 = não especificado.
* `ai_service_target_lang = "1"` : \* idioma a traduzir para, 1= inglês, 3= francês, 49= Português.
* `ai_service_url =` : \* link web do serviço a ser utilizado.

## Overlays

* `aspect_ratio_index = "23"` : \* índice de proporção, 23 = Personalizado.
* `input_overlay = ""` : link para o arquivo de configuração de overlay.
* `input_overlay_enable = "true"` : habilita o Overlay.
* `input_overlay_hide_in_menu = "true"`: Esconde o Overlay no menu do RetroArch.

### Coordenadas a serem definidas com os Overlays

* `custom_viewport_height = ""`
* `custom_viewport_width = ""`
* `custom_viewport_x = ""`
* `custom_viewport_y = ""`
* `video_message_pos_x = "0.050000"`
* `video_message_pos_y = "0.050000"`

## Netplay

* `netplay_nickname = ""` : \* Apelido Netplay.

## Rotação da tela

* `video_rotation = "1"` : Rotaciona o vídeo, 0=normal, 1=90°, 2=180°, 3=270°, preste atenção na relação de aspecto/proporção da tela.

## Analógicos e botões direcionais

Para desassociar/associar os botões direcionais com um dos analógicos:

* `input_player1_analog_dpad_mode = "0"` : desassocia
* `input_player1_analog_dpad_mode = "1"` : associa com joystick esquerdo.
* `input_player1_analog_dpad_mode = "2"` : associa com joystick direito.
* `input_player1_analog_dpad_mode = "3"` : associa forçadamente com joystick esquerdo.
* `input_player1_analog_dpad_mode = "4"` : associa forçadamente com joystick direito.


## Remapeando atalhos

>As configurações para refazer as teclas de atalho dependem do mapeamento do joystick no Recalbox. Se o joystick mudar, a sobrecarga de configuração destas linhas pode não funcionar mais.
{.is-warning}

Para obter o valor numérico para cada botão em seu controle, procure no arquivo `/recalbox/share/system/configs/retroarch/retroarchcustom.cfg` o valor do botão desejado de acordo com esta tabela:

## {.tabset}
### Esconder
Clique na aba "Exibir" para mostrar as chaves de cada botão

### Exibir
| Nome do botão | Chave em questão cujo valor deve ser considerado |
| :--- | :--- |
| A | input_player1_a_btn |
| B | input_player1_b_btn |
| X | input_player1_x_btn |
| Y | input_player1_y_btn |
| Cima | input_player1_up_btn |
| Baixo | input_player1_down_btn |
| Esquerda | input_player1_left_btn |
| Direita | input_player1_right_btn |
| Select | input_player1_select_btn |
| Start | input_player1_start_btn |
| L | input_player1_l_btn |
| L2 | input_player1_l2_btn |
| L3 | input_player1_l3_btn |
| R | input_player1_r_btn |
| R2 | input_player1_r2_btn |
| R3 | input_player1_r3_btn |
| Analógico Esquerdo para Cima | input_player1_l_y_minus_axis |
| Analógico Esquerdo para Baixo | input_player1_l_y_plus_axis |
| Analógico Esquerdo para Esquerda | input_player1_l_x_minus_axis |
| Analógico Esquerdo para Direita | input_player1_l_x_plus_axis |
| Analógico Direito para Cima | input_player1_r_y_minus_axis |
| Analógico Direito para Baixo | input_player1_r_y_plus_axis |
| Analógico Direito para Esquerda | input_player1_r_x_minus_axis |
| Analógico Direito para Direita | input_player1_r_x_plus_axis |

## 

> As modificações desses valores devem ser feitas no arquivo de sobrecarga.
>
>O arquivo citado anteriormente é usado apenas para verificar o mapeamento atual.
{.is-warning}

* `input_enable_hotkey_btn =` : Hotkey.
* `input_screenshot_btn =` : Botão para fazer uma captura de tela.
* `input_exit_emulator_btn =` : Botão para sair do jogo.
* `input_load_state_btn =` : Botão para carregar *savestate*.
* `input_save_state_btn =` : Botão para salvar um *savestate*.
* `input_menu_toggle_btn =` : Botão para abrir o menu do RetroArch.
* `input_reset_btn =` : Botão para reiniciar o jogo.
* `input_ai_service_btn =` : Botão para traduzir a tela atual.

Por exemplo, o jogo `The legend of Zelda: Link's Awakening` do Game Boy requer a utilização de `Start + Select + A + B` para salvar. Se seu controle não tiver um botão `home`, e a tecla Hotkey estiver configurada para `Select`, salvar o jogo da forma convencional será impossível. Você pode configurar exclusivamente para esse jogo a chave `input_enable_hotkey_btn` como sendo o botão `R` no seu controle. O valor da chave `input_player1_r_btn` é 4 para o botão `R`, então, você só precisará inserir a chave `input_enable_hotkey_btn = 4` no arquivo de sobrecarga do jogo.