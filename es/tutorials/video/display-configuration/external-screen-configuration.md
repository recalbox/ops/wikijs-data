---
title: Configurar una pantalla externa
description: 
published: true
date: 2024-07-23T22:58:58.557Z
tags: externa, pantalla, configurar
editor: markdown
dateCreated: 2023-09-14T11:38:17.259Z
---

>Este tutorial sólo se aplica a los PCs !
{.is-warning}

Con un PC portátil (por ejemplo), podéis configurar Recalbox para que la señal vídeo aparezca en una pantalla externa. Puede ser de utilidad cuando la tarjeta gráfica no está soportada por Recalbox o cuando vuestra pantalla interna no funciona.

## Averiguar las salidas de vídeo disponibles

* Conectaros vía [SSH](./../../../tutorials/system/access/root-access-terminal-cli)
* Escribid el comando siguiente:

```shell
xrandr
```

El resultado son las salidas de vídeo disponibles, sus resoluciones y frecuencias de actualización. Por ejemplo :

```shell
Screen 0: minimum 8 x 8, current 1920 x 1080, maximum 32767 x 32767
DVI-D-0 disconnected primary (normal left inverted right x axis y axis)
HDMI-0 connected 1920x1080+0+0 (normal left inverted right x axis y axis) 598mm x 336mm
   2560x1440     59.95 +  69.93
   1920x1080     60.00*   59.94    50.00    23.98    60.05    60.00    50.04
   1680x1050     59.95
   1440x900      59.89
   1280x1024     75.02    60.02
   1280x960      60.00
   1280x800      59.81
   1280x720      60.00    59.94    50.00
   1152x864      75.00
   1024x768      75.03    70.07    60.00
   800x600       75.00    72.19    60.32    56.25
   720x576       50.00
   720x480       59.94
   640x480       75.00    72.81    59.94    59.93
DP-0 disconnected (normal left inverted right x axis y axis)
DP-1 disconnected (normal left inverted right x axis y axis)
DP-2 disconnected (normal left inverted right x axis y axis)
DP-3 disconnected (normal left inverted right x axis y axis)
DP-4 disconnected (normal left inverted right x axis y axis)
DP-5 connected (normal left inverted right x axis y axis)
   2560x1440     59.95 +
   1920x1080     59.94    50.00
   1280x720      59.94    50.00
   1024x768      60.00
   800x600       60.32
   720x576       50.00
   720x480       59.94
   640x480       59.94    59.93

```

* Abrid el fichero [recalbox.conf](./../../../basic-usage/getting-started/recalboxconf-file)
* Seleccionad vuestra salida de entre la lista anterior y rellenad esta información en la parte correspondiente del fichero `recalbox.conf` :

```ini
## Prefered external screen retrieved from ssh : xrandr
system.externalscreen.prefered=DP-5
## Force selected external screen to resolution ex : 1920x1080
system.externalscreen.forceresolution=1920x1080
## Force selected external screen to frequency ex: 60.00
system.externalscreen.forcefrequency=59.94
```

>El símbolo **;** se encuentra por defecto delante de las 2 líneas que debéis modificar. Necesitáis suprimir este carácter en cada una de las líneas para activarlas.
{.is-info}