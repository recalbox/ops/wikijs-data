---
title: Libretro bsnes
description: 
published: true
date: 2024-07-29T18:00:51.192Z
tags: libretro, satellaview, bsnes, 8.1+
editor: markdown
dateCreated: 2023-09-07T20:28:03.260Z
---

## ![](/emulators/license.svg) Licencia

Este núcleo está bajo la licencia [**GPLv3**](https://github.com/libretro/bsnes-libretro/blob/master/LICENSE.txt).

## ![](/emulators/compatibility.png) Compatibilidad

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ❌ | ❌ | ✅ | ✅ | ❌ | ❌ | ✅ | ✅ |

## ![](/emulators/features.png) Características

## ![](/emulators/bios.svg) BIOS

>**No se requiere BIOS.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Extensiones soportadas

Las roms deben tener la siguiente extensión:

* .bs
* .sfc
* .smc
* .zip
* .7z

Este sistema soporta roms comprimidas en formato .zip/.7z. Sin embargo, se trata solo de un archivo.

Los archivos contenidos en los .zip/.7z deben corresponder a las extensiones mencionadas anteriormente.
Además, cada archivo .zip/.7z debe contener solo una rom comprimida.

### Ubicación

Coloca las roms de la siguiente manera:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 satellaview
┃ ┃ ┃ ┃ ┣ 🗒 **juego.zip**

>Se recomiendan encarecidamente las roms en formato **No-Intro**.
{.is-success}

>Para más información sobre las roms, visita [este tutorial](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

>Para poder conservar tus configuraciones personalizadas durante una actualización, te recomendamos utilizar nuestra funcionalidad [Sobrecargas de configuración](./../../../advanced-usage/configuration-override).
{.is-info}

### Acceder a las opciones

Puedes configurar varias opciones de dos maneras diferentes.

* A través del menú RetroArch:

┣ 📁 Menu RetroArch
┃ ┣ 📁 Opciones del núcleo
┃ ┃ ┣ 🧩 Name_option

* A través del archivo `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Opciones del núcleo

| Opciones | Valores de las opciones | Variable (para sobrecarga) | Valores de las variables |
| --- | --- | --- | --- |
| Relación de aspecto preferida | `Auto` ✅ / `Píxeles perfectos a escala` / `4:3` / `NTSC` / `PAL` | `bsnes_aspect_ratio` | `Auto` / `8:7` / `4:3` / `NTSC` / `PAL` |
| Recorte de sobreexposición | `8 píxeles`✅ / `Desactivado` | `bsnes_ppu_show_overscan` | `rrr` / `disabled` |
| Emulación de desenfoque | `Desactivado` ✅ / `Activado` | `bsnes_blur_emulation` | `disabled` / `enabled` |
| Filtro | `Ninguno` ✅ / `NTSC (RF)` / `NTSC (Composite)` / `NTSC (S-Video)` / `NTSC (RVB)` | `bsnes_video_filter` | `None` / `NTSC (RF)` / `NTSC (Composite)` / `NTSC (S-Video)` / `NTSC (RGB)` |
| PPU - modo rápido | `Activado` ✅ / `Desactivado` | `bsnes_ppu_fast` | `enabled` / `disabled` |
| PPU - desentrelazar | `Activado` ✅ / `Desactivado` | `bsnes_ppu_deinterlace` | `enabled` / `disabled` |
| PPU - sin límite de sprites | `Desactivado` ✅ / `Activado` | `bsnes_ppu_no_sprite_limit` | `disabled` / `enabled` |
| PPU - sin bloqueo VRAM | `Desactivado` ✅ / `Activado` | `bsnes_ppu_no_vram_blocking` | `disabled` / `enabled` |
| DSP - modo rápido | `Activado` ✅ / `Desactivado` | `bsnes_dsp_fast` | `enabled` / `disabled` |
| DSP - interpolación cúbica | `Desactivado` ✅ / `Activado` | `bsnes_dsp_cubic` | `disabled` / `enabled` |
| DSP - RAM de Echo separada | `Desactivado` ✅ / `Activado` | `bsnes_dsp_echo_shadow` | `disabled` / `enabled` |
| Escala | `240p (x1)` / `480p (x2)` ✅ / `720p (x3)` / `960p (x4)` / `1200p (x5)` / `1440p (x6)` / `1680p (x7)` / `1920p (x8)` | `bsnes_mode7_scale` | `1x` / `2x` / `3x` / `4x` / `5x` / `6x` / `7x` / `8x` |
| Corrección de perspectiva | `Activado` ✅ / `Desactivado` | `bsnes_mode7_perspective` | `enabled` / `disabled` |
| Sobremuestreo | `Desactivado` ✅ / `Activado` | `bsnes_mode7_supersample` | `disabled` / `enabled` |
| Mosaico HD->SD | `Activado` ✅ / `Desactivado` | `bsnes_mode7_mosaic` | `enabled` / `disabled` |
| Ejecución adelantada interna | `Desactivado ✅ / 1 / 2 / 3 / 4` | `bsnes_run_ahead_frames` | `disabled`/ `1` / `2` / `3` / `4` |
| Coprocesadores - modo rápido | `Activado` ✅ / `Desactivado` | `bsnes_coprocessor_delayed_sync` | `enabled` / `disabled` |
| Coprocesadores - preferir modo HLE | `Activado` ✅ / `Desactivado` | `bsnes_coprocessor_prefer_hle` | `enabled` / `disabled` |
| Correcciones | `Desactivado` ✅ / `Activado` | `bsnes_hotfixes` | `disabled` / `enabled` |
| Entropía (aleatoria) | `Baja` ✅ / `Alta` / `Ninguna` | `bsnes_entropy` | `Low` / `High` / `None` |
| Cálculos rápidos del procesador | `Desactivado` ✅ / `Activado` | `bsnes_cpu_fastmath` | `disabled` / `enabled` |
| Procesador | `10%` / `20%` / `30%` / `40%` / `50%` / `60%` / `70%` / `80%` / `90%` / `100% (por defecto)` ✅ / `110%` / `120%` / `130%` / `140%` / `150%` / `160%` / `170%` / `180%` / `190%` / `200%` / `210%` / `220%` / `230%` / `240%` / `250%` / `260%` / `270%` / `280%` / `290%` / `300%` / `310%` / `320%` / `330%` / `340%` / `350%` / `360%` / `370%` / `380%` / `390%` / `400%` | `bsnes_cpu_overclock` | `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` / `11%` / `120` / `130` / `140` / `150` / `160` / `170` / `180` / `190` / `200` / `210` / `220` / `230` / `240` / `250` / `260` / `270%` / `280%` / `290%` / `300%` / `310%` / `320%` / `330%` / `340%` / `350%` / `360%` / `370%` / `380%` / `390%` / `400%` |
| Coprocesador SA-1 | `10%` / `20%` / `30%` / `40%` / `50%` / `60%` / `70%` / `80%` / `90%` / `100% (por defecto)` ✅ / `110%` / `120%` / `130%` / `140%` / `150%` / `160%` / `170%` / `180%` / `190%` / `200%` / `210%` / `220%` / `230%` / `240%` / `250%` / `260%` / `270%` / `280%` / `290%` / `300%` / `310%` / `320%` / `330%` / `340%` / `350%` / `360%` / `370%` / `380%` / `390%` / `400%` | `bsnes_cpu_sa1_overclock` | `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` / `11%` / `120` / `130` / `140` / `150` / `160` / `170` / `180` / `190` / `200` / `210` / `220` / `230` / `240` / `250` / `260` / `270%` / `280%` / `290%` / `300%` / `310%` / `320%` / `330%` / `340%` / `350%` / `360%` / `370%` / `380%` / `390%` / `400%` |
| Coprocesador SuperFX | `10%` / `20%` / `30%` / `40%` / `50%` / `60%` / `70%` / `80%` / `90%` / `100% (por defecto)` ✅ / `110%` / `120%` / `130%` / `140%` / `150%` / `160%` / `170%` / `180%` / `190%` / `200%` / `210%` / `220%` / `230%` / `240%` / `250%` / `260%` / `270%` / `280%` / `290%` / `300%` / `310%` / `320%` / `330%` / `340%` / `350%` / `360%` / `370%` / `380%` / `390%` / `400%` / `410%` / `420%` / `430%` / `440%` / `450%` / `460%` / `470%` / `480%` / `490%` / `500%` / `510%` / `520%` / `530%` / `540%` / `550%` / `560%` / `570%` / `580%` / `590%` / `600%` / `610%` / `620%` / `630%` / `640%` / `650%` / `660%` / `670%` / `680%` / `690%` / `700%` / `710%` / `720%` / `730%` / `740%` / `750%` / `760%` / `770%` / `780%` / `790%` / `800%` | `bsnes_cpu_sfx_overclock` | `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` / `11%` / `120` / `130` / `140` / `150` / `160` / `170` / `180` / `190` / `200` / `210` / `220` / `230%` / `240%` / `250%` / `260%` / `270%` / `280%` / `290%` / `300%` / `310%` / `320%` / `330%` / `340%` / `350%` / `360%` / `370%` / `380%` / `390%` / `400%` / `410%` / `420%` / `430%` / `440%` / `450%` / `460%` / `470%` / `480%` / `490%` / `500%` / `510%` / `520%` / `530%` / `540%` / `550%` / `560%` / `570%` / `580%` / `590%` / `600%` / `610%` / `620%` / `630%` / `640%` / `650%` / `660%` / `670%` / `680%` / `690%` / `700%` / `710%` / `720%` / `730%` / `740%` / `750%` / `760%` / `770%` / `780%` / `790%` / `800%` |
| BIOS preferido del Super Game Boy (Reinicio requerido) | `SGB1.sfc` ✅ / `SGB2.sfc` | `bsnes_sgb_bios` | `Super Game Boy (SGB1.sfc)` / `Super Game Boy 2 (SGB2.sfc)` |
| Ocultar borde del SGB | `Desactivado` ✅ / `Activado` | `bsnes_hide_sgb_border` | `disabled` / `enabled` |
| Pistola de pantalla táctil | `Activado` ✅ / `Desactivado` | `bsnes_touchscreen_lightgun` | `enabled` / `disabled` |
| Invertir botones de disparo del Super Scope | `Desactivado` ✅ / `Activado` | `bsnes_touchscreen_lightgun_superscope_reverse` | `disabled` / `enabled` |

## ![](/emulators/external-links.png) Enlaces externos

* **Código fuente utilizado** : [https://github.com/libretro/bsnes-libretro/](https://github.com/libretro/bsnes-libretro/)
