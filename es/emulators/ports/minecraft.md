---
title: Minecraft
description: 
published: true
date: 2024-07-26T08:29:32.418Z
tags: ports, minecraft
editor: markdown
dateCreated: 2023-09-04T12:42:27.720Z
---

![](/emulators/ports/minecraft.svg){.align-center}

## Detalles técnicos

* **Autor**: Michael Fogleman

## Presentación

**Minecraft** es un videojuego sandbox desarrollado por el desarrollador sueco Markus Persson, alias Notch, y posteriormente por Mojang Studios. Se trata de un universo basado en vóxeles, generado aleatoriamente, con un sistema de artesanía basado en la explotación y transformación de los recursos naturales (mineralógicos, fósiles, animales y vegetales).

Minecon, una convención en honor a Minecraft, celebra el lanzamiento oficial del juego el 18 de noviembre de 2011. Disponible en 95 idiomas, el videojuego ha vendido más de 100 millones de copias en todas las plataformas en ocho años, y también está disponible en varias formas físicas: papercraft (origami), productos derivados (figuritas, ropa, peluches, etc.) y cajas de juegos de Lego.

Minecraft se desarrolló originalmente como un juego de navegador web, luego para Windows, Mac y Linux (usando Java). También existe una adaptación para teléfonos móviles, Minecraft Pocket Edition (abreviado Minecraft PE), lanzada en smartphones Android, terminales iOS y dispositivos Windows Phone y Windows 10. El 09 de mayo de 2012 se lanzó una versión para Xbox 360, desarrollada por 4J Studios. Desde el 18 de diciembre de 2013 está disponible una versión para PlayStation 3 desarrollada por Mojang. La versión para PS4 se lanzó el 4 de septiembre de 2014 en PlayStation Store, la de Xbox One al día siguiente, mientras que la de Wii U está disponible para descarga en Nintendo eShop desde el 17 de diciembre de 2015 y en formato físico desde el 30 de junio de 2016. La versión para Nintendo Switch se lanzó el 12 de mayo de 2017 y la de New Nintendo 3DS el 14 de septiembre de 2017. En mayo de 2020, Minecraft ha superado la marca de 200 millones de copias vendidas en todas las plataformas, lo que lo convierte tanto en el videojuego más vendido de todos los tiempos como en la sexta franquicia más vendida de todos los tiempos, con una comunidad de 126 millones de jugadores activos mensuales en mayo de 2020.

## Emuladores

[Libretro Craft](libretro-craft)