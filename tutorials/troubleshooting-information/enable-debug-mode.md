---
title: Enabling debug mode
description: 
published: true
date: 2024-06-28T15:27:35.501Z
tags: 
editor: markdown
dateCreated: 2022-04-24T21:07:24.783Z
---

In order to help you, someone can ask you to enable the debug mode (also known as « debug logs »). 
The debug mode allows to save more data regarding the system (using *.log files), it is helpful to find the issue root cause.

> Please note the debug mode is not solution as well, but it is useful for any investigation!
{.is-info}

## Procedure

- Open [recalbox.conf](../../basic-usage/getting-started/recalboxconf-file) file.
- On a new line, add this line:

```ini
emulationstation.debuglogs=1
```

- Once this is done, save the file and **reboot**.
- Once reboot is done, reproduce the issue and go to the directory `/recalbox/share/system/logs/` to get the debug file `frontend.log`

> Don't forget to remove or comment this line with a `;` above to stop the debug mode once the whole procedure is done!
{.is-info}

> Take care to disable the debug mode when useless, this option can affect emulator performances like MAME and at the end in-game experience!
{.is-info}