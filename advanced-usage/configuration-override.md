---
title: Configuration override
description: How to edit the local configuration overloads applied to specific games or to complete directories.
published: true
date: 2024-12-09T18:06:58.029Z
tags: configuration, override
editor: markdown
dateCreated: 2021-06-28T16:55:23.049Z
---

## Overview

You can override the configuration of a game or of an entire directory.

This means that by adding specific files in your roms directories, you will be able to modify the behavior of Recalbox, RetroArch or the emulator for a particular game or for all the games in a directory.

Concretely, you will be able to act on:

* Recalbox configuration
* The general configuration of RetroArch
* The configuration of RetroArch cores

Incidentally, you will also have the possibility of overridding the images and the descriptions of the roms directories as they are displayed in EmulationStation, we will see how a little later.

For now, we will be able, for a game or a set of games:

* Define a particular emulator or core
* Set a particular video resolution
* Edit RetroArch video configuration
* Modify the options of the cores
* etc.

## General info

The overrides will apply to a base file. These files are the files that are first loaded when you are launching a game:

| Override file | Target | Configuration file | 
| :--- | :--- | :--- |
| `.recalbox.conf` | **Recalbox** | `/recalbox/share/system/recalbox.conf` |
| `.retroarch.conf` | **RetroArch without core options** | `/recalbox/share/system/configs/retroarch/retroarchcustom.cfg.origin` |
| `.core.cfg` | **Core options** | `/recalbox/share/system/configs/retroarch/cores/retroarch-core-options.cfg` |

All the configuration files that can be overrided are keys / values type files. We will therefore be able to modify the value of a key of the basic configuration, or define a non-existent key.

We can even override the overrides! Indeed, the system makes it possible to define an override per directory level.
  
It will therefore start by loading the basic configuration, then successively apply all the override files that it finds in the directories, starting at the root. Then in the end, it will try to apply the game override, if it exists.

Let's take an example, if we run the game `/recalbox/share/roms/snes/platform/Aladdin (France).zip`, the configgen will try to override the RetroArch configuration, loading in order:

1. `Base: /recalbox/share/system/configs/retroarch/retroarchcustom.cfg`
2. `Path: /.retroarch.cfg`
3. `Path: /recalbox/.retroarch.cfg`
4. `Path: /recalbox/share/.retroarch.cfg`
5. `Path: /recalbox/share/roms/.retroarch.cfg`
6. `Path: /recalbox/share/roms/snes/.retroarch.cfg`
7. `Path: /recalbox/share/roms/snes/platform/.retroarch.cfg`
8. `File: /recalbox/share/roms/snes/platform/Aladdin (France).zip.retroarch.cfg`

Of course, it is not really advisable to override the configuration before reaching at least the directory of a system.

You will notice that in order to override a directory, the override files must be inside the directory, and start with a period (.).
  
Conversely, the override of a game, must be named exactly like the game, including the file extension, followed by the override suffix, `.retroarch.cfg` in the example above.

> Since the override files are located inside your roms directories, they won't be deleted or edited in case of a crash, a broken update, a reset factory or a crash of the SD card (assuming you are using an external drive for your data).
>
> They are also portable: take your USB key with you to play at a friend's house, your configuration will apply without having to touch anything!
{.is-info}

## Recalbox overrides

Overriding the Recalbox configuration has two immediate advantages:

* Be able to select a particular video mode for a game or a set of games. Pixel Perfect arcade aficionados will be delighted.
* Be able to choose a core or a standalone emulator for a game or a set of games.

There are other possibilities, and no doubt you will find some 😉

> Overriding keys that are not used by the configgen will have no effect! Do not expect to modify the behavior of EmulationStation (by overriding sorts for example).
>
> Directory override: `/path/to/your/roms/.recalbox.conf`
> Rom override: `/path/to/your/roms/game.zip.recalbox.conf`
{.is-warning}

### Example 1: multiple versions of MAME

Why settle for just one version of MAME when we could have them all?

For example, you could have MAME 2003 Plus and MAME 2010, each romset in its own directory: 

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 mame
┃ ┃ ┃ ┃ ┣ 📁 MAME2003Plus
┃ ┃ ┃ ┃ ┣ 📁 MAME2010
 
Then just add the file `/recalbox/share/roms/mame/MAME2003Plus/.recalbox.conf` with this content:

```ini
mame.emulator=libretro
mame.core=mame2003_plus
```

And the file `/recalbox/share/roms/mame/MAME2010/.recalbox.conf` with this content:

```ini
mame.emulator=libretro
mame.core=mame2010
```

And there you go ! From now on, all the games in the MAME2003Plus directory will launch with the libretro-mame2003_plus core, and those in the MAME2010 directory with the libretro-mame2010 core.

### Example 2: assign a particular core to a game

My game `/recalbox/share/pcengine/1943 Kai (Japan).zip` works better with core `libretro-mednafen_pce_fast` core than `libretro-mednafen_supergrafx` default core (totally arbitrary assumption for the example).

Until now, it was possible to do this via EmulationStation, by modifying the game's metadata. The information is stored in the `gamelist.xml` file. A file write error, an unfortunate scrape, and the entire configuration is lost.

Here you just have to add the file `/recalbox/share/roms/pcengine/1943 Kai (Japan).zip.recalbox.conf` with the following content:

```ini
global.emulator=libretro
global.core=mednafen_pce_fast
```

This time, no risk of losing the configuration! Of course, editing metadata via EmulationStation still works. On the other hand, an override file will have priority over what is stored in the `gamelist.xml` file.

### Example 3: change video mode for a game

On Raspberry Pi2, the default video mode is CEA 4 HDMI. But on the game `Blazing Stars` on FinalBurn Neo, it runs a bit slow. Switching it to 240p would certainly help, besides being Pixel Perfect (again, completely hypothetical, just for the example)!

We add in the file `/recalbox/share/roms/fba_libretro/blazstar.zip.recalbox.conf` with the following content:

```ini
global.videomode=CEA 8 HDMI
```
When I run this game, my TV switches to 240p, and I can fully enjoy `Blazing Stars`.

## RetroArch overrides

RetroArch configurations concern RetroArch itself (and the configuration options are very numerous!) As well as the different cores, which each have specific options depending on the machines they emulate.

> There are already mechanisms specific to Recalbox and RetroArch to override either the command line which launches the emulator (via recalbox.conf), or directly the RetroArch / Cores configurations (via the RetroArch menus).
>
> But none of these systems made it possible to apply the overrides to entire directories and / or to keep these specific configurations in the same place as the roms. This is particularly interesting for people who use network shares to supply roms to several Recalbox!
{.is-info}

### RetroArch configuration

The Retroarch configuration itself is extremely rich, and covers a lot of different areas.

There is no need to dwell on the question, the best is to look directly in the [RetroArch file](https://github.com/libretro/RetroArch/blob/master/retroarch.cfg), which is particularly well documented.

The possibilities offered by local overrides are enormous, among which we can count:

* Video configuration: ratio, scale, anti-alias, screen rotation, or even shader selections, etc.
* Audio tuning for some challenging games
* Overlays
* Some options concerning the inputs: mouse selection, sensitivity, etc.
* Force particular NetPlay configurations
* Change RetroArch directories (backup for example)
* Set various options: Rewind, Fast forward, etc.
* ...

Like the Recalbox configuration overrides, we will be able to create `.retroarch.cfg` files for the directories and for the roms.

#### Example: forcing a specific controller configuration

Some N64 games like GoldenEye 007 or Perfect Dark as well as Palm games must have the `Analog to Digital` option disabled.

You just need to add the file `/recalbox/share/roms/n64/.retroarch.cfg` :

```ini
input_player1_analog_dpad_mode = "0"
input_player2_analog_dpad_mode = "0"
input_player3_analog_dpad_mode = "0"
input_player4_analog_dpad_mode = "0"
```

For a game, name the file like this: `gamename.ext.retroarch.cfg`. For a whole system, `.retroarch.cfg` is sufficient.

### Cores configurations

Overrideing the options of the cores offers enormous possibilities, among which a feature eagerly awaited by computer fans: the possibility of defining a directory per subsystem!

> The cores overrides are added to the file `/recalbox/share/system/configs/retroarch/cores/retroarch-core-options.cfg` when the game in question is launched, this implies that, once the game is closed, they will be saved in this same file. To avoid any unpleasant surprises, we recommend overriding the keys in the directory with default values (for example `fbneo-frameskip = "0"`) if you want to override a particular game with specific values (for example `fbneo-frameskip = "2"`), so you'll keep your" base "values for files that don't have custom overrides.
{.is-info}

This is particularly interesting for multi-machine cores, like:

* **theodore**, which covers Thomson MO5, MO6 and TO7 up to TO9 +
* **atari800**, which covers all Atari 8bits, from the first 800 series, to XE, including XL.
* **vice**, which now emulates C64, PET, Vic20, CBM2, ...
* **hatari**, which emulates from the first 520ST to the last Falcons
* and even more...

>You can find a list of available options for each core in the core page in the `Emulators` section of this wiki.
{.is-success}

#### Example 1 : configure Thomson subsystems

The Thomsons, 8-bit French computers from the 80s, were split into 2 series:

* The MOs, which produced the first MO5, then later the MO6 with its mechanical keyboard and integrated cassette player
* The TO, which gave the first TO7 and TO7-70, then later the TO8 and TO8D, with floppy drives, and the TO9 and TO9 + series, computers with more professional looks

MO and TO games are not compatible at all. Within the same series, there is backward compatibility: An MO6 will (in general) run the games of the MO5.
  
Of course, we will try to emulate each game with the machine closest to the machine for which it was originally designed, in order to avoid any problems and maximize the chances of having a perfect emulation.

If we take the TOSEC packs([http://www.tosec.org](https://www.tosecdev.org/)), Thomson games have been divided into 4 subsystems:

* MO5
* MO6
* TO7 (TO7-70)
* TO8, TO8D, TO9 et TO9+

We will therefore create a similar tree structure:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 thomson
┃ ┃ ┃ ┃ ┣ 📁 MO5
┃ ┃ ┃ ┃ ┣ 📁 MO6
┃ ┃ ┃ ┃ ┣ 📁 T07
┃ ┃ ┃ ┃ ┣ 📁 TO8,T08D,T09,T09+

In the system root, we are going to place a core override file, to force some interesting options for everyone:

We will put in the file `/recalbox/share/roms/thomson/.core.cfg` this content:

```ini
theodore_rom = "Auto"
theodore_autorun = "enabled"
theodore_floppy_write_protect = "enabled"
theodore_tape_write_protect = "enabled"
```

If we add games to the root, or to another directory, we tell the emulator to try to find the best machine (based on the name of the file).
  
We also protect the roms files by default, and we engage the mechanics of autorun, very practical when we do not know too much about the original machines.

Then in each sub-directory, we will add an override on the key `theodore_rom` which determines the machine.

File `/recalbox/share/roms/thomson/MO5/.core.cfg`:

```ini
theodore_rom = "MO5"
```

File `/recalbox/share/roms/thomson/MO6/.core.cfg`:

```ini
theodore_rom = "MO6"
```

File `/recalbox/share/roms/thomson/TO7/.core.cfg`:

```ini
theodore_rom = "TO7"
```

File `/recalbox/share/roms/thomson/TO8,TO8D,TO9,TO9+.core.cfg`:

```ini
theodore_rom = "TO9+"
```

For the last series, the most powerful machine was selected: the TO9+.

It's over! You now have 4 Thomson subsystems. The emulator is no longer in automatic mode and the risk of it choosing the wrong system or a default system disappears.

Of course, the Theodore core can sometimes "auto-detect" the machine, but this is not the case with other cores which need to have the correct subsystem at launch.

## Override the appearance of directories

To perfect the possibilities offered by the definition of subsystems, whether by overrideing the Recalbox configuration for MAME for example, or by overriding the Theodore core to take full advantage of the TO and MO machines of the time, we added the possibility to override the image and the description of a directory.

This allows for example, to have on a directory, the photo and the description of the machine whose roms are in said directory.

All you have to do is add at least an image file in PNG format, named `.folder.picture.png` in the directory whose image you want to override in EmulationStation. The resolution does not matter, but keep in mind that the same or similar resolution to your scrape images is still recommended.

Optionally, you can add a text description, which will slip under the image, exactly like for scraped games. The file should be a simple text file, named `.folder.description.txt`.

### Exemple 1 : from MO5 to TO9

Let's go back to the `thomson` directory used above, which we had split into 4 subsystems as TOSEC did.

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 thomson
┃ ┃ ┃ ┃ ┣ 🗒 .core.cfg
┃ ┃ ┃ ┃ ┣ 🗒 gamelist.xml
┃ ┃ ┃ ┃ ┣ 📁 media
┃ ┃ ┃ ┃ ┃ ┣ 📁 box3d
┃ ┃ ┃ ┃ ┃ ┣ 📁 images
┃ ┃ ┃ ┃ ┃ ┣ 📁 videos
┃ ┃ ┃ ┃ ┣ 📁 MO5
┃ ┃ ┃ ┃ ┃ ┣ 🗒 .core.cfg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 .folder.description.txt
┃ ┃ ┃ ┃ ┃ ┣ 🗒 .folder.picture.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 [FD]
┃ ┃ ┃ ┃ ┃ ┣ 🗒 [K7]
┃ ┃ ┃ ┃ ┃ ┣ 🗒 [M5]
┃ ┃ ┃ ┃ ┃ ┣ 🗒 [QD]
┃ ┃ ┃ ┃ ┃ ┣ 🗒 [SAP]
┃ ┃ ┃ ┃ ┃ ┣ 🗒 [WAV]
┃ ┃ ┃ ┃ ┣ 📁 MO6
┃ ┃ ┃ ┃ ┃ ┣ 🗒 .core.cfg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 .folder.description.txt
┃ ┃ ┃ ┃ ┃ ┣ 🗒 .folder.picture.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 [K7]
┃ ┃ ┃ ┃ ┣ 📁 TO7
┃ ┃ ┃ ┃ ┃ ┣ 🗒 .core.cfg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 .folder.description.txt
┃ ┃ ┃ ┃ ┃ ┣ 🗒 .folder.picture.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 [K7]
┃ ┃ ┃ ┃ ┃ ┣ 🗒 [M7]
┃ ┃ ┃ ┃ ┣ 📁 TO8,TO8D,TO9,TO9+
┃ ┃ ┃ ┃ ┃ ┣ 🗒 .core.cfg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 .folder.description.txt
┃ ┃ ┃ ┃ ┃ ┣ 🗒 .folder.picture.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 [FD]
┃ ┃ ┃ ┃ ┃ ┣ 🗒 [K7]
┃ ┃ ┃ ┃ ┃ ┣ 🗒 [QD]
┃ ┃ ┃ ┃ ┃ ┣ 🗒 [SAP]

The `.folder.picture.png` file in the` /recalbox/share/roms/thomson` directory contains an image of the machine:

![MO5 photo](/advanced-usage/config-overloads/mo5.folder.picture.png)

And the file `/recalbox/share/roms/thomson/MO5/.folder.description.txt` contains:

```text
Brand:
    - Thomson (France)
CPU:
    - Motorola 6809E running at 1 MHz
RAM:
    - 48Kb (extensible)
ROM:
    - 16Kb
Graphics:
    - Text mode : 40 columns x 25 lines
    - Graphic mode: 320 x 200, 16 colors (proximity constraint)
Sound:
    - 1bit generator (6bit DAC module extention possible)
Input devices:
    - 57 keys integrated keyboard
    - Optical pen
    - Joysticks
Interfaces:
    - External power supply
    - DIN Connector (optical pen)
    - DIN Connector (tape drive)
    - Peritel (RGB)
    - Extension port
Software:
    - BASIC 1.0 Integrated
Standard devices:
    - Cartridge port (Mémo5)
    - External tape drive (MO5 specific model)
    - External disk drive
Dimensions:
    - 51 x 291 x 190 mm 
Weight:
    - 1.1 kg
Released:
    - 1984
```

And here is the result in EmulationStation:

![EmulationStation](/advanced-usage/config-overloads/thomson-gamelist.png)

### Example 2 : MAME

In the same way, on the first example which shows how to have several versions of MAME in the MAME directory, we could imagine having a nice logo MAME 2003 Plus and MAME 2010, followed by a small text which gives the number of games, and the corresponding MAME romset version.

A small logo:

![Photo MAME 2003 Plus](/advanced-usage/config-overloads/mame.folder.picture.png)

A short text in the file `/recalbox/share/roms/mame/MAME2003Plus/.folder.description.txt`:

```text
MAME 2003 Plus is an improved version of MAME2003, with huge bug fixes and hundreds of new games.

Romset Special based on the MAME 0.78 romset

4859 games in total!
```

And here is the result:

![](/advanced-usage/config-overloads/mame-gamelist.png)

## "Standalone" Emulators

Currently, we cannot override the configuration of standalone emulators, except partially those of Amiberry, the Amiga emulator for ARM systems and Hatari which have its own overriding system in `.hatari.cfg` files and works the same way.

There are no plans to add these overrides, as they require special code and tests. It is also not necessarily feasible for all emulators, but in all cases, it will require more or less time.

However, depending on the requests and their relevance, we may possibly see it on a case-by-case basis.