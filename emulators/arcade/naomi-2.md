---
title: Naomi 2
description: 
published: true
date: 2024-07-24T08:33:32.853Z
tags: arcade, naomi 2
editor: markdown
dateCreated: 2024-07-24T08:11:20.832Z
---

![](/emulators/arcade/naomi2.svg){.align-center}

## Technical data

* **CPU**: Hitachi SH-4 32-bit RISC CPU (@200MHz 360 MIPS / 1.4 GFLOPS)
* **GPU**: NEC-Videologic PowerVR2 (CLX2)
* **Sound**: ARM7 Yamaha AICA 45 MHz (with internal 32-bit RISC CPU, 64 channels)
* **RAM**: 56 MB
* **Media**: ROM Board, max size of 168 MB

## Presentation

The Naomi 2 stands for _New Arcade Operation Machine Idea_, and also translates as 'beauty' in Japanese.

The hardware architecture of the Naomi 2 is similar to that of its predecessor, so games are compatible.

The system offers improved graphics performance, benefiting from the increased graphics capacity of the original Naomi hardware. It is now powered by two PowerVR CLX2 graphics chips and a VideoLogic Elan chip, and the amount of graphics memory has been doubled from 16 MB to 32 MB (four times more than the Dreamcast), allowing more graphic detail to be displayed.

With the Naomi 2, Sega is offering its GD-ROM drive as an optional extra. The Dimm boards and GD-Roms, whatever their version (Naomi, Naomi 2), are fully compatible with the Naomi or Naomi 2 (if the BIOS is appropriate, of course).

The Naomi 2, like its predecessor, can be connected to a network, known as the Naomi 2 Satellite Terminal.

The Naomi 2 system established itself at the time as a benchmark for arcade power, benefiting from Sega's know-how in this field.

## Emulators

[Libretro Flycast-Next](libretro-flycast-next)