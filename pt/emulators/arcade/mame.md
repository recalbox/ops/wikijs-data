---
title: Mame
description: Multiple Arcade Machine Emulator (Emulador de Múltiplas Máquinas Arcade)
published: true
date: 2021-09-26T13:33:43.201Z
tags: arcade, mame
editor: markdown
dateCreated: 2021-09-26T13:33:43.201Z
---

![](/emulators/arcade/mame.svg){.align-center}

## História

MAME, um acrônimo para Multiple Arcade Machine Emulator (Emulador de Múltiplas Máquinas Arcade), é um software de emulação para PC que visa reproduzir fielmente o funcionamento de jogos e sistemas arcade, a fim de preservar a história dos videogames.
Também integra outros emuladores e versões, tais como MESS e SDLMAME ou MEWUI.
O primeiro lançamento público data de fevereiro de 1997 com a versão 0.1; hoje estamos na v0.230 (março de 2021). Hoje contém mais de 4000 jogos únicos e mais de 9000 romsets, com um fluxo constante de novos títulos.
Abrange uma base de jogos desde 1975 até o presente.

MAME contém vários componentes:

* vários emuladores de hardware imitando o comportamento dos processadores e placas-mãe dos jogos arcade suportados;
* um emulador de controle que simula os diferentes dispositivos (joystick) através de teclados ou outros periféricos
* um emulador para simular e redirecionar as saídas de áudio e vídeo dos terminais de arcade para as respectivas saídas de computador.

## Emuladores

[Libretro MAME](libretro-mame)
[Libretro MAME2000](libretro-mame-2000)
[Libretro MAME2003](libretro-mame-2003)
[Libretro MAME2003_Plus](libretro-mame-2003-plus)
[Libretro MAME2010](libretro-mame-2010)
[Libretro MAME2015](libretro-mame-2015)
[AdvanceMAME](advance-mame)