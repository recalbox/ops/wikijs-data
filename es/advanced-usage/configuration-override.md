---
title: Sobrecargas de configuración
description: Descripción de sobrecargas de configuración específicas para juegos o carpetas enteras
published: true
date: 2023-08-29T12:39:49.317Z
tags: sobrecarga, configuración
editor: markdown
dateCreated: 2023-08-28T22:18:50.346Z
---

## Vista previa de posibilidades

Podéis sobrecargar las configuraciones de un juego o de un repertorio entero.

Esto significa que añadiendo determinados ficheros dentro de vuestra carpeta de roms váis a poder modificar el comportamiento de Recalbox, de RetroArch o del emulador para un determinado juego o para todos los juegos de una carpeta.

En concreto, podéis ajustar el comportamiento de:

* La configuración de Recalbox
* La configuración general de RetroArch
* La configuración de cores RetroArch

Además de esto, tambien es posible sobrecargar las imágenes y descripciones de las carpetas de roms que se muestran en EmulationStation. Se os explicará esto un poco más adelante.

Todo mezclado, de forma general, para un juego o un conjunto de juegos, se puede:

* Definir un emulador o core en particular
* Definir una resolución de vídeo específica
* Modificar la configuración vídeo de RetroArch
* Modificar las opciones de los cores.
* etc.

## Generalidades

Las sobrecargas de configuración se aplican sobre un fichero base. Estos ficheros son los ficheros que se cargan en primer lugar cuando se lanza un juego:

| Fichero de sobrecarga | Objetivo | Fichero de configuración |
| :--- | :--- | :--- |
| `.recalbox.conf` | **Recalbox** | `/recalbox/share/system/recalbox.conf` |
| `.retroarch.cfg` | **RetroArch sin opciones del core** | `/recalbox/share/system/configs/retroarch/retroarchcustom.cfg.origin` |
| `.core.cfg` | **Opciones del core** | `/recalbox/share/system/configs/retroarch/cores/retroarch-core-options.cfg` |


Los ficheros de configuración que se pueden sobrecargar son de tipo "clave/valor". Es decir que podemos modificar el valor de una clave de la configuración de base o también definir una nueva clave que no existía antes junto con su valor.

Incluso de pueden sobrecargar los ficheros de sobrecargas! En efecto, el sistema permite crear varias sobrecargas de forma jerárquica siguiendo los directorios.
  
Al principio se cargará la configuración de base, depués se aplicaran sucesivamente todos los ficheros de sobrecargas que se encuentren en la ruta que nos lleva al juego, comenzando por la raíz. Al final se intenterá aplicar la sobrecarga específica al juego, si existe.

Tomemos un ejemplo, si lanzamos el juego `/recalbox/share/roms/snes/platform/Aladdin (France).zip`, el sistema (configgen) buscará las (posibles) sobrecargas de configuración RetroArch, y las aplicará en el siguiente orden:

1. `Base: /recalbox/share/system/configs/retroarch/retroarchcustom.cfg`
2. `Path: /.retroarch.cfg`
3. `Path: /recalbox/.retroarch.cfg`
4. `Path: /recalbox/share/.retroarch.cfg`
5. `Path: /recalbox/share/roms/.retroarch.cfg`
6. `Path: /recalbox/share/roms/snes/.retroarch.cfg`
7. `Path: /recalbox/share/roms/snes/platform/.retroarch.cfg`
8. `File: /recalbox/share/roms/snes/platform/Aladdin (France).zip.retroarch.cfg`

En realidad, no se aconseja ni tien mucho sentido crear sobrecargas de configuración antes de llegar a la carpeta de un sistema.

Para sobrecargar la configuración de una carpeta los ficheros de sobrecarga deben encontrarse dentro de dicha carpeta, y deben comenzar por un punto (.).  
  
Sin embargo, la sobrecarga de un juego específico debe renombrarse exactamente como la rom, con la extensión incluida, seguido del sufijo de la sobrecarga, que en el ejemplo de más arriba es `.retroarch.cfg`.

>Debido al hecho que los ficheros de sobrecargas se guardan dentro de vuestras carpetas de roms, no corren peligro de perderse/borrarse si Recalbox se cuelga, o si una actualización no funciona, o si es necesario realizar un retorno a los parámetros de Recalbox de fábrica, o si se estropea vuestra tarjeta SD (siempre y cuando las roms se encuentren en un soporte externo por supuesto).
>
>Además vuestros ficheros de configuración viajan con vuetras roms y son así transportables: si os lleváis vuestros juegos a casa de un amigo, vuestra configuración se aplicará automaticamente en la Recalbox de vuestro amigo sin tener que hacer nada!
{.is-info}

## Sobregarcar Recalbox

Es interesante poder sobrecargar la configuración de Recalbox por dos cosas:

* Para poder seleccionar un modo de vídeo particular para un juego o conjunto de juegos. Los aficionados del Pixel Perfect en los juegos de arcade estarán super-contentos con esto.
* Para poder elegir un core o un emulador standalone para un juego o un conjunto de juegos.

Existen muchos otros posibles usos, no dudamos que encontraréis el vuestro 😉 

>Sobrecargar claves que no son utilizadas por configgen no tendra ningún efecto! No esperéis poder sobrecargar el comportamiento de EmulationStation (sobrecargando la forma de realizar las clasificaciones por ejemplo)
>
>Sobrecarga de carpeta : `/path/to/your/roms/.recalbox.conf`
>Sobrecarga de rom : `/path/to/your/roms/jeu.zip.recalbox.conf`
{.is-warning}

### Ejemplo 1: varias versiones de MAME

¿Por qué deberíamos contentarnos con una sóla version de MAME cuando podemos tenerlas todas al mismo tiempo?.

Por ejemplo, podríamos tener MAME 2003 Plus y MAME 2010, cada uno en su propio directorio:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 mame
┃ ┃ ┃ ┃ ┣ 📁 MAME2003Plus
┃ ┃ ┃ ┃ ┣ 📁 MAME2010
 
Basta con añadir el fichero `/recalbox/share/roms/mame/MAME2003Plus/.recalbox.conf` con el siguiente contenido:

```ini
mame.emulator=libretro
mame.core=mame2003_plus
```

Y el fichero `/recalbox/share/roms/mame/MAME2010/.recalbox.conf` con este contenido :

```ini
mame.emulator=libretro
mame.core=mame2010
```

Y ya está! A partir de ahora, todos los juegos del directorio MAME2003Plus utilizarán el core libretro-mame2003_plus, mientras que los juegos del directorio MAME2010 se lanzarán con el core libretro-mame2010.

### Ejemplo 2: asignar un core particular a un juego

Mi juego `/recalbox/share/pcengine/1943 Kai (Japan).zip` fonciona mejor con el core `libretro-mednafen_pce_fast` que con el core `libretro-mednafen_supergrafx` por defecto (hipótesis totalmente arbritaria por supuesto).

Anteriormente, podíamos hacer esto a travès de EmulationStation, modificando el metadata del juego. La información se encontraba almacenada dentro del fichero `gamelist.xml`. Un error de escritura en este fichero, un scrapping que lo reescribe, y la configuración se pierde.

Con las sobrecargas de configuración, basta con añadir el fichero `/recalbox/share/roms/pcengine/1943 Kai (Japan).zip.recalbox.conf` con el siguiente contenido:

```ini
global.emulator=libretro
global.core=mednafen_pce_fast
```

De esta forma, no hay riesgo de perder la configuración! Por supuesto, todavía se puede modificar los metadata de los juegos a travès de EmulationStation, esto todavía funciona. Sin embargo, los ficheros de sobrecarga seràn prioritarios sobre las configuraciones almacenadas en los `gamelist.xml`.

### Ejemplo 3: modificar el modo vídeo de un juego

En los Raspberry Pi2, el modo vídeo por defecto es CEA 4 HDMI. Pero el juego `Blazing Stars` de FinalBurn Neo, funciona un poquito lento, si lo pasamos a 240p funciona mejor, además de ser Pixel Perfect (una vez más, se trata de un ejemplo hipotético).

Basta con añadir el fichero `/recalbox/share/roms/fba_libretro/blazstar.zip.recalbox.conf` con el siguiente contenido:

```ini
global.videomode=CEA 8 HDMI
```

Cuando se lance el juego, la TV pasa a 240p y puedo disfrutar a tope de `Blazing Stars`!.

## Sobrecargar RetroArch

Las configuraciones RetroArch tratan de las opciones de configuración de RetroArch en sí mismo (y hay muchísimas) junto con las configuraciones de los diferentes cores, que disponen de varias opciones particulares dependiendo de las máquinas que emulan.

>Existen otros mecanismos propios a Recalbox y a RetroArch para sobrecargar las configuraciones de RetroArch, como a través de los menús de la interfaz de RetroArch o modificando ciertas opciones de la línea de comandos que lanza el emulador a travès del fichero recalbox.conf.
>
>Pero ninguno de estos mecanismo permitían ni aplicar las sobrecargas a carpetas enteras ni conservar estas opciones junto con la carpeta de roms (algo muy útil cuando se utilizan carpetas compartidas en red para alimentar varias Recalboxes a la vez!)
{.is-info}

### Configuración de RetroArch

La configuración de RetroArch en sí mismo es extremandamente rica y cuvre muchísimos dominios diferentes.

No podéis describir aquí todas las opciones disponibles, lo mejor es consular directamente el [fichero de RetroArch](https://github.com/libretro/RetroArch/blob/master/retroarch.cfg), que está muy bien documentado.

Las posibilidades que se nos ofrecen por medio de las sobrecargas de configuración son enormes. Podemos citar las más importantes:

* Configuración vídeo: aspect-ratio, escala, anti-alias, rotación de pantalla, selección de shaders, etc.
* Fine-tuning del sonido para algunos juegos complicados en este aspecto.
* Overlays
* Algunas opciones relacionadas con los inputs: selección del ratón, sensibilidad, etc.
* Forzar ciertas configuraciones de Neplay específicas.
* Cambiar los directorios de RetroArch (saves por ejemplo)
* Ajustar opciones diversas como el rebobinado, avance rápido, etc.
* ...

De una forma similar a las sobrecargas de configuración de Recalbox, vamos a poder crear ficheros `.retroarch.cfg` para sobrecargar la configuración RetroArch a nivel de una carpeta o de un juego.

#### Ejemplo: forzar una configuración de mando concreta

Algunos juegos N64 como GoldenEye 007 o Perfect Dark, al igual que los juegos Palm deben tener la opción RetroArch `Analogique vers numérique` desactivada.

Basta con añadir el fichero `/recalbox/share/roms/n64/.retroarch.cfg`:

```ini
input_player1_analog_dpad_mode = "0"
input_player2_analog_dpad_mode = "0"
input_player3_analog_dpad_mode = "0"
input_player4_analog_dpad_mode = "0"
```

Para configurar lo mismo para un determinado juego, renombrad el fichero como sigue: `nomdujeu.ext.retroarch.cfg`. Para un sistema completo, basta con `.retroarch.cfg`.

### Configuración de Cores

Sobrecargar las opciones de los cores presenta enormes posibilidades, entre ellas una funcionalidad muy esperada por los fans de los ordenadores: la posibilida de definir un directorio por subsistema!

>Las sobrecargas de cores se añaden al fichero `/recalbox/share/system/configs/retroarch/cores/retroarch-core-options.cfg` justo antes de lanzarse el juego, lo que implica que, cuando se sale del juego, dichas modificaciones quedarán registradas en dicho fichero. Para evitar sorpresas, cuando se quiere sobrecargar una opción para un juego, se recomienda crear una sobrecarga a nivel de la carpeta con los valores por defecto de las opciones que queremos modificar en el juego. Por ejemplo, si queremos modificar para un juego determinado la opción `fbneo-frameskip = "2"`, hay que crear una configuración a nivel del directorio con `fbneo-frameskip = "0"` (el valor por defecto). De esta forma, para todos los otros juegos, se aplicará el valor por defecto.
{.is-info}

Este tipo de sobrecarga de configuración es interesante en particular para los cores multi-máquinas, como:

* **theodore**, que cubre los Thomson MO5, MO6 y TO7 hasta el  TO9+ 
* ** atari800**, que cubre todos los Atari 8bits, desde la primera serie 800, hasta los XE, pasando por los XL.
* **vice**, que emula actualmente el C64, el PET, el Vic20, el CBM2, ...
* **hatari**, que emula desde el primer 520ST a los últimos Falcons 
* y otros muchos más...

>Encontraréis cada opción disponible para cada core en la página de cores de la sección `Émulateurs` de la wiki.
{.is-success}

#### Ejemplo 1: configurar los subsistemas Thomson

Los Thomson, ordenadores franceses de 8 bits de los años 80, se dividen en dos series: 

* Les MO, cuyo primer representante es el MO5, un poco más tarde el MO6 con un teclado mecánico y casette integrados.
* Los TO, cuyo primer representante es el TO7 y el TO7-70, un poco más tarde los TO8 y TO8D, con lector de disquetes, junto con la serie de los TO9 y TO9+, ordenadores de aspecto más profesional.

Los juegos MO y TO no son compatibles. Dentro de una misma serie existe una compatibilidad ascendente: Un MO6 puede hacer funcionar sin problemas los juegos del MO5, pero no al revés.

Por supuesto, la idea es emular cada juego con la máquina que se aproxima más a la máquina para la cual el juego fue creado originalmente, para evitar posibles problemas y maximizar las probabilidades de obtener una emulación perfecta.

Si nos basamos en los packs de TOSEC ([http://www.tosec.org](https://www.tosecdev.org/)), los jeugos Thomson se encuentran clasificados en 4 subsistemas diferentes:

* MO5
* MO6
* TO7 (TO7-70)
* TO8, TO8D, TO9 et TO9+

En nuestra Recalbox, creamos una jerarquía de carpetas similar:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 thomson
┃ ┃ ┃ ┃ ┣ 📁 MO5
┃ ┃ ┃ ┃ ┣ 📁 MO6
┃ ┃ ┃ ┃ ┣ 📁 T07
┃ ┃ ┃ ┃ ┣ 📁 TO8,T08D,T09,T09+

En la raíz del sistema, vamos a crear un fichero de sobrecarga del core, para forzar algunas opciones interesantes que queremos aplicar a todo el mundo:

Creamos el fichero `/recalbox/share/roms/thomson/.core.cfg` con el siguiente contenido:

```ini
theodore_rom = "Auto"
theodore_autorun = "enabled"
theodore_floppy_write_protect = "enabled"
theodore_tape_write_protect = "enabled"
```

Si añadimos juegos a la raíz de thomson, o en un nuevo repertorio diferente de los ya creados, mediante este fichero de sobrecarga de cores indicamos al emulador que intente detectar la mejor máquina basándose en el nombre de la rom.
  
De esta forma nos aseguramos que los juegos no clasificados podrán lanzarse automáticamente, lo que resulta muy práctico cuando no está claro cúal es la máquina original para la que fue concebido.

A continuación, dentro de cada carpeta, añadimos una sobrecarga para la clave `theodore_rom` que sirve para dterminar la máquina a emular.

Fichero `/recalbox/share/roms/thomson/MO5/.core.cfg` :

```ini
theodore_rom = "MO5"
```

Fichero `/recalbox/share/roms/thomson/MO6/.core.cfg` :

```ini
theodore_rom = "MO6"
```

Fichero `/recalbox/share/roms/thomson/TO7/.core.cfg`

```ini
theodore_rom = "TO7"
```

Fichero `/recalbox/share/roms/thomson/TO8,TO8D,TO9,TO9+.core.cfg`

```ini
theodore_rom = "TO9+"
```

Para la última serie, se ha seleccionado la máquina más potente: el TO9+.

Ya hemos acabado! A partir de ahora disponéis de 4 subsistemas Thomson bien configurados. El emulador no utiliza más el modo automático para estas carpetas y así nos aseguramos que no escogerá un sistema no apto para los juegos.

Por supuesto, el core Theodore puede en la mayoría de los casos "auto-détectar" la maquina correspondiente al juego, pero esto no ocurre con todos los cores.

## Sobrecargar el aspecto de las carpetas

Para completar el sistema de sobrecargas proporcionado por Recalbox, hemos añadido la posibilidad de sobrecargar la image y la descripción de une carpeta en EmulationStation.

Esto permite, por ejemplo, tener en una carpeta la foto y la descripción de la máquina que es utilizada por los juegos de dicha carpeta.

Basta con añadir como mínimo un fichero imagen en formato PNG con el nombre `.folder.picture.png` dentro de la carpeta en cuestión. La resolución importa poco, pero se recomienda una resolución similar a la resolución de las imágenes de vuestro scrapping.

Opcionalmente, podeís añadir una descripción textual, que aparecerá debajo de la imagen, exactamenete como por los juegos scrapados. El fichero es un fichero de texto simple, llamado `.folder.description.txt`.

### Ejemplo 1: del MO5 al TO9

Retomamos la carpeta`thomson` que hemos utilizado anteriormente, y que habíamos dividido en 4 subsistemas siguiendo la estructura de TOSEC.

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 thomson
┃ ┃ ┃ ┃ ┣ 🗒 .core.cfg
┃ ┃ ┃ ┃ ┣ 🗒 gamelist.xml
┃ ┃ ┃ ┃ ┣ 📁 media
┃ ┃ ┃ ┃ ┃ ┣ 📁 box3d
┃ ┃ ┃ ┃ ┃ ┣ 📁 images
┃ ┃ ┃ ┃ ┃ ┣ 📁 videos
┃ ┃ ┃ ┃ ┣ 📁 MO5
┃ ┃ ┃ ┃ ┃ ┣ 🗒 .core.cfg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 .folder.description.txt
┃ ┃ ┃ ┃ ┃ ┣ 🗒 .folder.picture.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 [FD]
┃ ┃ ┃ ┃ ┃ ┣ 🗒 [K7]
┃ ┃ ┃ ┃ ┃ ┣ 🗒 [M5]
┃ ┃ ┃ ┃ ┃ ┣ 🗒 [QD]
┃ ┃ ┃ ┃ ┃ ┣ 🗒 [SAP]
┃ ┃ ┃ ┃ ┃ ┣ 🗒 [WAV]
┃ ┃ ┃ ┃ ┣ 📁 MO6
┃ ┃ ┃ ┃ ┃ ┣ 🗒 .core.cfg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 .folder.description.txt
┃ ┃ ┃ ┃ ┃ ┣ 🗒 .folder.picture.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 [K7]
┃ ┃ ┃ ┃ ┣ 📁 TO7
┃ ┃ ┃ ┃ ┃ ┣ 🗒 .core.cfg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 .folder.description.txt
┃ ┃ ┃ ┃ ┃ ┣ 🗒 .folder.picture.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 [K7]
┃ ┃ ┃ ┃ ┃ ┣ 🗒 [M7]
┃ ┃ ┃ ┃ ┣ 📁 TO8,TO8D,TO9,TO9+
┃ ┃ ┃ ┃ ┃ ┣ 🗒 .core.cfg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 .folder.description.txt
┃ ┃ ┃ ┃ ┃ ┣ 🗒 .folder.picture.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 [FD]
┃ ┃ ┃ ┃ ┃ ┣ 🗒 [K7]
┃ ┃ ┃ ┃ ┃ ┣ 🗒 [QD]
┃ ┃ ┃ ┃ ┃ ┣ 🗒 [SAP]

El fichero `.folder.picture.png` de la carpeta `/recalbox/share/roms/thomson` contiene la imagen de la máquina :

![Photo MO5](/advanced-usage/config-overloads/mo5.folder.picture.png)

Y el fichero `/recalbox/share/roms/thomson/MO5/.folder.description.txt` contiene :

```text
Brand:
    - Thomson (France)
CPU:
    - Motorola 6809E running at 1 MHz
RAM:
    - 48Kb (extensible)
ROM:
    - 16Kb
Graphics:
    - Text mode : 40 columns x 25 lines
    - Graphic mode: 320 x 200, 16 colors (proximity constraint)
Sound:
    - 1bit generator (6bit DAC module extention possible)
Input devices:
    - 57 keys integrated keyboard
    - Optical pen
    - Joysticks
Interfaces:
    - External power supply
    - DIN Connector (optical pen)
    - DIN Connector (tape drive)
    - Peritel (RGB)
    - Extension port
Software:
    - BASIC 1.0 Integrated
Standard devices:
    - Cartridge port (Mémo5)
    - External tape drive (MO5 specific model)
    - External disk drive
Dimensions:
    - 51 x 291 x 190 mm 
Weight:
    - 1.1 kg
Released:
    - 1984
```

A continuación se muestra el resultado visto desde EmulationStation:

![EmulationStation](/advanced-usage/config-overloads/thomson-gamelist.png)

### Ejemplo 2: MAME

De igual forma, en el primero ejemplo donde mostramos cómo tener varias versiones de MAME en el directorio `` roms/mame`` , podríamos haber imaginado un bonito logo MAME 2003 Plus y otro diferente para MAME 2010, junto con un pequeño texto que muestre el nombre del juego y la versión del romset MAME correspondiente.

Un pequeño logo:

![Photo MAME 2003 Plus](/advanced-usage/config-overloads/mame.folder.picture.png)

Un pequeño texto dentro del fichero `/recalbox/share/roms/mame/MAME2003Plus/.folder.description.txt` :

```text
MAME 2003 Plus est une version améliorée de MAME2003, avec énormement de correction de bugs et des centaines de nouveaux jeux.

Romset Special basé sur le romset MAME 0.78

4859 jeux au total !
```

Y aquí podéis ver el resultado final:

![](/advanced-usage/config-overloads/mame-gamelist.png)

## Emuladores "Standalone"

Actualemente, no podemos sobrecargar las configuraciones de los emuladores standalone, a la excepción de algunas opciones del emulador Amiberry, el emulador Amiga para los sitemas ARM, al igual que Hatari el cual permite crear sobrecargas mediante ficheros `.hatari.cfg`.

No está previsto añadir sobrecargas de configuración para estos emuladores, porque se necesitaría realizar mucho código y modificaciones junto con tests particulares. Además, esto no es necesariamente factible para todos los emuladores, pero en cualquier caso, requeriría un trabajo más o menos largo.

No obstante lo dicho en el párrafo anterior, en función de las solicitudes que recibamos y de su pertinencia, podríamos evaluar una posible implementación caso a caso.