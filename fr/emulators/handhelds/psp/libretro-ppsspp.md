---
title: Libretro PPSSPP
description: 
published: true
date: 2024-06-22T23:12:18.652Z
tags: libretro, psp, ppsspp, 8.1+
editor: markdown
dateCreated: 2022-07-22T09:56:21.922Z
---

**PPSSPP** est un émulateur libre pour la console portable PSP développer en C++.
Il est disponible sur de nombreuses plate-formes, dont Android, Windows, iOS et GNU/Linux.

Il est écrit par **Henrik Hrydgard**.

## Exigences

* OpenGL / Open GL ES 2.0 ou supérieur pour le moteur de rendu OpenGL.
* Vulkan pour le moteur de rendu Vulkan.
* Direct3D 11 pour le moteur de rendu Direct3D 11.

## ![](/emulators/license.svg) Licence

Ce core est sous licence [**GPLv2**](https://github.com/libretro/ppsspp/blob/master/LICENSE.TXT).

## ![](/emulators/compatibility.png) Compatibilité

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ❌ | ❌ | ✅ | ✅ | ❌ | ❌ | ✅ | ✅ |

## ![](/emulators/features.png) Fonctionnalités

| Fonctionnalité | Supporté |
| :---: | :---: |
| Redémarrage | ✔ |
| Sauvegardes | ✔ |
| Sauvegardes instantanées | ✔ |
| Options du core | ✔ |
| Cheats natifs | ✔ |
| Controls | ✔ |
| Remapping | ✔ |
| Utilisateur | ✔ |
| Langue | ✔ |

## ![](/emulators/bios.svg) BIOS

>**Aucun bios n'est requis.**
{.is-success}

## ![](/emulators/roms.png) Isos

### Extensions supportées

L'iso doit avoir l'extension :

* .cso
* .elf
* .iso
* .pbp
* .prx

### Emplacement

Placez les isos comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 psp
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.iso**

>Les isos au format **Redump** sont vivement conseillées.
{.is-success}

>Pour plus d'information sur les isos, rendez-vous sur [ce tutoriel](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuration avancée de l'émulateur

>Pour pouvoir conserver vos configurations personnalisées lors d'une mise à jour, nous vous conseillons d'utiliser notre fonctionnalité [Surcharges de configuration](./../../../advanced-usage/configuration-override).
{.is-info}

### Accéder aux options

Vous pouvez configurer diverses options de deux façons différentes.

* Via le menu RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via le fichier `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Options du core

| Options | Valeurs des options | Variable (pour surcharge) | Valeurs des variables |
| --- | --- | --- | --- |
| Language | `Automatic` ✅ / `English` / `Japanese` / `French` / `Spanish` / `German` / `Italian` / `Dutch` / `Portuguese` / `Russian` / `Korean` / `Chinese Traditional` / `Chinese Simplified` | `ppsspp_language` | `-1` / `en_US` / `ja_JP` / `fr_FR` / `es_ES` / `de_DE` / `it_IT` / `nl_NL` / `pt_PT` / `ru_RU` / `koKR` / `zh_TW` / `zh_CN` |
| CPU Core | `Dynarec (JIT)` ✅ / `IR Interpreter` / `Interpreter` | `ppsspp_cpu_core` | `JIT` / `IR JIT` / `Interpreter` |
| Fast Memory | `Désactivé` / `Activé` ✅ | `ppsspp_fast_memory` | `disabled` / `enabled` |
| Ignore bad memory accesses | `Désactivé` / `Activé` ✅ | `ppsspp_ignore_bad_memory_access` | `disabled` / `enabled` |
| IO Timing Method | `Fast` ✅ / `Host` / `Simulate UMD delays` | `ppsspp_io_timing_method` | `Fast` / `Host` / `Simulate UMD delays` |
| Force real clock sync (Slower, less lag) | `Désactivé` ✅ / `Activé` | `ppsspp_force_lag_sync` | `disabled` / `enabled` |
| Locked CPU Speed | `Désactivé` ✅ / `222MHz` / `266MHz` / `333MHz` | `ppsspp_locked_cpu_speed` | `disabled` / `222MHz` / `266MHz` / `333MHz` |
| Cache Full ISO in RAM | `Désactivé` ✅ / `Activé` | `ppsspp_cache_iso` | `disabled` / `enabled` |
| Internal Cheats Support | `Désactivé` ✅ / `Activé` | `ppsspp_cheats` | `disabled` / `enabled` |
| PSP Model | `PSP-1000` / `PSP-2000/3000` ✅ | `ppsspp_psp_model` | `psp_1000` / `psp_2000_3000` |
| Confirmation Button | `Cross` ✅ / `Circle` | `ppsspp_button_preference` | `Cross` / `Circle` |
| Analog Circle vs Square Gate Compensation | `Désactivé` ✅ / `Activé` | `ppsspp_analog_is_circular` | `disabled` / `enabled` |
| Internal Resolution (Restart) | `480x272` ✅ / `960x544` / `1440x816` / `1920x1088` / `2400x1360` / `2880x1632` / `3360x1904` / `3840x2176` / `4320x2448` / `4800x2720` | `ppsspp_internal_resolution` | `480x272` / `960x544` / `1440x816` / `1920x1088` / `2400x1360` / `2880x1632` / `3360x1904` / `3840x2176` / `4320x2448` / `4800x2720` |
| Skip GPU Readbacks | `Désactivé` ✅ / `Activé` | `ppsspp_skip_gpu_readbacks` | `disabled` / `enabled` |
| Frameskip | `Désactivé` ✅ / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` | `ppsspp_frameskip` | `Off` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` |
| Frameskip Type | `Number of frames` ✅ / `Percent of FPS` | `ppsspp_frameskiptype` | `Number of frames` / `Percent of FPS` |
| Auto Frameskip | `Désactivé` ✅ / `Activé` | `ppsspp_auto_frameskip` | `disabled` / `enabled` |
| Duplicate Frames in 30 Hz Games | `Désactivé` ✅ / `Activé` | `ppsspp_frame_duplication` | `disabled` / `enabled` |
| Detect Frame Rate Changes (Notify frontend) | `Désactivé` ✅ / `Activé` | `ppsspp_detect_vsync_swap_interval` | `disabled` / `enabled` |
| Buffered frames (Slower, less lag, restart) | `No buffer` / `Up to 1` / `Up to 2` ✅ | `ppsspp_inflight_frames` | `No buffer` / `Up to 1` / `Up to 2` |
| Hardware Transform | `Désactivé` / `Activé` ✅ | `ppsspp_gpu_hardware_transform` | `disabled` / `enabled` |
| Software Skinning | `Désactivé` / `Activé` ✅ | `ppsspp_software_skinning` | `disabled` / `enabled` |
| Vertex Cache | `Désactivé` ✅ / `Activé` | `ppsspp_vertex_cache` | `disabled` / `enabled` |
| Lazy Texture Caching (Speedup) | `Désactivé` ✅ / `Activé` | `ppsspp_lazy_texture_caching` | `disabled` / `enabled` |
| Spline/Bezier Curves Quality | `Low` / `Medium` / `High` ✅ | `ppsspp_spline_quality` | `Low` / `Medium` / `High` |
| Hardware Tesselation | `Désactivé` ✅ / `Activé` | `ppsspp_hardware_tesselation` | `disabled` / `enabled` |
| Lower Resolution for Effects | `Désactivé` ✅ / `Safe` / `Balanced` / `Aggressive` | `ppsspp_lower_resolution_for_effects` | `disabled` / `Safe` / `Balanced` / `Aggressive` |
| Texture Scaling Type | `xBRZ` ✅ / `Hybrid` / `Bicubic` / `Hybrid + Bicubic` | `ppsspp_texture_scaling_type` | `xbrz` / `hybrid` / `bicubic` / `hybrid_bicubic` |
| Texture Scaling Level | `Désactivé` ✅ / `2x` / `3x` / `4x` / `5x` | `ppsspp_texture_scaling_level` | `disabled` / `2x` / `3x` / `4x` / `5x` |
| Texture Deposterize | `Désactivé` ✅ / `Activé` | `ppsspp_texture_deposterize` | `disabled` / `enabled` |
| Texture Shader (Vulkan only, overrides Texture Scaling Type) | `Désactivé` ✅ / `Tex2xBRZ` / `Tex4xBRZ` / `TexMMPX` | `ppsspp_texture_shader` | `disabled` / `2xBRZ` / `4xBRZ` / `MMPX` |
| Anisotropic Filtering | `Désactivé` / `2px` / `4px` / `8px` / `16x` ✅ | `ppsspp_texture_anisotropic_filtering` | `disabled` / `2px` / `4px` / `8px` / `16x` |
| Texture Filtering | `Auto` ✅ / `Nearest` / `Linear` / `Auto max quality` | `ppsspp_texture_filtering` | `Auto` / `Nearest` / `Linear` / `Auto max quality` |
| Texture Replacement | `Désactivé` ✅ / `Activé` | `ppsspp_texture_replacement` | `disabled` / `enabled` |

## ![](/emulators/external-links.png) Liens externes

* **Code source utilisé** : [https://github.com/hrydgard/ppsspp](https://github.com/hrydgard/ppsspp)
* **Documentation Libretro** : [https://docs.libretro.com/library/ppsspp/](https://docs.libretro.com/library/ppsspp/)
* **Site officiel** : [http://www.ppsspp.org/](http://www.ppsspp.org/)
* **Forum officiel** : [https://forums.ppsspp.org/](https://forums.ppsspp.org/)
* **Documentation officielle** : [http://www.ppsspp.org/guides.html](http://www.ppsspp.org/guides.html)