---
title: Wonderswan Color
description: Bandai Wonderswan Color
published: true
date: 2024-07-26T08:35:08.697Z
tags: color, bandai, wonderswan, portátiles
editor: markdown
dateCreated: 2023-09-04T12:25:12.158Z
---

![](/emulators/handheld/wonderswancolor.svg){.align-center}

## Detalles técnicos

* **Fabricante**: Bandai
* **Año de lanzamiento**: 2000
* **Cantidad vendida**: 740.727
* **Procesador**: SSPGY-1002 @ 3.072 MHz 16-bit NEC V30MZ duplicado
* **Memoria**: 64 kB VRAM/WRAM compartidos
* **Resolución**: 224 x 144 píxeles
* **Pantalla**: LCD reflectante FSTN, 2,8 pulgadas (71 mm), 241 colores
* **Sonido**: 1 altavoz mono, auriculares opcionales

## Presentación

La **WonderSwan Color** es una videoconsola portátil creada por Bandai.

Se trata de una mejora de la WonderSwan original. Salió a la venta el 30 de diciembre de 2000 en Japón y tuvo un gran éxito, llegando a captar hasta el 8% del mercado japonés, dominado por Nintendo y sus diferentes Game Boys. La WonderSwan Color es compatible con los juegos de la WonderSwan original.

En noviembre de 2002, Bandai lanzó una evolución de la WonderSwan Color, bajo el nombre de SwanCrystal, con una pantalla mejorada.

## Emuladores

[Libretro Mednafen_WSwan](libretro-mednafen-wswan)