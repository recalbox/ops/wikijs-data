---
title: Comandos de Linux útiles
description: 
published: true
date: 2023-09-18T06:26:51.933Z
tags: linux, comandos, útiles
editor: markdown
dateCreated: 2023-09-18T06:26:51.933Z
---

## Comandos generales

Aquí tienes una lista de comandos para Linux que pueden resultarte útiles para navegar por Recalbox a través de un terminal.

* **ls**
Su equivalente en Windows es `dir`. Lista el contenido de una carpeta. Puedes consultar las opciones disponibles en [esta página](http://pwet.fr/man/linux/commandes/ls/).
* **cd**
Su equivalente en Windows es `cd`. Sirve para cambiar de directorio (cd = Change Directory). Puedes consultar las opciones disponibles en [esta página](http://pwet.fr/man/linux/commandes/posix/cd/).
* **cp**
Su equivalente en Windows es `copy`. Se utiliza para copiar ficheros o directorios. Puedes consultar las opciones disponibles en [esta página](http://pwet.fr/man/linux/commandes/cp/).
* mv**
Su equivalente en Windows es `move` o `ren`. Se utiliza para mover o renombrar un fichero o directorio. Puedes consultar las opciones disponibles en [esta página](http://pwet.fr/man/linux/commandes/mv/).
**mkdir**
Su equivalente en Windows es `mkdir` o `md`. Se utiliza para crear un directorio vacío. Puedes consultar las opciones disponibles en [esta página](http://pwet.fr/man/linux/commandes/mkdir/).
* **rmdir**
Su equivalente en Windows es `rmdir` o `rd`. Sirve para borrar un directorio. Puedes consultar las opciones disponibles en [esta página](http://pwet.fr/man/linux/commandes/rmdir/).
* **nano**
Esto abre un editor de texto. Puedes consultar las opciones disponibles en [esta página](http://pwet.fr/man/linux/commandes/nano/).
**chmod**
Permite cambiar los permisos de acceso a los ficheros. Puedes consultar las opciones disponibles en [esta página](http://pwet.fr/man/linux/commandes/chmod/).
**wget**
Permite descargar una fuente disponible en Internet. Puedes consultar las opciones disponibles en [esta página](http://pwet.fr/man/linux/commandes/wget/).

## Comandos específicos

### Comprobar la temperatura del procesador del Raspberry Pi

Para verificar la temperatura del procesador (CPU), lanzad el siguiente comando: 

`cat /sys/class/thermal/thermal_zone0/temp`

A continuación, dividid el resultado entre 1000 para obtener la temperatura en grados Celsius.

```shell
# cat /sys/class/thermal/thermal_zone0/temp
54530
```

### Comprobar los parámetros de overclocking aplicados

Para comprobar los valores de overclocking aplicados, ejecutad el siguiente comando:

`cat /boot/recalbox-oc-config.txt`

Obtendréis un resultado similar al siguiente:

```text
# Plateform: RPI4
# Description: Extrem
# Warning
# Frequency: 2000

over_voltage=6
arm_freq=2000
gpu_freq=750
```

### Evitar reiniciar el sistema si el emulador Mupen64plus de N64 se bloquea

Si tenéis problemas con el emulador standalone de N64 Mupen64 que se bloquea o se congela, principalmente debido a problemas de compatibilidad con las roms, podéis "matar" fácilmente el emulador en lugar de reiniciar a lo bruto el Raspberry Pi. Para ello, basta con identificar el proceso de Mupen64 con el siguiente comando:

`ps aux | grep mupen64`

Obtendréis unas líneas como las siguientes:

```shell
26193 root     mupen64plus --corelib /usr/lib/libmupen64plus.so.2.0.0 --gfx /usr/lib/mupen64plus/mupen64plus-video-gliden64.so --configdir /recalbox/share/system/configs/mupen64/ --datadir /recalbox/share/system/configs/mupen64/ /recalbox/share/roms/n64/007 - GoldenEye (Europe).n64
26196 root     grep mupen64
```

El "id" del proceso Mupen64 \(PID\) es el **26193**. Con este identificador, podéis matar el proceso con el siguiente comando:

`kill -1 <votre_PID>`

Para nuestro ejemplo sería: `kill -1 26193`

Cuando el proceso muere, volvéis automáticamente a EmulationStation.

### Detener / iniciar EmulationStation

Para actualizar la lista de juegos, por ejemplo, o para actualizar las imágenes de los juegos recién scrapeados con vuestro PC, tendréis que parar y reiniciar EmulationStation :

* Parada :  `es stop` 
* Inicio : `es start` 