---
title: Conectad vuestra Recalbox a una pantalla DVI
description: 
published: true
date: 2023-09-14T14:24:36.989Z
tags: dvi, pantalla
editor: markdown
dateCreated: 2023-09-14T14:24:36.989Z
---

¿Necesitáis conectar Recalbox a una pantalla DVI pero obtenéis una pantalla negra o una línea rosa alrededor del borde de la pantalla?

* Montad [la partición de arranque](./../../../tutorials/system/access/remount-partition-with-write-access) en modo lectura-escritura.
* Modificad el fichero [fichier recalbox-user-config.txt](./../../../tutorials/system/modification/configtxt-configuration) comentando la siguiente línea:

```ini
hdmi_drive=2
```

* Con un # por delante:

```ini
#hdmi_drive=2
```