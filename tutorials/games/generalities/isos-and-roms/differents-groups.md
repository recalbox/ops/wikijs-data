---
title: Different groups
description: 
published: true
date: 2022-01-05T00:21:14.720Z
tags: groups
editor: markdown
dateCreated: 2021-08-07T15:25:42.593Z
---

To know which roms you need for a console, you need to know the different groups that reference them.

The difference between these groups is simple, they are each specialized in a type of support (floppy disk, cartridge and CD-ROM).

## FinalBurn Neo (⭐⭐⭐⭐)

### Dat files

You can download these files on [this site](https://github.com/libretro/FBNeo/tree/master/dats).

## Mame (⭐⭐⭐⭐)

### Dat files

You can download these files on [this site](https://www.progettosnaps.net/dats/MAME/).

## No-intro (⭐⭐⭐⭐)

No-Intro focuses only on games published on **cartridge media** (Master-System, SNES, N64, Gameboy, etc).

No-Intro keeps only the best roms, without errors or changes, and which are as close as possible to the original cartridges.

No-Intro usually contains only one working version of each game for a given country (US, Eur, Jp, Fr, De, ...) as well as all updates of these games (Rev1, Rev2, Beta, ...).

>These sets are therefore the best roms for those who want to use the Netplay feature.
{.is-success}

### Dat files

You can download these files on this [site](https://datomatic.no-intro.org/?page=download).

## Redump (⭐⭐⭐⭐)

Redump focuses only on games published on **optical media** (CD, DVD, ...) (PlayStation, Sega Saturn, GameCube, Dreamcast, etc).

Like No-Intro, Redump keeps only the best isos, without errors or changes, and which are as close as possible to the original media.

Redump Isos are bigger than Tosec or Trurip Isos, but they have a better quality, which directly influences the behavior during emulation.

>You can find [here](http://wiki.redump.org/index.php?title=Discs_not_yet_dumped) the list of non-dumped disks.
{.is-info}

### Dat files

You can download these files on this [site](http://redump.org/downloads/).

## Tosec (⭐⭐⭐)

TOSEC aims to list all possible content for each console or computer (Floppy disks, Cartridges and CDs but also magazines, manuals and videos - Atari-ST, Amiga, Commodore, Dreamcast, Master-System, N64, etc).

TOSEC is a cataloging / naming system. Their mission is to identify and name all ROMs found in the wild.

This seems commendable at first sight but in the end becomes extremely chaotic.

You get good but also bad ROMs, bad files, extra backup files, alternative replacement files, hack roms, fake ones, homebrews, modified roms, etc...

This way you can have 27 different versions of the same game, of which only 3 are roms that work correctly.

For example, the _TOSEC 2012-12-28_ set contains 346 different versions of Super Mario World...

>TOSEC is **excellent for archiving**, but it is not usable for personal use. That's why we advise you to use only one Tosec set if there is no alternative in No-Intro or Redump, so take the time to sort it out carefully.
{.is-warning}

### Dat files

You can download these files from this [site](https://www.tosecdev.org/).

## Trurip (⭐⭐)

TruRip focuses solely on **CD-based games**.

TruRip's mission is to have the most accurate duplicate of the original media, while this statement may sound very similar to the goals of No-Intro and Redump, it is not.

It's another level, which means that there are no headers or settings to ensure that games work correctly in emulators.

TruRip can't make an exact copy in all cases, sometimes things like subcode are still missing. This is for sure the purist of all that is going on right now.

TruRip tried to be a private group and failed. It is difficult to get information about it online.

>These sets present a lot of missing games on some platforms.
{.is-info}

### Dat files

You can download these files from this [site](http://database.trurip.org/).

## Goodsets/Goodtools (⭐⭐)

>GoodSets focuses only on games based on certain cartridge systems.
{.is-info}

The purpose of GoodSets/GoodTools is very similar to TOSEC but slightly more refined since they are just a cataloging/naming system and not a purification system like No-Intro, Redump and TruRip.

The GoodTools sets are intended to be the ideal set "hence their names", they try to have every game and every version of the game.

As a result, you will get 20 versions of Super Mario (USA, JAP, EUR, USA pirated version V1, JAP Alternate V1, etc) so that each game has all versions of the roms.

They usually identify the roms (brackets, square brackets, etc. which are added to the ROM name, see [here](./../tags-used-in-rom-names)).

>These sets are therefore a mix of roms from several groups. But not necessarily the best (for those who want to use the Netplay feature).
{.is-info}

### Dat files

You can download these files on [this site](https://cowering.blogspot.com/).