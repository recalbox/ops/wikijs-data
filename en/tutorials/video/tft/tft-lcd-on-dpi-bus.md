---
title: Configure a small TFT screen on DPI bus
description: 
published: true
date: 2021-08-06T22:43:22.235Z
tags: tft, screen, dpi, bus, small
editor: markdown
dateCreated: 2021-08-06T22:43:22.235Z
---

## Set your small TFT display to DPI

### DPI (PARALLEL DISPLAY INTERFACE)

Source : [https://www.raspberrypi.org/documentation/hardware/raspberrypi/dpi/README.md](https://www.raspberrypi.org/documentation/hardware/raspberrypi/dpi/README.md)

A parallel RGB interface up to 24 bits is available on all Raspberry Pi boards with the 40-way header (A+, B+, Pi2, Pi3, Zero) and the compute module. This interface allows to connect parallel RGB displays to the Raspberry Pi GPIO either in RGB24 (8 bits for red, green and blue) or RGB666 (6 bits per color) or RGB565 (5 bits red, 6 green and 5 blue).

This interface is controlled by the GPU firmware and can be programmed by a user via special config.txt parameters and by enabling the correct Linux device tree overlay.

Sending data via this parallel interface, you have no problems with bus bandwidth and display speed (FPS). But it uses almost all GPIO pins.

This mode is very useful, you can fine-tune your display parameters: timings, resolutions, etc.

This mode is accompanied by new overlays, which allow to produce an RGB signal thanks to the VGA666 (get the 666 passive VGA adapter for Raspberry-Pi B+ : [code](https://github.com/fenlogic/vga666) and [hardware](https://www.banggood.com/VGA-666-Adapter-Board-For-Raspberry-Pi-3-Model-B-2B-B-A-p-1071309.html)).

```ini
dtoverlay=vga666
```

You will be able to use Adafruit's [5"](https://www.adafruit.com/product/1596) or [7"](https://www.adafruit.com/product/2354) TFT display with the [Adafruit Kippah DPI TFT panel for Raspberry Pi with touch support](https://www.adafruit.com/product/2453).

```ini
dtoverlay=dpi24
```

#### Setting up

* You need to open the file [recalbox-user-config.txt](./../../system/modification/configtxt-configuration) with nano or vim and add the following lines:

```ini
dtoverlay=dpi24
```

### Geekwrom HD 3.5 inch 800x480 TFT screen

This screen is small in size and offers a resolution of 800x480. Its specifications are quite good:

![](/tutorials/video/tft/geekwrom3-5.png){.full-width}

This amazing screen can be found on [this Chinese site](https://www.banggood.com/Geekwrom-HD-3_5-Inch-TFT-Display-Shield-800x480-For-Raspberry-Pi-3B-2B-With-2-Keys-And-Remote-IR-p-1069730.html) for less than 50€.

It can be used as is, without the fbcp program. Fbcp is not necessary anymore because the DPI mode uses directly the GPU.

To get the support of this screen, you just have to add the following lines to your file [recalbox-user-config.txt](./../../system/modification/configtxt-configuration)

#### Setting up

* You must open the file [recalbox-user-config.txt](./../../system/modification/configtxt-configuration) with nano or vim and add the following lines:

```ini
# 3.5 HD tft screen 800x480
dtoverlay=dpi24
overscan_left=0
overscan_right=0
overscan_top=0
overscan_bottom=0

# Banggood
framebuffer_width=800
framebuffer_height=480
dtparam=spi=off
dtparam=i2c_arm=off

enable_dpi_lcd=1
display_default_lcd=1
dpi_output_format=0x6f015
dpi_group=2
dpi_mode=87
hdmi_timings=480 0 16 16 24 800 0 4 2 2 0 0 0 60 0 32000000 6
display_rotate=3
```

* Save the file and restart your Recalbox. You can now view the incredible images displayed on this screen.

![Geekwrom HD 3.5 inch 800x480 TFT screen](/tutorials/video/tft/geekwrom-result.jpg)

### Adafruit 5" and 7" TFT display

To use these [5"](https://www.adafruit.com/product/1596) and [7"](https://www.adafruit.com/product/2353) displays, you will need the [Adafruit Kippah DPI TFT panel for Raspberry Pi with touch support](https://www.adafruit.com/product/2453) to operate them.

#### Setting up

* You need to open the file [recalbox-user-config.txt](./../../system/modification/configtxt-configuration) with nano or vim and add the following lines:

```ini
#3.5 HD tft screen 800x480
dtoverlay=dpi24
overscan_left=0
overscan_right=0
overscan_top=0
overscan_bottom=0

#Adafruit
framebuffer_width=800
framebuffer_height=480
dtparam=spi=off
dtparam=i2c_arm=off

enable_dpi_lcd=1
display_default_lcd=1
dpi_group=2
dpi_mode=87
dpi_output_format=0x6f005
hdmi_timings=800 0 40 48 88 480 0 13 3 32 0 0 0 60 0 32000000 6
display_rotate=3
```

### 4.0" HyperPixel screen

Thanks glook for this tuto : [https://forum.recalbox.com/topic/17854/ecran-hyperpixel-4-pouces/4](https://forum.recalbox.com/topic/17854/ecran-hyperpixel-4-pouces/4)

This screen is available [here](https://shop.pimoroni.com/products/hyperpixel-4?variant=12569539706963) with or without touch screen.

Start by reading the Github [here](https://github.com/pimoroni/hyperpixel4). You will need to follow these steps:

#### Set up

Depending on your board, the source code to use will be different. See the [manual installation](https://github.com/pimoroni/hyperpixel4#manual-installation) part to know which source code you should use.

Once you have chosen the right source code, here are the steps to follow:

* Download the source code by clicking on the big green `Code` button and choose to download in `.zip` format.
* Unzip the archive and place the extracted directory in `/recalbox/share/system/`.
* Log in via [SSH](./../../system/access/root-access-terminal-cli) and go to the directory you just put in your Recalbox.
* Mount the [boot partition](./../../system/access/remount-partition-with-write-access) as read/write.
* Compile hyperpixel4.dts into hyperpixel4.dtbo :

```shell
dtc -@ -I dts –O dtb –o dist/hyperpixel4.dtbo src/hyperpixel4.dts
```

* Copy the `hyperpixel4.dtbo` file to `/boot/overlays/`.

```shell
cd dist
cp hyperpixel4.dtbo /boot/overlays/
```

* Modify the file [recalbox-user-config.txt](./../../system/modification/configtxt-configuration) with nano or vim and add the following lines:

```ini
# Paramètres de l'écran LCD HyperPixel
dtoverlay=hyperpixel4
overscan_left=0
overscan_right=0
overscan_top=0
overscan_bottom=0
framebuffer_width=800
framebuffer_height=480
enable_dpi_lcd=1
display_default_lcd=1
dpi_group=2
dpi_mode=87
dpi_output_format=0x7f216
display_rotate=3
hdmi_timings=480 0 10 16 59 800 0 15 113 15 0 0 0 60 0 32000000 6
```

* Make sure the i2c and spi are turned off:

```ini
dtparam=i2c_arm=off
dtparam=spi=off
```

* Copy `hyperpixel4-init` located in the `/dist` directory to `/usr/bin/` on Recalbox.

```shell
cp -R /dist/hyperpixel4-init /usr/bin/
```

* Make sure you run this program at startup in Recalbox, by adding `hyperpixel4-init` to the command line in one of the init scripts in `/etc/init.d`.