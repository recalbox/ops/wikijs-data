---
title: Audio HiFiBerry
description: 
published: true
date: 2023-09-17T20:15:43.641Z
tags: hifiberry
editor: markdown
dateCreated: 2023-09-17T20:15:43.641Z
---

## Información general

Para obtener una señal de audio de calidad en el Pi, una solución es utilizar un HiFiBerry DAC (Digital - Analog Converter). 

Estos **DACs** vienen en varias versiones:

### Raspberry Pi A+, B+, 2 et 3

* DAC+ Light
* DAC+ Standard RCA
* DAC+ Standard Phone
* DAC+ Pro

### Raspberry Pi A et B

* DAC Versión estándar con conectores RCA

Las especificaciones de las distintas tarjetas pueden compararse en [**hifiberry.com**](http://www.hifiberry.com/).
Las tarjetas DAC+ pueden conectarse directamente al GPIO de la Pi. Los pines no utilizados por el DAC pueden ser reutilizados para otros fines, en cuyo caso los pines adicionales deben ser soldados a la tarjeta DAC+.

A continuación se muestra el diagrama de cableado para un HifiBerry DAC+: hifiberry

![](/tutorials/audio/gpio-hifiberry.png)

>La versión para Raspberry A y B debe instalarse en el pin 5, lo que requiere algunas operaciones adicionales.  
>Una vez instalada la tarjeta, hay unos cuantos pasos de configuración del software para que todo funcione.
{.is-info}

## Árbol de dispositivos y configuración

* En Recalbox, basta con añadir en el [fichero recalbox-user-config.txt](./../system/access/remount-partition-with-write-access) el DTB apropiado, dependiendo de vuestro hardware:
  * DAC/DAC+ Light `dtoverlay=hifiberry-dac`
  * DAC+ Standard/Pro `dtoverlay=hifiberry-dacplus`    
* En este mismo fichero, añadid la línea `force_eeprom_read=0`.
* Seleccionad la tarjeta de sonido por defecto. Necesitáis modificar `/etc/asound.conf` (creadlo si no existe) con el siguiente contenido:
  * `pcm.!default {` `type hw card 0` `}` `ctl.!default {` `type hw card 0` `}`
* Reiniciad vuestro Raspberry Pi.

Ahora deberíais poder escuchar el sonido a través de vuestra tarjeta HiFiBerry.

>Sólo se puede cambiar el volumen del sonido en EmulationStation usando _Alsamixer / Amixer_.
{.is-info}

## Alsamixer / Amixer

Las tarjetas de sonido ALSA (como es el caso de las HiFiBerrys) pueden controlarse mediante Alsamixer.
Alsamixer es una interfaz gráfica, mientras que Amixer funciona en modo texto, y se utiliza especialemente integrado en scripts.

Como `alsa-utils` está instalado en Recalbox, HifiBerry funcionará sin necesidad de instalar nada más.

>El DAC+ Light y el antiguo DAC no tienen ninguna configuración en ALSA.
{.is-warning}
  
Con el comando `amixer` tendréis una versión en texto de los distintos ajustes del DAC. 
La mejor manera de cambiar el volumen es modificar el "Volumen general". Su nombre puede variar dependiendo de la tarjeta: `PCM`, `Digital`, `Master`.

Los siguientes comandos os permiten cambiar el volumen a través del terminal:

* `amixer sset ‘Master’ -- 90%` (desaconsejado porque la escala es logarítmica)
* `amixer sset ‘Master’ -- -3dB` (en decibelios, es el ajuste recomendado)
* `amixer sset ‘Master’ – 2000` (unidad empírica, desaconsejado)


>Podéis usar Google para descubrir el resto de comandos posibles buscando con las palabras clave: amixer, alsamixer.
{.is-info}

A continuación podéis observar un ejemplo en python para gestionar el volumen.

Primero, importad el fichero:

```python
from subprocess import call

call(["amixer", "sset", "Digital", "--", str(YourDesiredVolume)+"dB"])
```

En particular, este script puede ser usado para cambiar el volumen usando los GPIO.

A continuación se muestra un resumen de algunos comandos:

* `amixer sset 'PCM' 70%` (porcentaje)
* `amixer sset 'Master' 3dB` (Decibelio)
* `amixer sset 'Mic' 4` (valores propios al hardware)
* `amixer sset 'PCM' 10%+` (aumento del valor en curso. La unidad puede ser en % o en dB)
* `amixer sset 'Line' cap` (Recording on/off: cap, nocap)
* `amixer sset 'Mic' mute` (Playback on/off: mute, unmute)
* `amixer sset 'Master' off` (On/Off: on/off)
* `amixer sset 'Mic Select' 'Mic1'` (Nombre del periférico)

Fuente: [https://wiki.ubuntuusers.de/amixer/](https://wiki.ubuntuusers.de/amixer/)