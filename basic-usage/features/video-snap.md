---
title: Vidéo Snaps
description: 
published: true
date: 2021-09-04T17:46:51.673Z
tags: snap, vidéos
editor: markdown
dateCreated: 2021-05-21T07:54:14.055Z
---

Les « Vidéos Snap », ce n’est ni plus ni moins que l’affichage d’une petite vidéo de gameplay en lieu et place de votre « scrap » habituel dans votre liste de jeux.

Vous pouvez en voir un extrait vidéo [ici](https://www.youtube.com/watch?v=r5oabVrfQpk).

>Pour ceux qui n’ont jamais utilisé cette fonction, il faut savoir que les vidéos, ça prend très vite de la place, malgré leur excellente compression MP4 _(comptez entre 1 et 6Mo/pièce selon le système)_.
>Si vous voulez profiter pleinement de cette fonction, un disque dur USB3 de 1 ou 2To sera donc indispensable !
{.is-warning}

## Configuration

Afin de profiter de ces vidéos, il vous faudra au préalable (re)scraper vos listes de jeux, à l’aide de Skraper ou le nouveau scrapeur interne de Recalbox.

### Avec le scrapeur interne

Reportez-vous à [cette page](./../../basic-usage/features/internal-scraper) de cette documentation.

### Via le logiciel Skraper

Vous pouvez consulter [cette vidéo](https://www.youtube.com/watch?v=G388Gc6kkRs) pour apprendre à vous servir de Skraper.