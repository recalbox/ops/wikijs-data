---
title: Libretro NXEngine
description: 
published: true
date: 2024-07-25T22:49:39.191Z
tags: libretro, cavestory, nxengine
editor: markdown
dateCreated: 2023-09-11T14:34:00.899Z
---

Libretro NXEngine es un motor de reescritura de Cave Story de código abierto para Dingux y MotoMAGX. Autor - Caitlin Shaw (rogueeve).

## ![](/emulators/license.svg) Licencia

El core posee la siguiente licencia [**GPLv3**](https://github.com/gameblabla/nxengine-nspire/blob/master/LICENSE).

## ![](/emulators/compatibility.png) Compatibilidad

 | RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | 
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Funcionalidades

| Funcionalidad | ¿Soportada? |
| :---: | :---: |
| Reinicio | ✔ |
| Capturas de pantalla | ✔ |
| Grabaciones | ✔ |
| Controles | ✔ |
| Remapping | ✔ |

## ![](/emulators/bios.svg) BIOS

>**No necesita bios.**
{.is-success}

## ![](/emulators/roms.png) Roms


Las roms deben tener las extensiones siguientes:

* .exe

### Emplazamiento

Disponed vuestras roms como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 ports
┃ ┃ ┃ ┃ ┣ 📁 Cave Story
┃ ┃ ┃ ┃ ┃ ┣ 📁 CaveStory
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 **Doukutsu.exe**

## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

>**Este core no tiene opciones.**
{.is-success}

## ![](/emulators/external-links.png) Enlaces externos

* **Código fuente utilizado** : [https://github.com/libretro/nxengine-libretro/](https://github.com/libretro/nxengine-libretro)
* **Código fuente oficial** : [https://github.com/EXL/NXEngine/](https://github.com/EXL/NXEngine)
* **Página web oficial** : [nxengine.sourceforge.net](http://nxengine.sourceforge.net/)
* **Wiki del juego** : [https://www.cavestory.org/guides-and-faqs/list.php](https://www.cavestory.org/guides-and-faqs/list.php)