---
title: Atari Lynx
description: 
published: true
date: 2024-06-02T21:00:41.476Z
tags: 
editor: markdown
dateCreated: 2021-07-12T08:52:21.604Z
---

![](/emulators/handheld/lynx.svg){.align-center}

## Technische Daten

* **Hersteller**: Epyx / Atari
* **Erscheinungsjahr**: 1989
* **Verkaufte Einheiten**: 1 million
* **CPU**: 8-bit with 16-bit address space
* **Koprozessoren**: Mikey and Suzy 2 x 16-bit
* **RAM**: 64K 120 ns DRAM
* **ROM**: 512 bytes
* **Auflösung**: 160 x 102 pixels
* **Sound**: 4 canaux, 8-bit DAC

## Beschreibung

Der **Atari Lynx** war die einzige Handheld-Konsole von Atari und der erste Handheld mit einem Farb-LCD-Bildschirm. Er wurde 1989 veröffentlicht.

Das Gerät wurde von Epyx unter dem Namen "Handy2" entwickelt (heute ist es der Name des fortschrittlichsten PC-Emulators dieser Konsole). In einem sehr fortgeschrittenen Entwicklungsstadium suchte die Firma nach Investoren für ihr Projekt und wählte zunächst Nintendo, die sich weigerten, in das Projekt zu investieren, was Epyx dazu veranlasste, sich an Atari zu wenden, die 1988 die Rechte kauften.

Die Firma modifizierte den internen Lautsprecher und entfernte den Stick, der sich damals auf dem Pad befand. Atari brachte die Konsole dann zwei Jahre später auf den Markt. Der Bildschirm konnte umgedreht werden, um auch als Linkshänder spielen zu können, und bis zu 8 Spieler konnten in einem Netzwerk miteinander spielen. Die technischen Möglichkeiten waren wesentlich besser als die des Game Boy (Farbe und 3D). Der Lynx hingegen war größer, zu batterieintensiv und es fehlte an vielen Titeln.

1991 brachte Atari eine zweite Version seiner Konsole in neuer Form und mit neu gestalteten Cartridges heraus. Die neue Konsole (von Atari "Lynx II" genannt) hatte Griffe, einen qualitativ besseren Bildschirm und eine Energiesparoption, die es erlaubte, die Konsole in den Schlafmodus zu versetzen.

## Emulatoren

[Libretro Handy](libretro-handy)
[Libretro Mednafen_Lynx](libretro-mednafen_lynx)