---
title: Acesso à rede via WinSCP
description: 
published: true
date: 2021-12-23T21:16:03.381Z
tags: winscp, acesso, rede
editor: markdown
dateCreated: 2021-12-23T21:14:37.859Z
---

Este tutorial é destinado a usuários do Windows. Consulte [este tutorial](./../../../tutorials/system/access/network-access-cyberduck) se você for um usuário macOS ou se quiser usar o Cyberduck no Windows.

## Pré-requisitos

* Verifique se o Recalbox está funcionando e conectado à rede por meio de um cabo Ethernet ou dongle Wifi.

> Você tem uma dúvida e seu Recalbox está conectado? Vá para [este tutorial](./../../../tutorials/network/ip/discover-recalbox-ip) para descobrir o endereço IP do seu Recalbox.
{.is-info}

* Baixe e instale o programa [WinSCP](https://winscp.net/)

## Configuração

* Inicie o `WinSCP` e uma janela aparecerá para criar sua primeira conexão.
* Nesta janela, preencha as informações da seguinte forma:
  * Protocolo de arquivo `SFTP`
  * Nome do host `recalbox` ou o endereço IP que você identificou anteriormente (a configuração será alterada cada vez que você reiniciar o Recalbox se você inserir o endereço IP e o IP for dinâmico).
  * Número da porta `22`.
  * Nome de usuário `root`.
  * Senha `recalboxroot`
* Clique em `Salvar...`.
* Preencha as informações da seguinte forma:
  * Salve a sessão como: `Recalbox` por exemplo.
  * Marque a caixa `Salvar senha (não recomendado)` e clique em `OK`.
* Seu programa agora está configurado.

## Login

* Na janela aberta, selecione a conexão criada e clique em `Logon`.
* Outra janela será aberta informando sobre a conexão atual.
* Você está conectado.

> Outra janela pode abrir solicitando que você `Continue se conectando ao servidor desconhecido e adicione a chave do host ao cache` Clique em `Sim` se for o caso.
{.is-warning}

## Mostrar arquivos em cache

Isso é útil para acessar a pasta `.emulationstation` para alterar o tema, por exemplo.

* Clique em `Opções`>`Preferências`.
* Selecione `Painéis` do lado esquerdo.
* Marque a caixa `Mostrar arquivos ocultos` e clique em `OK`.

## Uso

O uso é bastante intuitivo. Por padrão, você encontrará seu computador à esquerda e à direita, a estrutura em árvore do seu Recalbox.

**Algumas dicas**:

* Para retornar à pasta superior ou à raiz do seu Recalbox, basta clicar na primeira pasta com uma imagem para voltar e identificada como `..`.
* Você pode soltar pastas simplesmente arrastando e soltando-as na coluna da direita.
* Você pode alterar as permissões em um arquivo clicando com o botão direito sobre ele e depois em `Propriedades`.

## Adicionar Notepad ++

* Faça download do [Notepad ++](https://notepad-plus-plus.org/downloads/).
* No WinSCP, clique em `Opções`>`Preferências`.
* Selecione `Editores` à esquerda.
* Clique em `Adicionar` e encontre o local onde você instalou o Notepad ++.

## Use Winscp para usar o console

Clique no ícone `Console` e digite o comando para escrever a partição.

![](/tutorials/system/access/winscp.png)