---
title: Libretro EmuSCV
description: 
published: true
date: 2024-07-24T18:58:26.607Z
tags: libretro, 7.2+, super cassette vision, emuscv
editor: markdown
dateCreated: 2021-08-03T10:59:05.182Z
---



## ![](/emulators/license.svg) License

This core is under [**GPLv3**](https://gitlab.com/MaaaX-EmuSCV/libretro-emuscv/-/blob/master/licence.txt) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Features

| Feature | Supported |
| :---: | :---: |
| Restart | ✔ |
| Screenshots | ✔ |
| Saves | - |
| States | - |
| Rewind | - |
| Controls | ✔ |

## ![](/emulators/bios.svg) BIOS

### List of mandatory bios

| Filename | Description | MD5 | Provided |
| :---: | :---: | :---: | :---: |
| upd7801g.s01 | BIOS | 635a978fd40db9a18ee44eff449fc126 | ❌ |

### Location

Put the bios like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 scv
┃ ┃ ┃ ┃ ┣ 🗒 **upd7801g.s01**

## ![](/emulators/roms.png) Roms

### Supported extensions

Roms must have the extension:

* .0
* .bin
* .cart
* .rom
* .zip

This system supports compressed roms in .zip format. However, be careful, it is only an archive.

The files contained in the .zip must correspond to the extensions mentioned above.
Moreover, each .zip file must contain only one compressed rom.

### Location

Put the roms like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 scv
┃ ┃ ┃ ┃ ┣ 🗒 **game.zip**

The particularity is that if you load a known `.bin`, `.rom` or `.0` (`.1`, `.2`, etc.) ROM, it creates a `.CART` ROM file which tells the emulator the right data mapping (the cartridge can contain several ROMs, possibly RAM, or even SRAM saved by a stack). For games where you could save, it also creates a `.SAVE` file which contains the SRAM data (saved when you quit).

The `.CART` is created in the `roms` directory.

If it can't create the `.CART`, it doesn't load the game, it starts on the console test screen like when the console can't read a cartridge.
I'll modify the code so that it loads anyway and see how I can put it in the savegames directory.

>The roms using **No-Intro** format are highly recommended.
{.is-success}

>For more information about roms, go to [this tutorial](./../../../tutorials/games/generalities/isos-and-roms)!
{.is-info}

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Accessing to options

You can configure various options in two different ways.

* Via RetroArch menu:

┣ 📁 RetroArch Menu
┃ ┣ 📁 Core options
┃ ┃ ┣ 🧩 Name_option

* Via the file `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core options

| Options | Options values | Variables (for configuration override) | Variables values |
| --- | --- | --- | --- |
| CONSOLE | `AUTO` ✅ / `EPOCH` / `YENO` / `EPOCHLADY` | `emuscv_console` | `AUTO` / `EPOCH` / `YENO` / `EPOCHLADY` |
| DISPLAY | `AUTO` ✅ / `EMUSCV` / `EPOCH` / `YENO` | `emuscv_display` | `AUTO` / `EMUSCV` / `EPOCH` / `YENO` |
| PIXELASPECT | `AUTO` ✅ / `RECTANGULAR` / `SQUARE` | `emuscv_pixelaspect` | `AUTO` / `RECTANGULAR` / `SQUARE` |
| RESOLUTION | `AUTO` ✅ / `LOW` / `MEDIUM` / `HIGH` | `emuscv_resolution` | `AUTO` / `LOW` / `MEDIUM` / `HIGH` |
| PALETTE | `AUTO` ✅ / `STANDARD` / `OLDNTSC` | `emuscv_palette` | `AUTO` / `STANDARD` / `OLDNTSC` |
| FPS | `AUTO` ✅ / `EPOCH60` / `YENO50` | `emuscv_fps` | `AUTO` / `EPOCH60` / `YENO50` |
| DISPLAYFULLMEMORY | `AUTO` ✅ / `YES` / `NO` | `emuscv_displayfullmemory` | `AUTO` / `YES` / `NO` |
| DISPLAYINPUTS | `AUTO` ✅ / `YES` / `NO` | `emuscv_displayinputs` | `AUTO` / `YES` / `NO` |
| LANGAGE | `AUTO` ✅ / `JP` / `FR` / `EN` | `emuscv_langage` | `AUTO` / `JP` / `FR` / `EN` |
| BIOS | `AUTO` ✅ / `YES` / `NO` | `emuscv_checkbios` | `AUTO` / `YES` / `NO` |

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://gitlab.com/MaaaX-EmuSCV/libretro-emuscv/](https://gitlab.com/MaaaX-EmuSCV/libretro-emuscv/)
* **Libretro documentation**: [https://docs.libretro.com/library/emuscv/](https://docs.libretro.com/library/emuscv/)