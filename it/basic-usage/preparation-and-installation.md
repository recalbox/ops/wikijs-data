---
title: Preparazione ed Installazione
description: Come Installare Recalbox
published: true
date: 2024-11-28T15:23:36.236Z
tags: 
editor: markdown
dateCreated: 2021-06-18T17:12:30.856Z
---

## I - L'Hardware compatibile

È possibile installare Recalbox su differenti tipi di hardware:

- **Raspberry Pi 5 / CM5**
- Raspberry Pi 4 / 400 / CM4
- Raspberry Pi 3
- Raspberry Pi 2
- Raspberry Pi 1 / Zero / Zero 2
- Anbernic RG353M (console portatile) 
- Odroid Go Advance (console portatile)
- Odroid Go Super (console portatile)
- Computer a 32 o 64 bit
- Odroid XU4

Per scoprire Recalbox per la prima volta, il **Raspberry Pi 5** è sicuramente la scelta preferita dallo Staff!

## II - Installare Recalbox

### 1 - Scaricare / Flashare l'immagine

Recarsi alla pagina [https://www.recalbox.com/it/download/stable/](https://www.recalbox.com/it/download/stable/) e segui le istruzioni per scaricare e flashare l'immagine di Recalbox sul vostro supporto di memoria (Scheda SD, scheda eMMC oppure un Hard Disk/SSD)

>Solamente l'**ultima versione** di Recalbox è disponibile.
>Le **versioni precedenti** non possono essere **scaricate** nè **supportate dal team di sviluppo.**
{.is-warning}

>È preferibile usare un supporto di memoria di 8 GB o più per il sistema (raccomandiamo la serie **Sandisk Ultra** nel caso delle schede SD).
>
>* Per l'**installazione su Raspberry Pi** :
>   * Come supporto di memoria : una **scheda SD**.
>* Per l'**installazione su Odroid** :
>    * Come supporto di memoria : una **scheda SD o eMMC**. 
>* Per l'**installazione su Computer x86 e x86_64** :
>    * Come supporto di memoria : un **disco rigido (HDD)**.

### 2 - Installazione

#### Raspberry Pi / Odroid Go Advance / Odroid Go Super

* Inserisci la scheda microSD nello slot del microcomputer dove vuoi usare Recalbox.
* Accendi / collega il microcomputer e l'installazione di Recalbox si avvia automaticamente.
* L'installazione richiede alcuni minuti e varia a seconda dell'hardware.

#### Case Retroflag GPI

L'installazione sul Case GPI è altrettanto semplice: inserisci la scheda micro SD, collega il tuo GPI con un alimentatore e Recalbox rileverà e configurerà automaticamente il case. Per maggiori informazioni, vai alla pagina [Case RetroFlag GPi](./preparation-and-installation/retroflag-gpi-case)

#### Odroid XU4 / Odroid XU4Q

* Inserisci la tua scheda microSD o eMMC nel dispositivo con cui desideri utilizzare Recalbox.
* Accendi l'Odroid e dovresti vedere Recalbox avviarsi.

#### Computer x86 / x86_64

L'installazione su una chiave USB (tutto l'hardware incluso) è consigliata solo a scopo di test. Ma non essendo perenne, questo potrebbe causare bug non presenti sull'installazione su hard disk.

Ti consigliamo quindi di proseguire all'**installazione su hard disk:**

* Uno per il ** Sistema **.
* Uno per **Archiviazione** di rom, bios, salvataggi, ecc...
* Accendi il tuo PC e dovresti vedere Recalbox avviarsi.

## III - Hardware e accessori necessari

Controlla se disponi del dispositivo di archiviazione e dell'alimentazione necessari per il dispositivo scelto.

| Categoria| Raspberry Pi 0/0w/1/2/3 | Raspberry Pi 4 | Odroid XU4 | Odroid Go | PC |
| :---: | :---: | :---: | :---: | :---: | :---: |
| Alimentazione | micro USB 2.5A (di buona qualità) | USB-C 3A 5V | Utilizzare l'alimentatore ufficiale Odroid | Batteria interna | Alimentazione standard PC |
| Video | Cavo HDMI, HDMI2VGA o HDMI2DVI | Cavo micro HDMI-HDMI | Cavo HDMI | Interno | HDMI/VGA (DVI e DP in alcuni casi) |
| Controller/Joystick | USB o Bluetooth | USB o Bluetooth | USB o Bluetooth | USB o Bluetooth | USB o Bluetooth |
| Archiviazione Sistema | Scheda microSD 16GB+ classe 10, Pi1: SD | Scheda microSD 16GB+ | Scheda microSD 16GB+ | Scheda microSD 16GB | Hard Disk 16GB+ |
| Archiviazione configurazione, bios e roms | Hard Disk esterno con alimentazione dedicata | Hard Disk esterno con alimentazione dedicata | Hard Disk esterno con alimentazione dedicata | Scheda microSD | Hard Disk Interno o Esterno USB (Collegato alla scheda madre) |

>Se hai bisogno di componenti per creare il tuo Recalbox, Non esitare a visitare [il nostro negozio](https://www.kubii.fr/221-recalbox) presso Kubii!
{.is-info}

## Risoluzione dei Problemi per PC x86 / x86_64 :

* **Il tuo computer si avvia solo su Windows:** Verifica che il **Secure Boot** sia disattivato.

  * Cerca su Internet dove si trova l'opzione Secure Boot (è differente per ogni costruttore e modello di computer/scheda madre).
  * Modifica il Secure Boot su "Disabilita" o "Off".
  * Spegni e riavvia il tuo Computer.

* **Recalbox non si avvia:** Configura il tuo **bios** in **Legacy.**
  * Per configurare il vostro bios in Legacy, avvia il tuo computer, premi il tasto per accedere al bios `F*` (da `F1` a `F12`, varia in base al costruttore) oppure `Canc/Del`
  * Cerca su Internet dove si trova l'opzione Legacy (è differente per ogni costruttore e modello di computer/scheda madre).
  * Assicurati che l'opzione Legacy sia **attivata**.
* **In caso si abbia un PC con Multiboot**, per favore imposta il **Recalbox boot** davanti a tutti gli altri sistemi operativi:
  * Accedi al **Menu Boot** degli hard disk premendo il tasto `F*` (da `F1` a `F12`, dipende dal modello e marca di computer/scheda madre).
  * Seleziona l'**hard disk dove è installato Recalbox**.