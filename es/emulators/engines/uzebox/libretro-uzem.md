---
title: Libretro uzem
description: 
published: true
date: 2024-07-14T19:22:09.919Z
tags: libretro, uzebox, uzem
editor: markdown
dateCreated: 2023-09-11T06:38:35.533Z
---

**Libretro Uzem** es un emulador oficial de la Uzebox (una consola de juegos retro-minimalista de 8 bits de código abierto). El Uzebox es un sistema mínimo basado en un microcontrolador AVR ATmega644.

## ![](/emulators/license.svg) Licencia

El core posee la siguiente licencia [**GPLv3**](https://github.com/Uzebox/uzebox/blob/master/gpl-3.0.txt).

## ![](/emulators/compatibility.png) Compatibilidad

 | RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | 
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | 

## ![](/emulators/features.png) Funcionalidades

| Funcionalidad | ¿Soportada? |
| :---: | :---: |
| Grabaciones instantáneas | ✔ |
| Rebobinado | ✔ |

## ![](/emulators/bios.svg) BIOS

>**No necesita bios.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Extensiones de roms soportadas

Las roms deben tener las extensiones siguientes:

* .uze
* .zip
* .7z

Este sistema soporta las roms comprimidas en formato .zip/.7z. No obstante, se trata sólo de un archivo.

Los ficheros contenidos en los .zip/.7z deben tener las extensiones citadas previamente.
Además, cada fichero .zip/.7z sólo debe contener un fichero rom al interior.

### Emplazamiento

Disponed vuestras roms como en la siguiente imagen: 

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 uzebox
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.zip**

## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

>**Este core no tiene opciones.**
{.is-success}

## ![](/emulators/external-links.png) Enlaces externos

* **Código fuente utilizado** : [https://github.com/libretro/libretro-uzem/](https://github.com/libretro/libretro-uzem)
* **Código fuente oficial** : [https://github.com/Uzebox/uzebox/](https://github.com/Uzebox/uzebox)
* **Página oficial** : [http://belogic.com/uzebox/index.asp](http://belogic.com/uzebox/index.asp)
* **Foro oficial** : [http://uzebox.org/forums/](http://uzebox.org/forums/)
* **Wiki oficial** : [http://uzebox.org/wiki/index.php?title=Hello_World](http://uzebox.org/wiki/index.php?title=Hello_World)