---
title: Enable analog mode
description: 
published: true
date: 2025-03-22T01:07:08.250Z
tags: analog, ps1, mode
editor: markdown
dateCreated: 2021-08-08T14:00:54.439Z
---

## Introduction

On some games, you will need to have an analog PlayStation controller.

## Usage

* Go to the RetroArch menu by pressing `Hotkey + B`.
* Go to `Controls` > `Port 1 Controls`.

![](/tutorials/games/guides/playstation-1/enable-analog/psxanalog1-en.png){.full-width}

* On the `Device Type` option, scroll down until you have the text `analog` displayed.

![](/tutorials/games/guides/playstation-1/enable-analog/psxanalog2-en.png){.full-width}

* Press `Back` twice and go to `Resume`.

Enjoy the game!