---
title: Libretro MAME 2010​
description: 
published: true
date: 2024-07-24T07:02:05.056Z
tags: libretro, mame, mame2010
editor: markdown
dateCreated: 2021-07-05T23:10:43.110Z
---



## ![](/emulators/license.svg) License

This core is under **MAME non-commercial** license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ❌ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Features

| Feature | Supported |
| :---: | :---: |
| Restart | ✔ |
| Screenshots | ✔ |
| Core Options | ✔ |

## ![](/emulators/bios.svg) BIOS

BIOS romsets are not required when using "Full Non-Merged" arcade romsets. For "Split" and "Non-Merged" romsets, place the BIOS in the same directory as the game romset.

>**Note**:
>Place the bios in the directory: `/recalbox/share/roms/mame/`   
>or in its subdirectory if you use several Mame cores: `/recalbox/share/roms/mame/Mame2010/`
>Games and their bios must be in the same directory or sub-directory.
{.is-warning}

## ![](/emulators/roms.png) Roms

* Based on romset: MAME 0.139 (August 2010)
* DatFile: You can find the dat file in your recalbox at location `/recalbox/share/arcade/libretro/mame2010.dat`.

### Location

Put your roms like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 mame
┃ ┃ ┃ ┃ ┣ 🗒 **game.zip**
┃ ┃ ┃ ┃ ┣ 📁 **game**
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **game.chd**

You can also opt for a subdirectory (useful if you want to have a mame set for another core).

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 mame
┃ ┃ ┃ ┃ ┣ 📁 Mame 2010
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **game.zip**
┃ ┃ ┃ ┃ ┃ ┣ 📁 **game**
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 **game.chd**

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Accessing to options

You can configure various options in two different ways.

* Via RetroArch menu:

┣ 📁 RetroArch Menu
┃ ┣ 📁 Core options
┃ ┃ ┣ 🧩 Name_option

* Via the file `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core options

| Options | Options values | Variables (for configuration override) | Variables values |
| --- | --- | --- | --- |
| Mouse enabled | `Enabled` ✅ / `Disabled` | `mame_current_mouse_enabled` | `enabled` / `disabled` |
| Video approach 1 Enabled | `Disabled` ✅ / `Enabled` | `mame_current_videoappoach1_enable` | `disabled` / `enabled` |
| Hide nag screen | `Enabled` ✅ / `Disabled` | `mame_current_skip_nagscreen` | `enabled` / `disabled` |
| Hide game infos screen | `Disabled` ✅ / `Enabled` | `mame_current_skip_gameinfos` | `disabled` / `enabled` |
| Hide warning screen | `Disabled` ✅ / `Enabled` | `mame_current_skip_warnings` | `disabled` / `enabled` |
| Core provided aspect ratio | `DAR` ✅ / `PAR` | `mame_current_aspect_ratio` | `DAR` / `PAR` |
| Enable autofire | `Disabled` ✅ / `button 1` / `button 2` / `R2 to button 1 mapping` / `R2 to button 2 mapping` | `mame_current_turbo_button` | `disabled` / `button 1` / `button 2` / `R2 to button 1 mapping` / `R2 to button 2 mapping` |
| Set autofire pulse speed | `medium` ✅ / `slow` / `fast` | `mame_current_turbo_delay` | `medium` / `slow` / `fast` |
| Set frameskip | `0` ✅ / `1` / `2` / `3` / `4` / `automatic` | `mame_current_frame_skip` | `0` / `1` / `2` / `3` / `4` / `automatic` |
| Set sample rate (Restart) | `48000Hz` ✅ / `44100Hz` / `32000Hz` / `22050Hz` | `mame_current_sample_rate` | `48000Hz` / `44100Hz` / `32000Hz` / `22050Hz` |
| Set brightness | `default` ✅ / `+1%` / `+2%` / `+3%` / `+4%` / `+5%` / `+6%` / `+7%` / `+8%` / `+9%` / `+10%` / `+11%` / `+12%` / `+13%` / `+14%` / `+15%` / `+16%` / `+17%` / `+18%` / `+19%` / `+20%` / `-20%` / `-19%` / `-18%` / `-17%` / `-16%` / `-15%` / `-14%` / `-13%` / `-12%` / `-11%` / `-10%` / `-9%` / `-8%` / `-7%` / `-6%` / `-5%` / `-4%` / `-3%` / `-2%` / `-1%` | `mame_current_adj_brightness` | `default` / `+1%` / `+2%` / `+3%` / `+4%` / `+5%` / `+6%` / `+7%` / `+8%` / `+9%` / `+10%` / `+11%` / `+12%` / `+13%` / `+14%` / `+15%` / `+16%` / `+17%` / `+18%` / `+19%` / `+20%` / `-20%` / `-19%` / `-18%` / `-17%` / `-16%` / `-15%` / `-14%` / `-13%` / `-12%` / `-11%` / `-10%` / `-9%` / `-8%` / `-7%` / `-6%` / `-5%` / `-4%` / `-3%` / `-2%` / `-1%` |
| Set contrast | `default` ✅ / `+1%` / `+2%` / `+3%` / `+4%` / `+5%` / `+6%` / `+7%` / `+8%` / `+9%` / `+10%` / `+11%` / `+12%` / `+13%` / `+14%` / `+15%` / `+16%` / `+17%` / `+18%` / `+19%` / `+20%` / `-20%` / `-19%` / `-18%` / `-17%` / `-16%` / `-15%` / `-14%` / `-13%` / `-12%` / `-11%` / `-10%` / `-9%` / `-8%` / `-7%` / `-6%` / `-5%` / `-4%` / `-3%` / `-2%` / `-1%` | `mame_current_adj_contrast` | `default` / `+1%` / `+2%` / `+3%` / `+4%` / `+5%` / `+6%` / `+7%` / `+8%` / `+9%` / `+10%` / `+11%` / `+12%` / `+13%` / `+14%` / `+15%` / `+16%` / `+17%` / `+18%` / `+19%` / `+20%` / `-20%` / `-19%` / `-18%` / `-17%` / `-16%` / `-15%` / `-14%` / `-13%` / `-12%` / `-11%` / `-10%` / `-9%` / `-8%` / `-7%` / `-6%` / `-5%` / `-4%` / `-3%` / `-2%` / `-1%` |
| Set gamme | `default` ✅ / `+1%` / `+2%` / `+3%` / `+4%` / `+5%` / `+6%` / `+7%` / `+8%` / `+9%` / `+10%` / `+11%` / `+12%` / `+13%` / `+14%` / `+15%` / `+16%` / `+17%` / `+18%` / `+19%` / `+20%` / `-20%` / `-19%` / `-18%` / `-17%` / `-16%` / `-15%` / `-14%` / `-13%` / `-12%` / `-11%` / `-10%` / `-9%` / `-8%` / `-7%` / `-6%` / `-5%` / `-4%` / `-3%` / `-2%` / `-1%` | `mame_current_adj_gamma` | `default` / `+1%` / `+2%` / `+3%` / `+4%` / `+5%` / `+6%` / `+7%` / `+8%` / `+9%` / `+10%` / `+11%` / `+12%` / `+13%` / `+14%` / `+15%` / `+16%` / `+17%` / `+18%` / `+19%` / `+20%` / `-20%` / `-19%` / `-18%` / `-17%` / `-16%` / `-15%` / `-14%` / `-13%` / `-12%` / `-11%` / `-10%` / `-9%` / `-8%` / `-7%` / `-6%` / `-5%` / `-4%` / `-3%` / `-2%` / `-1%` |
| Use external hiscore.dat | `Disabled` ✅ / `Enabled` | `mame-external_hiscore` | `disabled` / `enabled` |

## ![](/emulators/external-links.png) External links

* **Progettosnaps**: [https://www.progettosnaps.net/](https://www.progettosnaps.net/)
* **Source code used**: [https://github.com/libretro/mame2010-libretro/](https://github.com/libretro/mame2010-libretro)
* **Libretro documentation**: [https://docs.libretro.com/library/mame\_2010/](https://docs.libretro.com/library/mame_2010/)