---
title: Verificação das BIOS
description: O novo analisador de BIOS do seu Recalbox.
published: true
date: 2021-08-13T00:07:34.570Z
tags: 
editor: markdown
dateCreated: 2021-08-13T00:07:34.570Z
---

## Apresentação

As **BIOS** são um problema para muitos usuários, e nós sabemos disso. Atualmente, a única maneira de verificá-las era com o _verificador_ disponível no WebManager. Prático, mas não é mais suficiente.

Enquanto esperávamos pelo novo WebManager, incluímos um **Verificador de BIOS** no EmulationStation. E vamos dizer com franqueza: _ **é como mudar da noite para o dia!** _

Este novo verificador lhe dirá se as BIOS são obrigatórias, se não são, se as assinaturas MD5 devem ou não corresponder, ele também lhe dirá para que servem algumas BIOS.

E isso não é tudo! Ele também suporta várias assinaturas MD5 diferentes, e o melhor de tudo: _**fará 90% do trabalho para você e lhe dará um relatório de status completo e preciso.**_

![](/basic-usage/features/biosmanager1.png){.full-width}

## Uso

* Na lista do sistema, pressione o botão `START`.
* Vá para `VERIFICAÇÃO DAS BIOS`.

Isto o levará diretamente para uma janela listando todas as BIOS.

## Informações no verificador de BIOS

#### No lado esquerdo

No lado esquerdo, você encontrará as seguintes informações:

* **Sistema**: o sistema com BIOS.
* **Nome da BIOS**: cada BIOS está listada em seu sistema.

### No lado direito

* `SISTEMA`: o sistema associado com a BIOS selecionada.
* `NÚCLEO`: o(s) núcleo(s) para o(s) qual(is) a BIOS é(são) solicitada(s).
* `CAMINHO` : o caminho onde colocar a BIOS, com seu nome exato de arquivo (incluindo a extensão).
* `OBRIGATÓRIO` : a BIOS é obrigatória na utilização do núcleo associado?
* `Compatível com MD5?`: a assinatura MD5 pode ser verificada?
* `Arquivo encontrado?`: A BIOS foi encontrada?
* `MD5 ok?`: A assinatura MD5 está correta?

### Resultado global

Você verá o resultado global por BIOS com a cor do polegar.

* **Verde** : está tudo certo.
* **Amarelo** : a BIOS está presente, mas sua assinatura MD5 pode não ser boa, ou, a BIOS está ausente, mas não é obrigatória.
* **Vermelho**: Faltam as BIOS necessárias.

## Busca de bios

Para facilitar a busca das BIOS que você precisa, na pasta `/recalbox/share/bios/`, você terá um arquivo chamado `missing_bios_report.txt` listando os nomes das BIOS ausentes e suas possíveis assinaturas MD5 válidas.