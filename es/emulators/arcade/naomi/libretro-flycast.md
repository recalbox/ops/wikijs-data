---
title: Libretro Flycast
description: 
published: true
date: 2024-07-29T12:13:30.195Z
tags: libretro, naomi, flycast
editor: markdown
dateCreated: 2023-09-06T06:37:28.376Z
---

**Libretro Flycast** es un emulador **multiplataforma de Sega Dreamcast** capaz de emular los siguientes sistemas:

* **NAOMI M1**
* **NAOMI M2**
* **NAOMI M4**

## ![](/emulators/license.svg) Licencia

Este núcleo está bajo la licencia [**GPLv2**](https://github.com/libretro/flycast/blob/master/LICENSE).

## ![](/emulators/compatibility.png) Compatibilidad

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ❌ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Funcionalidades

| Funcionalidad | Soportado |
| :---: | :---: |
| Reinicio | ✔ |
| Capturas de pantalla | ✔ |
| Guardados | ✔ |
| Opciones del núcleo | ✔ |
| Trucos de RetroArch | ✔ |
| Controles | ✔ |
| Remapeo | ✔ |
| Vibración | ✔ |
| Control del disco | ✔ |

## ![](/emulators/bios.svg) BIOS

### Lista de BIOS obligatorios

| Nombre de archivo | Descripción | MD5 | Proporcionado |
| :---: | :---: | :---: | :---: |
| naomi.zip | Formato MAME a partir del Romset MAME 0.154 | eb4099aeb42ef089cfe94f8fe95e51f6 | ❌ |

### Lista de BIOS opcionales

| Nombre del archivo BIOS | Descripción | MD5 | Proporcionado |
| :---: | :---: | :---: | :---: |
| airlbios.zip | BIOS Naomi para el juego «Airline Pilots Deluxe» de MAME | 3f348c88af99a40fbd11fa435f28c69d | ❌ |
| hod2bios.zip | BIOS Naomi para el juego «The House of the Dead 2» de MAME | 9c755171b222fb1f4e1439d5b709dbf1 | ❌ |

### Ubicación

Coloca los BIOS de la siguiente manera:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 dc
┃ ┃ ┃ ┃ ┣ 🗒 **naomi.zip**
┃ ┃ ┃ ┃ ┣ 🗒 **airlbios.zip**
┃ ┃ ┃ ┃ ┣ 🗒 **hod2bios.zip**

## ![](/emulators/roms.png) Roms

Flycast se basa en el Romset de **Mame** pero también en el formato **nullDC** para su parte Naomi.

### Romset Mame

Solo las roms Naomi **provenientes de un Romset MAME 0.135 o superior** son compatibles.
Te recomendamos el último Romset 0.258 que aportará su lote de compatibilidad adicional.
Para más información sobre la versión del Romset en curso: [MameDev](https://www.mamedev.org/release.html).

>El archivo dat para ordenar tus roms de arcade está disponible en el directorio `/recalbox/share/bios/dc/`
{.is-info}

### Romset NullDC

Estas roms son compatibles con Flycast pero menos fiables que las roms provenientes de un Romset Mame.

>Las Roms **NullDC** están en formato: _`.bin + .lst`_
{.is-info}

Ejemplo para el juego `cfield.zip`:

* `/recalbox/share/roms/naomi/cfield/...`
* `/recalbox/share/roms/naomi/cfield/cfield.bin`
* `/recalbox/share/roms/naomi/cfield/cfield.lst`

### Ubicación

Coloca las roms de la siguiente manera:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 naomi
┃ ┃ ┃ ┃ ┣ 🗒 **juego.zip**

## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

>Para poder conservar tus configuraciones personalizadas durante una actualización, te recomendamos utilizar nuestra funcionalidad [Sobrecargas de configuración](./../../../advanced-usage/configuration-override).
{.is-info}

### Acceder a las opciones

Puedes configurar diversas opciones de dos maneras diferentes.

* A través del Menú RetroArch:

┣ 📁 Menú RetroArch
┃ ┣ 📁 Opciones del núcleo
┃ ┃ ┣ 🧩 Nombre_opción

* A través del archivo `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Opciones del núcleo

| Opciones | Valor de las opciones | Variable (para sobrecarga) | Valores de las variables |
| --- | --- | --- | --- |
| System Type (Restart) | `Auto` ✅ / `Dreamcast` / `NAOMI` / `Atomiswave` | `reicast_system` | `auto` / `dreamcast` / `naomi` / `atomiswave` |
| Internal resolution (restart) | `320x240` ✅ / `640x480` / `800x600` / `960x720` / `1024x768` / `1280x960` / `1440x1080` / `1600x1200` / `1920x1440` / `2560x1920` / `2880x2160` / `3200x2400` / `3840x2880` / `4480x3360` / `5120x3840` / `5760x4320` / `6400x4800` / `7040x5280` / `7680x5760` / `8320x6240` / `8960x6720` / `9600x7200` / `10240x7680` / `10880x8160` / `11520x8640` / `12160x9120` / `12800x9600` | `reicast_internal_resolution` | `320x240` / `640x480` / `800x600` / `960x720` / `1024x768` / `1280x960` / `1440x1080` / `1600x1200` / `1920x1440` / `2560x1920` / `2880x2160` / `3200x2400` / `3840x2880` / `4480x3360` / `5120x3840` / `5760x4320` / `6400x4800` / `7040x5280` / `7680x5760` / `8320x6240` / `8960x6720` / `9600x7200` / `10240x7680` / `10880x8160` / `11520x8640` / `12160x9120` / `12800x9600` |
| Screen Orientation | `Horizontal` ✅ / `Vertical` | `reicast_screen_rotation` | `horizontal` / `vertical` |
| Alpha Sorting | `Per-Strip (fast, least accurate)` / `Per-Triangle (normal)` ✅ | `reicast_alpha_sorting` | `per-strip (fast, least accurate)` / `per-triangle (normal)` |
| Mipmapping | `Enabled` ✅ / `Disabled` | `reicast_mipmapping` | `enabled` / `disabled` |
| Fog Effects | `Enabled` ✅ / `Disabled` | `reicast_fog` | `enabled` / `disabled` |
| Volume Modifier | `Enabled` ✅ / `Disabled` | `reicast_volume_modifier_enable` | `enabled` / `disabled` |
| Widescreen Hack (Restart) | `Disabled` ✅ / `Enabled` | `reicast_widescreen_hack` | `disabled` / `enabled` |
| Widescreen Cheats (Restart) | `Disabled` ✅ / `Enabled` | `reicast_widescreen_cheats` | `disabled` / `enabled` |
| Region | `Default` ✅ / `Japan` / `USA` / `Europe` | `reicast_region` | `Default` / `Japan` / `USA` / `Europe` |
| DIV Matching | `Disabled` / `Auto` ✅ | `reicast_div_matching` | `disabled` / `auto` |
| Analog Stick Deadzone | `0%` / `5%` / `10%` / `15%` ✅ / `20%` / `25%` / `30%` | `reicast_analog_stick_deadzone` | `0%` / `5%` / `10%` / `15%` / `20%` / `25%` / `30%` |
| Trigger Deadzone | `0%` ✅ / `5%` / `10%` / `15%` / `20%` / `25%` / `30%` | `reicast_trigger_deadzone` | `0%` / `5%` / `10%` / `15%` / `20%` / `25%` / `30%` |
| Digital Triggers | `Disabled` ✅ / `Enabled` | `reicast_digital_triggers` | `disabled` / `enabled` |
| Enable DSP | `Disabled`  / `Enabled` ✅ | `reicast_enable_dsp` | `disabled` / `enabled` |
| Anisotropic Filtering | `Off` / `2` / `4` ✅ / `8` / `16` | `reicast_anisotropic_filtering` | `disabled` / `2` / `4` / `8` / `16` |
| PowerVR2 Post-processing Filter | `Disabled` ✅ / `Enabled` | `reicast_pvr2_filtering` | `disabled` / `enabled` |
| Texture Upscaling (xBRZ) | `Off` ✅ / `2` / `4` / `6` | `reicast_texupscale` | `disabled` / `2` / `4` / `6` |
| Enable RTT (Render To Texture) Buffer | `Disabled` ✅ / `Enabled` | `reicast_enable_rttb` | `disabled` / `enabled` |
| Render To Texture Upscaling | `1x` ✅ / `2x` / `3x` / `4x` / `8x` | `reicast_render_to_texture_upscaling` | `1x` / `2x` / `3x` / `4x` / `8x` |
| Threaded Rendering (Restart) | `Disabled`  / `Enabled` ✅ | `reicast_threaded_rendering` | `disabled` / `enabled` |
| Synchronous Rendering | `Disabled`  / `Enabled` ✅ | `reicast_synchronous_rendering` | `disabled` / `enabled` |
| Delay Frame Swapping | `Disabled` ✅ / `Enabled` | `reicast_delay_frame_swapping` | `disabled` / `enabled` |
| Frame Skipping | `Disabled` ✅ / `1` / `2` / `3` / `4` / `5` / `6` | `reicast_frame_skipping` | `off` / `1` / `2` / `3` / `4` / `5` / `6` |
| Allow NAOMI Service Buttons | `Disabled` ✅ / `Enabled` | `reicast_allow_service_buttons` | `disabled` / `enabled` |
| Enable NAOMI 15KHz Dipswitch | `Disabled` ✅ / `Enabled` | `reicast_enable_naomi_15khz_dipswitch` | `disabled` / `enabled` |
| Load Custom Textures | `Disabled` ✅ / `Enabled` | `reicast_custom_textures` | `disabled` / `enabled` |
| Dump Textures | `Disabled` ✅ / `Enabled` | `reicast_dump_textures` | `disabled` / `enabled` |
| Gun Crosshair 1 Display | `Disabled` ✅ / `White` / `Red` / `Green` / `Blue` | `reicast_lightgun1_crosshair` | `disabled` / `White` / `Red` / `Green` / `Blue` |
| Gun Crosshair 2 Display | `Disabled` ✅ / `White` / `Red` / `Green` / `Blue` | `reicast_lightgun2_crosshair` | `disabled` / `White` / `Red` / `Green` / `Blue` |
| Gun Crosshair 3 Display | `Disabled` ✅ / `White` / `Red` / `Green` / `Blue` | `reicast_lightgun3_crosshair` | `disabled` / `White` / `Red` / `Green` / `Blue` |
| Gun Crosshair 4 Display | `Disabled` ✅ / `White` / `Red` / `Green` / `Blue` | `reicast_lightgun4_crosshair` | `disabled` / `White` / `Red` / `Green` / `Blue` |

## ![](/emulators/external-links.png) Enlaces externos

* **Código fuente utilizado** : [https://github.com/libretro/flycast/](https://github.com/libretro/flycast/)
* **Documentación Libretro** : [https://docs.libretro.com/library/flycast/](https://docs.libretro.com/library/flycast/)
* **Código fuente oficial** : [https://github.com/flyinghead/flycast/](https://github.com/flyinghead/flycast)
