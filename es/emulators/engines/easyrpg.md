---
title: EasyRPG
description: 
published: true
date: 2024-07-26T08:23:57.483Z
tags: easyrpg, motor de juego
editor: markdown
dateCreated: 2023-09-04T07:11:53.980Z
---

![](/emulators/fantasy/easyrpg.svg){.align-center}

## Datos técnicos

* **Fabricante :** -
* **Año de lanzamiento :** -

## Presentación

EasyRPG es un proyecto comunitario que pretende crear una herramienta de creación de juegos de rol (RPG) gratuita, multiplataforma y multilingüe, inspirada en un producto comercial llamado RPG Maker.

El proyecto se compone de varias partes:

* [EasyRPG Player](https://easyrpg.org/player/) EasyRPG Player es un programa que permite jugar a juegos creados con RPG Maker 2000 y 2003 Pretende ser un intérprete multiplataforma gratuito de RPG Maker 2000/2003. El objetivo principal es jugar a todos los juegos creados con ellos como lo hace el intérprete original (RPG_RT).
* Reemplazo de RTP](https://easyrpg.org/rtp-replacement/)
* Herramientas](https://easyrpg.org/tools/)
* [Editor EasyRPG](https://easyrpg.org/editor/)

Sus principales objetivos:

* Libre como en libertad: cada componente del proyecto se publicará bajo una licencia libre.
* Multiplataforma: quieren que EasyRPG llegue a tantas plataformas como sea posible.
* Multi-idioma: cuantos más idiomas se soporten, mejor.

**Contribuye a este proyecto**

Este es un proyecto libre y de código abierto (FLOSS). Cualquiera puede ayudar, desde programadores y artistas hasta testers y traductores.

## Emuladores

[Libretro EasyRPG](libretro-easyrpg)