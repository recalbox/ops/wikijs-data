---
title: Sharp X68000
description: 
published: true
date: 2021-10-03T17:00:20.853Z
tags: sharp, x68000, ordinateurs
editor: markdown
dateCreated: 2021-05-21T07:57:33.418Z
---

![](/emulators/computers/sharpx68000.svg){.align-center}

## Fiche technique

* **Fabricant** : Sharp
* **Année de sortie** : 1987
* **Système d'exploitation** : Human68k
* **Processeur** : Motorola 68000 @ 10 MHz
* **RAM** : 1-4 MB (jusqu'à 12 MB)
* **VRAM** : 1056 KB
* **ROM** : 1 MB (128 KB BIOS, 768 KB Character Generator)
* **Processeur graphique** : Sharp-Hudson Chipset modifié
* **Puce sonore** : Yamaha YM2151
* **Résolution** : 512x512 (65536 couleurs)

## Présentation

Le **Sharp X68000**, communément appelé **X68000** ou **X68k**, est un ordinateur personnel de Sharp Corporation commercialisé au Japon à partir de 1987. Le premier modèle utilisait un microprocesseur Motorola 68000 à 10 MHz, avec 1 mégaoctet de mémoire vive et aucun disque dur. Le dernier modèle, lancé en 1993, avait un Motorola 68030 à 25 MHz, avec 4 Mio de mémoire vive et un disque dur SCSI de 80 Mo en option. Les modèles de base comportaient deux lecteurs de disquettes 5"¼.

Cet ordinateur fonctionne avec un système d'exploitation appelé Human68k, développé conjointement par le constructeur même, à savoir Sharp et Hudson Soft. Human68k fonctionne selon le même principe que MS-DOS, qui se base sur des commandes saisies par l'utilisateur. Les fichiers exécutables se terminent par l'extension `.X`. Trois versions majeures de ce système d'exploitation ont été lancées.

À l'instar d'ordinateurs personnels occidentaux comme l'Amiga et l'Atari ST, l'architecture du X68000 le rapprochait des systèmes d'arcade de l'époque. Un certain nombre de jeux d'arcade réputés ont en conséquence été développés sur cet ordinateur comme Bubble Bobble, Columns, Final Fight, Ys, Castlevania, Street Fighter, Ghosts'n Goblins, After Burner ou encore Parodius Da!.

Le système CPS-1 est un dérivé stand-alone du X68000. D'où les versions quasi pixel perfect de Final Fight et Ghouls'n Ghosts. Quasi, car par exemple dans Final Fight, le X68000 ne peut afficher autant de sprites simultanés que la version CPS1.

## Émulateurs

[Libretro PX68k](libretro-px68k)