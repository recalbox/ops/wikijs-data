---
title: Netplay (juegos online)
description: 
published: true
date: 2025-02-28T12:34:55.538Z
tags: netplay, online, juegos
editor: markdown
dateCreated: 2023-08-12T23:17:16.534Z
---

![](/basic-usage/features/netplay/logo-recalbox_netplay.png)

## Introducción

El Netplay es una funcionalidad de RetroArch que permite jugar en red en algunos emuladores usando una conexión punto a punto sin intermediarios, lo que permite reducir al máximo la latencia. Funciona sincronizando los emuladores cuando ambos lanzan el mismo juego. Tambien permite observar una partida gracias a la opción de "espectador". 

El Netplay permite jugar en red en consolas que no disponían de esta capacidad en su época !.


## Recomendaciones

### Conexión

* Es mejor utilizar un cable RJ45 en vez de utilizar la wifi, porque las conexiones wifi son mucho menos estables.
* Hay que abrir un puerto en el router o bien utilizar un router que soporte el protocolo UPNP.
* Si tu Recalbox utiliza una direción IP privada, no hay problema (NAT simple).
* Si tu Recalbox se encuentra detrás de 2 routers entonces el Netplay no funcionará (NAT doble)

### Roms

* **Para las consolas de cartuchos**
	* Utilizad roms de **No-Intro** (las grabacionesson más fideles à los juegos originales)
* **Para las consolas de CD:**
	* Usad roms de **Redump** (las grabaciones son más fideles a los juegos originales)

### Systema

* Si usas un Raspberry Pi0, Pi1, Pi2, Pi3-b and Pi3-b+, se recomienda un **overclock a 1300 MHZ**
* Para Raspberry Pi4, x86_64 (64bit), x86 (32bit) PCs y Odroid, no hace falta overclock.

## Mensajes de error Netplay

### Como cliente: "Failed to Initialize Netplay" ==> "Netplay Initialization Failed"

Estos mensajes indican que la conexión al sistema remoto (host) ha fallado. Verifica que la persona con la que quieres jugar ha lanzado el juego en modo Netplay y está diponible en red. Pide a tu pareja que compruebe que su firewall acepta las conexiones de RetroArch y que vérifique que la redireción de puerto necesaria functiona correctamente.

### Como servidor: "Port Mapping Failed" ==> "Port Mapping Failed" ==> "Port Mapping Failed" ==> "Port Mapping Failed" ==> "Port Mapping Failed

"Port Mapping Failed" indica que hay un problema con la redirección del puerto usando UPnP.

Si UPnP no funciona, debes configurar tu router para redirigir el puerto TCP 55435 a la dirección IP de tu Recalbox. Si aun así no funciona, puedes probar a utilizar una opción de RetroArch que permite establecer la conexión a través de un tercer servidor público de intercambio (relay server), aunque la latencia en este caso sera màs elevada.

Advertencia: por el momento, necesitáis tener una red IPv4 totalmente funcional. Si la red proporcionada por vuestro ISP es de tipo IPV4+CGNAT utilizando únicamente direcciones IPv6, puede ser la cause de este error. Consultad con vuestro proveedor d'internet para utilizar direcciones IPv4.
 
### Content not found, try loading content manually" ==> "Content not found, try loading content manually

* Carga el contenido a la mano, pon el contenido en tu histórico o escanea tu contenido en una playlist.

### Como cliente: "A netplay peer is running different core. Cannot connect." ==> « El otro sistema está ejecutando un core diferente. Implosible de efectuar la conexión »
 
 * Comprobad que ejecutáis el mismo core que el host.
 * Desactivad los modos HD y pantalla extendida. Estos modos fuerzan cores diferentes de los que habéis elegido.

## PREGUNTAS FRECUENTES

### 1 - ¿Por qué no puedo actúar como servidor?

Hay 2 posibilidades:

* Conexión a internet insuficiente.

  * Intenta ser tú el cliente.

* El puerto TCP no se abre automáticamente.

  * Activa UPnP en tu router de internet

  o bien

  * **Redirige el puerto manualmente (Recomendado)**

  o bien, como último recurso

  * Intenta user la funcionalidad de **NETPLAY MITM** (servidores intermedios de Netplay)

### 2 - Si intento servir un juego, el juego no funciona y vuelvo al menu de Recalbox

Verifica que el juego funciona sin netplay. Comprueba que el core que utilizas funciona bien en tu sistema (PC, Raspberry PI o otro). Además, algunas máquinas sólo soportan la opción cliente y no pueden actúar como servidor.

### 3 - ¿El Netplay necesita abrir un puerto para funcionar?

Si, tanto el cliente como el servidor deben abrir puertos para poder comunicar.

Si no quieres abrir puertos, puedes probar la funcionalidad de  **NETPLAY MITM**, pero la latencia sera más grande.

### 4 - ¿Qué necesito para poder jugar correctamente utilizando el Netplay?

* La misma version de Recalbox en el cliente y en el servidor.
* La misma version de Retroarch o Dolphin.
* El mismo core en el caso de que RetroArch proponga varios.
* La misma rom (se debe llamar igual y la firma digital del juego debe coincidir).
* Se recomienda encarecidamente de utilizar una conexión RJ45.

### 5 - ¿El Netplay funciona con PSX / N64 / Playstation / PSP / Dreamcast / DS netplay?

No, los requisitos de rendimiento en estas consolas hacen que sea muy complicado aplicar los conceptos implementados en la funcionalidad de Netplay.

### 6 - ¿Puedo jugar con otras personas a los juegos de GB / GBC / GBA / PSP / DS utilizando el Netplay?

No, el netplay de RetroArch no es como una emulación de conexion de cable (cable emulation).  
No se puede jugar en Netplay ni a la GB, GBC, GBA ni a la PSP. 
Existe una excepción notable si se utiliza el mismo juego GB / GBC (Netplay via TGB-Dual y con cores Sameboy). 
No es posible realizar intercambios de Pokemons y similares.

### 7 - ¿Puedo jugar partidas de 4 jugadores en 2 recalboxes mediante Netplay (2 jugadores por recalbox)?
 
 Sí, pero hay que:
 
 * Arrancar el juego de 4 jugadores correspondiente (por ejemplo, TMNT).
 * Dentro del juego, ejecutad RetroArch con la combinación de teclas Hotkey+B.
 * Id al menú "Red".
 * En el primer Recalbox, configurad las siguientes líneas como sigue: 
 «Solicitar dispositivo 1» --> Sí
 «Solicitar dispositivo 3» --> Sí
 
 * En el segundo Recalbox, configurad las siguientes líneas como sigue: 
 «Solicitar dispositivo 2» --> Sí
 «Solicitar dispositivo 4» --> Sí
 
 * Guardad la configuración 
 
 * Reiniciad el juego en Netplay y ¡à disfrutar!
 
 También es posible jugar à 4 en Netplay de forma local (con un puerto específico por recalbox, por ejemplo 55435 y 55436).
 