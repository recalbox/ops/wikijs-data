---
title: Wii
description: Nintendo Wii
published: true
date: 2024-07-26T08:21:25.898Z
tags: nintendo, wii, consolas
editor: markdown
dateCreated: 2023-09-03T20:33:14.833Z
---

![](/emulators/consoles/wii.svg){.align-center}

## Detalles técnicos

* **Fabricante**: Nintendo
* **Año de lanzamiento**: 2006
* **Cantidad vendida**: 101,63 millones
* **Juego más vendido**: Wii Sports
* **Procesador**: IBM PowerPC "Broadway" a 729 MHz
* **RAM**: 88 MB
* **Procesador gráfico**: ATI Hollywood a 243 MHz
* **Resolución**: 480i/p (PAL/NTSC) o 576i (PAL/SECAM), 4:3 y 16:9
* **Chip de sonido**: Estéreo - posibilidad de Dolby Pro Logic II
* **Soporte** : DVD de 4,7/8,54 GB

## Presentación

La **Wii** es una consola de videojuegos doméstica del fabricante japonés Nintendo, lanzada en 2006. Consola de séptima generación, al igual que la Xbox 360 y la PlayStation 3 con las que compite, la Wii es la consola doméstica más vendida de su generación, con 101,63 millones de unidades vendidas en 2016. Su particularidad es que utiliza un acelerómetro capaz de detectar la posición, orientación y movimientos del mando en el espacio. La Wii es la última consola de Nintendo que no tiene alta definición.

La Wii marcó un antes y un después en la historia de los videojuegos al abrir este pasatiempo a un público más amplio, dirigido a toda la sociedad, lo que explica en parte su éxito.

## Emuladores

[Dolphin](dolphin)
[Libretro Dolphin](libretro-dolphin)