---
title: Sistemas virtuales
description: 
published: true
date: 2024-11-17T15:27:06.464Z
tags: sistemas, virtuales
editor: markdown
dateCreated: 2023-08-25T14:14:00.183Z
---

## Presentación

Una funcionalidad muy esperada por nuestros usuarios. Los sistemas virtuales (como el sistema de Favoritos) permiten agrupar los juegos de diferentes consolas/ordenadores por temáticas variadas:

* Todos los juegos.
* Todos los juengos multi-jugador (2 players o más).
* Los últimos juegos jugados, ordenados automáticamente por fecha.

Sin olvidar el sistema **Arcade**, que reune todos los sitemas de arcade (MAME, FBN, NEO-GEO, ...) y que se puede activar/desactivar desde los menús de EmulationStation. Pero aún hay más.

Si utilizáis el scrapper interno  de Recalbox, _**podréis tener acceso a sistemas virtuales por género**_.
Habéis leído bien:

* Por género.
* Juegos de tipo Shoot'em up
* Juegos de tipo plataforma
* Juegos de combate o lucha
* Juegos de puzles.

Sólo tenéis que activar el sistema virtual de género que os interesa, y todos los juegos se reunirán bajo una misma lista en la interfaz de EmulationStation!

## Configuración de sistemas virtuales

Para su configuración, desde la interfaz, pulsad `START` e id a `CONFIGURACIÓN AVANZADA` > `SISTEMAS VIRTUALES`. Dispondréis de las siguientes opciones:

* Mostrar todos los juegos (On/Off)
* Mostrar partidas multijugador (On/Off)
* Mostrar últimas partidas jugadas (On/Off)
* Mostrar sistema Lightgun (On/Off)
* Mostrar sistema de puertos (On/Off)
* Sistemas virtuales por género - selecciona los géneros que se adapten a tus necesidades.

![](/basic-usage/features/virtualsystem3.png){.full-width}

![](/uso-básico/características/sistema-virtual4.png){.full-width}

## Configuración de los sistemas virtuales arcade

Para los sistemas virtuales arcade, la configuración es diferente.

Para configurarlos, desde la interfaz, pulsad `START` e id a `ARCADE SETTINGS` > `ALL-IN-ONE ARCADE SYSTEM`. Dispondréis de las siguientes opciones:

* Activar el Sistema Arcade Virtual (On/Off)
* Incluir Neo-Geo (On/Off)
* Ocultar los sistemas originales (On/Off)

![](/basic-usage/features/virtualsystem5.png){.full-width}

## Configuración de sistemas virtuales arcade por fabricante

Para los sistemas virtuales arcade por fabricante, la configuración vuelve a ser diferente.

Para configurarlos, desde la interfaz, pulsad `START` e id a `ARCADE SETTINGS` > `BUILDER VIRTUAL SYSTEMS`. Se presentará una lista de constructores que podéis activar o désactivar en función de vuestras necesidades.