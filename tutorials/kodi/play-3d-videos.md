---
title: Play 3D videos
description: 
published: true
date: 2022-12-27T15:13:56.427Z
tags: kodi, videos, 3d
editor: markdown
dateCreated: 2022-12-27T15:13:56.427Z
---

## Introduction

Amongst many other features, Kodi allows you to play 3D videos on 3D compatible televisions since version 13 with the appropriate glasses provided by the manufacturer of your television.

The 3D videos supported are the following:

* Side by side (SBS)
* Top and bottom (TAB)

Anaglyph 3D movies do not have any special settings and can be played back like any other conventional movie.

Each TV brand set has a certain compatibility issue regarding 3D rendering. You can test from the content below with videos from https://www.demolandia.net/3d-video/brands.html.

## Usage

### Rename movies

To be able to use Kodi to view 3D movies, you must first know if it is a 3D movie with side by side (SBS) or top and bottom (TAB) images. Depending on this feature, you will have to rename your movie following this example:

```text
yourmovie.3d.sbs.mp4
```

The `3d.sbs` part is important, it is what will allow you to view your movie correctly in 3D without making mistakes in the settings.

Here is a table with some examples of existing settings:

| 3D | Side by Side | Top / Bottom |
| :---: | :---: | :---: |
| .3D. | .sbs. | .tab. |
| -3d- | .SBS. | .TAB. |
| 3d | .HSBS. | .HTAB. |
| \_3d_ | -hsbs- | -htab- |
| .3D- | \_sbs_ | \_tab_ |
| 3D. | SBS | TAB |
| .3D | sbs | tab |
|  | -SBS | -TAB |
|  | HSBS_ | HTAB_ |

Examples of film names:

* My movie.3D.SBS.mp4
* My movie.3d.sbs.mp4
* My movie.3D-TAB.mp4

### Play movies

Once you have renamed your movie, you will search for it in Kodi in the `Movies` section. Once found, run it and you will be presented with a menu asking you what type of 3D video to apply:

* Preferred mode (Like the movie)
* Monoscopic / 2D

Depending on the renaming of your file and choosing the `Monoscopic / 2D` option, your movie will be visible in 3D with the right pair of glasses correctly (no duplicate image with the glasses).