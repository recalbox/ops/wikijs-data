---
title: Control del ventilador
description: Gestión de los ventiladores en los Raspberry Pi
published: true
date: 2023-08-30T12:42:46.903Z
tags: rpi, fan, 8.0+, ventilador
editor: markdown
dateCreated: 2023-08-30T12:42:46.903Z
---

# Descripción

El programa **recalbox-wpaf** permite controlar algunas placas madres (*hats*) que disponen de un ventilador.

La lista de hats soportados por recalbox-wpaf se encuentra [aquí](./../hardware-compatibility/compatible-devices/rpi-hats).

## Configuración

La configuración se fait dentro del fichero `recalbox.conf` bajo las claves `hat.wpaf.enabled` y `hat.wpaf.board`.

### `hat.wpaf.enabled`

Poned esta clave a `1` para activar el programa recalbox-wpaf.

### `hat.wpaf.board`

Esta clave permite seleccionar el hat de vuestro Raspberry PI. Los valores posibles son:

* `wspoehatb` para el Waveshare PoE hat (b),
* `argonforty` para el hat del Argon One,
* `piboy` para el hat del Experimental PI,
* `rpipoeplus` para el hat Raspberry PI PoE+,
* `fanshim` para el hat Pimoroni fan SHIM.

Ejemplo:

```ini
hat.wpaf.enabled=1
hat.wpaf.board=rpipoeplus
```

No olvidéis reiniciar para que las modificaciones se tomen en cuenta.