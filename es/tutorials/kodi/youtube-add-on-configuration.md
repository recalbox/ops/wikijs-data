---
title: Configurar la extensión YouTube
description: ¡Cómo ver los vídeos de YouTube sin problemas!
published: true
date: 2023-09-15T08:01:12.226Z
tags: kodi, youtube, configuración, extensiones
editor: markdown
dateCreated: 2023-09-15T08:01:12.226Z
---

## Introducción

La extensión de Youtube ya está preinstalada en Kodi. Sin embargo, hay que configurarla para poder identificarse en YouTube y acceder a vuestra cuenta (vídeos favoritos, suscripciones, etc.).

## Procedimiento

### Requisitos previos

- Hay que tener una cuenta de Google / YouTube.
- Tener Recalbox 9.0. Hacer esto en una versión anterior **puede** funcionar pero no está 100% garantizado.

### Configuración

#### Creación de las claves API / OAuth

##### Creación del proyecto

1. Visitad https://console.cloud.google.com/ y pulsad `Nuevo proyecto`. El botón se encuentra en la parte de arriba a la izquierda de la ventana.

>Si ya tenéis otros proyectos creados, pulsado en la lista de proyectos.
>{.is-info}

2. Pulsad `Nuevo proyecto`.

3. Rellenad el nombre del proyecto y pulsad en `Crear`. Podéis darle el nombre que queráis.

![](/tutorials/kodi/youtube/ytb1fr.png){.full-width}

##### Añadir la biblioteca YouTube

4. Una vez creado el proyecto, pulsad en el icono con las 3 líneas horizontales para abrir el menú, luego pulsad `API y servicios` > `Biblioteca`.

![](/tutorials/kodi/youtube/ytb2fr.png){.full-width}

5. En esta página, seleccionad el proyecto que habéis creado antes, buscad `YouTube Data API v3` y pulsad en él.

![](/tutorials/kodi/youtube/ytb3fr.png){.full-width}

6. En la página de la biblioteca, pulsad en `Activar`.

![](/tutorials/kodi/youtube/ytb4fr.png){.full-width}

##### Generación de la clave API

7. Abrid el menú del paso 4 y id a `API y servicios` > `Identificadores`.

![](/tutorials/kodi/youtube/ytb5fr.png){.full-width}

8. En la lista de credenciales de la parte superior de la página, pulsad "Crear credenciales" > "Clave API".

![](/tutorials/kodi/youtube/ytb6fr.png){.full-width}

9. Al cabo de unos segundos aparecerá una ventana emergente que os proporcionará una clave API. Copiad esta clave API para escribirla más tarde en Kodi.

![](/tutorials/kodi/youtube/ytb7fr.png){.full-width}

##### Pantalla de consentimiento OAuth

10. A la izquierda, pulsad en `Pantalla de consentimiento OAuth`.

![](/tutorials/kodi/youtube/ytb8fr.png){.full-width}

11. Especificad el tipo de usuario `Externo` y pulsad en `Crear`.

![](/tutorials/kodi/youtube/ytb9fr.png){.full-width}

12. Poned aquí el `nombre de la aplicación`, la `dirección de correo electrónico de asistencia al usuario` y una `dirección de correo electrónico` en la parte inferior de la página y pulsad en `Guardar y continuar`.

![](/tutorials/kodi/youtube/ytb10fr.png){.full-width}

>Las dos direcciones de correo electrónico pueden ser diferentes.
>{.is-info}

13. Volved a la "Pantalla de consentimiento de autenticación" y cread un usuario de prueba haciendo clic en "Añadir usuarios" e introduciendo una dirección de correo electrónico.

![](/tutorials/kodi/youtube/ytb11fr.png){.full-width}

14. En la misma página, no os olvidéis de pulsar `PUBLISH APPLICATION`.

![](/tutorials/kodi/youtube/ytb12fr.png){.full-width}

##### Generación de la clave OAuth

15. A la izquierda, pulsad `Identificadores` y luego, en la parte superior de la página, pulsad `Crear credenciales` y esta vez seleccionad `Identificador de cliente OAuth`.

![](/tutorials/kodi/youtube/ytb13fr.png){.full-width}

16. Es necesario configurar el tipo de aplicación para obtener el ID de cliente OAuth. Seleccionad `TV and Limited Input Devices` y pulsad en `Create`.

![](/tutorials/kodi/youtube/ytb14fr.png){.full-width}

>El nombre dado a la aplicación en la misma página puede cambiarse o dejarse como está.
>{.is-info}

![](/tutorials/kodi/youtube/ytb15fr.png){.full-width}

17. Esto os dará el "ID de cliente" y el "código secreto de cliente". Copiadlos para meter en Kodi más tarde.

![](/tutorials/kodi/youtube/ytb16fr.png){.full-width}

#### Configuration de YouTube dans Kodi

>Las siguientes etapas pueden ser diferentes si arrancáis Kodi o Youtube la primera vez.
>{.is-info}

1. Lanzad Kodi.

2. Id a `Extensiones` > `YouTube`.

![](/tutorials/kodi/youtube/ytb17fr.png){.full-width}

3. En la lista de la pantalla, seleccionad `Configuración` en la parte inferior.

![](/tutorials/kodi/youtube/ytb18fr.png){.full-width}

4. En los ajustes, seleccionad `API` a la izquierda.

![](/tutorials/kodi/youtube/ytb19fr.png){.full-width}

5. En el medio, modificad cada línea de la siguiente forma:

    1. Clave API : la primera clave que hemos generado antes.

    2. ID del API : el ID cliente que hemos obtenido antes. No hay que escribir la parte `.apps.googleusercontent.com` porque no se utiliza.

    3. API secret : el código secreto del cliente que hemos obtenido en último lugar.

6. Una vez que hayáis proporcionado estos 3 datos, pulsad en `OK` en la ventana de configuración.

7. Una ventana emergente os preguntará si queréis ejecutar el Asistente de arranque. Responded "Sí".

![](/tutorials/kodi/youtube/ytb20fr.png){.full-width}

8. Seleccionad el idioma y la región.

![](/tutorials/kodi/youtube/ytb21fr.png){.full-width}

9. Seleccionad si queréis activar la geolocalización.

![](/tutorials/kodi/youtube/ytb22fr.png){.full-width}

10. Elegid `Sugerencias de vídeos`. 

![](/tutorials/kodi/youtube/ytb23fr.png){.full-width}

#### Identificación

1. Pulsad en `Connexion`. Un popup os pedirá identificaros 2 veces.

![](/tutorials/kodi/youtube/ytb24fr.png){.full-width}

2. Id a https://google.com/device e introducid el primer código que os dará Kodi.

![](/tutorials/kodi/youtube/ytb25fr.png){.full-width}

3. En vuestro navigador web, seleccionad vuestra cuenta YouTube y validad las autorizaciones.

![](/tutorials/kodi/youtube/ytb26fr.png){.full-width}

4. Después de la validación, debéis volver a la misma URL con un segundo código que Kodi os presentará y volver a realizar el mismo procedimiento.

![](/tutorials/kodi/youtube/ytb27fr.png){.full-width}

>Veréis una ventana que os indica que la página es peligrosa. Continuad de todas formas para terminar la identificación.
>{.is-info}

5. Una vez realizadas estas 2 validaciones, ¡ya estáis conectados y podéis disfrutar de YouTube en Kodi sin publicidad!

![](/tutorials/kodi/youtube/ytb28fr.png){.full-width}