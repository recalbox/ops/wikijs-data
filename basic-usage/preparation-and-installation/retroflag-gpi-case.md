---
title: RetroFlag GPi Case
description: 
published: true
date: 2022-02-13T15:49:56.766Z
tags: gpi case, gpi, retroflag
editor: markdown
dateCreated: 2021-06-28T22:10:05.090Z
---

## Preparation

### Required items

* 1 x RetroFlag GPi box
* 1 x Raspberry Pi Zero W
* 1 x SD Card
* 1 x SD card adapter
* 1 x Heat sink (for your Raspberry Pi Zero W card)
* 3 X Batteries

### Recommended batteries

* [Duracell Recharge Ultra Rechargeable Batteries type AA 2500 mAh 5 or 6h autonomy](https://www.amazon.fr/dp/B00E3DVQFS/) 
* [Panasonic BK-3HCDE/4BE eneloop pro 2500mAh Lot of 4 Ni-MH AA/Mignon/LR6 batteries](https://www.amazon.fr/gp/product/B00JWC40JY/)

With an external battery of 10 000maH, it's the party!

## Presentation of the Gpi Case

![](/basic-usage/preparation-and-installation/gpicase1.png){.full-width}

![](/basic-usage/preparation-and-installation/gpicase2.png){.full-width}

![](/basic-usage/preparation-and-installation/gpicase3.png){.full-width}

## Flash and start !

>Do not install the GPi Case on batteries !
>Always use the USB cable for the installation and the first tests.
{.is-warning}

![GPi Case](/basic-usage/preparation-and-installation/gpicase4.png)

Just click and start, it's that easy! Recalbox detects and installs everything needed automatically.

To make your GPi Case work :

* Just flash the latest version of Recalbox for Raspberry Pi1 / Pi0 on your SD card, insert it in the cartridge of your GPI, and turn it on! 
* Wait a few seconds, then you should see this image on your screen:

![Installation image](/basic-usage/preparation-and-installation/gpicase5.png)

* Wait a few more seconds (it may take longer with high capacity SD cards) before EmulationStation starts up with its usual screen:

![EmulationStation starts...](/basic-usage/preparation-and-installation/gpicase6.png)

* Once it's done, enjoy an optimized theme that makes your experience rich, fluid and fun!

## What's different from a normal Recalbox?

When Recalbox detects and installs the GPi Case components, it also changes some settings to get you up and running quickly.  
  
Of course, all the settings are available through the `START` menu and you can go and reconfigure anything you want afterwards.

### Here is a list of the default settings:

* All video modes (videomode) are set to `default` to avoid unnecessary HDMI resolution changes.
* GPI hostname is set to **RECALBOXGPI**.
* Kodi and the X button are disabled
* Virtual controller is disabled
* Updates are disabled
* Netplay is disabled
* Bluetooth is disabled
* XArcade & PS3 drivers are disabled
* The music popup is disabled
* The XBOX360 configuration has been replaced by the GPi Case configuration (yes, the GPI controller is also an XBOX360 controller!)
* Our optimized `recalbox-gpicase` theme is already copied in `/recalbox/share/themes` and activated

### And are also installed:

* dpi-pizero-gpicase.dtbo: GPI screen driver 
* pwm-audio-pizero-gpicase.dtbo : GPI sound driver 
* A special shutdown script for the GPI, originally made by Retroflag and heavily modified so that the GPI shuts down quickly.

## Installing games

![](/basic-usage/preparation-and-installation/gpicase7.png){.full-width}

Now that you're connected to the internet, you can install your games as usual, using the SAMBA share ( `recalboxgpi\share` in a file explorer) or through the WebManager ( `http://recalboxgpi` in a web client)

Have fun with your favorite games!

## What you should know...

### What kind of games can I run on the GPI?

>You can run any game from 8/16 bit consoles and computers to the arcade.  
>Yes, really!
{.is-info}

For the arcade, you have **PiFBA**, a version of FBA optimized for ARM that requires a romset 0.2.96.37.  
**FBNeo** (former FBAlpha), **MAME2000** until **MAME2003plus** also run very well and many games are playable on them.

Some **GBA/SNES** games as well as some Arcade games may suffer from lag or slowdowns. In this case, you can try disabling the `REWIND` option in the advanced emulator options. However, even with optimized options, some games will not run at full speed.
 
>It is not recommended to install computer games, they often need a keyboard and/or mouse to start.
{.is-info}

### Can I overclock my Pi0?

Unfortunately, the Raspberry Pi0 is not easy to overclock. Also, the GPi cartridge doesn't contain any ventilation holes to let the hot air out (at least on the GPi v1 case used at the time this page was written).

You can still play with the O/C options, at your own risk!

>If your GPI is not stable after selecting an O/C option to the point where you can't change the options anymore, insert the SD card in your PC and remove the O/C lines from the `recalbox-oc-config.txt` file. This file is located in the `RECALBOX` partition.
{.is-danger}

You can add a small copper radiator as shown in the picture below. Its thickness should not exceed 2mm.

![](/basic-usage/preparation-and-installation/gpicase8.jpg)

### How long does the battery last?

The first tests show that you will have about 2 hours of play on a charge.

Don't forget that your GPi Case has a USB plug. In a car or at home, use the USB cable.

>If the back of the cartridge starts to get very hot, we recommend that you turn off the GPi Case and let it cool down for a while.
{.is-info}

### How can I scrape my games?

* **Internal scraper:** You can always use the internal scraper, it is configured to scrape large files in order to display them in a normal Recalbox. Using it over WIFI is slow and inefficient. 
* **External scraper:** **We strongly recommend using an external scraper**. It will be better and faster.

>You can also set the max image size to 170x170 to save space and increase the speed of EmulationStation with the **external scraper.**
{.is-info}

## Troubleshooting

* **If the display goes black or jumps when touching the cartridge or putting the console down:**
  * Check all connections.
  * Raise the rpi in the cartridge, to bring it closer to the printed circuit: play with the 4 pins that support the Pi 0, unscrewing them slightly until the Pi 0 is perfectly parallel with the cartridge. Depending on the case, unscrewing the 2 gold pins at the top may be enough.

>This bug occurs when there is a problem with the machining of the cartridge, the connection between the Pi 0 and the cartridge is not done correctly.
{.is-info}

* **Solve the stability problems:**

  * **1. The Micro-SD**

  Check your microSD.  
  Some models have problems with the Pi, not to mention the poor quality Chinese µSD.  
  Test with another SD, preferably from another brand.  

  * **2. Batteries**

  Do not install the GPi Case on batteries !   
  Always use the USB cord for installation and first tests.  
  If it works properly with the cord and not with the batteries, either they are discharged or they do not provide enough juice, despite the GPi regulator.  
  Most rechargeables are 1.2V, not 1.5V.  

  * **3. The model of Pi0.**

  It seems that some models have stability problems, at least with the original clocks, configured with the Pi firmware.  
  Open the `recalbox-oc-config.txt` file in the `RECALBOX` partition, and add the following lines to the end of the file:

  ```ini
  arm_freq=1000
  gpu_freq=500
  core_freq=500
  sdram_freq=500
  sdram_schmoo=0x02000020
  over_voltage=6
  sdram_over_voltage=2
  ```

  This will force the clocks to be configured with a slight overclocking of the GPU as a bonus.  

  * **4. Mounting**

  Although mounting the Pi is relatively easy, you may find that your mounting results in imperfect contacts. Remove the cartridge, clean the contacts of the Pi and the GPi Case with an alcohol-based cleaning solution, reassemble the Pi.
  Also clean the cartridge contacts in the GPi Case.
  Make sure the cartridge is properly seated. The ignition button should not force, either on or off. If it does, your cartridge is not properly inserted or something is in the way.

  * **5. Finally...**

  If despite everything, your Pi doesn't work, or if it remains unstable, try with another one to determine which of the Pi or the GPi Case is at fault. Lend one to yourself, or at the very least, given the price, buy one.
   If the GPi Case is at fault, you can try an exchange or a trade-in. In case of problems with the seller, give him all the procedure you followed in order to prove your good faith regarding the incrimination of the GPi Case.

* If you are in one of the following situations:

  * At the first startup, there is nothing on the screen even after waiting a while.
  * You cannot configure the WIFI. Contact us on the Discord server or on the Forum and give us the following files, available in the `RECALBOX` partition of your SD card:

    * `config.txt`
    * `recalbox-backup.conf`
    * `hardware.log`

>For other problems, write us on :   
>[Forum](https://forum.recalbox.com/) or [Gitlab issues](https://gitlab.com/recalbox/recalbox/issues)
{.is-info}