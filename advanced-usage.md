---
title: 2. 🔨 ADVANCED USAGE
description: 
published: true
date: 2025-03-22T01:30:40.859Z
tags: usage, advanced
editor: markdown
dateCreated: 2021-05-28T22:20:16.016Z
---


This part of the documentation will show you in more detail some useful functions for a small number of people.

Pages in this category:

[Configuration override](configuration-override)
[Arcade on Recalbox](arcade-in-recalbox)
[Fan controller](fan-controller)
[Manual and/or offline update](manual-update)
[Pad To Keyboard](pad-to-keyboard)
[RetroArch](retroarch)
[Scripts on EmulationStation events](scripts-on-emulationstation-events)
[Systems display](systems-display)