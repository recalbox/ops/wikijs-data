---
title: Amiga CDTV
description: 
published: true
date: 2025-01-05T23:45:46.229Z
tags: amiga-cdtv, consoles, amiga, commodore, cdtv
editor: markdown
dateCreated: 2021-07-27T17:42:44.633Z
---

![](/emulators/consoles/amigacdtv.svg){.align-center}

## Technical data

* **Manufacturer**: Commodore International
* **Year of release**: 1991
* **OS**: CDTV / AmigaOS 1.3
* **CPU**: Motorola 68000 @ 7.16 MHz (NTSC) or 7.09 (PAL)
* **RAM**: 1 MB (upgradable)
* **ROM**: 256 KB Kickstart 2.5 and 256 KB CDTV firmware
* **GPU**: Commodore's Enhanced Chip Set
* **Sound chip**: Four 8-bit PCM channels (two stereo channels)
* **Resolution**: 320 x 200 (32 colors) / 640 x 240 (16 colors)

## Presentation

The CDTV, for Commodore Dynamic Total Vision, commonly called Amiga CDTV, is a third generation personal computer and game console developed by Commodore and released in 1991. Taking the form of a black turntable, the system is based on an Amiga 500 computer with a Motorola 68000 processor at 7.16 MHz and an ECS fat Agnus graphics chipset using 1 MB of RAM and a 512 KB ROM with Kickstart 1.3. It integrates a 1x caddy CD-ROM drive, support used for the software. Its launch price was US$1,000 with a gamepad and two software. It was first presented at the Consumer Electronics Show in Las Vegas in 1991.

Optional extras include a keyboard, mouse, remote control, floppy drive, Commodore CDTV SCSI module card with a 60MB hard disk.

In Commodore's mind, this game console was aimed at a family audience that was new to computers, interested in games but also in multimedia capabilities and interactivity. The Grolier encyclopedia, for example, was one of its flagship products. The fact that the console could be transformed into an Amiga 500 computer was also one of the interesting features of the product. This console was not a huge success and was mainly intended for the American and German markets. A prototype of the CDTV-II, named CDTV CR (Cost Reduced), was built but abandoned. It should have integrated more memory and a floppy drive. Commodore later launched the Amiga CD32 based on the same technology as the Amiga 1200, which could be called the successor to the CDTV.

## Emulators

[Libretro PUAE](libretro-puae)
[Amiberry](amiberry)
[Libretro UAE4ARM](libretro-uae4arm)