---
title: No se puede acceder a la partición « share » desde Windows
description: La partición share no aparece bajo Windows
published: true
date: 2024-04-18T12:56:37.725Z
tags: share, roms, windows, tarjeta sd, visibilidad, partición
editor: markdown
dateCreated: 2023-09-01T14:46:33.004Z
---

A veces ocurre que, por una razón que ignoramos, Windows no muestra una de las 2 particiones que Windows puede leer. Sin embargo, existe una forma sencilla de obligar a Windows a mostrárnosla.

## Prerequisitos

Hay que:

- Haber arrancado al menos una vez Recalbox par terminar su instalación
- Tener enchufado a vuestro PC el soporte de almacenamiento al que queréis acceder.
- Disponer de una instalación de Windows funcional.

## Procedimiento

La mayoría de las veces, la partición que no se muestra es la que contiene los juegos. A veces ocurre lo contrario pero es más raro.

- Desde el escritorio de Windows 10+, haz click derecho en el botón "Arrancar" y selecciona `Gestión de discos`.

![](/tutorials/system/access/share1.png){.full-width}

- Se abre una ventana. En la parte de abajo, buscad la partición que no tiene letra de unidad asignada (`RECALBOX` o `SHARE`) y haced click derecho encima de ella para elegir la opción `Asignar una letra de unidad al lector`.

![](/tutorials/system/access/share2.png){.full-width}

## Windows XP y 7

En caso de utilizar una versión de Windows anterior a Windows 10, podéis acceder a la partición share si instaláis la actualización número `KB955704` (Microsoft no la proporciona en su página web, así que la deberéis encontrar por vuestra cuenta en internet).