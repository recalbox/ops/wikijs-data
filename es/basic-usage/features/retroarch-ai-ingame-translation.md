---
title: Retroarch AI (Traducciones des juegos instantáneas)
description: Traducid vuestros juegos en directo
published: true
date: 2024-11-11T05:34:59.617Z
tags: retroarch, juegos, traducción
editor: markdown
dateCreated: 2023-08-23T22:35:37.794Z
---

## Retroarch AI, ¿qué es?

Bienvenidos al futuro!

Desde hace algún tiempo ya, RetroArch permite utilizar un servicio online de traducción de tipo "[**OCR**](./../../basic-usage/glossary)**"** (Optique Character Recognition) y de síntesis vocal que permite traducir los juegos en lengua extranjera casi instantáneamente!

>Esta funcionalidad necesita una conexión a internet y como mínimo un joystick analógico izquierdo!!!
{.is-info}

![Avantg](/basic-usage/features/ocr1.png)

![Après](/basic-usage/features/ocr2.png)

## Configuración

Recalbox ha intentado que la configuración de este servicio sea lo más sencilla posible. Podéis utilizar la clave que se proporciona por defecto o si lo preferís, también podéis utilizar una cuenta propia.

### {.tabset}
#### Cuenta por defecto

La cuenta por efecto es muy sencilla de utilizar:

 * Abrid el fichero `recalbox.conf` situado en `/recalbox/share/system/`
 * Buscad la línea: `;global.translate.apikey=RECALBOX`
 * Borrad el `;` al principio de la línea
 * Aprovechad para indicar al sistema el idioma de la traducción modificando el parámetro `global.translate.to`. También podéis dejarlo en su valor por defecto `auto`, en cuyo caso RetroArch intentará adivinar el idioma de destino.
 
#### Vuestra propia cuenta

El primer paso consiste en abrise una cuenta en [Ztranslate.net](https://ztranslate.net/), un servicio de traducción accesible vía un API, aunque también se pueden utilizar otros servicios similares.

![](/basic-usage/features/ztranslate_signup.png)

* Una vez terminada la inscripción, visitad la página de parámetros para recuperar vuestra `API KEY`.

![](/basic-usage/features/ztranslate_settings.png)

* Abrid vuestro fichero `recalbox.conf` que se encuentra en `/recalbox/share/system/`
* Buscad la línea `;global.translate.apikey=RECALBOX`
* Suprimir el `;` al principio de la línea y escribid vuestra `API KEY` en lugar de `RECALBOX`.
* Modificad el parámetro `global.translate.to` para indicar en qué idioma queréis obtener la traducción. Si lo déjais en `auto`, RetroArch intentará descubrir el lenguage deseado.

![](/basic-usage/features/recalbox.conf_fr.png)

## Vamos!

Disfrutad lanzando por ejemplo un juego 100% en japonés! (salvo si usted es japones o lo entiende ya ;))

* Para activar la traducción, nada más sencillo: con vuestro mando, apretad los botonoes `HOTKEY` + `JOYSTICK-GAUCHE HACIA ABAJO`. El juego se pausa y mostrará una imagen con el texto traducido.
* Realizad de nuevo la misma combinación para continuar el juego.

Por defecto Recalbox utiliza el modo imagen en vez de la síntesis vocal. Para utilizar ésta última, basta con aplicar una simple [sobrecarga de configuración](./../../advanced-usage/configuration-override/retroarch-overrides).