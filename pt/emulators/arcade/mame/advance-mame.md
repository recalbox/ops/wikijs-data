---
title: ​AdvanceMAME
description: 
published: true
date: 2024-06-01T10:39:06.637Z
tags: mame, advancemame
editor: markdown
dateCreated: 2021-09-26T13:50:34.985Z
---

**AdvanceMAME** é um emulador autônomo (*standalone*), originalmente adicionado para a configuração de telas CRT. Baseia-se no Romset MAME 0.106.

## ![](/emulators/license.svg) Licença

Este núcleo está sob licença [**GPLv2**](https://github.com/amadvance/advancemame/blob/master/COPYING).

## ![](/emulators/compatibility.png) Compatibilidade

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ❌ |

## ![](/emulators/features.png) Recursos

| Recurso | Suportado |
| :---: | :---: |
| Saves |  |
| SaveStates |  |
| Rebobinar |  |
| Netplay |  |
| RetroAchievements |  |
| Trapaças do RetroArch |  |
| Trapaças Nativas |  |
| Controles |  |
| Multi-Mouse |  |
| Vibração |  |
| Sensores |  |
| Câmera |  |
| Localização |  |
| Subsistema |  |

## ![](/emulators/bios.svg) BIOS

Os Romsets BIOS não são necessários quando se utilizam romsets de arcade "Full Non-Merged" (Completamente Não-Mesclado). Para romsets "Split"(divididos) e "Non-Merged"(Não-mesclado), coloque a BIOS na mesma pasta que o romset do jogo.

>**Observação*** :  
>
>Coloque as BIOS na pasta: `/recalbox/share/roms/mame/`  
>ou nas subpastas, se você utilizar vários núcleos Mame: `/recalbox/share/roms/mame/Advance Mame/`
{.is-warning}

## ![](/emulators/roms.png) Roms

* Baseado no romset: MAME 0.106 (maio de 2006)
* Tamanho: 50gb
* Romsets emulados: 6166 (incluindo clones, etc...)
* Conjuntos ativos: 6166
* Pais: 1387
* Clones: 2822
* BIOS: 26
* CHDs: 86
* Versão Chds : 3
* Amostras: 64 (3 amostras ausentes)
* Arquivo dat: [aqui](https://www.progettosnaps.net/download/?tipo=dat_mame&file=/dats/MAME/packs/MAME_Dats_106.7z)

#### Local

Coloque as roms assim:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 mame
┃ ┃ ┃ ┃ ┣ 🗒 **jogo.zip**

Você também pode optar por uma subpasta (útil se você quiser ter um set mame para outro núcleo)

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 mame
┃ ┃ ┃ ┃ ┣ 📁 Advance Mame
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **jogo.zip**

## ![](/emulators/advanced-configuration.png) Configuração avançada do emulador

> Para mais informações sobre ROMs, veja [este tutorial](./../../../tutorials/games/generalities/isos-and-roms)!
{.is-info}

## ![external-links.png](/emulators/external-links.png) Links externos

* **Progettosnaps** : [https://www.progettosnaps.net/](https://www.progettosnaps.net/)
* **Código fonte utilizado** : [https://github.com/amadvance/advancemame/](https://github.com/amadvance/advancemame)
* **Site oficial** : [http://www.advancemame.it/](http://www.advancemame.it/)