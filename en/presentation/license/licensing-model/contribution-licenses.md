---
title: Contribution licenses
description: 
published: true
date: 2021-08-08T18:37:52.573Z
tags: contribution, license
editor: markdown
dateCreated: 2021-08-06T07:49:09.934Z
---

For software development projects and creative works, the usual "inbound=outbound" licensing scheme applies, unless otherwise specified.

As explained clearly in GitHub's [open-source guides](https://opensource.guide/legal/), this means that an open source license implicitly serves as an inbound (from contributors) and outbound (to other contributors and users) license.

This means that by contributing to one of our projects, you accept the terms of its license and submit your contribution under the same terms.