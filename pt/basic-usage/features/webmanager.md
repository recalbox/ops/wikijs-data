---
title: Gerenciador Web
description: 
published: true
date: 2021-07-27T23:04:49.242Z
tags: 
editor: markdown
dateCreated: 2021-07-27T22:59:58.912Z
---

## Para conectar

Basta digitar o nome em seu navegador web:

## {.tabset}

### Windows

[http://recalbox/](http://recalbox/)

Se isso não funcionar, você pode usar [o endereço IP do seu Recalbox](./../../tutorials/network/ip/discover-recalbox-ip).

### Linux e macOS

[http://recalbox.local/](http://recalbox.local/)

Se isso não funcionar, você pode usar [o endereço IP do seu Recalbox](./../../tutorials/network/ip/discover-recalbox-ip).

## Interface

Quando você está na página principal, você pode :

* Editar o arquivo `recalbox.conf`.
* Adicionar/remover ROMs.
* Consultar os logs.
* Monitorar sua placa.
* Iniciar o controle virtual (útil para configurar seu Recalbox pela primeira vez em 5 minutos) - via smartphone ou tablet apenas.
* Ver os arquivos de log do Recalbox.

## Desabilitando o Gerenciador Web

Por padrão, o gerenciador web inicia automaticamente. Se você quiser, você pode desativá-lo.

* No EmulationStation, pressione o botão `START`.
* Vá para `CONFIGURAÇÔES AVANÇADAS`.
* Defina a opção `GERENCIADOR RECALBOX` para OFF.

## Controle Virtual

Para acessá-lo diretamente, basta digitar o endereço IP do seu Recalbox com a porta 8080.

```text
http://192.168.0.X:8080
```

```text
http://recalbox:8080
```

Veja o Guia do Usuário do Controle Virtual [aqui](./../../basic-usage/first-use-and-configuration#7-manettes-virtuelles)

Se você encontrar um problema ou bug em potencial, por favor informe [aqui](https://github.com/recalbox/recalbox-manager).