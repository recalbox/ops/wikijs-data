---
title: Libretro Sameduck
description: 
published: true
date: 2024-06-23T11:59:46.665Z
tags: 8.1+, megaduck
editor: markdown
dateCreated: 2022-07-22T09:54:25.702Z
---



## ![](/emulators/license.svg) Licence

Ce core est sous licence [**MIT**](https://github.com/LIJI32/SameBoy/blob/master/LICENSE).

## ![](/emulators/compatibility.png) Compatibilité

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Fonctionnalités

| Fonctionnalité | Supporté |
| :---: | :---: |
| Redémarrage  | ✔ |
| Captures d'écran | ✔ |
| Sauvegardes | ✔ |
| Sauvegardes instantanées | ✔ |
| Rembobinage | ✔ |
| Options du core | ✔ |
| Contrôles | ✔ |
| Remapping | ✔ |
| Vibration | ✔ |
| Sous-systèmes | ✔ |

## ![](/emulators/bios.svg) BIOS

### Liste des bios optionnels

>**Aucun bios n'est requis.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Extensions supportées

La rom doit avoir l'extension :

* .bin

### Emplacement

Placez les roms comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 megaduck
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.bin**

>Les roms au format **No-Intro** sont vivement conseillées.
{.is-success}

>Pour plus d'information sur les roms, rendez-vous sur [ce tutoriel](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuration avancée de l'émulateur

>Pour pouvoir conserver vos configurations personnalisées lors d'une mise à jour, nous vous conseillons d'utiliser notre fonctionnalité [Surcharges de configuration](./../../../advanced-usage/configuration-override).
{.is-info}

### Accéder aux options

Vous pouvez configurer diverses options de deux façons différentes.

* Via le menu RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via le fichier `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Options du core

Utilisez ce tableau pour le mode 1 joueur :

| Options | Valeurs des options | Variable (pour surcharge) | Valeurs des variables |
| --- | --- | --- | --- |
| Color correction | `Désactivé` / `correct curves` / `emulate hardware` ✅ / `preserve brightness` / `reduce contrast` | `sameduck_color_correction_mode` | `off` / `correct curves` / `emulate hardware` / `preserve brightness` / `reduce contrast` |
| High-pass filter | `Désactivé` / `accurate` ✅ / `remove dc offset` | `sameduck_high_pass_filter_mode` | `off` / `accurate` / `remove dc offset` |
| Enable rumble | `all games` ✅ / `never` | `sameduck_rumble` | `all games` / `never` |

Utilisez ce tableau pour le mode 2 joueurs :

| Options | Valeurs des options | Variable (pour surcharge) | Valeurs des variables |
| --- | --- | --- | --- |
| Link cable emulation | `Activé` ✅ / `Désactivé` | `sameduck_link` | `enabled` / `disabled` |
| Screen layout | `top-down` ✅ / `left-right` | `sameduck_screen_layout` | `top-down` / `left-right` |
| Audio output | `Mega Duck #1` ✅ / `Mega Duck #2` | `sameduck_audio_output` | `Mega Duck #1` / `Mega Duck #2` |
| High-pass filter for Mega Duck #1 | `Désactivé` / `accurate` ✅ / `more dc offset` | `sameduck_high_pass_filter_mode_1` | `off` / `accurate` / `more dc offset` |
| High-pass filter for Mega Duck #2 | `Désactivé` / `accurate` ✅ / `more dc offset` | `sameduck_high_pass_filter_mode_2` | `off` / `accurate` / `more dc offset` |
| Enable rumble for Mega Duck #1 | `all games` ✅ / `never` | `sameduck_rumble_1` | `all games` / `never` |
| Enable rumble for Mega Duck #2 | `all games` ✅ / `never` | `sameduck_rumble_2` | `all games` / `never` |

## ![](/emulators/external-links.png) Liens externes

* **Code source utilisé :** [https://github.com/LIJI32/SameBoy/commits/SameDuck](https://github.com/LIJI32/SameBoy/commits/SameDuck)
* **Documentation Libretro** : [https://docs.libretro.com/library/sameduck/](https://docs.libretro.com/library/sameduck/)