---
title: And for advanced users
description: How to add lightgun games yourself
published: true
date: 2021-08-09T22:25:03.078Z
tags: lightgun, advanced users, 7.2+
editor: markdown
dateCreated: 2021-08-05T18:28:20.990Z
---

## How to go further?

### The basic idea...

The principle is to have a unique configuration file gathering all the necessary information for the "Lightgun" games of all systems & emulators. 

Thus, we can make inheritance, grouping of games but also manage particularities without having a file by game or by system as we can do it already but without being able to group systems for example. This is possible with this unique xml. 

Moreover, in the next versions, we will be able to add online help, messages when loading games, etc...

Another advantage compared to the classic overloads : we can add functions in the Configgen python to fetch/identify if a hardware is present like the Mayflash dolphin bar or to know on which board/pc we are. You can decide to configure between 1 and 3 Lightgun players in this way.

>As a reminder, this is only possible for Libretro for the moment. Nothing would prevent us to do it for other emulators with the xml file we already have.
{.is-info}

### The configuration XML file

In fact, the configuration of the games is in an xml file (for info, it's a ".cfg" in fact).

The file is organized like this in terms of structure _(here it is empty without configurations to show the complete xml structure)_ :

```xml
<?xml version="1.0"?>
<root>
	<common_inputs>
 	</common_inputs>
  <common_options>
  </common_options>
  <system>
 		<platform></platform>
		<emulator>
			<core></core>
		</emulator>
    <inputs>
    </inputs>
    <options>
    </options>
    <games>
      <inputs>
      </inputs>
      <options>
      </options>
      <game>
        <name></name> 
        <inputs>
        </inputs>
        <options>
        </options>
      </game>
    </games>
  </system>
</root>
```

### XML Tags

**&lt;root>**: this is the base of the XML file where everything will be found as well as a version to follow the evolution of this file

```xml
<?xml version="1.0"?>
<root>
	<!-- internal version of the file to help follow-up of update, same version until release -->
	<version>1.0 - Beta8 - 20-10-2020</version>
</root>
```

**<common_inputs>**: this is the part in &lt;root> and common override of `retroarchcustom.cfg` that we'll have for all systems, in the case of the lightgun, we'll put the basic keys like select/exit/start/trigger there.

```xml
	<!--MANDATORY inputs (for retroarchcustom.cfg): comon part to use for all systems (using dolphinbar and until 2 wiimotes) -->
	<common_inputs>
        <string name="input_player1_gun_start" value="enter" />
        <string name="input_player2_gun_start" value="enter" />
        <string name="input_player1_gun_select" value="escape" />
        <string name="input_player2_gun_select" value="escape" />
        <!-- force selecte and start for consoles games -->
        <string name="input_player1_select" value="escape" />
        <string name="input_player2_select" value="escape" />
        <string name="input_player1_start" value="enter" />
        <string name="input_player2_start" value="enter" />
        <!--for many it's necessary to move in menu or else -->
        <string name="input_player1_gun_dpad_up" value="up" />
        <string name="input_player1_gun_dpad_down" value="down" />
        <string name="input_player1_gun_dpad_right" value="right" />
        <string name="input_player1_gun_dpad_left" value="left" />
        <string name="input_player2_gun_dpad_up" value="up" />
        <string name="input_player2_gun_dpad_down" value="down" />
        <string name="input_player2_gun_dpad_right" value="right" />
        <string name="input_player2_gun_dpad_left" value="left" />
        <!-- index to manage distinction between guns -->
        <string name="input_player1_mouse_index" value="GUNP1" />
        <string name="input_player2_mouse_index" value="GUNP2" />
        <!-- wiimotes home button for exit game -->
		    <string name="input_exit_emulator" value="lsuper" />
	  	  <string name="input_enable_hotkey" value="lsuper" />
        <!-- set on fullscreen for don't see cursor mouse on x86(-64) -->
	  	  <string name="video_fullscreen" value="true" />
        <!-- to have help using overlay switch and finally ask to press 2 times home to exit -->
        <string name="input_overlay_next" value="lsuper" />
        <string name="quit_press_twice" value="true" />
 	</common_inputs>
```

**<common_options>**: this is the part in &lt;root> and common override of `retroarch-core-options.cfg` that we will have. In our case, it is not used in a common way but it would be possible.

```xml
    <!--OPTIONAL options (for retroarch-core-options.cfg): comon part to use for all systems (using dolphinbar and until 2 wiimotes) -->
  	<common_options>
        <!-- only for more log on test -->
        <!-- <string name="libretro_log_level" value="2" />
        <string name="log_verbosity" value="true" /> -->
  	</common_options>
```

**&lt;string>:** these are the basic configuration values always composed of 2 attributes _"name" and "value"_ :

```xml
<string name="input_player1_gun_start" value="enter" />
```

**&lt;system>:** this is the part that groups all the configuration of a system. You can find the tags _&lt;platform>, &lt;emulator>, &lt;inputs>, &lt;options>, &lt;games> and &lt;game>_.

```xml
  <!-- ********************************************************* Nintendo Entertainment System ************************************************************** -->
	<system>
    <!-- MANDATORY: name of the platform as in roms directory -->
		<platform>nes</platform>
		<!-- MANDATORY: emulator and core selected for lightgun-->
		<emulator name="libretro">
			<core>fceumm</core>
		</emulator>
    <!--MANDATORY: inputs common to this system -->
		<inputs>
      	<!-- Wiimote button B -->
		  	<string name="input_player2_gun_trigger_mbtn" value="1" />
      	<!-- gamepad -->
      	<string name="input_libretro_device_p1" value="513" />
      	<!-- lightgun -->
      	<string name="input_libretro_device_p2" value="258" />
        <!-- Set index to GUNP1 on player 2 to have first wiimote as player 1 -->
        <string name="input_player2_mouse_index" value="GUNP1" />
		</inputs>
		<!--MANDATORY: options common to this system-->
		<options>
		  	<string name="fceumm_zapper_mode" value="lightgun" />
		  	<string name="fceumm_zapper_tolerance" value="6" />
      	<string name="fceumm_show_crosshair" value="enabled" />
		</options>
        <games>
            <!--list of games using lightgun-->
            <!-- see 'duckhunt' game to see more explainations to know how to setup game part -->
            <game tested="no">
                <name>2in1</name>
            </game>
```

**&lt;platform>**: it is the tag that corresponds to the name of the system but finally, we can have several platforms in the same system as for the systems based on the emulator _flycast_ for example.

```xml
  	<!-- ******************************* Sega Naomi & Sega NaomiGd & Sega Atomiswave ********************************************************* -->
  	<system>
    <!-- MANDATORY: name of the platform as in roms directory -->
        <platform>atomiswave</platform>
        <platform>naomi</platform>
        <platform>naomigd</platform>
    		<!-- MANDATORY: emulator and core selected for lightgun-->
    		<emulator name="libretro">
      			<core>flycast</core>
    		</emulator>
```

>You can have several distinct systems with the same platform to have games using different emulators and/or cores. This is the case of the MAME platform using a "MAME" core (on PC) or "mame2003plus" (on boards).
{.is-warning}

**&lt;emulator>**: this tag has as attribute the name of the emulator to use for the system in question:

```xml
    	<!-- MANDATORY: emulator and core selected for lightgun-->
    	<emulator name="libretro">
      		<core>flycast</core>
    	</emulator>
```

**&lt;core>**: this tag has for value the name of the emulator to use. it is always in _&lt;emulator>_ as above.

```xml
<core>flycast</core>
```

**&lt;inputs>**: this tag allows to overload _retroarchcustom.cfg_ in addition to the _&lt;common_inputs>_ part at the level of _&lt;system>, &lt;games> and/or &lt;game>_

```xml
<system>
.....
		<inputs>
      	<!-- Wiimote button B -->
		  	<string name="input_player2_gun_trigger_mbtn" value="1" />
      	<!-- gamepad -->
      	<string name="input_libretro_device_p1" value="513" />
      	<!-- lightgun -->
      	<string name="input_libretro_device_p2" value="258" />
        <!-- Set index to GUNP1 on player 2 to have first wiimote as player 1 -->
        <string name="input_player2_mouse_index" value="GUNP1" />
.....
```

```xml
.....
        <games>
            <inputs>
                <!-- no reload, this game is a free fire but need secondary input fo grenade and more -->
                <string name="input_player1_a_mbtn" value="2" />
                <string name="input_player2_a_mbtn" value="2" />     
            </inputs>
            <game tested="ok">
                <name>alien3thegun</name>
            </game>
.....
```

```xml
.....            
            <game tested="ok">
                <name>gunbuster</name>
                <inputs>
                    <!-- no reload free fire game but ned secondary input fo grenade and more -->
                    <!-- move players with dpad on wiimote  -->
                    <string name="input_player1_a" value="2" />
                    <string name="input_player1_gun_offscreen_shot_mbtn" value="nul" />
                    <string name="input_player2_a" value="2" />
                    <string name="input_player2_gun_offscreen_shot_mbtn" value="nul" />
                </inputs>
            </game>
.....
```

**&lt;options>**: this tag allows to overload _retroarch-core-options.cfg_ in addition to the &lt;common_options> part at the level of _&lt;system>, &lt;games> and/or &lt;game>._ 

```xml
        <!-- options common to this system-->
        <options>
            <string name="genesis_plus_gx_gun_input" value="lightgun" />
            <string name="genesis_plus_gx_gun_cursor" value="enabled" />
        </options>
```

**&lt;games>**: this tag is the root of the games groups. It allows to have a distinct configuration for a part of the games in the same system, so you can have several _&lt;games>_ sections in the same system.

```xml
        <games>
            <inputs>
                <!-- no reload, these games have a free fire but need secondary input for grenade and more -->
                <string name="input_player1_a_mbtn" value="2" />
                <string name="input_player2_a_mbtn" value="2" />     
            </inputs>
            <game tested="ok">
                <name>alien3thegun</name>
            </game>
            <game tested="ok">
                <name>beastbusters</name>
            </game>
            <game tested="ok">
                <name>dragongun</name>
            </game>
        </games>

```

**&lt;game>**: this tag which is in &lt;games> is to define the configuration of the particular game.

```xml
            <game tested="ok">
                <name>gunbuster</name>
                <inputs>
                    <!-- no reload free fire game but ned secondary input fo grenade and more -->
                    <!-- move players with dpad on wiimote  -->
                    <string name="input_player1_a" value="2" />
                    <string name="input_player1_gun_offscreen_shot_mbtn" value="nul" />
                    <string name="input_player2_a" value="2" />
                    <string name="input_player2_gun_offscreen_shot_mbtn" value="nul" />
                </inputs>
            </game>
```

>**&lt;game>** has the attribute _"tested"_ to know if this particular rom has already been tested and its status (3 possible values):
>
>* **no** -> not tested but will be taken into account using the default system configuration.
>* **ok** -> tested with the defined configuration.
>* **nok** -> tested but not functional, so will not be configured to work in lightgun mode for the moment (we have hope).
{.is-warning}

**&lt;name>** : this tag in _&lt;game>_ is used to specify the name that will be used to identify it to activate the lightgun feature of recalbox at launch.

```xml
.....
<name>duckhunt</name>
.....
<name>mobilsuitgundamfinalshooting</name>
.....
```

>You must always put the name **without spaces/special characters and in lower case** for the parsing of the "gamelist" of a system by Recalbox. Be careful, it is the name of the game in its simplest form, it is neither the name of the file of the rom nor the name with the geographical zone, etc... it is necessary to remain the simplest possible so that the correspondence is done for sure. **Scraping will make sure you have a game with an identifiable name.
{.is-warning}

### Inheritance

In fact in this paragraph we want to explain that in order the configuration will be done like this: 

_**common -> system -> games -> game**_

.... We let you tell us what _input_player2_gun_trigger_mbtn_ will be in the following example

```xml
<?xml version="1.0"?>
<root>
	<common_inputs>
	  <string name="input_player2_gun_trigger_mbtn" value="1" />
 	</common_inputs>
  <common_options>
  </common_options>
  <system>
 		<platform></platform>
		<emulator>
			<core></core>
		</emulator>
    <inputs>
      <string name="input_player2_gun_trigger_mbtn" value="2" />
    </inputs>
    <options>
    </options>
    <games>
      <inputs>
        <string name="input_player2_gun_trigger_mbtn" value="1" />
      </inputs>
      <options>
      </options>
      <game>
        <name></name> 
        <inputs>
          <string name="input_player2_gun_trigger_mbtn" value="2" />
        </inputs>
        <options>
        </options>
      </game>
    </games>
  </system>
</root>
```

### Index management for multiplayer

We had to manage the mouse indexes and specify it in a common way or more precisely per game. For example when player 1 is finally on port 2, and player 2 on port 3. So we have keywords like **"GUNP1", "GUNP2" or "GUNP3".

```xml
.....
     <common_inputs>
        <!-- index to manage distinction between guns -->
        <string name="input_player1_mouse_index" value="GUNP1" />
        <string name="input_player2_mouse_index" value="GUNP2" />
        .....
```

```xml
        <games> <!-- games using justifier -->
            <inputs>
                <!-- lightguns always on port 2 -->
                <!-- For Justifier device (blue cross) -->
                <string name="input_libretro_device_p2" value="516" />
                <string name="input_player2_mouse_index" value="GUNP1" />
                <!-- Wiimote button B or Z -->
                <string name="input_player2_gun_trigger_mbtn" value="1" />
                <!-- For Justifier(P2) device (pink cross) -->
                <string name="input_libretro_device_p3" value="772" />
                <string name="input_player3_mouse_index" value="GUNP2" />
                <!-- Wiimote button B or Z -->
                <string name="input_player3_gun_trigger_mbtn" value="1" />
                <!-- Common inputs part replicated for this game using player 3 -->
                <string name="input_player3_gun_start" value="enter" />
                <string name="input_player3_gun_select" value="escape" />
                <!-- Common inputs part replicated to force select and start for consoles games -->
                <string name="input_player3_select" value="escape" />
                <string name="input_player3_start" value="enter" />
            </inputs>
            <game tested="ok">
                <name>lethalenforcers</name>
            </game>
        </games>
```

## But more simply...

In fact, the configuration xml file is not in your "share" but it is in the system and accessible only by SSH under: `/recalbox/share_init/system/.emulationstation`

and under the name: `lightgun.cfg`.

Here is what is displayed in an SSH console:

```shell
# cd /recalbox/share_init/system/.emulationstation
# ls -l
total 252
-rwxr-xr-x    1 root     root         51359 Oct 22 19:13 es_bios.xml
-rwxr-xr-x    1 root     root          1254 Oct 22 19:13 es_bios.xsd
-rwxr-xr-x    1 root     root         70383 Oct 22 19:13 es_input.cfg
-rwxr-xr-x    1 root     root          2979 Oct 22 19:13 es_settings.cfg
-rw-r--r--    1 root     root         61973 Oct 23 02:31 es_systems.cfg
-rw-r--r--    1 root     root         68513 Oct 22 14:19 lightgun.cfg
drwxr-xr-x    3 root     root            36 Oct 23 02:28 themes

```

>To be able to modify this file, it will be necessary to mount the [system partition](./../../../tutorials/system/access/remount-partition-with-write-access) as read/write.
{.is-warning}

Start of this file when opened with a text editor:

![](/basic-usage/features/lightgun/advanced-users/advancedusers1.png)

### How to add a game/rom ?

you just have to find the system, example for the NES:

![](/basic-usage/features/lightgun/advanced-users/advancedusers2.png)

And to add in the section &lt;games> the tag &lt;game> then &lt;name> like this:

```xml
           <game tested="ok">
                <name>monjeu2</name>
            </game>
```

>REMINDER: You must always put the name **without spaces/special characters and in lower case** for the parsing of the "gamelist" of a system by Recalbox. Be careful, it is the name of the game in its simplest form, it is not the name of the file of the rom or the name with the geographical zone, etc... it is necessary to remain the simplest possible so that the correspondence is done for sure. **Scraping will make sure you have a game with an identifiable name.
{.is-warning}

### To check that it is taken into account

* If the file is badly formatted, you go back to the list of games.
* If the name is not found/recognized, you will not be able to activate the Lightgun mode.
* If you are really in Lightgun mode, you should be able to exit the game with the HOME button of the Wiimote.

### To add configurations to a game

The best way is to look at what has been done for other games... In general, you will need to modify the settings corresponding to the primary and secondary fire buttons, or even disable some keys (see below) but it can be even more complicated... If this is the case, you will have to look at the manuals of the games of the time and spend a lot of time in the RetroArch menus... so master the art of overloading!

```xml
        <game tested="ok">
            <name>ninjaassault</name>
            <inputs>
                <!--for this game start p1 for P2 aux_c -->
                <string name="input_player1_gun_start" value="nul" />
                <string name="input_player1_gun_aux_b" value="enter" />
                <!--for this game trigger -->
                <string name="input_player1_gun_trigger_mbtn" value="nul" />
                <string name="input_player1_gun_aux_a_mbtn" value="1" />
                <!--for this game real offscreen reload -->
                <string name="input_player1_gun_offscreen_shot_mbtn" value="nul" />
            </inputs>
        </game>
```

### To add a new system

In this case, you really need to know the core to use, the RetroArch parameters and in general also configure the "device" in _&lt;inputs>_ as well as the "lightgun" parameters of the core in _&lt;options>_.

```xml
......
<system>
<!-- MANDATORY: name of the platform as in roms directory -->
    <platform>megadrive</platform>
    <!-- MANDATORY: emulator and core selected for lightgun-->
    <emulator name="libretro">
        <core>genesisplusgx</core>
    </emulator>
    <!-- MANDATORY: inputs common to this system-->
    <inputs>
        <!-- Wiimote button B or Z -->
        <string name="input_player2_gun_trigger_mbtn" value="1" />
        <!-- Wiimote button A or c - aux when it's necessary as jump, rocket, etc... -->
        <string name="input_player2_gun_aux_a_mbtn" value="2" />
        <!-- <string name="input_player2_gun_aux_a_mbtn" value="2" /> -->
        <!-- gamepad always on port 1-->
        <string name="input_libretro_device_p1" value="1" />
    </inputs>
    <!--MANDATORY: options common to this system-->
    <options>
        <string name="genesis_plus_gx_gun_input" value="lightgun" />
        <string name="genesis_plus_gx_gun_cursor" value="enabled" />
    </options>
    .....
```

**Good luck!!!**