---
title: Zurücksetzen auf Werkseinstellungen
description: 
published: true
date: 2021-10-02T10:40:21.756Z
tags: 7.2+, werkseinstellungen, zurücksetzen, einstellungen
editor: markdown
dateCreated: 2021-10-02T10:40:21.756Z
---

## Übersicht

Seit Recalbox 7.2 gibt es eine neue Option namens "Auf Werkseinstellungen zurücksetzen", mit der du dein System auf denselben Stand wie bei einer sauberen Neuinstallation zurücksetzen kannst. Mit dieser Option kannst du dein System einfacher in den ursprünglichen Zustand zurückversetzen, anstatt regelmäßig eine Neuinstallation durchführen zu müssen.

>Diese Option **beeinträchtigt** deine BIOS Dateien, Spiele, und deine Spielstände **nicht**!
{.is-success}

## Verwenden der Funktion

Die Funktion befindet sich in EmulationStation. Dafür drückst du einfach `START`, gehst zu `ERWEITERTE EINSTELLUNGEN` und wählst `AUF WERKSEINSTELLUNGEN ZURÜCKSETZEN`.

![](/basic-usage/features/resetfactory1-en.png){.full-width}

Sobald du die Option ausgewählt hast, werden zwei Warnfenster angezeigt.

![](/basic-usage/features/resetfactory2-en.png){.full-width}

![](/basic-usage/features/resetfactory3-en.png){.full-width}

>Sobald diese beiden Fenster bestätigt sind, **gibt es kein Zurück mehr**..
{.is-danger}

Wenn die Wiederherstellung auf die Werkseinstellungen abgeschlossen ist, befindet sich deine Recalbox-Installation in einem sauberen Zustand!