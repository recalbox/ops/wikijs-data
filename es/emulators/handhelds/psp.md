---
title: PSP
description: Sony PSP
published: true
date: 2024-07-26T08:34:07.903Z
tags: psp, sony, portátiles
editor: markdown
dateCreated: 2023-09-04T08:26:58.649Z
---

![](/emuladores/handheld/psp.svg){.align-center}

## Detalles técnicos

* **Fabricante**: Sony Computer Entertainment
* **Año de lanzamiento**: 2004
* **Cantidad vendida**: 80,82 millones
* **Juego más vendido**: GTA Liberty City Stories
* **Procesador**: MIPS RS4000 de 32 bits a 333 Mhz
* **RAM**: 32 Mbits
* **RAM**: 4 Mbits
* **Resolución**: 480x272 píxeles
* **Tamaño**: 170x74x23 mm
* **Peso**: 260 g

## Presentación

La **PlayStation Portable**, o **PSP**, es la primera videoconsola portátil de Sony, lanzada entre 2004 y 2014. Marcó un antes y un después en la historia de las consolas portátiles, ofreciendo una calidad de imagen y unas capacidades generales que se acercaban a las de la PlayStation 2, una consola doméstica.

PlayStation Portable permite jugar a videojuegos, ver vídeos e imágenes, escuchar música y navegar por Internet. Su sistema de conexión inalámbrica Wi-Fi permite que hasta dieciséis máquinas se comuniquen entre sí al mismo tiempo, gracias a la función de "red ad hoc".

## Emuladores

[Libretro PPSSPP](libretro-ppsspp)
[PPSSPP](ppsspp)