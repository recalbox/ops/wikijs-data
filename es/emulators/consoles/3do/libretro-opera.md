---
title: Libretro Opera
description: 
published: true
date: 2024-06-30T15:17:58.205Z
tags: libretro, 3do, opera
editor: markdown
dateCreated: 2023-09-12T11:16:38.828Z
---

**Libretro Opera** es un core Libretro capaz de emular la Panasonic 3DO.

## ![](/emulators/license.svg) Licencia

El core posee la siguiente licencia [**Modified GNU LGPL / Non-commercial**](https://github.com/libretro/opera-libretro/blob/master/libopera/opera_3do.c).

## ![](/emulators/compatibility.png) Compatibilidad

 | RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
 | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
 | ❌ | ✅ | ✅ | ✅ | ✅ | ✅ &nbsp;🐌 | ✅ | ✅ |

🐌 &nbsp;Bajo rendimiento pero jugable

## ![](/emulators/features.png) Funcionalidades

| Funcionalidad | ¿Soportada? |
| :---: | :---: |
| Reinicio | ✔ |
| Capturas de pantalla | ✔ |
| Grabaciones | ✔ |
| Grabaciones instantáneas | ✔ |
| Rebobinado | ✔ |
| Netplay | ✔ |
| Opciones del core | ✔ |
| RetroAchievements | ✔ |
| Controles | ✔ |
| Remapping | ✔ |

## ![](/emulators/bios.svg) BIOS

### Lista de BIOS obligatorias

| Nombre del fichero | Descripción | MD5 | ¿Preinstalada? |
| :---: | :---: | :---: | :---: |
| panafz1.bin | Panasonic FZ-1 | f47264dd47fe30f73ab3c010015c155b | ❌ |
| panafz1j.bin | Panasonic FZ-1J | a496cfdded3da562759be3561317b605 | ❌ |
| panafz1j-norsa.bin | Panasonic FZ-1J \[RSA Patch\] | f6c71de7470d16abe4f71b1444883dc8 | ❌ |

### Emplazamiento

Disponed vuestras bios como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 🗒 **panafz1.bin**
┃ ┃ ┃ ┣ 🗒 **panafz1j.bin**
┃ ┃ ┃ ┣ 🗒 **panafz1j-norsa.bin**

### Lista de BIOS opcionales

| Nombre del fichero | Descripción | MD5 | ¿Preinstalada? |
| :---: | :---: | :---: | :---: |
| panafz10.bin | Panasonic FZ-10 | 51f2f43ae2f3508a14d9f56597e2d3ce | ❌ |
| panafz10-norsa.bin | Panasonic FZ-10 \[RSA Patch\] | 1477bda80dc33731a65468c1f5bcbee9 | ❌ |
| panafz10e-anvil.bin | Panasonic FZ-10-E \[Anvil\] | a48e6746bd7edec0f40cff078f0bb19f | ❌ |
| panafz10e-anvil-norsa.bin | Panasonic FZ-10-E \[Anvil RSA Patch\] | cf11bbb5a16d7af9875cca9de9a15e09 | ❌ |
| goldstar.bin | Goldstar GDO-101M | 8639fd5e549bd6238cfee79e3e749114 | ❌ |
| sanyotry.bin | Sanyo IMP-21J TRY | 35fa1a1ebaaeea286dc5cd15487c13ea | ❌ |
| 3do_arcade_saot.bin | Shootout At Old Tucson | 8970fc987ab89a7f64da9f8a8c4333ff | ❌ |
| panafz1-kanji.bin | Panasonic FZ-1 (J) Kanji ROM. Utile pour lire les caractères japonais. | b8dc97f778a6245c58e064b0312e8281 | ❌ |
| panafz1j-kanji.bin | Panasonic FZ-1 (J) Kanji ROM. Utile pour lire les caractères japonais. | c23fb5d5e6bb1c240d02cf968972be37 | ❌ |
| panafz10ja-anvil-kanji.bin | Panasonic FZ-10 (J) Kanji ROM \[Anvil\]. Utile pour lire les caractères japonais. | 428577250f43edc902ea239c50d2240d | ❌ |

### Emplazamiento

Disponed vuestras bios como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 🗒 **panafz10.bin**
┃ ┃ ┃ ┣ 🗒 **panafz10-norsa.bin**
┃ ┃ ┃ ┣ 🗒 **panafz10e-anvil.bin**
┃ ┃ ┃ ┣ 🗒 **panafz10e-anvil-norsa.bin**
┃ ┃ ┃ ┣ 🗒 **goldstar.bin**
┃ ┃ ┃ ┣ 🗒 **sanyotry.bin**
┃ ┃ ┃ ┣ 🗒 **3do_arcade_saot.bin**
┃ ┃ ┃ ┣ 🗒 **panafz1-kanji.bin**
┃ ┃ ┃ ┣ 🗒 **panafz1j-kanji.bin**
┃ ┃ ┃ ┣ 🗒 **panafz10ja-anvil-kanji.bin**

## ![](/emulators/isos.svg) Isos

### Extensiones de roms soportadas

Las roms deben tener las extensiones siguientes:

* .iso
* .chd
* .bin/.cue

### Emplazamiento

Disponed vuestras roms como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 **3do**
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.iso**

>Se aconsejan las roms del proyecto **Redump**.
{.is-success}

>Para más información sobre las roms, consultad [este tutorial](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

>Para conservar las configuraciones personalizadas cuando se produce una actualización del sistema Recalbox, os aconsejamos utilizar las [Sobrecargas de configuración](./../../../advanced-usage/configuration-override).
{.is-info}

### Acceder a las opciones

Podéis configurar las opciones del core de dos formas diferentes.

* Via el Menú RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via el fichero `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Opciones del core

| Opción | Valor de la opción | Variable (para la sobrecarga) | Valores posibles |
| --- | --- | --- | --- |
| BIOS (rom1) | `Panasonic FZ-1 (U)` / `Panasonic FZ-1 (J)` / `Panasonic FZ-1 (J) [No RSA]` / `Panasonic FZ-10 (U)` / `Panasonic FZ-10 (U) [No RSA]` / `Panasonic FZ-10 (E) ANVIL` / `Panasonic FZ-10 (E) ANVIL [No RSA]` / `Goldstar GDO-101M` / `Sanyo Try IMP-21J` / `3DO Arcade - SAOT` | `opera_bios` | `Panasonic FZ-1 (U)` / `Panasonic FZ-1 (J)` / `Panasonic FZ-1 (J) [No RSA]` / `Panasonic FZ-10 (U)` / `Panasonic FZ-10 (U) [No RSA]` / `Panasonic FZ-10 (E) ANVIL` / `Panasonic FZ-10 (E) ANVIL [No RSA]` / `Goldstar GDO-101M` / `Sanyo Try IMP-21J` / `3DO Arcade - SAOT` |
| Font (rom2) | `Désactivé` / `Panasonic FZ-1 (J) Kanji ROM` / `Panasonic FZ-10 (J) ANVIL Kanji ROM` | `opera_font` | `disabled` / `Panasonic FZ-1 (J) Kanji ROM` / `Panasonic FZ-10 (J) ANVIL Kanji ROM` |
| CPU Overclock | `1.0x (12.50Mhz)` ✅ / `1.1x (13.75Mhz)` / `1.2x (15.00Mhz)` / `1.5x (18.75Mhz)` / `1.6x (20.00Mhz)` / `1.8x (22.50Mhz)` / `2.0x (25.00Mhz)` | `opera_cpu_overclock` | `1.0x (12.50Mhz)` / `1.1x (13.75Mhz)` / `1.2x (15.00Mhz)` / `1.5x (18.75Mhz)` / `1.6x (20.00Mhz)` / `1.8x (22.50Mhz)` / `2.0x (25.00Mhz)` |
| Mode | `NTSC 320x240@60` ✅ / `PAL1 320x288@50` / `PAL2 352x288@50` | `opera_region` | `ntsc` / `pal1` / `pal2` |
| VDLP Pixel Format | `0RGB1555` / `RGB565` / `XRGB8888` ✅ | `opera_vdlp_pixel_format` | `0RGB1555` / `RGB565` / `XRGB8888` |
| VDLP Bypass CLUT | `Désactivé` ✅ / `Activé` | `opera_vdlp_bypass_clut` | `disabled` / `enabled` |
| HiRes CEL Rendering | `Désactivé` ✅ / `Activé` | `opera_high_resolution` | `disabled` / `enabled` |
| MADAM Matrix Engine | `Hardware` ✅ / `Software` | `opera_madam_matrix_engine` | `hardware` / `software` |
| OperaOS SWI HLE | `Désactivé` ✅ / `Activé` | `opera_swi_hle` | `disabled` / `enabled` |
| Threaded DSP | `Désactivé` ✅ / `Activé` | `opera_dsp_threaded` | `disabled` / `enabled` |
| NVRAM Storage | `Per Game` ✅ / `Shared` | `opera_nvram_storage` | `per game` / `shared` |
| NVRAM Version | `0` ✅ / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` | `opera_nvram_version` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` |
| Active Input Devices | `0` / `1` ✅ / `2` / `3` / `4` / `5` / `6` / `7` / `8` | `opera_active_devices` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` |
 | Hide Lightgun Crosshairs | `Désactivé` ✅ / `Activé` | `opera_hide_lightgun_crosshairs` | `disabled` / `enabled` |
| Timing Hack 1 (Crash'n Burn) | `Désactivé` ✅ / `Activé` | `opera_hack_timing_1` | `disabled` / `enabled` |
| Timing Hack 3 (Dinopark Tycoon) | `Désactivé` ✅ / `Activé` | `opera_hack_timing_3` | `disabled` / `enabled` |
| Timing Hack 5 (Microcosm) | `Désactivé` ✅ / `Activé` | `opera_hack_timing_5` | `disabled` / `enabled` |
| Timing Hack 6 (Alone in the Dark) | `Désactivé` ✅ / `Activé` | `opera_hack_timing_6` | `disabled` / `enabled` |
| Graphics Step Y Hack (Samurai Showdown) | `Désactivé` ✅ / `Activé` | `opera_hack_graphics_step_y` | `disabled` / `enabled` |
| Debug Output | `Désactivé` ✅ / `Activé` | `opera_kprint` | `disabled` / `enabled` |

## ![](/emulators/external-links.png) Enlaces externos

* **Código fuente utilizado** : [https://github.com/libretro/opera-libretro/](https://github.com/libretro/opera-libretro)
* **Documención Libretro** : [https://docs.libretro.com/library/opera/](https://docs.libretro.com/library/opera/)
* **Lista de compatibilidades** : [http://wiki.fourdo.com/Compatibility_List](http://wiki.fourdo.com/Compatibility_List)