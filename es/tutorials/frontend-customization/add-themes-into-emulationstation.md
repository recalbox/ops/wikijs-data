---
title: Añadir nuevos temas a la interfaz
description: 
published: true
date: 2024-07-29T10:22:41.419Z
tags: emulationstation, temas
editor: markdown
dateCreated: 2023-09-15T09:02:58.902Z
---

¿Ganas de un nuevo tema para tu interfaz de Recalbox? ¡Estás en el lugar correcto!

## ¿Dónde encontrar un tema?
Todos los temas conocidos disponibles se presentan en esta página.

La mayoría de los enlaces te dirigirán al [foro](https://forum.recalbox.com/category/14/themes-interface) donde puedes descargar los temas creados por la comunidad.
No dudes en intercambiar con sus autores y dejar un comentario en los posts respectivos.

Otro gran agradecimiento a ellos por su trabajo y su compartición.

---

## Añadir un tema a Recalbox

Asegúrate simplemente de usar un tema compatible con tu versión de Recalbox (es decir, un tema actualizado con una versión actualizada).

---

Puedes añadir manualmente el tema de tu elección siguiendo estos pasos:

* Descomprime el archivo descargado
* Copia la carpeta que contiene el tema que deseas usar en tu interfaz de dos maneras diferentes:
  * O bien desde la red: `\\recalbox\share\themes\`
  * O bien en la tarjeta SD o en tu almacenamiento externo en su caso: `SHARE/themes/`

> Atención, la totalidad de las carpetas de tu tema debe estar en una única carpeta y no en la raíz de la carpeta `themes` **sin espacios**.
Esto da una estructura de directorios que se parece a la siguiente:
📁 recalbox
┣ 📁 share
┃ ┣ 📁 themes
┃ ┃ ┣ 📁 Hyperwall-720p
{.is-warning}

* Ahora puedes cambiar el tema en las `Opciones de la interfaz > Tema` sin reiniciar.
* Algunos temas disponen de opciones de personalización. Las encontrarás en las `Opciones de la interfaz > Configuración del tema`.

---

> Un tutorial en video está disponible en YouTube a través de este enlace: [Cambia el tema de Recalbox](https://youtu.be/U185lY2tKTg).
{.is-info}

## Lista de temas y compatibilidad
| |HDMI PI/PC/OXu4|RGB Dual/JAMMA|Anbernic RG353|GPICase|OGoA|OGoS|16/9|4/3|TATE|
|-|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
|  Artbook CRT	|⚠️|✅|✅|❌|:grey_question:|:grey_question:|❌|✅|✅|
|  ARTFLIX NX		|✅|⚠️|⚠️|❌|:grey_question:|:grey_question:|✅|:grey_question:|❌|
|  Authentica		|✅|⚠️|⚠️|❌|:grey_question:|:grey_question:|✅|:grey_question:|❌|
|  Blue Box 	  |✅|⚠️|⚠️|❌|:grey_question:|:grey_question:|✅|:grey_question:|❌|
|  Bresson 	    |✅|⚠️|⚠️|❌|:grey_question:|:grey_question:|✅|:grey_question:|❌|
|  ColorLines 	|✅|⚠️|⚠️|❌|:grey_question:|:grey_question:|✅|:grey_question:|❌|
|  DarkMachines |✅|⚠️|⚠️|❌|:grey_question:|:grey_question:|✅|:grey_question:|❌|
|  Dark Slide   |✅|⚠️|⚠️|❌|:grey_question:|:grey_question:|✅|:grey_question:|❌|
|  Dashboard X  |✅|✅|✅|❌|:grey_question:|:grey_question:|✅|✅|✅|
|  Dreambox		  |✅|⚠️|⚠️|❌|:grey_question:|:grey_question:|✅|:grey_question:|❌|
|  Epic Noir	  |✅|⚠️|⚠️|❌|:grey_question:|:grey_question:|✅|:grey_question:|❌|
|  FFArts			  |✅|⚠️|⚠️|❌|:grey_question:|:grey_question:|✅|:grey_question:|❌|
|  GameBoy		  |⚠️|✅|✅|❌|:grey_question:|:grey_question:|❌|✅|❌|
|  Hyperwall	  |✅|⚠️|⚠️|❌|:grey_question:|:grey_question:|✅|:grey_question:|❌|
|  Metal Slide  |✅|⚠️|⚠️|❌|:grey_question:|:grey_question:|✅|:grey_question:|❌|
|  Midnight			|✅|✅|✅|❌|:grey_question:|:grey_question:|✅|✅|✅|
|  Mister				|⚠️|✅|✅|✅|:grey_question:|:grey_question:|❌|✅|✅|
|  Muskashi			|✅|❌|❌|❌|❌|❌|✅|❌|❌|
|	 Next Pixel 	|✅|❌|❌|❌|:grey_question:|:grey_question:|✅|✅|❌|
|	 Retro Switch |✅|⚠️|⚠️|❌|:grey_question:|:grey_question:|✅|:grey_question:|❌|
|	 Unifiedspin	|✅|⚠️|⚠️|❌|:grey_question:|:grey_question:|✅|:grey_question:|❌|
|	 Uniflyered		|✅|⚠️|⚠️|❌|:grey_question:|:grey_question:|✅|:grey_question:|❌|

Leyenda:

✅ Optimizado: funciona perfectamente y ha sido optimizado para la plataforma en cuestión
⚠️ Compatible: funciona pero no está optimizado, algunos elementos no están dispuestos como deberían o los tamaños de fuente no son óptimos, pero estos problemas son menores y no impiden el buen funcionamiento del tema
❌ No compatible: no funciona o la disposición del tema no está adaptada en absoluto (elementos o textos que se superponen, textos ilegibles, imágenes deformadas...)
:grey_question: En espera de prueba: Sí, lleva tiempo, no dudes en participar [aquí](https://forum.recalbox.com/topic/24510)

## Los temas

### Artbook CRT
Autor: **Beudbeud**

- Tema optimizado para RGB Dual y RGB JAMMA
- Resolución 4/3 Yoko y Tate - 240p/480p
- Soporte de todos los sistemas y sistemas virtuales para recalbox 9.2

|HDMI PI/PC/OXu4|RGB Dual/JAMMA|Anbernic RG353|GPICase|OGoA|OGoS|16/9|4/3|TATE|
|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
|⚠️|✅|✅|❌|:grey_question:|:grey_question:|❌|✅|✅|

![9.2_artbookcrt_1.jpg](/tutorials/frontend-customization/add-themes-into-emulationstation/9.2_artbookcrt_1.jpg)
![9.2_artbookcrt_2.jpg](/tutorials/frontend-customization/add-themes-into-emulationstation/9.2_artbookcrt_2.jpg)

> Presentación y descarga: **[Foro recalbox](https://forum.recalbox.com/topic/33153/recalbox-9-theme-artbook-crt-240p)** o **[github](https://github.com/beudbeud/recalbox-artbookcrt)**
{.is-info}

### ARTFLIX NX
Autor: **Fagnerpc**

- ⚠️ Falta algunos sistemas pero es utilizable
- ⚠️ Falta el conjunto de sistemas virtuales arcade
- ⚠️ No toma en cuenta la opción de las banderas en las gamelist
- Resolución disponible: 540p - 720p
- Opciones: elección de la resolución

|HDMI PI/PC/OXu4|RGB Dual/JAMMA|Anbernic RG353|GPICase|OGoA|OGoS|16/9|4/3|TATE|
|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
|✅|⚠️|⚠️|❌|:grey_question:|:grey_question:|✅|:grey_question:|❌|

![8.0.1_artflix_nx_-_1.jpg](/tutorials/frontend-customization/add-themes-into-emulationstation/8.0.1_artflix_nx_-_1.jpg)
![8.0.1_artflix_nx_-_2.jpg](/tutorials/frontend-customization/add-themes-into-emulationstation/8.0.1_artflix_nx_-_2.jpg)
> Presentación y descarga: [Foro recalbox](https://forum.recalbox.com/topic/25918)
{.is-info}

### Authentica
Autor: **Butch Games**

- ⚠️ Faltan algunos visuales/fondos pero es utilizable
- Opciones: colores, vistas de sistemas, vistas de gamelist, clips, regiones

|HDMI PI/PC/OXu4|RGB Dual/JAMMA|Anbernic RG353|GPICase|OGoA|OGoS|16/9|4/3|TATE|
|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
|✅|⚠️|⚠️|❌|:grey_question:|:grey_question:|✅|:grey_question:|❌|

![8.0.1_authentica_1.jpg](/tutorials/frontend-customization/add-themes-into-emulationstation/8.0.1_authentica_1.jpg)
![8.0.1_authentica_2.jpg](/tutorials/frontend-customization/add-themes-into-emulationstation/8.0.1_authentica_2.jpg)
> Presentación y descarga: [Foro recalbox](https://forum.recalbox.com/topic/25888)
{.is-info}

### Blue Box
Autor: **Butch Games**

- ⚠️ Falta algunos sistemas pero es utilizable
- ⚠️ Falta el conjunto de sistemas virtuales arcade
- Opciones: colores, iconos, menús, vistas de sistemas, vistas de gamelist, clips, regiones

|HDMI PI/PC/OXu4|RGB Dual/JAMMA|Anbernic RG353|GPICase|OGoA|OGoS|16/9|4/3|TATE|
|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
|✅|⚠️|⚠️|❌|:grey_question:|:grey_question:|✅|:grey_question:|❌|

![8.1.1_blue_box_1.jpg](/tutorials/frontend-customization/add-themes-into-emulationstation/8.1.1_blue_box_1.jpg)
![8.1.1_blue_box_2.jpg](/tutorials/frontend-customization/add-themes-into-emulationstation/8.1.1_blue_box_2.jpg)
> Presentación y descarga: [Foro recalbox](https://forum.recalbox.com/topic/26947)
{.is-info}

### Bresom
Autor: **lemerle**

- ⚠️ Falta algunos sistemas pero es utilizable
- ⚠️ Falta el conjunto de sistemas virtuales arcade
- Opciones: elección de la resolución

|HDMI PI/PC/OXu4|RGB Dual/JAMMA|Anbernic RG353|GPICase|OGoA|OGoS|16/9|4/3|TATE|
|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
|✅|⚠️|⚠️|❌|:grey_question:|:grey_question:|✅|:grey_question:|❌|

![8.1.1_bresom_1.jpg](/tutorials/frontend-customization/add-themes-into-emulationstation/8.1.1_bresom_1.jpg)
![8.1.1_bresom_2.jpg](/tutorials/frontend-customization/add-themes-into-emulationstation/8.1.1_bresom_2.jpg)
> Presentación y descarga: [Foro recalbox](https://forum.recalbox.com/topic/29334)
{.is-info}

### ColorLines
Autor: **Butch Games**

- ⚠️ Falta algunos sistemas pero es utilizable
- ⚠️ Falta el conjunto de sistemas virtuales arcade
- ⚠️ Bug de visualización en la lista de juegos
- Opciones: colores, iconos, playlist, vistas de gamelist, clips, regiones

|HDMI PI/PC/OXu4|RGB Dual/JAMMA|Anbernic RG353|GPICase|OGoA|OGoS|16/9|4/3|TATE|
|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
|✅|⚠️|⚠️|❌|:grey_question:|:grey_question:|✅|:grey_question:|❌|

![8.0.1_colorlines_1.jpg](/tutorials/frontend-customization/add-themes-into-emulationstation/8.0.1_colorlines_1.jpg)
![8.0.1_colorlines_2.jpg](/tutorials/frontend-customization/add-themes-into-emulationstation/8.0.1_colorlines_2.jpg)
> Presentación y descarga: [Foro recalbox](https://forum.recalbox.com/topic/25363)
{.is-info}

### DARK MACHINES
Autor: **Jorge Rosa**

- ⚠️ Falta algunos sistemas pero es utilizable
- ⚠️ Falta el conjunto de sistemas virtuales arcade

|HDMI PI/PC/OXu4|RGB Dual/JAMMA|Anbernic RG353|GPICase|OGoA|OGoS|16/9|4/3|TATE|
|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
|✅|⚠️|⚠️|❌|:grey_question:|:grey_question:|✅|:grey_question:|❌|

![9.1_dark_machines_1.jpg](/tutorials/frontend-customization/add-themes-into-emulationstation/9.1_dark_machines_1.jpg)
![9.1_dark_machines_2.jpg](/tutorials/frontend-customization/add-themes-into-emulationstation/9.1_dark_machines_2.jpg)
> Presentación y descarga: [Foro recalbox](https://forum.recalbox.com/topic/30269)
{.is-info}

### Dark Slide
Autor: **Butch Games**
- Todos los sistemas y sistemas virtuales por género
- ⚠️ 33/51 sistemas virtuales arcade
- Opciones: colores, iconos, vistas de sistemas, vistas de gamelist, clips, regiones

|HDMI PI/PC/OXu4|RGB Dual/JAMMA|Anbernic RG353|GPICase|OGoA|OGoS|16/9|4/3|TATE|
|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
|✅|⚠️|⚠️|❌|:grey_question:|:grey_question:|✅|:grey_question:|❌|

![9.1_dark_slide_1.jpg](/tutorials/frontend-customization/add-themes-into-emulationstation/9.1_dark_slide_1.jpg)
![9.1_dark_slide_2.jpg](/tutorials/frontend-customization/add-themes-into-emulationstation/9.1_dark_slide_2.jpg)
> Presentación y descarga: [Foro recalbox](https://forum.recalbox.com/topic/30607)
{.is-info}

### Dashboard X
Autor: **Serviettzky**

- Tema compatible y optimizado tanto para versión HDMI, RGB Dual/JAMMA
- Resolución 16/9 + 4/3 - Yoko y Tate - HD/240p/480p
- Soporte de todos los sistemas y sistemas virtuales para recalbox 9.2
- ⚠️ Falta el conjunto de sistemas virtuales por género

|HDMI PI/PC/OXu4|RGB Dual/JAMMA|Anbernic RG353|GPICase|OGoA|OGoS|16/9|4/3|TATE|
|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
|✅|✅|✅|❌|:grey_question:|:grey_question:|✅|✅|✅|

![9.2_dashboard_x_1.jpg](/tutorials/frontend-customization/add-themes-into-emulationstation/9.2_dashboard_x_1.jpg)
![9.2_dashboard_x_2.jpg](/tutorials/frontend-customization/add-themes-into-emulationstation/9.2_dashboard_x_2.jpg)

> Presentación y descarga: **[Foro recalbox](https://forum.recalbox.com/topic/34488/recalbox-9-2-th%C3%A8me-dashboard_x)** o **[github](https://github.com/serviettzky/dashboard-X)**
{.is-info}

### Dreambox
Autor: **Butch Games**

- ⚠️ Falta algunos sistemas pero es utilizable
- ⚠️ Falta el conjunto de sistemas virtuales arcade
- ⚠️ Bug de visualización en la lista de juegos
- Opciones: iconos, vistas de sistemas, vistas de gamelist, clips, regiones

|HDMI PI/PC/OXu4|RGB Dual/JAMMA|Anbernic RG353|GPICase|OGoA|OGoS|16/9|4/3|TATE|
|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
|✅|⚠️|⚠️|❌|:grey_question:|:grey_question:|✅|:grey_question:|❌|

![8.0.1_dreambox_1.jpg](/tutorials/frontend-customization/add-themes-into-emulationstation/8.0.1_dreambox_1.jpg)
![8.0.1_dreambox_2.jpg](/tutorials/frontend-customization/add-themes-into-emulationstation/8.0.1_dreambox_2.jpg)
> Presentación y descarga: [Foro recalbox](https://forum.recalbox.com/topic/23272)
{.is-info}

### Epic Noir
Autor: **Chicuelo adaptado por Malixx**

- ⚠️ Falta algunos sistemas pero es utilizable
- ⚠️ Falta el conjunto de sistemas virtuales arcade
- Opciones: iconos, resoluciones

|HDMI PI/PC/OXu4|RGB Dual/JAMMA|Anbernic RG353|GPICase|OGoA|OGoS|16/9|4/3|TATE|
|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
|✅|⚠️|⚠️|❌|:grey_question:|:grey_question:|✅|:grey_question:|❌|

![9.1_epic_noir_1.jpg](/tutorials/frontend-customization/add-themes-into-emulationstation/9.1_epic_noir_1.jpg)
![9.1_epic_noir_2.jpg](/tutorials/frontend-customization/add-themes-into-emulationstation/9.1_epic_noir_2.jpg)
> Presentación y descarga: [Foro recalbox](https://forum.recalbox.com/topic/30440)
{.is-info}

### FFArts
Autor: **fpowergamesretro**

- ⚠️ Falta algunos sistemas pero es utilizable
- ⚠️ Falta el conjunto de sistemas virtuales arcade
- ⚠️ Bug de visualización en la lista de juegos
- Opciones: colores, iconos, menús, vistas de sistemas, vistas de gamelist, regiones

|HDMI PI/PC/OXu4|RGB Dual/JAMMA|Anbernic RG353|GPICase|OGoA|OGoS|16/9|4/3|TATE|
|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
|✅|⚠️|⚠️|❌|:grey_question:|:grey_question:|✅|:grey_question:|❌|

![8.1.1_ffart_1.jpg](/tutorials/frontend-customization/add-themes-into-emulationstation/8.1.1_ffart_1.jpg)
![8.1.1_ffart_2.jpg](/tutorials/frontend-customization/add-themes-into-emulationstation/8.1.1_ffart_2.jpg)
> Presentación y descarga: [Foro recalbox](https://forum.recalbox.com/topic/29668)
{.is-info}

### GAMEBOY
Autor: **Beudbeud**

- Tema optimizado para pantalla 4/3 pero funciona muy bien en 16/9
- Resolución 4/3 Yoko - 240p/480p
- Soporte de todos los sistemas y sistemas virtuales para recalbox 9.2

|HDMI PI/PC/OXu4|RGB Dual/JAMMA|Anbernic RG353|GPICase|OGoA|OGoS|16/9|4/3|TATE|
|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
|⚠️|✅|✅|❌|:grey_question:|:grey_question:|❌|✅|❌|

![9.2_gameboy_1.jpg](/tutorials/frontend-customization/add-themes-into-emulationstation/9.2_gameboy_1.jpg)
![9.2_gameboy_2.jpg](/tutorials/frontend-customization/add-themes-into-emulationstation/9.2_gameboy_2.jpg)

> Presentación y descarga: **[Foro recalbox](https://forum.recalbox.com/topic/34372/recalbox-9-2-theme-gameboy-240p-480p/)** o **[github](https://github.com/beudbeud/recalbox-gameboy)**
{.is-info}

### Hyperwall
Autor: **Butch Games actualizado por Aldébaran**

- ⚠️ Falta algunos sistemas pero es utilizable
- ⚠️ Falta el conjunto de sistemas virtuales arcade
- ⚠️ Bug de visualización en la lista de juegos
- Opciones: iconos, vistas de sistemas, vistas de gamelist, regiones

|HDMI PI/PC/OXu4|RGB Dual/JAMMA|Anbernic RG353|GPICase|OGoA|OGoS|16/9|4/3|TATE|
|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
|✅|⚠️|⚠️|❌|:grey_question:|:grey_question:|✅|:grey_question:|❌|

![8.0.1_hyperwall_1.jpg](/tutorials/frontend-customization/add-themes-into-emulationstation/8.0.1_hyperwall_1.jpg)
![8.0.1_hyperwall_2.jpg](/tutorials/frontend-customization/add-themes-into-emulationstation/8.0.1_hyperwall_2.jpg)
> Presentación y descarga: [Foro recalbox](https://forum.recalbox.com/topic/24597)
{.is-info}

### Metal Slide de Butch Games
Autor: **Butch Games**

- ⚠️ Falta algunos sistemas pero es utilizable
- ⚠️ Falta el conjunto de sistemas virtuales arcade
- Opciones: colores, iconos, menús, playlist, vistas de gamelist, clips, regiones

|HDMI PI/PC/OXu4|RGB Dual/JAMMA|Anbernic RG353|GPICase|OGoA|OGoS|16/9|4/3|TATE|
|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
|✅|⚠️|⚠️|❌|:grey_question:|:grey_question:|✅|:grey_question:|❌|

![8.0.1_metal_slide_1.jpg](/tutorials/frontend-customization/add-themes-into-emulationstation/8.0.1_metal_slide_1.jpg)
![8.0.1_metal_slide_20.jpg](/tutorials/frontend-customization/add-themes-into-emulationstation/8.0.1_metal_slide_20.jpg)
> Presentación y descarga: [Foro recalbox](https://forum.recalbox.com/topic/26314)
{.is-info}

### Midnight
Autor: **BOUNITOS**

- Tema compatible y optimizado tanto para versión HDMI, RGB Dual/JAMMA
- Resolución 16/9 + 4/3 - Yoko y Tate - HD/240p/480p
- Soporte de todos los sistemas y sistemas virtuales para recalbox 9.2
- Opción: Shaders CRT, Iconos, Gameclips
- ⚠️ Tema exigente (Resolución de la interfaz en 720p recomendada en pi4)

|HDMI PI/PC/OXu4|RGB Dual/JAMMA|Anbernic RG353|GPICase|OGoA|OGoS|16/9|4/3|TATE|
|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
|✅|✅|✅|❌|:grey_question:|:grey_question:|✅|✅|✅|

![9.2_midnight_2.jpg](/tutorials/frontend-customization/add-themes-into-emulationstation/9.2_midnight_2.jpeg)
![9.2_midnight_1.jpg](/tutorials/frontend-customization/add-themes-into-emulationstation/9.2_midnight_1.jpg)

> Presentación y descarga: **[Foro recalbox](https://forum.recalbox.com/topic/32569/recalbox-9-2-theme-midnight)** o **[Página dedicada](https://bounar.fr/Themes_es/Midnight/)** o **[gitlab](https://gitlab.com/BenoitBounar/Theme-Midnight)**
{.is-info}

### MISTER
Autor: **Beudbeud**

- Tema optimizado para pantalla 4/3 pero funciona muy bien en 16/9
- Resolución 4/3 YOKO y TATE - 240p/480p
- Soporte de todos los sistemas y sistemas virtuales para recalbox 9.2

|HDMI PI/PC/OXu4|RGB Dual/JAMMA|Anbernic RG353|GPICase|OGoA|OGoS|16/9|4/3|TATE|
|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
|⚠️|✅|✅|✅|:grey_question:|:grey_question:|❌|✅|✅|

![9.2_mister_1.jpg](/tutorials/frontend-customization/add-themes-into-emulationstation/9.2_mister_1.jpg)
![9.2_mister_2.jpg](/tutorials/frontend-customization/add-themes-into-emulationstation/9.2_mister_2.jpg)

> Presentación y descarga: **[Foro recalbox](https://forum.recalbox.com/topic/34364/recalbox-9-2-theme-mister-240p/)** o **[github](https://github.com/beudbeud/recalbox-mister)**
{.is-info}

### Muskashi
Autor: **Airdream**

- ⚠️ Falta algunos sistemas pero es utilizable
- ⚠️ Falta el conjunto de sistemas virtuales arcade y género
- Resolución 16/9 - 720p/1080p HDMI
- Opciones: vistas de sistemas

|HDMI PI/PC/OXu4|RGB Dual/JAMMA|Anbernic RG353|GPICase|OGoA|OGoS|16/9|4/3|TATE|
|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
|✅|❌|❌|❌|❌|❌|✅|❌|❌|

![9.2_mukashi_1](/tutorials/frontend-customization/add-themes-into-emulationstation/9.2_mukashi_1.jpg)
![9.2_mukashi_2](/tutorials/frontend-customization/add-themes-into-emulationstation/9.2_mukashi_2.jpg)
> Presentación y descarga: [Foro recalbox](https://forum.recalbox.com/topic/14323)
{.is-info}

### Next Pixel
Autor: **mYSt**

- ⚠️ Falta algunos sistemas pero es utilizable
- ⚠️ Falta el conjunto de sistemas virtuales arcade
- No toma en cuenta la opción de las banderas en las gamelist
- Resolución 4/3 - 240p/480p/768p y 16/9 - 720p/1080p
- Para OGoA / OGoS tomar 16/9 480p
- Opción: colores, iconos, vistas de sistemas, vistas de gamelist, regiones
- El tema utiliza también mix/scrap específicamente diseñados para Next Pixel (ver documentación del tema)
- ⚠️ Tema exigente (Resolución de la interfaz en 720p recomendada en pi4)

|HDMI PI/PC/OXu4|RGB Dual/JAMMA|Anbernic RG353|GPICase|OGoA|OGoS|16/9|4/3|TATE|
|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
|✅|❌|❌|❌|:grey_question:|:grey_question:|✅|✅|❌|

![8.0.1_next_pixel_1.jpg](/tutorials/frontend-customization/add-themes-into-emulationstation/8.0.1_next_pixel_1.jpg)
![8.0.1_next_pixel_2.jpg](/tutorials/frontend-customization/add-themes-into-emulationstation/8.0.1_next_pixel_2.jpg)
> Presentación y descarga: [Foro recalbox](https://forum.recalbox.com/topic/16064)
{.is-info}

### Retro Switch
Autor: **Dwayne Hurst adaptado por Butch Games**
- ⚠️ Falta algunos sistemas pero es utilizable
- ⚠️ Falta el conjunto de sistemas virtuales arcade
- ⚠️ Ligero bug de visualización del logo del sistema en la lista de juegos
- Opciones: Color del tema, iconos

|HDMI PI/PC/OXu4|RGB Dual/JAMMA|Anbernic RG353|GPICase|OGoA|OGoS|16/9|4/3|TATE|
|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
|✅|⚠️|⚠️|❌|:grey_question:|:grey_question:|✅|:grey_question:|❌|

![8.0.1_retro_switch_1.jpg](/tutorials/frontend-customization/add-themes-into-emulationstation/8.0.1_retro_switch_1.jpg)
![8.0.1_retro_switch_2.jpg](/tutorials/frontend-customization/add-themes-into-emulationstation/8.0.1_retro_switch_2.jpg)
> Presentación y descarga: [Foro recalbox](https://forum.recalbox.com/topic/24439)
{.is-info}

### Unifiedspin
Autor: **Butch Games inspirado en los temas Unified**
- ⚠️ Falta algunos sistemas pero es utilizable
- ⚠️ Falta el conjunto de sistemas virtuales arcade
- ⚠️ Bug de visualización en la lista de juegos
- Opción: iconos, vistas de sistemas, vistas de gamelist, clips, regiones

|HDMI PI/PC/OXu4|RGB Dual/JAMMA|Anbernic RG353|GPICase|OGoA|OGoS|16/9|4/3|TATE|
|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
|✅|⚠️|⚠️|❌|:grey_question:|:grey_question:|✅|:grey_question:|❌|

![8.0.1_unifiedspin_10.jpg](/tutorials/frontend-customization/add-themes-into-emulationstation/8.0.1_unifiedspin_10.jpg)
![8.0.1_unifiedspin_2.jpg](/tutorials/frontend-customization/add-themes-into-emulationstation/8.0.1_unifiedspin_2.jpg)
> Presentación y descarga: [Foro recalbox](https://forum.recalbox.com/topic/24433)
{.is-info}

### Uniflyered
Autor: **Dwayne Hurst adaptado por Butch Games**
- ⚠️ Falta algunos sistemas pero es utilizable
- ⚠️ Falta el conjunto de sistemas virtuales arcade
- Opciones: iconos, playlist, regiones

|HDMI PI/PC/OXu4|RGB Dual/JAMMA|Anbernic RG353|GPICase|OGoA|OGoS|16/9|4/3|TATE|
|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
|✅|⚠️|⚠️|❌|:grey_question:|:grey_question:|✅|:grey_question:|❌|

![8.0.1_uniflyered_1.jpg](/tutorials/frontend-customization/add-themes-into-emulationstation/8.0.1_uniflyered_1.jpg)
![8.0.1_uniflyered_2.jpg](/tutorials/frontend-customization/add-themes-into-emulationstation/8.0.1_uniflyered_2.jpg)
> Presentación y descarga: [Foro recalbox](https://forum.recalbox.com/topic/22527)
{.is-info}
