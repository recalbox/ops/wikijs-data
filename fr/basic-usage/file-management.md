---
title: Gestion des fichiers
description: 
published: true
date: 2023-11-02T00:50:02.377Z
tags: gestion, fichiers
editor: markdown
dateCreated: 2021-05-21T07:49:53.575Z
---

**Recalbox** vous permet de gérer vos fichiers via le dossier "share".

Vous avez deux possibilités :

* En connexion direct
* Par le réseau

## Dossier "Share"

>Le dossier **"Share"** est celui qui vous permet de gérer le contenu de votre Recalbox, il est disponible **via le réseau**.
{.is-info}

![](/basic-usage/file-management/share1.png){.full-width}

Il est constitué de plusieurs répertoires :

![](/basic-usage/file-management/share2.png){.full-width}

* **bios**

Placez ici **les bios** dont Recalbox a besoin, vous pouvez consulter la liste des bios nécessaires via EmulationStation (`START` > `VÉRIFICATION DES BIOS`).

>Certains émulateurs exigent d'avoir le bios dans un sous-dossier ou d'une copie en plus dans son répertoire roms. Référez-vous à la documentation de chaque émulateur.
{.is-warning}

* **bootvideos**

Mettez ici les vidéos que vous pouvez apercevoir quand vous allumez ou redémarrer votre Recalbox.

* **cheats**

Contenu des cheats téléchargeables via le menu RetroArch.

* **extractions**

Répertoire où vos roms zippées, s'extraient avant de lancer le jeu.

* **kodi**

Placez ici les médias que vous souhaiter intégrer à Kodi dans les sous-répertoires correspondants :

  * music
  * movie
  * picture
  
* **music**

Placez ici vos fichiers pour remplacer la musique par défaut d'arrière-plan dans EmulationStation.

>Les fichiers doivent être aux formats `.MP3` ou `.OGG` et avoir un taux d'échantillonnage de 44100Hz et un débit supérieur à 256 kb/s.
{.is-info}

* **overlays**

Placez ici vos overlays pour habiller les contours de votre écran quand un jeu est lancé.

* **roms**

Placez ici vos roms dans les sous-répertoires correspondant à la console souhaitée.

* **saves**

Répertoire où vos sauvegardes de jeux sont stockées.

* **screenshots**

Répertoire où vos screenshots de jeux sont stockés.

* **shaders**

Répertoire où se trouve les shaders du système.

* **system**

Répertoire relatif aux configurations et système de Recalbox.

* **themes**

Placez ici vos thèmes pour l'interface d'EmulationStation de votre Recalbox.

* **userscripts**

Répertoire où peuvent être stockées vos scripts personnels.

## Transfert de fichiers

Pour ajouter vos fichiers (roms, sauvegardes de jeux, bios, données de scrap etc...) sur votre périphérique de stockage, vous avez deux possibilités :

### En connexion direct

* Connectez votre support de stockage à votre ordinateur.
* Ouvrez l'explorateur de fichiers sur votre système d'exploitation.
* Allez dans l'onglet listant vos périphériques.
* Sélectionnez le nom de votre support de stockage, puis le répertoire `recalbox` et enfin le répertoire `share`.
* Copiez vos fichiers dans le répertoire souhaité.
* Une fois le transfert terminé, il ne vous reste plus qu'à rebrancher votre périphérique à votre Recalbox, de l'allumer et de jouer.

### Par le réseau

>Lorsque vous configurez votre wifi ou branchez un câble réseau sur Recalbox, celle-ci partage automatiquement des dossiers sur le réseau local.
{.is-info}

* Une fois votre machine connectée en wifi ou en câble Ethernet, ouvrez l'explorateur de fichiers sur votre système d'exploitation.
* Allez dans l'onglet Réseaux, puis `recalbox` et enfin `share`.
* Copiez vos fichiers dans le répertoire souhaité.

>Si vous ne voyez pas votre Recalbox sur le réseau, essayez de taper `\\RECALBOX` dans la barre d'adresse de l'explorateur de fichiers. 
>
>Si cela ne fonctionne toujours pas, récupérez votre [adresse IP](./../tutorials/network/ip/discover-recalbox-ip). Puis tapez alors votre adresse IP dans la barre d'adresse de l'explorateur de fichiers. (Par exemple : \\\192.168.1.30\).
>
>Si cela ne fonctionne toujours pas et que vous êtes sur Windows 10, vous pouvez aussi suivre [ce tutoriel](./../tutorials/network/share/access-network-share).
{.is-info}

### Pour que les ajouts apparaissent :

* Mettre à jour la liste des jeux :

  * Menu EmulationStation avec `START`.
  * `OPTIONS DE L'INTERFACE`.
  * `METTRE À JOUR LA LISTE DES JEUX`.

* Redémarrer votre Recalbox :

  * Menu EmulationStation avec `SELECT`.
  * Sélectionnez `REDÉMARRER`.

## Ajouter du contenu

### Ajouter des jeux

Il vous suffit de copier vos fichiers de roms dans le répertoire correspondant à la console.

>**Exemple pour la Super Nintendo:**
>
>`/recalbox/share/roms/snes/`
{.is-success}

Vous pouvez utiliser des roms compressées (.zip, .7z) ou des roms décompressées selon l'émulateur.

>Pour plus d'informations sur les formats des roms qui sont supportées par un émulateur, veuillez consulter le fichier `_lisez-moi.txt` présent dans chaque dossier de roms.
{.is-info}

### Ajouter des bios

Certains émulateurs nécessitent un bios afin d'émuler les jeux correctement.

* Si vous souhaitez ajouter un BIOS dans votre système, ouvrez le dossier BIOS partagé via Samba ou aller directement dans `/recalbox/share/bios/`.
* Vos noms de BIOS et leur signature MD5 doivent correspondre à la liste présente dans le [Bios Manager](./../basic-usage/features/bios-manager).

Pour vérifier la signature MD5 d'un BIOS, se référer à la page [suivante](./../tutorials/utilities/rom-management/check-md5sum-of-rom-or-bios).

>Pour la Neo-Geo, vous devez ajouter le BIOS `neogeo.zip` dans le dossier des bios ou dans le dossier des roms de l'émulateur avec vos jeux Neo-Geo /recalbox/share/roms/neogeo ou /recalbox/share/roms/fbneo
>Pour la Neo-Geo CD, vous devez ajouter le bios `neogeo.zip` et `neocdz.zip` dans le dossier /recalbox/share/bios/
{.is-warning}