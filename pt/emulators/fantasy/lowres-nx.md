---
title: LowRes NX
description: 
published: true
date: 2021-11-16T08:05:11.548Z
tags: fantasy, lowres-nx, 8.0+
editor: markdown
dateCreated: 2021-10-19T22:09:17.957Z
---

![](/emulators/fantasy/lowresnx.svg){.align-center}

## Ficha técnica

* **Desenvolvedor**: Timo Kloss
* **Ano de lançamento**: 2017
* **Exibição**: 160x128 pixels, 60 Hz, 8 paletas dinâmicas de 6 bits com 4 cores cada
* **Sprites**: 64, máx. 32x32 pixels
* **Som**: 4 vozes, saw/tri/pulse/noise, pulse width, volume, ADSR, LFO

## Apresentação

LowRes NX foi inspirado em sistemas reais de 8 e 16 bits e simula chips para gráficos, som e E/S que realmente funcionam como hardware real. Ele suporta sprites de hardware, bem como rolagem de paralaxe de hardware, e até mesmo possui branco vertical e quebras de estrutura para criar autênticos efeitos retro.

## Emuladores

[Libretro LowResNX](libretro-lowresnx)