---
title: Libretro bsnes HD
description: 
published: true
date: 2024-07-24T20:27:20.416Z
tags: libretro, snes, bsnes, 8.1+
editor: markdown
dateCreated: 2022-03-01T06:09:08.524Z
---


bsnes-hd (called "HD Mode 7 mod, for bsnes" in early betas) is a fork of bsnes (great SNES emulator by byuu) that adds HD video features, such as true color, widescreen playing mode and others.

## ![](/emulators/license.svg) License

This core is under [**GPLv3**](https://github.com/libretro/bsnes-hd/blob/master/LICENSE) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ❌ | ❌ | ✅ 🐌 | ✅ | ❌ | ❌ | ✅ | ✅ |

🐌 Low performance but playable

## ![](/emulators/features.png) Features



## ![](/emulators/bios.svg) BIOS

>**No bios is required.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Supported extensions

Roms must have the extension:

* .fig
* .gd3
* .sfc
* .smc
* .swc
* .zip
* .7z

This system supports compressed roms in .zip/.7z format. However, be careful, it is only an archive.

The files contained in the .zip/.7z must correspond to the extensions mentioned above.
Moreover, each .zip/.7z file must contain only one compressed rom.

### Location

Put the roms like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 snes
┃ ┃ ┃ ┃ ┣ 🗒 **game.zip**

>The roms using **No-Intro** format are highly recommended.
{.is-success}

>For more information about roms, go to [this tutorial](./../../../tutorials/games/generalities/isos-and-roms)!
{.is-info}

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Accessing to options

You can configure various options in two different ways.

* Via RetroArch menu:

┣ 📁 RetroArch Menu
┃ ┣ 📁 Core options
┃ ┃ ┣ 🧩 Name_option

* Via the file `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core options

| Options | Options values | Variables (for configuration override) | Variables values |
| --- | --- | --- | --- |
| Color Adjustement - Luminance | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `bsnes_video_luminance` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
| Color Adjustement - Saturation | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ / `110` / `120` / `130` / `140` / `150` / `160` / `170` / `180` / `190` / `200` | `bsnes_video_saturation` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` / `110` / `120` / `130` / `140` / `150` / `160` / `170` / `180` / `190` / `200` |
| Color Adjustement - Gamma | `100` ✅ / `110` / `120` / `130` / `140` / `150` / `160` / `170` / `180` / `190` / `200` | `bsnes_video_gamma` | `100` / `110` / `120` / `130` / `140` / `150` / `160` / `170` / `180` / `190` / `200` |
| Pixel Aspect Correction | `Enabled` / `Disabled` ✅ | `bsnes_video_aspectcorrection` | `ON` / `OFF` |
| Crop Overscan | `12 pixels` ✅ / `8 pixels` | `bsnes_ppu_show_overscan` | `OFF` / `ON` |
| Blur Emulation | `Enabled` / `Disabled` ✅ | `bsnes_blur_emulation` | `ON` / `OFF` |
| Hotfixes | `Enabled` / `Disabled` ✅ | `bsnes_hotfixes` | `ON` / `OFF` |
| Entropy (randomization) | `Low` ✅ / `High` / `None` | `bsnes_entropy` | `Low` / `High` / `None` |
| Overclocking - CPU | `10%` / `20%` / `30%` / `40%` / `50%` / `60%` / `70%` / `80%` / `90%` / `100%` ✅ / `110%` / `120%` / `130%` / `140%` / `150%` / `160%` / `170%` / `180%` / `190%` / `200%` / `210%` / `220%` / `230%` / `240%` / `250%` / `260%` / `270%` / `280%` / `290%` / `300%` / `310%` / `320%` / `330%` / `340%` / `350%` / `360%` / `370%` / `380%` / `390%` / `400%` | `bsnes_cpu_overclock` | `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` / `110` / `120` / `130` / `140` / `150` / `160` / `170` / `180` / `190` / `200` / `210` / `220` / `230` / `240` / `250` / `260` / `270` / `280` / `290` / `300` / `310` / `320` / `330` / `340` / `350` / `360` / `370` / `380` / `390` / `400` |
| CPU Fast Math | `Enabled` / `Disabled` ✅ | `bsnes_cpu_fastmath` | `ON` / `OFF` |
| Overclocking - SA1 Coprocessor | `10%` / `20%` / `30%` / `40%` / `50%` / `60%` / `70%` / `80%` / `90%` / `100%` ✅ / `110%` / `120%` / `130%` / `140%` / `150%` / `160%` / `170%` / `180%` / `190%` / `200%` / `210%` / `220%` / `230%` / `240%` / `250%` / `260%` / `270%` / `280%` / `290%` / `300%` / `310%` / `320%` / `330%` / `340%` / `350%` / `360%` / `370%` / `380%` / `390%` / `400%` | `bsnes_sa1_overclock` | `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` / `110` / `120` / `130` / `140` / `150` / `160` / `170` / `180` / `190` / `200` / `210` / `220` / `230` / `240` / `250` / `260` / `270` / `280` / `290` / `300` / `310` / `320` / `330` / `340` / `350` / `360` / `370` / `380` / `390` / `400` |
| Overclocking - SuperFX Coprocessor | `10%` / `20%` / `30%` / `40%` / `50%` / `60%` / `70%` / `80%` / `90%` / `100%` ✅ / `110%` / `120%` / `130%` / `140%` / `150%` / `160%` / `170%` / `180%` / `190%` / `200%` / `210%` / `220%` / `230%` / `240%` / `250%` / `260%` / `270%` / `280%` / `290%` / `300%` / `310%` / `320%` / `330%` / `340%` / `350%` / `360%` / `370%` / `380%` / `390%` / `400%` / `410%` / `420%` / `430%` / `440%` / `450%` / `460%` / `470%` / `480%` / `490%` / `500%` / `510%` / `520%` / `530%` / `540%` / `550%` / `560%` / `570%` / `580%` / `590%` / `600%` / `610%` / `620%` / `630%` / `640%` / `650%` / `660%` / `670%` / `680%` / `690%` / `700%` / `710%` / `720%` / `730%` / `740%` / `750%` / `760%` / `770%` / `780%` / `790%` / `800%` | `bsnes_sfx_overclock` | `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` / `110` / `120` / `130` / `140` / `150` / `160` / `170` / `180` / `190` / `200` / `210` / `220` / `230` / `240` / `250` / `260` / `270` / `280` / `290` / `300` / `310` / `320` / `330` / `340` / `350` / `360` / `370` / `380` / `390` / `400` / `410` / `420` / `430` / `440` / `450` / `460` / `470` / `480` / `490` / `500` / `510` / `520` / `530` / `540` / `550` / `560` / `570` / `580` / `590` / `600` / `610` / `620` / `630` / `640` / `650` / `660` / `670` / `680` / `690` / `700` / `710` / `720` / `730` / `740` / `750` / `760` / `770` / `780` / `790` / `800` |
| PPU (Video) - Fast Mode | `Enabled` ✅ / `Disabled` | `bsnes_ppu_fast` | `ON` / `OFF` |
| PPU (Video) - Deinterlace | `Enabled` ✅ / `Disabled` | `bsnes_ppu_deinterlace` | `ON` / `OFF` |
| PPU (Video) - No Sprite Limit | `Enabled` ✅ / `Disabled` | `bsnes_ppu_no_sprite_limit` | `ON` / `OFF` |
| PPU (Video) - No VRAM Blocking | `Enabled` / `Disabled` ✅ | `bsnes_ppu_no_vram_blocking` | `ON` / `OFF` |
| DSP (Audio) - Fast Mode | `Enabled` ✅ / `Disabled` | `bsnes_dsp_fast` | `ON` / `OFF` |
| DSP (Audio) - Cubic Interpolation | `Enabled` / `Disabled` ✅ | `bsnes_dsp_cubic` | `ON` / `OFF` |
| DSP (Audio) - Echo Shadow RAM | `Enabled` / `Disabled` ✅ | `bsnes_dsp_echo_shadow` | `ON` / `OFF` |
| Coprocessor - Fast Mode | `Enabled` ✅ / `Disabled` | `bsnes_coprocessor_delayed_sync` | `ON` / `OFF` |
| Coprocessor - Prefer HLE | `Enabled` ✅ / `Disabled` | `bsnes_coprocessor_prefer_hle` | `ON` / `OFF` |
| Preferred Super Game Boy BIOS (Requires Restart) | `Super Game Boy (SGB1.sfc)` ✅ / `Super Game Boy 2 (SGB2.sfc)` | `bsnes_sgb_bios` | `SGB1.sfc` / `SGB2.sfc` |
| Internal Run-Ahead | `Disabled` ✅ / `1 frame` / `2 frames` / `3 frames` / `4 frames` | `bsnes_run_ahead_frames` | `OFF` / `1` / `2` / `3` / `4` |
| Expect Headered ROMs for IPS Patches | `Enabled` / `Disabled` ✅ | `bsnes_ips_headered` | `ON` / `OFF` |
| HD Mode 7 - Scale | `Disabled` / `240p (1x)` / `480p (2x)` ✅ / `720p (3x)` / `960p (4x)` / `1200p (5x)` / `1440p (6x)` / `1680p (7x)` / `1920p (8x)` / `2160p (9x)` / `2400p (10x)` | `bsnes_mode7_scale` | `disable` / `1x` / `2x` / `3x` / `4x` / `5x` / `6x` / `7x` / `8x` / `9x` / `10x` |
| HD Mode 7 - Perspective Correction | `Auto (Wide)` ✅ / `Auto (Medium)` / `Auto (Narrow)` / `On (Wide)` / `On (Medium)` / `On (Narrow)` / `Disabled` | `bsnes_mode7_perspective` | `auto (wide)` / `auto (medium)` / `auto (narrow)` / `on (wide)` / `on (medium)` / `on (narrow)` / `off` |
| HD Mode 7 - Supersampling | `Disabled` ✅ / `2x` / `3x` / `4x` / `5x` / `6x` / `7x` / `8x` / `9x` / `10x` | `bsnes_mode7_supersample` | `none` / `2x` / `3x` / `4x` / `5x` / `6x` / `7x` / `8x` / `9x` / `10x` |
| HD Mode 7 - HD->SD Mosaic | `non-HD` / `ignore` / `1x scale` ✅ | `bsnes_mode7_mosaic` | `non-HD` / `ignore` / `1x scale` |
| HD Background Color Radius | `1` / `2` / `3` / `4` ✅ / `5` / `6` / `7` / `8` / `Disabled` | `bsnes_mode7_bgGrad` | `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `0` |
| HD Windowing (Experimental) | `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `Disabled` ✅ | `bsnes_mode7_windRad` | `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `0` |
| Widescreen Mode | `Only for Mode 7 scenes` ✅ / `Enable for all scenes` / `Disabled` | `bsnes_mode7_wsMode` | `Mode 7` / `all` / `none` |
| Widescreen - Aspect Ratio | `16:9` ✅ / `16:10` / `21:9` / `2:1` / `4:3` / `Disabled` | `bsnes_mode7_widescreen` | `16:9` / `16:10` / `21:9` / `2:1` / `4:3` / `none` |
| Widescreen - Background 1 | `Enabled` / `Enable if screen-wide and X position is 0` / `Enable if screen-wide and XY position is 0` ✅ / `Enable above line 40` / `Enable below line 40` / `Enable above line 80` / `Enable below line 80` / `Enable above line 120` / `Enable below line 120` / `Enable above line 160` / `Enable below line 160` / `Enable above line 200` / `Enable below line 200` / `Hide lateral 8 pixels` / `Hide lateral 8 black pixels` / `Disable widescreen for this layer` / `Hide layer entirely` | `bsnes_mode7_wsbg1` | `on` / `auto horizontal` / `auto horz and vert` / `above line 40` / `below line 40` / `above line 80` / `below line 80` / `above line 120` / `below line 120` / `above line 160` / `below line 160` / `above line 200` / `below line 200` / `crop edges` / `auto crop edges` / `off` / `disable entirely` |
| Widescreen - Background 2 | `Enabled` / `Enable if screen-wide and X position is 0` / `Enable if screen-wide and XY position is 0` ✅ / `Enable above line 40` / `Enable below line 40` / `Enable above line 80` / `Enable below line 80` / `Enable above line 120` / `Enable below line 120` / `Enable above line 160` / `Enable below line 160` / `Enable above line 200` / `Enable below line 200` / `Hide lateral 8 pixels` / `Hide lateral 8 black pixels` / `Disable widescreen for this layer` / `Hide layer entirely` | `bsnes_mode7_wsbg2` | `on` / `auto horizontal` / `auto horz and vert` / `above line 40` / `below line 40` / `above line 80` / `below line 80` / `above line 120` / `below line 120` / `above line 160` / `below line 160` / `above line 200` / `below line 200` / `crop edges` / `auto crop edges` / `off` / `disable entirely` |
| Widescreen - Background 3 | `Enabled` / `Enable if screen-wide and X position is 0` / `Enable if screen-wide and XY position is 0` ✅ / `Enable above line 40` / `Enable below line 40` / `Enable above line 80` / `Enable below line 80` / `Enable above line 120` / `Enable below line 120` / `Enable above line 160` / `Enable below line 160` / `Enable above line 200` / `Enable below line 200` / `Hide lateral 8 pixels` / `Hide lateral 8 black pixels` / `Disable widescreen for this layer` / `Hide layer entirely` | `bsnes_mode7_wsbg3` | `on` / `auto horizontal` / `auto horz and vert` / `above line 40` / `below line 40` / `above line 80` / `below line 80` / `above line 120` / `below line 120` / `above line 160` / `below line 160` / `above line 200` / `below line 200` / `crop edges` / `auto crop edges` / `off` / `disable entirely` |
| Widescreen - Background 4 | `Enabled` / `Enable if screen-wide and X position is 0` / `Enable if screen-wide and XY position is 0` ✅ / `Enable above line 40` / `Enable below line 40` / `Enable above line 80` / `Enable below line 80` / `Enable above line 120` / `Enable below line 120` / `Enable above line 160` / `Enable below line 160` / `Enable above line 200` / `Enable below line 200` / `Hide lateral 8 pixels` / `Hide lateral 8 black pixels` / `Disable widescreen for this layer` / `Hide layer entirely` | `bsnes_mode7_wsbg4` | `on` / `auto horizontal` / `auto horz and vert` / `above line 40` / `below line 40` / `above line 80` / `below line 80` / `above line 120` / `below line 120` / `above line 160` / `below line 160` / `above line 200` / `below line 200` / `crop edges` / `auto crop edges` / `off` / `disable entirely` |
| Widescreen - Sprites | `Render when in safe area` ✅ / `Render anywhere (unsafe)` / `Clip at widescreen edges` / `Hide sprites entirely` | `bsnes_mode7_wsobj` | `safe` / `unsafe` / `clip` / `disable entirely` |
| Widescreen - Area Background Color | `Fill if scene is widescreen` ✅ / `Always fill` / `Use black background` | `bsnes_mode7_wsBgCol` | `auto` / `color` / `black` |
| Widescreen - Ignore Window Effects | `Only 'outside' effects` ✅ / `Only 'outside' and 'always' effects` / `All window effects` / `Disabled` | `bsnes_mode7_igwin` | `outside` / `outside and always` / `all` / `none` |
| Widescreen - Fallback X-Coordinate | `40` / `88` / `128 (Center)` ✅ / `168` / `216` | `bsnes_mode7_igwinx` | `40` / `88` / `128` / `168` / `216` |
| Widescreen - Marker | `Show lines at edges` / `Darken areas` / `Disabled` ✅ | `bsnes_mode7_wsMarker` | `lines` / `darken` / `none` |
| Widescreen - Marker Alpha | `1/1` ✅ / `1/2` / `1/3` / `1/4` / `1/5` / `1/6` / `1/7` / `1/8` / `1/9` / `1/10` | `bsnes_mode7_wsMarkerAlpha` | `1/1` / `1/2` / `1/3` / `1/4` / `1/5` / `1/6` / `1/7` / `1/8` / `1/9` / `1/10` |
| Widescreen - Stretch Window | `Enabled` / `Disabled` ✅ | `bsnes_mode7_strWin` | `ON` / `OFF` |

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/libretro/bsnes-libretro/](https://github.com/libretro/bsnes-hd/)