---
title: Libretro melonDS
description: 
published: true
date: 2024-07-25T22:41:13.413Z
tags: libretro, ds, melonds
editor: markdown
dateCreated: 2023-09-11T13:52:39.877Z
---

**Libretro melonDS** es un prometedor emulador de Nintendo DS/DSI desarrollado en C y C++.

El emulador **melonDS** pretende proporcionar una emulación rápida y precisa de Nintendo DS. Aunque todavía es un trabajo en progreso, tiene un conjunto bastante sólido de características:

* Núcleo casi completo (CPU, vídeo, audio, ...)
* Recompilador JIT para una emulación rápida
* Renderizado OpenGL, conversión 3D
* RTC, micrófono, tapa cerrada/abierta
* Soporte para joystick
* Savestates
* Diferentes modos de visualización / tamaño / rotación
* (WIP) Wifi: multijugador local, conectividad online
* Emulación DSi (WIP)
* DLDI
* (WIP) GBA slot add-ons
* ¡y hay más planeados!

Ha sido escrito por **Arisotura** (conocido como StapleButter), antiguo colaborador de DeSmuME.

## ![](/emulators/license.svg) Licencia

El core posee la siguiente licencia [**GPLv3**](https://github.com/libretro/melonDS/blob/master/LICENSE).

## ![](/emulators/compatibility.png) Compatibilidad

 | RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | 
 | ❌ | ❌ | ❌ | ❌ | ❌ | ❌ | ✅ | ❌ |

## ![](/emulators/features.png) Funcionalidades

| Funcionalidad | ¿Soportada? |
| :---: | :---: |
| Reinicio | ✔ |
| Capturas de pantalla | ✔ |
| Grabaciones | ✔ |
| Grabaciones instantáneas | ✔ |
| Rebobinado | ✔ |
| Opciones del core | ✔ |
| RetroAchievements | ✔ |
| Cheats RetroArch | ✔ |
| Controles | ✔ |
| Remapping | ✔ |

## ![](/emulators/bios.svg) BIOS

### Lista de BIOS obligatorias

| Nombre del fichero | Descripción | MD5 | ¿Preinstalada? |
| :---: | :---: | :---: | :---: |
| bios7.bin | BIOS ARM7 | df692a80a5b1bc90728bc3dfc76cd948 | ❌ |
| bios9.bin | BIOS ARM9 | a392174eb3e572fed6447e956bde4b25 | ❌ |
| firmware.bin | Firmware NDS | 145eaef5bd3037cbc247c213bb3da1b3 e45033d9b0fa6b0de071292bba7c9d13 3ad72b2c9a736b24953f2d391da4bfcc | ❌ |

### Emplazamiento

Disponed vuestras bios como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 nds
┃ ┃ ┃ ┃ ┣ 🗒 **bios7.bin**
┃ ┃ ┃ ┃ ┣ 🗒 **bios9.bin**
┃ ┃ ┃ ┃ ┣ 🗒 **firmware.bin**

## ![](/emulators/roms.png) Roms

### Extensiones de roms soportadas

Las roms deben tener las extensiones siguientes:

* .nds
* .zip
* .7z

### Emplazamiento

Disponed vuestras roms como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 nds
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.zip**

>Se aconsejan las roms del proyecto **No-Intro**.
{.is-success}

>Para más información sobre las roms, consultad [este tutorial](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

>Para conservar las configuraciones personalizadas cuando se produce una actualización del sistema Recalbox, os aconsejamos utilizar las [Sobrecargas de configuración](./../../../advanced-usage/configuration-override).
{.is-info}

### Acceder a las opciones

Podéis configurar las opciones del core de dos formas diferentes.

* Via el Menú RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via el fichero `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Opciones del core

| Opción | Valor de la opción | Variable (para la sobrecarga) | Valores posibles |
| --- | --- | --- | --- |
| Console Mode | `DS` ✅ / `DSi` | `melonds_console_mode` | `DS` / `DSi` |
| Boot game directly | `Activé` ✅ / `Désactivé` | `melonds_boot_directly` | `enabled` / `disabled` |
| Use Firmware Settings | `Désactivé` ✅ / `Activé` | `melonds_use_fw_settings` | `disabled` / `enabled` |
| Language | `Japanese` / `English` ✅ / `French` / `German` / `Italian` / `Spanish` | `melonds_language` | `Japanese` / `English` / `French` / `German` / `Italian` / `Spanish` |
| Randomize MAC Address | `Désactivé` ✅ / `Activé` | `melonds_randomize_mac_address` | `disabled` / `enabled` |
| Enable DSi SD Card | `Désactivé` ✅ / `Activé` | `melonds_dsi_sdcard` | `disabled` / `enabled` |
| Threaded software renderer | `Désactivé` ✅ / `Activé` | `melonds_threaded_renderer` | `disabled` / `enabled` |
| OpenGL Renderer (Restart) | `Désactivé` ✅ / `Activé` | `melonds_opengl_renderer` | `disabled` / `enabled` |
| OpenGL Internal Resolution | `1x native (256x192)` ✅ / `2x native (512x384)` / `3x native (768x576)` / `4x native (1024x768)` / `5x native (1280x960)` / `6x native (1536x1152)` / `7x native (1792x1344)` / `8x native (2048x1536)` | `melonds_opengl_resolution` | `1x native (256x192)` / `2x native (512x384)` / `3x native (768x576)` / `4x native (1024x768)` / `5x native (1280x960)` / `6x native (1536x1152)` / `7x native (1792x1344)` / `8x native (2048x1536)` |
| OpenGL Improved polygon splitting | `Désactivé` ✅ / `Activé` | `melonds_opengl_better_polygons` | `disabled` / `enabled` |
| OpenGL Filtering | `nearest` ✅ / `linear` | `melonds_opengl_filtering` | `nearest` / `linear` |
| Microphone Input | `Blow Noise` ✅ / `White Noise` | `melonds_mic_input` | `Blow Noise` / `White Noise` |
| Audio Bitrate | `Automatic` ✅ / `10-bit` / `16-bit` | `melonds_audio_bitrate` | `Automatic` / `10-bit` / `16-bit` |
| Audio Interpolation | `None` ✅ / `Linear` / `Cosine` / `Cubic` | `melonds_audio_interpolation` | `None` / `Linear` / `Cosine` / `Cubic` |
| Touch mode | `Désactivé` ✅ / `Mouse` / `Touch` / `Joystick` | `melonds_touch_mode` | `disabled` / `Mouse` / `Touch` / `Joystick` |
| Swap Screen mode | `Toggle` ✅ / `Hold` | `melonds_swapscreen_mode` | `Toggle` / `Hold` |
| Screen Layout | `Top/Bottom` ✅ / `Bottom/Top` / `Left/Right` / `Right/Left` / `Top Only` / `Bottom Only` / `Hybrid Top` / `Hybrid Bottom` | `melonds_screen_layout` | `Top/Bottom` / `Bottom/Top` / `Left/Right` / `Right/Left` / `Top Only` / `Bottom Only` / `Hybrid Top` / `Hybrid Bottom` |
| Screen Gap | `0` ✅ / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `21` / `22` / `23` / `24` / `25` / `26` / `27` / `28` / `29` / `30` / `31` / `32` / `33` / `34` / `35` / `36` / `37` / `38` / `39` / `40` / `41` / `42` / `43` / `44` / `45` / `46` / `47` / `48` / `49` / `50` / `51` / `52` / `53` / `54` / `55` / `56` / `57` / `58` / `59` / `60` / `61` / `62` / `63` / `64` / `65` / `66` / `67` / `68` / `69` / `70` / `71` / `72` / `73` / `74` / `75` / `76` / `77` / `78` / `79` / `80` / `81` / `82` / `83` / `84` / `85` / `86` / `87` / `88` / `89` / `90` / `91` / `92` / `93` / `94` / `95` / `96` / `97` / `98` / `99` / `100` / `101` / `102` / `103` / `104` / `105` / `106` / `107` / `108` / `109` / `110` / `111` / `112` / `113` / `114` / `115` / `116` / `117` / `118` / `119` / `120` / `121` / `122` / `123` / `124` / `125` / `126` | `melonds_screen_gap` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `21` / `22` / `23` / `24` / `25` / `26` / `27` / `28` / `29` / `30` / `31` / `32` / `33` / `34` / `35` / `36` / `37` / `38` / `39` / `40` / `41` / `42` / `43` / `44` / `45` / `46` / `47` / `48` / `49` / `50` / `51` / `52` / `53` / `54` / `55` / `56` / `57` / `58` / `59` / `60` / `61` / `62` / `63` / `64` / `65` / `66` / `67` / `68` / `69` / `70` / `71` / `72` / `73` / `74` / `75` / `76` / `77` / `78` / `79` / `80` / `81` / `82` / `83` / `84` / `85` / `86` / `87` / `88` / `89` / `90` / `91` / `92` / `93` / `94` / `95` / `96` / `97` / `98` / `99` / `100` / `101` / `102` / `103` / `104` / `105` / `106` / `107` / `108` / `109` / `110` / `111` / `112` / `113` / `114` / `115` / `116` / `117` / `118` / `119` / `120` / `121` / `122` / `123` / `124` / `125` / `126` |
| JIT Enable (Restart) | `Activé` ✅ / `Désactivé` | `melonds_jit_enable` | `enabled` / `disabled` |
| JIT Block Size | `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `21` / `22` / `23` / `24` / `25` / `26` / `27` / `28` / `29` / `30` / `31` / `32` ✅ / `33` / `34` / `35` / `36` / `37` / `38` / `39` / `40` / `41` / `42` / `43` / `44` / `45` / `46` / `47` / `48` / `49` / `50` / `51` / `52` / `53` / `54` / `55` / `56` / `57` / `58` / `59` / `60` / `61` / `62` / `63` / `64` / `65` / `66` / `67` / `68` / `69` / `70` / `71` / `72` / `73` / `74` / `75` / `76` / `77` / `78` / `79` / `80` / `81` / `82` / `83` / `84` / `85` / `86` / `87` / `88` / `89` / `90` / `91` / `92` / `93` / `94` / `95` / `96` / `97` / `98` / `99` / `100` | `melonds_jit_block_size` | `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `21` / `22` / `23` / `24` / `25` / `26` / `27` / `28` / `29` / `30` / `31` / `32` / `33` / `34` / `35` / `36` / `37` / `38` / `39` / `40` / `41` / `42` / `43` / `44` / `45` / `46` / `47` / `48` / `49` / `50` / `51` / `52` / `53` / `54` / `55` / `56` / `57` / `58` / `59` / `60` / `61` / `62` / `63` / `64` / `65` / `66` / `67` / `68` / `69` / `70` / `71` / `72` / `73` / `74` / `75` / `76` / `77` / `78` / `79` / `80` / `81` / `82` / `83` / `84` / `85` / `86` / `87` / `88` / `89` / `90` / `91` / `92` / `93` / `94` / `95` / `96` / `97` / `98` / `99` / `100` |
| JIT Branch optimisations | `Activé` ✅ / `Désactivé` | `melonds_jit_branch_optimisations` | `enabled` / `disabled` |
| JIT Literal optimisations | `Activé` ✅ / `Désactivé` | `melonds_jit_literal_optimisations` | `enabled` / `disabled` |
| JIT Fast Memory | `Activé` ✅ / `Désactivé` | `melonds_jit_fast_memory` | `enabled` / `disabled` |

## ![](/emulators/external-links.png) Enlaces externos

* **Código fuente utilizado** : [https://github.com/libretro/melonds/](https://github.com/libretro/melonds/)
* **Documentación Libretro** : [https://docs.libretro.com/library/melonds/](https://docs.libretro.com/library/melonds/)
* **Código fuente oficial** : [https://github.com/Arisotura/melonDS](https://github.com/Arisotura/melonDS/)
* **Página web oficial** : [http://melonds.kuribo64.net/](http://melonds.kuribo64.net/)
* **Foro oficial** : [http://melonds.kuribo64.net/board/](http://melonds.kuribo64.net/board/)