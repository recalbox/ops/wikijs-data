---
title: EmulationStation
description: 
published: true
date: 2024-11-11T07:30:18.202Z
tags: 
editor: markdown
dateCreated: 2021-06-26T10:40:36.119Z
---

## Vorstellung

Wenn du deine Recalbox einschaltest, gelangst du zur Benutzeroberfläche namens **EmulationStation** (ES).

Damit lassen sich Spiele starten, bestimmte Optionen einstellen oder die Recalbox aktualisieren.

### Liste der Systeme

Der erste Bildschirm ist die Systemliste. Sie zeigt die verfügbaren Konsolen und Systeme an.

![](/basic-usage/getting-started/es1.png){.full-width}

Wenn du ein System auswählst, wechselt der Bildschirm zu der Liste der verfügbaren Spiele für das jeweilige System.

### Liste der Spiele

Für das auf dem vorherigen Bildschirm ausgewählte System ist eine Liste von Spielen verfügbar.

Sobald ein Spiel läuft, findest du unter [Während des Spiels](./basic-usage/getting-started/special-commands/in-game) die verfügbaren Optionen.

## Optionen in EmulationStation

Wenn du auf die Taste `START` drückst, gelangst Du in das Hauptmenü und kannst dort verschiedene Einstellungen deiner Recalbox ändern.

![](/basic-usage/getting-started/es2.png){.full-width}

### Kodi Media Center

* Startet **Kodi Media Center** (früher bekannt als `XBMC`).

Du kannst Kodi vom Startbildschirm aus starten, indem du die `X`-Taste auf deinem Controller drückst.

Um Kodi zu beenden, wählst du `VERLASSEN` im Programm, und schon bist du wieder in EmulationStation.

Controller werden in Kodi unterstützt. Aber wenn du es vorziehst, kannst du auch HDMI-CEC (erlaubt es dir, deine TV-Fernbedienung zu benutzen, um in Kodi zu navigieren) oder eine Smartphone-Fernbedienungs-App benutzen.

Für weitere Informationen zu Kodi kannst du folgende Anleitungen zu Rate ziehen:

[Kodi Media Center](./features/kodi-media-center)
[Verwendung einer Infrarot-Fernbedienung](./features/kodi-media-center/remote-control-usage)

### Systemeinstellungen

Du hast Zugriff auf diese Information in deiner Recalbox.

* **Version**: die aktuell installierte Version von Recalbox. 
* **Speicherplatz**: verwendeter/verfügbarer Speicherplatz.  
* **Speichermedien**: listet die verfügbaren Speichermedien auf:
  * Interne Share-Partition (1. Festplatte, die vom System für den ersten Bootvorgang verwendet wird).
  * Jeder externe Speicher (alle Festplatten und andere USB-Sticks werden erkannt).
* **Sprache**: die Auswahl der Menüsprache. 
* **Tastatur**: die Art der Tastatur, die du verwenden möchtest

### Updates

In diesem Menü kannst du Updates aktivieren/deaktivieren und auswählen, ob du nur stabile Versionen oder auch Beta-Versionen von Recalbox installiert werden sollen.

* **Nach Updates suchen**: Prüft, ob ein Update vorhanden ist.
* **Update verfügbar**: Teilt mit, dass ein Update verfügbar ist.
* **Changelog des Updates**: Zeigt den Inhalt des Updates an.
* **Update Starten**: Startet den Update-Vorgang, sofern ein Update zur Verfügung steht.

### Spieleinstellungen

Dieses Menü bietet die folgenden Einstellungen:

* **Bildformat** :
  * Auto
  * RetroArch Benutzerdefiniert
  * Vom Core zur Verfügung gestellt
  * Nicht einstellen
  * Integer Scale (Pixel Perfect)
  * Spécifique RetroArch
  * 1/1
  * 16/10
  * 16/15
  * 16/9
  * 19/12
  * 19/14
  * 2/1
  * 21/9
  * 3/2
  * 3/4
  * 30/17
  * 32/9
  * 4/1
  * 4/3
  * 5/4
  * 6/5
  * 7/9
  * 8/3
  * 8/7
  * 9/16 
* **Spiele glätten** : hiermit lässt sich den Pixeln in Spielen ein kleiner Unschärfeeffekt verleihen, damit es auf modernen Fernsehern etwas besser aussieht. 
* **Zurückspulen**: ermöglicht es dir, während eines Spiels zurückzuspulen.

>Einige Emulatoren (PS1, Dreamcast, ...) können langsamer werden, wenn diese Funktion bei diesen Emulatoren standardmäßig aktiviert ist.  
>Über das Menü [Erweiterte Einstellungen] (#advances-settings) lässt sie sich nur für einige Emulatoren aktivieren. 
{.is-warning}

* **Automatisches Speichern/Laden**: Ermöglicht dir, ein Spiel dort fortzusetzen, wo du aufgehört hast, als du es beendet hast. 
* **Zweimal drücken, um das Spiel zu beenden**: zum Bestätigen, dass das laufende Spiel beendet werden soll.
* **Skalierung anpassen (Pixel Perfect)**: Zeigt Spiele in ihrer ursprünglichen Auflösung an.
* **Shader**: 
* **Vordefinierte Shader**: Du kannst ganz einfach _Shader_ für verschiedene Systeme konfigurieren:
  * Die **Scanlines**-Shader aktivieren Scanlines auf allen Systemen, um eine CRT-ähnliche Darstellung zu erreichen. 
  * Die **Retro**-Shader sind ein Shader-Pack, das dem besten Shader für jedes System entspricht. Die Shader in diesem Pack wurden von der Community ausgewählt und bringen dir für jedes System das dem Original am nächsten kommende Spielerlebnis!
  Weitere Informationen findest du in der Anleitung zur [Shader-Konfiguration](./tutorials/games/generalities/shaders-configuration).

>Du kannst den _Shader_ auch während des Spiels ändern, indem du deinen Controller benutzt. Verwende die [Spezialbefehle](./special-commands/in-game) `Hotkey` + `R2` oder `Hotkey` + `L2`, um zjm nächsten oder vorherigen Shader zu wechseln.
{.is-info}

* **Optionen für Retroachievements**: Ermöglicht das Einrichten von Retroachievements (Äquivalent zu Erfolgen/Trophäen, aber für ältere Konsolen).
* **NetPlay-Optionen**: Ermöglicht die Einrichtung von NetPlay (Online-Spiel).
* **Vorinstallierte Spiele ausblenden**: Ermöglicht das Ausblenden vorinstallierter Spiele aus dem System.

### Controller Einstellungen

Hier kannst du deine Controller konfigurieren.

* **Controller konfigurieren**: Ermöglicht es dir, die Tastenbelegung deines Controllers festzulegen.
* **Bluetooth Controller verbinden**: Ermöglicht dir, einen drahtlosen Controller mit deiner Recalbox zu verbinden.
* **Bluetooth-Controller vergessen**: Ermöglicht das Entkoppeln aller Wireless-Controller.
* **Treiber**: Ermöglicht die Einstellung des Treibers, den du mit deinem Controller verwenden möchtest. Solange der Controller funktioniert, braucht man den Treiber nicht zu ändern.

### UI Einstellungen

Dieses Menü bietet Zugriff auf die Oberflächeneinstellungen.

* **Bildschirmschoner**: Ermöglicht dir, den Bildschirmschoner mit verschiedenen Optionen zu ändern.
* **Uhr im Menü**: Anzeige der aktuellen Uhrzeit in den Menüs von EmulationStation aktivieren/deaktivieren.
* **Bildschirmhilfe**: Ermöglicht die Anzeige eines Hilfe-Popups oben rechts auf dem Bildschirm für jede Option, durch drücken der `Y`-Taste.
* **Popup Einstellungen**: Ermöglicht es, Optionen für die Musik- und NetPlay-Hilfe-Popups zu ändern.


>Update-Popups sind nicht konfigurierbar.
{.is-info}

* **System Schnellauswahl**: Ermöglicht den Wechsel von einem System zu einem anderen durch Drücken der rechten oder linken Taste in den Spiellisten.
* **Theme**: eine Auswahl an verschiedenen Themes. (Die von der Community erstellten Themes sind nicht standardmäßig in Recalbox verfügbar).
* **Theme Konfiguration**: einige der installierten Themes haben Konfigurationsoptionen.
* **Spieleliste Aktualisieren**: Wenn du ein Spiel hinzugefügt oder gelöscht hast, musst du die Spieleliste aktualisieren, damit deine Änderungen wirksam werden.
* **Buttons zum Bestätigen/Abbrechen vertauschen**: Vertauscht die Tasten für "Bestätigen" und "Abbrechen" (`A/B` - `B/A`).

### Soundeinstellungen

Dieses Menü bietet folgende Optionen:

* **Lautstärke**: Legt die Audio-Lautstärke für das Front-End (EmulationStation) und die Spiele fest.
* **Audio-Modus**: Ermöglicht die Auswahl der Klänge, die in EmulationStation zu hören sein sollen.
* **Ausgabegerät**: legt fest, welches Ausgabegerät zur Wiedergabe von Ton genutzt werden soll
  * VC4-HDMI-0 - HDMI/DISPLAYPORT
  * KOPFHÖRER - ANALOGE AUSGABE

Es können auch weitere Audioausgänge aufgeführt sein. Dann wähle den, den Du nutzen möchtest.

### Netzwerkoptionen

Dieses Menü bietet folgende Optionen:

* **Status**: Zeigt den aktuellen Status der Netzwerkverbindung 
* **IP Adresse**: Zeigt die IP Adresse der Recalbox.
* **WLAN Aktivieren**: Hiermit lässt sich das WLAN aktivieren/deaktivieren.
* **WLAN SSID**: ermöglicht die manuelle Eingabe der SSID.
* **WLAN Passwort**: ermöglicht die manuelle Eingabe des WLAN Passworts.
* **Netzwerkname**: 

### Scraper

Für jedes Spiel können mit Hilfe des Scrapers Informationen aus dem Internet abgerufen werden (Cover, Zusammenfassung, etc.), die dann im EmulationStation-Spielauswahlmenü angezeigt werden.

Drücke `START` und rufe `SCRAPER` auf. Folge dann den Anweisungen auf dem Bildschirm.  Weitere Informationen zum internen Scraper findest du [hier](./features/internal-scraper).

### Erweiterte Einstellungen

* **Übertakten**: ermöglicht, je nach Gerät, folgende Optionen zum Übertakten.
  * KEINE
  * HOCH
  * TURBO
  * EXTREM
  * NOLIMIT

>TURBO- und EXTREM-Einstellungen können irreversible Schäden am Raspberry Pi verursachen, wenn sie nicht unter den richtigen Bedingungen (Wärmeabfuhr, Belüftung usw.) vorgenommen werden. Die Gewährleistung für deine Hardware kann erlöschen.
{.is-danger}

* **Boot Einstellungen**: ermöglicht es dir, einige Startparameter festzulegen..
* **Virtuelle Systeme**: ermöglicht die Anzeige von virtuellen Systemen in der Systemliste.
* **Spiele für Erwachsene ausblenden**: ermöglicht dir, Spiele, die als für Erwachsene gekennzeichnet sind, auszublenden. Funktioniert, sobald deine Spiele gescraped wurden.
* **Erweiterte Emulator Konfiguration**: ermöglicht die Konfiguration einzelner Emulatoren. 
* **Kodi Einstellungen**: ermöglicht die Aktivierung/Deaktivierung von Kodi, den automatischen Start von Kodi beim Start der Recalbox oder die Aktivierung der Verknüpfung zum Starten von Kodi aus der Systemliste heraus..
* **Sicherheit**: ermöglicht dir, dass root Passwort zu ändern.
* **Overscan**: hilft dir, wenn du schwarze Streifen siehst oder wenn das Bild größer als dein Bildschirm ist.
* **Bildrate anzeigen**: schlatet die Anzeige der Framerate in EmulationStation und in Spielen an oder aus.
* **Recalbox Manager**: eröglicht das Aktivieren/Deaktivieren des Recalbox Web Managers.
* **Werkseinstellungen wiederherstellen**: ermöglicht es dir, deine Recalbox in denselben Zustand zurückzusetzen, als ob sie neu installiert worden wäre. Persönliche Daten sind davon nicht betroffen.

### BIOS Überprüfung

Über dieses Menü lassen sich die BIOS verwalten, die von einigen der Recalbox-Emulatoren benötigt werden. Weitere Informationen dazu findest du auf [dieser Seite](./features/bios-manager).

### Open-Spurce Lizenz

Dieser Menüpunkt zeigt die Lizenz von Recalbox an.

### Beenden

Menü, mit dem die Recalbox ausgeschaltet werden kann.

* System Herunterfahren
* System scnhell Herunterfahren
* System Neustarten

## Steuerung

#### Befehle auf der Oberfläche:

* `A` → Auswählen
* `B` → Zurück
* `Y` → Favorit
* `X` → Kodi starten
* `Start` → Hauptmenü
* `Select` → Optionen (öffnet auf dem Systembildschirm das Neustartmenü)
* `R` → Nächste Seite
* `L` → Vorherige Seite

## Favoriten

Du kannst ein Spiel als Favorit festlegen, indem du die Taste `Y` drückst, nachdem du das Spiel in der Auswahl markiert hast. Das Spiel wird dann mit einem ☆ vor dem Namen an den Anfang der Liste gesetzt.

>Das System muss über das Menü der EmulationStation sauber heruntergefahren werden, damit die Liste der Lieblingsspiele gespeichert wird und sie beim nächsten Hochfahren wiedergefunden werden kann.
{.is-warning}