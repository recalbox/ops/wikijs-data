---
title: Los diferentes proyectos
description: Los diferentes grupos que trabajan en la preservación de la biblioteca lúdica mundial
published: true
date: 2023-11-30T19:58:38.322Z
tags: roms, juegos, grupos, proyectos, biblioteca, redump, tosec, nointro
editor: markdown
dateCreated: 2023-09-09T12:42:26.419Z
---

Para saber qué roms utilizar en cada sistema, conviene conocer los diferentes grupos que las referencian y catalogan.

La diferencia entre estos grupos es sencilla: cada uno de ellos está especializado en un tipo de soporte (disquete, cartucho y CD-ROM).

## FinalBurn Neo (⭐⭐⭐⭐)

### Archivos Dat

Podéis descargar estos ficheros desde [este sitio](https://github.com/libretro/FBNeo/tree/master/dats).

## Mame (⭐⭐⭐⭐)

### Los archivos dat

Podéis descargar estos ficheros de [este sitio](https://www.progettosnaps.net/dats/MAME/).

## No-intro (⭐⭐⭐⭐)

No-Intro sólo se centra en juegos publicados en **soportes de cartucho** (Master-System, SNES, N64, Gameboy, etc).

No-Intro sólo conserva las mejores roms, sin errores ni cambios, y que son lo más parecidas posible a los cartuchos originales.

No-Intro contiene generalmente una sola versión funcional de cada juego para un país determinado (US, Eur, Jp, Fr, De, ...), así como todas las actualizaciones de estos juegos (Rev1, Rev2, Beta, ...).

>Estos juegos son por tanto las mejores roms para aquellos que quieran utilizar la funcionalidad Netplay.
{.is-success}

### Los archivos dat

Podéis descargar estos ficheros desde este [sitio](https://datomatic.no-intro.org/?page=download).

## Redump (⭐⭐⭐⭐)

Redump se centra exclusivamente en juegos publicados en **soportes ópticos** (CD, DVD, etc.) (PlayStation, Sega Saturn, GameCube, Dreamcast, etc.).

Al igual que No-Intro, Redump sólo conserva las mejores isos, sin errores ni cambios, y que son lo más parecidas posible al soporte original.

Las isos de Redump son más grandes que las de Tosec o Trurip, pero son de mejor calidad, lo que repercute directamente en el comportamiento de la emulación.

>Encontrarás [aquí](http://wiki.redump.org/index.php?title=Discs_not_yet_dumped) la lista de discos no volcados.
{.is-info}

### Los archivos dat

Podéis descargar estos ficheros desde este [sitio](http://redump.org/downloads/).

## Tosec (⭐⭐⭐)

TOSEC pretende listar todo el contenido posible para cada consola u ordenador (disquetes, cartuchos y CDs, así como revistas, manuales y vídeos - Atari-ST, Amiga, Commodore, Dreamcast, Master-System, N64, etc).

TOSEC es un sistema de catalogación y denominación. Su misión es identificar y nombrar todas las ROMs encontradas en la naturaleza.

A primera vista parece una tarea loable, pero al final se convierte en algo extremadamente caótico.

Tienes ROMs buenas y malas, archivos defectuosos, archivos de copia de seguridad adicionales, archivos de sustitución alternativos, ROMs pirateadas, falsificaciones, homebrews, ROMs modificadas, etc...

Esto significa que puedes tener 27 versiones diferentes del mismo juego, de las cuales sólo 3 son roms que funcionan correctamente.

Por ejemplo, el conjunto _TOSEC 2012-12-28_ contiene 346 versiones diferentes de Super Mario World...

>TOSEC es **excelente para archivar**, pero es inutilizable para uso personal. Por eso te aconsejamos que sólo utilices un conjunto Tosec si no hay alternativa en No-Intro o Redump, y que te tomes el tiempo necesario para ordenarlo cuidadosamente.
{.is-warning}

### Los archivos dat

Podéis descargar estos ficheros desde este [sitio](https://www.tosecdev.org/).

## Trurip (⭐⭐)

TruRip se centra exclusivamente en **juegos basados en CD**.

La misión de TruRip es tener el duplicado más exacto del soporte original, aunque esta afirmación pueda sonar muy similar a los objetivos de No-Intro y Redump, no lo es.

Es otro nivel, lo que significa que no hay cabeceras ni ajustes para garantizar que los juegos funcionen correctamente en los emuladores.

TruRip no puede hacer una copia exacta en todos los casos, a veces faltan cosas como el subcódigo. Es sin duda lo más purista de todo en este momento.

TruRip intentó ser un grupo privado y fracasó. Es difícil conseguir información sobre él en Internet.

>Estos grupos tienen muchos juegos desaparecidos en algunas plataformas.
{.is-info}

### Dat files

Podéis descargar estos ficheros desde este [sitio](http://database.trurip.org/).

## Goodsets/Goodtools (⭐⭐)

>GoodSets se centra únicamente en juegos basados en determinados sistemas de cartuchos.
{.is-info}

El propósito de GoodSets/GoodTools es muy similar a TOSEC pero ligeramente más refinado ya que son sólo un sistema de catalogación / nomenclatura y no un sistema de depuración como No-Intro, Redump y TruRip.

Los sets de GoodTools pretenden ser el set ideal "de ahí sus nombres", intentan tener todos los juegos y todas las versiones del juego.

Como resultado, tendrás 20 versiones de Super Mario (USA, JAP, EUR, USA versión pirata V1, JAP Alternate V1, etc) para que cada juego tenga todas las versiones de las roms.

En general, identifican las roms (los paréntesis, corchetes, etc. que se añaden al nombre de la ROM, véase [aquí](./../tags-used-in-rom-names)).

>Estos conjuntos son, por tanto, una mezcla de roms de varios grupos. Pero no necesariamente las mejores (para aquellos que deseen utilizar la función Netplay).
{.is-info}

### Los archivos dat

Podéis descargar estos ficheros desde [este sitio](https://cowering.blogspot.com/).
