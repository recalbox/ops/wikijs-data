---
title: Almacenamiento
description: 
published: true
date: 2025-02-02T22:07:18.457Z
tags: compatibilidad, almacenamiento
editor: markdown
dateCreated: 2023-09-19T08:29:37.276Z
---

### TARJETAS SD

La mayoría de las tarjetas SD funcionarán en el Raspberry Pi. Si la tarjeta SD posee más de 32GB, consultad [este enlace](https://www.raspberrypi.org/documentation/installation/sdxc_formatting.md).

¡Tened cuidado con las falsificaciones (sobre todo en Aliexpress, Ebay, Wish y LeBonCoin y a veces en Amazon)!

| Periférico | Estado | Última versión probada | Tamaño real |
| :---: | :---: | :---: | :---: |
| **Adata** |  |  |  |
| Adata Premier 32GB microSDHC Card (UHS-I Class 10) | ❌ |  |  |
| **Energizer** |  |  |  |
| Energizer 8GB Micro Sd Card Class 10 UHS-I | ✅ |  |  |
| **Kingston** |  |  |  |
| Kingston SDC10/32GBSP Micro SD SDHC/SDXC Class 10 UHS-I 32Go | ❌ |  |  |
| Kingston MicroSD (Class 4) - 8GB | ✅ | Recalbox 4/RPi 2B | 7,21GB / 4GB libres para los juegos |
| Kingston SDC10/16GB Micro SD HC | ❌ |  |  |
| Kingston SDC10G2/16GB | ✅ | Recalbox 4.0.2/RPi 3B | 16GB / 11GB libres para los juegos |
| Kingston SDCS/64GBSP Micro SD SDXC C10 UHS-I 64Go | ✅ | Rasp. Pi3 B - Recalbox 18.07.13 | 58 GB libres para los juegos |
| **LD** |  |  |  |
| LD MicroSD1 XC I / 64GB | ✅ | Recalbox 18.06.27/RPi 3B | 64GB |
| **Lexar** |  |  |  |
| Lexar 633x UHC-i 16 GB | ❌ |  |  |
| **Netac** |  |  |  |
| Netac 32 GB | ❌ |  |  |
| **Philips** |  |  |  |
| Philips Class10 microSDHC Card 32GB | ✅ |  |  |
| **PNY** |  |  |  |
| PNY Elite microSDXC CARD 64GB Class-10 UHS-I | ✅ | v18.04.20 | 56Gb libres para los juegos |
| PNY Elite microSDXC CARD 128GB | ✅ | v4.0 final | 106gb libres para los juegos |
| **QUMOX** |  |  |  |
| QUMOX 64GB MICRO SD MEMORY CARD CLASS 10 UHS-I | ✅ |  |  |
| **Samsung** |  |  |  |
| Samsung plus (MB-MPAGC) (UHS-I Grade 0 Class 10) | ✅ |  |  |
| Samsung Evo (MB-MP16D - MB-MP128D) (UHS-I Grade 1 Class 10) | ✅ |  |  |
| Samsung Evo (MB-MP16DA - MB-MP128DA) (UHS-I Grade 1 Class 10) | ✅ |  |  |
| Samsung Evo (MB-MP32D - MB-MP128D) (UHS-I Grade 1 Class 10) | ✅ |  |  |
| Samsung Evo (MB-MC32GA - MB-MC256GA) (UHS-I Grade 1 Class 10) | ✅ |  |  |
| Samsung Evo Select (MB-ME32GA - MB-ME256GA) (UHS-I Grade 1 Class 10) | ✅ | Recalbox 17.12.02/RPi 3B |  |
| **SanDisk** |  |  |  |
| SanDisk Extreme Plus microSDHC | ✅ |  |  |
| SanDisk SDSQUNC MicroSDXC 128GB-GZFMA Class 10 | ✅ |  |  |
| SanDisk Ultra MicroSDXC 200GB (UHS-I Grade 1 Class 10) | ✅ | Recalbox 4.0.2/RPi 3B | 178GB libres para los juegos |
| SanDisk Ultra MicroSDHC 32GB UHS-I | ✅ | Recalbox 4.1.0/RPi 3B | 27.1GB libres para los juegos |
| SanDisk Ultra Plus 64GB UHS-1 Class 10 | ✅ |  |  |
| SanDisk Ultra Series class 10 | ✅ |  |  |
| SanDisk Ultra (GN6MA) (UHS-I Grade 1 Class 10) | ✅ |  |  |
| SanDisk Extreme Pro U3 64GB | ✅ |  |  |
| **Strontium** |  |  |  |
| Strontium 16GB MicroSDHC UHS-1 NITRO 433X Class 10 | ✅ |  |  |
| Strontium 32GB MicroSDHC UHS-1 NITRO 466X Class 10 | ✅ |  |  |
| **Toshiba** |  |  |  |
| Toshiba microSDHC UHS-1 32GB Class 10 | ✅ | Recalbox 4.1.0/RPi 3B |  |
| **Transcend** |  |  |  |
| Transcend 8GB & 32GB class 10 | ✅ |  |  |
| Transcend 32GB (TS32GUSDU1PE) UHS-1 Class 10 | ✅ |  |  |
| Transcend Premium 300x class 10 | ❌ |  |  |
| **Verbatim** |  |  |  |
| Verbatim Micro SDHC Card 32GB Class 10 | ❌ |  |  |
