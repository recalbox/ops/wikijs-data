---
title: Screensavers
description: 
published: true
date: 2021-08-08T19:03:27.227Z
tags: screensavers
editor: markdown
dateCreated: 2021-06-29T12:24:07.595Z
---

## Introduction

In EmulationStation, there is a possibility to set the screensaver to use, and some of them have detailed options in their own pages.

## Usage

* In the system list, press the `START` button and go to `INTERFACE OPTIONS` and then to `SCREEN SAVER`.
* Here you have several choices:

  * `SCREENSAVER AFTER`: allows you to define after how many minutes the screen saver will start. You can choose between 0 and 30 minutes.
  * `SCREENSAVER BEHAVIOR`: allows you to choose which screen saver to apply. Several choices are available.
  * `SYSTEMS TO USE IN DEMOS/GAMECLIPS`: Allows you to choose the systems with which the screen saver will act. Valid only for game demos and game clips.

## {.tabset}

### Demo mode

The demo mode is nothing more or less than a screensaver that runs your games in the background.

The demo mode picks from a list of systems (by default there are all 8/16 bits consoles, except for laptops), and it launches a game at random in a chosen system. It keeps an internal history to avoid running the same system twice in a row or running the same game twice.

![](/basic-usage/features/screensavers/demomode1.png)

#### Activation

To activate this feature, it's very easy:

* In EmulationStation, open the menu with the `START` button.
* Go to `UI SETTINGS` > `SCREENSAVER`.
* Here, go to `SCREENSAVER BEHAVIOR` and choose `DEMO`.

#### Configuration

You can configure the systems to use for game demos:

* In EmulationStation, open the menu with the `START` button.
* Go to `UI SETTINGS` > `SCREENSAVER`.
* Here, go to `SYSTEMS TO SHOW IN DEMOS/GAMECLIPS` and select the systems that the games will be used on.

### Game clip mode

The game clip mode will use the videos from your scraped games to display them on the screen as a screensaver.

The game clip mode draws from a list of systems (by default there are all 8/16 bit consoles, except for laptops), and it launches a random video in a chosen system. It keeps an internal history to avoid launching the same system twice in a row or launching the same video twice.

#### Activation

To activate this function, it's very easy:

* In EmulationStation, open the menu with the `START` button.
* Go to `UI SETTINGS` > `SCREENSAVER`.
* Here, go to `SCREENSAVER BEHAVIOR` and choose `GAMECLIP`.

#### Configuration

You can configure the systems to use for game clips:

* In EmulationStation, open the menu with the `START` button.
* Go to `UI SETTINGS` > `SCREENSAVER`.
* Here, go to `SYSTEMS TO SHOW IN DEMOS/GAMECLIPS` and select the systems the games will be used on.