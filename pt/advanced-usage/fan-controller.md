---
title: Controle da ventoinha
description: Gerenciamento dos coolers em RPIs
published: true
date: 2021-11-14T15:26:24.913Z
tags: fan, 8.0+
editor: markdown
dateCreated: 2021-08-17T23:50:31.425Z
---

# Descrição

O utilitário **recalbox-wpaf** permite controlar certas placas (*HAT - Hardware Attached on Top*) que possuem uma ventoinha.  
A lista de placas suportadas pelo recalbox-wpaf está disponível [aqui](./../hardware-compatibility/compatible-devices/rpi-hats).

Nota: esta utilidade ainda está em beta e estará disponível com o Recalbox 8.0.

## Configuração

A configuração é feita no arquivo `recalbox.conf` sob os comandos `hat.wpaf.enabled` e `hat.wpaf.board`.

### `hat.wpaf.enabled`

Defina este comando para `1` para habilitar o utilitário recalbox-wpaf.

### `hat.wpaf.board`

Este comando permite selecionar a placa que alimenta seu Raspberry PI. Os valores possíveis são:

* `wspoehatb` para a placa Waveshare PoE hat (b);
* `argonforty` para a placa que alimenta a case Argon One;
* `piboy` para a case Experimental PI;
* `rpipoeplus` para a placa Raspberry PI PoE+;
* `fanshim` para a placa Pimoroni fan SHIM.

Exemplo:

```ini
hat.wpaf.enabled=1
hat.wpaf.board=rpipoeplus
```

Reinicie para que as configurações sejam consideradas.