---
title: Libretro PicoDrive
description: 
published: true
date: 2024-07-24T09:05:19.125Z
tags: libretro, 32x, picodrive
editor: markdown
dateCreated: 2021-07-30T11:04:17.626Z
---

**Libretro PicoDrive** is an open-source Sega [8](./../master-system)/[16](./../megadrive) bit and [32X](./../32x) emulator. It has been thought for portable devices on [ARM](https://en.wikipedia.org/wiki/ARM_architecture) architecture.

## ![](/emulators/license.svg) License

This core is under [**non-commercial**](https://github.com/libretro/picodrive/blob/master/COPYING) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Features

| Feature | Supported |
| :---: | :---: |
| Restart | ✔ |
| Saves | ✔ |
| States | ✔ |
| Rewind | ✔ |
| Netplay | ✔ |
| Core Options | ✔ |
| RetroAchievements | ✔ |
| RetroArch Cheats | ✔ |
| Controls | ✔ |
| Remapping | ✔ |
| Disk Control | ✔ |

## ![](/emulators/bios.svg) BIOS

>**No bios is required.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Supported extensions

Roms must have the extension:

* .32x
* .bin
* .smd
* .zip
* .7z

This system supports compressed roms in .zip/.7z format. However, be careful, it is only an archive.

The files contained in the .zip/.7z must correspond to the extensions mentioned above.
Moreover, each .zip/.7z file must contain only one compressed rom.

### Location

Put the roms like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 **sega32x**
┃ ┃ ┃ ┃ ┣ 🗒 **game.zip**

>The roms using **No-Intro** format are highly recommended.
{.is-success}

>For more information about roms, go to [this tutorial](./../../../tutorials/games/generalities/isos-and-roms)!
{.is-info}

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Accessing to options

You can configure various options in two different ways.

* Via RetroArch menu:

┣ 📁 RetroArch Menu
┃ ┣ 📁 Core options
┃ ┃ ┣ 🧩 Name_option

* Via the file `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core options

| Options | Options values | Variables (for configuration override) | Variables values |
| --- | --- | --- | --- |
| Region | `Auto` ✅ / `Japan NTSC` / `Japan PAL` / `US` / `Europe` | `picodrive_region` | `Auto` / `Japan NTSC` / `Japan PAL` / `US` / `Europe` |
| Master System Type | `Auto` ✅ / `Game Gear` / `Master System` | `picodrive_smstype` | `Auto` / `Game Gear` / `Master System` |
| Master System ROM Mapping | `Auto` ✅ / `Sega` / `Codemasters` / `Korea` / `Korea MSX` / `Korea X-in-1` / `Korea 4-Pak` / `Korea Janggun` / `Korea Nemesis` | `picodrive_smsmapper` | `Auto` / `Sega` / `Codemasters` / `Korea` / `Korea MSX` / `Korea X-in-1` / `Korea 4-Pak` / `Korea Janggun` / `Korea Nemesis` |
| Sega CD RAM cart | `Disabled` ✅ / `Enabled` | `picodrive_ramcart` | `disabled` / `enabled` |
| Core-provided aspect ratio | `PAR` ✅ / `4/3` / `CRT` | `picodrive_aspect` | `PAR` / `4/3` / `CRT` |
| LCD Ghosting Filter | `Disabled` ✅ / `Weak` / `Normal` | `picodrive_ggghost` | `off` / `weak` / `normal` |
| Renderer | `Accurate` ✅ / `Good` / `Fast` | `picodrive_renderer` | `accurate` / `good` / `fast` |
| Sample Rate (Hz) | `16000` / `22050` / `32000` / `44100` ✅ | `picodrive_sound_rate` | `16000` / `22050` / `32000` / `44100` |
| FM filtering | `Disabled` ✅ / `Enabled` | `picodrive_fm_filter` | `off` / `on` |
| Master System FM Sound Unit | `Disabled` ✅ / `Enabled` | `picodrive_smsfm` | `off` / `on` |
| Mega Drive FM DAC noise | `Disabled` ✅ / `Enabled` | `picodrive_dacnoise` | `off` / `on` |
| Audio filter | `Disabled` ✅ / `Low-pass` | `picodrive_audio_filter` | `disabled` / `low-pass` |
| Low-pass filter % | `5` / `10` / `15` / `20` / `25` / `30` / `35` / `40` / `45` / `50` / `55` / `60` ✅ / `65` / `70` / `75` / `80` / `85` / `90` / `95` | `picodrive_lowpass_range` | `5` / `10` / `15` / `20` / `25` / `30` / `35` / `40` / `45` / `50` / `55` / `60` / `65` / `70` / `75` / `80` / `85` / `90` / `95` |
| Input Device 1 | `3 Button Pad` ✅ / `6 Button Pad` / `None` | `picodrive_input1` | `3 Button Pad` / `6 Button Pad` / `None` |
| Input Device 2 | `3 Button Pad` ✅ / `6 Button Pad` / `None` | `picodrive_input2` | `3 Button Pad` / `6 Button Pad` / `None` |
| Dynamic Recompilers | `Enabled` ✅ / `Disabled` | `picodrive_drc` | `enabled` / `disabled` |
| Frameskip | `Disabled` ✅ / `Auto` / `Manual` | `picodrive_frameskip` | `disabled` / `auto` / `manual` |
| Frameskip Threshold (%) | `15` / `18` / `21` / `24` / `27` / `30` / `33` ✅ / `36` / `39` / `42` / `45` / `48` / `51` / `54` / `57` / `60` | `picodrive_frameskip_threshold` | `15` / `18` / `21` / `24` / `27` / `30` / `33` / `36` / `39` / `42` / `45` / `48` / `51` / `54` / `57` / `60` |
| No Sprite Limit | `Disabled` ✅ / `Enabled` | `picodrive_sprlim` | `disabled` / `enabled` |
| 68K Overclock | `Disabled` ✅ / `+25%` / `+50%` / `+75%` / `+100%` / `+200%` / `+400%` | `picodrive_overclk68k` | `Disabled` / `+25%` / `+50%` / `+75%` / `+100%` / `+200%` / `+400%` |

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/libretro/picodrive/](https://github.com/libretro/picodrive/)
* **Libretro documentation**: [https://docs.libretro.com/library/picodrive/](https://docs.libretro.com/library/picodrive/)