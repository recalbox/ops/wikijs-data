---
title: Configurad vuestro CRT en Raspberry Pi 4 / 400 / 3 / Zero2
description: Cómo jugar con Recalbox en vuestras pantallas de rayos catódicos en el Raspberry Pi 4.
published: true
date: 2023-09-09T21:11:44.327Z
tags: video, crt, screen, cathodique, pixel perfect, trinitron, pvm, 240p, 224p, 15khz
editor: markdown
dateCreated: 2023-09-09T21:09:42.381Z
---

## Presentación

Como buen usuario de Recalbox, ya sabéis hasta qué punto es genial volver a sentir la nostalgia de nuestra infancia jugando a los juegos que nos han marcado cuando éramos pequeños.

Jugar a los juegos retro con un mando PS4 en nuestra TV OLED del salón nos permite compartir el placer, disfrutar y divertirse con dichos juegos en familia.

No obstante, algunos de nosotros estamos sedientos y necesitamos todavía más para acercarnos a la verdadera experiencia original de aquella época.

En esta búsqueda sin fín, la imagen en la pantalla es sin duda el punto más importante.

¿Y que hay más cercano a la experienca original que jugar directamente en las pantallas CRT de aquella época?

## Recalbox en vuestra pantalla CRT

Recalbox es capaz de reproducir la resolución y las frecuencias exactas de la mayoría de las consolas y juegos de arcade de la época en las pantallas de rayo catódicos (CRTs).

Equiparos de un [**Recalbox RGB DUAL**](https://www.recalbox.com/recalbox-rgb-dual), de un VGA666 + un câble scart (peritel), de un Pi2Scart o de un RGBPi.

Recalbox RGB Dual se configura automáticamente. Para los otros hardwares, una vez enchufado el periférico en cuestión, editad el fichero de configuración `/crt/recalbox-crt-options.cfg` que se encuentra en la partición `[RECALBOX]` cuando insertáis la tarjeta sd en vuestro ordinador como sigue:

```
# Para el recalboxrgbdual no hay nada que hacer aquí !
# Para vga666
adapter.type = vga666
# Para pi2scart
adapter.type = pi2scart
# Para rgbpi
adapter.type = rgbpi
```

También podéis utilizar el menú de EmulationStation "Configuración avanzada => Recalbox CRT".

**Esto es todo**, depués de reiniciar Recalbox, el sistema se configura correctamente para enviar la imagen directamente a vuestro CRT.

------------------

## Compatibilidad hardware

* **Recalbox RGB DUAL**: TV CRT, PVM, VGA Monitor
El Recalbox RGB Dual es **la solución plug and play para jugar a Recalbox en los CRT**. Además, se trata de una place madre desarrollada por el equipo de Recalbox.

* **VGA666**: TV CRT con cable modificado, Pantalla CRT VGA, PVM
El VGA666 os permite enchufar un cable VGA (de los antiguos monitores de rayos catódicos). Con un cable VGA->SCART(PERITEL) modificado, se trata de otra solución valable para usar en las pantallas CRT.
* **Pi2Scart**: TV CRT, PVM
Un H.A.T para Raspberry Pi 3 et 4.
* **RGBPi**: TV CRT, PVM
Un cable SCART(peritel) para enchufar Recalbox a vuestra pantalla CRT.

## Configuración intermedia

Dentro del fichero de configuración `/crt/recalbox-crt-options.cfg` que se encuentra en la partición `[RECALBOX]`:
```
# Desplazamiento de la imagen hacia la izquierda (negativo) o hacia la derecha (positivo)
mode.offset.horizontal = -2
# Desplazamiento de la imagen hacia arriba (negativo) o hacia abajo (positivo)
mode.offset.vertical = -2
```

## Configuración avanzada

La configuración por defecto de los diferentes modos de pantalla por sistema y por juego arcade se encuentra en el directorio `/recalbox/system/configs/crt/`

* `modes.txt` lista los posibles modos de visualización disponibles
* `system.txt` define para cada sistema el modo a utilizar
* `arcade_games.txt` define para cada juego arcade el modo a utilizar

Para sobrecargar o reemplazar estas configuraciones, basta con crear el fichero correspondiente dentro de la carpeta `/recalbox/share/system/configs/crt/`.

Estos ficheros de sobrecarga sólo deben contener los modos, sistemas o juegos que queréis sobrecargar o añadir.

Estructura de los ficheros:

┣ 📁 recalbox
┃ ┣ 📁 system
┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┣ 📁 crt
┃ ┃ ┃ ┃ ┣ 🗒 arcade_games.txt (sólo lectura)
┃ ┃ ┃ ┃ ┣ 🗒 modes.txt (sólo lectura)
┃ ┃ ┃ ┃ ┣ 🗒 systems.txt (sólo lectura)
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 crt
┃ ┃ ┃ ┃ ┃ ┣ 🗒 arcade_games.txt
┃ ┃ ┃ ┃ ┃ ┣ 🗒 modes.txt
┃ ┃ ┃ ┃ ┃ ┣ 🗒 systems.txt


Ver [un ejemplo de sobrecarga](#exemple-de-surcharge)

### Estructura de los ficheros

#### `modes.txt`

El fichero `modes.txt` contiene un modo por línea, con su identificador, con el siguiente format:
```ini
MODE_ID,MODE,REFRESH_RATE
```

Ejemplo:

```ini
snes:nes:ntsc:224@60.0988,1920 1 80 184 312 224 1 9 3 24 0 0 0 60 0 39001717 1,60.0988
```

Aquí el modo MODE_ID es `snes:nes:ntsc:224@60.0988` y el MODE es `1920 1 80 184 312 224 1 9 3 24 0 0 0 60 0 39001717 1`

#### `systems.txt`

El fichero `systems.txt` permite, para cada sitema, configurar un modo de visualización por cada región, y selccionar el tamaño de la zona geográfica:

```ini
SYSTEM_ID,REGION,SCREEN_TYPE,INTERLACED,MODE_ID,VIEWPORT_WIDTH,VIEWPORT_HEIGHT
```

Ejemplo:

```ini
vectrex,all,15kHz,progressive,standard:ntsc:240@60,0,0
```

Esta línea permite a Recalbox saber que la Super Nintendo (`snes`) debe utilizar el modo identificado por `snes:nes:ntsc:224@60.0988` cuando intentará cargar un juego NTSC (no europeo) en una TV de 15kHz (dans `modes.txt`).

Los dos ceros al final permiten a Recalbox definir el mejor valor para la altura y el ancho de la imagen.

#### `arcade_games.txt`

El fichero `arcade_games.txt` selecciona el modo de visualización para cada juego arcade, a partir del core (o emulador) que lo lanzará. Permite también modificar la el tamaño de la zona de presentación de la imagen:

```ini
GAME_ID,CORE_ID,MODE_ID,VIEWPORT_WIDTH,VIEWPORT_HEIGHT,ROTATION
```

Ejemplo:

```ini
dino,fbneo,snes:nes:ntsc:224@60.0988,0,0
```

Los GAME_ID son los nombres de los ficheros de juegos, sin la extensión `.zip`.
El CORE_ID es el nombre del core utilizado por Recalbox.

Esta línea permite a Recalbox saber que el juego `dino.zip`, cuando se lanza en RetroArch co el core `fbneo`, necesita utilizar el modo `arcade:224@59.637405`.

### Ejemplo de sobrecarga

La sobrecarga permite añadir o reemplazar modos, sistemas o juegos de arcade.

> Todas las sobrecargas se aplican utilizando nuevos ficheros, situados dentro de `/recalbox/share/system/configs/crt/`.
> {.is-info}

Cada uno de los modos, sistemas o juegos que aparecen en las sobrecargas se añaden a la configuración de base, o la reemplazan si el `id` ya existía.

Imaginemos que quiero cambiar el modo de visualización de `Cadillac and Dinosaurs` en arcade y que lo quiero pasar a 224 líneas en 60Hz.

En el fichero modes.txt, existe un modo que corresponde a  224 líneas a 60Hz. Su MODE_ID es `standard:all:240@60`

Sólo tengo que crear el fichero `/recalbox/share/system/configs/crt/arcade_games.txt` y escribir una única línea dentro:

```ini
dino,fbneo,standard:all:240@60,0,0,0
```

Los VIEWPORT_WIDTH y VIEWPORT_HEIGHT permiten redimensionar la imagen. No cambian el modo de visualización de la pantalla. 

Puede que os pueda interesar añadir configuraciónes específicas para determinados sistemas o juegos de arcade.

Definiremos de una vez por todas el ancho de la pantalla de `Metal Slug 3` para que sea de 1800 pixels dentro del fichero `/recalbox/share/system/configs/crt/arcade_games.txt`

```ini
mslug3,fbneo,arcade:224@59.185606,1800,0,0
```

Relanzamos `Metal Slug 3` y podréis observar que el ancho de la pantalla se ha hecho más pequeño.