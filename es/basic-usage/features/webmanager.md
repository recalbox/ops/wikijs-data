---
title: Gestor web
description: 
published: true
date: 2024-11-17T14:57:46.457Z
tags: web, gestor
editor: markdown
dateCreated: 2023-08-22T22:04:20.214Z
---

## Para acceder

Utilizando un navegador web, basta con escribir la siguiente dirección:

## {.tabset}

### Windows

[http://recalbox/](http://recalbox/)

Si este nombre no funciona, podéis también acceder utilizando [la dirección IP de vuestra Recalbox](./../../tutorials/network/ip/discover-recalbox-ip).

### Linux y macOS

[http://recalbox.local/](http://recalbox.local/)

Si este nombre no funciona, podéis también acceder utilziando [la dirección IP de vuestra Recalbox](./../../tutorials/network/ip/discover-recalbox-ip).

## Interfaz

En la página principal, podéis realizar las siguientes acciones:

* Modificar las diferentes opciones del fichero `recalbox.conf`.
* Añadir/borrar roms.
* Crear un archivo de soporte (debug).
* Vigilar el estado de vuestra máquina - temperatura, utilización de la CPU y del almacenamiento (monitoring).
* Consultar desde la página de inicio el sistema en ejecución y, si procede, el juego en curso de ejecución.
* Manipular vuestras capturas (capturas de pantalla o vídeos).
* Gestionar vuestra clave de acceso Patreon y las opciones asociadas.

## Desactivación del gestor web

Por defecto el gestor web se encuentra activado. Si queréis desactivarlo, basta con seguir los siguientes pasos:

* Dentro de EmulationStation, pulsado el botón `START`.
* Id dentro de `PARAMETROS AVANZADOS`.
* Cambiad la opción `GESTOR WEB RECALBOX` a OFF.

## Mando de juegos virtual

Para acceder directamente, podéis utilizar la dirección IP de vuestra Recalbox junto con el puerto 8080.

```text
http://192.168.0.X:8080
```

```text
http://recalbox:8080
```

Consultad el manual de utilización del mando de juegos virtual [aquí](./../../basic-usage/first-use-and-configuration#manettes-virtuelles)

Si os encontráis con un problema o bug, no dudéis en hacérnoslo saber [aquí](https://github.com/recalbox/recalbox-manager).