---
title: 8Bitdo no Recalbox
description: Lista completa de controles 8bitdo já testados
published: true
date: 2022-05-31T22:09:47.823Z
tags: joystick, 8bitdo, controle, compatibilidade
editor: markdown
dateCreated: 2021-11-28T14:45:20.603Z
---

![8bitdo](http://support.8bitdo.com/images/Logo-black.svg)

![Lista dos controles 8Bitdo em 13 de novembro de 2019](/compatibility/8bitdocollection.png){.full-width}

## Controles 8Bitdo suportados

### Controles estilo SNES :

| Controle | FW (min/rec*) | USB | BT | Como ligar | Associação BT* | Resetar BT | Restauração de fábrica |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| SN30 Pro+ | 4.01 / 5.04 | ✅ | ☑️ | START + B | Mantenha pressionado o botão PAIR por 2 segundos | Mantenha pressionado o botão PAIR por 2 segundos | [Veja o manual no site do 8Bitdo](https://download.8bitdo.com/Manual/Controller/SN30-Pro+/SN30_Pro+_Manual.pdf) |
| SN30 Pro v2.0 | ? / 1.32 | ✅ | ☑️ | MODE D (Android) | Mantenha pressionado o botão PAIR por 3 segundos | MMantenha pressionado o botão Select por 3 segundos | [Veja o manual no site do 8Bitdo](https://download.8bitdo.com/Manual/Controller/SN30pro+SF30pro/SN30pro_Manual.pdf) |
| SN30 & SF30 Pro | 1.23 / 1.33 | ✅ | ☑️ | START + B | Mantenha pressionado o botão PAIR por 3 segundos | Mantenha pressionado o botão Select por 3 segundos | [Mantenha pressionado o botão Select por 8 segundos](https://download.8bitdo.com/Manual/Controller/SN30pro+SF30pro/SN30pro+SF30pro_Manual.pdf) |
| SN30 Pro USB | ? / 1.03 | ? | n/a | n/a | n/a| n/a | [Veja o manual no site do 8Bitdo](https://download.8bitdo.com/Manual/Controller/SN30pro-USB/SN30-Pro-USB.pdf) |
| SN30 GP v2.0 | 6.11 / 6.16 | ? | ? | B + START | Mantenha pressionado o botão Select por 3 segundos | Mantenha pressionado o botão Select por 3 segundos | [Veja o manual no site do 8Bitdo](https://download.8bitdo.com/Manual/Controller/SN30+SF30/SN30_Manual.pdf) |
| SN30 Retro Set | ? / 4.10 | ? | ? | START | Mantenha pressionado o botão Select por 3 segundos | Mantenha pressionado o botão Select por 3 segundos | [Veja o manual no site do 8Bitdo](https://download.8bitdo.com/Manual/Controller/SN30+SF30/SN30_Analogue.pdf) |
| SN30 | 2.62 / 4.20 | ✅ | ☑️ | START + R | Mantenha pressionado o botão Select por 3 segundos | Mantenha pressionado o botão Select por 3 segundos | [Mantenha pressionado o botão Select por 8 segundos](https://download.8bitdo.com/Manual/Controller/SN30+SF30/SN30_Analogue.pdf) |
| SF30 &amp; SFC30 | 2.70 / 4.20 | ✅ | ☑️ | START + R | Mantenha pressionado o botão Select por 3 segundos | Mantenha pressionado o botão Select por 3 segundos | [Mantenha pressionado o botão Select por 8 segundos](https://download.8bitdo.com/Manual/Controller/SN30+SF30/SN30+SF30_Manual_V4.pdf) |

### Controles estilo SNESxNES :

| Controle | FW (min/rec*) | USB | BT | Como ligar | Associação BT* | Resetar BT | Restauração de fábrica |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| N30 Pro 2 | ? / 6.10 | ? | ? | START | Mantenha pressionado o botão Pair por 1 segundo | Mantenha pressionado o botão Pair por 3 segundos | [Veja o manual no site do 8Bitdo](https://download.8bitdo.com/Manual/Controller/N30pro2/N30pro2-Manual.pdf) |
| N30 &amp; NES30 Pro | 1.62 / 4.10 | ✅ | ☑️ | POWER | Mantenha pressionado o botão Pair por 1 segundo | Mantenha pressionado o botão Pair por 3 segundos | [Mantenha pressionado o botão Start por 8 segundos](https://download.8bitdo.com/Manual/Controller/N30pro+F30pro/N30pro+F30pro_Manual_V4.pdf) |
| F30 &amp; FC30 Pro | 1.62 / 4.10 | ✅ | ☑️ | POWER + X | Mantenha pressionado o botão Pair por 1 segundo | Mantenha pressionado o botão Pair por 3 segundos | [Mantenha pressionado o botão Start por 8 segundos](https://download.8bitdo.com/Manual/Controller/N30pro+F30pro/N30pro+F30pro_Manual_V4.pdf) |

### Controles estilo NES :

| Controle | FW (min/rec*) | USB | BT | Como ligar | Associação BT* | Resetar BT | Restauração de fábrica |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| N30 NS | ? / 6.10 | ? | ? | START | Mantenha pressionado o botão Select por 3 segundos | Mantenha pressionado o botão Select por 3 segundos | [Veja o manual no site do 8Bitdo](https://download.8bitdo.com/Manual/Controller/N30-NS/N30-NS.pdf) |
| N30 &amp; NES30 | 2.62 / 4.20 | ✅ | ☑️ | START + R | Mantenha pressionado o botão Select por 3 segundos | Mantenha pressionado o botão Select por 3 segundos | [Mantenha pressionado o botão Select por 8 segundos](https://download.8bitdo.com/Manual/Controller/N30+F30/N30+F30_Manual_V4.pdf) |
| F30 &amp; FC30 | 2.62 / 4.20 | ✅ | ☑️ | START + R | Mantenha pressionado o botão Select por 3 segundos | Mantenha pressionado o botão Select por 3 segundos | [Mantenha pressionado o botão Select por 8 segundos](https://download.8bitdo.com/Manual/Controller/N30+F30/N30+F30_Manual_V4.pdf) |

### Controles estilo MEGADRIVE :

| Controle | FW (min/rec*) | USB | BT | Como ligar | Associação BT* | Resetar BT | Restauração de fábrica |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| M30 | 1.13 / 1.14 | ✅ | ☑️ | START | MMantenha pressionado o botão Select por 1 segundo | Mantenha pressionado o botão Select por 3 segundos | [Veja o manual no site do 8Bitdo](https://download.8bitdo.com/Manual/Controller/M30/M30_Manual.pdf) |

### Controles estilo ARCADE :

| Controle | FW (min/rec*) | USB | BT | Como ligar | Associação BT* | Resetar BT | Restauração de fábrica |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| N30 &amp; NES30 Arcade Stick | 1.41 / 5.01 | ✅ | ☑️ | HOME | Mantenha pressionado o botão Select por 3 segundos | Mantenha pressionado o botão Select por 3 segundos | [Veja o manual no site do 8Bitdo](https://download.8bitdo.com/Manual/Controller/Joystick/N30-Arcade-Manual.pdf) |
| F30 & FC30 Arcade Stick | 1.42 | ✅ | ☑️ | HOME | Mantenha pressionado o botão Select por 3 segundos | Mantenha pressionado o botão Select por 3 segundos | [Veja o manual no site do 8Bitdo](https://download.8bitdo.com/Manual/Controller/Joystick/F30-ArcadeJoystick-eng.pdf) |

### Outros estilos de controleAutres styles de manettes :

| Controle | FW (min/rec*) | USB | BT | Como ligar | Associação BT* | Resetar BT | Restauração de fábrica |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| Lite | ?? / 1.20 | ❌ | ❌ | HOME | Mantenha pressionado o botão Pair por 2 segundos | Mantenha pressionado o botão Pair por 1 segundo | [Veja o manual no site do 8Bitdo](https://download.8bitdo.com/Manual/Controller/Lite/Lite_Manual.pdf) |
| Zero | n/a | ❌ | ☑️ | START + R1 | Mantenha pressionado o botão Select por 3 segundos | Mantenha pressionado o botão Select por 3 segundos | [Mantenha pressionado o botão Select por 8 segundos](https://download.8bitdo.com/Manual/Controller/ZERO/ZERO_Manual_V4.pdf) |
| Zero 2 | 1.05 / 1.06 | ✅ | ☑️ | B + START | Mantenha pressionado o botão Select por 3 segundos | Mantenha pressionado o botão Select por 3 segundos | [Mantenha pressionado o botão Select por 8 segundos](https://download.8bitdo.com/Manual/Controller/Zero2/Zero2_Manual.pdf) |

* FW = Firmware
* min = Mínimo
* rec = Recomendado
* BT = Bluetooth

## Adaptadores do tipo Dongle 8Bitdo Suportados

| Dongle | FW (min / rec*) | Compatibilidade com Recalbox | Controles compatíveis |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| USB Adapter for PS Classic (in X-Input Mode) | 1.28 / 1.33 | ✅ | PS3, PS4, Xbox One S, Wii Mote, Wii U Pro, Switch Joycons, Switch Pro, M30, SFC30, SF30 Pro. |
| USB Adapter Receiver (in X-Input Mode) | 1.28 / 2.00 | ✅ | PS3, PS4, PS5, Xbox One S, Xbox One X, Wii Mote, Wii U Pro, Switch Joycons, Switch Pro, M30, SFC30, SF30 Pro. |
| GBros Adapter Emeter (With 8Bitdo Receiver) | 2.23 / 2.24 | ✅ | GameCube, GameCube Wavebrid (avec le câble d'extension), Wii Classic, Wii Classic Pro, NES Mini, SNES Mini. |
| NES & SNES Mini Receiver | ? / 1.14 | ? | ? |
| SNES Receiver | ? / 1.34 | ? | ? |

## Suporte 8Bitdo

* [Suporte dos controles 8Bitdo](https://support.8bitdo.com/)
* [Upgrade tool (Ferramenta de atualização)](https://support.8bitdo.com/firmware-updater.html) 

Os controles 8Bitdo não precisam mais ser desemparelhados antes de emparelhá-los novamente. Se você tiver um problema de emparelhamento, pode tentar o método “Resetar Bluetooth” e, se não funcionar, pode tentar o método “Restauração de fábrica”.

Você pode encontrar o manual para cada um de seus controles no site de [suporte 8Bitdo](https://support.8bitdo.com/).

## FAQ - Perguntas Frequentes

### Como carregar seu controle?

De preferência, use a porta USB do computador ou console para carregar o controle.
Se estiver usando um carregador convencional ou um carregador de telefone, certifique-se de que nunca exceda 5V 1A.
Uma intensidade muito alta pode danificar a bateria e inutilizar o Bluetooth do controle.

### Como atualizar o Firmware?

Basta ir ao site de [suporte 8Bitdo](https://support.8bitdo.com/) e baixar o software `Upgrade tool` (Ferramenta de atualização) no topo da página.

1. Instale o aplicativo baixado `8BitDo Firmware Upgrader`.
2. Abra o aplicativo
3. Assim que o aplicativo for aberto, será solicitado que você conecte o controle ao PC.
4. Seu controle será reconhecido automaticamente. Clique no botão `Firmware Update` para acessar as atualizações disponíveis para o seu controle.
5. Selecione o firmware desejado e clique no botão azul `Update`.
6. Siga o procedimento fornecido para atualizar. Isso terá uma barra de progresso vermelha.
7. Quando a atualização for bem-sucedida, clique no botão verde `Sucess` e você pode desconectar seu controle.

### Como ligo / conecto meu controle corretamente?

Dependendo do dispositivo (Windows, Mac OS, Android, Switch, etc.), existem diferentes combinações de início:

* `START` + `Y` = Alternar modo
* `START` + `A` = Modo macOS
* `START` + `X` = Modo PC
* `START` + `B` = Modo Bluetooth (geralmente o melhor para Android)

Você deve segurar as teclas simultaneamente até que o controle ligue e faça a associação.

No caso de conexão via USB, segure as teclas simultaneamente até que o controle ligue e, em seguida, conecte o cabo USB.

### Como faço para conectar meu controle a outro dispositivo?

O controle lembra apenas um endereço de dispositivo externo. Se você quiser mudar de Switch para Android, por exemplo, precisará redefinir os dados de associação do controle. Dependendo do modelo, o procedimento difere um pouco. Consulte a tabela acima para verificar o procedimento certo.

### Como posso emparelhar meu adaptador USB com meu controle PS3?

Existe uma ferramenta para gerenciar seu controle PS3 e adaptadores USB.

### {.tabset}
#### Windows

Você pode baixar este [utilitário](https://download.8bitdo.com/Tools/Receiver/8Bitdo_PS3_Tool_Win_V1.1.zip)

Se você tiver um problema com o Windows, existem 2 soluções:

**Solução 1**:
Instale o SixaxisPairToolSetup-0.3.1.exe. Você pode baixá-lo [nesta página](https://sixaxispairtool.en.lo4d.com/download).

**Solução 2**:
Desative temporariamente o aprimoramento de assinatura de driver do Windows. Você encontrará imagens de ajuda na pasta `How To Temporarily Disable Driver Signature Enforcement On Win8 And Win10` (Como desativar temporariamente a aplicação de assinatura de driver no Win8 e Win10), contida no arquivo `8Bitdo_PS3_Tool_Win_V1.1.zip`.

No caso de outros problemas, você pode instalar o driver 'libusb-win32'.

> A função de aprimoramento da assinatura dos drivers do Windows pode ser reativada durante uma atualização ou outra.
{.is-info}

#### Mac OS
Você pode baixar este [utilitário](https://download.8bitdo.com/Tools/Receiver/8BitDo_PS3_Tool_MacOS_V1.1.zip)

### E quanto aos controles Arcade?

Dependendo do dispositivo (Windows, Mac OS, Android, Switch, etc.), existem diferentes combinações de início:

* Windows: modo XINPUT.
* Android: modo DINPUT.
* MacOS e Switch não são necessários.
  
Procedimento de início:

1. Selecione o modo XINPUT ou DINPUT, se necessário.
2. Pressione o botão PAIR para inicializar o dispositivo (o led azul piscará).
3. Configure o equipamento no seu dispositivo (Windows, macOS, Android ou Switch).
4. O LED deve ficar fixo em azul e o controle está pronto para uso.

Você deve segurar as teclas simultaneamente até que o controle ligue e, em seguida, faça a associação. No caso de conexão via USB, segure as teclas simultaneamente até que o controle ligue e, em seguida, conecte o cabo USB.

### Qual firmware é recomendado para Recalbox?

Consulte a tabela no topo da página para obter o firmware recomendado para o seu controle.

### E quanto à compatibilidade com o pad zero V1?

O pad zero v1 funciona apenas em Bluetooth. Ligue o controle pressionando os botões `START` + `R1`.

Ao usar dois pads zero V1:

- No Primeiro controle, pressione os botões `START` + `R1`.
- No Segundo controle, pressione os botões `START` + `R1` + `B`.

### Meu controle não é reconhecido com o cabo USB?

Certifique-se de usar o cabo que acompanha o controle. Caso contrário, certifique-se de ter um cabo USB de transferência de dados e não o cabo USB somente de carga. Alguns cabos USB passam apenas a corrente de carga e não o fluxo de dados.