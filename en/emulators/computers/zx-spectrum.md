---
title: ZX Spectrum
description: 
published: true
date: 2021-08-09T15:08:03.494Z
tags: sinclair, zx, spectrum, computers
editor: markdown
dateCreated: 2021-07-28T21:47:55.311Z
---

![](/emulators/computers/zxspectrum.svg){.align-center}

## Technical data

* **Manufacturer**: Sinclair Research
* **OS**: Sinclair BASIC
* **Year of release**: 1982
* **Units sold**: 5 millions
* **CPU**: Z80 @ 3.5 MHz and equivalent
* **Memory**: 16 KB / 48 KB / 128 KB
* **Media**: Cassette tape, 3-inch floppy disk on Spectrum +3

## Presentation

The **ZX Spectrum** is a small personal computer put on the market by the English computer manufacturer Sinclair Research in 1982. Based on the Zilog Z80 processor running at 3.5 MHz, the Spectrum was equipped with 16 or 48 kB of RAM (an extension was also available to go from 16 to 48 kB).

Sold for 125 GBP for the 16K model and 175 GBP for the 48K model, the Spectrum was the first consumer personal computer in the UK, similar in importance to the Commodore 64 in the US (which was also a rival of the Spectrum in Europe). A slightly modified version of the Spectrum, in a silver body with hard plastic keys, was sold in the US by Timex as the TS2068. It had an additional 8k ROM expansion, with the ZX 1 or ZX 2 interface sold separately: a cartridge port, two joystick ports and an AY-3-8912 audio microprocessor, and additional BASIC commands to control this hardware (STICK, SOUND).

In the following models, we find the ZX Spectrum +, with an improved keyboard, and the ZX Spectrum 128, with better sound and 128 kB of RAM. After Amstrad bought Sinclair Research in 1986, two more versions were released: the ZX Spectrum +2 with a cassette recorder included in the machine, and the ZX Spectrum +3 with a 3" floppy drive included.

A number of important game developers today began their careers on the ZX Spectrum, such as Ultimate Play the Game (now Rareware), Peter Molyneux (formerly Bullfrog Productions), and Shiny Entertainment.

Several clones were produced, especially in Eastern Europe (Elwro, HC85) and South America. Some remained in production until the 1990s, such as the Didaktik and the Sprinter from Peters Plus Ltd.

## Emulators

[Libretro-Fuse](libretro-fuse)
[Libretro FBNeo](libretro-fbneo)