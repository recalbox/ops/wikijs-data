---
title: 3. 📚 TUTORIALES
description: 
published: true
date: 2024-07-26T06:22:19.005Z
tags: tutoriales
editor: markdown
dateCreated: 2023-08-31T14:27:29.601Z
---

En esta parte de la documentación, encontraréis muchos tutoriales creados por el équipo de Recalbox y por la comunidad de usuarios Recalbox. No dudéis en crear el vuestro!

Aquí están las categorías disponibles:

[Red](network)
[Sistema](system)
[Vídeo](video)
[Themes](themes)
[Kodi](kodi)
[Controladores](controllers)
[Juegos](games)
[Otros](others)
[Sonido](audio)
[Solución de problemas](troubleshooting-information)
[Utilidades](utilities)
[Personalización del frontend](frontend-customization)