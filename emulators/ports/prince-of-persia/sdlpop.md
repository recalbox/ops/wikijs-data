---
title: SDLPoP
description: 
published: true
date: 2025-01-04T06:32:27.479Z
tags: 9.2+, prince of persia, sdlpop
editor: markdown
dateCreated: 2024-07-25T08:29:45.937Z
---



## ![](/emulators/license.svg) License

This core is under [**GPL-3.0**](https://github.com/NagyD/SDLPoP/blob/master/COPYING) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Features

| Feature | Supported |
| :---: | :---: |

## ![](/emulators/bios.svg) BIOS

>**No bios is required.**
{.is-success}

## ![](/emulators/roms.png) Roms

The game decompiled from the DOS version is provided.

### Location

Put the files in the following folder:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 ports
┃ ┃ ┃ ┃ ┣ 📁 Prince of Persia
┃ ┃ ┃ ┃ ┃ ┣ 📁 data

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

> **This core has no options!**
{.is-success}

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/NagyD/SDLPoP/](https://github.com/NagyD/SDLPoP/)