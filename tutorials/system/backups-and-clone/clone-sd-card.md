---
title: Clonez votre carte SD
description: 
published: true
date: 2021-08-06T09:24:59.207Z
tags: sd, clone, carte
editor: markdown
dateCreated: 2021-05-21T08:40:33.191Z
---

>Si vous souhaitez cloner vers une autre carte SD, deux cartes SD de fabricants différents, sensées être de même capacité, ne feront pas exactement la même taille. Vous risquez donc de ne pas pouvoir restaurer votre sauvegarde sur une carte SD de marque différente, sauf si celle-ci a une taille supérieure à celle de départ.
{.is-warning}

* Pour cloner votre carte SD et avoir une sauvegarde complète de votre Recalbox, vous pouvez utiliser ces logiciels :

## {.tabset}
### Windows

Logiciels :

* [Win32 Disk Imager](http://sourceforge.net/projects/win32diskimager/) avec la fonction READ.

### macOS

Logiciels :

* [ApplePi-Baker](https://www.tweaking4all.com/software/macosx-software/applepi-baker-v2/)
* [Carbon Copy Cloner](https://bombich.com/fr)

Vous pouvez aussi utiliser la commande `dd` dans un terminal :

#### Sauvegarde

* Branchez votre carte SD dans votre PC utilisant Linux.
* Déterminez quel périphérique est votre carte SD :

```shell
sudo fdisk -l
```

* Une fois dans votre dossier de sauvegarde (backup), il vous faut extraire et compresser votre carte SD avec cette commande (la lettre X doit être remplacée par la lettre déterminée à la première étape) :

```shell
sudo dd if=/dev/sdX | gzip -9 > ./recalbox-20150411-sdb.img.gz
```

#### Restauration

* Pour restaurer votre sauvegarde, branchez votre carte SD dans votre PC et utilisez cette commande (où `/dev/sdX` est votre carte SD) :

```shell
gunzip ./recalbox-20150411-sdb.img.gz | sudo dd of=/dev/sdX
```
 
Pour suivre l'avancée du processus `dd`, ouvrez un nouveau terminal et tapez la commande suivante :

```shell
watch -n 5 sudo pkill -USR1 -n -x dd
```

### Linux

Logiciels :

* GParted

Vous pouvez aussi utiliser la commande `dd` dans un terminal :

#### Sauvegarde

* Branchez votre carte SD dans votre PC utilisant Linux.
* Déterminez quel périphérique est votre carte SD :

```shell
sudo fdisk -l
```

* Une fois dans votre dossier de sauvegarde (backup), il vous faut extraire et compresser votre carte SD avec cette commande (la lettre X doit être remplacée par la lettre déterminée à la première étape) :

```shell
sudo dd if=/dev/sdX | gzip -9 > ./recalbox-20150411-sdb.img.gz
```

#### Restauration

* Pour restaurer votre sauvegarde, branchez votre carte SD dans votre PC et utilisez cette commande (où `/dev/sdX` est votre carte SD) :

```shell
gunzip ./recalbox-20150411-sdb.img.gz | sudo dd of=/dev/sdX
```
 
Pour suivre l'avancée du processus `dd`, ouvrez un nouveau terminal et tapez la commande suivante :

```shell
watch -n 5 sudo pkill -USR1 -n -x dd
```