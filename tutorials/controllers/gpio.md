---
title: GPIO
description: 
published: true
date: 2021-09-04T22:34:49.483Z
tags: gpio
editor: markdown
dateCreated: 2021-05-21T08:06:47.859Z
---

Vous trouverez ici des tutoriels pour pouvoir utiliser le port GPIO de vos Raspberry Pi de plusieurs façons différentes.

Voici les tutoriels disponibles :

[Activer le bouton GPIO pour fermer l'émulateur en appuyant sur un seul bouton](enable-gpio-button-close-emulator)
[Attribuer jusqu'à 4 fonctions sur chaque port GPIO](set-until-4-features-on-each-gpio-port)
[Brancher un monnayeur comparatif sur un Raspberry Pi](connect-a-comparative-coin-mechanism-to-a-raspberry-pi)
[Jouez avec votre manette originale en utilisant Gamecon](use-your-original-controller-with-gamecon)
[Les contrôleurs GPIO](gpio-controllers)
[Touches du clavier via GPIO](keyboard-touches-via-gpio)