---
title: Quake 2
description: 
published: true
date: 2024-07-26T08:30:57.730Z
tags: ports, quake 2, vitaquake, 8.1+
editor: markdown
dateCreated: 2023-09-04T12:47:41.609Z
---

![](/emulators/ports/nofile.svg){.align-center}

## Ficha técnica

* **Creado por :** id Software
* **Año de lanzamiento:** 1997
* **Género:** Shooter en primera persona
* **Motor:** Quake Engine
* **Última versión:** Quake Champions
* **Sistema operativo:** Windows 95/98/ME/NT 4.0/2000
* **Procesador:** Pentium 90 MHz o superior o Athlon
* **Almacenamiento:** 25 MB de espacio en disco + 45 MB para el archivo swap de Windows
* **RAM:** 16 MB mínimo con Windows; 24 MB mínimo en otros sistemas
* **Resolución:** 320x200 píxeles
* **Sonido:** Sound Blaster y AdLib o tarjeta de sonido 100% compatible

## Presentación

Un generador de agujeros negros supone una amenaza directa para la humanidad al permitir el acceso de las tropas Stroggs a la Tierra. Los líderes mundiales planean la Operación Alien Overlord, calcada de la invasión de Normandía de 1944, que implica el desembarco de un gran número de tropas en Stroggos, la capital de los Stroggs, donde se encuentra el famoso cañón Big Gun para evitar un desembarco a mayor escala (al iniciarse el juego, aparece en pantalla una orden de misión muy similar a la que se distribuyó a los soldados estadounidenses en 1944, salvo que termina con la frase: "¡Así que salid y patead culos soldados!").

Grandes naves de transporte atacan Stroggos a través de lo que parece ser un vórtice gigante, descargando numerosas pequeñas lanzaderas personales. El jugador se encuentra a bordo de una de estas lanzaderas, y en la cinemática introductoria, sobrevuela la ciudad, describe un gran círculo alrededor de la monstruosa Big Gun, antes de estrellarse en un suburbio remoto, pero aún sólidamente vigilado.

## Emuladores

[Libretro VitaQuake 2](libretro-vitaquake2)