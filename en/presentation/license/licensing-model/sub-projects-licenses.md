---
title: Licenses for associated projects
description: 
published: true
date: 2021-08-08T18:38:27.444Z
tags: license, projects
editor: markdown
dateCreated: 2021-08-06T07:50:28.007Z
---

## Software projects

Recalbox is not only composed of its main directory. We also maintain some other open-source projects included in Recalbox or part of the overall Recalbox infrastructure.

Here is an overview of the license of the main projects:

* The [EmulationStation Recalbox fork](https://gitlab.com/recalbox/recalbox-emulationstation) is under MIT license
* The Recalbox [website manager](https://gitlab.com/recalbox/recalbox-manager) is under "all rights reserved".
* The Recalbox API is licensed under the terms of the MIT license
* The Recalbox [website](https://gitlab.com/recalbox/www.recalbox.com) is under "all rights reserved".

## Creative works

We also maintain and distribute [themes](https://gitlab.com/recalbox/recalbox-themes) for Recalbox, which are released under the following licenses:

| Theme | License |
| :---: | :---: |
| recalbox-goa2 | [CreativeCommons BY-NC-ND 4.0](https://creativecommons.org/licenses/by-nc-nd/4.0/) |
| recalbox-next | [CreativeCommons BY-NC-ND 4.0](https://creativecommons.org/licenses/by-nc-nd/4.0/) |
