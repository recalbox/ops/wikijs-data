---
title: Erste Schritte
description: 
published: true
date: 2024-06-02T19:42:34.885Z
tags: 
editor: markdown
dateCreated: 2021-06-22T19:43:30.565Z
---

## Erste Schritte

### Erstmalige Inbetriebnahme

Nach der Installation musst du als erstes deine Recalbox über das HDMI-Kabel mit deinem Fernseher verbinden. Um deine Recalbox mit Strom zu versorgen und zu starten, steckst du einfach das Micro-USB- oder USB-C-Stromkabel ein.

>Viele Controller funktionieren direkt nach dem Anschließen. Wenn du jedoch einen USB-Controller konfigurieren willst/musst, eine USB-Tastatur anschließen willst, etc., schaue bitte in den Abschnitt [Controller-Konfiguration] (#Controller-Konfiguration).
{.is-info}

### Recalbox auf Deutsch stellen

Drücke im Systemauswahlbildschirm `START` auf deinem Controller. Wähle dann `SYSTEMEINSTELLUNGEN`  > `SPRACHE` und wähle dann `GERMAN` und gehe danach zurück. Recalbox teilt Dir dann mit, dass das System neu gestartet werden muss. Nach dem Hochfahren sind die Menüs komplett Deutsch.

###  Recalbox ausschalten

Drücke im Systemauswahlbildschirm `SELECT` auf deinem Controller und wähle `SYSTEM HERUNTERFAHREN`. Warte , bis die grün/orangefarbene LED des Pi aufhört zu blinken, um die microSD-Karte nicht zu beschädigen, und ziehe, sobald sie aufhört zu blinken, das Netzkabel ab.

### Mitgelieferte Spiele

Recalbox wird mit einigen lizenzfreien (Homebrew-)Spielen und/oder Demos für verschiedene Systeme bereitgestellt..

## Controller-Konfiguration

### Hinzufügen und Konfigurieren eines USB-Controllers

Du kannst USB-Controller der Recalbox hinzufügen. Die meisten Modelle sind kompatibel (siehe dazu die [Kompatibilitätsliste](./hardware-compatibility/compatible-devices/controllers)).

Nachdem du deinen USB-Controller angeschlossen hast (oder deinen Bluetooth-Controller gekoppelt hast), drücke `START` mit einem zuvor konfigurierten Controller. Wähle dann `CONTROLLER EINSTELLUNGEN` und folge den Anweisungen.

![](/basic-usage/first-use-and-configuration/padconfig1.png)

Wähle dazu `CONTROLLER KONFIGURIEREN` und folge den Anweisungen.

![](/basic-usage/first-use-and-configuration/padconfig2.png)

Drücke eine Taste, um die Konfiguration zu starten.

![](/basic-usage/first-use-and-configuration/padconfig3.png)

Halte eine beliebige Taste des Controllers gedrückt, um ihn zu konfigurieren.

![](/basic-usage/first-use-and-configuration/padconfig4.png)

Du kannst nun deinem Controller Tasten zuweisen. Die Namen, die für die Belegung der Tasten verwendet werden, entsprechen denen des Super Nintendo-Controllers:

![Super Nintendo (SNES) Controller](/basic-usage/first-use-and-configuration/snespad.jpg)

>Die Tasten L1, R1, L2, R2, L3 und R3 sind an den Playstation-Controller angelehnt.
{.is-info}

>**HOTKEY**
>
>Die letzte zu konfigurierende Taste, **HOTKEY**, ist diejenige, die zum Aktivieren der [Sonderbefehle](./basic-usage/getting-started/special-commands) verwendet wird, wenn man sich in einem Emulator befindet:
>
>Bei Xbox 360- und Xbox One-Controllern ist die **Hotkey**-Taste der Taste `HOME` zugewiesen.
>Bei PS3- und PS4-Controllern ist die **Hotkey**-Taste der `PS` Taste zugewiesen.
>
>Wenn keine eigene Taste vorhanden oder noch nicht zugewiesen ist, wird empfohlen, die Taste `Select` mit **Hotkey** zu belegen.
{.is-warning}

>Um einen beliebige Taste (außer **Hotkey**) zu überspringen, betätige die `Abwärts` Taste, um zur nächsten Taste zu gelangen.
{.is-info}

>Bei 6-Tasten-Controllern (SNES, Arcade, ...) sind die Tasten entsprechend dem SNES-Controller belegt (siehe oben).
>Bei 2-Tasten-Controllern (NES, PC Engine, Gameboy, ...) lauten die zugewiesenen Tasten `A` und `B`.
{.is-warning}

![](/basic-usage/first-use-and-configuration/padconfig5.png)

![](/basic-usage/first-use-and-configuration/padconfig6.png)

### Controller zuweisen

* Zurück auf dem Setup-Bildschirm kannst du den Controller einem Spieler zuweisen.
* Wähle `SPIELER 1`, um einen Controller zuzuweisen.

![](/basic-usage/first-use-and-configuration/padassign1.png)

* Wähle einen Controller aus.

![](/basic-usage/first-use-and-configuration/padassign2.png)

* Der Controller ist nun zugewiesen.

![](/basic-usage/first-use-and-configuration/padassign3.png)

### Verwendung einer Tastatur

Wenn kein Controller konfiguriert werden kann, besteht die Möglichkeit, eine kabelgebundene USB-Tastatur an die Recalbox anzuschließen, um diese zu konfigurieren. Alle Tastenkombinationen für die Tastatur findest du [hier](./tutorials/other/emulationstation-use-with-keyboard-and-mouse).

### PS3 Controller

So **verbindest** du einen PS3-Controller :

* Schließe zunächst den Controller an den USB-Anschluss an (über ein geeignetes Kabel) und warte 10 Sekunden.
* Danach kannst du den Controller vom Kabel abziehen und die **PS**-Taste einmal drücken, um die Funkverbindung zu starten.

>**Hinweis**:  
>Für asiatische Nachbauten des PS3 Dualshock 3 (wie bspw. GASIA oder SHANWAN) musst du einige Einstellungen anpassen.
{.is-warning}

>**Achtung**:  
>Bei Unsicherheiten bezüglich der Stromversorgung und des Stromverbrauchs ist es am besten, das Laden des PS3-Controllers am Raspberry Pi zu vermeiden, da dies zu Stabilitätsproblemen führen kann.
>
>Bitte verbinde den Controller nur mit dem Raspberry Pi, um ihn mit deiner Recalbox zu verbinden.
{.is-danger}

Wenn du die Einstellungen verstehst oder deinen Controller mit einer USB-Verbindung verwenden möchtest, musst du den PS3-Bluetooth-Treiber in der Datei `recalbox.conf` deaktivieren, indem du die entsprechende Zeile suchst und bearbeitest. 
`controllers.ps3.enabled=0`.

Denke bitte daran, dass die Controller-Konfiguration in Recalbox auf der SNES-Tastenkonfiguration basiert:

| PS3 Controller | SNES Controller |
| :---: | :---: |
| X | B |
| ◯ | A |
| ⬜ | Y |
| △ | X |

>**Information**:  
>Standardmäßig liegt der **HOTKEY** auf der Taste **PS** (die in der Mitte des Controllers).  
>Weitere Informationen zum HOTKEY findest du auf der Seite [Sonderbefehle](./basic-usage/getting-started/special-commands).
{.is-info}

### Xbox 360 Controller

>**Hinweis**:  
>Xbox 360 Wireless Controller erfordern einen speziellen Wireless-Empfänger.
{.is-warning}

Denke bitte daran, dass die Controller-Konfiguration in Recalbox auf der SNES-Tastenkonfiguration basiert:

| Xbox 360 Controller | SNES Controller |
| :---: | :---: |
| A | B |
| B | A |
| X | Y |
| Y | X |

>**Information**:  
>Standardmäßig liegt der **HOTKEY** auf der Taste **HOME** (die Taste in der Mitte des Joysticks).  
>Weitere Informationen zum **HOTKEY,** findest du auf der Seite [Sonderbefehle](./basic-usage/getting-started/special-commands).
{.is-info}

### Einen Bluetooth-Controller hinzufügen

So fügst du einen Bluetooth-Controller hinzu:

* Bringe den Controller in den _Pairing-Modus_.
* Drücke dann `START` und wähle `CONTROLLER EINSTELLUNGEN`.
* Anschließend `EINEN BLUETOOTH CONTROLLER VERBINDEN`.

Es erscheint eine **Liste der erkannten Geräte**!

* Wähle einfach deinen aus und schon ist der Controller gekoppelt.
* Danach kannst du mit der Konfiguration fortfahren (falls der Controller nicht bereits von Recalbox unterstützt wird).

>**Information**:  
>Für **8bitdo**-Controller schaue bitte auf die Seite [**8bitdo mit Recalbox**](./hardware-compatibility/compatible-devices/8bitdoon-recalbox).
{.is-info}

### GPIO Controller

Du kannst deine Arcade-Tasten und Joysticks direkt an die GPIOs des Raspberry Pi anschließen (siehe dazu Seite [**GPIO-Controller**](./tutorials/controllers/gpio/gpio-controllers)). Du kannst auch Original-Controller von PSOne, NES, SNES, Megadrive,etc. anschließen. (siehe **DB9-Steuerungen** und **Gamecon-Steuerungen**).

### Virtuelle Controller

Mit dem Projekt Miroof Virtual Gamepads kannst du bis zu 4 Controller über deine Smartphones/Tablets hinzufügen!

Starte dazu den Webbrowser deines Smartphones und gib die IP-Adresse deiner Recalbox gefolgt von dem Kommunikationsport (Port `:8080`) ein. 

>**Information**:  
>Die IP-Adresse der Recalbox findest du im Menü `NETZWERKEINSTELLUNGEN`.
{.is-info}

![Virtuelle Joystick-Tastzonen](/basic-usage/first-use-and-configuration/virutalgamepad_touch_zones.png)

## Verwendung externer Speicher

>**Hinweis**:  
>Du kannst ganz einfach **ein USB-Speichergerät** (USB-Stick, externe Festplatte mit eigener Stromversorgung, usw.) verwenden, um deine ROMs, persönlichen Dateien, usw. zu speichern.
>
>**Bei dieser Methode werden das **System** (auf der SD-Karte) und die **Partitionsfreigabe** (auf dem Gerät) voneinander **getrennt**.
>  
>Wenn du also dein **System neu installieren müsstest**, würdest du **alle deine Benutzerdaten beibehalten**.
>Alles, was du dazu tun musst, ist **den externen Speicher** mit deiner Recalbox zu verbinden und **ihn dann im System auswählen** und schon kann es losgehen.
{.is-info}

### Dateisystem des externen Speichers

>Es muss ein Gerät verwendet werden, das eines der folgenden Dateisysteme verwendet: **FAT32, EXFAT, EXT4 oder NTFS**.

>**Hinweis**:
>
>Wir empfehlen dringend, das **EXFAT**-Dateisystem zu verwenden.
{.is-info}

>**Achtung**:
>
>Die Übertragungsraten können im Fall von **NTFS** langsam sein.  
>Vergewissere dich auch, dass das **Dateisystem**, das du wählst, **mit dem Betriebssystem deines PCs kompatibel ist**.
{.is-danger}

| Dateiformat | Maximale Datenträgergröße | Maximale Dateigröße | Maximale Anzahl von Dateien | Lese-/Schreibmodus
| :---: | :---: | :---: | :---: |  :---:|
| FAT | 2 TiB | 4 [GiB](https://de.wiktionary.org/wiki/Gibibyte) (4,2949Go) | Mehr als **250 Millionen**. | Windows: &#x2705; Linux: &#x2705; |
| NTFS | 256 [TiB](https://de.wiktionary.org/wiki/Tebibyte) | 16 TiB | 4.294.967.295 | Windows: &#x2705; Linux: &#x2705; |
| exFAT | 128 [PiB](https://de.wiktionary.org/wiki/Pebibyte) | 128 PiB | 2.796.202 pro Verzeichnis | Windows: &#x2705; Linux: &#x2705; |
| ext4 | 1 [EiB](https://de.wiktionary.org/wiki/Exbibyte) (1.152.921,5046 TiB - begrenzt auf 16 TiB durch e2fsprogs) | 16 TiB) | 4 Milliarden | Windows: &#x274C; Lesen/Schreiben über ext2fsd Linux: &#x2705; |

>**Achtung**:
>
>Recalbox **formatiert** das Gerät nicht, es werden lediglich **neue Dateien darauf angelegt**.
{.is-danger}

### Konfiguration

So konfigurierst du Recalbox für die Verwendung eines USB-Speichergeräts

* Schließe dein Gerät an deine Recalbox an und schalte sie dann ein.
* Sobald du dich auf der Recalbox-Oberfläche befindest, drücke die `START`-Taste auf deinem Controller und gehe dann zu `SYSTEMEINSTELLUNGEN` und dann weiter zu `SPEICHER`.
* Wähle nun deinen Speicher aus der Liste aus, bestätige dann die Auswahl und warte, bis das System neu gestartet ist.

>**Information**:
>
>**Während dieses Neustarts wird**, Recalbox **im Stammverzeichnis** des externen Speichers **ein neues Verzeichnis** mit dem Namen **"recalbox"** anlegen, das den gesamten Verzeichnisbaum der **Share-Partition** beinhaltet.
{.is-info}

### Fehlersuche und -behebung

#### Nich sichtbare Festplatte

Was ist, wenn Recalbox nach einem Neustart die Festplatte immer noch nicht sieht?

>**Informations**: 
>
>Manchmal kann es passieren, dass nach der Auswahl des Laufwerks in EmulationStation und dem Neustart der Recalbox das Dateisystem auf dem Laufwerk nicht erstellt werden konnte. Es werden dann weiterhin die Dateien auf der SD-Karte verwendet. Das passiert vor allem bei einigen Festplatten, die sich nur langsam initialisieren lassen.
{.is-info}

Was man dann unternehmen kann:

* Verbinde dich über [SSH](./tutorials/system/access/root-access-terminal-cli) mit deiner Recalbox
* Mounte die [Boot-Partition](./tutorials/system/access/remount-partition-with-write-access) als read/write.
* Gib die folgenden Befehle ein: `cd /boot` und dann `nano recalbox-boot.conf`.
* Füge folgende Zeile der `recalbox.conf` am Ende hinzu: `sharewait=30`. Der Wert wird in Sekunden angegeben.
* Speichere die Änderung mit `Strg+X`,dann `Y` und anschließend `Enter`. 
* Gib dann `reboot` ein und bestätige dies, um Recalbox neu zu starten.

## Netzwerk

### Über RJ45-Kabel konfigurieren

* Verbinde dein RJ45-Kabel mit deiner Recalbox und das war es dann auch schon.

### Über WLAN konfigurieren

* Drücke im Hauptfenster auf `START`, gehe dann zu `NETZWERKEINSTELLUNGEN` und stelle anschließend die `WLAN AKTIVIEREN`.
* Das WLAN kann nun über WPS konfiguriert werden. An deinem Router muss es eine Taste geben, die WPS aktiviert. Einmal gedrückt, bleibt WPS für 2 Minuten aktiv.
* Wähle nun, immer noch in Recalbox, unten im Menü `WPS-VERBINDUNG`. Damit wird versucht, alle bestehenden und aktiven WPS-Verbindungen in der Umgebung zu finden.
* Sobald die Verbindung gefunden ist, wird sie automatisch in der Recalbox gespeichert.