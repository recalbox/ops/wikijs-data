---
title: Varios packs de texturas de alta resolución diferentes sobre el mismo juego
description: 
published: true
date: 2023-09-20T21:00:23.809Z
tags: n64, nintendo, 64, texturas, packs
editor: markdown
dateCreated: 2023-09-20T21:00:23.809Z
---

## Información

En la guía "[Texturas de alta resolución](./high-resolution-textures)" habéis aprendido a utilizar los paquetes de texturas de alta resolución.

## Generalidades

En algunos juegos, se han creado diferentes packs de texturas. El problema es que suelen tener todos el mismo nombre. Si quieres usar varias versiones, continúa leyendo.

## Etapas

Para cada versión de la textura de alta resolución, debemos cambiar el nombre de la rom interna. Una vez hecho esto, cambiaremos los nombres de las texturas de alta resolución.

## Lo que necesitamos

En esta guía, usaremos **N64 Rom Renamer** de Einstein II (Google es vuestro mejor amigo), para cambiar el nombre interno de la rom.

También necesitaremos un script batch para el renombrado de varios fichero (ejemplo Windows: **AntRenamer**). En esta guía, usaremos **Better Rename** (macOS).

## Comenzamos

### Modificad el nombre interno de la rom

* Abrid **N64 Rom Renamer** con derechos de administrador.

![](/tutorials/games/guides/nintendo-64/multiple-hires-textures/multiplehirespacks1.png){.full-width}

* Pulsad `F2` o id a `Tools` > `Headereditor`.
* Validad el aviso.

![](/tutorials/games/guides/nintendo-64/multiple-hires-textures/multiplehirespacks2.png)

* En la esquina superior izquierda, pulsad en el icono de la carpeta y seleccionad vuestra rom (en nuestro ejemplo, usaremos Mario Kart 64).

![](/tutorials/games/guides/nintendo-64/multiple-hires-textures/multiplehirespacks3.png){.full-width}

* Aquí podéis modificar el nombre interno de la rom.

### Atención

El nombre **no debe ser demasiado largo**. Hay que respetar la longitud total de la cadena junto con los espacios (20 caracteres). En nuestro caso, vemos que **MARIOKART64** tiene nueve espacios además de su nombre, lo que nos da la longitud total posible para el nombre interno.

![](/tutorials/games/guides/nintendo-64/multiple-hires-textures/multiplehirespacks4.png){.full-width}

* En nuestro ejemplo, eliminaremos cuatro espacios y los sustituiremos por cuatro letras (SNES).

![](/tutorials/games/guides/nintendo-64/multiple-hires-textures/multiplehirespacks5.png){.full-width}

* Pulsad `Save changes` para guardar los cambios y luego pulsad `Back` para volver atrás.
* El nombre interno de la rom se ha cambiado correctamente.

Cambiad el nombre del fichero de la rom; por ejemplo: Mario Kart 64 SNES Edition.ext El nombre del fichero de la rom no es importante para las texturas.

### Modificad el pack de texturas de alta resolución y los ficheros:

En nuestro ejemplo, la estructura de ficheros para nuestro paquete de alta resolución se parece a esto:

![](/tutorials/games/guides/nintendo-64/multiple-hires-textures/multiplehirespacks6.png)

La carpeta y los ficheros llamados **MARIOKART64** ahora no son correctos, por supuesto, porque hemos cambiado el nombre interno de la rom. ¡Hay que corregir esto!

* Abrid **Better Rename**
* Category : Text
* Action : Replace text
* Replace : **MARIOKART64**
* With : **MARIOKART64SNES**

![](/tutorials/games/guides/nintendo-64/multiple-hires-textures/multiplehirespacks7.png){.full-width}

Después de renombrar, debemos comprobar que todos los nombres coinciden. Si el nombre de la carpeta y los nombres de los ficheros coinciden, podréis copiar vuestro pack de texturas.

>**Por precaución:**
>
>Antiguo nombre de la carpeta: MARIOKART64  
>Antiguo nombre de fichero dentro de la carpeta: MARIOKART64#......
>
>Nuevo nombre de la carpeta: MARIOKART64SNES  
>Nuevo nombre de fichero dentro de la carpeta: MARIOKART64SNES#...
{.is-warning}

Aplicad estas mismas etapas para cada paquete de texturas de alta resolución que queréis integrar.