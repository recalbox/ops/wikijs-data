---
title: Libretro PicoDrive
description: 
published: true
date: 2024-06-29T17:43:09.390Z
tags: libretro, 32x, picodrive
editor: markdown
dateCreated: 2023-09-06T07:19:36.574Z
---

**Libretro PicoDrive** es un emulador open-source Sega [8](./../master-system)/[16](./../megadrive) bit y [32X](./../32x). Ha sido concebido pensando en los ordenadores mono-carta basados en la arquitectura [ARM](https://fr.wikipedia.org/wiki/Architecture_ARM).

## ![](/emulators/license.svg) Licencia

El core posee la siguiente licencia [**no-commercial**](https://github.com/libretro/picodrive/blob/master/COPYING).

## ![](/emulators/compatibility.png) Compatibilidad

 | RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
 | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Funcionalidades

| Fonctionnalité | Supporté |
| :---: | :---: |
| Reinicio | ✔ |
| Grabaciones | ✔ |
| Grabaciones instantáneas | ✔ |
| Rebobinado | ✔ |
| Netplay | ✔ |
| Optiones del core | ✔ |
| RetroAchievements | ✔ |
| Cheats RetroArch | ✔ |
| Controles | ✔ |
| Remapping | ✔ |
| Control de disco | ✔ |

## ![](/emulators/bios.svg) BIOS

>**Aucun bios n'est requis.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Extensiones de roms soportadas

Las roms deben tener las extensiones siguientes:

* .32x
* .bin
* .smd
* .zip
* .7z

Este sistema soporta las roms comprimidas en formato .zip/.7z. No obstante, se trata sólo de un archivo.

Los ficheros contenidos en los .zip/.7z deben tener las extensiones citadas previamente.
Además, cada fichero .zip/.7z sólo debe contener un fichero rom al interior.

### Emplazamiento

Disponed vuestras roms como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 **sega32x**
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.zip**

>Se aconsejan las roms del projecto **No-Intro**.
{.is-success}

>Para más información sobre las roms, consultad [este tutorial](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

### Compatibilidad

| Juegos 32x | Problemas identificados |
| :--- | :--- |
| Brutal Unleashed – Above the Claw | Softlock despues del primer combate. |
| FIFA Soccer ’96 | Texto del menú principal con bugs. |
| Knuckles’ Chaotix | Gráficos con bugs en la pantalla de selección del jugador. |
| NBA Jam Tournament Edition | Problemas de framerate. |
| NFL Quarterback Club | Faltan algunos gráficos del menú. |
| Virtua Racing Deluxe | Línea que parpadea durante la presentación del logo de SEGA. |
| World Series Baseball Starring Deion Sanders | Se cuelga cuando empieza un partido. |
| WWF Raw | Faltan algunos gráficos. |

## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

>Para conservar las configuraciones personalizadas cuando se produce una actualización del sistema Recalbox, os aconsejamos utilizar las [Sobrecargas de configuración](./../../../advanced-usage/configuration-override).
{.is-info}

### Acceder a las opciones

Podéis configurar las opciones del core de dos formas diferentes.

* Via el Menú RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via el fichero `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Opciones del core

| Opción | Valor de la opción | Variable (para la sobrecarga) | Valores posibles |
| --- | --- | --- | --- |
| Region | `Auto` ✅ / `Japan NTSC` / `Japan PAL` / `US` / `Europe` | `picodrive_region` | `Auto` / `Japan NTSC` / `Japan PAL` / `US` / `Europe` |
| Master System Type | `Auto` ✅ / `Game Gear` / `Master System` | `picodrive_smstype` | `Auto` / `Game Gear` / `Master System` |
| Master System ROM Mapping | `Auto` ✅ / `Sega` / `Codemasters` / `Korea` / `Korea MSX` / `Korea X-in-1` / `Korea 4-Pak` / `Korea Janggun` / `Korea Nemesis` | `picodrive_smsmapper` | `Auto` / `Sega` / `Codemasters` / `Korea` / `Korea MSX` / `Korea X-in-1` / `Korea 4-Pak` / `Korea Janggun` / `Korea Nemesis` |
| Sega CD RAM Cart | `Désactivé` ✅ / `Activé` | `picodrive_ramcart` | `disabled` / `enabled` |
| Core-provided aspect ratio | `PAR` ✅ / `4:3` / `CRT` | `picodrive_aspect` | `PAR` / `4/3` / `CRT` |
| LCD Ghosting Filter | `Désactivé` ✅ / `Weak` / `Normal` | `picodrive_ggghost` | `off` / `weak` / `normal` |
| Renderer | `Accurate` ✅ / `Good` / `Fast` | `picodrive_renderer` | `accurate` / `good` / `fast` |
| Sample Rate (Hz) | `16000` / `22050` / `32000` / `44100` ✅ | `picodrive_sound_rate` | `16000` / `22050` / `32000` / `44100` |
 | FM filtering | `Désactivé` ✅ / `Activé` | `picodrive_fm_filter` | `off` / `on` |
| Master System FM Sound Unit | `Désactivé` ✅ / `Activé` | `picodrive_smsfm` | `off` / `on` |
| Mega Drive FM DAC noise | `Désactivé` ✅ / `Activé` | `picodrive_dacnoise` | `off` / `on` |
| Audio filter | `Désactivé` ✅ / `Low-pass` | `picodrive_audio_filter` | `disabled` / `low-pass` |
| Low-pass filter % | `5` / `10` / `15` / `20` / `25` / `30` / `35` / `40` / `45` / `50` / `55` / `60` ✅ / `65` / `70` / `75` / `80` / `85` / `90` / `95` | `picodrive_lowpass_range` | `5` / `10` / `15` / `20` / `25` / `30` / `35` / `40` / `45` / `50` / `55` / `60` / `65` / `70` / `75` / `80` / `85` / `90` / `95` |
| Input Device 1 | `3 Button Pad` ✅ / `6 Button Pad` / `None` | `picodrive_input1` | `3 Button Pad` / `6 Button Pad` / `None` |
| Input Device 2 | `3 Button Pad` ✅ / `6 Button Pad` / `None` | `picodrive_input2` | `3 Button Pad` / `6 Button Pad` / `None` |
| Dynamic Recompilers | `Activé` ✅ / `Désactivé` | `picodrive_drc` | `disabled` / `enabled` |
| Frameskip | `Désactivé` ✅ / `Auto` / `Manual` | `picodrive_frameskip` | `disabled` / `auto` / `manual` |
| Frameskip Threshold (%) | `15` / `18` / `21` / `24` / `27` / `30` / `33` ✅ / `36` / `39` / `42` / `45` / `48` / `51` / `54` / `57` / `60` | `picodrive_frameskip_threshold` | `15` / `18` / `21` / `24` / `27` / `30` / `33` / `36` / `39` / `42` / `45` / `48` / `51` / `54` / `57` / `60` |
| No Sprite Limit | `Désactivé` ✅ / `Activé` | `picodrive_sprlim` | `disabled` / `enabled` |
| 68K Overclock | `Désactivé` ✅ / `+25%` / `+50%` / `+75%` / `+100%` / `+200%` / `+400%` | `picodrive_overclk68k` | `disabled` / `+25%` / `+50%` / `+75%` / `+100%` / `+200%` / `+400%` |

## ![](/emulators/external-links.png) Enlaces externos

* **Código fuente utilizado** : [https://github.com/libretro/picodrive/](https://github.com/libretro/picodrive)
* **Documentación Libretro** : [https://docs.libretro.com/library/picodrive/](https://docs.libretro.com/library/picodrive/)