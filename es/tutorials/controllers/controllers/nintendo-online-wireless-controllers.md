---
title: Mandos Nintendo Switch inhalambricos
description: 
published: true
date: 2024-07-26T06:53:10.190Z
tags: switch, nintendo, 7.2+, mando, inhalámbrico
editor: markdown
dateCreated: 2023-09-19T09:11:48.842Z
---

En esta sección describimos los mandos Nintendo oficiales inhalámbricos que son compatibles con Recalbox y cómo utilizarlos.

## ¿Cuáles son los mandos?

Los mandos Nintendo Switch que podéis utilizar nativamente en Recalbox son los siguientes:

* [Joy-Cons](joy-cons-usage)

![](/tutorials/controllers/controllers/switch-joycons/nintendo_switch_joy-con_controllers.png)

* [Nintendo Switch Online - Super Nintendo](snes-controller-usage)

![](/tutorials/controllers/controllers/switch-joycons/snes-controller.jpg)

En estos apartados encontraréis información sobre cómo sincronizar y utilizar vuestro mando en Recalbox.

## ¿Qué pasa con los mandos de Nintendo Switch Online - NES?

Estos mandos no se pueden usar en Recalbox. Cada uno de estos mandos se reconoce como un Joy-Con derecho.