---
title: Naomi
description: 
published: true
date: 2021-10-14T23:02:56.331Z
tags: arcade, naomi
editor: markdown
dateCreated: 2021-10-07T00:01:30.095Z
---

![](/emulators/arcade/naomi.svg){.align-center}

## Ficha técnica

* **Processador** : Hitachi SH-4 (128 bits) RISC 360 MIPS
* **Vídeo** : Processador PowerVR 2 a 100 MHz fabricado pela Nec
* **Áudio**: ARM7 Yamaha XG AICA RISC Processador de 32 bits 
* **RAM**: 32 MB
* **Vídeo RAM**: 16 MB
* **Áudio RAM**: 8 MB
* **Mídia** : Cartucho (PCB de ROMS)

## Apresentação

O sistema Naomi foi lançado sob o nome **Naomi Multi System** (também conhecido como **Naomi Multiboard**), um acrônimo para "_New Arcade Operation Machine Idea"_ literalmente "Nova Idéia de Máquina de Arcade"; Naomi também se traduz como "beleza" em japonês. 

O sistema tem uma arquitetura **muito semelhante à do console Dreamcast**: um microprocessador Hitachi SH-4 a 200 MHz, um processador gráfico PowerVR Series 2 GPU (PVR2DC) e um processador de som Yamaha AICA; mas com **mais memória RAM e memória de vídeo** (ou seja, duas vezes mais que o Dreamcast). Os jogos são armazenados em **um cartucho** (Naomi) **ou um GD-ROM** (NaomiGD)**.**
  
Produzido pela SEGA, a líder do arcade na época, para competir com o Sistema 23 da NAMCO, seu marketing começou em 1998 com **The House of the Dead 2** como o primeiro jogo até 2006. Mais de 160 jogos foram lançados neste hardware (o recorde mundial para o número de jogos em um hardware de arcade).

A Sega anuncia que seu **Naomi 2** permanece compatível com os **cartuchos e GD-Rom Naomi 1.**

## Emuladores

[Libretro Flycast](libretro-flycast)
