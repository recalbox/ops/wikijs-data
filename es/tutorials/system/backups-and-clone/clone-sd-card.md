---
title: Duplicación de vuestra tarjeta SD
description: Cómo clonar vuestra tarjeta SD
published: true
date: 2023-09-01T15:09:44.282Z
tags: clone, duplicación, tarjeta sd
editor: markdown
dateCreated: 2023-09-01T15:09:44.282Z
---

>Si queréis duplicar una tarjeta SD en otra tarjeta SD, tened en cuenta que dos tarjetas de fabricantes diferentes pueden presentar diferencias de la capacidad de almacenamiento, incluso cuando se suponen que presentan la misma capacidad. Os exponéis a no poder grabar vuestro backup en la nueva tarjeta, salvo si la nueva tarjeta presenta un tamaño superior a la tarjeta de origen.
{.is-warning}

* Para duplicar vuestra tarjeta SD y disponer de una copia de seguridad de vuestra Recalbox, podéis utilizar los siguientes programas:

## {.tabset}
### Windows

Programas :

* [Win32 Disk Imager](http://sourceforge.net/projects/win32diskimager/) avec la fonction READ.

### macOS

Programas :

* [ApplePi-Baker](https://www.tweaking4all.com/software/macosx-software/applepi-baker-v2/)
* [Carbon Copy Cloner](https://bombich.com/fr)

También podéis utilizar el comando `dd` desde un terminal:

#### Copia

* Enchufad vuestra tarjeta SD en vuestro PC Linux.
* Determinad cómo se identifica vuestro periférico:

```shell
sudo fdisk -l
```

* Situaros dentro de vuestro directorio backup. El siguiente comando extrae y comprime vuestra tarjeta SD (la letra X debe reemplazarse por la letra identificada en la primera etapa):

```shell
sudo dd if=/dev/sdX | gzip -9 > ./recalbox-20150411-sdb.img.gz
```

#### Restauración

* Para restaurar vuestra copia, enchufad vuestra tarjeta SD en vuestro PC y utilizad este comando  (donde `/dev/sdX` es vuestra tarjeta SD sobre la que queréis grabar la imagen) :

```shell
gunzip ./recalbox-20150411-sdb.img.gz | sudo dd of=/dev/sdX
```

Para seguir el avance del proceso `dd`, abrid un nuevo terminal y escribid el siguiente comando:

```shell
watch -n 5 sudo pkill -USR1 -n -x dd
```

### Linux

Programas :

* GParted

También podéis utilizar el comando `dd` desde un terminal:

#### Copia

* Enchufad vuestra tarjeta SD en vuestro PC Linux.
* Determinad cómo se identifica vuestro periférico:

```shell
sudo fdisk -l
```

* Situaros dentro de vuestro directorio backup. El siguiente comando extrae y comprime vuestra tarjeta SD (la letra X debe reemplazarse por la letra identificada en la primera etapa):

```shell
sudo dd if=/dev/sdX | gzip -9 > ./recalbox-20150411-sdb.img.gz
```

#### Restauración

* Para restaurar vuestra copia, enchufad vuestra tarjeta SD en vuestro PC y utilizad este comando  (donde `/dev/sdX` es vuestra tarjeta SD sobre la que queréis grabar la imagen) :

```shell
gunzip ./recalbox-20150411-sdb.img.gz | sudo dd of=/dev/sdX
```

Para seguir el avance del proceso `dd`, abrid un nuevo terminal y escribid el siguiente comando:

```shell
watch -n 5 sudo pkill -USR1 -n -x dd
```
