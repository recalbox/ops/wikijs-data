---
title: Play using a PSP with FuSa controller
description: 
published: true
date: 2021-08-07T14:34:07.723Z
tags: controller, psp, fusa
editor: markdown
dateCreated: 2021-08-07T14:34:07.722Z
---

With custom firmware such as **pro CFW** and **ME CFW**, but also thanks to the natural flexibility of the PSP and the semi-open hardware, with the help of the FuSa controller (a homebrew application for the PSP), you can get one of the best SNES controllers for your Raspberry Pi.

## What we need?

* A PSP model 1xxx, 2xxx or 3xxx (2xxx & 3xxx models preferred)
* A Memory Stick Pro Duo 1GB or higher
* A mini USB - USB A cable
* A custom firmware (Pro or ME / LME)
* A PC or your Raspberry Pi to transfer files via the network
* The [FuSa controller] application (http://foosa.do.am/load/fusa_gamepad_version_03/3-1-0-33)

## To get started

Download the FuSa gamepad zip, then turn on your PSP with your memory card in your console, and if it's a 3000 model, run your CFW (older models don't need to run it as these models already boot with CFW loaded), connect your USB cable to your PSP and PC, unzip your zip in `/PSP/GAMES/` and unplug it from your PC safely.

Now launch the application and connect it to your Raspberry Pi, your PSP screen should be off but the power LED should normally stay on.

## Recommended layout

For a consistent experience inside and outside of EmulationStation, including in Kodi and all RetroArch kernels, and because all Playstation gamepads in Japan use the same layout as an SNES gamepad but with different symbols, I suggest using this button configuration plus my little suggestion for the volume buttons and home button in Kodi.

![PSP layout](/tutorials/controllers/controllers/use-psp-as-controller/psp-config.png)

## Notes

* PSP Street is not supported.
* If your PSP is in good condition, it is much better than a generic controller or a 360 gamepad. The buttons are very responsive and the D-Pad is great for retro games.
* The Raspberry Pi is NOT able to fully charge a PSP.
* The volume buttons do not work in EmulationStation or RetroArch.
* You can access more detailed configuration in the FuSa .ini files.