---
title: Libretro Mednafen_SuperGrafx
description: 
published: true
date: 2024-07-24T15:48:26.035Z
tags: libretro, mednafen, supergrafx, turbografx cd
editor: markdown
dateCreated: 2021-08-01T23:03:47.735Z
---

**Libretro Mednafen_SuperGrafx** est un port standalone de Mednafen PCE Fast pour libretro.

## ![](/emulators/license.svg) License

This core is under [**GPLv2**](https://github.com/libretro/beetle-supergrafx-libretro/blob/master/COPYING) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Features

| Feature | Supported |
| :---: | :--- |
| Restart | ✔ |
| Saves | ✔ |
| States | ✔ |
| Rewind | ✔ |
| Netplay | ✔ |
| Core Options | ✔ |
| RetroAchievements | ✔ |
| RetroArch Cheats | ✔ |
| Controls | ✔ |
| Remapping | ✔ |
| Multi-Mouse | ✔ |

## ![](/emulators/bios.svg) BIOS

### List of mandatory bios

| Filename | Description | MD5 | Provided |
| :---: | :---: | :---: | :---: |
| syscard3.pce | Super CD-ROM^2 System V3.0 | ff1a674273fe3540ccef576376407d1d 1e47a1780aaa7e277a3896c1ba00e317 38179df8f4ac870017db21ebcbf53114 | ❌ |

### Location

Put the bios like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 pcenginecd
┃ ┃┃ ┃ ┣ 🗒 **syscard3.pce**

### List of optional bios

| Filename | Description | MD5 | Provided |
| :---: | :---: | :---: | :---: |
| syscard1.pce | CD-ROM System Card V1.0 | 9f770275393b8627cf9d24e5c56d2ab9 | ❌ |
| syscard2.pce | CD-ROM^2 System Card V2.0 | 424f1d6bf93259bf255afa7d1dc9f721 | ❌ |
| gexpress.pce | Game Express CD Card | 6d2cb14fc3e1f65ceb135633d1694122 | ❌ |

### Location

Put the bios like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 pcenginecd
┃ ┃ ┃ ┃ ┣ 🗒 **syscard1.pce**
┃ ┃ ┃ ┃ ┣ 🗒 **syscard2.pce**
┃ ┃ ┃ ┃ ┣ 🗒 **gexpress.pce**

## ![](/emulators/isos.svg) Isos

### Supported extensions

Isos must have the extension:

* .cue/.bin
* .ccd/.img
* .chd
* .m3u
* .toc

### Location

Put the isos like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 pcenginecd
┃ ┃ ┃ ┃ ┣ 🗒 **game.cue**
┃ ┃ ┃ ┃ ┣ 🗒 **game.bin**

>The isos using **Redump** format are highly recommended.
{.is-success}

>For more information about isos, go to [this tutorial](./../../../tutorials/games/generalities/isos-and-roms)!
{.is-info}

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Accessing to options

You can configure various options in two different ways.

* Via RetroArch menu:

┣ 📁 RetroArch Menu
┃ ┣ 📁 Core options
┃ ┃ ┣ 🧩 Name_option

* Via the file `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core options

| Options | Options values | Variables (for configuration override) | Variables values |
| --- | --- | --- | --- |
| Color Palette | `RVB` ✅ / `Composite` | `sgx_palette` | `RGB` / `Composite` |
| Aspect Ratio | `Auto` ✅ / `6:5` / `4:3` | `sgx_aspect_ratio` | `auto` / `6:5` / `4:3` |
| Horizontal Overscan (352 Width Mode Only) | `300` / `302` / `304` / `306` / `308` / `310` / `312` / `314` / `316` / `318` / `320` / `322` / `324` / `326` / `328` / `330` / `332` / `334` / `336` / `338` / `340` / `342` / `344` / `346` / `348` / `350` / `352` ✅ | `sgx_hoverscan` | `300` / `302` / `304` / `306` / `308` / `310` / `312` / `314` / `316` / `318` / `320` / `322` / `324` / `326` / `328` / `330` / `332` / `334` / `336` / `338` / `340` / `342` / `344` / `346` / `348` / `350` / `352` |
| Initial scanline | `0` / `1` / `2` / `3` ✅ / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `21` / `22` / `23` / `24` / `25` / `26` / `27` / `28` / `29` / `30` / `31` / `32` / `33` / `34` / `35` / `36` / `37` / `38` / `39` / `40` | `sgx_initial_scanline` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `21` / `22` / `23` / `24` / `25` / `26` / `27` / `28` / `29` / `30` / `31` / `32` / `33` / `34` / `35` / `36` / `37` / `38` / `39` / `40` |
| Last scanline | `208` / `209` / `210` / `211` / `212` / `213` / `214` / `215` / `216` / `217` / `218` / `219` / `220` / `221` / `222` / `223` / `224` / `225` / `226` / `227` / `228` / `229` / `230` / `231` / `232` / `233` / `234` / `235` / `236` / `237` / `238` / `239` / `240` / `241` / `242` ✅ | `sgx_last_scanline` | `208` / `209` / `210` / `211` / `212` / `213` / `214` / `215` / `216` / `217` / `218` / `219` / `220` / `221` / `222` / `223` / `224` / `225` / `226` / `227` / `228` / `229` / `230` / `231` / `232` / `233` / `234` / `235` / `236` / `237` / `238` / `239` / `240` / `241` / `242` |
| Mouse Sensitivity | `0.25` / `0.50` / `0.75` / `1.00` / `1.25` ✅ / `1.50` / `1.75` / `2.00` / `2.25` / `2.50` / `2.75` / `3.00` / `3.25` / `3.50` / `3.75` / `4.00` / `4.25` / `4.50` / `4.75` / `5.00` | `sgx_mouse_sensitivity` | `0.25` / `0.50` / `0.75` / `1.00` / `1.25` / `1.50` / `1.75` / `2.00` / `2.25` / `2.50` / `2.75` / `3.00` / `3.25` / `3.50` / `3.75` / `4.00` / `4.25` / `4.50` / `4.75` / `5.00` |
| Allow Opposite Directions | `Disabled` ✅ / `Enabled` | `sgx_up_down_allowed` | `disabled` / `enabled` |
| Disable Soft Reset (RUN+SELECT) | `Disabled` ✅ / `Enabled` | `sgx_disable_softreset` | `disabled` / `enabled` |
| Multitap 5-port Controller | `Enabled` ✅ / `Disabled` | `sgx_multitap` | `enabled` / `disabled` |
| P1 Default Joypad Type | `2 Buttons` ✅ / `6 Buttons` | `sgx_default_joypad_type_p1` | `2 Buttons` / `6 Buttons` |
| P2 Default Joypad Type | `2 Buttons` ✅ / `6 Buttons` | `sgx_default_joypad_type_p2` | `2 Buttons` / `6 Buttons` |
| P3 Default Joypad Type | `2 Buttons` ✅ / `6 Buttons` | `sgx_default_joypad_type_p3` | `2 Buttons` / `6 Buttons` |
| P4 Default Joypad Type | `2 Buttons` ✅ / `6 Buttons` | `sgx_default_joypad_type_p4` | `2 Buttons` / `6 Buttons` |
| P5 Default Joypad Type | `2 Buttons` ✅ / `6 Buttons` | `sgx_default_joypad_type_p5` | `2 Buttons` / `6 Buttons` |
| Turbo Hotkey Mode | `Disabled` ✅ / `Toggle` / `Dedicated` | `sgx_turbo_toggle` | `disabled` / `switch` / `dedicated` |
| Alternate Turbo Hotkey | `Disabled` ✅ / `Enabled` | `sgx_turbo_toggle_hotkey` | `disabled` / `enabled` |
| Turbo Delay | `1` / `2` / `3` ✅ / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` | `sgx_turbo_delay` | `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` |
| CD Image Cache (Restart) | `Disabled` ✅ / `Enabled` | `sgx_cdimagecache` | `disabled` / `enabled` |
| CD BIOS (Restart) | `Games Express` / `System Card 1` / `System Card 2` / `System Card 3` ✅ | `sgx_cdbios` | `Games Express` / `System Card 1` / `System Card 2` / `System Card 3` |
| Detect Games Express CD (Restart Required) | `Enabled` ✅ / `Disabled` | `sgx_detect_gexpress` | `enabled` / `disabled` |
| (CD) CD Speed | `1` ✅ / `2` / `4` / `8` | `sgx_cdspeed` | `1` / `2` / `4` / `8` |
| (CD) ADPCM Volume % | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ / `110` / `120` / `130` / `140` / `150` / `160` / `170` / `180` / `190` / `200` | `sgx_adpcmvolume` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` / `110` / `120` / `130` / `140` / `150` / `160` / `170` / `180` / `190` / `200` |
| (CD) CDDA Volume % | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ / `110` / `120` / `130` / `140` / `150` / `160` / `170` / `180` / `190` / `200` | `sgx_cddavolume` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` / `110` / `120` / `130` / `140` / `150` / `160` / `170` / `180` / `190` / `200` |
| (CD) PSG Volume % | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ / `110` / `120` / `130` / `140` / `150` / `160` / `170` / `180` / `190` / `200` | `sgx_cdpsgvolume` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` / `110` / `120` / `130` / `140` / `150` / `160` / `170` / `180` / `190` / `200` |
| Force SuperGrafx Emulation (Restart Required) | `Disabled` ✅ / `Enabled` | `sgx_forcesgx` | `disabled` / `enabled` |
| No Sprite Limit | `Disabled` ✅ / `Enabled` | `sgx_nospritelimit` | `disabled` / `enabled` |
| CPU Overclock Multiplier (Restart) | `1` ✅ / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `20` / `30` / `40` / `50` | `sgx_ocmultiplier` | `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `20` / `30` / `40` / `50` |

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/libretro/beetle-supergrafx-libretro/](https://github.com/libretro/beetle-supergrafx-libretro/)
* **Libretro documentation**: [https://docs.libretro.com/library/beetle_sgx/](https://docs.libretro.com/library/beetle_sgx/)