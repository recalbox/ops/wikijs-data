---
title: Uzebox
description: 
published: true
date: 2021-08-09T00:06:46.038Z
tags: fantasy, uzebox
editor: markdown
dateCreated: 2021-06-30T15:17:51.183Z
---

![](/emulators/fantasy/uzebox.svg){.align-center}

## Technical data

* **Developer**: Belogic Software
* **Year of release**: 2008
* **CPU**: Amtel ATmega644 microcontroller @ 28.6 Mhz
* **RAM**: 4 K
* **Program Memory**: 64 K
* **Sound**: 5 channels wavetable synthesis , 8-bit mono, mixed at ~15Khz and output via PWM
* **Resolution**: Various video modes supporting up to 360x224 pixels in tiles-only mode
* **Colors**: 256 simultaneous colors arranged in a 3:3:2 color space
* **Media**: SD Memory card

## Presentation 

The Uzebox is an open source retro-minimalist game console. It is based on an 8-bit general purpose AVR microcontroller made by Atmel. The special feature of the system is that it is based on an interrupt-powered engine and has no frame buffer. Functions such as video sync generation, tile rendering and music mixing are performed in real time in the software by a background task so that games can be easily developed in C.  
The design goal was to be as simple as possible while having good enough sound and graphics while leaving enough resources to implement interesting games. The emphasis was on making it easy and fun to assemble and program for all enthusiasts. The final design contains only two chips: an ATmega644 and an AD725 RGB to NTSC converter.

Many commercial versions are available or when available: Uzebox AVCore by Embedded Engineering llc and Fuzebox by Adafruit Industries. There is also the Uzebox deluxe kit and the EUzebox, a version with a SCART interface. Get one if you know nothing about electronics!

Features:

* Interruption: no cycle counting required, sound mixing and video generation are all done in the background
* 5-channel sound engine: the sound subsystem is composed of 3 wavetable channels, 1 noise and 1 PCM channel. The sound is 8-bit mono, mixed at ~ 15Khz
* 256 simultaneous colors arranged in a 3: 3: 2 color space (red: 3 bits, green: 3 bits, blue: 2 bits)
* Resolution: 9 video modes offering up to 360 x 224 pixels (mosaics only, mosaics and sprites and bitmap video modes)
* Full screen scrolling in some video modes
* Sprites: Up to 32 simultaneous sprites
* Supported inputs: two SNES compatible joypad inputs (including SNES mouse)
* MIDI In: With a music sequencer, allows the creation of music directly on the console
* SD / MicroSD API and FAT16
* Extension 'Uzenet' for wifi ESP8266 and 1 MB of RAM SPI
* PS / 2 keyboard interface
* GameLoader: 4K Bootloader that allows to flash games from a standard FAT16 formatted SD card
* Cross-platform emulator with GDB support for easy development
* Several tools to convert MIDI, audio and graphics files to include

Sources come with fully functional games, demos, content generation tools and even a cross-platform emulator!

## Emulators

[Libretro uzem](libretro-uzem)