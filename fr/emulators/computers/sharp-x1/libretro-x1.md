---
title: Libretro x1
description: 
published: true
date: 2024-06-23T11:54:20.618Z
tags: libretro, x1
editor: markdown
dateCreated: 2021-05-21T08:18:01.371Z
---



## ![](/emulators/license.svg) Licence

Ce core est sous licence [**BSD-3-Clause**](https://github.com/libretro/xmil-libretro/blob/master/LICENSE).

## ![](/emulators/compatibility.png) Compatibilité

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Fonctionnalités



## ![](/emulators/bios.svg) BIOS

### Liste des bios obligatoires

| Nom de fichier | Description | MD5 | Fourni |
| :---: | :---: | :---: | :---: |
| IPLROM.X1 | - | eeeea1cd29c6e0e8b094790ae969bfa7 59074727a953fe965109b7dbe3298e30 | ❌ |
| IPLROM.X1T | - | 56c28adcf1f3a2f87cf3d57c378013f5 | ❌ |

### Emplacement

Placez les BIOS comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 xmil
┃ ┃ ┃ ┃ ┣ 🗒 **IPLROM.X1**
┃ ┃ ┃ ┃ ┣ 🗒 **IPLROM.X1T**

## ![](/emulators/roms.png) Roms

### Extensions supportées

Les roms doivent avoir les extensions suivantes :

* .dx1
* .2d
* .2hd
* .tfd
* .d88
* .88d
* .hdm
* .xdf
* .dup
* .cmd
* .m3u
* .zip
* .7z

Ce système supporte les roms compressées au format .zip/.7z. Attention toutefois, il ne s'agit que d'une archive.

Les fichiers contenus dans les .zip/.7z doivent correspondre aux extensions citées précédemment.
De plus, chaque fichier .zip/.7z ne doit contenir qu'une seule rom compressée.

### Emplacement

Placez les roms comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 x1
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.zip**

>Les roms au format **TOSEC** sont vivement conseillées.
{.is-success}

>Pour plus d'information sur les roms, rendez-vous sur [ce tutoriel](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuration avancée de l'émulateur

>Pour pouvoir conserver vos configurations personnalisées lors d'une mise à jour, nous vous conseillons d'utiliser notre fonctionnalité [Surcharges de configuration](./../../../advanced-usage/configuration-override).
{.is-info}

### Accéder aux options

Vous pouvez configurer diverses options de deux façons différentes.

* Via le menu RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via le fichier `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Options du core

| Options | Valeur des options | Variable (pour surcharge) | Valeurs des variables |
| --- | --- | --- | --- |
| Resolution | `LOW` ✅ / `HIGH` | `X1_RESOLUTE` | `LOW` / `HIGH` |
| Boot media | `2HD` ✅ / `2D` | `X1_BOOTMEDIA` | `2HD` / `2D` |
| ROM type | `X1` ✅ / `TURBO` / `TURBOZ` | `X1_ROMTYPE` | `X1` / `TURBO` / `TURBOZ` |
| FPS | `AUTO` ✅ / `60` / `30` / `20` / `15` | `X1_FPS` | `AUTO` / `60` / `30` / `20` / `15` |
| Disp Vsync | `Disabled` ✅ / `Enabled` | `X1_DISPSYNC` | `OFF` / `ON` |
| Raster | `Disabled` ✅ / `Enabled` | `X1_RASTER` | `OFF` / `ON` |
| No wait | `Disabled` ✅ / `Enabled` | `X1_NOWAIT` | `OFF` / `ON` |
| Joy Reverse | `Disabled` ✅ / `Enabled` | `X1_BTN_MODE` | `OFF` / `ON` |
| Joy Rapid | `Disabled` ✅ / `Enabled` | `X1_BTN_RAPID` | `OFF` / `ON` |
| Audio sampling rate | `44100` ✅ / `22050` / `11025` | `X1_AUDIO_RATE` | `44100` / `22050` / `11025` |
| Seek Sound | `Disabled` ✅ / `Enabled` | `X1_SEEKSND` | `OFF` / `ON` |
| Scanlines in low-res | `Disabled` ✅ / `Enabled` | `X1_SCANLINES` | `OFF` / `ON` |
| Key mode | `Keyboard` ✅ / `JoyKey-1` / `JoyKey-2` / `Mouse-Key` | `X1_KEY_MODE` | `Keyboard` / `JoyKey-1` / `JoyKey-2` / `Mouse-Key` |
| FM Board | `Disabled` / `Enabled` ✅ | `X1_FMBOARD` | `OFF` / `ON` |
| Audio buffer (ms) | `100` / `150` / `200` / `250` ✅ / `300` / `350` / `500` / `750` / `1000` | `X1_AUDIO_DELAYMS` | `100` / `150` / `200` / `250` / `300` / `350` / `500` / `750` / `1000` |
| Cpu clock (MHz) | `1` / `2` / `3` / `4` ✅ / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` | `X1_CPU_CLOCK` | `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` |

## ![](/emulators/external-links.png) Liens externes

* **Code source utilisé** : [https://github.com/libretro/xmil-libretro/](https://github.com/libretro/xmil-libretro/)