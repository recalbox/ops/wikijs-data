---
title: MrBoom
description: 
published: true
date: 2024-07-26T08:30:01.886Z
tags: ports, mrboom
editor: markdown
dateCreated: 2023-09-04T12:43:20.996Z
---

![](/emulators/ports/mrboom.png){.align-center}

## Detalles técnicos

* **Desarrollador**: Javanaise

## Presentación

Mr.Boom es un clon de Bomberman para hasta 8 jugadores para LibRetro / RetroArch.

**Bomberman** es una serie de videojuegos de Hudson Soft en la que el jugador asume el papel de un artificiero, con el objetivo de hacer explotar a sus adversarios/enemigos para ganar. El juego ha tenido un gran éxito, especialmente gracias a su modo multijugador que, dependiendo de la máquina, permite jugar hasta a diez personas al mismo tiempo.

## Emuladores

[Libretro MrBoom](libretro-mrboom)