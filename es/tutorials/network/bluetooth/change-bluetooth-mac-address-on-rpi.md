---
title: Cambiar la dirección MAC del Bluetooth de vuestro Raspberry PI
description: 
published: true
date: 2023-09-01T14:37:45.179Z
tags: bluetooth
editor: markdown
dateCreated: 2023-09-01T14:37:45.179Z
---

## ¿Por qué?

En algunos casos, las Rasberry Pi pueden funcionar incorrectamente y se bloquean los puertos USB, lo que impide aparejar los mandos Bluetooth. Modificando la dirección MAC del Bluetooth, se pueden sincronizar mandos que han sido previamente sincronizados en otros sistemas.

## Instalar bdaddr

Conectaros vía [SSH](./../../../tutorials/system/access/root-access-terminal-cli) a vuestra Recalbox y ejecutad las siguientes órdenes:

```shell
wget https://www.tbit.com.br/files/static/bdaddr.gz
gunzip bdaddr.gz
mv bdaddr /bin
chmod +x /bin/bdaddr
```

## Modificar la dirección MAC

Ahora, podéis modificar la dirección MAC del Bluetooh con los comandos siguientes:

```shell
bdaddr -i hci0 -r B8:27:EB:00:00:00
hciconfig hci0 reset
```

## Aplicar la modificación en cada reinicio.

Modificad o cread un fichero de arranque en la siguiente ruta: `/recalbox/share/userscripts/custom.sh` con el contenido siguiente:

```bash
#!/bin/sh

sleep 2
/bin/bdaddr -i hci0 -r B8:27:EB:00:00:00
hciconfig hci0 reset
```
