---
title: Libretro QuickNES
description: 
published: true
date: 2024-07-24T15:06:34.577Z
tags: libretro, nes, quicknes
editor: markdown
dateCreated: 2021-08-01T21:03:17.389Z
---



## ![](/emulators/license.svg) License

This core is under [**LGPLv2.1+**](https://github.com/kode54/QuickNES/blob/master/COPYING) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Features

| Feature | Supported |
| :---: | :---: |
| Restart | ✔ |
| Screenshots | ✔ |
| Saves | ✔ |
| States | ✔ |
| Rewind | ✔ |
| Netplay | ✔ |
| Core Options | ✔ |
| RetroAchievements | ✔ |
| Controls | ✔ |
| Remapping | ✔ |
| Softpatching | ✔ |

## ![](/emulators/bios.svg) BIOS

>**No bios is required.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Supported extensions

Roms must have the extension:

* .nes
* .unf
* .unif
* .zip
* .7z

This system supports compressed roms in .zip/.7z format. However, be careful, it is only an archive.

The files contained in the .zip/.7z must correspond to the extensions mentioned above.
Moreover, each .zip/.7z file must contain only one compressed rom.

### Location

Put the roms like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 nes
┃ ┃ ┃ ┃ ┣ 🗒 **game.zip**

>The roms using **No-Intro** format are highly recommended.
{.is-success}

>For more information about roms, go to [this tutorial](./../../../tutorials/games/generalities/isos-and-roms)!
{.is-info}

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Accessing to options

You can configure various options in two different ways.

* Via RetroArch menu:

┣ 📁 RetroArch Menu
┃ ┣ 📁 Core options
┃ ┃ ┣ 🧩 Name_option

* Via the file `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core options

| Options | Options values | Variables (for configuration override) | Variables values |
| --- | --- | --- | --- |
| Aspect Ratio | `PAR` ✅ / `4:3` | `quicknes_aspect_ratio_par` | `PAR` / `4:3` |
| Show Horizontal Overscan | `Enabled` ✅ / `Disabled` | `quicknes_use_overscan_h` | `enabled` / `disabled` |
| Show Vertical Overscan | `Disabled` ✅ / `Enabled` | `quicknes_use_overscan_v` | `disabled` / `enabled` |
| Color Palette | `Default` ✅ / `AspiringSquire's Real` / `Nintendo Virtual Console` / `Nintenod RGB PPU` / `FBX's YUV-V3` / `FBX's Unsaturated-Final` / `Sony CXA2025AS US` / `PAL` / `BMF's Final 2` / `BMF's Final 3` / `FBX's Smooth` / `FBX's Composite Direct` / `FBX's PVM Style D93` / `FBX's NTSC Hardware` / `FBX's NES-Classic (fixed)` / `RGBSource's NESCAP` / `nakedarthur's Wavebeam` | `quicknes_palette` | `default` / `asqrealc` / `nintendo-vc` / `rgb` / `yuv-v3` / `unsaturated-final` / `sony-cxa2025as-us` / `pal` / `bmf-final2` / `bmf-final3` / `smooth-fbx` / `composite-direct-fbx` / `pvm-style-d93-fbx` / `ntsc-hardware-fbx` / `nes-classic-fbx-fs` / `nescap` / `wavebeam` |
| No Sprite Limit | `Disabled` ✅ / `Enabled` | `quicknes_no_sprite_limit` | `disabled` / `enabled` |
| Audio Mode | `Non-Linear` ✅ / `Linear` / `Stereo Panning` | `quicknes_audio_nonlinear` | `nonlinear` / `linear` / `stereo panning` |
| Audio Equalizer Preset | `Default` ✅ / `Famicom` / `TV` / `Flat` / `Crisp` / `Tinny` | `quicknes_audio_eq` | `default` / `famicom` / `tv` / `flat` / `crisp` / `tinny` |
| Turbo Enable | `None` ✅ / `Player 1` / `Player 2` / `Player 1 & 2` | `quicknes_turbo_enable` | `none` / `player 1` / `player 2` / `both` |
| Turbo Pulse Width (in frames) | `1` / `2` / `3` ✅ / `5` / `10` / `15` / `30` / `60` | `quicknes_turbo_pulse_width` | `1` / `2` / `3` / `5` / `10` / `15` / `30` / `60` |
| Allow Opposing Directions | `Disabled` ✅ / `Enabled` | `quicknes_up_down_allowed` | `disabled` / `enabled` |

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/libretro/QuickNES_Core/](https://github.com/libretro/QuickNES_Core/)
* **Libretro documentation**: [https://docs.libretro.com/library/quicknes/](https://docs.libretro.com/library/quicknes/)