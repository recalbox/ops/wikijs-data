---
title: Libretro Gearsystem
description: 
published: true
date: 2024-06-22T09:34:54.791Z
tags: libretro, gearsystem, sg-1000
editor: markdown
dateCreated: 2021-05-21T08:26:45.239Z
---

**Libretro Gearsystem** est un émulateur open source, multi-plateforme, Sega Mark III / Master System / Game Gear / SG-1000 / Othello Multivision écrit en C++.

* Noyau Z80 très précis, comprenant des opcodes et des comportements non documentés tels que les registres R et MEMPTR.
* Prise en charge de Multi-Mapper: cartouches SEGA, Codemasters, SG-1000 et ROM uniquement.
* Détection automatique de région: NTSC-JAP, NTSC-USA, PAL-EUR.
* Base de données interne pour la détection des rom
* Émulation VDP très précise, y compris la synchronisation et la prise en charge du mode SMS2 uniquement.
* Émulation audio à l'aide de SDL Audio et de la bibliothèque Sms_Snd_Emu.
* Prise en charge des économies de RAM alimentées par batterie.
* Enregistrer les états.
* Support de triche Game Genie et Pro Action Replay.
* Fonctionne sous Windows, Linux, Mac OS X, Raspberry Pi, iOS et en tant que noyau Libretro (RetroArch).

Le core Libretro Gearsystem a été créé par Ignacio Sanchez

## ![](/emulators/license.svg) Licence

Ce core est sous licence [**GPLv3**](https://github.com/drhelius/Gearsystem/blob/master/LICENSE).

## ![](/emulators/compatibility.png) Compatibilité

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Fonctionnalités

| Fonctionnalité | Supporté |
| :---: | :---: |
| Redémarrage | ✔ |
| Captures d'écran | ✔ |
| Sauvegardes | ✔ |
| Sauvegardes instantanées | ✔ |
| Rembobinage | ✔ |
| Options du core | ✔ |
| RetroAchievements | ✔ |
| Cheats RetroArch - Game Genie | ✔ |
| Cheats RetroArch - Pro Acion Replay | ✔ |
| Controls | ✔ |
| Remapping | ✔ |
| Softpatching | ✔ |

## ![](/emulators/bios.svg) BIOS

>**Aucun bios n'est requis.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Extensions supportées

Les roms doivent avoir l'extension suivante :

* .bin
* .sg
* .zip
* .7z

Ce système supporte les roms compressées au format .zip/.7z. Attention toutefois, il ne s'agit que d'une archive.

Les fichiers contenus dans les .zip/.7z doivent correspondre aux extensions citées précédemment.
De plus, chaque fichier .zip/.7z ne doit contenir qu'une seule rom compressée.

### Emplacement

Placez les roms comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 sg1000
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.zip**

>Les roms au format **No-Intro** sont vivement conseillées.
{.is-success}

>Pour plus d'information sur les roms, rendez-vous sur [ce tutoriel](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuration avancée de l'émulateur

>Pour pouvoir conserver vos configurations personnalisées lors d'une mise à jour, nous vous conseillons d'utiliser notre fonctionnalité [Surcharges de configuration](./../../../advanced-usage/configuration-override).
{.is-info}

### Accéder aux options

Vous pouvez configurer diverses options de deux façons différentes.

* Via le menu RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via le fichier `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Options du core

| Options | Valeur des options | Variable (pour surcharge) | Valeurs des variables |
| --- | --- | --- | --- |
| System (restart) | `Auto` ✅ / `Master System / Mark III` / `Game Gear` / `SG-1000 / Multivision` | `gearsystem_system` | `Auto` / `Master System / Mark III` / `Game Gear` / `SG-1000 / Multivision` |
| Region (restart) | `Auto` ✅ / `Master System Japan` / `Master System Export` / `Game Gear Japan` / `Game Gear Export` / `Game Gear International` | `gearsystem_region` | `Auto` / `Master System Japan` / `Master System Export` / `Game Gear Japan` / `Game Gear Export` / `Game Gear International` |
| Mapper (restart) | `Auto` ✅ / `ROM` / `SEGA` / `Codemasters` / `Korean` / `MSX` / `Janggun` / `SG-1000` | `gearsystem_mapper` | `Auto` / `ROM` / `SEGA` / `Codemasters` / `Korean` / `MSX` / `Janggun` / `SG-1000` |
| Refresh Rate (restart) | `Auto` ✅ / `NTSC (60 Hz)` / `PAL (50 Hz)` | `gearsystem_timing` | `Auto` / `NTSC (60 Hz)` / `PAL (50 Hz)` |
| Master System BIOS (restart) | `Désactivé` ✅ / `Activé` | `gearsystem_bios_sms` | `Disabled` / `Enabled` |
| Game Gear BIOS (restart) | `Désactivé` ✅ / `Activé` | `gearsystem_bios_gg` | `Disabled` / `Enabled` |
| 3D Glasses | `Both Eyes / OFF` ✅ / `Left Eye` / `Right Eye` | `gearsystem_glasses` | `Both Eyes / OFF` / `Left Eye` / `Right Eye` |
| Allow Up+Down / Left+Right | `Désactivé` ✅ / `Activé` | `gearsystem_up_down_allowed` | `Disabled` / `Enabled` |

## ![](/emulators/external-links.png) Liens externes

* **Code source utilisé** : [https://github.com/drhelius/Gearsystem/](https://github.com/drhelius/Gearsystem/)
* **Documentation Libretro** : [https://docs.libretro.com/library/gearsystem/](https://docs.libretro.com/library/gearsystem/)