---
title: Codes de triche
description: 
published: true
date: 2025-02-08T18:57:49.542Z
tags: fbneo, codes, finalburn, neo, triche
editor: markdown
dateCreated: 2021-06-05T07:04:06.912Z
---

## Introduction

Avec FinalBurn Neo, il est possible d'ajouter des codes de triche.

## Utilisation

* Téléchargez l'archive des [codes de triche](https://github.com/finalburnneo/FBNeo-cheats/archive/master.zip).
* Décompressez l'archive.
* Placez le répertoire décompressé `cheats` dans le répertoire `/recalbox/share/bios/fbneo`.

Pour accéder aux codes de triche, pendant le jeu :

* Allez dans le menu de RetroArch en faisant `Hotkey + B`.
* Allez dans `Options de cœur`.

![](/tutorials/games/guides/fbneo/fbneocheats1.png){.full-width}

* Tout en bas de l'écran des options, allez dans `Cheat`.

![](/tutorials/games/guides/fbneo/fbneocheats2.png){.full-width}

* Dans la liste des options, vous y trouverez la liste des codes de triche disponibles pour votre jeu.

![](/tutorials/games/guides/fbneo/fbneocheats3.png){.full-width}