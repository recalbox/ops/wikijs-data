---
title: Configuración de Recalbox para Bartops / Máquinas recreativas
description: 
published: true
date: 2023-09-14T06:29:20.364Z
tags: arcade, bartop, recreativas, máquinas recreativas
editor: markdown
dateCreated: 2023-09-14T06:29:20.364Z
---

Podéis configurar Recalbox para crear un « **Bartop** » o una **máquina recreativa (arcade)**.

## GPIO

El driver GPIO de Recalbox se configura dentro del fichero [recalbox.conf.](./../../../basic-usage/getting-started/recalboxconf-file). Este driver crea dos joysticks controlados directamente desde los PINS GPIO del Raspberry Pi.

Para ello, no hace falta ningún hub USB ya que los botones se conectan directamente en los [pins GPIO](./../../../tutorials/controllers/gpio/gpio-controllers)

## Configuración vídeo

Muchas bartop utilizan viejas pantallas LCD o pantallas de TV CRT que hacen uso de las siguientes conexiones:

* **Pantalla VGA**: necesitaréis un conversor HDMI / VGA (conversor activo) que cuesta unos 10€. Si estáis usando una pantalla 4/3 necesitaréis cambiar el modo de video a `global.videomode=default` para todos tus emuladores en el fichero [recalbox.conf](./../../../basic-usage/getting-started/recalboxconf-file).
* **Pantalla DVI**: necesitaréis un adaptador HDMI / DVI (conversor pasivo). Es probable que tengáis que modificar el fichero [recalbox.conf](./../../../basic-usage/getting-started/recalboxconf-file) basado en [este tutorial](./../../../tutorials/video/lcd/dvi-screen).
* **Pantalla CRT** : necesitaréis un conector jack a RCA con un cable de vídeo. Necesitaréis también cambiar el modo de video a `global.videomode=default` para todos los juegos en el fichero [recalbox.conf](./../../../basic-usage/getting-started/recalboxconf-file) y cambiar los ajustes descritos en [este tutorial](./../../tutorials/video/crt/crt-screen-with-compostite).

## Configuración audio

Podéis seleccionar el periférico de salida audio desde el fichero [recalbox.conf](./../../../basic-usage/getting-started/recalboxconf-file) :

* Utilizad `audio.device=alsa_card.0:analog-output` si queréis forzar la **salida audio sobre el conector jack.** 
* Utilizad `audio.device=alsa_card.1:hdmi-output-0` si queréis forzar la **salida audio sobre el HDMI.**

También podéis cambiar la salida audio desde EmulationStation. Pulsad `START` > `OPCIONES DE JUEGOS` y modificad el valor de la opción `SALIDA AUDIO`.

## Configuración del menú

Podéis desactivar Kodi desde el fichero [recalbox.conf](./../../../basic-usage/getting-started/recalboxconf-file) mediante el parámetro `kodi.enable=0` para suprimir el atajo de lanzamiento a través del botón "X".

También podeis querer cambiar la configuración del menú:

* **Para EmulationStation** :
  * Indicando `system.es.menu=bartop`, se presentará un menú minimal al pulsar el botón `START`.
  * Indicando `system.es.menu=none`, no se podrá acceder a ningún menú. 
* **Para los emuladores** :
  * Poniendo `system.emulators.specialkey` a `nomenu` , desactivaréis los menús dentro de los emuladores.
  * Poniendo `system.emulators.specialkey` a `none` , desactivaréis los menús y todas las funciones especiales de los emuladores (excepto la combinación para salir de los juegos).