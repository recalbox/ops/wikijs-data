---
title: Segunda pantalla miniTFT para los scrapes y los vídeos
description: 
published: true
date: 2023-09-14T15:07:48.192Z
tags: tft, minitft, scrap, videos, mini, pantalla
editor: markdown
dateCreated: 2023-09-14T15:07:48.192Z
---

Esta página le mostrará cómo configurar su segunda pantalla TFT para mostrar raspados y vídeos. Esto es para las personas que tienen una caja OGST o una pequeña pantalla TFT.

## Introducción

Recalbox soporta casi nativamente la visualización de videos y scrapes de sistemas emulados, imágenes y videos de juegos instalados (cuando son scrapeados) en una segunda pantalla conectada al puerto GPIO (SPI/DPI, pantalla OGST de Odroid).

## ¿Qué hace este script?

La operación de visualización funciona así:

* Cuando la interfaz está en los sistemas disponibles, un video de arranque de la consola/sistema es visible si existe, de lo contrario un video por defecto es visible.
* Cuando la interfaz está en la lista de juegos: 
  * En el caso de los juegos raspados, la imagen del juego aparece en la pantalla TFT.
  * En el caso de vídeos e imágenes de juegos scrapeados, la pantalla TFT mostrará la imagen del juego durante 1 segundo y el vídeo del juego se mostrará en bucle. 
* Cuando se inicia el juego: 
  * En el caso de juegos scrapeados, la imagen del juego aparecerá en la pantalla TFT. 
  * En el caso de vídeos e imágenes de juegos scrapeados, la pantalla TFT mostrará el vídeo del juego y después la imagen del juego.
* Cuando Recalbox muestra el salvapantallas, la pantalla TFT se apaga, y una vez fuera del salvapantallas, se vuelve a encender. Esto se puede utilizar en pantallas SPI en la Raspberry Pi.

## Instalación de una pantalla TFT SPI de 2.8 

Una pantalla como [2.8" 240x320 SPI TFT LCD display module serial port module with ILI9341 5V / 3.3V PCB](https://www.amazon.fr/dp/B07MXH92RL/) irá muy bien.

Este tipo de pantalla no es cara (< 15€). La conexión al puerto GPIO es la siguiente:

(falta la imagen, si tienes este hardware y tienes una imagen del cableado, ¡puedes insertarla aquí!)

### Diagrama de cableado

|  Módulo TFT |  Raspberry Pi |
| :--- | :--- |
|  VCC |  +3,3V (pin 1) |
|  GND |  GND (pin 6) |
|  CS |  SPI0 CE0 (pin 24) |
|  RESET |  GPIO 25 (pin 22) |
|  DC |  GPIO 24 (pin 18) |
|  SDA |  SPI0 MOSI (pin 19) |
|  SCL |  SPI0 SCLK (pin 23) |
|  LED |  GPIO 18 (pin 12) |
|  SDO |  SPI0 MISO (pin 21) |

### Esquema eléctrico

(falta imagen, si tiene este equipo y dispone de una imagen del cableado, puede insertarla aquí)

### Configuración

El soporte para la segunda mini pantalla TFT está incorporado en Recalbox 7. Aún necesitas configurar la mini pantalla TFT Arduino específicamente. Para ello, es necesario modificar el archivo `recalbox-user-config.txt` como se muestra a continuación para añadir soporte para la pantalla a través de una superposición.

#### Configuración

* Abrid [recalbox-user-config.txt](./../../../tutorials/system/modification/configtxt-configuration) con nano o vim y añadid:

```ini
dtparam=spi=on
#small tft spi 2.8 adafruit type (for arduino)
dtoverlay=rpi-display,reset=25,dc=24,led=18,rotate=90,speed=48000000
```

El soporte del bus SPI está habilitado y la pantalla está configurada a través del overlay apropiado. Todo lo que necesitas hacer ahora es configurar el archivo [recalbox.conf] (./../../../basic-usage/getting-started/recalboxconf-file) para habilitar el soporte de pantalla. Para hacer esto, configure las opciones como sigue:

```ini
#Second miniTFT screen as marquee to display scrapes and videos
#Enable it with system.secondMiniTFT.enabled=1, disable with system.secondMiniTFT.enabled=0
 
system.secondminitft.enabled=1
 
#Type of mini TFT : OGST, unknown
#for Support of XU4 OGSTCase mini screen, just configure it secondminitft.type=ogst
#Most of spi tft screen are enabled thanks to overlays in /boot/config.txt
#please check the specific configuration of your screen
#Some examples are available on the page .....
# values are :
# - overlay : screen configured through overlays in /boot/config.txt
# - ogst : screen of the XU4 OGST case. Supported out of the box
# - default : the rest of the world
 
system.secondminitft.type=overlay
 
#Choose the resolution of your screen
#miniTFT resolution  320x240 -> 240p, 480x320 -> 320p
 
system.secondminitft.resolution=240p

#Scraped image aspect on tft screen : fbv display option
# options available : stretch, ignore-aspect, enlarge.
# stretch : Strech (using a simple resizing routine) the image to fit onto screen if necessary
# alpha : use the alpha channel (if applicable)
# ignore-aspect : Ignore the image aspect while resizing
# enlarge : Enlarge the image to fit the whole screen if necessary

# 1 = enabled, 0 = disabled

system.secondminitft.imagestretchenabled=1
system.secondminitft.imageenlargeenabled=0
system.secondminitft.imagealphaenabled=1
system.secondminitft.imageignoreaspectenabled=1

#When activating the second TFT screen, you may want to display
#game scraped video only on the second screen. This variable
#allows you to disable scraped video playing in ES during game browsing
#system.secondminitft.disablevideoines=1 disable video playing in ES
#system.secondminitft.disablevideoines=0 enable video playing in ES

system.secondminitft.disablevideoines=1

#if the backlight of your your screen can be manage by PWM, 
#indicates the  GPIO value of your PWM control
#WiringPi and its gpio program will be used to control the backlight
#when RB goes in SLEEP mode, and when it wakes up.
#On raspberry pi, PWMs cannot be used simultaneously with Jack sound output.
#If your are using HDMI or a Audio hat you can use backlight PWM control for the second screen
#If you are using Jack output please leave commented.
#The Screen will be switch down in Sleep mode, and switch on when it wakes up.
#On OGST, only this mode is available.

;system.secondminitft.backlightcontrol=18
```

Normalmente, todo va bien. Reinicia y tu pantalla debería mostrar las imágenes y vídeos de los juegos raspados.

Puedes ver las pruebas de desarrollo en una Pi3 en [este vídeo](https://www.youtube.com/watch?v=vX4hZjsDEVY).

Aquí puedes ver cómo GameMakers utiliza esta función en una de sus creaciones: [un mini New Astro City](https://www.youtube.com/watch?v=Axzaw6IRtcI).

## Waveshare TFT Screen 3.5" RevA et Rev B

Esta pantalla se puede encontrar [aquí](https://fr.aliexpress.com/item/32284958830.html).

### Cableado eléctrico

|  PIN NO. |  SIMBOLO |  DESCRIPCION |
| :--- | :--- | :--- |
|  1, 17 |  3.3V |  Power positive (3.3V power input) |
|  2, 4 |  5V |  Power positive (5V power input) |
|  3, 5, 7, 8, 10, 12, 13, 15, 16 |  NC |  NC |
|  6, 9, 14, 20, 25 |  GND |  Ground |
|  11 |  TP_IRQ |  Touch Panel interrupt, low level while the Touch Panel detects touching |
|  18 |  LCD_RS |  Command/data register selection |
|  19 |  LCD_SI / TP_SI |  LCD display/SPI data input of Touch Panel |
|  21 |  TP_SO |  SPI data output of Touch Panel |
|  22 |  RST |  Reset |
|  23 |  LCD_SCK / TP_SCK |  LCD display/SPI clock of Touch Panel |
|  24 |  LCD_CS |  LCD chip selection, low active |
|  26 |  TP_CS |  Touch Panel chip selection, low active |

### Configuración

El procedimiento de configuración es idéntico al de la pantalla Arduino de 2.8", pero los parámetros son diferentes.

El soporte para la segunda mini pantalla TFT está incorporado en recalbox 7.0. Aún es necesario configurar la mini pantalla TFT de Waveshare (rev A/B) específicamente. Para hacer esto, necesita modificar el archivo `/boot/rcalbox-user-config.txt` como se muestra a continuación para agregar soporte para la pantalla a través de un overlay.

#### Instalación

* Abrid [recalbox-user-config.txt](./../../../tutorials/system/modification/configtxt-configuration) con nano o vim y añadid:

```ini
#minitft 3.5 waveshare
dtparam=spi=on
dtoverlay=waveshare35a:rotate=270,speed=27000000 #for revA
dtoverlay=waveshare35b:rotate=270,speed=27000000 #for revB
```

El soporte del bus SPI está habilitado y la pantalla está configurada a través del overlay apropiado. Todo lo que necesitas hacer ahora es configurar el archivo `recalbox.conf` para habilitar el soporte de pantalla. Para ello, configure las opciones de la siguiente manera:

```ini
#Second miniTFT screen as marquee to display scrapes and videos
#Enable it with system.secondMiniTFT.enabled=1, disable with system.secondMiniTFT.enabled=0
 
system.secondminitft.enabled=1
 
#Type of mini TFT : OGST, unknown
#for Support of XU4 OGSTCase mini screen, just configure it secondminitft.type=ogst
#Most of spi tft screen are enabled thanks to overlays in /boot/config.txt
#please check the specific configuration of your screen
#Some examples are available on the page .....
# values are :
# - overlay : screen configured through overlays in /boot/config.txt
# - ogst : screen of the XU4 OGST case. Supported out of the box
# - default : the rest of the world
 
system.secondminitft.type=overlay
 
#Choose the resolution of your screen
#miniTFT resolution  320x240 -> 240p, 480x320 -> 320p
 
system.secondminitft.resolution=320p

#Scraped image aspect on tft screen : fbv display option
# options available : stretch, ignore-aspect, enlarge.
# stretch : Strech (using a simple resizing routine) the image to fit onto screen if necessary
# alpha : use the alpha channel (if applicable)
# ignore-aspect : Ignore the image aspect while resizing
# enlarge : Enlarge the image to fit the whole screen if necessary

# 1 = enabled, 0 = disabled

system.secondminitft.imagestretchenabled=1
system.secondminitft.imageenlargeenabled=0
system.secondminitft.imagealphaenabled=1
system.secondminitft.imageignoreaspectenabled=1

#When activating the second TFT screen, you may want to display
#game scraped video only on the second screen. This variable
#allows you to disable scraped video playing in ES during game browsing
#system.secondminitft.disablevideoines=1 disable video playing in ES
#system.secondminitft.disablevideoines=0 enable video playing in ES

system.secondminitft.disablevideoines=1

#if the backlight of your your screen can be manage by PWM, 
#indicates the  GPIO value of your PWM control
#WiringPi and its gpio program will be used to control the backlight
#when RB goes in SLEEP mode, and when it wakes up.
#On raspberry pi, PWMs cannot be used simultaneously with Jack sound output.
#If your are using HDMI or a Audio hat you can use backlight PWM control for the second screen
#If you are using Jack output please leave commented.
#The Screen will be switch down in Sleep mode, and switch on when it wakes up.
#On OGST, only this mode is available.

;system.secondminitft.backlightcontrol=18
```

Normalmente, todo va bien. Reinicia y tu pantalla debería mostrar las imágenes y vídeos de los juegos raspados.

## Adafruit TFT Screen 3.5"

Esta pantalla se puede ecnontrar [aquí](https://www.adafruit.com/product/2097) y es bastante cara.

### Cableado eléctrico

Podéis consultar [esta página](https://pinout.xyz/pinout/pitft_plus_35).

### Configuración

El procedimiento de configuración es idéntico al de la pantalla Arduino de 2.8", pero los parámetros son diferentes.

El soporte para la segunda mini pantalla TFT está incorporado en recalbox 7.0. Aún es necesario configurar la mini pantalla TFT de Adafruit específicamente. Para ello, es necesario editar el archivo `/boot/config.txt` como se muestra a continuación para añadir soporte para la pantalla a través de una superposición.

#### Instalación

Abrid el fichero [recalbox-user-config.txt](./../../../tutorials/system/modification/configtxt-configuration) con nano o vim y añadid:

```ini
#minitft 3.5 adafruit
dtparam=spi=on
dtoverlay=pitft35-resistive,rotate=270,fps=30
```

El soporte del bus SPI está habilitado y la pantalla está configurada a través del overlay apropiado. Todo lo que necesitas hacer ahora es configurar el archivo [recalbox.conf] (./../../../basic-usage/getting-started/recalboxconf-file) para habilitar el soporte de pantalla. Para hacer esto, configure las opciones como sigue:

```ini
#Second miniTFT screen as marquee to display scrapes and videos
#Enable it with system.secondMiniTFT.enabled=1, disable with system.secondMiniTFT.enabled=0
 
system.secondminitft.enabled=1
 
#Type of mini TFT : OGST, unknown
#for Support of XU4 OGSTCase mini screen, just configure it secondminitft.type=ogst
#Most of spi tft screen are enabled thanks to overlays in /boot/config.txt
#please check the specific configuration of your screen
#Some examples are available on the page .....
# values are :
# - overlay : screen configured through overlays in /boot/config.txt
# - ogst : screen of the XU4 OGST case. Supported out of the box
# - default : the rest of the world
 
system.secondminitft.type=overlay
 
#Choose the resolution of your screen
#miniTFT resolution  320x240 -> 240p, 480x320 -> 320p
 
system.secondminitft.resolution=320p

#Scraped image aspect on tft screen : fbv display option
# options available : stretch, ignore-aspect, enlarge.
# stretch : Strech (using a simple resizing routine) the image to fit onto screen if necessary
# alpha : use the alpha channel (if applicable)
# ignore-aspect : Ignore the image aspect while resizing
# enlarge : Enlarge the image to fit the whole screen if necessary

# 1 = enabled, 0 = disabled

system.secondminitft.imagestretchenabled=1
system.secondminitft.imageenlargeenabled=0
system.secondminitft.imagealphaenabled=1
system.secondminitft.imageignoreaspectenabled=1

#When activating the second TFT screen, you may want to display
#game scraped video only on the second screen. This variable
#allows you to disable scraped video playing in ES during game browsing
#system.secondminitft.disablevideoines=1 disable video playing in ES
#system.secondminitft.disablevideoines=0 enable video playing in ES

system.secondminitft.disablevideoines=1

#if the backlight of your your screen can be manage by PWM, 
#indicates the  GPIO value of your PWM control
#WiringPi and its gpio program will be used to control the backlight
#when RB goes in SLEEP mode, and when it wakes up.
#On raspberry pi, PWMs cannot be used simultaneously with Jack sound output.
#If your are using HDMI or a Audio hat you can use backlight PWM control for the second screen
#If you are using Jack output please leave commented.
#The Screen will be switch down in Sleep mode, and switch on when it wakes up.
#On OGST, only this mode is available.

;system.secondminitft.backlightcontrol=18
```

Normalmente, todo va bien. Reinicia y tu pantalla debería mostrar las imágenes y vídeos de los juegos raspados.

## ODROID Game Station Turbo (OGST XU4)

### ¿Qué es la pantalla TFT OGST?

https://www.hardkernel.com/shop/ogst-gaming-console-kit-for-xu4/

Con la caja OGST, tendrá que modificar dos archivos de configuración: 
* `SHARE:\system\recalbox.conf` (`/recalbox/share/system/recalbox.conf` bajo SSH) y 
* `RECALBOX\config.ini` (`/boot/config.ini` bajo SSH).

### Configuración

1) Opcionalmente, sólo para sistemas actualizados desde una versión <= 7.2.1. Para otros sistemas recién flasheados (>= 7.2.2) puede proceder directamente al paso 2.
Necesitará copiar 2 ficheros antes de proceder al siguiente paso: en la partición RECALBOX, copie el fichero `boot.ini.sample` a `boot.ini`. Ten cuidado de comprobar la configuración de la pantalla (las líneas que empiezan por setenv). En general, todo lo que necesitas hacer es quitar el # delante de la resolución que deseas. (Ten en cuenta que puedes comentar todas las opciones setenv videoconfig, y el XU4 tomará la mejor resolución por sí mismo).  
También copie el archivo `config.ini.sample` a `config.ini`, todavía en la partición RECALBOX (`/boot`).
A continuación se muestran los comandos a ejecutar en SSH:

```
mount -o remount,rw /boot
cp /boot/boot.ini.sample /boot/boot.ini
cp /boot/config.ini.sample /boot/config.ini
```

2) Para esta configuración, es necesario abrir el archivo `config.ini` en la partición RECALBOX.

Una vez abierto, busque la siguiente línea:

```ini
;overlay_profile=hktft_cs_ogst
```

Elimine el `;` al principio de la línea y guarde. Ahora debería tener este contenido:

```ini
[generic]
; Device Tree Overlay
overlay_resize=16384
; uncomment next line to enable Odroid XU4 OGST
overlay_profile=hktft_cs_ogst
overlays="spi0 i2c0 i2c1 uart0"
 
[overlay_hktft_cs_ogst]
overlays="hktft-cs-ogst i2c0"
```

Aquí están los comandos SSH a ejecutar:

```
mount -o remount,rw /boot
sed -i '/overlay_profile=hktft_cs_ogst/s/^;//' /boot/config.ini
```

3) Ahora hay que hacer algunos cambios en el archivo `system/recalbox.conf`, y esto se puede hacer accediendo a la tarjeta SD desde Windows (partición SHARE). También es posible hacer estos cambios utilizando el gestor o SSH.

La configuración se realiza con las siguientes líneas en el fichero [recalbox.conf](./../../../basic-usage/getting-started/recalboxconf-file) :

```ini
#Second miniTFT screen as marquee to display scrapes and videos
#Enable it with system.secondMiniTFT.enabled=1, disable with system.secondMiniTFT.enabled=0

#this configuration is REQUIRED 
system.secondminitft.enabled=1
 
#Type of mini TFT : OGST, unknown
#Most of spi tft screen are enabled thanks to overlays in /boot/config.txt in rpi and /boot/config.ini on odroidxu4
#please check the specific configuration of your screen
#Some examples are available on the page .....
# values are :
# - overlay : screen configured through overlays in /boot/config.txt or /boot/config.ini (odroidxu4/ogst)
# - default : the rest of the world
#this configuration is REQUIRED if system.secondminitft.enabled=1
system.secondminitft.type=overlay
 
#Choose the resolution of your screen
#miniTFT resolution  320x240 -> 240p, 480x320 -> 320p

#this configuration is REQUIRED if system.secondminitft.enabled=1
system.secondminitft.resolution=240p

#Scraped image aspect on tft screen : fbv display option
# options available : stretch, ignore-aspect, enlarge.
# stretch : Strech (using a simple resizing routine) the image to fit onto screen if necessary
# alpha : use the alpha channel (if applicable)
# ignore-aspect : Ignore the image aspect while resizing
# enlarge : Enlarge the image to fit the whole screen if necessary

# 1 = enabled, 0 = disabled

#this configuration is REQUIRED if system.secondminitft.enabled=1
system.secondminitft.imagestretchenabled=1
system.secondminitft.imageenlargeenabled=0
system.secondminitft.imagealphaenabled=1
system.secondminitft.imageignoreaspectenabled=1

#When activating the second TFT screen, you may want to display
#game scraped video only on the second screen. This variable
#allows you to disable scraped video playing in ES during game browsing
#system.secondminitft.disablevideoines=1 disable video playing in ES
#system.secondminitft.disablevideoines=0 enable video playing in ES

#this configuration is OPTIONAL if system.secondminitft.enabled=1
;system.secondminitft.disablevideoines=1

#if the backlight of your your screen can be manage by PWM, 
#indicates the  GPIO value of your PWM control
#WiringPi and its gpio program will be used to control the backlight
#when RB goes in SLEEP mode, and when it wakes up.
#On raspberry pi, PWMs cannot be used simultaneously with Jack sound output.
#If your are using HDMI or a Audio hat you can use backlight PWM control for the second screen
#If you are using Jack output please leave commented.
#The Screen will be switch down in Sleep mode, and switch on when it wakes up.
#On OGST, only this mode is available.

#this configuration is OPTIONAL if system.secondminitft.enabled=1
;system.secondminitft.backlightcontrol=18

```

4) Reiniciad el sistema.

### Odroid XU4 OGST Solución de problemas

#### La pantalla no se enciende

Primero, comprueba los archivos `/boot/config.ini` y `recalbox.conf`. Los dos archivos de la partición de arranque **deberían** llamarse `boot.ini` y `config.ini`, no `boot.ini.sample` y `config.ini.sample`. (Windows puede ocultar la extensión del archivo).

Si el XU4 arranca desde una eMMC, puede que necesites actualizar la partición de arranque de la MMC. Ejecute estos comandos para hacerlo:

```
echo 0 > /sys/block/mmcblk0boot0/force_ro
dd if=/dev/mmcblk0 of=/dev/mmcblk0boot0 bs=512 count=8192 skip=1
echo 1 > /sys/block/mmcblk0boot0/force_ro
reboot
```