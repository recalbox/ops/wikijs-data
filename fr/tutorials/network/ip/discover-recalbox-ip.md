---
title: Trouver l'adresse IP d'une Recalbox
description: Toutes les astuces pour trouver l'adresse de votre Recalbox
published: true
date: 2023-07-27T07:04:20.054Z
tags: ip, réseau, adresse, administration
editor: markdown
dateCreated: 2021-05-21T08:38:50.833Z
---

## Vérifier la connectivité

Avant toute chose votre Recalbox doit connectée au réseau, soit à l'aide d'un câble Ethernet soit à l'aide d'un dongle wifi.

Votre Recalbox est à priori connectée mais vous ne savez pas vraiment si elle est connectée au réseau ? 

Pour le savoir rien de plus simple, appuyez sur `START` puis sélectionnez `MISES A JOUR` et enfin `LANCER LA MISE A JOUR` :
- Si un message s'affiche indiquant `Une mise à jour est disponible` ou `Aucune mise à jour de disponible` : votre Recalbox est connectée à internet donc au réseau. 
- Si le message `Veuillez brancher un cable réseau` s'affiche : votre Recalbox n'est pas connectée au réseau, il peut s'agir d'un problème de configuration de votre routeur, d'un câble défectueux, du dongle wifi qui ne fonctionne pas, etc.

>Généralement l'adresse IP n'est pas fixe et est suceptible de changer à chaque redémarrage.   
>Pour éviter cela vous pouvez en définir une adresse IP fixe au niveau de votre routeur, vous pouvez également [définir une adresse IP en définissant manuellement les paramètres réseau](./../../../tutorials/network/ip/static-manual-ip).
{.is-warning}

## Identifier l'adresse IP de votre Recalbox



Appuyez sur `START` et allez dans `OPTIONS RÉSEAU`, l'adresse IP de votre Recalbox s'affiche ici, il s'agit généralement d'une adresse ayant la forme `192.168.0.XX`.

## Depuis votre ordinateur

## {.tabset}

### Windows

* Dans la barre des tâches, vous disposez d'un champ de recherche, saisissez `cmd` et appuyez sur `Entrée`.
* Une invite de commande s'ouvre ensuite tapez `ping recalbox` et appuyez sur `Entrée`.
* Vous obtiendrez le message ci-dessous faisant référence à l'adresse IP de votre Recalbox (192.168.0.XX):

```dos
> Envoi d'une requête 'ping' sur RECALBOX avec 32 octets de données :  
> Réponse de 192.168.0.XX : octets=32 temps=1 ms TTL=128  
> Réponse de 192.168.0.XX : octets=32 temps=1 ms TTL=128  
> Réponse de 192.168.0.XX : octets=32 temps=1 ms TTL=128  
> Statistiques Ping pour [...]
```

### macOS

* Allez dans `Applications/Utilitaires/Terminal`.
* Une fenêtre du terminal s'exécute, tapez `arp -a` et appuyez sur `Entrée`.
* La liste des adresses IP utilisées par les appareils connectés au réseau s'affiche, vous y trouverez l’adresse IP de votre Recalbox (devant avoir la forme `192.168.0.XX`).

## Autres solutions

* Des applications smartphones gratuites permettent de scanner votre réseau pour identifier les apparails connectés. Rendez-vous sur le store de votre choix et lancez une recherche.
* Rendez-vous sur le panneau d'administration de votre box ou de votre routeur, habituellement ces outils proposent une interface listant tous les appareils connectés au réseau.