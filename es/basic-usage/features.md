---
title: Funcionalidades
description: 
published: true
date: 2023-08-17T15:10:02.272Z
tags: funcionalidades
editor: markdown
dateCreated: 2023-08-17T15:10:02.272Z
---

En esta sección de la documentación podéis consultar las funcionalidades de base que os permitirán de utilizar Recalbox de la forma más sencilla posible.