---
title: Pico-8
description: 
published: true
date: 2024-07-24T20:54:26.610Z
tags: fantasy, pico-8, 7.2+
editor: markdown
dateCreated: 2021-06-30T09:09:47.124Z
---

![](/emulators/fantasy/pico8.svg){.align-center}

## Technical data

* **Display:** 128x128 16 colors
* **Cartiridge size:** 32k
* **Sound:** 4 channels chip blerps
* **Code:** Lua
* **Sprites:** 256 8x8 sprites
* **Map:** 128x32 cels

## Presentation

**Pico-8** is a virtual machine, developed by **Lexaloffle Games**. Its goal is to offer a fantasy console, on which it is possible to create, share and play small video games and other computer programs.

It looks like an ordinary console, but runs on Windows / Mac / Linux.

The development is done through a Lua environment, in which the user can create the different elements of the game (maps, sounds, sprites, ...).

Pico-8 has been used by veteran creators, but also by non-professionals of the video game.

The graphic display is limited to 16 colors, 128x128 pixels, which easily gives the created games a minimalist and retro look.

When you turn it on, the machine greets you with a command line, a suite of cartridge creation tools and an online cartridge browser called SPLORE.

## Emulators

[Libretro Fake08](libretro-fake08)
[Libretro Retro8](libretro-retro8)