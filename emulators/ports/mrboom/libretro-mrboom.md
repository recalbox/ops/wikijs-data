---
title: Libretro MrBoom
description: 
published: true
date: 2025-01-04T06:27:06.784Z
tags: libretro, mrboom
editor: markdown
dateCreated: 2021-06-29T22:10:24.106Z
---

Libretro MrBoom is a core to play a clone of Bomberman up to 8 players.

## ![](/emulators/license.svg) License

This core is under [**MIT**](https://github.com/libretro/mrboom-libretro/blob/master/LICENSE) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Features

| Feature | Supported |
| :---: | :---: |
| States | ✔ |
| Rewind | ✔ |
| Netplay | ✔ |
| Core Options | ✔ |
| Controllers | ✔ |

## ![](/emulators/bios.svg) BIOS

>**No bios is required.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Supported extensions

The Mr.Boom core does not involve the use of extensions. Just load and start the core.

### Location

Put your roms like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 ports
┃ ┃ ┃ ┃ ┣ 📁 mrboom
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **mrboom.game**

This rom is included in your Recalbox.

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Accessing to options

You can configure various options in two different ways.

* Via RetroArch menu:

┣ 📁 RetroArch Menu
┃ ┣ 📁 Core options
┃ ┃ ┣ 🧩 Name_option

* Via the file `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core options

| Options | Options values | Variables (for configuration override) | Variables values |
| --- | --- | --- | --- |
| Team mode | `Selfie` ✅ / `Color` / `Sex` / `Skynet` | `mrboom-teammode` | `Selfie` / `Color` / `Sex` / `Skynet` |
| Monsters | `Enabled` ✅ / `Disabled` | `mrboom-nomonster` | `ON` / `OFF` |
| Level select | `Normal` ✅ / `Candy` / `Penguins` / `Pink` / `Jungle` / `Board` / `Soccer` / `Sky` / `Aliens` / `Random` | `mrboom-levelselect` | `Normal` / `Candy` / `Penguins` / `Pink` / `Jungle` / `Board` / `Soccer` / `Sky` / `Aliens` / `Random` |
| Aspect ratio | `Native` ✅ / `4:3` / `16:9` | `mrboom-aspect` | `Native` / `4:3` / `16:9` |
| Music volume | `0` / `5` / `10` / `15` / `20` / `25` / `30` / `35` / `40` / `45` / `50` / `55` / `60` / `65` / `70` / `75` / `80` / `85` / `90` / `95` / `100` ✅ | `mrboom-musicvolume` | `0` / `5` / `10` / `15` / `20` / `25` / `30` / `35` / `40` / `45` / `50` / `55` / `60` / `65` / `70` / `75` / `80` / `85` / `90` / `95` / `100` |
| Sfx volume | `0` / `5` / `10` / `15` / `20` / `25` / `30` / `35` / `40` / `45` / `50` ✅ / `55` / `60` / `65` / `70` / `75` / `80` / `85` / `90` / `95` / `100` | `mrboom-sfxvolume` | `0` / `5` / `10` / `15` / `20` / `25` / `30` / `35` / `40` / `45` / `50` / `55` / `60` / `65` / `70` / `75` / `80` / `85` / `90` / `95` / `100` |

## ![](/emulators/external-links.png) External links

* **Source code used** : [https://github.com/libretro/mrboom-libretro/](https://github.com/libretro/mrboom-libretro)
* **Official website** : [http://mrboom.mumblecore.org/](http://mrboom.mumblecore.org/)