---
title: Solarus
description: Arpg Game Engine
published: true
date: 2023-08-06T10:12:46.018Z
tags: solarus, engines
editor: markdown
dateCreated: 2021-06-30T09:11:39.896Z
---

![](/emulators/fantasy/solarus.svg){.align-center}

## Technical data

* **Manufacturer**: Solarus
* **Date of release**: September 19, 2020

## Presentation

**Solarus** is a lightweight, free and open-source 2D Action-RPG game engine specifically designed with the cult Action-RPGs of the 2D era in mind, such as **The Legend of Zelda: A Link to the Past** and **Secret of Mana** on Super Nintendo, or **Sunshine** on Sega Megadrive/Genesis.

## Emulators

[Solarus](solarus)