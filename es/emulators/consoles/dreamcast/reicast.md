---
title: Reicast
description: 
published: true
date: 2024-06-30T19:26:11.256Z
tags: dreamcast, reicast
editor: markdown
dateCreated: 2023-09-06T09:43:29.842Z
---

**Reicast** es un emulador capaz de emular la **Dreamcast**.

## ![](/emulators/license.svg) Licencia

El core posee la siguiente licencia [**BSD-3**](https://github.com/reicast/reicast-emulator/blob/alpha/LICENSE).

## ![](/emulators/compatibility.png) Compatibilidad

 | RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | 
 | ❌ | ✅ | ❌ | ❌ | ❌ | ❌ | ❌ | ❌ |

## ![](/emulators/features.png) Funcionalidades

| Funcionalidad | ¿Soportada? |
| :---: | :---: |
|  |  |

## ![](/emulators/bios.svg) BIOS

### Lista de BIOS obligatorias

| Nombre del fichero | Descripción | MD5 | ¿Preinstalada? |
| :---: | :---: | :---: | :---: |
| dc_boot.bin | BIOS Dreamcast | e10c53c2f8b90bab96ead2d368858623 d552d8b577faa079e580659cd3517f86 d407fcf70b56acb84b8c77c93b0e5327 | ❌  |
| dc_flash.bin | Date / Heure / Langue | d6e11a23f1fa01cddb5dfccf7e4cc8d7 93a9766f14159b403178ac77417c6b68 0a93f7940c455905bea6e392dfde92a4 23df18aa53c8b30784cd9a84e061d008 69c036adfca4ebea0b0c6fa4acfc8538 74e3f69c2bb92bc1fc5d9a53dcf6ffe2 2f818338f47701c606ade664a3e16a8a | ❌  |

### Emplazamiento

Disponed vuestras bios como en la siguiente imagen:

┣ 📁 recalbox  
┃ ┣ 📁 share  
┃ ┃ ┣ 📁 bios  
┃ ┃ ┃ ┣ 📁 dc
┃ ┃ ┃ ┃ ┣ 🗒 **dc_boot.bin**  
┃ ┃ ┃ ┃ ┣ 🗒 **dc_flash.bin**  

## ![](/emulators/isos.svg) Isos

### Extensiones de roms soportadas

Las roms deben tener las extensiones siguientes:

* .bin/.cue
* .bin/.gdi
* .cdi
* .chd (format v4 !)

### Emplazamiento

Disponed vuestras isos como en la siguiente imagen: 

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 dreamcast
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.gdi**
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.bin**

>Se aconsejan las isos del projecto **TOSEC**.
{.is-success}

>Para más información sobre las isos, consultad [este tutorial](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

>Para conservar las configuraciones personalizadas cuando se produce una actualización del sistema Recalbox, os aconsejamos utilizar las [Sobrecargas de configuración](./../../../advanced-usage/configuration-override).
{.is-info}

## ![](/emulators/external-links.png) Enlaces externos

* **Código fuente utilizado:** [https://github.com/reicast/reicast-emulator/](https://github.com/reicast/reicast-emulator/)
* **Documentación** : [https://reicast.com/guide/](https://reicast.com/guide/)