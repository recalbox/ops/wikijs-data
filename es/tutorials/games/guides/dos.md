---
title: DOS
description: 
published: true
date: 2024-07-26T06:58:22.852Z
tags: dos, msdos, pc
editor: markdown
dateCreated: 2023-08-31T14:10:46.323Z
---

Aquí tenéis varios tutoriales relacionados con el sistema DOS.

Los tutoriales son:

[DOSbox para emular los juegos MSDOS](dosbox-to-play-with-dos-games)
[Jugar a los juegos de Windows 95 & 98 gracias a Dosbox Pure](play-win-95-98-games-dosbox-pure)
[Utilización del ratón con el mando de juegos en DOSBox Pure](mouse-use-with-controller-in-dosbox-pure)