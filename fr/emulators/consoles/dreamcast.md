---
title: Dreamcast
description: 
published: true
date: 2024-07-23T22:40:04.563Z
tags: consoles, sega, dreamcast
editor: markdown
dateCreated: 2021-05-21T07:59:16.813Z
---

![](/emulators/consoles/dreamcast.svg){.align-center}

## Fiche technique

* **Fabricant :** SEGA
* **Année de sortie :**
  * 27 novembre 1998 (Japon)
  * 9 septembre 1999 (USA/Canada)
  * 14 octobre 1999 (Europe)
* **Processeur :** Hitachi SH4 RISC (128 bits)
* **Processeur graphique :** NEC Power VR II 
* **Puce sonore :** Yamaha 64 canaux (200MHz)
* **RAM :** 16 MB
* **Média :** GD-Rom (capacité 1Go)

## Présentation

La dernière console de **SEGA** restera gravée longtemps dans les mémoires.

La console qui propose le jeu online de série n’aura pourtant pas su convaincre suffisamment de monde malgré son catalogue de jeux impressionnant, peut être trop en avance sur son temps.

Malgré que la console offre un bon rapport qualité prix, elle fut un échec commercial.

La **Dreamcast** ne fait pas la même erreur que la [**Saturn**](./saturn) et dispose d’un unique processeur principal.

Concernant les graphismes, le travail est effectué par un Power VR II tout droit issue du monde du PC, ce qui, allié à un OS dérivé de Windows CE, permet une programmation plus facile (surtout en ce qui concerne les portages).

Le format **GD-Rom** propriétaire permet d’éviter une copie des jeux trop facile mais, c’est surtout la capacité de 1 Go qui donne tout l’intérêt du support.

Nous arrivons aux ports manettes : la **Dreamcast** en dispose de 4, ce qui permet de profiter pleinement des jeux comme **Virtua Tennis** sans avoir à investir dans un multipad.

Nous n’avons par contre aucun port pour les **VMS**. En effet, ces derniers s’insèrent dans les manettes, ce qui vous permettra d’en avoir jusqu’à 8 insérés dans la console en même temps.

Maintenant, le plus intéressant : le **modem**. Ce dernier se branche sur le côté de la console ce qui vous permet de la changer par un plus rapide si vous en avez envie. 

Pour terminer, on notera que la **Dreamcast** est la base du système arcade [**NAOMI**](./../arcade/naomi). Ce système est principalement constitué d’une carte aux caractéristiques techniques quasi-identiques à celles de la **Dreamcast** (mais avec 2 fois plus de mémoire). L’avantage du système [**NAOMI**](./../arcade/naomi) est qu’il permet de connecter jusqu’à 16 de ces cartes qui travaillent alors en parallèle. Néanmoins, le système [**NAOMI**](./../arcade/naomi) va suivre sa propre évolution séparément de la **Dreamcast**.

## Émulateurs

[Libretro Flycast](libretro-flycast)
[Libretro Flycast-Next](libretro-flycast-next)
[Reicast](reicast)