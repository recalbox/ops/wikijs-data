---
title: Los Overlays
description: Añadir ilustraciones gráficas alrededor de la pantalla de juego
published: true
date: 2023-08-22T22:16:06.700Z
tags: bezel, custom, 8.0+, overlay
editor: markdown
dateCreated: 2023-08-22T22:16:06.700Z
---

## Presentación

Los **overlays** (también llamados *bezels*) son ilustraciones que decoran vuestra pantalla durante el juego. Estás imágenes se muestran en las zonas negras de vuestra pantalla 16/9, zonas negras que no pueden ser utilizadas en los juegos con formato 4/3.

**Street of Rage 3** con un overlay Megadrive, en una pantalla 16/9:
![overlays.png](/basic-usage/features/overlays/overlays.png)

**Street of Rage 3** sin ningún overlay, en una pantalla 16/9:
![43.png](/basic-usage/features/overlays/43.png)

**Street of Rage 3** sin overlays, en una pantalla 16/9, con la imagen estirada:
![169.png](/basic-usage/features/overlays/169.png)

## Los overlays Recalbox

Recalbox integra los **Overlays Recalbox** que se muestran automáticamente en las pantallas compatibles (16/9).

Podéis activar o désactivar los **overlays** en el menú **Opciones de juegos** de EmulationStation:

![overlays-set-option-es.png](/basic-usage/features/overlays/overlays-set-option-es.png)

### Activación por sistema

Dentro del fichero `recalbox.conf`, es posible activar o desactivar los Overlays Recalbox de forma general:

```ini
global.recalboxoverlays=1
```

O para cada sistema :
```ini
snes.recalboxoverlays=0
```

## Añadir vuestros propios overlays

Existen muchos packs de overlays en internet, algunos hasta proponen un overlay diferente para cada juego!

Si queréis instalar un pack de overlays en vuestra Recalbox, sólo tenéis que copiar todas las carpetas dentro de la carpeta `overlays` de vuestra partición `SHARE`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 overlays
┃ ┃ ┃ ┣ 📁 megadrive
┃ ┃ ┃ ┃ ┣ 🗒 megadrive.cfg
┃ ┃ ┃ ┃ ┣ 🗒 megadrive_overlay.cfg
┃ ┃ ┃ ┃ ┣ 🗒 megadrive.png
┃ ┃ ┃ ┣ 📁 snes
┃ ┃ ┃ ┃ ┣ 🗒 .....

> Los overlays custom son prioritarios sobre los overlays automáticos de Recalbox.
> {.is-info}