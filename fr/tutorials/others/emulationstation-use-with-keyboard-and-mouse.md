---
title: Utilisation de l'interface au clavier / à la souris
description: Si votre seule manette tombe en panne...
published: true
date: 2024-07-26T07:11:12.546Z
tags: emulationstation, clavier, souris
editor: markdown
dateCreated: 2021-05-21T08:09:18.247Z
---

Au cas où vous aurez votre seule manette qui tomberait en passe en pleine utilisation de l'interface, vous pouvez naviguer avec un clavier et souris.

## Commandes de la souris

Voici ce que vous pouvez faire à la souris :

* A : clic droit
* B : clic gauche

## Commandes du clavier

Voici ce que vous pouvez faire au clavier :

* A = S
* B = A
* X = P
* Y = L
* Start = Entrée
* Select = Espace
* L1 = Page Up (⇞)
* R1 = Page Down (⇟)
* Hotkey = Échap

