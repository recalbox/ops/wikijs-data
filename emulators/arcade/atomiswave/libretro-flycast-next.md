---
title: Libretro Flycast-Next
description: 
published: true
date: 2024-07-24T07:51:42.492Z
tags: libretro, atomiswave, 9.2+, flycast-next
editor: markdown
dateCreated: 2024-07-24T07:25:04.747Z
---

**Libretro Flycast** is a multi-platform **Sega Dreamcast** emulator able to emulate the **Sammy Atomiswave**.

## ![](/emulators/license.svg) License

This core is under [**GPLv2**](https://github.com/libretro/flycast/blob/master/LICENSE) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ❌ | ❌ | ✅ | ✅ | ✅ | ✅  🐌 |✅ | ✅ |

🐌  Low performances but playable

## ![](/emulators/features.png) Features

| Feature | Supported |
| :---: | :---: |
| Restart | ✔ |
| Screenshots | ✔ |
| Saues | ✔ |
| Core Options | ✔ |
| RetroArch Cheats | ✔ |
| Controls | ✔ |
| Remapping | ✔ |
| Rumble | ✔ |
| Disk Control | ✔ |

## ![](/emulators/bios.svg) BIOS

### List of mandatory bios

| Filename | Description | MD5 | Provided |
| :---: | :---: | :---: | :---: |
| awbios.zip | Atomiswave BIOS from MAME | 0ec5ae5b5a5c4959fa8b43fcf8687f7c | ❌ |

### Location

Put your bios like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 dc
┃ ┃ ┃ ┃ ┣ 🗒 **awbios.zip**

## ![](/emulators/roms.png) Roms

Libretro Flycast is based on the Romset of **MAME** but also on **NullDC** formats for its Atomiswave part.

### Supported extensions

Roms must have this extension:

* .zip
* .7z
* .bin/.lst
* .dat

## Mame Romset

Only Atomiswave roms **from a RomSet MAME 0.135 or higher** are compatible.  
We recommend the **RomSet Mame 0.235** which will bring a lot of additional compatibilities.  
For more information on the current RomSet version, see the [MameDev] page (https://www.mamedev.org/release.html).

>To sort your arcade roms, the **dat file** is available in the folder: `/recalbox/share/bios/dc/`
{.is-info}

### Location

Put your roms like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 atomiswave
┃ ┃ ┃ ┃ ┣ 🗒 **game.zip**

## NullDC Romset

These roms are compatible with Flycast but **less reliable** than roms **from a MAME set**.

>The **NullDC** roms are in the format : _`.bin + .lst`_.
{.is-info}

### Location

Put your roms like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 atomiswave
┃ ┃ ┃ ┃ ┣ 📁 **game**
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **game.bin**
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **game.lst**

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Accessing to options

You can configure various options in two different ways.

* Via RetroArch menu:

┣ 📁 RetroArch Menu
┃ ┣ 📁 Core options
┃ ┃ ┣ 🧩 Name_option

* Via the file `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core options

| Options | Options values | Variables (for configuration override) | Variables values |
| --- | --- | --- | --- |
| Region | `Japon` / `États-Unis` ✅ / `Europe` / `Par défaut` | `reicast_region` | `Japan` / `USA` / `Europe` / `Default` |
| Enable DSP | `Disabled` / `Enabled` ✅ | `reicast_enable_dsp` | `disabled` / `enabled` |
| Allow NAOMI Service Buttons | `Disabled` ✅ / `Enabled` | `reicast_allow_service_buttons` | `disabled` / `enabled` |
| Set NAOMI Games to Free Play | `Disabled` / `Enabled` ✅ | `reicast_force_freeplay` | `disabled` / `enabled` |
| Broadband Adapter Emulation | `Disabled` ✅ / `Enabled` | `reicast_emulate_bba` | `disabled` / `enabled` |
| Enable UPnP | `Disabled` / `Enabled` ✅ | `reicast_upnp` | `disabled` / `enabled` |
| Internal Resolution | `320x240` / `640x480` ✅ / `800x600` / `960x720` / `1024x768` / `1280x960` / `1440x1080` / `1600x1200` / `1920x1440` / `2560x1920` / `2880x2160` / `3200x2400` / `3840x2880` / `4480x3360` / `5120x3840` / `5760x4320` / `6400x4800` / `7040x5280` / `7680x5760` / `8320x6240` / `8960x6720` / `9600x7200` / `10240x7680` / `10880x8160` / `11520x8640` / `12160x9120` / `12800x9600` | `reicast_internal_resolution` | `320x240` / `640x480` / `800x600` / `960x720` / `1024x768` / `1280x960` / `1440x1080` / `1600x1200` / `1920x1440` / `2560x1920` / `2880x2160` / `3200x2400` / `3840x2880` / `4480x3360` / `5120x3840` / `5760x4320` / `6400x4800` / `7040x5280` / `7680x5760` / `8320x6240` / `8960x6720` / `9600x7200` / `10240x7680` / `10880x8160` / `11520x8640` / `12160x9120` / `12800x9600` |
| Screen Orientation | `Horizontal` ✅ / `Vertical` | `reicast_screen_rotation` | `horizontal` / `vertical` |
| Alpha Sorting | `Per-Strip (fast, least accurate)` / `Per-Triangle (normal)` ✅ / `Per-Pixel (accurate, but slower)` | `reicast_alpha_sorting` | `per-strip (fast, least accurate)` / `per-triangle (normal)` / `per-pixel (accurate)` |
| Full framebuffer emulation | `Disabled` ✅ / `Enabled` | `reicast_emulate_framebuffer` | `disabled` / `enabled` |
| Enable RTT (Render To Texture) Buffer | `Disabled` ✅ / `Enabled` | `reicast_enable_rttb` | `disabled` / `enabled` |
| Mipmapping | `Disabled` / `Enabled` ✅ | `reicast_mipmapping` | `disabled` / `enabled` |
| Fog Effects | `Disabled` / `Enabled` ✅ | `reicast_fog` | `disabled` / `enabled` |
| Volume Modifier | `Disabled` / `Enabled` ✅ | `reicast_volume_modifier_enable` | `disabled` / `enabled` |
| Anisotropic Filter | `Disabled` / `2` / `4` ✅ / `8` / `16` | `reicast_anisotropic_filtering` | `off` / `2` / `4` / `8` / `16` |
| Texture Filtering | `Default` ✅ / `Force Nearest-Neighbor` / `Force Linear` | `reicast_texture_filtering` | `0` / `1` / `2` |
| Delay Frame Swapping | `Disabled` ✅ / `Enabled` | `reicast_delay_frame_swapping` | `disabled` / `enabled` |
| Detect Frame Rate Changes | `Disabled` ✅ / `Enabled` | `reicast_detect_vsync_swap_interval` | `disabled` / `enabled` |
| PowerVR2 Post-processing Filter | `Disabled` ✅ / `Enabled` | `reicast_pvr2_filtering` | `disabled` / `enabled` |
| Texture Upscaling (xBRZ) | `Disabled` ✅ / `x2` / `x4` / `x6` | `reicast_texupscale` | `disabled` / `2` / `4` / `6` |
| Texture Upscaling Max. Filtered Size | `256` / `512` / `1024` | `reicast_` | `256` / `512` / `1024` |
| Native Depth Interpolation | `Disabled` ✅ / `Enabled` | `reicast_native_depth_interpolation` | `disabled` / `enabled` |
| Threaded Rendering | `Disabled` / `Enabled` ✅ | `reicast_thraded_rendering` | `disabled` / `enabled` |
| Auto Skip Frame | `Disabled` ✅ / `Normal` / `Maximal` | `reicast_auto_skip_frame` | `disabled` / `some` / `more` |
| Frame Skipping | `Disabled` ✅ / `1` / `2` / `3` / `4` / `5` / `6` | `reicast_frame_skipping` | `disabled` / `1` / `2` / `3` / `4` / `5` / `6` |
| Widescreen Cheats (Restart Required) | `Disabled` ✅ / `Enabled` | `reicast_widescreen_cheats` | `disabled` / `enabled` |
| Widescreen Hack | `Disabled` ✅ / `Enabled` | `reicast_widescreen_hack` | `disabled` / `enabled` |
| SH4 CPU under/overclock | `100 MHz` / `110 MHz` / `120 MHz` / `130 MHz` / `140 MHz` / `150 MHz` / `160 MHz` / `170 MHz` / `180 MHz` / `190 MHz` / `200 MHz` / `210 MHz` / `220 MHz` / `230 MHz` / `240 MHz` / `250 MHz` / `260 MHz` / `270 MHz` / `280 MHz` / `290 MHz` / `300 MHz` | `reicast_sh4clock` | `100` / `110` / `120` / `130` / `140` / `150` / `160` / `170` / `180` / `190` / `200` / `210` / `220` / `230` / `240` / `250` / `260` / `270` / `280` / `290` / `300`  |
| Load Custom Textures | `Disabled` ✅ / `Enabled` | `reicast_custom_textures` | `disabled` / `enabled` |
| Dump Textures | `Disabled` ✅ / `Enabled` | `reicast_dump_textures` | `disabled` / `enabled` |
| Analog Stick Deadzone | `0%` / `5%` / `10%` / `15%` ✅ / `20%` / `25%` / `30%` | `reicast_analog_stick_deadzone` | `0%` / `5%` / `10%` / `15%` / `20%` / `25%` / `30%` |
| Trigger Deadzone | `0%` ✅ / `5%` / `10%` / `15%` / `20%` / `25%` / `30%` | `reicast_trigger_deadzone` | `0%` / `5%` / `10%` / `15%` / `20%` / `25%` / `30%` |
| Digital Triggers | `Disabled` ✅ / `Enabled` | `reicast_digital_triggers` | `disabled` / `enabled` |
| Broadcast Digital Outputs | `Disabled` ✅ / `Enabled` | `reicast_network_output` | `disabled` / `enabled` |
| Gun Crosshair 1 Display | `Disabled` ✅ / `White` / `Red` / `Green` / `Blue` | `reicast_lightgun1_crosshair` | `disabled` / `White` / `Red` / `Green` / `Blue` |
| Gun Crosshair 2 Display | `Disabled` ✅ / `White` / `Red` / `Green` / `Blue` | `reicast_lightgun2_crosshair` | `disabled` / `White` / `Red` / `Green` / `Blue` |
| Gun Crosshair 3 Display | `Disabled` ✅ / `White` / `Red` / `Green` / `Blue` | `reicast_lightgun3_crosshair` | `disabled` / `White` / `Red` / `Green` / `Blue` |
| Gun Crosshair 4 Display | `Disabled` ✅ / `White` / `Red` / `Green` / `Blue` | `reicast_lightgun4_crosshair` | `disabled` / `White` / `Red` / `Green` / `Blue` |
| VMU Sounds | `Disabled` ✅ / `Enabled` | `reicast_vmu_sound` | `disabled` / `enabled` |
| VMU Screen 1 Display | `Disabled` ✅ / `Enabled` | `reicast_vmu1_screen_display` | `disabled` / `enabled` |
| VMU Screen 1 Position | `Upper Left` ✅ / `Upper Right` / `Lower Left` / `Lower Right` | `reicast_vmu1_screen_position` | `Upper Left` / `Upper Right` / `Lower Left` / `Lower Right` |
| VMU Screen 1 Size | `1x` ✅ / `2x` / `3x` / `4x` / `5x` | `reicast_vmu1_screen_size_mult` | `1x` / `2x` / `3x` / `4x` / `5x` |
| VMU Screen 1 Pixel On Color | `Default ON` ✅ / `Default OFF` / `Black` / `Blue` / `Light Blue` / `Green` / `Cyan` / `Cyan Blue` / `Light Green` / `Cyan Green` / `Light Cyan` / `Red` / `Purple` / `Light Purple` / `Yellow` / `Grey` / `Light Purple (2)` / `Light Green (2)` / `Light Green (3)` / `Light Cyan (2)` / `Light Red (2)` / `Magenta` / `Light Purple (3)` / `Light Orange` / `Orange` / `Light Violet (4)` / `Light Yellow` / `Light Yellow (2)` / `White` | `reicast_vmu1_pixel_on_color` | `DEFAULT_ON 00` / `DEFAULT_OFF 01` / `BLACK 02` / `BLUE 03` / `LIGHT_BLUE 04` / `GREEN 05` / `CYAN 06` / `CYAN_BLUE 07` / `LIGHT_GREEN 08` / `CYAN_GREEN 09` / `LIGHT_CYAN 10` / `RED 11` / `PURPLE 12` / `LIGHT_PURPLE 13` / `YELLOW 14` / `GREY 15` / `LIGHT_PURPLE_2 16` / `LIGHT_GREEN_2 17` / `LIGHT_GREEN_3 18` / `LIGHT_CYAN_2 19` / `LIGHT_RED_2 20` / `MAGENTA 21` / `LIGHT_PURPLE_3 22` / `LIGHT_ORANGE 23` / `ORANGE 24` / `LIGHT_PURPLE_4 25` / `LIGHT_YELLOW 26` / `LIGHT_YELLOW_2 27` / `WHITE 28` |
| VMU Screen 1 Pixel Off Color | `Default OFF` ✅ / `Default ON` / `Black` / `Blue` / `Light Blue` / `Green` / `Cyan` / `Cyan Blue` / `Light Green` / `Cyan Green` / `Light Cyan` / `Red` / `Purple` / `Light Purple` / `Yellow` / `Grey` / `Light Purple (2)` / `Light Green (2)` / `Light Green (3)` / `Light Cyan (2)` / `Light Red (2)` / `Magenta` / `Light Purple (3)` / `Light Orange` / `Orange` / `Light Violet (4)` / `Light Yellow` / `Light Yellow (2)` / `White` | `reicast_vmu1_pixel_off_color` | `DEFAULT_OFF 00` / `DEFAULT_ON 01` / `BLACK 02` / `BLUE 03` / `LIGHT_BLUE 04` / `GREEN 05` / `CYAN 06` / `CYAN_BLUE 07` / `LIGHT_GREEN 08` / `CYAN_GREEN 09` / `LIGHT_CYAN 10` / `RED 11` / `PURPLE 12` / `LIGHT_PURPLE 13` / `YELLOW 14` / `GREY 15` / `LIGHT_PURPLE_2 16` / `LIGHT_GREEN_2 17` / `LIGHT_GREEN_3 18` / `LIGHT_CYAN_2 19` / `LIGHT_RED_2 20` / `MAGENTA 21` / `LIGHT_PURPLE_3 22` / `LIGHT_ORANGE 23` / `ORANGE 24` / `LIGHT_PURPLE_4 25` / `LIGHT_YELLOW 26` / `LIGHT_YELLOW_2 27` / `WHITE 28` |
| VMU Screen 1 Opacity | `10%` / `20%` / `30%` / `40%` / `50%` / `60%` / `70%` / `80%` / `90%` / `100%` | `reicast_vmu1_screen_opacity` | `10%` / `20%` / `30%` / `40%` / `50%` / `60%` / `70%` / `80%` / `90%` / `100%` |
| VMU Screen 2 Display | `Disabled` ✅ / `Enabled` | `reicast_vmu2_screen_display` | `disabled` / `enabled` |
| VMU Screen 2 Position | `Upper Left` ✅ / `Upper Right` / `Lower Left` / `Lower Right` | `reicast_vmu2_screen_position` | `Upper Left` / `Upper Right` / `Lower Left` / `Lower Right` |
| VMU Screen 2 Size | `1x` ✅ / `2x` / `3x` / `4x` / `5x` | `reicast_vmu2_screen_size_mult` | `1x` / `2x` / `3x` / `4x` / `5x` |
| VMU Screen 2 Pixel On Color | `Default ON` ✅ / `Default OFF` / `Black` / `Blue` / `Light Blue` / `Green` / `Cyan` / `Cyan Blue` / `Light Green` / `Cyan Green` / `Light Cyan` / `Red` / `Purple` / `Light Purple` / `Yellow` / `Grey` / `Light Purple (2)` / `Light Green (2)` / `Light Green (3)` / `Light Cyan (2)` / `Light Red (2)` / `Magenta` / `Light Purple (3)` / `Light Orange` / `Orange` / `Light Violet (4)` / `Light Yellow` / `Light Yellow (2)` / `White` | `reicast_vmu2_pixel_on_color` | `DEFAULT_ON 00` / `DEFAULT_OFF 01` / `BLACK 02` / `BLUE 03` / `LIGHT_BLUE 04` / `GREEN 05` / `CYAN 06` / `CYAN_BLUE 07` / `LIGHT_GREEN 08` / `CYAN_GREEN 09` / `LIGHT_CYAN 10` / `RED 11` / `PURPLE 12` / `LIGHT_PURPLE 13` / `YELLOW 14` / `GREY 15` / `LIGHT_PURPLE_2 16` / `LIGHT_GREEN_2 17` / `LIGHT_GREEN_3 18` / `LIGHT_CYAN_2 19` / `LIGHT_RED_2 20` / `MAGENTA 21` / `LIGHT_PURPLE_3 22` / `LIGHT_ORANGE 23` / `ORANGE 24` / `LIGHT_PURPLE_4 25` / `LIGHT_YELLOW 26` / `LIGHT_YELLOW_2 27` / `WHITE 28` |
| VMU Screen 2 Pixel Off Color | `Default OFF` ✅ / `Default ON` / `Black` / `Blue` / `Light Blue` / `Green` / `Cyan` / `Cyan Blue` / `Light Green` / `Cyan Green` / `Light Cyan` / `Red` / `Purple` / `Light Purple` / `Yellow` / `Grey` / `Light Purple (2)` / `Light Green (2)` / `Light Green (3)` / `Light Cyan (2)` / `Light Red (2)` / `Magenta` / `Light Purple (3)` / `Light Orange` / `Orange` / `Light Violet (4)` / `Light Yellow` / `Light Yellow (2)` / `White` | `reicast_vmu2_pixel_off_color` | `DEFAULT_OFF 00` / `DEFAULT_ON 01` / `BLACK 02` / `BLUE 03` / `LIGHT_BLUE 04` / `GREEN 05` / `CYAN 06` / `CYAN_BLUE 07` / `LIGHT_GREEN 08` / `CYAN_GREEN 09` / `LIGHT_CYAN 10` / `RED 11` / `PURPLE 12` / `LIGHT_PURPLE 13` / `YELLOW 14` / `GREY 15` / `LIGHT_PURPLE_2 16` / `LIGHT_GREEN_2 17` / `LIGHT_GREEN_3 18` / `LIGHT_CYAN_2 19` / `LIGHT_RED_2 20` / `MAGENTA 21` / `LIGHT_PURPLE_3 22` / `LIGHT_ORANGE 23` / `ORANGE 24` / `LIGHT_PURPLE_4 25` / `LIGHT_YELLOW 26` / `LIGHT_YELLOW_2 27` / `WHITE 28` |
| VMU Screen 2 Opacity | `10%` / `20%` / `30%` / `40%` / `50%` / `60%` / `70%` / `80%` / `90%` / `100%` | `reicast_vmu2_screen_opacity` | `10%` / `20%` / `30%` / `40%` / `50%` / `60%` / `70%` / `80%` / `90%` / `100%` |
| VMU Screen 3 Display | `Disabled` ✅ / `Enabled` | `reicast_vmu3_screen_display` | `disabled` / `enabled` |
| VMU Screen 3 Position | `Upper Left` ✅ / `Upper Right` / `Lower Left` / `Lower Right` | `reicast_vmu3_screen_position` | `Upper Left` / `Upper Right` / `Lower Left` / `Lower Right` |
| VMU Screen 3 Size | `1x` ✅ / `2x` / `3x` / `4x` / `5x` | `reicast_vmu3_screen_size_mult` | `1x` / `2x` / `3x` / `4x` / `5x` |
| VMU Screen 3 Pixel On Color | `Default ON` ✅ / `Default OFF` / `Black` / `Blue` / `Light Blue` / `Green` / `Cyan` / `Cyan Blue` / `Light Green` / `Cyan Green` / `Light Cyan` / `Red` / `Purple` / `Light Purple` / `Yellow` / `Grey` / `Light Purple (2)` / `Light Green (2)` / `Light Green (3)` / `Light Cyan (2)` / `Light Red (2)` / `Magenta` / `Light Purple (3)` / `Light Orange` / `Orange` / `Light Violet (4)` / `Light Yellow` / `Light Yellow (2)` / `White` | `reicast_vmu3_pixel_on_color` | `DEFAULT_ON 00` / `DEFAULT_OFF 01` / `BLACK 02` / `BLUE 03` / `LIGHT_BLUE 04` / `GREEN 05` / `CYAN 06` / `CYAN_BLUE 07` / `LIGHT_GREEN 08` / `CYAN_GREEN 09` / `LIGHT_CYAN 10` / `RED 11` / `PURPLE 12` / `LIGHT_PURPLE 13` / `YELLOW 14` / `GREY 15` / `LIGHT_PURPLE_2 16` / `LIGHT_GREEN_2 17` / `LIGHT_GREEN_3 18` / `LIGHT_CYAN_2 19` / `LIGHT_RED_2 20` / `MAGENTA 21` / `LIGHT_PURPLE_3 22` / `LIGHT_ORANGE 23` / `ORANGE 24` / `LIGHT_PURPLE_4 25` / `LIGHT_YELLOW 26` / `LIGHT_YELLOW_2 27` / `WHITE 28` |
| VMU Screen 3 Pixel Off Color | `Default OFF` ✅ / `Default ON` / `Black` / `Blue` / `Light Blue` / `Green` / `Cyan` / `Cyan Blue` / `Light Green` / `Cyan Green` / `Light Cyan` / `Red` / `Purple` / `Light Purple` / `Yellow` / `Grey` / `Light Purple (2)` / `Light Green (2)` / `Light Green (3)` / `Light Cyan (2)` / `Light Red (2)` / `Magenta` / `Light Purple (3)` / `Light Orange` / `Orange` / `Light Violet (4)` / `Light Yellow` / `Light Yellow (2)` / `White` | `reicast_vmu3_pixel_off_color` | `DEFAULT_OFF 00` / `DEFAULT_ON 01` / `BLACK 02` / `BLUE 03` / `LIGHT_BLUE 04` / `GREEN 05` / `CYAN 06` / `CYAN_BLUE 07` / `LIGHT_GREEN 08` / `CYAN_GREEN 09` / `LIGHT_CYAN 10` / `RED 11` / `PURPLE 12` / `LIGHT_PURPLE 13` / `YELLOW 14` / `GREY 15` / `LIGHT_PURPLE_2 16` / `LIGHT_GREEN_2 17` / `LIGHT_GREEN_3 18` / `LIGHT_CYAN_2 19` / `LIGHT_RED_2 20` / `MAGENTA 21` / `LIGHT_PURPLE_3 22` / `LIGHT_ORANGE 23` / `ORANGE 24` / `LIGHT_PURPLE_4 25` / `LIGHT_YELLOW 26` / `LIGHT_YELLOW_2 27` / `WHITE 28` |
| VMU Screen 3 Opacity | `10%` / `20%` / `30%` / `40%` / `50%` / `60%` / `70%` / `80%` / `90%` / `100%` | `reicast_vmu3_screen_opacity` | `10%` / `20%` / `30%` / `40%` / `50%` / `60%` / `70%` / `80%` / `90%` / `100%` |
| VMU Screen 4 Display | `Disabled` ✅ / `Enabled` | `reicast_vmu4_screen_display` | `disabled` / `enabled` |
| VMU Screen 4 Position | `Upper Left` ✅ / `Upper Right` / `Lower Left` / `Lower Right` | `reicast_vmu4_screen_position` | `Upper Left` / `Upper Right` / `Lower Left` / `Lower Right` |
| VMU Screen 4 Size | `1x` ✅ / `2x` / `3x` / `4x` / `5x` | `reicast_vmu4_screen_size_mult` | `1x` / `2x` / `3x` / `4x` / `5x` |
| VMU Screen 4 Pixel On Color | `Default ON` ✅ / `Default OFF` / `Black` / `Blue` / `Light Blue` / `Green` / `Cyan` / `Cyan Blue` / `Light Green` / `Cyan Green` / `Light Cyan` / `Red` / `Purple` / `Light Purple` / `Yellow` / `Grey` / `Light Purple (2)` / `Light Green (2)` / `Light Green (3)` / `Light Cyan (2)` / `Light Red (2)` / `Magenta` / `Light Purple (3)` / `Light Orange` / `Orange` / `Light Violet (4)` / `Light Yellow` / `Light Yellow (2)` / `White` | `reicast_vmu4_pixel_on_color` | `DEFAULT_ON 00` / `DEFAULT_OFF 01` / `BLACK 02` / `BLUE 03` / `LIGHT_BLUE 04` / `GREEN 05` / `CYAN 06` / `CYAN_BLUE 07` / `LIGHT_GREEN 08` / `CYAN_GREEN 09` / `LIGHT_CYAN 10` / `RED 11` / `PURPLE 12` / `LIGHT_PURPLE 13` / `YELLOW 14` / `GREY 15` / `LIGHT_PURPLE_2 16` / `LIGHT_GREEN_2 17` / `LIGHT_GREEN_3 18` / `LIGHT_CYAN_2 19` / `LIGHT_RED_2 20` / `MAGENTA 21` / `LIGHT_PURPLE_3 22` / `LIGHT_ORANGE 23` / `ORANGE 24` / `LIGHT_PURPLE_4 25` / `LIGHT_YELLOW 26` / `LIGHT_YELLOW_2 27` / `WHITE 28` |
| VMU Screen 4 Pixel Off Color | `Default OFF` ✅ / `Default ON` / `Black` / `Blue` / `Light Blue` / `Green` / `Cyan` / `Cyan Blue` / `Light Green` / `Cyan Green` / `Light Cyan` / `Red` / `Purple` / `Light Purple` / `Yellow` / `Grey` / `Light Purple (2)` / `Light Green (2)` / `Light Green (3)` / `Light Cyan (2)` / `Light Red (2)` / `Magenta` / `Light Purple (3)` / `Light Orange` / `Orange` / `Light Violet (4)` / `Light Yellow` / `Light Yellow (2)` / `White` | `reicast_vmu4_pixel_off_color` | `DEFAULT_OFF 00` / `DEFAULT_ON 01` / `BLACK 02` / `BLUE 03` / `LIGHT_BLUE 04` / `GREEN 05` / `CYAN 06` / `CYAN_BLUE 07` / `LIGHT_GREEN 08` / `CYAN_GREEN 09` / `LIGHT_CYAN 10` / `RED 11` / `PURPLE 12` / `LIGHT_PURPLE 13` / `YELLOW 14` / `GREY 15` / `LIGHT_PURPLE_2 16` / `LIGHT_GREEN_2 17` / `LIGHT_GREEN_3 18` / `LIGHT_CYAN_2 19` / `LIGHT_RED_2 20` / `MAGENTA 21` / `LIGHT_PURPLE_3 22` / `LIGHT_ORANGE 23` / `ORANGE 24` / `LIGHT_PURPLE_4 25` / `LIGHT_YELLOW 26` / `LIGHT_YELLOW_2 27` / `WHITE 28` |
| VMU Screen 4 Opacity | `10%` / `20%` / `30%` / `40%` / `50%` / `60%` / `70%` / `80%` / `90%` / `100%` | `reicast_vmu4_screen_opacity` | `10%` / `20%` / `30%` / `40%` / `50%` / `60%` / `70%` / `80%` / `90%` / `100%` |

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/flyinghead/flycast](https://github.com/flyinghead/flycast)