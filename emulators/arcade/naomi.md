---
title: Naomi
description: 
published: true
date: 2024-07-24T07:52:32.310Z
tags: arcade, naomi
editor: markdown
dateCreated: 2021-07-04T18:38:03.866Z
---

![](/emulators/arcade/naomi.svg){.align-center}

## Technical data

* **CPU**: Hitachi SH-4 (128 bits) RISC 360 MIPS
* **GPU**: NEC-Videologic PowerVR (PVR2DC/CLX2) @ 100 MHz
* **Audio**: ARM7 Yamaha AICA 45 MHZ (with internal 32-bit RISC CPU, 64 channel ADPCM)
* **RAM**: 32 Mo1,2
* **Video RAM**: 56 MB (64 MB with GD-ROM)
* **Media**: ROM Board (max. size of 168 MB) / GD-ROM

## Presentation

The Naomi system was released under the name **Naomi Multi System** (also called **Naomi Multiboard**), an acronym for "_New Arcade Operation Machine Idea"_ literally "new arcade machine idea"; Naomi also translates as "beauty" in Japanese. 

The system has an architecture **very close to that of the Dreamcast console**: a Hitachi SH-4 microprocessor at 200 MHz, a PowerVR Series 2 GPU (PVR2DC) graphics processor and a Yamaha AICA sound processor; but with **more RAM and video memory** (i.e. twice as much as the Dreamcast). Games are stored on **a cartridge** (Naomi) **or a GD-ROM** (NaomiGD)**.
  
Produced by SEGA, leader of the arcade at the time, to compete with NAMCO's System 23, its commercialization starts in 1998 with **The House of the Dead 2** as first game until 2006. More than 160 games will be released on this hardware (the world record of the number of games on an arcade hardware).

Sega announces that its **Naomi 2** remains compatible with the **cartridges and GD-Rom Naomi 1.**

## Emulators

[Libretro Flycast](libretro-flycast)
[Libretro Flycast-Next](libretro-flycast-next)