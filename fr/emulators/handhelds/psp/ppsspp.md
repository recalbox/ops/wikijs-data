---
title: PPSSPP
description: 
published: true
date: 2024-06-23T12:00:48.611Z
tags: psp, ppsspp
editor: markdown
dateCreated: 2021-05-21T08:32:11.239Z
---

**PPSSPP** est un émulateur libre pour la console portable PSP développer en C++.  
Il est disponible sur de nombreuses plate-formes, dont Android, Windows, iOS et GNU/Linux.

Il est écrit par **Henrik Hrydgard**.

## Exigences

* OpenGL / Open GL ES 2.0 ou supérieur pour le moteur de rendu OpenGL.
* Vulkan pour le moteur de rendu Vulkan.
* Direct3D 11 pour le moteur de rendu Direct3D 11.

## ![](/emulators/license.svg) Licence

Ce core est sous licence [**GPLv2**](https://github.com/hrydgard/ppsspp/blob/master/LICENSE.TXT).

## ![](/emulators/compatibility.png) Compatibilité

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ❌ | ✅ 🐌 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Fonctionnalités

| Fonctionnalité | Supporté |
| :---: | :---: |
| Redémarrage | ✔ |
| Sauvegardes | ✔ |
| Sauvegardes instantanées | ✔ |
| Options du core | ✔ |
| Cheats natifs | ✔ |
| Controls | ✔ |
| Remapping | ✔ |
| Utilisateur | ✔ |
| Langue | ✔ |

## ![](/emulators/bios.svg) BIOS

>**Aucun bios n'est requis.**
{.is-success}

## ![](/emulators/roms.png) Isos

### Extensions supportées

L'iso doit avoir l'extension :

* .cso
* .elf
* .iso
* .pbp
* .prx

### Emplacement

Placez les isos comme ceci : 

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 psp
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.iso**

>Les isos au format **Redump** sont vivement conseillées.
{.is-success}

>Pour plus d'information sur les isos, rendez-vous sur [ce tutoriel](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuration avancée de l'émulateur

>Pour pouvoir conserver vos configurations personnalisées lors d'une mise à jour, nous vous conseillons d'utiliser notre fonctionnalité [Surcharges de configuration](./../../../advanced-usage/configuration-override).
{.is-info}

## ![](/emulators/external-links.png) Liens externes

* **Code source utilisé** : [https://github.com/hrydgard/ppsspp/](https://github.com/hrydgard/ppsspp/)
* **Documentation Libretro** : [https://docs.libretro.com/library/ppsspp/](https://docs.libretro.com/library/ppsspp/)
* **Site officiel** : [http://www.ppsspp.org/](http://www.ppsspp.org/)
* **Forum officiel** : [https://forums.ppsspp.org/](https://forums.ppsspp.org/)
* **Documentation officielle** : [http://www.ppsspp.org/guides.html](http://www.ppsspp.org/guides.html)