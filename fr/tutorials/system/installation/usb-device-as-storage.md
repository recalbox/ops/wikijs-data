---
title: Utiliser un périphérique USB de stockage sur Recalbox
description: Pour mettre vos jeux et préservez votre carte SD
published: true
date: 2024-12-17T22:31:29.290Z
tags: usb, stockage
editor: markdown
dateCreated: 2021-05-21T08:41:16.891Z
---

## Introduction

Vous pouvez très facilement utiliser un périphérique USB de stockage supplémentaire (clé USB, disque dur externe, etc.) pour stocker vos roms.

Vous devez utiliser un périphérique utilisant le système de fichiers exFAT. Recalbox ne formatera pas votre périphérique, il ne créera que de nouveaux fichiers dessus.

## Configurer Recalbox pour utiliser un périphérique de stockage USB supplémentaire

Vous pouvez ajouter plusieurs périphériques de stockage USB supplémentaires.

Quand vous êtes dans l'interface, branchez le périphérique de stockage USB supplémentaire souhaité. Vous aurez un message sur votre écran vous demandant soit d'initialiser, soit de déplacer le share. Faites `INITIALISER`.

>**Ne faites pas `DÉPLACER LE SHARE` sauf si vous savez ce que vous faites !**
{.is-warning}

Pendant quelques instants, Recalbox va créer un dossier nommé `recalbox` à la racine du support de stockage, ainsi qu'un dossier `roms` dans le dossier précédent, ainsi que tous les dossiers nécessaires correspondants à tous les systèmes présents sur votre installation actuelle.

Pour ajouter vos roms sur votre périphérique de stockage USB supplémentaire, vous avez deux possibilités :

* Vous pouvez débrancher le périphérique de stockage USB supplémentaire à chaud (sans éteindre votre Recalbox) pour ajouter vos roms dedans. Au débranchement, l'interface va vous demander de redémarrer afin de mettre à jour les jeux actuellement disponible sans le périphérique débranché. Une fois que vous rebranchez votre périphérique à votre Recalbox toujours allumée, vous aurez un message vous demandant de redémarrer pour mettre à jour la liste des jeux.
* Vous pouvez aussi transférer par le réseau local en gardant votre Recalbox allumée. Vous aurez besoin de votre [adresse IP](./../../../tutorials/network/ip/discover-recalbox-ip).