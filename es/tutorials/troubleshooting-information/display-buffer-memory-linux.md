---
title: Visualizar el contenido de los buffers de memoria de Linux
description: 
published: true
date: 2023-09-17T19:49:24.919Z
tags: linux, contenido, memoria, buffers
editor: markdown
dateCreated: 2023-09-17T19:49:24.919Z
---

El siguiente comando muestra en pantalla el contenido de los buffers de memoria correspondiente a los mensajes del núcleo de Linux. Puede que nuestro equipo de desarrollo os pida ejecutar este comando dependiendo de vuestro problema.

## Utilización

* Conectaros [vía SSH](./../../tutorials/system/access/root-access-terminal-cli)
* Desenchufad vuestro periférico USB.
* Escribid: 

```shell
dmesg
```

* Tomad nota de lo que sale en la pantalla.
* Conectad el dispositivo USB.
* Escribid de nuevo el mismo comando:

```shell
dmesg
```

* Una vez ejecutado de nuevo este comando, podéis copiar y pegar la información necesaria allí donde te lo hayan pedido. Debería haber diferencias entre la primera y la segunda ejecución.

## Ejemplo para un dongle Wifi

```text
 [    2.817251] usb 1-1.4: new high-speed USB device number 5 using dwc_otg
 [    2.918993] usb 1-1.4: New USB device found, idVendor=7392, idProduct=7811
 [    2.919019] usb 1-1.4: New USB device strings: Mfr=1, Product=2, SerialNumber=3
 [    2.919030] usb 1-1.4: Product: 802.11n WLAN Adapter
 [    2.919041] usb 1-1.4: Manufacturer: Realtek
 [    2.919052] usb 1-1.4: SerialNumber: 00e04c000001
```

## Ejemplo para un dongle Bluetooth que no tiene firmware

```text
[ 194.192286] usb 1-1.4: new full-speed USB device number 4 using dwc_otg
[ 194.297324] usb 1-1.4: New USB device found, idVendor=0cf3, idProduct=3000
[ 194.297350] usb 1-1.4: New USB device strings: Mfr=0, Product=0, SerialNumber=0
[ 194.303210] usb 1-1.4: Direct firmware load for ath3k-1.fw failed with error -2
[ 194.303265] Bluetooth: Firmware file "ath3k-1.fw" not found
[ 194.303307] ath3k: probe of 1-1.4:1.0 failed with error -2
```