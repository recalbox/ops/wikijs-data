---
title: Amiga CD32
description: 
published: true
date: 2024-07-26T07:43:49.925Z
tags: amiga-cd32, amiga, commodore, cd32, consolas
editor: markdown
dateCreated: 2023-09-03T15:36:44.097Z
---

![](/emulators/consoles/amigacd32.svg){.align-center}


## Datos técnicos


* **Fabricante:** Commodore International
* **Año de lanzamiento:** 1993
* **Sistema operativo:** AmigaOS 3.1
* **Procesador:** Motorola 68EC020 a 14,32 MHz (NTSC), 14,18 MHz (PAL)
* **RAM:** 2 MB
* **ROM :** 1 MB Kickstart con firmware CD32, 1 KB EEPROM para copias de seguridad de juegos
* **Procesador gráfico:** Arquitectura gráfica avanzada
* **Chip de sonido:** PCM de 4 canales y 8 bits
* **Resolución:** 320x200 a 1280x400i (NTSC), 320x256 a 1280x512i (PAL)


## Presentación


La **Amiga CD32** es una videoconsola de quinta generación desarrollada por Commodore y lanzada al mercado en septiembre de 1993. La CD32 es una consola de 32 bits cuya arquitectura se basa en el ordenador **Amiga 1200**.


La **Amiga CD32** es la primera consola de 32 bits que ve la luz en Occidente (la FM Towns Marty la precedió en Japón). Al igual que la **3DO** y la **Atari Jaguar**, que salieron al mercado al mismo tiempo, la CD32 no logró imponerse en el mercado a pesar del apoyo de los aficionados a la gama **Amiga**. Sus limitaciones técnicas, su modo de visualización bit-plane, que limitaba seriamente su capacidad para mostrar juegos 3D texturizados como **Doom**, tan de moda en aquella época, la falta de implicación de los desarrolladores y las dificultades financieras de Commodore se esgrimieron como explicaciones de su fracaso.


## Emuladores


[Amiberry](amiberry)
[Libretro PUAE](libretro-puae)
[Libretro UAE4ARM](libretro-uae4arm)