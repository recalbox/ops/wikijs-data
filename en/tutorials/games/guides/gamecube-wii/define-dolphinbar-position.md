---
title: Define Dolphin Bar position
description: 
published: true
date: 2021-10-16T11:46:08.916Z
tags: wii, dolphin, bar
editor: markdown
dateCreated: 2021-08-08T08:28:33.026Z
---

* Open the file `/recalbox/share/system/recalbox.conf`.
* In the section "D3 - Dolphin Controllers" look for the line :

```ini
wii.sensorbar.position
```

* If you place your Dolphin Bar above your screen, replace with this:

```ini
wii.sensorbar.position=1
```

* If you place your Dolphin Bar below your screen, replace with this:

```ini
wii.sensorbar.position=0
```
