---
title: Vice_xcbm2
description: 
published: true
date: 2024-07-29T11:57:17.736Z
tags: commodore-64, c64, vice, xcbm2, 9.1+
editor: markdown
dateCreated: 2024-07-29T11:57:17.736Z
---

## ![](/emulators/license.svg) Licencia

Este núcleo está bajo la licencia [**GPLv2**](https://sourceforge.net/p/vice-emu/code/HEAD/tree/trunk/vice/COPYING).

## ![](/emulators/compatibility.png) Compatibilidad

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Funcionalidades

## ![](/emulators/bios.svg) BIOS

>**No se requiere ningún BIOS.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Extensiones soportadas

Las roms deben tener las siguientes extensiones:

* .bin
* .cmd
* .crt
* .d1m
* .d2m
* .d4m
* .d64
* .d6z
* .d67
* .d71
* .d7z
* .d80
* .d81
* .d82
* .d8z
* .d90
* .dhd
* .g41
* .g4z
* .g64
* .g6z
* .m3u
* .nbz
* .nib
* .p00
* .p64
* .prg
* .t64
* .tap
* .tcrt
* .vfl
* .vsf
* .x64
* .x6z
* .zip
* .7z
* .gz

Este sistema soporta roms comprimidas en formato .zip/.7z. Sin embargo, solo se trata de un archivo.

Los archivos contenidos en los .zip/.7z deben corresponder a las extensiones mencionadas anteriormente.
Además, cada archivo .zip/.7z debe contener solo una rom comprimida.

### Ubicación

Coloque las roms de la siguiente manera:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 c64
┃ ┃ ┃ ┃ ┣ 🗒 **juego.zip**

>Se recomiendan encarecidamente las roms en formato **No-Intro**.
{.is-success}

>Para más información sobre las roms, visite [este tutorial](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/external-links.png) Enlaces externos

* **Código fuente utilizado**: [https://sourceforge.net/projects/vice-emu/](https://sourceforge.net/projects/vice-emu/)
