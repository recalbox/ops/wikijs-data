---
title: IP address management and network settings
description: 
published: true
date: 2024-07-23T22:55:52.828Z
tags: network, address, ip, admin
editor: markdown
dateCreated: 2021-08-06T08:16:51.335Z
---

A problem with the IP address of your Recalbox? You should find your solution there!

Here are the available tutorials:

[Finding the IP address of your Recalbox](discover-recalbox-ip)
[Defining a static IP address for the Wi-Fi network](static-manual-ip)