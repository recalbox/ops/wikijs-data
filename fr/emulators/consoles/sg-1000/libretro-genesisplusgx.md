---
title: Libretro GenesisPlusGX
description: 
published: true
date: 2024-07-24T17:35:56.047Z
tags: libretro, genesisplusgx, sg-1000
editor: markdown
dateCreated: 2021-05-21T08:26:51.562Z
---

**Libretro GenesisPlusGX** est un émulateur Sega 8/16 bits open-source axé sur la précision et la portabilité.

## ![](/emulators/license.svg) Licence

Ce core est sous licence [**non-commerciale**](https://github.com/libretro/Genesis-Plus-GX/blob/master/LICENSE.txt).

## ![](/emulators/compatibility.png) Compatibilité

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Fonctionnalités

| Fonctionnalité | Supporté |
| :---: | :---: |
| Redémarrage | ✔ |
| Sauvegardes | ✔ |
| Sauvegardes instantanées | ✔ |
| Rembobinage | ✔ |
| Netplay | ✔ |
| Options du core | ✔ |
| RetroAchievements | ✔ |
| Cheats RetroArch | ✔ |
| Controls | ✔ |
| Remapping | ✔ |
| Multi-Mouse | ✔ |

## ![](/emulators/bios.svg) BIOS

>**Aucun bios n'est requis.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Extensions supportées

Les roms doivent avoir l'extension suivante :

* .sg
* .zip
* .7z

Ce système supporte les roms compressées au format .zip/.7z. Attention toutefois, il ne s'agit que d'une archive.

Les fichiers contenus dans les .zip/.7z doivent correspondre aux extensions citées précédemment.
De plus, chaque fichier .zip/.7z ne doit contenir qu'une seule rom compressée.

### Emplacement

Placez les roms comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 sg1000
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.zip**

>Les roms au format **No-Intro** sont vivement conseillées.
{.is-success}

>Pour plus d'information sur les roms, rendez-vous sur [ce tutoriel](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuration avancée de l'émulateur

>Pour pouvoir conserver vos configurations personnalisées lors d'une mise à jour, nous vous conseillons d'utiliser notre fonctionnalité [Surcharges de configuration](./../../../advanced-usage/configuration-override).
{.is-info}

### Accéder aux options

Vous pouvez configurer diverses options de deux façons différentes.

* Via le menu RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via le fichier `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Options du core

| Options | Valeur des options | Variable (pour surcharge) | Valeurs des variables |
| --- | --- | --- | --- |
| Matériel système | `Auto` ✅ / `SG-1000` / `SG-1000 II` / `SG-1000 II + Ext. RAM` / `Mark III` / `Master System` / `Master System II` / `Game Gear` / `Mega Drive/Genesis` | `genesis_plus_gx_system_hw` | `auto` / `sg-1000` / `sg-1000 II` / `sg-1000 II + ram ext.` / `mark-III` / `master system` / `master system II` / `game gear` / `mega drive / genesis` |
 | Région du système | `Auto` ✅ / `NTSC-U` / `PAL` / `NTSC-J` | `genesis_plus_gx_region_detect` | `auto` / `ntsc-u` / `pal` / `ntsc-j` |
 | ROM de démarrage du système | `Désactivé` ✅ / `Activé` | `genesis_plus_gx_bios` | `disabled` / `enabled` |
 | BRAM du système de CD (Redémarrage requis) | `Par BIOS` ✅ / `Par jeu` | `genesis_plus_gx_system_bram` | `per bios` / `per game` |
 | BRAM de la cartouche de sauvegarde du CD (Redémarrage requis) | `Par cartouche` ✅ / `Par jeu` | `genesis_plus_gx_cart_bram` | `per cart` / `per game` |
 | Quantité de BRAM de la cartouche de sauvegarde du CD (Redémarrage requis) | `Désactivé` / `128 kbit` / `256 kbit` / `512 kbit` / `1 Mbit` / `2 Mbit` / `4 Mbit` ✅ | `genesis_plus_gx_cart_size` | `disabled` / `128k` / `256k` / `512k` / `1meg` / `2meg` / `4meg` |
 | Module CD complémentaire (mode MD) (Redémarrage requis) | `Auto` ✅ / `Sega/Mega CD` / `MegaSD` / `Ne pas partager` | `genesis_plus_gx_add_on` | `auto` / `sega/mega cd` / `megasd` / `none` |
 | Cartouche lock-on | `Désactivé` ✅ / `Game Genie` / `Action Replay (Pro)` / `Sonic & Knuckles` | `genesis_plus_gx_lock_on` | `disabled` / `game genie` / `action replay (pro)` / `sonic & knuckles` |
 | Rapport d'aspect fourni par le cœur | `Auto` ✅ / `Rapport d'aspect préféré NTSC` / `Rapport d'aspect préféré PAL` / `4:3` / `Non corrigé` | `genesis_plus_gx_aspect_ratio` | `auto` / `NTSC PAR` / `PAL PAR` / `4:3` / `Uncorrected` |
 | Bordures | `Désactivé` ✅ / `Haut/Bas` / `Gauche/Droite` / `Complète` | `genesis_plus_gx_overscan` | `disabled` / `top/bottom` / `left/right` / `full` |
 | Masquer les bordures latérales de la Master System | `Désactivé` ✅ / `Bordure de gauche uniquement` / `Bordures gauche et droite` | `genesis_plus_gx_left_border` | `disabled` / `left border` / `left & right borders` |
 | Écran étendu pour la Game Gear | `Désactivé` ✅ / `Activé` | `genesis_plus_gx_gg_extra` | `disabled` / `enabled` |
 | Filtre NTSC de Blargg | `Désactivé` ✅ / `Monochrome` / `Composite` / `S-Video` / `RGB` | `genesis_plus_gx_blargg_ntsc_filter` | `disabled` / `monochrome` / `composite` / `svideo` / `rgb` |
 | Filtre de rémanence LCD | `Désactivé` ✅ / `Activé` | `genesis_plus_gx_lcd_filter` | `disabled` / `enabled` |
 | Sortie Mode 2 entrelacée | `Un seul champ` ✅ / `Double champ` | `genesis_plus_gx_render` | `single field` / `double field` |
 | Saut d'images | `Désactivé` ✅ / `Auto` / `Manuel` | `genesis_plus_gx_frameskip` | `disabled` / `auto` / `manual` |
 | Seuil de saut d'images (%) | `15` / `18` / `21` / `24` / `27` / `30` / `33` ✅ / `36` / `39` / `42` / `45` / `48` / `51` / `54` / `57` / `60` | `genesis_plus_gx_frameskip_threshold` | `15` / `18` / `21` / `24` / `27` / `30` / `33`/ `36` / `39` / `42` / `45` / `48` / `51` / `54` / `57` / `60` |
 | FM Master System (YM2413) | `Auto` ✅ / `Désactivé` / `Activé` | `genesis_plus_gx_ym2413` | `auto` / `disabled` / `enabled` |
 | Cœur FM Master System (YM2413) | `MAME` ✅ / `Nuked` | `genesis_plus_gx_ym2413_core` | `mame` / `nuked` |
 | FM Mega Drive / Genesis | `MAME (YM2612)` ✅ / `MAME (ASIC YM3438)` / `MAME (YM3438 amélioré)` / `Nuked (YM2612)` / `Nuked (YM3438)` | `genesis_plus_gx_ym2612` | `mame (ym2612)` / `mame (asic ym3438)` / `mame (enhanced ym3438)` / `nuked (ym2612)` / `nuked (ym3438)` |
 | Sortie audio | `Stéréo` ✅ / `Mono` | `genesis_plus_gx_sound_output` | `stereo` / `mono` |
 | Filtre audio | `Désactivé` ✅ / `Passe-bas` / `EQ` | `genesis_plus_gx_audio_filter` | `disabled` / `low-pass` / `EQ` |
 | Filtre passe-bas (%) | `5` / `10` / `15` / `20` / `25` / `30` / `35` / `40` / `45` / `50` / `55` / `60` ✅ / `65` / `70` / `75` / `80` / `85` / `90` / `95` | `genesis_plus_gx_lowpass_range` | `5` / `10` / `15` / `20` / `25` / `30` / `35` / `40` / `45` / `50` / `55` / `60` / `65` / `70` / `75` / `80` / `85` / `90` / `95` |
 | Niveau du préampli PSG | `0` / `5` / `10` / `15` / `20` / `25` / `30` / `35` / `40` / `45` / `50` / `55` / `60` / `65` / `70` / `75` / `80` / `85` / `90` / `95` / `100` / `105` / `110` / `115` / `120` / `125` / `130` / `135` / `140` / `145` / `150` ✅ / `155` / `160` / `165` / `170` / `175` / `180` / `185` / `190` / `195` / `200` | `genesis_plus_gx_psg_preamp` | `0` / `5` / `10` / `15` / `20` / `25` / `30` / `35` / `40` / `45` / `50` / `55` / `60` / `65` / `70` / `75` / `80` / `85` / `90` / `95` / `100` / `105` / `110` / `115` / `120` / `125` / `130` / `135` / `140` / `145` / `150` / `155` / `160` / `165` / `170` / `175` / `180` / `185` / `190` / `195` / `200` |
 | Niveau du préampli FM | `0` / `5` / `10` / `15` / `20` / `25` / `30` / `35` / `40` / `45` / `50` / `55` / `60` / `65` / `70` / `75` / `80` / `85` / `90` / `95` / `100` ✅ / `105` / `110` / `115` / `120` / `125` / `130` / `135` / `140` / `145` / `150` / `155` / `160` / `165` / `170` / `175` / `180` / `185` / `190` / `195` / `200` | `genesis_plus_gx_fm_preamp` | `0` / `5` / `10` / `15` / `20` / `25` / `30` / `35` / `40` / `45` / `50` / `55` / `60` / `65` / `70` / `75` / `80` / `85` / `90` / `95` / `100` / `105` / `110` / `115` / `120` / `125` / `130` / `135` / `140` / `145` / `150` / `155` / `160` / `165` / `170` / `175` / `180` / `185` / `190` / `195` / `200` |
 | Volume CD-DA | `0` / `5` / `10` / `15` / `20` / `25` / `30` / `35` / `40` / `45` / `50` / `55` / `60` / `65` / `70` / `75` / `80` / `85` / `90` / `95` / `100` ✅ | `genesis_plus_gx_cdda_volume` | `0` / `5` / `10` / `15` / `20` / `25` / `30` / `35` / `40` / `45` / `50` / `55` / `60` / `65` / `70` / `75` / `80` / `85` / `90` / `95` / `100` |
 | Volume PCM | `0` / `5` / `10` / `15` / `20` / `25` / `30` / `35` / `40` / `45` / `50` / `55` / `60` / `65` / `70` / `75` / `80` / `85` / `90` / `95` / `100` ✅ | `genesis_plus_gx_pcm_volume` | `0` / `5` / `10` / `15` / `20` / `25` / `30` / `35` / `40` / `45` / `50` / `55` / `60` / `65` / `70` / `75` / `80` / `85` / `90` / `95` / `100` |
 | EQ Low | `0` / `5` / `10` / `15` / `20` / `25` / `30` / `35` / `40` / `45` / `50` / `55` / `60` / `65` / `70` / `75` / `80` / `85` / `90` / `95` / `100` ✅ | `genesis_plus_gx_eq_low` | `0` / `5` / `10` / `15` / `20` / `25` / `30` / `35` / `40` / `45` / `50` / `55` / `60` / `65` / `70` / `75` / `80` / `85` / `90` / `95` / `100` |
 | EQ Mid | `0` / `5` / `10` / `15` / `20` / `25` / `30` / `35` / `40` / `45` / `50` / `55` / `60` / `65` / `70` / `75` / `80` / `85` / `90` / `95` / `100` ✅ | `genesis_plus_gx_eq_mid` | `0` / `5` / `10` / `15` / `20` / `25` / `30` / `35` / `40` / `45` / `50` / `55` / `60` / `65` / `70` / `75` / `80` / `85` / `90` / `95` / `100` |
 | EQ High | `0` / `5` / `10` / `15` / `20` / `25` / `30` / `35` / `40` / `45` / `50` / `55` / `60` / `65` / `70` / `75` / `80` / `85` / `90` / `95` / `100` ✅ | `genesis_plus_gx_eq_high` | `0` / `5` / `10` / `15` / `20` / `25` / `30` / `35` / `40` / `45` / `50` / `55` / `60` / `65` / `70` / `75` / `80` / `85` / `90` / `95` / `100` |
 | Entrée du pistolet | `Pistolet` ✅ / `Écran tactile` | `genesis_plus_gx_gun_input` | `lightgun` / `touchscreen` |
 | Afficher le réticule du pistolet | `Désactivé` ✅ / `Activé` | `genesis_plus_gx_gun_cursor` | `disabled` / `enabled` |
 | Inverser l'axe Y de la souris | `Désactivé` ✅ / `Activé` | `genesis_plus_gx_invert_mouse` | `disabled` / `enabled` |
 | Supprimer la limite de sprites par ligne | `Désactivé` ✅ / `Activé` | `genesis_plus_gx_no_sprite_limit` | `disabled` / `enabled` |
 | Défilement vertical par tile amélioré | `Désactivé` ✅ / `Activé` | `genesis_plus_gx_enhanced_vscroll` | `disabled` / `enabled` |
 | Limite du défilement vertical par tile amélioré | `2` / `3` / `4` / `5` / `6` / `7` / `8` ✅ / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` | `genesis_plus_gx_enhanced_vscroll_limit` | `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` |
 | CPU Speed | `100%` ✅ / `125%` / `150%` / `175%` / `200%` | `genesis_plus_gx_overclock` | `100%` / `125%` / `150%` / `175%` / `200%` |
 | System Lock-Ups | `Activé` ✅ / `Désactivé` | `genesis_plus_gx_force_dtack` | `enabled` / `disabled` |
 | 68K Address Error | `Activé` ✅ / `Désactivé` | `genesis_plus_gx_addr_error` | `enabled` / `disabled` |
 | PSG Tone Channel 0 Volume % | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `genesis_plus_gx_psg_channel_0_volume` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
 | PSG Tone Channel 1 Volume % | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `genesis_plus_gx_psg_channel_1_volume` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
 | PSG Tone Channel 2 Volume % | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `genesis_plus_gx_psg_channel_2_volume` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
 | PSG Tone Channel 3 Volume % | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `genesis_plus_gx_psg_channel_3_volume` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
 | Mega Drive / Genesis FM Channel 0 Volume % | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `genesis_plus_gx_md_channel_0_volume` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
 | Mega Drive / Genesis FM Channel 1 Volume % | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `genesis_plus_gx_md_channel_1_volume` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
 | Mega Drive / Genesis FM Channel 2 Volume % | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `genesis_plus_gx_md_channel_2_volume` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
 | Mega Drive / Genesis FM Channel 3 Volume % | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `genesis_plus_gx_md_channel_3_volume` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
 | Mega Drive / Genesis FM Channel 4 Volume % | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `genesis_plus_gx_md_channel_4_volume` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
 | Mega Drive / Genesis FM Channel 5 Volume % | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `genesis_plus_gx_md_channel_5_volume` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
 | Master System FM (YM2413) Channel 0 Volume % | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `genesis_plus_gx_sms_fm_channel_0_volume` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
 | Master System FM (YM2413) Channel 1 Volume % | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `genesis_plus_gx_sms_fm_channel_1_volume` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
 | Master System FM (YM2413) Channel 2 Volume % | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `genesis_plus_gx_sms_fm_channel_2_volume` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
 | Master System FM (YM2413) Channel 3 Volume % | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `genesis_plus_gx_sms_fm_channel_3_volume` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
 | Master System FM (YM2413) Channel 4 Volume % | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `genesis_plus_gx_sms_fm_channel_4_volume` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
 | Master System FM (YM2413) Channel 5 Volume % | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `genesis_plus_gx_sms_fm_channel_5_volume` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
 | Master System FM (YM2413) Channel 6 Volume % | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `genesis_plus_gx_sms_fm_channel_6_volume` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
 | Master System FM (YM2413) Channel 7 Volume % | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `genesis_plus_gx_sms_fm_channel_7_volume` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
 | Master System FM (YM2413) Channel 8 Volume % | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `genesis_plus_gx_sms_fm_channel_8_volume` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
 | Blocages du système | `Activé` ✅ / `Désactivé` | `genesis_plus_gx_force_dtack` | `enabled` / `disabled` |
 | Erreur d'adresse 68K | `Activé` ✅ / `Désactivé` | `genesis_plus_gx_addr_error` | `enabled` / `disabled` |
 | Temps d'accès au CD |  `Activé` ✅ / `Désactivé` | `genesis_plus_gx_cd_latency` | `enabled` / `disabled` |
 | Volume du canal de tonalité PSG 0 (%) | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `genesis_plus_gx_psg_channel_0_volume` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
 | Volume du canal de tonalité PSG 1 (%) | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `genesis_plus_gx_psg_channel_1_volume` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
 | Volume du canal de tonalité PSG 2 (%) | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `genesis_plus_gx_psg_channel_2_volume` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
 | Volume du canal de tonalité PSG 3 (%) | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `genesis_plus_gx_psg_channel_3_volume` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
 | Volume du canal FM 0 de la Mega Drive/Genesis (%) | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `genesis_plus_gx_md_channel_0_volume` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
 | Volume du canal FM 1 de la Mega Drive/Genesis (%) | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `genesis_plus_gx_md_channel_1_volume` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
 | Volume du canal FM 2 de la Mega Drive/Genesis (%) | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `genesis_plus_gx_md_channel_2_volume` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
 | Volume du canal FM 3 de la Mega Drive/Genesis (%) | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `genesis_plus_gx_md_channel_3_volume` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
 | Volume du canal FM 4 de la Mega Drive/Genesis (%) | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `genesis_plus_gx_md_channel_4_volume` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
 | Volume du canal FM 5 de la Mega Drive/Genesis (%) | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `genesis_plus_gx_md_channel_5_volume` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
 | Volume du canal FM (YM2413) 0 de la Master System (%) | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `genesis_plus_gx_sms_fm_channel_0_volume` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
 | Volume du canal FM (YM2413) 1 de la Master System (%) | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `genesis_plus_gx_sms_fm_channel_1_volume` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
 | Volume du canal FM (YM2413) 2 de la Master System (%) | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `genesis_plus_gx_sms_fm_channel_2_volume` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
 | Volume du canal FM (YM2413) 3 de la Master System (%) | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `genesis_plus_gx_sms_fm_channel_3_volume` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
 | Volume du canal FM (YM2413) 4 de la Master System (%) | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `genesis_plus_gx_sms_fm_channel_4_volume` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
 | Volume du canal FM (YM2413) 5 de la Master System (%) | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `genesis_plus_gx_sms_fm_channel_5_volume` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
 | Volume du canal FM (YM2413) 6 de la Master System (%) | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `genesis_plus_gx_sms_fm_channel_6_volume` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
 | Volume du canal FM (YM2413) 7 de la Master System (%) | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `genesis_plus_gx_sms_fm_channel_7_volume` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
 | Volume du canal FM (YM2413) 8 de la Master System (%) | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `genesis_plus_gx_sms_fm_channel_8_volume` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
 
## ![](/emulators/external-links.png) Liens externes

* **Code source utilisé** : [https://github.com/libretro/Genesis-Plus-GX/](https://github.com/libretro/Genesis-Plus-GX/)
* **Documentation Libretro** : [https://docs.libretro.com/library/genesis_plus_gx/](https://docs.libretro.com/library/genesis_plus_gx/)