---
title: Game Gear
description: 
published: true
date: 2021-08-09T12:51:31.984Z
tags: sega, handhelds, gamegear, gg
editor: markdown
dateCreated: 2021-07-06T07:46:29.970Z
---

![](/emulators/handheld/gamegear.svg){.align-center}

## Technical data

* **Manufacturer**: Sega
* **Year of release**: 1990
* **Units sold**: 11 millions
* **CPU**: 8-Bit Zilog Z80 @ 3.58 MHz
* **RAM**: 8kB
* **VRAM**: 16kB
* **Video**: Sega VDP (custom Texas Instruments TMS9918)
* **Resolution**: 160 x 146 pixels, 32 colors (4096 colors palette)
* **Sound**: TI SN76489 PSG
* **Cart size**: 32kB - 1MB
* **Screen**: 8.128cm (3.2 inches), backlit LCD

## Presentation

The **Game Gear** is a portable color video game console, released in 1990 and produced by Sega in order to compete with the Nintendo Game Boy that appeared one year earlier.

In terms of hardware, the Game Gear is so close to its big sister the Master System (of which it is the adaptation into a portable console with some differences), that its game library is largely composed of conversions of games made for the latter with the only notable difference being the resolution, which is lower in order to be adapted to the screen of the portable console.

The Master Gear Converter allows you to use Master System cartridges directly on the Game Gear without the need to emulate the hardware of the home console.

The Game Gear was never as successful as expected, among other things because of a few major flaws:

* low autonomy: 6 LR6 batteries allow 4 hours of use
* fragile AC connector
* price that was almost double that of the Game Boy but justified by the backlit color screen. It was not until 1998 that Nintendo released a color console, the Game Boy Color, and 2003 for a backlit color handheld console, the Game Boy Advance SP
* components of the machine ageing very badly, especially the capacitors which lose their capacity, causing a loss of sound and screen brightness.

## Emulators

[Libretro GenesisPlusGX](libretro-genesisplusgx)
[Libretro Gearsystem](libretro-gearsystem)
[Libretro FBNeo](libretro-fbneo)