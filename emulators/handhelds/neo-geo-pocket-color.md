---
title: Neo-Geo Pocket Color
description: 
published: true
date: 2021-09-16T06:12:26.913Z
tags: color, snk, neo-geo, pocket, ngpc, portables
editor: markdown
dateCreated: 2021-05-21T08:03:23.290Z
---

![](/emulators/handheld/ngpc.svg){.align-center}

## Fiche technique

* **Fabricant** : SNK
* **Année de sortie** : 1999
* **Quantités vendues** : 2 millions, incluant la version couleur
* **Processeurs** : Toshiba TLCS900H @ 6.144 MHz, Z80 @ 3.072 MHz
* **RAM** : 12K pour le 900H, 4K pour le Z80
* **ROM** : 64 Kbytes
* **Picture Processor Unit** : 16-bit
* **Résolution** : 160x152 (256×256 écran virtuel)
* **Média** : cartouche de 4 MB maximum

## Présentation

La **Neo-Geo Pocket Color** (ou **NGPC**) est une console portable 16 bits conçue par SNK. La console sort le 16 mars 1999 au Japon, le 6 août 1999 en Amérique du Nord, et le 1er octobre 1999 dans certains pays d’Europe. Elle succède à la console monochrome Neo-Geo Pocket, sortie une année plus tôt.

Le système d'exploitation possède une fonction originale, la langue est réglée dans la console. De ce fait, les jeux affichent leurs textes dans la langue sélectionnée (en supposant que la cartouche fournisse la langue demandée). D'autres paramètres peuvent être réglés dans la console, l'heure et la date par exemple, et le système d'exploitation peut fournir des horoscopes personnalisés quand une date de naissance est entrée.

Des câbles pour relier plusieurs consoles sont disponibles, ainsi qu'un câble pour connecter la console avec une Dreamcast de Sega pour certains jeux. Il existe également un connecteur sans fil sorti au Japon permettant à plusieurs joueurs de jouer ensemble. Une extension lecteur MP3 a été développée mais n'est pas sortie en raison de la fermeture de SNK.

## Émulateurs

[Libretro Mednafen_NGP](libretro-mednafen_ngp)
[Libretro RACE](libretro-race)