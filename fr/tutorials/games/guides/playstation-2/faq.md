---
title: FAQ
description: 
published: true
date: 2023-08-17T08:58:11.389Z
tags: faq, ps2, 8.0+
editor: markdown
dateCreated: 2021-10-12T20:48:10.020Z
---

## Existe-t-il une série de combinaisons de touches pour PCSX2 ?

Oui, voici la liste de ces combinaisons :

- `HotKey` + `haut`/`bas` : sélection de l'emplacement de sauvegarde
- `HotKey` + `X`/`Y` : chargement/sauvegarde rapide
- `HotKey` + `gauche` : bascule en mode ralenti
- `HotKey` + `droite` : bascule en mode rapide
- `HotKey` + `L1` : modification du ratio de l'écran (4:3, 16:9, plein écran étiré)
- `HotKey` + `R1` : capture d'écran
- `Select` + `Start` : quitter PCSX2.

## Peut-on afficher le compteur de FPS en jeu ?

Oui, il faut effectuer la manipulation suivante :

- Lancer n'importe quel jeu, faudra tout quitter à un moment
- Quand le jeu aura démarré, avec un clavier appuyez sur Echap. Vous devrez arriver sur l'interface graphique de PCSX2
- Cliquez sur `Config` > `Graphics settings`
- Dans la fenêtre, cliquez sur `OSD` > `Enable monitor` et cliquez sur `OK`
- Quittez PCSX2
- Démarrez le jeu souhaité