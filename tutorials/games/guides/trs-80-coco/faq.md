---
title: FAQ
description: 
published: true
date: 2021-11-09T20:47:11.707Z
tags: color, faq, trs-80, computer, coco, 8.0+
editor: markdown
dateCreated: 2021-08-29T17:07:44.095Z
---

## Quelques informations utiles...

### Quelles machines puis-je utiliser avec le TRS-80 Color Computer ?

Par défaut, la machine utilisée sera le Color Computer 2 PAL (CoCo 2 PAL).

Suivant les sous-répertoires, la machine à utiliser sera automatiquement sélectionnée en fonction de ces critères :

```text
"/coco/": "coco",
"/coco-pal/": "coco",
"/cocous/": "cocous",
"/coco-ntsc/": "cocous",
"/coco2/": "coco2b",
"/coco2b/": "coco2b",
"/coco2-pal/": "coco2b",
"/coco2b-pal/": "coco2b",
"/coco2us/": "coco2bus",
"/coco2bus/": "coco2bus",
"/coco2-ntsc/": "coco2bus",
"/coco2b-ntsc/": "coco2bus",
"/mx/": "mx1600",
"/mx1600/": "mx1600",
```

### Est-ce que le TRS-80 Color Computer bénéficie du lissage des jeux ?

Oui, le core XRoar supporte le lissage (dans EmulationStation, `START` > `OPTIONS DES JEUX` > `LISSAGE`).

### Est-ce que le TRS-80 Color Computer bénéficie des surcharges de configuration ?

Oui ! Mais les surcharges de configuration ne se surchargent pas entre-elles.

La première trouvée dans l'ordre est celle qui est prise en compte : `rom.ext.config` > `<folder>.config` > `<parent>.config` > ... La syntaxe du fichier de configuration ne permet pas de gérer les surcharges sans faire une usine à gaz.

Pour générer un fichier config, vous pouvez utiliser cette commande SSH : `xroar --config-all > .config`

### Est-ce que les 2 joysticks sont pris en compte ?

Oui, les joysticks 0 et 1 sont automatiquement pris en compte quelque soit l'ordre choisi dans EmulationStation.

### On pouvait imprimer avec le TRS-80 Color Computer. Est-ce quelque chose qu'on peut refaire ?

Oui ! Quand la machine « imprime », le texte imprimé ira dans le fichier `/recalbox/share/saves/trs80coco/printer/rom-extension.txt`