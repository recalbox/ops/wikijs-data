---
title: Odyssey²
description: Magnavox Odyssey2
published: true
date: 2021-10-03T20:47:11.488Z
tags: consoles, odyssey2, magnavox
editor: markdown
dateCreated: 2021-07-28T18:10:21.409Z
---

![](/emulators/consoles/odyssey2.svg){.align-center}

## Technical data

* **Manufacturer**: Magnavox/Philips
* **Year of release**: 1978
* **Units sold**: 2 millions
* **CPU**: Intel 8048 @ 1.79 MHz
* **RAM**: 64 bytes
* **Audio/Video RAM**: 128 bytes
* **ROM**: 1024 bytes
* **Resolution**: 160×200 pixels with 12 colors
* **Display**: 16-color fixed palette; sprites may only use 8 of these colors

## Presentation

The **Odyssey²** is a video game console designed by Magnavox, manufactured by Philips and marketed in the late 1970s and early 1980s under the brand names Philips (Videopac/Odyssey), Radiola-Radiotechnique (Videopac), Schneider (Videopac), Siera (Videopac), Magnavox (Odyssey²), Brandt (Jopac), Continental Edison (Jopac) and Saba (Jopac)

## Emulators

[Libretro O2EM](libretro-o2em)