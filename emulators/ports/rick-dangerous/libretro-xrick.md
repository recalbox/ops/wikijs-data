---
title: Libretro XRick
description: 
published: true
date: 2025-01-04T06:29:33.116Z
tags: libretro, rick dangerous, xrick
editor: markdown
dateCreated: 2021-06-29T22:27:19.686Z
---

**Free Xrick** is an open source implementation of the game Rick Dangerous.

## ![](/emulators/license.svg) License

This core is under [**GPLv3**](https://github.com/libretro/xrick-libretro/blob/master/README) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Features

| Feature | Supported |
| :---: | :---: |
| Controls | ✔ |
| Screenshots | ✔ |

## ![](/emulators/bios.svg) BIOS

>**No bios is required.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Supported extensions

Roms must have the extension:

* .zip

### Location

Put your roms like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 ports
┃ ┃ ┃ ┃ ┣ 📁 Rick Dangerous
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **data.zip**

This rom is included in your Recalbox.

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Accessing to options

You can configure various options in two different ways.

* Via RetroArch menu:

┣ 📁 RetroArch Menu
┃ ┣ 📁 Core options
┃ ┃ ┣ 🧩 Name_option

* Via the file `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core options

| Options | Options values | Variables (for configuration override) | Variables values |
| --- | --- | --- | --- |
| Crop Borders | `Enabled` ✅ / `Disabled` | `xrick_crop_borders` | `enabled` / `disabled` |
| Trainer Mode | `Disabled` ✅ / `Enabled` | `xrick_cheat1` | `disabled` / `enabled` |
| Invulnerability Mode | `Disabled` ✅ / `Enabled` | `xrick_cheat2` | `disabled` / `enabled` |
| Expose Mode | `Disabled` ✅ / `Enabled` | `xrick_cheat3` | `disabled` / `enabled` |

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/libretro/xrick-libretro/](https://github.com/libretro/xrick-libretro)
* **Official source code**: [https://github.com/r-type/xrick-libretro/](https://github.com/r-type/xrick-libretro)
* **Official website**: [http://www.bigorno.net/xrick/](http://www.bigorno.net/xrick/)