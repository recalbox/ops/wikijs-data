---
title: Sauvegarde/Clonage
description: 
published: true
date: 2021-09-04T21:57:09.875Z
tags: sauvegarde, clonage
editor: markdown
dateCreated: 2021-05-21T08:09:55.472Z
---

Si vous avez une carte SD qui commence à avoir des soucis, vous pouvez trouver ici plusieurs façons de sauvegarder ou de cloner votre périphérique de stockage.

Voici les tutoriels disponibles :

[Clonez votre carte SD](clone-sd-card)