---
title: Apple 2GS
description: 
published: true
date: 2021-10-03T16:11:23.341Z
tags: apple, apple-2gs, ordinateurs
editor: markdown
dateCreated: 2021-05-21T07:55:55.362Z
---

![](/emulators/computers/apple2gs.svg){.align-center}

## Fiche technique

* **Fabriquant** : Apple
* **Année de sortie** : 1986
* **Système d'exploitation** : Apple ProDOS, Apple GS/OS, GNO/ME
* **Processeur** : Western Design Center 65C816 16-bit @ 2.8 MHz
* **RAM** : 256 kB ou 1 MB (pouvant aller jusqu'à 8 MB)
* **ROM** : 128 KB pouvant aller jusqu'à 1 MB
* **Puce sonore** : puce sonore Ensoniq ES5503 DOC 8-bit wavetable synthesis, 32-canaux, stéréo
* **Affichage** : VGC 12-bpp palette (4096 couleurs), 320×200, 640×200

## Présentation

L'**Apple IIGS** comportait un processeur 65C816 à 2,8 MHz avec des registres 16 bits et 24-bit d'adressage, un processeur sonore Ensoniq, plus de mémoire, de meilleures couleurs, plus de périphériques (avec un contrôleur permutable entre les modèles de cartes pour IIe et IIc) dont certains compatibles avec ceux du Macintosh (clavier, souris, adaptateur réseau LocalTalk) et GS/OS, un système d'exploitation dérivé de Mac OS. La société Applied Engineering a développé plusieurs cartes d'extension pour la gamme Apple II dont la PC Transporter, permettant d'ajouter un PC XT (processeur NEC V30, RAM de 768 ko, mode graphique CGA) sous le capot. À noter qu'Apple commercialisa un kit permettant de transformer un Apple IIe en Apple IIGS.

## Émulateurs

[GSPlus](gsplus)