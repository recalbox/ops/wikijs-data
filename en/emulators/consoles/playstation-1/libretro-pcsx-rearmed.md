---
title: Libretro PCSX-ReARMed
description: 
published: true
date: 2021-10-17T17:06:08.809Z
tags: libretro, ps1, playstation 1, pcsx-rearmed
editor: markdown
dateCreated: 2021-08-01T23:44:14.815Z
---

[Libretro PCSX-ReARMed](https://github.com/libretro/pcsx_rearmed) is a fork of [PCSX-ReARMed](https://github.com/notaz/pcsx_rearmed) based on the PCSX-Reloaded project.  
This version is oriented for an ARM architecture and was created to improve performance on the Pandora handheld.
This emulator was then ported to other devices like the Raspberry Pi.

## ![](/emulators/license.svg) License

This core is under [**GPLv2**](https://github.com/libretro/pcsx_rearmed/blob/master/COPYING) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Features

| Feature | Supported |
| :---: | :---: |
| Restart | ✔ |
| Saves | ✔ |
| States | ✔ |
| Core Options | ✔ |
| RetroArch Cheats | ✔ |
| Controls | ✔ |
| Remapping | ✔ |
| Rumble | ✔ |
| Disk Control | ✔ |

## ![](/emulators/bios.svg) BIOS

### List of mandatory bios

Although some games can start without bios under PCSX-ReARMed, it is strongly advised to add these files in the bios directory in order to improve the performance of the emulator and the management of saves on "virtual" memory cards.

| Filename | Description | MD5 | Provided |
| :---: | :---: | :---: | :---: |
| scph101.bin | Version 4.4 03/24/00 A | 6e3735ff4c7dc899ee98981385f6f3d0 | ❌ |
| scph7001.bin | Version 4.1 12/16/97 A | 1e68c231d0896b7eadcad1d7d8e76129 | ❌ |
| scph5501.bin | Version 3.0 11/18/96 A | 490f666e1afb15b7362b406ed1cea246 | ❌ |
| scph1001.bin | Version 2.0 05/07/95 A | dc2b9bf8da62ec93e868cfd29f0d067d 924e392ed05558ffdb115408c263dccf | ❌ |

### Location

Put the bios like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 🗒 **scph101.bin**
┃ ┃ ┃ ┣ 🗒 **scph7001.bin**
┃ ┃ ┃ ┣ 🗒 **scph5501.bin**
┃ ┃ ┃ ┣ 🗒 **scph1001.bin**

## ![](/emulators/isos.svg) Isos

### Supported extensions

Isos must have the extension:

* .bin/.cue
* .img/.ccd/.sub
* .mdf/.mts
* .pbp
* .bin/.toc
* .cbn
* .m3u

### Location

Put the isos like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 psx
┃ ┃ ┃ ┃ ┣ 🗒 **game.bin**
┃ ┃ ┃ ┃ ┣ 🗒 **game.cue**

>The isos using **Redump** format are highly recommended.
{.is-success}

>For more information about isos, go to [this tutorial](./../../../tutorials/games/generalities/isos-and-roms)!
{.is-info}

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Accessing to options

You can configure various options in two different ways.

* Via RetroArch menu:

┣ 📁 RetroArch Menu
┃ ┣ 📁 Core options
┃ ┃ ┣ 🧩 Name_option

* Via the file `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core options

| Options | Options values | Variables (for configuration override) | Variables values |
| --- | --- | --- | --- |
| Frameskip | `0` ✅ / `1` / `2` / `3` | `pcsx_rearmed_frameskip` | `0` / `1` / `2` / `3` |
| Use BIOS | `auto` ✅ / `hle` | `pcsx_rearmed_bios` | `auto` / `hle` |
| Region | `auto` ✅ / `NTSC` / `PAL` | `pcsx_rearmed_region` | `auto` / `ntsc` / `pal` |
| Enable Second Memory Card (Shared) | `Disabled` ✅ / `Enabled` | `pcsx_rearmed_memcard2` | `disabled` / `enabled` |
| Show other input settings | `Disabled` ✅ / `Enabled` | `pcsx_rearmed_show_other_input_settings` | `disabled` / `enabled` |
| Emulated Mouse Sensitivity | `0.05` / `0.10` / `0.15` / `0.20` / `0.25` / `0.30` / `0.35` / `0.40` / `0.45` / `0.50` / `0.55` / `0.60` / `0.65` / `0.70` / `0.75` / `0.80` / `0.85` / `0.90` / `0.95` / `1.00` ✅ / `1.05` / `1.10` / `1.15` / `1.20` / `1.25` / `1.30` / `1.35` / `1.40` / `1.45` / `1.50` / `1.55` / `1.60` / `1.65` / `1.70` / `1.75` / `1.80` / `1.85` / `1.90` / `1.95` / `2.00` | `pcsx_rearmed_input_sensitivity` | `0.05` / `0.10` / `0.15` / `0.20` / `0.25` / `0.30` / `0.35` / `0.40` / `0.45` / `0.50` / `0.55` / `0.60` / `0.65` / `0.70` / `0.75` / `0.80` / `0.85` / `0.90` / `0.95` / `1.00` / `1.05` / `1.10` / `1.15` / `1.20` / `1.25` / `1.30` / `1.35` / `1.40` / `1.45` / `1.50` / `1.55` / `1.60` / `1.65` / `1.70` / `1.75` / `1.80` / `1.85` / `1.90` / `1.95` / `2.00` |
| Multitap 1 (Restart) | `auto` / `Disabled` ✅ / `Enabled` | `pcsx_rearmed_multitap1` | `auto` / `disabled` / `enabled` |
| Multitap 2 (Restart) | `auto` / `Disabled` ✅ / `Enabled` | `pcsx_rearmed_multitap2` | `auto` / `disabled` / `enabled` |
| NegCon Twist Deadzone (Percent) | `0` ✅ / `5` / `10` / `15` / `20` / `25` / `30` | `pcsx_rearmed_negcon_deadzone` | `0` / `5` / `10` / `15` / `20` / `25` / `30` |
| NegCon Twist Response | `linear` ✅ / `quadratic` / `cubic` | `pcsx_rearmed_negcon_response` | `linear` / `quadratic` / `cubic` |
| Analog axis bounds. | `circle` ✅ / `square` | `pcsx_rearmed_analog_axis_modifier` | `circle` / `square` |
| Enable Vibration | `'Disabled` ✅ / `Enabled` | `pcsx_rearmed_vibration` | `disabled` / `enabled` |
| Guncon Adjust X | `0` ✅ / `-25` / `-24` / `-23` / `-22` / `-21` / `-20` / `-19` / `-18` / `-17` / `-16` / `-15` / `-14` / `-13` / `-12` / `-11` / `-10` / `-09` / `-08` / `-07` / `-06` / `-05` / `-04` / `-03` / `-02` / `-01` / `00` / `01` / `02` / `03` / `04` / `05` / `06` / `07` / `08` / `09` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `21` / `22` / `23` / `24` / `25` | `pcsx_rearmed_gunconadjustx` | `0` / `-25` / `-24` / `-23` / `-22` / `-21` / `-20` / `-19` / `-18` / `-17` / `-16` / `-15` / `-14` / `-13` / `-12` / `-11` / `-10` / `-09` / `-08` / `-07` / `-06` / `-05` / `-04` / `-03` / `-02` / `-01` / `00` / `01` / `02` / `03` / `04` / `05` / `06` / `07` / `08` / `09` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `21` / `22` / `23` / `24` / `25` |
| Guncon Adjust Y | `0` ✅ / `-25` / `-24` / `-23` / `-22` / `-21` / `-20` / `-19` / `-18` / `-17` / `-16` / `-15` / `-14` / `-13` / `-12` / `-11` / `-10` / `-09` / `-08` / `-07` / `-06` / `-05` / `-04` / `-03` / `-02` / `-01` / `00` / `01` / `02` / `03` / `04` / `05` / `06` / `07` / `08` / `09` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `21` / `22` / `23` / `24` / `25` | `pcsx_rearmed_gunconadjusty` | `0` / `-25` / `-24` / `-23` / `-22` / `-21` / `-20` / `-19` / `-18` / `-17` / `-16` / `-15` / `-14` / `-13` / `-12` / `-11` / `-10` / `-09` / `-08` / `-07` / `-06` / `-05` / `-04` / `-03` / `-02` / `-01` / `00` / `01` / `02` / `03` / `04` / `05` / `06` / `07` / `08` / `09` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `21` / `22` / `23` / `24` / `25` |
| Guncon Adjust Ratio X | `1` ✅ / `0.75` / `0.76` / `0.77` / `0.78` / `0.79` / `0.80` / `0.81` / `0.82` / `0.83` / `0.84` / `0.85` / `0.86` / `0.87` / `0.88` / `0.89` / `0.90` / `0.91` / `0.92` / `0.93` / `0.94` / `0.95` / `0.96` / `0.97` / `0.98` / `0.99` / `1.00` / `1.01` / `1.02` / `1.03` / `1.04` / `1.05` / `1.06` / `1.07` / `1.08` / `1.09` / `1.10` / `1.11` / `1.12` / `1.13` / `1.14` / `1.15` / `1.16` / `1.17` / `1.18` / `1.19` / `1.20` / `1.21` / `1.22` / `1.23` / `1.24` / `1.25` | `pcsx_rearmed_gunconadjustratiox` | `1` / `0.75` / `0.76` / `0.77` / `0.78` / `0.79` / `0.80` / `0.81` / `0.82` / `0.83` / `0.84` / `0.85` / `0.86` / `0.87` / `0.88` / `0.89` / `0.90` / `0.91` / `0.92` / `0.93` / `0.94` / `0.95` / `0.96` / `0.97` / `0.98` / `0.99` / `1.00` / `1.01` / `1.02` / `1.03` / `1.04` / `1.05` / `1.06` / `1.07` / `1.08` / `1.09` / `1.10` / `1.11` / `1.12` / `1.13` / `1.14` / `1.15` / `1.16` / `1.17` / `1.18` / `1.19` / `1.20` / `1.21` / `1.22` / `1.23` / `1.24` / `1.25` |
| Guncon Adjust Ratio Y | `1` ✅ / `0.75` / `0.76` / `0.77` / `0.78` / `0.79` / `0.80` / `0.81` / `0.82` / `0.83` / `0.84` / `0.85` / `0.86` / `0.87` / `0.88` / `0.89` / `0.90` / `0.91` / `0.92` / `0.93` / `0.94` / `0.95` / `0.96` / `0.97` / `0.98` / `0.99` / `1.00` / `1.01` / `1.02` / `1.03` / `1.04` / `1.05` / `1.06` / `1.07` / `1.08` / `1.09` / `1.10` / `1.11` / `1.12` / `1.13` / `1.14` / `1.15` / `1.16` / `1.17` / `1.18` / `1.19` / `1.20` / `1.21` / `1.22` / `1.23` / `1.24` / `1.25` | `pcsx_rearmed_gunconadjustratioy` | `1` / `0.75` / `0.76` / `0.77` / `0.78` / `0.79` / `0.80` / `0.81` / `0.82` / `0.83` / `0.84` / `0.85` / `0.86` / `0.87` / `0.88` / `0.89` / `0.90` / `0.91` / `0.92` / `0.93` / `0.94` / `0.95` / `0.96` / `0.97` / `0.98` / `0.99` / `1.00` / `1.01` / `1.02` / `1.03` / `1.04` / `1.05` / `1.06` / `1.07` / `1.08` / `1.09` / `1.10` / `1.11` / `1.12` / `1.13` / `1.14` / `1.15` / `1.16` / `1.17` / `1.18` / `1.19` / `1.20` / `1.21` / `1.22` / `1.23` / `1.24` / `1.25` |
| Enable Dithering | `Disabled` / `Enabled` ✅ | `pcsx_rearmed_dithering` | `disabled` / `enabled` |
| Dynamic Recompiler | `Disabled` / `Enabled` ✅ | `pcsx_rearmed_drc` | `disabled` / `enabled` |
| PSX CPU Clock | `30` / `31` / `32` / `33` / `34` / `35` / `36` / `37` / `38` / `39` / `40` / `41` / `42` / `43` / `44` / `45` / `46` / `47` / `48` / `49` / `50` / `51` / `52` / `53` / `54` / `55` / `56` / `57` ✅ / `58` / `59` / `60` / `61` / `62` / `63` / `64` / `65` / `66` / `67` / `68` / `69` / `70` / `71` / `72` / `73` / `74` / `75` / `76` / `77` / `78` / `79` / `80` / `81` / `82` / `83` / `84` / `85` / `86` / `87` / `88` / `89` / `90` / `91` / `92` / `93` / `94` / `95` / `96` / `97` / `98` / `99` / `100` | `pcsx_rearmed_psxclock` | `30` / `31` / `32` / `33` / `34` / `35` / `36` / `37` / `38` / `39` / `40` / `41` / `42` / `43` / `44` / `45` / `46` / `47` / `48` / `49` / `50` / `51` / `52` / `53` / `54` / `55` / `56` / `57` / `58` / `59` / `60` / `61` / `62` / `63` / `64` / `65` / `66` / `67` / `68` / `69` / `70` / `71` / `72` / `73` / `74` / `75` / `76` / `77` / `78` / `79` / `80` / `81` / `82` / `83` / `84` / `85` / `86` / `87` / `88` / `89` / `90` / `91` / `92` / `93` / `94` / `95` / `96` / `97` / `98` / `99` / `100` |
| Enable Interlacing Mode | `Disabled` ✅ / `Enabled` | `pcsx_rearmed_neon_interlace_enable` | `disabled` / `enabled` |
| Enhanced Resolution (Slow) | `Disabled` ✅ / `Enabled` | `pcsx_rearmed_neon_enhancement_enable` | `disabled` / `enabled` |
| Enhanced Resolution (Speed Hack) | `Disabled` ✅ / `Enabled` | `pcsx_rearmed_neon_enhancement_no_main` | `disabled` / `enabled` |
| Frame Duping | `Disabled` / `Enabled` ✅ | `pcsx_rearmed_duping_enable` | `disabled` / `enabled` |
| Display Internal FPS | `Disabled` ✅ / `Enabled` | `pcsx_rearmed_display_internal_fps` | `disabled` / `enabled` |
| Advanced GPU P.E.Op.S. Settings | `Disabled` ✅ / `Enabled` | `pcsx_rearmed_show_gpu_peops_settings` | `disabled` / `enabled` |
| (GPU) Odd/Even Bit Hack | `Disabled` ✅ / `Enabled` | `pcsx_rearmed_gpu_peops_odd_even_bit` | `disabled` / `enabled` |
| (GPU) Expand Screen Width | `Disabled` ✅ / `Enabled` | `pcsx_rearmed_gpu_peops_expand_screen_width` | `disabled` / `enabled` |
| (GPU) Ignore Brightness Color | `Disabled` ✅ / `Enabled` | `pcsx_rearmed_gpu_peops_ignore_brightness` | `disabled` / `enabled` |
| (GPU) Disable Coordinate Check | `Disabled` ✅ / `Enabled` | `pcsx_rearmed_gpu_peops_disable_coord_check` | `disabled` / `enabled` |
| (GPU) Lazy Screen Update | `Disabled` ✅ / `Enabled` | `pcsx_rearmed_gpu_peops_lazy_screen_update` | `disabled` / `enabled` |
| (GPU) Old Frame Skipping | `Disabled` / `Enabled` ✅ | `pcsx_rearmed_gpu_peops_old_frame_skip` | `disabled` / `enabled` |
| (GPU) Repeated Flat Tex Triangles | `Disabled` ✅ / `Enabled` | `pcsx_rearmed_gpu_peops_repeated_triangles` | `disabled` / `enabled` |
| (GPU) Draw Quads with Triangles | `Disabled` ✅ / `Enabled` | `pcsx_rearmed_gpu_peops_quads_with_triangles` | `disabled` / `enabled` |
| (GPU) Fake 'Gpu Busy' States | `Disabled` ✅ / `Enabled` | `pcsx_rearmed_gpu_peops_fake_busy_state` | `disabled` / `enabled` |
| Advance GPU UNAI/PCSX4All Settings | `Disabled` ✅ / `Enabled` | `pcsx_rearmed_show_gpu_unai_settings` | `disabled` / `enabled` |
| (GPU) Enable Blending | `Disabled` / `Enabled` ✅ | `pcsx_rearmed_gpu_unai_blending` | `disabled` / `enabled` |
| (GPU) Enable Lighting | `Disabled` / `Enabled` ✅ | `pcsx_rearmed_gpu_unai_lighting` | `disabled` / `enabled` |
| (GPU) Enable Fast Lighting | `Disabled` ✅ / `Enabled` | `pcsx_rearmed_gpu_unai_fast_lighting` | `disabled` / `enabled` |
| (GPU) Enable Forced Interlace | `Disabled` ✅ / `Enabled` | `pcsx_rearmed_gpu_unai_ilace_force` | `disabled` / `enabled` |
| (GPU) Enable Pixel Skip | `Disabled` ✅ / `Enabled` | `pcsx_rearmed_gpu_unai_pixel_skip` | `disabled` / `enabled` |
| (GPU) Enable Hi-Res Downscaling | `Disabled` ✅ / `Enabled` | `pcsx_rearmed_gpu_unai_scale_hires` | `disabled` / `enabled` |
| Threaded Rendering | `Disabled` ✅ / `sync` / `async` | `pcsx_rearmed_gpu_thread_rendering` | `disabled` / `sync` / `async` |
| Show Bios Logo | `Disabled` ✅ / `Enabled` | `pcsx_rearmed_show_bios_bootlogo` | `disabled` / `enabled` |
| Sound Reverb | `Disabled` / `Enabled` ✅ | `pcsx_rearmed_spu_reverb` | `disabled` / `enabled` |
| Sound Interpolation | `Simple` ✅ / `Gaussian` / `Cubic` / `Disabled` | `pcsx_rearmed_spu_interpolation` | `simple` / `gaussian` / `cubic` / `off` |
| Diablo Music Fix | `Disabled` ✅ / `Enabled` | `pcsx_rearmed_idiablofix` | `disabled` / `enabled` |
| Parasite Eve 2/Vandal Hearts 1/2 Fix | `Disabled` ✅ / `Enabled` | `pcsx_rearmed_pe2_fix` | `disabled` / `enabled` |
| InuYasha Sengoku Battle Fix | `Disabled` ✅ / `Enabled` | `pcsx_rearmed_inuyasha_fix` | `disabled` / `enabled` |
| CD Access Method (Restart) | `Synchronous` ✅ / `Asynchronous` / `Precache` | `pcsx_rearmed_async_cd` | `sync` / `async` / `precache` |
| XA Decoding | `Disabled` / `Enabled` ✅ | `pcsx_rearmed_noxadecoding` | `disabled` / `enabled` |
| CD Audio | `Disabled` / `Enabled` ✅ | `pcsx_rearmed_nocdaudio` | `disabled` / `enabled` |
| SPU IRQ Always Enabled | `Disabled` ✅ / `Enabled` | `pcsx_rearmed_spuirq` | `disabled` / `enabled` |
| (Speed Hack) Disable SMC Checks | `Disabled` ✅ / `Enabled` | `pcsx_rearmed_nosmccheck` | `disabled` / `enabled` |
| (Speed Hack) Assume GTE Regs Unneeded | `Disabled` ✅ / `Enabled` | `pcsx_rearmed_gteregsunneeded` | `disabled` / `enabled` |
| (Speed Hack) Disable GTE Flags | `Disabled` ✅ / `Enabled` | `pcsx_rearmed_nogteflags` | `disabled` / `enabled` |

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/libretro/pcsx_rearmed/](https://github.com/libretro/pcsx_rearmed/)
* **Libretro documentation**: [https://docs.libretro.com/library/pcsx_rearmed/](https://docs.libretro.com/library/pcsx_rearmed/)