---
title: Libretro YabaSanshiro
description: 
published: true
date: 2024-06-23T11:31:16.600Z
tags: libretro, saturn, yabasanshiro
editor: markdown
dateCreated: 2021-05-21T08:26:26.148Z
---

**Libretro YabaSanshiro** est un port de l'émulateur **Yabause** pour Libretro prenant en charge la Sega Saturn.

## ![](/emulators/license.svg) Licence

Ce core est sous licence [**GPLv2**](https://github.com/libretro/yabause/blob/master/yabause/COPYING).

## ![](/emulators/compatibility.png) Compatibilité

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ❌ | ❌ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Fonctionnalités



## ![](/emulators/bios.svg) BIOS

### Liste des bios obligatoires

| Nom de fichier | Description | MD5 | Fourni |
| :---: | :---: | :---: | :---: |
| saturn_bios.bin | BIOS Saturn | af5828fdff51384f99b3c4926be27762 | ❌ |

### Emplacement

Placez les BIOS comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 saturn
┃ ┃ ┃ ┃ ┣ 🗒 **saturn_bios.bin**

## ![](/emulators/isos.svg) Isos

### Extensions supportées

Les isos doivent avoir les extensions suivantes :

* .bin/.cue
* .img/.ccd
* .iso
* .mds/.mdf
* .chd

### Emplacement

Placez les isos comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 saturn
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.bin**
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.cue**

>Les isos au format **Redump** sont vivement conseillés.
{.is-success}

>Pour plus d'information sur les isos, rendez-vous sur [ce tutoriel](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuration avancée de l'émulateur

>Pour pouvoir conserver vos configurations personnalisées lors d'une mise à jour, nous vous conseillons d'utiliser notre fonctionnalité [Surcharges de configuration](./../../../advanced-usage/configuration-override).
{.is-info}

### Accéder aux options

Vous pouvez configurer diverses options de deux façons différentes.

* Via le menu RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via le fichier `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Options du core

| Options | Valeurs des options | Variable (pour surcharge) | Valeurs des variables |
| --- | --- | --- | --- |
| Force HLE BIOS (restart) | `Désactivé` ✅ / `Activé` | `yabasanshiro_force_hle_bios` | `disabled` / `enabled` |
| Auto-frameskip | `Activé` ✅ / `Désactivé` | `yabasanshiro_frameskip` | `enabled` / `disabled` |
| Addon Cartridge (restart) | `1M_extended_ram` / `4M_extended_ram` ✅ | `yabasanshiro_addon_cart` | `1M_extended_ram` / `4M_extended_ram` |
| System Language (restart) | `english` ✅ / `deutsch` / `french` / `spanish` / `italian` / `japanese` | `yabasanshiro_system_language` | `english` / `deutsch` / `french` / `spanish` / `italian` / `japanese` |
| 6Player Adaptor on Port 1 | `Désactivé` ✅ / `Activé` | `yabasanshiro_multitap_port1` | `disabled` / `enabled` |
| 6Player Adaptor on Port 2 | `Désactivé` ✅ / `Activé` | `yabasanshiro_multitap_port2` | `disabled` / `enabled` |
| SH2 Core (restart) | `dynarec` ✅ / `interpreter` | `yabasanshiro_sh2coretype` | `dynarec` / `interpreter` |
| Polygon Mode (restart) | `perspective_correction` ✅ / `gpu_tesselation` / `cpu_tesselation` | `yabasanshiro_polygon_mode` | `perspective_correction` / `gpu_tesselation` / `cpu_tesselation` |
| Resolution Mode (restart) | `original` ✅ / `2x` / `4x` / `720p` / `1080p` / `4k` | `yabasanshiro_resolution_mode` | `original` / `2x` / `4x` / `720p` / `1080p` / `4k` |
| RGB resolution mode | `original` ✅ / `2x` / `720p` / `1080p` / `Fit_to_emulation` | `yabasanshiro_rbg_resolution_mode` | `original` / `2x` / `720p` / `1080p` / `Fit_to_emulation` |
| RGB use compute shader for RGB | `Désactivé` ✅ / `Activé` | `yabasanshiro_rbg_use_compute_shader` | `disabled` / `enabled` |

## ![](/emulators/external-links.png) Liens externes

* **Code source utilisé** : [https://github.com/libretro/yabause/](https://github.com/libretro/yabause/)
* **Documentation Libretro** : [https://docs.libretro.com/library/yabasanshiro/](https://docs.libretro.com/library/yabasanshiro/)