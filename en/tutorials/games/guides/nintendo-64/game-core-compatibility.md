---
title: Roms / Cores Compatibility 
description: We have tested for you the compatibility of N64 games, with each core. May the odds be ever in your favor.
published: true
date: 2021-08-01T23:46:44.663Z
tags: core n64, roms n64, core, n64 compatibility, compatibility
editor: markdown
dateCreated: 2021-06-29T14:52:21.360Z
---

#### Version 7.2.2 Reloaded
If you can't find your game in the array, it means that it hasn't been tested yet.
Be patient, we are working on it, you can also join the testing team if you want to give some of your time to help us.
Contact m.r.b.i on the Recalbox Discord for more informations :wink: 

## Caption

 :star2: High performance. To infinity and beyond!
 :star: Medium performance. Good balance, but slower with minimal bugs.
 :anger: Low performance. Slow and buggy. _**Not recommended**_.
 :boom: Does not work. Crash, Lags, Freeze, EmulationStation feedback, bugs. _**Not recommended**_.
 
 ## CORE

### Raspberry Pi3 b 

| JEUX  | Extension | Gliden 64 | GLES2 | Rice_GLES2 | Rice | Parallel 64 | Plus_Next |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | 
| **F Zéro X**  | .n64 | :star: | :star: | :anger: | :anger: | :anger:  | :boom:
| **Zelda Majora Mask**  | .n64  | :boom: | :boom: | :star: | :boom: | :anger: | :boom:
| **Paper Mario**  | .n64 | :star2: |:boom:  | :anger: | :boom: |  |  |
| **The New Tetris**  | .z64 | :star: |  |  |  |  |  |  
| **Paperboy** | ❓ | :boom: | :boom: | :boom: | :boom: | :boom: | :boom: |  
| **Mario Tennis** | ❓ |  |  |  |  |  |  |  
| **Pokemon Snap** | .z64 | :boom: | :boom: | :boom: | :boom: | :boom: |:boom:
| **1080** | .z64 | :star2: | :boom: | :star: | :anger: |:anger:  | :boom: | 
| **Aidyn** | .z64 |  :star: | :anger: | :anger: |  |  |  | 
| **Diddy Kong Racing** | .n64 | :boom: | :star: | :star: | :anger: | :boom: | :anger: | 
| **Super Mario 64** | .n64 | :star2: | :star: | :star2: | :star: | :star2: | :anger: |
| **Micro Machines 64 Turbo** | .z64 | :star2: | :anger: | :star: | :anger: |  |  |
| **Wipeout 64** | .z64 | :star2: | :boom: | :star: | :anger: |  |  |
| **Wipeout 64** | .n64 | :star2: | :boom: | :star: | :anger: | :star2: | :boom: |
| **Dr.Mario 64** | .z64 | :anger: | :boom: | :anger: | :anger: | | |
| **Wave Race** | .n64 | :star: | :anger: | :boom: | :boom: | :anger: | :boom: |
| **Star Wars Episode I Racer** | .n64 | :star: | :boom: | :anger: | :boom: | :boom: |:boom: |
| **Star Wars Rogue Squadron** | .n64 | :boom: | :boom: | :boom: | :boom: | :boom: |:boom: |
| **Star Wars Shadows of the Empire** | .n64 | :boom: | :boom: | :anger: | :anger: | :boom: | :boom: |
| **Star Wars Episode I Battle for Naboo** | .n64 | :boom: | :boom: | :boom: | :boom: | :boom: | :boom: |
| **Pokémon Stadium** | .n64 | :boom: | :boom: | :anger: | :anger: | :anger: | :boom: |
| **Pokémon Stadium 2** | .n64 | :boom: | :boom: | :anger: | :anger: | :star: | :boom: |
| **Mario Kart 64** | .n64 | :star2: | :star2: | :star2: | :anger: | :star2: | :boom: |
| **Lylat Wars** | .n64 | :star2: | :star: | :star: | :star: | :star: | :boom: |
| **Banjo-Kazooie** | .z64 | :star2: | :boom: | :star2: | :star: |  |  |
| **Banjo-Tooie** | .z64 | :boom: | :boom: | :star: | :anger: |  |  |
| **Kirby 64: The Crystal Shards** | .z64 | :star2: | :anger: | :star: | :anger: |  |  |


### Raspberry Pi 4

| Jeux | Extension | GlideMK2 | RICE | Plus_Next | ParaLLEl_N64 |
| :---: | :---: | :---: | :---: | :---: | :---: |
|  |  |  |  |  |  |
