---
title: Libretro EasyRPG
description: 
published: true
date: 2024-07-24T20:58:57.798Z
tags: libretro, easyrpg
editor: markdown
dateCreated: 2021-06-30T15:21:50.089Z
---

**Libretro EasyRPG** is a port of the **EasyRPG** emulator.

## ![](/emulators/license.svg) License

This core is under [**GPLv3**](https://github.com/EasyRPG/Player/blob/master/COPYING) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Features

| Feature | Supported |
| :---: | :---: |
| Restart | ✔ |
| Screenshots | ✔ |
| Saves | ✔ |
| Controls | ✔ |

## ![](/emulators/bios.svg) BIOS

>**No bios is required.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Supported extensions

Roms must have the extension:

* .ini

### Location

Put your roms like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 easyrpg
┃ ┃ ┃ ┃ ┣ 📁 game.rpg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **RPG_RT.ini**

Each game has its own set of files and must have the **RPG_RT.ini** file provided with it. Each game must be in its own directory.

### List of compatible games

You can find the list of compatible games by following [this link](https://community.easyrpg.org/t/compatibility-list/283).

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Accessing to options

You can configure various options in two different ways.

* Via RetroArch menu:

┣ 📁 RetroArch Menu
┃ ┣ 📁 Core options
┃ ┃ ┣ 🧩 Name_option

* Via the file `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core options

| Options | Options values | Variables (for configuration override) | Variables values |
| --- | --- | --- | --- |
| Debug menu and walk through walls | `Disabled` ✅ / `Enabled` | `easyrpg_debug_mode` | `disabled` / `enabled` |

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/EasyRPG/Player/](https://github.com/EasyRPG/Player/)
* **Libretro documentation**: [https://docs.libretro.com/library/easyrpg/](https://docs.libretro.com/library/easyrpg/)
* **RPG Maker 2000 Project**: [https://community.easyrpg.org/t/rpg-maker-2000-preservation-project/199](https://community.easyrpg.org/t/rpg-maker-2000-preservation-project/199)
* **RPG Maker**: [https://rpgmaker.net/](https://rpgmaker.net/)