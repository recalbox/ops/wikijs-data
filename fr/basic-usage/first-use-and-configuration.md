---
title: Première utilisation et configuration
description: Pour configurer votre système Recalbox pour la première fois
published: true
date: 2024-06-02T19:40:46.584Z
tags: configuration, première utilisation
editor: markdown
dateCreated: 2021-05-21T07:49:59.419Z
---

## Premiers pas

### Premier démarrage

Après l’installation, la première chose à faire est de connecter votre boitier avec Recalbox à votre téléviseur via le câble HDMI. Pour alimenter et démarrer votre Recalbox, il suffit de brancher le câble d’alimentation micro USB ou USB-C.

>Beaucoup de manettes fonctionnent directement lors de la première utilisation de Recalbox. Cependant, si vous voulez configurer directement une manette USB, branchez un clavier USB, ... veuillez consulter la partie [Configuration d'une manette](#configuration-dune-manette).
{.is-info}

### Mettre Recalbox en Français

Sur l'écran de sélection des systèmes, appuyez sur `START`. Choisissez `SYSTEM SETTINGS` > `LANGUAGE`, sélectionnez `FRANÇAIS` et revenez en arrière. Recalbox va vous indiquer qu'il doit redémarrer. Une fois le démarrage terminé, les menus seront entièrement en français.

###  Arrêter Recalbox

Sur l'écran de sélection des systèmes, appuyez sur `SELECT` et choisissez `ÉTEINDRE`. Attendez que la LED verte/orange du Pi cesse de clignoter afin de ne pas endommager la carte microSD et une fois qu'elle ne clignote plus, débranchez le câble d’alimentation.

### Jeux par défaut

Recalbox est livré avec quelques jeux libres de droits (homebrew) et/ou démos pour plusieurs systèmes.

## Configuration d'une manette

### Ajouter et configurer une manette USB

Vous pouvez ajouter des manettes USB sur Recalbox. La plupart des modèles sont compatibles (voir la [liste de compatibilité](./../hardware-compatibility/compatible-devices/controllers)).

Après avoir branché votre manette USB (ou appairé votre manette Bluetooth), appuyez sur `START` avec une manette déjà configuré. Sélectionnez `RÉGLAGES MANETTES` et suivez les instructions.

![](/basic-usage/first-use-and-configuration/padconfig1-fr.png)

Sélectionnez `CONFIGURER MANETTES` et suivez les instructions.

![](/basic-usage/first-use-and-configuration/padconfig2-fr.png)

Appuyez sur une touche pour commencer la configuration.

![](/basic-usage/first-use-and-configuration/padconfig3-fr.png)

Maintenez n'importe quel bouton sur la manette pour la configurer.

![](/basic-usage/first-use-and-configuration/padconfig4-fr.png)

Vous pouvez maintenant assigner les boutons de votre manette. Les noms utilisés pour assigner les boutons sont ceux de la manette Super Nintendo :

![Manette Super Nintendo (SNES)](/basic-usage/first-use-and-configuration/snespad.jpg)

>Les boutons L1, R1, L2, R2, L3 et R3 sont basés sur la manette Playstation.
{.is-info}

>**HOTKEY**
>
>Le dernier bouton à configurer, **HOTKEY**, est celui qui servira à activer les [commandes spéciales](./getting-started/special-commands) lorsque l'on est dans un émulateur :
>
>Pour les manettes Xbox 360 et Xbox One, le bouton **Hotkey** est assigné au bouton `HOME`.
>Pour les manettes PS3 et PS4, le bouton **Hotkey** est assigné au bouton `PS`.
>
>S'il n'y a pas de bouton dédié ou pas encore assigné, il est recommandé d'assigner **Hotkey** au bouton `Select`.
{.is-warning}

>Pour passer n'importe quel bouton (sauf **Hotkey**), pressez la touche `Bas` pour passer au bouton suivant.
{.is-info}

>Pour les manettes à 6 boutons (SNES, arcade, ...), les boutons sont assignés en correspondance avec la manette SNES (voir ci-dessus).
>Pour les manettes à 2 boutons (NES, PC Engine, Gameboy, ...), les boutons assignés sont `A` et `B`.
{.is-warning}

![](/basic-usage/first-use-and-configuration/padconfig5-fr.png)

![](/basic-usage/first-use-and-configuration/padconfig6-fr.png)

### Assignation de la manette

* De retour sur l'écran de configuration, vous pouvez assigner la manette à un joueur.
* Sélectionnez `JOUEUR 1` afin de lui assigner une manette.

![](/basic-usage/first-use-and-configuration/padassign1-fr.png)

* Sélectionnez une manette.

![](/basic-usage/first-use-and-configuration/padassign2-fr.png)

* Votre manette est maintenant configurée.

![](/basic-usage/first-use-and-configuration/padassign3-fr.png)

### Utiliser un clavier

Si vous n'arrivez pas à configurer votre manette, vous pouvez brancher un clavier USB filaire à votre Recalbox pour le configurer. Vous trouverez toutes les combinaisons de touches du clavier [ici](./../tutorials/others/emulationstation-use-with-keyboard-and-mouse).

### Manette PS3

Pour **associer** une manette PS3 :

* Connectez d’abord la manette au port USB (via un câble adapté) et attendez 10 secondes.
* Après cela, vous pouvez déconnecter la manette et appuyez sur le bouton **PS** pour lancer la connexion sans fil.

>**Remarque :**  
>Pour les copies asiatiques de la PS3 Dualshock 3 (comme GASIA ou SHANWAN), vous aurez besoin d’ajuster [quelques paramètres](./../tutorials/controllers/controllers/ps3-controllers-drivers).
{.is-warning}

>**Attention :**  
>En cas de doute sur l’alimentation et de la consommation d’énergie, il vaut mieux éviter de charger la manette PS3 sur le Raspberry Pi car cela peut causer des problèmes de stabilité.
>
>Veuillez branchez la manette sur le Raspberry Pi uniquement pour associer votre manette à votre Recalbox.
{.is-danger}

Si vous comprenez les paramètres impliqués ou voulez utiliser votre manette avec une connexion USB, vous devez désactiver le pilote bluetooth PS3 dans recalbox.conf en trouvant et en modifiant la ligne `controllers.ps3.enabled=0`.

Rappelez-vous que la configuration des manettes dans Recalbox est basée sur la configuration des boutons SNES :

| Manette PS3 | Manette SNES |
| :---: | :---: |
| X | B |
| ◯ | A |
| ⬜ | Y |
| △ | X |

>**Information :**  
>Par défaut, **HOTKEY** est associé au bouton **PS** (celui au milieu de la manette).  
>Pour plus d'information sur HOTKEY, voir la page sur les [commandes spéciales](./getting-started/special-commands).
{.is-info}

### Manette Xbox 360 

>**Remarque :**  
>Les manettes sans fil Xbox 360 ont besoin d'un dongle de réception sans fil spécifique.
{.is-warning}

Rappelez-vous que la configuration des manettes dans Recalbox est basée sur la configuration des boutons SNES :

| Manette Xbox 360 | Manette SNES |
| :---: | :---: |
| A | B |
| B | A |
| X | Y |
| Y | X |

>**Information :**  
>Par défaut, **HOTKEY** est associé au bouton **HOME** (celui au milieu de la manette).  
>Pour plus d'information sur **HOTKEY,** voir la page sur les [commandes spéciales](./getting-started/special-commands).
{.is-info}

### Ajouter une manette Bluetooth

Pour ajouter une manette Bluetooth :

* Mettez votre manette en mode _appairage_.
* Ensuite, appuyez sur `START` et choisissez `RÉGLAGE MANETTES`
* Puis `ASSOCIER UNE MANETTE BLUETOOTH`.

Une **liste de manetttes** détectés apparaît !

* Il suffit de sélectionner le vôtre et votre manette est appairé.
* Vous pouvez maintenant passer à la configuration (s'il la manette n’est pas déjà supporté par Recalbox).

>**Information :**  
>Pour les manettes **8bitdo**, veuillez consulter la page [**8bitdo sur Recalbox**](./../hardware-compatibility/compatible-devices/8bitdo-on-recalbox).
{.is-info}


### Contrôleur GPIO

Vous pouvez connecter vos boutons et joysticks arcade directement sur les GPIO du Raspberry Pi (voir la page [**Les contrôleurs GPIO**](./../tutorials/controllers/gpio/gpio-controllers)). Vous pouvez également connecter des manettes originales de PSOne, Nes, Snes, Megradrive , ... (voir **contrôleurs DB9** et **contrôleurs Gamecon**).

### Manettes virtuelles

>Les manettes virtuelles ne sont pas accessibles sur les Raspberry Pi0 et Pi1 par défaut.
{.is-info}

Avec le projet de Miroof Virtual Gamepads, vous pouvez ajouter jusqu'à 4 manettes avec vos smartphones/tablettes !

Pour cela, démarrez le navigateur web de votre smartphone puis tapez l'adresse IP de votre Recalbox suivie du port de communication (le port `:8080`). 

>**Information :**  
>Vous pouvez trouver l'adresse IP de votre Recalbox dans le menu `OPTIONS RÉSEAU`
{.is-info}

![Zones de touche de la manette virtuelle](/basic-usage/first-use-and-configuration/virutalgamepad_touch_zones.png)

## Utilisation d'un stockage externe

>**Information :**  
>Vous pouvez très facilement **utiliser un périphérique USB de stockage** (clé USB, disque dur externe auto alimenté etc.) pour stocker vos roms, fichiers perso etc...
>
>**Avec cette méthode**, le **système** (sur la carte SD) et la **partition share** (sur le périphérique) **sont séparés.**
>  
>Donc **si vous deviez réinstaller votre système**, vous **conserveriez toutes vos données utilisateur.**
>Vous n'aurez alors qu'à **rebrancher votre périphérique** à votre Recalbox, puis **le sélectionner dans le système, et jouer.**
{.is-info}

### Le format de votre support

Premièrement, vous devez utiliser **un périphérique utilisant un système de fichiers** suivant : **FAT32, EXFAT, EXT4 ou NTFS.**

>**Remarque :**
>
>Il est vivement recommandé d'utiliser le système de fichier **EXFAT**.
{.is-info}

>**Attention :**
>
>Les taux de transferts peuvent **être lents** dans le cas du **NTFS.**  
>Soyez également certain que **le système de fichiers** que vous choisirez **soit bien compatible avec le système d'exploitation de votre PC.**
{.is-danger}

| Formats | Taille maximale du volume | Taille maximale d'un fichier | Nombre maximal de fichiers | Mode lecture/écriture |
| :---: | :---: | :---: | :---: |  :---:|
| Fat | 2Tio | 4Gio (4,2949Go) | Supérieur à **250 millions** | Windows : &#x2705; Linux : &#x2705; |
| NTFS | 256Tio (2 815,8492To) | 16 Tio (17,5921To) | 4 294 967 295 | Windows : &#x2705; Linux : &#x2705; |
| exFAT | 128Pio (144 115,1880To) | 128Pio (144 115,1880To) | 2 796 202 par répertoire | Windows : &#x2705; Linux : &#x2705; |
| ext4 | 1 Eio (1 152 921,5046To - limité à 16 Tio par e2fsprogs) | 16 Tio (17,5921To) | 4 milliards | Windows : &#x274C; Lecture/Ecriture via ext2fsd Linux : &#x2705; |

>**Attention :**
>
>Recalbox **ne formatera pas** votre périphérique, il ne **créera que de nouveaux fichiers dessus.**
{.is-danger}

### Configuration

Pour configurer Recalbox pour **utiliser un périphérique de stockage USB.**

* Branchez votre périphérique à votre Recalbox, puis allumez-la.
* Une fois sous l'interface de Recalbox, pressez le bouton `START` de votre manette, allez dans `OPTIONS SYSTÈME` puis dans `MÉDIA DE STOCKAGE`.
* Maintenant, sélectionnez votre périphérique dans la liste, puis validez et attendez que le système redémarre.

>**Information :**
>
>**Durant cette phase de redémarrage**, Recalbox va **créer à la racine** de votre périphérique **un nouveau répertoire** nommé **"recalbox"** qui contiendra toute l'arborescence de votre **partition "share".**
{.is-info}

### Dépannage

#### Disque dur invisible

Que faire si après le redémarrage, Recalbox ne voit toujours pas le disque dur ?

>**Informations :** 
>
>Parfois, après avoir sélectionné le périphérique dans EmulationStation et redémarré, Recalbox ne parvient pas à créer le système de fichiers sur celui-ci. Il continue alors d'utiliser les fichiers de la carte SD. Cela se produit notamment avec certains disques durs assez lents à s'initialiser.
{.is-info}

Ce que vous pouvez faire:

* Connectez-vous via [SSH](./../tutorials/system/access/root-access-terminal-cli)
* Montez la [partition de démarrage](./../tutorials/system/access/remount-partition-with-write-access) en lecture/écriture.
* Tapez les commandes suivantes : `cd /boot` et `nano recalbox-boot.conf`
* Rajoutez cette ligne `sharewait=30`. Cette valeur s'exprime en secondes.
* Sauvegardez par `Ctrl+X`,`Y`,`Entrée`. 
* Tapez `reboot` et validez pour que Recalbox redémarre.

## Réseau

### Configurer via le câble RJ45

* Brancher votre câble RJ45 à votre Recalbox et c'est tout, votre Recalbox est configurée.

### Configurer le Wifi

* Dans l'interface générale, appuyez sur `Start` > `OPTIONS RÉSEAU`. Pour activer, mettre l'option `ACTIVER LE WIFI` sur **On**.
* Vous devez maintenant configurer le wifi via le WPS. Sur votre box Internet, vous devez avoir un bouton sur le modem ou dans l'interface de votre modem pour activer la fonction WPS. Une fois appuyé, celle-ci reste activée que 2 minutes.
* Toujours dans Recalbox, sélectionnez `CONNEXION WPS` en bas du menu. Celui-vi va tenter de trouver toutes les connexions WPS existantes et activées autour.
* Une fois votre connexion trouvée, elle sera automatiquement sauvegardé dans votre Recalbox.