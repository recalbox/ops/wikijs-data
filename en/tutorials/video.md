---
title: 🎞️ Video
description: 
published: true
date: 2021-08-06T23:05:02.058Z
tags: tutorial, video
editor: markdown
dateCreated: 2021-08-06T07:58:14.171Z
---

If you're here, it's because you're looking for a tutorial on everything related to video!

Here are the available categories:

[CRT](crt)
[Display configuration](display-configuration)
[HDMI](hdmi)
[LCD](lcd)
[TFT](tft)