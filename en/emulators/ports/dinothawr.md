---
title: DinoThawr
description: 
published: true
date: 2021-08-08T23:50:17.862Z
tags: ports, dinothawr
editor: markdown
dateCreated: 2021-06-29T18:35:10.153Z
---

![](/emulators/ports/dinothawr.png){.align-center}

## Technical data

* **Author**: Themaister, Agnes Heyer 
* **Levels Design**: Agnes Heyer 
* **Programmation**: Hans-Kristian Arntzen 
* **Music**: Hans-Kristian Arntzen 
* **Some level designs**: Hans-Kristian Arntzen 
* **Genre**: Puzzle

## Presentation 

**Dinothawr** is a puzzle game pushing blocks on slippery surfaces.

Our hero is a dinosaur whose friends are trapped in the ice. Through puzzles, your task is to free the dinosaurs from their ice prison.

## Emulators

[Libretro Dinothawr](libretro-dinothawr)