---
title: Libretro Mupen64plus_Next
description: 
published: true
date: 2024-07-05T14:54:04.968Z
tags: libretro, 64dd, n64dd, mupen64plus next
editor: markdown
dateCreated: 2023-09-07T06:34:09.088Z
---

## ![](/emulators/license.svg) Licencia

El core posee la siguiente licencia [**GPLv2**](https://github.com/libretro/mupen64plus-libretro-nx/blob/develop/LICENSE).

## ![](/emulators/compatibility.png) Compatibilidad

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | 
| ❌ | ✅ | ✅ | ✅ | ❌ | ✅ &nbsp;🐌 | ❌ | ✅ |

🐌 Bajo rendimiento pero jugable

## ![](/emulators/features.png) Funcionalidades

| Funcionalidad | ¿Soportada? |
| :---: | :---: |
| Reinicio | ✔ |
| Capturas de pantalla | ✔ |
| Grabaciones | ✔ |
| Grabaciones instantáneas | ✔ |
| Rebobinado | ✔ |
| Opciones del core | ✔ |
| RetroAchievements | ✔ |
| Cheats RetroArch | ✔ |
| Controles | ✔ |
| Remapping | ✔ |
| Vibración | ✔ |
| Subsistemas | ✔ |
| Softpatching | ✔ |


## ![](/emulators/bios.svg) BIOS

### Lista de BIOS obligatorias

| Nombre del fichero | Descripción | MD5 | ¿Preinstalada? |
| :---: | :---: | :---: | :---: |
| IPL.n64 | BIOS N64DD | 8d3d9f294b6e174bc7b1d2fd1c727530 | ❌ |

### Emplazamiento

Disponed vuestras bios como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 Mupen64plus
┃ ┃ ┃ ┃ ┣ 🗒 **IPL.n64**

## ![](/emulators/roms.png) Roms

### Extensiones de roms soportadas

Las roms deben tener las extensiones siguientes:

* .bin
* .ndd
* .n64
* .v64
* .z64

### Emplazamiento

Disponed vuestras roms como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 64dd
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.ndd**

>Se aconsejan las rom del proyecto **No-Intro**.
{.is-success}

>Para más información sobre las roms, consultad [este tutorial](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

>Para conservar las configuraciones personalizadas cuando se produce una actualización del sistema Recalbox, os aconsejamos utilizar las [Sobrecargas de configuración](./../../../advanced-usage/configuration-override).
{.is-info}

### Acceder a las opciones

Podéis configurar las opciones del core de dos formas diferentes.

* Via el Menú RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via el fichero `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Opciones del core

| Opción | Valor de la opción | Variable (para la sobrecarga) | Valores posibles |
| --- | --- | --- | --- |
| RDP Plugin | `GLideN64` ✅ | `mupen64plus-rdp-plugin` | `gliden64` |
| 4:3 Resolution | `320x240` / `640x480` ✅ / `960x720` / `1280x960` / `1440x1080` / `1600x1200` / `1920x1440` / `2240x1680` / `2560x1920` / `2880x2160` / `3200x2400` / `3520x2640` / `3840x1880` | `mupen64plus-43screensize` | `320x240` / `640x480` / `960x720` / `1280x960` / `1440x1080` / `1600x1200` / `1920x1440` / `2240x1680` / `2560x1920` / `2880x2160` / `3200x2400` / `3520x2640` / `3840x1880` |
| Wide Resolution | `640x360 (16:9)` / `960x540 (16:9)` ✅ / `1280x720 (16:9)` / `1706x720 (64:27)` / `1366x768 (16:9)` / `1920x810 (64:27)` / `1920x1080 (16:9)` / `2560x1080 (64:27)` / `2560x1440 (16:9)` / `3141x1440 (64:27)` / `3840x2160 (16:9)` / `4096x2160 (17:9)` / `5120x2160 (64:27)` / `7680x3240 (64:27)` / `7680x4320 (16:9)` / `10240x4320 (64:27)` | `mupen64plus-169screensize` | `640x360` / `960x540` / `1280x720` / `1706x720` / `1366x768` / `1920x810` / `1920x1080` / `2560x1080` / `2560x1440` / `3141x1440` / `3840x2160` / `4096x2160` / `5120x2160` / `7680x3240` / `7680x4320` / `10240x4320` |
| Aspect Ratio | `Original (4:3)` / `Wide (Stretched)` / `Wide (Ajusted)` | `mupen64plus-aspect` | `4:3` / `16:9` / `16:9 adjusted` |
| Native Resolution Factor | `Désactivé` ✅ / `1x` / `2x` / `3x` / `4x` / `5x` / `6x` / `7x` / `8x` | `mupen64plus-EnableNativeResFactor` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` |
| Threaded Renderer | `Activé` / `Désactivé` ✅ | `mupen64plus-ThreadedRenderer` | `True` / `False` |
| Bilinear filtering mode | `3point` / `standard` ✅ | `mupen64plus-BilinearMode` | `3point` / `standard` |
| Hybrid Filter | `Activé` ✅ / `Désactivé` | `mupen64plus-HybridFilter` | `True` / `False` |
| Dithering | `Activé` / `Désactivé` ✅ | `mupen64plus-DitheringPattern` | `True` / `False` |
| Dithering Quantization | `Activé` / `Désactivé` ✅ | `mupen64plus-DitheringQuantization` | `True` / `False` |
| Image Dithering Mode | `Désactivé` ✅ / `Bayer` / `Magic Square` / `Blue Noise` | `mupen64plus-RDRAMImageDitheringMode` | `False` / `Bayer` / `MagicSquare` / `BlueNoise` |
| MSAA level | `0` ✅ / `2` / `4` / `8` / `16` | `mupen64plus-MultiSampling` | `0` / `2` / `4` / `8` / `16` |
| FXAA | `0` ✅ / `1` | `mupen64plus-FXAA` | `0` / `1` |
| LOD Emulation | `Disbaled` / `Activé` ✅ | `mupen64plus-EnableLODEmulation` | `False` / `True` |
| Framebuffer Emulation | `Désactivé` / `Activé` ✅ | `mupen64plus-EnableFBEmulation` | `False` / `True` |
| Copy auxiliary buffers to RDRAM | `Désactivé` ✅ / `Activé` | `mupen64plus-EnableCopyAuxToRDRAM` | `False` / `True` |
| Color buffer to RDRAM | `Désactivé` / `Sync` / `DoubleBuffer` ✅ / `TripleBuffer` | `mupen64plus-EnableCopyColorToRDRAM` | `Off` / `Sync` / `Async` / `TripleBuffer` |
| Depth buffer to RDRAM | `Désactivé` / `Software` ✅ / `FromMem` | `mupen64plus-EnableCopyDepthToRDRAM` | `Off` / `Software` / `FromMem` |
| Background Mode | `Stripped` / `OnePiece` ✅ | `mupen64plus-BackgroundMode` | `Stripped` / `OnePiece` |
| Hardware per-pixel lighting | `Désactivé` ✅ / `Activé` | `mupen64plus-EnableHWLighting` | `False` / `True` |
| Continuous texrect coords | `Désactivé` ✅ / `Auto` / `Force` | `mupen64plus-CorrectTexrectCoords` | `Off` / `Auto` / `Force` |
| Enbale inaccurate texture coordinates | `Désactivé` ✅ / `Activé` | `mupen64plus-EnableInaccurateTextureCoordinates` | `False` / `True` |
| Enable native-res boundaries for texture coordinates | `Désactivé` ✅ / `Activé` | `mupen64plus-EnableTexCoordBounds` | `False` / `True` 
| Native res. 2D texrects | `Désactivé` ✅ / `Unoptimized` / `Optimized` | `mupen64plus-EnableNativeResTexrects` | `Disabled` / `Unoptimized` / `Optimized` |
| Less accurate blending mode | `Désactivé` / `Activé` ✅ | `mupen64plus-EnableLegacyBlending` | `False` / `True` |
| GPU shader depth write | `Désactivé` ✅ / `Activé` | `mupen64plus-EnableFragmentDepthWrite` | `False` / `True` |
| Cache Textures | `Désactivé` / `Activé` ✅ | `mupen64plus-EnableTextureCache` | `False` / `True` |
| Overscan | `Désactivé` / `Activé` ✅ | `mupen64plus-EnableOverscan` | `Disabled` / `Activé` |
| Overscan Offset (Top) | `0` ✅ / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `21` / `22` / `23` / `24` / `25` / `26` / `27` / `28` / `29` / `30` / `31` / `32` / `33` / `34` / `35` / `36` / `37` / `38` / `39` / `40` / `41` / `42` / `43` / `44` / `45` / `46` / `47` / `48` / `49` / `50` | `mupen64plus-OverscanTop` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `21` / `22` / `23` / `24` / `25` / `26` / `27` / `28` / `29` / `30` / `31` / `32` / `33` / `34` / `35` / `36` / `37` / `38` / `39` / `40` / `41` / `42` / `43` / `44` / `45` / `46` / `47` / `48` / `49` / `50` |
| Overscan Offset (Left) | `0` ✅ / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `21` / `22` / `23` / `24` / `25` / `26` / `27` / `28` / `29` / `30` / `31` / `32` / `33` / `34` / `35` / `36` / `37` / `38` / `39` / `40` / `41` / `42` / `43` / `44` / `45` / `46` / `47` / `48` / `49` / `50` | `mupen64plus-OverscanLeft` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `21` / `22` / `23` / `24` / `25` / `26` / `27` / `28` / `29` / `30` / `31` / `32` / `33` / `34` / `35` / `36` / `37` / `38` / `39` / `40` / `41` / `42` / `43` / `44` / `45` / `46` / `47` / `48` / `49` / `50` |
| Overscan Offset (Right) | `0` ✅ / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `21` / `22` / `23` / `24` / `25` / `26` / `27` / `28` / `29` / `30` / `31` / `32` / `33` / `34` / `35` / `36` / `37` / `38` / `39` / `40` / `41` / `42` / `43` / `44` / `45` / `46` / `47` / `48` / `49` / `50` | `mupen64plus-OverscanRight` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `21` / `22` / `23` / `24` / `25` / `26` / `27` / `28` / `29` / `30` / `31` / `32` / `33` / `34` / `35` / `36` / `37` / `38` / `39` / `40` / `41` / `42` / `43` / `44` / `45` / `46` / `47` / `48` / `49` / `50` |
| Overscan Offset (Bottom) | `0` ✅ / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `21` / `22` / `23` / `24` / `25` / `26` / `27` / `28` / `29` / `30` / `31` / `32` / `33` / `34` / `35` / `36` / `37` / `38` / `39` / `40` / `41` / `42` / `43` / `44` / `45` / `46` / `47` / `48` / `49` / `50` | `mupen64plus-OverscanBottom` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `21` / `22` / `23` / `24` / `25` / `26` / `27` / `28` / `29` / `30` / `31` / `32` / `33` / `34` / `35` / `36` / `37` / `38` / `39` / `40` / `41` / `42` / `43` / `44` / `45` / `46` / `47` / `48` / `49` / `50` |
| Max High-Res VRAM Limit | `0` ✅ / `500` / `1000` / `1500` / `2000` / `2500` / `3000` / `3500` / `4000` | `mupen64plus-MaxHiResTxVramLimit` | `0` / `500` / `1000` / `1500` / `2000` / `2500` / `3000` / `3500` / `4000` |
| Max texture cache size | `1500` / `4000` / `8000` ✅ | `mupen64plus-MaxTxCacheSize` | `1500` / `4000` / `8000` |
| Texture filter | `None` ✅ / `Smooth filtering 1` / `Smooth filtering 2` / `Smooth filtering 3` / `Smooth filtering 4` / `Sharp filtering 1` / `Sharp filtering 2` | `mupen64plus-txFilterMode` | `None` / `Smooth filtering 1` / `Smooth filtering 2` / `Smooth filtering 3` / `Smooth filtering 4` / `Sharp filtering 1` / `Sharp filtering 2` |
| Texture Enhancement | `None` ✅ / `As Is` / `X2` / `X2SAI` / `HQ2X` / `HQ2XS` / `LQ2X` / `LQ2XS` / `HQ4X` / `2xBRZ` / `3xBRZ` / `4xBRZ` / `5xBRZ` / `6xBRZ` | `mupen64plus-txEnhancementMode` | `None` / `As Is` / `X2` / `X2SAI` / `HQ2X` / `HQ2XS` / `LQ2X` / `LQ2XS` / `HQ4X` / `2xBRZ` / `3xBRZ` / `4xBRZ` / `5xBRZ` / `6xBRZ` |
| Don't filter background textures | `Désactivé` / `Activé` ✅ | `mupen64plus-txFilterIgnoreBG` | `False` / `True` |
| Use High-Res textures | `Désactivé` ✅ / `Activé` | `mupen64plus-txHiresEnable` | `False` / `True` |
| Use High-Res Texture Cache Compression | `Désactivé` / `Activé` ✅ | `mupen64plus-txCacheCompression` | `False` / `True` |
| Use High-Res Full Alpha Channel | `Désactivé` ✅ / `Activé` | `mupen64plustxHiresFullAlphaChannel` | `False` / `True` |
| Use extended Texture Storage | `Désactivé` ✅ / `Activé` | `mupen64plus-EnableEnhancedTextureStorage` | `False` / `True` |
| Use alternate method for High-Res Checksums | `Désactivé` ✅ / `Activé` | `mupen64plus-EnableHiResAltCRC` | `False` / `True` |
| Use enhanced Hi-Res Storage | `Désactivé` ✅ / `Activé` | `mupen64plus-EnableEnhancedHiResStorage` | `False` / `True` |
| INI Behaviour | `Prioritize INI over Core Options` ✅ / `Prioritize Core Options over INI` / `Disable INI` | `mupen64plus-GlideN64IniBehaviour` | `late` / `early` / `disabled` |
| CPU Core | `Pure Interpreter` / `Cached Interpreter` / `Dynarec` ✅ | `mupen64plus-cpucore` | `pure_interpreter` / `cached_interpreter` / `dynarec_recompiler` |
| RSP Plugin | `HLE` ✅ | `mupen64plus-rsp-plugin` |  |
| Frame Duplication | `Désactivé` ✅ / `Activé` | `mupen64plus-FrameDuping` | `False` / `True` |
| Framerate | `Original` ✅ / `Fullspeed` | `mupen64plus-Framerate` | `Original` / `Fullspeed` |
| VI Refresh (Overclock) | `Auto` ✅ / `1500` / `2200` | `mupen64plus-virefresh` | `Auto` / `1500` / `2200` |
| Disable Expansion Pak | `Désactivé` ✅ / `Activé` | `mupen64plus-ForceDisableExtraMem` | `False` / `True` |
| Ignore emulated TLB Exceptions | `Don't Ignore` ✅ / `Ignore TLB Exceptions if not using TLB` / `Always Ignore TLB Exceptions` | `mupen64plus-IgnoreTLBExceptions` | `False` / `OnlyNotActivé` / `AlwaysIgnoreTLB` |
| Count Per Op | `0` ✅ / `1` / `2` / `3` / `4` / `5` | `mupen64plus-CountPerOp` | `0` / `1` / `2` / `3` / `4` / `5` |
| Count Per Op Divider (Overclock) | `0` ✅ / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `1
1` | `mupen64plus-CountPerOpDenomPot` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` |
| Analog Deadzone (percent) | `0` / `5` / `10` / `15` ✅ / `20` / `25` / `30` | `mupen64plus-astick-deadzone` | `0` / `5` / `10` / `15` / `20` / `25` / `30` |
| Analog Sensitivity (percent) | `50` / `55` / `60` / `65` / `70` / `75` / `80` / `85` / `90` / `95` / `100` ✅ / `105` / `110` / `115` / `120` / `125` / `130` / `135` / `140` / `145` / `150` | `mupen64plus-astick-sensitivity` | `50` / `55` / `60` / `65` / `70` / `75` / `80` / `85` / `90` / `95` / `100` / `105` / `110` / `115` / `120` / `125` / `130` / `135` / `140` / `145` / `150` |
| Right C Button | `C1` ✅ / `C2` / `C3` / `C4` | `mupen64plus-r-cbutton` | `C1` / `C2` / `C3` / `C4` |
| Left C Button | `C1` / `C2` ✅ / `C3` / `C4` | `mupen64plus-l-cbutton` | `C1` / `C2` / `C3` / `C4` |
| Down C Button | `C1` / `C2` / `C3` ✅ / `C4` | `mupen64plus-d-cbutton` | `C1` / `C2` / `C3` / `C4` |
| Up C Button | `C1` / `C2` / `C3` / `C4` ✅ | `mupen64plus-u-cbutton` | `C1` / `C2` / `C3` / `C4` |
| Independent C-buttons Controls | `Désactivé` ✅ / `Activé` | `mupen64plus-alt-map` | `False` / `True` |
| Player 1 Pak | `none` / `memory` ✅ / `rumble` / `transfert` | `mupen64plus-pak1` | `none` / `memory` / `rumble` / `transfert` |
| Player 2 Pak | `none` ✅ / `memory` / `rumble` / `transfert` | `mupen64plus-pak2` | `none` / `memory` / `rumble` / `transfert` |
| Player 3 Pak | `none` ✅ / `memory` / `rumble` / `transfert` | `mupen64plus-pak3` | `none` / `memory` / `rumble` / `transfert` |
| Player 4 Pak | `none` ✅ / `memory` / `rumble` / `transfert` | `mupen64plus-pak4` | `none` / `memory` / `rumble` / `transfert` |

## ![](/emulators/external-links.png) Enlaces externos

* **Código fuente utilizado** : [https://github.com/libretro/mupen64plus-libretro-nx/](https://github.com/libretro/mupen64plus-libretro-nx/)
* **Documentación Libretro** : [https://docs.libretro.com/library/mupen64plus/](https://docs.libretro.com/library/mupen64plus/)