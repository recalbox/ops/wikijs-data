---
title: Libretro Tic80
description: 
published: true
date: 2024-07-24T20:56:03.613Z
tags: libretro, tic-80, tic80
editor: markdown
dateCreated: 2021-07-04T18:21:52.322Z
---

Libretro TIC-80 is an emulator that allows you to emulate a fantastic computer to create, play and share small games.

## ![](/emulators/license.svg) License

This core is under [**MIT**](https://github.com/libretro/TIC-80/blob/master/LICENSE) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Features

| Feature | Supported |
| :---: | :---: |
| Restart | ✔ |
| Saves | ✔ |
| Core Options | ✔ |
| Netplay | ✔ |
| Native Cheats | ✔ |
| Controls | ✔ |

## ![](/emulators/bios.svg) BIOS

>**No bios is required.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Supported extensions

Roms must have the extension:

* .fd
* .sap
* .k7
* .m7
* .rom
* .zip
* .7z

This system supports compressed roms in .zip/.7z format. However, be careful, it is only an archive.

The files contained in the .zip/.7z must correspond to the extensions mentioned above.
Moreover, each .zip/.7z file must contain only one compressed rom.

### Location

Put your roms like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 tic80
┃ ┃ ┃ ┃ ┣ 🗒 **game.zip**

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Accessing to options

You can configure various options in two different ways.

* Via RetroArch menu:

┣ 📁 RetroArch Menu
┃ ┣ 📁 Core options
┃ ┃ ┣ 🧩 Name_option

* Via the file `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core options

| Options | Options values | Variables (for configuration override) | Variables values |
| --- | --- | --- | --- |
| Crop Border | `Désactivé` ✅ / `Activé` | `tic80_crop_border` | `disabled` / `enabled` |
| Pointer Device | `Mouse` ✅ / `Touchscreen (Pointer API)` / `Left Analog` / `Right Analog` / `D-Pad` | `tic80_pointer_device` | `mouse` / `touchscreen` / `left_analog` / `right_analog` / `dpad` |
| Pointer Speed | `50%` / `60%` / `70%` / `80%` / `90%` / `100%` ✅ / `110%` / `120%` / `130%` / `140%` / `150%` / `160%` / `170%` / `180%` / `190%` / `200%` / `210%` / `220%` / `230%` / `240%` / `250%` / `260%` / `270%` / `280%` / `290%` / `300%` | `tic80_pointer_speed` | `50` / `60` / `70` / `80` / `90` / `100` / `110` / `120` / `130` / `140` / `150` / `160` / `170` / `180` / `190` / `200` / `210` / `220` / `230` / `240` / `250` / `260` / `270` / `280` / `290` / `300` |
| Mouse Cursor | `Désactivé` ✅ / `Dot` / `Cross` / `Arrow` | `tic80_mouse_cursor` | `disabled` / `dot` / `cross` / `arrow` |
| Mouse Cursor Color | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` ✅ | `tic80_mouse_cursor_color` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` |
| Mouse Cursor Hide Delay | `Disabled` / `1` / `2` / `3` / `4` / `5` ✅ / `6` / `7` / `8` / `9` / `10` | `tic80_mouse_hide_delay` | `disabled` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` |
| Gamepad Analog Deadzone | `0%` / `3%` / `6%` / `9%` / `12%` / `15%` ✅ / `18%` / `21%` / `24%` / `27%` / `30%` | `tic80_analog_deadzone` | `0` / `3` / `6` / `9` / `12` / `15` / `18` / `21` / `24` / `27` / `30` |

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/nesbox/TIC-80/](https://github.com/nesbox/TIC-80/)
* **Official website**: [https://tic.computer/](https://tic.computer/)
* **Official wiki**: [https://github.com/nesbox/TIC-80/wiki](https://github.com/nesbox/TIC-80/wiki)
* **Official Libretro database**: [Libretro TIC-80 Database](https://github.com/libretro/libretro-database/blob/master/rdb/TIC-80.rdb)
* **Libretro Tic-80 cheats**: [https://github.com/libretro/libretro-database/tree/master/cht/TIC-80/](https://github.com/libretro/libretro-database/tree/master/cht/TIC-80)