---
title: RetroAchievements
description: 
published: true
date: 2024-06-02T20:39:32.075Z
tags: retroachievements, trophäen, errungenschaften
editor: markdown
dateCreated: 2023-07-21T06:04:48.061Z
---

## Einführung in RetroAchievements

Die Retrogaming-Community hat eine Plattform [retroachievements.org](http://www.retroachievements.org/)
für Spieler, die Herausforderungen in ihren Lieblingsspielen meistern wollen.

Diese Plattform unterstützt einen großen Teil der alten Videospielsysteme (NES, SNES, GB, GBC, GBA, Genesis, PC-Engine usw.) und bietet Herausforderungen für die meisten Spiele.

Recalbox ermöglicht es Dir, ein [retroachievements.org](http://www.retroachievements.org/) Konto zu verknüpfen, Dich Herausforderungen zu stellen und im Wettbewerb mit den besten Spielern zu sein. 

## Erstellung eines Kontos 

* Besuche [retroachievements.org](http://www.retroachievements.org/) und [erstelle ein Konto](http://retroachievements.org/createaccount.php)
* Merke Dir Deinen Benutzernamen und Dein Passwort, denn sie werden für die Einrichtung der Funktion in Recalbox verwendet.

## Konto in Recalbox einrichten

Das Konto wird über die Einstellungen in Emulationstation eingerichtet. 

* Öffne die EmulationStation-Einstellungen mit der Taste "START". 
* Gehe zu `SPIELEINSTELLUNGEN` > `RETROACHIEVEMENTS EINSTELLUNGEN`.
* Fülle folgende Felder aus:

  * `RETROACHIEVEMENTS`: AN
  * `BENUTZERNAME`: Dein Benutzername
  * `PASSWORT`: Dein Passwort

- Außerdem kann man den **Hardcore-Modus** aktivieren, der es ermöglicht, das Spiel im Originalformat zu spielen und die Funktionen des Emulators zu entfernen (Speicherstände, Rückspulen, Verwendung von Cheats usw.).

## Emulatoren

Die Retroachievements-Funktion unterstützt einen großen Teil der Recalbox-Emulatoren, allerdings werden einige von ihnen auch nicht unterstützt. Beispielsweise Game Cube und Wii-Emulatoren werden nicht unterstützt. Bei andere Emulatoren hingegen entscheidet die genutze Core.

Um die Kompatibilität eines Core zu überprüfen, lies bitte [die offizielle Tabelle zur Unterstützung von Retroachievements](https://docs.retroachievements.org/Emulator-Support-and-Issues/).

## Spiele und ROMs

Die meisten gängigen Spiele werden unterstützt. Lies [die offizielle Spieleliste](http://retroachievements.org/gameList.php), um sicherzugehen, dass dein Spiel unterstützt wird. 

Für jedes unterstützte Spiel werden nur einige ROMs unterstützt. Da Retroachievements.org ein amerikanisches Projekt ist, gibt es eine bessere Unterstützung für US und "No-Intro" ROMs, es sind aber auch einige europäische ROMs kompatibel. 

Du kannst die Kompatibilität deiner Spieldatei überprüfen, indem du einen Vergleich zwischen dem Hash deiner Datei und der "Liste der unterstützten Spieldateien" machst, die auf der Seite eines jeden Spiels unter dem Punkt "List of Supported Game Files" findest [retroachievements.org](http://www.retroachievements.org/).

## RetroAchievements in Aktion sehen

Um einen Blick auf deine Errungenschaften zu werfen, öffne im laufenden Spiel RetroArch mit den Tasten `Hotkey`+`B` und wähle dann `Errungenschaften` (Achievements).

Um wieder ins Spiel zurückzukehren, drücke `A`, um in das Retroarch Hauptmenü zu gelangen und wähle dann `Fortsetzen` mit der `B`-Taste.

