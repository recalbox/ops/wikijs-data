---
title: Connect your Recalbox to a CRT screen with composite
description: 
published: true
date: 2024-08-25T19:08:13.518Z
tags: crt, screen, composite
editor: markdown
dateCreated: 2021-08-06T19:27:44.055Z
---

If you want to connect your Recalbox to a CRT monitor, you will need a mini-jack to RCA cable like this one:

![RPi Audio/Video Socket](/tutorials/video/crt/rpi_av_socket.jpg)

>On camcorder cables, the video output could be on the right (red) audio socket.
>
>Moreover, as the type of cable needed is not really widespread, it is better to test with a multimeter that the cable used corresponds to the diagram proposed above (an inversion of the ground and video on the jack leads to a jumpy black & white image, sign of an unsuitable cable).
{.is-warning}

## Configuration

### config.txt file

>This file is replaced at each update. You will need to do the manipulations in this file each time!
{.is-warning}

* Modify the file [config.txt](./../../../tutorials/system/modification/configtxt-configuration) by finding the following line (there are 2, choose the one for your board):

```ini
dtoverlay=vc4-kms-v3d,cma-512
```

And replace with this:

```ini
dtoverlay=vc4-kms-v3d,composite=1
```

* Find the next line and comment on it by putting a ‘#’ in front of it, like this:

```ini
#hdmi_enable_4kp60=1
```

### recalbox-user-config.txt file

* Edit the file [recalbox-user-config.txt](./../../system/modification/configtxt-configuration) by commenting out all lines starting with `hdmi_` with the `#` symbol, and adding the supported `sdtv_mode` (one line only from the followings):

```ini
sdtv_mode=0    # Normal NTSC
sdtv_mode=1    # Japanese version of NTSC – no pedesta
sdtv_mode=2    # Normal PAL
sdtv_mode=3    # Brazilian version of PAL – 525/60 rather than 625/50, different subcarrier
```

* To get better sound from the composite output, enable the experimental audio driver for the Raspberry Pi :

```ini
audio_pwm_mode=2
```

* This mode can create a global slowdown of Recalbox. If this is the case, switch the line to comment and force the audio output to the jack:

```ini
#audio_pwm_mode=2
hdmi_drive=1
```

* Finally, add `hdmi_ignore_hotplug=1` to force composite. Your recalbox-user-config.txt file should look like this:

```ini
sdtv_mode=2
hdmi_ignore_hotplug=1
audio_pwm_mode=2
```

## Everything is slow with composite mode

The `audio_pwm_mode=2` improves the sound but can slow down the system in composite mode, especially on Pi zero. In this case, use the following mode:

```ini
audio_pwm_mode=0
```