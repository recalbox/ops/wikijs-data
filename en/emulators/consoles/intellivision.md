---
title: Intellivision
description: Mattel Intellivision
published: true
date: 2021-10-03T18:44:15.359Z
tags: consoles, mattel, intellivision
editor: markdown
dateCreated: 2021-07-27T20:05:36.983Z
---

![](/emulators/consoles/intellivision.svg){.align-center}

## Technical data

* **Manufacturer**: Mattel Electronics
* **Year of release**: 1979
* **Units sold**: 3+ millions
* **Best-selling game**: Las Vegas Poker & Blackjack
* **CPU**: GI CP1610 @ 894.886 kHz
* **RAM**: 1kB
* **Video**: General Instrument AY-3-8900-1
* **Resolution**: 159x96, 16 colors
* **Sound**: General Instrument AY-3-8914
* **Cart size**: 8kB - 32kB

## Presentation

The **Intellivision** is a video game console produced by Mattel and released in 1979. The development of the console began in 1978, less than a year after the introduction of its main competitor, the Atari 2600.

## Emulators

[Libretro FreeIntv](libretro-freeintv)