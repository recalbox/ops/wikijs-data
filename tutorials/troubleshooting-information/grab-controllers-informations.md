---
title: Récupérer les informations sur les manettes
description: 
published: true
date: 2021-09-04T18:11:13.026Z
tags: informations, manettes
editor: markdown
dateCreated: 2021-05-21T08:10:31.504Z
---

Il existe quelques commandes utiles pour obtenir des informations sur les appareils pouvant être utilisés pour aider à résoudre les problèmes. Elles se font toutes [via SSH](./../../tutorials/system/access/root-access-terminal-cli).

Lister les appareils disponibles :

```shell
cat /proc/bus/input/devices
```

Obtenir des informations sur les périphériques USB :

```shell
lsusb -v
```

Obtenir des informations de l'`udev` sur les appareils qui ont été mappés lors d'un événement :

```shell
for i in /dev/input/event*; do echo $i;udevadm info -q all -n $i;done
```

Obtenir la liste de tous les boutons/axes/touches trouvés (même s'il s'agit d'un clavier) :

```shell
for i in /dev/input/event*; do echo $i;(evtest $i) & ( evtestpid=$! && sleep 0.1 && kill -15 $evtestpid );done
```

Chaque fois que vous exécuterez une de ces commandes, il est préférable de ne pas coller la totalité de la sortie sur le forum ou Discord. Utilisez plutôt quelque chose comme [http://pastebin.com/](http://pastebin.com/), collez ce qui est affiché après avoir exécuté la commande puis donnez le lien aux personnes avec qui vous communiquez.