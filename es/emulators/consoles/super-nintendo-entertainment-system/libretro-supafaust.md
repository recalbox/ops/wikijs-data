---
title: Libretro-supafaust
description: 
published: true
date: 2024-07-29T18:23:18.224Z
tags: libretro, snes, super nintendo, 9.1+, supafaust
editor: markdown
dateCreated: 2023-09-08T09:22:24.031Z
---

**Libretro-supafaust** es un emulador SNES para las placas ARM multi-core Cortex A7, A9, A15, A53.

## ![](/emulators/license.svg) Licencia

Este núcleo está bajo la licencia **[GPLv2](https://github.com/libretro/supafaust/blob/master/COPYING)**.

## ![](/emulators/compatibility.png) Compatibilidad

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ❌ | ✅ | ✅ | ✅ | ✅ | ❌ | ✅ | ✅ |

## ![](/emulators/features.png) Características

| Característica | Soportado |
| :---: | :---: |

## ![](/emulators/bios.svg) BIOS

>**No se requiere BIOS.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Extensiones soportadas

Las roms deben tener la siguiente extensión:

* .fig
* .sfc
* .smc
* .swc
* .zip
* .7z

Este sistema soporta roms comprimidas en formato .zip/.7z. Sin embargo, se trata solo de un archivo.

Los archivos contenidos en los .zip/.7z deben corresponder a las extensiones mencionadas anteriormente.
Además, cada archivo .zip/.7z debe contener solo una rom comprimida.

### Ubicación

Coloca las roms de la siguiente manera:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 snes
┃ ┃ ┃ ┃ ┣ 🗒 **juego.zip**

>Se recomiendan encarecidamente las roms en formato **No-Intro**.
{.is-success}

>Para más información sobre las roms, visita [este tutorial](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

>Para poder conservar tus configuraciones personalizadas durante una actualización, te recomendamos utilizar nuestra funcionalidad [Sobrecargas de configuración](./../../../advanced-usage/configuration-override).
{.is-info}

### Acceder a las opciones

Puedes configurar diversas opciones de dos maneras diferentes.

* A través del menú RetroArch:

┣ 📁 Menu RetroArch
┃ ┣ 📁 Opciones del núcleo
┃ ┃ ┣ 🧩 Name_option

* A través del archivo `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Opciones del núcleo

| Opciones | Valores de las opciones | Variable (para sobrecarga) | Valores de las variables |
| --- | --- | --- | --- |
| Formato de píxel | `rgb565` ✅ / `xrgb8888` / `0rgb1555` | `supafaust_pixel_format` | `rgb565` / `xrgb8888` / `0rgb1555` |
| Corrección de relación de aspecto de píxel | `Activado` ✅ / `Desactivado` / `force_ntsc` / `force_pal` | `supafaust_correct_aspect` | `enabled` / `disabled` / `force_ntsc` / `force_pal` |
| Filtro horizontal de mezcla/doble | `phr256blend_auto512` ✅ / `phr256blend_512` / `512_blend` / `512` / `phr256blend` | `supafaust_h_filter` | `phr256blend_auto512` / `phr256blend_512` / `512_blend` / `512` / `phr256blend` |
| Desentrelazado | `bob_offset` ✅ / `weave` / `blend` | `supafaust_deinterlacer` |  `bob_offset` / `weave` / `blend` |
| Primera línea de escaneo mostrada en modo NTSC | `0` ✅ / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` | `supafaust_slstart` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` |
| Última línea de escaneo mostrada en modo NTSC | `223` ✅ / `222` / `221` / `220` / `219` / `218` / `217` / `216` / `215` / `214` / `213` / `212` / `211` / `210` / `209` / `208` / `207` | `supafaust_slend` | `223` / `222` / `221` / `220` / `219` / `218` / `217` / `216` / `215` / `214` / `213` / `212` / `211` / `210` / `209` / `208` / `207` |
| Primera línea de escaneo mostrada en modo PAL | `0` ✅ / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `21` / `22` / `23` / `24` | `supafaust_slstartp` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `21` / `22` / `23` / `24` |
| Última línea de escaneo mostrada en modo PAL | `238` ✅ / `237` / `236` / `235` / `234` / `233` / `232` / `231` / `230` / `229` / `228` / `227` / `226` / `225` / `224` / `223` / `222` / `221` / `220` / `219` / `218` / `217` / `216` / `215` / `214` | `supafaust_slendp` | `238` / `237` / `236` / `235` / `234` / `233` / `232` / `231` / `230` / `229` / `228` / `227` / `226` / `225` / `224` / `223` / `222` / `221` / `220` / `219` / `218` / `217` / `216` / `215` / `214` |
| Región/Tipo de SNES a emular | `auto` ✅ / `ntsc` / `pal` / `ntsc_lie_auto` / `pal_lie_auto` / `ntsc_lie_pal` / `pal_lie_ntsc` | `supafaust_region` | `auto` / `ntsc` / `pal` / `ntsc_lie_auto` / `pal_lie_auto` / `ntsc_lie_pal` / `pal_lie_ntsc` |
| Comenzar fotograma en SNES VBlank para reducir latencia | `Activado` ✅ / `Desactivado` | `supafaust_frame_begin_vblank` | `enabled` / `disabled` |
| Habilitar ejecución anticipada de 1 fotograma | `Desactivado` ✅ / `video` / `video+audio` | `supafaust_run_ahead` | `disabled` / `video` / `video+audio` |
| Habilitar multitap | `Desactivado` ✅ / `port1` / `port2` / `port1+port2` | `supafaust_multitap` | `disabled` / `port1` / `port2` / `port1+port2` |
| Tasa de reloj de CX4 % | `100` ✅ / `125` / `150` / `175` / `200` / `250` / `300` / `400` / `500` | `supafaust_cx4_clock_rate` | `100` / `125` / `150` / `175` / `200` / `250` / `300` / `400` / `500` |
| Tasa de reloj de SuperFX % | `95` / `100` ✅ / `125` / `150` / `175` / `200` / `250` / `300` / `400` / `500` | `supafaust_superfx_clock_rate` | `95` / `100` / `125` / `150` / `175` / `200` / `250` / `300` / `400` / `500` |
| Emular caché de instrucciones SuperFX | `Desactivado` ✅ / `Activado` | `supafaust_superfx_icache` | `disabled` / `enabled` |
| Tasa de audio interna (tasa de salida) | `Desactivado` ✅ / `44100` / `48000` / `96000` | `supafaust_audio_rate` | `disabled` / `44100` / `48000` / `96000` |
| Máscara de afinidad de hilo de emulación | `0x0` / `0x1` / `0x2` ✅ / `0x3` / `0x4` / `0x5` / `0x6` / `0x7` / `0x8` / `0x9` / `0xa` / `0xb` / `0xc` / `0xd` / `0xe` / `0xf` | `supafaust_thread_affinity_emu` | `0x0` / `0x1` / `0x2` / `0x3` / `0x4` / `0x5` / `0x6` / `0x7` / `0x8` / `0x9` / `0xa` / `0xb` / `0xc` / `0xd` / `0xe` / `0xf` |
| Máscara de afinidad de hilo de renderizado de PPU | `0x0` / `0x1` ✅ / `0x2` / `0x3` / `0x4` / `0x5` / `0x6` / `0x7` / `0x8` / `0x9` / `0xa` / `0xb` / `0xc` / `0xd` / `0xe` / `0xf` | `supafaust_thread_affinity_ppu` | `0x0` / `0x1` / `0x2` / `0x3` / `0x4` / `0x5` / `0x6` / `0x7` / `0x8` / `0x9` / `0xa` / `0xb` / `0xc` / `0xd` / `0xe` / `0xf` |
| Renderizador de PPU | `mt` ✅ / `st` | `supafaust_renderer` | `mt` / `st` |

## ![](/emulators/external-links.png) Enlaces externos

* **Código fuente utilizado** : [https://github.com/libretro/supafaust/](https://github.com/libretro/supafaust/)
