---
title: Known issues 
description: Here every issue got a solution 
published: true
date: 2023-08-11T20:17:54.934Z
tags: 
editor: markdown
dateCreated: 2023-08-11T20:10:46.345Z
---

In this page we share already known use cases that can be defective, here are answers and solutions provided by the development team.

## Web Manager: roms editor doesn't work as expected 

The web editor is deprecated thanks to the roms editor feature already available in Recalbox. 
The web feature is still available but doesn't not work as expected (images are not displayed anymore, roms data not available in editor, etc.). 
We strongly recommend to use the roms editor feature embedded in Recalbox to manage your games. 



