---
title: Apple 2GS
description: 
published: true
date: 2024-07-26T12:45:35.406Z
tags: apple, apple-2gs, ordenadores
editor: markdown
dateCreated: 2023-09-02T18:35:42.748Z
---

![](/emulators/computers/apple2gs.svg){.align-center}

## Especificaciones técnicas

* **Fabricante**: Apple
* **Año de lanzamiento**: 1986
* **Sistema operativo**: Apple ProDOS, Apple GS/OS, GNO/ME
* **Procesador**: Western Design Center 65C816 16 bits a 2,8 MHz
* **RAM**: 256 kB o 1 MB (hasta 8 MB)
* **ROM**: 128 KB hasta 1 MB
* **Chip de sonido**: Ensoniq ES5503 DOC 8-bit wavetable synthesis, 32-channel stereo sound chip
* **Pantalla**: paleta VGC 12-bpp (4096 colores), 320×200, 640×200

## Presentación

El **Apple IIGS** disponía de un procesador 65C816 a 2,8 MHz con registros de direccionamiento de 16 y 24 bits, un procesador de sonido Ensoniq, más memoria, mejores colores, más periféricos (con un controlador intercambiable entre los modelos de placa IIe y IIc), algunos de los cuales eran compatibles con los del Macintosh (teclado, ratón, adaptador de red LocalTalk) y GS/OS, un sistema operativo derivado de Mac OS. Applied Engineering desarrolló varias tarjetas de expansión para la gama Apple II, entre ellas la PC Transporter, que permitía añadir un PC XT (procesador NEC V30, 768k RAM, modo gráfico CGA) bajo el capó. Apple también comercializó un kit para transformar un Apple IIe en un Apple IIGS.

## Emuladores

[GSPlus](gsplus)