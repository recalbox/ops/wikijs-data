---
title: Intellivision
description: Mattel Intellivision
published: true
date: 2021-10-03T18:43:59.984Z
tags: consoles, mattel, intellivision
editor: markdown
dateCreated: 2021-05-21T07:59:38.172Z
---

![](/emulators/consoles/intellivision.svg){.align-center}

## Fiche technique

* **Fabricant :** Mattel Electronics
* **Année de sortie :** 1979
* **Quantités vendues :** 3+ millions
* **Meilleur jeu vendu :** Las Vegas Poker & Blackjack
* **Processeur :** GI CP1610 @ 894.886 kHz
* **RAM :** 1kB
* **Vidéo :** General Instrument AY-3-8900-1
* **Résolution :** 159x96, 16 couleurs
* **Son :** General Instrument AY-3-8914
* **Taille des cartouches :** 8kB - 32kB

## Présentation

L'**Intellivision** est une console de jeux vidéo produite par Mattel et sortie en 1979. Le développement de la console a commencé en 1978, moins d'une année après l'introduction de sa principale concurrente, l'Atari 2600.

## Émulateurs

[Libretro FreeIntv](libretro-freeintv)