---
title: Conectad vuestra Recalbox a un CRT utilizando la salida HDMI
description: 
published: true
date: 2023-09-12T12:01:06.840Z
tags: crt, hdmi, pantalla
editor: markdown
dateCreated: 2023-09-12T12:01:06.840Z
---

Para reproducir Recalbox en una pantalla CRT conectada vía HDMI, hay varias opciones, dependiendo de los conectores de la pantalla.

* Conexión a través de la toma HDMI mediante un adaptador HDMI / VGA ==> VGA / BNC
* Conexión a través de la toma HDMI mediante un adaptador HDMI / VGA ==> VGA / SCART

## Hardware

### Conectar Recalbox a un monitor CRT con entrada BNC RGB, vía HDMI

Muchas pantallas CRT profesionales tienen una entrada BNC RGB, como se muestra en esta imagen en la Línea 3 (Entrada).

* Por lo tanto, tenemos 3 cables para los 3 colores (Rojo, Verde, Azul), así como 2 cables para la sincronización (vertical y horizontal).

![](/tutorials/video/crt/sony-trinitron.jpg)

### Conexiones

* Necesitaréis un adaptador [como este](https://www.amazon.fr/dp/B00NBUTHJG/).

![Adaptateur HDMI VGA](/tutorials/video/crt/hdmi-to-vga-adapter.png)

* Una vez conectado al puerto HDMI de tu Recalbox, tendrás una toma de salida VGA y una toma de audio.

>Necesitarás una fuente de alimentación externa micro-usb, 1A debería ser suficiente.
{.is-danger}

* También necesitaréis un adaptador VGA ==> BNC como [este](https://www.amazon.fr/dp/B0033AF5Y0/).

![Adaptateur VGA BNC](/tutorials/video/crt/vga-to-bnc-adapter.png)

* Como no hemos podido encontrar un adaptador VGA/BNC con salida BNC hembra, hemos tenido que utilizar 5 conectores BNC macho-macho, que podéis encontrar en tiendas especializadas de electrónica.

![BNC male male](/tutorials/video/crt/bnc-male.png)

## Programas

### Configuración del boot de Recalbox

Las resoluciones aceptadas por este tipo de pantallas varían según el modelo, pero en general queremos :

* 480i para EmulationStation (mi pantalla no soporta 480p, y el texto es ilegible en 240p).
* 240p para juegos (la resolución de salida de casi todas las consolas de esta época).

Aquí tenéis un ejemplo de cómo debería ser vuestro fichero [recalbox-user-config.txt](./../../../tutorials/system/modification/configtxt-configuration) :

```ini
# Définition de la résolution du mode custom DMT 87 HDMI pour la résolution des jeux
hdmi_cvt=1920 240 60 1 1 0 0
# Ignore le EDID de votre TV (le CRT peut envoyer de mauvaises informations)
hdmi_ignore_edid=0xa5000080
# Force le mode d'encodage des pixels, 2 correspond à Full RGB
hdmi_pixel_encoding=2
# Désactive le safe mode au boot du raspberry pi
avoid_safe_mode=1
# Ne pas désactiver l'overscan
disable_overscan=0
# Evite les interférences de l'adaptateur en boostant le signal HDMI
config_hdmi_boost=4
# Force le son sur HDMI
hdmi_drive=2
# Selectionne le groupe HDMI CEA
hdmi_group=1
# Résolution CEA 6 = 480 interlaced
hdmi_mode=6

# Kernel utilisé
kernel=zImage
# Utiliser le mode HDMI même si aucun câble n'est connecté
hdmi_force_hotplug=1
# paramètres mémoire
gpu_mem_256=128
gpu_mem_512=256
gpu_mem_1024=512

# Paramètre lié à l'audio
dtparam=audio=on
# Permet de temporiser le boot
boot_delay=3
```

Algunas explicaciones:

* La Raspberry Pi arrancará en modo `hdmi_group=1` y `hdmi_mode=6`, que corresponde a 480i (entrelazado).
* Si arranca directamente en 240p, los textos de los menús y juegos serán ilegibles.
* La línea `hdmi_cvt=1920 240 60 1 1 0 0` especifica una resolución de 1920 x 240 x 60 Hz, que no se usará para el arranque, pero se guardará como el modo `DMT 87 HDMI`, que usaremos más tarde para lanzar juegos.

Aquí teneis una pequeña leyenda de explicación sobre las opciones de esta línea:

```ini
hdmi_cvt=<width> <height> <framerate> <aspect> <margins> <interlace> <rb>
width        width in pixels
height       height in pixels
framerate    framerate in Hz
aspect       aspect ratio 1=4:3, 2=14:9, 3=16:9, 4=5:4, 5=16:10, 6=15:9
margins      0=margins disabled, 1=margins enabled
interlace    0=progressive, 1=interlaced
rb           0=normal, 1=reduced blanking
```

### Configuración del fichero recalbox.conf

Aunque la Raspberry Pi arranca en modo de vídeo CEA 6 (a través del archivo recalbox-user-config.txt), es preferible forzar esta configuración para EmulationStation :

```ini
system.es.videomode=CEA 6 HDMI
```

A continuación, tenemos que especificar la resolución de arranque para nuestros emuladores, de forma global:

```ini
global.videomode=DMT 87 HDMI
```

Con esto, todos los emuladores arrancarán en el modo `DMT 87 HDMI` (modo "personalizado") que contiene, a través de `hdmi_cvt` en el fichero `recalbox-user-config.txt`, la resolución 1920 x 240x 60Hz.

>Pequeña explicación para el ancho de 1920 píxeles:
>
>Para hacer corto, una pantalla CRT sólo necesita un número vertical de píxeles (240 aquí) y una frecuencia de actualización vertical (60 Hz aquí).
>
>Por lo tanto, especificamos 1920 píxeles de ancho, porque 1920 es un múltiplo de 160, 320, 384... que son resoluciones que se encuentran a menudo.
>
> >Para las resoluciones que no son múltiplos, tendremos que utilizar RetroArch para calcular el overscan, es decir, las bandas negras necesarias alrededor de la pantalla para mantener la proporción original.
>
>Todo debe pasar por esta resolución. Pero puedes especificar una resolución diferente por sistema, con la siguiente línea:
{.is-info}

```ini
dreamcast.videomode=hdmi_cvt 320 240 60 1 1 0 0
```

### Configuración del viewport RetroArch (respetar el aspect-ratio de origen y centrar la imagen en la pantalla)

#### Configuración por sistema

Cuando se lanza un juego, la resolución inicial debe ser de 320 x 240 x 60 Hz píxeles. Para cada sistema, podemos especificar el tamaño real de la pantalla y el desplazamiento si es necesario.

Estos archivos de configuración deben crearse en la carpeta compartida: `/recalbox/share/system/configs/retroarch/`.
Deben contener el nombre del sistema (el mismo que en la carpeta roms), por ejemplo: `nes.cfg`, o `mastersystem.cfg`.

A continuación se muestra el contenido de un archivo `nes.cfg`:

```ini
aspect_ratio_index = "22"
video_smooth = "false"
video_scale_integer = "false"
video_threaded = "false"
custom_viewport_width = "1680"
custom_viewport_height = "224"
custom_viewport_x = "118"
custom_viewport_y = "17"
```

* Para ajustar la posición de la pantalla, cambiad los valores de `custom_viewport_x` y `custom_viewport_y` directamente en el juego a través del menú RetroArch (`Hotkey` + `B`), luego copiad estos valores en el archivo .cfg del sistema. 
* Aquí tenéis un ejemplo de las alturas (height) y anchuras (width) para CRT:

```ini
nes.cfg
custom_viewport_width = "1680"
custom_viewport_height = "224"
        
snes.cfg
custom_viewport_width = "1792"
custom_viewport_height = "224"

gb.cfg gbc.cfg gamegear.cfg
custom_viewport_width = "960"
custom_viewport_height = "144"

gba.cfg
custom_viewport_width = "1440"
custom_viewport_height = "160"

megadrive.cfg neogeo.cfg segacd.cfg sega32x.cfg fba_libretro.cfg
custom_viewport_width = "1920"
custom_viewport_height = "224"

mastersystem.cfg
custom_viewport_width = "1536"
custom_viewport_height = "192"

psx.cfg n64.cfg
custom_viewport_width = "1920"
custom_viewport_height = "240"

pcenginecd.cfg
custom_viewport_width = "1760"
custom_viewport_height = "240"
```

#### Configuración por juego

Para el arcade, el equipo Recalbox utiliza fba_libretro. Como casi cada juego tiene una resolución diferente, hemos definido un fichero por juego para mantener la proporción original.

Se aplica el mismo procedimiento que para los sistemas, excepto que los ficheros se encuentran en la carpeta compartida: `/recalbox/share/system/configs/retroarch/fba_libretro/`.

El contenido del fichero es idéntico al de la configuración del sistema. El nombre del fichero debe tener la forma `rom.zip.cfg`. Por ejemplo: `1944.zip.cfg`.

### Casos particulares

#### AdvanceMame

Para los juegos arcade de Mame, el core de AdvanceMame calcula automáticamente la resolución de cada juego, adaptándola a la resolución de tu pantalla.

Esto significa que no necesitamos configurar cada juego (contrariamiente al caso de fba_libretro).

### Themes EmulationStation

Los themes deben estar en 4/3 para garantizar una imagen bien encuadrada. Os aconsejamos [un theme](https://forum.recalbox.com/topic/6580/release-wip-theme-recalbox-multi-help-needed) creado por Supernature2K.

Este theme es configurable y se adapta perfectamente a los CRT.

![Thème Recalbox Multi](/tutorials/video/crt/crt-theme.png)