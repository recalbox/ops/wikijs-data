---
title: Solarus
description: 
published: true
date: 2024-06-02T17:18:56.820Z
tags: solarus
editor: markdown
dateCreated: 2021-10-23T14:44:29.414Z
---

![](/emulators/fantasy/solarusarpg.svg){.align-center}

**Solarus*** é um núcleo autônomo(standalone) que permite jogar jogos 2D Action-RPG caseiros/amadores (homebrews).

## ![](/emulators/license.svg) Licença

Este núcleo é licenciado sob [**GPLv3**](https://www.solarus-games.org/fr/about/legal).

## ![](/emulators/compatibility.png) Compatibilidade

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Recursos

| Recurso | Suportado |
| :---: | :---: |
| Reiniciar | ✔ |
| Capturas de tela | ✔ |
| Saves | ✔ |
| Opções do núcleo | ✔ |
| Controles | ✔ |
| Remapeamento | ✔ |
| Vibração | ✔ |
| Controle de disco | ✔ |

## ![](/emulators/bios.svg) BIOS

>**Nenhuma BIOS é necessária**.
{.is-success}

## ![](/emulators/roms.png) Roms

### Extensões suportadas

As ROMs devem ter as seguintes extensões:

* .solarus

### Local

Coloque as roms assim:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 solarus
┃ ┃ ┃ ┃ ┣ 🗒 **jogo.solarus**

#### Lista de jogos

Ele possui um catálogo de jogos compatível com o motor:  
[https://www.solarus-games.org/en/games](https://www.solarus-games.org/en/games)

![](/emulators/fantasy/solarusgamelist.png){.align-center}

## ![](/emulators/advanced-configuration.png) Configuração avançada do emulador

>**Este núcleo não tem opção.**
{.is-success}

## ![external-links.png](/emulators/external-links.png) Links externos

* **Código-fonte utilizado** : [https://gitlab.com/bkg2k/solarus](https://gitlab.com/bkg2k/solarus)
* **Site oficial** : [https://solarus-games.org/en/](https://solarus-games.org/en)
* **Jogos caseiros/amadores (homebrews)** : [https://www.solarus-games.org/en/games/](https://www.solarus-games.org/en/games)