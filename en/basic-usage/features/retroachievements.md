---
title: Retroachievements
description: 
published: true
date: 2021-08-08T19:02:16.716Z
tags: retroachievements
editor: markdown
dateCreated: 2021-06-29T09:59:06.891Z
---

## Challenges / Achievements with RetroArch

The website [retroachievements.org](http://www.retroachievements.org/) offers challenges / achievements / trophies on platforms such as NES, SNES, GB, GBC, GBA, Megadrive/Genis, PCengine etc...

## Create an account

* You must [create an account](http://retroachievements.org/createaccount.php) on the [retroachievements.org](http://www.retroachievements.org/) website. 
* Note your username and password in a corner as they will be used to set up Retroachievements.

## Account activation in Recalbox

This is done in the EmulationStation menu.

* Open the EmulationStation menu with the `START` button. 
* Go to `GAME SETTINGS` > `RETROACHIEVEMENTS SETTINGS`.
* Edit it like this:

  * `RETROACHIEVEMENTS`: ON
  * `USER NAME`: put your nickname
  * `PASSWORD`: your password

You also have the option of hardcore mode, you can decide if you want to activate it or not.

>The hardcore mode disables the rewind, unlimited credits, etc...
{.is-info}

## Emulators compatible with Retroachievements.

Not all Libretro cores are compatible with Retroachievements. If you want to know for a particular core if it is supported, you have to go to the emulator page in the part [emulators](./../../emulators).

## View the challenges of a game in progress

To view the challenges / trophies in RetroArch, you just have to activate the RetroArch menu (`Hotkey`+`B`) > `Success`.

## Which roms are compatible?

The list of compatible games is available on [this page](http://retroachievements.org/gameList.php)

In general, for consoles the US version of the No-Intro roms work best for Retroachievements, with a few rare exceptions where you can also get some European roms and for the arcade you can get the right version of the set.