---
title: Amiberry
description: 
published: true
date: 2024-06-30T15:19:36.384Z
tags: amiga-cd32, amiga, amiberry, cd32
editor: markdown
dateCreated: 2023-09-06T07:39:08.254Z
---

**Amiberry** es un core ARM optimizado para Amiga.

## ![](/emulators/license.svg) Licencia

El core posee la siguiente licencia [**GPLv3**](https://github.com/midwan/amiberry/blob/master/COPYING).

## ![](/emulators/compatibility.png) Compatibilidad

 | RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
 | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
 | ❌ | ✅ | ✅ | ✅ | ✅ | ✅ &nbsp;🐌 | ❌ | ✅ |

🐌  Bajo rendimiento pero jugable

## ![](/emulators/features.png) Funcionalidades



## ![](/emulators/bios.svg) BIOS

### Lista de bios obligatorias

Encontraréis las bios con el nombre indicado en la columna **Descripción**. Tendréis que renomarlas con el nombre indicado en la columna **Nombre del fichero**.

| Nombre del fichero | Descripción | MD5 | ¿Preinstalada? |
| :---: | :---: | :---: | :---: |
| kick40060.CD32 | Kickstart v3.1 r40.060 (1993-05)(Commodore)(CD32)\[!\].rom | f2f241bf094168cfb9e7805dc2856433 5f8924d013dd57a89cf349f4cdedc6b1 | ❌ |
| kick40060.CD32.ext | CD32 Extended-ROM r40.60 (1993)(Commodore)(CD32).rom | bb72565701b1b6faece07d68ea5da639 | ❌ |

### Emplazamiento

Disponed vuestras bios como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 🗒 **kick40060.CD32**
┃ ┃ ┃ ┣ 🗒 **kick40060.CD32.ext**

## ![](/emulators/isos.svg) Isos

### Extensiones soportadas

Las isos deben tener las siguientes extensiones:

* .bin/.cue
* .iso
* .mds/.mdf
* .img/.ccd

### Emplazamiento

Disponed vuestras isos como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 amigacd32
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.cue**
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.bin**

>Se aconseja las isos del projecto **Redump** 
{.is-success}

>Para más información sobre las isos, consultad [este tutorial](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

>Para conservar las configuraciones personalizadas cuando se produce una actualización del sistema Recalbox, os aconsejamos utilizar las [Sobrecargas de configuración](./../../../advanced-usage/configuration-override).
{.is-info}

## ![](/emulators/external-links.png) Enlaces externos

* **Código fuente utilizado** : [https://github.com/midwan/amiberry/](https://github.com/midwan/amiberry/)
* **Documentación** : [https://blitterstudio.com/amiberry/](https://blitterstudio.com/amiberry/)