---
title: Overlays
description: On wide screens, display system images that wrap around emulated screen.
published: true
date: 2021-11-09T20:58:03.502Z
tags: overlays, bezel, custom, 8.0+
editor: markdown
dateCreated: 2021-06-24T15:23:40.152Z
---

## Presentation

The **overlays** (also called *bezels*) are illustrations which come to "decorate" your screen during the game. These illustrations are often placed on the black areas of your 16/9 screen, not used by the game which is displayed in 4/3.

**Street of Rage 3** with the Megadrive overlay, on a 16:9 screen: 
![overlays.png](/basic-usage/features/overlays/overlays.png)


**Street of Rage 3** without overlay, on a 16/9 screen:
![43.png](/basic-usage/features/overlays/43.png)

**Street of Rage 3** without overlay, on a 16:9 screen, with the image stretched:
![169.png](/basic-usage/features/overlays/169.png)



## Recalbox Overlays

Recalbox integrates the **Recalbox Overlays** which are displayed by default on compatible screens.

You can enable or disable **Recalbox Overlays** in the **Game Options** menu of EmulationStation:

![overlays-set-option-es.png](/basic-usage/features/overlays/overlays-set-option-es.png)

### Activation by system

In `recalbox.conf` it is possible to enable or disable overlays in general:

```ini
global.recalboxoverlays=1
```

Or for a system :
```ini
snes.recalboxoverlays=0
```

## Add your custom overlays

There are different overlay packs, some of which offer one overlay per game!

When you have downloaded your overlay pack, copy all the directories it contains into the `overlays` directory of your `SHARE` partition:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 overlays
┃ ┃ ┃ ┣ 📁 megadrive
┃ ┃ ┃ ┃ ┣ 🗒 megadrive.cfg
┃ ┃ ┃ ┃ ┣ 🗒 megadrive_overlay.cfg
┃ ┃ ┃ ┃ ┣ 🗒 megadrive.png
┃ ┃ ┃ ┣ 📁 snes
┃ ┃ ┃ ┃ ┣ 🗒 .....

> Custom overlays have priority over automatic overlays. 
> {.is-info}