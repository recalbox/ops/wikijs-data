---
title: Configuração de exibição
description: 
published: true
date: 2021-12-23T20:22:11.789Z
tags: video, configuração, tela, vídeo, exibição
editor: markdown
dateCreated: 2021-12-23T20:22:11.789Z
---

Aqui você encontrará tutoriais sobre como configurar sua tela / TV / monitor.

Aqui estão os tutoriais disponíveis:

[Guia completo de configuração de vídeo](complete-video-configuration-guide)
[Configurar uma tela externa](external-screen-configuration)
[Configurando o tamanho da imagem usando overscan em TFT](image-size-settings-overscan-tft)