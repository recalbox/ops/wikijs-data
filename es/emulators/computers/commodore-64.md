---
title: Commodore 64
description: 
published: true
date: 2024-07-26T12:47:18.455Z
tags: commodore, commodore-64, c64, ordenadores
editor: markdown
dateCreated: 2023-09-02T18:43:30.620Z
---

![](/emulators/computers/commodore64.svg){.align-center}

## Datos técnicos

* **Fabricante**: Commodore
* **Año de lanzamiento**: 1982
* **Sistema operativo**: Commodore KERNAL / Commodore BASIC 2.0 GEOS
* **Procesador**: MOS Technology 6510/8500 @ 1.023 MHz (versión NTSC)
* **RAM**: 64 KB DE RAM
* **ROM**: 20 KB
* **Procesador gráfico**: MOS Technology VIC-II
* **Chip de sonido**: MOS Technology 6581 SID
* **Resolución**: 320×200 (16 colores)

## Presentación

El **Commodore 64** es un ordenador personal diseñado por Commodore Business Machines Inc. en 1982, bajo la égida de Jack Tramiel. Fue la primera máquina en vender varios millones de copias (entre 17 y 25 millones según las estimaciones), y sigue siendo el ordenador personal más vendido hasta la fecha, según el Libro Guinness de los Récords.

El Commodore 64 utiliza un microprocesador 6510 de 8 bits (un derivado cercano del 6502, que puede gestionar bancos de memorias introduciéndolos bajo demanda en el espacio de direcciones del procesador) y tiene 64 kilobytes de RAM. En el Reino Unido rivalizó en popularidad con el ZX Spectrum y se benefició de un teclado de tamaño completo y de chips gráficos y de sonido más avanzados.

El chip gráfico, VIC-II, proporciona 16 colores, ocho sprites, capacidad de desplazamiento y dos modos gráficos de mapa de bits. El modo de texto estándar proporciona 40 columnas, como la mayoría de los modelos PET de Commodore.

El chip de sonido, SID, tiene tres voces, varias formas de onda, modulaciones de sonido y capacidades de filtrado. Era muy avanzado para su época. Su diseñador, Bob Yannes, sería cofundador de la empresa de sintetizadores Ensoniq.

El BASIC incorporado no ofrecía una forma sencilla de acceder a las avanzadas capacidades gráficas y de sonido de la máquina, por lo que los usuarios tenían que utilizar los comandos PEEK y POKE para dirigirse directamente a la memoria y obtener el resultado deseado, o utilizar extensiones como el BASIC de Simon, o programar directamente en ensamblador. Las limitaciones extremas de BASIC, la necesidad de investigar la información de la máquina cerca del sistema (chip de audio, chip de vídeo) para mostrar gráficos, reproducir música y crear sonidos, lo engorroso de utilizar las instrucciones PEEK y POKE en BASIC en programas grandes, Sin duda, todo esto llevó a los programadores de la época a pasar muy rápidamente del BASIC al lenguaje ensamblador, que era mucho más rápido y ofrecía más posibilidades, lo que puede explicar en parte el gran entusiasmo por esta máquina y la calidad superior de los juegos y demos en ella, en comparación con otros microordenadores de la misma época. Dicho esto, Commodore tenía una mejor implementación de BASIC, pero finalmente optó por vender el C64 con el mismo BASIC 2.0 utilizado en el VIC-20 por miedo a que el C64 hiciera caer en picado las ventas del PET/CBM.

El C64 heredó de las máquinas CBM y del VIC-20 un puerto de usuario programable (6522) y un puerto serie propietario que funcionaba según un principio cercano al IEEE-488 y permitía conectar (y direccionar) periféricos, en particular una o más unidades de disquete de 5,25 pulgadas 1540 (disquetera del VIC-20), 1541 y 1542.

También existía un modelo portátil con disquetera y pantalla integradas, pero sin el puerto para la unidad de casete.

Con este ordenador (probablemente) surgió una cultura informática underground conocida como la escena demo.

## Emuladores

[Libretro vice_x64](libretro-vice_x64)
[Libretro vice_x64sc](libretro-vice_x64sc)
[Libretro vice_xplus4](libretro-vice_xplus4)
[Libretro vice_xpet](libretro-vice_xpet)
[Libretro vice_xvic](libretro-vice_xvic)
[Libretro vice_x128](libretro-vice_x128)
[Libretro vice_xcbm2](libretro-vice_xcbm2)
[Libretro vice_xscpu64](libretro-vice_xscpu64)
[Vice_x64](vice_x64)
[Vice_x64sc](vice_x64sc)
[Vice_xplus4](vice_xplus4)
[Vice_xpet](vice_xpet)
[Vice_xvic](vice_xvic)
[Vice_x128](vice_x128)
[Vice_xcbm2](vice_xcbm2)
[Vice_xscpu64](vice_xscpu64)