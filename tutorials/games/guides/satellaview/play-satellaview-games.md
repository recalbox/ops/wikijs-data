---
title: Play Satellaview games
description: 
published: true
date: 2024-10-30T18:32:43.206Z
tags: nintendo, satellaview
editor: markdown
dateCreated: 2024-10-30T18:32:43.206Z
---

## Introduction

Most of the time, you'll want to play Satellaview games with the first valid bios. Except that this bios, the vast majority of the time, will be in Japanese. Here's how to play Satellaview games if you don't feel like looking for an English bios.

## Procedure

- Launch your game. You will arrive in front of the first screen (a television that talks to you).

![](/tutorials/games/guides/satellaview/satellaview1.png){.full-width}

- Confirm and fill in the nickname you want to use. This will be used in the system and in the games.

![](/tutorials/games/guides/satellaview/satellaview2.png){.full-width}

- You can click on the bottom left word `PAGE` to change the alphabet. When you're done, press `DONE` at the bottom right (pressing the `START` button will put you on `DONE`). You will be asked whether you prefer a boy or a girl and then press `DONE`.

![](/tutorials/games/guides/satellaview/satellaview3.png){.full-width}

- Once all this has been done, the system will try to connect to the original system but will not succeed, as it no longer exists. You'll find yourself in a town that was bustling when the service was active. There's nothing you can do but enter the building you're standing in front of (`UP` then `A`).

![](/tutorials/games/guides/satellaview/satellaview4.png){.full-width}

- You will land on a menu with 4 possible choices:

  - `Load Stored Data`: play the game you started at the beginning
  - `Delete Stored Data`: delete the contents of the game
  - `Receive Program`: receive an update (can no longer function)
  - `Clear Personal Data`: delete the profile you have created so far

![](/tutorials/games/guides/satellaview/satellaview5.png){.full-width}

- Select `Load Stored Data`, select the game and off you go!

![](/tutorials/games/guides/satellaview/satellaview6.png){.full-width}