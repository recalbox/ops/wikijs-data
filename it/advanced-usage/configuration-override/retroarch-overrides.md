---
title: Sovraccarichi RetroArch
description: Esempi di chiavi per sovrascrivere la configurazione di RetroArch.
published: true
date: 2024-06-22T06:11:09.273Z
tags: 
editor: markdown
dateCreated: 2024-06-22T05:52:57.289Z
---

Ecco un elenco non esaustivo delle chiavi che possono essere modificate in un file di override di tipo `.retroarch.cfg`. Queste modifiche vengono applicate solo se l'emulatore è un core RetroArch.

> Un \* dopo il valore indica che il parametro può essere sovrascritto anche tramite un .recalbox.conf
{.is-info}

## Audio

* `audio_enable = "true"` : attivare o disattivare l'audio.
* `audio_volume = "0.000000"` : regolare il volume, 0 = volume predefinito.

## Menu RetroArch

* `quick_menu_show_save_content_dir_overrides = "false"` : mostrare o nascondere l'opzione Sostituzione configurazione per la directory.
* `quick_menu_show_save_core_overrides = "false"` : mostrare o nascondere l'opzione Sostituzione configurazione per il core.
* `quick_menu_show_save_game_overrides = "false"` : mostrare o nascondere l'opzione Sostituzione configurazione per il gioco.

> Le Sostituzioni di configurazione sono una funzionalità di RetroArch, simile agli override, ma che conservano molte informazioni. Nel contesto di Recalbox, è meglio preferire gli override se possibile.
{.is-info}

## Debug

* `fps_show = "true"` : mostrare gli FPS in gioco.
* `menu_driver = "ozone"` : scegliere il menu retroarch, di default ozone, tranne su GPi Case, dove viene utilizzato rgui.
* `menu_enable_widgets = "true"` : attivare i popup in gioco, se impostato su false, mostrerà le notifiche come testo giallo.

## Cartelle

* `recording_output_directory = ""` : cartella dove vanno le registrazioni video.
* `savefile_directory = ""` : dove salvare/caricare i salvataggi.
* `savestate_directory = ""` : dove salvare/caricare i salvataggi istantanei.
* `screenshot_directory = ""` : cartella dove vanno gli screenshot.

## Servizio di traduzione

* `ai_service_enable = "true"` : \* attivare o disattivare il servizio di traduzione.
* `ai_service_mode = "0"` : modalità del servizio di traduzione, 0: immagine, 1: voce.
* `ai_service_source_lang = "0"` : \* lingua da leggere per il servizio di traduzione, 0 = non specificato.
* `ai_service_target_lang = "1"` : \* lingua in cui tradurre, 1 = Inglese, 3 = Francese.
* `ai_service_url =` : \* link internet del servizio da utilizzare.

## Overlays

* `aspect_ratio_index = "23"` : \* indice del rapporto, 23 = Custom.
* `input_overlay = ""` : link al file di configurazione dell'overlay.
* `input_overlay_enable = "true"` : attivazione dell'overlay.
* `input_overlay_hide_in_menu = "true"` : overlay nascosto nel menu RetroArch.

### Coordinate da definire con l'overlay

* `custom_viewport_height = ""`
* `custom_viewport_width = ""`
* `custom_viewport_x = ""`
* `custom_viewport_y = ""`
* `video_message_pos_x = "0.050000"`
* `video_message_pos_y = "0.050000"`

## Netplay

* `netplay_nickname = ""` : \* nickname Netplay.

## Rotazione dello schermo

* `video_rotation = "1"` : ruota il rendering video, 0 = normale, 1 = 90°, 2 = 180°, 3 = 270°, attenzione al rapporto d'aspetto.

## Joystick e Pad direzionale

Per dissociare/associare il Pad direzionale a uno dei joystick:

* `input_player1_analog_dpad_mode = "0"` : dissocia.
* `input_player1_analog_dpad_mode = "1"` : associa al joystick sinistro.
* `input_player1_analog_dpad_mode = "2"` : associa al joystick destro.
* `input_player1_analog_dpad_mode = "3"` : associa forzatamente al joystick sinistro.
* `input_player1_analog_dpad_mode = "4"` : associa forzatamente al joystick destro.

## Rimappatura delle hotkeys

> Le impostazioni per cambiare le hotkeys dipendono dal mapping del controller in Recalbox. Se il controller cambia, la configurazione sovrascritta tramite queste righe potrebbe non funzionare più.
{.is-warning}

Per ottenere il valore numerico per ogni tasto del tuo controller, guarda nel file `/recalbox/share/system/configs/retroarch/retroarchcustom.cfg` il valore della chiave desiderata secondo questa tabella:

| Nome del tasto | Chiave corrispondente da cui prendere il valore |
| :--- | :--- |
| A | input_player1_a_btn |
| B | input_player1_b_btn |
| X | input_player1_x_btn |
| Y | input_player1_y_btn |
| Su | input_player1_up_btn |
| Giù | input_player1_down_btn |
| Sinistra | input_player1_left_btn |
| Destra | input_player1_right_btn |
| Select | input_player1_select_btn |
| Start | input_player1_start_btn |
| L | input_player1_l_btn |
| L2 | input_player1_l2_btn |
| L3 | input_player1_l3_btn |
| R | input_player1_r_btn |
| R2 | input_player1_r2_btn |
| R3 | input_player1_r3_btn |
| Stick Sinistro Su | input_player1_l_y_minus_axis |
| Stick Sinistro Giù | input_player1_l_y_plus_axis |
| Stick Sinistro Sinistra | input_player1_l_x_minus_axis |
| Stick Sinistro Destra | input_player1_l_x_plus_axis |
| Stick Destro Su | input_player1_r_y_minus_axis |
| Stick Destro Giù | input_player1_r_y_plus_axis |
| Stick Destro Sinistra | input_player1_r_x_minus_axis |
| Stick Destro Destra | input_player1_r_x_plus_axis |

> Le modifiche ai valori che seguono devono essere fatte nel file di override.
>
> Il file precedente serve solo a osservare il mapping attuale.
{.is-warning}

* `input_enable_hotkey_btn =` : Tasto Hotkey.
* `input_screenshot_btn =` : Tasto per fare uno screenshot.
* `input_exit_emulator_btn =` : Tasto per uscire dal gioco.
* `input_load_state_btn =` : Tasto per caricare un salvataggio istantaneo.
* `input_save_state_btn =` : Tasto per fare un salvataggio istantaneo.
* `input_menu_toggle_btn =` : Tasto per accedere al menu RetroArch.
* `input_reset_btn =` : Tasto per riavviare il gioco.
* `input_ai_service_btn =` : Tasto per tradurre lo schermo attuale.

Per esempio, `The legend of Zelda: Link's Awakening` su Game Boy richiede l'uso di `Start+Select+A+B` per salvare. Se il tuo controller non ha un tasto home, e la hotkey è su `Select`, il salvataggio naturale del gioco sarà impossibile. Impostare il tasto `input_enable_hotkey_btn` sul tasto R del tuo controller solo per questo gioco è possibile. Il valore della chiave `input_player1_r_btn` è 4 per il tasto R, quindi dovrai inserire `input_enable_hotkey_btn = 4` nell'override del gioco.
