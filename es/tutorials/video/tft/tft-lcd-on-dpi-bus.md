---
title: Configurar vuestra pantallita TFT a través del bus DPI
description: 
published: true
date: 2023-09-14T14:46:30.839Z
tags: tft, dpi, bus, mini, pantalla, pequeña
editor: markdown
dateCreated: 2023-09-14T14:46:30.839Z
---

## Configure su pequeña pantalla TFT a DPI

### DPI (INTERFAZ DE PANTALLA PARALELA)

Fuente : [https://www.raspberrypi.org/documentation/hardware/raspberrypi/dpi/README.md](https://www.raspberrypi.org/documentation/hardware/raspberrypi/dpi/README.md)Una interfaz paralela RGB de hasta 24 bits está disponible en todas las placas Raspberry Pi con el cabezal de 40 canales (A+, B+, Pi2, Pi3, Zero) y el módulo de cálculo. Esta interfaz permite conectar pantallas RGB paralelas al GPIO de Raspberry Pi, ya sea en RGB24 (8 bits para rojo, verde y azul) o RGB666 (6 bits por color) o RGB565 (5 bits rojo, 6 verde y 5 azul).

Esta interfaz está controlada por el firmware de la GPU y puede ser programada por un usuario a través de parámetros especiales config.txt y habilitando la superposición correcta del árbol de dispositivos Linux.

Al enviar datos a través de esta interfaz paralela, no tienes problemas con el ancho de banda del bus y la velocidad de visualización (FPS). Pero utiliza casi todos los pines GPIO.

Este modo es muy útil, ya que puedes ajustar con precisión los parámetros de visualización: tiempos, resoluciones, etc.

Este modo va acompañado de nuevas superposiciones, que permiten producir una señal RGB utilizando el VGA666 (consigue el adaptador VGA pasivo 666 para Raspberry-Pi B+: [code](https://github.com/fenlogic/vga666) y [hardware](https://www.banggood.com/VGA-666-Adapter-Board-For-Raspberry-Pi-3-Model-B-2B-B-A-p-1071309.html)).

```ini
dtoverlay=vga666
```

Podéis utilizar la pantalla TFT [5"](https://www.adafruit.com/product/1596) o [7"](https://www.adafruit.com/product/2354) de Adafruit gracias al [panel TFT Kippah DPI de Adafruit para Raspberry Pi con soporte táctil](https://www.adafruit.com/product/2453).

```ini
dtoverlay=dpi24
```

#### Instalación

* Abrid el fichero [recalbox-user-config.txt](./../../../tutorials/system/modification/configtxt-configuration) con nano o vim y añadir estas líneas:

```ini
dtoverlay=dpi24
```

### Pantalla TFT Geekwrom HD 3,5 pulgadas, 800x480

Esta pantalla es de pequeño tamaño y ofrecie una resolución de 800x480. Sus especificaciones técnicas son bastante buenas.

![](/tutorials/video/tft/geekwrom3-5.png)

Esta pantalla se puede encontrar en [esta página china](https://www.banggood.com/Geekwrom-HD-3_5-Inch-TFT-Display-Shield-800x480-For-Raspberry-Pi-3B-2B-With-2-Keys-And-Remote-IR-p-1069730.html) por un precio inferior a 50€.

Se puede utilizar tal cual, sin el programa fbcp. Fbcp no es neceario ya que el modo DPI utiliza directamente la GPU.

Para soportar esta pantalla en vuestra REcalbox, basta con añadir las siguientes líneas a vuestro fichero [recalbox-user-config.txt](./../../../tutorials/system/modification/configtxt-configuration)

#### Instalación

* Abrid el fichero [recalbox-user-config.txt](./../../../tutorials/system/modification/configtxt-configuration) con nano o vim añadid estas líneas:

```ini
# 3.5 HD tft screen 800x480
dtoverlay=dpi24
overscan_left=0
overscan_right=0
overscan_top=0
overscan_bottom=0

# Banggood
framebuffer_width=800
framebuffer_height=480
dtparam=spi=off
dtparam=i2c_arm=off

enable_dpi_lcd=1
display_default_lcd=1
dpi_output_format=0x6f015
dpi_group=2
dpi_mode=87
hdmi_timings=480 0 16 16 24 800 0 4 2 2 0 0 0 60 0 32000000 6
display_rotate=3
```

* Guarda el archivo y reiniciad Recalbox. Ahora podéis ver las increíbles imágenes que aparecen en esta pantalla.

![Ecran TFT Geekwrom HD 3,5 pouces 800x480](/tutorials/video/tft/geekwrom-result.jpg)

### Pantalla TFT 5" y 7" de Adafruit

Para utilizar estas pantallas de [5"](https://www.adafruit.com/product/1596) y [7"](https://www.adafruit.com/product/2353) pulgadas, necesitaréis el [panel TFT Kippah DPI de Adafruit para Raspberry Pi con soporte táctil](https://www.adafruit.com/product/2453).

#### Instalación

* Abrid el fichero [recalbox-user-config.txt](./../../../tutorials/system/modification/configtxt-configuration) con nano o vim y añadid estas líneas:

```ini
#3.5 HD tft screen 800x480
dtoverlay=dpi24
overscan_left=0
overscan_right=0
overscan_top=0
overscan_bottom=0

#Adafruit
framebuffer_width=800
framebuffer_height=480
dtparam=spi=off
dtparam=i2c_arm=off

enable_dpi_lcd=1
display_default_lcd=1
dpi_group=2
dpi_mode=87
dpi_output_format=0x6f005
hdmi_timings=800 0 40 48 88 480 0 13 3 32 0 0 0 60 0 32000000 6
display_rotate=3
```

### Pantalla 4.0" HyperPixel

Gracias a glook por este tutorial : [https://forum.recalbox.com/topic/17854/ecran-hyperpixel-4-pouces/4](https://forum.recalbox.com/topic/17854/ecran-hyperpixel-4-pouces/4)

La pantalla se puede comprar [aquí](https://shop.pimoroni.com/products/hyperpixel-4?variant=12569539706963) con o sin pantalla táctil.

Leed el GitHub [aquí](https://github.com/pimoroni/hyperpixel4) (en inglés). Necesitaréis seguir las siguientes etapas:

#### Instalación

Dependiendo de vuestro hardware, el código fuente a utilizar será diferente. Consultad la sección [instalación manual] (https://github.com/pimoroni/hyperpixel4#manual-installation) para saber qué código fuente hay que utilizar.

Una vez que hayáis elegido el código fuente correcto, seguid estos pasos:

* Descargad el código fuente haciendo clic en el botón verde grande `Code` y eligiendo descargar en formato `.zip`.
* Descomprimid el fichero y colocad el directorio extraído en `/recalbox/share/system/`.
* Conectaros vía [SSH](./../../../tutorials/system/access/root-access-terminal-cli) y situaros en el directorio anterior.
* Montad la [partición de arranque](./../../../tutorials/system/access/remount-partition-with-write-access) en modo lectura/escritura.
* Compilad el fichero hyperpixel4.dts en hyperpixel4.dtbo :

```shell
dtc -@ -I dts –O dtb –o dist/hyperpixel4.dtbo src/hyperpixel4.dts
```

* Copiad `hyperpixel4.dtbo` dentro del directorio `/boot/overlays/`.

```shell
cd dist
cp hyperpixel4.dtbo /boot/overlays/
```

* Modificad el fichero [recalbox-user-config.txt](./../../../tutorials/system/modification/configtxt-configuration) con nano o vim y añadid estas líneas:

```ini
# Paramètres de l'écran LCD HyperPixel
dtoverlay=hyperpixel4
overscan_left=0
overscan_right=0
overscan_top=0
overscan_bottom=0
framebuffer_width=800
framebuffer_height=480
enable_dpi_lcd=1
display_default_lcd=1
dpi_group=2
dpi_mode=87
dpi_output_format=0x7f216
display_rotate=3
hdmi_timings=480 0 10 16 59 800 0 15 113 15 0 0 0 60 0 32000000 6
```

* Comprobad que el i2c y el spi están desactivados:

```ini
dtparam=i2c_arm=off
dtparam=spi=off
```

* Copiad el fichero `hyperpixel4-init` situado dentro de la carpeta `/dist` hacia `/usr/bin/` en Recalbox.

```shell
cp -R /dist/hyperpixel4-init /usr/bin/
```

* Aseguráos de ejecutar este programa al inicio de Recalbox, añadiendo `hyperpixel4-init` a la línea de comandos en uno de los scripts init del archivo `/etc/init.d`.