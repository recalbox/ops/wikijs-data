---
title: Libretro MAME
description: 
published: true
date: 2024-07-23T23:20:00.894Z
tags: libretro, mame
editor: markdown
dateCreated: 2023-09-05T11:51:50.337Z
---



## ![](/emulators/license.svg) Licencia

El core posee la licencia **MAME no-commercial**.

## ![](/emulators/compatibility.png) Compatibilidad

 | RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
 | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
 | ❌ | ❌ | ✅ | ✅ | ❌ | ❌ | ✅ | ❌ |

## ![](/emulators/features.png) Funcionalidades



## ![](/emulators/bios.svg) BIOS

Las romsets de BIOS no son necesarias si utilizáis las romsets de arcade "Full Non-Merged". Para las romset de tipo "Split" y "Non-Merged", disponed las BIOS necesarias en la misma carpeta que la romset del juego.

>**Aviso** :
>Poned las bios dentro de la carpeta: `/recalbox/share/roms/mame/`   
>o dentro de una subcarpeta si utilizáis un directorio diferente por core Mame, por ejemplo: `/recalbox/share/roms/mame/Mame/`
>Los juegos y sus bios deben estar juntos en la misma carpeta o subdirectorio.
{.is-warning}

## ![](/emulators/roms.png) Roms

Libretro-MAME se basa en las versiones más recientes del emulador MAME y posee muchísimos juegos.
[Pulsad aquí](https://www.mamedev.org/) para saber más sobre el desarrollo de Mame.

* Basado en el romset : MAME 0.258 (30 Août 2023) Non-merged
* Fichero dat : el fichero dat se encuentra en vuestra Recalbox dentro del directorio `/recalbox/share/arcade/libretro/mame.dat`.

### Extensiones soportadas

Las roms deben tener las siguientes extensiones:

* .zip
* .chd

Los ficheros CHD sólo son necesarios para algunos juegos. Si no existe ningún fichero CHD para una rom, la rom funcionará. Si un juego necesita un fichero CHD y no lo tenéis, no funcionará.

### Emplazamiento

Disponed vuestras roms como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 **mame**
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.zip**
┃ ┃ ┃ ┃ ┣ 📁 **jeu**
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **jeu.chd**

También podéis crear un subdirectorio por versión del core de Mame (si queréis clasificar vuestros fullsets por el core que las hace funcionar).

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 mame
┃ ┃ ┃ ┃ ┣ 📁 Mame
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **jeu.zip**
┃ ┃ ┃ ┃ ┃ ┣ 📁 **jeu**
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 **jeu.chd**

## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

>Para conservar las configuraciones personalizadas cuando se produce una actualización del sistema Recalbox, os aconsejamos utilizar las [Sobrecargas de configuración](./../../../advanced-usage/configuration-override).
{.is-info}


### Acceder a las opciones

Podéis configurar las opciones del core de dos formas diferentes.

* Via el Menú RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via el fichero `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Opciones del core

| Opción | Valor de la opción | Variable (para la sobrecarga) | Valores posibles |
 | --- | --- | --- | --- |
 | Thread Mode | `Désactivé` / `Activé` ✅ | `mame_thread_mode` | `disabled` / `enabled` |
 | Cheats | `Désactivé` ✅ / `Activé` | `mame_cheats_enable` | `disabled` / `enabled` |
 | Throttle | `Désactivé` ✅ / `Activé` | `mame_throttle` | `disabled` / `enabled` |
 | Boot ot BIOS | `Désactivé` ✅ / `Activé` | `mame_boot_to_bios` | `disabled` / `enabled` |
 | Boot to OSD | `Désactivé` ✅ / `Activé` | `mame_boot_to_osd` | `disabled` / `enabled` |
 | Read Configuration | `Désactivé` ✅ / `Activé` | `mame_read_config` | `disabled` / `enabled` |
 | Write Configuration | `Désactivé` ✅ / `Activé` | `mame_write_config` | `disabled` / `enabled` |
 | MAME INI Paths | `Désactivé` ✅ / `Activé` | `mame_mame_paths_enable` | `disabled` / `enabled` |
 | Save State Naming | `Game` / `System` ✅ | `mame_saves` | `game` / `system` |
 | Auto Save/Load States | `Désactivé` ✅ / `Activé` | `mame_auto_save` | `disabled` / `enabled` |
 | Softlists | `Désactivé` / `Activé` ✅ | `mame_softlists_enable` | `disabled` / `enabled` |
 | Softlist Automatic Media Type | `Désactivé` / `Activé` ✅ | `mame_softlistst_auto_media` | `disabled` / `enabled` |
 | Media Type | `cart` / `cass` / `cdrm` / `flop` / `hard` / `prin` / `rom` ✅ / `serl` | `mame_media_type` | `cart` / `cass` / `cdrm` / `flop` / `hard` / `prin` / `rom` / `serl` |
 | Joystick Deadzone | `0.00` / `0.05` / `0.10` / `0.15` ✅ / `0.20` / `0.25` / `0.30` / `0.35` / `0.40` / `0.45` / `0.50` / `0.55` / `0.60` / `0.65` / `0.70` / `0.75` / `0.80` / `0.85` / `0.90` / `0.95` / `1.00` | `mame_joystick_deadzone` | `0.00` / `0.05` / `0.10` / `0.15` / `0.20` / `0.25` / `0.30` / `0.35` / `0.40` / `0.45` / `0.50` / `0.55` / `0.60` / `0.65` / `0.70` / `0.75` / `0.80` / `0.85` / `0.90` / `0.95` / `1.00` |
 | Joystick Saturation | `0.05` / `0.10` / `0.15` / `0.20` / `0.25` / `0.30` / `0.35` / `0.40` / `0.45` / `0.50` / `0.55` / `0.60` / `0.65` / `0.70` / `0.75` / `0.80` / `0.85` ✅ / `0.90` / `0.95` / `1.00` | `mame_joystick_saturation` | `0.05` / `0.10` / `0.15` / `0.20` / `0.25` / `0.30` / `0.35` / `0.40` / `0.45` / `0.50` / `0.55` / `0.60` / `0.65` / `0.70` / `0.75` / `0.80` / `0.85` / `0.90` / `0.95` / `1.00` |
 | Joystick Threshold | `0.05` / `0.10` / `0.15` / `0.20` / `0.25` / `0.30` ✅ / `0.35` / `0.40` / `0.45` / `0.50` / `0.55` / `0.60` / `0.65` / `0.70` / `0.75` / `0.80` / `0.85` / `0.90` / `0.95` / `1.00` | `mame_joystick_threshold` | `0.05` / `0.10` / `0.15` / `0.20` / `0.25` / `0.30` / `0.35` / `0.40` / `0.45` / `0.50` / `0.55` / `0.60` / `0.65` / `0.70` / `0.75` / `0.80` / `0.85` / `0.90` / `0.95` / `1.00` |
 | Joystick 4-way Simulation | `Désactivé` ✅ / `Activé` | `mame_mame_4way_enable` | `disabled` / `enabled` |
 | Profile Buttons Per Game | `Désactivé` ✅ / `Activé` | `mame_buttons_profiles` | `disabled` / `enabled` |
 | Mouse | `Désactivé` / `Activé` ✅ | `mame_mouse_enable` | `disabled` / `enabled` |
 | Lightgun Mode | `Lightgun` ✅ / `Touchscreen` / `None` | `mame_lightgun_mode` | `lightgun` / `touchscreen` / `none` |
 | Lightgun Offscreen Position | `Free` ✅ / `Fixed (Top Left)` / `Fixed (Bottom Right)` | `mame_lightgun_offscreen_mode` | `free` / `fixed (top left)` / `fixed (bottom right)` |
 | Screen Rotation Mode | `Libretro` ✅ / `Internal` / `TATE-ROL` / `TATE_ROR` / `None` | `mame_rotation_mode` | `libretro` / `internal` / `tate-rol` / `tate-ror` / `none` |
 | Alternate Renderer | `Désactivé` ✅ / `Activé` | `mame_alternate_renderer` | `disabled` / `enabled` |
 | Alternate Renderer Resolution | `640x480` ✅ / `640x360` / `800x600` / `800x450` / `960x720` / `960x540` / `1024x768` / `1024x576` / `1280x960` / `1280x720` / `1600x1200` / `1600x900` / `1440x1080` / `1920x1080` / `1920x1440` / `2560x1440` / `2880x2160` / `3840x2160` | `mame_altres` | `640x480` / `640x360` / `800x600` / `800x450` / `960x720` / `960x540` / `1024x768` / `1024x576` / `1280x960` / `1280x720` / `1600x1200` / `1600x900` / `1440x1080` / `1920x1080` / `1920x1440` / `2560x1440` / `2880x2160` / `3840x2160` |
 | CPU Overclock | `Default` / `25` / `26` / `27` / `28` / `29` / `30` / `31` / `32` / `33` / `34` / `35` / `36` / `37` / `38` / `39` / `40` / `41` / `42` / `43` / `44` / `45` / `46` / `47` / `48` / `49` / `50` / `51` / `52` / `53` / `54` / `55` / `60` / `65` / `70` / `75` / `80` / `85` / `90` / `95` / `100` / `105` / `110` / `115` / `120` / `125` / `130` / `135` / `140` / `145` / `150` / `155` / `160` / `165` / `170` / `175` / `180` / `185` / `190` / `195` / `200` / `205` / `210` / `215` / `220` / `225` / `230` / `235` / `240` / `245` / `250` / `255` / `260` / `265` / `270` / `275` / `280` / `285` / `290` / `295` / `300` | `mame_cpu_overclock` | `default` / `25` / `26` / `27` / `28` / `29` / `30` / `31` / `32` / `33` / `34` / `35` / `36` / `37` / `38` / `39` / `40` / `41` / `42` / `43` / `44` / `45` / `46` / `47` / `48` / `49` / `50` / `51` / `52` / `53` / `54` / `55` / `60` / `65` / `70` / `75` / `80` / `85` / `90` / `95` / `100` / `105` / `110` / `115` / `120` / `125` / `130` / `135` / `140` / `145` / `150` / `155` / `160` / `165` / `170` / `175` / `180` / `185` / `190` / `195` / `200` / `205` / `210` / `215` / `220` / `225` / `230` / `235` / `240` / `245` / `250` / `255` / `260` / `265` / `270` / `275` / `280` / `285` / `290` / `295` / `300` |
 | Automatic Load Fast-Forward | `Désactivé` ✅ / Activé | `mame_autoloadfastforward` | `disabled` / `enabled` |


## ![](/emulators/external-links.png) Enlace externos

* **Progettosnaps** : [https://www.progettosnaps.net/](https://www.progettosnaps.net/)
* **Página officiel** : [https://github.com/libretro/mame/](https://github.com/libretro/mame)