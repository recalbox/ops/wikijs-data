---
title: Libretro Mednafen_VB
description: 
published: true
date: 2024-06-22T11:22:25.072Z
tags: libretro, mednafen, virtual boy, vb
editor: markdown
dateCreated: 2021-05-21T08:28:07.021Z
---



## ![](/emulators/license.svg) Licence

Ce core est sous licence [**GPLv2**](https://github.com/libretro/beetle-vb-libretro/blob/master/COPYING).

## ![](/emulators/compatibility.png) Compatibilité

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: |:---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Fonctionnalités

| Fonctionnalité | Supporté |
| :---: | :---: |
| Redémarrage  | ✔ |
| Captures d'écran | ✔ |
| Sauvegardes | ✔ |
| Sauvegardes instantanées | ✔ |
| Rembobinage | ✔ |
| Netplay | ✔ (pas d'émulation de câble link) |
| Options du core | ✔ |
| RetroAchievements | ✔ |
| Contrôles | ✔ |
| Remapping | ✔ |
| Crop Overscan | ✔ |

## ![](/emulators/bios.svg) BIOS

>**Aucun bios n'est requis.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Extensions supportées

Les roms doivent avoir l'extension suivante :

* .bin
* .vb
* .zip
* .7z

Ce système supporte les roms compressées au format .zip/.7z. Attention toutefois, il ne s'agit que d'une archive.

Les fichiers contenus dans les .zip/.7z doivent correspondre aux extensions citées précédemment.
De plus, chaque fichier .zip/.7z ne doit contenir qu'une seule rom compressée.

### Emplacement

Placez les roms comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 virtualboy
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.zip**

>Les roms au format **No-Intro** sont vivement conseillées.
{.is-success}

>Pour plus d'information sur les roms, rendez-vous sur [ce tutoriel](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuration avancée de l'émulateur

>Pour pouvoir conserver vos configurations personnalisées lors d'une mise à jour, nous vous conseillons d'utiliser notre fonctionnalité [Surcharges de configuration](./../../../advanced-usage/configuration-override).
{.is-info}

### Accéder aux options

Vous pouvez configurer diverses options de deux façons différentes.

* Via le menu RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via le fichier `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Options du core

| Options | Valeurs des options | Variable (pour surcharge) | Valeurs des variables |
| --- | --- | --- | --- |
| 3D mode | `anaglyph` / `cyberscope` ✅ / `side-by-side` / `vli` / `hli` | `vb_3dmode` | `anaglyph` / `cyberscope` / `side-by-side` / `vli` / `hli` |
| Anaglyph preset | `Désactivé` ✅ / `red & blue` / `red & cyan` / `red & electric cyan` / `green & magenta` / `yellow & blue` | `vb_anaglyph_preset` | `disabled` / `red & blue` / `red & cyan` / `red & electric cyan` / `green & magenta` / `yellow & blue` |
| Palette | `black & red` ✅ / `black & white` / `black & blue` / `black & cyan` / `black & electric cyan` / `black & green` / `black & magenta` / `black & yellow` | `vb_color_mode` | `black & red` / `black & white` / `black & blue` / `black & cyan` / `black & electric cyan` / `black & green` / `black & magenta` / `black & yellow` |
| Right analog to digital | `Désactivé` ✅ / `Activé` / `invert x` / `invert y` / `invert both` | `vb_right_analog_to_digital` | `disabled` / `enabled` / `invert x` / `invert y` / `invert both` |
| CPU emulation (Restart) | `accurate` ✅ / `fast` | `vb_cpu_emulation` | `accurate` / `fast` |

## ![](/emulators/external-links.png) Liens externes

* **Code source utilisé** : [https://github.com/libretro/beetle-vb-libretro/](https://github.com/libretro/beetle-vb-libretro/)
* **Documentation Libretro** : [https://docs.libretro.com/library/beetle_vb/](https://docs.libretro.com/library/beetle_vb/)