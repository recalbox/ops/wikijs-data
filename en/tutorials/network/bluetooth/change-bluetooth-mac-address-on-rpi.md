---
title: Change the Bluetooth MAC address on Raspberry Pi
description: 
published: true
date: 2021-08-06T14:52:01.669Z
tags: bluetooth, mac, address
editor: markdown
dateCreated: 2021-08-06T08:39:02.382Z
---

## Why?

In some cases, Raspberry Pi's can malfunction and none of the USB ports are working, so it is not possible to pair wireless controllers. By changing the Bluetooth MAC address, you can synchronize controllers with other Raspberry Pi by duplicating the MAC address.

## Install bdaddr

Connect via [SSH](./../../../tutorials/system/access/root-access-terminal-cli) and run these command lines:

```shell
wget https://www.tbit.com.br/files/static/bdaddr.gz
gunzip bdaddr.gz
mv bdaddr /bin
chmod +x /bin/bdaddr
```

## Change your MAC address

Now you can change the Bluetooth MAC address with these commands:

```shell
bdaddr -i hci0 -r B8:27:EB:00:00:00
hciconfig hci0 reset
```

## Apply the change at each boot

Edit or create the boot file at `/recalbox/share/userscripts/custom.sh` :

```bash
#!/bin/sh

sleep 2
/bin/bdaddr -i hci0 -r B8:27:EB:00:00:00
hciconfig hci0 reset
```