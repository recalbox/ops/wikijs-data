---
title: 🎨 Thèmes
description: Manuel détaillé pour la création de thèmes
published: true
date: 2024-01-29T00:16:14.306Z
tags: themes, 9.2+
editor: markdown
dateCreated: 2024-01-18T22:20:09.187Z
---

>Cette partie des tutoriels est en cours de rédaction.
>
>Elle est valide **à partir de Recalbox 9.2** !
{.is-warning}

Cette partie des tutoriels et les pages listées ci-dessous vont vous détailler comment fonctionnent les thèmes sur Recalbox : comment ça fonctionne, qu'est-ce qui est disponible, etc.

Voici les tutoriels disponibles :

[1. Arborescence](tree)
[2. Les variables](variables)
[3. Les conditions](conditions)
[4. Les objets](objects)