---
title: ColecoVision
description: 
published: true
date: 2021-08-09T17:40:04.663Z
tags: consoles, coleco, colecovision
editor: markdown
dateCreated: 2021-07-27T17:19:15.889Z
---

![](/emulators/consoles/colecovision.svg){.align-center}

## Technical data

* **Manufacturer**: Coleco
* **Year of release**: 1982
* **Units sold**: 2+ million
* **Best-selling game**: Donkey Kong
* **CPU**: Zilog Z80 @ 3.58 MHz
* **RAM**: 1kB
* **VRAM**: 16kB
* **Video**: Texas Instruments TMS9928A
* **Resolution**: 256x192, 16 colors
* **Sound**: Texas Instruments SN76489A
* **Cart size**: 8kB - 32kB

## Presentation

The **ColecoVision** is a second generation video game console marketed by Coleco (Connecticut Leather Company), released in August 1982. It features graphics and controllers similar to arcade games of the time.

The ColecoVision was released in the summer of 1982 for a price of $399. The launch catalog included twelve titles, with ten more to come. Eventually, about 400 titles were released on cartridge during its lifetime. The majority of the titles in its catalog are conversions of arcade games.

Coleco released a module that made the console compatible with the Atari 2600, giving it the largest library of games of any console at the time. The module caused Atari to take legal action, but Atari was unable to prevent the module from being sold. For the anecdote, thanks to this module, the owners of French VCS2600 cartridges discovered finally their games in 128 colors instead of the simple suspicious 8 colors colorization of the black and white version of those, on VCS2600 SECAM.

Another module, released in summer 1983, turns the console into a real computer, the Coleco Adam, with keyboard and cassette player.

By Christmas 1982, Coleco had sold 500,000 consoles, mainly thanks to the quality of the games delivered. While Atari's fortune was made with Space Invaders, the ColecoVision was the first console to host the game Donkey Kong. The rights were negotiated for $250,000.

Sales quickly topped one million in early 1983, before the 1983 video game crash in the United States. By the end of the first quarter of 1984, sales were down to 2 million in total. Marketed in Europe as CBS ColecoVision, the console was available in France in the summer of 1983 for a price of 1,790 francs. The Adam module was briefly offered for sale in early 1984.

Coleco Industries considered withdrawing from the video game industry in June 1985 and the ColecoVision was officially discontinued in October 1985.

## Emulators

[Libretro blueMSX](libretro-bluemsx)
[Libretro FBNeo](libretro-fbneo)