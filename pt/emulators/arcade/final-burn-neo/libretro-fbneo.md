---
title: Libretro FBNeo
description: 
published: true
date: 2021-09-23T23:38:43.767Z
tags: libretro, fbneo, finalburn, neo
editor: markdown
dateCreated: 2021-09-23T23:38:43.767Z
---

**FinalBurn Neo**, um emulador para jogos de Arcade e alguns consoles. Baseia-se nos emuladores FinalBurn e nas versões mais antigas do [MAME](https://www.mamedev.org/) (emulando menos máquinas de arcade), mas funciona com os mesmos princípios.

FB Neo (também conhecido como FinalBurn Neo) é um emulador de arcade que suporta as seguintes plataformas:

* Capcom CPS-1
* Capcom CPS-2
* Capcom CPS-3
* Cave
* Jogos baseados no Data East DEC-0, DEC-8, Cassette System, DECO IC16 e DECO-32
* Hardware baseado no Galaxian
* Hardware Irem M62, M63, M72, M90, M92 e M107
* Kaneko 16
* Konami
* Namco Mappy, Sistema 86, Sistema 1 & 2 e outros
* Neo-Geo
* NMK16
* Hardware baseado no Pacman
* PGM
* Hardware baseado em Psikyo 68EC020 e SH-2
* Sega System 1, System 16 (e similar), System 18, X-Board e Y-Board
* Hardware baseado em Seta-Sammy-Visco (SSV)
* Super Kaneko Nova System
* Toaplan 1
* Toaplan 2
* Taito F2, F3, X, Z e outros
* Vários drivers para outros Hardwares
* FB Neo também contém drivers funcionais para os seguintes sistemas:
  * Sega Megadrive/Genesis
  * ColecoVision
  * Sega SG-1000
  * Sega Master System
  * Game Gear
  * MSX-1
  * ZX Spectrum
  * PC-Engine
  * TurboGrafx 16
  * SuperGrafx
  * Fairchild Channel F.

## ![](/emulators/license.svg) Licença

Este núcleo é licenciado sob uma licença [**não-comercial**](https://github.com/finalburnneo/FBNeo/blob/master/src/license.txt).

## ![](/emulators/compatibility.png) Compatibilidade

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Recursos

| Recurso | Suportado |
| :---: | :---: |
| Saves | ✔ |
| Savestates | ✔ |
| Rebobinar | ✔ |
| Netplay | ✔ |
| RetroAchievements | ✔ |
| Trapaças do RetroArch | ✔ |
| Trapaças Nativas | ✔ |
| Controles | ✔ |
| Multi-Mouse | ✔ |
| Subsistema | ✔ |

## ![](/emulators/bios.svg) BIOS

### Lista das BIOS

Dependendo do jogo, algumas BIOS serão necessárias e devem ser colocadas na mesma pasta que os jogos.

#### Local

Coloque as BIOS dessa forma:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 fbneo
┃ ┃ ┃ ┃ ┣ 🗒 **bios.zip**


## ![](/emulators/roms.png) Roms

### Extensões suportadas

Todos os jogos FinalBurn Neo usam os seguintes formatos de jogo:

* .fba
* .zip
* .7z

#### Local

Coloque as roms assim:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 fbneo
┃ ┃ ┃ ┃ ┣ 🗒 **jogo.zip**

> Para mais informações sobre ROMs, vá para [este tutorial](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuração avançada do emulador

> Para poder manter suas configurações personalizadas durante uma atualização, aconselhamos que você use nosso recurso de [Sobrecarga de configuração](./../../../advanced-usage/configuration-override).
{.is-info}

### Acessar as Opções

Você pode configurar várias opções de duas maneiras diferentes.

* Por meio do menu RetroArch:

┣ 📁 Menu RetroArch
┃ ┣ 📁 Opções do núcleo
┃ ┃ ┣ 🧩 Nome_da_opção

* Por meio do arquivo `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Opções de núcleo



## ![external-links.png](/emulators/external-links.png) Links externos

* **Código fonte utilizado** : [https://github.com/libretro/FBNeo/tree/master](https://github.com/libretro/FBNeo/tree/master)
* **Documentação Libretro** : [https://docs.libretro.com/library/fbneo/](https://docs.libretro.com/library/fbneo/)
* **Site oficial** : [https://neo-source.com/](https://neo-source.com/)
* **Wiki do núcleo** : [http://emulation.gametechwiki.com/index.php/FinalBurn_Neo](http://emulation.gametechwiki.com/index.php/FinalBurn_Neo)
* **Wiki oficial** : [https://github.com/finalburnneo/FBNeo/wiki](https://github.com/finalburnneo/FBNeo/wiki)