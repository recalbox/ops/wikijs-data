---
title: Scraping tools
description: 
published: true
date: 2022-01-13T23:10:29.370Z
tags: scrap, tools, scraping
editor: markdown
dateCreated: 2021-08-07T00:04:14.472Z
---

Work in progress... trying to gather all the scrapbooking tools in one place.

| Tools | Windows | Linux | macOS | Console | Arcade | Sources |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| [Internal scraper] |  | recalbox |  | 🎮 | 👾 | [ScreenScraper](https://screenscraper.fr/), [TheGamesDB](http://thegamesdb.net/), [MameDB-mirror](http://mamedb.blu-ferret.co.uk/) |
| [UXS] | ✔️ |  |  | 🎮 | 👾 | [ScreenScraper](https://screenscraper.fr/) |
| [sselph's scraper] | ✔️ | ✔️ | ✔️ | 🎮 | 👾 | [ScreenScraper](https://screenscraper.fr/), [TheGamesDB](http://thegamesdb.net/), [MameDB-mirror](http://mamedb.blu-ferret.co.uk/), [ArcadeItalia](http://adb.arcadeitalia.net/), [OpenVGDB](https://github.com/OpenVGDB/OpenVGDB/) |
| [Skraper] | ✔️ | ✔️ | ✔️ | 🎮 | 👾 | [ScreenScraper](https://screenscraper.fr/) |
| [GameList Editor] | ✔️ |  |  | 🎮 | 👾 | [ScreenScraper](https://screenscraper.fr/) |
| [ARRM] | ✔️ |  |  | 🎮 | 👾 | [ScreenScraper](https://screenscraper.fr/), [TheGamesDB](http://thegamesdb.net/), [MameDB-mirror](http://mamedb.blu-ferret.co.uk/), [HFSDb](https://db.hfsplay.fr/), [LaunchBox](https://gamesdb.launchbox-app.com/), [Steam](https://store.steampowered.com/), [GOG](https://www.gog.com/) |
| [mamescraper] | ✔️ | ✔️ | ✔️ |  | 👾 | bigode, [ArcadeItalia](http://adb.arcadeitalia.net/) |
| [SkyScraper] |  | ✔️ | ✔️ | 🎮 |  | [ScreenScraper](https://screenscraper.fr/), [TheGamesDB](http://thegamesdb.net/), [ArcadeItalia](http://adb.arcadeitalia.net/), [OpenRetro](http://openretro.org), [WorldOfSpectrum](http://worldofspectrum.org), [IGDB](http://igdb.com), [MobyGames](http://mobygames.com) |

## Scraper interne

[https://gitlab.com/recalbox/recalbox-emulationstation](https://gitlab.com/recalbox/recalbox-emulationstation)

 [![Internal scraper](https://i.ytimg.com/vi/xd1i1mJdkjU/maxresdefault.jpg)](https://www.youtube.com/watch?v=xd1i1mJdkjU)  
\* Click on the image to view a video presentation.

## Universal XML Scraper (UXS)

[https://github.com/Universal-Rom-Tools/Universal-XML-Scraper](https://github.com/Universal-Rom-Tools/Universal-XML-Scraper)

## sselph's scraper

[https://github.com/sselph/scraper](https://github.com/sselph/scraper)  
Tip: use a [fastscraper](https://github.com/paradadf/recaltools/tree/master/fastscraper) with it!

## Skraper

[https://www.skraper.net/](https://www.skraper.net/)

## GameList Editor

[https://github.com/NeeeeB/GameList\_Editor](https://github.com/NeeeeB/GameList_Editor)

## Another Recalbox Roms Manager (ARRM)

[http://jujuvincebros.fr/telechargements2/file/10-arrm-another-recalbox-roms-manager](http://jujuvincebros.fr/telechargements2/file/10-arrm-another-recalbox-roms-manager)

## mamescraper

[https://github.com/pdrb/mamescraper](https://github.com/pdrb/mamescraper)

## SkyScraper

[https://github.com/muldjord/skyscraper](https://github.com/muldjord/skyscraper)