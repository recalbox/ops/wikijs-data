---
title: Libretro Mednafen_WSwan
description: 
published: true
date: 2021-11-04T18:31:10.424Z
tags: libretro, mednafen, wswan, wonderswan
editor: markdown
dateCreated: 2021-07-27T16:15:26.174Z
---

**Libretro beetle-wswan** is an emulator for the Wonderswan, Wonderswan Color and Pocket Challenge V2 portable game consoles developed in C and C++.

It is a standalone port of Mednafen WonderSwan to Libretro, itself a fork of Cygne.

## ![](/emulators/license.svg) License

This core is under [**GPLv2**](https://github.com/libretro/beetle-wswan-libretro/blob/master/COPYING) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Features

| Feature | Supported |
| :---: | :---: |
| Restart | ✔ |
| Screenshots | ✔ |
| Saves | ✔ |
| States | ✔ |
| Rewind | ✔ |
| Netplay | ✔ (pas d'émulation du câble link) |
| Controls | ✔ |
| Remapping | ✔ |
| Softpatching | ✔ |

## ![](/emulators/bios.svg) BIOS

>**No bios is required.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Supported extensions

Roms must have the extension:

* .ws
* .zip
* .7z

This system supports compressed roms in .zip/.7z format. However, be careful, it is only an archive.

The files contained in the .zip/.7z must correspond to the extensions mentioned above.
Moreover, each .zip/.7z file must contain only one compressed rom.

### Location

Put your roms like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 wswan
┃ ┃ ┃ ┃ ┣ 🗒 **game.zip**

>The roms using **No-Intro** format are highly recommended.
{.is-success}

>For more information about roms, go to [this tutorial](./../../../tutorials/games/generalities/isos-and-roms)!
{.is-info}

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Accessing to options

You can configure various options in two different ways.

* Via RetroArch menu:

┣ 📁 RetroArch Menu
┃ ┣ 📁 Core options
┃ ┃ ┣ 🧩 Name_option

* Via the file `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core options

| Options | Options values | Variables (for configuration override) | Variables values |
| --- | --- | --- | --- |
| Rotate Display | `Manual` ✅ / `Enabled` | `wswan_rotate_display` | `manual` / `enabled` |
| Rotate Button Mappings | `Auto` ✅ / `Disabled` / `Enabled` | `wswan_rotate_keymap` | `auto` / `disabled` / `enabled` |
| WS Colorization | `Greyscale` ✅ / `WS - WonderSwan` / `WS - WonderSwan Color` / `WS - SwanCrystal` / `Game Boy DMG` / `Game Boy Pocket` / `Game Boy Light` / `Blossom Pink` / `Bubbles Blue` / `Buttercup Green` / `Digivice` / `Game.com` / `GameKing` / `Game Master` / `Golden Wild` / `Greenscale` / `Hokage Orange` / `Labo Fawn` / `Legendary Super Saiyan` / `Microvision` / `Million Live Gold` / `Odyssey Gold` / `Shiny Sky Blue` / `Slime Blue` / `TI-83` / `Travel Wood` / `Virtual Boy` | `wswan_mono_palette` | `default` / `wonderswan` / `wondeswan_color` / `swancrystal` / `gb_dmg` / `gb_pocket` / `gb_light` / `blossom_pink` / `bubbles_blue` / `buttercup_green` / `digivice` / `game_com` / `gameking` / `game_master` / `golden_wild` / `greenscale` / `hokage_orange` / `labo_fawn` / `legendary_super_saiyan` / `microvision` / `million_live_gold` / `odyssey_gold` / `shiny_sky_blue` / `slime_blue` / `ti_83` / `travel_wood` / `virtual_boy` |
| Frameskip | `Disabled` ✅ / `Auto` / `Manual` | `wswan_frameskip` | `disabled` / `auto` / `manual` |
| Frameskip Threshold (%) | `15` / `18` / `21` / `24` / `27` / `30` / `33` ✅ / `36` / `39` / `42` / `45` / `48` / `51` / `54` / `57` / `60` | `wswan_frameskip_threshold` | `15` / `18` / `21` / `24` / `27` / `30` / `33` / `36` / `39` / `42` / `45` / `48` / `51` / `54` / `57` / `60` |
| Sound Output Sample Rate | `11025` / `22050` / `44100` ✅ / `48000` / `96000` / `192000` / `384000` | `wswan_sound_sample_rate` | `11025` / `22050` / `44100` / `48000` / `96000` / `192000` / `384000` |
| Color Depth (Restart) | `16bit` ✅ / `24bit` | `wswan_gfx_colors` | `16bit` / `24bit` |

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/libretro/beetle-wswan-libretro/](https://github.com/libretro/beetle-wswan-libretro/)
* **Libretro documentation**: [https://docs.libretro.com/library/beetle_cygne/](https://docs.libretro.com/library/beetle_cygne/)
* **Official source code**: [https://mednafen.github.io/releases/](https://mednafen.github.io/releases/)
* **Official website**: [https://mednafen.github.io/](https://mednafen.github.io/)
* **Official forum**: [https://forum.fobby.net/](https://forum.fobby.net/)
* **Official documentation**: [https://mednafen.github.io/documentation/wswan.html](https://mednafen.github.io/documentation/wswan.html)