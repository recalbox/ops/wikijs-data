---
title: Vídeos de inicialização
description: 
published: true
date: 2021-08-12T23:22:20.071Z
tags: 
editor: markdown
dateCreated: 2021-08-12T23:22:20.071Z
---

No Recalbox você tem a possibilidade de adicionar seus próprios vídeos pessoais para serem reproduzidos na inicialização.

Veremos:

* Como adicionar vídeos
* Como configurar sua reprodução

## Adicionando novos vídeos de inicialização

A adição de um novo vídeo de inicialização é feita através da pasta `/recalbox/share/bootvideos/`.

>Adicionar vídeos só pode ser feito **através da rede**!
> 
>A totalidade de seus vídeos não deve exceder 500 Mb.
{.is-warning}

Para se conectar via rede ao seu Recalbox, você tem várias possibilidades:

* Via [WinSCP](./../../tutorials/system/access/network-access-winscp)
* Via [Cyberduck](./../../tutorials/system/access/netword-access-cyberduck)
* Via [MobaXTerm](./../../tutorials/system/access/network-access-mobaxterm)
* Através de seu explorador de arquivos. Para isso, você precisará do [endereço IP](./../../tutorials/network/ip/discover-recalbox-ip) de seu Recalbox e inserir o endereço ``\\seu-endereço-ip`` em seu explorador de arquivos.

## Configurações relacionadas a vídeos de inicialização

### Freqüência de reprodução

Você pode definir a freqüência na qual você pode ver seu(s) vídeo(s) de inicialização através do parâmetro `system.splash.select` no arquivo [recalbox.conf](./../../basic-usage/getting-started/recalboxconf-file).

Aqui estão as opções disponíveis:

* **all**: o sistema escolhe entre um vídeo nativo ou um de seus vídeos
* **custom**: o sistema escolhe um de seus vídeos
* **recalbox**: o sistema escolhe um vídeo nativo

### Tempo de reprodução

Você pode definir o tempo de reprodução de todos os vídeos (nativos ou seus próprios) através do parâmetro `system.splash.length` no arquivo [recalbox.conf](./../../basic-usage/getting-started/recalboxconf-file).

Aqui estão as opções disponíveis:

* **0**: A reprodução do vídeo será interrompida quando o EmulationStation estiver pronto
* **-1**: A reprodução do vídeo será concluída antes do início do EmulationStation
* **Qualquer valor maior que 0**: A reprodução do vídeo durará o número especificado de segundos antes do início do EmulationStation