---
title: Mega Duck
description: 
published: true
date: 2022-07-22T09:58:45.071Z
tags: 8.1+, megaduck
editor: markdown
dateCreated: 2022-07-22T09:53:34.632Z
---

![](/emulators/handheld/megaduck.png){.align-center}

## Fiche technique

* **Fabricant** : Welback Holdings
* **Année de sortie** : 1993
* **Processeur** : version MOS du Z80 (embarqué dans le VLSI principal)
* **RAM** : 16kB
* **Résolution** : 160x144 pixels, 4 niveaux de gris
* **Son** : haut-parleur intégré (8Ω 200mW)
* **Média** : cartouches de 36 pins

## Présentation

La Mega Duck (aussi connu sous le nom Cougar Boy) est une console portable créée par plusieurs compagnies (Creatonic, Videojet et Timlex). Elle est apparue sur le marché du jeu vidéo en 1993 et était vendue à un prix d'environ 60€ (principalement en France, aux Pays-Bas et en Allemagne). En Amérique du Sud (principalement au Brésil), la version Creatonic développée en Chine était distribuée par Cougar USA et portait le nom "Cougar Boy".

Les cartouches sont assez semblables à celles de la Watara Supervision, bien qu'elles soient légèrement moins large que ces dernières (36 fiches pour la Mega Duck contre 40 pour la Supervision). L'électronique des deux consoles est elle aussi tout à fait similaire. L'emplacement des contrôles de volume sonore et du contraste, ainsi que les boutons et connecteurs est quasi identique. Cependant, l'écran de la Supervision est plus large que celui de la Megaduck.

La Cougar Boy était vendue avec une cartouche de jeu 4-en-1 et des écouteurs stéréo. Aussi, deux joueurs pouvaient jouer simultanément l'un contre l'autre à l'aide d'un joystick externe (vendu séparément de la console).

## Émulateurs

[Libretro Sameduck](libretro-sameduck)