---
title: DOS
description: 
published: true
date: 2021-09-04T23:25:54.857Z
tags: dos, msdos, pc
editor: markdown
dateCreated: 2021-06-15T23:23:57.261Z
---

Vous aurez ici quelques tutoriels concernant DOS.

Voici les tutoriels disponibles :

[DOSbox pour émuler des jeux DOS](dosbox-to-play-with-dos-games)
[Utilisation de la souris avec la manette dans DOSBox Pure](mouse-use-with-controller-in-dosbox-pure)