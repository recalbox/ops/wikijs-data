---
title: Convertir las imágenes de disco .BIN + .CUE
description: 
published: true
date: 2023-09-21T10:40:56.553Z
tags: ps1, bin, cue, convertir
editor: markdown
dateCreated: 2023-09-21T10:40:56.553Z
---

## Informaciones

Los juegos SegaCD o PSX utilizan varios formatos:

* `.ISO` + `.CUE` acompañado de ficheros de pistas al formato `.WAV`, `.BIN`
* `.ISO`
* `.BIN` + `.CUE`
* `.CCD` + `.IMG` + `.SUB`
* `.BIN` sólo

**Ejemplo:**

```text
Wonder Dog (1993)(Sega)(PAL)(Track 01 of 21)[!].iso
Wonder Dog (1993)(Sega)(PAL)(Track 02 of 21)[!].wav
Wonder Dog (1993)(Sega)(PAL)(Track 03 of 21)[!].wav
Wonder Dog (1993)(Sega)(PAL)(Track 04 of 21)[!].wav
Wonder Dog (1993)(Sega)(PAL)(Track 05 of 21)[!].wav
Wonder Dog (1993)(Sega)(PAL)(Track 06 of 21)[!].wav
Wonder Dog (1993)(Sega)(PAL)(Track 07 of 21)[!].wav
Wonder Dog (1993)(Sega)(PAL)(Track 08 of 21)[!].wav
Wonder Dog (1993)(Sega)(PAL)(Track 09 of 21)[!].wav
Wonder Dog (1993)(Sega)(PAL)(Track 10 of 21)[!].wav
Wonder Dog (1993)(Sega)(PAL)(Track 11 of 21)[!].wav
Wonder Dog (1993)(Sega)(PAL)(Track 12 of 21)[!].wav
Wonder Dog (1993)(Sega)(PAL)(Track 13 of 21)[!].wav
Wonder Dog (1993)(Sega)(PAL)(Track 14 of 21)[!].wav
Wonder Dog (1993)(Sega)(PAL)(Track 15 of 21)[!].wav
Wonder Dog (1993)(Sega)(PAL)(Track 16 of 21)[!].wav
Wonder Dog (1993)(Sega)(PAL)(Track 17 of 21)[!].wav
Wonder Dog (1993)(Sega)(PAL)(Track 18 of 21)[!].wav
Wonder Dog (1993)(Sega)(PAL)(Track 19 of 21)[!].wav
Wonder Dog (1993)(Sega)(PAL)(Track 20 of 21)[!].wav
Wonder Dog (1993)(Sega)(PAL)(Track 21 of 21)[!].wav
Wonder Dog (1993)(Sega)(PAL)[!].cue   

Rockman 8 - Metal Heroes (Japan) (Track 1).bin   
Rockman 8 - Metal Heroes (Japan) (Track 2).bin   
Rockman 8 - Metal Heroes (Japan) (Track 3).bin   
Rockman 8 - Metal Heroes (Japan) (Track 4).bin   
Rockman 8 - Metal Heroes (Japan).cue
```

## Convertir una imagen de disco `ISO` a `.BIN` + `.CUE`

Aquí tenéis la técnica para convertir los `.ISO` + `.CUE` con ficheros `.WAV`, para todos aquellos que no quieren utilizar directorios para almacenar juntos los ficheros de un juego.

>Esta técnica funciona con la PSX y la SEGACD.
{.is-info}

* Descargad [Daemons Tools Lite](https://www.daemon-tools.cc/fra/products/dtLite).
* Instalad el programa.
* Seleccionad las imágenes de disco que queréis convertir y pulsad en `+`.
  * Seleccionad el fichero `*.CUE` y pulsad `Intro`.
  * Pulsad con el botón derecho y seleccionad `Mount` en el menú contextual para crear una unidad virtual.
* Descargad [ImgBurn](http://imgburn.com/).
* Descargad el fichero en francés haciendo clic en [el enlace](http://download.imgburn.com/translations/french.zip).
* Instalad el software.
* Descomprimid el fichero de idioma y colocadlo en la carpeta `C:\Program Files(x86)\imgburn\languages`.
* Abrid el software y estará en francés.
* Pulsad el botón "Crear imagen desde el disco".
* Elejid la unidad virtual como fuente.
* Elejid una carpeta de destino para la creación los ficheros BIN y CUE.
* Pulsad el botón `Play` y esperad.

Una vez finalizada la conversión, podéis desmontar la unidad virtual (pulsad con el botón derecho en el icono de **Daemon Tools** y, a continuación, pulsad en `Unmount`).

## Crear un fichero `.CUE` para vuestro único fichero `.BIN`

Ejemplo con el finchero `r-type.bin`.

* Abrid [Notepad++](https://notepad-plus-plus.org/downloads/) en un nuevo fichero vacío.
* Tendréis que escribir 3 líneas en este nuevo fichero:
  * La primera línea debe contener el **BINARIO**.
  * La segunda línea debe contener las pistas aún en **MODE2/2352**.
  * La tercera línea debe contener el índice **01**.
* Obtendréis algo como esto, que debería funcionar:

```text
FILE "r-type.bin" BINARY
TRACK 01 MODE2/2352
INDEX 01 00:00:00
```

* Guardad el fichero con la extensión `.CUE`: `r-type.cue`.
* Todo lo que os queda por hacer es transferir vuestros ficheros `.bin` y `.cue` dentro de vuestra Recalbox.

>Si tenéis varios juegos en los que falta el fichero `.CUE`, podéis ir al [Redump site](http://redump.org/downloads/) para obtener todos los ficheros .CUE de cada sistema.
{.is-info}