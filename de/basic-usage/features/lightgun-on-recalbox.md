---
title: Lightgun mit Recalbox
description: Die Lightgun für jeden! ;-)
published: true
date: 2024-06-02T20:16:57.808Z
tags: 
editor: markdown
dateCreated: 2021-07-28T10:18:21.798Z
---

![](/basic-usage/features/lightgun/lightgun.png)

## Worum geht es dabei?

Das Prinzip besteht darin, automatisch Lightgun-kompatible Spiele für die meisten von Recalbox unterstützten Systeme zu konfigurieren. Aus Kosten- und Verfügbarkeitsgründen wollen wir dies mit einer Wiimote (oder entsprechend zwei für 2-Spieler-Spiele) bewerkstelligen. Wir können auch einen Wii Nunchunk einstecken, um das primäre und sekundäre Feuer je nach Vorliebe auszulösen. Dieser Lightgun-Modus wird automatisch aktiviert, wenn du eine Mayflash-Dolphin-Leiste im Modus 2 hast und nur für Spiele, die diesen auch Modus unterstützen.

Mit der Lightgun-Funktion von Recalbox kannst du endlich wieder mit Super Scope und anderen Retro-Waffen spielen!

**Systeme, die diese Funktion in Recalbox bereits unterstützen:**

* 3DO
* Atomiswave
* Dreamcast
* Fbneo
* Mame
* Master System
* Megadrive
* Mega-CD
* Naomi
* Naomi GD ROM System
* NES
* Playstation 1
* Saturn
* SNES

>Die libretro-Cores werden bisher unterstützt. Es gibt leider noch keinen eigenständigen Emulator, was nicht ausschließt, dass dieser noch erscheint.
{.is-info}

>ACHTUNG: dies funktioniert nur, wenn die Spiele mit eindeutig identifizierbaren Namen wie `Duck hunt (world) v1.1` und nicht `dhnes` betitelt sind! Es muss möglich sein, den Begriff "Duck Hunt" im Namen zu finden.
{.is-warning}

## Welche Materialien sind erforderlich?

![](/basic-usage/features/lightgun/wiimote.png)

![](/basic-usage/features/lightgun/dolphinbar.png)

>**Du brauchst einen Mayflash Dolphin Bar pro Wiimote im Lightgun-Modus (die Wiimote wird damit quasi zu einer USB-Maus)**
{.is-warning}

![](/basic-usage/features/lightgun/nunchuck.png)

![](/basic-usage/features/lightgun/gun1.png)![](/basic-usage/features/lightgun/gun2.png)

#![](/basic-usage/features/lightgun/crossbow.png)

## Wie verbinde ich eine Lightgun?

### 1. Verbinde die Mayflash Dolphin Bar(s) über USB mit deiner Recalbox

![](/basic-usage/features/lightgun/usbports.png)

### 2. Stelle Modus 2 an der/den Mayflash Delphin Bar(s) ein

![Appuyer sur le bouton mode jusqu'à avoir la led bleu en face du chiffre 2](/basic-usage/features/lightgun/dolphinbarmode2.png)

### 3. Synchronisiere die Wiimote mit der Mayflash Dolphin Bar

![Drücke die Sync-Taste bis eine blaue LED blinkt](/basic-usage/features/lightgun/dolphinbarsync.png)

![Dann musst du die Sync-Taste auf der Wiimote drücken](/basic-usage/features/lightgun/wiimotesync.png)

>Wenn du mit 2 Spielern spielen willst, musst du eine Wiimote mit jeder Mayflash Dolphin Bar auf die gleiche Weise verbinden, wie oben beschrieben.
{.is-info}

## Wie wird der Lightgun-Modus aktiviert?

Der Lightgun-Modus wird automatisch für Spiele aktiviert, die diesen Modus unterstützen, wenn du eine Mayflash Dolphin Bar im Modus 2.

>Der Lightgun-Modus kann durch einfaches Ein- und Ausschalten der Mayflash Dolphin Bar aktiviert/deaktiviert werden, aber es ist nicht notwendig, ihn zu ändern, es sei denn, man möchte im selben Spiel zwischen Lightgun- und Joystick-Modus wechseln.
{.is-info}

## Und schließlich... wie spielt man?

### Standardsteuerung der Wiimote

Start: ![](/basic-usage/features/lightgun/button_plus.png) um einen Spielmodus zu starten / bei einigen Spielen zu pausieren

Select: ![](/basic-usage/features/lightgun/button_minus.png) um einen Spielmodus auszuwählen / Credits einzugeben

Home: ![](/basic-usage/features/lightgun/button_home.png) um das Spiel zu beenden und zum Spielauswahlmenü zurückzukehren

Primäres Feuern: ![](/basic-usage/features/lightgun/button_z.png) mit dem Nunchuk

Sekundäres Feuern:  ![](/basic-usage/features/lightgun/button_c.png) mit dem Nunchuk 

### Hinweise zu den Wiimote-Tasten

>* Andere Tasten können für bestimmte Systeme z.B. auf dem Richtungskreuz konfiguriert werden (obwohl wir versuchen, diese Fälle zu vermeiden)
>* Die Taste ![](/basic-usage/features/lightgun/button_a.png) zum Vergeben von Credits.
>* Darüber hinaus können einige Spiele zum Beispiel mit ![](/basic-usage/features/lightgun/button_a.png) gestartet werden.
>* Bei einigen Spielen musst du auch 2 Tasten gleichzeitig drücken, um ein Spiel zu starten oder ein Menü zu verlassen/aufzurufen.
>* Der linke Analogstick ![](/basic-usage/features/lightgun/stick.png) verhält sich wie das Richtungskreuz der Wiimote.
>* Die Power-Taste ![](/basic-usage/features/lightgun/button_power.png) kann verwendet werden, um die Wiimote mit einem langen Druck auszuschalten.
>* Die folgenden Tasten sind im Lightgun-Modus nicht verfügbar: ![](/basic-usage/features/lightgun/button_1.png) und ![](/basic-usage/features/lightgun/button_2.png)
>* Wenn das Nunchuck eingesteckt ist, sind die Tasten ![](/basic-usage/features/lightgun/button_a.png) und ![](/basic-usage/features/lightgun/button_b.png) auf der Wiimote vertauscht.
{.is-info}

## Das virtuelle Lightgun-System

Du kannst ein virtuelles Lightgun-System aktivieren, um alle Lightgun-Spiele unter einem Systemmenü zusammenzuführen.  
Um es zu aktivieren, [siehe hier](./../../basic-usage/features/virtual-systems).

![VIEL SPAß!](/basic-usage/features/lightgun/duckhunt.png)