---
title: Usar un periférico USB de almacenamiento en Recalbox
description: 
published: true
date: 2024-12-27T15:57:08.090Z
tags: usb, periféricos, almacenamiento
editor: markdown
dateCreated: 2023-09-14T06:50:50.294Z
---

## Introducción

Se puede utilizar de forma muy sencilla un dispositivo de almacenamiento USB (llave USB, disco duro externo autoalimentado, etc.) para almacenar las roms, los ficheros personales, etc. 

Recalbox no formatea vuestro dispositivo, por lo que lo téneis que formatear vosotros mismos. El sistema de ficheros debe ser extFAT. Recalbox se ocupará de copiar la estructura de ficheros y carpetas adecuados.

## Configuración de Recalbox con un dispositivo de almacenamiento USB adicional

Se pueden añadir varios dispositivos de almacenamiento USB adicionales.

Una vez delante de la interfaz, conectad el nuevo dispositivo de almacenamiento USB. Veréis un mensaje en la pantalla que os pregunta si queréis inicializar el dispositivo o bien si queréis desplazar el `SHARE`. Seleccionad `INICIALIZAR`.

>**No desplazéis el `SHARE` a menos que sepáis lo que estás haciendo y lo que significa!**
{.is-warning}

Recalbox pasará un cierto tiempo creando una carpeta llamada `recalbox` a la raíz del soporte de almacenamiento, así como una carpeta `roms` dentro de ella, y también todas las carpetas necesarias correspondientes a todos los sistemas presentes en vuestra instalación.

Para añadir vuestras roms a vuestro nuevo dispositivo de almacenamiento USB, existen dos opciones:

* Podéis desconectar el dispositivo en caliente (es decir sin apagar vuestra Recalbox) para copiar vuestras roms utilizando otro ordenador. Cuando lo desconectéis, la interfaz os pedirá reiniciar para actualizar la interfaz con los juegos que quedan disponibles. Si más tarde volvéis a conectarlo a Recalbox, recibiréis un mensaje pidiendo que reiniciéis para actualizar de nuevo la lista de juegos.
* También se pueden copiar las roms a través de la red local, manteniendo Recalbox encendido y el dispositivo enchufado. Necesitaréis conocer la [dirección IP de vuestra Recalbox](./../../../tutorials/network/ip/discover-recalbox-ip).
