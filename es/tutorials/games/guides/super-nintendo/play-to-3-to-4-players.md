---
title: Juegar en modo multi-jugador (de 3 a 4 players)
description: 
published: true
date: 2024-02-04T17:09:52.622Z
tags: nintendo, multiplayer, super, multijugador
editor: markdown
dateCreated: 2023-09-21T13:21:22.458Z
---

Se puede jugar hasta a 4 a la vez en el emulador SNES de Recalbox.

Si queréis disfrutar del modo multijugador de SNES, necesitaréis un mando por jugador, debidamente configurado.

* Iniciad un juego de SNES, como Micro Machines.
* Cuando se inicie el juego, id al menú RetroArch pulsando `Hotkey + B`.
* Pulsad "Atrás" dos veces e id a "Ajustes" > "Configuración".

![](/tutorials/games/guides/snes/multitap/snesmultitap1.png){.full-width}

* Activad la opción `Save configuration on exit`.

![](/tutorials/games/guides/snes/multitap/snesmultitap2.png){.full-width}

* Haced « Back » 2 veces e id dentro de `Menú principal` > `Menú rapide` > `Entrées` > `Touches du port 2`.

![](/tutorials/games/guides/snes/multitap/snesmultitap3.png){.full-width}

* En la opción `Type de périphérique`, listad las opciones hasta obtener la valor `Multitap`.

![](/tutorials/games/guides/snes/multitap/snesmultitap4.png){.full-width}

* Haced « Back » 1 sola vez e id dentro de `Gestión de los ficheros de mapping`. En esta sección, seleccionad la opción por la cual queréis utilizar el multitap (por el jeugo, por la carpeta de todos los juegos, etc.)
 
* Haced "Back" 2 veces y seleccionad `Reiniciar` para aplicar los cambios.

Y ya está todo, el juego funciona, y vais a disfrutar como un enano!