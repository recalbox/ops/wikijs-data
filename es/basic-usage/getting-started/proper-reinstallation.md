---
title: Reinstalación limpia
description: 
published: true
date: 2024-11-17T16:24:32.087Z
tags: reinstalacion limpia
editor: markdown
dateCreated: 2023-09-04T19:42:14.190Z
---

## ¿Qué debo hacer antes de reinstalar el sistema?

### 1. No tengo ningún medio extraíble, sólo la microsd

* Ten siempre una copia de tus roms, backups y bios en tu PC o en un disco de respaldo.
* Haz una copia de seguridad de la carpeta Recalbox que contiene las roms, bios y saves en tu PC.
* Descarga y reinstala la [última versión de Recalbox]((https://www.recalbox.com/fr/download/stable/) en tu microsd.

#### Bios

* Comprueba la firma de tu bios dentro de la interfaz.
* Recalbox añade nuevos emuladores que a veces necesitan bios.

### 2. Tengo medios extraibles (llave usb/disco duro externo usb)

* Se recomienda el formato exFAT para discos duros de gran capacidad.
* Conectad el disco duro externo o la llave USB a tu PC, y renombrad la carpeta `recalbox` a `recalbox.old`.
* Descarga e instala [la última versión de Recalbox]((https://www.recalbox.com/fr/download/stable/) en tu microsd.
* Conectad vuestro disco duro externo o llave USB a vuestro sistema.
* Elige tu medio extraíble.
* El árbol de carpetas Recalbox se creará de nuevo en tu dispositivo externo.
* Apagad vuestra Recalbox.
* Conectad vuestro disco duro externo o llave USB a vuestro PC.
* Desplazad ÚNICAMENTE las carpetas ROMS, BIOS Y SAVE.
* NO MOVÁIS el gamelist/scrap, los theme personalizados ni los OVERLAYS** (responsables de muchos bugs).
* Probad vuestra Recalbox con la configuración por defecto.
* Si todo va bien, podéis empezar a personalizar la configuración gradualmente.

## Reinstalación limpia del sistema

Sigue la guía [Preparación e instalación](./../preparation-and-installation)