---
title: Game Boy
description: 
published: true
date: 2022-07-22T09:59:27.508Z
tags: nintendo, game, boy, gameboy, portables
editor: markdown
dateCreated: 2021-05-21T08:03:11.886Z
---

![](/emulators/handheld/gameboy.svg){.align-center}

## Fiche technique

* **Fabricant** : Nintendo
* **Année de sortie** : 1989
* **Quantités vendues** : 118.69 millions
* **Meilleur jeu vendu** : Tetris
* **Processeur** : 8-bit Zilog Z80 @ 4.194304MHz
* **RAM** : 8kB
* **VRAM** : 8k
* **Vidéo** : PPU (embarqué dans le processeur)
* **Résolution** : 160x144 pixels, 4 niveaux de gris
* **Son** : 4 canaux stéréo
* **Taille des cartouches** : 32kB - 1MB

## Présentation

La **Game Boy** est une console portable de jeu vidéo 8-bits de quatrième génération développée et fabriquée par Nintendo. Mise en vente au Japon le 21 avril 1989, puis en Amérique du Nord en octobre 1989, et enfin en Europe le 28 septembre 1990, elle est la première console portable de la gamme des Game Boy. Elle fut conçue par Gunpei Yokoi et Nintendo Research & Development 1 — la même équipe ayant conçu la série des Game and Watch ainsi que de nombreux jeux à succès sur Nintendo Entertainment System.

Malgré la sortie de consoles portables techniquement plus avancées, la Game Boy connaît un franc succès. Elle atteint alors la troisième place parmi les consoles les plus vendues de l'histoire, avec 119 millions d'exemplaires vendus, derrière la PlayStation 2 de Sony et la Nintendo DS. Malgré une qualité graphique minimaliste, la Game Boy a su s'imposer grâce à de nombreux atouts : petite taille, prix bas, grande autonomie et catalogue de jeux aussi riche que varié.

## Émulateurs

[Libretro Gambatte](libretro-gambatte)
[Libretro Mesen_S](libretro-mesen_s)
[Libretro SameBoy](libretro-sameboy)
[Libretro TGBDual](libretro-tgbdual)
[Libretro bsnes](libretro-bsnes)
[Libretro mGBA](libretro-mgba)