---
title: Actualización manual y/o sin conexión a internet
description: Cómo actualizar Recalbox sin conexión a internet, de forma offline
published: true
date: 2024-09-29T14:05:38.054Z
tags: offline, actualizaciones
editor: markdown
dateCreated: 2023-08-31T07:21:38.900Z
---

## ¿Cuándo hay que utilizar la actualización manual?

En ciertos casos, puede ser necesario actualizar Recalbox manualmente:

- Cuando vuestra Recalbox no tienen conexión a internet.
- Cuando el equipo técnico te pide que pruebes una imagen de desarrollo para comprobar que se ha corregido un error.
- Cuando tengas un problema en el sistema (ocurre muy pocas veces).

>Los siguientes ejemplos utilizan la imagen Recalbox para Raspberry Pi 4. Por supuesto, tendrás que adaptar estos ejemplos a tu sistema.
> {.is-info}

Para hacer una actualización offline, necesitas dos ficheros:

- El fichero **image** de Recalbox, llamado `recalbox-rpi4_64.img.xz`.
- El fichero **checksum**, que contiene la suma de comprobación de la imagen para poder verificar automáticamente su integridad. Tiene el mismo nombre que la imagen, pero con extensión `.sha1`, en nuestro ejemplo sería `recalbox-rpi4_64.img.xz.sha1`.


## Descargar la imagen oficial

Para actualizar Recalbox a la última versión disponible en modo offline, visitad https://www.recalbox.com/download/stable/allimages/ :

A continuación, descargad la imagen correspondiente a vuestro sistema junto con el fichero checksum `sha1`:

![download.png](/advanced-usage/manual-update/download.png)

## Copiar los archivos a la tarjeta SD

Se puede proceder de dos formas diferentes:
 
 ## {.tabset}
 ### Tarjeta SD dentro de vuestro PC
 
Una vez en posesión de los dos archivos `recalbox-rpi4_64.img.xz` y `recalbox-rpi4_64.img.xz.sha1`, lo único que hay que hacer es colocarlos en el directorio `update` de la partición `RECALBOX` de la tarjeta SD en la que se encuentra instalada Recalbox:

![images-in-boot-partition.png](/advanced-usage/manual-update/images-in-boot-partition.png)

 ### Tarjeta SD dentro de vuestra Recalbox encendida
 
 También podéis colocar los dos ficheros en vuestra Recalbox mientras está encendida. Para ello:
 
 - Montad la partición [boot-with-write-access](/tutorials/system/access/remount-partition-with-write-access).
 - Colocad los dos ficheros `recalbox-rpi4_64.img.xz` y `recalbox-rpi4_64.img.xz.sha1` dentro de la carpeta `/boot/update`.
 - Reiniciad.
 
## Lanzar la instalación

La instalación se lanza automáticamenete al reiniciar vuestra Recalbox. Para comprobar que la actualización se ha aplicado correctamente una vez terminado el proceso, basta con ir al menu de EmulationStation con el botón `START` y comprobar la versión inscrita en la parte de abajo de la ventana.
