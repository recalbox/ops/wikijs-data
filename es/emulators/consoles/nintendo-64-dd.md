---
title: Nintendo 64 DD
description: Nintendo 64 DD
published: true
date: 2024-07-26T07:56:10.011Z
tags: n64, nintendo, 64dd, n64dd, consolas
editor: markdown
dateCreated: 2023-09-03T20:00:33.149Z
---

![](/emulators/consoles/nintendo64dd.svg){.align-center}

## Detalles técnicos

* **Fabricante**: Nintendo
* **Año de lanzamiento**: 1999
* **Cantidades vendidas**: 15000
* **Juego más vendido**: Super Mario 64
* **Procesador**: NEC VR4300 de 64 bits a 93,75 MHz
* **Coprocesador**: MIPS RISC "Reality Immersion" RCP de 64 bits a 62,5 MHz
* **Procesador 64DD**: coprocesador de 32 bits para leer y transferir datos desde discos
* **RAM**: 4 MB RAMBUS RDRAM (ampliable a 8 MB)
* **Chip de sonido**: Estéreo 16-Bit y 48 kHz

## Presentación

El **Nintendo 64DD** (Disk Drive) es un periférico para la consola Nintendo 64 lanzado en Japón el 1 de diciembre de 1999.

El 64DD se conecta a la Nintendo 64 a través del puerto de expansión situado en la parte inferior de la consola. Permite a Nintendo 64 leer discos magnéticos.   
También se utilizaba un módem para conectarse a la red Randnet, que ofrecía diversas opciones, como juegos en línea, un servicio de mensajería y navegación por Internet.

El 64DD permite comunicar dos juegos, uno en la Nintendo 64 en formato cartucho y otro en el 64DD en formato disco, lo que hace posible dar cabida a proyectos más ambiciosos o expansiones.

## Emulador

[Libretro Mupen64plus_Next](libretro-mupen64plus_next)
[Libretro ParaLLEl_n64](libretro-parallel_n64)

