---
title: Libretro Mesen_S
description: 
published: true
date: 2024-07-24T23:19:13.184Z
tags: libretro, color, gameboy, mesen, gbc
editor: markdown
dateCreated: 2021-07-14T06:36:56.063Z
---

**Libretro Mesen-S** is a cross-platform SNES emulator for Windows and Linux built in C++ and C#.

## ![](/emulators/license.svg) License

This core is under [**GPLv3**](https://github.com/SourMesen/Mesen-S/blob/master/README.md) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ❌ | ❌ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Features

| Feature | Supported |
| :---: | :---: |
| Restart | ✔ |
| Saves | ✔ |
| States | ✔ |
| Rewind | ✔ |
| Netplay | ✔ |
| Core Options | ✔ |
| RetroAchievements | ✔ |
| RetroArch Cheats | ✔ |
| Native Cheats | ✔ |
| Controls | ✔ |
| Remapping | ✔ |
| Multi-Mouse | ✔ |
| Softpatching | ✔ |
| Crop Overscan | ✔ |

## ![](/emulators/bios.svg) BIOS

### List of optional bios

| Filename | Description | MD5 | Provided |
| :---: | :---: | :---: | :---: |
| cgb_boot.bin | Game Boy Color BIOS | b560efd759d87634a03b243f22bba27a | ✅ |
| sgb_bios.bin | Super Game Boy BIOS | d574d4f9c12f305074798f54c091a8b4 | ❌ |
| sgb2_boot.bin | Super Game Boy 2 BIOS | b4331a9e612b4738867a30af9c96df52 | ✅ |

### Location

Put your bios like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 🗒 **cgb_boot.bin**
┃ ┃ ┃ ┣ 🗒 **sgb_bios.bin**
┃ ┃ ┃ ┣ 🗒 **sgb2_boot.bin**

## ![](/emulators/roms.png) Roms

### Supported extensions

Roms must have the extension:

* .gbc
* .sgb
* .zip
* .7z

This system supports compressed roms in .zip/.7z format. However, be careful, it is only an archive.

The files contained in the .zip/.7z must correspond to the extensions mentioned above.
Moreover, each .zip/.7z file must contain only one compressed rom.

### Location

Put your roms like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 gbc
┃ ┃ ┃ ┃ ┣ 🗒 **game.zip**

>The roms using **No-Intro** format are highly recommended.
{.is-success}

>For more information about roms, go to [this tutorial](./../../../tutorials/games/generalities/isos-and-roms)!
{.is-info}

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Accessing to options

You can configure various options in two different ways.

* Via RetroArch menu:

┣ 📁 RetroArch Menu
┃ ┣ 📁 Core options
┃ ┃ ┣ 🧩 Name_option

* Via the file `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core options

| Options | Options values | Variables (for configuration override) | Variables values |
| --- | --- | --- | --- |
| NTSC Filter | `Disabled` ✅ / `Composite (Blargg)` / `S-Video (Blargg)` / `RGB (Blargg)` / `Monochrome (Blargg)` | `mesen-s_ntsc_filter` | `Disabled` / `Composite (Blargg)` / `S-Video (Blargg)` / `RGB (Blargg)` / `Monochrome (Blargg)` |
| Region | `Auto` ✅ / `NTSC` / `PAL` | `mesen-s_region` | `Auto` / `NTSC` / `PAL` |
| Game Boy Model | `Auto` ✅ / `Game Boy` / `Game Boy Color` / `Super Game Boy` | `mesen-s_gbmodel` | `Auto` / `Game Boy` / `Game Boy Color` / `Super Game Boy` |
| Use SGB2 | `Enabled` ✅ / `Disabled` | `mesen-s_sgb2` | `enabled` / `disabled` |
| Vertical Overscan | `None` ✅ / `8px` / `16px` | `mesen-s_overscan_vertical` | `None` / `8px` / `16px` |
| Horizontal Overscan | `None` ✅ / `8px` / `16px` | `mesen-s_overscan_horizontal` | `None` / `8px` / `16px` |
| Aspect Ratio | `Auto` ✅ / `No Stretching` / `NTSC` / `PAL` / `4:3` / `16:9` | `mesen-s_aspect_ratio` | `Auto` / `No Stretching` / `NTSC` / `PAL` / `4:3` / `16:9` |
| Blend Hi-Res Modes | `Disabled` ✅ / `Enabled` | `mesen-s_blend_high_res` | `disabled` / `enabled` |
| Cubic Interpolation (Audio) | `Disabled` ✅ / `Enabled` | `mesen-s_cubic_interpolation` | `disabled` / `enabled` |
| Overclock | `None` ✅ / `Low` / `Medium` / `High` / `Very High` | `mesen-s_overclock` | `None` / `Low` / `Medium` / `High` / `Very High` |
| Overclock Type | `Before NMI` ✅ / `After NMI` | `mesen-s_overclock_type` | `Before NMI` / `After NMI` |
| Super FX Clock Speed | `100%` ✅ / `200%` / `300%` / `400%` / `500%` / `1000%` | `mesen-s_superfx_overclock` | `100%` / `200%` / `300%` / `400%` / `500%` / `1000%` |
| Default power-on state for RAM | `Random Values (Default)` ✅ / `All 0s` / `All 1s` | `mesen-s_ramstate` | `Random Values (Default)` / `All 0s` / `All 1s` |

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/libretro/Mesen-S/](https://github.com/libretro/Mesen-S/)
* **Libretro documentation**: [https://docs.libretro.com/library/mesen-s/](https://docs.libretro.com/library/mesen-s/)