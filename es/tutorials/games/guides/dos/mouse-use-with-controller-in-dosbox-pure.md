---
title: Utilización del ratón con el mando de juegos en DOSBox Pure
description: 
published: true
date: 2023-09-21T20:09:45.207Z
tags: dosbox pure, 7.2+, ratón
editor: markdown
dateCreated: 2023-09-21T20:09:45.207Z
---

## Introducción

Con el core Libretro DOSBox Pure, podéis activar el control del ratón a través de uno de los joysticks de vuestro mando.

## Utilización

Para utilizar el ratón en un joystick del mando, hay que realizar una pequeña manipulación en las opciones de RetroArch. A modo de ejemplo os explicaremos cómo utilizar el ratón con el joystick derecho.

* Lanzad vuestro juego
* Una vez lanzado, id dentro de las opciones de RetroArch y bajad hasta **Keys**.
* Dentro de la configuración de las teclas, entrad dentro de **Port 1 Keys**.
* Seleccionad la opción **Type of peripheral** y elegid `Mouse with Right Analog Stick`.
* Volved al juego y disfrutad ! Podréis simular los clicks del ratón con los botones del mando `L1` y `R1` (click izquierdo y derecho respectivamente).