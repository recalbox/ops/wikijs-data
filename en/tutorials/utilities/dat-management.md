---
title: Dat management
description: 
published: true
date: 2021-08-06T23:54:08.361Z
tags: dat, management
editor: markdown
dateCreated: 2021-08-06T23:25:29.073Z
---

You will be able to create dats files from existing dat files in order to create your own dats files with your own settings such as clone removal.

Here are the available tutorials:

[DatUtil](datutil)