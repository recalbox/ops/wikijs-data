---
title: Convertir una grabación .GME en .SRM
description: 
published: true
date: 2024-07-26T07:05:09.345Z
tags: ps1, gme, srm, convertir, grabación
editor: markdown
dateCreated: 2023-09-21T10:45:42.313Z
---

Es posible utilizar las grabaciones en formato Dexdrive (`.GME`) y convertirlas a formato `.SRM` de RetroArch.

* Descargad el software [MemcardRex v1.8](http://www.mediafire.com/download/7ytiync7oxuhw4m/MemcardRex%201.8.zip) y descomprímalo.
* Pulsad en el ejecutable MemcardRex.exe.
* En los menús, pulsad `File` y luego `Open`. Seleccionad el fichero de grabación `.gme` que queréis convertir.
* Elijid `File` y pulsad `Save as...`. Poned el nombre de vuestro juego. Por ejemplo **juego.mcr**.
* A continuación cambiad el nombre del fichero **gamename.mcr** a **gamename.srm**.
* Por último, colocad el juego recién creado en el directorio `/recalbox/share/saves/psx/`.