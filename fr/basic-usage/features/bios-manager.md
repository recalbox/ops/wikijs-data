---
title: Vérification des Bios
description: Le nouvel analyseur de BIOS depuis votre Recalbox.
published: true
date: 2023-07-15T08:44:28.670Z
tags: bios, gestionnaire, vérification
editor: markdown
dateCreated: 2021-05-21T07:53:02.727Z
---

## Présentation

Les **bios** sont un problème pour beaucoup d'utilisateurs, et nous le savons bien. Actuellement, le seul moyen de les vérifier était le _checker_ disponible dans le WebManager. Pratique, mais ce dernier n'est aujourd'hui plus suffisant.

En attendant le nouveau Gestionnaire web, nous avons inclus un **vérificateur de Bios** dans EmulationStation. Et disons le franchement : _**c'est le jour et la nuit!**_

Ce nouveau checker va vous dire si les bios sont obligatoires, s'ils ne le sont pas, si les signatures MD5 doivent obligatoirement correspondre ou pas, il va vous dire aussi à quoi servent certains bios.

Et ce n'est pas fini ! Il supporte également de multiples signatures MD5 différentes, et surtout : _**il va faire 90% du travail pour vous et vous donner un état des lieux complet et précis.**_

![](/basic-usage/features/biosmanager1.png)

## Utilisation

* Dans la liste des systèmes, appuyez sur le bouton `START`.
* Allez dans `VÉRIFICATION DES BIOS`.

Vous arrivez directement dans la fenêtre avec tous les bios listés.

## Informations dans le vérificateur de bios

### Sur la partie de gauche

Sur la gauche, vous trouverez les informations suivantes :

* **Système** : le système avec des bios.
* **Nom du bios** : chaque bios est listé dans son système.

### Sur la partie de droite

* `SYSTÈME` : le système associé au bios sélectionné.
* `CORE` : le(s) core(s) auquel le bios est demandé.
* `CHEMIN` : le chemin où placer le bios avec son nom de fichier exact, extension inclus.
* `OBLIGATOIRE` : le bios est obligatoire pour utiliser le core associé ?
* `MD5 CONTRÔLÉ` : la signature MD5 a pu être contrôlé ?
* `BIOS TROUVÉ ?` : le bios a été trouvé ?
* `MD5 OK ?` : la signature MD5 est correcte ?

### Résultat global

Vous verrez le résultat global par bios avec la couleur du pouce.

* **Vert** : tout est OK.
* **Jaune** : le bios est présent mais sa signature MD5 peut ne pas être bonne ou le bios est manquant mais pas requis.
* **Rouge** : bios requis manquant.

## Recherche de bios

Pour les bios qui vous seront nécessaires, dans le répertoire `/recalbox/share/bios/`, vous aurez un fichier nommé `missing_bios_report.txt` listant les noms des bios manquants et leurs potentiels signatures MD5 valides.