---
title: Doom
description: 
published: true
date: 2021-08-08T23:44:04.327Z
tags: ports, doom
editor: markdown
dateCreated: 2021-06-29T18:41:18.752Z
---

![](/emulators/ports/doom.svg){.align-center}

## Technical data

* **Created by**: id Software
* **Year of release**: 1993
* **Genre**: First person shooter
* **Engine**: Doom Engine
* **Latest version**: The Ultimate DOOM (DOOM. WAD v1.9)
* **Operating system**: MS-DOS 5.0 or higher
* **CPU**: 386/33 minimum
* **Storage**: 20 MB space disk
* **RAM**: 4 MB RAM minimum; 8 MB recommended
* **Resolution**: 320x200 pixels
* **Sound**: Sound Blaster and AdLib or 100% compatible sound card
* **Chief Executive Officer :** Jay Wilbur
* **Software Engineers**: John Carmack, John Romero, Dave Taylor
* **Artists**: Adrian Carmack, Kevin Cloud
* **Designer**: Sandy Petersen
* **Test and technical support**: Shawn Green, Maurice Hale
* **PEGI**: 15

## Presentation

### Synopsis

![](/emulators/ports/the_ultimate_doom_library_hero.jpg)

_" **The Ultimate Doom** takes you far beyond any known territory. The Ultimate Doom includes all three original installments of the most fantastic, disturbing, and intense game ever created: 27 doomsday levels to challenge even the most hardened gamer. And that's just the beginning... Then, and only if you're persistent enough, you'll be able to take on the challenge of a brand new episode... **Thy Flesh Consumed**. The satanic geniuses at **ID Software** have dug deep into their evil souls to bring you this unique experience: 9 brand new levels of the most twisted, unforgiving and tortured action ever. "_ - Back of the game box - 

### Main versions of Doom

In order to rough out all the names we can hear, a little recap:

* **Doom**, released in 1993, by Id Software, has 3 episodes of 9 levels each (_Knee-Deep in the Dead_, _The Shores of Hell_ and _Inferno_)
* **Doom1** is the freely downloadable shareware version, which includes only the first episode.
* **Doom II: Hell on Earth**, released in 1994, sequel of Doom, with 30 levels (+2 hidden) in a row.
* **The Ultimate Doom**, released in 1995, = Doom + a 4th episode of 9 levels (_Thy Flesh Consumed_)
* **Master Levels for Doom II**, released in 1995, adding 21 levels for Doom II.
* **Final Doom**, released in 1996, expansion for Doom II with two separate episodes:
  * **TNT Evilution** (32 levels)
  * **The Plutonia Experiment** (32 levels)
* **Sigil**, released in 2019, is a 5th episode of Doom, created by John Romero for the 25th anniversary of Doom ... and of course, a lot of additional levels for Doom or Doom II !

## Emulators

[Libretro PrBoom](libretro-prboom)