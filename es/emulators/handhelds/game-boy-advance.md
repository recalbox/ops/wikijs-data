---
title: Game Boy Advance
description: Nintendo Game Boy Advance
published: true
date: 2024-07-26T08:32:39.768Z
tags: nintendo, gameboy, advance, gba, portátiles
editor: markdown
dateCreated: 2023-09-04T08:06:20.257Z
---

![](/emulators/handheld/gameboyadvance.svg){.align-center}

## Detalles técnicos

* **Fabricante**: Nintendo
* **Año de lanzamiento**: 2001
* **Cantidad vendida**: 82 millones
* **Juego más vendido**: Pokémon Rubí
* **Procesador**: CPU RISC de 16 MHz y 32 bits + CPU CISC de 8 bits
* **RAM**: 32KB WRAM + 256KB WRAM
* **Pantalla** : TFT color reflectante LCD
* **Resolución**: 240 x 160 píxeles
* **Memoria de vídeo**: 256 KB
* **Capacidad de visualización**: 32.000 colores

## Presentación

La **Game Boy Advance** es una videoconsola portátil creada por Nintendo en 2001. Tiene una pantalla en color y un procesador de 32 bits, lo que la convirtió en la consola portátil más potente cuando salió al mercado.

En 2003, Nintendo lanzó la Game Boy Advance SP (SP por SPecial), técnicamente idéntica a la Game Boy Advance, pero con un formato más compacto y mejoras como una pantalla retroiluminada y una batería recargable de iones de litio. Por último, en 2005, Nintendo lanzó la Game Boy Micro, la última versión de su portátil, una consola en formato "mini" con una pantalla mejorada.

El informe anual de Nintendo de 2006 afirma que se vendieron 75 millones de Game Boy Advances (todas las versiones combinadas). El 12 de febrero de 2008 salió a la venta en Estados Unidos el último juego de la consola (Samurai Deeper Kyo). Por tanto, la consola habrá vivido casi 7 años. Las ventas finales se estiman en 81,51 millones, lo que convierte a la consola en otro éxito tras los de Game Boy y Game Boy Color.

Descendiente de Game Boy Color (a su vez descendiente de Game Boy), ofrecía un rendimiento ligeramente superior al de las consolas domésticas de 16 bits (Mega Drive y Super Nintendo).

Nintendo quería que su consola tuviera un aspecto original y llamativo y no estaba satisfecha con los prototipos propuestos por los diseñadores tradicionales de la compañía. En su afán por renovar su imagen, Nintendo se puso en contacto con **Gwénaël Nicolas**, un diseñador francés que visitaba Japón y que era un completo desconocido en el mundo de los videojuegos. Se le ocurrieron docenas de prototipos totalmente fuera de lo común. Al final, fue el prototipo "panda" el elegido, y por eso la vista frontal de la consola debe sugerir la cabeza de este animal.

Su compatibilidad con los juegos de la primera versión y de la versión Color le permite disponer de un enorme catálogo de juegos (lo que no ocurre con la Game Boy Micro, la Nintendo DS y la Nintendo DS Lite). Como la GBA no está dividida en zonas, es posible jugar a juegos americanos y japoneses en una consola europea. Además, el bajo coste de desarrollo de juegos para consolas portátiles (en comparación con los juegos para consolas domésticas de la misma época) hizo que muchas producciones se lanzaran en Game Boy Advance.

La posibilidad de jugar hasta cuatro jugadores, a veces con un solo cartucho, conectando Game Boy Advance entre sí es otro de sus puntos fuertes.

Su gran punto débil, aunque existen accesorios para compensarlo, es la falta de retroiluminación, lo que hace que la pantalla sea muy oscura. Esta es una de las razones por las que Nintendo decidió lanzar la Game Boy Advance SP, un modelo con una pantalla iluminada por LEDs en sus laterales.

Otro punto débil es el sonido de la consola. De hecho, no tiene ningún circuito integrado dedicado al sonido, lo que obliga a los juegos a dedicarle parte de la potencia del procesador central.

## Emuladores

[Libretro Meteor](libretro-meteor)
[Libretro gpSP](libretro-gpsp)
[Libretro mGBA](libretro-mgba)