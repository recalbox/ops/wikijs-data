---
title: Pas de son sur la sortie casque avec mupen64plus
description: 
published: true
date: 2021-09-08T12:50:25.781Z
tags: n64, nintendo, 64, son, casque
editor: markdown
dateCreated: 2021-05-21T08:47:24.598Z
---

Si vous utilisez la prise casque de votre Raspberry Pi comme sortie son et que le son n'est pas disponible avec l'émulateur N64 `mupen64plus`, veuillez suivre ce qui suit.

* Connectez-vous via [SSH](./../../../system/access/root-access-terminal-cli) 
* Modifiez le fichier de configuration de `mupen64plus` avec la commande suivante :

`nano /recalbox/share/system/configs/mupen64/mupen64plus.cfg`

* Trouvez la ligne suivante :

```text
OUTPUT_PORT = 1
```

**Modifiez cette ligne par ceci :**

```text
OUTPUT_PORT = 0
```

* Vous n'avez alors plus qu'à enregistrer le fichier avec `CTRL + X` puis `Y` pour confirmer, et validez avec `Entrée`.
* Redémarrez votre Raspberry Pi avec la commande `reboot`.

Une fois redémarré, le son devrait être disponible.

>Cette modification peut également être faite à partir de [WinSCP](./../../../system/access/network-access-winscp), de [Cyberduck](./../../../system/access/network-access-cyberduck) ou de [MobaXTerm](./../../../system/access/network-access-mobaxterm) si vous êtes allergique au terminal.
{.is-info}