---
title: Scraper Interno
description: 
published: true
date: 2024-11-11T05:37:42.586Z
tags: 
editor: markdown
dateCreated: 2021-08-11T23:56:32.654Z
---

## Introdução

Desta vez, não enrolamos: refizemos completamente o scraper interno, começando do zero. Nada resta do antigo, nem mesmo uma migalha.

O resultado é um scraper totalmente novo, muito mais limpo, mais rápido, com muitas opções e _ **que aproveita ao máximo sua conta ScreenScraper**_ (número de motores de busca, cota, ...).

![](/basic-usage/features/internalscraper1.png)

## Como Usar

* Na lista de sistemas, pressione o botão `START` e vá para`SCRAPER`.
* Aqui você terá várias opções de scrape:
  * `ORIGEM DO SCRAPE`: permite que você escolha a fonte de seu scrape.
  * `OPÇÕES DO SCRAPER`: permite que você escolha todas as opções de scraper:
    * `SELECIONE O TIPO DE IMAGEM`: permite escolher o visual a ser recuperado para os jogos.
    * `SELECIONE O TIPO DE VÍDEO`: permite que você escolha se deseja recuperar os vídeos e que tipo de vídeo (vídeos otimizados/uniformes ocuparão menos espaço).
    * `SELECIONE O TIPO DE MINIATURA`: permite que você escolha se deseja recuperar as imagens.
    * `SELECIONE A PRIORIDADE DA REGIÃO`: permite que você escolha a região a ser usada: região preferida ou detectada.
    * `SELECIONE A REGIÃO PREFERIDA`: permite que você escolha sua região preferida.
    * `SELECIONE O IDIOMA PREFERIDO`: permite que você escolha o seu idioma preferido para os textos dos scrapes.
    * `BAIXAR MANUAIS DOS JOGOS`: permite que você baixe os manuais originais de cada jogo.
    * `BAIXAR MAPAS DOS JOGOS`: permite que você baixe os mapas originais de cada jogo.
    * `BAIXAR CONFIGURAÇÕES DE PAD-2-KEYBOARD`: permite baixar as configurações dos controles para o teclado, muito útil para emular computadores antigos.
    * `USUÁRIO`: permite que você indique seu usuário do ScreenScraper.
    * `SENHA`: permite que você insira sua senha do ScreenScraper.
  * `OBTER O NOME DO JOGO DO`: permite que você escolha se deseja que o scrape mantenha o nome das roms ou isos.
  * `FILTRO`: permite que você escolha se apenas faça scrape das imagens ausentes ou todos os jogos de um sistema.
  * `SISTEMAS`: permite que você escolha qual sistema faça srape.
* Depois de fazer todas as suas escolhas, clique no botão `FAZER SCRAPE AGORA` para iniciar o scrape.

Dependendo das opções, da quantidade de jogo, do número de motores de busca associados à sua conta ScreenScraper, bem como do número de scrapes permitidos por dia, o scraper pode levar alguns minutos ou algumas horas.