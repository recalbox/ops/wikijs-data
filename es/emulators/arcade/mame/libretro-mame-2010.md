---
title: Libretro MAME 2010​
description: 
published: true
date: 2024-06-29T17:36:01.642Z
tags: libretro, mame, mame2010
editor: markdown
dateCreated: 2023-09-05T12:55:13.114Z
---



## ![](/emulators/license.svg) Licencia

El core posee la licencia **MAME no-commercial**

## ![](/emulators/compatibility.png) Compatibilidad

 | RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
 | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
 | ❌ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Funcionalidades

| Funcionalidad | ¿Soportada? |
| :---: | :---: |
| Reinicio | ✔ |
| Capturas de pantalla | ✔ |
| Opciones del core | ✔ |

## ![](/emulators/bios.svg) BIOS

Las romsets de BIOS no son necesarias si utilizáis las romsets de arcade "Full Non-Merged". Para las romset de tipo "Split" y "Non-Merged", disponed las BIOS necesarias en la misma carpeta que la romset del juego.

>**Aviso** :
>Poned las bios dentro del directorio : `/recalbox/share/roms/mame/`   
>o dentro de una subcarpeta si utilizáis un directorio diferente por core Mame, por ejemplo: `/recalbox/share/roms/mame/Mame2010/`
Los juegos junto con sus bios deben encontrarse dentro del mismo directorio o subdirectorio.
{.is-warning}

## ![](/emulators/roms.png) Roms

* Basado en el romset: MAME 0.139 (Août 2010)
* Fichero dat : el fichero dat se encuentra en vuestra Recalbox dentro del directorio `/recalbox/share/arcade/libretro/mame2010.dat`.

### Emplazamiento

Disponed vuestras roms como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 mame
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.zip**
┃ ┃ ┃ ┃ ┣ 📁 **jeu**
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **jeu.chd**

También podéis utilizar el subdirectorio mame2010, si queréis clasificar vuestros fullsets por el core que las hace funcionar. Si utilizáis este subdirectorio vuestros juegos se lanzarán automáticamente bajo el emulador mame2010.


┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 mame
┃ ┃ ┃ ┃ ┣ 📁 mame2010
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **jeu.zip**
┃ ┃ ┃ ┃ ┃ ┣ 📁 **jeu**
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 **jeu.chd**

## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

>Para conservar las configuraciones personalizadas cuando se produce una actualización del sistema Recalbox, os aconsejamos utilizar las [Sobrecargas de configuración](./../../../advanced-usage/configuration-override).
{.is-info}

### Acceder a las opciones

Podéis configurar las opciones del core de dos formas diferentes.

* Via el Menú RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via el fichero `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Opciones del core

| Opción | Valor de la opción | Variable (para la sobrecarga) | Valores posibles |
| --- | --- | --- | --- |
| Mouse enabled | `Activé` ✅ / `Désactivé` | `mame_current_mouse_enabled` | `enabled` / `disabled` |
| Video approach 1 Enabled | `Désactivé` ✅ / `Activé` | `mame_current_videoappoach1_enable` | `disabled` / `enabled` |
| Hide nag screen | `Activé` ✅ / `Désactivé` | `mame_current_skip_nagscreen` | `enabled` / `disabled` |
| Hide game infos screen | `Désactivé` ✅ / `Activé` | `mame_current_skip_gameinfos` | `disabled` / `enabled` |
| Hide warning screen | `Désactivé` ✅ / `Activé` | `mame_current_skip_warnings` | `disabled` / `enabled` |
| Core provided aspect ratio | `DAR` ✅ / `PAR` | `mame_current_aspect_ratio` | `DAR` / `PAR` |
| Enable autofire | `Désactivé` ✅ / `button 1` / `button 2` / `R2 to button 1 mapping` / `R2 to button 2 mapping` | `mame_current_turbo_button` | `disabled` / `button 1` / `button 2` / `R2 to button 1 mapping` / `R2 to button 2 mapping` |
| Set autofire pulse speed | `medium` ✅ / `slow` / `fast` | `mame_current_turbo_delay` | `medium` / `slow` / `fast` |
| Set frameskip | `0` ✅ / `1` / `2` / `3` / `4` / `automatic` | `mame_current_frame_skip` | `0` / `1` / `2` / `3` / `4` / `automatic` |
| Set sample rate (Restart) | `48000Hz` ✅ / `44100Hz` / `32000Hz` / `22050Hz` | `mame_current_sample_rate` | `48000Hz` / `44100Hz` / `32000Hz` / `22050Hz` |
| Set brightness | `default` ✅ / `+1%` / `+2%` / `+3%` / `+4%` / `+5%` / `+6%` / `+7%` / `+8%` / `+9%` / `+10%` / `+11%` / `+12%` / `+13%` / `+14%` / `+15%` / `+16%` / `+17%` / `+18%` / `+19%` / `+20%` / `-20%` / `-19%` / `-18%` / `-17%` / `-16%` / `-15%` / `-14%` / `-13%` / `-12%` / `-11%` / `-10%` / `-9%` / `-8%` / `-7%` / `-6%` / `-5%` / `-4%` / `-3%` / `-2%` / `-1%` | `mame_current_adj_brightness` | `default` / `+1%` / `+2%` / `+3%` / `+4%` / `+5%` / `+6%` / `+7%` / `+8%` / `+9%` / `+10%` / `+11%` / `+12%` / `+13%` / `+14%` / `+15%` / `+16%` / `+17%` / `+18%` / `+19%` / `+20%` / `-20%` / `-19%` / `-18%` / `-17%` / `-16%` / `-15%` / `-14%` / `-13%` / `-12%` / `-11%` / `-10%` / `-9%` / `-8%` / `-7%` / `-6%` / `-5%` / `-4%` / `-3%` / `-2%` / `-1%` |
| Set contrast | `default` ✅ / `+1%` / `+2%` / `+3%` / `+4%` / `+5%` / `+6%` / `+7%` / `+8%` / `+9%` / `+10%` / `+11%` / `+12%` / `+13%` / `+14%` / `+15%` / `+16%` / `+17%` / `+18%` / `+19%` / `+20%` / `-20%` / `-19%` / `-18%` / `-17%` / `-16%` / `-15%` / `-14%` / `-13%` / `-12%` / `-11%` / `-10%` / `-9%` / `-8%` / `-7%` / `-6%` / `-5%` / `-4%` / `-3%` / `-2%` / `-1%` | `mame_current_adj_contrast` | `default` / `+1%` / `+2%` / `+3%` / `+4%` / `+5%` / `+6%` / `+7%` / `+8%` / `+9%` / `+10%` / `+11%` / `+12%` / `+13%` / `+14%` / `+15%` / `+16%` / `+17%` / `+18%` / `+19%` / `+20%` / `-20%` / `-19%` / `-18%` / `-17%` / `-16%` / `-15%` / `-14%` / `-13%` / `-12%` / `-11%` / `-10%` / `-9%` / `-8%` / `-7%` / `-6%` / `-5%` / `-4%` / `-3%` / `-2%` / `-1%` |
| Set gamme | `default` ✅ / `+1%` / `+2%` / `+3%` / `+4%` / `+5%` / `+6%` / `+7%` / `+8%` / `+9%` / `+10%` / `+11%` / `+12%` / `+13%` / `+14%` / `+15%` / `+16%` / `+17%` / `+18%` / `+19%` / `+20%` / `-20%` / `-19%` / `-18%` / `-17%` / `-16%` / `-15%` / `-14%` / `-13%` / `-12%` / `-11%` / `-10%` / `-9%` / `-8%` / `-7%` / `-6%` / `-5%` / `-4%` / `-3%` / `-2%` / `-1%` | `mame_current_adj_gamma` | `default` / `+1%` / `+2%` / `+3%` / `+4%` / `+5%` / `+6%` / `+7%` / `+8%` / `+9%` / `+10%` / `+11%` / `+12%` / `+13%` / `+14%` / `+15%` / `+16%` / `+17%` / `+18%` / `+19%` / `+20%` / `-20%` / `-19%` / `-18%` / `-17%` / `-16%` / `-15%` / `-14%` / `-13%` / `-12%` / `-11%` / `-10%` / `-9%` / `-8%` / `-7%` / `-6%` / `-5%` / `-4%` / `-3%` / `-2%` / `-1%` |
| Use external hiscore.dat | `Désactivé` ✅ / `Activé` | `mame-external_hiscore` | `disabled` / `enabled` |

## ![](/emulators/external-links.png) Enlaces externos

* **Progettosnaps** : [https://www.progettosnaps.net/](https://www.progettosnaps.net/)
* **Código fuente utilizado** : [https://github.com/libretro/mame2010-libretro/](https://github.com/libretro/mame2010-libretro)
* **Documención Libretro** : [https://docs.libretro.com/library/mame\_2010/](https://docs.libretro.com/library/mame_2010/)