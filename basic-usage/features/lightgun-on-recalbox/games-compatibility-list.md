---
title: Games compatibility list
description: 
published: true
date: 2025-02-23T14:11:08.934Z
tags: lightgun, games, 7.2+
editor: markdown
dateCreated: 2021-08-19T19:13:52.852Z
---

## Introduction

Here you can have the list of games recognized as lightgun by system.

This list contains all games currently recognized as using lightgun. Only games with ✅ compatibility are listed in the LightGun virtual system in EmulationStation. The games marked with ❌ do not work or perhaps badly and those marked with ❔ have not been tested for various reasons (only work partially and not at the beginning of the game, etc.).

The core compatibility list does not take into account whether it is a portable version or not, it just indicates whether the necessary emulator is included or not.

This list will be updated for each new version if necessary.

## Nintendo Entertainment System

### Emulator

The required emulator is `Libretro FCEUmm`.

### Compatibility

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

### Games

| Game name | Compatibility | Notes |
| :--- | :--- | :--- |
| 3 In 1 Supergun | ✅ | - |
| Adventures of Bayou Billy | ✅ | - |
| Asder - 20 In 1 | ✅ | - |
| Baby Boomer | ✅ | - |
| Barker Bill's Trick Shooting | ✅ | - |
| Chiller | ✅ | - |
| Cobra Mission | ✅ | - |
| Crime Busters | ✅ | - |
| Day Dreamin' Davey | ✅ | - |
| Duck Hunt | ✅ | - |
| Freedom Force | ✅ | - |
| Gotcha! The Sport! | ✅ | - |
| Gumshoe | ✅ | - |
| Hit Marmot | ✅ | - |
| Hogan's Alley | ✅ | - |
| Laser Invasion | ❌ | Crosshair doesn't move on game. |
| Lethal Weapon | ❌ | Seems not a lightgun game. |
| Liesha Zhuluoji - Blood of Jurassic | ✅ | - |
| Lightgun Games 2 in 1 - Cosmocop + Cyber Monster | ✅ | - |
| Lightgun Games 2 in 1 - Tough Cop + Super Tough Cop | ✅ | - |
| Lone Ranger, The | ✅ | Some parts are playable with lightgun, allothers via the controller. |
| Master Shooter | ✅ | - |
| Mechanized Attack | ✅ | - |
| Operation Wolf | ✅ | - |
| Russian Roulette | ✅ | Only to replicate the move to put the lightgun on the temple. |
| Shooting Range | ✅ | - |
| Space Shadow | ✅ | - |
| Strike Wolf | ✅ | - |
| Super Russian Roulette | ❌ | Failed to load content. |
| To The Earth | ✅ | - |
| Track & Field II | ✅ | Only for a mini-game difficult to unlock. |
| Wild Gunman | ✅ | - |

## Super Nintendo

### Emulator

The required emulator is `Libretro snes9x`.

### Compatibility

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

### Games

| Game name | Compatibility | Notes |
| :--- | :--- | :--- |
| Battle Clash | ✅ | - |
| Bazooka Blitzkrieg | ✅ | - |
| Lamborghini American Challenge | ✅ | - |
| Lethal Enforcers | ✅ | - |
| Metal Combat - Falcon's Revenge | ✅ | - |
| Nintendo Scope 6 | ✅ | - |
| Operation Thunderbolt | ✅ | - |
| Revolution X | ❌ | Crosshair shifted, no calibration in game, fire in on button L of controller. |
| Super Scope 6 | ✅ | - |
| T2 - The Arcade Game | ✅ | - |
| Tin Star | ✅ | - |
| Yoshi's Safari | ✅ | - |
| X Zone | ✅ | - |

## Megadrive

### Emulator

The required emulator is `Libretro GenesisPlusGX`.

### Compatibility

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

### Games

| Game name | Compatibility | Notes |
| :--- | :--- | :--- |
| Bodycount | ❌ | Impossibility to select Menacer in game. |
| Lethal Enforcers | ✅ | - |
| Lethal Enforcers II - Gun Fighters | ✅ | - |
| Menacer - 6-game Cartridge | ✅ | - |
| T2 - The Arcade Game | ✅ | Calibration is not done correctly but can be playable. |

## Mega-CD

### Emulator

The required emulator is `Libretro GenesisPlusGX`.

### Compatibility

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

### Games

| Game name | Compatibility | Notes |
| :--- | :--- | :--- |
| Corpse Killer | ✅ | - |
| Crime Patrol | ✅ | - |
| Ground Zero Texas | ❌ | - |
| Mad Dog McCree | ✅ | - |
| Mad Dog II: The Lost Gold | ✅ | - |
| Snatcher | ❌ | Some parts are playable with the lightgun but not the whole game. |
| Who Shot Johnny Rock ? | ✅ | - |

## Master System

### Emulator

The required emulator is `Libretro GenesisPlusGX`.

### Compatibility

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

### Games

| Game name | Compatibility | Notes |
| :--- | :--- | :--- |
| Assault City (Light Phaser) | ✅ | - |
| Bank Panic | ❌ | Shoot doesn't follow the crosshair. |
| Gangster Town | ✅ | - |
| Laser Ghost | ✅ | You need to press 1 or 2 to start the Light Phaser mode from the controller 2. |
| Marksman Shooting & Trap Shooting & Safari Hunt | ✅ | - |
| Missile Defense 3-D | ✅ | - |
| Operation Wolf | ✅ | - |
| Rambo III | ✅ | - |
| Rescue Mission | ✅ | - |
| Shooting Gallery ~ Shooting G. | ✅ | - |
| Space Gun | ✅ | - |
| Wanted | ✅ | - |

## Dreamcast

### Emulator

The required emulator is `Libretro Flycast`.

### Compatibility

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ❌ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

### Games

| Game name | Compatibility | Notes |
| :--- | :--- | :--- |
| Confidential Mission | ✅ | - |
| Death Crimson 2 | ✅ | - |
| Death Crimson OX | ✅ | - |
| House of the Dead 2 | ✅ | - |
| Virtua Cop 2 | ✅ | - |

## Playstation 1

### Emulator

The required emulator is `Libretro Mednafen_PSX`.

### Compatibility

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ❌ | ❌ | ✅ | ✅ | ❌ | ✅ | ✅ | ✅ |

### Games

| Game name | Compatibility | Notes |
| :--- | :--- | :--- |
| Area 51 | ✅ | - |
| Crypt Killer | ✅ | - |
| Die Hard Trilogy | ✅ | - |
| Die Hard Trilogy 2 | ✅ | - |
| Elemental Gearbolt | ✅ | - |
| Extreme Ghostbusters: The Ultimate Invasion | ✅ | You need to press - and + on the wiimote to start or pause the game (corresponds to button A and B on Gcon45) |
| Ghoul Panic | ✅ | - |
| Gumbare! Game Tengoku - The Game Paradise 2 | ✅ | - |
| Gunfighter: The Legend of Jesse James | ✅ | - |
| Guntu: Western Front June, 1944: Tetsu no Kioku | ✅ | - |
| Judge Dredd | ✅ | - |
| Lethal Enforcers I & II | ✅ | - |
| Maximum Force | ✅ | - |
| Mighty Hits Special | ✅ | - |
| Moorhen 3: Chicken Chase | ✅ | - |
| Moorhuhn 2: Die Jagd Geht Weiter | ✅ | - |
| Moorhuhn 3 - Chasse aux poulets !!! | ❌ | Dump seems non-existing, impossible to test. |
| Moorhuhn X | ✅ | - |
| Omega Assault | ❌ | Impossible to pass start screen. |
| Point Blank | ✅ | - |
| Point Blank 2 | ✅ | - |
| Point Blank 3 | ✅ | - |
| Project: Horned Owl | ❌ | - |
| Puffy no P.S. I Love You | ❌ | No command. |
| Rescue Shot | ❔ | - |
| Rescue Shot Bubibo | ❔ | - |
| Rescue Shot Bubibo & Biohazard: Gun Survivor: GunCon Taiou Shooting Taikenban | ❔ | - |
| Resident Evil: Survivor | ✅ | - |
| Revolution X | ❌ | No command. |
| Serofans | ❌ | Game doesn't start. |
| Simple 1500 Series Vol. 24: The Gun Shooting | ✅ | - |
| Simple 1500 Series Vol. 63: The Gun Shooting 2 | ✅ | - |
| Time Crisis | ✅ | - |
| Time Crisis: Project Titan | ✅ | - |

## Saturn

### Emulator

The required emulator is `Libretro Mednafen_Saturn`.

### Compatibility

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ❌ | ❌ | ❌ | ✅ | ❌ | ❌ | ✅ | ❌ |

### Games

| Game name | Compatibility | Notes |
| :--- | :--- | :--- |
| Area 51 | ✅ | - |
| Chaos Control | ✅ | - |
| Corpse Killer: Graveyard Edition | ❌ | No reaction on start screen. |
| Crypt Killer | ✅ | - |
| Death Crimson | ❌ | No fire in game. |
| Die Hard Trilogy | ❔ | - |
| House of the Dead | ✅ | - |
| Maximum Force | ✅ | - |
| Mechanical Violator Hakaider: Last Judgement | ❌ | - |
| Mighty Hits | ✅ | - |
| Policenauts | ❌ | Point & click |
| Revolution X | ❌ | Doesn't start. |
| Scud: The Disposable Assassin | ✅ | Impossible to calibrate. |
| Virtua Cop | ✅ | - |
| Virtua Cop 2 | ✅ | - |

## 3DO

### Emulator

The required emulator is `Libretro Opera`.

### Compatibility

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ❌ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

### Games

| Game name | Compatibility | Notes |
| :--- | :--- | :--- |
| Burning Soldier | ❌ | Crash during intro. |
| Corpse Killer | ✅ | - |
| Creature Shock | ❌ | No reaction on start screen. |
| Crime Patrol | ✅ | - |
| CyberDillo | ❌ | No reaction on start screen. |
| Demolition Man | ❌ | No reaction on start screen. |
| Drug Wars | ✅ | - |
| Killing Time | ❌ | Crash during intro. |
| Last Bounty Hunter, The | ✅ | - |
| Mad Dog McCree | ✅ | - |
| Mad Dog II: The Lost Gold | ✅ | - |
| Shootout at Old Tucson | ❌ | Game not found on start. |
| Space Pirates | ✅ | - |
| Who Shot Johnny Rock ? | ✅ | - |

## Atomiswave

### Emulator

The required emulator is `Libretro Flycast`.

### Compatibility

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ❌ | ❌ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

### Games

| Game name | Compatibility | Notes |
| :--- | :--- | :--- |
| Extreme Hunting | ✅ | - |
| Extreme Hunting 2 | ❌ | Impossible to reload. |
| Ranger Mission | ✅ | - |
| Sega Clay Challenge | ✅ | - |
| Sports Shooting USA | ✅ | - |

## Naomi

### Emulator

The required emulator is `Libretro Flycast`.

### Compatibility

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ❌ | ❌ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

### Games

| Game name | Compatibility | Notes |
| :--- | :--- | :--- |
| Death Crimson OX | ✅ | - |
| House of the Dead 2 | ✅ | - |
| Gun Survivor 2 Biohazard Code: Veronica | ❌ | No crosshair, no response from weapons. |
| House of the Dead 2, The | ✅ | - |
| Mazan: Flash of the Blade | ✅ | - |
| Ninja Assault | ❌ | No command is working. |

## Naomi GD

### Emulator

The required emulator is `Libretro Flycast`.

### Compatibility

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ❌ | ❌ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

### Games

| Game name | Compatibility | Notes |
| :--- | :--- | :--- |
| Confidential Mission | ✅ | - |
| Lupin The Third - The Shooting | ✅ | - |
| Maze of the King, The | ✅ | - |

## Mame

### Emulator

The required emulator is `Libretro MAME`.

### Compatibility

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ❌ | ❌ | ✅ | ✅ | ❌ | ❌ | ✅ | ❌ |

### Games

| Game name | Compatibility | Notes |
| :--- | :--- | :--- |
| Alien3: The Gun | ✅ | - |
| Area 51 (R3000) | ✅ | - |
| Area 51 / Maximum Force Duo v2.0 | ✅ | - |
| Area51: Site 4 | ✅ | - |
| Bang! | ✅ | Add credits from the controller. Crash if credits are added via wiimote. |
| Bank Panic | ❌ | Pas de viseur. |
| Beast Busters | ✅ | - |
| Beast Busters: Second Nightmare | ✅ | - |
| Born to Fight | ✅ | - |
| Bubble Trouble | ✅ | - |
| CarnEvil | ❌ | Ne démarre pas. |
| Catch-22 | ✅ | - |
| Cheyenne | ✅ | - |
| Chiller | ✅ | - |
| Clay Pigeon | ✅ | - |
| Claybuster | ✅ | - |
| Combat Hawk | ❔ | - |
| Combat School | ❔ | - |
| Crackshot (version 2.0) | ✅ | - |
| Critter Crusher | ✅ | - |
| Crossbow | ✅ | - |
| Crypt Killer | ✅ | - |
| Cycle Shooting | ❌ | Pressing Start restarts the whole game. |
| Deer Hunting USA | ✅ | - |
| Desert Gun | ✅ | - |
| Dragon Gun | ✅ | - |
| Egg Venture | ✅ | - |
| Egg Venture Deluxe | ✅ | - |
| Freedom Force | ❔ | - |
| Ghost Hunter | ✅ | - |
| Golgo 13 (Japan, GLG1/VER.A) | ✅ | - |
| Golgo 13 Kiseki no Dandou | ✅ | - |
| Golly! Ghost! | ✅ | - |
| Great Guns | ✅ | - |
| Gun Bullet | ✅ | - |
| Gun Gabacho | ✅ | - |
| Gunbuster | ✅ | - |
| Hit'n Miss | ✅ | - |
| Hit'n Miss 2 | ✅ | - |
| Invasion - The Abductors | ✅ | - |
| Jurassic Park | ✅ | - |
| Jurassic Park III | ❌ | - |
| Laser Ghost | ❌ | Can't pass calibration screen. |
| Lethal Enforcers | ✅ | - |
| Lethal Enforcers II: Gun Fighters | ✅ | - |
| Lethal Justice | ✅ | - |
| Line of Fire / Bakudan Yarou | ❌ | Can't pass calibration screen. |
| Lord of Gun | ❌ | No reload. |
| Mallet Madnes | ✅ | - |
| Maximum Force | ✅ | - |
| Mechanized Attack | ✅ | - |
| Mobile Suit Gundam Final Shooting | ✅ | - |
| Night Stocker | ✅ | - |
| N.Y. Captor | ✅ | - |
| Oh! Bakyuuun | ✅ | Add credits from the controller. Crash if credits are added via wiimote. |
| One Shot One Kill | ✅ | - |
| Operation Thunderbolt | ✅ | - |
| Operation Wolf | ✅ | - |
| Operation Wolf 3 | ❌ | Doesn't start. |
| Point Blank | ✅ | - |
| Point Blank 2 | ✅ | - |
| Point Blank 3 | ✅ | - |
| Police Trainer | ✅ | - |
| Police Trainer 2 | ❌ | "No screens attached to the system". |
| Rail Chase | ✅ | - |
| Rapid Fire | ✅ | - |
| Revolution X | ❌ | Doesn't use 100% of the screen for the crosshair. |
| Sharpshooter | ✅ | - |
| Shooting Gallery | ✅ | - |
| Space Gun | ✅ | - |
| Steel Gunner | ✅ | - |
| Steel Gunner 2 | ✅ | - |
| Target Hits | ✅ | - |
| Terminator 2 - Judgment Day | ❌ | Doesn't use 100% of the screen for the crosshair. |
| Tickee Tickats | ✅ | - |
| Time Crisis | ✅ | - |
| Time Crisis II | ✅ | - |
| Time Crisis 3 | ❌ | No reaction. |
| Time Crisis 4 | ❌ | No reaction. |
| Trophy Hunting - Bear & Moose | ❔ | - |
| Tut's Tomb | ✅ | - |
| Under Fire | ✅ | - |
| Virtua Cop | ✅ | - |
| Virtua Cop 2 | ✅ | - |
| Virtua Cop 3 | ❌ | Doesn't start. |
| Vs. Duck Hunt | ✅ | - |
| Vs. Freedom Force | ✅ | Add credits from the controller. Crash if credits are added via wiimote. |
| Vs. Hogan's Alley | ✅ | - |
| Who Dunit | ✅ | - |
| Wild Gunman | ❌ | - |
| Wild Pilot | ✅ | - |
| Wing Shooting Championship | ❔ | - |
| Zero Point | ✅ | - |
| Zero Point 2 | ✅ | - |
| Zombie Raid | ✅ | - |

## Mame 2003-Plus

### Emulator

The required emulator is `Libretro MAME 2003 Plus`.

### Compatibility

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

### Games

| Game name | Compatibility | Notes |
| :--- | :--- | :--- |
| Alien 3: The Gun | ✅ | Add credits from the controller. Crash if credits are added via wiimote. |
| Area 51 | ✅ | Add credits from the controller. Crash if credits are added via wiimote. |
| Area 51 / Maximum Force Duo | ✅ | Add credits from the controller. Crash if credits are added via wiimote. |
| Bang! | ✅ | Add credits from the controller. Crash if credits are added via wiimote. |
| Beast Busters | ✅ | - |
| Born To Fight | ✅ | - |
| Bubble Trouble - Golly Ghost 2 | ❔ | - |
| Cheyenne | ✅ | - |
| Chiller | ✅ | - |
| Clay Pigeon | ✅ | - |
| Crossbow | ✅ | - |
| Crypt Killer | ✅ | - |
| Deer Hunting USA | ✅ | Bad calibration. |
| Dragon Gun | ✅ | - |
| Egg Venture | ✅ | - |
| Egg Venture Venture | ✅ | - |
| Ghost Hunter | ❔ | - |
| Golgo 13 | ❌ | Softlocked after the warning. |
| Golgo 13 Kiseki no Dandou | ❔ | - |
| Golly! Ghost! | ✅ | - |
| Great Guns | ❌ | Not playable. |
| Gunbuster | ✅ | - |
| Hit'n Miss | ✅ | - |
| Hit'n Miss 2 | ✅ | - |
| Invasion - The Abductors | ❔ | - |
| Jurassic Park | ✅ | - |
| Lethal Enforcers | ✅ | - |
| Lethal Enforcers II: Gun Fighters | ✅ | - |
| Lethal Justice | ✅ | - |
| Mallet Madnes | ❔ | - |
| Maximum Force | ✅ | - |
| N.Y. Captor | ✅ | - |
| Oh! Bakyuuun | ❔ | - |
| One Shot One Kill | ✅ | - |
| Operation Wolf | ✅ | - |
| Operation Wolf 3 | ❌ | No crosshair or fire possible. |
| Point Blank | ✅ | - |
| Rail Chase | ❔ | - |
| Rapid Fire v1.1 | ❔ | - |
| Steel Gunner | ✅ | - |
| Steel Gunner 2 | ✅ | - |
| Target Hits | ❌ | Crosshair doesn't follow wiimote and stays in center. |
| Tickee Tickats | ✅ | - |
| Time Crisis | ❌ | Bad initialisation. |
| Trophy Hunting - Bear & Moose v1.0 | ✅ | - |
| Tut's Tomb | ❔ | - |
| Under Fire | ✅ | - |
| Virtua Cop 2 | ❔ | - |
| Vs. Duck Hunt | ✅ | - |
| Vs. Freedom | ✅ | - |
| Vs. Hogan's Alley | ✅ | - |
| Who Dunit | ✅ | - |
| Wild Pilot | ❔ | - |
| Wing Shooting Championship v2.00 | ✅ | - |
| Zero Point | ✅ | - |
| Zero Point 2 | ✅ | - |
| Zombie Raid | ✅ | - |

## FinalBurn Neo

### Emulator

The required emulator is `Libretro FBNeo`.

### Compatibility

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

### Games

| Game name | Compatibility | Notes |
| :--- | :--- | :--- |
| Alien3: The Gun | ✅ | - |
| Area 51 (R-3000) | ❔ | - |
| Area 51 / Maximum Force Duo v2.0 | ❔ | - |
| Bang! | ❌ | Adding credits quits the game. |
| Bank Panic | ❌ | Adding credits quits the game. |
| Bubble Trouble - Golly! Ghost! 2 | ❌ | Parent doesn't work. |
| Beast Busters | ❔ | - |
| Born to Fight | ❔ | - |
| Cheyenne | ✅ | - |
| Chiller | ✅ | - |
| Clay Pigeon | ✅ | - |
| Crossbow | ✅ | - |
| Crypt Killer | ❔ | - |
| Deer Hunting USA | ✅ | - |
| Dragon Gun | ✅ | - |
| Egg Venture | ❔ | - |
| Ghost Hunter | ❔ | - |
| Golgo 13 | ❔ | - |
| Golgo 13 Kiseki no Dandou | ❔ | - |
| Golly! Ghost! | ❌ | Adding credits quits the game. |
| Great Guns | ❔ | - |
| Gunbuster | ❔ | - |
| Hit'n Miss 1 | ✅ | - |
| Hit'n Miss 2 | ✅ | - |
| Invasion - The Abductors | ❔ | - |
| Jurassic Park | ✅ | - |
| Lethal Enforcers II: Gun Fighters | ✅ | - |
| Lethal Justice | ❔ | - |
| Line of Fire / Bakudan Yarou | ❔ | - |
| Lord of Gun | ✅ | - |
| Mechanized Attack | ✅ | - |
| Mobile Suit Gundam Final Shooting | ❌ | No crosshair with lightgun, only with dpad of the wiimote. |
| Operation Thunderbolt | ✅ | - |
| Operation Wolf | ✅ | - |
| Operation Wolf 3 | ✅ | - |
| Point Blank | ✅ | - |
| Rail Chase | ✅ | - |
| Space Gun | ✅ | - |
| Steel Gunner | ✅ | - |
| Steel Gunner 2 | ✅ | - |
| Trophy Hunting - Bear & Moose v1.0 | ✅ | - |
| Turkey Hunting USA v1.0 | ✅ | - |
| Wing Shooting Championship v2.00 | ✅ | - |
| Zero Point | ✅ | - |
| Zero Point 2 | ✅ | - |
| Zombie Raid | ✅ | - |