---
title: Libretro GearColeco
description: 
published: false
date: 2021-11-09T20:25:47.897Z
tags: libretro, colecovision, gearcoleco, 8.0+
editor: markdown
dateCreated: 2021-09-06T13:03:29.493Z
---



## ![](/emulators/license.svg) License

This core is under [**GPL 3.0**](https://github.com/drhelius/Gearcoleco/blob/main/LICENSE) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ❌ | ✅ |

## ![](/emulators/features.png) Features

| Feature | Supported |
| :---: | :---: |

## ![](/emulators/bios.svg) BIOS

### List of mandatory bios

| Filename | Description | MD5 | Provided |
| :---: | :---: | :---: | :---: |
| boot.rom | - | 2c66f5911e5b42b8ebe113403548eee7 | ❌ |

### Location

Put the bios like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 coleco
┃ ┃ ┃ ┃ ┣ 🗒 **boot.rom**

## ![](/emulators/roms.png) Roms

### Supported extensions

Roms must have the extension:

* .bin
* .col
* .cv
* .rom
* .zip

### Location

Put the roms like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 colecovision
┃ ┃ ┃ ┃ ┣ 🗒 **game.zip**

>The roms using **No-Intro** format are highly recommended.
{.is-success}

>For more information about roms, go to [this tutorial](./../../../tutorials/games/generalities/isos-and-roms)!
{.is-info}

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Accessing to options

You can configure various options in two different ways.

* Via RetroArch menu:

┣ 📁 RetroArch Menu
┃ ┣ 📁 Core options
┃ ┃ ┣ 🧩 Name_option

* Via the file `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core options

| Options | Options values | Variables (for configuration override) | Variables values |
| --- | --- | --- | --- |
| Refresh Rate (restart) | `Auto` ✅ / `NTSC (60 Hz)` / `PAL (50 Hz)` | `gearcoleco_timing` | `Auto` / `NTSC (60 Hz)` / `Pal (50 Hz)` |
| Allow Up+Down / Left+Right | `Disabled` ✅ / `Enabled` | `gearcoleco_up_down_allowed` | `disabled` / `enabled` |

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/drhelius/Gearcoleco/](https://github.com/drhelius/Gearcoleco/)