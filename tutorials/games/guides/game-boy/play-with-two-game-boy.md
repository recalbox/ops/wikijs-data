---
title: Play with two Game Boy
description: 
published: true
date: 2023-07-14T03:31:15.465Z
tags: two, gameboy
editor: markdown
dateCreated: 2021-08-08T08:08:25.644Z
---

## Introduction

It is possible to play two players with two GameBoy on Recalbox. There are 3 ways to do this.

>Although it may be tempting, Recalbox does not support Netplay in this mode.
>
>Moreover, in the one game for two Game Boy mode, saves are not supported for the second Game Boy, unlike in the [two games two Game Boy](./play-two-players-games-with-gamelink) mode, where again, Netplay is not supported.
{.is-warning}

## Usage

Starting with Recalbox 8, a folder named `gamelink` was automatically added into `gb` and `gbc` systems. This foler allows you to play with 2 players on the same game without any configuration!

1. Put the game to play into the `gamelink` folder of the corresponding system.
2. Once you updated the game lists, select the Game Boy or Game Boy Color.

![](/tutorials/games/guides/game-boy/play-two-players/samegametwoplayers1.png){.full-width}

3. In the list of games, select the game you want to play and press `START`.

![](/tutorials/games/guides/game-boy/play-two-players/samegametwoplayers2.png){.full-width}

The started game will duplicate automatically on your screen.

![](/tutorials/games/guides/game-boy/play-two-players/samegametwoplayers3.png){.full-width}

>To play with 2 different games at the same time, you need to use the [following tutorial](./play-two-players-games-with-gamelink).
{.is-info}