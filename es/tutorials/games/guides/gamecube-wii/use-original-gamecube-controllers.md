---
title: Jugar con los verdaderos mandos Gamecube
description: 
published: true
date: 2023-09-20T09:10:56.518Z
tags: gamecube, mandos
editor: markdown
dateCreated: 2023-09-20T09:10:56.518Z
---

>Para esta función se necesita un adaptador USB para mandos Gamecube.
{.is-info}

* Enchufad el adaptador Gamecube a Recalbox 
* Abrid el fichero: `/recalbox/share/system/recalbox.conf`
  * En la sección de controladores, buscad la siguiente línea:

```ini
gamecube.realgamecubepads=0
```

  * Modificarla por esta otra:

```ini
gamecube.realgamecubepads=1
```

* Esto es todo, vuestro mando Gamecube funcionará a partir de ahora con Dolphin.