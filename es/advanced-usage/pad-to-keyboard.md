---
title: Pad To Keyboard
description: Resucitad vuestros juegos de ordenador jugando con vuestro mando, incluso cuando sólo se puede jugar con el teclado!
published: true
date: 2023-09-30T17:15:35.102Z
tags: p2k, teclado, mando, ordenadores
editor: markdown
dateCreated: 2023-09-01T08:11:36.422Z
---

Bajo esta nomenclatura inglesa espeluznante se esconde una funcionalidad muy importante para uno de los desarrolladores de Recalbox, porque permite resucitar los juegos de **ordinosaurios**. Estos juegos no interesan a usuarios de Recalbox que no utilizan un teclado, es decir a la mayoría.

## Del mando al teclado...

Durante el glorioso decenio de los 80, han visto la luz un gran número de ordenadores... Pero en ésta época, los mandos todavía no existían, y el joystick sólo se utilizaba en las consolas. Los juegos en los micro-ordenadores presentan una ergonomía orientada a los teclados, y en raras excepciones, teclado o joystick. A menudo, el teclado es obligatorio para poder pasar la introducción o para lanzar el juego.
Es decir, resulta imposible jugar a ellos sin tener un teclado conectado.

A partir de la version 7.0 de Recalbox, se incluye una nueva funcionalidad llamada Pad-To-Keyboard que permite hacer corresponder las acciones de los mandos con las pulsaciones en un teclado virtual!
Este módulo se configura con la ayuda de pequeños ficheros de texto muy sencillos de construir. Si no os lo créeis, continuad leyendo.

Es muy fácil:

* ¿Tenéis un super juego Commodore 64 pero no podéis jugar porque os pide pulsar una tecla para lanzar la partida? Ningún problema:
	* Mapead el botón `START` del mando a la tecla `ESPACE`, y problema solucionado.
  * Lanzad el juego, apretad `START` y el juego verá que habéis pulsado `ESPACE` en un teclado.
* ¿Tenéis un juego de Amstrad que se juega con las 4 flechas del teclado y la barra espaciadora? Ningún problema:
	* Mapead la cruz direccional de vuestro mando en las 4 flechas, y el botón `A` en `ESPACE`.
  * Podéis jugar a vuestro juego como si hubiera sido concebido para jugar con un joystick!

Y sin duda, talentuosos desarrolladores externos podrán incluso crearnos una pequeña interfaz para generar estas configuraciones sin esfuerzo.

### ¿Cómo funciona?

Cuando EmulationStation detecta una configuración _**P2K**_ (Pad to Keyboard) para un juego, EmulationStation crear un teclado virtual que el juego verá como si fuera un teclado físico. A lo largo de la ejecución del juego, EmulationStation interceptará los eventos del mando y en función del fichero de configuración _**P2K**_, generará pulsaciones y liberaciones de teclas.

Los emuladores que toman en cuenta el teclado transmitirán estos eventos al juego, como si fueran eventos procedentes de un teclado físico.

### ¿Y el input-lag?

_**Garantía 100% sin input-lag, y esto se aplica a todos los ordenadores!**_

El modulo P2K utiliza una alta prioridad, lo que significa que es prioritario para capturar los eventos del mando, para traducirlo en pulsaciones y enviarlo al teclado virtual casi de forma instantánea.

En medidas efectuadas en un Raspberry Pi 3, la diferencia de tiempo entre la recepción de la acción del mando y la recepción de la pulsación de la tecla virtual es tan minúscula que no se puede considerar que impacte el input lag.

### Lo que todavía no se puede hacer

Por el momento, el mapping es un mapping instantáneo 1 a 1. Es decir que las acciones del mando se traducen al momento en pulsación/liberación de una sola tecla del teclado.

Es decir que no se puede:

* Generar una pulsación multi-tecla: SHIFT + cualquier otra tecla, por ejemplo.
* Generar pulsaciones/liberaciones consecutivas para escribir varios caracteres: no se puede utilizar este sistema para simular la escritura de una palabra, por ejemplo.

Quizá seremos capaces de realizar esto en las próximas versiones 😉 

## Configuración

### Fichero de configuración

Los ficheros de configuración de un mapping pad-to-keyboard se pueden definir para una carpeta completa (y sus subcarpetas) o para un juego en particular.

En los directorios, se debe colocar dentro de ellos y se debe llamar `.p2k.cfg`. La ruta completa sería del tipo `/chemin/vers/.p2k.cfg`.

Para una rom (o juego) en concreto, debe llamarse como el nombre de la rom, con la extensión `.p2k.cfg` añadida a la extensión existente. Por ejemplo para un juego `/chemin/vers/mijuego.ext`, el fichero de configuración p2k se llamará `/chemin/vers/mijuego.ext.p2k.cfg`.

Las configuraciones p2k de directorios se aplican a todos los juegos del directorio y de los subdirectorios que contiene.

Si existen varios ficheros p2k a varios niveles, se producen sobrecargas de configuración en orden descendiente siguiendo los directorios de la ruta, hasta la configuración propia al juego (si existe). Se os monstrará un ejemplo un poco más adelante.

### Configuración del mapping

Comenzamos con un ejemplo que ha sido utilizado en nuestros tests internos:

```ini
# dpad to arrow keys
0:up = up
0:down = down
0:right = right
0:left = left

# button A to SPACE
0:a = space
# B to Y key (for Y/N prompts)
0:b = y
# And finally, map START button to ENTER 
0:start = enter
```

Como podéis ver, la estructura es muy sencilla.

Se peuden insertar líneas vacías para separar diferentes bloques y utilizar # al principio de la línea para añadir comentarios o descripciones.

Las líneas de configuración se construyen siguiendo el patrón `PAD:ACTION = KEY`, siendo :

* `PAD`: N° del mando tal y como aparece en EmulationStation, o el orden natural del mando si no se ha realizado ninguna asignación desde EmulationStation. En pocos casos necesitaréis utilizar más de un mando, asi que el nº **0** se utilizá en la gran mayoría de los casos.
* `ACTION`: Acción del mando. Se pueden mapear todos los botones, las fechas direccionales (D-Pad), y las 4 direcciones primarias de los joysticks analógicos, en total hasta 25 acciones diferentes se pueden asignar a las teclas de un teclado.
* `KEY`: La tecla del teclado que se asocia a la acción del mando. Cuando el mando ejecuta una acción, se enviará al juego la tecla aquí configurada. Este mapping es 1 a 1. Si queréis simular por ejemplo _**LEFT-SHIFT + A**_, hará falta asignar _**LEFT-SHIFT**_ a un botón del mando (por ejemplo _**L1**_), y la tecla _**A**_ será asignada a otro botón (por ejemplo el _**Y**_). Si apretamos en el mando simultáneamente _**L1+Y**_, se enviará un _**LEFT-SHIFT + A**_. Para los joysticks analógicos, como se pueden poner en diagonal, podemos utilizarlos para realizar esta combinación de 2 teclas al mismo tiempo, pero hay que mapear en cualquier caso la acción joystick arriba y la acción joysticks derecha, por poner un ejemplo.

No importa si escribís en mayúsculas o minúsculas dentro de `ACTION` o `KEY`, y tampoco importan los espacios blancos al principio de la línea, al final o alrededor del símbolo `=`.

#### Acciones de los joysticks

Aquí tenéis la lista completa de acciones del mando que se pueden utilizar:

| Acción | Descripción | Acción | Descripción |
| :--- | :--- | :--- | :--- |
| `up` | Digital PAD, dirección arriba | `j1up` | Joystick #1 (izquierda), dirección arriba |
| `down` | Digital PAD, dirección abajo | `j1down` | Joystick #1 (izquierda), dirección abajo |
| `left` | Digital PAD, dirección izquierda | `j1left` | Joystick #1 (izquierda), dirección izquierda |
| `right` | Digital PAD, dirección derecha | `j1right` | Joystick #1 (izquierda), dirección derecha |
| `a` | Botón A | `j2up` | Joystick #2 (droite), dirección arriba |
| `b` | Botón B | `j2down` | Joystick #2 (droite), dirección abajo |
| `x` | Botón X | `j2left` | Joystick #2 (droite), dirección izquierda |
| `y` | Botón Y | `j2right` | Joystick #2 (droite), dirección derecha |
| `l1` | Botón L1 | `l2` | Botón L2 |
| `r1` | Botón R1 | `r2` | Botón R2 |
| `start` | Botón START | `l3` | Botón L3 |
| `select` | Botón SELECT | `r3` | Botón R3 |

#### Combinaciones con HotKey (a partir de Recalbox 8.0)

A partir de la versión 8.0 de Recalbox, se pueden prefixar las acciones con el signo `+` para indicar que deben ejecutarse junto con el botón `HOTKEY` del mando.

El siguiente ejemplo permite activar las teclas de F1 a F4 cuando se pulsa el botón HotKey junto con alguna de las direcciones del DPAD:

```ini
# Spécial actions on dpad
0:+up = f1
0:+down = f2
0:+right = f3
0:+left = f4
```

#### Teclas del teclado utilizables

EmulationStation crear un teclado virtual agnóstico. No conoce ni idioma, ni disposición particular. Se trata de un teclado de 105 teclas, QWERTY, de los más básicos.

Para poder crear un sistema genérico y no perder a los usuarios, sólo se pueden configurar las teclas que son comunes a todos los teclados existentes:

* Si os digo que mi teclado es portugés y que he mapeado el botón Y a la tecla %, hay muchas probabilidades que la tecla % de mi teclado no corresponda con vuestra tecla %.
* El emulador también debe hacer corresponder vuestro teclado con el teclado esperado por el juego. Es algo tan complejo y tan poco intuitivo que algunos emuladores proponen un teclado virtual  directamente en la pantalla del juego para facilitar la entrada.

No obstante, no os preocupéis, en la gran mayoría de los casos, no váis a necesitar configurar teclas especiales. Sólo tenéis que recordar que la disposición del teclado virtual es QWERTY.

Una imagen vale más que mil palabras:

![Noms des touches clavier](/advanced-usage/p2k/p2k-layout-v2.png)

#### Sobrecargas de configuración (sí, otra vez!)

Como ya se ha hablado más arriba, los ficheros de configuración p2k pueden aplicarse en cadena y sobrecargarse los unos a los otros.

El módulo P2K empieza por leer el que se encuentra en la ruta más alta, y desciende de carpeta en carpeta hasta llegar al directorio de la rom. Finalmente aplicará el fichero p2k asociado a la rom si existe.

Esto significa que las configuraciones se pueden añadir o pueden modificarse!

**Ejemplo 1: Las configuraciones se añaden**

**/ruta/hacia/.p2k.cfg :**

```ini
# Map START to ENTER
0:start = enter
```

**/ruta/hacia/monjeu.ext.p2k.cfg :**

```ini
# Map SELECT to SPACE
0:select = space
```

Al final el fichero agregado contendrá:

```ini
# Map START to ENTER
0:start = enter
# Map SELECT to SPACE
0:select = space
```

**Ejemplo 2: Las configuraciones se modifican**

**/ruta/hacia/.p2k.cfg**

```ini
# Map START to ENTER
0:start = enter
# Map SELECT to SPACE
0:select = space
```

**/ruta/hacia/monjeu.ext.p2k.cfg :**

```ini
# Map SELECT to F1
0:select = f1
```

Al final el fichero agregado contendrá:

```ini
# Map START to ENTER
0:start = enter
# Map SELECT to F1
0:select = f1
```

**Ejemplo 3: Las configuraciones se modifican suprimiendo mappings**

**/ruta/hacia/.p2k.cfg**

```ini
# Map START to ENTER
0:start = enter
# Map SELECT to SPACE
0:select = space
```

**/hacia/hacia/monjeu.ext.p2k.cfg :**

```ini
# Remove SELECT mapping!
0:select =
```

Al final el fichero agregado contendrá:

```ini
# Map START to ENTER
0:start = enter
```

### Adición de una breve descripción dentro de EmulationStation

![](/advanced-usage/p2k/configp2k.png)

Para añadir una descipción como la siguiente:

* Utilizad dos punto y comas (;;) para que aparezcan en la ventana de EmulationStation.

```ini
# Recalbox's Pad-to-Keyboard configuration
#Arkanoid Revenge of Doh (1987)(Sharp - SPS)
#SHARP X6800

#left move key 4
0:left = kp4 ;; Move left
#right move key 6
0:right = kp6 ;; Move right
#shoot key space
0:a = space ;; Fire!
#hispeed move key shift
0:b = leftshift ;; Speedup!
```

## Extensión del sistema P2K v2 (A partir de Recalbox 8.0)

A partir de la versión 8.0 de Recalbox, el sistema P2K añade nuevas funcionalidades para soportar:

* La emulación del ratón de 3 botones
* Las combinaciones de teclas
* Las secuencias de teclas

### Emulación del ratón de 3 botones

A partir de ahora podéis emular el desplazamiento del raton y las pulsaciones/liberaciones de sus 3 botones con vuestro mando de juegos.

Para una emulación precisa, se aconseja un mando con un joystic analógico, pero el ratón también se puede simular con los botonos o la cruz direccional del mando.

#### Configuración completa

A las teclas `KEY` del teclado virtual se añaden 7 nuevos valores, que corresponden a las 4 direcciones de desplazamiento del ratón más sus 3 botones.

| Elemento del ratón | Descripción |
| :--- | :--- |
| `mouse.button.left` | Botón izquierdo del ratón |
| `mouse.button.middle` | Botón central del ratón o clic molette |
| `mouse.button.right` | Botón derecho del ratón |
| `mouse.move.left` | Desplazamiento del ratón hacia la izquierda |
| `mouse.move.up` | Desplazamiento del ratón hacia arriba |
| `mouse.move.right` | Desplazamiento del ratón hacia la derecha |
| `mouse.move.down` | Desplazamiento del ratón hacia abajo |

Estos valores se pueden asignar a cualquier `ACTION`.
Supongamos que disponemos de un mando tipo SNES con un DPAD y 4 botones, y que queremos jugar a un juego DOS al que sólo se puede jugar con un ratón. Aquí tenéis como ejemplo un posible fichero p2k:

```ini
# Recalbox's Pad-to-Keyboard configuration
# DOS GAME

# Mouse moves
0:up = mouse.move.up
0:right = mouse.move.right
0:down = mouse.move.down
0:left = mouse.move.left

# Mouse buttons
0:a = mouse.button.left
0:b = mouse.button.right
```

Con esta configuración, el desplazamiento del cursor acelera poco a poco al inicio para estabilizarse a una determinada velocidad pasados unos segundos.

También podéis utilizar un joystick analógico, por ejemplo el joystick derecho de un mando XBox:

```ini
# Recalbox's Pad-to-Keyboard configuration
# DOS GAME

# Mouse moves
0:j2up = mouse.move.up  ;; Movimiento del ratón
0:j2right = mouse.move.right ;; Movimiento del ratón
0:j2down = mouse.move.down ;; Movimiento del ratón
0:j2left = mouse.move.left ;; Movimiento del ratón

# Mouse buttons
0:a = mouse.button.left ;; Botón izquierdo
0:b = mouse.button.right ;; Botón derecho
```

En este caso, la velocidad de desplazamiento del ratón se regulará según la presión ejercida sobre el joystick analógico.

#### Configuración simplificada

Si utilizáis los joysticks izquierdo o derecho para simular el desplazamiento del ratón, podéis utilizar la siguiente configuración simplificada `0:j1 = mouse.moves` o `0:j2 = mouse.moves` (*atención a la 's' !*).
Retomando la configuración anterior, se puede simplificar de la siguiente forma:

```ini
# Recalbox's Pad-to-Keyboard configuration
# DOS GAME

# Mouse moves
0:j2 = mouse.moves ;; Movimiento del ratón

# Mouse buttons
0:a = mouse.button.left ;; Botón izquierdo
0:b = mouse.button.right ;; Botón derecho
```

Para los juegos que sólo nécesitan un ratón (y son muchos), podéis mapear el ratón en varios elementos del mando al mismo tiempo, como aquí:

```ini
# Recalbox's Pad-to-Keyboard configuration
# DOS GAME

# Mouse moves
0:up = mouse.move.up
0:right = mouse.move.right
0:down = mouse.move.down
0:left = mouse.move.left
0:j1 = mouse.moves ;; Movimiento del ratón
0:j2 = mouse.moves ;; Movimiento del ratón

# Mouse buttons
0:a = mouse.button.left ;; Botón izquierdo
0:b = mouse.button.right ;; Botón derecho
```

Con este fichero p2k, se pueden utilizar los dos joystics y el dpad para controlar el cursor del ratón, en función de las preferencias de cada jugador y del nivel de precisión que se quiere obtener.

### Configuración extendida del teclado

Con la version 2 de los ficheros P2K, se añaden nuevas posibilidades de simulación del teclado, soportando las combinaciones de teclas y las secuencias (como cuando se escribe en un teclado).

En los dos casos, el sistema soporta hasta 32 teclas, es decir, o bien 32 teclas pulsadas *casi* simultáneamente, o bien una secuencia de hasta 32 teclas consecutivas.

#### Combinaciones de teclas

Para declarar una combinación de teclas, es muy fácil, basta con separar las teclas con espacios (y nada más que espacios!), como se muestra aquí:

```ini
# Assign CTRL+C to R1
0:r1 = leftctrl c ;; Break the program!
```

Cuando se pulsa R1, las teclas se pulsan en el orden indicado, y luego se liberan en el orden inverso.

Si el emulador necesita un retardo entre las pulsaciones, o en el caso de querer aplicar un tiempo de espera específico por cualquier otro motivo, podéis prefijar las teclas con `+xxx` donde xxx indica un retardo en milisegundos.

Ejemplo :

```ini
# Assign CTRL+C to R1
0:r1 = +100 leftctrl c ;; Break the program!
```

En este caso: cuando se pulsa R1, el sistema P2k pulsa las teclas virtuales CONTROL IZQUIERDA, espera 100ms, y luego pulsa la tecla C. La última tecla no introduce ningún retardo. A continuación, cuando se suelta/libera R1, las teclas virtuales se liberan en el orden contrario: se libera primero C, se espera 100ms, y finalmente se libera CONTROL IZQUIERDA.

El `+` indica explícitamente que se trata de una combinación de teclas. Se puede utilizar solo, sin explicitar un tiempo, pero en ausencia del tiempo, no servirá para nada, porque las combinaciones se construyen separando varias teclas con espacios.

#### Secuencias de teclas

Para realizar una secuencia de teclas, debemos utilizar un indicativo explícito `/`, como aquí:

```ini
# Assign the sequence FGHJ to R1
0:r1 = / f g h j ;; Type FGHJ!
```

Aquí, cuando se pulsa R1, el sistema genera la siguiente secuencia: pulsa F, libera F, pulsa G, libera G, pulsa H, libera H, pulsa J, libera J. Sin ninguna espera entre las pulsaciones y las liberaciones, y siguiendo el orden estricto de la declaración.

Observad que cuando se libera R1, no ocurre nada. La secuencia se envía únicamente a la pulsación.

Una vez más, algunos emuladores/juegos pueden tener problemas con las secuencias de teclas sin retardo. Se puede declarar un retardo entre cada pulsación/liberación como sigue:

```ini
# Assign the sequence FGHJ to R1
0:r1 = /25 f g h j ;; Type FGHJ!
```

En este caso, la secuencia se ejecutará de la siguiente forma: pulsa F, espera de 25ms, libera F, espera de 25ms, pulsa G, espera de 25ms, libera G, espera de 25ms, pulsa H, espera de 25ms, libera H, espera de 25ms, pulsa J, espera de 25ms, libera J. No hay ningún retardo depués de la última liberación de tecla.

### ¿Qué nos queda todavía por soportar?

Una sóla cosa: las secuencias de teclas con combinaciones en el medio. Actualmente es o todo secuencias, o todo combinaciones, pero no los dos mezclados.

Esto implica que no podréis por ejemplo escribir una palabra que necesite caracteres accesibles a través de SHIFT o de cualquier otra combinación, por poner un ejemplo.

Quizás en la versión 3... :)

## Apartado especial para compartir vuestra configuraciones

¿Ya está?, ¿habéis configurado vuestros juegos favoritos utilizando el sistema Pad-To-Keyboard para poder jugar con vuestro mando?

No seáis egoistas!: compartid vuestras configuraciones con la comunidad de usuarios Recalbox para que no tengan que realizar el mismo trabajo de configuración que vosotros una y otra vez!

Hemos abierto un thread de discusión especial [**aquí en el forum de Recalbox**](https://forum.recalbox.com/topic/21287/partager-vos-fichiers-pad2keyboard-p2k-cfg?_=1600282719468) para que podáis compartir y poner en común y mejorar vuestras configuraciones p2k.

> También podéis incluir vuestras configuraciones en [ScreenScraper](https://www.screenscraper.fr) para que se puedan descargar automáticamente al realizar un scraping.
{.is-info}

No obstante, atención: intentad aplicad una misma lógica para todos vuestros ficheros de configuración para obtener mappings intuitivos y fáciles de utilizar:

* Los juegos se deben lanzar con el botón START o el botón A.
* Si un juego necesita una pulsación de una tecla para configurar el joystick (a menudo es el caso de los juegos ZX Spectrum, por ejemplo), seleccionad la utilización del teclado y no del joystick, y mapead todas las teclas necesarias para jugar.

Buen juego para todos!
