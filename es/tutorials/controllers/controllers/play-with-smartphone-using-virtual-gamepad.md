---
title: Jugar desde vuestro móvil utilizando el mando virtual
description: 
published: true
date: 2023-09-19T09:26:50.910Z
tags: virtual, smartphone, mando, móvil
editor: markdown
dateCreated: 2023-09-19T09:26:50.910Z
---

[El proyecto Virtual Gamepads de Miroof](https://github.com/miroof/node-virtual-gamepads) aporta una nueva función a Recalbox: ¡jugar utilizando un teléfono móvil como mando!

Podéis añadir hasta 4 mandos a la vez.

* Con vuestra Recalbox conectada a la red, todo lo que necesitáis hacer es obtener vuestra [dirección IP](./../../... /tutorials/network/ip/discover-recalbox-ip), conectad vuestro móvil a la misma red wifi que vuetra Recalbox y abrid un navegador web en la dirección IP encontrada junto con el puerto 8080 (consultad [este tutorial](./../../../tutorials/system/access/recalbox-manager-web-interface) para saber cómo acceder al Web Manager).
* Después de haber hecho esto, se añadirá automáticamente un nuevo controlador/mando a vuestra Recalbox.

A continuación, podéis lanzar un juego o id a la configuración de los controladores para asignar este nuevo controlador a un jugador determinado.

![Mando virtual](/tutorials/controllers/controllers/virtualgamepad.png)

Para más información sobre esta funcionalidad, consultad la fuente original [en este enlace](https://github.com/jehervy/node-virtual-gamepads#features).