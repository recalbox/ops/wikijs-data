---
title: 6. 💡 F.A.Q. - Domande Frequenti
description: 
published: true
date: 2024-11-28T13:43:06.833Z
tags: 
editor: markdown
dateCreated: 2021-06-17T21:07:15.556Z
---

## Generale

### Dove posso installare Recalbox?

Puoi installare Recalbox sui seguenti sistemi:

- **Raspberry Pi 5**
- Raspberry Pi 4
- Raspberry Pi 400
- Raspberry Pi 3 (B/B+)
- Raspberry Pi 2
- Raspberry Pi 1 (B/B+)
- Raspberry Pi Zero
- Raspberry Pi Zero 2
- Odroid XU4
- Odroid Go Advance (Console Portatile)
- Odroid Go Super (Console Portatile)
- PC x86 (32bits)
- PC x86_64 (64bits)
- Anbernic RG353(M/V/P)
- Anbernic RG351

### Come posso partecipare o aiutare?

- Puoi fare una donazione tramite Paypal
- Puoi acquistare l'hardware presso il sito kubii.fr/221-recalbox
- Puoi segnalare problemi (bug) o miglioramenti sul nostro [Gitlab](https://gitlab.com/recalbox/recalbox/issues)
- Puoi partecipare presso il [forum Recalbox](http://forum.recalbox.com)
- Puoi [contribuire a questa wiki](./contribute)

### Dove posso vedere il progresso sul progetto Recalbox?

Presso il [GitLab di Recalbox](https://gitlab.com/recalbox). Puoi accedere :  
- [problemi/issues](https://gitlab.com/recalbox/recalbox/issues) dove sono presenti le problematiche a cui si sta lavorando.  
- Le [pietre miliari/milestones](https://gitlab.com/recalbox/recalbox/-/milestones) dove puoi vedere le problematiche ancora presenti, quelle a cui si sta lavorando e quelle che sono state completate.

### Quanti giochi contiene Recalbox?

Stiamo provando ad aggiungere il maggior numero di giochi gratuiti a questa distro, cosicchè puoi iniziare a divertirti con Recalbox già dal primo avvio!
Vai su [questa pagina](https://forum.recalbox.com/topic/52/homebrews-sur-la-recalbox) per inviare il gioco creato da te!

### Posso usare Recalbox in un chiosco / tavolo da bar (bartop)?

Sì, il sistema Recalbox OS è stato progettato per essere integrato in chioschi e bartop.
- Puoi connettere tasti e joystick direttamente al GPIO del RaspberryPi.
- Puoi disabilitare i menà per evitare l'uso improprio del sistema.
- È anche possibile usare uscite HDMI / DVI / VGA (con adattatori) o RCA/Composito.
Puoi trovare più informazioni presso la [pagina dedicata](/it/tutorials/system/installation/bartop-arcade-configuration).

### Recalbox supporta molti sistemi, ma vedo solo alcuni di essi nell'interfaccia, cosa dovrei fare?

Solo i sistemi con videogiochi disponibili verranno mostrati dall'interfaccia. Prova ad aggiungere le tue roms (i giochi) tramite una Connessione di Rete e riavvia Recalbox per vedere i sistemi interessati apparire. Inoltre, alcuni hardware non supportano tutti i sistemi/console. Controlla la pagina [Compatibilità Emulatori](it/hardware-compatibility/emulators-compatibility) per verificare quale sistema è supportato dal tuo hardware.

### Devo overclockare il mio Raspberry Pi?

Sul Raspberry Pi 1 (incluso lo Zero), è altamente consigliato l'overlock in modalità EXTREM.

## EmulationStation

### Come posso cambiare l'aspetto dell'interfaccia/tema di EmulationStation?

Premi `Start` sul tuo controller → `OPZIONI INTERFACCIA` → `TEMA`

### Come posso disattivare la musica in Background?

Premi `Start` sul tuo controller → `OPZIONI SUONO` → Imposta su OFF

### Dove sono collocati i file di configurazione di EmulationStation nel sistema?

```
/recalbox/share/system/recalbox.conf
```

### Dove sono collocati i temi di EmulationStation?

```
/recalbox/share/themes
```

## Emulazione

### Perché ci sono più emulatori disponibili per lo stesso sistema?

L'emulazione non è un operazione senza imperfezioni, c'è un bilanciamento costante tra prestazioni, funzionalità e fedeltà.
Poiché l'emulazione non è perfetta, è bene avere più opzioni disponibili per gestirla, talvolta un'emulazione veloce è migliore, ma potrebbe avere problemi inaspettati, mentre una più lenta sarà più accurata ma non supporta alcune caratterstiche specifiche.

### Come posso cambiare l'emulatore di default per un sistema?

Premendo `START` sul controller, e poi su `OPZIONI AVANZATE`, ed infine su `CONFIGURAZIONE AVANZATA EMULATORE`. Puoi selezionare il sistema desiderato, puoi cambiare il tipo di emulatore e le opzioni di kernel. (Ricorda che le opzioni di base non cambieranno se l'emulatore selezionato è quello di Default.)

## Sistema

### Recalbox supporta oltre 100 consoles ma vedo solo alcuni di essi, nell'interfaccia di Emulation Station o nella cartella `SHARE`?

In base al computer or microcomputer (Raspberry) che stai usando, avrai solamente gli emulatori compatibili con esso.
Per favore controlla la lista della compatibilità in base all'hardware [qui](/it/hardware-compatibility/).


### Perché in alcuni sistemi come il GBA, i miei giochi mostrano uno schermo nero e poi torno su EmulationStation?

Alcuni sistemi hanno emulatori che necessitano dei BIOS per funzionare. Ottieni i bios necessari e aggiungili alla cartella `/recalbox/share/bios`. Nel menù principale è presente anche la sezione `BIOS CHECKING` per aiutarti a scegliere i BIOS corretti.

## Risoluzione dei Problemi - Generale

### Recalbox è crashato, devo staccare e riattaccare la corrente?

No, nella maggior parte dei casi il sistema non è crashato completamente, solo EmulationStation.

Ci sono molti metodi per risolvere questa spiacevole situazione, la più semplice è usare le opzioni  di debug presenti nel Webmanager per riavviare EmulationStation o per spegnere/riavviare correttamente Recalbox.

Se non si seguono queste procedure potresti incorrere in corruzione dei dati, specialmente se stai usando un Raspberry Pi con tutti i tuoi dati solo sulla scheda SD.

## Risoluzione dei Problemi - Connettività

### Windows 10 - Impossibile accedere al Recalbox dalle connessioni di Rete

* Caso 1: Nessun computer, Recalbox incluso, sono visibili da Windows

Se non puoi vedere nessun altro computer dalla rete di Windows 10, è perché la modalità di scoperta della Rete è disabilitata di default e la configurazione di rete è impostata su "Pubblica" oppure "Permetti di scoprire questo PC" è impostato su "disabilitato".

Bisogna quindi cambiare le impostazioni di rete e impostare la Rete su "Privata", o attivare l'opzioni "Permetti di scoprire questo PC"

* Caso 2: Recalbox non è più visibile sin dall'update n°1709 di Windows 10

Sin da questo aggiornamento, puoi vedere Recalbox sulla Rete, ma non puoi più accederci.

Puoi vedere una spiegazione ufficiale [qui](https://tech.nicolonsky.ch/windows-10-1709-cannot-access-smb2-share-guest-access/) (Inglese) o [qui](https://support.microsoft.com/fr-fr/help/4046019/guest-access-smb2-disabled-by-default-in-windows-10-server-2016) (inglese).

Soluzione più rapida: installa [questa patch](https://mega.nz/#!rUA3xDgI!a14w9TqQWinLriLANpk7BF_WkoNg8mw6fHloyPEZMPg)

Questa patch disabiliterà la sicurezza su accessi di rete anonimi su Windows per ripristinare l'accesso al Recalbox.

* Caso 3 (il più comune)**

Vai su Opzioni / Applicazioni / Programmi / Abilita o Disabilita programmi di Windows e seleziona:
`SMB 1.0/CIFS file sharing support / SMB1.0/CIFS client...` ed infine riavvia Windows..