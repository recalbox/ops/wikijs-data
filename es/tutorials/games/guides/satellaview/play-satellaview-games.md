---
title: Jugar a los juegos de la Satellaview
description: 
published: true
date: 2024-11-17T16:40:38.177Z
tags: nintendo, satellaview
editor: markdown
dateCreated: 2024-11-17T16:40:38.177Z
---

## Introducción

La mayoría de las veces, jugaréis a los juegos de Satellaview con la primera bios válida. Sin embargo hay que saber que, en la gran mayoría de los casos, esta bios estará en japonés. A continuación os explicamos cómo jugar a los juegos de Satellaview si no tenéis ganas de buscar la bios en inglés.

## Procedimiento

- Iniciad el juego. Llegaréis a la primera pantalla (una televisión que os habla).

![](/tutorials/games/guides/satellaview/satellaview1.png){.full-width}

- Confirmad y rellenad el apodo que queréis utilizar. Este nickname se utilizará en el sistema y en los juegos.

![](/tutorials/games/guides/satellaview/satellaview2.png){.full-width}

- Podéis pulsar en la palabra de abajo a la izquierda `PAGE` para cambiar el alfabeto. Cuando hayas terminado, pulsad «HECHO» en la parte inferior derecha (pulsando el botón «INICIO» estaréis en «HECHO»). Se os preguntará si preferís chico o chica y a continuación validad.

![](/tutorials/games/guides/satellaview/satellaview3.png){.full-width}

- Una vez hecho esto, el sistema intentará conectarse al sistema original, pero no lo conseguirá, puesto que ya no existe. Os encontraréis en una ciudad que era bulliciosa cuando el servicio estaba activo. No podéis hacer otra cosa que entrar en el edificio frente a vosotros (`ARRIBA` y luego `A`).

![](/tutorials/games/guides/satellaview/satellaview4.png){.full-width}

- Aparecerá un menú con 4 opciones posibles

  - `Load Stored Data`: jugar la partida que empezásteis al principio
  - `Delete Stored Data`: borrar el contenido del juego
  - `Receive Program`: recibir una actualización (ya no funciona más)
  - `Clear Personal Data`: borrar el perfil que habéis creado hasta ahora

![](/tutorials/games/guides/satellaview/satellaview5.png){.full-width}

- Seleccionad `Load Stored Data`, seleccionad el juego y ¡listo!

![](/tutorials/games/guides/satellaview/satellaview6.png){.full-width}
