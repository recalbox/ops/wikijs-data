---
title: Indicativos wifi de países
description: 
published: true
date: 2023-09-13T14:44:20.133Z
tags: wifi, indicativos, países
editor: markdown
dateCreated: 2023-09-13T14:44:20.133Z
---

El uso de las bandas de frecuencia está regulado en cada país. Por ejemplo, Japón permite el uso del canal 14, mientras que la mayoría de los demás países no. Configurar el código de país en tu Recalbox te asegura que utilizarás las frecuencias correctas con tu conexión inalámbrica.

En Recalbox, el valor por defecto es `JP` porque este valor es lo suficientemente permisivo para simplificar la vida de los usuarios. El valor puede ser cambiado para adaptarse a vuestras necesidades a través del fichero `recalbox.conf`.

## Canales deshabilitados por país

Algunos canales están deshabilitados en ciertos países:

| Canal | Frecuencia (Mhz) | América del Norte | Japón | La mayor parte del mundo
| :---: | :---: | :---: | :---: | :---: |
| 1 | 2412 | ✅ | ✅ | ✅ |
| 2 | 2417 | ✅ | ✅ | ✅ |
| 3 | 2422 | ✅ | ✅ | ✅ |
| 4 | 2427 | ✅ | ✅ | ✅ |
| 5 | 2432 | ✅ | ✅ | ✅ |
| 6 | 2437 | ✅ | ✅ | ✅ |
| 7 | 2442 | ✅ | ✅ | ✅ |
| 8 | 2447 | ✅ | ✅ | ✅ |
| 9 | 2452 | ✅ | ✅ | ✅ |
| 10 | 2457 | ✅ | ✅ | ✅ |
| 11 | 2462 | ✅ | ✅ | ✅ |
| 12 | 2467 | ❌ | ✅ | ✅ |
| 13 | 2472 | ❌ | ✅ | ✅ |
| 14 | 2484 | ❌ | ✅  (sólo 11b) | ❌ |

[Fuente](https://en.wikipedia.org/wiki/List_of_WLAN_channels)

## Configuración

Este parámetro se puede modificar en el fichero `recalbox.conf` :

* Abrid el fichero [recalbox.conf](./../../../basic-usage/getting-started/recalboxconf-file).
* Buscad las siguientes líneas:

```ini
## Set Wifi region
wifi.region=JP
```

Modificad el valor `JP` del ejemplo pour el valor que queréis utilizar.

## Lista de indicativos de país de la Wifi

Aquí tenéis la lista de indicativos principales:

| Pays | Code |
| :--- | :--- |
| Austria | AT |
| Australia | AU |
| Bélgica | BE |
| Brasil | BR |
| Canadá | CA |
| Suiza y Liechtenstein | CH |
| China | CN |
| Chipre | CY |
| República Checa | CZ |
| Alemania | DE |
| Dinamarca | DK |
| Estonia | EE |
| España | ES |
| Finlandia | FI |
| Francia | FR |
| Reino Unido | GB |
| Grecia | GR |
| Hong Kong | HK |
| Hungría | HU |
| Indonesia | ID |
| Irlanda | IE |
| Israel | IL |
| India | IN |
| Islandia | IS |
| Italia | IT |
| Japon | JP |
| Republica Coreana | KR |
| Lituania | LT |
| Luxembourgo | LU |
| Letonia | LV |
| Malaisia | MY |
| Países-Bajos | NL |
| Noruega | NO |
| Nueva Zelanda | NZ |
| Filipinas | PH |
| Polonia | PL |
| Portugal | PT |
| Suecia | SE |
| Singapur | SG |
| Slovenia | SI |
| Slovaquia | SK |
| Thailandia | TH |
| Taiwan | TW |
| Estados-Unidos | US |
| Sudáfrica | ZA |

## Otros indicativos

Podéis encontrar más indicativos [en esta página](http://www.arubanetworks.com/techdocs/InstantWenger_Mobile/Advanced/Content/Instant%20User%20Guide%20-%20volumes/Country_Codes_List.htm#regulatory_domain_3737302751_1017918).