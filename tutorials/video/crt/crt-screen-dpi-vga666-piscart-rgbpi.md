---
title: Configure your cathodic screen on DPI bus (VGA666 / PiScart / RGBPi) (outdated)
description: 
published: true
date: 2022-01-20T15:52:48.247Z
tags: screen, dpi, bus, cathodic, vga666, piscart, rgbpi
editor: markdown
dateCreated: 2021-08-06T17:42:20.089Z
---


>**This article is outdated !**
Please see [CRT Scart and VGA on Raspberry Pi 4 / 400 / 3](./recalbox-on-crt-with-scart-dac)
{.is-warning}

## What is the DPI bus?

The full description can be found [here](https://www.raspberrypi.org/documentation/hardware/raspberrypi/dpi/README.md).

A parallel RGB interface up to 24 bits is available on all Raspberry Pi boards with the 40 channel header (A+, B+, Pi2, Pi3, Zero) and the "Compute Module". This interface allows to connect parallel RGB displays to the Raspberry Pi GPIO, either in RGB24 (8 bits for red, green and blue), or in RGB666 (6 bits per color), or in RGB565 (5 bits for red, 6 for green and 5 for blue).

This interface is controlled by the GPU firmware and can be programmed by a user via special config.txt parameters and by enabling the correct Linux device tree overlay.

Note that there are different ways to represent the color values on the DPI output pins in 565, 666 or 24-bit mode (see the following table and the `output_format` part of the `dpi_output_format` parameter below):

![GPIOs used according to the mode used](/tutorials/video/crt/dpi_output.png)

Note that all other peripheral overlay devices that use conflicting GPIO pins must be disabled. In the [recalbox-user-config.txt](./../../system/modification/configtxt-configuration) file, be sure to comment out or reverse any dtp parameters that enable I2C or SPI :

```ini
dtparam=i2c_arm=off
dtparam=spi=off
```

## The Gert VGA666

The Gert VGA 666 (6 bits per color channel, so 666) is an add-on/expansion board for the Raspberry Pi model B+ (will not work with the model A/B because the extra GPIO pins of the model B+ are required). This is an open source hardware creation recently made public by Gert van Loo, one of the hardware engineers who contributed to the original Raspberry Pi design (also one of the architects of the BCM2835 chip at the heart of the Raspberry Pi) and someone many of you may have talked to at Raspberry Jams or on the Raspberry Pi forums.

This is a clean and very useful method of using a VGA display/monitor with your Raspberry Pi and is much cheaper than an HDMI to VGA adapter or similar. The VGA connection is driven natively in hardware by the GPIO pins (using a parallel interface) and uses about the same CPU load as the HDMI connection on board. It is capable of displaying 1080p60 VGA video without CPU load.

It is also possible to drive this interface at the same time as the HDMI connection, so a dual screen setup is also possible. This add-on was not possible on the Raspberry Pi A and B models, because not all the necessary pins were placed in the GPIO header. Another great improvement that the B+ model has made possible!

![The Gert VGA666 module](/tutorials/video/crt/vga666-1.jpg)

First we have to open the file [recalbox-user-config.txt](./../../../tutorials/system/modification/configtxt-configuration) :

Note that for the Gert VGA 666 to work properly, you must disable SPI and I2C. Then add these lines at the bottom of the file

```ini
dtparam=spi=off
dtparam=i2c_arm=off
dtoverlay=vga666 
enable_dpi_lcd=1 
display_default_lcd=1 
```

You also need to specify the resolution of your screen (not a TV). After the lines you added above, you will also need to add one of the following configurations:

```ini
#For 1920x1080 60Hz
dpi_group=2
dpi_mode=82
#For 1280x1024 60Hz
dpi_group=2
dpi_mode=35
#For 1024x768 60Hz
dpi_group=2
dpi_mode=16
#For 800x600 60Hz
dpi_group=2
dpi_mode=9
```

![VGA666 on a PC monitor](/tutorials/video/crt/vga666-2.jpg)

If the resolution you are looking for is not among these, you can consult [this link](https://www.raspberrypi.org/documentation/configuration/config-txt/video.md).

For TV screens, the configuration will be different. We need to use the custom `dpi_mode` 87 and we need to correct the `hdmi_timings` part to configure the custom 15KHz mode. This custom mode will be applied to the EmulationStation interface. Several different `hdmi_timings` can be used.

```ini
dtparam=i2c_arm=off
dtparam=spi=off
 
# Enable VGA666
dtoverlay=vga666
enable_dpi_lcd=1
display_default_lcd=1
 
dpi_group=2
dpi_mode=87
 
#hdmi_timings=506 1 8 48 56 240 1 3 10 6 0 0 0 60 0 9600000 1
#hdmi_timings=512 1 16 48 64 288 1 3 5 6 0 0 0 50 0 9600000 1
 
# Custom 15kHz mode
hdmi_timings=506 1 8 44 52 240 1 6 10 6 0 0 0 60 0 9600000 1
#hdmi_timings=320 1 17 33 34 224 1 14 8 18 0 0 0 60 0 6400000 1
#hdmi_timings=960 0 173 8 0 160 0 40 10 0 0 0 0 60 0 19200000 8
#hdmi_timings=320 1 25 30 30 240 1 9 3 10 0 0 0 60 0 6400000 1
#hdmi_timings=1920 1 52 208 260 240 1 6 10 6 0 0 0 60 0 38400000 1
```

Several different `hdmi_timings` can be used. Here is a list of usable timings, it will save you time for your configuration.

```ini
OK
hdmi_timings=996 1 24 96 120 240 1 3 10 6 0 0 0 60 0 19200000 1
hdmi_timings=506 1 8 44 52 240 1 6 10 6 0 0 0 60 0 9600000 1
hdmi_timings=336 1 10 30 34 252 1 15 3 42 0 0 0 50 0 6400000 1 #with borders
hdmi_timings 480 1 29 35 66 234 1 4 10 18 0 0 0 60 0 9600000 1
hdmi_timings=990 1 22 94 116 240 1 6 10 6 0 0 0 60 0 19200000 1
hdmi_timings=820 1 42 260 100 240 1 6 10 6 0 0 0 60 0 19200000 1
Perfect
hdmi_timings=320 1 12 32 44 240 1 6 10 6 0 0 0 60 0 6400000 1
hdmi_timings=506 1 8 44 52 240 1 6 10 6 0 0 0 60 0 9600000 1
hdmi_timings=996 1 24 96 120 240 1 3 10 6 0 0 0 60 0 19200000 1
hdmi_timings=1920 1 52 208 260 240 1 6 10 6 0 0 0 60 0 38400000 1
hdmi_timings=512 1 16 48 64 288 1 3 5 6 0 0 0 50 0 9600000 1
OK
hdmi_timings=996 1 24 96 120 240 1 3 10 6 0 0 0 60 0 19200000 1
hdmi_timings=506 1 8 44 52 240 1 6 10 6 0 0 0 60 0 9600000 1
hdmi_timings=336 1 10 30 34 252 1 15 3 42 0 0 0 50 0 6400000 1 #with borders
hdmi_timings 480 1 29 35 66 234 1 4 10 18 0 0 0 60 0 9600000 1
hdmi_timings=990 1 22 94 116 240 1 6 10 6 0 0 0 60 0 19200000 1
hdmi_timings=820 1 42 260 100 240 1 6 10 6 0 0 0 60 0 19200000 1
Perfect
hdmi_timings=320 1 12 32 44 240 1 6 10 6 0 0 0 60 0 6400000 1
hdmi_timings=506 1 8 44 52 240 1 6 10 6 0 0 0 60 0 9600000 1
hdmi_timings=996 1 24 96 120 240 1 3 10 6 0 0 0 60 0 19200000 1
hdmi_timings=1920 1 52 208 260 240 1 6 10 6 0 0 0 60 0 38400000 1
hdmi_timings=512 1 16 48 64 288 1 3 5 6 0 0 0 50 0 9600000 1
OK
hdmi_timings=996 1 24 96 120 240 1 3 10 6 0 0 0 60 0 19200000 1
hdmi_timings=506 1 8 44 52 240 1 6 10 6 0 0 0 60 0 9600000 1
hdmi_timings=336 1 10 30 34 252 1 15 3 42 0 0 0 50 0 6400000 1 #with borders
hdmi_timings 480 1 29 35 66 234 1 4 10 18 0 0 0 60 0 9600000 1
hdmi_timings=990 1 22 94 116 240 1 6 10 6 0 0 0 60 0 19200000 1
hdmi_timings=820 1 42 260 100 240 1 6 10 6 0 0 0 60 0 19200000 1
Perfect
hdmi_timings=320 1 12 32 44 240 1 6 10 6 0 0 0 60 0 6400000 1
hdmi_timings=506 1 8 44 52 240 1 6 10 6 0 0 0 60 0 9600000 1
hdmi_timings=996 1 24 96 120 240 1 3 10 6 0 0 0 60 0 19200000 1
hdmi_timings=1920 1 52 208 260 240 1 6 10 6 0 0 0 60 0 38400000 1
hdmi_timings=512 1 16 48 64 288 1 3 5 6 0 0 0 50 0 9600000 1
```

Next, we need to tell Recalbox how to display EmulationStation and the default resolution of emulators.

* Open the file [recalbox.conf](./../../../basic-usage/getting-started/recalboxconf-file)
* Find the line `global.videomode`.
* Specify the value `DMT 87 HDMI` like this:

```ini
global.videomode=DMT 87 HDMI
```

![VGA666 with a small TV screen](/tutorials/video/crt/vga666-3.png)

RetroArch allows a specific basic configuration for the video output. You can make one configuration file per kernel. To do this, create the configuration files in the directory `/share/system/configs/retroarch`.

We use the custom aspect ratio "23" to modify the visual of RetroArch.

For snes | snes.cfg

```ini
aspect_ratio_index = "23"
custom_viewport_width = "900"
custom_viewport_height = "224"
custom_viewport_x = "58"
custom_viewport_y = "13"
```

For megadrive | megadrive.cfg

```ini
aspect_ratio_index = "23"
custom_viewport_width = "900"
custom_viewport_height = "224"
custom_viewport_x = "58"
custom_viewport_y = "13"
```

For nes | nes.cfg

```ini
aspect_ratio_index = "23"
custom_viewport_width = "900"
custom_viewport_height = "224"
custom_viewport_x = "58"
custom_viewport_y = "13"
```

For fba | fba_libretro.cfg

```ini
aspect_ratio_index = "23"
custom_viewport_width = "900"
custom_viewport_height = "224"
custom_viewport_x = "58"
custom_viewport_y = "13"
```

For mame | mame.cfg

```ini
aspect_ratio_index = "23"
custom_viewport_width = "900"
custom_viewport_height = "224"
custom_viewport_x = "58"
custom_viewport_y = "13"
```

![Image details](/tutorials/video/crt/vga666-4.png)

In the emulators, you will probably have to manually adjust the custom variables in the display window. To do this, you need to launch the RetroArch menu by pressing `Hotkey + B`.

## RGBPi: Scart cable for CRT televisions

Enjoy the simplicity of a ready to plug cable to your CRT TV and your RaspberryPi with our custom operating system in RGB and PixelPerfect resolutions for all games, with improved stereo sound and a new auto-switching feature, compatible with 2B, 3B, 3B+ models for the moment...

New automatic switching function on the AV channel in 4:3, 18-bit RGB without conversions and clean CSync signal generated on the PCB in the SCART providing the best possible signal quality and lowest loss, two enhanced filtered audio channels, all pre-configured.

![RGB-Pi adapter](/tutorials/video/crt/rgbpi1.png)

![RGB-Pi adapter details](/tutorials/video/crt/rgbpi2.png)

If you want to use it with Recalbox, you can use the same procedure as for the VGA666, with some small modifications. Here are the contents of the file [recalbox-user-config.txt](./../../system/modification/configtxt-configuration):

```ini
hdmi_timings=320 1 10 30 40 240 1 3 4 6 0 0 0 60 0 6400000 1

## RGB-Pi Custom Configuration
## IMPORTANT! hdmi_timings must be always in the first line
## hdmi_timings=<h_active_pixels> <h_sync_polarity <h_front_porch> <h_sync_pulse
> <h_back_porch> <v_active_lines> <v_sync_polarity> <v_front_porch> <v_sync_puls
e> <v_back_porch> <v_sync_offset_a> <v_sync_offset_b> <pixel_rep> <frame_rate> <
interlaced> <pixel_freq> <aspect_ratio>
## Usable Pixel clock are : 4800000 - 6400000 - 9600000 - 19200000 and anything 
over 38400000

disable_overscan=1
hdmi_force_hotplug=1
config_hdmi_boost=0
dtparam=audio=on
display_rotate=0
audio_pwm_mode=2
disable_audio_dither=1
hdmi_drive=2
boot_delay=3
disable_splash=1
avoid_safe_mode=1
dtoverlay=pwm-2chan,pin=18,func=2,pin2=19,func2=2
framebuffer_depth=32
framebuffer_ignore_alpha=1
#dtoverlay=rgb-pi
#dpi24 is equal to rgb-pi
dtoverlay=dpi24
enable_dpi_lcd=1
display_default_lcd=1
dpi_output_format=6
dpi_group=2
dpi_mode=87
kernel=zImage
overscan_scale=0
avoid_warnings=1
gpu_mem_256=64
gpu_mem_512=128
gpu_mem_1024=256
dtparam=i2c_vc=on
dtoverlay=i2c-gpio,i2c_gpio_sda=10,i2c_gpio_scl=11
```

## Pi2Scart

The Pi2SCART is a circuit board that allows you to add a 15Khz audio/video output to your Raspberry Pi on a SCART socket. The PI2SCART connects very simply to the GPIO interface of the Raspberry. The sound is taken from the headphone jack via an audio cable. The PI2SCART does not need any additional power to operate.

![Pi2Scart module connected to a Pi](/tutorials/video/crt/rgbpi3.png)

Like the two previous systems, the Pi2Scart uses the DPI bus and the GPIO ports to work. It uses the VGA66 overlay and `dpi_mode 87`. To make it work, you need to modify the file [recalbox-user-config.txt](./../../system/modification/configtxt-configuration) and add the following lines:

```ini
disable_audio_dither=1
dtparam=audio=on
dtoverlay=vga666
enable_dpi_lcd=1
display_default_lcd=1
dpi_group=2
dpi_mode=87
hdmi_timings=320 1 16 30 34 240 1 2 3 22 0 0 0 60 0 6400000 1 #240p
```

>This resolution setting corresponds to a resolution common to all emulators which may not be perfectly adapted to your CRT monitor. It is up to you to adjust either your screen (factory menu, potentiometer) or via Recalbox (overscan or via RetroArch). In the last case (RetroArch), you can override the basic configuration to define a specific video resolution.
{.is-warning}

>When testing on a terminal or bartop, don't forget to change the `controllers.gpio.enabled` option which enables GPIO commands by changing the value from 1 to 0.
{.is-warning}

RetroArch allows a specific basic configuration for the video output. We can make one configuration file per core. To do this, create the configuration files in the directory `/share/system/configs/retroarch`.

We use the custom aspect ratio "23" to modify the visual of RetroArch.

For snes | snes.cfg

```ini
aspect_ratio_index = "23"
custom_viewport_width = "900"
custom_viewport_height = "224"
custom_viewport_x = "58"
custom_viewport_y = "13"
```