---
title: Supervision
description: 
published: true
date: 2021-08-09T22:08:56.513Z
tags: handhelds, watara, supervision, 7.2+
editor: markdown
dateCreated: 2021-07-06T09:36:10.030Z
---

![](/emulators/handheld/supervision.svg){.align-center}

## Technical data

* **Manufacturer**: Watara
* **Year of release**: 1992
* **CPU**: 8-bit 65SC02 @ 4 MHz
* **RAM**: 8kB
* **VRAM**: 8kB
* **Display**: 160x160 pixels, 4 shades of grey
* **Sound**: 2 Tonal and 1 Noise Channel plus additional audio DMA stereo output

## Presentation

The **Watara Supervision** is a monochrome handheld console designed in Hong Kong and introduced in 1992 as a competitor to the Game Boy. It was distributed in France by the company AudioSonic.

The Supervision's main arguments to compete with the Game Boy were its larger screen (6 × 6 cm) and its low launch price. However, the console was not as successful as expected, mainly because of its very small software library and the quality of the screen. Note the low resolution of the screen which, aggravated by the size of the screen, makes the display most horrible and archaic (cf. TI-80/81 or the first Nokia cell phones), and also that its cartridges strongly resemble those of Mega Duck/Cougar Boy which are moreover superior to the Watara Supervision.

## Emulators

[Libretro Potator](libretro-potator)