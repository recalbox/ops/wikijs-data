---
title: Mega Drive
description: Sega Mega Drive
published: true
date: 2024-07-26T07:54:34.464Z
tags: megadrive, sega, consolas
editor: markdown
dateCreated: 2023-09-03T19:31:58.809Z
---

![](/emulators/consoles/megadrive.svg){.align-center}

## Detalles técnicos

* **Fabricante :** Sega
* **Año de lanzamiento :** 1988
* **Cantidad vendida :** 30 millones
* **Juego más vendido :** Sonic the Hedgehog
* **Procesador principal:** Motorola 68000 de 16 bits a 7,6 MHz
* **Segundo procesador:** Zilog Z80 de 8 bits a 3,58 Mhz (procesador de sonido)
* **RAM:** 64 kB
* **VRAM:** 64kB
* **Sonido RAM** : 8kB
* **Vídeo**: Sega VDP de 16 bits
* **Resolución**: 320×224 píxeles, 64 colores (de una paleta de 512 colores)
* **Chip de sonido**: Yamaha YM2612 FM y TI SN76489 PSG

## Presentación

La **Mega Drive** o **Sega Genesis** en Norteamérica, es una videoconsola de cuarta generación diseñada y comercializada por el fabricante japonés Sega Enterprises, Ltd. La Mega Drive es la tercera consola de Sega, sucesora de la Master System. Sega lanzó por primera vez la consola en Japón con el nombre de Mega Drive en 1988, antes de lanzarla en Norteamérica en 1989, donde pasó a llamarse Genesis. En 1990, la consola se lanzó en la mayoría de los demás territorios como Mega Drive.

Diseñada por un equipo de investigación y desarrollo supervisado por Masami Ishikawa, el hardware de la Mega Drive se adaptó del System 16 para terminales arcade. El sistema es compatible con una biblioteca de más de 900 juegos creados por Sega y una amplia gama de editores de terceros, publicados en forma de cartuchos de videojuegos.

En Japón, la Mega Drive no se vendió tan bien como sus dos principales rivales, la Super Nintendo de Nintendo y la PC-Engine de NEC. Sin embargo, gracias a su dilatada experiencia en los salones recreativos y al hecho de haber pillado desprevenida a Nintendo en el mercado de las consolas de 16 bits, Sega consiguió posicionar bien la Mega Drive en Norteamérica y Europa, donde cosechó un gran éxito, acaparando la mayor parte del mercado de 16 bits en varios territorios, sobre todo en Estados Unidos y el Reino Unido. Los principales factores de su éxito fueron sus ports de videojuegos arcade, la popularidad de la serie Sonic the Hedgehog, una serie de franquicias de juegos deportivos muy conocidas y un marketing agresivo dirigido a los jóvenes, que posicionó la consola como la consola "guay" para los adolescentes.

En 1990, Nintendo lanzó su Super Nintendo, lo que dio lugar a una feroz batalla entre los dos fabricantes durante los años siguientes para hacerse con la mayor parte posible del mercado de consolas de 16 bits, sobre todo en Norteamérica y Europa. Esta rivalidad, a menudo calificada por los periodistas como la primera "guerra de consolas" real, ha dejado su huella en la memoria colectiva de los jugadores de videojuegos, sobre todo a través del enfrentamiento ficticio y simbólico entre las respectivas mascotas de los dos fabricantes, Mario de Nintendo y Sonic de Sega. A medida que esta competición atrae cada vez más la atención pública de la industria del videojuego, la Mega Drive y varios de sus juegos más destacados están generando una gran cantidad de análisis sobre cuestiones relacionadas con la ingeniería inversa y la violencia en los videojuegos. La polémica en torno a títulos violentos como Night Trap y Mortal Kombat llevó a Sega a crear el Videogame Rating Council, predecesor del Entertainment Software Rating Board.

Al final de su ciclo de vida, aunque acabó siendo superada por la Super Nintendo, sobre todo en Japón, la consola vendió alrededor de 40 millones de unidades en todo el mundo. Fue la consola más exitosa de Sega. La Mega Drive y sus juegos siguen siendo populares entre fans, coleccionistas, aficionados a la música de videojuegos y entusiastas de la emulación. En la década de 2010 se siguen produciendo reediciones de la consola con licencia, y varios desarrolladores independientes de videojuegos siguen produciendo juegos compatibles con el sistema. Lanzada en 1994, la Saturn es la sucesora de la Mega Drive.

## Emuladores

[Libretro FBNeo](libretro-fbneo)
[Libretro GenesisplusGX](libretro-genesisplusgx)
[Libretro GenesisplusGX WIDE](libretro-genesisplusgxwide)
[Libretro Picodrive](libretro-picodrive)