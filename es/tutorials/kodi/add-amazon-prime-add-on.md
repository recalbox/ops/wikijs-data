---
title: Añadir la extensión Amazon Prime
description: 
published: true
date: 2024-09-29T14:08:21.244Z
tags: kodi, amazon, extensiones
editor: markdown
dateCreated: 2023-09-15T07:09:58.075Z
---

## Introducción

Kodi posee una extensión que permite leer los vídeos de Amazon Prime. Este tutorial os explica cómo instalarla.

>Esta extensión no es compatible con los Raspberry Pi 4 y Pi 5 porque el módulo de vídeo WideVine para los ARM 64 bits no existe a día de hoy.
>{.is-info}

## Prerequisitos

Tenéis que comprobar varios puntos antes de empezar:

1. Desactivar la doble autenticación en Amazon. En la mayoría de los casos, el login a través de Kodi no funciona a cause de esto.

2. Activar la posibilidad de instalar extensiones a través de ficheros zip en Kodi:
    * `Parámetros` > `Sistema` > `Extensiones`.
    * Activad la opción `Fuentes desconocidas`. Pulsad OK cuando se os pida confirmación.

## Instalación

1. Lanzad Kodi.

2. En primer lugar hay que instalar un fichero zip que contiene información sobre el repositorio donde se encuentra la extensión Amazon Prime. Id a la siguiente dirección y descargad el último repositorio de plugins:

https://github.com/Sandmann79/xbmc/releases

3. Copiad el fichero zip descargado dentro de vuestra Recalbox dentro de la carpeta `SHARE`

4. En Kodi, id a  `Extensiones` > `Instalación de ficheros Zip` y seleccionad el fichero zip que acabáis de copiar. Confirmad las siguientes ventanas.

5. Una vez hecho esto, dentro de `Extensiones`, dirigíros a  `Instalación desde un repositorio`.

6. Buscad la extensión dentro de `Sandmann79s Repository` > `Extensions Vidéos` > `Amazon VOD`. Abrid la extensión para instalarla. Confirmad las dependencias en las siguientes ventanas.

7. Una vez instalada, la extensión sera visible en la lista de extensiones instaladas.

## Utilización

Debéis identificaros con Amazon para poder utilizar la extensión.

1. Abrid la extensión.

2. Automáticamente se abrirá la ventana de opciones. A la izquierda, id a `Conexión` y seleccionad `Iniciar sesión...`.

3. Deberéis introducir vuestra dirección de correo electrónico y la contraseña en 2 ventanas sucesivas.

>Probablemente veréis ventanas en las que tendréis que volver a escribir tu contraseña y un captcha. Esto es perfectamente normal, Amazon hace sus propias comprobaciones, aunque puedan ser demasiado repetitivas.
>{.is-info}

4. Al final tendréis una ventana que os indicará que estáis conectados correctamente.

5. A partir de este momento podéis disfrutar de Amazon Prime en vuestro Kodi.