---
title: Mise à jour du bootloader du Raspberry Pi 4/400/5
description: 
published: true
date: 2024-09-18T14:37:18.580Z
tags: update, rpi4, bootloader, rpi400, rpi5
editor: markdown
dateCreated: 2023-01-28T00:51:06.793Z
---

## Introduction

Le bootloader (appelé aussi eeprom) du Raspberry est un micrologiciel qui permet de démarrer le Raspberry Pi 4/400/5.

Il est conseillé de mettre à jour le bootloader régulièrement, ce qui permet de corriger certains bugs dans l'utilisation du Raspberry Pi 4/400/5.

## Procédure

Le processus de mise à jour du bootloader est très simple. Il vous faut uniquement votre Recalbox à jour à la dernière version.

Dans l'interface, appuyez sur `START` et allez dans `PARAMÈTRES AVANCÉES` > `MISE À JOUR DU BOOTLOADER`.

Une fois validé dans les menus, vous aurez une fenêtre vous indiquant la version installée et la version disponible.

![](/tutorials/utilities/upgrade-rpi4-bootloader/bootloaderfr1.png){.full-width}

Valisez la mise à jour avec `METTRE À JOUR`. Moins de 10 secondes plus tard, vous devriez avoir un message de mise à jour bien effectuée.

![](/tutorials/utilities/upgrade-rpi4-bootloader/bootloaderfr2.png){.full-width}

Une fois le message validé, vous serez amené à redémarrer. Validez le redémarrage pour utiliser le bootloader mis à jour.

![](/tutorials/utilities/upgrade-rpi4-bootloader/bootloaderfr3.png){.full-width}

Vous pouvez vérifier si le bootloader est bien à jour en retournant dans le même menu. Vous aurez alors un message vous indiquant que le bootloader est à jour.

![](/tutorials/utilities/upgrade-rpi4-bootloader/bootloaderfr4.png){.full-width}