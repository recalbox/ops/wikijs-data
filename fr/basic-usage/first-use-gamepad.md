---
title: Première utilisation d'une manette sur Recalbox
description: Pour configurer une manette pour la première fois sur Recalbox
published: true
date: 2023-12-03T01:16:15.376Z
tags: usb, configuration, bluetooth, 8bitdo, manette, manettes, première utilisation
editor: markdown
dateCreated: 2023-12-02T01:31:07.862Z
---

Ce tutoriel vous indique comment connecter et utiliser une manette pour la première fois sur Recalbox.

# 8Bitdo
>**COMPATIBILITÉ**
>Avant toute chose, vérifiez la compatibilité de votre [manette 8bitdo avec Recalbox](../hardware-compatibility/compatible-devices/8bitdo-on-recalbox).
>Les manettes 8Bitdo ne sont pas reconnues en mode Switch et Apple/Mac.
{.is-warning}

>**EN CAS DE PROBLÈME**
>Si la procédure décrite pour votre manette ne fonctionne pas, veuillez [mettre à jour le firmware](https://support.8bitdo.com/firmware-updater.html) de votre manette, en prenant soin de vérifier quelle est la version qui est conseillée [ici](../hardware-compatibility/compatible-devices/8bitdo-on-recalbox).
{.is-warning}

## SF30 Pro / SN30 Pro / SN30 Pro+ - Version Bluetooth
>**ALLUMER SA MANETTE**
>Vérifiez que **TOUTES** les LEDs de la manette sont éteintes, puis :
>- En USB : Brancher le câble USB
>- En Bluetooth : Appuyer sur `START`
>
>Au moins une des 4 LEDs de la tranche inférieure de la manette va s'allumer.
{.is-info}

>**ÉTEINDRE SA MANETTE**
>Si au moins une des LEDs de la manette est allumée :
>- Débrancher le câble USB s'il y en a un
>- Maintenir `START` appuyé pendant 3 secondes
>- Si ça ne fonctionne pas, maintenir `START` appuyé pendant 8 secondes
>
>Lorsque les LEDs de la manette s'éteignent, cela signifie que la manette est éteinte.
{.is-info}

>Veuillez suivre la procédure dans l'ordre
{.is-warning}

### 1 - Configuration du mode (étape **OBLIGATOIRE**)
>**MODE CONSEILLÉ**
>Ces manettes ne sont compatibles qu'en mode Windows (X-Input) ou Androïd (D-Input).
>Le mode Switch et Apple/Mac ne sont **PAS** compatibles.
>
>Nous conseillons d'utiliser le mode **Androïd (D-Input)**.
{.is-warning}

Lorsque la manette est éteinte (**TOUTES** les LEDs de la manette sont donc éteintes), effectuez les étapes suivantes :
#### En mode Androïd (D-Input) :
- Maintenez la touche `B` appuyée
- Appuyez sur `START` pour que la manette s'allume
- Relâchez la touche `B` (ou `X` pour le mode Windows/X-Input) ainsi que la touche START
- La première LED de la tranche inférieur de la manette va s'allumer et clignoter
- Maintenez `START` appuyé pendant 3 secondes pour éteindre la manette

***OU***

#### En mode Windows (X-Input) :
- Maintenez la touche `X` appuyée
- Appuyez sur `START` pour que la manette s'allume
- Relâchez la touche `X` ainsi que la touche START
- Les 2 premières LEDs de la tranche inférieur de la manette vont s'allumer et clignoter
- Maintenez `START` appuyé pendant 3 secondes pour éteindre la manette

***Le mode de la manette est désormais configuré et sauvegardé dans la manette***

### 2 - Connexion en USB :
>Veuillez d'abord [configurer un mode](./first-use-gamepad#h-1-configuration-du-mode-%C3%A9tape-obligatoire) compatible sur votre manette pour pouvoir appliquer les étapes suivantes.
{.is-warning}

>Si vous souhaitez utiliser votre manette en Bluetooth, cette étape est à passer si la barre d'appairage Bluetooth est affichée en haut à gauche de l'écran et que le temps n'est pas totalement écoulé.
{.is-info}

- Connectez un câble USB reliant votre manette à votre Recalbox
- Votre manette devrait être utilisable dans Recalbox

### 3 - Connexion en Bluetooth :
#### Sans l'appairage Bluetooth démarré
>Cette étape est à réaliser **UNIQUEMENT** si la barre d'appairage Bluetooth n'est pas affichée en haut à gauche de l'écran.
>Sinon, rendez-vous à la section suivante [***"Avec l'appairage Bluetooth démarré"***](./first-use-gamepad#avec-lappairage-bluetooth-d%C3%A9marr%C3%A9).
{.is-info}

>Veuillez d'abord configurer un mode compatible sur votre manette pour pouvoir appliquer les étapes suivantes.
{.is-warning}

Réalisez les actions suivantes avec la manette 8Bitdo que vous êtes en train de configurer en utilisant un câble USB (voir paragraphe précédent) ou avec une autre manette déjà configurée sur Recalbox :
- Dans l'écran principal de Recalbox, appuyez sur `START` pour ouvrir le menu principal
- Allez dans le menu `REGLAGES MANETTES`
- Allez dans `ASSOCIER DES MANETTES BLUETOOTH`
- Une fenêtre indiquant que l'appairage Bluetooth va démarrer apparaît. Validez avec `B`
- Un nouvel écran listant les manettes disponibles apparaît
- Débranchez le câble USB de la manette. La 1ère LED va clignoter
- Maintenez appuyé pendant 3 secondes le bouton situé sur la tranche supérieure de la manette (à côté de la prise USB)
- Les 4 LEDs de la tranche inférieure de la manette vont s'allumer et s'éteindre l'une après l'autre, de gauche à droite puis de droite à gauche
- La manette devrait être appairée au bout de quelques secondes. Lorsque c'est le cas, une seule LED sera allumée de manière fixe, une popup indiquant que la manette est connectée apparaît en haut à droite de l'écran et la manette apparaît dans la liste des manettes
- Sortez de l'écran en allant vers le bas pour atteindre le bouton OK, puis validez

>Désormais, votre manette est appairée avec votre Recalbox.
Au prochain démarrage de Recalbox, vous n'avez qu'à allumer votre manette pour qu'elle se reconnecte automatiquement en Bluetooth.
{.is-info}

#### Avec l'appairage Bluetooth démarré
>Cette étape est à réaliser **UNIQUEMENT** si la barre d'appairage Bluetooth est affichée en haut à gauche de l'écran.
>Sinon, rendez-vous à la section précédente [***"Sans l'appairage Bluetooth démarré"***](./first-use-gamepad#sans-lappairage-bluetooth-d%C3%A9marr%C3%A9).
{.is-info}

>Les actions suivantes sont à réaliser avec la manette **ÉTEINTE**
{.is-warning}

- Maintenez appuyé pendant 3 secondes le bouton situé sur la tranche supérieure de la manette (à côté de la prise USB)
- Les 4 LEDs de la tranche inférieure de la manette vont s'allumer et s'éteindre l'une après l'autre, de gauche à droite puis de droite à gauche
- La manette devrait être appairée au bout de quelques secondes. Lorsque c'est le cas, une seule LED sera allumée de manière fixe, une popup indiquant que la manette est connectée apparaît en haut à droite de l'écran et la manette apparaît dans la liste des manettes
- Sortez de l'écran en allant vers le bas pour atteindre le bouton OK, puis validez

>Désormais, votre manette est appairée avec votre Recalbox.
Au prochain démarrage de Recalbox, vous n'avez qu'à allumer votre manette pour qu'elle se reconnecte automatiquement en Bluetooth.
{.is-info}

# Sony 
## Dual Shock 4 / Dual Sense
>**ALLUMER SA MANETTE**
>Vérifiez que la barre de lumière de la manette est éteinte, puis :
>- En USB : Branchez le câble USB
>- En Bluetooth : Appuyez brièvement sur le bouton `PS HOME`
>
>La barre de lumière de la manette va s'allumer et s'éteindre par alternance.
{.is-info}

>**ÉTEINDRE SA MANETTE**
>Si la barre de lumière de la manette est allumée :
>- Débranchez le câble USB s'il y en a un
>- Maintenez le bouton `PS HOME` appuyé pendant 8 secondes
>
>Lorsque la barre de lumière de la manette s'éteint, cela signifie que la manette est éteinte.
{.is-info}

### 1 - Connexion en USB :
>Si vous souhaitez utiliser votre manette en Bluetooth, cette étape est à passer si la barre d'appairage Bluetooth est affichée en haut à gauche de l'écran et que le temps n'est pas totalement écoulé.
{.is-info}

- Connectez un câble USB reliant votre manette à votre Recalbox
- Votre manette devrait être utilisable dans Recalbox

### 2 - Connexion en Bluetooth :
#### Sans l'appairage Bluetooth démarré
>Cette étape est à réaliser **UNIQUEMENT** si la barre d'appairage Bluetooth n'est pas affichée en haut à gauche de l'écran.
>Sinon, rendez-vous à la section suivante [***"Avec l'appairage Bluetooth démarré"***](./first-use-gamepad#avec-lappairage-bluetooth-d%C3%A9marr%C3%A9-1).
{.is-info}

Réalisez les actions suivantes avec la manette Sony que vous êtes en train de configurer en utilisant un câble USB (voir paragraphe précédent) ou avec une autre manette déjà configurée sur Recalbox :
- Dans l'écran principal de Recalbox, appuyez sur `START` pour ouvrir le menu principal
- Allez dans le menu `REGLAGES MANETTES`
- Allez dans `ASSOCIER DES MANETTES BLUETOOTH`
- Une fenêtre indiquant que l'appairage Bluetooth va démarrer apparaît. Validez avec `B`
- Un nouvel écran listant les manettes disponibles apparaît
- Débranchez le câble USB de la manette. La barre de lumière de la manette va clignoter
- Maintenez le bouton `SHARE` appuyé
- Appuyez sur le bouton `PS HOME`
- Lâchez les boutons `SHARE` et `PS HOME`
- La barre de lumière de la manette va clignoter rapidement jusqu'à ce que la manette s'appaire à votre Recalbox
- La manette devrait être appairée au bout de quelques secondes. Lorsque c'est le cas, la  barre de lumière de la manette sera allumée de manière fixe, une popup indiquant que la manette est connectée apparaît en haut à droite de l'écran et la manette apparaît dans la liste des manettes
- Sortez de l'écran en allant vers le bas pour atteindre le bouton OK, puis validez avec `CROIX`

>Désormais, votre manette est appairée avec votre Recalbox.
Au prochain démarrage de Recalbox, vous n'avez qu'à allumer votre manette pour qu'elle se reconnecte automatiquement en Bluetooth.
{.is-info}

#### Avec l'appairage Bluetooth démarré
>Cette étape est à réaliser **UNIQUEMENT** si la barre d'appairage Bluetooth est affichée en haut à gauche de l'écran.
>Sinon, rendez-vous à la section précédente [***"Sans l'appairage Bluetooth démarré"***](./first-use-gamepad#sans-lappairage-bluetooth-d%C3%A9marr%C3%A9-1).
{.is-info}

>Les actions suivantes sont à réaliser avec la manette **ÉTEINTE**
{.is-warning}

- Maintenez le bouton `SHARE` appuyé
- Appuyez sur le bouton `PS HOME`
- Lâchez les boutons `SHARE` et `PS HOME`
- La barre de lumière de la manette va clignoter rapidement jusqu'à ce que la manette s'appaire à votre Recalbox
- La manette devrait être appairée au bout de quelques secondes. Lorsque c'est le cas, la  barre de lumière de la manette sera allumée de manière fixe, une popup indiquant que la manette est connectée apparaît en haut à droite de l'écran et la manette apparaît dans la liste des manettes
- Sortez de l'écran en allant vers le bas pour atteindre le bouton OK, puis validez avec `CROIX`

>Désormais, votre manette est appairée avec votre Recalbox.
Au prochain démarrage de Recalbox, vous n'avez qu'à allumer votre manette pour qu'elle se reconnecte automatiquement en Bluetooth.
{.is-info}
