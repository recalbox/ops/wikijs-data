---
title: Gestionnaire web
description: 
published: true
date: 2025-03-16T15:48:38.080Z
tags: web, gestionnaire
editor: markdown
dateCreated: 2021-05-21T07:54:20.497Z
---

## Pour vous connecter

Il suffit de saisir dans votre navigateur web simplement son nom :

## {.tabset}

### Windows

[http://recalbox/](http://recalbox/)

Si cela ne fonctionne pas, vous pouvez utiliser [l'adresse IP de votre Recalbox](./../../tutorials/network/ip/discover-recalbox-ip).

### Linux et macOS

[http://recalbox.local/](http://recalbox.local/)

Si cela ne fonctionne pas, vous pouvez utiliser [l'adresse IP de votre Recalbox](./../../tutorials/network/ip/discover-recalbox-ip).

## Interface

Lorsque vous êtes sur la page principale, vous pouvez :

* Modifier les options qui composent le fichier `recalbox.conf`.
* Ajouter/Supprimer des roms.
* Créer une archive de support.
* Surveiller votre board - température, utilisation des cores et du stockage (monitoring).
* Consulter sur la page d'accueil le système en cours et, le cas échéant, le jeu en cours.
* Manipuler vos captures (captures d'écran ou vidéos).
* Gérer votre clé Patreon et les options associés.

## Désactivation du Gestionnaire web

Par défaut, le gestionnaire web démarre automatiquement. Si vous souhaitez, vous pouvez le désactiver.

* Dans EmulationStation, appuyez sur le bouton `START`.
* Allez dans `PARAMÈTRES AVANCÉS`.
* Mettez l'option `GESTIONNAIRE WEB RECALBOX` sur OFF.