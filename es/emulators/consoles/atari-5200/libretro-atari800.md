---
title: Libretro Atari800
description: 
published: true
date: 2024-07-29T13:52:58.620Z
tags: libretro, atari, atari-800, 5200, atari-5200
editor: markdown
dateCreated: 2023-09-06T08:45:24.636Z
---

## ![](/emulators/license.svg) Licencia

Este núcleo está bajo la licencia [**GPLv2**](https://github.com/atari800/atari800/blob/master/COPYING).

## ![](/emulators/compatibility.png) Compatibilidad

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Funcionalidades

| Funcionalidad | Soportado |
| :---: | :---: |
| Capturas de pantalla | ✔ |
| Guardados | - |
| Guardados instantáneos | - |
| Opciones del núcleo | ✔ |
| Trucos nativos | - |
| Controles | ✔ |
| Remapeo | ✔ |

## ![](/emulators/bios.svg) BIOS

### Lista de BIOS obligatorios

| Nombre de archivo | Descripción | MD5 | Proporcionado |
| :---: | :---: | :---: | :---: |
| 5200.rom | BIOS 5200 | 281f20ea4320404ec820fb7ec0693b38 | ❌ |

### Ubicación

Coloque los BIOS de la siguiente manera:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 atari5200
┃ ┃ ┃ ┃ ┣ 🗒 **5200.rom**

## ![](/emulators/roms.png) Roms

### Extensiones soportadas

Los roms deben tener las siguientes extensiones:

* .a52
* .zip

Este sistema soporta roms comprimidos en formato .zip. Sin embargo, solo se trata de un archivo.

Los archivos contenidos en los .zip deben corresponder a las extensiones mencionadas anteriormente.
Además, cada archivo .zip no debe contener más de una rom comprimida.

### Ubicación

Coloque los roms de la siguiente manera:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 atari5200
┃ ┃ ┃ ┃ ┣ 🗒 **juego.zip**

>Se recomienda encarecidamente el uso de roms en formato **TOSEC**.
{.is-success}

>Para más información sobre los roms, visite [este tutorial](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

>Para poder conservar sus configuraciones personalizadas durante una actualización, le recomendamos utilizar nuestra funcionalidad [Sobrecargas de configuración](./../../../advanced-usage/configuration-override).
{.is-info}

### Acceder a las opciones

Puede configurar diversas opciones de dos maneras diferentes.

* A través del menú RetroArch:

┣ 📁 Menú RetroArch
┃ ┣ 📁 Opciones del núcleo
┃ ┃ ┣ 🧩 Name_option

* A través del archivo `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Opciones del núcleo

| Opciones | Valor de las opciones | Variable (para sobrecarga) | Valores de las variables |
| --- | --- | --- | --- |
| Video Standard | `NTSC` ✅ / `PAL` | `atari800_ntscpal` | `NTSC` / `PAL` |
| Hi-Res Artifacting | `None` ✅ / `blue/brown 1` / `blue/brown 2` / `GTIA` / `CTIA` | `atari800_artifacting` | `none` / `blue/brown 1` / `blue/brown 2` / `GTIA` / `CTIA` |
| Internal resolution | `336x240` ✅ / `320x240` / `384x240` / `384x272` / `384x288` / `400x300` | `atari800_resolution` | `336x240` / `320x240` / `384x240` / `384x272` / `384x288` / `400x300` |
| Controller Hacks | `none` ✅ / `Dual Stick` / `Swap Ports` | `atari800_opt2` | `none` / `enabled` / `Swap Ports` |
| Activate Paddle Mode | `Desactivado` ✅ /  `Activado` | `paddle_active` | `disabled` / `enabled` |
| Paddle Movement Speed | `1` / `2` / `3` ✅ / `4` / `5` / `6` / `7` / `8` / `9` | `paddle_movement_speed` | `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` |
| Digital Joystick Sensitivity | `5%` / `10%` / `15%` / `20%` / `25%` / `30%` / `35%` / `40%` / `45%` / `50%` / `55%` / `60%` / `65%` / `70%` / `75%` / `80%` / `85%` / `90%` / `95%` / `100%` ✅ | `pot_digital_sensitivity` | `5` / `10` / `15` / `20` / `25` / `30` / `35` / `40` / `45` / `50` / `55` / `60` / `65` / `70` / `75` / `80` / `85` / `90` / `95` / `100` |
| Analog Joystick Sensitivity | `5%` / `10%` / `15%` / `20%` / `25%` / `30%` / `35%` / `40%` / `45%` / `50%` / `55%` / `60%` / `65%` / `70%` / `75%` / `80%` / `85%` / `90%` / `95%` / `100%` ✅ | `pot_analog_sensitivity` | `5` / `10` / `15` / `20` / `25` / `30` / `35` / `40` / `45` / `50` / `55` / `60` / `65` / `70` / `75` / `80` / `85` / `90` / `95` / `100` |
| Analog Joystick Deadzone | `0%` / `3%` / `5%` / `7%` / `10%` / `13%` / `15%` ✅ / `17%` / `20%` / `23%` / `25%` / `27%` / `30%` | `pot_analog_deadzone` | `0` / `3` / `5` / `7` / `10` / `13` / `15` / `17` / `20` / `23` / `25` / `27` / `30` |
| Retroarch Keyboard type | `poll` ✅ / `callback` | `atari800_keyboard` | `poll` / `callback` |
| Atari Keyboard Defines | `informational` ✅ | `keyboard_defines` | `informational` |
| Atari System | `Atari 400/800 (OS B)` ✅ / `Atari 800XL (64K)` / `Atari 130XE (128K)` / `Modern XL/XE(320K Comby Shop)` / `Modern XL/XE(576K)` / `Modern XL/XE(1088K)` / `Atari 5200 Super System` | `atari800_system` | `400/800 (OS B)` / `800XL (64K)` / `130XE (128K)` / `Modern XL/XE(320K CS)` / `Modern XL/XE(576K)` / `Modern XL/XE(1088K)` / `5200` |
| Internal BASIC (hold OPTION on boot) | `Desactivado` ✅ / `Activado` | `atari800_internalbasic` | `disabled` / `enabled` |
| SIO Acceleration | `Desactivado` / `Activado` ✅ | `atari800_sioaccel` | `disabled` / `enabled` |
| Boot from Cassette (Reboot) | `Desactivado` ✅ / `Activado` | `atari800_cassboot` | `disabled` / `enabled` |
| Autodetect Atari Cartridge Type (Restart) | `Desactivado` ✅ / `Activado` | `atari800_opt1` | `disabled` / `enabled` |

## ![](/emulators/external-links.png) Enlaces externos

* **Código fuente utilizado** : [https://github.com/atari800/atari800/](https://github.com/atari800/atari800/)
* **Documentación Libretro** : [https://docs.libretro.com/library/atari800/](https://docs.libretro.com/library/atari800/)
