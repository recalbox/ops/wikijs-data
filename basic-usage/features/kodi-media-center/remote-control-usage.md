---
title: Utiliser une télécommande infrarouge
description: 
published: true
date: 2021-09-04T17:49:30.835Z
tags: kodi, télécommande
editor: markdown
dateCreated: 2021-05-21T08:12:03.755Z
---

La Recalbox peut être controlée via n'importe quelle télécommande Infrarouge. Cette fonctionnalité est pour Kodi.

Cela vous coutera environ 2€ et est très simple à mettre en place.

## I - Prérequis

### A - Un récepteur Infrarouge

Pour fonctionner, vous allez avoir besoin d'un récepteur IR comme un module 38KHz TSOP4838 ; notez que cela peut aussi fonctionner avec certains autres composants. Vous pouvez en acheter sur internet pour environ 1€ chez n'importe quel revendeur d'électronique ou en ligne.

![Récepteur IR](/basic-usage/features/kodi/ir-control/ir1.jpg)

### B - Des câbles dupont femelle/femelle

Afin de raccorder facilement votre récepteur sans soudure, vous aurez besoin de 3 câbles dupont F/F. Vous pouvez vous en procurer chez les mêmes revendeurs que votre récepteur pour quelques centimes.

![Câbles dupont F/F](/basic-usage/features/kodi/ir-control/ir2.jpg)

### C - Schéma

Pour connecter le récepteur et les câbles, suivez le schéma suivant :

![](/basic-usage/features/kodi/ir-control/ir3.png)

Exemple de montage :

![](/basic-usage/features/kodi/ir-control/ir4.jpg)

### D - Télécommande

Quasiment toutes les télécommandes sont supportés, à condition qu'elles utilisent des standards.

Toutes ces télécommandes, ont été testé avec succès :

* Une télécommande Philips hifi.
* Une télécommande Samsung d'un enregistreur vidéo.
* Une télécommande Universelle.
* Une télécommande d'un Ordinateur MAC.

![](/basic-usage/features/kodi/ir-control/ir5.jpg)

## II - Configuration

### A - config.txt

* Ouvrez le fichier [recalbox-user-config.txt](./../../../tutorials/system/modification/configtxt-configuration) via [SSH](./../../../tutorials/system/access/root-access-terminal-cli).
* Pensez à activer le mode lecture-écriture sur la [partition de démarrage](./../../../tutorials/system/access/remount-partition-with-write-access)
* Décommentez la ligne suivante en retirant le `#` :

```ini
#dtoverlay=lirc-rpi
```

Devient :

```ini
dtoverlay=lirc-rpi
```

* Redémarrez votre Recalbox.

### B - Configuration télécommande

#### 1. Vérification des évènements IR

Nousa llons vérifier que le matériel fonctionne :

* Connectez vous à votre Recalbox via [SSH](./../../../tutorials/system/access/root-access-terminal-cli)
* Lancez la commande suivante :

```shell
lsmod
```

Vous devez voir s'afficher une ligne commençant par `lirc_rpi`.

![](/basic-usage/features/kodi/ir-control/ir6.png)

* Ensuite, lancer la commande suivante :

```shell
mode2 -d /dev/lirc0
```

* À chaque pression d'une touche de votre télécommande en face de votre récepteur, vous devriez voir apparaître des chiffres sur le terminal. Si c'est le cas, la télécommande et le récepteur fonctionne correctement.
* Appuyez sur `Ctrl`+`C` pour quitter.

![](/basic-usage/features/kodi/ir-control/ir7.png)

#### 2. Enregistrer votre télécommande

* Lancez la commande :

```shell
irrecord -H /recalbox/share/custom.conf
```

* Appuyez sur `Entrée` pour continuer.
* Entrez `customremote` comme nom de télécommande et appuyez sur `Entrée` pour continuer.

![](/basic-usage/features/kodi/ir-control/ir8.png)

* Commencez maintenant à appuyer sur les touches de votre télécommande pendant environ une seconde.

![](/basic-usage/features/kodi/ir-control/ir9.png)

* Entrez le nom de votre touche parmi les noms ci-dessous et appuyez sur la touche de la télécommande :

  * KEY_DOWN (Bas)
  * KEY_EXIT (Retour/Quitter)
  * KEY_INFO (Affichage des infos à propos du média en cours de lecture)
  * KEY_LEFT (Gauche)
  * KEY_MENU (Menu)
  * KEY_MUTE (Sourdine)
  * KEY_OK (Ok)
  * KEY_PLAY (Lecture et Pause)
  * KEY_POWER (Quitter)
  * KEY_RIGHT (Droite)
  * KEY_STOP (Stop)
  * KEY_UP (Haut)
  * KEY_VOLUMEDOWN (Volume -)
  * KEY_VOLUMEUP (Volume +)

* Et éventuellement :

  * KEY_AUDIO
  * KEY_BLUE
  * KEY_CHANNELDOWN
  * KEY_CHANNELUP
  * KEY_DELETE
  * KEY_ENTER
  * KEY_EPG
  * KEY_FASTFORWARD
  * KEY_GREEN
  * KEY_LANGUAGE
  * KEY_MEDIA
  * KEY_NEXT
  * KEY_NUMERIC_1
  * KEY_NUMERIC_2
  * KEY_NUMERIC_3
  * KEY_NUMERIC_4
  * KEY_NUMERIC_5
  * KEY_NUMERIC_6
  * KEY_NUMERIC_7
  * KEY_NUMERIC_8
  * KEY_NUMERIC_9
  * KEY_NUMERIC_0
  * KEY_PAUSE
  * KEY_PREVIOUS
  * KEY_PVR
  * KEY_RADIO
  * KEY_RECORD
  * KEY_RED
  * KEY_REWIND
  * KEY_SUBTITLE
  * KEY_VIDEO
  * KEY_YELLOW
  * KEY_ZOOM

![](/basic-usage/features/kodi/ir-control/ir10.png)

* Répétez l'opération pour l'ensemble ou un maximum de boutons sur votre télécommande.

Pour paramétrer à nouveau une touche, retapez son nom et recommencez.

![](/basic-usage/features/kodi/ir-control/ir11.png)

* Lorsque vous avez enregistré l’ensemble des touches voulues, appuyez sur `Entrée` pour continuer.
* Puis appuyez sur une des touches très rapidement sans la maintenir et sans changer de touches.

![](/basic-usage/features/kodi/ir-control/ir12.png)

* À la fin, le programme est automatiquement fermé.
* Si vous voulez recommencer, effacer le fichier de configuration via la commande :

```shell
rm /tmp/custom.conf
```

* Et redémarrer `irrecord`.

#### 3. Fichier de configuration

* Déplacez ensuite le fichier avec la commande suivante pour remplacer le fichier `lircd.conf` par votre fichier :

```shell
mv /recalbox/share/custom.conf /recalbox/share/system/.config/lirc/lircd.conf
```

* Relancez le service `lircd` via la commande :

```shell
/etc/init.d/S25lircd restart
```

![](/basic-usage/features/kodi/ir-control/ir13.png)

#### 4. Vérifier que le paramétrage fonctionne

* Tapez la commande `irw`.
* À chaque fois que vous appuyez sur une touche, vous devriez voir une ligne apparaître avec le nom de la touche.
* Appuyez sur `Ctrl` + `C` pour quitter.

![](/basic-usage/features/kodi/ir-control/ir14.png)

* Lancez Kodi et vérifiez le fonctionnement.

## III - Configuration avancée

### A - Lircmap.xml

* Vous pouvez personnaliser le paramétrage des touches des télécommandes en modifiant le fichier `/recalbox/share/system/.kodi/userdata/Lircmap.xml`.

>Si vous avez effacé manuellement le dossier `~/.kodi` et relancé Kodi sans avoir au préalable redémarrer votre Recalbox, vous ne verrez pas les personnalisations de Kodi ou du fichier `Lircmap.xml`.
{.is-warning}

* Considérez la copie de `/recalbox/share_init/system/.kodi/userdata/Lircmap.xml` vers `/recalbox/share/system/.kodi/userdata/Lircmap.xml`

### B - remote.xml

Vous pouvez modifier la définition des touches et des actions dans le fichier `/recalbox/share/system/.kodi/userdata/keymaps/remote.xml`

### C - QUITTER / STOP

Ceci s'adresse aux personnes qui n'aime pas le fait que le bouton `Retour` ne stoppe pas un film dans Kodi ou parce que vous n'avez qu'un bouton unique Stop / Retour sur votre télécommande.

Vous pouvez modifier dans le fichier remote.xml la section "", "Back" par "Stop".

### D - VOLUME HAUT/BAS

>Ce point concerne les télécommandes CEC.
{.is-info}

Si votre télécommande CEC ne transfère pas les ordres des volumes, vous pouvez utiliser d'autres touches en remplaçant par exemple dans remote.xml dans la section globale :

```xml
  <skipplus>SkipNext</skipplus>
  <skipminus>SkipPrevious</skipminus>
```

par

```xml
  <skipplus>VolumeUp</skipplus>
  <skipminus>VolumeDown</skipminus>
```

### E - PAUSE sur OK

Sur la skin Refocus pour rendre la pause plus facile (principalement sur une télécommande Apple), vous pouvez modifier le fichier `~/.kodi/addons/skin.refocus/720p/VideoOSD.xml` en remplaçant :

```xml
  <defaultcontrol always="true">700</defaultcontrol>
```

Par ceci :

```xml
  <defaultcontrol always="true">705</defaultcontrol>
```