---
title: Libretro Kronos
description: 
published: true
date: 2021-10-18T22:39:45.637Z
tags: libretro, saturn, kronos
editor: markdown
dateCreated: 2021-08-03T10:22:25.494Z
---

**Libretro Kronos** is a port of **Yabause** emulator for Libretro emulating the Sega Saturn.

## ![](/emulators/license.svg) License

This core is under [**GPLv2**](https://github.com/libretro/yabause/blob/master/yabause/COPYING) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ❌ | ❌ | ❌ | ❌ | ❌ | ❌ | ❌ | ✅ |

## ![](/emulators/features.png) Features



## ![](/emulators/bios.svg) BIOS

### List of mandatory bios

| Filename | Description | MD5 | Provided |
| :---: | :---: | :---: | :---: |
| mpr-18811-mx.ic1 | The King of Fighters '95 - Required for this game | 255113ba943c92a54facd25a10fd780c | ❌ |
| mpr-19367-mx.ic1 | Ultraman: Hikari no Kyojin Densetsu - Required for this game | 1cd19988d1d72a3e7caa0b73234c96b4 | ❌ |
| saturn_bios.bin | Saturn BIOS | af5828fdff51384f99b3c4926be27762 | ❌ |
| stvbios.zip | ST-V BIOS | 2b99d9dcecde22db4371580b6bb33f32 53a094ad3a188f86de4e64624fe9b3ca 17116d2aea72454096b3f80ab714bfba 2b99d9dcecde22db4371580b6bb33f32 4cb34733b741ada074f11e3abcdc56d8 6b6d422a6c392eaec178b64b92987e99 88b9da6cda268ba1d9e37dd82d038801 8c34608cc140e1bfbf9c945a12fed0e8 b876d86e244d00b336980b4e26e94132 | ❌ |

### Location

Put the bios like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 🗒 **mpr-18811-mx.ic1**
┃ ┃ ┃ ┣ 🗒 **mpr-19367-mx.ic1**
┃ ┃ ┃ ┣ 🗒 **saturn_bios.bin**
┃ ┃ ┃ ┣ 🗒 **stvbios.zip**

## ![](/emulators/isos.svg) Isos

### Supported extensions

Isos must have the extension:

* .bin/.cue
* .m3u
* .img/.ccd
* .chd

### Location

Put the isos like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 saturn
┃ ┃ ┃ ┃ ┣ 🗒 **game.bin**
┃ ┃ ┃ ┃ ┣ 🗒 **game.cue**

>The isos using **Redump** format are highly recommended.
{.is-success}

>For more information about isos, go to [this tutorial](./../../../tutorials/games/generalities/isos-and-roms)!
{.is-info}

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Accessing to options

You can configure various options in two different ways.

* Via RetroArch menu:

┣ 📁 RetroArch Menu
┃ ┣ 📁 Core options
┃ ┃ ┣ 🧩 Name_option

* Via the file `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core options

| Options | Options values | Variables (for configuration override) | Variables values |
| --- | --- | --- | --- |
| Force HLE BIOS (restart) | `Disabled` ✅ / `Enabled` | `kronos_force_hle_bios` | `disabled` / `enabled` |
| Video format | `auto` ✅ / `NTSC` / `PAL` | `kronos_videoformattype` | `auto` / `NTSC` / `PAL` |
| Frameskip | `No skipping` ✅ / `Skip rendering of 1 frames out of 2` / `Skip rendering of 2 frames out of 3` / `Skip rendering of 3 frames out of 4` / `Skip rendering of 4 frames out of 5` / `Skip rendering of 5 frames out of 6` | `kronos_skipframe` | `0` / `1` / `2` / `3` / `4` / `5` |
| SH-2 cpu core | `kronos` ✅ / `old` | `kronos_sh2coretype` | `kronos` / `old` |
| Video renderer | `OpenGL (requires OpenGL 4.2+)` ✅ / `OpenGL CS (requires OpenGL 4.3+)` | `kronos_videocoretype` | `opengl` / `opengl_cs` |
| Share saves with beetle | `Disabled` ✅ / `Enabled` | `kronos_use_beetle_saves` | `disabled` / `enabled` |
| Addon Cartridge (restart) | `None` / `1M_extended_ram` / `4M_extended_ram` / `16M_extended_ram` / `512K_backup_ram` ✅ / `1M_backup_ram` / `2M_backup_ram` / `4M_backup_ram` | `kronos_addon_cartridge` | `None` / `1M_extended_ram` / `4M_extended_ram` / `16M_extended_ram` / `512K_backup_ram` / `1M_backup_ram` / `2M_backup_ram` / `4M_backup_ram` |
| 6Player Adaptor on Port 1 | `Disabled` ✅ / `Enabled` | `kronos_multitap_port1` | `disabled` / `enabled` |
| 6Player Adaptor on Port 2 | `Disabled` ✅ / `Enabled` | `kronos_multitap_port2` | `disabled` / `enabled` |
| Resolution | `original` ✅ / `480p` / `720p` / `1080p` / `4k` / `8k` | `kronos_resolution_mode` | `original` / `480p` / `720p` / `1080p` / `4k` / `8k` |
| Output to original resolution | `Disabled` ✅ / `Enabled` | `kronos_force_downsampling` | `disabled` / `enabled` |
| Polygon Mode | `perspective_correction` / `gpu_tesselation` / `cpu_tesselation` ✅ | `kronos_polygon_mode` | `perspective_correction` / `gpu_tesselation` / `cpu_tesselation` |
| Improved mesh | `Disabled` ✅ / `Enabled` | `kronos_meshmode` | `disabled` / `enabled` |
| Improved banding | `Disabled` ✅ / `Enabled` | `kronos_bandingmode` | `disabled` / `enabled` |
| RGB Compute shaders | `Disabled` ✅ / `Enabled` | `kronos_use_cs` | `disabled` / `enabled` |
| Wireframe mode | `Disabled` ✅ / `Enabled` | `kronos_wireframe_mode` | `disabled` / `enabled` |
| ST-V Service/Test Buttons | `Disabled` ✅ / `Enabled` | `kronos_service_enabled` | `disabled` / `enabled` |
| ST-V Favorite Region | `EU` ✅ / `US` / `JP` / `TW` | `kronos_stv_favorite_region` | `EU` / `US` / `JP` / `TW` |
| Bios Language | `English` ✅ / `German` / `French` / `Spanish` / `Italian` / `Japanese` | `kronos_language_id` | `English` / `German` / `French` / `Spanish` / `Italian` / `Japanese` |

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/libretro/yabause/](https://github.com/libretro/yabause/)