---
title: PC-88
description: 
published: true
date: 2021-08-09T14:56:26.816Z
tags: computers, nec, pc-88
editor: markdown
dateCreated: 2021-07-28T21:16:14.800Z
---

![](/emulators/computers/pc88.svg){.align-center}

## Technical data

* **Manufacturer**: NEC Corporation
* **Year of release**: 1981
* **OS**: N88-BASIC, N-BASIC (PC8001 MODE)
* **CPU**: µPD780c-1 (Z80A-compatible) @ 4 MHz
* **RAM**: 64 KB (up to 576 KB)
* **VRAM**: 48 KB
* **Sound**: 3 FM channels + 3 SSG + 6 rhythms + 1 ADPCM

## Presentation

The **PC-88** (full name **NEC PC-8801**) is a Japanese microcomputer created by NEC in 1981 and based on the Zilog Z80 microprocessor. The PC-88 was a great success in Japan, but was not marketed in any other country.

For its time, the PC-88 offered high screen resolution. However, it could only offer 8 colors simultaneously out of a total of 512 colors and its 640×400 mode was monochrome. Its sound capabilities were also far ahead of other machines of the time.

Some companies developed exclusive software for this platform such as Enix, Square, Sega, Falcom, Bandai Soft, HAL Laboratory, ASCII, Pony Canyon, T&E Soft, or WolfTeam. Many popular video game series were released on this microcomputer such as Snatcher, Thexder, Dragon Slayer and Ys.

Hudson Soft asked Nintendo to port some titles previously released on Famicom such as Excitebike, Balloon Fight, Tennis, Donkey Kong 3, Golf and Ice Climber, as well as PC-88 exclusive ports of Mario Bros. like Mario Bros. Special and Punch Ball Mario Bros. Most notably, they developed a unique version of Super Mario Bros. for the PC-88 called Super Mario Bros. Special. The game has since fallen into oblivion; thus, fans call it the real "Lost Levels".

The system also had its own BASIC language (N88-BASIC), which was quite common for personal computers during the 1980s.

## Emulators

[Libretro-QUASI88](libretro-quasi88)