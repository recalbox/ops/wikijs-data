---
title: 3. 📚 TUTORIALS
description: 
published: true
date: 2023-07-18T11:48:33.656Z
tags: tutorials
editor: markdown
dateCreated: 2021-05-28T22:10:30.283Z
---

In this part of the documentation, you can find a lot of tutorials from the team as well as your own since you can also create them!

Here are the available categories:

[Network](network)
[System](system)
[Video](video)
[Kodi](kodi)
[Controllers](controllers)
[Games](games)
[Others](others)
[Audio](audio)
[Troubleshooting information](troubleshooting-information)
[Utilities](utilities)
[Frontend customization](frontend-customization)