---
title: GSPlus
description: 
published: true
date: 2024-06-23T11:45:43.282Z
tags: apple-2, gsplus
editor: markdown
dateCreated: 2021-05-21T08:15:11.333Z
---

**GSPlus** est un émulateur crée sur les restes de KEGS et GSPort pour émuler l'Apple ]\[GS (Apple 2GS).

## ![](/emulators/license.svg) Licence

Ce core est sous licence [**GPLv2**](https://github.com/digarok/gsplus/blob/master/LICENSE.txt).

## ![](/emulators/compatibility.png) Compatibilité

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Fonctionnalités



## ![](/emulators/bios.svg) BIOS

>**Aucun bios n'est requis.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Extensions supportées

Les roms doivent avoir les extensions suivantes :

* .po
* .dsk

### Emplacement

Placez les roms comme ceci : 

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 apple2
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.dsk**

>Les roms au format **TOSEC** sont vivement conseillées.
{.is-success}

>Pour plus d'information sur les roms, rendez-vous sur [ce tutoriel](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuration avancée de l'émulateur

>Pour pouvoir conserver vos configurations personnalisées lors d'une mise à jour, nous vous conseillons d'utiliser notre fonctionnalité [Surcharges de configuration](./../../../advanced-usage/configuration-override).
{.is-info}

## ![](/emulators/external-links.png) Liens externes

* **Code source utilisé** : [https://github.com/digarok/gsplus/](https://github.com/digarok/gsplus/)
* **Documentation** : [http://apple2.gs/plus/](http://apple2.gs/plus/)