---
title: 🔧 HARDWARE COMPATIBILITY
description: 
published: true
date: 2021-08-08T18:31:12.876Z
tags: compatibility, hardware
editor: markdown
dateCreated: 2021-05-28T22:16:44.454Z
---

On the following pages you can check if your hardware has been tested and is supported, as well as check if a specific system or emulator is compatible with your hardware.