---
title: Gestión de scrapes
description: 
published: true
date: 2024-07-26T07:34:10.262Z
tags: scrapes, gestión
editor: markdown
dateCreated: 2023-09-16T08:26:59.024Z
---

El scrape consiste en añadir multitud de información asociada a tus juegos, como un resumen textual que lo describe, capturas de pantalla, vídeos, manual del juego, ficheros pad-to-keyboard, etc.

Lista de tutoriales disponibles :

[Lista de herramientas de scraping](scraping-tools)
[Skraper - Scrapear vuestras roms](scrape-your-roms-with-skraper)