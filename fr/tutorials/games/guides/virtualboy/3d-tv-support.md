---
title: Afficher les jeux en 3D
description: 
published: true
date: 2024-01-07T14:06:12.083Z
tags: nintendo, virtual boy, 3d
editor: markdown
dateCreated: 2024-01-07T13:07:57.819Z
---

## Afficher les jeux en 3D

### Introduction

La virtual boy est une console qui permettait de profiter d'effet de profondeur grâce à un affichage 3D via un système de lentille similaire à ce que l'on retrouve aujourd'hui dans les casques de réalité virtuelle. Il est possible de retrouver cet effet de profondeur via deux techniques différentes selon le type de télé que vous utilisez.

### Télé non compatible 3D

Si votre télé n'est pas compatible 3D nativement il reste possible d'utiliser des lunettes anaglyphes (les fameuses lunettes rouge et bleu). 
* Pour cela lancez un jeu virtual boy et accédez au menu retroarch via la combinaison Hotkey + croix (ou équivalent selon votre manette).

* Allez ensuite dans les options du coeur

* Puis entrez dans le menu 3D mode et sélectionnez anaglyph

Enfilez vos magnifiques lunettes et à vous les joies du jeu en 3D


### Télé compatible 3D

Si votre télé est compatible 3D (que vous ayez des lunettes actives ou passives) il est possible de profiter de ses capacités pour afficher la Virtual Boy en 3D.

* Pour cela lancez un jeu virtual boy et accédez au menu retroarch via la combinaison Hotkey + croix (ou équivalent selon votre manette).

* Allez ensuite dans les options du coeur

* Puis entrez dans le menu 3D mode et sélectionnez side by side

* Retournez ensuite en arrière jusqu'à voir le menu réglage dans la colonne de gauche

* Rendez vous dans le menu vidéo, mise à l'échelle

* Entrez dans rapport d'aspect et sélectionnez personnalisé

Enfilez vos magnifiques lunettes, basculez votre télé en mode 3D et à vous les joies du jeu en 3D
