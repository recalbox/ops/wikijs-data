---
title: Contribute to the project
description: How to contribute to the Recalbox project
published: true
date: 2021-08-12T07:14:33.590Z
tags: open source, contribution, project, aide
editor: markdown
dateCreated: 2021-06-28T17:43:03.364Z
---

Recalbox is a free open source project, and we would be happy to count you among our contributors.

## Profiles wanted
If you have skills and would like to use them to improve the project, you can tell us about it on our discord.

We are always looking for profiles that can help us:
- **Graphic designer** : recalbox, website, social networks
- **UX Designer** : ux recalbox, web, ui
- **Developer**: bash, c, c++, python (python 2 > 3)
- **Translator**: translation and proofreading of Recalbox, websites, wiki
- **Advanced users**: detailed bug reports

## Bug reports

The first form of participation is to report bugs you find on Recalbox in detail.

The **[Issues](https://gitlab.com/recalbox/recalbox/-/issues)** gitlab are bug or enhancement request tickets that allow users, developers and testers to exchange around the topic in one place.

When you report a bug, don't hesitate to be as precise as possible. Describe the context, add screenshots, and why not explain how you worked around the bug if it is the case.

Go to https://gitlab.com/recalbox/recalbox/-/issues !

## Development

Recalbox sources are available on https://gitlab.com/recalbox/recalbox

The development on Recalbox requires to read a minimum of documentation on the functioning of the foundations of the system: **buildroot**.

Once you understand how it works, you can start by making your own image of Recalbox as indicated in the [README.md](https://gitlab.com/recalbox/recalbox/-/blob/master/README.md) of the project.

Then, it will be necessary to know the basics of **bash** and linux commands.

Depending on your skills, you can start on different topics:

- Update package in buildroot (bash, compilation)
- Adding a package in buildroot (bash, compilation)
- Adding a standalone emulator (bash, compilation, c/c++, python)
- Adding functionality to EmulationStation (c++)

We are not necessarily looking for super technical people, but rather someone motivated, autonomous and able to work in team on the open source model :D

## Translations

### Wiki

If you want to participate in the **translation of the wiki**, visit the [Contribute to the wiki page](../../contribute)

### Interface

If you want to participate in the **translation of the EmulationStation interface**, you can join the project page at [poeditor.com](https://poeditor.com/join/project/hEp5Khj4Ck). Regular updates of the source language file are made, which we will bring you to translate into the languages of your choice.

### Youtube videos

If you want to participate in the **subtitle translation for Recalbox's tutorial videos**, please contact the team on our **[social networks, Discord or forum](./../presentation/useful-links).**