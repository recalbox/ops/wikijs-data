---
title: CRT
description: 
published: true
date: 2024-07-26T06:43:45.287Z
tags: crt
editor: markdown
dateCreated: 2023-09-10T11:59:53.407Z
---

En esta sección encontraréis varios tutoriales relacionados con la conexión de vuestra Recalbox a una pantalla de rayos catódicos (CRT).

Aquí tenéis los tutoriales disponibles:

[Conectad vuestra Recalbox a un CRT a travès de HDMI y SCART](crt-screen-with-hdmi-and-scart)
[Conectad vuestra Recalbox a un CRT en composite](crt-screen-with-composite)
[Conectad vuestra Recalbox a un CRT utilizando la salida HDMI](crt-screen-with-hdmi)
[Configurad vuestra pantalla CRT a través del bus DPI (VGA666 / PiScart / RGBPi)](crt-screen-dpi-vga666-piscart-rgbpi)
[Configurad vuestra CRT en Raspberry Pi 4 / 400 / 3 / Zero2](recalbox-on-crt-with-scart-dac)

