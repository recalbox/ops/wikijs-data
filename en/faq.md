---
title: 💡 FAQ
description: 
published: true
date: 2021-08-08T18:34:52.384Z
tags: faq
editor: markdown
dateCreated: 2021-05-28T22:31:34.313Z
---

## General

### What can I install Recalbox on?

You can install Recalbox on the following devices:

- **Raspberry Pi 4**
- Raspberry Pi 400
- Raspberry Pi Zero
- Raspberry Pi 1 (B/B+)
- Raspberry Pi 2
- Raspberry Pi 3 (B/B+)
- Odroid XU4
- Odroid Go Advance (Portable Console)
- Odroid Go Super (Laptop)
- PC x86 (32bits)
- PC x86_64 (64bits)

### How can I participate?

- You can make a donation on our Paypal
- You can order your hardware on kubii.fr/221-recalbox
- You can report bugs or improvement ideas on our [Gitlab](https://gitlab.com/recalbox/recalbox/issues)
- You can participate on the [recalbox forums](http://forum.recalbox.com)
- You can [contribute to this wiki](./contribute)

### Where can I see the progress of the Recalbox development?

On the [recalbox gitlab](https://gitlab.com/recalbox). You can access :  
- [issues](https://gitlab.com/recalbox/recalbox/issues) which are the current tasks.  
- the [milestones](https://gitlab.com/recalbox/recalbox/-/milestones) where you can see the tasks that are still to be developed, and those that are finished.

### How many games does Recalbox contain?

We're trying to add as many free games as possible to the distribution, so you can enjoy the Recalbox experience from the first launch!
Go to [this page](https://forum.recalbox.com/topic/52/homebrews-sur-la-recalbox) to submit your homebrews!

### Can I use Recalbox in a kiosk / bartop?

Yes, the Recalbox OS system has been designed to be integrated in kiosks and bartops.
- You can connect the buttons and joysticks directly to the GPIOs of the Raspberry.
- You can disable the menus to avoid any misuse.
- It is also possible to use the HDMI / DVI / VGA outputs (with adapter) or RCA.
You can find more information on [the dedicated page](/en/tutorials/system/installation/bartop-arcade-configuration).

### The Recalbox supports many systems but I only see a few in the interface, what should I do?

Only systems with available games are displayed. Try to add your roms via the network and restart the Recalbox to see the new systems appear. Also, some hardware does not support all consoles.

### Do I need to overclock my Raspberry Pi?

On Raspberry Pi 1 (including zero), it is highly recommended to overclock the Raspberry in EXTREM mode.

## EmulationStation

### How do I change the look of the EmulationStation theme?

Press `Start` on the controller → `INTERFACE SETTINGS` → `THEME`

### How to turn off the background music?

Press `Start` on the controller → `SOUND OPTIONS` → Set to Off

### Where are the EmulationStation configuration files located in the file system?

```
/recalbox/share/system/recalbox.conf
```

### Where are EmulationStation's themes located?

```
/recalbox/share/themes
```

## Emulation

### Why are there multiple emulators for each system?

Emulation is not a flawless operation, there is a balance between performance, functionality and fidelity.
Since emulation is not perfect, it is better to have several options to handle it, sometimes a faster emulator will be better but may have unexpected problems, while a slower one may have higher accuracy but does not support some specific features.

### How do I change the default emulator for a given system?

By pressing `START` on the gamepad, then going to the advanced settings, and finally to the advanced emulator configuration. You can select the desired system, you can change the emulator and kernel settings (note that the basic setting will not change if the emulator setting says Default).

## System

### Recalbox supports about 100 consoles but I only see a few of them, either in the EmulationStation interface or in the share folder?

Depending on the card you use, you will have emulators compatible with it.  
Please look at the compatibility list for each card [here](/en/hardware-compatibility/).

### Why on some systems like the GBA, my games show a black screen and then return under EmulationStation?

> Some systems require bios to work.

## General Troubleshooting

### My Recalbox just crashed, should I unplug it and plug it back in?

No, in most cases, the system did not completely crash, only EmulationStation did.

There are several ways to handle this situation, the easiest one is to use the debugging features of Webmanager to restart EmulationStation or to shut down Recalbox properly.

Not doing so can corrupt your data, especially if you are using a Raspberry Pi with an SD card.