---
title: Esperad a la red antes de lanzar Kodi
description: 
published: true
date: 2023-09-13T15:02:08.216Z
tags: kodi, red, acceso
editor: markdown
dateCreated: 2023-09-13T15:02:08.216Z
---

Cuando Kodi está en modo autoarranque en Recalbox, es posible, en algunos sistemas, que el servicio de red aún no se haya iniciado cuando se lanza. Esto causa problemas a los usuarios que tienen recursos compartidos de red o utilizan Kodi con conexiones externas.

## Procedimiento a aplicar

* En el fichero `recalbox.conf`, buscad la sección `A - System options` y las siguientes líneas:

```text
;kodi.network.waitmode=required
;kodi.network.waittime=10
;kodi.network.waithost=192.168.0.50
```

* Descomentad las líneas borrando el `;` y modificad los valores según vuestras necesidades.

## Explicación detallada de las opciones

* _**kodi.network.waitmode**_

  * **required** - Utilizad esta opción para forzar Kodi a bloquearse hasta que se pueda contactar la dirección IP especificada.
  * **wish** - Utiliza una cuenta atrás en lugar de la opción de tener que hacer ping a una máquina antes de comenzar.


* _**kodi.network.waittime**_

  * Tiempo en segundos que se espera antes de suponer que Kodi esta muerto (cuando `required` está configurado). O bien el tiempo en segundos que se espera antes de continuar con el lanzamiento de Kodi (cuando `wish` está configurado).

* _**kodi.network.waithost**_

  * La dirección IP del sistema utilizado para comprobar la conexión de red (por ejemplo, la dirección IP de vuestro router o la dirección IP de vuestro NAS).