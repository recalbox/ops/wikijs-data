---
title: Libretro A5200
description: 
published: true
date: 2024-06-22T14:04:39.622Z
tags: libretro, atari, 5200, atari-5200, 8.1+, a5200
editor: markdown
dateCreated: 2023-09-06T08:39:21.582Z
---



## ![](/emulators/license.svg) Licencia

El core posee la siguiente licencia [**GPLv2**](https://github.com/libretro/a5200/blob/master/License.txt).

## ![](/emulators/compatibility.png) Compatibilidad

 | RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
 | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Funcionalidades

| Funcionalidad | ¿Soportada? |
| :---: | :---: |

## ![](/emulators/bios.svg) BIOS

### Lista de BIOS obligatorias

| Nombre del fichero | Descripción | MD5 | ¿Preinstalada? |
| :---: | :---: | :---: | :---: |
| 5200.rom | 5200 - Requis | 281f20ea4320404ec820fb7ec0693b38 | ❌ |

### Emplazamiento

Disponed vuestros juegos como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 atari5200
┃ ┃ ┃ ┃ ┣ 🗒 **5200.rom**

## ![](/emulators/roms.png) Roms

### Extensiones de roms soportadas

Las roms deben tener las extensiones siguientes:

* .a52
* .atr
* .atr.gz
* .bas
* .bin
* .car
* .cdm
* .xex
* .xfd
* .xfd.gz
* .zip

Este sistema soporta las roms comprimidas en formato .zip/.7z. No obstante, se trata sólo de un archivo.

Los ficheros contenidos en los .zip/.7z deben tener las extensiones citadas previamente.
Además, cada fichero .zip/.7z sólo debe contener un fichero rom al interior.

### Emplazamiento

Disponed vuestras roms como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 atari5200
┃ ┃ ┃ ┃ ┣ 🗒 &nbsp;**juego.zip**

>Se aconseja las roms del projecto **No-Intro**.
{.is-success}

>Para más información, consultad [este tutorial](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

>Para conservar las configuraciones personalizadas cuando se produce una actualización del sistema Recalbox, os aconsejamos utilizar las [Sobrecargas de configuración](./../../../advanced-usage/configuration-override).
{.is-info}

### Acceder a las opciones

Podéis configurar las opciones del core de dos formas diferentes.

* Via el Menú RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via el fichero `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Opciones del core

| Opción | Valor de la opción | Variable (para la sobrecarga) | Valores posibles |
| --- | --- | --- | --- |
| BIOS (Restart) | `Official` ✅ / `Internal (Altirra OS)` | `a5200_bios` | `official` / `internal` |
| Interframe Blending | `Désactivé` ✅ / `Simple` / `Ghosting (65%)` / `Ghosting (75%)` / `Ghosting (85%)` / `Ghosting (95%)` | `a5200_mix_frames` | `disabled` / `mix` / `ghost_65` / `ghost_75` / `ghost_85` / `ghost_95` |
| Hi-Res Artifacting Mode | `None` ✅ / `blue/brown 1` / `blue/brown 2` / `GTIA` / `CTIA` | `a5200_artifacting_mode` | `none` / `blue/brown 1` / `blue/brown 2` / `GTIA` / `CTIA` |
| High Fidelity POKEY (Restart) | `Désactivé` / `Activé` ✅ | `a5200_enable_new_pokey` | `disabled` / `enabled` |
| Audio Filter | `Désactivé` ✅ / `Activé` | `a5200_low_pass_filter` | `disabled` / `enabled` |
| Audio Filter Level | `5%` / `10%` / `15%` / `20%` / `25%` / `30%` / `35%` / `40%` / `45%` / `50%` / `55%` / `60%` ✅ / `65%` / `70%` / `75%` / `80%` / `85%` / `90%` / `95%` | `a5200_low_pass_range` | `5` / `10` / `15` / `20` / `25` / `30` / `35` / `40` / `45` / `50` / `55` / `60` / `65` / `70` / `75` / `80` / `85` / `90` / `95` |
| Controller Hacks | `Désactivé` ✅ / `Dual Stick` / `Swap Ports` | `a5200_input_hack` | `disabled` / `dual_sticks` / `swap_ports` |
| Pause acts as Reset | `Désactivé` ✅ / `Activé` | `a5200_pause_is_reset` | `disabled` / `enabled` |
| Digital Joystick Sensitivity | `Auto` ✅ / `5%` / `7%` / `10%` / `12%` / `15%` / `17%` / `20%` / `22%` / `25%` / `27%` / `30%` / `32%` / `35%` / `37%` / `40%` / `42%` / `45%` / `47%` / `50%` / `52%` / `55%` / `57%` / `60%` / `62%` / `65%` / `67%` / `70%` / `72%` / `75%` / `77%` / `80%` / `82%` / `85%` / `87%` / `90%` / `92%` / `95%` / `97%` / `100%` | `a5200_digital_sensitivity` | `auto` / `5` / `7` / `10` / `12` / `15` / `17` / `20` / `22` / `25` / `27` / `30` / `32` / `35` / `37` / `40` / `42` / `45` / `47` / `50` / `52` / `55` / `57` / `60` / `62` / `65` / `67` / `70` / `72` / `75` / `77` / `80` / `82` / `85` / `87` / `90` / `92` / `95` / `97` / `100` |
| Analog Joystick Sensitivity | `Auto` ✅ / `5%` / `7%` / `10%` / `12%` / `15%` / `17%` / `20%` / `22%` / `25%` / `27%` / `30%` / `32%` / `35%` / `37%` / `40%` / `42%` / `45%` / `47%` / `50%` / `52%` / `55%` / `57%` / `60%` / `62%` / `65%` / `67%` / `70%` / `72%` / `75%` / `77%` / `80%` / `82%` / `85%` / `87%` / `90%` / `92%` / `95%` / `97%` / `100%` | `a5200_analog_sensitivity` | `auto` / `5` / `7` / `10` / `12` / `15` / `17` / `20` / `22` / `25` / `27` / `30` / `32` / `35` / `37` / `40` / `42` / `45` / `47` / `50` / `52` / `55` / `57` / `60` / `62` / `65` / `67` / `70` / `72` / `75` / `77` / `80` / `82` / `85` / `87` / `90` / `92` / `95` / `97` / `100` |
| Analog Joystick Response | `Linear` ✅ / `Quadratic` | `a5200_analog_response` | `linear` / `quadratic` |
| Analog Joystick Deadzone | `0` / `3` / `5` / `7` / `10` / `13` / `15` ✅ / `17` / `20` / `23` / `25` / `27` / `30` | `a5200_analog_deadzone` | `0` / `3` / `5` / `7` / `10` / `13` / `15` / `17` / `20` / `23` / `25` / `27` / `30` |
| Analog Device | `Analog Stick` ✅ / `Mouse` | `a5200_analog_device` | `analog_stick` / `mouse` |


## ![](/emulators/external-links.png) Enlaces externos

* **Código fuente utilizado** : [https://github.com/libretro/a5200/](https://github.com/libretro/a5200/)