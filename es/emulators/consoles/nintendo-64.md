---
title: Nintendo 64
description: 
published: true
date: 2024-07-25T19:14:21.707Z
tags: n64, nintendo, nintendo 64, consolas
editor: markdown
dateCreated: 2023-09-03T19:58:40.166Z
---

![](/emulators/consoles/nintendo64.svg){.align-center}

## Detalles técnicos

* **Fabricante**: Nintendo
* **Año de lanzamiento**: 1996
* **Cantidad vendida**: 32,93 millones
* **Juego más vendido**: Super Mario 64
* **Procesador**: NEC VR4300 de 64 bits a 93,75 MHz
* **Coprocesador**: MIPS RISC "Reality Immersion" RCP de 64 bits a 62,5 MHz
* **RAM**: 4 MB RAMBUS RDRAM (ampliable a 8 MB)
* **Chip de sonido** : Estéreo 16-Bit y 48 kHz

## Presentación

A principios de los años 90, con el éxito de las consolas **NES** y **Super Nintendo**, Nintendo dominaba el mercado mundial de los videojuegos, a pesar de la feroz competencia de Sega.
La aparición del CD-ROM llevó a Nintendo a asociarse con Sony para desarrollar un reproductor de CD para la Super Nintendo, el **SNES-CD**, más conocido como **Nintendo PlayStation**, para competir con el **PC-Engine** de NEC y el **Mega-CD** de Sega.
Pero una disputa llevó a Nintendo y Sony a abandonar su proyecto conjunto, que Sony recicló para desarrollar su propia consola, la **PlayStation**. Nintendo recurrió entonces a la empresa holandesa Philips para continuar su proyecto.

El anuncio de la PlayStation en 1991, unido al fracaso del **Mega-CD** de Sega y del **CD-i** de Philips, convenció a Nintendo para enterrar definitivamente la extensión CD-ROM de la **Super Nintendo** y volcarse en el desarrollo de una consola completamente nueva.


## Emuladores

[Libretro mupen64plus_Next](libretro-mupen64plus_next)
[Libretro ParaLLEl_n64](libretro-parallel_n64)
[Mupen64plus GLide64MK2](mupen64plus-glide64mk2)
[Mupen64plus GLideN64](mupen64plus-gliden64)
[Mupen64plus N64_GLES2](mupen64plus-n64_gles2)
[Mupen64plus RICE](mupen64plus-rice)
[Mupen64plus RICE_GLES2](mupen64plus-rice_gles2)