---
title: Conecte seu Recalbox a uma Tela CRT com um cabo composto
description: Use seu Recalbox em uma TV "de tubo" com um cabo do tipo "P2 x RCA 3"
published: true
date: 2021-12-22T15:28:37.333Z
tags: crt, composite, tela
editor: markdown
dateCreated: 2021-12-01T00:16:43.613Z
---

Se você deseja conectar seu Recalbox a uma tela CRT, você precisará de um cabo do tipo P2 para RCA como este:

![RPi Audio/Video Socket](/tutorials/video/crt/rpi_av_socket.jpg)

> ATENÇÃO: Nos cabos de filmadoras, a saída de vídeo pode estar no conector de áudio direito (vermelho).
>
> Além disso, o tipo de cabo necessário não é muito comum, é preferível testar com um multímetro se o cabo usado corresponde ao diagrama proposto acima (se a ordem do conector for diferente, você terá como resultado uma imagem trêmula em preto e branco, sinal de um cabo inadequado).
{.is-warning}

## Configuração

* Modifique o arquivo [recalbox-user-config.txt](./../../../tutorials/system/modification/configtxt-configuration) comentando todas as linhas que começam com `hdmi_` (adicione no início da linha o símbolo `#`), e adicione o `sdtv_mode` suportado:

```ini
sdtv_mode=0    NTSC Normal
sdtv_mode=1    Versão japonesa do NTSC – sem "pedestal"
sdtv_mode=2    PAL Normal
sdtv_mode=3    PAL-M - Versão brasileira do PAL – 525/60 em vez de 625/50, sub-portada diferente
```

* Para ter um som melhor por meio da saída composta, ative o driver de áudio experimental para o Raspberry Pi:

```ini
audio_pwm_mode=2
```

* Este modo pode criar uma lentidão geral do Recalbox. Se for esse o caso, comente a linha e force a saída de áudio para o conector:

```ini
#audio_pwm_mode=2
hdmi_drive=1
```

* Finalmente, adicione `hdmi_ignore_hotplug=1` para forçar a saída composta. Seu arquivo `recalbox-user-config.txt` deve ter a seguinte aparência:

```ini
sdtv_mode=2
hdmi_ignore_hotplug=1
audio_pwm_mode=2
```

* Em seguida, modifique [recalbox.conf](./../../../basic-usage/getting-started/recalboxconf-file) e defina `global.videomode` para `default`:

```ini
global.videomode=default
```

* Se você estiver usando o emulador n64, também considere comentar esta linha:

```ini
n64.videomode=DMT 9 HDMI
```

* e descomente essa (remova o símbolo `#` do início da linha):

```ini
n64.videomode=default
```

* Finalmente, desative a suavização do jogo e todos os shaders no menu do EmulationStation (`START` ->`CONFIGURAÇÕES DOS JOGOS`), o que permitirá que você tenha a renderização original em todos os sistemas!

## Tudo fica lento com o modo composto

`audio_pwm_mode=2` melhora o som, mas pode desacelerar o sistema no modo composto, especialmente no RPi zero. Nesse caso, use o seguinte modo:

```ini
audio_pwm_mode=0
```