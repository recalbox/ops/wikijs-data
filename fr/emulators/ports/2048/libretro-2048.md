---
title: Libretro 2048
description: 
published: true
date: 2024-06-23T12:03:32.998Z
tags: libretro, 2048
editor: markdown
dateCreated: 2021-05-21T08:32:38.929Z
---

Libretro 2048 est un portage du jeu 2048.

## ![](/emulators/license.svg) Licence

Ce core est sous licence [**domaine public**](https://github.com/libretro/libretro-2048/blob/master/COPYING).

## ![](/emulators/compatibility.png) Compatibilité

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Fonctionnalités

| Fonctionnalité | Supporté |
| :---: | :---: |
| Redémarrage | ✔ |
| Captures d'écran | ✔ |
| Sauvegardes | ✔ |
| Sauvegardes instantanées | ✔ |
| Rembobinage | ✔ |
| Netplay | ✔ |
| Controls | ✔ |
| Remapping | ✔ |

## ![](/emulators/bios.svg) BIOS

>**Aucun bios n'est requis.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Extensions supportées

Les roms doivent avoir l'extension :

* .game

### Emplacement

Placez les roms comme ceci : 

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 ports
┃ ┃ ┃ ┃ ┣ 📁 2048
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **2048.game**

Cette rom est fournie dans votre Recalbox.

## ![](/emulators/advanced-configuration.png) Configuration avancée de l'émulateur

>**Ce core n'a pas d'option.**
{.is-success}

## ![](/emulators/external-links.png) Liens externes

* **Code source utilisé** : [https://github.com/libretro/libretro-2048/](https://github.com/libretro/libretro-2048)
* **Code source officiel** : [https://github.com/gabrielecirulli/2048/](https://github.com/gabrielecirulli/2048)
* **Site officiel** : [https://play2048.co**/**](https://play2048.co/)​