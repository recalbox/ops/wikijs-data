---
title: Gestion des multi-disques au format .PBP
description: 
published: true
date: 2024-07-26T07:07:18.235Z
tags: ps1, pbp, gestion, multi-disques
editor: markdown
dateCreated: 2021-05-21T08:48:07.884Z
---

Avoir un seul fichier pour vos jeux multi-disques est possible. Il suffit de créer un fichier eboot contenant vos images iso, bin, img via le logiciel gratuit **psx2psp**.

>On vous rappelle que vous devez disposez de vos propres images de disques PSX !
{.is-info}

## Création du fichier eboot

* Recherchez la version **psx2psp v1.42**, la télécharger et l’exécuter.
* Choisissez le mode classic puis sur la partie de gauche, charger un à un vos ISOs.
* Choisissez votre répertoire d'enregistrement.
* Cliquez sur le bouton `Convert`, puis patientez.

Placez votre dossier ou renommez le fichier eboot créé vers votre recalbox dans le dossier `/recalbox/share/roms/psx`.

## Changement de CD

Pour changer de CD en cours de jeu, il faut activer 2-3 options dans le menu RetroArch.

* Ouvrez le menu RetroArch pendant le jeu avec `Hotkey + B`.
* Allez dans settings/general settings et passer l’option configuration save on exit sur on. 

>À partir de ce moment là, toutes les modifications que vous effectuerez dans les options seront sauvegardées quand vous quitterez le menu RetroArch.
{.is-warning}

* Allez dans settings/input settings et chercher les options portant le nom, disk eject toggle, disk next, disk previous. Il faut alors leur affecter des touches de votre manettes n’ayant pas encore de fonction spéciale attribuée.
* Quittez RetroArch et retournez en jeu.

>Quand vous serez à l’écran d’un jeu vous demandant de changer de CD, il vous suffira alors d’éjecter virtuellement le CD (en faisant hotkey + le raccourci attribué à l’option disk eject toggle), de changer de cd, et de refermer virtuellement le lecteur cd.  
>Votre jeu démarrera alors automatiquement sur le CD suivant.
>
>Vous pouvez [consulter cette discussion](https://forum.recalbox.com/topic/482/roms-playstation-et-nombre-de-jeux) sur le forum en cas de problème.
{.is-warning}

## Pour changer de CD sur PCSX-reARMed (r22)

Méthode pour des fichiers .bin

* Rentrez dans le menu RetroArch avec `Hotkey + B`.
* Choisissez Quick menu, puis core disk option.
* Éjectez le cd virtuel en choisissant disk cycle tray status.
* Cherchez le répertoire où se trouve le cd que vous voulez charger (logiquement dans `/recalbox/share/roms/psx`) avec disk image append.

## Pourquoi il est déconseillé de convertir au format .PBP ?

Quand vous convertissez un jeu au format `.PBP`, le format de ce fichier va changer l'alignement des pistes sur les jeux multi-pistes et/ou multi-disques avec le programme de conversion (en général PSX2PSP). Si jamais vous souhaitez revenir en arrière, il y a un rique, voire plus, de se retrouver avec des pistes mal tronquées ou qui se chevauchent en inversant le procédé.

Quand il va redécouper les pistes en `.BIN`/`.CUE`, il va couper en général 2048 bytes trop loin. Donc, vous avez un bout de la piste 2 dans ta piste 1.

Le problème arrive avec la piste 3 : vous avez un bout de la piste 3 dans la piste 2, mais décalé de ... 2x 2048 bytes, et ainsi de suite.

La qualité du dump est toujours là, mais vous allez couper n'importe comment, et les entêtes PBP ne permettent pas de recréer fidèlement le `.CUE`, ainsi que les marqueurs de pistes (LEAD IN/LEAD OUT).

Il est plus rapide de dumper à nouveau son disque que de tenter de corriger automatiquement ou à la main.