---
title: Play game extensions
description: 
published: true
date: 2021-08-08T09:20:59.350Z
tags: disk, 64dd, drive, n64dd, extensions, fzero
editor: markdown
dateCreated: 2021-08-08T09:12:49.862Z
---

## Introduction

At least 1 game, F-Zero X, has its game extension through the Nintendo 64 Disk Drive. We will see here how to proceed.

## Usage

1. Put the game `F-Zero X (Japan).z64` in the `64dd` directory.
2. Put the `F-Zero X - Expansion Kit (Japan).ndd` game in the `64dd` directory.
3. Update your list of games.
4. In the list of games, move to the desired game and press `START`.

![](/tutorials/games/guides/64-dd/play-game-extensions/fzerodd1-en.png){.full-width}

5. Go to `MODIFY GAME` > `CLEAR WITH` and choose the core `LIBRETRO MUPEN64PLUS_NEXT`.
   
![](/tutorials/games/guides/64-dd/play-game-extensions/fzerodd2-en.png){.full-width}

6. Close all menus.
7. Run `F-Zero X (Japan)` in the `64dd` system.
8. When the game starts, go to the RetroArch menu by pressing `Hotkey + B`.

![](/tutorials/games/guides/64-dd/play-game-extensions/fzerodd3-en.png){.full-width}

9. Do "Back" once and then go to "Subsystems".

![](/tutorials/games/guides/64-dd/play-game-extensions/fzerodd4-en.png){.full-width}

10. Select the `Load N64 Disk Drive` option.

![](/tutorials/games/guides/64-dd/play-game-extensions/fzerodd5-en.png){.full-width}

11. Select the game `F-Zero X - Expansion Kit (Japan).ndd`.

![](/tutorials/games/guides/64-dd/play-game-extensions/fzerodd6-en.png){.full-width}

12. You will automatically go back. Go back to `Subsystems` and choose `Load N64 Disk Drive` again.

![](/tutorials/games/guides/64-dd/play-game-extensions/fzerodd7-en.png){.full-width}

13. Select the game `F-Zero X (Japan).z64`.

![](/tutorials/games/guides/64-dd/play-game-extensions/fzerodd8-en.png){.full-width}

14. You will automatically go back. Go back to `Subsystems` and choose `Start N64 Disk Drive`.

![](/tutorials/games/guides/64-dd/play-game-extensions/fzerodd9-en.png){.full-width}

15. And your game starts with the extension.

![](/tutorials/games/guides/64-dd/play-game-extensions/fzerodd10.png){.full-width}
![](/tutorials/games/guides/64-dd/play-game-extensions/fzerodd11.png){.full-width}

Enjoy the game!