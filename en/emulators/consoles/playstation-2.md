---
title: Playstation 2
description: Sony Playstation 2
published: false
date: 2021-11-09T20:23:43.486Z
tags: consoles, sony, playstation 2, ps2, 8.0+
editor: markdown
dateCreated: 2021-08-02T18:05:59.881Z
---

![](/emulators/consoles/playstation2.svg){.align-center}

## Technical data

* **Manufacturer**: Sony Computer Entertainment
* **Year of release**: 2000
* **Units sold**: 158 millions
* **Best-selling game**: Gran Theft Auto : San Andreas
* **CPU**: R5900-based "Emotion Engine" @ 294-299 MHz
* **GPU**: "Graphics Synthesizer" @ 149 MHz
* **RAM**: 32 MB PC800 32-bit dual-channel RDRAM @ 400 MHz
* **Video RAM**: 4MB
* **Resolution**: 256x224 to 1920x1080
* **Media**: DVD, CD

## Presentation

The **PlayStation 2** (officially abbreviated **PS2**) is a sixth-generation video game console marketed by Sony Computer Entertainment, a subsidiary of Sony. It was released on March 4, 2000 in Japan, October 26, 2000 in North America, November 24, 2000 in Europe and November 30, 2000 in Australia. The console competed with Sega's Dreamcast, Nintendo's GameCube and Microsoft's Xbox.

The PlayStation 2 succeeded the PlayStation in the line of the same name. It was an immediate success with one million copies sold in a few days in Japan. The PlayStation 2 reached a total of 150 million units shipped as of January 31, 2011, making it the best-selling home console in video game history. Sony claims that there are 10,828 video game titles available on the console and that 1.52 billion copies of these titles have been sold since the console's launch. In late 2009, after nearly a decade on the market, Sony says the PlayStation 2 will remain on the market as long as there are buyers for its games. This was followed by the PlayStation 3 in 2006.

The PlayStation 2 is now the best-selling console in the history of video games.

More than twelve years after its launch, Sony officially announced the end of distribution of the PlayStation 2 in Japan on December 28, 20128 (it was still selling well and was still in the Top 10) and in the rest of the world on January 4, 2013.

A page is turned in the history of video games and for the Japanese manufacturer, which had caused a sensation with the release of this console in March 2000, at a time when including DVD playback (a format that was very new at the time) as standard in this type of device was an important innovation.

## Emulators

[Libretro PCSX2](libretro-pcsx2)
[PCSX2](pcsx2)