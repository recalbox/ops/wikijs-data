---
title: Libretro FBNeo
description: 
published: true
date: 2024-06-30T19:44:28.398Z
tags: libretro, system, fbneo, master
editor: markdown
dateCreated: 2023-09-06T13:06:49.568Z
---

**FinalBurn Neo** es un emulador dedicado a los juegos de máquinas recreativas (arcade) y para determinadas consolas. Su código fuente se basa en los emuladores FinalBurn y en versiones antiguas de [MAME](https://www.mamedev.org/) (emulando menos máquinas) pero con un funcionamiento similar.

FB Neo (tambien llamado FinalBurn Neo) es un emulador de arcade que soporta las siguientes plataformas:

* Capcom CPS-1
* Capcom CPS-2
* Capcom CPS-3
* Cave
* Juegos basados en Data East DEC-0, DEC-8, Cassette System, DECO IC16 y DECO-32
* Hardware basado en la Galaxian
* Hardware Irem M62, M63, M72, M90, M92 y M107
* Kaneko 16
* Konami
* Namco Mappy, System 86, System 1 & 2 y otros
* Neo-Geo
* NMK16
* Hardware basado en Pacman
* PGM
* Hardware basado en Psikyo 68EC020 y SH-2
* Sega System 1, System 16 (y similares), System 18, X-Board y Y-Board
* Hardware basado en Seta-Sammy-Visco (SSV)
* Super Kaneko Nova System
* Toaplan 1
* Toaplan 2
* Taito F2, F3, X, Z y otros
* Varios drivers/pilotos para otros hardwares
* FB Neo posee también pilotos/drivers en construcción para los siguientes sistemas:
  * Sega Megadrive/Genesis
  * ColecoVision
  * Sega SG-1000
  * Sega Master System
  * Game Gear
  * MSX-1
  * ZX Spectrum
  * PC-Engine
  * TurboGrafx 16
  * SuperGrafx
  * Fairchild Channel F.

## ![](/emulators/license.svg) Licencia

El core posee la siguiente licencia [**non commerciale**](https://github.com/finalburnneo/FBNeo/blob/master/src/license.txt).

## ![](/emulators/compatibility.png) Compatibilidad

 | RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | 
 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Funcionalidades

| Funcionalidad | ¿Soportada? |
| :---: | :---: |
| Grabaciones | ✔ |
| Grabaciones instantáneas | ✔ |
| Rebobinado | ✔ |
| Netplay | ✔ |
| RetroAchievements | ✔ |
| Cheats RetroArch | ✔ |
| Cheats nativos | ✔ |
| Mandos | ✔ |
| Multi-Ratón | ✔ |
| Subsistemas | ✔ |

## ![](/emulators/bios.svg) Bios

### Lista de bios

Dependiendo del juego, algunas bios deberán colocarse en la misma carpeta que la rom.

### Emplazamiento

Disponed vuestras bios como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 fbneo
┃ ┃ ┃ ┃ ┣ 🗒 **bios.zip**

## ![](/emulators/roms.png) Roms

### Extensiones soportadas

Todos los juegos FinalBurn Neo utilizan únicamente las siguientes extensiones:

* .zip
* .7z

### Emplazamiento

Disponed vuestros juegos como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 fbneo
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.zip**

>Para más información sobre las roms, consultad [este tutorial](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

>Para conservar las configuraciones personalizadas cuando se produce una actualización del sistema Recalbox, os aconsejamos utilizar las [Sobrecargas de configuración](./../../../advanced-usage/configuration-override).
{.is-info}

### Acceder a las opciones

Podéis configurar las opciones del core de dos formas diferentes.

* Via el Menú RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via el fichero `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Opciones del core

| Opción | Valor de la opción | Variable (para la sobrecarga) | Valores posibles |
| --- | --- | --- | --- |
| Use 32-bits color depth when available | `Désactivé` / `Activé` ✅ / | `fbneo-allow-depth-32` | `disabled` / `enabled` |
| Vertical mode | `Désactivé` ✅ / `Activé` / `alternate` | `fbneo-vertical-mode` | `disabled` / `enabled` / `alternate` |
| Force 60Hz | `Désactivé` ✅ / `Activé` | `fbneo-force-60hz` | `disabled` / `enabled` |
| Allow patched romsets | `Désactivé` / `Activé` ✅ | `fbneo-allow-patched-romsets` | `disabled` / `enabled` |
| Analog Speed | `25%` / `30%` / `35%` / `40%` / `45%` / `50%` / `55%` / `60%` / `65%` / `70%` / `75%` / `80%` / `85%` / `90%` / `95%` / `100%` ✅ / `105%` / `110%` / `115%` / `120%` / `125%` / `130%` / `135%` / `140%` / `145%` / `150%` / `155%` / `160%` / `165%` / `170%` / `175%` / `180%` / `185%` / `190%` / `195%` / `200%` / `205%` / `210%` / `215%` / `220%` / `225%` / `230%` / `235%` / `240%` / `245%` / `250%` / `255%` / `260%` / `265%` / `270%` / `275%` / `280%` / `285%` / `290%` / `295%` / `300%` / `305%` / `310%` / `315%` / `320%` / `325%` / `330%` / `335%` / `340%` / `345%` / `350%` / `355%` / `360%` / `365%` / `370%` / `375%` / `380%` / `385%` / `390%` / `395%` / `400%` | `fbneo-analog-speed` | `25%` / `30%` / `35%` / `40%` / `45%` / `50%` / `55%` / `60%` / `65%` / `70%` / `75%` / `80%` / `85%` / `90%` / `95%` / `100%` / `105%` / `110%` / `115%` / `120%` / `125%` / `130%` / `135%` / `140%` / `145%` / `150%` / `155%` / `160%` / `165%` / `170%` / `175%` / `180%` / `185%` / `190%` / `195%` / `200%` / `205%` / `210%` / `215%` / `220%` / `225%` / `230%` / `235%` / `240%` / `245%` / `250%` / `255%` / `260%` / `265%` / `270%` / `275%` / `280%` / `285%` / `290%` / `295%` / `300%` / `305%` / `310%` / `315%` / `320%` / `325%` / `330%` / `335%` / `340%` / `345%` / `350%` / `355%` / `360%` / `365%` / `370%` / `375%` / `380%` / `385%` / `390%` / `395%` / `400%` |
| Crosshair emulation | `hide with lightgun device` ✅ / `always hide` / `always show` | `fbneo-lightgun-crosshair-emulation` | `hide with lightgun device` / `always hide` / `always show` |
| CPU clock | `25%` / `30%` / `35%` / `40%` / `45%` / `50%` / `55%` / `60%` / `65%` / `70%` / `75%` / `80%` / `85%` / `90%` / `95%` / `100%` ✅ / `105%` / `110%` / `115%` / `120%` / `125%` / `130%` / `135%` / `140%` / `145%` / `150%` / `155%` / `160%` / `165%` / `170%` / `175%` / `180%` / `185%` / `190%` / `195%` / `200%` / `205%` / `210%` / `215%` / `220%` / `225%` / `230%` / `235%` / `240%` / `245%` / `250%` / `255%` / `260%` / `265%` / `270%` / `275%` / `280%` / `285%` / `290%` / `295%` / `300%` / `305%` / `310%` / `315%` / `320%` / `325%` / `330%` / `335%` / `340%` / `345%` / `350%` / `355%` / `360%` / `365%` / `370%` / `375%` / `380%` / `385%` / `390%` / `395%` / `400%` | `fbneo-cpu-speed-adjust` | `25%` / `30%` / `35%` / `40%` / `45%` / `50%` / `55%` / `60%` / `65%` / `70%` / `75%` / `80%` / `85%` / `90%` / `95%` / `100%` / `105%` / `110%` / `115%` / `120%` / `125%` / `130%` / `135%` / `140%` / `145%` / `150%` / `155%` / `160%` / `165%` / `170%` / `175%` / `180%` / `185%` / `190%` / `195%` / `200%` / `205%` / `210%` / `215%` / `220%` / `225%` / `230%` / `235%` / `240%` / `245%` / `250%` / `255%` / `260%` / `265%` / `270%` / `275%` / `280%` / `285%` / `290%` / `295%` / `300%` / `305%` / `310%` / `315%` / `320%` / `325%` / `330%` / `335%` / `340%` / `345%` / `350%` / `355%` / `360%` / `365%` / `370%` / `375%` / `380%` / `385%` / `390%` / `395%` / `400%` |
| Hiscores | `Désactivé` / `Activé` ✅ | `fbneo-hiscores` | `disabled` / `enabled` |
| Frameskip | `Désactivé` ✅ / `Fixed` / `Auto` / `Manual` | `fbneo-frameskip-type` | `disabled` / `Fixed` / `Auto` / `Manual` |
| Frameskip Threshold (%) | `15` / `18` / `21` / `24` / `27` / `30` / `33` ✅ / `36` / `39` / `42` / `45` / `48` / `51` / `54` / `57` / `60` | `fbneo-frameskip-manual-threshold` | `15` / `18` / `21` / `24` / `27` / `30` / `33` / `36` / `39` / `42` / `45` / `48` / `51` / `54` / `57` / `60` |
 | Fixed Frameskip | `No skipping` ✅ / `Skip rendering of 1 frames out of 2` / `Skip rendering of 2 frames out of 3` / `Skip rendering of 3 frames out of 4` / `Skip rendering of 4 frames out of 5` / `Skip rendering of 5 frames out of 6` | `fbneo-fixed-frameskip` | `0` / `1` / `2` / `3` / `4` / `5` |
| Samplerate | `44100` / `48000` ✅ | `fbneo-samplerate` | `44100` / `48000` |
| Sample Interpolation | `Désactivé` / `2-point 1st order` / `4-point 3rd order` ✅ | `fbneo-sample-interpolation` | `disabled` / `2-point 1st order` / `4-point 3rd order` |
| FM Interpolation | `Désactivé` / `4-point 3rd order` ✅ | `fbneo-fm-interpolation` | `disabled` / `4-point 3rd order` |
| LowPass Filter | `Désactivé` ✅ / `Activé` | `fbneo-lowpass-filter` | `disabled` / `enabled` |

## ![](/emulators/external-links.png) Enlaces externos

* **Código fuente utilizado** : [https://github.com/libretro/FBNeo/](https://github.com/libretro/FBNeo/)
* **Documentación Libretro** : [https://docs.libretro.com/library/fbneo/](https://docs.libretro.com/library/fbneo/)
* **Página oficial** : [https://neo-source.com/](https://neo-source.com/)
* **Wiki del core** : [http://emulation.gametechwiki.com/index.php/FinalBurn_Neo](http://emulation.gametechwiki.com/index.php/FinalBurn_Neo)
* **Wiki oficial** : [https://github.com/finalburnneo/FBNeo/wiki](https://github.com/finalburnneo/FBNeo/wiki)