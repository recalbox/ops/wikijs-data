---
title: Libretro Mednafen_Lynx
description: 
published: true
date: 2024-07-24T22:36:42.166Z
tags: libretro, atari, lynx, mednafen
editor: markdown
dateCreated: 2021-07-09T20:31:15.132Z
---

**Libretro Mednafen\_lynx** is an emulator of the Atari Lynx video game system.
More precisely, it is a port of Mednafen Lynx which is a fork of Handy.

## ![](/emulators/license.svg) License

This core is under [**zlib**](https://github.com/libretro/beetle-lynx-libretro/blob/master/mednafen/lynx/license.txt) and [**GPLv2**](https://github.com/libretro/beetle-lynx-libretro/blob/master/COPYING) licenses.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Features

| Feature | Supported |
| :---: | :---: |
| Restart | ✔ |
| Screenshots | ✔ |
| States | ✔ |
| Rewind | ✔ |
| Netplay | ✔ (not link-cable emulation) |
| Core Options | ✔ |
| RetroAchievements | ✔ |
| Cheats (Cheats menu) | ✔ |
| Controls | ✔ |
| Remapping | ✔ |
| Softpatching | ✔ |
| Crop Overscan | ✔ |

## ![](/emulators/bios.svg) BIOS

### List of mandatory bios

| Filename | Description | MD5 | Provided |
| :---: | :---: | :---: | :---: |
| lynxboot.img | Lynx Boot Image | fcd403db69f54290b51035d82f835e7b | ❌  |

### Location

Put the bios like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 lynx
┃ ┃ ┃ ┃ ┣ 🗒 **lynxboot.img**

## ![](/emulators/roms.png) Roms

### Supported extensions

Roms must have the extension:

* .lnx
* .o
* .zip
* .7z

This system supports compressed roms in .zip/.7z format. However, be careful, it is only an archive.

The files contained in the .zip/.7z must correspond to the extensions mentioned above.
Moreover, each .zip/.7z file must contain only one compressed rom.

### Location

Put your roms like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 lynx
┃ ┃ ┃ ┃ ┣ 🗒 **game.zip**

>The roms using **No-Intro** format are highly recommended.
{.is-success}

>For more information about roms, go to [this tutorial](./../../../tutorials/games/generalities/isos-and-roms)!
{.is-info}

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Accessing to options

You can configure various options in two different ways.

* Via RetroArch menu:

┣ 📁 RetroArch Menu
┃ ┣ 📁 Core options
┃ ┃ ┣ 🧩 Name_option

* Via the file `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core options

| Options | Options values | Variables (for configuration override) | Variables values |
| --- | --- | --- | --- |
| Display rotation | `Enabled` ✅ / `Disabled` | `lynx_rot_screen` | `enabled` / `disabled` |
| Color Format (Restart Required) | `16-Bit (RGB565)` ✅ / `32-Bit (RGB8888)` | `lynx_pix_format` | `16` / `32` |
| Force 60Hz | `Disabled` ✅ / `Enabled` | `lynx_force_60hz` | `disabled` / `enabled` |

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/libretro/beetle-lynx-libretro/](https://github.com/libretro/beetle-lynx-libretro/)
* **Libretro documentation**: [https://docs.libretro.com/library/beetle\_lynx/](https://docs.libretro.com/library/beetle_lynx/)
* **Official source code**: [https://mednafen.github.io/releases/](https://mednafen.github.io/releases/)
* **Official website**: [https://mednafen.github.io/](https://mednafen.github.io/)