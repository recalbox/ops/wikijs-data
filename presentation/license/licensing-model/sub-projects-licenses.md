---
title: Licences pour projets associés
description: 
published: true
date: 2021-08-12T07:22:32.846Z
tags: licence, projets
editor: markdown
dateCreated: 2021-05-21T08:34:20.860Z
---

## Projets logiciels

Recalbox n'est pas seulement composé de son répertoire principal. Nous maintenons également quelques autres projets open-source inclus dans Recalbox ou faisant partie de l'infrastructure globale de Recalbox.

Voici un aperçu de la licence des principaux projets :

* Le [fork Recalbox d'EmulationStation](https://gitlab.com/recalbox/recalbox-emulationstation) est sous licence MIT
* Le [gestionnaire du site web](https://gitlab.com/recalbox/recalbox-manager) de Recalbox est sous formalité "tous droits réservés".
* L'API Recalbox est licenciée selon les termes de la licence MIT
* Le [site web](https://gitlab.com/recalbox/www.recalbox.com) de Recalbox est sous formalité "tous droits réservés".

## Œuvres créatives

Nous maintenons et distribuons également des [thèmes](https://gitlab.com/recalbox/recalbox-themes) pour Recalbox, qui sont publiés sous les licences suivantes :

| Thème | Licence |
| :---: | :---: |
| recalbox-goa2 | [CreativeCommons BY-NC-ND 4.0](https://creativecommons.org/licenses/by-nc-nd/4.0/) |
| recalbox-next | [CreativeCommons BY-NC-ND 4.0](https://creativecommons.org/licenses/by-nc-nd/4.0/) |

