---
title: Lista de utilidades interesantes
description: 
published: true
date: 2023-09-16T08:18:17.677Z
tags: utilidades, programas, lista, interesantes
editor: markdown
dateCreated: 2023-09-16T08:10:47.508Z
---

Antes de comenzar vuestra aventura con Recalbox, aquí tenéis una lista de utilidades/programas que deberíais tener en vuestro PC para simplificaros la vida.

>Usuarios de MacOS y Linux: si encontráis programas equivalentes, editad esta página para añadirlos.
{.is-info}

## utilidades de formateo de microSD

* SDFormatter](https://www.sdcard.org/downloads/formatter_4/): software para formatear SD/SDHC/SDXC.
* Minitool Partition wizard free](http://www.partitionwizard.com/free-partition-manager.html): software para formatear en varios formatos.

## Utilidad de flasheo de imágenes 

* **(Recomendado)** [Raspberry Pi Imager](https://www.raspberrypi.org/software/) : herramienta disponible en todos los sistemas (Windows, Linux, macOS, ...) para flashear un archivo `.img` en dispositivos de almacenamiento (tarjeta microsd/disco duro).
* WIN32 Disk Imager](https://sourceforge.net/projects/win32diskimager/): crea una imagen de su microSD.
* **(No recomendado)** Balena [Etcher](https://etcher.io/): herramienta multiplataforma para flashear un archivo `.img` en dispositivos de almacenamiento (tarjeta microsd/disco duro).

## Utilidades para descomprimir ficheros

* [7zip](http://www.7-zip.org/download.html): ideal para descomprimir tus romsets en 7zip.001, 7zip.002 y otros formatos.

## Utilidades para gestionar tu Recalbox a distancia

## {.tabset}
### Windows
* [MobaXTerm](https://mobaxterm.mobatek.net/)
* [WINSCP](http://winscp.net/)
* [Putty](https://www.chiark.greenend.org.uk/~sgtatham/putty/latest.html) 
* [Notepad++](https://notepad-plus-plus.org/downloads/)
\
Consultad [este tutorial](./../../tutorials/system/access/network-access-winscp) para configurar estos programas.

### macOS

* **Terminal** existe por defecto en el sistema operativo (Applications > Utilitaires).
* **TextEdit** existe por defecto en el sistema operativo.

### Linux

* **Terminal** existe por defecto en el sistema operativo.

## Utilidad para comprobar firmas digitales de las bios

## {.tabset}

### Windows
* [winmd5free](http://www.winmd5.com/)

* [md5sum](http://esrg.sourceforge.net/utils_win_up/md5sum/)

### macOS

* Commande **md5sum** via el terminal.

### Linux

* Commande **md5sum** via el terminal.

## Utilidades para gestionar las roms en el PC

* [Universal XML Editor – Editad simplemente vuestros ficheros XML](https://github.com/Universal-Rom-Tools/Universal-XML-Editor/releases) (local) 
* [Universal ROM Cleaner – Limpiad vuestras romset en un sólo click](https://forum.recalbox.com/topic/2344/soft-universal-rom-cleaner-nettoyez-vos-romset-d-une-main-clean-your-romset-with-one-hand) (local)

## Utilidades para gestionar romsets de CD (iso, img, bin)

Para Sega CD, PC-Engine CD y PSX.

* [Daemon Tools Lite](https://www.daemon-tools.cc/fra/products/dtLite) : Monta las imágenes CD en vuestro PC; su [tutorial aquí](./../games/guides/playstation-1/convert-disc-files-to-bin+cue)
* [IMGBurn](https://www.imgburn.com/) : Permite crear imágenes en format bin+cue ; su [tutorial aquí](./../games/guides/playstation-1/convert-disc-files-to-bin+cue)
* [Monkey Audio](http://www.monkeysaudio.com/download.html) : Permite descomprimir ficheros ape en wav.
* [emctools](https://app.box.com/s/l8x54nof3k53myk6yueaz2d1p02x8d5t) : Permite descomprimir el formato bin.ec.
* [psx2psp](https://github.com/recalbox/recalbox-os/wiki/Gestion-multidisc-PSX) : Permite fusionar varios CD.
* [SegaCD Cue Maker](http://www.racketboy.com/downloads/SegaCueMaker.zip) : Permite crear los cue que faltan para vuestros ficheros bin Sega CD únicamente.

## Utilidad para renombrar el nombre interno de la rom

* [N64 Rom Renamer](https://www.romhacking.net/utilities/791/) **:** Por Einstein II.
