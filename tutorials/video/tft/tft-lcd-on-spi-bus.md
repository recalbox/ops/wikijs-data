---
title: Configure a small TFT screen on SPI bus
description: 
published: true
date: 2022-11-11T22:11:20.410Z
tags: tft, screen, bus, small, spi
editor: markdown
dateCreated: 2021-08-06T22:50:02.988Z
---

## Configure your Adafruit 2.8" capacitive/resistive TFT screen

This screen is located on [this site](https://www.adafruit.com/product/1601). Since overlays already exist for this display in the `/boot/overlays` directory, it will be supported with a simple configuration.

```ini
pitft28-capacitive.dtbo
pitft28-resistive.dtbo
```

Check if you have the capacitive or resistive version of the display and adjust the `dt-overlay` line in the [recalbox-user-config.txt](./../../system/modification/configtxt-configuration) file.

This screen uses the SPI bus to be driven. Its low resolution allows us to get a good frame rate in the emulation frequencies. Not really 60fps, but more than the minimum of 30 fps thanks to the modified `fcbp` program.

### Modification to match the screen resolution

* You need to open the file [recalbox-user-config.txt](./../../system/modification/configtxt-configuration).
* Add this to this file:

```ini
dtparam=spi=on
dtparam=i2c1=on
dtparam=i2c_arm=on
hdmi_cvt=320 240 60 1 0 0 0
hdmi_group=2
hdmi_mode=87
# working config for speed
# 900 Mhz -> 48000000
# 1Ghz -> 35000000
#modification of the speed   
dtoverlay=pitft28-resistive,rotate=90,speed=22000000,fps=60
```

The SPI bus speed must be adjusted if you overclock your Raspberry Pi to get a good image. These settings have been tested on a Raspberry Pi1 at 1.1Ghz.

To get started, you can try this:

```ini
dtoverlay=pitft28-resistive,rotate=90,speed=32000000,fps=20
```

In the case of a capacitive screen, just replace with this:

```ini
dtoverlay=pitft28-capacitive,rotate=90,speed=32000000,fps=20
```

When restarting, the screen should start with a white screen and after a few seconds it should turn black. This is a sign that the screen is now supported. But you will not get any images until you run `fbcp`.

### Activating fbcp

* Open the file [recalbox.conf](./../../../basic-usage/getting-started/recalboxconf-file)
* Find the following line at the very top:

```ini
system.fbcp.enabled=0
```

You must disable GPIO controllers to use `fbcp`.

* Change this line to this:

```ini
system.fbcp.enabled=1
```

* Find the following line and update it if necessary:

```ini
controllers.gpio.enabled=0
```

If you are using the GPIO controllers, you can instead change the GPIO pin configuration by using pin 4 or 5 to set the pin used, avoiding the pins used by your display. See [here](./../../controllers/gpio/gpio-controllers) for details.

* Edit the [recalbox.conf](./../../../basic-usage/getting-started/recalboxconf-file) file like this:

```ini
#global.videomode=DMT 87 HDMI
global.videomode=default

#global.ratio=auto
global.ratio=4/3
```

* Save the file and reboot. The screen should now work fine. If you have any problems, first check that the `fbcp` program works with :

```shell
ps aux | grep fbcp
```

If another line than the one indicated is listed, it's all good.

This 2.8" screen works well but may be too small and too expensive (35€) as far as Chinese clones are concerned. So let's configure a 3.2" TFT screen from Waveshare that can be found on Banggood for less than 15€.

## Configure your Waveshare 3.2" capacitive/resistive TFT screen

The screen is the following: a 3.2" TFT LCD touchscreen display module for the Raspberry Pi B+, B, A+. Its resolution is the same as the 2.8": 320x240. It is in fact a [Waveshare screen](https://www.waveshare.com/3.2inch-rpi-lcd-b.htm).

This screen is not supported by default by the kernel file systems. We will have to add more to make the screen work. These files can be found [here](https://github.com/swkim01/waveshare-dtoverlays).

Get the files `waveshare35a-overlay.dtb` and `waveshare32b-overlay.dtb` for the WaveShare 3.2" 320x240 display and the WaveShare 3.5" 320x480 display respectively. For the new version 4.4 kernels, we need to rename the dtb files to dtbo files to match the new overlay tree name. Rename `waveshare35a-overlay.dtb` to `waveshare35a.dtbo` and `waveshare32b-overlay.dtb` to `waveshare32b.dtbo` and copy them to the `/boot/overlays` directory.

* You need to open the file [recalbox-user-config.txt](./../../system/modification/configtxt-configuration) with nano or vim and add the following lines:

```ini
#tft screen

#Waveshare 3.2 TFT Screen
#same resolution for hdmi and tft
hdmi_cvt=320 240 60 1 0 0 0
hdmi_group=2                
hdmi_mode=1                 
hdmi_mode=87                

dtparam=spi=on              
dtoverlay=waveshare32b:rotate=270,speed=82000000
```

### Activating fbcp

* Open the file [recalbox.conf](./../../../basic-usage/getting-started/recalboxconf-file)
* Find the following line at the very top:

```ini
system.fbcp.enabled=0
```

You must disable GPIO controllers to use `fbcp`.

* Change this line to this:

```ini
system.fbcp.enabled=1
```

* Find the following line and update it if necessary:

```ini
controllers.gpio.enabled=0
```

If you are using the GPIO controllers, you can instead change the GPIO pin configuration by using pin 4 or 5 to set the pin used, avoiding the pins used by your display. See [here](./../../controllers/gpio/gpio-controllers) for details.

* Edit the [recalbox.conf](./../../../basic-usage/getting-started/recalboxconf-file) file like this:

```ini
#global.videomode=DMT 87 HDMI
global.videomode=default

#global.ratio=auto
global.ratio=4/3
```

* Save the file and reboot. The screen should now work fine. If you have any problems, first check that the `fbcp` program works with :

```shell
ps aux | grep fbcp
```

If another line than the one indicated is listed, it's all good.

A working screen in video :

![Waveshare 3.2" resistive TFT](/tutorials/video/tft/waveshare-3-2.jpg)

[https://youtu.be/hcFk_vjQLVo](https://youtu.be/hcFk_vjQLVo)

## Why can't I use the Waveshare 3.5" capacitive/resistive TFT screen with Recalbox?

This display cannot be used for arcade games with Recalbox. The SPI bus does not have enough bandwidth to handle this higher resolution of 480x320. If you increase the bus speed, the display becomes unstable (colors, flickering). During my tests, I could only get 20-25 FPS. This display is usable with an X server with a slow frame rate but not in arcade mode which requires a higher frame rate.

As described on [this site](https://learn.adafruit.com/), it is not recommended to use this screen for gaming. With twice as many pixels to push on the screen, the PiTFT 3.5" is significantly slower than its more compact brothers and we strongly advise against it for games. Now you know!

But if you want to make it work, you can do the following:

### Change the /boot/recalbox-user-config.txt file to match the screen resolution

Get the files `waveshare35a-overlay.dtb` and `waveshare32b-overlay.dtb` for the WaveShare 3.2" 320x240 screen and the WaveShare 3.5" 320x480 screen respectively. For the new version 4.4 kernels, we need to rename the dtb files to dtbo files to match the new overlay tree name. Rename `waveshare35a-overlay.dtb` to `waveshare35a.dtbo` and `waveshare32b-overlay.dtb` to `waveshare32b.dtbo` and copy them to the **/boot/overlays** directory

* You need to open the file [recalbox-user-config.txt](./../../system/modification/configtxt-configuration) with nano or vim and add the following lines:

```ini
#tft screen
#Waveshare 3.5 TFT Screen
#same resolution for hdmi and tft
hdmi_cvt=480 320 60 1 0 0 0
hdmi_group=2
hdmi_mode=1
hdmi_mode=87

dtparam=spi=on
dtoverlay=waveshare35a:rotate=270,speed=27000000
# speed=41000000,fps=60 for better FPS, but the colors will look a little weird.
```

### Activating fbcp

* Open the file [recalbox.conf](./../../../basic-usage/getting-started/recalboxconf-file)
* Find the following line at the very top:

```ini
system.fbcp.enabled=0
```

You must disable GPIO controllers to use `fbcp`.

* Change this line to this:

```ini
system.fbcp.enabled=1
```

* Find the following line and update it if necessary:

```ini
controllers.gpio.enabled=0
```

If you are using the GPIO controllers, you can instead change the GPIO pin configuration by using pin 4 or 5 to set the pin used, avoiding the pins used by your display. See [here](./../../../tutorials/controllers/gpio/gpio-controllers) for details.

* Edit the [recalbox.conf](./../../../basic-usage/getting-started/recalboxconf-file) file like this:

```ini
#global.videomode=DMT 87 HDMI
global.videomode=default

#global.ratio=auto
global.ratio=3/2
```

* Save the file and reboot. The screen should now work fine. If you have any problems, first check that the `fbcp` program is running with :

```shell
ps aux | grep fbcp
```

If a line other than the one shown is listed, it's all good.

A working 3.5 inch screen with video FPS problems:

![Waveshare 3.5' resistive TFT](/tutorials/video/tft/waveshare-3-5.jpg)

[https://youtu.be/HJXQAEaVhKE](https://youtu.be/HJXQAEaVhKE)

Working with speed=410000,fps=60; this is clearly risky, some screens will give strange results... It will work on some screens depending on the controller of the embedded chip.

In my humble opinion, if you have the [3.5" (C) LCD for Raspberry Pi (480x320; 125Mhz)](https://www.waveshare.com/3.5inch-RPi-LCD-C.htm), it should work, but with the [3.5" (B) LCD for Raspberry Pi (480x320; IPS)](https://www.waveshare.com/3.5inch-RPi-LCD-B.htm), you won't be able to get 60fps!

Recalbox 3.5 TFT LCD Raspberry Pi 3 B](/tutorials/video/tft/waveshare-3-5-result.jpg)

[https://www.youtube.com/watch?v=cgznOcvRRqQ](https://www.youtube.com/watch?v=cgznOcvRRqQ)

## Get your Waveshare 3.5" resistive TFT screen working at full speed!

Coming soon.
This is for the brave: [https://github.com/juj/fbcp-ili9341](https://github.com/juj/fbcp-ili9341)