---
title: FAQ
description: 
published: false
date: 2021-11-09T20:39:36.817Z
tags: faq, bbc micro, 8.0+
editor: markdown
dateCreated: 2021-10-16T11:07:39.870Z
---

## What is the version of the BeebEm emulator?

We have worked to transpose the latest version of BeebEm to SDL2 and improved it to meet Recalbox needs.

## What systems are supported by BeebEm?

Four systems are supported:

* BBC Micro
* B Plus
* Integra B
* Master128

## Are there controller shortcuts for BeebEm?

Yes, here is the list:

| Button combination | Action |
| --- | --- |
| `Hotkey` + `Start` | Exit |
| `Hotkey` + `X` | Display of keyboard and peripheral LEDs (bottom icons, tape, floppy, hdd, special keyboard keys) |
| `Hotkey` + `Y` | Screen capture in native resolution |
| `Hotkey` + `B` | Reboot |
| `Hotkey` + `A` | Enable smoothing |
| `Hotkey` + `L1` / `Hotkey` + `R1`. | Use with `.M3U` files to change cassettes and disks. |
| `Hotkey` + `L2` | Pause |
|` Hotkey` + `R2` | Display FPS |
| `Hotkey` + `L3` | Change screen (color, black and white, green, amber) |
| `Hotkey` + `R3` | Change floppy disk protection (read-only files or zip files remain read-only) |

## What is the keyboard mapping?

The keyboard is QWERTY.

## Do the configuration overrides work?

Yes, on 2 levels only: `<game>.<ext>.xxx.cfg` and `.xxx.cfg`.

## Do the original peripherals work?

Yes, like the joystick and the mouse. The joystick is modeled on the left stick and the directional arrows (d-pad) as well as the A and B buttons.

Tube expansion cards are also supported, via the core options or by configuration overload.

## Can IDE or SCSI hard drives be loaded?

No, this is not possible at the moment.

## Are there any peripherals or parts of the system not available?

Yes, like teletext, econet support, debugger, disassembler and serial ports.

## Are only certain floppy disk controllers supported?

Yes, only these controllers are supported:

* acorn1770
* opusddos
* watford

These have to be activated in the core options.

For the B Plus model, the `acorn1770` floppy controller is enabled by default.

## Are `.ZIP` files supported?

Yes, for floppy disks only. Cassette formats (.uef and .csw) are already compressed, so zip takes up space.