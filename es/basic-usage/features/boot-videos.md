---
title: Vídeos de introducción
description: 
published: true
date: 2025-03-19T12:11:32.881Z
tags: videos, introducción
editor: markdown
dateCreated: 2023-08-25T14:35:19.260Z
---

En Recalbox, podéis añadir vuestros propios vídeos de arranque del sistema.

Vamos a describir:

* Cómo añadir los vídeos
* Cómo configurar su reproducción

## Añadir nuevos vídeos de introducción

Los vídeos se deben añadir dentro de la carpeta `/recalbox/share/bootvideos/`.

>Para añadir estos vídeos se debe acceder a esta carpeta únicamente **a través de la red**!
>
>El espacio total que ocupan todos los vídeos no debe superar los 500 MB.
{.is-warning}

Para conectaros a travès de la red a vuestra Recalbox, hay varias formas posibles:

* A través de [WinSCP](./../../tutorials/system/access/network-access-winscp)
* A través de [Cyberduck](./../../tutorials/system/access/netword-access-cyberduck)
* A través de [MobaXTerm](./../../tutorials/system/access/network-access-mobaxterm)
* A través de vuestro explorador de ficheros. Para ello necesitaréis conocer [la dirección IP de vuestra Recalbox](./../../tutorials/network/ip/discover-recalbox-ip) y tendréis que escribir dicha IP con este formato `\\<vuestra-ip-aquí>\` en vuestro explorador de ficheros.

## Parámetros relacionados con los vídeos de introducción

### Frecuencia de lectura

Podéis definir la frecuencia de reproducción de vuestro(s) vídeo(s) a través del parámetro `system.splash.select` dentro del fichero [recalbox.conf](./../../basic-usage/getting-started/recalboxconf-file).

Aquí tenéis las opciones disponibles:

* **all**: el sistema eligirá un vídeo predefinido o uno de los vuestros de forma indistinta.
* **custom**: el sistema eligirá sólamente vuestros vídeos
* **recalbox**: el sistema eligirá únicamente los vídeos  predefinidos de origen.

### Tiempo de lectura

También podéis definir el tiempo de reproducción de todos los vídeos (los de origen y/o los vuestros) a través del parámetro `system.splash.length` del fichero [recalbox.conf](./../../basic-usage/getting-started/recalboxconf-file).

A continuación se presentan los valores disponibles:

* **0** : la reproducción se cortará cuando EmulationStation estará listo para funcionar.
* **-1** : se reproducirá completamente el vídeo antes de lanzar la interfaz de EmulationStation.
* **Cualquier valor superior a 0** : El vídeo sera reproducido como máximo el numéro de segundos indicado aquí antes de lanzar la interfaz de EmulationStation.