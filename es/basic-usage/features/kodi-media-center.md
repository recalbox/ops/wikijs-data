---
title: Kodi Media Center
description: 
published: true
date: 2023-08-18T15:09:02.993Z
tags: kodi, media, center
editor: markdown
dateCreated: 2023-08-18T15:09:02.993Z
---

![](/basic-usage/features/kodi/kodilogo.png)

Kodi es un sistema multimedia integrado a Recalbox. Sirve principalmente para reproducir películas y música en local o en remoto, asi como para ver la televisión por internet.

Podéis conectar varios plugins mediante una conexión a internet para disfrutar de películas, musica y muchas cosas más.

Los videos en directo como aquellos grabados en el disco son accesibles desde la instalación por defecto de KODI, que se encuentra correctamente configurado. También podéis conectar vuestros discos duros externos mediante una conexión USB o en red.

>Los mandos de juegos están soportados en Kodi por défecto, pero también podéis utilizar un mando de la televisión gracias al protocolo HDMI CEC o una aplicación para móviles como la aplicación oficial de Kodi. 
{.is-info}

## Funcionalidad básica

### Arranque y apagado

Para lanzar **Kodi Media Center** se utiliza el botón `X` desde la lista de sistemas de EmulationStation, o bien accediendo al menú principal mediante `START` + la primera entrada del menú (KODI).

Para salir de KODI, navigad hasta el icono de alimentación arriba a la izquierda del menú principal de KODI y seleccionad:

* "Power Off System"
* "Reboot".

Los dos elementos os devolverán al menú principal de EmulationStation; a partir de aquí, utilizad el menu de Recalbox si queréis apagar completamente vuestro Recalbox.

No se puede apagar el sistema directamente desde KODI, es obligatorio pasar por EmulationStation.

### Nuevos usuarios de KODI:

Al primer arranque, KODI tiene unas pocas opciones solamente. Para activar las funcionalidades completas de KODI se necesita una conexión a internet, salvo si sólo os interesa utilizar discos locales o conectados por USB.

El [Wiki de KODI](http://kodi.wiki/view/Main_Page) dispone de información más detallada sobre su utilización, aunque se trata de un sistema bastante intuitivo como Netflix o Amazon Prime.

KODI detecta automáticamente los dispositivos USB conectados cuando entráis en los menús de Vídeo o Música; deberías encontrar vuestro disco o su ruta de acceso.

Además, las pestañas Vídeo, Música y otras permiten añadir  "Addons" (mini aplicación que aumentan la funcionalidad inicial de KODI).

Existen varios "Addons" que se pueden descargar e instalar desde los menús de KODI, y existen repositorios con colecciones de addons creados por otros usuarios. No dudéis en informaros sobre los "Addons" de la comunidad KODI para poder añadir funcionalidades impresionantes.

* Para instalar los "Addons" de música o de vídeo, navigad hasta la pestaña "Addon" y haced click, o bien
* Selecionad la pestaña principal y haced click para ver todos los medias o dispositivos locales. Haced click en "Addons" y pulsad el botón que dice "Get More". La siguiente pantalla presenta una lista de "Addons" aprobados por KODI. Elegid el que os interesa y pulsad en "Instalar".

La instalación se realiza en segundo plano y si disponéis de una buena conexión a internet, la descarga y la instalación durarán menos de un minuto.

Si tenéis acceso a un disco de red, debéis configurar la ruta de acceso dentro de los parámetros de configuración del sistema de ficheros. Todos estos pasos están bien documentados en el [Wiki de KODI](http://kodi.wiki/view/Main_Page) y también existen muchos vídeos e información en internet para profundizar sobre la configuración de cada "Addon".

Observaciones:

* Existen varias opciones en la [configuration de Recalbox](./../../basic-usage/getting-started/recalboxconf-file) que permiten configurar la consola para que arranque directamente en KODI en vez de pasar por EmulationStation.
* Algunas personas venden dispositivos USB o instrucciones para tener acceso a contenidos exclusivos de KODI, pero generalmente se trata de repertorios de "Addons" agrupados y pre-configurados que podéis encontrar vosotros mismos.
* Algunos "Addons" pueden pedir una subscripción, como los de Netflix o Amazon Vídeo, mientras que otros como Twitch o Pandora necesitan la creación de una cuenta de usuario.
* Si vuestros mandos de Recalbox se comportan erráticamente al entrar en KODI, quiere decir que no están bien configurados. Realizad otra vez la configuración de mandos; sección de [mandos de Recalbox](./../../basic-usage/first-use-and-configuration#configuration-dune-manette) del menú de EmulationStation.
* La versión KODI para Raspberry Pi no posee todas las funcionalidades y Add-Ons, pero la mayoria y los más importantes han sido añadidos.
* Cada vez que se lanza KODI, el sistema realiza una búsqueda de actualizaciones del sistema, de "Addons" y de contenido, lo que implica un pequeño retardo al comienzo. Teniendo esto en cuenta, basta con no ser impaciente y esperar un poco para que el sistema se estabilice y sea funcional. Cuantos más addons tenéis instalados, más tiempo tarda en arrancar el sistema, aunque algunos "Addons" se pueden configurar para realizar un arranque rápido o no buscar actualizaciones.
* La version KODI preinstalada con Recalbox se llama "Leia".
* Haced click en el siguiente enlace para acceder al soporte de esta version relacionado con SAMBA [SMB v2+ sur KODI](https://discourse.osmc.tv/t/kodi-and-smbv1/37441/5).

## Opciones

El fichero [recalbox.conf](./../../basic-usage/getting-started/recalboxconf-file) contiene varias opciones para la configuración de Kodi. Podéis activar o désactivar completamente Kodi (si no os interesa esta funcionalidad y sólo queréis utilizar Recalbox para jugar), también podéis activar el atajo para lanzar Kodi con el botón `X` del mando o bien configurad vuestra Recalbox para que Kodi se ejecute al inicio sin pasar por EmulationStation (`START` > `PARAMÈTRES AVANCÉES` > `OPTIONS KODI`).
 
>Kodi soporta bien los mandos de los juegos configurados en Recalbox.
>
>Podéis utilizar la funcionalidad de **HDMI CEC** de la tele para controlar el menú de Kodi con el mando de la tele, y también **Yatse**, un mando virtual para los teléfonos móviles.
{.is-info}

## Cosillas variadas

### Gestión de la alimentación con HDMI CEC

Cuando intentáis de apagar Kodi, vuestro Raspberry envía tambien una orden de apagado HDMI a vuestra televisión, lo que significa que todos vuestros "periféricos" se apagarán.

Para evitar este comportamiento, tenéis que desactivar la opción "Apagado del periférico al salir", yendo dentro de `Sistema` -> `Parámetros` -> `Sistema` -> `Dispositivos de entrada` -> `Périféricos` -> `Adaptador CEC`.

El adaptador CEC permitira (si vuestra televisión lo soporta) controlar KODI con el mando de la tele. CEC envía las órdenes de control a través del cable HDMI hasta la IP de vuestro sistema KODI [Wiki Kodi CEC](http://kodi.wiki/view/CEC).

### Configuración de Yatse

Yatse (junto con otras aplicaciones) es un mando virtual para controlar KODI. No es obligatorio su uso, pero suelen permitir un control más fino e intuitivo del sistema. Si os gusta KODI, probablemente os gustará también utilizar un mando virtual con vuestro teléfono móvil.

Antes de nada hay que lanzar KODI desde vuestra Recalbox.

Una vez que habéis instalado Yatse en vuestro móvil, Yatse buscará en vuestra red local una instalación de KODI a la que conectarse.

![](/basic-usage/features/kodi/configure_yatse_for_kodi_recalbox_1-300px.png)

Si queréis saltar esta etapa automática, tambien podéis configurar [la dirección IP de vuestra Recalbox](./../../tutorials/network/ip/discover-recalbox-ip) y como  puerto el **8081**

![](/basic-usage/features/kodi/configure_yatse_for_kodi_recalbox_2-300px.png)

Vuestro mando virtual Yatse se encuentra configurado:

![](/basic-usage/features/kodi/configure_yatse_for_kodi_recalbox_3-300px.png)

Si os gusta Yatse, no dudéis en comprar el **Unlocker** para obtener nuevas funcionalidades y colaborar con el desarrolador de la aplicación.

### Configuración de un joystick

La configuración o mapeo del joystick y de la acción que se produce dentro de Kodi se puede modificar en el siguiente fichero `/recalbox/share/system/configs/kodi/input.xml` siguiendo este tutorial: [http://kodi.wiki/view/Keymap#Commands](http://kodi.wiki/view/Keymap#Commands)

Los botones de vuestro mando de juegos se encuentran preconfigurados de la siguiente forma:

| Boton del mando | Función |
| ---: | :--- |
| `A` | Séleccionar |
| `B` | Volver |
| `X` | Stop |
| `Y` | Menu |
| `INICIO` | Pausa |
| `SELECCIÓN` | Menú contextual |
| `ARRIBA` | Arriba |
| `ABAJO` | Abajo |
| `DERECHA` | Derecha |
| `IZQUIERDA` | Izquierda |
| `STICK IZQUIERDO HACIA ARRIBA` | Aumentar el volumen |
| `STICK IZQUIERDO HACIA ABAJO` | Disminuir el volumen |
| `STICK IZQUIERDO HACIA LA IZQUIERDA` | Disminuir el volumen |
| `STICK IZQUIERDO HACIA LA DERECHA` | Aumentar el volumen |
| `STICK DERECHO HACIA ARRIBA` | Arriba |
| `STICK DERECHO HACIA ABAJO` | Ajabo |
| `STICK DERECHO HACIA LA IZQUIERDA` | Izquierda |
| `STICK DERECHO HACIA LA DERECHA` | Derecha |

### No tengo sonido el la salida para cascos:

Si habéis configurado KODI para utilizar la salida audio para los cascos pero no parece funcionar, intentad introducir las siguientes variables de configuración dentro del fichero `/boot/recalbox-user-config.txt` :

```ini
hdmi_ignore_edid_audio=1
hdmi_force_edid_audio=0
```

Estas opciones fuerzan la salida del audio para que salga por la salida de cascos.