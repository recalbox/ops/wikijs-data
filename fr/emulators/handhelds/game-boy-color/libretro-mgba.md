---
title: Libretro mGBA
description: 
published: true
date: 2024-06-22T11:29:16.073Z
tags: libretro, color, gameboy, mgba, gbc
editor: markdown
dateCreated: 2021-05-21T08:30:26.224Z
---

L'émulateur **mGBA** permet d'exécuter des jeux Game Boy Advance.  
Il vise à être plus rapide et plus précis que de nombreux émulateurs Game Boy Advance existants, ainsi qu'à ajouter des fonctionnalités qui manquent aux autres émulateurs. Il prend également en charge les jeux Game Boy et Game Boy Color.

## ![](/emulators/license.svg) Licence

Ce core est sous licence [**MPLv2.0**](https://github.com/libretro/mgba/blob/master/LICENSE).

## ![](/emulators/compatibility.png) Compatibilité

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Fonctionnalités

| Fonctionnalité | Supporté |
| :---: | :---: |
| Redémarrage | ✔ |
| Sauvegardes | ✔ |
| Sauvegardes instantanées | ✔ |
| Rembobinage | ✔ |
| Options du core | ✔ |
| RetroAchievements | ✔ |
| Cheats RetroArch | ✔ |
| Controls | ✔ |
| Remapping | ✔ |
| Vibration | ✔ |
| Softpatching | ✔ |

## ![](/emulators/bios.svg) BIOS

### Liste des bios optionnels

| Nom de fichier | Description | MD5 | Fourni |
| :---: | :---: | :---: | :---: |
| gbc_bios.bin | BIOS Game Boy Color | dbfce9db9deaa2567f6a84fde55f9680 | ❌ |
| sgb_bios.bin | BIOS Super Game Boy | d574d4f9c12f305074798f54c091a8b4 | ❌ |

### Emplacement

Placez les BIOS comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 gbc
┃ ┃ ┃ ┃ ┣ 🗒 **gbc_bios.bin**
┃ ┃ ┃ ┣ 📁 sgb
┃ ┃ ┃ ┃ ┣ 🗒 **sgb_bios.bin**

## ![](/emulators/roms.png) Roms

### Extensions supportées

La rom doit avoir l'extension :

* .bin
* .gbc
* .zip
* .7z

Ce système supporte les roms compressées au format .zip/.7z. Attention toutefois, il ne s'agit que d'une archive.

Les fichiers contenus dans les .zip/.7z doivent correspondre aux extensions citées précédemment.
De plus, chaque fichier .zip/.7z ne doit contenir qu'une seule rom compressée.

### Emplacement

Placez les roms comme ceci : 

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 gbc
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.zip**

>Les roms au format **No-Intro** sont vivement conseillées.
{.is-success}

>Pour plus d'information sur les roms, rendez-vous sur [ce tutoriel](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuration avancée de l'émulateur

>Pour pouvoir conserver vos configurations personnalisées lors d'une mise à jour, nous vous conseillons d'utiliser notre fonctionnalité [Surcharges de configuration](./../../../advanced-usage/configuration-override).
{.is-info}

### Accéder aux options

Vous pouvez configurer diverses options de deux façons différentes.

* Via le menu RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via le fichier `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Options du core

| Options | Valeur des options | Variable (pour surcharge) | Valeurs des variables |
| --- | --- | --- | --- |
| Modèle de Game Boy (Redémarrage requis) | `Détection automatique` ✅ / `Game Boy` / `Super Game Boy` / `Game Boy Color` / `Game Boy Advance` | `mgba_gb_model` | `Autodetect` / `Game Boy` / `Super Game Boy` / `Game Boy Color` / `Game Boy Advance` |
| Utiliser le fichier BIOS si trouvé (Redémarrage requis) | `Activé` ✅ / `Désactivé` | `mgba_use_bios` | `ON` / `OFF` |
| Ignorer l'intro du BIOS (Redémarrage requis) | `Désactivé` ✅ / `Activé` | `mgba_skip_bios` | `OFF` / `ON` |
| Palette Game Boy par défaut | `Niveaux de gris` ✅ / `DMG Green` / `GB Pocket` / `GB Light` / `GBC Brown ↑` / `GBC Red ↑A` / `GBC Dark Brown ↑B` / `GBC Pale Yellow ↓` / `GBC Orange ↓A` / `GBC Yellow ↓B` / `GBC Blue ←` / `GBC Dark Blue ←A` / `GBC Gray ←B` / `GBC Green →` / `GBC Dark Green →A` / `GBC Reverse →B` / `SGB 1-A` / `SGB 1-B` / `SGB 1-C` / `SGB 1-D` / `SGB 1-E` / `SGB 1-F` / `SGB 1-G` / `SGB 1-H` / `SGB 2-A` / `SGB 2-B` / `SGB 2-C` / `SGB 2-D` / `SGB 2-E` / `SGB 2-F` / `SGB 2-G` / `SGB 2-H` / `SGB 3-A` / `SGB 3-B` / `SGB 3-C` / `SGB 3-D` / `SGB 3-E` / `SGB 3-F` / `SGB 3-G` / `SGB 3-H` / `SGB 4-A` / `SGB 4-B` / `SGB 4-C` / `SGB 4-D` / `SGB 4-E` / `SGB 4-F` / `SGB 4-G` / `SGB 4-H` | `mgba_gb_colors` | `Grayscale` / `DMG Green` / `GB Pocket` / `GB Light` / `GBC Brown ↑` / `GBC Red ↑A` / `GBC Dark Brown ↑B` / `GBC Pale Yellow ↓` / `GBC Orange ↓A` / `GBC Yellow ↓B` / `GBC Blue ←` / `GBC Dark Blue ←A` / `GBC Gray ←B` / `GBC Green →` / `GBC Dark Green →A` / `GBC Reverse →B` / `SGB 1-A` / `SGB 1-B` / `SGB 1-C` / `SGB 1-D` / `SGB 1-E` / `SGB 1-F` / `SGB 1-G` / `SGB 1-H` / `SGB 2-A` / `SGB 2-B` / `SGB 2-C` / `SGB 2-D` / `SGB 2-E` / `SGB 2-F` / `SGB 2-G` / `SGB 2-H` / `SGB 3-A` / `SGB 3-B` / `SGB 3-C` / `SGB 3-D` / `SGB 3-E` / `SGB 3-F` / `SGB 3-G` / `SGB 3-H` / `SGB 4-A` / `SGB 4-B` / `SGB 4-C` / `SGB 4-D` / `SGB 4-E` / `SGB 4-F` / `SGB 4-G` / `SGB 4-H` |
| Palettes de Game Boy prédéfinies par le matériel (Redémarrage requis) | `Préréglage Game Boy par défaut` / `Préréglages de Game Boy Color uniquement` / `Préréglages de Super Game Boy uniquement` / `Tous les préréglages disponibles` | `mgba_gb_colors_preset` | `0` / `1` / `2` / `3` |
| Utiliser les bordures de Super Game Boy (Redémarrage requis) | `Activé` ✅ / `Désactivé` | `mgba_sgb_borders` | `ON` / `OFF` |
| Correction colorimétrique | `Désactivé` ✅ / `Game Boy Advance` / `Game Boy Color` / `Auto` | `mgba_color_correction` | `OFF` / `GBA` / `GBC` / `Auto` |
| Interframe Blending | `Désactivé` ✅ / `Simple` / `Intelligent` / `Rémanence LCD (réaliste)` / `Rémanence LCD (rapide)` | `mgba_interframe_blending` | `OFF` / `mix` / `mix_smart` / `lcd_ghosting` / `lcd_ghosting_fast` |
| Filtre passe-bas | `Désactivé` ✅ / `Activé` | `mgba_audio_low_pass_filter` | `disabled` / `enabled` |
| Niveau du filtre | `5%` / `10%` / `15%` / `20%` / `25%` / `30%` / `35%` / `40%` / `45%` / `50%` / `55%` / `60%` / `65%` / `70%` / `75%` / `80%` / `85%` / `90%` / `95%` | `mgba_audio_low_pass_range` | `5` / `10` / `15` / `20` / `25` / `30` / `35` / `40` / `45` / `50` / `55` / `60` / `65` / `70` / `75` / `80` / `85` / `90` / `95` |
| Autoriser les entrées directionnelles opposées | `Désactivé` ✅ / `Activé` | `mgba_allow_opposing_directions` | `no` / `yes` |
| Niveau de capteur solaire | `Utiliser le capteur de l'appareil si disponible` / `0` ✅ / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` | `mgba_solar_sensor_level` | `sensor` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` |
| Vibration du Game Boy Player (Redémarrage requis) | `Désactivé` ✅ / `Activé` | `mgba_force_gbp` | `OFF` / `ON` |
| Suppression de la boucle inactive | `Supprimer celles connues` ✅ / `Détecter et supprimer` / `Ne pas supprimer` | `mgba_idle_optimization` | `Remove Known` / `Detect and Remove` / `Don't Remove` |
| Saut d'images | `Désactivé` ✅ / `Auto` / `Auto (seuil)` / `Intervalle fixe` | `mgba_frameskip` | `disabled` / `auto` / `auto_threshold` / `fixed_interval` |
| Seuil de saut d'images (%) | `15` / `18` / `21` / `24` / `27` / `30` / `33` ✅ / `36` / `39` / `42` / `45` / `48` / `51` / `54` / `57` / `60` | `mgba_frameskip_threshold` | `15` / `18` / `21` / `24` / `27` / `30` / `33` / `36` / `39` / `42` / `45` / `48` / `51` / `54` / `57` / `60` |
| Intervalle de saut d'images | `0` ✅ / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` | `mgba_frameskip_interval` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` |

## ![](/emulators/external-links.png) Liens externes

* **Code source utilisé** : [https://github.com/libretro/mgba/](https://github.com/libretro/mgba/)
* **Documentation Libretro** : [https://docs.libretro.com/library/mgba/](https://docs.libretro.com/library/mgba/)
* **Code source officiel** : [https://github.com/mgba-emu/mgba/](https://github.com/mgba-emu/mgba/)
* **Site officiel** : [https://mgba.io/](https://mgba.io/)