---
title: Libretro Cap32
description: 
published: true
date: 2021-10-08T13:55:49.657Z
tags: libretro, cap32, gx-4000
editor: markdown
dateCreated: 2021-07-30T19:00:58.251Z
---



## ![](/emulators/license.svg) License

This core is under [**GPLv2**](https://github.com/ColinPitrat/caprice32/blob/master/COPYING.txt) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Features

| Feature | Supported |
| :---: | :---: |
| Restart | ✔ |
| Screenshots | ✔ |
| Saves | ✔ |
| States | ✔ |
| Rewind | ✔ |
| Netplay | ✔ |
| Core Options | ✔ |
| Controls | ✔ |
| Remapping | ✔ |
| Disk Control | ✔ |

## ![](/emulators/bios.svg) BIOS

>**No bios is required.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Supported extensions

Roms must have the extension:

* .cpr
* .m3u
* .zip
* .7z

This system supports compressed roms in .zip/.7z format. However, be careful, it is only an archive.

The files contained in the .zip/.7z must correspond to the extensions mentioned above.
Moreover, each .zip/.7z file must contain only one compressed rom.

### Location

Put the roms like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 gx4000
┃ ┃ ┃ ┃ ┣ 🗒 **game.zip**

>The roms using **TOSEC** format are highly recommended.
{.is-success}

>For more information about roms, go to [this tutorial](./../../../tutorials/games/generalities/isos-and-roms)!
{.is-info}

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Accessing to options

You can configure various options in two different ways.

* Via RetroArch menu:

┣ 📁 RetroArch Menu
┃ ┣ 📁 Core options
┃ ┃ ┣ 🧩 Name_option

* Via the file `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core options

| Options | Options values | Variables (for configuration override) | Variables values |
| --- | --- | --- | --- |
| User 1 Amstrad Joystick Config | `joystick` ✅ / `qaop` / `incentive` | `cap32_retrojoy0` | `joystick` / `qaop` / `incentive` |
| User 2 Amstrad Joystick Config | `joystick` ✅ / `qaop` / `incentive` / `joystick_port2` | `cap32_retrojoy1` | `joystick` / `qaop` / `incentive` / `joystick_port2` |
| Combo Key | `select` / `y` ✅ / `b` / `Disabled` | `cap32_combokey` | `select` / `y` / `b` / `disabled` |
| Autorun | `Enabled` ✅ / `Disabled` | `cap32_autorun` | `enabled` / `disabled` |
| Model | `6128` / `464` / `6128+` ✅ | `cap32_model` | `6128` / `464` / `6128+` |
| Advanced > Ram size | `128` ✅ / `64` / `192` / `512` / `576` | `cap32_ram` | `128` / `64` / `192` / `512` / `576` |
| Advanced > Green Phosphor blueish | `15` ✅ / `20` / `30` / `5` / `10` | `cap32_advanced_green_phosphor` | `15` / `20` / `30` / `5` / `10` |
| Advanced > Monitor Intensity | `8` / `9` / `10` ✅ / `11` / `12` / `13` / `14` / `15` / `5` / `6` / `7` | `cap32_scr_intensity` | `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `5` / `6` / `7` |
| Monitor Type | `color` ✅ / `green` / `white` | `cap32_scr_tube` | `color` / `green` / `white` |
| CPC Language | `english` ✅ / `french` / `spanish` | `cap32_lang_layout` | `english` / `french` / `spanish` |

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/libretro/libretro-cap32/](https://github.com/libretro/libretro-cap32/)
* **Libretro documentation**: [https://docs.libretro.com/library/caprice32/](https://docs.libretro.com/library/caprice32/)