---
title: Libretro-cdi2015
description: 
published: true
date: 2024-07-24T09:54:49.648Z
tags: libretro, cdi, cdi2015, 8.1+
editor: markdown
dateCreated: 2022-02-03T17:26:17.385Z
---

**Libretro-cdi2015** is a fork core from MAME2015.

## ![](/emulators/license.svg) License

This core is under **MAME non-commercial** license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ❌ | ❌ | ✅ | ✅ | ❌ | ❌ | ✅ | ✅ |

## ![](/emulators/features.png) Features

| Feature | Supported |
| :---: | :---: |
| Restart | ✔ |
| Screenshots | ✔ |
| Saves | ✔ |
| States | ✔ |
| Core Options | ✔ |

## ![](/emulators/bios.svg) BIOS

### List of mandatory bios

| Filename | Description | MD5 | Provided |
| :---: | :---: | :---: | :---: |
| cdimono1.zip | CD-i (Mono-I) (PAL) | ce590c4787460161359f2b680136cb60 bbe6636ac11d3744f0710e8e820187e7 | ❌ |

### Location

Put the bios like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 cdi
┃ ┃ ┃ ┃ ┣ 🗒 **cdimono1.zip**

## ![](/emulators/isos.svg) Isos

### Supported extensions

Isos must have the extensions:

* .chd
* .cmd

### Location

Put the isos like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 cdi
┃ ┃ ┃ ┃ ┣ 🗒  **game.chd**

>The isos using **Redump** format are highly recommended.
{.is-success}

>For more information about roms, go to [this tutorial](./../../../tutorials/games/generalities/isos-and-roms)!
{.is-info}

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Accessing to options

You can configure various options in two different ways.

* Via RetroArch menu:

┣ 📁 RetroArch Menu
┃ ┣ 📁 Core options
┃ ┃ ┣ 🧩 Name_option

* Via the file `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core options

| Options | Options values | Variables (for configuration override) | Variables values |
| --- | --- | --- | --- |
| Read configuration | `Disabled` ✅ / `Enabled` | `cdi2015_read_config` | `disabled` / `enabled` |
| Auto save/load states | `Disabled` ✅ / `Enabled` | `cdi2015_auto_save` | `disabled` / `enabled` |
| XY device (Restart) | `none` ✅ / `lightgun` / `mouse` | `cdi2015_mouse_mode` | `none` / `lightgun` / `mouse` |
| Enable throttle | `Disabled` ✅ / `Enabled` | `cdi2015_throttle` | `disabled` / `enabled` |
| Enable cheats | `Disabled` ✅ / `Enabled` | `cdi2015_cheats_enable` | `disabled` / `enabled` |
| Hide nag screen | `Disabled` ✅ / `Enabled` | `cdi2015_hide_nagscreen` | `disabled` / `enabled` |
| Hide gameinfo screen | `Disabled` ✅ / `Enabled` | `cdi2015_hide_infoscreen` | `disabled` / `enabled` |
| Hide warnings screen | `Disabled` ✅ / `Enabled` | `cdi2015_hide_warnings` | `disabled` / `enabled` |
| Alternate rendre method | `Disabled` ✅ / `Enabled` | `cdi2015_alternate_renderer` | `disabled` / `enabled` |

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/libretro/mame2015-libretro/](https://github.com/libretro/mame2015-libretro/)