---
title: Pico
description: 
published: true
date: 2024-07-26T07:58:46.590Z
tags: 
editor: markdown
dateCreated: 2024-07-11T19:46:00.720Z
---

![](/emulators/consoles/pico.svg){.align-center}

## Ficha técnica

* **Fabricante :** Sega
* **Año de lanzamiento :** 1993
* **Procesador :** 16-Bit Motorola 68000 @ 7.6 MHz
* **Resolución :** 320 x 224 píxeles  NTSC, 320 x 240 PAL

## Presentación

El Sega Pico, también conocido como Kids Computer Pico, es una consola de videojuegos educativos de Sega Toys. El Pico salio en junio de 1993 en Japón y en noviembre de 1994 en América del Norte y Europa, y luego en China en 2002.

Comercializada bajo el nombre de "edutainment", la Pico estaba principalmente destinada a videojuegos educativos para niños de 3 a 7 años. Las versiones del Pico se centraban en la educación infantil e incluían títulos respaldados por personajes animados con licencia, incluyendo la serie Sonic the Hedgehog de Sega.

Aunque la Pico se vendió sin interrupción en Japón hasta el lanzamiento de la Beena, en América del Norte y Europa tuvo menos éxito y fue descontinuada a principios de 1998, antes de ser reeditada por Majesco Entertainment. En total, Sega reclama la venta de 3,4 millones de consolas Pico y 11,2 millones de cartuchos de juego, y más de 350,000 consolas Beena y 800,000 cartuchos. La Advanced Pico Beena, lanzada en Japón en 2005, la sucedió.

## Emuladores

[Libretro PicoDrive](libretro-picodrive)