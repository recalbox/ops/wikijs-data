---
title: Super Nintendo
description: 
published: true
date: 2021-08-08T09:31:08.656Z
tags: snes, super nintendo, super famicom
editor: markdown
dateCreated: 2021-08-07T23:58:23.029Z
---

With the tutorials available here, you will discover how you can play with up to 4 players!

Here are the available tutorials:

[Play to 3 to 4 players](play-to-3-to-4-players)