---
title: Update Raspberry Pi 4/400/5 bootloader
description: 
published: true
date: 2024-09-18T14:37:31.698Z
tags: update, rpi4, bootloader, rpi400, rpi5
editor: markdown
dateCreated: 2023-01-28T00:52:31.868Z
---

## Introduction

The Raspberry bootloader (also known as eeprom) is a firmware that allows the Raspberry Pi 4/400/5 to boot.

It is advisable to update the bootloader regularly, which can correct some bugs in the use of the Raspberry Pi 4/400/5.

## Procedure

The bootloader update process is very simple. You just need to update your Recalbox to the latest version.

In the interface, press `START` and go to `ADVANCED SETTINGS` > `BOOTLOADER UPDATE`.

Once you have validated the menus, you will be presented with a window showing the version installed and the version available.

![](/tutorials/utilities/upgrade-rpi4-bootloader/bootloaderen1.png){.full-width}

Complete the update with `UPDATE`. Less than 10 seconds later, you should have a successful update message.

![](/tutorials/utilities/upgrade-rpi4-bootloader/bootloaderen2.png){.full-width}

Once the message has been validated, you will be prompted to reboot. Confirm the reboot to use the updated bootloader.

![](/tutorials/utilities/upgrade-rpi4-bootloader/bootloaderen3.png){.full-width}

You can check that the bootloader is up to date by returning to the same menu. You will then get a message telling you that the bootloader is up to date.

![](/tutorials/utilities/upgrade-rpi4-bootloader/bootloaderen4.png){.full-width}