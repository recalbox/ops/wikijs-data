---
title: PCSX-ReARMed
description: 
published: true
date: 2024-07-06T19:58:51.717Z
tags: ps1, playstation 1, psx, pcsx-rearmed
editor: markdown
dateCreated: 2023-09-07T16:35:49.983Z
---



## ![](/emulators/license.svg) Licencia

El core posee la siguiente licencia [**GPLv2**](https://github.com/notaz/pcsx_rearmed/blob/master/COPYING).

## ![](/emulators/compatibility.png) Compatibilidad

| RPI0/RPI1 |RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅  🐌 | ❌ | ❌ | ❌ | ❌ | ❌ | ❌ | ❌ |

🐌 &nbsp;Bajo rendimiento pero jugable

## ![](/emulators/features.png) Funcionalidades



## ![](/emulators/bios.svg) BIOS

### Lista de BIOS obligatorias

Aunque algunos juegos pueden iniciarse sin bios bajo _PCSX-ReARMed_, se recomienda encarecidamente añadir estos ficheros al directorio de bios para mejorar el rendimiento del emulador y la gestión de copias de seguridad sobre tarjetas de memoria "virtuales".

| Nombre del fichero | Descripción | MD5 | ¿Preinstalada? |
| :---: | :---: | :---: | :---: |
| scph101.bin | Version 4.4 03/24/00 A | 6e3735ff4c7dc899ee98981385f6f3d0 | ❌ |
| scph7001.bin | Version 4.1 12/16/97 A | 1e68c231d0896b7eadcad1d7d8e76129 | ❌ |
| scph5501.bin | Version 3.0 11/18/96 A | 490f666e1afb15b7362b406ed1cea246 | ❌ |
| scph1001.bin | Version 2.0 05/07/95 A | dc2b9bf8da62ec93e868cfd29f0d067d 924e392ed05558ffdb115408c263dccf | ❌ |

### Emplazamiento

Disponed vuestras bios como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 🗒 **scph101.bin**
┃ ┃ ┃ ┣ 🗒 **scph7001.bin**
┃ ┃ ┃ ┣ 🗒 **scph5501.bin**
┃ ┃ ┃ ┣ 🗒 **scph1001.bin**

## ![](/emulators/isos.svg) Isos

### Extensiones de roms soportadas

Las isos deben tener las extensiones siguientes:

* .bin/.cue
* .img/.ccd/.sub
* .mdf/.mts
* .pbp
* .bin/.toc
* .cbn

### Emplazamiento

Disponed vuestras isos como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 psx
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.bin**
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.cue**

>Se aconsejan las isos del proyecto **Redump**.
{.is-success}

>Para más información sobre las isos, consultad [este tutorial](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

>Para conservar las configuraciones personalizadas cuando se produce una actualización del sistema Recalbox, os aconsejamos utilizar las [Sobrecargas de configuración](./../../../advanced-usage/configuration-override).
{.is-info}

## ![](/emulators/external-links.png) Enlaces externos

* **Código fuente utilizado** : [https://github.com/notaz/pcsx_rearmed/](https://github.com/notaz/pcsx_rearmed/)