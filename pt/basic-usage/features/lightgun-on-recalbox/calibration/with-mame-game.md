---
title: Em um jogo do MAME
description: 
published: true
date: 2021-07-18T14:32:05.022Z
tags: 
editor: markdown
dateCreated: 2021-07-18T14:30:21.441Z
---

## Exemplo de calibração no MAME 2003 plus

1) Inicie o jogo e ative este menu pressionando L3 em seu controle.

![](/basic-usage/features/lightgun/calibration/mame1.png){.full-width}

2) Vá para baixo com seu dpad ou stick esquerdo para "Analog Controls" e pressione `X` _(Controle SNES como referência, claro)_

![](/basic-usage/features/lightgun/calibration/mame2.png){.full-width}

3) Escolha e defina "Lightgun X sensitivity" e "Lightgun Y sensitivity" para 50 ou 100% (pode estar em 25% por padrão) com seu dpad ou stick esquerdo.

![](/basic-usage/features/lightgun/calibration/mame3.png){.full-width}

4) Saia dos menus pressionando `X` _(Controle SNES como referência)_ para voltar ao jogo. Os parâmetros funcionarão imediatamente, Aproveite!!! 

5) No próximo início, a configuração será mantida (mantenha seu "save" seguro!!!)

> Você deve manter estes arquivos. Assim, você não terá que refazer a manipulação, que estará em seus saves: 

>![](/basic-usage/features/lightgun/calibration/calibration1.png){.full-width}
{.is-warning}