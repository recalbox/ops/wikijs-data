---
title: Cómo contribuir a la wiki de Recalbox
description: Descubra cómo puede contribuir a la wiki
published: true
date: 2021-08-02T16:31:43.986Z
tags: 
editor: markdown
dateCreated: 2021-07-29T10:00:27.496Z
---

## Nomenclatura de los documentos

Los documentos siguen una nomenclatura específica que permite que la Wiki Recalbox tenga URLs idénticas para las diferentes traducciones de los documentos. 

Por ejemplo, esta página, en lengua francesa, tiene la URL :
`/fr/contribute`
Y en inglés:
`/en/contribute`

Esto le permite encontrar la traducción de un documento muy rápidamente, o crear traducciones más fácilmente para los colaboradores.

Por eso las URL de los documentos aparecen en inglés.

## Reglas de formato

Los documentos están escritos en [markdown](https://docs.requarks.io/editors/markdown). Markdown es una herramienta de formato de documentos muy sencilla y accesible. No dude en utilizar este documento como ejemplo para empezar a escribir el suyo propio.

### Títulos

El título de la página se establece en las opciones de la página. Es el único título de nivel 1 del documento.
![título-metadatos.png](/contribute/title-metadata.png)

En las páginas de contenido, los títulos comienzan con el nivel 2 (*doble hashtag* en markdown: `##`):
![double-hash.png](/contribute/double-hash.png)

### Bloques de citas

Para alertar al usuario sobre información o elementos importantes, puede utilizar bloques de citas:

```markdown
> Esto es información
> {.is-info}
```

> Esto es información
> {.is-info}

## Versión de markdown soportada

Para obtener más información sobre las funciones de markdown compatibles, visite https://docs.requarks.io/editors/markdown.

## Imágenes

Cuando añada imágenes a sus documentos, asegúrese de replicar la jerarquía del documento en las carpetas de imágenes, omitiendo el idioma.

Por ejemplo, para esta página, las imágenes se crearán en el directorio `/contribute`

## Internacionalización

### Procedimiento para traducir una página

1. Inicie sesión en la wiki con su cuenta de Discord
2. Ir a la página a traducir, en el idioma por defecto de la Wiki (FR)
3. Haga clic en el icono del idioma en la parte superior de la página y seleccione el idioma al que desea traducir la página.
![select-lang.png](/contribute/select-lang.png)
4. La pantalla le ofrecerá crear una nueva página, haga clic en el botón de crear página
5. Haga clic en el editor Markdown
![markdown.png](/contribute/markdown.png)
6. Abra una nueva pestaña en la página original y haga clic en editar en el menú de acciones de la página
![edit.png](/contribute/edit.png)
7. Acceda al título, las *etiquetas* y la descripción de la página haciendo clic en el botón PAGE:
![editpage.png](/contribute/editpage.png)
8. Puede traducir estos elementos y añadirlos al formulario de la página recién creada
9. Después de cerrar el título *popup* de la página original, copie el contenido de la página y vaya a https://www.deepl.com/translator para pegar su texto y obtener una versión pretraducida de la página.
![deepl.png](/contribute/deepl.png)
10. Ahora sólo tienes que comprobar la traducción, las imágenes y los enlaces de la página recién creada.
11. No olvides eliminar la última línea de texto que añade automáticamente deepl: **(traducido por deepl)**
12. Muchas gracias por tu aportación 🙏


### Links relativos

Salvo en casos excepcionales, los enlaces de las páginas deben ser relativos al idioma actual.

Por ejemplo, en esta página, un enlace a la página `es/home` debe rellenarse con la URL `./home` :

```markdown
Visita la [página](./home)
```

Esto permite crear páginas de traducción que ya tienen enlaces válidos.

### Añadiendo nuevos idiomas

Si tu idioma no aparece en el menú de la wiki y quieres iniciar una traducción, ponte en contacto con el equipo en nuestras **[redes sociales, Discord o foro](./presentation/useful-links).**

## Nivel de dificultad de los tutoriales

Para que la experiencia de Recalbox sea divertida y accesible, cuando crees un tutorial, por favor menciona el nivel de dificultad del mismo, teniendo en cuenta la siguiente escala:

1.  Cualquier acción que no requiera introducir la configuración avanzada en el menú general o en los archivos .conf de Recalbox será considerada como : _nivel :star:_

1. Cualquier acción que requiera configurar los ajustes avanzados para realizar cambios que puedan causar inestabilidades : _nivel:star::star:_

1.  Cualquier acción que requiera cambiar las entradas en el archivo recalbox.conf: _nivel:star::star::star:_ (se recomienda precaución).

1.  Cualquier acción que requiera manipulaciones SSH y/o modificaciones en el archivo de inicio (MobaXTerm o Powershell): _nivel:star::star::star::star:_

1.  Cualquier acción que requiera una *cuestión* abierta, o hacer una *solicitud de fusión*: _nivel:star::star::star::star:_

1.  Por último, cualquier tutorial que intente explicar el uso de clrmamepro será automáticamente clasificado como : :scream: _**Pesadilla**_

Ejemplo:

- Añadir una ROM, una BIOS, cambiar el idioma: nivel:star: 
- Cambiar el tema, restaurar la configuración de fábrica, cambiar el núcleo por defecto: nivel :star: :star: 
- Activar la traducción inteligente (Retroarch IA): nivel :star: :star: :star: 


*Para que el emoji de la estrella aparezca en tus tutoriales, pon ":" delante y detrás de la palabra "star" = :star: todo junto.*