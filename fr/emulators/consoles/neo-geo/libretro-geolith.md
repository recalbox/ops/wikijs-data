---
title: Libretro Geolith
description: 
published: true
date: 2024-06-22T09:59:00.851Z
tags: libretro, neo-geo, 9.2+, geolith
editor: markdown
dateCreated: 2024-06-22T09:59:00.851Z
---


## ![](/emulators/license.svg) Licence

Ce core est sous licence [**BSD-3**](https://github.com/libretro/geolith-libretro/blob/master/LICENSE).

## ![](/emulators/compatibility.png) Compatibilité

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Fonctionnalités

| Fonctionnalité | Supporté |
| :---: | :---: |

## ![](/emulators/bios.svg) BIOS

### Liste des bios optionnels

| Nom de fichier | Description | MD5 | Fourni |
| :---: | :---: | :---: | :---: |
| aes.zip | BIOS Neo-Geo AES | ad9585c72130c56f04ae26aae87c289d 7a4dcd56b3924215aac7f903ff907339 | ❌ |
| neogeo.zip | BIOS Neo-Geo | 00dad01abdbf8ea9e79ad2fe11bdb182 912c0a56bafe0fba39d0c668b139b250 | ❌ |

### Emplacement

Placez les BIOS comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 geolith
┃ ┃ ┃ ┃ ┣ 🗒 **aes.zip**
┃ ┃ ┃ ┃ ┣ 🗒 **neogeo.zip**

## ![](/emulators/roms.png) Roms

### Extensions supportées

La rom doit avoir l'extension :

* .neo

### Emplacement

Placez les roms comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 neogeo
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.neo**

>Pour plus d'information sur les roms, rendez-vous sur [ce tutoriel](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuration avancée de l'émulateur

>Pour pouvoir conserver vos configurations personnalisées lors d'une mise à jour, nous vous conseillons d'utiliser notre fonctionnalité [Surcharges de configuration](./../../../advanced-usage/configuration-override).
{.is-info}

### Accéder aux options

Vous pouvez configurer diverses options de deux façons différentes.

* Via le menu RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via le fichier `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Options du core

| Options | Valeurs des options | Variable (pour surcharge) | Valeurs des variables |
| --- | --- | --- | --- |
| System Type (Restart) | `Neo Geo AES (Home Console)` ✅ / `Neo Geo MVS (Arcade)` / `Universe BIOS (Community-enhanced BIOS)` | `geolith_system_type` | `aes` / `mvs` / `uni` |
| Region (Restart) | `USA` ✅ / `Japan` / `Asia` / `Europe` | `geolith_region` | `us` / `jp` / `as` / `eu` |
| Setting Mode (Restart, DIP Switch) | `Désactivé` ✅ / `Activé` | `geolith_settingmode` | `off` / `on` |
| Four Player Mode (Restart, Asia/Japan MVS Only) | `Désactivé` ✅ / `Activé` | `geolith_4player` | `off` / `on` |
| Freeplay (DIP Switch) | `Désactivé` ✅ / `Activé` | `geolith_freeplay` | `off` / `on` |
| Mask Overscan (Top) | `16` / `12` / `8` ✅ / `4` / `0` | `geolith_overscan_t` | `16` / `12` / `8` / `4` / `0` |
| Mask Overscan (Bottom) | `16` / `12` / `8` ✅ / `4` / `0` | `geolith_overscan_b` | `16` / `12` / `8` / `4` / `0` |
| Mask Overscan (Left) | `16` / `12` / `8` ✅ / `4` / `0` | `geolith_overscan_l` | `16` / `12` / `8` / `4` / `0` |
| Mask Overscan (Right) | `16` / `12` / `8` ✅ / `4` / `0` | `geolith_overscan_r` | `16` / `12` / `8` / `4` / `0` |
| Aspect Ratio | `Perfectly Square Pixels (1:1 PAR)` ✅ / `Ostensibly Accurate NTSC Aspect Ratio (45:44 PAR)` / `Very Traditional NTSC Aspect Ratio (4:3 PAR)` | `geolith_aspect` | `1:1` / `45:44` / `4:3` |
| Sprites-per-line limit (Hack) | `Hardware Accurate (96)` ✅ / `Double (192)` / `Triple (288)` / `MAX 381 MEGA PRO-GEAR SPEC` | `geolith_sprlimit` | `96` / `192` / `288` / `381` |
| Overclocking (Hack) | `Désactivé` ✅ / `Activé` | `geolith_oc` | `off` / `on` |

## ![](/emulators/external-links.png) Liens externes

* **Code source utilisé :** [https://github.com/libretro/geolith-libretro/](https://github.com/libretro/geolith-libretro)
* **Documentation Libretro** :