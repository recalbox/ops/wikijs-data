---
title: HDMI
description: 
published: true
date: 2021-09-04T22:21:20.991Z
tags: hdmi
editor: markdown
dateCreated: 2021-05-21T08:11:41.290Z
---

Vous pouvez gérer certains aspects des sorties HDMI, auquel vous trouverez des tutoriaux liés ici.

Liste des tutoriaux disponibles :

[Ignorer le HDMI CEC sur l'Odroid XU4](ignore-hdmi-cec-odroidxu4)