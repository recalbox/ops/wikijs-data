---
title: DatUtil
description: 
published: true
date: 2023-09-17T07:58:07.683Z
tags: datutil
editor: markdown
dateCreated: 2023-09-17T07:58:07.683Z
---

## Introducción

Desde hace algún tiempo, nos llegan preguntas sobre cómo crear un fichero dat filtrado a partir del fichero dat original.

Aquí se explica cómo hacerlo con la herramienta de línea de comandos [DatUtil](http://www.logiqx.com/Tools/DatUtil/dutil246.zip) mediante un ejemplo.  
En el último aparto de esta página se presentan los comandos más utilizados.

## Descripción

DatUtil es una herramienta de línea de comandos, que os permite modificar y ajustar los ficheros DAT de muchas formas diferentes. Podéis modificar ficheros DAT de cualquier tamaño, de forma más sencilla que con DatWorkshop Pro.

### Ejemplos

* Queréis crear un fichero DAT que incluya SOLO Juegos NeoGeo.
* En resumen, este fichero sólo incluye las roms padre (no Clones).
* En este ejemplo, usamos el fichero "MAME-ROMs (v0.176_XML).dat".

## Contenido

La herramienta viene con un `readme.txt`, que es bastante claro.

A continuación, podéis ver la parte más importante del `readme.txt`:

### Valores por defecto

> Fix merge (antes la opción -m)  
> Borrar ROMs / discos / muestras duplicadas

### Registros

| Argumento | Descripción |
| :---: | :---: |
| -f | formato de salida (listinfo, listxml, romcenter2, délimité, sous-liste, etc.) | 
| -q | utilizad siempre comillas en las cadenas (sólo se aplica a la salida listinfo) |
| -k | conservad el máximo de información posible del fichero fuente |
| -j | Nebula Jukebox - cargar Z80 y las ROMs de muestra de romdata |
| -o | escribir en el fichero |
| -a | añadir al directorio |
| -t | modo de test - no se modifica nada (sólo cargar y limpiar) |

### Texto de cabecera

| Argumento | Descripción |
| :---: | :---: |
| -un | autor |
| -V | versión |
| -C | categoría |
| -R | réf nombre |
| -F | nombre completo (des decir, Descripción) |
| -T | fecha |
| -E | correo |
| -H | página de inicio |
| -U | url |
| -O | comentario |
| -M | fusión (ninguna, fraccionada o completa) |
| -P | empaquetado (zip o unzip) |
| -N | nodump (obsoleto, obligatorio o ignorado) |

### Selección de juegos

| Argumento | Descripción |
| :---: | :---: |
| -g | selección individual de conjuntos. Utilice el símbolo @ para especificar un archivo de nombre. |
| -c | incluir clones (utilizar con la opción -g) |
| -g | seleccionar conjuntos de un archivo de origen especificado. |
| -G | seleccionar conjuntos de un archivo fuente especificado. También se admite el método @. |
| -S | seleccionar conjuntos utilizando una descripción de subcadena. Se admite el método @. |
| -! | Cambiar las opciones -g, -G, -S (incluyendo la opción -c) para excluir sets |
| -r | borrar clones |


### Limpieza

| Argumento | Descripción |
| :---: | :---: |
| -l | poner en minúsculas los nombres de juegos y ROMs |
| -s | ordenar los juegos por padre (ordenados por el nombre del padre y luego por el nombre del juego) | 
| -i | incluir la información que falta en el archivo de datos de referencia
| -i | incluir información que falta de un archivo de datos de referencia |
| -I | incorporar juegos de un archivo de datos "complementario" | 
| -X | reparar fusión (¡no lo use si no entiende las consecuencias!) |
| -D | eliminar ROMs/discos/muestras duplicados (como arriba en términos de uso) | 
| -p | plum (borrar) ROMs, discos y/o muestras (por ejemplo `-p disco, muestra`) |

### MD5 / SHA1:

| Argumento | Descripción |
| :---: | :---: |
| -x | Calcular SHA1 / MD5 al escanear archivos o ZIPs en un directorio | 
| -m | Utilizar sumas de comprobación MD5 en lugar de SHA1 (por ejemplo, si se utiliza -x, se puede utilizar -m) |

### Information:  

| Argumento | Descripción |
| :---: | :---: |
| -v | Registro detallado |
| -d | Mostrar mensajes de depuración |

## Instrucciones sobre un ejemplo

1. Copiad el fichero `MAME - ROMs (v0.176_XML).dat` al directorio DatUtil.
2. Abrid un símbolo del sistema como administrador
3. En el símbolo del sistema, abrid el directorio DatUtil
4. Ejecutad la siguiente línea:

```shell
datutil.exe -g neogeo -c -r -f generic -A Nachtgarm -C "Standard DatFile" -F NeoGeoOnlyNoClones -R NeoGeo_only_NO_clones -V "1.0" -o NeoGeoOnlyNoClones.dat "MAME - ROMs (v0.176_XML).dat"
```

El resultado será un nuevo fichero `dat` con el nombre `NeoGeoOnlyNoClones.dat`, que puede ser abierto con Romulus o clrMamePro. Incluye 159 juegos y una biografía.

Aquí hay un extracto del fichero creado:

```xml
<?xml version="1.0"?>

<!DOCTYPE datafile PUBLIC "-//Logiqx//DTD ROM Management Datafile//EN" "http://www.logiqx.com/Dats/datafile.dtd">
<datafile>
	<header>
		<name>NeoGeo_only_NO_clones</name>
		<description>NeoGeoOnlyNoClones</description>
		<category>Standard DatFile</category>
		<version>1.0</version>
		<author>Nachtgarm</author>
	</header>
	<game name="2020bb" romof="neogeo">
		<description>2020 Super Baseball (set 1)</description>
		<year>1991</year>
		<manufacturer>SNK / Pallas</manufacturer>
		<rom name="sp-s2.sp1" merge="sp-s2.sp1" size="131072" crc="9036d879" sha1="4f5ed7105b7128794654ce82b51723e16e389543"/>
		<rom name="sp-s.sp1" merge="sp-s.sp1" size="131072" crc="c7f2fa45" sha1="09576ff20b4d6b365e78e6a5698ea450262697cd"/>
		<rom name="sp-u2.sp1" merge="sp-u2.sp1" size="131072" crc="e72943de" sha1="5c6bba07d2ec8ac95776aa3511109f5e1e2e92eb"/>
		<rom name="sp-e.sp1" merge="sp-e.sp1" size="131072" crc="2723a5b5" sha1="5dbff7531cf04886cde3ef022fb5ca687573dcb8"/>
		<rom name="v2.bin" merge="v2.bin" size="131072" crc="62f021f4" sha1="62d372269e1b3161c64ae21123655a0a22ffd1bb"/>
		<rom name="asia-s3.rom" merge="asia-s3.rom" size="131072" crc="91b64be3" sha1="720a3e20d26818632aedf2c2fd16c54f213543e1"/>
		<rom name="vs-bios.rom" merge="vs-bios.rom" size="131072" crc="f0e8f27d" sha1="ecf01eda815909f1facec62abf3594eaa8d11075"/>
		<rom name="sp-j2.sp1" merge="sp-j2.sp1" size="131072" crc="acede59c" sha1="b6f97acd282fd7e94d9426078a90f059b5e9dd91"/>
		<rom name="sp1.jipan.1024" merge="sp1.jipan.1024" size="131072" crc="9fb0abe4" sha1="18a987ce2229df79a8cf6a84f968f0e42ce4e59d"/>
		<rom name="sp-45.sp1" merge="sp-45.sp1" size="524288" crc="03cc9f6a" sha1="cdf1f49e3ff2bac528c21ed28449cf35b7957dc1"/>
		<rom name="japan-j3.bin" merge="japan-j3.bin" size="131072" crc="dff6d41f" sha1="e92910e20092577a4523a6b39d578a71d4de7085"/>
		<rom name="sp-1v1_3db8c.bin" merge="sp-1v1_3db8c.bin" size="131072" crc="162f0ebe" sha1="fe1c6dd3dfcf97d960065b1bb46c1e11cb7bf271"/>
		<rom name="uni-bios_3_2.rom" merge="uni-bios_3_2.rom" size="131072" crc="a4e8b9b3" sha1="c92f18c3f1edda543d264ecd0ea915240e7c8258"/>
		<rom name="uni-bios_3_1.rom" merge="uni-bios_3_1.rom" size="131072" crc="0c58093f" sha1="29329a3448c2505e1ff45ffa75e61e9693165153"/>
		<rom name="uni-bios_3_0.rom" merge="uni-bios_3_0.rom" size="131072" crc="a97c89a9" sha1="97a5eff3b119062f10e31ad6f04fe4b90d366e7f"/>
		<rom name="uni-bios_2_3.rom" merge="uni-bios_2_3.rom" size="131072" crc="27664eb5" sha1="5b02900a3ccf3df168bdcfc98458136fd2b92ac0"/>
		<rom name="uni-bios_2_3o.rom" merge="uni-bios_2_3o.rom" size="131072" crc="601720ae" sha1="1b8a72c720cdb5ee3f1d735bbcf447b09204b8d9"/>
		<rom name="uni-bios_2_2.rom" merge="uni-bios_2_2.rom" size="131072" crc="2d50996a" sha1="5241a4fb0c63b1a23fd1da8efa9c9a9bd3b4279c"/>
		<rom name="uni-bios_2_1.rom" merge="uni-bios_2_1.rom" size="131072" crc="8dabf76b" sha1="c23732c4491d966cf0373c65c83c7a4e88f0082c"/>
		<rom name="uni-bios_2_0.rom" merge="uni-bios_2_0.rom" size="131072" crc="0c12c2ad" sha1="37bcd4d30f3892078b46841d895a6eff16dc921e"/>
		<rom name="uni-bios_1_3.rom" merge="uni-bios_1_3.rom" size="131072" crc="b24b44a0" sha1="eca8851d30557b97c309a0d9f4a9d20e5b14af4e"/>
		<rom name="uni-bios_1_2.rom" merge="uni-bios_1_2.rom" size="131072" crc="4fa698e9" sha1="682e13ec1c42beaa2d04473967840c88fd52c75a"/>
		<rom name="uni-bios_1_2o.rom" merge="uni-bios_1_2o.rom" size="131072" crc="e19d3ce9" sha1="af88ef837f44a3af2d7144bb46a37c8512b67770"/>
		<rom name="uni-bios_1_1.rom" merge="uni-bios_1_1.rom" size="131072" crc="5dda0d84" sha1="4153d533c02926a2577e49c32657214781ff29b7"/>
		<rom name="uni-bios_1_0.rom" merge="uni-bios_1_0.rom" size="131072" crc="0ce453a0" sha1="3b4c0cd26c176fc6b26c3a2f95143dd478f6abf9"/>
		<rom name="030-p1.p1" size="524288" crc="d396c9cb" sha1="47ba421d14d05b965a8d44e7475b227a208e5a07"/>
		<rom name="030-s1.s1" size="131072" crc="7015b8fc" sha1="8c09bc3e6c62e0f7c9557c1e10c901be325bae7f"/>
		<rom name="sfix.sfix" merge="sfix.sfix" size="131072" crc="c2ea0cfd" sha1="fd4a618cdcdbf849374f0a50dd8efe9dbab706c3"/>
		<rom name="000-lo.lo" merge="000-lo.lo" size="131072" crc="5a86cff2" sha1="5992277debadeb64d1c1c64b0a92d9293eaf7e4a"/>
		<rom name="sm1.sm1" merge="sm1.sm1" size="131072" crc="94416d67" sha1="42f9d7ddd6c0931fd64226a60dc73602b2819dcf"/>
		<rom name="030-m1.m1" size="131072" crc="4cf466ec" sha1="6a003b53c7a4af9d7529e2c10f27ffc4e58dcda5"/>
		<rom name="030-v1.v1" size="1048576" crc="d4ca364e" sha1="b0573744b0ea2ef1e2167a225f0d254883f5af04"/>
		<rom name="030-v2.v2" size="1048576" crc="54994455" sha1="76eb62b86e8ed51a77f44313d5cc8091b3f58d57"/>
		<rom name="030-c1.c1" size="1048576" crc="4f5e19bd" sha1="ef7975c4b33a7aea4a25a385f604799f054d3200"/>
		<rom name="030-c2.c2" size="1048576" crc="d6314bf0" sha1="0920cc580d7997fcb0170dd619af2f305d635577"/>
		<rom name="030-c3.c3" size="1048576" crc="47fddfee" sha1="297c505a63448c999a2510c27bf4549102134db8"/>
		<rom name="030-c4.c4" size="1048576" crc="780d1c4e" sha1="2e2cf9de828e3b48642dd2203637103438c62142"/>
	</game>
[...]
</datafile>
```

## Explicación de las cadenas:

`datutil.exe` = El comando para ejecutar la herramienta.
`-g` = Conjunto individual o selección del sistema. En nuestro caso neogeo  
`neogeo` = ver arriba
`-c` = Incluye todos los juegos (el readme es un poco confuso aquí). Si no usas esta opción, sólo se encontrarán las bios de NEOGEO.
`-r` = borra todos los clones
`-f` = determina el formato del nuevo DAT. En nuestro caso `generic`.  
`-A` = Determina el autor. En nuestro caso `Nachtgarm`.
`-C` = Determina el tipo de DAT (Categoría). En nuestro caso `"Standard DatFile"` . Tenga en cuenta el `""`, que son necesarios, debido a los borradores  
`F` = Determina el nombre (Descripción) del DAT en un gestor ROM. En nuestro caso `NeoGeoOnlyNoClones`.  
`R` = Determina el nombre que se muestra en un gestor ROM. En este caso `NeoGeo_only_NO_clones`.  
`V` = Determina la versión del fichero DAT. En nuestro caso `"1.0"` . Nótese el `""` aquí también, por el punto final.  
`-o` = Determina el nuevo nombre del fichero. En nuestro caso `NeoGeoOnlyNoClones.dat`.

## Cadenas más utilizadas

Cada una de las siguientes cadenas espera que el DAT original se coloque en el mismo directorio que la herramienta `datutil.exe`.

**Crear un archivo DAT que incluya sólo juegos NEOGEO y sus clones**.

`datutil.exe -g neogeo -c -f generic -C "Standard DatFile" -o NewFileName.dat "OriginalFile.dat"`

**Crea un archivo DAT que sólo incluya juegos NEOGEO (sin clones)**.

`datutil.exe -g neogeo -c -r -f generic -C "Standard DatFile" -o NewFileName.dat "OriginalFile.dat"`

**Crear un archivo DAT de roms padres solamente (sin NEO-GEO y clones)**.

`datutil.exe -g neogeo -c -! -r -f generic -C "Standard DatFile" -o NewFileName.dat "OriginalFile.dat"`