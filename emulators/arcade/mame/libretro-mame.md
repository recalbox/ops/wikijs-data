---
title: Libretro MAME
description: 
published: true
date: 2024-07-23T23:18:49.813Z
tags: libretro, mame
editor: markdown
dateCreated: 2021-07-05T23:04:51.065Z
---



## ![](/emulators/license.svg) License

This core is under **MAME non-commercial** license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ❌ | ❌ | ✅ | ✅ | ❌ | ❌ | ✅ | ❌ |

## ![](/emulators/features.png) Features



## ![](/emulators/bios.svg) BIOS

BIOS romsets are not required when using "Full Non-Merged" arcade romsets. For "Split" and "Non-Merged" romsets, place the necessary BIOS in the same directory as the game romset.

>**Note**:  
>Place the bios in the directory: `/recalbox/share/roms/mame/`   
>or in its subdirectory if you use several Mame cores, for example: `/recalbox/share/roms/mame/`
>Games and their bios must be in the same directory or sub-directory.
{.is-warning}

## ![](/emulators/roms.png) Roms

Libretro-MAME is based on the latest versions of the MAME emulator and the game catalog is extensive.

[Click here](https://www.mamedev.org/) to get more info about Mame development.

* Based on romset: MAME 0.258 (August 30, 2023) Non-merged
* DatFile: You can find the dat file in your recalbox at location `/recalbox/share/arcade/libretro/mame.dat`.

### Supported extensions

Roms must have the extension:

* .zip
* .chd

CHD files are only needed for certain roms. If there is no CHD file for a rom, the rom will work.

### Location

Put your roms like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 **mame**
┃ ┃ ┃ ┃ ┣ 📁 **game**
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **game.chd**

You can also opt for a subdirectory (useful if you want to have a mame set for another core).

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 mame
┃ ┃ ┃ ┃ ┣ 📁 Mame
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **game.zip**
┃ ┃ ┃ ┃ ┃ ┣ 📁 **game**
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 **game.chd**

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Accessing to options

You can configure various options in two different ways.

* Via RetroArch menu:

┣ 📁 RetroArch Menu
┃ ┣ 📁 Core options
┃ ┃ ┣ 🧩 Name_option

* Via the file `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core options

| Options | Options values | Variables (for configuration override) | Variables values |
| --- | --- | --- | --- |
| Thread Mode | `Désactivé` / `Activé` ✅ | `mame_thread_mode` | `disabled` / `enabled` |
| Cheats | `Désactivé` ✅ / `Activé` | `mame_cheats_enable` | `disabled` / `enabled` |
| Throttle | `Désactivé` ✅ / `Activé` | `mame_throttle` | `disabled` / `enabled` |
| Boot ot BIOS | `Désactivé` ✅ / `Activé` | `mame_boot_to_bios` | `disabled` / `enabled` |
| Boot to OSD | `Désactivé` ✅ / `Activé` | `mame_boot_to_osd` | `disabled` / `enabled` |
| Read Configuration | `Désactivé` ✅ / `Activé` | `mame_read_config` | `disabled` / `enabled` |
| Write Configuration | `Désactivé` ✅ / `Activé` | `mame_write_config` | `disabled` / `enabled` |
| MAME INI Paths | `Désactivé` ✅ / `Activé` | `mame_mame_paths_enable` | `disabled` / `enabled` |
| Save State Naming | `Game` / `System` ✅ | `mame_saves` | `game` / `system` |
| Auto Save/Load States | `Désactivé` ✅ / `Activé` | `mame_auto_save` | `disabled` / `enabled` |
| Softlists | `Désactivé` / `Activé` ✅ | `mame_softlists_enable` | `disabled` / `enabled` |
| Softlist Automatic Media Type | `Désactivé` / `Activé` ✅ | `mame_softlistst_auto_media` | `disabled` / `enabled` |
| Media Type | `cart` / `cass` / `cdrm` / `flop` / `hard` / `prin` / `rom` ✅ / `serl` | `mame_media_type` | `cart` / `cass` / `cdrm` / `flop` / `hard` / `prin` / `rom` / `serl` |
| Joystick Deadzone | `0.00` / `0.05` / `0.10` / `0.15` ✅ / `0.20` / `0.25` / `0.30` / `0.35` / `0.40` / `0.45` / `0.50` / `0.55` / `0.60` / `0.65` / `0.70` / `0.75` / `0.80` / `0.85` / `0.90` / `0.95` / `1.00` | `mame_joystick_deadzone` | `0.00` / `0.05` / `0.10` / `0.15` / `0.20` / `0.25` / `0.30` / `0.35` / `0.40` / `0.45` / `0.50` / `0.55` / `0.60` / `0.65` / `0.70` / `0.75` / `0.80` / `0.85` / `0.90` / `0.95` / `1.00` |
| Joystick Saturation | `0.05` / `0.10` / `0.15` / `0.20` / `0.25` / `0.30` / `0.35` / `0.40` / `0.45` / `0.50` / `0.55` / `0.60` / `0.65` / `0.70` / `0.75` / `0.80` / `0.85` ✅ / `0.90` / `0.95` / `1.00` | `mame_joystick_saturation` | `0.05` / `0.10` / `0.15` / `0.20` / `0.25` / `0.30` / `0.35` / `0.40` / `0.45` / `0.50` / `0.55` / `0.60` / `0.65` / `0.70` / `0.75` / `0.80` / `0.85` / `0.90` / `0.95` / `1.00` |
| Joystick Threshold | `0.05` / `0.10` / `0.15` / `0.20` / `0.25` / `0.30` ✅ / `0.35` / `0.40` / `0.45` / `0.50` / `0.55` / `0.60` / `0.65` / `0.70` / `0.75` / `0.80` / `0.85` / `0.90` / `0.95` / `1.00` | `mame_joystick_threshold` | `0.05` / `0.10` / `0.15` / `0.20` / `0.25` / `0.30` / `0.35` / `0.40` / `0.45` / `0.50` / `0.55` / `0.60` / `0.65` / `0.70` / `0.75` / `0.80` / `0.85` / `0.90` / `0.95` / `1.00` |
| Joystick 4-way Simulation | `Désactivé` ✅ / `Activé` | `mame_mame_4way_enable` | `disabled` / `enabled` |
| Profile Buttons Per Game | `Désactivé` ✅ / `Activé` | `mame_buttons_profiles` | `disabled` / `enabled` |
| Mouse | `Désactivé` / `Activé` ✅ | `mame_mouse_enable` | `disabled` / `enabled` |
| Lightgun Mode | `Lightgun` ✅ / `Touchscreen` / `None` | `mame_lightgun_mode` | `lightgun` / `touchscreen` / `none` |
| Lightgun Offscreen Position | `Free` ✅ / `Fixed (Top Left)` / `Fixed (Bottom Right)` | `mame_lightgun_offscreen_mode` | `free` / `fixed (top left)` / `fixed (bottom right)` |
| Screen Rotation Mode | `Libretro` ✅ / `Internal` / `TATE-ROL` / `TATE_ROR` / `None` | `mame_rotation_mode` | `libretro` / `internal` / `tate-rol` / `tate-ror` / `none` |
| Alternate Renderer | `Désactivé` ✅ / `Activé` | `mame_alternate_renderer` | `disabled` / `enabled` |
| Alternate Renderer Resolution | `640x480` ✅ / `640x360` / `800x600` / `800x450` / `960x720` / `960x540` / `1024x768` / `1024x576` / `1280x960` / `1280x720` / `1600x1200` / `1600x900` / `1440x1080` / `1920x1080` / `1920x1440` / `2560x1440` / `2880x2160` / `3840x2160` | `mame_altres` | `640x480` / `640x360` / `800x600` / `800x450` / `960x720` / `960x540` / `1024x768` / `1024x576` / `1280x960` / `1280x720` / `1600x1200` / `1600x900` / `1440x1080` / `1920x1080` / `1920x1440` / `2560x1440` / `2880x2160` / `3840x2160` |
| CPU Overclock | `Default` / `25` / `26` / `27` / `28` / `29` / `30` / `31` / `32` / `33` / `34` / `35` / `36` / `37` / `38` / `39` / `40` / `41` / `42` / `43` / `44` / `45` / `46` / `47` / `48` / `49` / `50` / `51` / `52` / `53` / `54` / `55` / `60` / `65` / `70` / `75` / `80` / `85` / `90` / `95` / `100` / `105` / `110` / `115` / `120` / `125` / `130` / `135` / `140` / `145` / `150` / `155` / `160` / `165` / `170` / `175` / `180` / `185` / `190` / `195` / `200` / `205` / `210` / `215` / `220` / `225` / `230` / `235` / `240` / `245` / `250` / `255` / `260` / `265` / `270` / `275` / `280` / `285` / `290` / `295` / `300` | `mame_cpu_overclock` | `default` / `25` / `26` / `27` / `28` / `29` / `30` / `31` / `32` / `33` / `34` / `35` / `36` / `37` / `38` / `39` / `40` / `41` / `42` / `43` / `44` / `45` / `46` / `47` / `48` / `49` / `50` / `51` / `52` / `53` / `54` / `55` / `60` / `65` / `70` / `75` / `80` / `85` / `90` / `95` / `100` / `105` / `110` / `115` / `120` / `125` / `130` / `135` / `140` / `145` / `150` / `155` / `160` / `165` / `170` / `175` / `180` / `185` / `190` / `195` / `200` / `205` / `210` / `215` / `220` / `225` / `230` / `235` / `240` / `245` / `250` / `255` / `260` / `265` / `270` / `275` / `280` / `285` / `290` / `295` / `300` |
| Automatic Load Fast-Forward | `Désactivé` ✅ / Activé | `mame_autoloadfastforward` | `disabled` / `enabled` |

## ![](/emulators/external-links.png) External links

* **Progettosnaps**: [https://www.progettosnaps.net/](https://www.progettosnaps.net/)
* **Official website**: [https://github.com/libretro/mame/](https://github.com/libretro/mame)