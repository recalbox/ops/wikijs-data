---
title: El "Lightgun" de Recalbox
description: El "lightgun" para todos ! :)
published: true
date: 2023-08-19T14:37:48.230Z
tags: lightgun, 7.2+
editor: markdown
dateCreated: 2023-08-19T14:37:16.164Z
---

![](/basic-usage/features/lightgun/lightgun.png)

## ¿Cual es la idea?

La idea consiste en configurar automáticamente los juegos compatibles lightgun en los sistemas soportados por Recalbox. Por razones financieras y de disponibilidad del material, queremos jugar con una Wiimote (o dos Wiimotes en los juegos que se juegan a dobles). Tambien podemos utilizar el Nunchunk para desplazar el disparo primario y secundario en función de nuestros gustos. El modo de funcionamiento lightgun se activa automaticamente cuando el sitema detecta un dispositivo "Mayflash dolphin bar" en modo 2 y únicamente si el juego soporta este modo.

La idea del Lightgun de Recalbox es de permitiros jugar a los juegos de pistola, bazooka y otros juegos de puntería y precisión retros !

**Sistemas Recalbox que implementan esta funcionalidad:**

* 3DO
* Atomiswave
* Dreamcast
* Fbneo
* Mame
* Master System
* Megadrive
* Mega-CD
* Naomi
* Naomi GD ROM System
* NES
* Playstation 1
* Saturn
* SNES

>Los cores del emulador libretro están bien soportados actualmente, pero no existe ningún emulador 'standalone' por el momento, esperamos añadirlos algún día.
{.is-info}

>ATENCIÓN: para que la configuración automática funcione, los juegen deben haber sido escrapados con nombres bien precisos como `duck hunt (world) v1.1` y no `dhnes` !!! El sistema necesita poder encontrar el término "duck hunt" en el nombre.
{.is-warning}

## ¿Cuáles son los prerequisitos materiales?

![De uno a dos Nintendo Wiimotes (o copia compatible)](/basic-usage/features/lightgun/wiimote.png)

![Uno o dos Mayflash dolphin bar](/basic-usage/features/lightgun/dolphinbar.png)

>**Se necesita una barra Myaflash dolphin por cada Wiimote en mode lightgun (internamente la Wiimote se comporta como un ratón USB)**
{.is-warning}

![Opcional : Nunchuck para disparar con "C" & "Z"](/basic-usage/features/lightgun/nunchuck.png)

![](/basic-usage/features/lightgun/gun1.png)![](/basic-usage/features/lightgun/gun2.png)

![Opcional : una funda de verdadera pistola para estar mas confortable y parecer más realista](/basic-usage/features/lightgun/crossbow.png)

## ¿Cómo se instala?

### 1. Enchufad vuestra barra Mayflash dolphin a través de un puerto libre USB.

![](/basic-usage/features/lightgun/usbports.png)

### 2. Configurad la barra en modo 2

![Apretad el botón hasta que la luz azul se quede fija en la posición 2](/basic-usage/features/lightgun/dolphinbarmode2.png)

### 3. Sincronizad la Wiimote con la barra Mayflash dolphin

![Apretad el botón Sync para que la luz azul empiece a parpadear](/basic-usage/features/lightgun/dolphinbarsync.png)

![A continuación apretad el botón Sync de la Wiimote](/basic-usage/features/lightgun/wiimotesync.png)

>Si queréis jugar a 2, el procedimiento es el mismo asociando cada Wiimote su correspondiente barra Mayflash dolphin.
{.is-info}

## ¿Cómo activar el lightgun?

El modo lightgun se activa automáticamente cuando el sistema detecta una barra Mayflsh dolphin conectada y en modo 2, únicamente en los juegos que soportan este modo de funcionamiento.

>El modo 'Lightgun' se puede así activar/desactivar simplemente encendiendo/apagando la barra Mayflash dolphin pero sinceramente, es raro querer jugar a veces con la barra y a veces con el mando en un mismo juego.
{.is-info}

## Y finalmente... ¿cómo se juega?

### Los comandos predefinidos de la Wiimote

Start : ![](/basic-usage/features/lightgun/button_plus.png)
para arrancar un juego o ponerlo en pausa en aquellos que lo soportan

Select : ![](/basic-usage/features/lightgun/button_minus.png) para seleccionar un modo de juego o introducir créditos

Home : ![](/basic-usage/features/lightgun/button_home.png) para salir del juego y volver al menu de EmulationStation

Disparo principal : ![](/basic-usage/features/lightgun/button_z.png) en el Nunchuk

Disparo secundario :  ![](/basic-usage/features/lightgun/button_c.png) en el Nunchuk 

### Observaciones sobre los botones de la Wiimote

>* En algunos juegos se pueden configurar otras teclas como por ejemplo sobre la cruz direcional (aunque intentamos evitar estos casos)
>* La tecla ![](/basic-usage/features/lightgun/button_a.png) para los créditos.
>* Además algunos juegos se pueden lanzar con ![](/basic-usage/features/lightgun/button_a.png).
>* Otros juegos necesitan que se aprieten 2 teclas al mismo tiempo para entrar en el juego para salir/entrar de los menús.
>* Las teclas direccionales de la izquierda ![](/basic-usage/features/lightgun/stick.png) se comportan como la cruz direcional de la Wiimote.
>* La tecla "power" ![](/basic-usage/features/lightgun/button_power.png) puede servir para apagar la Wiimote mediante una pulsación larga.
>* Los siguientes botones no funcionan en modo lightgun: ![](/basic-usage/features/lightgun/button_1.png) y ![](/basic-usage/features/lightgun/button_2.png)
>* Si el Nunchuck está enchufado, los botones ![](/basic-usage/features/lightgun/button_a.png) y ![](/basic-usage/features/lightgun/button_b.png) se invierten sobre la Wiimote.
{.is-info}

## El sistema virtual Lightgun

Podéis activar un sistema virtual llamado "Lightgun" para reunir todos los juegos de vuestra biblioteca que está soportados automáticamente.
Para activarlo, [consultad esta página](./../../basic-usage/features/virtual-systems).

![DISFRUTAD !!!](/basic-usage/features/lightgun/duckhunt.png)
