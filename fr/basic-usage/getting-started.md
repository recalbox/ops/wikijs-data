---
title: Premières notions
description: 
published: true
date: 2023-07-06T20:12:48.264Z
tags: premières notions
editor: markdown
dateCreated: 2021-05-21T07:50:06.156Z
---

Nouvel utilisateur ou avec déjà certaines connaissances du fonctionnement de Recalbox, vous trouverez ici tout un tas d'informations concernant l'utilisation des premières choses.