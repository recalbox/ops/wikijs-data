---
title: Send commands to emulators with GPIO
description: 
published: true
date: 2021-08-06T14:50:14.895Z
tags: commands, emulators, gpio
editor: markdown
dateCreated: 2021-08-06T08:55:17.348Z
---

Network commands are used to control certain parts of RetroArch. To do this, commands are sent to RetroArch via the User Datagram Protocol (UDP).

Usually, in Recalbox, these commands are sent through the **Hotkey** function of the controller. For example, to reset the game, you press the `Hotkey + B` keys or to save the game, you press the `Hotkey + Y` keys.

>This only works with RetroArch / emulators that are part of Libretro.
{.is-info}

## Activation

But these commands can also be sent to RetroArch via a command line or GPIO pins. To do this, you need to enable the **network commands** option in the RetroArch menu or change the following line in `retroarch.cfg` :

```text
network_cmd_enable = true
```

## Send a command via GPIO

#### Application examples:

You are using an existing retro game console box (NES, N64, etc.) and you want to use the original reset button to reset the game you are currently playing and not to reset the entire hardware (Raspberry Pi, etc.). The following script shows how to send the **RESET** network command to RetroArch via GPIO pins 5 and 6 to reset the current game.

```python
import RPi.GPIO as GPIO
import time
import socket

# addressing information of target
IPADDR = "127.0.0.1"
PORTNUM = 55355

# enter the data content of the UDP packet
COMMAND = "RESET"

# initialize a socket, think of it as a cable
# SOCK_DGRAM specifies that this is UDP
try:
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
except socket.error:
    print 'Failed to create socket'
    sys.exit()

GPIO.setmode(GPIO.BCM)
GPIO.setup(3, GPIO.IN, pull_up_down=GPIO.PUD_UP)

def exitEmulator(channel):
    s.sendto(COMMAND, (IPADDR, PORTNUM))

GPIO.add_event_detect(3, GPIO.FALLING, callback=exitEmulator, bouncetime=500)
while True:
    time.sleep(10)
```

With this script, it is not only possible to **RESET** emulators, but you can also use the necessary command from various commands. You can **QUIT** the emulators (back to EmulationStation), **LOAD** or **SAVE** a game state, **PAUSE** the game, etc. To do this, simply replace the **RESET** command in the **COMMAND = RESET** with a command of your choice.

>This script may not work with all RetroArch network commands. For some it works, but for others it needs more work to make it work (for example, FASTFORWARD_HOLD).
{.is-warning}

You can also remap the pins of your Raspberry Pi.

And don't forget: Recalbox already has built-in options to reset, start and stop the hardware you are using [via buttons and GPIOs](./../../tutorials/others/add-on-off-button-to-your-recalbox).

## Commands

The following commands are supported by RetroArch:

* CHEAT_INDEX_MINUS
* CHEAT_INDEX_PLUS
* CHEAT_TOGGLE
* DISK_EJECT_TOGGLE
* DISK_NEXT
* DISK_PREV
* FAST_FORWARD
* FAST_FORWARD_HOLD
* FRAMEADVANCE
* FULLSCREEN_TOGGLE
* GRAB_MOUSE_TOGGLE
* LOAD_STATE
* MENU_TOGGLE
* MOVIE_RECORD_TOGGLE
* MUTE
* NETPLAY_FLIP
* OVERLAY_NEXT
* PAUSE_TOGGLE
* QUIT
* RESET
* REWIND
* SAVE_STATE
* SCREENSHOT
* SHADER_NEXT
* SHADER_PREV
* SLOWMOTION
* STATE_SLOT_MINUS
* STATE_SLOT_PLUS
* VOLUME_DOWN
* VOLUME_UP

## Send a command via the command line

You can also send network commands via the command line in Linux. Here's how to do it:

```shell
echo -n "QUIT" | nc -u -w1 127.0.0.1 55435
```

RetroArch is listening on port 55435 by default (the same port as for Netplay).