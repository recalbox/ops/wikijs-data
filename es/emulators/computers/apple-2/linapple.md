---
title: LinApple
description: 
published: true
date: 2024-07-16T19:14:56.631Z
tags: apple-2, linapple
editor: markdown
dateCreated: 2023-09-11T07:49:10.540Z
---

**LinApple** es un emulador Apple ]\[ (Apple 2) especialmente creado para los entornos Linux.

## ![](/emulators/license.svg) Licencia

El core posee la siguiente licencia [**GPLv2**](https://github.com/dabonetn/linapple-pie/blob/master/LICENSE).

## ![](/emulators/compatibility.png) Compatibilidad

 | RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | 
 | ✅ | ✅ | ✅ | ✅ | ✅ | ❌ | ❌ | ✅ |

## ![](/emulators/features.png) Funcionalidades



## ![](/emulators/bios.svg) BIOS

>**No necesita bios.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Extensiones de roms soportadas

Las roms deben tener las extensiones siguientes:

* .nib
* .do
* .po
* .dsk

### Emplazamiento

Disponed vuestras roms como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 apple2
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.dsk**

>Se aconsejan las roms del proyecto **TOSEC**.
{.is-success}

>Para más información sobre las roms, consultad [este tutorial](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

>Para conservar las configuraciones personalizadas cuando se produce una actualización del sistema Recalbox, os aconsejamos utilizar las [Sobrecargas de configuración](./../../../advanced-usage/configuration-override).
{.is-info}

## ![](/emulators/external-links.png) Enlaces externos

* **Código fuente utilizado** : [https://github.com/dabonetn/linapple-pie/](https://github.com/dabonetn/linapple-pie/)
* **Documentación** : [http://linapple.sourceforge.net/](http://linapple.sourceforge.net/)