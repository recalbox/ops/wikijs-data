---
title: Los controladores GPIO
description: 
published: true
date: 2024-08-25T14:48:05.218Z
tags: gpio, controladores
editor: markdown
dateCreated: 2023-09-18T20:13:57.648Z
---

Recalbox está compilado con el driver `mk_arcade_joystick_gpio` que permite gestionar un controlador conectado directamente en los pines GPIO del Raspberry Pi. Si tenéis previsto construiros una máquina recreativa o un joystick de arcade portátil, no necesitáis comprar un controlador USB. Seguid leyendo.

El driver puede gestionar hasta 2 controladores compuestos cada uno de un joystick de 4 direcionnes y 9 botones.

Si tenéis un RPi1 révision B, consultad el pinout `mk_arcade_joystick`.

>Los GPIO del Raspberry Pi 5 no están soportados actualmente. Hay que utilizar codificadores USB para poder enchufar los botones.
{.is-warning}


## Pinout

Tomemos como ejemplo una botonera de 7 botones con esta disposición:

```text
. ↑      Ⓨ Ⓧ Ⓛ  
←  →     Ⓑ Ⓐ Ⓡ Ⓗ
  ↓  
```

Con

```text
Ⓡ = Gâchette droite = TR  |  Ⓛ = Gâchette gauche = TL  |  Ⓗ = HK = Hotkey
```

En los RPI 1B+, RPI2 et RPi3, tenéis que conectar vuestros botonoes siguiendo este esquema:

![](/tutorials/controllers/gpio/gpio-controllers/mk_joystick_arcade_gpiosb+-hk.png)

>La parte inferior de la imagen corresponde al lado del Raspberry Pi donde se encuentran los puertos USB.
{.is-info}

Podéis conectar los botones directamente a tierra, ya que el driver activa los pullups internos del _**gpio**_.

## Configuración

* En el fichero [recalbox.conf](./../../../basic-usage/getting-started/recalboxconf-file), activad el driver GPIO ajustando `controllers.gpio.enabled` a 1: `controllers.gpio.enabled=1` y ya estáis listos para jugar!

Los GPIOs están configurados y listos para su uso en la interfaz de EmulationStation y en los distintos sistemas emulados.