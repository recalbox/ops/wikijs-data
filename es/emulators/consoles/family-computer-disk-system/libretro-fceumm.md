---
title: Libretro FCEUmm
description: 
published: true
date: 2024-07-29T13:41:48.881Z
tags: libretro, famicom, family computer disk system, fceumm
editor: markdown
dateCreated: 2023-09-06T09:56:31.665Z
---

## ![](/emulators/license.svg) Licencia

Este núcleo está bajo la licencia [**GPLv2**](https://github.com/libretro/libretro-fceumm/blob/master/Copying).

## ![](/emulators/compatibility.png) Compatibilidad

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Funcionalidades

| Funcionalidad | Soportado |
| :---: | :---: |
| Reinicio | ✔ |
| Capturas de pantalla | ✔ |
| Guardados | ✔ |
| Guardados instantáneos | ✔ |
| Rebobinado | ✔ |
| Netplay | ✔ |
| Opciones del núcleo | ✔ |
| RetroAchievements | ✔ |
| Cheats de RetroArch | ✔ |
| Cheats nativos | ✔ |
| Controles | ✔ |
| Remapeo | ✔ |
| Softpatching | ✔ |

## ![](/emulators/bios.svg) BIOS

### Lista de BIOS obligatorios

| Nombre de archivo | Descripción | MD5 | Proporcionado |
| :---: | :---: | :---: | :---: |
| disksys.rom | BIOS Family Computer Disk System | ca30b50f880eb660a320674ed365ef7a | ❌  |

### Ubicación

Coloque los BIOS de la siguiente manera:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 fds
┃ ┃ ┃ ┃ ┣ 🗒 **disksys.rom**

## ![](/emulators/roms.png) Roms

### Extensiones soportadas

Los roms deben tener las siguientes extensiones:

* .fds
* .zip
* .7z

Este sistema soporta roms comprimidos en formato .zip/.7z. Sin embargo, solo se trata de un archivo.

Los archivos contenidos en los .zip/.7z deben corresponder a las extensiones mencionadas anteriormente.
Además, cada archivo .zip/.7z no debe contener más de una rom comprimida.

### Ubicación

Coloque los roms de la siguiente manera:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 fds
┃ ┃ ┃ ┃ ┣ 🗒 **juego.zip**

>Se recomienda encarecidamente el uso de roms en formato **No-Intro**.
{.is-success}

>Para más información sobre los roms, visite [este tutorial](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

>Para poder conservar sus configuraciones personalizadas durante una actualización, le recomendamos utilizar nuestra funcionalidad [Sobrecargas de configuración](./../../../advanced-usage/configuration-overload).
{.is-info}

### Acceder a las opciones

Puede configurar diversas opciones de dos maneras diferentes.

* A través del menú RetroArch:

┣ 📁 Menú RetroArch
┃ ┣ 📁 Opciones del núcleo
┃ ┃ ┣ 🧩 Name_option

* A través del archivo `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Opciones del núcleo

| Opciones | Valor de las opciones | Variable (para sobrecarga) | Valores de las variables |
| --- | --- | --- | --- |
| Región | `Auto` ✅ / `NTSC` / `PAL` / `Dendy` | `fceumm_region` | `Auto` / `NTSC` / `PAL` / `Dendy` |
| Módulo Game Genie (Reinicio requerido) | `Desactivado` ✅ / `Activado` | `fceumm_game_genie` | `disabled` / `enabled` |
| Relación de aspecto | `Relación de aspecto de píxeles 8:7 (PAR)` ✅ / `4:3` / `Píxeles perfectos a escala` | `fceumm_aspect` | `8:7 PAR` / `4:3` / `PP` |
| Recorte de overscan horizontal izquierdo | `Desactivado` ✅ / `Activado` | `fceumm_overscan_h_left` | `disabled` / `enabled` |
| Recorte de overscan horizontal derecho | `Desactivado` ✅ / `Activado` | `fceumm_overscan_h_right` | `disabled` / `enabled` |
| Recorte de overscan vertical superior | `Desactivado` ✅ / `Activado` | `fceumm_overscan_v_top` | `disabled` / `enabled` |
| Recorte de overscan vertical inferior | `Desactivado` ✅ / `Activado` | `fceumm_overscan_v_bottom` | `disabled` / `enabled` |
| Paleta de colores | `Por defecto` ✅ / `Real por AspiringSquire` / `Consola virtual Nintendo` / `PPU RGB Nintendo` / `YUV-V3 de FBX` / `Final no saturado de FBX` / `Sony CXA2025AS US` / `PAL` / `Final 2 de BMF` / `Final 3 de BMF` / `Smooth de FBX` / `Composite directa de FBX` / `Estilo PVM D93 de FBX` / `Hardware NTSC de FBX` / `FBX's NES Classic (fixed)` / `NESCAP de RGBSource` / `Wavebeam de nakedarthur` / `FBX's Digital Prime` / `FBX's Magnum` / `FBX's Smooth V2` / `FBX's NES Classic` / `Datos en bruto` / `Personalizada` | `fceumm_palette` | `default` / `asqrealc` / `nintendo-vc` / `rgb` / `yuv-v3` / `unsaturated-final` / `sony-cxa2025as-us` / `pal` / `bmf-final2` / `bmf-final3` / `smooth-fbx` / `composite-direct-fbx` / `pvm-style-d93-fbx` / `ntsc-hardware-fbx` / `nes-classic-fbx-fs` / `nescap` / `wavebeam` / `dogotal-prime-fbx` / `magnum-fbx` / `smooth-v2-fbx` / `nes-classic-fbx` / `raw` / `custom` |
| Filtro NTSC | `Desactivado` ✅ / `Composite` / `S-Video` / `RGB` / `Monochrome` | `fceumm_ntsc_filter` | `disabled` / `composite` / `svideo` / `rgb` / `monochrome` |
| Calidad de audio | `Baja` ✅ / `Alta` / `Muy alta` | `fceumm_sndquality` | `Low` / `High` / `Very High` |
| Filtro RF de audio | `Desactivado` ✅ / `Activado` | `fceumm_sndlowpass` | `disabled` / `enabled` |
| Efecto de sonido estéreo | `Desactivado` ✅ / `1ms Delay` / `2ms Delay` / `3ms Delay` / `4ms Delay` / `5ms Delay` / `6ms Delay` / `7ms Delay` / `8ms Delay` / `9ms Delay` / `10ms Delay` / `11ms Delay` / `12ms Delay` / `13ms Delay` / `14ms Delay` / `15ms Delay` / `16ms Delay` / `17ms Delay` / `18ms Delay` / `19ms Delay` / `20ms Delay` / `21ms Delay` / `22ms Delay` / `23ms Delay` / `24ms Delay` / `25ms Delay` / `26ms Delay` / `27ms Delay` / `28ms Delay` / `29ms Delay` / `30ms Delay` / `31ms Delay` / `32ms Delay` | `fceumm_sndstereodelay` | `disabled` / `01_ms_delay` / `02_ms_delay` / `03_ms_delay` / `04_ms_delay` / `05_ms_delay` / `06_ms_delay` / `07_ms_delay` / `08_ms_delay` / `09_ms_delay` / `10_ms_delay` / `11_ms_delay` / `12_ms_delay` / `13_ms_delay` / `14_ms_delay` / `15_ms_delay` / `16_ms_delay` / `17_ms_delay` / `18_ms_delay` / `19_ms_delay` / `20_ms_delay` / `21_ms_delay` / `22_ms_delay` / `23_ms_delay` / `24_ms_delay` / `25_ms_delay` / `26_ms_delay` / `27_ms_delay` / `28_ms_delay` / `29_ms_delay` / `30_ms_delay` / `31_ms_delay` / `32_ms_delay` |
| Intercambiar ciclos de servicio | `Desactivado` ✅ / `Activado` | `fceumm_swapduty` | `disabled` / `enabled` |
| Volumen principal | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` ✅ / `8` / `9` / `10` | `fceumm_sndvolume` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` |
| Canal 1 (cuadrado 1) | `Activado` ✅ / `Desactivado` | `fceumm_apu_1` | `enabled` / `disabled` |
| Canal 2 (cuadrado 2) | `Activado` ✅ / `Desactivado` | `fceumm_apu_2` | `enabled` / `disabled` |
| Canal 3 (triángulo) | `Activado` ✅ / `Desactivado` | `fceumm_apu_3` | `enabled` / `disabled` |
| Canal 4 (ruido) | `Activado` ✅ / `Desactivado` | `fceumm_apu_4` | `enabled` / `disabled` |
| Canal 5 (PCM) | `Activado` ✅ / `Desactivado` | `fceumm_apu_5` | `enabled` / `disabled` |
| Activar turbo | `Ninguno` ✅ / `Jugador 1` / `Jugador 2` / `Ambos` | `fceumm_turbo_enable` | `None` / `Player 1` / `Player 2` / `Both` |
| Retraso del turbo (en cuadros) | `1` / `2` / `3` ✅ / `5` / `10` / `15` / `30` / `60` | `fceumm_turbo_delay` | `1` / `2` / `3` / `5` / `10` / `15` / `30` / `60` |
| Modo Zapper | `Crosshair light gun` ✅ / `Sequential Targets light gun` / `Pantalla táctil` / `Ratón` | `fceumm_zapper_mode` | `clightgun` / `stlightgun` / `touchscreen` / `mouse` |
| Mostrar mira del Zapper | `Activado` ✅ / `Desactivado` | `fceumm_show_crosshair` | `enabled` / `disabled` |
| Tolerancia del Zapper | `0` / `1` / `2` / `3` / `4` / `5` / `6` ✅ / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` | `fceumm_zapper_tolerance` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` |
| Invertir señal de disparo del Zapper | `Activado` ✅ / `Desactivado` | `fceumm_zapper_trigger` | `enabled` / `disabled` |
| Invertir señal del sensor del Zapper | `Activado` ✅ / `Desactivado` | `fceumm_zapper_sensor` | `enabled` / `disabled` |
| Modo Arkanoid | `Ratón absoluto` / `Ratón` ✅ / `Stelladaptor` / `Pantalla táctil` | `fceumm_arkanoid_mode` | `abs_mouse` / `mouse` / `stelladaptor` / `touchscreen` |
| Sensibilidad del ratón | `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ / `110` / `120` / `130` / `140` / `150` / `160` / `170` / `180` / `190` / `200` | `fceumm_mouse_sensitivity` | `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` / `110` / `120` / `130` / `140` / `150` / `160` / `170` / `180` / `190` / `200` |
| Permitir direcciones opuestas | `Desactivado` ✅ / `Activado` | `fceumm_up_down_allowed` | `disabled` / `enabled` |
| Sin límite de sprites | `Desactivado` ✅ / `Activado` | `fceumm_nospritelimit` | `disabled` / `enabled` |
| Overclock | `Desactivado` ✅ / `Post-renderizado x2` / `Refresco vertical x2` | `fceumm_overclocking` | `disabled` / `2x-Postrender` / `2x-VBlank` |
| Llenar RAM al encender (Reinicio requerido) | `$FF` ✅ / `$00` / `Aleatorio` | `fceumm_ramstate` | `fill $ff` / `fill $00` / `random` |

## ![](/emulators/external-links.png) Enlaces externos

* **Código fuente utilizado** : [https://github.com/libretro/libretro-fceumm/](https://github.com/libretro/libretro-fceumm/)
* **Documentación Libretro** : [https://docs.libretro.com/library/fceumm/](https://docs.libretro.com/library/fceumm/)
