---
title: FAQ Recalbox 8
description: 
published: true
date: 2023-08-31T13:29:27.212Z
tags: faq
editor: markdown
dateCreated: 2023-07-18T07:37:01.715Z
---

Dans cette FAQ, vous trouverez les problèmes connus et identifiés. Nous essayons de les corriger :)

## Problèmes connus

### Ecran noir sur RPi3 ou RPi4

Si vous avez un problème d'écran noir sur RPi4 ou RPi3, vous pouvez le résoudre en suivant ces étapes :
* Mettez votre carte SD dans votre PC
* Dans la partition `RECALBOX`, ouvrez le fichier `cmdline.txt`.
* Ajoutez ce texte à la fin de la ligne : `video=HDMI-A-1:D video=HDMI-A-2:D`.
* Le fichier doit ressembler à ceci :
```
dwc_otg.fiq_fix_enable=1 sdhci-bcm2708.sync_after_dma=0 dwc_otg.lpm_enable=0 console=tty3 consoleblank=0 loglevel=3 elevator=deadline vt.global_cursor_default=0 logo.nologo splash label=RECALBOX rootwait fastboot noswap video=HDMI-A-1:D video=HDMI-A-2:D
```
* Sauvegardez le fichier
* Ejectez correctement la carte SD
* Testez à nouveau dans le Raspberry Pi.


### RPI3

| Lieu de problème | Description du problème | Résolu ? |
| --- | --- | --- |
| Général | Pas d'affichage sur certains écrans HDMI et DSI | ✅ |

### RPI4

| Lieu de problème | Description du problème | Résolu ? |
| --- | --- | --- |
| Kodi | Message d'erreur au démarrage | ✅ |
| Kodi | Manette pas reconnue, obligé de remapper dans les options de Kodi | ✅ |
| Kodi | Difficulté de lecture de contenu Netflix | ❌ |

### PC

| Lieu de problème | Description du problème | Résolu ? |
| --- | --- | --- |
| Kodi | Message d'erreur au démarrage | ✅ |
| Kodi | Manette pas reconnue, obligé de remapper dans les options de Kodi | ✅ |
| PCSX2 | Problèmes identifiés sur DPAD avec rotation 1/4 de tour (gauche pour bas, etc.) | ✅ |
| Dolphin | Impossible de sortir d'un jeu GameCube avec une manette (obligé d'utiliser un clavier ALT + F4) | ❌ |

### Odroid GO Advanced/Super briquée

La première version de l'image 8.0.1 était corrompue. Une toute nouvelle image est maintenant disponible.
Pour ceux qui ont une Odroid GO Advanced ou Super blockée sur le logo gris de Recalbox, voici quelques étapes pour débriquer l'appareil :
1. téléchargez ce fichier http://upgrade.recalbox.com/latest/odroidgo2/boot.tar.xz,
2. utilisez votre décompresseur préféré, celui-ci doit supporter le fomat XZ et TAR (Easy 7 zip semble pouvoir le faire). Ouvrez le fichier boot.tar.xz,
3. insérer la carte SD de l'Odroid dans votre PC,
4. copier les 3 fichiers du répertoire boot de l'archive (linux, recalbox et uInitrd) sur la carte SD, dans le répertoire boot aussi. Cela écrasera les fichiers corrompus de la carte SD,
5. éjecter la carte SD et l'insérer dans l'Odroid
