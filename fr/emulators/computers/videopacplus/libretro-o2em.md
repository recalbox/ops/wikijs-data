---
title: Libretro O2EM
description: 
published: true
date: 2024-06-22T22:42:56.957Z
tags: libretro, videopac+, o2em
editor: markdown
dateCreated: 2021-05-21T08:18:32.352Z
---

Libretro O2EM est un émulateur multi-plateforme open source Odyssey2 / Videopac +.

## ![](/emulators/license.svg) Licence

Ce core est sous licence [**artistique**](https://sourceforge.net/projects/o2em/).

## ![](/emulators/compatibility.png) Compatibilité

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Fonctionnalités

| Fonctionnalité | Supporté |
| :---: | :---: |
| Redémarrage | ✔ |
| Sauvegardes instantanées | ✔ |
| Rembobinage | ✔ |
| Options du core | ✔ |
| RetroAchievements | ✔ |
| Controls | ✔ |
| Remapping | ✔ |

## ![](/emulators/bios.svg) BIOS

### Liste des bios obligatoires

| Nom de fichier | Description | MD5 | Fourni |
| :---: | :---: | :---: | :---: |
| g7400.bin | BIOS Videopac+ Européen - modèle G7400 - Requis | c500ff71236068e0dc0d0603d265ae76 | ❌ |
| jopac.bin | BIOS Videopac+ Français - modèle G7400 - Requis | 279008e4a0db2dc5f1c048853b033828 | ❌ |

### Emplacement

Placez les bios comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 o2em
┃ ┃ ┃ ┃ ┣ 🗒 **g7400.bin**
┃ ┃ ┃ ┃ ┣ 🗒 **jopac.bin**

## ![](/emulators/roms.png) Roms

### Extensions supportées

Les roms doivent avoir les extensions suivantes :

* .bin
* .zip
* .7z

Ce système supporte les roms compressées au format .zip/.7z. Attention toutefois, il ne s'agit que d'une archive.

Les fichiers contenus dans les .zip/.7z doivent correspondre aux extensions citées précédemment.
De plus, chaque fichier .zip/.7z ne doit contenir qu'une seule rom compressée.

### Emplacement

Placez les roms comme ceci : 

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 videopacplus
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.zip**

>Les roms au format **No-Intro** sont vivement conseillées.
{.is-success}

>Pour plus d'information sur les roms, rendez-vous sur [ce tutoriel](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuration avancée de l'émulateur

>Pour pouvoir conserver vos configurations personnalisées lors d'une mise à jour, nous vous conseillons d'utiliser notre fonctionnalité [Surcharges de configuration](./../../../advanced-usage/configuration-override).
{.is-info}

### Accéder aux options

Vous pouvez configurer diverses options de deux façons différentes.

* Via le menu RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via le fichier `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Options du core

| Options | Valeurs des options | Variable (pour surcharge) | Valeurs des variables |
| --- | --- | --- | --- |
| Emulated Hardware (Restart) | `Odyssey 2 (NTSC)` ✅ / `Videopac G7000 (European)` / `Videopac+ G7400 (European)` / `Videopac+ G7400 (French)` | `o2em_bios` | `o2rom.bin` / `c52.bin` / `g7400` / `jopac.bin` |
| Console Region (Restart) | `Auto` ✅ / `NTSC` / `PAL` | `o2em_region` | `auto` / `NTSC` / `PAL` |
| Swap Gamepads | `Désactivé` ✅ / `Activé` | `o2em_swap_gamepads` | `disabled` / `enabled` |
| Virtual KBD Transparency | `0%` ✅ / `25%` / `50%` / `75%` | `o2em_vkbd_transparency` | `0` / `25` / `50` / `75` |
| Crop Overscan | `Désactivé` ✅ / `Activé` | `o2em_crop_overscan` | `disabled` / `enabled` |
| Interframe Blending | `Désactivé` ✅ / `Simple` / `Ghosting (65%)` / `Ghosting (75%)` / `Ghosting (85%)` / `Ghosting (95%)` | `o2em_mix_frames` | `disabled` / `mix` / `ghost_65` / `ghost_75` / `ghost_85` / `ghost_95` |
| Audio Volume | `0%` / `10%` / `20%` / `30%` / `40%` / `50%` ✅ / `60%` / `70%` / `80%` / `90%` / `100%` | `o2em_audio_volume` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
| Voice Volume | `0%` / `5%` / `10%` / `15%` / `20%` / `25%` / `30%` / `35%` / `40%` / `45%` / `50%` / `55%` / `60%` / `65%` / `70%` ✅ / `75%` / `80%` / `85%` / `90%` / `95%` / `100%` | `o2em_voice_volume` | `0` / `5` / `10` / `15` / `20` / `25` / `30` / `35` / `40` / `45` / `50` / `55` / `60` / `65` / `70` / `75` / `80` / `85` / `90` / `95` / `100` |
| Audio Filter | `Désactivé` ✅ / `Activé` | `o2em_low_pass_filter` | `disabled` / `enabled` |
| Audio Filter Level | `5%` / `10%` / `15%` / `20%` / `25%` / `30%` / `35%` / `40%` / `45%` / `50%` / `55%` / `60%` ✅ / `65%` / `70%` / `75%` / `80%` / `85%` / `90%` / `95%` | `o2em_low_pass_range` | `5` / `10` / `15` / `20` / `25` / `30` / `35` / `40` / `45` / `50` / `55` / `60` / `65` / `70` / `75` / `80` / `85` / `90` / `95` |

## ![](/emulators/external-links.png) Liens externes

* **Code source utilisé** : [https://github.com/libretro/libretro-o2em/](https://github.com/libretro/libretro-o2em/)
* **Documentation Libretro** : [https://docs.libretro.com/library/o2em/](https://docs.libretro.com/library/o2em/)