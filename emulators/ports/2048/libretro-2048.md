---
title: Libretro 2048
description: 
published: true
date: 2024-07-25T08:11:06.888Z
tags: libretro, 2048
editor: markdown
dateCreated: 2021-06-29T19:53:51.959Z
---

Libretro 2048 is a portage of the game 2048.

## ![](/emulators/license.svg) License

This core est under [**public domain**](https://github.com/libretro/libretro-2048/blob/master/COPYING) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Features

| Feature | Supported |
| :---: | :---: |
| Restart | ✔ |
| Screenshots | ✔ |
| Saves | ✔ |
| States | ✔ |
| Rewind | ✔ |
| Netplay | ✔ |
| Controls | ✔ |
| Remapping | ✔ |

## ![](/emulators/bios.svg) BIOS

>**No bios is required.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Supported extensions

Roms must have the extension:

* .game

### Location

Put roms like this: 

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 ports
┃ ┃ ┃ ┃ ┣ 📁 2048
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **2048.game**

This rom is included in your Recalbox.

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>**This core have no option.**
{.is-success}

## ![](/emulators/external-links.png) External links

* **Source code used** : [https://github.com/libretro/libretro-2048/](https://github.com/libretro/libretro-2048)
* **Official source code** : [https://github.com/gabrielecirulli/2048/](https://github.com/gabrielecirulli/2048)
* **Official website** : [https://play2048.co/](https://play2048.co/)