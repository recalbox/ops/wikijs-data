---
title: Configuración de controladores Xin-mo
description: 
published: true
date: 2023-09-18T11:52:52.780Z
tags: xin-mo, xinmo, configuración, controladores, mandos
editor: markdown
dateCreated: 2023-09-18T11:52:52.780Z
---

## Xin-mo

Las intefaces USB Xin Mo funcionan nativamente con Recalbox y pueden gestionar hasta 2 jugadores.

## Pinout

Tomemos como ejemplo un panel con 6 botones + stick + 1 botón de crédito (`SELECT`) + 1 botón `START` (generalmente botones de 1 jugador y 2 jugadores) con esta disposición para cada jugador:

```text
. ↑     Ⓨ Ⓧ Ⓛ  
←  →	 Ⓑ Ⓐ Ⓡ    ($) (1P)
  ↓
```

Es preferible añadir un botón hotkey dedicado: Ⓗ

Podéis mapear los dos jugadores de la misma forma, pero no es obligatorio.

Ejemplo con los antiguos modelos que gestionan menos botones:

![Branchement ancien du Xin-Mo](/tutorials/controllers/usb-encoders/xin-mo/xinmo_arcade_recalbox.jpg)

Con los modelos más recientes, las conexiones son las siguientes:

![Branchement nouveau du Xin-Mo](/tutorials/controllers/usb-encoders/xin-mo/xinmo_arcade_new_recalbox.jpg){.full-width}

## Configuración

## {.tabset}

### Modelos antíguos

Con los antiguos modelos basta con realizar sólo la configuración del jugador 1 directamente desde EmulationStation. La configuración del jugador 2 debería funcionar si queréis utilizar el mismo mapping.

>No intentéis configurar el jugador 2 desde EmulationStation porque sobreescribirá la configuración del jugador 1.
>
>Los joysticks tienen que estar configurados sobre el D-PAD y no como joystick dentro del menú de EmulationStation.
{.is-warning}

>La configuración como joysticks está reservada a los sticks analógicos como los que presentan los mandos de PlayStation o Xbox.
{.is-info}

### Modelos recientes

Con los modelos más recientes, hay que mapear los jugadores 1 y 2 como para las interfaces DragonRise.