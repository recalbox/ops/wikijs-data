---
title: Libretro Mednafen_WSwan
description: 
published: true
date: 2024-06-22T11:23:02.980Z
tags: libretro, mednafen, wswan, wonderswan
editor: markdown
dateCreated: 2021-05-21T08:32:25.432Z
---

**Libretro beetle-wswan** est un émulateur pour les consoles de jeux portables Wonderswan, Wonderswan Color et Pocket Challenge V2 développé en C et C++.

Il est un port autonome de Mednafen WonderSwan vers Libretro, lui-même un fork de Cygne.

## ![](/emulators/license.svg) Licence

Ce core est sous licence [**GPLv2**](https://github.com/libretro/beetle-wswan-libretro/blob/master/COPYING).

## ![](/emulators/compatibility.png) Compatibilité

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Fonctionnalités

| Fonctionnalité | Supporté |
| :---: | :---: |
| Redémarrage | ✔ |
| Captures d'écran | ✔ |
| Sauvegardes | ✔ |
| Sauvegardes instantanées | ✔ |
| Rembobinage | ✔ |
| Netplay | ✔ (pas d'émulation du câble link) |
| RetroAchievements | ✔ |
| Controls | ✔ |
| Remapping | ✔ |
| Softpatching | ✔ |

## ![](/emulators/bios.svg) BIOS

>**Aucun bios n'est requis.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Extensions supportées

La rom doit avoir l'extension :

* .ws
* .zip
* .7z

Ce système supporte les roms compressées au format .zip/.7z. Attention toutefois, il ne s'agit que d'une archive.

Les fichiers contenus dans les .zip/.7z doivent correspondre aux extensions citées précédemment.
De plus, chaque fichier .zip/.7z ne doit contenir qu'une seule rom compressée.

### Emplacement

Placez les roms comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 wswan
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.zip**

>Les roms au format **No-Intro** sont vivement conseillées.
{.is-success}

>Pour plus d'information sur les roms, rendez-vous sur [ce tutoriel](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuration avancée de l'émulateur

>Pour pouvoir conserver vos configurations personnalisées lors d'une mise à jour, nous vous conseillons d'utiliser notre fonctionnalité [Surcharges de configuration](./../../../advanced-usage/configuration-override).
{.is-info}

### Accéder aux options

Vous pouvez configurer diverses options de deux façons différentes.

* Via le menu RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via le fichier `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Options du core

| Options | Valeur des options | Variable (pour surcharge) | Valeurs des variables |
| --- | --- | --- | --- |
| Rotation d'écran | `Manuelle` ✅ / `Paysage` / `Portrait` | `wswan_rotate_display` | `manual` / `landscape` / `portrait` |
| Faire pivoter le mappage des touches | `Automatiquement` ✅ / `Paysage` / `Portrait` | `wswan_rotate_keymap` | `auto` / `disabled` / `enabled` |
| Palette de couleurs | `Niveaux de gris` ✅ / `WS - WonderSwan` / `WS - WonderSwan Color` / `WS - SwanCrystal` / `DMG Game Boy` / `Game Boy Pocket` / `Game Boy Light` / `Rose Belle` / `Bleu Bulle` / `Vert Rebelle` / `Digivice` / `Game.com` / `GameKing` / `Game Master` / `Doré Wild` / `Niveaux de vert` / `Orange Hokage` / `Faon Labo` / `Super Saiyan Légendaire` / `Microvision` / `Or Million Live` / `Or Odyssey` / `Bleu Shiny Sky` / `Bleu Slime` / `TI-83` / `Bois des voyages` / `Virtual Boy` | `wswan_mono_palette` | `default` / `wonderswan` / `wondeswan_color` / `swancrystal` / `gb_dmg` / `gb_pocket` / `gb_light` / `blossom_pink` / `bubbles_blue` / `buttercup_green` / `digivice` / `game_com` / `gameking` / `game_master` / `golden_wild` / `greenscale` / `hokage_orange` / `labo_fawn` / `legendary_super_saiyan` / `microvision` / `million_live_gold` / `odyssey_gold` / `shiny_sky_blue` / `slime_blue` / `ti_83` / `travel_wood` / `virtual_boy` |
| Profondeur des couleurs (Redémarrage requis) | `Milliers (16 bits)` ✅ / `Millions (24 bits)` | `wswan_gfx_colors` | `16bit` / `24bit` |
| Saut d'images | `Désactivé` ✅ / `Automatiquement` / `Manuelle` | `wswan_frameskip` | `disabled` / `auto` / `manual` |
| Seuil de saut d'images (%) | `15` / `18` / `21` / `24` / `27` / `30` / `33` ✅ / `36` / `39` / `42` / `45` / `48` / `51` / `54` / `57` / `60` | `wswan_frameskip_threshold` | `15` / `18` / `21` / `24` / `27` / `30` / `33` / `36` / `39` / `42` / `45` / `48` / `51` / `54` / `57` / `60` |
| Mode 60 Hz | `Désactivé` / `Activé` ✅ | `wswan_60hz_mode` | `disabled` / `enabled` |
| Sound Output Sample Rate | `11025` / `22050` / `44100` ✅ / `48000` | `wswan_sound_sample_rate` | `11025` / `22050` / `44100` / `48000` |
| Filtre audio | `Désactivé` ✅ / `Activé` | `wswan_sound_low_pass` | `disabled` / `enabled` |

## ![](/emulators/external-links.png) Liens externes

* **Code source utilisé** : [https://github.com/libretro/beetle-wswan-libretro/](https://github.com/libretro/beetle-wswan-libretro/)
* **Documentation Libretro** : [https://docs.libretro.com/library/beetle_cygne/](https://docs.libretro.com/library/beetle_cygne/)
* **Code source officiel** : [https://mednafen.github.io/releases/](https://mednafen.github.io/releases/)
* **Site officiel** : [https://mednafen.github.io/](https://mednafen.github.io/)
* **Forum officiel** : [https://forum.fobby.net/](https://forum.fobby.net/)
* **Documentation officielle** : [https://mednafen.github.io/documentation/wswan.html](https://mednafen.github.io/documentation/wswan.html)