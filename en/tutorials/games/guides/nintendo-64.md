---
title: Nintendo 64
description: 
published: true
date: 2021-08-08T13:48:03.187Z
tags: n64, nintendo, 64
editor: markdown
dateCreated: 2021-08-07T16:19:05.890Z
---

With the tutorials available here, you will discover how to put the sound on the headphone output in case of refusal of the system or what to do if the game does not launch.

Here are the available tutorials:

[Back to EmulationStation with mupen64plus](return-to-emulationstation-with-mupen64plus)
[High-resolution textures](high-resolution-textures)
[Multiple high resolution texture packs on the same game](load-differents-high-resolution-texture-packs-on-same-game)
[No sound on jack with mupen64plus](no-sound-on-jack-with-mupen64plus)
[Roms / Cores Compatibility](game-core-compatibility)