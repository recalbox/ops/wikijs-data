---
title: Carcasas
description: 
published: true
date: 2023-09-19T08:16:12.405Z
tags: compatibilidad, carcasa
editor: markdown
dateCreated: 2023-09-19T08:15:33.487Z
---

## PI4
 
### PIStation
☑️ Soportado en recalbox
 
![boitier-pistation-retroflag-et-ecran-lcd_(1).jpg](/compatibility/cases/boitier-pistation-retroflag-et-ecran-lcd_(1).jpg)
 
[Enlace para comprar](https://www.kubii.fr/boitiers/3031-boitier-nespi-case-pour-raspberry-pi-4-3272496302303.html)
 
### NESPi 4 CASE
☑️ Autodetectado por Recalbox
 
![For Pi4](/compatibility/cases/nespi4case.png)
 
[Enlace para comprar](https://www.kubii.fr/boitiers/3031-boitier-nespi-case-pour-raspberry-pi-4-3272496302303.html)
 
### Argon One
☑️ Soportado en recalbox
 
![For Pi4](/compatibility/cases/argonone.png)
 
[Enlace para comprar](https://www.kubii.fr/boitiers-et-supports/2762-boitier-argon1-pour-raspberry-pi-4-3272496299672.html)

### PiBoy DMG
☑️ Autodetectado por Recalbox
 
![piboy-dmg-2.jpg](/compatibility/cases/piboy-dmg-2.jpg)
 
[Enlace para comprar](https://www.kubii.fr/consoles-systemes/3046-piboy-dmg-725272730706.html)
 
### GPi CASE 2 (CM4)
☑️ Autodetectado por Recalbox

> No recomendamos usar un CM4 con memoria flash (eMMC) porque, debido al diseño de la Raspberry Foundation, la eMMC reemplazará la tarjeta microSD del GPI Case 2, haciendo que la tarjeta sea inutilizable.
Como resultado, sólo podréis acceder a la eMMC para instalar Recalbox.
{.is-warning}
 
![gpi-case-2-dock.jpg](/compatibility/cases/gpi-case-2-dock.jpg)
 
[Enlace para comprar](https://www.kubii.fr/boitiers-console/3554-gpi-case-2-dock-6971727910221.html)

### Raspberry Pi Touch Display
☑️ Soportado en recalbox

> Hay que enchufar una pantalla HDMI para poder proceder a la activación del Touch Display.
{.is-info}

![rpi-touch-display.png](/compatibility/cases/rpi-touch-display.png)

[Enlace para comprar](https://www.kubii.fr/ecrans-afficheurs/1131-ecran-tactile-officiel-7-800x480-kubii-5056070923850.html)

## Pi ZERO/Pi ZERO W

### GPi CASE
☑️ Autodetectado por Recalbox

![Pour Zero et Zero W](/compatibility/cases/gpicase.png)

[Enlace para comprar](https://www.kubii.fr/consoles-retro-gaming/2719-gpi-case-retroflag-kubii-3272496299276.html)

## PI2/PI3/PI3B/PI3B+

### NESPi CASE
☑️ Soportado en recalbox

![](/compatibility/cases/nespicase.png)

[Enlace para comprar](https://www.reichelt.com/fr/en/case-for-raspberry-pi-3-nes-design-gray-rpi-nespi-case-p211465.html?&trstct=pos_0&nbc=1)

### NESPi CASE PLUS
☑️ Soportado en recalbox

![Pour 2, 3, 3B et 3B +](/compatibility/cases/nespicaseplus.png)

[Enlace para comprar](https://www.kubii.fr/boitiers-et-supports/2036-boitier-nespi-case-pour-raspberry-pi-3-2-b-kubii-327249600861.html)

### SUPERPI CASE J
☑️ Soportado en recalbox

![Pour 2B, 3B et 3B+](/compatibility/cases/superpicase.png)

[Enlace para comprar](https://www.kubii.fr/boitiers-et-supports/2283-superpicase-j-kubii-3272496011922.html)

### SUPERPI CASE U
☑️ Soportado en recalbox

![Pour 2, 3 et 3B+](/compatibility/cases/superpicaseusa.png)

[Enlace para comprar](https://www.amazon.com/dp/B07G34TTKL?m=A3I7DCARNWUK2P&ref_=v_sp_detail_page&th=1)

### SUPERPI CASE U (New)
☑️ Soportado en recalbox

![Pour 2, 3 et 3B+](/compatibility/cases/superpicaseusanew.png)

[Enlace para comprar](https://www.amazon.com/dp/B07W5L95KK?ref=myi_title_dp)

### MEGAPi CASE M
☑️ Soportado en recalbox

![Pour 2B, 3B et 3B+](/compatibility/cases/megapicase.png)

[Enlace para comprar](https://www.kubii.fr/consoles-retro-gaming/2337-boitier-megapi-case-kubii-3272496012646.html)

### Raspberry Pi Touch Display
☑️ Soportado en recalbox

> Hay que enchufar una pantalla HDMI para poder proceder a la activación del Touch Display.
{.is-info}

![rpi-touch-display.png](/compatibility/cases/rpi-touch-display.png)

[Enlace para comprar](https://www.kubii.fr/ecrans-afficheurs/1131-ecran-tactile-officiel-7-800x480-kubii-5056070923850.html)

### Thingivers 

Diferentes carcasas para imprimir con una impresora 3D.

[Enlace para comprar](https://www.thingiverse.com/tag:raspberry_pi_4)

## Boitiers fonctionnels pas cher Pi4

### GeeekPi

![Pour 4](/compatibility/cases/geekpi.png)

[Enlace para comprar](https://www.amazon.fr/dp/B07XXQ34PZ/ref=cm_sw_r_cp_apa_i_MdpqFb2V1Q6F6)

### GeeekPi

![Pour 4](/compatibility/cases/geekpi2.png)

[Enlace para comprar](https://www.amazon.fr/GeeekPi-Raspberry-Ventilateur-40X40X10mm-Dissipateurs/dp/B07XCKNM8J/ref=sr_1_3?__mk_fr_FR=%C3%85M%C3%85%C5%BD%C3%95%C3%91&dchild=1&keywords=boitier+pi+4&qid=1598103872&sr=8-3)

### Bruphny

![Pour 4](/compatibility/cases/bruphny.png)

[Enlace para comprar](https://www.amazon.fr/Bruphny-Ventilateur-Alimentation-Dissipateurs-Adaptateur/dp/B07WN3CHGH/ref=sr_1_5?__mk_fr_FR=%C3%85M%C3%85%C5%BD%C3%95%C3%91&dchild=1&keywords=boitier+pi+4&qid=1598103952&sr=8-5)

## Odroid XU4

### Boîtier Gaming

![Pour XU4](/compatibility/cases/ogst.png)

[Enlace para comprar](https://www.kubii.fr/odroid/2214-boitier-gaming-pour-odroid-xu4-kubii-3272496011250.html)