---
title: Atari Lynx
description: 
published: true
date: 2024-07-26T08:31:50.802Z
tags: atari, lynx, portátiles
editor: markdown
dateCreated: 2023-09-04T07:59:10.905Z
---

![](/emulators/handheld/lynx.svg){.align-center}

## Detalles técnicos

* **Fabricante**: Epyx / Atari
* **Año de lanzamiento**: 1989
* **Cantidad vendida**: 1 millón
* **Procesador**: 8 bits con un espacio de direcciones de 16 bits
* **Coprocesadores**: Mikey y Suzy 2 x 16 bits
* **RAM**: 64K 120 ns DRAM
* **ROM**: 512 bytes
* **Resolución**: 160 x 102 píxeles
* **Sonido**: 4 canales, DAC de 8 bits

## Presentación

La **Atari Lynx** fue la única consola portátil de Atari y la primera con pantalla LCD en color. Salió a la venta en 1989.

La máquina fue desarrollada por Epyx bajo el nombre de "Handy2" (hoy en día el nombre del emulador de PC más avanzado de esta consola). En una fase muy avanzada de desarrollo, la empresa buscó inversores para su proyecto, e inicialmente eligió a Nintendo, que se negó a invertir en el proyecto, lo que llevó a Epyx a recurrir a Atari, que compró los derechos en 1988.

La empresa modificó el altavoz interno y retiró el joystick del pad. Atari comercializó la consola dos años después. La pantalla podía invertirse para jugar con la mano izquierda y hasta 8 jugadores podían conectarse en red. Sus capacidades técnicas eran muy superiores a las de la Game Boy (color y 3D). La Lynx, en cambio, era más imponente, consumía demasiadas pilas y carecía de una serie de títulos prometedores.

En 1991, Atari lanzó una segunda versión de su consola con una nueva forma y cartuchos rediseñados. La nueva consola (llamada 'Lynx II' por Atari) tenía empuñaduras, una pantalla de mejor calidad y una opción de ahorro de energía que ponía la consola en modo de espera.

## Emuladores

[Libretro Handy](libretro-handy)
[Libretro Mednafen_Lynx](libretro-mednafen_lynx)