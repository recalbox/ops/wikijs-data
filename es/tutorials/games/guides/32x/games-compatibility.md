---
title: Compatibilidad de juegos
description: 
published: true
date: 2023-09-19T12:29:29.706Z
tags: juegos, compatibilidad 32x
editor: markdown
dateCreated: 2023-09-19T12:29:29.706Z
---

## Introducción

Algunos juegos de la 32X pueden presentar problemas de emulación. Algunos de ellos se listan a continuación.

## Cuadro de compatibilidades

| Juego | Problemas conocidos |
| :--- | :--- |
| Brutal Unleashed - Above the Claw | Softlock después del primer combate. |
| FIFA Soccer '96. |Texto del menú principal con diapositivas. Knuckles' Chaotix. |
| Knuckles' Chaotix | gráficos defectuosos en la pantalla de selección de jugador. |
| NBA Jam Tournament Edition | Problemas de framerate. |
| NFL Quarterback Club| Faltan algunos gráficos del menú. |
| Virtua Racing Deluxe | línea intermitente al mostrar el logotipo de SEGA. |
| World Series Baseball Starring Deion Sanders | Planta al empezar un partido. |
| WWF Raw | Faltan algunos gráficos. |