---
title: Cheats
description: 
published: true
date: 2024-09-18T12:43:45.527Z
tags: mame, cheats
editor: markdown
dateCreated: 2024-09-18T12:43:45.527Z
---

## Introduction

With MAME, it is possible to add cheat codes.

>It's best to use a controller with separate `Select` and `Hotkey` buttons if you want to keep the use of save states.
{.is-info}

## Usage

* Download the archive of [cheat codes](https://www.mamecheat.co.uk/mame_downloads.htm) corresponding to the integrated version of MAME in Recalbox.

>If you can't find the current version of MAME, download the first previous version and it will work at 100%.
{.is-info}

* Unzip the archive. You will get 3 files: the only file we are interested in is `cheat.7z`.
* Place the `cheat.7z` file in the `/recalbox/share/bios/mame` directory **without uncompressing it!** and run your game.

To access the cheat codes during the game:

* Go to the RetroArch menu by pressing `Hotkey + B`.
* Go to `Core Options` > `System`.

![](/tutorials/games/guides/mame/cheatsen1.png){.full-width}

* At the top of the core options screen, activate `Cheats` and exit the game.

![](/tutorials/games/guides/mame/cheatsen2.png){.full-width}

* Restart your game and go to the MAME menu by pressing `Select` + `Y` and go to `Cheat`.

![](/tutorials/games/guides/mame/cheatsen3.png){.full-width}

* You'll see the list of cheats for your game. All you have to do is select the cheats you want to use.

![](/tutorials/games/guides/mame/cheatsen4.png){.full-width}