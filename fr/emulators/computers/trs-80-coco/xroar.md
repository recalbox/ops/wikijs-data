---
title: XRoar
description: 
published: true
date: 2024-06-23T11:51:03.952Z
tags: color, trs-80, computer, xroar, trs-80-coco, 8.0+
editor: markdown
dateCreated: 2021-08-22T17:31:08.819Z
---



## ![](/emulators/license.svg) Licence

Ce core est sous licence [**GPL-3.0**](https://gitlab.com/recalbox/packages/standalone/xroar/-/blob/master/COPYING.GPL) et [**LGPL-3.0**](https://gitlab.com/recalbox/packages/standalone/xroar/-/blob/master/COPYING.LGPL).

## ![](/emulators/compatibility.png) Compatibilité

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Fonctionnalités

| Fonctionnalité | Supporté |
| :---: | :---: |

## ![](/emulators/bios.svg) BIOS

### Liste des bios obligatoires

| Nom de fichier | Description | MD5 | Fourni |
| :---: | :---: | :---: | :---: |
| bas11.rom | CoCo 1 BASIC | c73fb4bff9621c5ab17f6220b20db82f | ❌ |
| bas13.rom | CoCo 2 BASIC | c2fc43556eb6b7b25bdf5955bd9df825 | ❌ |
| extbas10.rom | CoCo 1 Extended BASIC | fda72f415afe99b36f953bb9bc1253da | ❌ |
| extbas11.rom | CoCo 2 Extended BASIC | 21070aa0496142b886c562bf76d7c113 | ❌ |

### Emplacement

Placez les bios comme ceci : 

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 trs80coco
┃ ┃ ┃ ┃ ┣ 🗒  **bas11.rom**
┃ ┃ ┃ ┃ ┣ 🗒  **bas13.rom**
┃ ┃ ┃ ┃ ┣ 🗒  **extbas10.rom**
┃ ┃ ┃ ┃ ┣ 🗒  **extbas11.rom**

### Liste des bios optionnels

| Nom de fichier | Description | MD5 | Fourni |
| :---: | :---: | :---: | :---: |
| bas10.rom | CoCo 1 BASIC | a74f3d95b395dad7cdca19d560eeea74 | ❌ |
| bas12.rom | CoCo 2 BASIC | c933316c7d939532a13648850c1c2aa6 | ❌ |
| disk10.rom | RSDos 1.0 | a64b3ef9efcc066b18d35b134068d1cc | ❌ |
| disk11.rom | RSDos 1.1 | 8cab28f4b7311b8df63c07bb3b59bfd5 | ❌ |
| hdbdw3bck.rom | RSDos Becker | ef750d93d24d7bc87a8ecd6e5a15a845 | ❌ |
| mx1600bas.rom | MX1600 BASIC | 88d1504e93366f498105a846cdbf7fb7 | ❌ |

### Emplacement

Placez les bios comme ceci : 

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 trs80coco
┃ ┃ ┃ ┃ ┣ 🗒  **bas10.rom**
┃ ┃ ┃ ┃ ┣ 🗒  **bas12.rom**
┃ ┃ ┃ ┃ ┣ 🗒  **disk10.rom**
┃ ┃ ┃ ┃ ┣ 🗒  **disk11.rom**
┃ ┃ ┃ ┃ ┣ 🗒  **hdbdw3bck.rom**
┃ ┃ ┃ ┃ ┣ 🗒  **mx1600bas.rom**

## ![](/emulators/roms.png) Roms

### Extensions supportées

La rom doit avoir l'extension :

* .cas
* .wav
* .bas
* .asc
* .dmk
* .jvc
* .os9
* .dsk
* .vdk
* .rom
* .ccc

### Emplacement

Placez les roms comme ceci : 

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 trs80coco
┃ ┃ ┃ ┃ ┣ 🗒  **jeu.dsk**

>Pour plus d'information sur les roms, rendez-vous sur [ce tutoriel](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuration avancée de l'émulateur

>Pour pouvoir conserver vos configurations personnalisées lors d'une mise à jour, nous vous conseillons d'utiliser notre fonctionnalité [Surcharges de configuration](./../../../advanced-usage/configuration-override).
{.is-info}

## ![](/emulators/external-links.png) Liens externes

* **Code source utilisé** : [https://gitlab.com/recalbox/packages/standalone/xroar/](https://gitlab.com/recalbox/packages/standalone/xroar/)
* **Site web officiel** : [https://www.6809.org.uk/xroar/](https://www.6809.org.uk/xroar/)