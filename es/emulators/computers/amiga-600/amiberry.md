---
title: Amiberry
description: 
published: true
date: 2024-07-14T19:28:49.906Z
tags: amiga-600, amiga, amiberry, 600
editor: markdown
dateCreated: 2023-09-11T07:26:30.794Z
---

**Amiberry** es un core ARM optimizado para emular el Amiga.

## ![](/emulators/license.svg) Licencia

El core posee la siguiente licencia [**GPLv3**](https://github.com/midwan/amiberry/blob/master/COPYING).

## ![](/emulators/compatibility.png) Compatibilidad

 | RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | 
 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ❌ | ✅ |

## ![](/emulators/features.png) Funcionalidades



## ![](/emulators/bios.svg) BIOS

### Lista de BIOS obligatorias

Encontraréis los ficheros de bios con el nombre indicado en la columna **Descripción**. Tendréis que renombrarlos para que tengan el nombre indicado en la columna **Nombre del fichero**.

| Nombre del fichero | Descripción | MD5 | ¿Preinstalada? |
| :---: | :---: | :---: | :---: |
| kick33180.A500 | Kickstart v1.2 rev 33.180 (1986)(Commodore)(A500-A1000-A2000).rom | 85ad74194e87c08904327de1a9443b7a | ❌ |
| kick34005.A500 | Kickstart v1.3 rev 34.5 (1987)(Commodore)(A500-A1000-A2000-CDTV).rom | 82a21c1890cae844b3df741f2762d48d | ❌ |
| kick37175.A500 | Kickstart v2.04 rev 37.175 (1991)(Commodore)(A500+).rom | dc10d7bdd1b6f450773dfb558477c230 | ❌ |
| kick40063.A600 | Kickstart v3.1 rev 40.63 (1993)(Commodore)(A500-A600-A2000)\[!\].rom | e40a5dfb3d017ba8779faba30cbd1c8e | ❌ |
| kick02019.AROS.ext | AROS Free BIOS | 4fa7042d73d0f77bf5723cf8430989bf | ✅ |
| kick02019.AROS | AROS Free BIOS | 186d23cccd9fbd6336d0f988b39dce4d | ✅ |

### Emplazamiento

Disponed vuestras bios como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 🗒 **kick33180.A500**
┃ ┃ ┃ ┣ 🗒 **kick34005.A500**
┃ ┃ ┃ ┣ 🗒 **kick37175.A500**
┃ ┃ ┃ ┣ 🗒 **kick40063.A600**
┃ ┃ ┃ ┣ 🗒 **kick02019.AROS.ext**
┃ ┃ ┃ ┣ 🗒 **kick02019.AROS**

## ![](/emulators/roms.png) Roms

>La emulación Amiga se presenta en dos formas diferentes: Amiga 600 et Amiga 1200.
>Algunos juegos pueden ser caprichosos no dudéis a probar los dos sistemas si un juego no funciona.
>Los juegos se pueden utilizar con un formato de disco ADF o en formato WHDLoad, utilizando en este último caso un fichero UAE adicional.
{.is-info}

### Extensiones de roms soportadas

Las roms deben tener las extensiones siguientes:

* .adf
* .ipf
* .rp9
* .whd
* .zip

>Los otros formatos de imágenes de disco __pueden__ estar soportados pero no han sido probados por el equipo de Recalbox.
>
>Encontraréis más información sobre estos formatos en [esta página](./../../../tutorials/games/guides/amiga/faq).
{.is-info}

### Emplazamiento

Disponed vuestras roms como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 amiga600
┃ ┃ ┃ ┃ ┣ 🗒 **jeux.bin**
┃ ┃ ┃ ┃ ┣ 🗒 **jeux.lst**

>Se aconsejan las roms del proyecto **TOSEC**.
{.is-success}

>Para más información sobre las roms, consultad [este tutorial](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

>Para conservar las configuraciones personalizadas cuando se produce una actualización del sistema Recalbox, os aconsejamos utilizar las [Sobrecargas de configuración](./../../../advanced-usage/configuration-override).
{.is-info}

## ![](/emulators/external-links.png) Enlaces externos

* **Código fuente utilizado** : [https://gitlab.com/recalbox/packages/standalone/amiberry/](https://gitlab.com/recalbox/packages/standalone/amiberry)
* **Página oficial** : [https://blitterstudio.com/amiberry/](https://blitterstudio.com/amiberry/)