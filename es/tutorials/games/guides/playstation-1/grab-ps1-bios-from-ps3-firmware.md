---
title: Recuperar la bios ps1_rom.bin desde el firmware PS3
description: 
published: true
date: 2023-09-21T12:18:21.137Z
tags: bios, ps3, ps1
editor: markdown
dateCreated: 2023-09-21T12:18:21.137Z
---

## ¿Qué es la bios llamada ps1_rom.bin?

Esta bios es un fichero que se suministra con las actualizaciones de PS3.

Sus ventajas:

* Universal, apta para juegos de cualquier origen.
* Es la única bios que se puede obtener legalmente sin extraer la bios de vuestra consola original.

Este tutorial se os mostrará cómo obtener esta bios.

## ¿Cómo recuperar esta bios?

* Primero hay que descargar y descomprimir el emulador [RCPS3] (https://rpcs3.net/), un emulador para juegos de la PS3. Existe para Windows y Linux.

* Una vez descomprimido, id a [esta página](https://www.playstation.com/fr-fr/support/hardware/ps3/system-software/) y descarga el firmware de la PS3.

* Una vez descargado el fichero PS3UPDAT.PUP, abrid el emulador RCPS3.

* Abrid el menú `File` y seleccionad `Install Firmware`.

![](/tutorials/games/guides/playstation-1/ps1-bios-from-ps3/ps1biosps3-1.png){.full-width}

* En la nueva ventana, buscad el fichero PS3UPDAT.PUP y abridlo.

![](/tutorials/games/guides/playstation-1/ps1-bios-from-ps3/ps1biosps3-2.png){.full-width}

* Debéis dejar RCPS3 trabajar durante algunos minutos.

![](/tutorials/games/guides/playstation-1/ps1-bios-from-ps3/ps1biosps3-3.png){.full-width}

* Cuand habrá terminado, id dentro de la carpeta donde habéis descomprimido RCPS3 e id dentro del directorio `/dev_flash/ps1_emu/`. Encontraréis el famoso fichero BIOS delante de vosotros.

![](/tutorials/games/guides/playstation-1/ps1-bios-from-ps3/ps1biosps3-4.png){.full-width}