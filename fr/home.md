---
title: Bienvenue sur le wiki de Recalbox !
description: Comment commencer avec Recalbox ?
published: true
date: 2023-09-02T17:10:04.873Z
tags: premier démarrage, comment faire, bienvenue, accueil
editor: markdown
dateCreated: 2021-05-25T09:28:04.520Z
---

![welcome.png](/home/welcome.png)

## Recalbox c'est quoi ?

[Recalbox](https://www.recalbox.com) c’est **LA console retrogaming ultime** qui vous permet de rejouer à tous les jeux, consoles et ordinateurs de votre enfance !

Avec [Recalbox](https://www.recalbox.com), (re)découvrez et (re)jouez aux titres qui ont marqués l’histoire du jeu vidéo, en toute simplicité !

[Recalbox](https://www.recalbox.com) s’installe très simplement sur des nanos-ordinateurs très bon marché comme le Raspberry Pi 4, sur des consoles portables comme l'Odroid Go Super, ou sur tout ordinateur (récent ou de récupération).

## Je veux installer Recalbox, comment procéder ?

Pour installer Recalbox sur votre board/pc, rendez vous sur la page [Préparation et Installation](./basic-usage/preparation-and-installation).

Pour en savoir plus sur les systèmes et émulateurs supportés, vous pouvez consulter la section [Compatibilité matériel](./hardware-compatibility).

## Je recherche une page en particulier ?

Utilisez et abusez de la fonction de **recherche** en haut de cette page !

Vous pouvez simplement renseigner votre recherche dans la zone de texte :
![search-text.png](/home/search-text.png)

Ou bien rechercher les pages par tags :
![search-tags.png](/home/search-tags.png)

## Je suis un débutant, par quoi commencer ?

* Nous vous suggérons de consulter dans la documentation :
  * [L'usage basique](./basic-usage), pour comprendre **les bases du fonctionnement** de [Recalbox](https://www.recalbox.com).
  * [La F.A.Q](./faq).
  * [La compatibilité des émulateurs](./hardware-compatibility/emulators-compatibility).
  * [La compatibilité des périphériques](./hardware-compatibility/compatible-devices).
  * Vous pouvez également consulter la section [Émulateur](./emulators) pour avoir plus d'informations sur ceux présents dans [Recalbox](https://www.recalbox.com).
  * Si certains termes vous semblent trop techniques, le [glossaire](./basic-usage/glossary) peut être un précieux allié.
* Pour une utilisation plus poussée de Recalbox :
  *  Référez-vous à la partie [Tutoriels](./tutorials).
  *  Visitez la section [Usage avancé](./advanced-usage).
* Vous pouvez aussi consulter pour obtenir des renseignements supplémentaires : 
  * Le [site internet](https://www.recalbox.com/fr/) de Recalbox.
  * Le [forum](https://forum.recalbox.com/).
  * Le [Discord](https://discord.gg/NbQFbGM).

## Avez-vous des conseils pour un nouvel utilisateur ?

**[Recalbox](https://www.recalbox.com)** est un projet **open source** et donc **gratuit !**

En tant que tel, les personnes qui travaillent dessus le font sur leurs temps libres, et le projet et sa documentation évoluent au rythme des contributions de chacun. 

**Les meilleurs conseils** que nous pouvons vous donner sont :

* D’être curieux.
* De lire la documentation.
* De lire les [tutoriels](./tutorials).
* De participer a l'effort de rédaction et de traduction sur ce wiki (voir [Comment contribuer](./contribute))
* D'effectuer des recherches sur le  [forum](https://forum.recalbox.com/).
* De toujours faire une sauvegarde de votre dossier **share** avant de faire une manipulation dont l'issue est incertaine pour vous.