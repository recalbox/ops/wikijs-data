---
title: DOSBox
description: 
published: true
date: 2024-07-16T19:24:31.378Z
tags: dos, msdos, dosbox
editor: markdown
dateCreated: 2023-09-11T09:13:13.324Z
---

DOSBox es un emulador capaz de emular un PC compatible con IBM que ejecute el sistema operativo DOS.  
Se pueden emular varios tipos de tarjetas gráficas o de sonido.

Esto hace posible jugar a viejos juegos de PC en Recalbox.

## ![](/emulators/license.svg) Licencia

El core posee la siguiente licencia [**GPLv2**](https://gitlab.com/lmerckx/recalbox-dosbox/-/blob/master/COPYING).

## ![](/emulators/compatibility.png) Compatibilidad

 | RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | 
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | 

## ![](/emulators/features.png) Funcionalidades

| Funcionalidad | ¿Soportada? |
| :---: | :---: |
| Capturas de pantalla | ✔ |
| Grabaciones | ✔ |
| Opciones del core | ✔ |
| Controles | ✔ |
| Remapping | ✔ |


## ![](/emulators/bios.svg) BIOS

>**No necesita bios.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Extensiones de roms soportadas

Las roms deben tener las extensiones siguientes:

* .exe
* .com
* .bat
* .conf

### Emplazamiento

Disponed vuestras roms como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 dos
┃ ┃ ┃ ┃ ┣ 📁 **jeu**
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **jeu.exe**

## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

>Para conservar las configuraciones personalizadas cuando se produce una actualización del sistema Recalbox, os aconsejamos utilizar las [Sobrecargas de configuración](./../../../advanced-usage/configuration-override).
{.is-info}

## ![](/emulators/external-links.png) Enlaces externos

* **Código fuente utilizado** : [https://gitlab.com/lmerckx/recalbox-dosbox/](https://gitlab.com/lmerckx/recalbox-dosbox/)
* **Wiki oficial** : [https://www.dosbox.com/wiki/](https://www.dosbox.com/wiki/)
* **Lista de juegos** : [https://www.dosbox.com/comp_list.php?letter=a](https://www.dosbox.com/comp_list.php?letter=a)
* **Juegos jugables** : [https://www.dosbox.com/comp_list.php?letter=playable](https://www.dosbox.com/comp_list.php?letter=playable)