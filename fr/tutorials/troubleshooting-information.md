---
title: 🔖 Dépannage
description: 
published: true
date: 2022-08-14T20:02:23.011Z
tags: dépannage, tutoriel
editor: markdown
dateCreated: 2021-05-21T07:52:36.044Z
---

Cet endroit concerne les plus aguerris d'entre-vous, à base de lignes de commandes SSH. Vous pourrez exécutr certains commandes utiles si on vous le demande.

Voici les tutoriels disponibles :

[Activer le mode debug](enable-debug-mode)
[Afficher le contenu de la mémoire tampon Linux](display-buffer-memory-linux)
[Lister les modules chargés](list-loaded-modules)
[Récupérer les informations sur les manettes](grab-controllers-informations)
[Script de support](support-script)