---
title: Usar un dispositivo NVMe de almacenamiento en Recalbox
description: 
published: true
date: 2025-01-11T01:25:26.286Z
tags: almacenamiento, nvme
editor: markdown
dateCreated: 2024-09-29T17:34:21.559Z
---

## Introducción

Puede utilizar un dispositivo NVMe con Recalbox para almacenar sus ROMs a través de un hat M.2. Este dispositivo funcionará como un disco duro interno.

![Hat M.2 para Raspberry Pi 5](/tutorials/system/installation/m.2-dual-hat-for-raspberry-pi-5.jpg)

Debe formatear su soporte NVMe en exFAT. No se realizará ningún formateo desde Recalbox.

Existen varias marcas de hat M.2, algunas aceptan solo un NVMe y otras pueden aceptar 2. Este tutorial está basado en un hat M.2 que acepta 2 soportes NVMe pero con solo uno conectado.

>Este tutorial está principalmente dirigido a la Raspberry Pi 5. También puede aplicarse a discos duros en PC.
{.is-info}

## Procedimiento

Siga los siguientes pasos y podrá utilizar su NVMe como almacenamiento adicional para sus ROMs.

### Actualización del bootloader

Primero, debe actualizar el bootloader de su Raspberry Pi 5 siguiendo [este tutorial](../../utilities/update-rpi4-bootloader).

### Creación del directorio de acceso

Debe crear un directorio en su share para acceder al contenido de su NVMe. Para ello, use el siguiente comando [SSH](../../system/access/root-access-terminal-cli):

```sh
mkdir -p /recalbox/share/externals/hdd0
```

### Modificación del archivo `recalbox-boot.conf`

Debe añadir una línea en el archivo `recalbox-boot.conf` de 2 maneras diferentes:

#### Si su tarjeta SD está en su PC

En la unidad llamada `RECALBOX`, abra el archivo `recalbox-boot.conf` y añada la siguiente línea en cualquier línea vacía:

```sh
sharenetwork_cmd=mount /dev/nvme0n1p2 /recalbox/share/externals/hdd0
```

#### Si lo hace en su Recalbox encendida

- Conéctese vía [SSH](../../system/access/root-access-terminal-cli).
- Permita que la [partición de arranque](../../system/access/remount-partition-with-write-access) pueda ser usada en modo escritura.
- Abra el archivo `/boot/recalbox-boot.conf` y añada la siguiente línea en cualquier línea vacía:

```ini
sharenetwork_cmd=mount /dev/nvme0n1p2 /recalbox/share/externals/hdd0
```

### Modificación del archivo `smb.conf`

Aquí, es obligatorio modificar el archivo `/etc/samba/smb.conf` desde su Recalbox encendida:

- Conéctese vía [SSH](../../system/access/root-access-terminal-cli).
- Permita que la [partición del sistema](../../system/access/remount-partition-with-write-access) pueda ser usada en modo escritura.
- Abra el archivo `/etc/samba/smb.conf` y añada las siguientes líneas al final del archivo:

```ini
[nvme]
comment = Recalbox user data
path = /recalbox/share/externals/hdd0
writeable = yes
guest ok = yes
create mask = 0644
directory mask = 0755
force user = root
veto files = /._*/.DS_Store/
delete veto files = yes
```

### Prueba de las modificaciones

Inicie o reinicie su Recalbox y compruebe:

- Si tiene la carpeta `nvme` en el recurso compartido de red.
- Si el NVMe aparece en los menús de la interfaz (`START` > `AJUSTES DEL SISTEMA` > `MEDIO DE ALMACENAMIENTO`).
- Vía SSH, con el comando `mount`, su NVMe debería aparecer:

```sh
# mount
...
/dev/nvme0n1p2 on /recalbox/share/externals/hdd0 type exfat (rw,relatime,fmask=0022,dmark=0022,iocharset=utf8,errors=remount-ro)
...
```

## ¿Qué hacer si no funciona?

Si su NVMe no aparece, puede verificar el nombre de su NVMe interno con este comando [SSH](../../system/access/root-access-terminal-cli):

```sh
blkid
```

Debería obtener un resultado similar a este:

```sh
/dev/nvme0n1p1: LABEL="EFI" UUID="67E3-17ED" TYPE="vfat"
/dev/nvme0n1p2: LABEL="NVMe 256G" UUID="66E9-99AE" TYPE="exfat"
/dev/mmcblk0p1: LABEL="RECALBOX" UUID="2A19-621E" TYPE="vfat"
/dev/mmcblk0p2: LABEL="SHARE" UUID="4140-70D7" TYPE="exfat"
/dev/mmcblk0p3: LABEL="OVERLAY" UUID="007f6e1f-ef13-4abd-9a69-1b70d8317e3b" TYPE="ext4"
```

Aquí, `nvme0n1p1` es una pequeña partición que puede haber sido creada por algunos sistemas y que no necesitamos, por lo que utilizamos `nvme0n1p2` en el resto del tutorial.

## Notas

### Poner el NVMe en modo de solo lectura

Puede poner su NVMe en modo de solo lectura si no quiere que Recalbox modifique los archivos con este comando SSH:

```sh
mount -o ro /dev/<su_nvme> /recalbox/share/externals/hdd0
```

### NVMe formateado en NTFS

No se **recomienda** tener su NVMe formateado en NTFS. Si aún así quiere usarlo en NTFS, se aconseja **MUY ENCARECIDAMENTE** montarlo en modo de solo lectura. El formato NTFS aún no es totalmente compatible con un entorno Linux como Recalbox lo que podría impedir que pueda escribir datos en su NVMe.
