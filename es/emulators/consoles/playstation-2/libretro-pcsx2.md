---
title: Libretro PCSX2
description: 
published: true
date: 2024-07-06T19:59:57.963Z
tags: libretro, playstation 2, ps2, pcsx2, 8.0+
editor: markdown
dateCreated: 2023-09-07T16:40:04.943Z
---



## ![](/emulators/license.svg) Licencia

El core posee la siguiente licencia [**GPLv2**](https://github.com/libretro/pcsx2/blob/main/COPYING.GPLv2).

## ![](/emulators/compatibility.png) Compatibilidad

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | 
| ❌ | ❌ | ❌ | ❌ | ❌ | ❌ | ✅ | ❌ |

## ![](/emulators/features.png) Funcionalidades

| Funcionalidad | ¿Soportada? |
| :---: | :---: |
| Reinicio | ✔ |
| Capturas de pantalla | ✔ |
| Grabaciones | ✔ |
| Opciones del core | ✔ |
| Cheats RetroArch | ✔ |
| Controles | ✔ |
| Remapping | ✔ |
| Vibración | ✔ |

## ![](/emulators/bios.svg) BIOS

### Lista de BIOS obligatorias

| Nombre del fichero | Descripción | MD5 | ¿Preinstalada? |
| :---: | :---: | :---: | :---: |
| ps2-0230a-20080220.bin | SCPH-90001/SCPH-90010 (Version 5.0 02/20/08 A) | 21038400dc633070a78ad53090c53017 | ❌ |
| ps2-0230e-20080220.bin | SCPH-90002/SCPH-90003/SCPH-90004/SCPH-90008 (Version 5.0 02/20/08 E) | dc69f0643a3030aaa4797501b483d6c4 | ❌ |
| ps2-0230h-20080220.bin | SCPH-90005/SCPH-90006/SCPH-90007 (Version 5.0 02/20/08 J) | 30d56e79d89fbddf10938fa67fe3f34e | ❌ |
| ps2-0230j-20080220.bin | SCPH-90000 (Version 5.0 02/20/08 J) | 80ac46fa7e77b8ab4366e86948e54f83 | ❌ |

### Emplazamiento

Disponed vuestras bios como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 ps2
┃ ┃ ┃ ┃ ┣ 🗒 **ps2-0230a-20080220.bin**
┃ ┃ ┃ ┃ ┣ 🗒 **ps2-0230e-20080220.bin**
┃ ┃ ┃ ┃ ┣ 🗒 **ps2-0230h-20080220.bin**
┃ ┃ ┃ ┃ ┣ 🗒 **ps2-0230j-20080220.bin**
┃ ┃ ┃ ┃ ┣ 🗒 **ps2-0250e-20100415.bin**

## ![](/emulators/isos.svg) Isos

### Extensiones de roms soportadas

Las isos deben tener las extensiones siguientes:

* .bin
* .chd
* .cso
* .gz
* .iso
* .img
* .mdf
* .nrg

### Emplazamiento

Disponed vuestras isos como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 ps2
┃ ┃ ┃ ┃ ┣ 🗒 **juego.iso**

>Se aconjesan las isos del proyecto **Redump**.
{.is-success}

>Para más información sobre las isos, consultad [este tutorial](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

>Para conservar las configuraciones personalizadas cuando se produce una actualización del sistema Recalbox, os aconsejamos utilizar las [Sobrecargas de configuración](./../../../advanced-usage/configuration-override).
{.is-info}

### Acceder a las opciones

Podéis configurar las opciones del core de dos formas diferentes.

* Via el Menú RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via el fichero `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Opciones del core

| Opción | Valor de la opción | Variable (para la sobrecarga) | Valores posibles |
| --- | --- | --- | --- |
| System: BIOS | `HK v02.30(20/02/2008) Console` ✅ / `USA v02.30(20/02/2008) Console` / `Europe v02.30(20/02/2008) Console` / `Japan v02.30(20/02/2008) Console` | `pcsx2_bios` | `ps2-0230h-20080220.bin` / `ps2-0230a-20080220.bin` / `ps2-0230e-20080220.bin` / `ps2-0230j-20080220.bin` |
| System: Language | `English` ✅ / `French` / `Spanish` / `German` / `Italian` / `Dutch` / `Portuguese` | `pcsx2_system_language` | `English` / `French` / `Spanish` / `German` / `Italian` / `Dutch` / `Portuguese` |
| System: Fast Loading | `Disabled` ✅ / `Enabled` | `pcsx2_fastcdvd` | `disabled` / `enabled` |
| System: Fast Boot | `Disabled` ✅ / `Enabled` | `pcsx2_fastboot` | `disabled` / `enabled` |
| System: Boot To BIOS | `Disabled` ✅ / `Enabled` | `pcsx2_boot_bios` | `disabled` / `enabled` |
| Memory Card: Slot 1 | `Empty` / `Shared Memory Card (8MB)` ✅ / `Shared Memory Card (32 MB)` | `pcsx2_memcard_slot_1` | `empty` / `shared8` / `shared32` |
| Memory Card: Slot 2 | `Empty` ✅ / `Shared Memory Card (8MB)` / `Shared Memory Card (32 MB)` | `pcsx2_memcard_slot_2` | `empty` / `shared8` / `shared32` |
| Video: Renderer | `Auto` ✅ / `OpenGl` | `pcsx2_renderer` | `Auto` / `OpenGl` |
| Video: Internal Resolution | `Native PS2` ✅ / `2x Native ~720p` / `3x Native ~1080p` / `4x Native ~1440p 2K` / `5x Native ~1620 3K` / `6x Native ~2160p 4K` / `8x Native ~2880p 5K` | `pcsx2_upscale_multiplier` | `1` / `2` / `3` / `4` / `5` / `6` / `8` |
| Video: Deinterlacing Mode | `Automatic (default)` ✅ / `Blend bff - slight blur, 1/2 fps` / `Blend tff - slight blur, 1/2 fps` / `Bob bff   - use blend if shaking` / `Bob tff   - use blend if shaking` / `Weave bff - saw-tooth` / `Weave tff - saw-tooth` / `None` / `No-interlacing patch` | `pcsx2_deinterlace_mode` | `7` / `6` / `5` / `4` / `3` / `2` / `1` / `0` / `-1` |
| Video: Aspect Ratio | `Standard (4:3)` ✅ / `Widescreen (16:9)` | `pcsx2_aspect_ratio` | `0` / `1` |
| Video: Enable Widescreen Patches | `Disabled` ✅ / `Enabled` | `pcsx2_enable_widescreen_patches` | `disabled` / `enabled` |
| Video: Enable 60fps Patches | `Disabled` ✅ / `Enabled` | `pcsx2_enable_60fps_patches` | `disabled` / `enabled` |
| Video: FXAA | `Disabled` ✅ / `Enabled` | `pcsx2_fxaa` | `0` / `1` |
| Video: Anisotropic Filtering | `None` ✅ / `2x` / `4x` / `8x` / `16x` | `pcsx2_anisotropic_filter` | `0` / `2` / `4` / `8` / `16` |
| Video: Dithering | `Off` / `Scaled` / `Unscaled (default)` ✅ | `pcsx2_dithering` | `0` / `1` / `2` |
| Video: Texture Filtering | `Nearest` / `Bilinear (Forced)` / `Bilinear (PS2 - Default)` ✅ / `Bilinear (Forced Excluding Sprite)` | `pcsx2_texture_filtering` | `0` / `1` / `2` / `3` |
| Video: Mipmapping | `Automatic (default)` ✅ / `Off` / `Basic` / `Full` | `pcsx2_mipmapping` | `-1` / `0` / `1` / `2` |
| Video: Conservative Buffer Allocation | `Disabled` / `Enabled` ✅ | `pcsx2_conservative_buffer` | `disabled` / `enabled` |
| Video: Accurate DATE | `Disabled` / `Enabled` ✅ | `pcsx2_accurate_date` | `disabled` / `enabled` |
| Video: Frame Skip | `Disabled` ✅ / `Enabled` | `pcsx2_frameskip` | `disabled` / `enabled` |
| Video: Frameskip - Frames to Draw | `1` ✅ / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` | `pcsx2_frames_to_draw` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` |
| Video: Frameskip - Frames to Skip | `1` ✅ / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` | `pcsx2_frames_to_skip` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` |
| Gamepad: Enable Rumble | `Disabled` / `Enabled` ✅ | `pcsx2_rumble_enable` | `disabled` / `enabled` |
| Gamepad: Rumble Intensity | `10%` / `20%` / `30%` / `40%` / `50%` / `60%` / `70%` / `80%` / `90%` / `100%` ✅ | `pcsx2_rumble_intensity` | `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
| Gamepad: Left Stick Dead Zone | `No Dead Zone (default)` ✅ / `1%` / `2%` / `3%` / `4%` / `5%` / `6%` / `7%` / `8%` / `9%` / `10%` / `11%` / `12%` / `13%` / `14%` / `15%` / `16%` / `17%` / `18%` / `19%` / `20%` / `21%` / `22%` / `23%` / `24%` / `25%` / `26%` / `27%` / `28%` / `29%` / `30%` / `31%` / `32%` / `33%` / `34%` / `35%` / `36%` / `37%` / `38%` / `39%` / `40%` / `41%` / `42%` / `43%` / `44%` / `45%` / `46%` / `47%` / `48%` / `49%` / `50%` | `pcsx2_gamepad_l_deadzone` | `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `21` / `22` / `23` / `24` / `25` / `26` / `27` / `28` / `29` / `30` / `31` / `32` / `33` / `34` / `35` / `36` / `37` / `38` / `39` / `40` / `41` / `42` / `43` / `44` / `45` / `46` / `47` / `48` / `49` / `50` |
| Gamepad: Right Stick Dead Zone | `No Dead Zone (default)` ✅ / `1%` / `2%` / `3%` / `4%` / `5%` / `6%` / `7%` / `8%` / `9%` / `10%` / `11%` / `12%` / `13%` / `14%` / `15%` / `16%` / `17%` / `18%` / `19%` / `20%` / `21%` / `22%` / `23%` / `24%` / `25%` / `26%` / `27%` / `28%` / `29%` / `30%` / `31%` / `32%` / `33%` / `34%` / `35%` / `36%` / `37%` / `38%` / `39%` / `40%` / `41%` / `42%` / `43%` / `44%` / `45%` / `46%` / `47%` / `48%` / `49%` / `50%` | `pcsx2_gamepad_r_deadzone` | `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `21` / `22` / `23` / `24` / `25` / `26` / `27` / `28` / `29` / `30` / `31` / `32` / `33` / `34` / `35` / `36` / `37` / `38` / `39` / `40` / `41` / `42` / `43` / `44` / `45` / `46` / `47` / `48` / `49` / `50` |
| Patches: Enable Cheats | `Disabled` ✅ / `Enabled` | `pcsx2_enable_cheats` | `disabled` / `enabled` |
| Emulation: Speed Hacks Preset | `Safest - No Hacks` / `Safe (default)` ✅ / `Balanced` / `Aggressive` / `Very Aggressive` / `Mostly Harmful` | `pcsx2_speedhacks_presets` | `0` / `1` / `2` / `3` / `4` / `5` |
| Emulation: Vsyncs in MTGS Queue | `0` / `1` / `2 (default)` ✅ / `3` | `pcsx2_vsync_mtgs_queue` | `0` / `1` / `2` / `3` |
| Emulation: EE/FPU Clamping Mode | `None` / `Normal (default)` ✅ / `Extra + Preserve Sign` / `Full` | `pcsx2_clamping_mode` | `0` / `1` / `2` / `3` |
| Emulation: EE/FPU Round Mode | `Nearest` / `Negative` / `Positive` / `Chop/Zero (default)` ✅ | `pcsx2_round_mode` | `0` / `1` / `2` / `3` |
| Emulation: VUs Clamping Mode | `None` / `Normal (default)` ✅ / `Extra + Preserve Sign` / `Full` | `pcsx2_vu_clamping_mode` | `0` / `1` / `2` / `3` |
| Emulation: VUs Round Mode | `Nearest` / `Negative` / `Positive` / `Chop/Zero (default)` ✅ | `pcsx2_vu_round_mode` | `0` / `1` / `2` / `3` |
| Hack: Align Sprite | `Disabled` ✅ / `Enabled` | `pcsx2_userhack_align_sprite` | `disabled` / `enabled` |
| Hack: Merge Sprite | `Disabled` ✅ / `Enabled` | `pcsx2_userhack_merge_sprite` | `disabled` / `enabled` |
| Hack: Skipdraw - Start Layer | `0 (default)` ✅ / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` | `pcsx2_userhack_skipdraw_start` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` |
| Hack: Skipdraw - Layers to Skip | `+0 (default)` ✅ / `+1` / `+2` / `+3` / `+4` / `+5` / `+6` / `+7` / `+8` / `+9` / `+10` / `+11` / `+12` / `+13` / `+14` / `+15` / `+16` / `+17` / `+18` / `+19` / `+20` | `pcsx2_userhack_skipdraw_layers` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` |
| Hack: Half-pixel Offset | `Off (default)` ✅ / `Normal (Vertex)` / `Special (Texture)` / `Special (Texture-Aggessive)` | `pcsx2_userhack_halfpixel_offset` | `0` / `1` / `2` / `3` |
| Hack: Round Sprite | `Off (default)` ✅ / `Half` / `Full` | `pcsx2_userhack_round_sprite` | `0` / `1` / `2` |
| Hack: Wild Arms Offset | `Disabled` ✅ / `Enabled` | `pcsx2_userhack_wildarms_offset` | `disabled` / `enabled` |
| Hack: Half-screen fix | `Automatic (default)` ✅ / `Force-Disabled` / `Force-Enabled` | `pcsx2_userhack_halfpixel_fix` | `-1` / `0` / `1` |
| Hack: Auto Flush | `Disabled` ✅ / `Enabled` | `pcsx2_userhack_auto_flush` | `disabled` / `enabled` |
| Hack: Frame Buffer Conversion | `Disabled` ✅ / `Enabled` | `pcsx2_userhack_fb_conversion` | `disabled` / `enabled` |
| Hack: Texture Offset X - Hundreds | `0 (default)` ✅ / `100` / `200` / `300` / `400` / `500` / `600` / `700` / `800` / `900` | `pcsx2_userhack_texture_offset_x_hundreds` | `0` / `100` / `200` / `300` / `400` / `500` / `600` / `700` / `800` / `900` |
| Hack: Texture Offset X - Tens | `0 (default)` ✅ / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` | `pcsx2_userhack_texture_offset_x_tens` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` |
| Hack: Texture Offset Y - Hundreds | `0 (default)` ✅ / `100` / `200` / `300` / `400` / `500` / `600` / `700` / `800` / `900` | `pcsx2_userhack_texture_offset_y_hundreds` | `0` / `100` / `200` / `300` / `400` / `500` / `600` / `700` / `800` / `900` |
| Hack: Texture Offset Y - Tens | `0 (default)` ✅ / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` | `pcsx2_userhack_texture_offset_y_tens` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` |

## ![](/emulators/external-links.png) Enlaces externos

* **Código fuente utilizado** : [https://github.com/libretro/pcsx2/](https://github.com/libretro/pcsx2/)
* **Documentación Libretro** : [https://docs.libretro.com/library/pcsx2/](https://docs.libretro.com/library/pcsx2/)