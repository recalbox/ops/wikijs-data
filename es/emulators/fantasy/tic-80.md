---
title: Tic-80
description: Libretro-tic80
published: true
date: 2024-07-26T08:23:01.851Z
tags: tic-80, fantasy, consolas
editor: markdown
dateCreated: 2023-09-04T07:02:03.984Z
---

![](/emulators/fantasy/tic80.svg){.align-center}

## Detalles técnicos

* **Fabricante**: Vadim Grigoruk
* **Año de lanzamiento**: 2017
* **Características incluidas**: Editor de código, Editor de sprites, Editor de mapas de tiles, Editor de SFX
* **Resolución**: 240x136 píxeles, paleta de 16 colores
* **Sprites**: 256 sprites de primer plano de 8x8, 256 tiles de fondo de 8x8
* **Sonido**: 4 canales (con envolventes de onda modificables)

## Presentación

El TIC-80 es un fantástico ordenador GRATUITO y OPEN SOURCE para crear, jugar y compartir pequeños juegos.

Con TIC-80, tienes herramientas de desarrollo incorporadas: código, sprites, mapas, editores de sonido y línea de comandos, ¡lo cual es suficiente para crear un minijuego retro!

Los juegos se agrupan en un archivo de cartucho, que se puede distribuir fácilmente. El TIC-80 funciona en todas las plataformas populares. Esto significa que su cartucho se puede jugar en cualquier dispositivo.

Para crear un juego de estilo retro, todo el proceso de creación y ejecución se desarrolla dentro de ciertos límites técnicos: pantalla de 240x136 píxeles, paleta de 16 colores, 256 sprites de 8x8 colores, sonido de 4 canales, etc.

## Emuladores

[Libretro Tic80](libretro-tic80)
