---
title: ¿Quiénes somos?
description: 
published: true
date: 2023-09-26T16:53:06.839Z
tags: buildroot, quienes, somos
editor: markdown
dateCreated: 2021-08-02T18:33:35.147Z
---

## Presentación

**Recalbox** es una **consola de juegos retro** de código abierto que te permite jugar a una gran variedad de consolas y plataformas en tu salón, ¡muy fácilmente!

**Recalbox** es el **sistema operativo** del proyecto Recalbox, una consola de emulación **lista para usar** basada en la **Raspberry Pi** que es totalmente **libre y de código abierto**, y que ha sido diseñada específicamente para que **puedas crear la tuya propia en poco tiempo**.

Recalbox ofrece una **amplia selección de consolas y sistemas**, desde las primeras máquinas arcade, hasta plataformas como Playstation, Nes y Megadrive.

**Recalbox** utiliza varios elementos existentes, como [EmulationStation](https://github.com/Aloshi/EmulationStation) como **interfaz**, [piFBA](https://github.com/digitalLumberjack/pifba) y [RetroArch](https://github.com/libretro/RetroArch) como **emuladores**.

**Recalbox** puede describirse en una palabra: **Fácil**.
Queremos ofrecer un sistema lo más fácil e intuitivo posible.  
No es necesario modificar una multitud de archivos de configuración ni conectarse por SSH.
Sólo hay que instalar el sistema y jugar.
El sistema se configura a través de un único archivo, llamado `recalbox.conf`.   
Esto permite un ajuste fino de todos los emuladores.

Pero para los **usuarios avanzados** que quieran tener una **configuración más avanzada**, les dimos la posibilidad de hacer un sistema a su gusto con **overrides** (sólo modificar si se está seguro de lo que se está haciendo).

### Docenas de software de código abierto...... juntos en un solo sistema.

**Recalbox** se basa en el sistema **GNU/Linux** con Buildroot.  
Integra cientos de programas de código abierto, desde la más pequeña utilidad, hasta sus emuladores y su frontend EmulationStation.

Encuentra las docenas de emuladores RetroArch/Libretro, el famoso reproductor multimedia Kodi y el frontend personalizado EmulationStation.

![](/presentation/opensource.png)

### Con homebrews y hacks, descubre juegos nunca lanzados en consola

Los homebrews son juegos creados desde cero por los desarrolladores. Muchas de ellos te darán muy buenas sorpresas, y te harán pasar un buen rato.

Los hacks son modificaciones de juegos existentes. Toman todo o parte de los gráficos y la jugabilidad de un juego, y se diferencian por la historia, la dificultad y la atmósfera.
Algunos de ellos se consideran secuelas en toda regla de los juegos originales, como Super Mario Bros 4 (hack de Super Mario World) o Legend Of Zelda - Parallels Worlds (hack de A Link To The Past).

![](/presentation/zeldaparallelsworlds.png)

### Media Center

Con la ayuda de **Kodi**, Recalbox también actúa como **Centro multimedia**.  
Al conectarlo a la red doméstica, puedes reproducir vídeos desde todos tus dispositivos compatibles (caja de Internet, ordenador).

### Características

* Sistema :
  * [Sistemas compatibles](./../hardware-compatibility/emulators-compatibility)
  * **Versión optimizada de FBA con soporte para 4 jugadores**
  * **Compilado con Buildroot :** Lo que significa que el sistema de archivos base solo tiene 150 MB comprimidos.
  * **Sistema de rescate basado en NOOBS :** Posibilidad de reinstalar el sistema directamente desde la tarjeta SD, u obteniendo la última versión desde Internet.
  * **Idiomas del sistema soportados :** francés, inglés, español, alemán, italiano, portugués y quizás otras por venir si se participa.
  * **Interfaz :** Basada en EmulationStation desarrollada por Aloshi.
  * **Música de fondo en la interfaz.**
  * **Gestión de favoritos.**
* Red :
  * **Acceso a través de la red :** a los directorios de roms, capturas de pantalla y partidas guardadas.
  * **Soporte Wifi y RJ45**
  * **Actualizaciones en línea**
* Controladores:
  * **Configura tu mando de juego directamente desde la interfaz:** Una vez configurado, será compatible con todos los emuladores.
  * **Compatibilidad integrada con mandos de PS3, Xbox360, 8BitDo y Bluetooth :** ¡Enlaza un mando y juega!
  * **Presencia de controladores GPIO :** Para usar controles arcade o sus controladores originales Nes, Snes, Megadrive, PSX.
  * Para utilizar los controles arcade o tus mandos originales de Nes, Snes, Megadrive, PSX.
  * **Compatibilidad con los mandos de 2 jugadores Xin-Mo**
  * **Soporte de Gamepad Virtual** [Miroof's Virtual Gamepad](https://github.com/miroof/node-virtual-gamepads) : Utilizad vuestro teléfono como si fuera un mando.
* Funcionalidades:
  * **Kodi Media Center**
  * **Moonlight :** Enviad vuestros juegos a través de la red local o de Internet (requiere una tarjeta gráfica Nvidia).
  * **Netplay :** Para jugar en línea.
  * **Retroarch AI :** Traducid el texto de los juegos sobre la marcha.
  * **Scraper interno y externo :** insertad una imagen, un vídeo y una descripción para cada juego.
  * **Snaps videos :** Presentación vídeo de los juegos en los scrapes.

## Agradecimientos

Recalbox **no existiría** sin los emuladores **de código abierto** que utiliza.  
Queremos **agradecer a todas las personas** que contribuyen a proporcionar **emuladores de código abierto,** gracias a los cuales hemos tenido la oportunidad de jugar, de nuevo, a **todos esos videojuegos clásicos** 🤩

### Aquí está la lista de software utilizado en Recalbox

* Buildroot: [http://buildroot.uclibc.org/](http://buildroot.uclibc.org/)
* EmulationStation: [http://emulationstation.org/](http://emulationstation.org/)
* Diseño de la interfaz de usuario de EmulationStation y tema original de stock "simple": Nils Bonenberger - [http://blog.nilsbyte.de/](http://blog.nilsbyte.de/)

### Emuladores

* Núcleos RetroArch y Libretro: [http://www.libretro.com/](http://www.libretro.com/)
* PiFBA: [https://code.google.com/p/pifba/](https://code.google.com/p/pifba/)
* Mupen64: [https://github.com/mupen64plus/](https://github.com/mupen64plus/)
* Dolphin-emu: [https://fr.dolphin-emu.org/](https://fr.dolphin-emu.org/)
* PPSSPP: [https://www.ppsspp.org/](https://www.ppsspp.org/)

### Otros

* Reproductor multimedia Kodi: [http://kodi.tv/](http://kodi.tv/)
* Qtsixa: [http://qtsixa.sourceforge.net/](http://qtsixa.sourceforge.net/)

