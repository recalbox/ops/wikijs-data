---
title: Desactivación del chip T2 en el Mac
description: 
published: true
date: 2023-09-18T06:43:21.600Z
tags: mac, t2, chip, desactivar
editor: markdown
dateCreated: 2023-09-18T06:43:21.600Z
---

## Introducción

Si utilizáis un Mac Intel con un chip de seguridad T2, debéis cambiar la seguridad del arranque para poder arrancar desde un medio externo (tarjeta SD o memoria USB).

## Utilización

* Reiniciad el Mac.
* Al comienzo del reinicio, mantened pulsada la combinación de teclas `Comando (⌘) + R`.
* Una vez finalizado el arranque, seleccionad la cuenta de administrador que queréis utilizar e introducid su contraseña.
* Seleccionad `Utilidades` > `Utilidad de Seguridad de Arranque`.
* En la nueva ventana, seleccionad las opciones `No security` junto con `Allow booting from external media` y cerrad la ventana.
* Reiniciad
* Al comienzo del reinicio, mantened pulsada la tecla `Opción (⌥)` y cuando aparezcan vuestros discos en la pantalla pantalla, seleccionad `EFI Boot`.

¡Y a jugar!