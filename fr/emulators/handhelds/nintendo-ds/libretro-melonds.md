---
title: Libretro melonDS
description: 
published: true
date: 2024-06-22T11:25:21.925Z
tags: libretro, ds, melonds
editor: markdown
dateCreated: 2021-05-21T08:31:44.847Z
---

**Libretro melonDS** est un émulateur Nintendo DS/DSI prometteur développé en C et C++.

L'émulateur **melonDS** vise à fournir une émulation Nintendo DS rapide et précise. Bien qu'il s'agisse encore d'un travail en cours, il possède un ensemble assez solide de fonctionnalités :

* Cœur presque complet (CPU, vidéo, audio, ...)
* Recompilateur JIT pour une émulation rapide
* Rendu OpenGL, conversion ascendante 3D
* RTC, microphone, couvercle fermé / ouvert
* Prise en charge du joystick
* Savestates
* Différents modes d'affichage / dimensionnement / rotation
* (WIP) Wifi: multijoueur local, connectivité en ligne
* Émulation DSi (WIP)
* DLDI
* (WIP) Modules complémentaires d’emplacement GBA
* et plus sont prévus !

Il a été écrit par **Arisotura** (connu sous le nom de StapleButter), un ancien contributeur à DeSmuME.

## ![](/emulators/license.svg) Licence

Cet émulateur est sous licence [**GPLv3**](https://github.com/libretro/melonDS/blob/master/LICENSE).

## ![](/emulators/compatibility.png) Compatibilité

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ❌ | ❌ | ❌ | ❌ | ❌ | ❌ | ✅ | ❌ |

## ![](/emulators/features.png) Fonctionnalités

| Fonctionnalité | Supporté |
| :---: | :---: |
| Redémarrage | ✔ |
| Captures d'écran | ✔ |
| Sauvegardes | ✔ |
| Sauvegardes instantanées | ✔ |
| Rembobinage | ✔ |
| Options du core | ✔ |
| RetroAchievements | ✔ |
| Cheats RetroArch | ✔ |
| Controls | ✔ |
| Remapping | ✔ |

## ![](/emulators/bios.svg) BIOS

### Liste des bios obligatoires

| Nom de fichier | Description | MD5 | Fourni |
| :---: | :---: | :---: | :---: |
| bios7.bin | BIOS ARM7 | df692a80a5b1bc90728bc3dfc76cd948 | ❌ |
| bios9.bin | BIOS ARM9 | a392174eb3e572fed6447e956bde4b25 | ❌ |
| firmware.bin | Firmware NDS | 145eaef5bd3037cbc247c213bb3da1b3 e45033d9b0fa6b0de071292bba7c9d13 3ad72b2c9a736b24953f2d391da4bfcc | ❌ |

### Emplacement

Placez les bios comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 nds
┃ ┃ ┃ ┃ ┣ 🗒 **bios7.bin**
┃ ┃ ┃ ┃ ┣ 🗒 **bios9.bin**
┃ ┃ ┃ ┃ ┣ 🗒 **firmware.bin**

## ![](/emulators/roms.png) Roms

### Extensions supportées

La rom doit avoir l'extension :

* .nds
* .zip
* .7z

### Emplacement

Placez les roms comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 nds
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.zip**

>Les roms au format **No-Intro** sont vivement conseillées.
{.is-success}

>Pour plus d'information sur les roms, rendez-vous sur [ce tutoriel](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuration avancée de l'émulateur

>Pour pouvoir conserver vos configurations personnalisées lors d'une mise à jour, nous vous conseillons d'utiliser notre fonctionnalité [Surcharges de configuration](./../../../advanced-usage/configuration-override).
{.is-info}

### Accéder aux options

Vous pouvez configurer diverses options de deux façons différentes.

* Via le menu RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via le fichier `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Options du core

| Options | Valeur des options | Variable (pour surcharge) | Valeurs des variables |
| --- | --- | --- | --- |
| Console Mode | `DS` ✅ / `DSi` | `melonds_console_mode` | `DS` / `DSi` |
| Boot game directly | `Activé` ✅ / `Désactivé` | `melonds_boot_directly` | `enabled` / `disabled` |
| Use Firmware Settings | `Désactivé` ✅ / `Activé` | `melonds_use_fw_settings` | `disabled` / `enabled` |
| Language | `Japanese` / `English` ✅ / `French` / `German` / `Italian` / `Spanish` | `melonds_language` | `Japanese` / `English` / `French` / `German` / `Italian` / `Spanish` |
| Randomize MAC Address | `Désactivé` ✅ / `Activé` | `melonds_randomize_mac_address` | `disabled` / `enabled` |
| Enable DSi SD Card | `Désactivé` ✅ / `Activé` | `melonds_dsi_sdcard` | `disabled` / `enabled` |
| Threaded software renderer | `Désactivé` ✅ / `Activé` | `melonds_threaded_renderer` | `disabled` / `enabled` |
| OpenGL Renderer (Restart) | `Désactivé` ✅ / `Activé` | `melonds_opengl_renderer` | `disabled` / `enabled` |
| OpenGL Internal Resolution | `1x native (256x192)` ✅ / `2x native (512x384)` / `3x native (768x576)` / `4x native (1024x768)` / `5x native (1280x960)` / `6x native (1536x1152)` / `7x native (1792x1344)` / `8x native (2048x1536)` | `melonds_opengl_resolution` | `1x native (256x192)` / `2x native (512x384)` / `3x native (768x576)` / `4x native (1024x768)` / `5x native (1280x960)` / `6x native (1536x1152)` / `7x native (1792x1344)` / `8x native (2048x1536)` |
| OpenGL Improved polygon splitting | `Désactivé` ✅ / `Activé` | `melonds_opengl_better_polygons` | `disabled` / `enabled` |
| OpenGL Filtering | `nearest` ✅ / `linear` | `melonds_opengl_filtering` | `nearest` / `linear` |
| Microphone Input | `Blow Noise` ✅ / `White Noise` | `melonds_mic_input` | `Blow Noise` / `White Noise` |
| Audio Bitrate | `Automatic` ✅ / `10-bit` / `16-bit` | `melonds_audio_bitrate` | `Automatic` / `10-bit` / `16-bit` |
| Audio Interpolation | `None` ✅ / `Linear` / `Cosine` / `Cubic` | `melonds_audio_interpolation` | `None` / `Linear` / `Cosine` / `Cubic` |
| Touch mode | `Désactivé` ✅ / `Mouse` / `Touch` / `Joystick` | `melonds_touch_mode` | `disabled` / `Mouse` / `Touch` / `Joystick` |
| Swap Screen mode | `Toggle` ✅ / `Hold` | `melonds_swapscreen_mode` | `Toggle` / `Hold` |
| Screen Layout | `Top/Bottom` ✅ / `Bottom/Top` / `Left/Right` / `Right/Left` / `Top Only` / `Bottom Only` / `Hybrid Top` / `Hybrid Bottom` | `melonds_screen_layout` | `Top/Bottom` / `Bottom/Top` / `Left/Right` / `Right/Left` / `Top Only` / `Bottom Only` / `Hybrid Top` / `Hybrid Bottom` |
| Screen Gap | `0` ✅ / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `21` / `22` / `23` / `24` / `25` / `26` / `27` / `28` / `29` / `30` / `31` / `32` / `33` / `34` / `35` / `36` / `37` / `38` / `39` / `40` / `41` / `42` / `43` / `44` / `45` / `46` / `47` / `48` / `49` / `50` / `51` / `52` / `53` / `54` / `55` / `56` / `57` / `58` / `59` / `60` / `61` / `62` / `63` / `64` / `65` / `66` / `67` / `68` / `69` / `70` / `71` / `72` / `73` / `74` / `75` / `76` / `77` / `78` / `79` / `80` / `81` / `82` / `83` / `84` / `85` / `86` / `87` / `88` / `89` / `90` / `91` / `92` / `93` / `94` / `95` / `96` / `97` / `98` / `99` / `100` / `101` / `102` / `103` / `104` / `105` / `106` / `107` / `108` / `109` / `110` / `111` / `112` / `113` / `114` / `115` / `116` / `117` / `118` / `119` / `120` / `121` / `122` / `123` / `124` / `125` / `126` | `melonds_screen_gap` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `21` / `22` / `23` / `24` / `25` / `26` / `27` / `28` / `29` / `30` / `31` / `32` / `33` / `34` / `35` / `36` / `37` / `38` / `39` / `40` / `41` / `42` / `43` / `44` / `45` / `46` / `47` / `48` / `49` / `50` / `51` / `52` / `53` / `54` / `55` / `56` / `57` / `58` / `59` / `60` / `61` / `62` / `63` / `64` / `65` / `66` / `67` / `68` / `69` / `70` / `71` / `72` / `73` / `74` / `75` / `76` / `77` / `78` / `79` / `80` / `81` / `82` / `83` / `84` / `85` / `86` / `87` / `88` / `89` / `90` / `91` / `92` / `93` / `94` / `95` / `96` / `97` / `98` / `99` / `100` / `101` / `102` / `103` / `104` / `105` / `106` / `107` / `108` / `109` / `110` / `111` / `112` / `113` / `114` / `115` / `116` / `117` / `118` / `119` / `120` / `121` / `122` / `123` / `124` / `125` / `126` |
| JIT Enable (Restart) | `Activé` ✅ / `Désactivé` | `melonds_jit_enable` | `enabled` / `disabled` |
| JIT Block Size | `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `21` / `22` / `23` / `24` / `25` / `26` / `27` / `28` / `29` / `30` / `31` / `32` ✅ / `33` / `34` / `35` / `36` / `37` / `38` / `39` / `40` / `41` / `42` / `43` / `44` / `45` / `46` / `47` / `48` / `49` / `50` / `51` / `52` / `53` / `54` / `55` / `56` / `57` / `58` / `59` / `60` / `61` / `62` / `63` / `64` / `65` / `66` / `67` / `68` / `69` / `70` / `71` / `72` / `73` / `74` / `75` / `76` / `77` / `78` / `79` / `80` / `81` / `82` / `83` / `84` / `85` / `86` / `87` / `88` / `89` / `90` / `91` / `92` / `93` / `94` / `95` / `96` / `97` / `98` / `99` / `100` | `melonds_jit_block_size` | `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `21` / `22` / `23` / `24` / `25` / `26` / `27` / `28` / `29` / `30` / `31` / `32` / `33` / `34` / `35` / `36` / `37` / `38` / `39` / `40` / `41` / `42` / `43` / `44` / `45` / `46` / `47` / `48` / `49` / `50` / `51` / `52` / `53` / `54` / `55` / `56` / `57` / `58` / `59` / `60` / `61` / `62` / `63` / `64` / `65` / `66` / `67` / `68` / `69` / `70` / `71` / `72` / `73` / `74` / `75` / `76` / `77` / `78` / `79` / `80` / `81` / `82` / `83` / `84` / `85` / `86` / `87` / `88` / `89` / `90` / `91` / `92` / `93` / `94` / `95` / `96` / `97` / `98` / `99` / `100` |
| JIT Branch optimisations | `Activé` ✅ / `Désactivé` | `melonds_jit_branch_optimisations` | `enabled` / `disabled` |
| JIT Literal optimisations | `Activé` ✅ / `Désactivé` | `melonds_jit_literal_optimisations` | `enabled` / `disabled` |
| JIT Fast Memory | `Activé` ✅ / `Désactivé` | `melonds_jit_fast_memory` | `enabled` / `disabled` |

## ![](/emulators/external-links.png) Liens externes

* **Code source utilisé** : [https://github.com/libretro/melonds/](https://github.com/libretro/melonds/)
* **Documentation Libretro** : [https://docs.libretro.com/library/melonds/](https://docs.libretro.com/library/melonds/)
* **Code source officiel** : [https://github.com/Arisotura/melonDS](https://github.com/Arisotura/melonDS/)
* **Site officiel** : [http://melonds.kuribo64.net/](http://melonds.kuribo64.net/)
* **Forum officiel** : [http://melonds.kuribo64.net/board/](http://melonds.kuribo64.net/board/)