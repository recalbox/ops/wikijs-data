---
title: Increase the power available on the USB ports
description: 
published: true
date: 2021-08-06T09:28:51.910Z
tags: usb, increase, power
editor: markdown
dateCreated: 2021-08-06T09:28:51.910Z
---

>This is reserved for the Raspberry Pi 2.
{.is-danger}

With the default settings, the Raspberry Pi 2 limits the current to **600mA for all 4 USB ports**. The software modification proposed here will extend this limit to 1200mA.

>* This modification is not only for Recalbox.
>* This modification is reversible.
>* The use of a 2000mA power supply is always recommended, whatever the use of the Raspberry Pi 2.
>* The maximum current allowed by the Raspberry Pi 2 is 2000mA.
{.is-warning}

## Why make this change?

If you need to connect, without using a self-powered USB hub, several power-hungry USB devices, without exceeding the maximum current of 1200mA on the 4 USB ports. For example: A 2.5" USB self-powered hard drive.

## Examples of use

* Use of a self-powered USB SSD to store all roms, bios and backups.
* USB wifi key greedy
* Reload / use several USB controllers simultaneously.

## How to do it

* You must open the file [recalbox-user-config.txt](./../../../tutorials/system/modification/configtxt-configuration)
* In this file, look for the line `max_usb_current=...`.
* Add the line `max_usb_current=1` or, if it already exists, change its value to 1.
* Save the `recalbox-user-config.txt` file.
* Reboot your Raspberry Pi 2.

>This tip also works on the Raspberry Pi 3, but the line `max_usb_current=...` does not exist in the `recalbox-user-config.txt` file. You will have to add it manually.
{.is-info}