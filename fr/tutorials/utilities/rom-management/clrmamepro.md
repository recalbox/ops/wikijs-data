---
title: Clrmamepro
description: Ou comment avoir son set de jeux propre et à jour
published: true
date: 2024-11-09T19:53:02.252Z
tags: clrmamepro
editor: markdown
dateCreated: 2021-05-21T08:42:24.386Z
---

Dans ce tutoriel, nous allons vous apprendre en quelques clics à vous servir de Clrmamepro pour extraire (rebuilder) ou pour scanner un romset arcade. 

## I - Avant de commencer

### 1 - Éléments nécessaires

Au préalable il vous faut :

* Le logiciel Clrmamepro [ici](https://mamedev.emulab.it/clrmamepro/).
* Un [romset MAME](./../../../advanced-usage/arcade-in-recalbox) qui correspond à votre émulateur.
* Les samples (optionnel selon le romset). 
* Les [CHDs](./../../../advanced-usage/arcade-in-recalbox) (optionnel selon le romset).
* Un fichiers .DAT ou .XML dans le dossier /recalbox/share/arcade/... de votre Recalbox.

### 2 - Définitions

* **Set** : Fichier compressé (.zip/.7z/.rar) de jeux, drivers et bios.
* **Rom** : Fichier contenu dans un set.

![](/tutorials/utilities/rom-management/clrmamepro/clrmamepro1.png)

* **Qu'est ce qu'un fichier dat ?** Les fichiers dat sont des fichiers textes avec des informations sur les jeux émulés avec une version spécifique de l'émulateur MAME.  Dans ses fichiers dat, on trouve les informations pour des sets particuliers comme : nom de la rom (name), année (year), fabricant (manufacturer), information de fusion (merge), rom parent (romof) ou clone de (cloneof), taille de la rom (size), CRC pour chaque rom. CRC est un algorithme qui permet de vérifier l'intégrité d'un fichier, clrmamepro l'utilise pour vérifier vos rom-sets.
* **ClrmamePro** : C'est un logiciel qui vous permet de vérifier et reconstruire vos romsets arcade en fonction d'un fichier d'information au format xml ou dat.

### 3 - Installation

#### Windows

Téléchargez ClrmamePro pour [Windows](http://mamedev.emulab.it/clrmamepro/#downloads)

#### macOS

Téléchargez ClrmamePro pour [macOS](http://www.emulab.it/)

#### Linux

Téléchargez ClrmamePro sous Linux avec [Wine](https://doc.ubuntu-fr.org/wine).

>Le fichier zip est une version portable.
{.is-info}

* Installez-le.
* Récupérez le fichier dat de l’émulateur arcade souhaité.
* Démarrer Clrmamepro :
  * Exécutez clrmamepro via un clic droit sur l’icône.
  * _Puis "Exécuter en tant qu'administrateur_".

>**Astuce sous Windows** :
>
>* Rendez vous sur l’exécutable cmpro64.exe ou cmpro32.exe
>* Puis clic droit
>* Choisir _Propriétés_
>* Aller dans l'onglet _Compatibilité_
>* Puis cocher la case "_Exécuter en tant qu'administrateur"_.
>* Valider par Ok.
>
>Le programme s’exécutera toujours en administrateur désormais.
{.is-success}

## II - Utilisation

### 1 - Descriptif des fonctions

![](/tutorials/utilities/rom-management/clrmamepro/clrmamepro2.png){.full-width}

### 2 - Rebuilder (reconstruire)

* Démarrer clrmamepro.

![](/tutorials/utilities/rom-management/clrmamepro/clrmamepro3.png){.full-width}

* Pour sélectionner votre fichier .dat cliquez sur "Add DatFile".

![](/tutorials/utilities/rom-management/clrmamepro/clrmamepro4.png){.full-width}

* Sélectionnez votre fichier puis "ouvrir".

![](/tutorials/utilities/rom-management/clrmamepro/clrmamepro5.png){.full-width}

* Une fois sélectionné, vous pouvez indiquer un dossier ou un sous dossier. En sélectionnant "ok" clrmamepro va créer un dossier "NEW DATFILES" automatiquement.

![](/tutorials/utilities/rom-management/clrmamepro/clrmamepro6.png){.full-width}

* Sélectionnez votre fichier .dat puis "Load/Update" afin de le charger

![](/tutorials/utilities/rom-management/clrmamepro/clrmamepro7.png){.full-width}

* Clrmamepro vous propose de créer ou de mettre à jour la configuration. Nous vous conseillons de cliquer sur "Default".

![](/tutorials/utilities/rom-management/clrmamepro/clrmamepro8.png){.full-width}

* Clrmamepro va lire et charger votre fichier et vous devriez arriver sur le menu général.
  * Pour extraire un romset cliquer sur "Rebuilder"

![](/tutorials/utilities/rom-management/clrmamepro/clrmamepro9.png)

* Dans la "source" vous devez indiquer le dossier de votre romset MAME.

![](/tutorials/utilities/rom-management/clrmamepro/clrmamepro10.png){.full-width}

* Sélectionnez le dossier ou se trouve votre romset puis "OK".

![](/tutorials/utilities/rom-management/clrmamepro/clrmamepro11.png){.full-width}

* Dans la "destination" vous devez indiquer un dossier pour récolter le romset de l'extraction.

![](/tutorials/utilities/rom-management/clrmamepro/clrmamepro12.png){.full-width}

* Sélectionnez le dossier de destination puis "OK".

![](/tutorials/utilities/rom-management/clrmamepro/clrmamepro13.png){.full-width}

* **Dans le menu Rebuilder, vous pouvez paramétrer** :

  * 1 - [**Le type de romset**](./../../../advanced-usage/arcade-in-recalbox) que vous voulez créer **(Merge Options)** :

    * Non-merged
    * Split
    * Merged

  * 2 - **Les options** : (Nous vous conseillons les paramètres par defaut)

    * **Compress Files** : ce sont les options de compression (.zip/.7z/.rar).
    * **Recompress Files** : ce sont les options de recompressions. Si vous souhaitez ou non recompresser les fichiers.
    * **Show Statistics** : pour voir les résultats à la fin du Rebuild.
    * **Remove Matched Sourcefiles** : si cette option est activée, clrmamepro va extraire et supprimer les fichiers correspondant à votre fichier .DAT de votre Romset MAME.
    * **Systems** et **Advanced** : ne vous aventurez pas ici pour le moment.

* Lancez enfin l'extraction en cliquant sur "Rebuild".

![](/tutorials/utilities/rom-management/clrmamepro/clrmamepro14.png){.full-width}

* Laissez faire clrmamepro jusqu'à la fin.

![](/tutorials/utilities/rom-management/clrmamepro/clrmamepro15.png){.full-width}

* Et voilà ! Nous vous conseillons de faire un scanner du dossier de destination afin de vérifier qu'il est parfait🙂.

![](/tutorials/utilities/rom-management/clrmamepro/clrmamepro16.png){.full-width}

### 3 - Scanner (Vérifier)

* Démarrer clrmamepro.

![](/tutorials/utilities/rom-management/clrmamepro/clrmamepro3.png){.full-width}

* Pour sélectionner votre fichier .dat, cliquez sur "Add DatFile".

![](/tutorials/utilities/rom-management/clrmamepro/clrmamepro4.png){.full-width}

* Sélectionnez votre fichier puis "ouvrir". 

![](/tutorials/utilities/rom-management/clrmamepro/clrmamepro5.png){.full-width}

* Une fois sélectionné, vous pouvez indiquer un dossier ou un sous dossier. En sélectionnant "ok", clrmamepro va créer un dossier "NEW DATFILES" automatiquement.

![](/tutorials/utilities/rom-management/clrmamepro/clrmamepro6.png){.full-width}

* Sélectionnez votre fichier .dat puis "Load/Update" afin de le charger.

![](/tutorials/utilities/rom-management/clrmamepro/clrmamepro7.png){.full-width}

* Clrmamepro vous propose de créer ou de mettre à jour la configuration. Nous vous conseillons de cliquer sur "Default".

![](/tutorials/utilities/rom-management/clrmamepro/clrmamepro8.png){.full-width}

* Clrmamepro va lire et charger votre fichier et vous devriez arriver sur le menu général. Pour indiquer ou se trouve votre romset nous allons cliquer sur "Settings".

![](/tutorials/utilities/rom-management/clrmamepro/clrmamepro17.png)

* Le chemin vers votre romset dans "ROM-Paths" en cliquant sur "Add".

![](/tutorials/utilities/rom-management/clrmamepro/clrmamepro18.png){.full-width}

* Sélectionnez le dossier ou se trouve votre romset que vous souhaitez scanner puis "OK". 

![](/tutorials/utilities/rom-management/clrmamepro/clrmamepro19.png){.full-width}

* Pour Mame, si le romset doit contenir des samples, vous devez donner le chemin des samples de la même manière avec "Sample-Paths".

![](/tutorials/utilities/rom-management/clrmamepro/clrmamepro20.png){.full-width}

* Sauvegardez les chemins en cliquant sur "Save As Def.". 

![](/tutorials/utilities/rom-management/clrmamepro/clrmamepro21.png){.full-width}

* Clrmamepro vous confirme que c'est pris en compte alors cliquez sur "OK".

![](/tutorials/utilities/rom-management/clrmamepro/clrmamepro22.png){.full-width}

* Vous pouvez fermer cette fenêtre.

![](/tutorials/utilities/rom-management/clrmamepro/clrmamepro23.png){.full-width}

* Une fois revenu sur le menu principal, cliquez sur "Scanner".

![](/tutorials/utilities/rom-management/clrmamepro/clrmamepro24.png)

* Dans le menu **Scanner** vous devez paramétrer :

  * 1 - **Les éléments à scanner (You want to scan)** :

    * Sets (bios/jeux/drivers)
    * Roms (puces qui composent chaque Sets)
    * Samples (Drivers complémentaires pour certains jeux Mame)
    * [CHDs](./../../../advanced-usage/arcade-in-recalbox) (Que vous avez ajouté à votre Romset)

  * 2 - [**Le type de romset**](./../../../advanced-usage/arcade-in-recalbox) que vous avez **(You prefer)** :

    * Non-merged
    * Split
    * Merged

  * 3 - **Les messages pendant et après le scan (Prompts)** :

    * Ask Before Fixing
    * Add/Show Statistics

  * 4 - **Check/Fix** : Clrmamepro va vérifier ce qui est dans la colonne de gauche "check". Si vous souhaitez que clrmamepro fasse des corrections vous pouvez cocher les cases de la colonne de droite "Fix". Attention avec l'option "fix" clrmamepro va enlever les dossier et fichiers qui ne correspondent pas à votre DAT et les placer dans son "backup".

  * 5 - **Options** : Ne vous aventurez pas ici pour le moment 

Lancez enfin le scan en cliquant sur "New Scan"

![](/tutorials/utilities/rom-management/clrmamepro/clrmamepro25.png){.full-width}

* Dans cette fenêtre vont s'afficher les différents problèmes de votre set. (Exemple: missing rom) Vous pouvez exporter une liste de ce qu'il vous manque dans l'onglet "Miss List" afin de compléter.

![](/tutorials/utilities/rom-management/clrmamepro/clrmamepro26.png){.full-width}

* Enfin, vous aurez les résultats du scanner dans la fenêtre "statistics". Le plus important est la partie "Missing" ci-dessous il manque rien donc parfait.

![](/tutorials/utilities/rom-management/clrmamepro/clrmamepro27.png){.full-width}

#### A - Set Information

Dans la fenêtre du listing des jeux manquants, vous avez un bouton en bas à gauche nommé **Set Information**. En cliquant dessus, vous pouvez affiner les jeux que vous souhaitez conserver de ceux que vous ne souhaitez pas. Nous allons montrer ci-dessous un exemple en supprimant tous les clones.

* Dans le champ de texte **Select Sets**, indiquez `%c=?*`.
* Cliquez sur le bouton **Invert** à droite.
* Fermez la fenêtre
* Dans la fenêtre **Scanner** toujours ouverte, cliquez sur **Advanced**.
* Dans cette nouvelle fenêtre, cochez la case **Mark disabled sets as unneeded**.
* Fermez la fenêtre.

Maintenant, vous pouvez rescannez vos roms qui vérifiera uniquement les roms parentes.

Pour en savoir plus et connaitre encore plus de possibilitées de tri, vous pouvez consulter [cette page](https://www.emulab.it/forum/index.php?topic=3565.0) (anglais).