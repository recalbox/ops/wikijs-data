---
title: Jugar a las extensiones de los juegos
description: 
published: true
date: 2023-09-21T09:02:14.769Z
tags: disk, 64dd, drive, n64dd, fzero, extensiones
editor: markdown
dateCreated: 2023-09-21T09:02:14.769Z
---

## Introducción

Al menos 1 juego, F-Zero X, posee su extensión en el Disk Drive de Nintendo 64. Aquí veremos cómo hacerlo.

## Utilización

1. Colocad el juego `F-Zero X (Japan).z64` en el directorio `64dd`.
2. Colocad el juego `F-Zero X - Expansion Kit (Japan).ndd` en el directorio `64dd`.
3. Actualizad la lista de juegos.
4. En la lista de juegos, seleccionad el juego que queréis y pulsad `START`.

![](/tutorials/games/guides/64-dd/play-game-extensions/fzerodd1.png){.full-width}

5. Id dentro de `MODIFIER LE JEU` > `LANCER AVEC` y elegid el core `LIBRETRO MUPEN64PLUS_NEXT`.
   
![](/tutorials/games/guides/64-dd/play-game-extensions/fzerodd2.png){.full-width}

6. Cerrad todos los menús.
7. Lanzad `F-Zero X (Japan)` dentro del sistema `64dd`.
8. Cuando arranque, id dentro del menú de RetroArch con `Hotkey + B`.

![](/tutorials/games/guides/64-dd/play-game-extensions/fzerodd3.png){.full-width}

9. Haced « Volver » 1 vez y dirigíros a `Sub-sistemas`.

![](/tutorials/games/guides/64-dd/play-game-extensions/fzerodd4.png){.full-width}

10. Sélectionnez l'option `Load N64 Disk Drive`.

![](/tutorials/games/guides/64-dd/play-game-extensions/fzerodd5.png){.full-width}

11. Seleccionad el juego `F-Zero X - Expansion Kit (Japan).ndd`.

![](/tutorials/games/guides/64-dd/play-game-extensions/fzerodd6.png){.full-width}

12. Vais a volver hacia atrás automáticamente. Volved dentro de `Subsistemas` y elegid otra vez `Load N64 Disk Drive`.

![](/tutorials/games/guides/64-dd/play-game-extensions/fzerodd7.png){.full-width}

13. Seleccionad el juego `F-Zero X (Japan).z64`.

![](/tutorials/games/guides/64-dd/play-game-extensions/fzerodd8.png){.full-width}

14. Volved atrás automáticamente. Volved dentro de `Subsistemas` et elegid `Start N64 Disk Drive`.

![](/tutorials/games/guides/64-dd/play-game-extensions/fzerodd9.png){.full-width}

15. Vuestro juego arrancará con su extensión.

![](/tutorials/games/guides/64-dd/play-game-extensions/fzerodd10.png){.full-width}
![](/tutorials/games/guides/64-dd/play-game-extensions/fzerodd11.png){.full-width}

Buen juego !