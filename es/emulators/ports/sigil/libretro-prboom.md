---
title: Libretro PrBoom
description: 
published: true
date: 2024-07-25T23:04:45.327Z
tags: libretro, prboom, sigil
editor: markdown
dateCreated: 2023-09-12T06:30:25.119Z
---

## ![](/emulators/license.svg) Licencia

El core posee la siguiente licencia [**GPLv2**](https://github.com/libretro/libretro-prboom/blob/master/COPYING).

## ![](/emulators/compatibility.png) Compatibilidad

 | RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | 
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | 

## ![](/emulators/features.png) Funcionalidades

| Funcionalidad | ¿Soportada? |
| :---: | :---: |
| Opciones del core | ✔ |

## ![](/emulators/bios.svg) BIOS

>**No necesita bios.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Extensiones de roms soportadas

Las roms deben tener las extensiones siguientes:

* .zip

### Emplazamiento

Disponed vuestras roms como en la siguiente imagen: 

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 ports
┃ ┃ ┃ ┃ ┣ 📁 Doom
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **SIGIL_v1_21.wad**

Esta rom se encuentra presente por defecto en vuestra Recalbox.

## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

>Para conservar las configuraciones personalizadas cuando se produce una actualización del sistema Recalbox, os aconsejamos utilizar las [Sobrecargas de configuración](./../../../advanced-usage/configuration-override).
{.is-info}

### Acceder a las opciones

Podéis configurar las opciones del core de dos formas diferentes.

* Via el Menú RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via el fichero `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Opciones del core

| Opción | Valor de la opción | Variable (para la sobrecarga) | Valores posibles |
| --- | --- | --- | --- |
| Internal resolution (restart) | `320x200` ✅ / `640x400` / `960x600` / `1280x800` / `1600x1000` / `1920x1200` / `2240x1400` / `2560x1600` | `prboom-resolution` | `320x200` / `640x400` / `960x600` / `1280x800` / `1600x1000` / `1920x1200` / `2240x1400` / `2560x1600` |
| Mouse active when using Gamepad | `Désactivé` ✅ / `Activé` | `prboom-mouse_on` | `disabled` / `enabled` |
| Look on parent folders for IWADs | `Activé` ✅ / `Désactivé` | `prboom-find_recursive_one` | `enabled` / `disabled` |
| Rumble Effects | `Désactivé` ✅ / `Activé` | `prboom-rumble` | `disabled` / `enabled` |
| Analog Deadzone (percent) | `0` / `5` / `10` / `15` ✅ / `20` / `25` / `30` | `prboom-analog_deadzone` | `0` / `5` / `10` / `15` / `20` / `25` / `30` |

## ![](/emulators/external-links.png) Enlaces externos

