---
title: Vice_x64sc
description: 
published: true
date: 2024-07-26T07:00:26.055Z
tags: commodore-64, c64, vice, x64sc, 9.1+
editor: markdown
dateCreated: 2024-07-26T07:00:26.055Z
---

## ![](/emulators/license.svg) Licencia

Este núcleo está licenciado bajo [**GPLv2**](https://sourceforge.net/p/vice-emu/code/HEAD/tree/trunk/vice/COPYING).

## ![](/emulators/compatibility.png) Compatibilidad

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Funcionalidades

## ![](/emulators/bios.svg) BIOS

### Lista de BIOS opcionales

| Nombre del archivo | Descripción | MD5 | Proporcionado |
| :---: | :---: | :---: | :---: |
| JiffyDOS_C64.bin | JiffyDOS C64 Kernal | be09394f0576cf81fa8bacf634daf9a2 | ❌ |
| JiffyDOS_C128.bin | JiffyDOS C128 Kernal | cbbd1bbcb5e4fd8046b6030ab71fc021 | ❌ |
| JiffyDOS_C1541-II.bin | JiffyDOS 1541 drive BIOS | 1b1e985ea5325a1f46eb7fd9681707bf | ❌ |
| JiffyDOS_1571_repl310654.bin | JiffyDOS 1571 drive BIOS | 41c6cc528e9515ffd0ed9b180f8467c0 | ❌ |
| JiffyDOS_1581.bin | JiffyDOS 1581 drive BIOS | 20b6885c6dc2d42c38754a365b043d71 | ❌ |

### Ubicación

Coloque los BIOS de esta manera:

┣ 📁 recalbox  
┃ ┣ 📁 share  
┃ ┃ ┣ 📁 bios  
┃ ┃ ┃ ┣ 📁 vice  
┃ ┃ ┃ ┃ ┣ 🗒 **JiffyDOS_C64.bin**  
┃ ┃ ┃ ┃ ┣ 🗒 **JiffyDOS_C128.bin**  
┃ ┃ ┃ ┃ ┣ 🗒 **JiffyDOS_C1541-II.bin**  
┃ ┃ ┃ ┃ ┣ 🗒 **JiffyDOS_1571_repl310654.bin**  
┃ ┃ ┃ ┃ ┣ 🗒 **JiffyDOS_1581.bin**  

## ![](/emulators/roms.png) Roms

### Extensiones compatibles

Las roms deben tener las siguientes extensiones:

* .bin
* .cmd
* .crt
* .d1m
* .d2m
* .d4m
* .d64
* .d6z
* .d67
* .d71
* .d7z
* .d80
* .d81
* .d82
* .d8z
* .d90
* .dhd
* .g41
* .g4z
* .g64
* .g6z
* .m3u
* .nbz
* .nib
* .p00
* .p64
* .prg
* .t64
* .tap
* .tcrt
* .vfl
* .vsf
* .x64
* .x6z
* .zip
* .7z
* .gz

Este sistema soporta roms comprimidas en formato .zip/.7z. Sin embargo, esto solo es un archivo.

Los archivos contenidos en los .zip/.7z deben corresponder a las extensiones mencionadas anteriormente.  
Además, cada archivo .zip/.7z debe contener solo una rom comprimida.

### Ubicación

Coloque las roms de esta manera:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 c64
┃ ┃ ┃ ┃ ┣ 🗒 **juego.zip**

>Las roms en formato **No-Intro** son altamente recomendadas.
{.is-success}

>Para más información sobre las roms, consulte [este tutorial](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/external-links.png) Enlaces externos

* **Código fuente utilizado** : [https://sourceforge.net/projects/vice-emu/](https://sourceforge.net/projects/vice-emu/)
