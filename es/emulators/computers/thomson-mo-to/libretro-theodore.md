---
title: Libretro Theodore
description: 
published: true
date: 2024-07-18T18:50:50.386Z
tags: libretro, mo5, to8, theodore
editor: markdown
dateCreated: 2023-09-11T11:56:21.873Z
---

**Este ordenador** está emulado por el **core Libretro** [theodore](https://github.com/Zlika/theodore).  
**Este core** es capaz de emular **todos los modelos de la gama MOTO** (MO5, MO6, TO7, TO7/70, TO8, TO8D, TO9, TO9+) así como el Olivetti PC128, un clon del MO6 para el mercado italiano.

## ![](/emulators/license.svg) Licencia

El core posee la siguiente licencia [**GPLv3**](https://github.com/Zlika/theodore/blob/master/LICENSE).

## ![](/emulators/compatibility.png) Compatibilidad

 | RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | 
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | 

## ![](/emulators/features.png) Funcionalidades

| Funcionalidad | ¿Soportada? |
| :---: | :---: |
| Reinicio | ✔ |
| Capturas de pantalla | ✔ |
| Grabaciones | ✔ |
| Grabaciones instantáneas | ✔ |
| Rebobinado | ✔ |
| Netplay | ✔ |
| Opciones del core | ✔ |
| Controles | ✔ |
| Remapping | ✔ |
| Multi-Ratón | ✔ |

## ![](/emulators/bios.svg) BIOS

>**No necesita bios.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Extensiones de roms soportadas

Las roms deben tener las extensiones siguientes:

* .fd (disquette)
* .sap (disquette)
* .k7 (cassette)
* .rom (cartouche)
* .m7 (cartouche)
* .m5 (cartouche)
* .zip
* .7z

Este sistema soporta las roms comprimidas en formato .zip/.7z. No obstante, se trata sólo de un archivo.

Los ficheros contenidos en los .zip/.7z deben tener las extensiones citadas previamente.
Además, cada fichero .zip/.7z sólo debe contener un fichero rom al interior.

### Emplazamiento

Disponed vuestras roms como en la siguiente imagen: 

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 thomson
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.zip**

>Se aconsejan las roms del proyecto **TOSEC**.
{.is-success}

>Para más información sobre las roms, consultad [este tutorial](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

>Para conservar las configuraciones personalizadas cuando se produce una actualización del sistema Recalbox, os aconsejamos utilizar las [Sobrecargas de configuración](./../../../advanced-usage/configuration-override).
{.is-info}

### Acceder a las opciones

Podéis configurar las opciones del core de dos formas diferentes.

* Via el Menú RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via el fichero `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Opciones del core

| Opción | Valor de la opción | Variable (para la sobrecarga) | Valores posibles |
| --- | --- | --- | --- |
| Thomson model | `Auto` ✅ / `TO8` / `TO8D` / `TO9` / `TO9+` / `MO5` / `MO6` / `PC128` / `TO7` / `TO7/70` | `theodore_rom` | `Auto` / `TO8` / `TO8D` / `TO9` / `TO9+` / `MO5` / `MO6` / `PC128` / `TO7` / `TO7/70` |
 | Auto run game | `Activé` ✅ / `Désactivé` | `theodore_autorun` | `enabled` / `disabled` |
 | Use game hash for autostart | `Activé` ✅ / `Désactivé` | `theodore_autostart_use_game_hash` | `enabled` / `disabled` |
 | Display hint to start a game | `Activé` ✅ / `Désactivé` | `theodore_autostart_message_hint` | `enabled` / `disabled` |
| Virtual keyboard transparency | `0%` ✅ / `10%` / `20%` / `30%` / `40%` / `50%` / `60%` / `70%` / `80%` / `90%` | `theodore_vkb_transparency` | `0%` / `10%` / `20%` / `30%` / `40%` / `50%` / `60%` / `70%` / `80%` / `90%` |
| Floppy write protection | `Activé` ✅ / `Désactivé` | `theodore_floppy_write_protect` | `enabled` / `disabled` |
| Tape write protection | `Activé` ✅ / `Désactivé` | `theodore_tape_write_protect` | `enabled` / `disabled` |
| Dump printer data to file | `Désactivé` ✅ / `Activé` | `theodore_printer_emulation` | `disabled` / `enabled` |
| Interactive disassembler | `Désactivé` ✅ / `Activé` | `theodore_disassembler` | `disabled` / `enabled` |
| Break on illegal opcode | `Désactivé` ✅ / `Activé` | `theodore_break_illegal_opcode` | `disabled` / `enabled` |

## ![](/emulators/external-links.png) Enlaces externos

* **Código fuente utilizado** : [https://github.com/Zlika/theodore/](https://github.com/Zlika/theodore/)
* **Documentación Libretro** : [https://docs.libretro.com/library/theodore/](https://docs.libretro.com/library/theodore/)