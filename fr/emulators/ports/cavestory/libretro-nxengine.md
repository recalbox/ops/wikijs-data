---
title: Libretro NXEngine
description: 
published: true
date: 2024-06-23T12:04:13.987Z
tags: libretro, cavestory, nxengine
editor: markdown
dateCreated: 2021-05-21T08:32:45.616Z
---

Libretro NXEngine est un moteur de réécriture open-source de Cave Story pour Dingux et MotoMAGX. Auteur - Caitlin Shaw (rogueeve).

## ![](/emulators/license.svg) Licence

Ce core est sous licence [**GPLv3**](https://github.com/gameblabla/nxengine-nspire/blob/master/LICENSE).

## ![](/emulators/compatibility.png) Compatibilité

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Fonctionnalités

| Fonctionnalité | Supporté |
| :---: | :---: |
| Redémarrage | ✔ |
| Captures d'écran | ✔ |
| Sauvegardes | ✔ |
| Controls | ✔ |
| Remapping | ✔ |

## ![](/emulators/bios.svg) BIOS

>**Aucun bios n'est requis.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Extensions supportées

Les roms doivent avoir l'extension :

* .exe

### Emplacement

Placez les roms comme ceci : 

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 ports
┃ ┃ ┃ ┃ ┣ 📁 Cave Story
┃ ┃ ┃ ┃ ┃ ┣ 📁 CaveStory
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 **Doukutsu.exe**

## ![](/emulators/advanced-configuration.png) Configuration avancée de l'émulateur

>**Ce core n'a pas d'option.**
{.is-success}

## ![](/emulators/external-links.png) Liens externes

* **Code source utilisé** : [https://github.com/libretro/nxengine-libretro/](https://github.com/libretro/nxengine-libretro)
* **Code source officiel** : [https://github.com/EXL/NXEngine/](https://github.com/EXL/NXEngine)
* **Site officiel** : [nxengine.sourceforge.net](http://nxengine.sourceforge.net/)
* **Wiki du jeu** : [https://www.cavestory.org/guides-and-faqs/list.php](https://www.cavestory.org/guides-and-faqs/list.php)