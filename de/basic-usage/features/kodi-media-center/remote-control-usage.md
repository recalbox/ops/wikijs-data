---
title: Verwendung einer Infrarot-Fernbedienung
description: 
published: true
date: 2024-06-02T20:15:05.703Z
tags: 
editor: markdown
dateCreated: 2021-07-28T09:10:36.276Z
---

Die Recalbox kann über jede Infrarot-Fernbedienung gesteuert werden. Diese Funktion ist für Kodi.

Das kostet etwa 2 € und ist sehr einfach einzurichten.

## I - Voraussetzung

### A - Infrarotempfänger

Für den Betrieb benötigt man einen IR-Empfänger, wie z.B. ein 38KHz TSOP4838 Modul; es kann aber auch mit anderen Komponenten funktionieren. Man bekommt so ein Modul ganz einfach im Internet für etwa 1 €, oder in jedem guten Elektronikfachgeschäft.

![IR-Empfänger](/basic-usage/features/kodi/ir-control/ir1.jpg)

### B - Dupont-Kabel (Jumper Kabel) Buchse/Buchse

Um den Empfänger einfach und ohne Löten anschließen zu können, benötigst Du 3 Dupont B/B Kabel. Du kannst sie für ein paar Cent bei den gleichen Händlern bekommen, bei denen Du den IR-Empfänger kaufst.

![Dupont Kabel B/B](/basic-usage/features/kodi/ir-control/ir2.jpg)

### C - Schema

Zum Anschließen des Empfängers und der Kabel folgst Du dem untenstehenden Schema:

![](/basic-usage/features/kodi/ir-control/ir3.png)

Beispiel für die Montage:

![](/basic-usage/features/kodi/ir-control/ir4.jpg)

### D - Fernbedienung

Fast alle Fernbedienungen werden unterstützt, sofern sie Standards verwenden.

Die folgenden Fernbedienungen haben wir bereits erfolgreich getestet:

* Eine Philips HiFi-Fernbedienung.
* Eine Samsung-Fernbedienung für einen Videorekorder.
* Eine Universalfernbedienung.
* Eine Fernbedienung für einen MAC.

![](/basic-usage/features/kodi/ir-control/ir5.jpg)

## II - Konfiguration

### A - config.txt

* Öffnen die Datei [recalbox-user-config.txt](./../../../tutorials/system/modification/configtxt-configuration) über [SSH](./../../../tutorials/system/access/root-access-terminal-cli).
* Achte darauf, dass der Schreib-Lese-Modus auf der [Boot-Partition](./../../../tutorials/system/access/remount-partition-with-write-access) aktiviert ist.
* Entferne die Kommentare in der folgenden Zeile, indem du das `#` entfernst:

```ini
#dtoverlay=lirc-rpi
```

wird:

```ini
dtoverlay=lirc-rpi
```

* Starte deine Recalbox neu.

### B - Konfigurieren der Fernbedienung

#### 1. IR-Ereignisüberprüfung

Jetzt prüft man, ob die Geräte funktionieren:

* Verbinde dich mit deiner Recalbox über [SSH](./../../../tutorials/system/access/root-access-terminal-cli).
* Führe dann den folgenden Befehl aus:

```shell
lsmod
```

Du solltest eine Zeile sehen, die mit `lirc_rpi` beginnt.

![](/basic-usage/features/kodi/ir-control/ir6.png)

* Führe dann den folgenden Befehl aus:

```shell
mode2 -d /dev/lirc0
```

* Jedes Mal, wenn du eine Taste auf der Fernbedienung vor deinem Receiver drückst, solltest du Zahlen auf dem Terminal sehen. Sofern dies der Fall ist, funktionieren die Fernbedienung und der Empfänger ordnungsgemäß.
* Drücke `Strg`+`C` zum Beenden.

![](/basic-usage/features/kodi/ir-control/ir7.png)

#### 2. Registrieren der Fernbedienung

* Führe folgenden Befehl aus (noch immer in der aktiven SSH Verbindung):

```shell
irrecord -H /recalbox/share/custom.conf
```

* Drücke `Enter`, um fortzufahren.
* Gib `customremote` als Fernbedienungsnamen ein und drücke `Enter` um fortzufahren.

![](/basic-usage/features/kodi/ir-control/ir8.png)

* Drücke nun etwa eine Sekunde lang die Tasten auf deiner Fernbedienung.

![](/basic-usage/features/kodi/ir-control/ir9.png)

* Gib den Namen der Taste aus den unten stehenden Bezeichnungen ein und drücke die Taste auf der Fernbedienung:

  * KEY_DOWN (Runter)
  * KEY_EXIT (Zurück/Beenden)
  * KEY_INFO (Anzeige von Informationen über die aktuell wiedergegebene Datei)
  * KEY_LEFT (Links)
  * KEY_MENU (Menü)
  * KEY_MUTE (Stummschalten)
  * KEY_OK (Ok)
  * KEY_PLAY (Wiedergabe und Pause)
  * KEY_POWER (Beenden)
  * KEY_RIGHT (Rechts)
  * KEY_STOP (Stop)
  * KEY_UP (Hoch)
  * KEY_VOLUMEDOWN (Lautstärke -)
  * KEY_VOLUMEUP (Lautstärke +)

* Und gegebenenfalls:

  * KEY_AUDIO
  * KEY_BLUE
  * KEY_CHANNELDOWN
  * KEY_CHANNELUP
  * KEY_DELETE
  * KEY_ENTER
  * KEY_EPG
  * KEY_FASTFORWARD
  * KEY_GREEN
  * KEY_LANGUAGE
  * KEY_MEDIA
  * KEY_NEXT
  * KEY_NUMERIC_1
  * KEY_NUMERIC_2
  * KEY_NUMERIC_3
  * KEY_NUMERIC_4
  * KEY_NUMERIC_5
  * KEY_NUMERIC_6
  * KEY_NUMERIC_7
  * KEY_NUMERIC_8
  * KEY_NUMERIC_9
  * KEY_NUMERIC_0
  * KEY_PAUSE
  * KEY_PREVIOUS
  * KEY_PVR
  * KEY_RADIO
  * KEY_RECORD
  * KEY_RED
  * KEY_REWIND
  * KEY_SUBTITLE
  * KEY_VIDEO
  * KEY_YELLOW
  * KEY_ZOOM

![](/basic-usage/features/kodi/ir-control/ir10.png)

* Wiederhole diesen Vorgang entsprechend für alle oder so viele Tasten wie du möchtest.

Um eine Taste erneut einzurichten, gib den Namen einfach erneut ein verfahre wie oben beschrieben.

![](/basic-usage/features/kodi/ir-control/ir11.png)

* Wenn du alle gewünschten Tasten registriert hast, drücke `Enter`, um fortzufahren.
* Drücke dann eine der Tasten wiederholt sehr schnell, ohne sie gedrückt zu halten und ohne die Tasten zu wechseln.

![](/basic-usage/features/kodi/ir-control/ir12.png)

* Am Ende wird das Programm automatisch beendet.
* Wenn du neu anfangen willst, lösche die Konfigurationsdatei mit dem Befehl:

```shell
rm /tmp/custom.conf
```

* Und starte `irrecord` danach dann neu.

#### 3. Die Konfigurationsdatei

* Verschiebe die entsprechende Datei dann mit folgendem Befehl, um die Datei `lircd.conf` durch deine eigene zu ersetzen:

```shell
mv /recalbox/share/custom.conf /recalbox/share/system/.config/lirc/lircd.conf
```

* Starten den Dienst `lircd` mit dem folgenden Befehl wieder:

```shell
/etc/init.d/S25lircd restart
```

![](/basic-usage/features/kodi/ir-control/ir13.png)

#### 4. Prüfen der Einstellungen

* Gib den Befehl `irw` ein.
* Jedes Mal, wenn du eine Taste drückst, solltest du eine Zeile mit dem Namen der Taste erscheinen sehen.
* Drücke anschließend `Strg` + `C` zum Beenden.

![](/basic-usage/features/kodi/ir-control/ir14.png)

* Starte Kodi und überprüfe die Funktion.

## III - Erweiterte Konfiguration

### A - Lircmap.xml

* Du kannst die Einstellungen für die Fernbedienungstasten anpassen, indem du die Datei `/recalbox/share/system/.kodi/userdata/Lircmap.xml` bearbeitest.

>Solltest du den Ordner `~/.kodi` manuell löschen und Kodi neu starten, ohne vorher deine Recalbox neu zu starten, siehst du weder die Kodi-Anpassungen noch die Datei `Lircmap.xml`.
{.is-warning}

* Ziehe in Betracht, die Datei `/recalbox/share_init/system/.kodi/userdata/Lircmap.xml` nach `/recalbox/share/system/.kodi/userdata/Lircmap.xml` zu kopieren.

### B - remote.xml

Du kannst die Festlegung der Tasten und Aktionen in der Datei `/recalbox/share/system/.kodi/userdata/keymaps/remote.xml` ändern.

### C - BEENDEN/STOPPEN

Dies ist für Leute, die es nicht mögen, dass die `Zurück`-Taste einen Film in Kodi nicht anhält, oder weil Sie nur eine einzige Stop/Rewind-Taste auf Ihrer Fernbedienung haben.

Du kannst in der remote.xml-Datei die Funktion "Back" durch "Stop" ersetzen.

### D - LAUTSTÄRKE HOCH/RUNTER

>Dieser Punkt betrifft CEC-Fernbedienungen.
{.is-info}

Wenn deine CEC-Fernbedienung keine Lautstärkebefehle überträgt, kannst du andere Tasten verwenden, indem du sie z.B. in der remote.xml im globalen Abschnitt ersetzt:

```xml
  <skipplus>SkipNext</skipplus>
  <skipminus>SkipPrevious</skipminus>
```

wird dann bspw. zu

```xml
  <skipplus>VolumeUp</skipplus>
  <skipminus>VolumeDown</skipminus>
```

### E - PAUSE mit OK

Beim Refocus-Skin in Kodi kannst du die Datei `~/.kodi/addons/skin.refocus/720p/VideoOSD.xml` ändern, um das Pausieren zu vereinfachen (hauptsächlich für Apple-Fernbedienungen), indem du:

```xml
  <defaultcontrol always="true">700</defaultcontrol>
```

durch das folgende ersetzt

```xml
  <defaultcontrol always="true">705</defaultcontrol>
```