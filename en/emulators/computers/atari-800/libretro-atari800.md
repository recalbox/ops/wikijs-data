---
title: Libretro Atari800
description: 
published: true
date: 2021-10-07T00:02:17.276Z
tags: libretro, atari, 800, atari-800, atari800
editor: markdown
dateCreated: 2021-07-29T15:52:19.454Z
---



## ![](/emulators/license.svg) License

This core is under [**GPLv2**](https://github.com/atari800/atari800/blob/master/COPYING).

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Features



## ![](/emulators/bios.svg) BIOS

### List of mandatory bios

| Filename | Description | MD5 | Provided |
| :---: | :---: | :---: | :---: |
| ATARIBAS.ROM | BASIC interpreter | 0bac0c6a50104045d902df4503a4c30b | ❌ |
| ATARIOSA.ROM | Atari 400/800 PAL | eb1f32f5d9f382db1bbfb8d7f9cb343a | ❌ |
| ATARIOSB.ROM | Atari 400/800 NTSC | a3e8d617c95d08031fe1b20d541434b2 | ❌ |
| ATARIXL.ROM | Atari XL/XE OS | 06daac977823773a3eea3422fd26a703 | ❌ |

### Location

Put the bios like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 🗒 **ATARIXL.ROM**
┃ ┃ ┃ ┣ 🗒 **ATARIBAS.ROM**
┃ ┃ ┃ ┣ 🗒 **ATARIOSA.ROM**
┃ ┃ ┃ ┣ 🗒 **ATARIOSB.ROM**
┃ ┃ ┃ ┣ 🗒 **ATARIXL.ROM**

## ![](/emulators/roms.png) Roms

### Supported extensions

Roms must have the extension:

* .atr
* .atx
* .bin
* .car
* .cas
* .com
* .dcm
* .rom
* .xex
* .xfd
* .zip

This system supports compressed roms in .zip/.7z format. However, be careful, it is only an archive.

The files contained in the .zip/.7z must correspond to the extensions mentioned above.
Moreover, each .zip/.7z file must contain only one compressed rom.

### Location

Put the roms like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 atari800
┃ ┃ ┃ ┃ ┣ 🗒 **game.zip**

>The roms using **TOSEC** format are highly recommended.
{.is-success}

>For more information about roms, go to [this tutorial](./../../../tutorials/games/generalities/isos-and-roms)!
{.is-info}

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Accessing to options

You can configure various options in two different ways.

* Via RetroArch menu:

┣ 📁 RetroArch Menu
┃ ┣ 📁 Core options
┃ ┃ ┣ 🧩 Name_option

* Via the file `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core options

| Options | Options values | Variables (for configuration override) | Variables values |
| --- | --- | --- | --- |
| Atari System | `400/800 (OS B)` ✅ / `800XL (64K)` / `130XE (128K)` / `Modern XL/XE(320K CS)` / `Modern XL/XE(576K)` / `Modern XL/XE(1088K)` / `5200` | `atari800_system` | `400/800 (OS B)` / `800XL (64K)` / `130XE (128K)` / `Modern XL/XE(320K CS)` / `Modern XL/XE(576K)` / `Modern XL/XE(1088K)` / `5200` |
| Video Standard | `NTSC` ✅ / `PAL` | `atari800_ntscpal` | `NTSC` / `PAL` |
| Internal BASIC (hold OPTION on boot) | `Disabled` ✅ / `Enabled` | `atari800_internalbasic` | `disabled` / `enabled` |
| SIO Acceleration | `Disabled` ✅ / `Enabled` | `atari800_sioaccel` | `disabled` / `enabled` |
| Boot from Cassette | `Disabled` ✅ / `Enabled` | `atari800_cassboot` | `disabled` / `enabled` |
| Hi-Res Artifacting | `Disabled` ✅ / `Enabled` | `atari800_artifacting` | `disabled` / `enabled` |
| Autodetect A5200 CartType | `Disabled` ✅ / `Enabled` | `atari800_opt1` | `disabled` / `enabled` |
| Joy hack A5200 for Robotron | `Disabled` ✅ / `Enabled` | `atari800_opt2` | `disabled` / `enabled` |
| Internal resolution | `336x240` ✅ / `320x240` / `384x240` / `384x272` / `384x288` / `400x300` | `atari800_resolution` |  |
| Retroarch Keyboard type | `Disabled` ✅ / `Enabled` | `atari800_keyboard` | `disabled` / `enabled` |

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/atari800/atari800/](https://github.com/atari800/atari800/)
* **Libretro documentation**: [https://docs.libretro.com/library/atari800/](https://docs.libretro.com/library/atari800/)