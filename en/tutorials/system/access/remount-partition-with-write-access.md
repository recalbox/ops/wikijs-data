---
title: Remount partition with write access
description: 
published: true
date: 2021-08-06T09:13:46.221Z
tags: remount, partition, write
editor: markdown
dateCreated: 2021-08-06T09:13:46.221Z
---

At times, you will need to modify files that may be write-protected.

In the system, there are 2 write-protected locations:

* Boot partition: `/boot`
* System partition: `/`

## Command

Via [SSH](./../../../tutorials/system/access/root-access-terminal-cli), you will need to use the `mount` command.

## {.tabset}
### Boot partition

```shell
mount -o remount,rw /boot
```

### System partition :

```shell
mount -o remount,rw /
```