---
title: Lizenzen für zugehörige Projekte
description: 
published: true
date: 2021-10-02T10:28:24.466Z
tags: lizenz, projekt
editor: markdown
dateCreated: 2021-10-02T10:28:24.466Z
---

## Software-Projekte

Recalbox besteht nicht nur aus seinem Hauptwerk. Wir pflegen auch einige andere Open-Source-Projekte, die in Recalbox enthalten oder Teil der gesamten Recalbox-Infrastruktur sind.

Hier findet ihr einen Überblick über die Lizenzen der wichtigsten Projekte:

* Der [EmulationStation Recalbox fork](https://gitlab.com/recalbox/recalbox-emulationstation) steht unter der MIT Lizenz.
* Der Recalbox [Website Manager](https://gitlab.com/recalbox/recalbox-manager) steht unter "Alle Rechte vorbehalten".
* Die Recalbox-API steht unter den Bedingungen der MIT Lizenz.
* Die Recalbox [Websit(https://gitlab.com/recalbox/www.recalbox.com) steht unter "Alle Rechte vorbehalten".

## Kreative Werke

Wir pflegen und verteilen auch [Themes](https://gitlab.com/recalbox/recalbox-themes) für Recalbox, die unter den folgenden Lizenzen veröffentlicht werden:

| Theme | Lizenz |
| :---: | :---: |
| recalbox-goa2 | [CreativeCommons BY-NC-ND 4.0](https://creativecommons.org/licenses/by-nc-nd/4.0/) |
| recalbox-next | [CreativeCommons BY-NC-ND 4.0](https://creativecommons.org/licenses/by-nc-nd/4.0/) |
