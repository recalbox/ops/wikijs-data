---
title: Libretro MAME 2003
description: 
published: true
date: 2024-07-24T06:55:52.895Z
tags: libretro, mame, mame2003
editor: markdown
dateCreated: 2021-07-05T23:20:49.077Z
---

**Libretro MAME2003** is a more recent emulator than imame4all. Many more games are functional.

## ![](/emulators/license.svg) License

This core is under [**MAME non-commercial**](https://github.com/libretro/mame2003-libretro/blob/master/LICENSE.md) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :----: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Features

| Feature | Supported |
| :---: | :---: |
| Saves | ✔ |
| States | Game-dependent |
| Rewind | ✔ |
| Netplay | ✔ |
| Native Cheats | ✔ |
| Controllers | ✔ |
| Multi-Mouse | ✔ |

## ![](/emulators/bios.svg) BIOS

BIOS romsets are not required when using "Full Non-Merged" arcade romsets. For "Split" and "Non-Merged" romsets, place the necessary BIOS in the same directory as the game romset.

>**Note**:
>Place the bios in the directory: `/recalbox/share/roms/mame/`   
>or in its subdirectory if you use several Mame cores, for example: `/recalbox/share/roms/mame/Mame 2003/`
>Games and their bios must be in the same directory or sub-directory.
{.is-warning}

## ![](/emulators/roms.png) Roms

* Based on romset: MAME 0.78 (December 2003)
* DatFile: You can find the dat file in your recalbox at location `/recalbox/share/arcade/libretro/mame2003.dat`.

### Location

Put your roms like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 mame
┃ ┃ ┃ ┃ ┣ 🗒 **game.zip**
┃ ┃ ┃ ┃ ┣ 📁 **game**
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **game.chd**

You can also opt for a subdirectory (useful if you want to have a mame set for another core).

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 mame
┃ ┃ ┃ ┃ ┣ 📁 Mame 2003
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **game.zip**
┃ ┃ ┃ ┃ ┃ ┣ 📁 **game**
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 **game.chd**

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Accessing to options

You can configure various options in two different ways.

* Via RetroArch menu:

┣ 📁 RetroArch Menu
┃ ┣ 📁 Core options
┃ ┃ ┣ 🧩 Name_option

* Via the file `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core options

| Options | Options values | Variables (for configuration override) | Variables values |
| --- | --- | --- | --- |
| Skip Disclaimer | `Disabled` ✅ / `Enabled` | `mame2003_skip_disclaimer` | `disabled` / `enabled` |
| Skip Warnings | `Disabled` ✅ / `Enabled` | `mame2003_skip_warnings` | `disabled` / `enabled` |
| Display MAME Menu | `Disabled` ✅ / `Enabled` | `mame2003_display_setup` | `disabled` / `enabled` |
| Legacy Remapping | `Enabled` ✅ / `Disabled` | `mame2003_mame_remapping` | `enabled` / `disabled` |
| Autosave Hiscore | `default` ✅ / `recursively` / `disabled` | `mame2003_autosave_highscore` | `default` / `recursively` / `disabled` |
| NVRAM Bootstraps * | `Enabled` ✅ / `Disabled` | `mame2003_nvram_bootstraps` | `enabled` / `disabled` |
| Locate System Files Within a Subfolder | `Enabled` ✅ / `Disabled` | `mame2003_core_sys_subfolder` | `enabled` / `disabled` |
| Locate Save Files Within a Subfolder | `Enabled` ✅ / `Disabled` | `mame2003_core_save_subfolder` | `enabled` / `disabled` |
| Mouse Device | `mouse` ✅ / `pointer` / `Disabled` | `mame2003_mouse_device` | `mouse` / `pointer` / `disabled` |
| Input Interface | `simultaneous` ✅ / `retropad` / `keyboard` | `mame2003_input_interface` | `simultaneous` / `retropad` / `keyboard` |
| 4-Way Joystick Emulation on 8-Way Joysticks * | `Disabled` ✅ / `Enabled` | `mame2003_four_way_emulation` | `disabled` / `enabled` |
| Show Lightgun Crosshairs * | `Enabled` ✅ / `Disabled` | `mame2003_crosshair_enabled` | `enabled` / `disabled` |
| Map Right Analog Stick as Buttons | `Enabled` ✅ / `Disabled` | `mame2003_rstick_to_btns` | `enabled` / `disabled` |
| Dip Switch/Cheat Input Ports * | `Disabled` ✅ / `Enabled` | `mame2003_cheat_input_ports` | `disabled` / `enabled` |
| Use Samples | `Enabled` ✅ / `Disabled` | `mame2003_use_samples` | `enabled` / `disabled` |
| DCS Speedhack | `Enabled` ✅ / `Disabled` | `mame2003_dcs_speedhack` | `enabled` / `disabled` |
| Sample Rate | `8000 KHz` / `11025 KHz` / `22050 KHz` / `30000 KHz` / `44100 KHz` / `48000 KHz` ✅ | `mame2003_sample_rate` | `8000` / `11025` / `22050` / `30000` / `44100` / `48000` |
| Bypass Timing Skew | `Disabled` ✅ / `Enabled` | `mame2003_machine_timing` | `disabled` / `enabled` |
| Brightness | `0.2` / `0.3` / `0.4` / `0.5` / `0.6` / `0.7` / `0.8` / `0.9` / `1.0` ✅ / `1.1` / `1.2` / `1.3` / `1.4` / `1.5` / `1.6` / `1.7` / `1.8` / `1.9` / `2.0` | `mame2003_brightness` | `0.2` / `0.3` / `0.4` / `0.5` / `0.6` / `0.7` / `0.8` / `0.9` / `1.0` / `1.1` / `1.2` / `1.3` / `1.4` / `1.5` / `1.6` / `1.7` / `1.8` / `1.9` / `2.0` |
| Gamma Correction | `0.5` / `0.6` / `0.7` / `0.8` / `0.9` / `1.0` ✅ / `1.1` / `1.2` / `1.3` / `1.4` / `1.5` / `1.6` / `1.7` / `1.8` / `1.9` / `2.0` | `mame2003_gamma` | `0.5` / `0.6` / `0.7` / `0.8` / `0.9` / `1.0` / `1.1` / `1.2` / `1.3` / `1.4` / `1.5` / `1.6` / `1.7` / `1.8` / `1.9` / `2.0` |
| TATE Mode | `Disabled` ✅ / `Enabled` | `mame2003_tate_mode` | `disabled` / `enabled` |
| Frameskip | `Disabled` ✅ / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `9` / `10` / `11` / `auto` / `auto aggressive` / `auto max` | `mame2003_frameskip` | `disabled` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `9` / `10` / `11` / `auto` / `auto_aggressive` / `auto_max` |
| Display Artwork | `Enabled` ✅ / `Disabled` | `mame2003_display_artwork` | `enabled` / `disabled` |
| Resolution Multiplier | `1` ✅ / `2` | `mame2003_art_resolution` | `1` / `2` |
| Resolution * | `640x480` / `1024x768` ✅ / `1280x960` / `1440x1080` / `1600x1200` / `original` | `mame2003_vector_resolution` | `640x480` / `1024x768` / `1280x960` / `1440x1080` / `1600x1200` / `original` |
| Antialiasing * | `Enabled` ✅ / `Disabled` | `mame2003_vector_antialias` | `enabled` / `disabled` |
| Beam Width * | `1` / `1.2` ✅ / `1.4` / `1.6` / `1.8` / `2` / `2.5` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` | `mame2003_vector_beam_width` | `1` / `1.2` / `1.4` / `1.6` / `1.8` / `2` / `2.5` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` |
| Transculency * | `Enabled` ✅ / `Disabled` | `mame2003_vector_translucency` | `enabled` / `disabled` |
| Flicker * | `0` / `10` / `20` ✅ / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` | `mame2003_vector_flicker` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
| Intensity * | `0.5` / `1` / `1.5` ✅ / `2` / `2.5` / `3` | `mame2003_vector_intensity` | `0.5` / `1` / `1.5` / `2` / `2.5` / `3` |
| Specify Sega ST-V BIOS * | `default` ✅ / `japan` / `japana` / `us` / `japan_b` / `taiwan` / `europe` | `mame2003_stv_bios` | `default` / `japan` / `japana` / `us` / `japan_b` / `taiwan` / `europe` |
| CPU Clock Scale | `default` ✅ / `25%` / `30%` / `35%` / `40%` / `45%` / `50%` / `55%` / `60%` / `65%` / `70%` / `75%` / `80%` / `85%` / `90%` / `95%` / `105%` / `110%` / `115%` / `120%` / `125%` / `200%` / `250%` / `300%` | `mame2003_cpu_clock_scale` | `default` / `25` / `30` / `35` / `40` / `45` / `50` / `55` / `60` / `65` / `70` / `75` / `80` / `85` / `90` / `95` / `105` / `110` / `115` / `120` / `125` / `200` / `250` / `300` |

## ![](/emulators/external-links.png) External links

* **Progettosnaps**: [https://www.progettosnaps.net/](https://www.progettosnaps.net/)
* **Source code used**: [https://github.com/libretro/mame2003-libretro/](https://github.com/libretro/mame2003-libretro)
* **Libretro documentation**: [https://docs.libretro.com/library/mame_2003/](https://docs.libretro.com/library/mame_2003/)