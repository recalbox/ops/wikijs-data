---
title: DuckStation
description: 
published: false
date: 2021-11-09T20:24:59.870Z
tags: ps1, playstation 1, duckstation, 8.0+
editor: markdown
dateCreated: 2021-09-23T23:02:01.828Z
---

 

## ![](/emulators/license.svg) License

This core is under [**GPLv3**](https://github.com/stenzek/duckstation/blob/master/LICENSE) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 | 
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ❌ | ❌ | ✅ | ✅ | ✅ | ✅ | ❌ | ✅ |

## ![](/emulators/features.png) Features

| Feature | Supported |
| :---: | :---: |
|  |  |

## ![](/emulators/bios.svg) BIOS

### List of mandatory bios

| Filename | Description | MD5 | Provided |
| :---: | :---: | :---: | :---: |
| scph5500.bin | Japanese PS1 bios - required to play japanese games | 8dd7d5296a650fac7319bce665a6a53c | ❌ |
| scph5501.bin | American PS1 bios - required to play american games | 490f666e1afb15b7362b406ed1cea246 | ❌ |
| scph5502.bin | European PS1 bios - required to play european games | 32736f17079d0b2b7024407c39bd3050 | ❌ |

### Location

Put the bios like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 🗒 **scph5500.bin**
┃ ┃ ┃ ┣ 🗒 **scph5501.bin**
┃ ┃ ┃ ┣ 🗒 **scph5502.bin**

### List of optional bios

| Filename | Description | MD5 | Provided |
| :---: | :---: | :---: | :---: |
| ps1_rom.bin | PS1 bios - Provided with latest PS3 update | 81bbe60ba7a3d1cea1d48c14cbcc647b | ❌ |

### Location

Put the bios like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 🗒 **ps1_rom.bin**

## ![](/emulators/isos.svg) Isos

### Supported extensions

Isos must have the extension:

* .bin
* .chd
* .cue
* .ecm
* .img
* .iso
* .m3u
* .mds
* .pbp

### Location

Put the isos like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 psx
┃ ┃ ┃ ┃ ┣ 🗒 **game.bin**
┃ ┃ ┃ ┃ ┣ 🗒 **game.cue**

>The isos using **Redump** format are highly recommended.
{.is-success}

>For more information about isos, go to [this tutorial](./../../../tutorials/games/generalities/isos-and-roms)!
{.is-info}

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/stenzek/duckstation/](https://github.com/stenzek/duckstation/)