---
title: TFT
description: 
published: true
date: 2024-07-26T06:46:17.516Z
tags: tft
editor: markdown
dateCreated: 2023-09-14T07:27:50.273Z
---

Si queréis utilizar una pantalla TFT, en esta sección encontraréis lo que buscáis:

Aqui tenéis la lista de tutoriales disponibles:

[Conectar una minipantalla LCD a través de la interfaz I2C](lcd-display-with-i2c)
[Configurar vuestra pantalla TFT HDMI](hdmi-tft-screen)
[Configurar vuestra pantallita TFT a travès del bus SPI](tft-lcd-on-spi-bus)
[Configurar vuestra pantallita TFT a través del bus DPI](tft-lcd-on-dpi-bus)
[Segunda pantalla miniTFT para los scrapes y los vídeos](second-minitft-or-ogst-for-scraps-and-videos)