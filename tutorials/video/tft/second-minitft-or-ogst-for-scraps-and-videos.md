---
title: Second miniTFT screen for scraps and videos
description: 
published: true
date: 2022-07-08T22:22:58.434Z
tags: screen, minitft, scrap, videos
editor: markdown
dateCreated: 2021-08-06T23:02:23.943Z
---

This page will show you how to configure your second TFT screen to display scrapes and videos. This is for people who have an OGST box or a small TFT screen.

## Introduction

Recalbox supports almost natively the display of videos and scrapes from emulated systems, images and videos from installed games (when scraped) on a second screen connected on the GPIO port (SPI/DPI, Odroid OGST screen).

## What does this script do ?

The display operation proceeds like this:

* When the interface is on the available systems, a boot video of the console / system is visible if it exists, otherwise a default video is visible.
* When the interface is on the list of games: 
  * In the case of scraped games, the game image appears on the TFT screen.
  * In the case of scraped game videos and images, the TFT screen will display the game image for 1 second and the game video will be displayed in a loop. 
* When the game is launched: 
  * In case of scraped games, the game image will appear on the TFT screen. 
  * In case of scraped game video and image, the TFT screen will display the game video and then display the game image.
* When Recalbox displays the screen saver, the TFT screen is turned off, and once out of the screen saver, is turned on again. This is usable on SPI screens on the Raspberry Pi.

## Installing a 2.8" TFT SPI display 

A screen such as [2.8" 240x320 SPI TFT LCD display module serial port module with ILI9341 5V / 3.3V PCB](https://www.amazon.fr/dp/B07MXH92RL/) will work fine.

This kind of display is not expensive (< 15€). The connection to the GPIO port is as follows:

(missing picture, if you have this hardware and have a wiring picture, you can insert it here!)

### Wiring diagram

| TFT Module | Raspberry Pi |
| :--- | :--- |
| VCC | +3,3V (pin 1) |
| GND (pin 6) |
| CS | SPI0 CE0 (pin 24) |
| RESET | GPIO 25 (pin 22) |
| DC | GPIO 24 (pin 18) |
| SDA | SPI0 MOSI (pin 19) |
| SPI0 SCLK (pin 23) |
| GPIO 18 (pin 12) | SDO
| SDO | SPI0 MISO (pin 21) |

### Wiring diagram

(missing image, if you have this hardware and have a wiring image, you can insert it here!)

### Configuration

The support for the second mini TFT screen is integrated in Recalbox 7. It is still necessary to configure the Arduino mini TFT screen specifically. To do this, you need to modify the `recalbox-user-config.txt` file as shown below to add support for the display via an overlay.

#### Setting up

* You need to open the file [recalbox-user-config.txt](./../../system/modification/configtxt-configuration) with nano or vim and add the following lines:

```ini
dtparam=spi=on
#small tft spi 2.8 adafruit type (for arduino)
dtoverlay=rpi-display,reset=25,dc=24,led=18,rotate=90,speed=48000000
```

The SPI bus support is enabled and the display is configured via the appropriate overlay. You only have to configure the [recalbox.conf] file (./../../../basic-usage/getting-started/recalboxconf-file) to enable the display support. To do this, configure the options like this:

```ini
#Second miniTFT screen as marquee to display scrapes and videos
#Enable it with system.secondMiniTFT.enabled=1, disable with system.secondMiniTFT.enabled=0
 
system.secondminitft.enabled=1
 
#Type of mini TFT : OGST, unknown
#for Support of XU4 OGSTCase mini screen, just configure it secondminitft.type=ogst
#Most of spi tft screen are enabled thanks to overlays in /boot/config.txt
#please check the specific configuration of your screen
#Some examples are available on the page .....
# values are :
# - overlay : screen configured through overlays in /boot/config.txt
# - ogst : screen of the XU4 OGST case. Supported out of the box
# - default : the rest of the world
 
system.secondminitft.type=overlay
 
#Choose the resolution of your screen
#miniTFT resolution  320x240 -> 240p, 480x320 -> 320p
 
system.secondminitft.resolution=240p

#Scraped image aspect on tft screen : fbv display option
# options available : stretch, ignore-aspect, enlarge.
# stretch : Strech (using a simple resizing routine) the image to fit onto screen if necessary
# alpha : use the alpha channel (if applicable)
# ignore-aspect : Ignore the image aspect while resizing
# enlarge : Enlarge the image to fit the whole screen if necessary

# 1 = enabled, 0 = disabled

system.secondminitft.imagestretchenabled=1
system.secondminitft.imageenlargeenabled=0
system.secondminitft.imagealphaenabled=1
system.secondminitft.imageignoreaspectenabled=1

#When activating the second TFT screen, you may want to display
#game scraped video only on the second screen. This variable
#allows you to disable scraped video playing in ES during game browsing
#system.secondminitft.disablevideoines=1 disable video playing in ES
#system.secondminitft.disablevideoines=0 enable video playing in ES

system.secondminitft.disablevideoines=1

#if the backlight of your your screen can be manage by PWM, 
#indicates the  GPIO value of your PWM control
#WiringPi and its gpio program will be used to control the backlight
#when RB goes in SLEEP mode, and when it wakes up.
#On raspberry pi, PWMs cannot be used simultaneously with Jack sound output.
#If your are using HDMI or a Audio hat you can use backlight PWM control for the second screen
#If you are using Jack output please leave commented.
#The Screen will be switch down in Sleep mode, and switch on when it wakes up.
#On OGST, only this mode is available.

;system.secondminitft.backlightcontrol=18
```

Normally, everything is OK. Reboot and your screen should display the images and videos of the scraped games.

You can see the development tests on a Pi3 in [this video](https://www.youtube.com/watch?v=vX4hZjsDEVY).

Here you can see how GameMakers uses this feature on one of their creations: [a mini New Astro City](https://www.youtube.com/watch?v=Axzaw6IRtcI).

## Waveshare TFT Screen 3.5" RevA and Rev B

This screen is located [here](https://fr.aliexpress.com/item/32284958830.html).

### Wiring Chart

| PIN NO. | SYMBOL | DESCRIPTION |
| :--- | :--- | :--- |
| 1, 17 | 3.3V | Power positive (3.3V power input) |
| 2, 4 | 5V | Power positive (5V power input) |
| 3, 5, 7, 8, 10, 12, 13, 15, 16 | NC | NC |
| 6, 9, 14, 20, 25 | GND | Ground |
| 11 TP_IRQ Touch Panel interrupt, low level while the Touch Panel detects touching
| LCD_RS | Command/data register selection | 19
| 19 LCD_SI / TP_SI | LCD display/SPI data input of Touch Panel
| TP_SO | SPI data output of Touch Panel | 22
| 22 RST | Reset |
| LCD display/SPI clock of Touch Panel | 24 LCD_CSK / TP_SCK
| 24 LCD_CS | LCD chip selection, low active |
| 26 TP_CS | Touch Panel chip selection, low active |

### Configuration

The configuration procedure is identical to the 2.8" Arduino display but the parameters are different.

The support of the second mini TFT display is integrated in recalbox 7.0. It is still necessary to configure the Waveshare mini TFT display (rev A/B) specifically. To do this, you need to modify the `/boot/rcalbox-user-config.txt` file as shown below to add support for the display via an overlay.

#### Setting up

* You need to open the file [recalbox-user-config.txt](./../../system/modification/configtxt-configuration) with nano or vim and add the following lines:

```ini
#minitft 3.5 waveshare
dtparam=spi=on
dtoverlay=waveshare35a:rotate=270,speed=27000000 #for revA
dtoverlay=waveshare35b:rotate=270,speed=27000000 #for revB
```

SPI bus support is enabled and the display is configured via the appropriate overlay. Now you just have to configure the `recalbox.conf` file to enable the display support. To do this, configure the options like this:

```ini
#Second miniTFT screen as marquee to display scrapes and videos
#Enable it with system.secondMiniTFT.enabled=1, disable with system.secondMiniTFT.enabled=0
 
system.secondminitft.enabled=1
 
#Type of mini TFT : OGST, unknown
#for Support of XU4 OGSTCase mini screen, just configure it secondminitft.type=ogst
#Most of spi tft screen are enabled thanks to overlays in /boot/config.txt
#please check the specific configuration of your screen
#Some examples are available on the page .....
# values are :
# - overlay : screen configured through overlays in /boot/config.txt
# - ogst : screen of the XU4 OGST case. Supported out of the box
# - default : the rest of the world
 
system.secondminitft.type=overlay
 
#Choose the resolution of your screen
#miniTFT resolution  320x240 -> 240p, 480x320 -> 320p
 
system.secondminitft.resolution=320p

#Scraped image aspect on tft screen : fbv display option
# options available : stretch, ignore-aspect, enlarge.
# stretch : Strech (using a simple resizing routine) the image to fit onto screen if necessary
# alpha : use the alpha channel (if applicable)
# ignore-aspect : Ignore the image aspect while resizing
# enlarge : Enlarge the image to fit the whole screen if necessary

# 1 = enabled, 0 = disabled

system.secondminitft.imagestretchenabled=1
system.secondminitft.imageenlargeenabled=0
system.secondminitft.imagealphaenabled=1
system.secondminitft.imageignoreaspectenabled=1

#When activating the second TFT screen, you may want to display
#game scraped video only on the second screen. This variable
#allows you to disable scraped video playing in ES during game browsing
#system.secondminitft.disablevideoines=1 disable video playing in ES
#system.secondminitft.disablevideoines=0 enable video playing in ES

system.secondminitft.disablevideoines=1

#if the backlight of your your screen can be manage by PWM, 
#indicates the  GPIO value of your PWM control
#WiringPi and its gpio program will be used to control the backlight
#when RB goes in SLEEP mode, and when it wakes up.
#On raspberry pi, PWMs cannot be used simultaneously with Jack sound output.
#If your are using HDMI or a Audio hat you can use backlight PWM control for the second screen
#If you are using Jack output please leave commented.
#The Screen will be switch down in Sleep mode, and switch on when it wakes up.
#On OGST, only this mode is available.

;system.secondminitft.backlightcontrol=18
```

Normally, everything is OK. Reboot and your screen should display the images and videos of the scraped games.

## Adafruit TFT Screen 3.5"

You can get this screen [here](https://www.adafruit.com/product/2097) and it is quite expensive.

### Wiring Chart

You can see the wiring chart on [this page](https://pinout.xyz/pinout/pitft_plus_35).

### Configuration

The configuration procedure is identical to the 2.8" Arduino screen but the parameters are different.

The support of the second mini TFT screen is integrated in recalbox 7.0. It is still necessary to configure the Adafruit mini TFT screen specifically. To do this, you need to modify the `/boot/config.txt` file as shown below to add support for the screen via an overlay.

#### Set up

You need to open the file [recalbox-user-config.txt](./../../system/modification/configtxt-configuration) with nano or vim and add the following lines:

```ini
#minitft 3.5 adafruit
dtparam=spi=on
dtoverlay=pitft35-resistive,rotate=270,fps=30
```

The SPI bus support is enabled and the display is configured via the appropriate overlay. Now you just have to configure the [recalbox.conf] file (./../../../basic-usage/getting-started/recalboxconf-file) to enable the display support. To do this, configure the options like this:

```ini
#Second miniTFT screen as marquee to display scrapes and videos
#Enable it with system.secondMiniTFT.enabled=1, disable with system.secondMiniTFT.enabled=0
 
system.secondminitft.enabled=1
 
#Type of mini TFT : OGST, unknown
#for Support of XU4 OGSTCase mini screen, just configure it secondminitft.type=ogst
#Most of spi tft screen are enabled thanks to overlays in /boot/config.txt
#please check the specific configuration of your screen
#Some examples are available on the page .....
# values are :
# - overlay : screen configured through overlays in /boot/config.txt
# - ogst : screen of the XU4 OGST case. Supported out of the box
# - default : the rest of the world
 
system.secondminitft.type=overlay
 
#Choose the resolution of your screen
#miniTFT resolution  320x240 -> 240p, 480x320 -> 320p
 
system.secondminitft.resolution=320p

#Scraped image aspect on tft screen : fbv display option
# options available : stretch, ignore-aspect, enlarge.
# stretch : Strech (using a simple resizing routine) the image to fit onto screen if necessary
# alpha : use the alpha channel (if applicable)
# ignore-aspect : Ignore the image aspect while resizing
# enlarge : Enlarge the image to fit the whole screen if necessary

# 1 = enabled, 0 = disabled

system.secondminitft.imagestretchenabled=1
system.secondminitft.imageenlargeenabled=0
system.secondminitft.imagealphaenabled=1
system.secondminitft.imageignoreaspectenabled=1

#When activating the second TFT screen, you may want to display
#game scraped video only on the second screen. This variable
#allows you to disable scraped video playing in ES during game browsing
#system.secondminitft.disablevideoines=1 disable video playing in ES
#system.secondminitft.disablevideoines=0 enable video playing in ES

system.secondminitft.disablevideoines=1

#if the backlight of your your screen can be manage by PWM, 
#indicates the  GPIO value of your PWM control
#WiringPi and its gpio program will be used to control the backlight
#when RB goes in SLEEP mode, and when it wakes up.
#On raspberry pi, PWMs cannot be used simultaneously with Jack sound output.
#If your are using HDMI or a Audio hat you can use backlight PWM control for the second screen
#If you are using Jack output please leave commented.
#The Screen will be switch down in Sleep mode, and switch on when it wakes up.
#On OGST, only this mode is available.

;system.secondminitft.backlightcontrol=18
```

Normally, everything is OK, restart and your screen should display images and videos of scraped games

## ODROID Game Station Turbo (OGST XU4)

### What is the OGST TFT screen?

https://www.hardkernel.com/shop/ogst-gaming-console-kit-for-xu4/

With the OGST box, You have to modfiy two configuration files: 
* `SHARE:\system\recalbox.conf` (`/recalbox/share/system/recalbox.conf` under SSH) and 
* `RECALBOX\config.ini` (`/boot/config.ini` under SSH).

### Configuration

1) Optionally, to do only on upgraded systems that was <= 7.2.1. Other freshly flashed systems (>= 7.2.2), you can go directly to step 2. 
You will need to copy 2 files before proceeding to the next step: in the RECALBOX partition, copy the `boot.ini.sample` file to `boot.ini`. Take care to check the screen configuration (the lines that start with setenv). In general, it is sufficient to remove the # in front of the resolution you want. (Note that it is possible to comment out all the setenv videoconfig options, the XU4 will take the best resolution by itself).  
Also copy the `config.ini.sample` file to `config.ini`, still in RECALBOX partition (`/boot`).
Here are the commands you can execute under SSH to achieve this:
```
mount -o remount,rw /boot
cp /boot/boot.ini.sample /boot/boot.ini
cp /boot/config.ini.sample /boot/config.ini
```

2) Now, you have to enable OGST overlay. For this configuration, you have to open the `config.ini` file which is in the RECALBOX partition.

Once opened, find the following line:

```ini
;overlay_profile=hktft_cs_ogst
```
and remove the `;` in front of the line and save. You must have this **exact** same file:

```ini
[generic]
; Device Tree Overlay
overlay_resize=16384
; uncomment next line to enable Odroid XU4 OGST
overlay_profile=hktft_cs_ogst
overlays="spi0 i2c0 i2c1 uart0"
 
[overlay_hktft_cs_ogst]
overlays="hktft-cs-ogst i2c0"
```
You can achieve this with the following commands:
```
mount -o remount,rw /boot
sed -i '/overlay_profile=hktft_cs_ogst/s/^;//' /boot/config.ini
```

3) Now some simple modifications of the recalbox.conf file has to be done and this is possible by accessing the SD card from Windows, byt the web manager or directly from SSH.
The configuration is done with the following lines in the file [recalbox.conf](./../../../basic-usage/getting-started/recalboxconf-file):

```ini
#Second miniTFT screen as marquee to display scrapes and videos
#Enable it with system.secondMiniTFT.enabled=1, disable with system.secondMiniTFT.enabled=0

#this configuration is REQUIRED 
system.secondminitft.enabled=1
 
#Type of mini TFT : OGST, unknown
#Most of spi tft screen are enabled thanks to overlays in /boot/config.txt in rpi and /boot/config.ini on odroidxu4
#please check the specific configuration of your screen
#Some examples are available on the page .....
# values are :
# - overlay : screen configured through overlays in /boot/config.txt or /boot/config.ini (odroidxu4/ogst)
# - default : the rest of the world
#this configuration is REQUIRED if system.secondminitft.enabled=1
system.secondminitft.type=overlay
 
#Choose the resolution of your screen
#miniTFT resolution  320x240 -> 240p, 480x320 -> 320p

#this configuration is REQUIRED if system.secondminitft.enabled=1
system.secondminitft.resolution=240p

#Scraped image aspect on tft screen : fbv display option
# options available : stretch, ignore-aspect, enlarge.
# stretch : Strech (using a simple resizing routine) the image to fit onto screen if necessary
# alpha : use the alpha channel (if applicable)
# ignore-aspect : Ignore the image aspect while resizing
# enlarge : Enlarge the image to fit the whole screen if necessary

# 1 = enabled, 0 = disabled

#this configuration is REQUIRED if system.secondminitft.enabled=1
system.secondminitft.imagestretchenabled=1
system.secondminitft.imageenlargeenabled=0
system.secondminitft.imagealphaenabled=1
system.secondminitft.imageignoreaspectenabled=1

#When activating the second TFT screen, you may want to display
#game scraped video only on the second screen. This variable
#allows you to disable scraped video playing in ES during game browsing
#system.secondminitft.disablevideoines=1 disable video playing in ES
#system.secondminitft.disablevideoines=0 enable video playing in ES

#this configuration is OPTIONAL if system.secondminitft.enabled=1
;system.secondminitft.disablevideoines=1

#if the backlight of your your screen can be manage by PWM, 
#indicates the  GPIO value of your PWM control
#WiringPi and its gpio program will be used to control the backlight
#when RB goes in SLEEP mode, and when it wakes up.
#On raspberry pi, PWMs cannot be used simultaneously with Jack sound output.
#If your are using HDMI or a Audio hat you can use backlight PWM control for the second screen
#If you are using Jack output please leave commented.
#The Screen will be switch down in Sleep mode, and switch on when it wakes up.
#On OGST, only this mode is available.

#this configuration is OPTIONAL if system.secondminitft.enabled=1
;system.secondminitft.backlightcontrol=18

```

4) Finally, reboot the system.

### Troubleshooting Odroid XU4 OGST

#### Screen does not turn on

First, check both `/boot/config.ini` and `recalbox.conf`. Both files in boot partition **must** be called `boot.ini` and `config.ini` not `boot.ini.sample` and `config.ini.sample`. (Windows may hide the file extension).

If the XU4 boots from eMMC, you may have to update MMC boot partition. Execute those commands to achieve that:
```
echo 0 > /sys/block/mmcblk0boot0/force_ro
dd if=/dev/mmcblk0 of=/dev/mmcblk0boot0 bs=512 count=8192 skip=1
echo 1 > /sys/block/mmcblk0boot0/force_ro
reboot
```
