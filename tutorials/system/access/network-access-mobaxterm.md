---
title: Accès réseau via MobaXTerm
description: 
published: true
date: 2021-09-04T21:47:20.225Z
tags: mobaxterm, accès, réseau
editor: markdown
dateCreated: 2021-05-31T08:55:57.492Z
---

Avec MobaXTerm, vous pouvez :

* Effectuer une connexion SFTP
* Utiliser les lignes de commandes SSH

## Pré-requis

Vous devez télécharger [MobaXTerm](https://mobaxterm.mobatek.net/). Vous n'êtes pas obligé de l'installer, il existe une version portable.

## Configuration

### Connexion via SFTP

* Lancez MobaXTerm pour vous retrouver devant cette fenêtre.

![](/tutorials/system/access/mobaxterm1.png){.full-width}

* Cliquez sur `Session` en haut de la fenêtre.
* Dans cette fenêtre, cliquez sur `SFTP` pour vous retrouver face à cette fenêtre.

![](/tutorials/system/access/mobaxterm2.png){.full-width}

* Dans le champ `Remote host`, indiquez [l'adresse IP](./../../../tutorials/network/ip/discover-recalbox-ip) de votre Recalbox, et dans le champ `Username`, indiquez `root` et cliquez sur OK en bas.
* Vous allez avoir une fenêtre vous demandant le mot de passe pour vous connecter.

![](/tutorials/system/access/mobaxterm3.png){.full-width}

* Dans le champ visible, indiquez `recalboxroot` et cliquez sur OK. Vous pouvez, dans la fenêtre suivante, demander à enregistrer le mot de passe pour vous connecter à cette session.

>Vous allez avoir une fenêtre vous demandant de créer un mot de passe maître. Celui-ci vous permettra d'enregistrer toutes les connexions effectuées dans MobaXTerm.
{.is-info}

* Une fois toutes les fenêtres validées, vous serez connecté.

![](/tutorials/system/access/mobaxterm4.png){.full-width}

>Une fois que vous serez connecté une fois, la connexion sera enregistrée pour pouvoir vous y connecter plus facilement plus tard.
{.is-success}

### Connexion via SSH

Une fois que votre connexion est enregistrée, vous pouvez créer une autre connexion pour vous connecter via SSH.

* Dans la partie de gauche, faites un clic droit sur votre connexion et sélectionnez `Duplicate session`.
* Sur la connexion dupliquée, faites un clic droit dessus et choisissez `Edit session`.
* En haut de la nouvelle fenêtre, cliquez sur `SSH`.
* Renseignez la même adresse IP, cochez la case `Specify username` et indiquez le même nom d'utilisateur (à savoir `root`), puis cliquez sur OK.
* Votre connexion SSH est crée.

![](/tutorials/system/access/mobaxterm5.png){.full-width}

## Utilisation

Quand vous êtes connecté via SFTP, vous pouvez voir :

* Les fichiers sur votre PC d'un côté,
* Les fichiers sur votre Recalbox de l'autre côté.

Par défaut, vous serez connecté à l'emplacement `/recalbox/share/system`.