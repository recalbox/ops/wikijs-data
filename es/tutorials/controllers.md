---
title: 🎮 Controladores
description: Mandos de juego, tambien llamados controladores
published: true
date: 2024-07-26T06:47:19.815Z
tags: tutorial, controladores, mandos
editor: markdown
dateCreated: 2023-09-01T09:22:59.954Z
---

Toda esta sección y sus apartados están consacrados a los mandos y otros sticks.

Aquí tenéis la lista de categorías disponibles:

[Codificadores USB](usb-encoders)
[Configuración de botones personalizados](configuration-of-customized-buttons)
[Configuración/Test de mandos](test-configuration)
[GPIO](gpio)
[Mandos](controllers)