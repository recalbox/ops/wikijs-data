---
title: Durante el juego
description: Al jugar, cuando el emulador lo permite,  podéis ejecutar comandos especiales para mejorar vuestra experiencia lúdica
published: true
date: 2024-11-17T15:30:19.873Z
tags: comandos especiales, juego
editor: markdown
dateCreated: 2023-09-03T13:54:59.900Z
---

>En Recalbox, los atajos de comandos se relizan en la mayoría de los casos mediante combinaciones de botones o de movimientos del joystick que implican casi siempre un botón llamado **HOTKEY** (`HK`), que debe configurarse al configurar el mando desde la interfaz de Recalbox.
>
>Los nombres que vamos a utilizar para los botones son los nombres utilizados por el controlador Super Nintendo estandard (los botones `L1`, `R1`, `L2`, `R2`, `L3` y `R3` que se encuentran presentes en cualquier mando reciente).
>
>Consultad [esta página](./../../../basic-usage/first-use-and-configuration#configuration-dune-manette) para obtener más información.
{.is-info}

![](/basic-usage/getting-started/special-commands/controllers.png)

## Comandos de base

### Volver a la lista de juegos [Back to Gamelist]

La siguiente combinación interrumpe el juego en curso y vuelve a la interfaz de EmulationStation a la lista de juegos:

* `HK` + `START`

### Añadir créditos [Insert Coin].

El siguiente botón añade un crédito en los juegos de Arcade y en los juegos NeoGeo:

* `SELECT`

### Reiniciar el juego [Reset]

Reinicia el juego en curso:

* `HK` + `A`

### Rebobinar [Rewind]

Hace que el juego vuelva atrás en el tiempo:

* `HK` + `GAUCHE`

### Acelerar [Fast Forward]

Avanza el juego más rápido que el ritmo normal:

* `HK` + `DROITE`

## Grabación

### Crear una grabación instantánea [Save State]

Crea una grabación instantánea de la memoria del emulador en el emplazamiento actual:

* `HK` + `Y`

### Cargar una grabación instantánea [Load State]

Carga la grabación instantánea más reciente del emplazamiento actual:

* `HK` + `X`

### Cambiar el emplazamiento de grabaciones [Select Slot]

Selecciona un lugar para las grabaciones entre los emplazamientos disponibles (se encuentran enumerados comenzando por 0)

* `HK` + `ARRIBA` (incrementa) o `ABAJO` (decrementa) 

## Cambiar el CD

### Eyectar el CD [Disk Eject]

Simula la salida o la entrada de un CD en la consola emulada (función útil para los juegos que necesitan varios CD o discos):

* `HK` + `STICK IZQ.` hacia _ARRIBA_

### Cambiar de CD [Disk Swap]

>Para poder cambiar de disco de esta forma, todas vuestras imágenes deben encontrarse dentro de un fichero `.M3U`.
{.is-info}

Selecciona el disco precedente o el disco siguiente (función útil para los juegos compuestos de varios CD o discos):

* `HK` + `STICK IZQ.` hacia la _IZQUIERDA_ o hacia la _DERECHA_

>En resumen, un cambio de CD se puede realizar en 3 etapas:
>
>1. _**Pulso Eject y la caja del CD se abre:**_
>   * `HK` + `STICK IZQ.` hacia _ARRIBA_ 
>2. _**Cambio de CD (-1, -2, ... o +1, +2, ...) :**_
>   * `HK` + `STICK IZQ.` hacia la _IZQUIERDA_ o hacia la _DERECHA_
>3. _**Pulso Eject otra vez, y la caja del CD se cierra y el juego continúa:**_
>   * `HK` + `STICK IZQ.` hacia _ARRIBA_ 
{.is-success}

## Presentación

### Capturas de pantalla [Screenshot]

Para realizar una captura de pantalla en juego, con la siguiente combinación se grabará un fichero .png dentro del directorio `screenshots`:

* `HK` + `L1`

### Grabación de un vídeo [Recording]

Como en el caso anterior, con esta combinación se grabará un vídeo dentro de la carpeta `screenshots`:

* `HK` + `R3` (`R3` equivale a una presión sobre el joystick `Stick DCH.`)

>La primera presión arranca la grabación del vídeo y la segunda lo interrumpe.
{.is-info}

### Cambiar de shader [Shaders Swap]

Para cambiar el filtro de vídeo (shader) que se utiliza durante el juego:

* `HK` + `L2` o `R2`

### Traducción [Translate]

Traducir en tiempo real mediante una image que se imprime por encima de la imagen del juego:

* `HK` + `STICK IZQ.` hacia _ABAJO_

>La traducción necesita una conexión a Internet para su funcionamiento. Para obtener más información consulta la página [Retroarch AI (Traducción durante el juego)](./../../../basic-usage/features/retroarch-ai-ingame-translation)
{.is-warning}

### Mostrar los FPS [FPS Display]

Para mostrar en pantalla los FPS (numéro de imágenes por segundo) en cada instante:

* `HK` + `STICK DCH.` hacia _ABAJO_

## Usuarios avanzados

### Acceso al menú de RetroArch [RetroArch Menu]

Para acceder al menú de RetroArch:

* `HK` + `B`

>Si queréis modificar y grabar las modificaciones realizadas en RetroArch, podéis activar la opción "**Save settings on Exit**" dentro de su menú.
>Una vez activada esta opción, **todas las modificaciones** realizadas dentro de los menús de RetroArch se grabarán automáticamente al salir del menú.
{.is-info}

>No obstante, para evitar problemas a largo plazo y para no perder las modificaciones realizadas en caso de reinstalación de Recalbox o cuando se ejecuta la opción de "aplicar las opciones de fábrica", se aconseja no modificar las opciones de RetroArch directamente desde los menús de RetroArch y utilizar en su lugar las [sobrecargas de configuración de Recalbox](/advanced-usage/configuration-override).
{.is-warning}

## Retroflag GPi Case

![](/basic-usage/getting-started/special-commands/gpicase.png)