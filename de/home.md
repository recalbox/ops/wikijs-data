---
title: Willkommen im Recalbox-Wiki!
description: 
published: true
date: 2024-06-02T16:53:23.359Z
tags: 
editor: markdown
dateCreated: 2021-06-21T11:41:40.897Z
---

![welcome.png](/home/welcome.png)

## Was ist Recalbox?

[Recalbox](https://www.recalbox.com) ist die **ultimative Retro-Gaming-Konsole**, mit der du alle Konsolen- und Computerspiele aus deiner Kindheit wieder spielen kannst.

Mit [Recalbox](https://www.recalbox.com) kannst du die Titel, die die Welt der Videospiele geprägt haben, ganz einfach (wieder)entdecken und (wieder) spielen!

[Recalbox](https://www.recalbox.com) kann einfach auf preiswerten Mikrocomputern wie dem Raspberry Pi und sogar auf deinem PC (modern oder nicht) installiert werden!

## Ich möchte Recalbox installieren! Wo fange ich an?

Um Recalbox zu installieren, gehst du auf die Seite [Vorbereitung und Installation](./basic-usage/preparation-and-installation).

Um mehr über unterstützte Systeme zu erfahren, besuche den Abschnitt [Hardware-Kompatibilität](./hardware-compatibility).

## Ich suche eine bestimmte Seite!

Nutze die Suchfunktion oben auf der Seite!
Du kannst entweder deine Suche in das Feld eingeben...
![search-text.png](/home/search-text.png)
oder du kannst nach Tags suchen.
![search-tags.png](/home/search-tags.png)

## Ich bin ein Newbie, wo soll ich anfangen?
* Wir empfehlen dringend, die Dokumentation zu lesen:
  * [Grundlegende Verwendung](./basic-usage), um die **Grundlagen zur Verwendung** von [Recalbox](https://www.recalbox.com) zu verstehen.
  * [Emulator-Kompatibilität](./hardware-compatibility/emulators-compatibility).
  * [Gerätekompatibilität](./hardware-compatibility/compatible-devices).
  * Rufe den Abschnitt [Emulatoren](./emulators) auf, um mehr Informationen über die in [Recalbox](https://www.recalbox.com) vorhandenen zu erhalten.
  * Wenn einige **Begriffe** für dich **zu technisch** sind, kann das [Glossar](./basic-usage/glossary) ein **wertvolles Hilfsmittel** sein.  
* Für die fortgeschrittene Nutzung von Recalbox:
  * Schlage in den [Anleitungen](./tutorials) nach.
  * Besuche den Abschnitt [Fortgeschrittene Benutzung](./advanced-usage).
* Du kannst auch die folgenden Abschnitte für zusätzliche Informationen überprüfen: 
  * Die Recalbox [Webseite](https://www.recalbox.com).
  * Das [Forum](https://forum.recalbox.com/).
  * Das [Discord](https://discord.gg/NbQFbGM).
  
## Irgendwelche Ratschläge für einen neuen Benutzer?

[Recalbox](https://www.recalbox.com) ist ein **Open-Source**-Projekt und zudem **kostenlos**!

Die Leute, die daran arbeiten, tun dies in ihrer Freizeit, so dass sich das Projekt und seine Dokumentation so schnell weiterentwickeln, wie Leute dazu beitragen.

Der **beste Rat**, den wir dir geben können, ist:

* **sei wissbegierig**
* **lies die Dokumentation**
* **lies die [Anleitungen](./tutorials).**
* **trage zum Wiki bei. Sei es durch das Erstellen von Inhalten oder das Übersetzen** (siehe [Zum Recalbox Wiki beitragen](/de/contribute))
* **Suche auch im [Forum](https://forum.recalbox.com/) nach Antworten**
* **Mache immer ein Backup** deines **share** Ordners, bevor du etwas an deiner Recalbox machst, dessen du dir nicht sicher bist.

  
  
  