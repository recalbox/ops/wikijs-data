---
title: DOS
description: 
published: true
date: 2025-03-22T00:55:06.437Z
tags: dos, msdos, pc
editor: markdown
dateCreated: 2021-08-07T23:42:16.347Z
---

You will have here some tutorials about DOS.

Here are the available tutorials:

[DOSBox to play with DOS games](dosbox-to-play-with-dos-games)
[Mouse use with controller in DOSBox Pure](mouse-use-with-controller-in-dosbox-pure)
[Play Windows 95 & 98 games with DOSBox Pure](play-win-95-98-games-dosbox-pure)