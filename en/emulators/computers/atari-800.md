---
title: Atari 800
description: 
published: true
date: 2021-08-09T14:18:14.186Z
tags: atari, computers, 800, atari-800
editor: markdown
dateCreated: 2021-07-28T20:01:50.147Z
---

![](/emulators/computers/atari800.svg){.align-center}

## Technical data

* **Manufacturer**: Atari, Inc.
* **Year of release**: 1979
* **OS**: Atari OS
* **CPU**: MOS Technology 6502B @ 1.8 Mhz
* **RAM**: 8kB base, 48 kB max.
* **ROM**: 8KB (Atari Basic)
* **Sound chip**: 4× oscillators with noise mixing
* **Display**: 320x192 monochrome, 160x96 with 128 colors

## Presentation

In December 1978, **Atari** introduced the **Atari 400** and **800**. These computers were not available until late 1979 due to production problems.

The Atari **800** is an 8 bit computer based on the **MOS Technology 6502A** clocked at 7.70 MHz and can hold up to 48Kb of RAM. It uses a standard QWERTY keyboard with 62 classical keys and 4 special keys on the right side of the keyboard.

It stood out from other computers thanks to its graphic and sound capabilities. It was capable of displaying 128 colors on the screen using the **CTIA** graphics processor and up to 256 colors with the superior **GTIA** graphics processor on later versions of the computer. The 400 was the first among computers to be able to simultaneously display 4 programmable objects on the screen called "Player-missiles" (also known as "Sprints" on **Commodore** computers). At that time, most computers displayed only monochrome or 8-color screens in a primitive way. Graphics were supported by a custom chip called "**ANTIC**" (**CTIA/GTIA**). This chip was designed to function as a co-processor to take the workload of the main processor to display the graphics and colors on the screen.

The team that developed the custom chips in the **400** and **800** was managed by **Jay Miner** who, after leaving Atari, managed the teams that developed the custom chips surrounding the **Motorola MC68000** processor that would provide the most advanced computer of the time, the **Amiga 1000**!

The sound was provided by another chip called "**POKEY**" and produced 4 voices to provide a more realistic sound compared to other computers on the market. You could connect a tape drive, a 5 1/4" disk drive, a modem or a printer in series via special 13-pin D-type sockets. The **Atari 800** could also receive up to 4 9-pin D-type joysticks on the front ports of the machine. The joysticks used were the standard type used on the **Atari 2600 VCS**.

Although the 800 could be connected to a standard TV with the antenna cable, it included a 5-pin DIN connector on the right side to connect a high-resolution color monitor.

The computer was originally released with 16K of RAM, but could be easily upgraded to 48K by adding 16K RAM modules in the slots provided under the cover that could be removed on top.

The Atari 800 was one of the few computers that did not use a version of Microsoft's BASIC but used an in-house version, making it more difficult to convert programs written in BASIC. For some strange reason, the **BASIC** was not included in the **ROM** but had to be loaded by installing a cartridge in one of the 2 slots under the trapdoor on the front of the machine. It must be inserted in the left slot as most cartridges did on the **800**. Some cartridges only used the right slot. Also, the cartridges for the **Atari VCS** could not be inserted there. The **800** had a built-in RF modulator, no extra cost, that could be plugged directly into any TV.

## Emulators

[Libretro Atari800](libretro-atari800)