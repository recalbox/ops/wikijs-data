---
title: PSP
description: 
published: true
date: 2021-08-09T13:00:50.830Z
tags: psp, sony, handhelds
editor: markdown
dateCreated: 2021-07-06T09:11:16.709Z
---

![](/emulators/handheld/psp.svg){.align-center}

## Technical data

* **Manufacturer**: Sony Computer Entertainment
* **Year of release**: 2004
* **Units sold**: 80.82 millions
* **Best-selling game** : GTA Liberty City Stories
* **CPU**: 32-bit MIPS RS4000 running at 333 Mhz
* **RAM**: 32 Mbits
* **DRAM**: 4 Mbits
* **Resolution**: 480x272 pixels
* **Size**: 170x74x23 mm
* **Weight**: 260 g

## Presentation

The **PlayStation Portable**, or (**PSP**) is Sony's first portable video game console, released between 2004 and 2014. It marks a turning point in the history of portable consoles, as it offers image quality and general capabilities approaching those of the PlayStation 2, a home console.

The PlayStation Portable allows you to play video games, watch videos and pictures, listen to music and offers the ability to browse the Internet. Its Wi-Fi wireless connection system allows up to sixteen machines to communicate with each other at the same time, thanks to the "ad hoc networking" function.

## Emulators

[PPSSPP](ppsspp)