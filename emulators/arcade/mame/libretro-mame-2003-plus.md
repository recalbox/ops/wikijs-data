---
title: Libretro MAME 2003 Plus
description: 
published: true
date: 2024-07-24T06:58:47.720Z
tags: libretro, mame, mame2003+, mame2003plus
editor: markdown
dateCreated: 2021-07-05T23:26:19.800Z
---

**Libretro MAME2003_Plus** (also called MAME 2003+ and mame2003-plus) is a **Libretro** arcade system emulator core that emphasizes high performance and broad compatibility with mobile devices, single board computers, embedded systems and similar platforms.

In order to take advantage of the lower performance and hardware requirements of an earlier MAME architecture, MAME 2003-Plus started with the MAME 2003 code base, itself derived from MAME 0.78.

On this basis, MAME 2003-Plus contributors retro ported support for several hundred additional games, as well as other features not originally present in MAME 0.78.

## ![](/emulators/license.svg) License

This core is under [**MAME non-commercial**](https://github.com/libretro/mame2003-plus-libretro/blob/master/LICENSE.md) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Features

| Feature | Supported |
| :---: | :---: |
| Saves | ✔ |
| States | Game-dependent |
| Rewind | ✔ |
| Netplay | ✔ |
| Native Cheats | ✔ |
| Controllers | ✔ |
| Multi-Mouse | ✔ |

## ![](/emulators/bios.svg) BIOS

BIOS romsets are not required when using "Full Non-Merged" arcade romsets. For "Split" and "Non-Merged" romsets, place the BIOS in the same directory as the game romset.

>**Note**:
>Place the bios in the directory: `/recalbox/share/roms/mame/`   
>or in its subdirectory if you use several Mame cores: `/recalbox/share/roms/mame/Mame2003-Plus/`
>Games and their bios must be in the same directory or sub-directory.
{.is-warning}

## ![](/emulators/roms.png) Roms

* Based on romset: 0.78-0.188 (MAME 0.78 as baseline with others ROMs based on higher MAME romsets)
* DatFile: You can find the dat file in your recalbox at location `/recalbox/share/arcade/libretro/mame2003-plus.dat`.

### Location

Put your roms like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 mame
┃ ┃ ┃ ┃ ┣ 🗒 **game.zip**
┃ ┃ ┃ ┃ ┣ 📁 **game**
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **game.chd**

You can also opt for a subdirectory (useful if you want to have a mame set for another core).

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 mame
┃ ┃ ┃ ┃ ┣ 📁 Mame 2003-Plus
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **game.zip**
┃ ┃ ┃ ┃ ┃ ┣ 📁 **game**
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 **game.chd**

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Accessing to options

You can configure various options in two different ways.

* Via RetroArch menu:

┣ 📁 RetroArch Menu
┃ ┣ 📁 Core options
┃ ┃ ┣ 🧩 Name_option

* Via the file `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core options

| Options | Options values | Variables (for configuration override) | Variables values |
| --- | --- | --- | --- |
| Skip Disclaimer | `Disabled` ✅ / `Enabled` | `mame2003-plus_skip_disclaimer` | `disabled` / `enabled` |
| Skip Warnings | `Disabled` ✅ / `Enabled` | `mame2003-plus_skip_warnings` | `disabled` / `enabled` |
| Display MAME Menu | `Disabled` ✅ / `Enabled` | `mame2003-plus_display_setup` | `disabled` / `enabled` |
| Legacy Remapping | `Enabled` ✅ / `Disabled` | `mame2003-plus_mame_remapping` | `enabled` / `disabled` |
| Autosave Hiscore | `default` ✅ / `recursively` / `Disabled` | `mame2003-plus_autosave_hiscore` | `default` / `recursively` / `disabled` |
| Locate System Files Within a Subfolder | `Enabled` ✅ / `Disabled` | `mame2003-plus_core_sys_subfolder` | `enabled` / `disabled` |
| Locate Save Files Within a Subfolder | `Enabled` ✅ / `Disabled` | `mame2003-plus_core_save_subfolder` | `enabled` / `disabled` |
| X-Y Device | `mouse` ✅ / `pointer` / `lightgun` / `Disabled` | `mame2003-plus_xy_device` | `mouse` / `pointer` / `lightgun` / `disabled` |
| Input Interface | `simultaneous` ✅ / `retropad` / `keyboard` | `mame2003-plus_input_interface` | `simultaneous` / `retropad` / `keyboard` |
| Show Lightgun Crosshairs | `Enabled` ✅ / `Disabled` | `mame2003-plus_crosshair_enabled` | `enabled` / `disabled` |
| Lightgun Crosshair Appearance | `simple` ✅ / `enhanced` | `mame2003-plus_crosshair_appearance` | `simple` / `enhanced` |
| Allow Input Button to Act as a Toggle Switch | `Enabled` / `Disabled` | `mame2003-plus_input_toggle` | `enabled` / `disabled` |
| Center Joystick Axis for Digital Controls | `Enabled` ✅ / `Disabled` | `mame2003-plus_digital_joy_centering` | `enabled` / `disabled` |
| Use Samples | `Enabled` ✅ / `Disabled` | `mame2003-plus_use_samples` | `enabled` / `disabled` |
| Sample Rate | `8000 KHz` / `11025 KHz` / `22050 KHz` / `30000 KHz` / `44100 KHz` / `48000 KHz` ✅ | `mame2003-plus_sample_rate` | `8000` / `11025` / `22050` / `30000` / `44100` / `48000` |
| Brightness | `0.2` / `0.3` / `0.4` / `0.5` / `0.6` / `0.7` / `0.8` / `0.9` / `1.0` ✅ / `1.1` / `1.2` / `1.3` / `1.4` / `1.5` / `1.6` / `1.7` / `1.8` / `1.9` / `2.0` | `mame2003-plus_brightness` | `0.2` / `0.3` / `0.4` / `0.5` / `0.6` / `0.7` / `0.8` / `0.9` / `1.0` / `1.1` / `1.2` / `1.3` / `1.4` / `1.5` / `1.6` / `1.7` / `1.8` / `1.9` / `2.0` |
| Gamma Correction | `0.5` / `0.6` / `0.7` / `0.8` / `0.9` / `1.0` ✅ / `1.1` / `1.2` / `1.3` / `1.4` / `1.5` / `1.6` / `1.7` / `1.8` / `1.9` / `2.0` | `mame2003-plus_gamma` | `0.5` / `0.6` / `0.7` / `0.8` / `0.9` / `1.0` / `1.1` / `1.2` / `1.3` / `1.4` / `1.5` / `1.6` / `1.7` / `1.8` / `1.9` / `2.0` |
| TATE Mode | `Disabled` ✅ / `Enabled` | `mame2003-plus_tate_mode` | `disabled` / `enabled` |
| Frameskip | `Disabled` ✅ / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `9` / `10` / `11` / `auto` / `auto aggressive` / `auto max` | `mame2003-plus_frameskip` | `disabled` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `9` / `10` / `11` / `auto` / `auto_aggressive` / `auto_max` |
| Display Artwork | `Enabled` ✅ / `Disabled` | `mame2003-plus_display_artwork` | `enabled` / `disabled` |
| Resolution Multiplier | `1` ✅ / `2` / `3` / `4` / `5` / `6` / `7` / `8` | `mame2003-plus_art_resolution` | `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` |
| Hardcoded Overlay Opacity | `default` ✅ / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `21` / `22` / `23` / `24` / `25` / `50` / `70` | `mame2003-plus_art_overlay_opacity` | `default` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `21` / `22` / `23` / `24` / `25` / `50` / `70` |
| Specify Neo Get BIOS | `default` ✅ / `euro` / `euro-s1` / `us` / `us-e` / `asia` / `japan` / `japan-s2` / `unibios40` / `unibios33` / `unibios20` / `unibios13` / `unibios11` / `unibios10` / `debug` / `asia-aes` | `mame2003-plus_` | `default` / `euro` / `euro-s1` / `us` / `us-e` / `asia` / `japan` / `japan-s2` / `unibios40` / `unibios33` / `unibios20` / `unibios13` / `unibios11` / `unibios10` / `debug` / `asia-aes` |
| CPU Clock Scale | `default` ✅ / `25%` / `30%` / `35%` / `40%` / `45%` / `50%` / `55%` / `60%` / `65%` / `70%` / `75%` / `80%` / `85%` / `90%` / `95%` / `100%` / `105%` / `110%` / `115%` / `120%` / `125%` / `200%` / `250` / `300` | `mame2003-plus_cpu_clock_scale` | `default` / `25` / `30` / `35` / `40` / `45` / `50` / `55` / `60` / `65` / `70` / `75` / `80` / `85` / `90` / `95` / `100` / `105` / `110` / `115` / `120` / `125` / `200` / `250` / `300` |

## ![](/emulators/external-links.png) External links

* **Progettosnaps**: [https://www.progettosnaps.net/](https://www.progettosnaps.net/)
* **Source code used**: [https://github.com/libretro/mame2003-plus-libretro/](https://github.com/libretro/mame2003-plus-libretro)
* **Libretro documentation**: [https://docs.libretro.com/library/mame2003\_plus/](https://docs.libretro.com/library/mame2003_plus/)