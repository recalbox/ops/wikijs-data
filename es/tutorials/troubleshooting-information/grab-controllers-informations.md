---
title: Recuperar las informaciones de vuestros mandos
description: Informaciones técnicas sobre vuestros controladores / mandos de juego
published: true
date: 2023-09-17T19:57:54.678Z
tags: controladores, mandos, informaciones
editor: markdown
dateCreated: 2023-09-17T19:57:54.678Z
---

Existen varios comandos que podéis utilizar para obtener informaciones técnicas sobre el funcionamiento de vuestros mandos, con el fin de solucionar problemas. Estos comandos se ejecutan todos [vía SSH](./../../tutorials/system/access/root-access-terminal-cli).

Mostrar todos los dispositivos de entrada :

```shell
cat /proc/bus/input/devices
```

Obtener informaciones sobre los periféricos USB:

```shell
lsusb -v
```

Obtener informaciones de los dispositivos de entrada que han sido identificados cuando se produce un evento:

```shell
for i in /dev/input/event*; do echo $i;udevadm info -q all -n $i;done
```

Obtener la lista de todos los botones/ejes/teclas encontrados (incluso cuando se trata de un teclado, no sólo para mandos):

```shell
for i in /dev/input/event*; do echo $i;(evtest $i) & ( evtestpid=$! && sleep 0.1 && kill -15 $evtestpid );done
```

Siempre que ejecutéis uno de estos comandos, es mejor no pegar todo el resultado en el foro o en nuestro Discord. En lugar de esto, utilizad algo como [http://pastebin.com/](http://pastebin.com/), pegad lo que os interesa después de ejecutar el comando y luego dad el enlace a las personas con las que os estáis comunicando.