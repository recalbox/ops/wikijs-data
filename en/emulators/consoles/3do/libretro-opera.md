---
title: Libretro Opera
description: 
published: true
date: 2021-10-05T22:43:37.187Z
tags: libretro, 3do, opera
editor: markdown
dateCreated: 2021-07-30T11:21:27.211Z
---

**Libretro Opera** is a Libretro core capable of emulating the Panasonic 3DO.

## ![](/emulators/license.svg) License

This core is under [**Modified GNU LGPL / Non-commercial**](https://github.com/libretro/opera-libretro/blob/master/libopera/opera_3do.c) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ❌ | ❌ | ✅ | ✅ | ✅ | ✅  🐌 | ✅ | ✅ |

🐌  Low performances but playable.

## ![](/emulators/features.png) Features

| Feature | Supported |
| :---: | :---: |
| Restart  | ✔ |
| Screenshots | ✔ |
| Saves | ✔ |
| States | ✔ |
| Rewind | ✔ |
| Core Options | ✔ |
| Controls | ✔ |
| Remapping | ✔ |

## ![](/emulators/bios.svg) BIOS

### List of mandatory bios

| Filename | Description | MD5 | Provided |
| :---: | :---: | :---: | :---: |
| panafz1.bin | Panasonic FZ-1 | f47264dd47fe30f73ab3c010015c155b | ❌ |
| panafz1j.bin | Panasonic FZ-1J | a496cfdded3da562759be3561317b605 | ❌ |
| panafz1j-norsa.bin | Panasonic FZ-1J \[RSA Patch\] | f6c71de7470d16abe4f71b1444883dc8 | ❌ |

### Location

Put the bios like this:

┣ 📁 recalbox  
┃ ┣ 📁 share  
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 🗒 **panafz1.bin**
┃ ┃ ┃ ┣ 🗒 **panafz1j.bin**
┃ ┃ ┃ ┣ 🗒 **panafz1j-norsa.bin**

### List of optional bios

| Nom de fichier | Description | MD5 | Fourni |
| :---: | :---: | :---: | :---: |
| panafz10.bin | Panasonic FZ-10 | 51f2f43ae2f3508a14d9f56597e2d3ce | ⚠️ |
| panafz10-norsa.bin | Panasonic FZ-10 \[RSA Patch\] | 1477bda80dc33731a65468c1f5bcbee9 | ⚠️ |
| panafz10e-anvil.bin | Panasonic FZ-10-E \[Anvil\] | a48e6746bd7edec0f40cff078f0bb19f | ⚠️ |
| panafz10e-anvil-norsa.bin | Panasonic FZ-10-E \[Anvil RSA Patch\] | cf11bbb5a16d7af9875cca9de9a15e09 | ⚠️ |
| goldstar.bin | Goldstar GDO-101M | 8639fd5e549bd6238cfee79e3e749114 | ⚠️ |
| sanyotry.bin | Sanyo IMP-21J TRY | 35fa1a1ebaaeea286dc5cd15487c13ea | ⚠️ |
| 3do_arcade_saot.bin | Shootout At Old Tucson | 8970fc987ab89a7f64da9f8a8c4333ff | ⚠️ |
| panafz1-kanji.bin | Panasonic FZ-1 (J) Kanji ROM. Useful to read japanese characters. | b8dc97f778a6245c58e064b0312e8281 | ⚠️ |
| panafz1j-kanji.bin | Panasonic FZ-1 (J) Kanji ROM. Useful to read japanese characters. | c23fb5d5e6bb1c240d02cf968972be37 | ⚠️ |
| panafz10ja-anvil-kanji.bin | Panasonic FZ-10 (J) Kanji ROM \[Anvil\]. Useful to read japanese characters. | 428577250f43edc902ea239c50d2240d | ⚠️ |

⚠️ Provided but it's not the correct bios.

### Location

Put the bios like this:

┣ 📁 recalbox  
┃ ┣ 📁 share  
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 🗒 **panafz10.bin**
┃ ┃ ┃ ┣ 🗒 **panafz10-norsa.bin**
┃ ┃ ┃ ┣ 🗒 **panafz10e-anvil.bin**
┃ ┃ ┃ ┣ 🗒 **panafz10e-anvil-norsa.bin**
┃ ┃ ┃ ┣ 🗒 **goldstar.bin**
┃ ┃ ┃ ┣ 🗒 **sanyotry.bin**
┃ ┃ ┃ ┣ 🗒 **3do_arcade_saot.bin**
┃ ┃ ┃ ┣ 🗒 **panafz1-kanji.bin**
┃ ┃ ┃ ┣ 🗒 **panafz1j-kanji.bin**
┃ ┃ ┃ ┣ 🗒 **panafz10ja-anvil-kanji.bin**

## ![](/emulators/roms.png) Isos

### Supported extensions

Isos must have the extension:

* .iso
* .chd
* .bin/.cue

### Location

Put the isos like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 **3do**
┃ ┃ ┃ ┃ ┣ 🗒 **game.iso**

>The isos using **Redump** format are highly recommended.
{.is-success}

>For more information about isos, go to [this tutorial](./../../../tutorials/games/generalities/isos-and-roms)!
{.is-info}

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Accessing to options

You can configure various options in two different ways.

* Via RetroArch menu:

┣ 📁 RetroArch Menu
┃ ┣ 📁 Core options
┃ ┃ ┣ 🧩 Name_option

* Via the file `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core options

| Options | Options values | Variables (for configuration override) | Variables values |
| --- | --- | --- | --- |
| BIOS (rom1) | `Panasonic FZ-1 (U)` / `Panasonic FZ-1 (J)` / `Panasonic FZ-1 (J) [No RSA]` / `Panasonic FZ-10 (U)` / `Panasonic FZ-10 (U) [No RSA]` / `Panasonic FZ-10 (E) ANVIL` / `Panasonic FZ-10 (E) ANVIL [No RSA]` / `Goldstar GDO-101M` / `Sanyo Try IMP-21J` / `3DO Arcade - SAOT` | `opera_bios` | `Panasonic FZ-1 (U)` / `Panasonic FZ-1 (J)` / `Panasonic FZ-1 (J) [No RSA]` / `Panasonic FZ-10 (U)` / `Panasonic FZ-10 (U) [No RSA]` / `Panasonic FZ-10 (E) ANVIL` / `Panasonic FZ-10 (E) ANVIL [No RSA]` / `Goldstar GDO-101M` / `Sanyo Try IMP-21J` / `3DO Arcade - SAOT` |
| Font (rom2) | `Disabled` / `Panasonic FZ-1 (J) Kanji ROM` / `Panasonic FZ-10 (J) ANVIL Kanji ROM` | `opera_font` | `disabled` / `Panasonic FZ-1 (J) Kanji ROM` / `Panasonic FZ-10 (J) ANVIL Kanji ROM` |
| CPU Overclock | `1.0x (12.50Mhz)` ✅ / `1.1x (13.75Mhz)` / `1.2x (15.00Mhz)` / `1.5x (18.75Mhz)` / `1.6x (20.00Mhz)` / `1.8x (22.50Mhz)` / `2.0x (25.00Mhz)` | `opera_cpu_overclock` | `1.0x (12.50Mhz)` / `1.1x (13.75Mhz)` / `1.2x (15.00Mhz)` / `1.5x (18.75Mhz)` / `1.6x (20.00Mhz)` / `1.8x (22.50Mhz)` / `2.0x (25.00Mhz)` |
| Mode | `NTSC 320x240@60` ✅ / `PAL1 320x288@50` / `PAL2 352x288@50` | `opera_region` | `ntsc` / `pal1` / `pal2` |
| VDLP Pixel Format | `0RGB1555` / `RGB565` / `XRGB8888` ✅ | `opera_vdlp_pixel_format` | `0RGB1555` / `RGB565` / `XRGB8888` |
| VDLP Bypass CLUT | `Disabled` ✅ / `Enabled` | `opera_vdlp_bypass_clut` | `disabled` / `enabled` |
| HiRes CEL Rendering | `Disabled` ✅ / `Enabled` | `opera_high_resolution` | `disabled` / `enabled` |
| MADAM Matrix Engine | `Hardware` ✅ / `Software` | `opera_madam_matrix_engine` | `hardware` / `software` |
| OperaOS SWI HLE | `Disabled` ✅ / `Enabled` | `opera_swi_hle` | `disabled` / `enabled` |
| Threaded DSP | `Disabled` ✅ / `Enabled` | `opera_dsp_threaded` | `disabled` / `enabled` |
| NVRAM Storage | `Per Game` ✅ / `Shared` | `opera_nvram_storage` | `per game` / `shared` |
| Active Input Devices | `0` / `1` ✅ / `2` / `3` / `4` / `5` / `6` / `7` / `8` | `opera_active_devices` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` |
| Timing Hack 1 (Crash'n Burn) | `Disabled` ✅ / `Enabled` | `opera_hack_timing_1` | `disabled` / `enabled` |
| Timing Hack 3 (Dinopark Tycoon) | `Disabled` ✅ / `Enabled` | `opera_hack_timing_3` | `disabled` / `enabled` |
| Timing Hack 5 (Microcosm) | `Disabled` ✅ / `Enabled` | `opera_hack_timing_5` | `disabled` / `enabled` |
| Timing Hack 6 (Alone in the Dark) | `Disabled` ✅ / `Enabled` | `opera_hack_timing_6` | `disabled` / `enabled` |
| Graphics Step Y Hack (Samurai Showdown) | `Disabled` ✅ / `Enabled` | `opera_hack_graphics_step_y` | `disabled` / `enabled` |
| Debug Output | `Disabled` ✅ / `Enabled` | `opera_kprint` | `disabled` / `enabled` |

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/libretro/opera-libretro/](https://github.com/libretro/opera-libretro)
* **Libretro documentation**: [https://docs.libretro.com/library/opera/](https://docs.libretro.com/library/opera/)
* **Compatibility list**: [http://wiki.fourdo.com/Compatibility_List](http://wiki.fourdo.com/Compatibility_List)