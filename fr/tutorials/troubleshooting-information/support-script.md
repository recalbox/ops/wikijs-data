---
title: Script de support
description: 
published: true
date: 2024-07-26T07:18:50.924Z
tags: script, support, logs, debug
editor: markdown
dateCreated: 2021-05-21T08:10:43.738Z
---

Un script nommé `recalbox-support.sh` a été ajouté afin d'aider les développeurs sur les problèmes que vous pouvez rencontrer avec vos périphériques.

Ce fichier donne les informations suivantes sur :

* joystick
* kodi
* lirc
* system (lsmod, lsusb, tv service, es_settings, recalbox.conf, recalbox.log,config, df, etc...)

## Pré-requis

Pour que l'archive de support soit d'utilité, il faut reproduire votre souci avec le [mode debug activé](../troubleshooting-information/enable-debug-mode). Il n'y a pas besoin de faire l'étape après le redémarrage. Après avoir activé le mode debug et après avoir redémarré, essayez de reproduire votre erreur.

## Utilisation

Son utilisation s'effectue via le Web manager.

* Rendez-vous dans le [Webmanager](./../../basic-usage/features/webmanager).
* Cliquez sur la roue crantée en bas à droite (en haut à droite sur mobile).
* Cliquez sur `Archive de support`.

Au bout d'un moment, un lien de téléchargement vous sera donné. Téléchargez l'archive et fournissez le fichier téléchargé dans un message adressé à la personne qui vous l'a demandé.

>N'oubliez pas de [desactiver le mode debug](../troubleshooting-information/enable-debug-mode) après avoir envoyé le fichier de support.
{.is-info}