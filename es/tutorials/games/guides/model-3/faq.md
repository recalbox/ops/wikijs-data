---
title: FAQ
description: 
published: true
date: 2024-07-29T09:45:07.242Z
tags: model3, faq
editor: markdown
dateCreated: 2024-07-29T09:43:55.698Z
---

## Error « Network board not present »

Para solucionar este error al iniciar algunos juegos como Daytona, sigue estos pasos:

Inicia el juego y presiona `L3` para acceder al menú de prueba.

![](/tutorials/games/guides/model-3/networkerror1.png)

Muévete con los botones `X` o `Y` hasta llegar a `GAME ASSIGNMENTS` y confirma con `A` o `B`.

![](/tutorials/games/guides/model-3/networkerror2.png)

Ubícate en `LINK ID` y confirma con `A` o `B` para cambiar de `MASTER` a `SINGLE`.

![](/tutorials/games/guides/model-3/networkerror3.png)

Vuelve a los menús anteriores hasta salir del menú de prueba.
