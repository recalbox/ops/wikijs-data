---
title: ZX Spectrum
description: 
published: true
date: 2024-07-26T07:08:48.331Z
tags: sinclair, zx, spectrum, juegos
editor: markdown
dateCreated: 2023-09-14T08:09:30.674Z
---

En esta sección encontraréis varias informaciones útiles para bien disfrutar de vuestro emulador ZX Spectrum.

Aqui tenéis la lista de tutoriales disponibles:

[FAQ](faq)