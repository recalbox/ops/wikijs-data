---
title: 5. 🛠️ EMULADORES
description: 
published: true
date: 2023-08-31T12:49:48.046Z
tags: emuladores
editor: markdown
dateCreated: 2021-09-21T23:56:23.925Z
---

Você chegou em uma parte um pouco mais sofisticada de toda a documentação, relativa a todos os sistemas de jogos incluídos no Recalbox:

* [Arcada](arcade)
* [Consoles](consoles)
* [Fantasia](fantasy)
* [Motores de jogos](engines)
* [Computadores](computers)
* [Portables](handhelds)
* [Ports](ports)

Aqui está a [lista de jogos incluídos](included-games) na última versão do Recalbox.