---
title: Displaying games in 3D
description: 
published: true
date: 2024-01-07T14:05:56.690Z
tags: nintendo, virtual boy, 3d
editor: markdown
dateCreated: 2024-01-07T14:05:56.690Z
---

## Displaying Games in 3D

### Introduction

The Virtual Boy is a console that allowed for depth perception through a 3D display using a lens system similar to what we find today in virtual reality headsets. It is possible to recreate this depth effect using two different techniques depending on the type of TV you use.

### Non-3D Compatible TV
If your TV is not natively 3D compatible, it is still possible to use anaglyph glasses (the famous red and blue glasses).

* To do this, launch a Virtual Boy game and access the RetroArch menu via the Hotkey + cross combination (or equivalent depending on your controller).
* Then go to the core options
* Then enter the 3D mode menu and select anaglyph
Put on your magnificent glasses and enjoy the joys of 3D gaming.

### 3D Compatible TV

If your TV is 3D compatible (whether you have active or passive glasses), it is possible to take advantage of its capabilities to display the Virtual Boy in 3D.

* To do this, launch a Virtual Boy game and access the RetroArch menu via the Hotkey + cross combination (or equivalent depending on your controller).
* Then go to the core options
* Then enter the 3D mode menu and select side by side
* Then go back until you see the settings menu in the left column
* Go to the video menu, scaling
* Enter aspect ratio and select custom

Put on your magnificent glasses, switch your TV to 3D mode and enjoy the joys of 3D gaming.