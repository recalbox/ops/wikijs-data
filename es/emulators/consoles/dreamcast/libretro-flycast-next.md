---
title: Libretro Flycast-Next
description: 
published: true
date: 2024-07-29T11:41:07.040Z
tags: libretro, dreamcast, 9.2+, flycast-next
editor: markdown
dateCreated: 2024-07-29T11:41:07.040Z
---

**Libretro Flycast** es un emulador **Sega Dreamcast multiplataforma** capaz de emular la **Dreamcast.**

## ![](/emulators/license.svg) Licencia

Este núcleo está bajo licencia [**GPLv2**](https://github.com/libretro/flycast/blob/master/LICENSE).

## ![](/emulators/compatibility.png) Compatibilidad

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ❌  | ❌  | ✅  | ✅  | ✅  | ✅  | ❌ | ✅ | ✅ |

## ![](/emulators/features.png) Funcionalidades

| Funcionalidad | Soportado |
| :---: | :---: |
| Reinicio | ✔ |
| Capturas de pantalla | ✔ |
| Guardados | ✔ |
| Opciones del núcleo | ✔ |
| Trucos RetroArch | ✔ |
| Controles | ✔ |
| Remapeo | ✔ |
| Vibración | ✔ |
| Control del disco | ✔ |

## ![](/emulators/bios.svg) BIOS

### Lista de bios obligatorios

| Nombre de archivo | Descripción | MD5 | Proporcionado |
| :---: | :---: | :---: | :---: |
| dc_boot.bin | BIOS Dreamcast | e10c53c2f8b90bab96ead2d368858623 d552d8b577faa079e580659cd3517f86 d407fcf70b56acb84b8c77c93b0e5327 | ❌  |
| dc_flash.bin | Fecha / Hora / Idioma | d6e11a23f1fa01cddb5dfccf7e4cc8d7 93a9766f14159b403178ac77417c6b68 0a93f7940c455905bea6e392dfde92a4 23df18aa53c8b30784cd9a84e061d008 69c036adfca4ebea0b0c6fa4acfc8538 74e3f69c2bb92bc1fc5d9a53dcf6ffe2 2f818338f47701c606ade664a3e16a8a | ❌  |

### Ubicación

Coloca los bios de la siguiente manera:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 dc
┃ ┃ ┃ ┃ ┣ 🗒 **dc_boot.bin**
┃ ┃ ┃ ┃ ┣ 🗒 **dc_flash.bin**

## ![](/emulators/isos.svg) Isos

### Extensiones soportadas

Las isos deben tener las siguientes extensiones:

* .bin/.cue
* .bin/.gdi
* .cdi
* .chd
* .elf
* .iso
* .m3u

### Ubicación

Coloca las isos de la siguiente manera:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 dreamcast
┃ ┃ ┃ ┃ ┣ 🗒 **juego.gdi**
┃ ┃ ┃ ┃ ┣ 🗒 **juego.bin**

>Se recomienda encarecidamente el uso de isos en formato **TOSEC**.
{.is-success}

>Para más información sobre las isos, visita [este tutorial](./../../../tutorials/games/generalities/isos-and-roms)!
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

>Para conservar tus configuraciones personalizadas durante una actualización, te recomendamos usar nuestra funcionalidad [Sobrecargas de configuración](./../../../advanced-usage/configuration-override).
{.is-info}

### Acceder a las opciones

Puedes configurar diversas opciones de dos maneras diferentes.

* A través del menú RetroArch:

┣ 📁 Menú RetroArch
┃ ┣ 📁 Opciones del núcleo
┃ ┃ ┣ 🧩 Name_option

* A través del archivo `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Opciones del núcleo

| Opciones | Valor de las opciones | Variable (para sobrecarga) | Valores de las variables |
| --- | --- | --- | --- |
| Región | `Japón` / `Estados Unidos` ✅ / `Europa` / `Por defecto` | `reicast_region` | `Japan` / `USA` / `Europe` / `Default` |
| Idioma | `Japonés` / `Inglés` ✅ / `Alemán` / `Francés` / `Español` / `Italiano` / `Por defecto` | `reicast_language` | `Japanese` / `English` / `German` / `French` / `Spanish` / `Italian` / `Default` |
| BIOS HLE (Reinicio requerido) | `Desactivado` ✅ / `Activado` | `reicast_hle_bios` | `disabled` / `enabled` |
| Iniciar en el BIOS (Reinicio requerido) | `Desactivado` ✅ / `Activado` | `reicast_boot_to_bios` | `disabled` / `enabled` |
| Activar DSP | `Desactivado` / `Activado` ✅ | `reicast_enable_dsp` | `disabled` / `enabled` |
| Forzar modo Windows CE | `Desactivado` ✅ / `Activado` | `reicast_force_wince` | `disabled` / `enabled` |
| Establecer juegos NAOMI en Free Play | `Desactivado` / `Activado` ✅ | `reicast_force_freeplay` | `disabled` / `enabled` |
| Emulación del adaptador de banda ancha | `Desactivado` ✅ / `Activado` | `reicast_emulate_bba` | `disabled` / `enabled` |
| Activar UPnP | `Desactivado` / `Activado` ✅ | `reicast_upnp` | `disabled` / `enabled` |
| Resolución interna | `320x240` / `640x480` ✅ / `800x600` / `960x720` / `1024x768` / `1280x960` / `1440x1080` / `1600x1200` / `1920x1440` / `2560x1920` / `2880x2160` / `3200x2400` / `3840x2880` / `4480x3360` / `5120x3840` / `5760x4320` / `6400x4800` / `7040x5280` / `7680x5760` / `8320x6240` / `8960x6720` / `9600x7200` / `10240x7680` / `10880x8160` / `11520x8640` / `12160x9120` / `12800x9600` | `reicast_internal_resolution` | `320x240` / `640x480` / `800x600` / `960x720` / `1024x768` / `1280x960` / `1440x1080` / `1600x1200` / `1920x1440` / `2560x1920` / `2880x2160` / `3200x2400` / `3840x2880` / `4480x3360` / `5120x3840` / `5760x4320` / `6400x4800` / `7040x5280` / `7680x5760` / `8320x6240` / `8960x6720` / `9600x7200` / `10240x7680` / `10880x8160` / `11520x8640` / `12160x9120` / `12800x9600` |
| Tipo de cable | `VGA` / `TV (RGB)` / `TV (Compuesto)` ✅ | `reicast_cable_type` | `VGA` / `TV (RGB)` / `TV (Composite)` |
| Estándar de transmisión | `NTSC` ✅ / `PAL (Mundo)` / `PAL-N (Argentina, Paraguay, Uruguay)` / `PAL-M (Brasil)` / `Por defecto` | `reicast_broadcast` | `NTSC` / `PAL` / `PAL_N` / `PAL_M` / `Default` |
| Orientación de la pantalla | `Horizontal` ✅ / `Vertical` | `reicast_screen_rotation` | `horizontal` / `vertical` |
| Ordenación alfa | `Por tira (rápido, menos preciso)` / `Por triángulo (normal)` ✅ / `Por píxel (preciso, pero más lento)` | `reicast_alpha_sorting` | `per-strip (fast, least accurate)` / `per-triangle (normal)` / `per-pixel (accurate)` |
| Emulación completa del framebuffer | `Desactivado` ✅ / `Activado` | `reicast_emulate_framebuffer` | `disabled` / `enabled` |
| Activar el búfer de RTT (renderizado a textura) | `Desactivado` ✅ / `Activado` | `reicast_enable_rttb` | `disabled` / `enabled` |
| Mipmapping | `Desactivado` / `Activado` ✅ | `reicast_mipmapping` | `disabled` / `enabled` |
| Efectos de niebla | `Desactivado` / `Activado` ✅ | `reicast_fog` | `disabled` / `enabled` |
| Modificador del volumen | `Desactivado` / `Activado` ✅ | `reicast_volume_modifier_enable` | `disabled` / `enabled` |
| Filtrado anisotrópico | `Desactivado` / `2` / `4` ✅ / `8` / `16` | `reicast_anisotropic_filtering` | `off` / `2` / `4` / `8` / `16` |
| Filtrado de texturas | `Por defecto` ✅ / `Forzar al más cercano` / `Forzar lineal` | `reicast_texture_filtering` | `0` / `1` / `2` |
| Retrasar el intercambio de cuadros | `Desactivado` ✅ / `Activado` | `reicast_delay_frame_swapping` | `disabled` / `enabled` |
| Detectar cambios de intervalo de sincronización vertical | `Desactivado` ✅ / `Activado` | `reicast_detect_vsync_swap_interval` | `disabled` / `enabled` |
| Filtro de posprocesamiento PowerVR2 | `Desactivado` ✅ / `Activado` | `reicast_pvr2_filtering` | `disabled` / `enabled` |
| Escalado de texturas (xBRZ) | `Desactivado` ✅ / `x2` / `x4` / `x6` | `reicast_texupscale` | `disabled` / `2` / `4` / `6` |
| Factor máximo del filtro de escalado de texturas | `256` / `512` / `1024` | `reicast_` | `256` / `512` / `1024` |
| Interpolación de profundidad nativa | `Desactivado` ✅ / `Activado` | `reicast_native_depth_interpolation` | `disabled` / `enabled` |
| Renderizado en múltiples hilos | `Desactivado` / `Activado` ✅ | `reicast_thraded_rendering` | `disabled` / `enabled` |
| Salto automático de cuadros | `Desactivado` ✅ / `Normal` / `Máximo` | `reicast_auto_skip_frame` | `disabled` / `some` / `more` |
| Salto de cuadros | `Desactivado` ✅ / `1` / `2` / `3` / `4` / `5` / `6` | `reicast_frame_skipping` | `disabled` / `1` / `2` / `3` / `4` / `5` / `6` |
| Trucos de pantalla ancha (Reinicio requerido) | `Desactivado` ✅ / `Activado` | `reicast_widescreen_cheats` | `disabled` / `enabled` |
| Hack de pantalla ancha | `Desactivado` ✅ / `Activado` | `reicast_widescreen_hack` | `disabled` / `enabled` |
| Carga rápida de GD-ROM (inexacta) | `Desactivado` ✅ / `Activado` | `reicast_gdrom_fast_loading` | `disabled` / `enabled` |
| SH4 CPU under/overclock | `100 MHz` / `110 MHz` / `120 MHz` / `130 MHz` / `140 MHz` / `150 MHz` / `160 MHz` / `170 MHz` / `180 MHz` / `190 MHz` / `200 MHz` / `210 MHz` / `220 MHz` / `230 MHz` / `240 MHz` / `250 MHz` / `260 MHz` / `270 MHz` / `280 MHz` / `290 MHz` / `300 MHz` | `reicast_sh4clock` | `100` / `110` / `120` / `130` / `140` / `150` / `160` / `170` / `180` / `190` / `200` / `210` / `220` / `230` / `240` / `250` / `260` / `270` / `280` / `290` / `300`  |
| Cargar texturas personalizadas | `Desactivado` ✅ / `Activado` | `reicast_custom_textures` | `disabled` / `enabled` |
| Importar texturas | `Desactivado` ✅ / `Activado` | `reicast_dump_textures` | `disabled` / `enabled` |
| Zona muerta del stick analógico | `0%` / `5%` / `10%` / `15%` ✅ / `20%` / `25%` / `30%` | `reicast_analog_stick_deadzone` | `0%` / `5%` / `10%` / `15%` / `20%` / `25%` / `30%` |
| Zona muerta de los gatillos | `0%` ✅ / `5%` / `10%` / `15%` / `20%` / `25%` / `30%` | `reicast_trigger_deadzone` | `0%` / `5%` / `10%` / `15%` / `20%` / `25%` / `30%` |
| Gatillos digitales | `Desactivado` ✅ / `Activado` | `reicast_digital_triggers` | `disabled` / `enabled` |
| Pack Puru Puru/vibración | `Desactivado` / `Activado` ✅ | `reicast_enable_purupuru` | `disabled` / `enabled` |
| Difundir salidas digitales | `Desactivado` ✅ / `Activado` | `reicast_network_output` | `disabled` / `enabled` |
| Mostrar la mira del arma 1 | `Desactivado` ✅ / `Blanco` / `Rojo` / `Verde` / `Azul` | `reicast_lightgun1_crosshair` | `disabled` / `White` / `Red` / `Green` / `Blue` |
| Mostrar la mira del arma 2 | `Desactivado` ✅ / `Blanco` / `Rojo` / `Verde` / `Azul` | `reicast_lightgun2_crosshair` | `disabled` / `White` / `Red` / `Green` / `Blue` |
| Mostrar la mira del arma 3 | `Desactivado` ✅ / `Blanco` / `Rojo` / `Verde` / `Azul` | `reicast_lightgun3_crosshair` | `disabled` / `White` / `Red` / `Green` / `Blue` |
| Mostrar la mira del arma 4 | `Desactivado` ✅ / `Blanco` / `Rojo` / `Verde` / `Azul` | `reicast_lightgun4_crosshair` | `disabled` / `White` / `Red` / `Green` / `Blue` |
| VMU por juego | `Desactivado` ✅ / `VMU A1` / `Todos los VMU` | `reicast_per_content_vmus` | `disabled` / `VMU A1` / `All VMUs` |
| Sonidos VMU | `Desactivado` ✅ / `Activado` | `reicast_vmu_sound` | `disabled` / `enabled` |
| Visualización de la pantalla del VMU 1 | `Desactivado` ✅ / `Activado` | `reicast_vmu1_screen_display` | `disabled` / `enabled` |
| Posición de la pantalla del VMU 1 | `Arriba a la izquierda` ✅ / `Arriba a la derecha` / `Abajo a la izquierda` / `Abajo a la derecha` | `reicast_vmu1_screen_position` | `Upper Left` / `Upper Right` / `Lower Left` / `Lower Right` |
| Tamaño de la pantalla del VMU 1 | `1x` ✅ / `2x` / `3x` / `4x` / `5x` | `reicast_vmu1_screen_size_mult` | `1x` / `2x` / `3x` / `4x` / `5x` |
| Color de los píxeles encendidos de la pantalla del VMU 1 | `Por defecto activado` ✅ / `Por defecto desactivado` / `Negro` / `Azul` / `Azul claro` / `Verde` / `Cian` / `Cian azul` / `Verde claro` / `Verde cian` / `Cian claro` / `Rojo` / `Púrpura` / `Púrpura claro` / `Amarillo` / `Gris` / `Púrpura claro (2)` / `Verde claro (2)` / `Verde claro (3)` / `Cian claro (2)` / `Rojo claro (2)` / `Magenta` / `Púrpura claro (3)` / `Naranja claro` / `Naranja` / `Púrpura claro (4)` / `Amarillo claro` / `Amarillo claro (2)` / `Blanco` | `reicast_vmu1_pixel_on_color` | `DEFAULT_ON 00` / `DEFAULT_OFF 01` / `BLACK 02` / `BLUE 03` / `LIGHT_BLUE 04` / `GREEN 05` / `CYAN 06` / `CYAN_BLUE 07` / `LIGHT_GREEN 08` / `CYAN_GREEN 09` / `LIGHT_CYAN 10` / `RED 11` / `PURPLE 12` / `LIGHT_PURPLE 13` / `YELLOW 14` / `GREY 15` / `LIGHT_PURPLE_2 16` / `LIGHT_GREEN_2 17` / `LIGHT_GREEN_3 18` / `LIGHT_CYAN_2 19` / `LIGHT_RED_2 20` / `MAGENTA 21` / `LIGHT_PURPLE_3 22` / `LIGHT_ORANGE 23` / `ORANGE 24` / `LIGHT_PURPLE_4 25` / `LIGHT_YELLOW 26` / `LIGHT_YELLOW_2 27` / `WHITE 28` |
| Color de los píxeles apagados de la pantalla del VMU 1 | `Por defecto desactivado` ✅ / `Por defecto activado` / `Negro` / `Azul` / `Azul claro` / `Verde` / `Cian` / `Cian azul` / `Verde claro` / `Verde cian` / `Cian claro` / `Rojo` / `Púrpura` / `Púrpura claro` / `Amarillo` / `Gris` / `Púrpura claro (2)` / `Verde claro (2)` / `Verde claro (3)` / `Cian claro (2)` / `Rojo claro (2)` / `Magenta` / `Púrpura claro (3)` / `Naranja claro` / `Naranja` / `Púrpura claro (4)` / `Amarillo claro` / `Amarillo claro (2)` / `Blanco` | `reicast_vmu1_pixel_off_color` | `DEFAULT_OFF 00` / `DEFAULT_ON 01` / `BLACK 02` / `BLUE 03` / `LIGHT_BLUE 04` / `GREEN 05` / `CYAN 06` / `CYAN_BLUE 07` / `LIGHT_GREEN 08` / `CYAN_GREEN 09` / `LIGHT_CYAN 10` / `RED 11` / `PURPLE 12` / `LIGHT_PURPLE 13` / `YELLOW 14` / `GREY 15` / `LIGHT_PURPLE_2 16` / `LIGHT_GREEN_2 17` / `LIGHT_GREEN_3 18` / `LIGHT_CYAN_2 19` / `LIGHT_RED_2 20` / `MAGENTA 21` / `LIGHT_PURPLE_3 22` / `LIGHT_ORANGE 23` / `ORANGE 24` / `LIGHT_PURPLE_4 25` / `LIGHT_YELLOW 26` / `LIGHT_YELLOW_2 27` / `WHITE 28` |
| Opacidad de la pantalla del VMU 1 | `10%` / `20%` / `30%` / `40%` / `50%` / `60%` / `70%` / `80%` / `90%` / `100%` | `reicast_vmu1_screen_opacity` | `10%` / `20%` / `30%` / `40%` / `50%` / `60%` / `70%` / `80%` / `90%` / `100%` |
| Visualización de la pantalla del VMU 2 | `Desactivado` ✅ / `Activado` | `reicast_vmu2_screen_display` | `disabled` / `enabled` |
| Posición de la pantalla del VMU 2 | `Arriba a la izquierda` ✅ / `Arriba a la derecha` / `Abajo a la izquierda` / `Abajo a la derecha` | `reicast_vmu2_screen_position` | `Upper Left` / `Upper Right` / `Lower Left` / `Lower Right` |
| Tamaño de la pantalla del VMU 2 | `1x` ✅ / `2x` / `3x` / `4x` / `5x` | `reicast_vmu2_screen_size_mult` | `1x` / `2x` / `3x` / `4x` / `5x` |
| Color de los píxeles encendidos de la pantalla del VMU 2 | `Por defecto activado` ✅ / `Por defecto desactivado` / `Negro` / `Azul` / `Azul claro` / `Verde` / `Cian` / `Cian azul` / `Verde claro` / `Verde cian` / `Cian claro` / `Rojo` / `Púrpura` / `Púrpura claro` / `Amarillo` / `Gris` / `Púrpura claro (2)` / `Verde claro (2)` / `Verde claro (3)` / `Cian claro (2)` / `Rojo claro (2)` / `Magenta` / `Púrpura claro (3)` / `Naranja claro` / `Naranja` / `Púrpura claro (4)` / `Amarillo claro` / `Amarillo claro (2)` / `Blanco` | `reicast_vmu2_pixel_on_color` | `DEFAULT_ON 00` / `DEFAULT_OFF 01` / `BLACK 02` / `BLUE 03` / `LIGHT_BLUE 04` / `GREEN 05` / `CYAN 06` / `CYAN_BLUE 07` / `LIGHT_GREEN 08` / `CYAN_GREEN 09` / `LIGHT_CYAN 10` / `RED 11` / `PURPLE 12` / `LIGHT_PURPLE 13` / `YELLOW 14` / `GREY 15` / `LIGHT_PURPLE_2 16` / `LIGHT_GREEN_2 17` / `LIGHT_GREEN_3 18` / `LIGHT_CYAN_2 19` / `LIGHT_RED_2 20` / `MAGENTA 21` / `LIGHT_PURPLE_3 22` / `LIGHT_ORANGE 23` / `ORANGE 24` / `LIGHT_PURPLE_4 25` / `LIGHT_YELLOW 26` / `LIGHT_YELLOW_2 27` / `WHITE 28` |
| Color de los píxeles apagados de la pantalla del VMU 2 | `Por defecto desactivado` ✅ / `Por defecto activado` / `Negro` / `Azul` / `Azul claro` / `Verde` / `Cian` / `Cian azul` / `Verde claro` / `Verde cian` / `Cian claro` / `Rojo` / `Púrpura` / `Púrpura claro` / `Amarillo` / `Gris` / `Púrpura claro (2)` / `Verde claro (2)` / `Verde claro (3)` / `Cian claro (2)` / `Rojo claro (2)` / `Magenta` / `Púrpura claro (3)` / `Naranja claro` / `Naranja` / `Púrpura claro (4)` / `Amarillo claro` / `Amarillo claro (2)` / `Blanco` | `reicast_vmu2_pixel_off_color` | `DEFAULT_OFF 00` / `DEFAULT_ON 01` / `BLACK 02` / `BLUE 03` / `LIGHT_BLUE 04` / `GREEN 05` / `CYAN 06` / `CYAN_BLUE 07` / `LIGHT_GREEN 08` / `CYAN_GREEN 09` / `LIGHT_CYAN 10` / `RED 11` / `PURPLE 12` / `LIGHT_PURPLE 13` / `YELLOW 14` / `GREY 15` / `LIGHT_PURPLE_2 16` / `LIGHT_GREEN_2 17` / `LIGHT_GREEN_3 18` / `LIGHT_CYAN_2 19` / `LIGHT_RED_2 20` / `MAGENTA 21` / `LIGHT_PURPLE_3 22` / `LIGHT_ORANGE 23` / `ORANGE 24` / `LIGHT_PURPLE_4 25` / `LIGHT_YELLOW 26` / `LIGHT_YELLOW_2 27` / `WHITE 28` |
| Opacidad de la pantalla del VMU 2 | `10%` / `20%` / `30%` / `40%` / `50%` / `60%` / `70%` / `80%` / `90%` / `100%` | `reicast_vmu2_screen_opacity` | `10%` / `20%` / `30%` / `40%` / `50%` / `60%` / `70%` / `80%` / `90%` / `100%` |
| Visualización de la pantalla del VMU 3 | `Desactivado` ✅ / `Activado` | `reicast_vmu3_screen_display` | `disabled` / `enabled` |
| Posición de la pantalla del VMU 3 | `Arriba a la izquierda` ✅ / `Arriba a la derecha` / `Abajo a la izquierda` / `Abajo a la derecha` | `reicast_vmu3_screen_position` | `Upper Left` / `Upper Right` / `Lower Left` / `Lower Right` |
| Tamaño de la pantalla del VMU 3 | `1x` ✅ / `2x` / `3x` / `4x` / `5x` | `reicast_vmu3_screen_size_mult` | `1x` / `2x` / `3x` / `4x` / `5x` |
| Color de los píxeles encendidos de la pantalla del VMU 3 | `Por defecto activado` ✅ / `Por defecto desactivado` / `Negro` / `Azul` / `Azul claro` / `Verde` / `Cian` / `Cian azul` / `Verde claro` / `Verde cian` / `Cian claro` / `Rojo` / `Púrpura` / `Púrpura claro` / `Amarillo` / `Gris` / `Púrpura claro (2)` / `Verde claro (2)` / `Verde claro (3)` / `Cian claro (2)` / `Rojo claro (2)` / `Magenta` / `Púrpura claro (3)` / `Naranja claro` / `Naranja` / `Púrpura claro (4)` / `Amarillo claro` / `Amarillo claro (2)` / `Blanco` | `reicast_vmu3_pixel_on_color` | `DEFAULT_ON 00` / `DEFAULT_OFF 01` / `BLACK 02` / `BLUE 03` / `LIGHT_BLUE 04` / `GREEN 05` / `CYAN 06` / `CYAN_BLUE 07` / `LIGHT_GREEN 08` / `CYAN_GREEN 09` / `LIGHT_CYAN 10` / `RED 11` / `PURPLE 12` / `LIGHT_PURPLE 13` / `YELLOW 14` / `GREY 15` / `LIGHT_PURPLE_2 16` / `LIGHT_GREEN_2 17` / `LIGHT_GREEN_3 18` / `LIGHT_CYAN_2 19` / `LIGHT_RED_2 20` / `MAGENTA 21` / `LIGHT_PURPLE_3 22` / `LIGHT_ORANGE 23` / `ORANGE 24` / `LIGHT_PURPLE_4 25` / `LIGHT_YELLOW 26` / `LIGHT_YELLOW_2 27` / `WHITE 28` |
| Color de los píxeles apagados de la pantalla del VMU 3 | `Por defecto desactivado` ✅ / `Por defecto activado` / `Negro` / `Azul` / `Azul claro` / `Verde` / `Cian` / `Cian azul` / `Verde claro` / `Verde cian` / `Cian claro` / `Rojo` / `Púrpura` / `Púrpura claro` / `Amarillo` / `Gris` / `Púrpura claro (2)` / `Verde claro (2)` / `Verde claro (3)` / `Cian claro (2)` / `Rojo claro (2)` / `Magenta` / `Púrpura claro (3)` / `Naranja claro` / `Naranja` / `Púrpura claro (4)` / `Amarillo claro` / `Amarillo claro (2)` / `Blanco` | `reicast_vmu3_pixel_off_color` | `DEFAULT_OFF 00` / `DEFAULT_ON 01` / `BLACK 02` / `BLUE 03` / `LIGHT_BLUE 04` / `GREEN 05` / `CYAN 06` / `CYAN_BLUE 07` / `LIGHT_GREEN 08` / `CYAN_GREEN 09` / `LIGHT_CYAN 10` / `RED 11` / `PURPLE 12` / `LIGHT_PURPLE 13` / `YELLOW 14` / `GREY 15` / `LIGHT_PURPLE_2 16` / `LIGHT_GREEN_2 17` / `LIGHT_GREEN_3 18` / `LIGHT_CYAN_2 19` / `LIGHT_RED_2 20` / `MAGENTA 21` / `LIGHT_PURPLE_3 22` / `LIGHT_ORANGE 23` / `ORANGE 24` / `LIGHT_PURPLE_4 25` / `LIGHT_YELLOW 26` / `LIGHT_YELLOW_2 27` / `WHITE 28` |
| Opacidad de la pantalla del VMU 3 | `10%` / `20%` / `30%` / `40%` / `50%` / `60%` / `70%` / `80%` / `90%` / `100%` | `reicast_vmu3_screen_opacity` | `10%` / `20%` / `30%` / `40%` / `50%` / `60%` / `70%` / `80%` / `90%` / `100%` |
| Visualización de la pantalla del VMU 4 | `Desactivado` ✅ / `Activado` | `reicast_vmu4_screen_display` | `disabled` / `enabled` |
| Posición de la pantalla del VMU 4 | `Arriba a la izquierda` ✅ / `Arriba a la derecha` / `Abajo a la izquierda` / `Abajo a la derecha` | `reicast_vmu4_screen_position` | `Upper Left` / `Upper Right` / `Lower Left` / `Lower Right` |
| Tamaño de la pantalla del VMU 4 | `1x` ✅ / `2x` / `3x` / `4x` / `5x` | `reicast_vmu4_screen_size_mult` | `1x` / `2x` / `3x` / `4x` / `5x` |
| Color de los píxeles encendidos de la pantalla del VMU 4 | `Por defecto activado` ✅ / `Por defecto desactivado` / `Negro` / `Azul` / `Azul claro` / `Verde` / `Cian` / `Cian azul` / `Verde claro` / `Verde cian` / `Cian claro` / `Rojo` / `Púrpura` / `Púrpura claro` / `Amarillo` / `Gris` / `Púrpura claro (2)` / `Verde claro (2)` / `Verde claro (3)` / `Cian claro (2)` / `Rojo claro (2)` / `Magenta` / `Púrpura claro (3)` / `Naranja claro` / `Naranja` / `Púrpura claro (4)` / `Amarillo claro` / `Amarillo claro (2)` / `Blanco` | `reicast_vmu4_pixel_on_color` | `DEFAULT_ON 00` / `DEFAULT_OFF 01` / `BLACK 02` / `BLUE 03` / `LIGHT_BLUE 04` / `GREEN 05` / `CYAN 06` / `CYAN_BLUE 07` / `LIGHT_GREEN 08` / `CYAN_GREEN 09` / `LIGHT_CYAN 10` / `RED 11` / `PURPLE 12` / `LIGHT_PURPLE 13` / `YELLOW 14` / `GREY 15` / `LIGHT_PURPLE_2 16` / `LIGHT_GREEN_2 17` / `LIGHT_GREEN_3 18` / `LIGHT_CYAN_2 19` / `LIGHT_RED_2 20` / `MAGENTA 21` / `LIGHT_PURPLE_3 22` / `LIGHT_ORANGE 23` / `ORANGE 24` / `LIGHT_PURPLE_4 25` / `LIGHT_YELLOW 26` / `LIGHT_YELLOW_2 27` / `WHITE 28` |
| Color de los píxeles apagados de la pantalla del VMU 4 | `Por defecto desactivado` ✅ / `Por defecto activado` / `Negro` / `Azul` / `Azul claro` / `Verde` / `Cian` / `Cian azul` / `Verde claro` / `Verde cian` / `Cian claro` / `Rojo` / `Púrpura` / `Púrpura claro` / `Amarillo` / `Gris` / `Púrpura claro (2)` / `Verde claro (2)` / `Verde claro (3)` / `Cian claro (2)` / `Rojo claro (2)` / `Magenta` / `Púrpura claro (3)` / `Naranja claro` / `Naranja` / `Púrpura claro (4)` / `Amarillo claro` / `Amarillo claro (2)` / `Blanco` | `reicast_vmu4_pixel_off_color` | `DEFAULT_OFF 00` / `DEFAULT_ON 01` / `BLACK 02` / `BLUE 03` / `LIGHT_BLUE 04` / `GREEN 05` / `CYAN 06` / `CYAN_BLUE 07` / `LIGHT_GREEN 08` / `CYAN_GREEN 09` / `LIGHT_CYAN 10` / `RED 11` / `PURPLE 12` / `LIGHT_PURPLE 13` / `YELLOW 14` / `GREY 15` / `LIGHT_PURPLE_2 16` / `LIGHT_GREEN_2 17` / `LIGHT_GREEN_3 18` / `LIGHT_CYAN_2 19` / `LIGHT_RED_2 20` / `MAGENTA 21` / `LIGHT_PURPLE_3 22` / `LIGHT_ORANGE 23` / `ORANGE 24` / `LIGHT_PURPLE_4 25` / `LIGHT_YELLOW 26` / `LIGHT_YELLOW_2 27` / `WHITE 28` |
| Opacidad de la pantalla del VMU 4 | `10%` / `20%` / `30%` / `40%` / `50%` / `60%` / `70%` / `80%` / `90%` / `100%` | `reicast_vmu4_screen_opacity` | `10%` / `20%` / `30%` / `40%` / `50%` / `60%` / `70%` / `80%` / `90%` / `100%` |

## ![](/emulators/external-links.png) Enlaces externos

* **Código fuente utilizado** : [https://github.com/libretro/flycast/](https://github.com/libretro/flycast/)
* **Documentación Libretro** : [https://docs.libretro.com/library/flycast/](https://docs.libretro.com/library/flycast/)
* **Código fuente oficial** : [https://github.com/flyinghead/flycast](https://github.com/flyinghead/flycast)
