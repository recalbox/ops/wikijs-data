---
title: Complete video configuration guide
description: 
published: true
date: 2024-03-14T17:31:09.392Z
tags: configuration, video, guide
editor: markdown
dateCreated: 2021-08-06T17:26:55.849Z
---

## Introduction

In Recalbox, you can set different screen resolutions to suit your needs:

- Global resolution (whole system)
- Interface screen resolution
- System screen resolution (including Kodi)

Until now, to set a screen resolution to use, you had to use video modes, and this was not available outside Raspberry Pis. No more, there's something much simpler, and accessible to everyone.

To define a screen resolution to use, you simply need to indicate the screen resolution to be used. For example :

```ini
global.videomode=1280x1080
```

## Manual configuration

### Global resolution configuration

To define the global resolution to be used, you need to modify the value of `global.videomode` in the `recalbox.conf` file.

```ini
global.videomode=1280x1080
```

You can also use the `default` value to use your screen's native resolution.

>If the `global.videomode` line doesn't exist, you must add it on an empty line in the same file.
{.is-info}

### Setting interface resolution

To define the interface resolution to be used, you need to modify the value of `es.videomode` in the `recalbox.conf` file.

```ini
es.videomode=1280x1080
```

You can also use the `default` value to use your screen's native resolution.

>If the `es.videomode` line doesn't exist, you must add it on an empty line in the same file.
{.is-info}

### System resolution configuration

To set the resolution for a particular system, you need to modify the value that corresponds to the system. Examples: ```ini

```ini
nes.videomode=640x480
snes.videomode=1280x1024
```

You can also use the value `default` to use your screen's native resolution.

>If the system line doesn't exist, you must add it on an empty line in the same file.
{.is-info}

## Practical configuration

Outside PCs, you can set the same resolutions from the interface by pressing `START` and going to `Advanced settings` > `Resolutions`.