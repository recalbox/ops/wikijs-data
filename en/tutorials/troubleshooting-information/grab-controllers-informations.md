---
title: Grab controllers informations
description: 
published: true
date: 2021-08-06T14:37:45.921Z
tags: controllers, informations
editor: markdown
dateCreated: 2021-08-06T14:36:03.953Z
---

There are a few useful commands to get information about devices that can be used to help solve problems. They are all done [via SSH](./../system/access/root-access-terminal-cli).

List the available devices:

```shell
cat /proc/bus/input/devices
```

Get information about USB devices:

```shell
lsusb -v
```

Get information from `udev` about devices that were mapped in an event:

```shell
for i in /dev/input/event*; do echo $i;udevadm info -q all -n $i;done
```

Get a list of all buttons/axes/keys found (even if it is a keyboard):

```shell
for i in /dev/input/event*; do echo $i;(evtest $i) & ( evtestpid=$! && sleep 0.1 && kill -15 $evtestpid );done
```

Whenever you run one of these commands, it is best not to paste the entire output to the forum or Discord. Instead, use something like [http://pastebin.com/](http://pastebin.com/), paste what is displayed after you run the command, and then give the link to the people you are communicating with.