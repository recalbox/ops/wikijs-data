---
title: Modos HD y pantalla panorámica
description: 
published: true
date: 2024-11-17T15:04:34.030Z
tags: pantalla, hd, panorámica
editor: markdown
dateCreated: 2024-11-17T15:04:34.030Z
---

## Presentación

Los modos HD y panorámico están disponibles en algunos sistemas y permiten obtener una imagen de mejor calidad (HD) y ampliada para que cubra toda la superficie de la pantalla (pantalla panorámica).

>El modo panorámico está disponible en Raspberry Pi 4 / 400 / 5 y PC. El modo HD está disponible en Raspberry Pi 5 y PC.
{.is-info}

## Configuración

En la interfaz, pulsad `START` y luego id dentro de `GAME OPTIONS`. En función de vuestro hardware, dispondréis de una o ambas opciones.

## Notas sobre estas opciones

¡Estas opciones **fuerzan el emulador/núcleo** que utiliza Recalbox para lanzar los juegos! A continuación se muestran los cores utilizados para ambas funciones:

| Système | Core utilisé | Ecran large | HD |
| :---: | :---: | :---: | :---: |
| Atomiswave | Libretro-Flycast | ✅ | ✅ |
| Dreamcast | Libretro-Flycast | ✅ | ✅ |
| Megadrive | Libretro-GenesisPlusGX-Wide | ✅ | ❌ |
| Naomi | Libretro-Flycast | ✅ | ✅ |
| Nintendo 64 | Libretro-Parallel_n64 | ❌ | ✅ |
| Playstation | Libretro-PCSX_reARMed | ❌ | ✅ |
| Saturn | Libretro-Yabasanshiro | ❌ | ✅ |
| Super Nintendo | Libretro-bsneshd | ✅ | ❌ |