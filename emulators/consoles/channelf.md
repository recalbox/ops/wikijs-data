---
title: Channel F
description: 
published: true
date: 2025-01-05T23:32:37.561Z
tags: channelf, consoles, fairchild
editor: markdown
dateCreated: 2021-07-27T17:14:28.170Z
---

![](/emulators/consoles/channelf.svg){.align-center}

## Technical data

* **Manufacturer**: Fairchild Semiconductor
* **Year of release**: 1976
* **Units sold**: 250000
* **CPU**: Fairchild F8 @ 1.79 MHz (PAL 2.22 MHz)
* **RAM**: 64 bytes
* **VRAM**: 2 kB
* **Resolution**: 128x64, 8 colors
* **Audio**: 500 Hz, 1 kHz and 1.5 kHz tones (can be modulated)
* **Media**: ROM Cartridge

## Presentation

The **Fairchild Channel F** is a video game console produced by Fairchild Semiconductor. Released in November 1976 in North America, it was sold there for $169.95. The console is notable for being the first in the world based on a cartridge system, as well as the first to have a microprocessor.

At the time of its launch, the console was known as the Video Entertainment System, or VES, but when Atari released the VCS the following year, Fairchild renamed the console. By 1977, the Fairchild Channel had sold 250,000 units and remained on the heels of the VCS, then renamed the Atari 2600.

## Emulators

[Libretro FreeChaF](libretro-freechaf)
[Libretro FBNeo](libretro-fbneo)