---
title: Utilización de un mando infrarojo
description: 
published: true
date: 2023-09-23T12:18:38.935Z
tags: kodi, mando, infrarojo
editor: markdown
dateCreated: 2023-09-23T12:18:38.935Z
---

El Kodi de Recalbox puede controlarse a travès de cualquier mando infrarojo.

Para ello tendréis que pagar alrededor de 2 euros sólamente y el procedimiento de instalación es muy sencillo, como os explicaremos a continuación.

## I - Prerequisitos

### A - Un receptor de infrarojos

Para que funcione, necesitaréis comprar un receptor de infrarrojos como un módulo TSOP4838 de 38KHz; tened en cuenta que también podéis intentar hacer esto con algunos otros componentes disponibles en el mercado. Podéis comprar este módulo por alrededor de 1€ en cualquier tienda de electrónica o por Internet.

![Récepteur IR](/basic-usage/features/kodi/ir-control/ir1.jpg)

### B - Cables dupont hembra/hembra

Para conectar fácilmente vuestro receptor sin necesidad de soldar, necesitaréis 3 cables dupont H/H. Se pueden comprar en las mismas tiendas que vuestro receptor por unos pocos céntimos.

![Câbles dupont F/F](/basic-usage/features/kodi/ir-control/ir2.jpg)

### C - Diagrama

Para conectar el receptor y los cables, seguid el siguiente diagrama:

![](/basic-usage/features/kodi/ir-control/ir3.png)

Ejemplo de montaje:

![](/basic-usage/features/kodi/ir-control/ir4.jpg)

### D - Mando a distancia

Prácticamente todos los mandos a distancia son compatibles, siempre que respeten estándares.

Todos estos mandos a distancia han sido probados con éxito:

* Un mando a distancia Philips para equipos de alta fidelidad.
* Un mando a distancia Samsung para un grabador de vídeo.
* Un mando a distancia Universal.
* Un mando a distancia de ordenador MAC.

![](/basic-usage/features/kodi/ir-control/ir5.jpg)

## II - Configuración

### A - config.txt

* Abrid el fichero [recalbox-user-config.txt](./../../../tutorials/system/modification/configtxt-configuration) via [SSH](./../../../tutorials/system/access/root-access-terminal-cli).
* Activad el modo lectura-escritura en la [partición de arranque](./../../../tutorials/system/access/remount-partition-with-write-access)
* Descomentad la siguiente línea borrando el `#` :

```ini
#dtoverlay=lirc-rpi
```

Pasa a ser así:

```ini
dtoverlay=lirc-rpi
```

* Reiniciad Recalbox.

### B - Configuración del mando a distancia

#### 1. Verificación de eventos IR

Vamos a comprobar que el sistema funciona correctamente:

* Conectaros a Recalbox vía [SSH](./../../../tutorials/system/access/root-access-terminal-cli)
* Lanzad el siguiente comando:

```shell
lsmod
```

Encontraréis una lína que comienza por `lirc_rpi`.

![](/basic-usage/features/kodi/ir-control/ir6.png)

* Si es así, lanzad el siguiente comando:

```shell
mode2 -d /dev/lirc0
```

* Cada vez que pulséis un botón del mando a distancia, deberían aparecer números en el terminal. Si es así, el mando a distancia y el receptor funcionan correctamente.
* Pulsad `Ctrl`+`C` para salir del comando.

![](/basic-usage/features/kodi/ir-control/ir7.png)

#### 2. Grabad la configuración del mando a distancia

* Lanzad el comando:

```shell
irrecord -H /recalbox/share/custom.conf
```

* Pulsad `Enter` para continuar.
* Escribid `customremote` como nombre del mando y pulsad `Enter` para continuar.

![](/basic-usage/features/kodi/ir-control/ir8.png)

* Apretad uno a uno todos los botones de vuestro mando a distancia durante alreadedo de un segundo.

![](/basic-usage/features/kodi/ir-control/ir9.png)

* Escribid el nombre de vuesto botón eligiendo de entre la lista que os vamos a presentar y apretad dicho botón.

  * KEY_DOWN (Abajo)
  * KEY_EXIT (Volver/Salir)
  * KEY_INFO (Muestra información sobre el medio que se está reproduciendo)
  * KEY_LEFT (Izquierda)
  * KEY_MENU (Menú)
  * KEY_MUTE (Silenciar)
  * KEY_OK (Ok)
  * KEY_PLAY (Lectura y Pausa)
  * KEY_POWER (Salir)
  * KEY_RIGHT (Derecha)
  * KEY_STOP (Stop)
  * KEY_UP (Arriba)
  * KEY_VOLUMEDOWN (Volumen -)
  * KEY_VOLUMEUP (Volumen +)

* Y si lo necesitáis tambien existen:

  * KEY_AUDIO
  * KEY_BLUE
  * KEY_CHANNELDOWN
  * KEY_CHANNELUP
  * KEY_DELETE
  * KEY_ENTER
  * KEY_EPG
  * KEY_FASTFORWARD
  * KEY_GREEN
  * KEY_LANGUAGE
  * KEY_MEDIA
  * KEY_NEXT
  * KEY_NUMERIC_1
  * KEY_NUMERIC_2
  * KEY_NUMERIC_3
  * KEY_NUMERIC_4
  * KEY_NUMERIC_5
  * KEY_NUMERIC_6
  * KEY_NUMERIC_7
  * KEY_NUMERIC_8
  * KEY_NUMERIC_9
  * KEY_NUMERIC_0
  * KEY_PAUSE
  * KEY_PREVIOUS
  * KEY_PVR
  * KEY_RADIO
  * KEY_RECORD
  * KEY_RED
  * KEY_REWIND
  * KEY_SUBTITLE
  * KEY_VIDEO
  * KEY_YELLOW
  * KEY_ZOOM

![](/basic-usage/features/kodi/ir-control/ir10.png)

* Repetid esta operación para los botones de vuestro mando que os interesa hacer funcionar.

Para volver a configurar un botón, volved a escribir su nombre y empezad de nuevo.

![](/basic-usage/features/kodi/ir-control/ir11.png)

* Cuando hayáis guardado todos botones que queréis, pulsad `Enter` para continuar.
* A continuación, pulsad varias veces uno de los botones, rápidamente, sin mantenerlo pulsado y sin cambiar de botón.

![](/basic-usage/features/kodi/ir-control/ir12.png)

* Al final, el programa se cerrará él solo.
* Si quereis volver a empezar, borrad el fichero de configuración que se ha generado con el comando:

```shell
rm /tmp/custom.conf
```

* Y reiniciad `irrecord`.

#### 3. Fichero de configuración

* Moved el fichero de configuración con el siguiente comando para reemplazar el fichero `lircd.conf` con el vuestro:

```shell
mv /recalbox/share/custom.conf /recalbox/share/system/.config/lirc/lircd.conf
```

* Relanzad el servicio `lircd` con:

```shell
/etc/init.d/S25lircd restart
```

![](/basic-usage/features/kodi/ir-control/ir13.png)

#### 4. Verificar que la configuración funciona

* Escribid el comando `irw`.
* Cada vez que pulséis un botón, deberíais ver aparecer una línea con el nombre del botón.
* Pulsad "Ctrl" + "C" para salir.

![](/basic-usage/features/kodi/ir-control/ir14.png)

* Lanzad Kodi y verificar el funcionamiento:

## III - Configuración avanzada

### A - Lircmap.xml

* Podéis personalizar la configuración de botones del mando modificando el fichero `/recalbox/share/system/.kodi/userdata/Lircmap.xml`.

>Si habéis borrado el directorio `~/.kodi` y reiniciado Kodi sin haber reiniciado antes vuestra Recalbox, no encontraréis las personalizaciones de Kodi o del fichero `Lircmap.xml`.
{.is-warning}

* Si os ocurre esto, copiad `/recalbox/share_init/system/.kodi/userdata/Lircmap.xml` sobre `/recalbox/share/system/.kodi/userdata/Lircmap.xml`

### B - remote.xml

Podéis modificar la definición de los botones y de las acciones dentro del fichero `/recalbox/share/system/.kodi/userdata/keymaps/remote.xml`

### C - SALIR / STOP

Lo siguiente es útil para aquellas personas a las que no les gusta el hecho de que el botón `Atrás` no pare la reproducción, o para aquellos que sólo tienen un único botón que debe realizar las dos funciones de `Stop / Atrás`.

En este caso, podéis cambiar dentro del fichero remote.xml "Back" por "Stop".

### D - SUBIR/BAJAR EL VOLUMEN

>Este apartado está relacionado con mandos a distancia que soportan CEC.
{.is-info}

Si vuestro mando a distancia CEC no puede ejecutar los comandos de volumen, se pueden utilizar otros botones sustituyendo, por ejemplo, dentro del fichero remote.xml, en la sección global:

```xml
  <skipplus>SkipNext</skipplus>
  <skipminus>SkipPrevious</skipminus>
```

por

```xml
  <skipplus>VolumeUp</skipplus>
  <skipminus>VolumeDown</skipminus>
```

### E - PAUSO en OK

Con la skin Refocus, para que la acción de pausa sea más facil de realizar (principalmente con los mandos Apple), podéis modificar el fichero `~/.kodi/addons/skin.refocus/720p/VideoOSD.xml` sustituyendo:

```xml
  <defaultcontrol always="true">700</defaultcontrol>
```

Por esto:

```xml
  <defaultcontrol always="true">705</defaultcontrol>
```