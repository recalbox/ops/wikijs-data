---
title: 🎬 Kodi
description: Cómo obtener el mejor Kodi posible !
published: true
date: 2023-09-26T07:02:37.567Z
tags: tutorial, kodi
editor: markdown
dateCreated: 2023-09-01T09:20:53.489Z
---

En esta página, encontraréis algunos consejos y trucos relacionados con Kodi.

Aquí tenéis la lista de categorías disponibles:

[Añadir la extensión Amazon Prime](add-amazon-prime-add-on)
[Añadir la extensión Disney+](add-disney-plus-add-on)
[Configurar la extensión Twitch](twitch-add-on-configuration)
[Configurar la extensión YouTube](youtube-add-on-configuration)
[Lectura de vídeos en 3D](play-3d-videos)