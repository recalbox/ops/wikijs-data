---
title: Glosario
description: Lista de términos en relación con Recalbox
published: true
date: 2023-09-24T20:46:21.136Z
tags: ayuda, glosario, definicion
editor: markdown
dateCreated: 2023-04-18T14:38:09.151Z
---

Esta sección explica los términos técnicos relacionados con Recalbox, para consultar cuando se encuentra una palabra que no se entienda bien.

## A

* **Arcade** : El arcade hace referencia a las máquinas de video juegos en las que se introduce una moneda para poder jugar y obtener créditos. Cada máquina posee un hardware espécifico al juego (mientras que las consolas permiten jugar a varios juegos bajo un mismo hardware). Es importante informarse sobre el funcionamiento de los emuladores de arcade, no es buena idea descargar roms de internet sin vérificar que corresponden bien a la versión del emulador de arcade, y muy probablemente no funcionaran. Además, en el contexto de Recalbox, los juegos Neogeo se emulan de la misma forma que los juegos de arcade.
* **API** : Application Programming Interface.
* **Archivo de soporte** : ver _Archivo de soporte_.

## B

* **Big-Endian** : es la forma mas corriente de almacenar los ficheros [binarios](https://techlib.fr/definition/binary.html). En primer lugar se encuentra la valor más significativa. Por ejemplo, la representación big-endian del numéro [entero](https://techlib.fr/definition/integer.html) 123 sitúa las centenas (1) en primer lugar, seguido de las decenas (2) y finalemente las unidades (3), es decir[123].
* **Bios** : Ficheros que representan programas leídos por el sistema de origen, necesarios para que funcionen correctamente algunos emuladores.
* **Buildroot** : Conjunto de ficheros Makefiles y de correctivos que permiten simplificar y automatizar el proceso de construcción de un entorno Linux y su arranque en los sistemas embarcados utilizando una compilación cruzada para permitir à partir de una misma base de código fuente construir binarios ejecutables en diferentes plataformas hardware predefinidas. Buildroot se encarga de construire las herramientas de compilación cruzada necesarias para cada sistema, de crear un sistema de ficheros raíz, compila una imagen del núcleo de Linux y genera un código de arranque para cada sistema objetivo. Estas etapas se pueden encadenar o lanzar individualmente. Por ejemplo, las herramientas de compilación cruzada que ya ha sido generadas pueden reutilizarse mientras Buildroot crea el sistema de ficheros raíz sin necesidad de recompilar todo cada vez.

## C

* **Changelog** : Fichero de texto que contiene una lista de las modificationes aplicadas al programa. Los ficheros [changelog](https://gitlab.com/recalbox/recalbox/raw/master/CHANGELOG.md) y [release-notes](https://gitlab.com/recalbox/recalbox/raw/master/RELEASE-NOTES.md) son públicamente disponibles para todo el mundo.
* **CHD** : Imagen de disco para los juegos de arcade utilizada por algunos juegos MAME. Contiene una versión comprimida del contenido del disco duro de la máquina de arcade original.
* **Cheats** : En el contexto de Recalbox, los cheats permiten reproducir el comportamiento de algunos dispositivos hardware que antiguamente se utilizaban para "hacer trampas". Estos códigos permiten activar o désactivar funcionalidades que modifican ciertos aspectos del juego (vidas infinitas, invulnerabilidad, etc).
* **Checksum** _(o suma de control)_ : Una cadena de caracteres calculada a partir del contenido de un fichero. Si dos ficheros tienen el mismo checksum, su contenido es idéntico. Los checksum se utilizan para Netplay.
* **Core** : Un programa cargado en memoria por une emulador (normalemente RetroArch).
* **CRC** : Un tipo de checksum utilizado por Netplay.
* **Cue** : _(o .cue)_ : ficheros que describen cómo se reparten los datos de una image de CD, utilizados principalmente por las consolas de discos, junto con los ficheros de extension .bin.

## D

* **Dat** _ (o .dat)_ : ficheros utilizados para verificar los packs de roms. Permiten asociar un pack de roms a una determinada version. Se utilizan sobre todo para ajustar y vérificar los sets de arcade y para el netplay.
* **Debugar** : búsqueda de un problema informático producido por un bug. Un bug es un disfuncionamiento del programa. Por ejemplo : para debugar Recabox hay que activar los logs y reiniciar.
* **Modo demo** : Una funcionalidad de Recalbox que permite a EmulationStation lanzar juegos al azar después de un cierto tiempo de inactividad del usuario.
* **DragonBlaze** : Nombre dado a la version 6.0 de Recalbox.

## E

* **Endian** : En [Informatica](https://techlib.fr/definition/computer_science.html), término que describe cómo se almacenan las [fechas](https://techlib.fr/definition/data.html). En concreto, este término define qué extremidad de un multi-byte [Tipos de datos](https://techlib.fr/definition/datatype.html) contiene los valores más significativos. Los dos tipos de endian son el big-endian y el little-endian.
* **EmulationStation** _(o ES)_ : La interface front-end utilizada por Recalbox para gestionar y acceder a los juegos.
* **Emulador** : Programa informático que imita el comportamiento de un hardware específico.

## F

* **Front-end** :

## G

* **Gamelist** _(o gamelist.xml)_ : Fichero específico a cada sistema émulado que contiene una lista de múltiples informaciones sobre cada juego en formato XML.
* **Gitlab** : 
* **GPI** :
* **GPIO** _(ou General Purpose Input/Output)_ : contactos metálicos que se encuentran en los Raspberry Pi y que sirven para conectar botones y otros controles a Recalbox.

## H

* **Hash** : Funcionalidad que calcula los checksums de las roms para el Netplay.
* **Hotkey** : Botón utilizado en combinación con otros botones/teclas para ejecutar atajos de teclado y otras funcionalidades relacionadas con el emulador y/o le sistema Recalbox.

## I

* **Interface** :
* **Iso** _(ou .iso)_ : Fichero image de un CD o disco duro.
* **Issue** : En el caso de Gitlab, un issue representa un informe de bug o una petición para añadir funcionalidades. Se pueden consultar todos los issues en curso o crear nuevos issues siguiendo [este enlace](https://gitlab.com/recalbox/recalbox/-/issues).

## K

* **Kodi** : Lector multimédia incluido en Recalbox.

## L

* **Librero** : API multiplataforma utilizada por Retroarch.
* **Licencia** : Una licencia define las cosas que se pueden hacer con un programa. La licencia de Recalbox se puede leer [aquí](https://gitlab.com/recalbox/recalbox/-/blob/master/LICENSE.md). Hay que tener en cuenta que Recalbox utiliza otros programas, que tambien están sujetos a licencia.
* **Link-Cable** : Un tipo de cable utilizado para establecer una conexión entre dos sistemas portables para poder jugar a varias personas en los juegos que lo soportan. Algunos emuladores son capaces de imitar el comportamiento de un link-cable.
* **Little Endian** : Sistema que almacena en primer lugar el valor menos significativo. Por ejemplo, el número 123 en notación little-endian es [321]. El texto [cadenas](https://techlib.fr/definition/string.html) "ABC" est représenté par [CBA].

## M

* **MAME** : Multiple Arcade Machine Emulator es un emulador gratuito y de código abierto concebido para recrear el material hardware de los sistemas de juegos de arcade de antaño en los ordenadores modernos.
* **MD5** : un tipo de checksum utilizado por las BIOS.
* **MITM** : _(o Man In The Middle)_ : Configuración utilizada por el Netplay en la que se utiliza un servidor que sirve para sincronizar una partida multiplayer. Los jugadores se conectan a este servidor en lugar de conectarse directamente entre ellos. El incoveniente de usar una conexión intermedia es la perdida de fiabilidad y de velocidad.
* **Multitap** : Periférico que sirve para aumentar el número de mandos que se pueden conectar directamente a una consola. Permite obtener un modo de 4 jugadores en sistemas que cuentan con dos puertos para los mandos (en los juegos que lo soportan). Algunos emuladores son capaces de simular este comportamiento.

## N

* **Netplay** : Una tecnología que permite jugar en red, en el caso de Recalbox, esta tecnología permite jugar a juegos multijugadores à traves de internet.
* **No-intro** : Nombre de un grupo de activistas que cataloga las roms. Están especialicados en los juegos de cartuchos y tienen por objetivo preservar los juegos haciendo dumps lo más fiables posibles à la obra original, sin modificaciones.
* **Romsets de referencia** : son conjuntos de roms principalmente para los sistemas que utilizan **cartuchos**, principalmente utilizados para el Netplay.
* **NOOBS** : (New Out Of the Box Software) aunque en inglés parece una palabra peyorativa, se trata de una herramienta que permite instalar una lista de sistemas operativos de una forma fiable y rápida. NOOBS es une gestor de instalacion de sistemas operativos para Raspberry Pi. Ha sido concebido para simplificar esta tarea al máximo e instalar un sistema operativo de la forma más sencilla posible en sólo unos clics de ratón.

## O

* **OCR** : El **reconocimiento óptico de caracteres** (ROC), en ingles `optical character recognition`, o ocerización, designa los procedimientos informáticos para la tradución de imágenes de textos impresos o escritos a mano en ficheros informáticos de texto.
* **Open-source** : término que se aplica a los programas informáticos cuya [licence](https://fr.wikipedia.org/wiki/Licence_de_logiciel) respeta lso criterios establecidos por la [Open Source Initiative](https://fr.wikipedia.org/wiki/Open_Source_Initiative), es decir, que permite la libre distribución y el libre acceso al [código fuente](https://fr.wikipedia.org/wiki/Code_source) y que permite también la creación de productos derivados. El código fuente puesto a disposición bajo esta licencia para todo el mundo es generalemente el fruto de una colaboración entre varios programadores. 
* **OS** : Ver **Sistema operativo**.
* **Overlays** : Imagen que aparece alrededor de un juego, sin formar parte de él, principalmente utilizadas para rellenar el espacio vacío y por razones puramente cosméticas.

## P

* **PBP** _(o .pbp)_ : Ficheros de imagen que corresponden al formato UMD de la Playstation Portable, o a juegos multidiscos de la Playstation.
* **Pi** : Ver **Raspberry Pi**.
* **Plug and Play** : Tambien conocido bajo el acrónimo de **PnP**, se trata de "conectar y jugar" o de "enchufar y utilizar". Es un término que permite a los sistemas operativos reconocer automaticamente los periféricos compatibles con esta tecnología en cuanto se enchufan, sin necesidad de reiniciar el ordenador. El Plug and Play permite una puesta en funcionamiento automática sin ninguna configuración por parte del usuario, sin instalar ningun driver y minimizando así los errores de manipulación y de configuración.

## R

* **Raspberry Pi** : 
* **Rebuild** : 
* **Recalbox** : El sistema operativo en sí mismo, basado en Buildroot System From Scratch.
* **Recalbox.conf** : Fichero que contiene la mayoría de la configuración de Recalbox y de los diferentes emuladores. Ver ["El fichero recalbox.conf"](./../basic-usage/getting-started/recalboxconf-file).
* **Redump** : Nombre de un grupo de activistas que cataloga las roms. Están especializados en los juegos de CD/disco.
* **Reloaded** : Nombre de la version 7 de Recalbox.
* **Retroachievements** : Un servicio de internet que permite activar un sistema de recompensas en ciertos juegos y sistemas.
* **RetroArch** : El front-end Libretro utilizado por varios emuladores de Recalbox. Ver [RetroArch](./../advanced-usage/retroarch/).
* **Rewrind** : Una configuración que permite en algunos emuladores de rebobinar. Su activación implica una ligera pérdida de rendimiento del emulador en cuestión.
* **Roms** : Ficheros de imágenes de los juegos.
* **Romset** : Un grupo concreto de roms utilizado sobre todo en el Netplay y en el Arcade.

## S

* **Savesstates** : Funcionalidad que permite grabar el estado del emulador, lo que permite recuperar la partida en el punto exacto de la grabación, incluso si el juego emulado no soporta las grabaciones de forma nativa. Es importate señalar que las grabacaciones tradicionales de un juego y los savestates pueden entrar en conflito entre ellas.
* **Samba** : SMB (Server Message Block). Funcionalidad que permite compartir carpetas e impresoras entre varios ordenadores conectados a la misma red local.
* **Scraper** : Programa informático capaz de recuperar los metadatos de los juegos en internet y de grabarlos en un formato compatible con EmulationStation.
* **Shaders** : Filtros que alteran la visualización de un juego en contrapartida de una bajada del rendimiento debida al tratamiento de las imagénes. Los shaders se utilizan principalmente por razones cosméticas.
* **Share** : Partición de Recalbox, accesible a través de la red.
* **Suma de control** : Ver _Checkbox_
* **SSH** :
* **Sobrecarga** : Ficheros que permiten saltar la configuración predeterminada de ciertos juegos o de ciertas carpetas. Ver [Surcharge de configuration](./../advanced-usage/configuration-override/).
* **Archivo de soporte** _(o Support Archive)_ : Opción del Webmanager que permite generar un lien de debugage que se puede enviar a los desarrolladores Recalbox en caso de problemas.
* **Sistema operativo** : Tambien llamado OS (del inglés Operating System), es un conjunto de programas informáticos que gestionan la utilización de los recursos hardware de la máquina y que permiten compartir estos mismos recursos entre diferentes programas tambien llamados "aplicaciones".

## T

* **Temas** : Ficheros que definen la apariencia global de EmulationStation.

## U

* **UMD** :

## V

* **Verbose** : Para Recalbox se trata de un modo de arranque que muestra informaciones detalladas en la pantalla. Se utiliza para debugar el proceso de arranque y el sistema cuando hay errores en el inicio del mismo.

## W

* **Webmanager** : Una interfaz web de Recalbox que permite aplicar diferentes configuraciones de una forma sencilla a través de un navegador HTML.

## X

* **Xin-Mo** :