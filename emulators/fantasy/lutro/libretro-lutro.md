---
title: Libretro Lutro
description: 
published: true
date: 2024-07-24T20:50:58.498Z
tags: libretro, lutro
editor: markdown
dateCreated: 2021-07-04T17:58:00.247Z
---

**Lutro** is an experimental lua game framework that follows the LÖVE API. Lutro games can be played with LibRetro / RetroArch via the Lutro core.

## ![](/emulators/license.svg) License

This core is under [**MIT**](https://github.com/libretro/libretro-lutro/blob/master/LICENSE) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Features

| Feature | Supported |
| :---: | :---: |
| Controls | ✔ |
| Remapping | ✔ |

## ![](/emulators/bios.svg) BIOS

>**No bios is required.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Supported extensions

Roms must have the extension:

* .lutro
* .lua

### Location

Put your roms like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 lutro
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.lutro**

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>**This core have no option.**
{.is-success}

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/libretro/libretro-lutro](https://github.com/libretro/libretro-lutro)
* **Libretro documentation**: [https://docs.libretro.com/library/lutro/](https://docs.libretro.com/library/lutro/)