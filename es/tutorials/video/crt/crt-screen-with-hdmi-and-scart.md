---
title: Conectad vuestra Recalbox a un CRT a travès de HDMI y SCART
description: 
published: true
date: 2023-09-12T14:59:09.305Z
tags: crt, hdmi, scart, pantalla
editor: markdown
dateCreated: 2023-09-12T14:58:24.717Z
---

## Principios

### Cableado

En primer lugar, deberéis crear el super-cable para convertir el VGA en SCART (Peritel).  
Gracias a @ian57 por [este mensaje](https://forum.recalbox.com/topic/3475/recalbox-sur-tv-crt-en-rgb/745) !

Hemos utilizado esto (câble HDMI VGA SCART) en una máquina recreativa con una Tv de 36 cm, y no ha habido ningún problema.

Aquí está el esquema del cableado a la salida del convertidor VGA.

![](/tutorials/video/crt/ac9ba3d3-29fc-4ff2-8644-aee71b62f043.png)

En cuanto a las configuraciones, es muy diferente de VGA666 / pi2scart / rgbPi (los 3 utilizan DPI en modo 18 bits (6 bits/color)).

En el fichero [recalbox-user-config.txt](./../../../tutorials/system/modification/configtxt-configuration), configuramos como si tuviéramos una pantalla 1920x240 :

```ini
hdmi_cvt=1920 240 60 1 1 0 0
hdmi_disable_edid=0xa5000080 #Enables the ignoring of EDID/display data if your display doesn't have an accurate EDID.

hdmi_pixel_encoding=2 #Force the pixel encoding mode. By default it will use the mode requested from edid so shouldn't need changing. 
avoid_safe_mode=1
disable_overscan=0 #oversan enabled
hdmi_drive=2
hdmi_group=1
hdmi_mode=6
hdmi_force=1
```

En el fichero [recalbox.conf](./../../../basic-usage/getting-started/recalboxconf-file)), forzamos EmulationStation para que aparezca en el modo 480 entrelazado (hace un poquito de daño a los ojos); de otra forma podemos utilizar el modo `CEA 8 HDMI` en 240 no entrelazado, pero haría falta un theme adaptado a esta resolución tan pequeña.

```ini
system.es.videomode=CEA 6 HDMI
 
#global.videomode=CEA 4 HDMI
global.videomode=DMT 87 HDMI
```

A continuación, tienes que decirle a Libretro para cada emulador que utilice sólo una parte de la ventana de 1920x240.  
Para ello, hace falta [crear un fichero](https://forum.recalbox.com/topic/18927/recalbox-6-1-sur-%C3%A9cran-crt/9) de configuración para cada emulador y depositarlo dentro de `/share/system/configs/retroarch`.

Por ejemplo, el fichero `megadrive.cfg`

```ini
aspect_ratio_index = "23"
custom_viewport_width = "1792"
custom_viewport_height = "224"
custom_viewport_x = "104"
custom_viewport_y = "16"
```

Las 4 últimas líneas deben adaptarse a la geometría de la pantalla. Puedes encontrar los valores ajustándolos en el menú de RetroArch.

Si no se hace esto, la imagen del emulador quedará completamente aplastada en el centro de la pantalla.

## Enlaces útiles para la configuración del núcleo de RetroArch

[Este enlace](https://surchargeur-ra-rb.netlify.app/) os permite generar fácilmente ficheros de configuración para RetroArch a partir de una interfaz web.