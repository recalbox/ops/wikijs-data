---
title: Amiga 600
description: 
published: true
date: 2024-07-26T12:44:50.123Z
tags: amiga-600, amiga, 600, commodore, ordenadores
editor: markdown
dateCreated: 2023-09-02T18:30:31.119Z
---

![](/emulators/computers/amiga600.svg){.align-center}

## Datos técnicos

* **Fabricante**: Commodore
* **Año de lanzamiento**: 1992
* **Sistema operativo**: AmigaOS 2.05
* **Procesador**: Motorola 68000 a 7,09 (PAL) o 7,16 MHz (NTSC)
* **RAM**: 1 MB (hasta 6 MB oficialmente)
* **VRAM**: 1024 KB
* **ROM**: 512 KB Kickstart 2.05
* **Procesador gráfico**: Enhanced Chip Set de Commodore
* **Chip de sonido**: 8 bits y 4 canales DMA
* **Resolución**: 320x200 a 1280x512 (4096 colores en resoluciones inferiores)

## Presentación

El **A600**, también conocido como **Amiga 600**, fue el último ordenador de la línea A500. Lanzado a finales de 1991, era esencialmente un A500 renovado. La intención de Commodore International era impulsar las ventas del A500 antes de que estuviera disponible el nuevo y más sofisticado A1200.

La característica más notable del A600 era su pequeño tamaño. Sin el teclado numérico, el A600 sólo medía 35 cm de largo por 24 cm de fondo, 8 cm de alto y pesaba aproximadamente 3 kg.

Venía con Kickstart/Workbench v2, que era mucho más práctico y atractivo que su predecesor. Estaba destinado al mercado básico, mientras que el ordenador de gama más alta de la gama en aquel momento era el A3000. El A600 utilizaba el procesador Motorola 68000 a 7,09 MHz (PAL) o 7,16 MHz (NTSC).

La memoria estándar era de 1 MB. Sin embargo, mucha gente la aumentó hasta un máximo de 2 MB de chip RAM. Se podían añadir hasta 4 MB de RAM rápida, utilizando el puerto PCMCIA.

El diseño original no permitía cambiar el procesador, ya que el 68000 estaba soldado a la placa base. A pesar de ello, los fabricantes de tarjetas de expansión ofrecían actualizaciones que incluían el Motorola 68010, 68020 (hasta 25 MHz) y 68030 (hasta 50 MHz). En algunas tarjetas se podían añadir hasta 32 MB de RAM rápida.

Sus conectores más interesantes son quizás el puerto PCMCIA Tipo II, y la interfaz IDE interna (para el disco duro de 2,5" de 20 ó 40 MB). El modelo con el disco duro IDE incorporado de 20 o 40 MB se vendía en los años 90 a casi el doble de precio que un A600 estándar, y se llamaba "A600HD". El resto de los periféricos son similares a los del A500.

En la década de 2010, se siguen fabricando tarjetas aceleradoras basadas en 680202 y 680303. La presencia del puerto PCMCIA, que permite añadir una tarjeta Compact Flash mediante un adaptador, lo convierte en uno de los modelos de Amiga más populares para retrogaming, junto con el Amiga 1200.

## Emuladores

[Amiberry](amiberry)
[Libretro PUAE](libretro-puae)
[Libretro UAE4ALL](libretro-uae4all)
[Libretro UAE4ARM](libretro-uae4arm)