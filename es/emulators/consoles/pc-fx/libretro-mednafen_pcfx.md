---
title: Libretro Mednafen_PCFX
description: 
published: true
date: 2024-07-05T15:17:26.192Z
tags: libretro, mednafen, pc-fx, pcfx
editor: markdown
dateCreated: 2023-09-07T16:09:16.221Z
---

**Libretro Mednafen_PCFX** es una adaptación del emulador Mednafen PC-FX para la NEC PC-FX.

## ![](/emulators/license.svg) Licencia

El core posee la siguiente licencia [**GPLv2**](https://github.com/libretro/beetle-pcfx-libretro/blob/master/COPYING).

## ![](/emulators/compatibility.png) Compatibilidad

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | 
| ❌ | ❌ | ✅ | ✅ | ✅ | ❌ | ✅ | ✅ |

## ![](/emulators/features.png) Funcionalidades

| Funcionalidad | ¿Soportada? |
| :---: | :---: |
| Reinicio | ✔ |
| Capturas de pantalla | ✔ |
| Grabaciones | ✔ |
| Grabaciones instantáneas | ✔ |
| Rebobinado | ✔ |
| Netplay | ✔ |
| Opciones del core | ✔ |
| RetroAchievements | ✔ |
| Controles | ✔ |
| Remapping | ✔ |
| Crop Overscan | ✔ |

## ![](/emulators/bios.svg) BIOS

### Liste des bios obligatoires

| Nom de fichier | Description | MD5 | Fourni |
| :---: | :---: | :---: | :---: |
| pcfx.rom | BIOS PC-FX v1.00 - 2 Sep 1994 | 08e36edbea28a017f79f8d4f7ff9b6d7 | ❌  |

### Emplazamiento

Disponed vuestras bios como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 pcfx
┃ ┃ ┃ ┃ ┣ 🗒 **pcfx.rom**

## ![](/emulators/isos.svg) Isos

### Extensiones de roms soportadas

Las isos deben tener las extensiones siguientes:

* .cue
* .ccd
* .toc
* .chd

### Emplazamiento

Disponed vuestras roms como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 pcfx
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.cue**

>Se recomiendan las isos del proyecto **Redump**.
{.is-success}

>Para más información sobre las roms, consultad [este tutorial](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

>Para conservar las configuraciones personalizadas cuando se produce una actualización del sistema Recalbox, os aconsejamos utilizar las [Sobrecargas de configuración](./../../../advanced-usage/configuration-override).
{.is-info}


### Acceder a las opciones

Podéis configurar las opciones del core de dos formas diferentes.

* Via el Menú RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via el fichero `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Opciones del core

| Opción | Valor de la opción | Variable (para la sobrecarga) | Valores posibles |
| --- | --- | --- | --- |
| Mise en cache des images CD (Redémarrage requis) | `Désactivé` ✅ / `Activé` | `pcfx_cdimagecache` | `disabled` / `enabled` |
| Grande largeur d'horloge pour les pixels (Redémarrage requis) | `256` / `341` / `1024` ✅ | `pcfx_high_dotclock_width` | `256` / `341` / `1024` |
| Supprimer les clics de réinitialisation du canal (Redémarrage requis) | `Activé` ✅ / `Désactivé` | `pcfx_suppress_channel_reset_clicks` | `enabled` / `disabled` |
| Émuler le Codec buggé (Redémarrage requis) | `Désactivé` ✅ / `Activé` | `pcfx_emulate_buggy_codec` | `disabled` / `enabled` |
| Qualité du son (Redémarrage requis) | `0` / `1` / `2` / `3` ✅ / `4` / `5` | `pcfx_resamp_quality` | `0` / `1` / `2` / `3` / `4` / `5` |
| Interpolation bilinéaire du canal chromatique (Redémarrage requis) | `Désactivé` ✅ / `Activé` | `pcfx_rainbow_chromaip` | `disabled` / `enabled` |
| Aucune limite de sprites (Redémarrage requis) | `Désactivé` ✅ / `Activé` | `pcfx_nospritelimit` | `disabled` / `enabled` |
| Ligne de balayage initiale | `0` / `1` / `2` / `3` / `4` ✅ / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `21` / `22` / `23` / `24` / `25` / `26` / `27` / `28` / `29` / `30` / `31` / `32` / `33` / `34` / `35` / `36` / `37` / `38` / `39` / `40` | `pcfx_initial_scanline` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `21` / `22` / `23` / `24` / `25` / `26` / `27` / `28` / `29` / `30` / `31` / `32` / `33` / `34` / `35` / `36` / `37` / `38` / `39` / `40` |
| Dernière ligne de balayage | `208` / `209` / `210` / `211` / `212` / `213` / `214` / `215` / `216` / `217` / `218` / `219` / `220` / `221` / `222` / `223` / `224` / `225` / `226` / `227` / `228` / `229` / `230` / `231` / `232` / `233` / `234` / `235` ✅ / `236` / `237` / `238` / `239` | `pcfx_last_scanline` | `208` / `209` / `210` / `211` / `212` / `213` / `214` / `215` / `216` / `217` / `218` / `219` / `220` / `221` / `222` / `223` / `224` / `225` / `226` / `227` / `228` / `229` / `230` / `231` / `232` / `233` / `234` / `235` / `236` / `237` / `238` / `239` |
| Sensibilité de la souris | `0.25` / `0.50` / `0.75` / `1.00` / `1.25` ✅ / `1.50` / `1.75` / `2.00` / `2.25` / `2.50` / `2.75` / `3.00` / `3.25` / `3.50` / `3.75` / `4.00` / `4.25` / `4.50` / `4.75` / `5.00` | `pcfx_mouse_sensitivity` | `0.25` / `0.50` / `0.75` / `1.00` / `1.25` / `1.50` / `1.75` / `2.00` / `2.25` / `2.50` / `2.75` / `3.00` / `3.25` / `3.50` / `3.75` / `4.00` / `4.25` / `4.50` / `4.75` / `5.00` |


## ![](/emulators/external-links.png) Enlaces externos

* **Código fuente utilizado** : [https://github.com/libretro/beetle-pcfx-libretro/](https://github.com/libretro/beetle-pcfx-libretro/)
* **Documentación Libretro** : [https://docs.libretro.com/library/beetle_pc_fx/](https://docs.libretro.com/library/beetle_pc_fx/)