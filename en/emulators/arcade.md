---
title: Arcade
description: Arcade systems
published: true
date: 2021-08-08T23:53:54.657Z
tags: arcade
editor: markdown
dateCreated: 2021-06-29T16:55:58.827Z
---

![](/emulators/arcade/arcade-logo.png)

![](/emulators/arcade/arcade-bartop.svg)

An arcade game or cash game is a coin-operated entertainment. Most arcade games are video games, pinball machines, electro-mechanical games, trading games or merchandise.

While the exact dates are debated, the golden age of arcade video games is generally defined as a period beginning in the late 1970s and ending in the mid 1980s.

Apart from a brief resurgence in the early 1990s, the arcade industry then declined in the Western Hemisphere as the number of video game consoles competing in graphics and gaming capabilities increased and costs decreased.