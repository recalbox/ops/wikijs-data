---
title: 7. 📋 A PROPÓSITO DE RECALBOX
description: Cómo contribuir, licencias, enlaces útiles...
published: true
date: 2023-08-31T14:20:18.206Z
tags: 
editor: markdown
dateCreated: 2021-08-02T17:36:47.648Z
---

Aquí encontrarás la presentación general de Recalbox, quiénes somos, las licencias y la posibilidad de contribuir al proyecto de diferentes maneras.