---
title: 🔖 Troubleshooting information
description: 
published: true
date: 2021-08-06T15:15:29.031Z
tags: troubleshoot, tutorial
editor: markdown
dateCreated: 2021-08-06T08:02:57.588Z
---

This place is for the more experienced among you, based on SSH command lines. You will be able to execute some useful commands if you are asked.

Here are the available tutorials:

[Display buffer memory on Linux](display-buffer-memory-linux)
[Grab controllers informations](grab-controllers-informations)
[List loaded modules](list-loaded-modules)
[Support script](support-script)