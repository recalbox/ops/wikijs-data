---
title: Libretro bsnes
description: 
published: true
date: 2024-07-25T22:04:29.503Z
tags: libretro, game, boy, gameboy, bsnes, 8.1+, gb
editor: markdown
dateCreated: 2023-09-11T12:54:02.799Z
---



## ![](/emulators/license.svg) Licencia

El core posee la siguiente licencia [**GPLv3**](https://github.com/libretro/bsnes-libretro/blob/master/LICENSE.txt).

## ![](/emulators/compatibility.png) Compatibilidad

 | RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | 
 | ❌ | ❌ | ❌ | ❌ | ❌ | ❌ | ✅ | ❌ |

## ![](/emulators/features.png) Funcionalidades

| Funcionalidad | ¿Soportada? |
| :---: | :---: |
| Reinicio | ✔ |
| Capturas de pantalla | ✔ |
| Grabaciones | ✔ |
| Grabaciones instantáneas | ✔ |
| Rebobinado | ✔ |
| Netplay | ✔ |
| RetroAchievements | ✔ |
| Cheats RetroArch | ✔ |
| Opciones del core | ✔ |
| Controles | ✔ |
| Remapping | ✔ |
| Sous-systèmes | ✔ |
| Crop Overscan | ✔ |

## ![](/emulators/bios.svg) BIOS

### Lista de BIOS obligatorias

| Nombre del fichero | Descripción | MD5 | ¿Preinstalada? |
| :---: | :---: | :---: | :---: |
| SGB1.sfc | Super Game Boy BIOS | b15ddb15721c657d82c5bab6db982ee9 | ❌ |
| SGB2.sfc | Super Game Boy 2 BIOS | 8ecd73eb4edf7ed7e81aef1be80031d5 | ❌ |

### Emplazamiento

Disponed vuestras bios como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 sgb
┃ ┃ ┃ ┃ ┣ 🗒 **SGB1.sfc**
┃ ┃ ┃ ┃ ┣ 🗒 **SGB2.sfc**

## ![](/emulators/roms.png) Roms

### Extensiones de roms soportadas

Las roms deben tener las extensiones siguientes:

* .gb
* .zip
* .7z

Este sistema soporta las roms comprimidas en formato .zip/.7z. No obstante, se trata sólo de un archivo.

Los ficheros contenidos en los .zip/.7z deben tener las extensiones citadas previamente.
Además, cada fichero .zip/.7z sólo debe contener un fichero rom al interior.

### Emplazamiento

Disponed vuestras roms como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 gb
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.zip**

>Se aconsejan las roms del proyecto **No-Intro**.
{.is-success}

>Para más información sobre las roms, consultad [este tutorial](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

>Para conservar las configuraciones personalizadas cuando se produce una actualización del sistema Recalbox, os aconsejamos utilizar las [Sobrecargas de configuración](./../../../advanced-usage/configuration-override).
{.is-info}

### Acceder a las opciones

Podéis configurar las opciones del core de dos formas diferentes.

* Via el Menú RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via el fichero `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Opciones del core

| Opción | Valor de la opción | Variable (para la sobrecarga) | Valores posibles |
| --- | --- | --- | --- |
| Rapport d'aspect préféré | `Auto` ✅ / `Pixels parfaits à l'échelle` / `4:3` / `NTSC` / `PAL` | bsnes_aspect_ratio | `Auto` / `8:7` / `4:3` / `NTSC` / `PAL` |
| Recadrage du surbalayage | `8 pixels`✅ / `Désactivé` | bsnes_ppu_show_overscan | `rrr` / `disabled` |
| Émulation du flou | `Désactivé` ✅ / `Activé` | bsnes_blur_emulation | `disabled` / `enabled` |
| Filtre | `Aucune` ✅ / `NTSC (RF)` / `NTSC (Composite)` / `NTSC (S-Video)` / `NTSC (RVB)` | `bsnes_video_filter` | `None` / `NTSC (RF)` / `NTSC (Composite)` / `NTSC (S-Video)` / `NTSC (RGB)` |
| PPU - mode rapide | `Activé` ✅ / `Désactivé` | bsnes_ppu_fast | `enabled` / `disabled` |
| PPU - désentrelacer | `Activé` ✅ / `Désactivé` | bsnes_ppu_deinterlace | `enabled` / `disabled` |
| PPU - aucune limite de sprites | `Désactivé` ✅ / `Activé` | bsnes_ppu_no_sprite_limit | `disabled` / `enabled` |
| PPU - pas de blocage VRAM | `Désactivé` ✅ / `Activé` | bsnes_ppu_no_vram_blocking | `disabled` / `enabled` |
| DSP - mode rapide | `Activé` ✅ / `Désactivé` | bsnes_dsp_fast | `enabled` / `disabled` |
| DSP - interpolation cubique | `Désactivé` ✅ / `Activé` | bsnes_dsp_cubic | `disabled` / `enabled` |
| DSP - RAM de l'Echo séparée | `Désactivé` ✅ / `Activé` | bsnes_dsp_echo_shadow | `disabled` / `enabled` |
| Échelle | `240p (x1)` / `480p (x2)` ✅ / `720p (x3)` / `960p (x4)` / `1200p (x5)` / `1440p (x6)` / `1680p (x7)` / `1920p (x8)` | bsnes_mode7_scale | `1x` / `2x` / `3x` / `4x` / `5x` / `6x` / `7x` / `8x` |
| Correction de la perspective | `Activé` ✅ / `Désactivé` | bsnes_mode7_perspective | `enabled` / `disabled` |
| Suréchantillonnage | `Désactivé` ✅ / `Activé` | bsnes_mode7_supersample | `disabled` / `enabled` |
| Mosaïque HD->SD | `Activé` ✅ / `Désactivé` | bsnes_mode7_mosaic | `enabled` / `disabled` |
| Exécution en avance interne | `Désactivé ✅ / 1 / 2 / 3 / 4` | bsnes_run_ahead_frames | `disabled`/ `1` / `2` / `3` / `4` |
| Coprocesseurs - mode rapide | `Activé` ✅ / `Désactivé` | bsnes_coprocessor_delayed_sync | `enabled` / `disabled` |
| Coprocesseurs - préférer le mode HLE | `Activé` ✅ / `Désactivé` | bsnes_coprocessor_prefer_hle | `enabled` / `disabled` |
| Correctifs | `Désactivé` ✅ / `Activé` | bsnes_hotfixes | `disabled` / `enabled` |
| Entropie (aléatoire) | `Faible` ✅ / `Élevée` / `Aucune` | bsnes_entropy | `Low` / `High` / `None` |
| Calculs rapides du processeur | `Désactivé` ✅ / `Activé` | bsnes_cpu_fastmath | `disabled` / `enabled` |
| Processeur | `10%` / `20%` / `30%` / `40%` / `50%` / `60%` / `70%` / `80%` / `90%` / `100% (par défaut)` ✅ / `110%` / `120%` / `130%` / `140%` / `150%` / `160%` / `170%` / `180%` / `190%` / `200%` / `210%` / `220%` / `230%` / `240%` / `250%` / `260%` / `270%` / `280%` / `290%` / `300%` / `310%` / `320%` / `330%` / `340%` / `350%` / `360%` / `370%` / `380%` / `390%` / `400%` | bsnes_cpu_overclock | `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` / `11%` / `120` / `130` / `140` / `150` / `160` / `170` / `180` / `190` / `200` / `210` / `220` / `230` / `240` / `250` / `260` / `270` / `280` / `290` / `300` / `310` / `320` / `330` / `340` / `350` / `360` / `370` / `380` / `390` / `400` |
| Coprocesseur SA-1 | `10%` / `20%` / `30%` / `40%` / `50%` / `60%` / `70%` / `80%` / `90%` / `100% (par défaut)` ✅ / `110%` / `120%` / `130%` / `140%` / `150%` / `160%` / `170%` / `180%` / `190%` / `200%` / `210%` / `220%` / `230%` / `240%` / `250%` / `260%` / `270%` / `280%` / `290%` / `300%` / `310%` / `320%` / `330%` / `340%` / `350%` / `360%` / `370%` / `380%` / `390%` / `400%` | bsnes_cpu_sa1_overclock | `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` / `11%` / `120` / `130` / `140` / `150` / `160` / `170` / `180` / `190` / `200` / `210` / `220` / `230` / `240` / `250` / `260` / `270` / `280` / `290` / `300` / `310` / `320` / `330` / `340` / `350` / `360` / `370` / `380` / `390` / `400` |
| Coprocesseur SuperFX | `10%` / `20%` / `30%` / `40%` / `50%` / `60%` / `70%` / `80%` / `90%` / `100% (par défaut)` ✅ / `110%` / `120%` / `130%` / `140%` / `150%` / `160%` / `170%` / `180%` / `190%` / `200%` / `210%` / `220%` / `230%` / `240%` / `250%` / `260%` / `270%` / `280%` / `290%` / `300%` / `310%` / `320%` / `330%` / `340%` / `350%` / `360%` / `370%` / `380%` / `390%` / `400%` / `410%` / `420%` / `430%` / `440%` / `450%` / `460%` / `470%` / `480%` / `490%` / `500%` / `510%` / `520%` / `530%` / `540%` / `550%` / `560%` / `570%` / `580%` / `590%` / `600%` / `610%` / `620%` / `630%` / `640%` / `650%` / `660%` / `670%` / `680%` / `690%` / `700%` / `710%` / `720%` / `730%` / `740%` / `750%` / `760%` / `770%` / `780%` / `790%` / `800%` | bsnes_cpu_sfx_overclock | `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` / `11%` / `120` / `130` / `140` / `150` / `160` / `170` / `180` / `190` / `200` / `210` / `220` / `230` / `240` / `250` / `260` / `270` / `280` / `290` / `300` / `310` / `320` / `330` / `340` / `350` / `360` / `370` / `380` / `390` / `400` / `410` / `420` / `430` / `440` / `450` / `460` / `470` / `480` / `490` / `500` / `510` / `520` / `530` / `540` / `550` / `560` / `570` / `580` / `590` / `600` / `610` / `620` / `630` / `640` / `650` / `660` / `670` / `680` / `690` / `700` / `710` / `720` / `730` / `740` / `750` / `760` / `770` / `780` / `790` / `800` |
| BIOS préféré du Super Game Boy (Redémarrage requis) | `SGB1.sfc` ✅ / `SGB2.sfc` | bsnes_sgb_bios | `Super Game Boy (SGB1.sfc)` / `Super Game Boy 2 (SGB2.sfc)` |
| Masquer la bordure du SGB | `Désactivé` ✅ / `Activé` | bsnes_hide_sgb_border | `disabled` / `enabled` |
| Pistolet écran tactile | `Activé` ✅ / `Désactivé` | bsnes_touchscreen_lightgun | `enabled` / `disabled` |
| Inverser les touches de tir du Super Scope | `Désactivé` ✅ / `Activé` | bsnes_touchscreen_lightgun_superscope_reverse | `disabled` / `enabled` |


## ![](/emulators/external-links.png) Enlaces externos

* **Código fuente utilizado** : [https://github.com/libretro/bsnes-libretro/](https://github.com/libretro/bsnes-libretro/)
* **Documentación Libretro** : [https://docs.libretro.com/library/bsnes_performance](https://docs.libretro.com/library/bsnes_performance)