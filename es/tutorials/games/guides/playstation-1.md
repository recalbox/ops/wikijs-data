---
title: Playstation 1
description: 
published: true
date: 2024-07-26T07:06:44.073Z
tags: sony, ps1, playstation 1, psx, juegos
editor: markdown
dateCreated: 2023-09-14T08:00:25.683Z
---

Tutoriales relacionados con la PlayStation.

Aqui tenéis la lista de tutoriales disponibles:

[Activar el modo analog](enable-analog-mode)
[Convertir las imágenes de disco .BIN + .CUE](convert-disc-files-to-bin+cue)
[Convertir las imágenes de disco .BIN en .ISO](convert-psx-bin-to-iso)
[Convertir una grabación .GME en .SRM](convert-gme-save-to-srm)
[FAQ](faq)
[Gestión de las cartas de memoria](memory-cards-management)
[Gestión de multi-discos con el formato .M3U](multidisc-management-with-m3u)
[Gestión de multi-discos con el formato .PBP](multidisc-management-with-pbp)
[Recuperar la bios ps1_rom.bin desde el firmware PS3](grab-ps1-bios-from-ps3-firmware)
[Utilizar vuestras grabaciones ePSXe (.MCR) en RetroArch (.SRM)](use-epsxe-mcr-saves-into-srm)
[Volcad vuestros propios juegos rápidamente](dump-you-own-games-quickly)