---
title: Multiboot Recalbox / Ubuntu / Windows
description: Pouvoir booter sur Recalbox / Ubuntu / Windows dans l'ordre que l'on souhaite !
published: false
date: 2023-09-19T20:02:07.616Z
tags: windows, multiboot, ubuntu
editor: markdown
dateCreated: 2021-11-09T22:20:40.336Z
---

![](/tutorials/others/multiboot/0000.png){.full-width}

## Introduction

Il existe dans l'univers de l'informatique plusieurs systèmes d'exploitation (OS) ayant chacun des avantages et inconvénients et pour vous pouvez avoir à en utiliser différents OS sur un poste de travail.

## Pourquoi ?

Afin d'avoir la possibilité de choisir l'OS à utiliser facilement et rapidement.

## Pré-requis

* Un PC sous Windows 10 avec un compte Windows "Administrateur" avec à minima un processeur 2GHz dual core et 4Gb de RAM.

* Une Clé USB Recalbox version PC déjà paramétrée et fonctionnelle.

* Télécharger Ubuntu en version LTS (de préférence) [ici](https://ubuntu.com/download/desktop) et graver le fichier .ISO téléchargé sur un DVD ou sur Clé USB.

>LTS => Long Time Support => En résumé, mise à jour supporté pendant 5 ans minimum.
{.is-info}

>Si votre disque Windows n'est pas un SSD mais un HDD : lancer une défragmentation afin d'obtenir le maximum de place lors de votre réduction de partition Windows.
{.is-warning}

>En de rares occasions. Il arrivera que le boot sélectionné n'aboutisse pas. Pas d'inquiétude redémarer votre PC. Et reselectionner votre Boot.
{.is-info}

## 1 - Réduction de la partition Windows 10

Let's Start the Party ;-)

Démarrez votre ordinateur sous Windows puis dans la barre de menu démarrer, tapez dans la zone de recherche `Gestion de l'ordinateur` et cliquez sur `Gestion de l'ordinateur`.

![](/tutorials/others/multiboot/1000.png){.full-width}

La fenêtre gestion de l'ordinateur va s'ouvrir. Cliquez sur `Gestion des disques` dans la colonne de gauche.

![](/tutorials/others/multiboot/1001.png){.full-width}

Faites un clic droit sur la partition (C:) la partition principale de Windows. Un menu déroulant va apparaître, cliquez sur `Réduire le volume...`.

![](/tutorials/others/multiboot/1002.png){.full-width}

Une nouvelle fenêtre `Requête de l'espace de comptage` apparaitra  vous invitant à patienter (temps d'attente plus ou moins rapide suivant votre PC).

![](/tutorials/others/multiboot/1003.png){.full-width}

La fenêtre `Réduire C:` apparait à son tour ci-dessous. Vous invitant à saisir la diminution de votre partition Windows (libres d'espace vierge sur votre disque dur). La taille minimale à libérer pour une installation Linux est de 32Go. Pour une utilisation confortable, nous vous proposons de réduire de 100Go dans mon exemple sur un disque dur de 500Go. 

Il faut donc saisir la valeur suivante en Mo soit `100000` et cliquer sur `Réduire` pour lancer le processus qui sera plus ou moins long suivant votre PC équipé d'un SSD ou d'un HDD.

![](/tutorials/others/multiboot/1004.png){.full-width}

Après la fin du processus, vous verrez apparaître une partition `Non alloué` de `97,66Go` ici.

![](/tutorials/others/multiboot/1005.png){.full-width}

Voilà, la première étape de réduction de partition est maintenant terminée. Avant d'arrêter votre PC, vous êtes invités à insérer votre clé USB ou CD d'installation Ubuntu.

## 2 - Installer Ubuntu

Démarrez votre système en bootant sur votre DVD Ubuntu ou votre support USB et attendez patiemment d'arriver à l'écran suivant. Puis sélectionnez la langue `Français` et cliquez sur `Installer Ubuntu`.

![](/tutorials/others/multiboot/2001.png){.full-width}

Puis à l'écran `Disposition du clavier`, laissez comme ci-dessous et cliquez sur `Continuer`.

![](/tutorials/others/multiboot/2002.png){.full-width}

Vous arriverez ci-dessous à l'écran `Mises à jour et autres logiciels`. Il vous est conseillé de cocher la case `Installer un logiciel tiers...` avant de cliquer sur `Continuer`. Vous gagnerez ainsi en automatisation si vous êtes novice sous Linux.

![](/tutorials/others/multiboot/2003.png){.full-width}

A l'écran `Type d'installation`, laissez cocher `Installer Ubuntu à côté de Windows 10`. Le logiciel d'installation détectera automatiquement la place non allouée récupérée précédemment au paragraphe `Réduction de la partition Windows`. Ensuite, cliquez sur `Installer maintenant`.

![](/tutorials/others/multiboot/2004.png){.full-width}

Vous verrez apparaître la fenêtre `Faut-il appliquer les changements sur les disques ?`. Cliquez sur `Continuer` pour permettre au logiciel d'installation d'écrire les nouvelles partitions sur votre disque sur dans la zone non alloué précédemment. Cette étape est nécessaire pour pouvoir finaliser l'installation d'Ubuntu.

![](/tutorials/others/multiboot/2005.png){.full-width}

À la fenêtre `Où êtes vous ?`, cliquez sur votre zone horaire ou laisser par défaut Paris et cliquer sur `Continuer`.

![](/tutorials/others/multiboot/2006.png){.full-width}

Pour la fenêtre `Qui êtes-vous ?`, on vous laisse le soin de mettre les renseignements de votre choix. Il faudra vous en souvenir pour les étapes suivantes.

>Ce compte sera votre compte administrateur par défaut sous Ubuntu.
{.is-warning}

![](/tutorials/others/multiboot/2007.png){.full-width}

L'installation débutera et durera plus ou moins longtemps suivant votre PC et votre disque dur. Je vous invite donc à lire les différents messages proposés pendant cette phase d'installation afin de vous familiariser avec les fonctionnalités d'Ubuntu.

![](/tutorials/others/multiboot/2007b.png){.full-width}

Une fois l'installation terminée, l'interface vous invitera à cliquer sur redémarrer maintenant. Cliquez sur `Redémarrer maintenant`.

![](/tutorials/others/multiboot/2008.png){.full-width}

L'interface vous invite à retirer le support d'installation. Exécutez cette demande en déconnectant votre clé USB ou en retirant votre DVD d'installation d'Ubuntu du lecteur puis appuyez sur `Entrée` de votre clavier.

![-](/tutorials/others/multiboot/2008b.png){.full-width}

Une fois votre PC redémarré, vous arriverez directement sur l'interface de sélection d'OS appelé GRUB comme affiché ci-dessous. Trois choix vous sont possibles :

1) Vous ne touchez à rien Ubuntu ce lancera automatiquement (à privilégier dans un premier temps);
2) vous utilisez les flèches haut et bas de votre clavier pour arrêter le timer;
3) Vous sectionnez Windows pour retourner sous Windows avant de continuer (ou pour faire une pause).

![](/tutorials/others/multiboot/2009.png){.full-width}

Vous arrivez sur l'écran d'identification, sélectionnez votre seul est unique compte et tapez votre mot de passe.

![](/tutorials/others/multiboot/2010.png){.full-width}

Voilà votre installation d'Ubuntu est maintenant terminé vous arriverez sur l'écran ci-dessous : votre bureau Ubuntu.

![](/tutorials/others/multiboot/2011.png){.full-width}

## 3 - Préparation des logiciels nécessaires sur Ubuntu

Démarrez votre PC sur Ubuntu. Identifiez-vous et récupérez les logiciels comme indiqué ci-dessous.

Cliquez sur l'icône `Ubuntu Software` dans la barre de menu à gauche de votre bureau.

![](/tutorials/others/multiboot/3001.png){.full-width}

Des mises à jour peuvent vous être demandés, laissez-vous guider et faites les mises à jour jusqu'à un éventuel redémarrage de l'ordinateur. Le tout est de retourner cliquer sur l'icone `Ubuntu Software` à la fin des ces mises à jour pour arriver sur la fenêtre `Ubuntu Software` ci-dessous (équivalent d'un AppStore, PlayStore ou Microsoft Store). Cliquez sur la loupe de recherche et tapez `Grub Cutomizer`.

![](/tutorials/others/multiboot/3002.png){.full-width}

![](/tutorials/others/multiboot/3004.png){.full-width}

Puis cliquez sur l'icone `Grub Cutomizer` et sur `Installer` dans la suivante et saisissez votre mot de passe pour valider.

![](/tutorials/others/multiboot/3005.png){.full-width}

Et quittez le fenêtre `Ubuntu Software`. Allez ensuite cliquer sur `Afficher les applications` en bas à gauche sur votre bureau.

![](/tutorials/others/multiboot/3007.png){.full-width}

Vous arriverez ensuite ci-dessous dans le menu de recherche de vos applications.

![](/tutorials/others/multiboot/3008.png){.full-width}

Visuellement ou grâce au menu recherche, recherchez les icônes `Grub Customizer` et `Terminal` en faisant un clic droit sur chacune pour les ajouter aux favoris.

![](/tutorials/others/multiboot/3009.png){.full-width}

Elles apparaitront dans la barre de menu latérale gauche de votre bureau.

![](/tutorials/others/multiboot/3011.png){.full-width}

## 4 - Sauvegarde de votre GRUB actuel

>Faites une copie du fichier grub.cfg original avant tout modification !
{.is-warning}

Cliquez sur l'icône du bureau qui porte le nom de votre login pour ouvrir une fenêtre d'exploration, puis `Autres emplacements`, puis `Ordinateur`, puis `Boot` et enfin `grub`.

![](/tutorials/others/multiboot/4000.png){.full-width}

Sauvegardez une copie du fichier `grub.cfg` sur clé USB, disque dur externe ou en vous envoyant le fichier par email.

## 5 - Organiser le GRUB (démarrage)

>A partir de ce point, il peut y avoir des différences entre votre menu et les menus ci-dessous en raison de la configurations des différents PC. On va donc vous expliquer la philosophie des manipulations à réaliser suivant l'exemple suivant pour aboutir au menu que vous souhaitez avoir.
{.is-info}

Nous allons dans un premier temps modifier l'ordre de démarrage que vous souhaitez avoir au démarrage de votre ordinateur.

Cliquez sur l'icone `Grub Customizer`, entrez votre mot de passe et patientez le temps que la fenêtre ci-dessous s'ouvre.

![](/tutorials/others/multiboot/4001.png){.full-width}

Ci-dessous les correspondances avec le menu de démarrage de votre PC :

![](/tutorials/others/multiboot/5000.png){.full-width}

Vous constaterez que l'arborescence est la même. Les icônes répertoire représentent les sous-menus et les roues dentées les différents démarrage(s) possible(s).

Vous avez la possibilité d'organiser les démarrage(s) dans l'ordre que vous souhaitez.

>Sans action de votre part, à la fin du chrono, le démarrage le plus en haut de la liste sera le démarage par défaut de votre PC.
{.is-info}

Dans notre exemple, nous allons faire les manipulations suivantes afin d'obtenir la liste suivante au démarrage du PC :

* Windows 10
* Ubuntu
* Options avancées pour Ubuntu
  * Ubuntu, avec Linux 5.8.0-38-generic
  * Ubuntu, avec Linux 5.8.0-38-generic (recovery mode)
  * Ubuntu, avec Linux 5.4.0-42-generic
  * Ubuntu, avec Linux 5.4.0-42-generic (recovery mode)
  * Memory test (memtest86+)
  * Memory test (memtest86+, serial console 115200)

Pour obtenir cette structure, commencez par sélectionner la ligne `Windows 10 (sur /dev/sha1)` et faites un clic droit puis cliquez sur `Renommer`. Supprimez tous les caractères en trop pour obtenir `Windows 10` et appuyez sur la touche `Entrée` du clavier pour terminer la saisie.

Avec la ligne Windows 10 sélectionnée, utilisez les flèches de directions haut/bas dans la barre de menu de la fenêtre `Grub Customizer` ci-dessous jusqu'à mettre la ligne Windows 10 en 1ère ligne.

![](/tutorials/others/multiboot/5002.png){.full-width}

Vous obtiendrez le classement suivant :

![](/tutorials/others/multiboot/5004.png){.full-width}

Utilisez la même fonction pour mettre `Memory test (memtest86+)` et `Memory test (memtest86+, serial console 115200)` dans le sous-menu `Options avancées pour Ubuntu`.

>Si il y a des démarrage(s) que vous ne souhaitez pas voir apparaitre au démarrage du PC, il vous est conseillé de les mettre dans un sous-menu que vous pouvez créer avec l'utilisation des flèches gauche / droite.
{.is-info}

![](/tutorials/others/multiboot/5003.png){.full-width}

>Ne supprimez pas les démarrage(s) que vous n'utiliserez pas ! Ils peuvent servir en cas de dépannage.
{.is-warning}

Une fois toutes ces modifications réalisés, vous devriez avoir le menu de boot configuré comme ci-dessous :

![](/tutorials/others/multiboot/5005.png){.full-width}

Après une dernière vérification, cliquez sur l'icone `Enregistrer votre configuration` et attendez la fin de la mise à jour de la configuration avant de quitter `Grub Customiser`.

Maintenant, redémarrez votre PC pour voir ce nouvel écran de démarrage :

![](/tutorials/others/multiboot/5008.png){.full-width}

## 6 - Ajouter la ligne de démarrage de Recalbox

>Dans votre bios, assurez-vous que votre PC démarre sur le HDD en priorité. Si ce n'est pas le cas, faites la modification !
{.is-warning}

>RAPPEL : Branchez votre clé USB Recalbox sur votre PC. Cette clé doit avoir été démarré au moins une fois avec succès sur ce PC avant le début de ce tutoriel !
{.is-warning}

Allumez votre PC avec la clé USB Recalbox branchée et démarrez sur Ubuntu. Une fois Ubuntu démarré, vous devriez voir apparaitre les trois icônes USB `RECALBOX` / `OVERLAY` / `SHARE` dans le menu latéral gauche du bureau Ubuntu. Ces trois icônes correspondent aux 3 partitions `RECALBOX` / `OVERLAY` / `SHARE` de votre clé USB.

![](/tutorials/others/multiboot/6001.png){.full-width}

Retournez dans le `Grub Customiser` et cliquez sur l'icone ![](/tutorials/others/multiboot/6002a.png) pour ouvrir une nouvelle fenêtre Editeur d'entrée comme suit :

* `Nom` : mettre le texte `Recalbox`
* `Type` : dans le menu déroulant, sélectionner `Chargeur en chaîne`
* `Partition` : sélectionner votre partition de clé USB nommé `Recalbox`
* `Séquence d'amorcage` : cette case se renseigne automatiquement lors de la sélection de la partition.

Ci-dessous ce que vous devez obtenir à l'écran :

![](/tutorials/others/multiboot/6003.png){.full-width}

Suivant votre PC, la ligne `Partition` peut être différente et du type `/dev/sd__(RECALBOX, vfat)`.

Idem, suivant votre PC la 1ère ligne de `Séquence d'amorçage` peut être différente et du type `set root='(hd_,_)'` :

* Le premier chiffre est celui du disque concerné dans votre table des partitions (la numérotation des disques commence par 0 pour le premier),
* Le second chiffre est celui de la partition concernée (la numérotation des partitions commence par 1 pour la première).

En l'état, la `Séquence d'amorçage` ne vous permettra pas de démarrer Recalbox sur votre clé USB. Pour corriger le problème, conservez votre 1ère ligne du type `set root='(hd_,_)'` puis remplacer les suivantes par le texte ci-dessous :

```shell
search --no-floppy --fs-uuid --set=root C8E4-A108
set gfxpayload=auto
linux /boot/linux label=RECALBOX console=tty3 quiet loglevel=0
initrd /boot/initrd.gz
```

>Attention à la partie `C8E4-A108` qui sera sûrement nommée différemment sur votre installation, reprenez la série de lettres et de chiffres de votre installation !
{.is-warning}

Dans mon cas, j'obtiens la fenêtre suivante avec `Type` qui passe automatiquement de `Chargeur en chaîne` à `Autre` (ce qui est normal).

![](/tutorials/others/multiboot/6005.png){.full-width}

Cliquez sur `Valider` et remontez le démarrage de Recalbox qui vient d'être créé entre Windows 10 et Ubuntu pour obtenir le résultat suivant :

![](/tutorials/others/multiboot/6006.png){.full-width}

Cliquez sur `Enregistrer` pour sauvegarder votre modification ! Au redémarrage du PC, vous obtiendrez l'écran de démarrage ci-dessous. À ce stade, vous pouvez déjà démarrer sur les trois OS : Windows 10, Recalbox ou Ubuntu au choix.

![](/tutorials/others/multiboot/6007.png){.full-width}

Ccei marque la fin de la configuration basique du Grub Multiboot. Vous pouvez vous arrêter là ou vous pouvez poursuivre pour personnaliser graphiquement votre écran de démarrage et ajouter des fonctionnalités intéressantes.

## 7 - Ajouter un thème Recalbox au Grub

Dans un premier temps, rendez-vous sous Ubuntu, ouvrez la page actuelle dessus et téléchargez le fichier ci-dessous. Par défaut, votre fichier sera sauvegardé dans le répertoire `Téléchargements` de votre compte Ubuntu.

[Recalbox.zip](https://mega.nz/file/zAIUBTpT#dz7umAN-q39Xd_agltjb5_5cle-GX9TSY4XGFjqgf9g)

Lancez `Grub Customizer` et allez dans l'onglet `Paramètres d'Apparence`. Cliquez sur l'icone `+` pour ajouter un nouveau thème. Dans l'exemple, le fichier a été automatiquement téléchargé dans `/home/user/Téléchargements/`. Pour pouvoir faire apparaitre votre fichier .zip, il faut cliquer sur `Fichiers d'archive` et sélectionner `Tous les fichiers` et enfin double-cliquer sur le fichier Recalbox.zip pour sélectionner le nouveau thème.

![](/tutorials/others/multiboot/7001.png){.full-width}

Enfin, cliquez sur l'icône `Enregistrer` de Grub Customizer et fermez Grub Customizer.

![](/tutorials/others/multiboot/7002.png){.full-width}

Redémarrez votre ordinateur pour voir votre nouveau thème installé !

![](/tutorials/others/multiboot/7003.png){.full-width}

>L'image ci-dessus et en 4/3 car je ne peux pas vous faire de capture d'écran du démarrage au format original 16/9 1080p. Pour cette raison, l'ensemble des images du bios qui suivront seront toujours en 4/3 (contrairement à votre affichage si vous êtes équipé d'un écran 16/9).
{.is-info}

>L'icône de Recalbox est absente. C'est normal à ce stade. Pour l'ajouter, suivez la suite du tutoriel ci-dessous...
{.is-warning}

## 8 - Personnalisation manuelle du GRUB (optionnel)

>À partir de ce point, si vous retournez sous `Grub Customizer`, le simple fait de ré-ouvrir l'application recharge automatiquement la config du Grub à l'étape ou vous vous étiez arrêté ci-dessus au §7 !
{.is-warning}

Commençons par faire une copie de sauvegarde de votre fichier `grub.cfg` original en allant chercher votre fichier dans `/boot/grub/grub.cfg`. Pour réaliser cette copie, cliquez sur l'icone `Fichier` dans la barre latérale gauche de votre ordinateur (équivalent à l'explorateur de fichiers sous Windows) en suivant les étapes 1, 2, 3 et 4 ci-dessous pour retrouver votre fichier grub.cfg .

![](/tutorials/others/multiboot/8000.png){.full-width}

Faites un clic droit sur le fichier `grub.cfg` et sélectionnez copier puis coller idéalement sur un support externe comme une clé USB ou vous pouvez aussi vous l'envoyer par email.

>Cette étape vous permettra de récupérer par la suite des informations importantes que vous pourriez avoir effacé par mégarde !
{.is-warning}

### Ajouter une icône pour votre ligne Recalbox à l'écran de démarrage du Grub

À présent, nous allons modifier en écriture le fichier `grub.cfg`. Pour réaliser cette étape, ouvrez une fenêtre Terminal en cliquant sur l'icone Terminal dans la barre latérale gauche du bureau.

![](/tutorials/others/multiboot/8001.png){.full-width}

Tapez la ligne ci-dessous et appuyez sur la touche `Entrée` du clavier.

```shell
sudo gedit /boot/grub/grub.cfg
```

Votre mot de passe vous sera demandé. Saisissez-le et appuyer sur `Entrée`.

![](/tutorials/others/multiboot/8100.png){.full-width}

>Lors de la saisie de votre mot de passe sous un terminal Linux, les caractères n'apparaitront pas, ce qui est normal sous Linux.
{.is-info}

La fenêtre de modification du fichier grub.cfg ci-dessous s'ouvrira et vous pourrez fermer la fenêtre du terminal définitivement.

![](/tutorials/others/multiboot/8101.png){.full-width}

Dans le fichier `grub.cfg`, remplacez le texte suivant :

```ini
menuentry "Recalbox"{
```

Par ceci :

```ini
menuentry "Recalbox" --class recalbox {
```

Comme ci-dessous :

![](/tutorials/others/multiboot/8102.png){.full-width}

Cliquez sur `Enregistrer` et redémarrez votre ordinateur pour voir apparaître l'icône Recalbox au démarrage de l'ordinateur ou continuer la suite du tutoriel pour ajouter d'autres fonctionnalités indiqué ci-dessous avant de redémarrer !

![](/tutorials/others/multiboot/8103.png){.full-width}

### Ajouter la fonction Arrêter et/ou Re-démarrer au démarrage du PC

Suivez les étapes du § précédent pour éditer en écriture le fichier `grub.cfg`.

Ensuite, allez copier la ligne ci-dessous à la fin du fichier `grub.cfg` pour ajouter la fonction **Re-démarer l'ordinateur** dans le menu de démarrage.

```shell
### BEGIN Restart-Option ###
menuentry "Restart" --class restart {reboot}
### END Restart-Option ###
```

Si vous souhaiter ajouter la fonction **Arrêter l'ordinateur** dans le menu de démarrage, allez copier la ligne ci-dessous à la fin du fichier grub.cfg :

```shell
### BEGIN Shutdown-Option ###
menuentry "Shutdown" --class shutdown {halt}
### END Shutdown-Option ###
```

Vous pouvez à présent enregistrer vos modifications et redémarrez votre ordinateur.

Le résultat final :

![](/tutorials/others/multiboot/8201.png){.full-width}

Par exemple, sans écrasement d'image sur un PC portable, le résultat est le suivant :

![](/tutorials/others/multiboot/0000.png){.full-width}