---
title: Konfiguration überschreiben
description: Wie man lokale Konfigurationen für bestimmte Spiele oder ganze Verzeichnisse überschreibt 
published: true
date: 2024-06-02T20:44:25.361Z
tags: 
editor: markdown
dateCreated: 2021-08-23T11:28:15.256Z
---

## Übersicht

Du kannst die Konfiguration eines Spiels oder eines ganzen Verzeichnisses überschreiben.

Das bedeutet, dass du das Verhalten von Recalbox, RetroArch oder dem Emulator für ein bestimmtes Spiel oder für alle Spiele in einem Verzeichnis ändern kannst, indem du bestimmte Dateien zu deinen Roms-Verzeichnissen hinzufügst.

Konkret kannst du auf Folgendes einwirken:

* Recalbox-Konfiguration
* Die allgemeine Konfiguration von RetroArch
* Die Konfiguration der Cores von RetroArch

Übrigens hast du auch die Möglichkeit, Bilder und Beschreibungen der ROMs-Verzeichnisse, wie sie in EmulationStation angezeigt werden, zu überschreiben.

Im Moment können wir für ein Spiel oder eine Reihe von Spielen:

* Einen bestimmten Emulator oder Core festlegen.
* Eine bestimmte Videoauflösung einstellen
* die RetroArch-Videokonfiguration bearbeiten
* die Optionen der Cores ändern.
* usw.


## Allgemeine Informationen

Die Überschreibungen werden auf eine Basisdatei angewendet. Diese Dateien sind die Dateien, die vom Python-Launcher (genannt configgen) beim Starten eines Spiels zuerst geladen werden:

| Datei überladen | Ziel | Konfigurationsdatei |
| :--- | :--- | :--- |
| `.recalbox.conf` | **Recalbox** | `/recalbox/share/system/recalbox.conf` |
| `.retroarch.cfg` | **RetroArch ohne Kernoptionen** | `/recalbox/share/system/configs/retroarch/retroarchcustom.cfg.origin` |
| `.core.cfg` | **Kernoptionen** | `/recalbox/share/system/configs/retroarch/cores/retroarch-core-options.cfg` |

Alle Konfigurationsdateien, die überschrieben werden können, sind Dateien vom Typ Schlüssel/Werte. Es ist also möglich, den Wert eines Schlüssels der Basiskonfiguration zu ändern oder einen nicht vorhandenen Schlüssel festzulegen.

Wir können sogar die Überschreibungen außer Kraft setzen! Das System ermöglicht es nämlich, eine Überschreibung pro Verzeichnisebene zu definieren.
  
Es lädt also zunächst die Basiskonfiguration und wendet dann nacheinander alle Überschreibungsdateien an, die es in den Verzeichnissen findet, beginnend mit dem Stammverzeichnis. Am Ende wird versucht, die Überschreibungsdatei für das Spiel anzuwenden, falls sie existiert.

Nehmen wir ein Beispiel: Wenn wir das Spiel `/recalbox/share/roms/snes/platform/Aladdin (Germany).zip` starten, wird configgen versuchen, die RetroArch-Konfiguration zu überschreiben und in dieser Reihenfolge zu laden:

1. `Basis: /recalbox/share/system/configs/retroarch/retroarchcustom.cfg`
2. `Pfad: /.retroarch.cfg`
3. `Pfad: /recalbox/.retroarch.cfg`
4. `Pfad: /recalbox/share/.retroarch.cfg`
5. `Pfad: /recalbox/share/roms/.retroarch.cfg`
6. `Pfad: /recalbox/share/roms/snes/.retroarch.cfg`
7. `Pfad: /recalbox/share/roms/snes/platform/.retroarch.cfg`
8. `Datei: /recalbox/share/roms/snes/platform/Aladdin (Germany).zip.retroarch.cfg`

Natürlich ist es nicht wirklich ratsam, die Konfiguration zu überschreiben, bevor man nicht zumindest das Verzeichnis eines Systems erreicht hat.

Um ein Verzeichnis zu überschreiben, müssen die Überschreibungsdateien innerhalb des Verzeichnisses liegen und mit einem Punkt (.) beginnen, was sie für das Linux-System unsichtbar macht.
  
Umgekehrt muss die Überschreibung eines Spiels genau wie das Spiel benannt werden, einschließlich der Dateierweiterung, gefolgt von dem Suffix für die Überschreibung, im obigen Beispiel `.retroarch.cfg`.

> Da sich die Überschreibungsdateien in den Verzeichnissen der Roms befinden, werden sie im Falle eines Absturzes, eines fehlerhaften Updates oder eines Absturzes der SD-Karte nicht gelöscht oder bearbeitet (vorausgesetzt, du verwendest ein externes Laufwerk).
>
> Sie sind auch portable: Nimm deinen USB-Stick mit, um bei einem Freund zu spielen, und deine Konfiguration wird angewendet, ohne dass du etwas ändern musst!
{.is-info}

## Recalbox Überschreibungen

Das Überschreiben der Recalbox-Konfiguration hat zwei unmittelbare Vorteile:

* Die Möglichkeit, einen bestimmten Videomodus für ein Spiel oder eine Reihe von Spielen auszuwählen. Pixel Perfect Arcade-Liebhaber werden begeistert sein.
* Die Möglichkeit, einen Core- oder Standalone-Emulator für ein Spiel oder eine Reihe von Spielen zu wählen.

Es gibt noch andere Möglichkeiten, und zweifellos wirst du einige finden 😉

> Das Überschreiben von Schlüsseln, die nicht vom configgen verwendet werden, hat keinen Effekt! Erwarte nicht, daß du das Verhalten von EmulationStation änderst (indem du z.B. sorts überschreibst).
>
> Verzeichnis Überschreibung: `/pfad/zu/deinen/roms/.recalbox.conf`
> ROM Überschreibung: `/pfad/zu/deinen/roms/datei.zip.recalbox.conf`
{.is-warning}

### Beispiel 1: mehrere Versionen von MAME

Warum sollte man sich mit nur einer Version von MAME zufrieden geben, wenn man sie alle haben kann.

Zum Beispiel könntest du MAME 2003 Plus und MAME 2010 ROMsets jeweils in ihrem eigenen Verzeichnis haben: 

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 mame
┃ ┃ ┃ ┃ ┣ 📁 MAME2003Plus
┃ ┃ ┃ ┃ ┣ 📁 MAME2010
 
Füge dann einfach die Datei `/recalbox/share/roms/mame/MAME2003Plus/.recalbox.conf` hinzu:

```ini
mame.emulator=libretro
mame.core=mame2003_plus
```

und die Datei `/recalbox/share/roms/mame/MAME2010/.recalbox.conf`

```ini
mame.emulator=libretro
mame.core=mame2010
```

Und los geht's! Von nun an werden alle Spiele im MAME2003Plus-Verzeichnis mit der Core mame2003_plus-libretro gestartet, und die im MAME2010-Verzeichnis mit der Core mame2010-libretro.

### Beispiel 2: einem Spiel eine bestimmte Core zuweisen

Mein Spiel `/recalbox/share/pcengine/1943 Kai (Japan).zip` funktioniert standardmäßig besser mit der Core `libretro-mednafen_pce_fast` als mit der Core `libretro-mednafen_supergrafx` (völlig willkürliche Annahme für das Beispiel).

Bisher war es möglich, dies über EmulationStation zu tun, indem die Metadaten des Spiels geändert wurden. Die Informationen werden dann in der Datei `gamelist.xml` gespeichert. Ein Schreibfehler in der Datei, ein unglücklicher Scrape, und die gesamte Konfiguration ist verloren.

Hier muss man nur die Datei `/recalbox/share/roms/pcengine/1943 Kai (Japan).zip.recalbox.conf` hinzufügen.

```ini
global.emulator=libretro
global.core=mednafen_pce_fast
```

Diesmal besteht keine Gefahr, die Konfiguration zu verlieren! Natürlich funktioniert die Bearbeitung von Metadaten über EmulationStation weiterhin. Andererseits hat eine Override-Datei Vorrang vor dem, was in der `gamelist.xml` gespeichert ist.

### Beispiel 3: Videomodus für ein Spiel ändern

Auf dem Raspberry Pi2 ist der Standard-Videomodus CEA 4 HDMI, aber das Spiel `Blazing Stars` auf FinalBurn Neo läuft etwas langsam. Umschalten auf 240p würde sicherlich helfen, mal von Pixel Perfect abgesehen (wieder völlig hypothetisch, nur als Beispiel)!

Wir fügen in der Datei `/recalbox/share/roms/fba_libretro/blazstar.zip.recalbox.conf` folgendes hinzu

```ini
global.videomode=CEA 8 HDMI
```
Wenn ich dieses Spiel ausführe, schaltet mein Fernseher auf 240p um, und ich kann "Blazing Stars" in vollem Umfang genießen.

## RetroArch Überschreibungen

Die RetroArch-Konfigurationen betreffen sowohl RetroArch selbst (und die Konfigurationsoptionen sind sehr zahlreich!) als auch die verschiedenen Cores, die je nach den von ihnen emulierten Maschinen spezifische Optionen haben.

> Es gibt bereits spezifische Mechanismen für Recalbox und RetroArch, um entweder die Befehlszeile, die den Emulator startet (über recalbox.conf), oder direkt die RetroArch/Core-Konfigurationen (über die RetroArch-Menüs) zu überschreiben.
>
> Aber keines dieser Systeme erlaubte es, die Überschreibungen auf ganze Verzeichnisse anzuwenden und/oder diese spezifischen Konfigurationen am gleichen Ort wie die ROMs zu speichern. Dies ist besonders interessant für Leute, die Netzwerkfreigaben verwenden, um Roms an mehrere Recalboxen bereitzustellen!
{.is-info}

### RetroArch Konfiguration

Die Retroarch-Konfiguration selbst ist sehr umfangreich und deckt viele verschiedene Bereiche ab.

Es ist jedoch nicht nötig, sich mit dieser Frage zu befassen. Am besten wirft man direkt einen Blick in die [RetroArch-Datei](https://github.com/libretro/RetroArch/blob/master/retroarch.cfg), die besonders gut dokumentiert ist.

Die Möglichkeiten, die die lokalen Überschreibungen bieten, sind enorm:

* Video-Konfiguration: Verhältnis, Skalierung, Anti-Alias, Bildschirmdrehung oder sogar Shader-Auswahl usw.
* Audio-Tuning für einige anspruchsvolle Spiele
* Overlays.
* Einige Optionen für die Eingaben: Mausauswahl, Empfindlichkeit usw.
* Erzwingen bestimmter NetPlay-Konfigurationen
* Ändern der RetroArch-Verzeichnisse (z.B. Backup)
* Einstellen verschiedener Optionen: Zurückspulen, Vorspulen, usw.
* ...

Wie bei den Recalbox-Konfigurationsüberschreibungen können wir auch hier `.retroarch.cfg`-Dateien für Verzeichnisse und Roms erstellen.

#### Beispiel: Erzwingen einer bestimmten Controller-Konfiguration

Bei einigen N64-Spielen wie GoldenEye 007 oder Perfect Dark sowie bei Palm-Spielen muss die Option `Analog zu Digital` deaktiviert sein.

Dazu muss man nur die Datei `/recalbox/share/roms/n64/.retroarch.cfg` hinzufügen:

```ini
input_player1_analog_dpad_mode = "0"
input_player2_analog_dpad_mode = "0"
input_player3_analog_dpad_mode = "0"
input_player4_analog_dpad_mode = "0"
```

Für ein Spiel benennt man die Datei wie folgt: `gamename.ext.retroarch.cfg`. Für ein ganzes System ist `.retroarch.cfg` ausreichend.

### Core Konfigurationen

Das Überschreiben der Optionen der Cores bietet enorme Möglichkeiten, darunter eine von Computerfans sehnsüchtig erwartete Funktion: die Möglichkeit, ein Verzeichnis pro Subsystem zu definieren!

> Die Core-Überschreibungen werden in der Datei `/recalbox/share/system/configs/retroarch/cores/retroarch-core-options.cfg` gespeichert. Wenn das betreffende Spiel gestartet wird, bedeutet dies, dass sie nach dem Beenden des Spiels in derselben Datei gespeichert werden. Um unangenehme Überraschungen zu vermeiden, empfehlen wir, die Schlüssel im Verzeichnis mit Standardwerten zu überschreiben (z.B. `fbneo-frameskip = "0"`), wenn du ein bestimmtes Spiel mit spezifischen Werten überschreiben willst (z.B. `fbneo-frameskip = "2"`), so dass du deine" Basis"-Werte für Dateien behältst, die keine benutzerdefinierten Überschreibungen haben.
{.is-info}

Dies ist vor allem für Multimaschinen-Cores interessant, wie z. B:

* **theodore**, das Thomson MO5, MO6 und TO7 bis TO9+ abdeckt
* **atari800**, das alle Atari 8bits abdeckt, von der ersten 800-Serie bis zum XE, einschließlich XL.
* **vice**, das C64, PET, Vic20, CBM2, ... emuliert
* **hatari**, das vom ersten 520ST bis zu den letzten Falcons emuliert
* und noch mehr....

>Sie finden jede für jeden Core verfügbare Option auf der Seite des Core im `Emulatorteil` dieses Wikis.
{.is-success}

#### Beispiel 1: Konfigurieren der Thomson-Subsysteme

Die Thomsons, französische 8-Bit-Computer aus den 80er Jahren, wurden in 2 Serien aufgeteilt:

* Die MOs, die den ersten MO5 und später den MO6 mit mechanischer Tastatur und integriertem Kassettenspieler hervorbrachten
* Der TO, aus dem der erste TO7 und der TO7-70 hervorgingen, später der TO8 und der TO8D mit Diskettenlaufwerk sowie die Serien TO9 und TO9+, Computer mit professionellerem Aussehen

MO- und TO-Spiele sind überhaupt nicht miteinander kompatibel. Innerhalb der gleichen Serie gibt es eine Abwärtskompatibilität: Auf einem MO6 laufen (im Allgemeinen) die Spiele des MO5.
  
Natürlich werden wir versuchen, jedes Spiel mit dem Gerät zu emulieren, das dem Gerät, für das es ursprünglich entwickelt wurde, am nächsten kommt, um Probleme zu vermeiden und die Chancen auf eine perfekte Emulation zu erhöhen.

Nimmt man die TOSEC-Pakete ([http://www.tosec.org](https://www.tosecdev.org/)), so wurden die Thomson-Spiele in 4 Teilsysteme unterteilt:

* MO5
* MO6
* TO7 (TO7-70)
* TO8, TO8D, TO9 und TO9+

Wir werden daher eine ähnliche Baumstruktur erstellen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 thomson
┃ ┃ ┃ ┃ ┣ 📁 MO5
┃ ┃ ┃ ┃ ┣ 📁 MO6
┃ ┃ ┃ ┃ ┣ 📁 T07
┃ ┃ ┃ ┃ ┣ 📁 TO8,T08D,T09,T09+

Im Stammverzeichnis des Systems werden wir eine Core-Override-Datei anlegen, um einige interessante Optionen für alle zu erzwingen:

Wir legen die Datei `/recalbox/share/roms/thomson/.core.cfg` wie folgt an:

```ini
theodore_rom = "Auto"
theodore_autorun = "enabled"
theodore_floppy_write_protect = "enabled"
theodore_tape_write_protect = "enabled"
```

Wenn wir Spiele zum Stammverzeichnis oder zu einem anderen Verzeichnis hinzufügen, sagen wir dem Emulator, dass er versuchen soll, die beste Maschine zu finden (basierend auf dem Namen der Datei).
  
Außerdem schützen wir die ROMs standardmäßig und schalten die Mechanik des Autostarts ein, was sehr praktisch ist, wenn wir nicht allzu viel über die Originalgeräte wissen.

Dann fügen wir in jedem Unterverzeichnis einen Override auf den Schlüssel `theodore_rom` hinzu, der den Rechner bestimmt.

Datei `/recalbox/share/roms/thomson/MO5/.core.cfg`

```ini
theodore_rom = "MO5"
```

Datei `/recalbox/share/roms/thomson/MO6/.core.cfg`

```ini
theodore_rom = "MO6"
```

Datei `/recalbox/share/roms/thomson/TO7/.core.cfg`

```ini
theodore_rom = "TO7"
```

Datei `/recalbox/share/roms/thomson/TO8,TO8D,TO9,TO9+.core.cfg`

```ini
theodore_rom = "TO9+"
```

Für die letzte Serie wurde die leistungsstärkste Maschine ausgewählt: der TO9 +.

Es ist vorbei! Du hast jetzt 4 Thomson-Subsysteme. Der Emulator befindet sich nicht mehr im automatischen Modus, und das Risiko, dass er das falsche System oder ein Standardsystem auswählt, besteht nicht mehr.

Natürlich kann die Theodore-Core manchmal die Maschine "automatisch erkennen", aber das ist nicht der Fall bei anderen Cores, die beim Start das richtige Subsystem haben müssen.

## Das Erscheinungsbild von Verzeichnissen überschreiben

Um die Möglichkeiten zu vervollkommnen, die die Festlegung von Subsystemen bietet, sei es durch Überschreiben der Recalbox-Konfiguration für MAME oder durch Überschreiben der Theodore-Core, um die Vorteile der damaligen TO- und MO-Maschinen voll auszunutzen, haben wir die Möglichkeit hinzugefügt, das Bild und die Beschreibung eines Verzeichnisses zu überschreiben.

Dies erlaubt es zum Beispiel, in einem Verzeichnis das Bild und die Beschreibung der Maschine zu haben, deren Roms sich in diesem Verzeichnis befinden.

Alles, was man dazu tun muß, ist mindestens eine Bilddatei im PNG-Format mit dem Namen `.folder.picture.png` in das Verzeichnis einzufügen, dessen Bild man in EmulationStation überschreiben möchte. Die Auflösung spielt keine Rolle, aber wir empfehlen, die gleiche oder eine ähnliche Auflösung wie bei den Scrape-Bildern zu verwenden.

Optional kann man eine Textbeschreibung hinzufügen, die unter das Bild geschoben wird, genau wie bei gescrapten Spielen. Die Datei sollte eine einfache Textdatei mit dem Namen `.folder.description.txt` sein.

### Beispiel 1: von MO5 nach TO9

Kehren wir zu dem oben verwendeten Verzeichnis `thomson` zurück, das wir wie bei TOSEC in 4 Subsysteme aufgeteilt hatten.

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 thomson
┃ ┃ ┃ ┃ ┣ 🗒 .core.cfg
┃ ┃ ┃ ┃ ┣ 🗒 gamelist.xml
┃ ┃ ┃ ┃ ┣ 📁 media
┃ ┃ ┃ ┃ ┃ ┣ 📁 box3d
┃ ┃ ┃ ┃ ┃ ┣ 📁 images
┃ ┃ ┃ ┃ ┃ ┣ 📁 videos
┃ ┃ ┃ ┃ ┣ 📁 MO5
┃ ┃ ┃ ┃ ┃ ┣ 🗒 .core.cfg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 .folder.description.txt
┃ ┃ ┃ ┃ ┃ ┣ 🗒 .folder.picture.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 [FD]
┃ ┃ ┃ ┃ ┃ ┣ 🗒 [K7]
┃ ┃ ┃ ┃ ┃ ┣ 🗒 [M5]
┃ ┃ ┃ ┃ ┃ ┣ 🗒 [QD]
┃ ┃ ┃ ┃ ┃ ┣ 🗒 [SAP]
┃ ┃ ┃ ┃ ┃ ┣ 🗒 [WAV]
┃ ┃ ┃ ┃ ┣ 📁 MO6
┃ ┃ ┃ ┃ ┃ ┣ 🗒 .core.cfg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 .folder.description.txt
┃ ┃ ┃ ┃ ┃ ┣ 🗒 .folder.picture.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 [K7]
┃ ┃ ┃ ┃ ┣ 📁 TO7
┃ ┃ ┃ ┃ ┃ ┣ 🗒 .core.cfg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 .folder.description.txt
┃ ┃ ┃ ┃ ┃ ┣ 🗒 .folder.picture.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 [K7]
┃ ┃ ┃ ┃ ┃ ┣ 🗒 [M7]
┃ ┃ ┃ ┃ ┣ 📁 TO8,TO8D,TO9,TO9+
┃ ┃ ┃ ┃ ┃ ┣ 🗒 .core.cfg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 .folder.description.txt
┃ ┃ ┃ ┃ ┃ ┣ 🗒 .folder.picture.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 [FD]
┃ ┃ ┃ ┃ ┃ ┣ 🗒 [K7]
┃ ┃ ┃ ┃ ┃ ┣ 🗒 [QD]
┃ ┃ ┃ ┃ ┃ ┣ 🗒 [SAP]

Die Datei `.folder.picture.png` im Verzeichnis ` /recalbox/share/roms/thomson` enthält ein Bild des Rechners:

![mo5.folder.picture.png](/advanced-usage/config-overloads/mo5.folder.picture.png)

und die Datei `/recalbox/share/roms/thomson/MO5/.folder.description.txt` enthält:

```text
Brand:
    - Thomson (France)
CPU:
    - Motorola 6809E running at 1 MHz
RAM:
    - 48Kb (extensible)
ROM:
    - 16Kb
Graphics:
    - Text mode : 40 columns x 25 lines
    - Graphic mode: 320 x 200, 16 colors (proximity constraint)
Sound:
    - 1bit generator (6bit DAC module extention possible)
Input devices:
    - 57 keys integrated keyboard
    - Optical pen
    - Joysticks
Interfaces:
    - External power supply
    - DIN Connector (optical pen)
    - DIN Connector (tape drive)
    - Peritel (RGB)
    - Extension port
Software:
    - BASIC 1.0 Integrated
Standard devices:
    - Cartridge port (Mémo5)
    - External tape drive (MO5 specific model)
    - External disk drive
Dimensions:
    - 51 x 291 x 190 mm 
Weight:
    - 1.1 kg
Released:
    - 1984
```

Und so sieht das Ergebnis in EmulationStation aus:

![EmulationStation](/advanced-usage/config-overloads/thomson-gamelist.png){.full-width}

### Beispiel 2: MAME

Im ersten Beispiel, das zeigt, wie man mehrere MAME-Versionen im MAME-Verzeichnis haben kann, könnten wir uns vorstellen, ein schönes MAME 2003 Plus und MAME 2010 Logo zu haben, gefolgt von einem kleinen Text, der die Anzahl der Spiele und die entsprechende MAME-ROMset-Version angibt.

Ein kleines Logo:

![Photo MAME 2003 Plus](/advanced-usage/config-overloads/mame.folder.picture.png)

Ein kurzer Text in der Datei `/recalbox/share/roms/mame/MAME2003Plus/.folder.description.txt` :

```text
MAME 2003 Plus is an improved version of MAME2003, with huge bug fixes and hundreds of new games.

Romset Special based on the MAME 0.78 romset

4859 games in total!
```

Und hier ist das Ergebnis:

![mame-gamelist.png](/advanced-usage/config-overloads/mame-gamelist.png){.full-width}

## "Standalone"-Emulatoren

Derzeit können wir die Konfiguration von Standalone-Emulatoren nicht überschreiben, außer teilweise die von Amiberry, dem Amiga-Emulator für ARM-Systeme sowie Hatari, das seine Überladungen in `.hatari.cfg` hat und auf die gleiche Weise funktioniert.

Es gibt keine Pläne, diese Überschreibungen hinzuzufügen, da sie speziellen Code und Tests erfordern. Es ist auch nicht unbedingt für alle Emulatoren machbar, aber in allen Fällen wird es mehr oder weniger Zeit in Anspruch nehmen.

Abhängig von den Anfragen und deren Relevanz werden wir es jedoch möglicherweise von Fall zu Fall berücksichtigen.