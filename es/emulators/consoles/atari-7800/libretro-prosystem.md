---
title: Libretro Prosystem
description: 
published: true
date: 2024-06-30T15:40:07.455Z
tags: libretro, atari, 7800, atari-7800, prosystem
editor: markdown
dateCreated: 2023-09-06T08:50:28.633Z
---



## ![](/emulators/license.svg) Licencia

El core posee la siguiente licencia [**GPLv2**](https://github.com/libretro/prosystem-libretro/blob/master/License.txt).

## ![](/emulators/compatibility.png) Compatibilidad

 | RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | 
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Funcionalidades

| Funcionalidad | ¿Soportada? |
| :---: | :---: |
| Reinicio | ✔ |
| Capturas de pantalla | ✔ |
| Grabaciones instantáneas | ✔ |
| Rebobinado | ✔ |
| RetroAchievements | ✔ |
| Controles | ✔ |
| Remapping | ✔ |

## ![](/emulators/bios.svg) Bios

### Lista de BIOS opcionales

| Nombre del fichero | Descripción | MD5 | ¿Preinstalada? |
| :---: | :---: | :---: | :---: |
| 7800 BIOS (E).rom | BIOS 7800 | 397bb566584be7b9764e7a68974c4263 | ❌ |
| 7800 BIOS (U).rom | BIOS 7800 | 0763f1ffb006ddbe32e52d497ee848ae | ❌ |

### Emplazamiento

Disponed vuestras bios como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 atari7800
┃ ┃ ┃ ┃ ┣ 🗒 **7800 BIOS (E).rom**
┃ ┃ ┃ ┃ ┣ 🗒 **7800 BIOS (U).rom**

## ![](/emulators/roms.png) Roms

### Extensiones de roms soportadas

Las roms deben tener las extensiones siguientes:

* .a78
* .bin
* .cdf
* .zip
* .7z

Este sistema soporta las roms comprimidas en formato .zip/.7z. No obstante, se trata sólo de un archivo.

Los ficheros contenidos en los .zip/.7z deben tener las extensiones citadas previamente.
Además, cada fichero .zip/.7z sólo debe contener un fichero rom al interior.

### Emplazamiento

Disponed vuestras roms como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 atari7800
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.zip**

>Se aconsejan las roms del projecto **No-Intro**.
{.is-success}

>Para más información, consultad [este tutorial](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

>Para conservar las configuraciones personalizadas cuando se produce una actualización del sistema Recalbox, os aconsejamos utilizar las [Sobrecargas de configuración](./../../../advanced-usage/configuration-override).
{.is-info}

### Acceder a las opciones

Podéis configurar las opciones del core de dos formas diferentes.

* Via el Menú RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via el fichero `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Opciones del core

 | Options | Valeur des options | Variable (pour surcharge) | Valeurs des variables |
 | --- | --- | --- | --- |
 | Color Depth (Restart) | `Thousands (16-bit)` ✅ / `Millions (24-bit)` | `prosystem_color_depth` | `16bit` / `24bit` |
 | Audio Filter | `Désactivé` ✅ / `Activé` | `prosystem_low_pass_filter` | `disabled` / `enabled` |
 | Audio Filter Level | `5%` / `10%` / `15%` / `20%` / `25%` / `30%` / `35%` / `40%` / `45%` / `50%` / `55%` / `60%` ✅ / `65%` / `70%` / `75%` / `80%` / `85%` / `90%` / `95%` | `prosystem_low_pass_range` | `5` / `10` / `15` / `20` / `25` / `30` / `35` / `40` / `45` / `50` / `55` / `60` / `65` / `70` / `75` / `80` / `85` / `90` / `95` |
 | Dual Stick Controller | `Désactivé` ✅ / `Activé` | `prosystem_gamepad_dual_stick_hack` | `disabled` / `enabled` |
 
## ![](/emulators/external-links.png) Enlaces externos

* **Código fuente utilizado:** [https://github.com/libretro/prosystem-libretro/](https://github.com/libretro/prosystem-libretro/)
* **Documentación Libretro :** [https://docs.libretro.com/library/prosystem/](https://docs.libretro.com/library/prosystem/)