---
title: Libretro Virtualjaguar
description: 
published: true
date: 2024-06-30T15:42:13.749Z
tags: libretro, atari, jaguar, virtualjaguar
editor: markdown
dateCreated: 2023-09-06T08:55:23.922Z
---

**Libretro Virtualjaguar** es un fork del core del mismo nombre desarrollado para el Atari Jaguar.

## ![](/emulators/license.svg) Licencia

El core posee la siguiente licencia [**GPLv3**](https://github.com/libretro/virtualjaguar-libretro/blob/master/docs/GPLv3).

## ![](/emulators/compatibility.png) Compatibilidad

 | RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | ODROID GO | PC | RG353 |
 | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
 | ❌ | ❌ | ✅ | ✅ | ✅ | ❌ | ✅ | ✅ |

## ![](/emulators/features.png) Funcionalidades

| Funcionalidad | ¿Soportada? |
| :---: | :---: |
| Reinicio | ✔ |
| Capturas de pantalla | ✔ |
| Grabaciones | ✔ |
| Optiones del core | ✔ |
| RetroAchievements | ✔ |
| Controles | ✔ |
| Remapping | ✔ |

## ![](/emulators/bios.svg) BIOS

>**No se necesitan bios.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Extensiones de roms soportadas

Las roms deben tener las extensiones siguientes:

* .j64
* .jag
* .rom
* .abs
* .cof
* .bin
* .prg
* .zip
* .7z

Este sistema soporta las roms comprimidas en formato .zip/.7z. No obstante, se trata sólo de un archivo.

Los ficheros contenidos en los .zip/.7z deben tener las extensiones citadas previamente.
Además, cada fichero .zip/.7z sólo debe contener un fichero rom al interior.

### Emplazamiento

Disponed vuestras roms como en la siguiente imagen: 

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 jaguar
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.zip**

>Se aconsejan las roms del projecto **No-Intro**.
{.is-success}

>Para más información sobre las roms, consultad [este tutorial](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

>Para conservar las configuraciones personalizadas cuando se produce una actualización del sistema Recalbox, os aconsejamos utilizar las [Sobrecargas de configuración](./../../../advanced-usage/configuration-override).
{.is-info}

### Acceder a las opciones

Podéis configurar las opciones del core de dos formas diferentes.

* Via el Menú RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via el fichero `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Opciones del core

| Opción | Valor de la opción | Variable (para la sobrecarga) | Valores posibles |
| --- | --- | --- | --- |
| Fast Blitter | `Désactivé` ✅ / `Activé` | `virtualjaguar_usefastblitter` | `disabled` / `enabled` |
| Doom Res Hack | `Désactivé` ✅ / `Activé` | `virtualjaguar_doom_res_hack` | `disabled` / `enabled` |
| Bios | `Désactivé` ✅ / `Activé` | `virtualjaguar_bios` | `disabled` / `enabled` |
| Pal (Restart) | `Désactivé` ✅ / `Activé` | `virtualjaguar_pal` | `disabled` / `enabled` |
 | Enable Core Options Remapping | `Désactivé` ✅ / `Activé` | `virtualjaguar_alt_inputs` | `disabled` / `enabled` |

## ![](/emulators/external-links.png) Enlaces externos

* **Código fuente utilizado** : [https://github.com/libretro/virtualjaguar-libretro/](https://github.com/libretro/virtualjaguar-libretro/)
* **Documentación Libretro** : [https://docs.libretro.com/library/virtual_jaguar/](https://docs.libretro.com/library/virtual_jaguar/)