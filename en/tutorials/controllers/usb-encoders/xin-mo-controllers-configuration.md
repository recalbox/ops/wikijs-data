---
title: Xin-mo controllers configuration
description: 
published: true
date: 2021-08-07T13:30:03.848Z
tags: configuration, controllers, xin-mo, xinmo
editor: markdown
dateCreated: 2021-08-07T13:30:03.848Z
---

## Xin-mo

Xin Mo USB interfaces work natively with Recalbox and support 2 players.

## Pinout

Let's take as an example a panel with 6 buttons + stick + 1 credit button (`SELECT`) + 1 `START` button (usually 1 player and 2 players button) with this layout for each player:

```text
. ↑     Ⓨ Ⓧ Ⓛ  
←  →	 Ⓑ Ⓐ Ⓡ    ($) (1P)
  ↓
```

It is better to add a dedicated hotkey button: Ⓗ

You must _**absolutely**_ map both players in the same way.

Example:

![XinMo pinout](/tutorials/controllers/usb-encoders/xin-mo/xinmo_arcade_recalbox.jpg)

## Configuration

Then you just have to do _**only**_ the configuration of player 1 directly in EmulationStation. Player 2's configuration should work if the mapping is the same.

>Don't try to configure player 2, it will overwrite player 1's configuration.
>
>Joysticks should be set on the D-PAD and not on joystick in the Emulationstation menu.
{.is-warning}

>The joystick configuration is reserved for analog sticks such as PlayStation or Xbox controllers.
{.is-info}