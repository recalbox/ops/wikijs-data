---
title: Palm
description: 
published: true
date: 2024-07-26T08:34:18.561Z
tags: palm, palm-os, portátiles
editor: markdown
dateCreated: 2023-09-04T12:17:39.904Z
---

![](/emulators/handheld/palm.svg){.align-center}

## Datos técnicos

* **Fabricante**: Palm, Inc.
* **Año de lanzamiento**: 2002
* **Sistema operativo**: Palm OS versión 4.1
* **Procesador**: Motorola DragonBall VZ MC68VZ328 a 33 MHz
* **RAM**: 16 MB RAM
* **ROM**: 4 MB Flash
* **Pantalla**: 160×160 píxeles (65.000+ colores)

## Presentación

**Palm OS** (también conocido como **Garnet OS**) es un sistema operativo embebido desarrollado inicialmente por U.S. Robotics Corp, propietaria de Palm Computing, Inc. desde 1995, para PDAs (asistentes personales digitales) en 1996.

La interfaz gráfica de usuario de Palm OS está diseñada para utilizarse con una pantalla táctil. Incluye un conjunto de aplicaciones básicas para gestionar la información personal. Las versiones posteriores de este sistema operativo se mejoraron para funcionar en teléfonos inteligentes. Otras empresas han obtenido licencias para fabricar dispositivos electrónicos con Palm OS.

## Emuladores

[Libretro Mu](libretro-mu)