---
title: Playstation 2
description: Sony Playstation 2
published: true
date: 2024-08-27T22:04:22.911Z
tags: consoles, sony, playstation 2, ps2, 8.0+
editor: markdown
dateCreated: 2021-06-28T14:36:37.616Z
---

![](/emulators/consoles/playstation2.svg){.align-center}

## Fiche technique

* **Fabricant** : Sony Computer Entertainment
* **Année de sortie** : 2000
* **Quantités vendues** : 158 millions
* **Meilleur jeu vendu** : Gran Theft Auto : San Andreas
* **Processeur** : R5900-based "Emotion Engine" @ 294-299 MHz
* **Processeur grphique** : "Graphics Synthesizer" @ 149 MHz
* **RAM** : 32 MB PC800 32-bit dual-channel RDRAM @ 400 MHz
* **RAM vidéo** : 4MB
* **Résolution** : 256x224 to 1920x1080
* **Media** : DVD, CD

## Présentation

La **PlayStation 2** (abrégé officiellement **PS2**) est une console de jeux vidéo de sixième génération commercialisée par Sony Computer Entertainment, filiale de Sony. Elle est sortie le 4 mars 2000 au Japon, le 26 octobre 2000 en Amérique du Nord, le 24 novembre 2000 en Europe et le 30 novembre 2000 en Australie. La console était en concurrence avec la Dreamcast de Sega, la GameCube de Nintendo et la Xbox de Microsoft.

La PlayStation 2 a succédé à la PlayStation dans la gamme du même nom. Elle a connu un succès immédiat avec un million d'exemplaires vendus en quelques jours au Japon. La PlayStation 2 atteint un total de 150 millions d'exemplaires expédiés en date du 31 janvier 2011, ce qui en fait la console de salon la plus vendue de l'histoire des jeux vidéo. Sony affirme qu'il existe 10 828 titres de jeu vidéo disponibles sur la console et que 1,52 milliard d'exemplaires de ces titres ont été vendus depuis le lancement de la console. Durant la fin de l'année 2009, alors que la console est commercialisée depuis près d'une décennie, Sony explique que la PlayStation 2 restera sur le marché tant qu'il y aura des acheteurs pour ses jeux. Celle-ci est suivie par la PlayStation 3 en 2006.

La PlayStation 2 est aujourd'hui la console la plus vendue de l'histoire du jeu vidéo.

Plus de douze ans après son lancement, Sony annonce officiellement l'arrêt de la distribution de la PlayStation 2 au Japon, le 28 décembre 2012 (elle continuait toujours de se vendre honorablement, figurant encore dans le Top 10) et dans le reste du monde, le 4 janvier 2013.

Une page se tourne dans l'histoire du jeu vidéo et pour le constructeur japonais, qui avait fait sensation avec la sortie de cette console en mars 2000, à une époque où inclure notamment de série la lecture des DVD (format alors tout récent) dans ce type d'appareil était une innovation importante.

## Émulateurs

[Libretro PCSX2](libretro-pcsx2)
[PCSX2](pcsx2)