---
title: Systèmes virtuels
description: 
published: true
date: 2024-11-11T05:48:50.176Z
tags: systèmes, virtuels
editor: markdown
dateCreated: 2021-06-12T00:06:46.772Z
---

## Présentation

Longtemps attendus également, des systèmes virtuels (comme le système Favoris) parmi les classiques :

* Tous les jeux.
* Tous les jeux multi-joueurs (2 joueurs et plus).
* Tous les derniers jeux joués, triés automatiquement par date.

Sans oublier le système **Arcade**, qui réunit tous les systèmes MAME, FBN, NEO-GEO, ... qui sera désormais activable/désactivable dans les menus.  
Mais ce n'est pas tout.

Pour ceux qui rescraperont leurs systèmes avec le nouveau scrapeur interne (Skraper n’étant pas encore à jour), _**de nouveaux systèmes virtuels par genre de jeux pourront être activés**_.  
Oui, vous avez bien lu :

* Par genre.
* Par jeux de Shoot'em up ?
* Par jeux de plateforme ?
* Par jeux de combat ?
* Par jeux de puzzle ?

Activez les systèmes virtuel des genres qui vous intéressent, et ils seront tous réunis au sein d'une même liste !

## Configuration des systèmes virtuels

Pour la configuration, dans l'interface, faites `START` et allez dans `PARAMÈTRES AVANCÉES` > `SYSTÈMES VIRTUELS`. Vous aurez les options suivantes :

* Montrer tout les jeux (On/Off)
* Montrer les jeux multijoueurs (On/Off)
* Montrer les derniers jeux joués (On/Off)
* Afficher le système Lightgun (On/Off)
* Afficher le système Ports (On/Off)
* Systèmes Virtuels par genre - sélectonnez les genres selon vos besoins.

![](/basic-usage/features/virtualsystem3.png){.full-width}

![](/basic-usage/features/virtualsystem4.png){.full-width}

## Configuration des systèmes virtuels arcade

Pour les systèmes virtuels arcade, la configuration est différente.

Pour la configuration, dans l'interface, faites `START` et allez dans `PARAMÈTRES ARCADE` > `SYSTÈME ARCADE TOUT-EN-UN`. Vous aurez les options suivantes :

* Activer le système Virtuel Arcade (On/Off)
* Inclure la Neo-Geo (On/Off)
* Cacher les systèmes originels (On/Off)

![](/basic-usage/features/virtualsystem5.png){.full-width}

## Configuration des systèmes virtuels arcade par constructeur

Pour les systèmes virtuels arcade par constructeur, la configuration est encore différente.

Pour la configuration, dans l'interface, faites `START` et allez dans `PARAMÈTRES ARCADE` > `SYSTÈMES VIRTUELS DES CONSTRUCTEURS`. Vous aurez une liste de constructeurs à activer ou désactiver en fonction de vos besoins.