---
title: Visualizzazione dei sistemi
description: Funzionamento e modifiche della visualizzazione dei sistemi.
published: true
date: 2024-06-22T06:09:46.888Z
tags: 
editor: markdown
dateCreated: 2024-06-22T06:09:46.888Z
---

<h2>Funzionamento della visualizzazione</h2>

<p>La visualizzazione dei sistemi nel menu di Recalbox è gestita dal file <code>systemlist.xml</code>. Questo file si trova nella cartella <code>/recalbox/share_init/system/.emulationstation/systemlist.xml</code>.</p>

<p>Contiene i nomi dei diversi sistemi che sono supportati dalla tua versione di Recalbox. È costruito nel modo seguente:</p>

<pre><code class="language-xml">&lt;?xml version="1.0"?&gt;
&lt;systemList&gt;
  &lt;defaults command="python /usr/bin/emulatorlauncher.pyc %CONTROLLERSCONFIG% -system %SYSTEM% -rom %ROM% -emulator %EMULATOR% -core %CORE% -ratio %RATIO% %NETPLAY%"/&gt;
  &lt;system uuid="b58b9072-0e44-4330-869d-96b72f53de1f" name="3do" fullname="Panasonic 3DO" platforms="3do"&gt;
    &lt;descriptor path="%ROOT%/3do" theme="3do" extensions=".iso .chd .cue"/&gt;
    &lt;scraper screenscraper="29"/&gt;
    &lt;properties type="console" pad="mandatory" keyboard="no" mouse="no" lightgun="optional" releasedate="1993-10" retroachievements="1"/&gt;
    &lt;emulatorList&gt;
      &lt;emulator name="libretro"&gt;
        &lt;core name="opera" priority="1" extensions=".chd .cue .iso" netplay="0" compatibility="high" speed="high"/&gt;
      &lt;/emulator&gt;
    &lt;/emulatorList&gt;
  &lt;/system&gt;
  &lt;system&gt;
      [... ]
  &lt;/system&gt;
  [... ]
&lt;/systemList&gt;
</code></pre>

<p>La visualizzazione dei sistemi rispetta l'ordine in cui sono elencati in questo file. Contiene anche la configurazione di questi sistemi.</p>

<h2>Modificare l'ordine di visualizzazione</h2>

<blockquote>
  <p><strong>Non modificare</strong> il file <code>systemlist.xml</code> originale (che si trova nella cartella <code>share_init</code>). In caso di problemi con le modifiche apportate in seguito, questo file rimane l'unica e unica fonte per far funzionare correttamente Recalbox.</p>
</blockquote>

<p>La modifica dell'ordine di visualizzazione deve essere effettuata solo dal file <code>systemlist.xml</code> presente nella cartella <code>/recalbox/share/system/.emulationstation/systemlist.xml</code>.</p>

<p>Originariamente, questo file non esiste. È necessario copiare il file originale o creare un nuovo file. Una volta creato il nuovo file, è possibile ordinare i sistemi nell'ordine desiderato. La configurazione dei sistemi sarà sempre presa dal file <code>systemlist.xml</code> originale, ma l'ordine dei sistemi sarà quello definito nel nuovo file.</p>

<p>Se, nel nuovo file, un sistema è assente o mal compilato, la priorità viene data al file originale. Per il nuovo file, c'è solo una chiave di accesso obbligatoria: "UUID", tutte le altre sono facoltative. Il file dovrà essere costruito almeno nel modo seguente:</p>

<pre><code class="language-xml">&lt;?xml version="1.0"?&gt;
&lt;systemList&gt;
  &lt;system uuid="62f2cbed-5bcb-46d8-bca9-daaa36613c7a"/&gt; &lt;!-- nes --&gt;
  &lt;system uuid="0a7fd1d3-9673-44ab-bfef-5bb3c8a0d79a"/&gt; &lt;!-- fds --&gt;
  &lt;system uuid="3df92492-d69a-48f8-8e14-9a62bd9805a6"/&gt; &lt;!-- snes --&gt;
  &lt;system uuid="99e312bc-7602-4038-a871-e53c17fd1d76"/&gt; &lt;!-- satellaview --&gt;
  [... ]
&lt;/systemList&gt;
</code></pre>

<h2>Aggiungere un sistema "Custom"</h2>

<blockquote>
  <p><strong>Non modificare</strong> il file <code>systemList</code> originale (che si trova nella cartella <code>share_init</code>). In caso di problemi con le modifiche apportate in seguito, questo file rimane l'unica e unica fonte per far funzionare correttamente Recalbox.</p>
</blockquote>

<p>Questa operazione non consente di aggiungere un nuovo emulatore a Recalbox, ma di aggiungere una nuova voce di sistema nel menu di selezione.</p>

<p>È possibile combinare la modifica dell'ordine dei sistemi e l'aggiunta di uno o più sistemi personalizzati.</p>

<p>Come per la modifica dell'ordine dei sistemi, l'aggiunta di un sistema "custom" deve essere effettuata solo dal file <code>systemlist.xml</code> presente nella cartella seguente: <code>/recalbox/share/system/.emulationstation/systemlist.xml</code>.</p>

<p>Originariamente, questo file non esiste. È necessario copiare il file originale o creare un nuovo file. Una volta creato il nuovo file, è ora possibile aggiungervi un nuovo sistema.</p>

<p>Se, nel nuovo file, un sistema è mal compilato, la priorità viene data al file originale. Per il nuovo file, tutte le chiavi di accesso sono obbligatorie. Pertanto, per creare un nuovo sistema, il più semplice è partire da un sistema esistente (e corrispondente alle ROM che si desidera includervi) e modificare solo il necessario:</p>

<ul>
  <li><strong>"fullname"</strong>: consente di dare un nome al nuovo sistema.</li>
  <li><strong>"path"</strong>: consente di indicare la directory contenente le ROM del nuovo sistema.</li>
  <li><strong>"theme"</strong>: consente di indicare quale tema utilizzare. È necessario creare preventivamente questo nuovo tema (logo, sfondo, ecc.).</li>
</ul>

<p><strong>Tutte le altre voci non devono essere modificate.</strong></p>

<p>Ecco un esempio di aggiunta per un sistema basato su SNES per includervi solo ROM tradotte:</p>

<pre><code class="language-xml">&lt;?xml version="1.0"?&gt;
&lt;systemList&gt;
  &lt;system uuid="21b8873a-a93e-409c-ad0c-8bb6d682bef8" name="snes" fullname="Super Nintendo Fan Trad" platform="snes"&gt;
    &lt;descriptor path="%ROOT%/snestrad" theme="snestrad" extensions=".smc .sfc .mgd .zip .7z"/&gt;
    &lt;scraper screenscraper=""/&gt;
    &lt;properties type="console" pad="mandatory" keyboard="no" mouse="optional" lightgun="optional" releasedate="1990-11" retroachievements="1"/&gt;
    &lt;emulatorList&gt;
      &lt;emulator name="libretro"&gt;
        &lt;core&gt;snes9x2005&lt;/core&gt;
        &lt;core&gt;snes9x2010&lt;/core&gt;
        &lt;core&gt;snes9x2002&lt;/core&gt;
      &lt;/emulator&gt;
    &lt;/emulatorList&gt;
  &lt;/system&gt;
&lt;/systemList&gt;
</code></pre>
