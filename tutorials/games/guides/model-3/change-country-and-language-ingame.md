---
title: Change country and language in-game
description: 
published: true
date: 2023-01-28T22:17:42.910Z
tags: country, language, model3
editor: markdown
dateCreated: 2021-08-08T09:02:09.764Z
---

>For the following games the country is linked to the rom (no change possible in the "TEST MENU")
>
>* Dirt Devils
>* Emergency Call Ambulance
>* Scud Race: 
> * For the Australian version (Australia), there is no menu to change the region.
> For the other versions, the change is made directly in the "TEST MENU".
{.is-info}

![](/emulators/arcade/supermodel/changelanguage1.png){.full-width}

For each game, when it is launched, enter the "TEST MENU" using the `L3` key.

Then the handling differs according to the game.

![](/emulators/arcade/supermodel/changelanguage2.png){.full-width}

![](/emulators/arcade/supermodel/changelanguage3.png){.full-width}

## Daytona USA 2

>Works for "Battle on the edge" and "Power Edition".
{.is-info}

Functions of the different keys in the "TEST MENU".

| Features | Shortcuts |
| :---: | :---: |
| Up arrow in the menu | **Y** |
| Downward arrow in the menu | **X** |
| Downward arrow in the menu | **A** |
| Downward Selection | **B** |
| Validate | **L3** |

* While holding down the "START" key, enter this sequence of keys: "B", "B", "X", "A", "Y", "A", "X
* You're now in "COUNTRY ASSIGNMENTS".
  * Move the arrow to "COUNTRY".
  * Use one of the selection keys to change to the desired country (Japan, Korea, Australia, Export, USA).
* Confirm.

## Fighting Vipers 2

Functions of the different keys in the "TEST MENU".

| Features | Shortcuts |
| :---: | :---: |
| Move arrow up in menu | **X** |
| Move arrow down in menu | **B** or **R3** |
| Validate/Change Value | **A** or **L3** |

* Go to the "GAME ASSIGNMENTS" submenu.
  * Move the arrow to "Country".
  * Enter this sequence of keys on the D-pad: 3X **Left**, 2X **Right**, 1X **Left**.
  * Change the country by pressing **L3** (Japan, USA, Export, Asia).
* Exit the menu (Move the arrow down to "EXIT").

## Harley Davidson & L.A. Riders

Functions of the different keys in the "TEST MENU".

| Features | Shortcuts |
| :---: | :---: |
| Move arrow up in menu | **X** or **R1** |
| Move arrow down in menu | **B** or **L1** or **R3** |
| Enter / Change value | **A** or **L3** |

* Go to the "GAME ASSIGNMENTS" sub-menu.
  * Enter this sequence of keys: 2x `R1`, 2x `L1`, `X`, `B`, `X`, `B`.
  * The "Country" line at the bottom of the screen appears.
  * Move the arrow down one step. 
  * Change the country using `L3` (Japan, USA, Export, Korea, Australia).
* Exit the menu (Move the arrow up to "EXIT").

## L.A. Machine Guns

Functions of the different keys in the "TEST MENU".

| Features | Shortcut |
| :---: | :---: |
| Move the arrow down in the menu | **R3** |
| Validate / Change value | **L3** |

* Enter this key sequence: 2x `START`, 1x `R3`, 3x `START`, 1x `R3`, 1x `L3`.

>The functions of the different keys in the "TEST MENU" are not the same in the "COUNTRY SELECT".
{.is-warning}

The functions of the different keys in the COUNTRY SELECT MODE.

| Features | Shortcut |
| :---: | :---: |
| Change value | **R3** |
| Validate | **L3** |

* You are now in COUNTRY SELECT MODE.
  * Change the value until you have the country you want (Japan, USA, Export, Australia).
* Validate.

## Le Mans 24H

Functions of the different keys in the "TEST MENU

| Functions | Shortcut |
| :---: | :---: |
| Move down the arrow in the menu | **R3** |
| Validate / Change value | **L3** |

* Enter this sequence of keys: 2x `START`, 2x `R3`, 1x `START`, 1x `L3` .

>The functions of the different keys in the "TEST MENU" are not the same in the "COUNTRY SELECT".
{.is-warning}

The functions of the different keys in the COUNTRY SELECT MODE.

| Features | Shortcut |
| :---: | :---: |
| Change value | **R3** |
| Validate | **L3** |

* You are now in COUNTRY SELECT MODE.
  * Change the value until you have the country you want (Japan, USA, Export, Australia).
* Validate.

## Magical Truck Adventure

>For this game, there is only Japan as an accessible region.
{.is-info}

Functions of the different keys in the "TEST MENU"

| Features | Shortcut |
| :---: | :---: |
| Move the arrow down in the menu | **R3** |
| Validate / Change value | **L3** |

* Enter this key sequence: `START`, `START`, `R3`, `START`, `R3`, `L3`.
  * Change the value until you have the country you want (Japan).
  * Validate.

## Scud Race

Functions of the different keys in the "TEST MENU"

| Features | Shortcut |
| :---: | :---: |
| Move the arrow down in the menu | **R3** |
| Validate / Change value | **L3** |

If you get a NETWORK BOARD NOT PRESENT error, do this:

* Enter the test menu.
* Go down to GAME ASSIGNEMENTS.
* Go to the LINK ID line and change the MASTER value to SINGLE.

## Sega Bass Fishing

Functions of the different keys in the "TEST MENU"

| Features | Shortcut |
| :---: | :---: |
| Down arrow in the menu | **R3** |
| Validate / Change value | **L3** |

* Enter the "C.R.T. TEST".
  * Press the `L3` key to go to page 2/2 (the page is shown at the top of the screen).
  * Press the 4x `R3` key.
  * Press the `L3` key to exit.
* You are back in the TEST MENU. Select the "GAME ASSIGNMENTS" sub-menu.
  * Press the `R3` key 3 times, then `L3` + `R3`.

>The functions of the different keys in the TEST MENU are not the same in COUNTRY SELECT.
{.is-warning}

The functions of the different keys in the COUNTRY SELECT.

| Features | Shortcut |
| :---: | :---: |
| Change value | **R3** |
| Validate | **L3** |

* You are now in COUNTRY SELECT MODE.
  * Change the value until you have the country you want (Japan, USA, Export, Australia).
* Validate.

## Sega Rally 2

Functions of the different keys in the "TEST MENU

| Features | Shortcut |
| :---: | :---: |
| Move down arrow in the menu | **R3** |
| Validate / Change value | **L3** |

* Enter this sequence of actions with the `R3` key (Long or Short).
  * 4x Press Short, 2x Long, 2x Short, 1x Long .

>The functions of the different keys in the TEST MENU are not the same in COUNTRY SELECT.
{.is-warning}

The functions of the different keys in the COUNTRY SELECT.

| Features | Shortcut |
| :---: | :---: |
| Change value | **R3** |
| Validate | **L3** |

* You are now in COUNTRY SELECT MODE.
  * Change the value until you have the country you want (Japan, USA, Australia, Export).
* Validate.

## Ski Field

Functions of the different keys in the "TEST MENU

| Features | Shortcut |
| :---: | :---: |
| Move the arrow down in the menu | **R3** |
| Validate / Change value | **L3** |

* Enter this key sequence: `Y`, `A`, `Y`, `A`, `R3`.

>The functions of the different keys in the TEST MENU are not the same in COUNTRY SELECT.
{.is-warning}

The functions of the different keys in the COUNTRY SELECT.

| Features | Shortcut |
| :---: | :---: |
| Change value | **R3** |
| Validate | **L3** |

* You are now in COUNTRY SELECT MODE.
  * Change the value until you have the country you want (Japan, USA, Export, Korea, Australia).
* Validate.