---
title: Utilizar um controle remoto infravermelho
description: 
published: true
date: 2021-07-17T22:16:27.726Z
tags: 
editor: markdown
dateCreated: 2021-07-17T21:45:10.149Z
---

O Recalbox pode ser controlado através de qualquer controle remoto infravermelho. Esta característica é para a Kodi.

Custará cerca de R$ 20 e é muito simples de montar.

## I - Requisitos

#### A - Receptor infravermelho

Para funcionar, você precisará de um receptor IR como um módulo TSOP483838 de 38KHz; note que ele também pode trabalhar com alguns outros componentes. Você pode comprar um na internet por cerca de R$ 10 em qualquer varejista de eletrônicos ou on-line.

![Receptor IR](/basic-usage/features/kodi/ir-control/ir1.jpg)

### B - Cabos Dupont fêmea/fêmea

A fim de conectar facilmente seu receptor sem solda, você precisará de 3 cabos dupont F/F. Você pode compra-los dos mesmos varejistas que seu receptor, por alguns reais.

![cabos F/F Dupont](/basic-usage/features/kodi/ir-control/ir2.jpg)

#### C - Diagrama

Para conectar o receptor e os cabos, siga o diagrama abaixo:

![](/basic-usage/features/kodi/ir-control/ir3.png)

Exemplo de instalação:

![](/basic-usage/features/kodi/ir-control/ir4.jpg)

### D - Controle remoto

Quase todos os controles remotos são suportados, desde que utilizem os padrões.

Todos esses controles remotos foram testados com sucesso:

* Um controle remoto hifi da Philips.
* Um controle remoto da Samsung para um gravador de vídeo.
* Um controle remoto universal.
* Um controle remoto para um computador MAC.

![](/basic-usage/features/kodi/ir-control/ir5.jpg)

## II - Configuração

#### A - config.txt

* Abra o arquivo [recalbox-user-config.txt](./../../../tutorials/system/modification/configtxt-configuration) via [SSH](./../../../tutorials/system/access/root-access-terminal-cli).
* Lembre-se de ativar o modo de escrita na partição [boot](./../../../tutorials/system/access/remount-partition-with-write-access)
* Descomente a seguinte linha, removendo o `#`:

```ini
#dtoverlay=lirc-rpi
```

Torna-se :

```ini
dtoverlay=lirc-rpi
```

* Reinicie seu Recalbox.

### B - Configuração do controle remoto

#### 1. Verificação de eventos de IR

Vamos verificar se o hardware está funcionando:

* Conecte-se ao seu Recalbox via [SSH](./../../../tutorials/system/access/root-access-terminal-cli)
* Execute o seguinte comando:

```shell
lsmod
```

Você deve ver uma linha começando com `lirc_rpi`.

![](/basic-usage/features/kodi/ir-control/ir6.png)

* Em seguida, execute o seguinte comando:

```shell
mode2 -d /dev/lirc0
```

* Cada vez que você apertar um botão em seu controle remoto na frente de seu receptor, você deverá ver os números aparecerem no terminal. Se este for o caso, tanto o controle remoto quanto o receptor estão funcionando corretamente.
* Pressione `Ctrl`+`C` para sair.

![](/basic-usage/features/kodi/ir-control/ir7.png)

#### 2. Registre seu controle remoto

* Execute o comando:

```shell
irrecord -H /recalbox/share/custom.conf
```

* Pressione `Enter` para continuar.
* Digite `customremote` como o nome remoto e pressione `Enter` para continuar.

![](/basic-usage/features/kodi/ir-control/ir8.png)

* Agora comece a pressionar as teclas do seu controle remoto por cerca de um segundo.

![](/basic-usage/features/kodi/ir-control/ir9.png)

* Digite o nome da tecla a partir dos nomes abaixo e pressione a tecla no controle remoto:

  * KEY_DOWN (Baixo)
  * KEY_EXIT (Voltar/Sair)
  * KEY_INFO (Exibir informações sobre a mídia que está sendo reproduzida)
  * KEY_LEFT (Esquerda)
  * KEY_MENU (Menu)
  * KEY_MUTE (Mudo)
  * KEY_OK (Ok)
  * KEY_PLAY (Iniciar e Pausar)
  * KEY_POWER (Sair)
  * KEY_RIGHT(Direita)
  * KEY_STOP (Parar)
  * KEY_UP (Cima)
  * KEY_VOLUMEDOWN (Volume -)
  * KEY_VOLUMEUP (Volume +)

* E possivelmente :

  * KEY_AUDIO
  * KEY_BLUE
  * KEY_CHANNELDOWN
  * KEY_CHANNELUP
  * KEY_DELETE
  * KEY_ENTER
  * KEY_EPG
  * KEY_FASTFORWARD
  * KEY_GREEN
  * KEY_LANGUAGE
  * KEY_MEDIA
  * KEY_NEXT
  * KEY_NUMERIC_1
  * KEY_NUMERIC_2
  * KEY_NUMERIC_3
  * KEY_NUMERIC_4
  * KEY_NUMERIC_5
  * KEY_NUMERIC_6
  * KEY_NUMERIC_7
  * KEY_NUMERIC_8
  * KEY_NUMERIC_9
  * KEY_NUMERIC_0
  * KEY_PAUSE
  * PRÉVIO_CHAVE
  * KEY_PVR
  * KEY_RADIO
  * KEY_RECORD
  * KEY_RED
  * KEY_REWIND
  * KEY_SUBTITLE
  * KEY_VIDEO
  * KEY_YELLOW
  * KEY_ZOOM

![](/basic-usage/features/kodi/ir-control/ir10.png)

* Repita a operação para todos ou para o maior número possível de botões em seu controle remoto.

Para configurar um botão novamente, digite novamente seu nome e comece novamente.

![](/basic-usage/features/kodi/ir-control/ir11.png)

* Quando você tiver registrado o conjunto de teclas que deseja, pressione `Enter` para continuar.
* Em seguida, pressione uma das teclas muito rapidamente sem segurá-la e sem alterar as teclas.

![](/basic-usage/features/kodi/ir-control/ir12.png)

* No final, o programa será fechado automaticamente.
* Se você quiser começar novamente, exclua o arquivo de configuração com o comando :

```shell
rm /tmp/custom.conf
```

* E reinicie o `irrecord`.

#### 3. Arquivo de configuração

* Em seguida, mova o arquivo com o seguinte comando para substituir o arquivo `lircd.conf` original pelo seu:

```shell
mv /recalbox/share/custom.conf /recalbox/share/system/.config/lirc/lircd.conf
```

* Reinicie o serviço `lircd' com o comando :

```shell
/etc/init.d/S25lircd restart
```

![](/basic-usage/features/kodi/ir-control/ir13.png)

#### 4. Verificar se a configuração funciona

* Digite o comando `irw`.
* Cada vez que você apertar uma tecla, deverá ver aparecer uma linha com o nome da tecla.
* Pressione `Ctrl` + `C` para sair.

![](/basic-usage/features/kodi/ir-control/ir14.png)

* Inicie o Kodi e verifique o funcionamento.

## III - Configuração avançada

### A - Lircmap.xml

* Você pode personalizar as configurações das teclas do controle remoto editando o arquivo `/recalbox/share/system/.kodi/userdata/Lircmap.xml`.

>Se você tiver apagado manualmente a pasta `~/.kodi` e reiniciado o Kodi sem reiniciar o Recalbox, você não verá as personalizações no Kodi ou no arquivo `Lircmap.xml`.
{.is-warning}

* Considere copiar `/recalbox/share_init/system/.kodi/userdata/Lircmap.xml` para `/recalbox/share/system/.kodi/userdata/Lircmap.xml`.

### B - remote.xml

Você pode mudar a definição de teclas e ações no arquivo `/recalbox/share/system/.kodi/userdata/keymaps/remote.xml`.

### C - SAIR / PARAR

Isto é para pessoas que não gostam do fato de que o botão `Voltar` não pára um filme no Kodi ou porque você só tem um único botão 'Parar / Voltar' em seu controle remoto.

Você pode alterar no arquivo remote.xml a seção "", "Back" por "Stop".

### D - VOLUME ACIMA/ABAIXO

> Este ponto diz respeito aos comandos CEC.
{.is-info}

Se seu controle remoto CEC não transferir comandos de volume, você pode usar outras teclas substituindo, por exemplo, em remote.xml na seção global:

```xml
  <skipplus>SkipNext</skipplus>
  <skipminus>SkipPrevious</skipminus>
```

por

```xml
  <skipplus>VolumeUp</skipplus>
  <skipminus>VolumeDown</skipminus>
```

### E - PAUSAR e OK

Na *skin* Refocus, para facilitar a pausa (principalmente em um controle remoto Apple), você pode modificar o arquivo `~/.kodi/addons/skin.refocus/720p/VideoOSD.xml` substituindo :

```xml
  <defaultcontrol always="true">700</defaultcontrol>
```

Com isto:

```xml
  <defaultcontrol always="true">705</defaultcontrol>
```
