---
title: Libretro GearColeco
description: 
published: true
date: 2024-07-29T13:17:23.491Z
tags: libretro, colecovision, gearcoleco, 8.0+
editor: markdown
dateCreated: 2023-09-06T09:28:48.909Z
---

## ![](/emulators/license.svg) Licencia

Este núcleo está bajo la licencia [**GPL 3.0**](https://github.com/drhelius/Gearcoleco/blob/main/LICENSE).

## ![](/emulators/compatibility.png) Compatibilidad

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Características

| Característica | Soportado |
| :---: | :---: |
| Reinicio | ✔ |
| Capturas de pantalla | ✔ |
| Guardados | ✔ |
| Guardados instantáneos | ✔ |
| Rebobinado | ✔ |
| Opciones del núcleo | ✔ |
| RetroAchievements | ✔ |
| Controles | ✔ |
| Remapeo | ✔ |
| Softpatching | ✔ |

## ![](/emulators/bios.svg) BIOS

### Lista de BIOS obligatorios

| Nombre de archivo | Descripción | MD5 | Proveído |
| :---: | :---: | :---: | :---: |
| boot.rom | - | 2c66f5911e5b42b8ebe113403548eee7 | ❌ |

### Ubicación

Coloca los BIOS de la siguiente manera:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 coleco
┃ ┃ ┃ ┃ ┣ 🗒 **boot.rom**

## ![](/emulators/roms.png) Roms

### Extensiones soportadas

Los roms deben tener las siguientes extensiones:

* .bin
* .col
* .cv
* .rom
* .zip

Este sistema soporta roms comprimidos en formato .zip. Sin embargo, se trata solo de un archivo.

Los archivos contenidos en los .zip deben corresponder a las extensiones mencionadas anteriormente.
Además, cada archivo .zip debe contener solo un rom comprimido.

### Ubicación

Coloca los roms de la siguiente manera:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 colecovision
┃ ┃ ┃ ┃ ┣ 🗒 **juego.zip**

>Se recomienda encarecidamente el uso de roms en formato **No-Intro**.
{.is-success}

>Para más información sobre los roms, visita [este tutorial](./../../tutorials/games/generalities/isos-and-roms)!
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

>Para poder conservar tus configuraciones personalizadas durante una actualización, te recomendamos usar nuestra funcionalidad [Sobrecargas de configuración](./../../../advanced-usage/configuration-override).
{.is-info}

### Acceder a las opciones

Puedes configurar diversas opciones de dos maneras diferentes.

* A través del menú RetroArch:

┣ 📁 Menu RetroArch
┃ ┣ 📁 Opciones del núcleo
┃ ┃ ┣ 🧩 Name_option

* A través del archivo `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Opciones del núcleo

| Opciones | Valor de las opciones | Variable (para sobrecarga) | Valores de las variables |
| --- | --- | --- | --- |
| Tasa de refresco (reinicio) | `Auto` ✅ / `NTSC (60 Hz)` / `PAL (50 Hz)` | `gearcoleco_timing` | `Auto` / `NTSC (60 Hz)` / `Pal (50 Hz)` |
| Permitir Arriba+Abajo / Izquierda+Derecha | `Desactivado` ✅ / `Activado` | `gearcoleco_up_down_allowed` | `Disabled` / `Enabled` |
| Sin límite de sprites | `Desactivado` ✅ / `Activado` | `gearcoleco_no_sprite_limit` | `Disabled` / `Enabled` |

## ![](/emulators/external-links.png) Enlaces externos

* **Código fuente utilizado**: [https://github.com/drhelius/Gearcoleco/](https://github.com/drhelius/Gearcoleco/)
