---
title: FAQ
description: 
published: true
date: 2024-11-04T20:20:05.460Z
tags: system, famicom, disk, faq, face
editor: markdown
dateCreated: 2021-07-30T18:43:59.189Z
---

## Changer de face de disquette

### Introduction

Les jeux sont écrits des 2 côtés d'une disquette, et vous devez la retourner pour pouvoir lire l'autre face.

### Utilisation

Son utilisation dépend de l'émulateur que vous utilisez.

#### Libretro FBNeo

Les boutons `L3` et `R3` permettent pour l'un d'éjecter le disque (`L3`) et pour l'autre de changer la face et lire la nouvelle face (`R3`). Il vous faut donc effectuer la manipulation `L3` puis `R3` à la suite pour changer la face.

#### Les autres émulateurs du système

Les boutons `L1` et `R1` permettent pour l'une d'éjecter le disque (`R1`) et pour l'autre de changer la face (`L1`). Donc il faut faire la combinaison `R1` puis `L1` puis `R1` (éjecter + retourner + lire) à la suite pour changer la face.