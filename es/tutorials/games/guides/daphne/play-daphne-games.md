---
title: Jugar a los juegos Daphné
description: 
published: true
date: 2024-08-25T14:43:25.808Z
tags: daphne, juegos
editor: markdown
dateCreated: 2023-09-19T14:06:58.732Z
---

## Lista de juegos Daphne

Los juegos Daphne son juegos que no utilizan la extensiones de Singe.

Aquí está la lista de juegos Daphne:

* Astron Belt
* Badlands
* Bega's Battle
* Cliff Hanger
* Cobra Command
* Dragon's Lair
* Dragon's Lair II : Time Warp
* Esh's Aurunmilla
* Firefox
* Galaxy Ranger
* GP World
* Interstellar Laser Fantasy
* M.A.C.H. 3
* Road Blaster
* Space Ace
* Super Don Quix-Ote
* Thayer's Quest
* Us Vs Them

## Instalación de los juegos

Cada juego se separa en dos partes bien diferenciadas:

* Los datos necesarios para hacer funcionar el juego (vídeos, músicas, etc.)
* Los datos del juego (fichero zip)

### Para los datos del juego

Los datos del juego se encuentran contenidos en un fichero `.ZIP` y poseen extensiones específicas. Estos ficheros no se deben descomprimir y deben tener el mismo nombre que la carpeta `nombredeljuego.daphne` (sin la extensión .daphne).

Debéis disponer este fichero en el siguiente directorio:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┃ ┃ ┣ 🗒 nombredeljuego.zip

>La mayoría de los ficheros pesan menos de 100 kb, salvo raras excepciones.
{.is-info}


### Para los datos necesarios para hacer funcionar el juego

Hay que tener los siguientes ficheros:

* 1 fichero `nombredeljuego.commands` (opcional)
* 1 fichero `nombredeljuego.dat` (opcional)
* 1 o varios ficheros `nombredeljuego.m2v`
* 1 o varios ficheros `nombredeljuego.m2v.bf` (opcional)
* 1 o varios ficheros `nombredeljuego.ogg`
* 1 o varios ficheros `nombredeljuego.ogg.bf` (opcional)
* 1 fichero `nombredeljuego.txt`

Hay que disponerlos en el siguiente directorio:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 nombredeljuego.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 nombredeljuego.commands
┃ ┃ ┃ ┃ ┃ ┣ 🗒 nombredeljuego.dat
┃ ┃ ┃ ┃ ┃ ┣ 🗒 nombredeljuego.m2v
┃ ┃ ┃ ┃ ┃ ┣ 🗒 nombredeljuego.ogg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 nombredeljuego.txt
┃ ┃ ┃ ┃ ┃ ┣ 🗒 ...

>Algunos nombres de fichero pueden no tener los mismos nombres que el juego, como los ficheros `.DAT`, `.M2V` y `.OGG`. Esto no es un problema en sí mismo, siempre y cuando los ficheros `.M2V` estén correctamente listados en el archivo `.TXT`, el juego debería ejecutarse sin problemas.
{.is-info}

## Ubicación de los archivos por juego

Como es particularmente difícil colocar los ficheros correctos en los lugares apropiados, a continuación os explicamos cómo colocar los ficheros necesarios en el árbol de Recalbox, para cada uno de los juegos existentes.

> Todo lo que sigue es sólo un ejemplo de un romset que funciona, el vuestro puede funcionar de manera diferente.
{.is-info}

## {.tabset}
### Astron Best

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 astron.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 astron.commands
┃ ┃ ┃ ┃ ┃ ┣ 🗒 astron.txt
┃ ┃ ┃ ┃ ┃ ┣ 🗒 astron512.m2v
┃ ┃ ┃ ┃ ┃ ┣ 🗒 astron512.ogg
┃ ┃ ┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┃ ┃ ┣ 🗒 astron.zip

### Badlands

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 badlands.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 badlands-pc.m2v
┃ ┃ ┃ ┃ ┃ ┣ 🗒 badlands-pc.ogg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 badlands.commands
┃ ┃ ┃ ┃ ┃ ┣ 🗒 badlands.txt
┃ ┃ ┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┃ ┃ ┣ 🗒 badlands.zip

### Bega's Battle

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 bega.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 bega.commands
┃ ┃ ┃ ┃ ┃ ┣ 🗒 bega.m2v
┃ ┃ ┃ ┃ ┃ ┣ 🗒 bega.ogg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 bega.txt
┃ ┃ ┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┃ ┃ ┣ 🗒 bega.zip

### Cliff Hanger

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 cliff.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 cliff.m2v
┃ ┃ ┃ ┃ ┃ ┣ 🗒 cliff.ogg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 cliff.txt
┃ ┃ ┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┃ ┃ ┣ 🗒 cliff.zip

### Cobra Command

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 cobra.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 cc-frame53930_to_end.m2v
┃ ┃ ┃ ┃ ┃ ┣ 🗒 cobra.commands
┃ ┃ ┃ ┃ ┃ ┣ 🗒 cobra.m2v
┃ ┃ ┃ ┃ ┃ ┣ 🗒 cobra.ogg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 cobra.txt
┃ ┃ ┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┃ ┃ ┣ 🗒 cobra.zip

### Dragon's Lair

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 lair.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 lair.commands
┃ ┃ ┃ ┃ ┃ ┣ 🗒 lair.m2v
┃ ┃ ┃ ┃ ┃ ┣ 🗒 lair.ogg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 lair.txt
┃ ┃ ┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┃ ┃ ┣ 🗒 lair.zip

### Dragon's Lair II : Time Warp

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 lair2.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 dl2-00001.m2v
┃ ┃ ┃ ┃ ┃ ┣ 🗒 dl2-00001.m2v.bf
┃ ┃ ┃ ┃ ┃ ┣ 🗒 dl2-00001.ogg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 dl2-00001.ogg.bf
┃ ┃ ┃ ┃ ┃ ┣ 🗒 ...
┃ ┃ ┃ ┃ ┃ ┣ 🗒 dl2-44550.m2v
┃ ┃ ┃ ┃ ┃ ┣ 🗒 dl2-44550.m2v.bf
┃ ┃ ┃ ┃ ┃ ┣ 🗒 dl2-44550.ogg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 dl2-44550.ogg.bf
┃ ┃ ┃ ┃ ┃ ┣ 🗒 lair2.commands
┃ ┃ ┃ ┃ ┃ ┣ 🗒 lair2.txt
┃ ┃ ┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┃ ┃ ┣ 🗒 lair2.zip

### Esh's Aurunmilla

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 esh.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 esh.m2v
┃ ┃ ┃ ┃ ┃ ┣ 🗒 esh.ogg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 esh.txt
┃ ┃ ┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┃ ┃ ┣ 🗒 esh.zip

### Firefox

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 firefox.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 firefox.m2v
┃ ┃ ┃ ┃ ┃ ┣ 🗒 firefox.ogg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 firefox.txt
┃ ┃ ┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┃ ┃ ┣ 🗒 firefox.zip

### Galaxy Ranger

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 galaxy.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 galaxy.m2v
┃ ┃ ┃ ┃ ┃ ┣ 🗒 galaxy.ogg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 galaxy.txt
┃ ┃ ┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┃ ┃ ┣ 🗒 galaxy.zip

### GP World

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 gpworld.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 gpdiags.m2v
┃ ┃ ┃ ┃ ┃ ┣ 🗒 gpdiags.ogg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 gpworld.m2v
┃ ┃ ┃ ┃ ┃ ┣ 🗒 gpworld.ogg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 gpworld.txt
┃ ┃ ┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┃ ┃ ┣ 🗒 gpworld.zip

### Interstellar Laser Fantasy

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 interstellar.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 interstellar.commands
┃ ┃ ┃ ┃ ┃ ┣ 🗒 interstellar.m2v
┃ ┃ ┃ ┃ ┃ ┣ 🗒 interstellar.ogg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 interstellar.txt
┃ ┃ ┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┃ ┃ ┣ 🗒 interstellar.zip

### M.A.C.H. 3

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 mach3.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 mach3.commands
┃ ┃ ┃ ┃ ┃ ┣ 🗒 mach3.m2v
┃ ┃ ┃ ┃ ┃ ┣ 🗒 mach3.ogg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 mach3.txt
┃ ┃ ┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┃ ┃ ┣ 🗒 mach3.zip

### Road Blaster

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 roadblaster.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 roadblaster.m2v
┃ ┃ ┃ ┃ ┃ ┣ 🗒 roadblaster.ogg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 roadblater.txt
┃ ┃ ┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┃ ┃ ┣ 🗒 roadblaster.zip

### Space Ace

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 ace.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 ace.commands
┃ ┃ ┃ ┃ ┃ ┣ 🗒 ace.m2v
┃ ┃ ┃ ┃ ┃ ┣ 🗒 ace.ogg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 ace.txt
┃ ┃ ┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┃ ┃ ┣ 🗒 ace.zip

### Super Don Quix-Ote

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 sdq.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 sdq-blank.m2v
┃ ┃ ┃ ┃ ┃ ┣ 🗒 sdq-colorbars.m2v
┃ ┃ ┃ ┃ ┃ ┣ 🗒 sdq-crosshairs.m2v
┃ ┃ ┃ ┃ ┃ ┣ 🗒 sdq.commands
┃ ┃ ┃ ┃ ┃ ┣ 🗒 sdq.m2v
┃ ┃ ┃ ┃ ┃ ┣ 🗒 sdq.ogg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 sdq.txt
┃ ┃ ┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┃ ┃ ┣ 🗒 sdq.zip

### Thayer's Quest

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 tq.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 tq.m2v
┃ ┃ ┃ ┃ ┃ ┣ 🗒 tq.ogg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 tq.txt
┃ ┃ ┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┃ ┃ ┣ 🗒 tq.zip

### Us Vs Them

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 uvt.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 uvt-170606.m2v
┃ ┃ ┃ ┃ ┃ ┣ 🗒 uvt-170606.ogg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 uvt.commands
┃ ┃ ┃ ┃ ┃ ┣ 🗒 uvt.txt
┃ ┃ ┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┃ ┃ ┣ 🗒 uvt.zip