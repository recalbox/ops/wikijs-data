---
title: LinApple
description: 
published: true
date: 2024-07-24T21:48:50.297Z
tags: apple-2, linapple
editor: markdown
dateCreated: 2021-07-29T15:42:42.029Z
---

**LinApple** is an Apple ]\[ (Apple 2) emulator specially created for Linux environments.

## ![](/emulators/license.svg) License

This core is under [**GPLv2**](https://github.com/dabonetn/linapple-pie/blob/master/LICENSE) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ❌ | ❌ | ✅ |

## ![](/emulators/features.png) Features



## ![](/emulators/bios.svg) BIOS

>**No bios is required.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Supported extensions

Roms must have the extension:

* .nib
* .do
* .po
* .dsk

### Location

Put the roms like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 apple2
┃ ┃ ┃ ┃ ┣ 🗒 **jgame.dsk**

>The roms using **TOSEC** format are highly recommended.
{.is-success}

>For more information about roms, go to [this tutorial](./../../../tutorials/games/generalities/isos-and-roms)!
{.is-info}

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/dabonetn/linapple-pie/](https://github.com/dabonetn/linapple-pie/)
* **Documentation**: [http://linapple.sourceforge.net/](http://linapple.sourceforge.net/)