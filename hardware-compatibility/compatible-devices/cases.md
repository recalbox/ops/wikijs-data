---
title: Cases support on recalbox
description: List of supported cases for your recalbox, by board.
published: true
date: 2022-12-26T15:23:06.370Z
tags: compatibility, cases
editor: markdown
dateCreated: 2021-08-05T19:12:20.834Z
---

## PI4

### PIStation
☑️ Supported by recalbox

![boitier-pistation-retroflag-et-ecran-lcd_(1).jpg](/compatibility/cases/boitier-pistation-retroflag-et-ecran-lcd_(1).jpg)

[Link to buy](https://www.kubii.fr/boitiers/3031-boitier-nespi-case-pour-raspberry-pi-4-3272496302303.html)

### NESPi 4 CASE
☑️ Auto detected by Recalbox

![For Pi4](/compatibility/cases/nespi4case.png)

[Link to buy](https://www.kubii.fr/boitiers/3031-boitier-nespi-case-pour-raspberry-pi-4-3272496302303.html)

### Argon One
☑️ Supported by recalbox

![For Pi4](/compatibility/cases/argonone.png)

[Link to buy](https://www.kubii.fr/boitiers-et-supports/2762-boitier-argon1-pour-raspberry-pi-4-3272496299672.html)

### PiBoy DMG
☑️ Auto detected by Recalbox

![piboy-dmg-2.jpg](/compatibility/cases/piboy-dmg-2.jpg)

[Link to buy](https://www.kubii.fr/consoles-systemes/3046-piboy-dmg-725272730706.html)

### GPi CASE 2 (CM4)
☑️ Auto detected by Recalbox

> We advice against using CM4 with flash memory (eMMC), because, by design by Raspberry foundation, eMMC will take place of GPI Case 2 microSD access, making unusable this port.
You will have access only to eMMC for installing Recalbox.
{.is-warning}

![gpi-case-2-dock.jpg](/compatibility/cases/gpi-case-2-dock.jpg)

[Link to buy](https://www.kubii.fr/boitiers-console/3554-gpi-case-2-dock-6971727910221.html)

### Raspberry Pi Touch Display
☑️ Supported by Recalbox

> You'll have to plug an HDMI display first to enable the display.
{.is-info}

![rpi-touch-display.png](/compatibility/cases/rpi-touch-display.png)

[Link to buy](https://www.kubii.fr/ecrans-afficheurs/1131-ecran-tactile-officiel-7-800x480-kubii-5056070923850.html)

## Pi ZERO/Pi ZERO W

### GPi CASE
☑️ Auto detected by Recalbox

![For Pi Zero and Pi Zero W](/compatibility/cases/gpicase.png)

[Link to buy](https://www.kubii.fr/consoles-retro-gaming/2719-gpi-case-retroflag-kubii-3272496299276.html)

## PI2/PI3/PI3B/PI3B+

### NESPi CASE
☑️ Supported by recalbox

![](/compatibility/cases/nespicase.png)

[Link to buy](https://www.reichelt.com/fr/en/case-for-raspberry-pi-3-nes-design-gray-rpi-nespi-case-p211465.html?&trstct=pos_0&nbc=1)

### NESPi CASE PLUS
☑️ Supported by recalbox

![For Pi2, Pi3, Pi3B and Pi3B+](/compatibility/cases/nespicaseplus.png)

[Link to buy](https://www.kubii.fr/boitiers-et-supports/2036-boitier-nespi-case-pour-raspberry-pi-3-2-b-kubii-327249600861.html)

### SUPERPI CASE J
☑️ Supported by recalbox

![For Pi2B, Pi3B and Pi3B+](/compatibility/cases/superpicase.png)

[Link to buy](https://www.kubii.fr/boitiers-et-supports/2283-superpicase-j-kubii-3272496011922.html)

### SUPERPI CASE U
☑️ Supported by recalbox

![For Pi2, Pi3 and Pi3B+](/compatibility/cases/superpicaseusa.png)

[Link to buy](https://www.amazon.com/dp/B07G34TTKL?m=A3I7DCARNWUK2P&ref_=v_sp_detail_page&th=1)

### SUPERPI CASE U (New)
☑️ Supported by recalbox

![For Pi2, Pi3 and Pi3B+](/compatibility/cases/superpicaseusanew.png)

[Link to buy](https://www.amazon.com/dp/B07W5L95KK?ref=myi_title_dp)

### MEGAPi CASE M
☑️ Supported by recalbox


![For Pi2B, Pi3B and Pi3B+](/compatibility/cases/megapicase.png)

[Link to buy](https://www.kubii.fr/consoles-retro-gaming/2337-boitier-megapi-case-kubii-3272496012646.html)

### Raspberry Pi Touch Display
☑️ Supported by Recalbox

> You'll have to plug an HDMI display first to enable the display.
{.is-info}

![rpi-touch-display.png](/compatibility/cases/rpi-touch-display.png)

[Link to buy](https://www.kubii.fr/ecrans-afficheurs/1131-ecran-tactile-officiel-7-800x480-kubii-5056070923850.html)

### Thingivers 

There is no preview, there is much differents cases to print with a 3D printer.

[Link to buy](https://www.thingiverse.com/tag:raspberry_pi_4)

## PI4 Functional cheap case

### GeeekPi

![For Pi4](/compatibility/cases/geekpi.png)

[Link to buy](https://www.amazon.fr/dp/B07XXQ34PZ/ref=cm_sw_r_cp_apa_i_MdpqFb2V1Q6F6)

### GeeekPi

![For Pi4](/compatibility/cases/geekpi2.png)

[Link to buy](https://www.amazon.fr/GeeekPi-Raspberry-Ventilateur-40X40X10mm-Dissipateurs/dp/B07XCKNM8J/ref=sr_1_3?__mk_fr_FR=%C3%85M%C3%85%C5%BD%C3%95%C3%91&dchild=1&keywords=boitier+pi+4&qid=1598103872&sr=8-3)

### Bruphny

![For Pi4](/compatibility/cases/bruphny.png)

[Link to buy](https://www.amazon.fr/Bruphny-Ventilateur-Alimentation-Dissipateurs-Adaptateur/dp/B07WN3CHGH/ref=sr_1_5?__mk_fr_FR=%C3%85M%C3%85%C5%BD%C3%95%C3%91&dchild=1&keywords=boitier+pi+4&qid=1598103952&sr=8-5)

## Odroid XU4

### Gaming case

![For XU4](/compatibility/cases/ogst.png)

[Link to buy](https://www.kubii.fr/odroid/2214-boitier-gaming-pour-odroid-xu4-kubii-3272496011250.html)