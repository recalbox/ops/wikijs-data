---
title: Libretro Flycast
description: 
published: true
date: 2024-11-14T19:56:57.893Z
tags: libretro, atomiswave, flycast
editor: markdown
dateCreated: 2021-05-21T08:12:57.844Z
---

**Libretro Flycast** est un émulateur **Sega Dreamcast multi-plateformes** capable d'émuler le **Sammy Atomiswave.**

## ![](/emulators/license.svg) Licence

Ce core est sous licence [**GPLv2**](https://github.com/libretro/flycast/blob/master/LICENSE).

## ![](/emulators/compatibility.png) Compatibilité

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ❌ | ❌ | ✅ | ✅ | ✅ | ✅  🐌 |✅ | ✅ |

🐌  Basses performances mais jouable

## ![](/emulators/features.png) Fonctionnalités

| Fonctionnalité | Supporté |
| :---: | :---: |
| Redémarrage | ✔ |
| Captures d'écran | ✔ |
| Sauvegardes | ✔ |
| Options du core | ✔ |
| Cheats RetroArch | ✔ |
| Controls | ✔ |
| Remapping | ✔ |
| Vibration | ✔ |
| Disk Control | ✔ |

## ![](/emulators/bios.svg) BIOS

### Liste des bios obligatoires

| Nom de fichier | Description | MD5 | Fourni |
| :---: | :---: | :---: | :---: |
| awbios.zip | Bios « Atomiswave » de Mame | 0ec5ae5b5a5c4959fa8b43fcf8687f7c | ❌ |

### Emplacement

Placez les bios comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 dc
┃ ┃ ┃ ┃ ┣ 🗒 **awbios.zip**

## ![](/emulators/roms.png) Roms

Libretro Flycast se base sur le Romset de **MAME** mais aussi sur des formats **NullDC** pour sa partie Atomiswave.

### Extensions supportées

Les roms doivent avoir les extensions suivantes :

* .zip
* .7z
* .bin/.lst
* .dat

## Romset Mame

Atomiswave se base sur le romset de Mame indiqué [ici](../).

>Vous devez obligatoirement passer votre romset MAME dans ClrMamePro pour avoir un romset Atomiswave fonctionnel.
{.is-info}

>Afin de trier vos roms arcade, le **fichier dat** est disponible dans le dossier :`/recalbox/share/arcade/libretro/`
{.is-info}

### Emplacement

Placez les roms comme ceci : 

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 atomiswave
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.zip**

## Romset NullDC

Ces roms sont compatibles avec Flycast mais **moins fiables** que des roms **issues d'un set MAME.**

>Les Roms **NullDC** sont au format : _`.bin + .lst`_
{.is-info}

### Emplacement

Placez les roms comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 atomiswave
┃ ┃ ┃ ┃ ┣ 📁 **jeu**
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **jeu.bin**
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **jeu.lst**

## ![](/emulators/advanced-configuration.png) Configuration avancée de l'émulateur

>Pour pouvoir conserver vos configurations personnalisées lors d'une mise à jour, nous vous conseillons d'utiliser notre fonctionnalité [Surcharges de configuration](./../../../advanced-usage/configuration-override).
{.is-info}

### Accéder aux options

Vous pouvez configurer diverses options de deux façons différentes.

* Via le Menu RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via le fichier `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Options du core

| Options | Valeur des options | Variable (pour surcharge) | Valeurs des variables |
| --- | --- | --- | --- |
| System Type (Restart) | `Auto` ✅ / `Dreamcast` / `NAOMI` / `Atomiswave` | `reicast_system` | `auto` / `dreamcast` / `naomi` / `atomiswave` |
| Internal resolution (restart) | `320x240` ✅ / `640x480` / `800x600` / `960x720` / `1024x768` / `1280x960` / `1440x1080` / `1600x1200` / `1920x1440` / `2560x1920` / `2880x2160` / `3200x2400` / `3840x2880` / `4480x3360` / `5120x3840` / `5760x4320` / `6400x4800` / `7040x5280` / `7680x5760` / `8320x6240` / `8960x6720` / `9600x7200` / `10240x7680` / `10880x8160` / `11520x8640` / `12160x9120` / `12800x9600` | `reicast_internal_resolution` | `320x240` / `640x480` / `800x600` / `960x720` / `1024x768` / `1280x960` / `1440x1080` / `1600x1200` / `1920x1440` / `2560x1920` / `2880x2160` / `3200x2400` / `3840x2880` / `4480x3360` / `5120x3840` / `5760x4320 / ``6400x4800` / `7040x5280` / `7680x5760` / `8320x6240` / `8960x6720` / `9600x7200` / `10240x7680` / `10880x8160` / `11520x8640` / `12160x9120` / `12800x9600` |
| Screen Orientation | `Horizontal` ✅ / `Vertical` | `reicast_screen_rotation` | `horizontal` / `vertical` |
| Alpha Sorting | `Per-Strip (fast, least accurate)` / `Per-Triangle (normal)` ✅ | `reicast_alpha_sorting` | `per-strip (fast, least accurate)` / `per-triangle (normal)` |
| Mipmapping | `Activé` ✅ / `Désactivé` | `reicast_mipmapping` | `enabled` / `disabled` |
| Fog Effects | `Activé` ✅ / `Désactivé` | `reicast_fog` | `enabled` / `disabled` |
| Volume modifier | `Activé` ✅ / `Désactivé` | `reicast_volume_modifier_enable` | `enabled` / `disabled` |
| Widescreen Hack (Restart) | `Désactivé` ✅ / `Activé` | `reicast_widescreen_hack` | `disabled` / `enabled` |
| Widescreen Cheats (Restart) | `Désactivé` ✅ / `Activé` | `reicast_widescreen_cheats` | `disabled` / `enabled` |
| Region | `Default` ✅ / `Japan` / `USA` / `Europe` | `reicast_region` | `Default` / `Japan` / `USA` / `Europe` |
| DIV Matching | `Désactivé` / `Auto` ✅ | `reicast_div_matching` | `disabled` / `auto` |
| Analog Stick Deadzone | `0%` / `5%` / `10%` / `15%` ✅ / `20%` / `25%` / `30%` | `reicast_analog_stick_deadzone` | `0%` / `5%` / `10%` / `15%` / `20%` / `25%` / `30%` |
| Trigger Deadzone | `0%` ✅ / `5%` / `10%` / `15%` / `20%` / `25%` / `30%` | `reicast_trigger_deadzone` | `0%` / `5%` / `10%` / `15%` / `20%` / `25%` / `30%` |
| Digital Triggers | `Désactivé` ✅ / `Activé` | `reicast_digital_triggers` | `disabled` / `enabled` |
| Enable DSP | `Désactivé`  / `Activé` ✅ | `reicast_enable_dsp` | `disabled` / `enabled` |
| Anisotropic Filtering | `Off` / `2` / `4` ✅ / `8` / `16` | `reicast_anisotropic_filtering` | `disabled` / `2` / `4` / `8` / `16` |
| PowerVR2 Post-processing Filter | `Désactivé` ✅ / `Activé` | `reicast_pvr2_filtering` | `disabled` / `enabled` |
| Texture Upscaling (xBRZ) | `Off` ✅ / `2` / `4` / `6` | `reicast_texupscale` | `disabled` / `2` / `4` / `6` |
| Enable RTT (Render To Texture) Buffer | `Désactivé` ✅ / `Activé` | `reicast_enable_rttb` | `disabled` / `enabled` |
| Render To Texture Upscaling | `1x` ✅ / `2x` / `3x` / `4x` / `8x` | `reicast_render_to_texture_upscaling` | `1x` / `2x` / `3x` / `4x` / `8x` |
| Threaded Rendering (Restart) | `Désactivé`  / `Activé` ✅ | `reicast_threaded_rendering` | `disabled` / `enabled` |
| Synchronous Rendering | `Désactivé`  / `Activé` ✅ | `reicast_synchronous_rendering` | `disabled` / `enabled` |
| Delay Frame Swapping | `Désactivé` ✅ / `Activé` | `reicast_delay_frame_swapping` | `disabled` / `enabled` |
| Frame Skipping | `Désactivé` ✅ / `1` / `2` / `3` / `4` / `5` / `6` | `reicast_frame_skipping` | `off` / `1` / `2` / `3` / `4` / `5` / `6` |
| Allow NAOMI Service Buttons | `Désactivé` ✅ / `Activé` | `reicast_allow_service_buttons` | `disabled` / `enabled` |
| Enable NAOMI 15KHz Dipswitch | `Désactivé` ✅ / `Activé` | `reicast_enable_naomi_15khz_dipswitch` | `disabled` / `enabled` |
| Load Custom Textures | `Désactivé` ✅ / `Activé` | `reicast_custom_textures` | `disabled` / `enabled` |
| Dump Textures | `Désactivé` ✅ / `Activé` | `reicast_dump_textures` | `disabled` / `enabled` |
| Gun Crosshair 1 Display | `Désactivé` ✅ / `White` / `Red` / `Green` / `Blue` | `reicast_lightgun1_crosshair` | `disabled` / `White` / `Red` / `Green` / `Blue` |
| Gun Crosshair 2 Display | `Désactivé` ✅ / `White` / `Red` / `Green` / `Blue` | `reicast_lightgun2_crosshair` | `disabled` / `White` / `Red` / `Green` / `Blue` |
| Gun Crosshair 3 Display | `Désactivé` ✅ / `White` / `Red` / `Green` / `Blue` | `reicast_lightgun3_crosshair` | `disabled` / `White` / `Red` / `Green` / `Blue` |
| Gun Crosshair 4 Display | `Désactivé` ✅ / `White` / `Red` / `Green` / `Blue` | `reicast_lightgun4_crosshair` | `disabled` / `White` / `Red` / `Green` / `Blue` |

## ![external-links.png](/emulators/external-links.png) Liens externes

* **Code source utilisé** : [https://github.com/libretro/flycast](https://github.com/libretro/flycast)
* **Documentation Libretro** : [https://docs.libretro.com/library/flycast/](https://docs.libretro.com/library/flycast/)
* **Code source officiel** : [https://github.com/flyinghead/flycast](https://github.com/flyinghead/flycast)