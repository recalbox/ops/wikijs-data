---
title: Y para los usuarios avanzados
description: Cómo añadir uno mismo juegos "lightgun" ,  como un adulto !
published: true
date: 2024-07-27T21:47:25.305Z
tags: lightgun, 7.2+, usuarios avanzados
editor: markdown
dateCreated: 2023-09-24T13:24:26.960Z
---

## ¿Cómo avanzar y modificar el sistema Lightgun?

### La idea básica...

El principio básico consiste en tener un único fichero de configuración que contiene toda la información necesaria para los juegos Lightgun en todos los sistemas y emuladores disponibles. 

De esta manera, podemos heredar, agrupar juegos y también gestionar características especiales sin tener que hacer un fichero por cada juego o por cada sistema, que es lo que se podía hacer hasta ahora (pero sin poder agrupar sistemas, por ejemplo). Mediante un único fichero xml, disponemos de más flexibilidad. 

Es más, en versiones posteriores de Recalbox se podrá añadir ayuda online, mensajes cuando se carguen los juegos, etc...

Otra ventaja de este único fichero xml comparado con los ficheros de sobrecargas tradicionales es que se pueden añadir funciones al script python de Configgen para buscar/identificar si hay hardware presente, como la Mayflash dolphin bar, o para saber exactamente en qué hardware está instalado Recalbox. A día de hoy, Recalbox permite configurar de 1 a 3 jugadores Lightgun.

>Por el momento, esto sólo es posible para los core Libretro. No hay nada que nos impida hacerlo para otros emuladores con el fichero xml que hemos puesto en funcionamiento.
{.is-info}

### El fichero XML de configuración

La configuración "lightgun" de los juegos se encuentra dentro de un fichero xml (por información, en realidad se trada de un fichero ".cfg").

A continuación os mostramos cómo se encuentra organizado el fichero (aquí abajo el fichero está vacío sin ninguna configuración con el objetivo de mostraros la jerarquía del xml al completo)_:

```xml
<?xml version="1.0"?>
<root>
	<emulatorOptions>
	  <option />
 	</emulatorOptions>
  <coreOptions>
	  <option />
  </coreOptions>
  <system>
 		<emulatorList>
      <emulator />
    </emulatorList>
		<emulatorOptions>
			<option />
		</emulatorOptions>
    <coreOptions>
			<option />
    </coreOptions>
    <gameList>
      <game>
        <emulatorOptions>
          <option />
        </emulatorOptions> 
        <coreOptions>
          <option />
        </coreOptions>
      </game>
    </gameList>
  </system>
</root>
```

### Los Tags XML

**&lt;root>** : esta es la raíz XML donde se encontrará todo anidado, así como una versión para realizar un seguimiento de los cambios del fichero

```xml
<?xml version="1.0"?>
<root>
	<!-- internal version of the file to help follow-up of update, same version until release -->
	<version>1.0.6 - 18-03-2024</version>
</root>
```

 **&lt;emulatorOptions>** : Es una sección de &lt;root> que sobrecarga el fichero `retroarchcustom.cfg` que existe en todos los core Libretro. Para el sitema lightgun, vamos a configurar dentro de esta estructura los botones básicos como select/exit/start/trigger.

```xml
	<!-- MANDATORY inputs (for retroarchcustom.cfg): comon part to use for all systems (using dolphinbar and until 2 wiimotes) -->
	<emulatorOptions>
        <option name="input_player1_gun_start" value="enter" />
        <option name="input_player2_gun_start" value="enter" />
        <option name="input_player1_gun_select" value="escape" />
        <option name="input_player2_gun_select" value="escape" />
        <!-- force selecte and start for consoles games -->
        <option name="input_player1_select" value="escape" />
        <option name="input_player2_select" value="escape" />
        <option name="input_player1_start" value="enter" />
        <option name="input_player2_start" value="enter" />
        <!-- for many it's necessary to move in menu or else -->
        <option name="input_player1_gun_dpad_up" value="up" />
        <option name="input_player1_gun_dpad_down" value="down" />
        <option name="input_player1_gun_dpad_right" value="right" />
        <option name="input_player1_gun_dpad_left" value="left" />
        <option name="input_player2_gun_dpad_up" value="up" />
        <option name="input_player2_gun_dpad_down" value="down" />
        <option name="input_player2_gun_dpad_right" value="right" />
        <option name="input_player2_gun_dpad_left" value="left" />
        <!-- index to manage distinction between guns -->
        <option name="input_player1_mouse_index" value="GUNP1" />
        <option name="input_player2_mouse_index" value="GUNP2" />
        <!-- wiimotes home button for exit game -->
		    <option name="input_exit_emulator" value="lsuper" />
	  	  <option name="input_enable_hotkey" value="lsuper" />
        <!-- set on fullscreen for don't see cursor mouse on x86(-64) -->
	  	  <option name="video_fullscreen" value="true" />
        <!-- to have help using overlay switch and finally ask to press 2 times home to exit -->
        <option name="input_overlay_next" value="lsuper" />
        <option name="quit_press_twice" value="true" />
 	</emulatorOptions>
```

 **&lt;coreOptions>** :  Es una sección de &lt;root> que sobrecarga el fichero `retroarch-core-options.cfg` para los core Libretro. En nuestro caso esta sección no se utiliza de forma compartida pero técnicamente podría implementarse de forma común si necesario.

```xml
    <!-- OPTIONAL options (for retroarch-core-options.cfg): comon part to use for all systems (using dolphinbar and until 2 wiimotes) -->
  	<coreOptions>
        <!-- only for more log on test -->
        <!-- <option name="libretro_log_level" value="2" />
        <option name="log_verbosity" value="true" /> -->
  	</coreOptions>
```

 **&lt;option>** : estos son los valores básicos de configuración que se componen de dos atributos `name` y `value`:

```xml
<option name="input_player1_gun_start" value="enter" />
```

**&lt;system>** : esta parte regrupa toda la configuración de un determinado sistema. Encontramos los tags`&lt;emulatorList>`, `&lt;emulator>`, `&lt;emulatorOptions>`, `&lt;coreOptions>`, `&lt;gameList>` et `&lt;game>`.

```xml
  <!-- ********************************************************* Nintendo Entertainment System ************************************************************** -->
	<system name="nes">
		<!-- MANDATORY: emulator and core selected for lightgun -->
		<emulatorList name="libretro">
			<emulator priority="1" name="libretro" core="fceumm" />
		</emulatorList>
    <!-- MANDATORY: inputs common to this system -->
		<emulatorOptions>
      	<!-- gamepad -->
      	<option name="input_libretro_device_p0" value="1" />
      	<!-- lightgun -->
      	<option name="input_libretro_device_p1" value="258" />
      	<!-- Wiimote button B -->
		  	<option name="input_player2_gun_trigger_mbtn" value="1" />
        <!-- Set index to GUNP1 on player 2 to have first wiimote as player 1 -->
        <option name="input_player2_mouse_index" value="GUNP1" />
		</emulatorOptions>
		<!-- MANDATORY: options common to this system -->
		<coreOptions>
		  	<option name="fceumm_zapper_mode" value="clightgun" />
		  	<option name="fceumm_zapper_tolerance" value="6" />
      	<option name="fceumm_show_crosshair" value="enabled" />
		</coreOptions>
        <gameList>
            <!-- list of games using lightgun -->
            <!-- see 'duckhunt' game to see more explainations to know how to setup game part -->
            <game name="3in1supergun" tested="true" />
```


**&lt;emulatorList>** : esta etiqueta enumera la lista de emuladores compatibles, en la mayoría de los casos se trata de un solo emulador:

```xml
    	<!-- MANDATORY: emulator and core selected for lightgun-->
    	<emulatorList>
      		<emulator priority="1" name="libretro" core="flycast" />
    	</emulatorList>
```

 **&lt;emulator>** : esta etiqueta tiene como valor la prioridad, el nombre del emulador y el núcleo a utilizar. Siempre se encuentra dentro de  `&lt;emulatorList>` como se puede ver justo arriba.

```xml
<emulator priority="1" name="libretro" core="flycast" />
```

**&lt;emulatorOptions>** : este tag permite sobrecargar `retroarchcustom.cfg` en la parte de `&lt;emulatorOptions>` a nivel de `&lt;system>`, `&lt;gameList>` y/o `&lt;game>`.

```xml
<system>
.....
		<emulatorOptions>
      	<!-- gamepad -->
      	<option name="input_libretro_device_p0" value="1" />
      	<!-- lightgun -->
      	<option name="input_libretro_device_p1" value="258" />
      	<!-- Wiimote button B -->
		  	<option name="input_player2_gun_trigger_mbtn" value="1" />
        <!-- Set index to GUNP1 on player 2 to have first wiimote as player 1 -->
        <option name="input_player2_mouse_index" value="GUNP1" />
.....
```

```xml
.....
        <gameList>
            <emulatorOptions>
                <!-- no reload, this game is a free fire but need secondary input for grenade and more -->
                <option name="input_player1_a_mbtn" value="2" />
                <option name="input_player2_a_mbtn" value="2" />     
            </emulatorOptions>
            <game name="alien3thegun" tested="true" />
.....
```

```xml
.....            
            <game name="gunbuster" tested="true">
                <name>gunbuster</name>
                <emulatorOptions>
                    <!-- no reload free fire game but ned secondary input for grenade and more -->
                    <!-- move players with dpad on wiimote -->
                    <option name="input_player1_a" value="2" />
                    <option name="input_player1_gun_offscreen_shot_mbtn" value="nul" />
                    <option name="input_player2_a" value="2" />
                    <option name="input_player2_gun_offscreen_shot_mbtn" value="nul" />
                </emulatorOptions>
            </game>
.....
```

**&lt;coreOptions>**: este tag permite sobrecargar `retroarch-core-options.cfg` en la zona de `&lt;coreOptions>` al nivel de `&lt;system>`, `&lt;gameList>` y/o `&lt;game>`. 

```xml
        <!-- options common to this system-->
        <coreOptions>
            <option name="genesis_plus_gx_gun_input" value="lightgun" />
            <option name="genesis_plus_gx_gun_cursor" value="enabled" />
        </coreOptions>
```

**&lt;gameList>** : esta etiqueta es la raíz de los grupos de juegos. Esto permite obtener una configuración separada para algunos de los juegos del sistema en cuestión. Podemos tener varias secciones _&lt;games>_ dentro del mismo sistema.

```xml
        <gameList>
            <emulatorOptions>
                <!-- no reload, these games have a free fire but need secondary input for grenade and more -->
                <option name="input_player1_a_mbtn" value="2" />
                <option name="input_player2_a_mbtn" value="2" />     
            </emulatorOptions>
            <game name="alien3thegun" tested="true" />
            <game name="beastbusters" tested="true" />
            <game name="dragongun" tested="true" />
        </gameList>
```

 **&lt;game>** : este tag que se encuentra dentro de &lt;gameList> se utiliza para definir la configuración de un juego en concreto.

```xml
            <game name="gunbuster" tested="ok">
                <emulatorOptions>
                    <!-- no reload free fire game but ned secondary input for grenade and more -->
                    <!-- move players with dpad on wiimote -->
                    <option name="input_player1_a" value="2" />
                    <option name="input_player1_gun_offscreen_shot_mbtn" value="nul" />
                    <option name="input_player2_a" value="2" />
                    <option name="input_player2_gun_offscreen_shot_mbtn" value="nul" />
                </emulatorOptions>
            </game>
```


>**&lt;game>** posee los atributos `name` (para reconocer el juego que se lanza) y `tested` para saber si ya se ha testeado la rom y si se conoce su estado (2 valores posibles):
>* **true** -> testeado con la configuración especificada.
>* **false** -> testeado pero no funcional, lo que quiere decir que no será configurado en modo lightgun por el momento (aunque siempre nos queda la esperanza de que esto cambie en el futuro).

>Siempre hay que poner el nombre **sin espacios/caracteres especiales y en minúsculas** para que Recalbox realice correctamente el parsing de la «gamelist» de un sistema. Ojo, es el nombre del juego en su forma más simple, no el nombre del fichero rom, ni el nombre con la zona geográfica, etc... hacedlo lo más sencillo posible para que la correspondencia funcione. **Un scraping del juego os permite aseguraros que utilizáis un nombre correcto e identificable por Recalbox**.
{.is-warning}

### Herencia

En este párrafo, nos gustaría explicar que la configuración se aplicará de la siguiente forma:

 `common` -> `system` -> `gameList` -> `game`

.... Os dejamos a modo de ejercicio explicarnos qué valor tendrá la clave + `input_player2_gun_trigger_mbtn` en el siguiente ejemplo:

```xml
<?xml version="1.0"?>
<root>
	<emulatorOptions>
	  <option name="input_player2_gun_trigger_mbtn" value="1" />
 	</emulatorOptions>
  <coreOptions>
  </coreOptions>
  <system>
		<emulatorList>
			<emulator></emulator>
		</emulatorList>
    <emulatorOptions>
      <string name="input_player2_gun_trigger_mbtn" value="2" />
    </emulatorOptions>
    <coreOptions>
    </coreOptions>
    <gameList>
      <emulatorOptions>
        <string name="input_player2_gun_trigger_mbtn" value="1" />
      </emulatorOptions>
      <coreOptions>
      </coreOptions>
      <game>
        <emulatorOptions>
          <option name="input_player2_gun_trigger_mbtn" value="2" />
        </emulatorOptions>
        <coreOptions>
        </coreOptions>
      </game>
    </gameList>
  </system>
</root>
```

### Gestión de índices para multijugador

Hemos tenido que gestionar los índices de "ratón" y especificarlos de manera común o por cada juego. Por ejemplo, cuando el jugador 1 está en el puerto 2, y el jugador 2 en el puerto 3. Para ello, hemos creado varias palabras clave como `GUNP1`, `GUNP2` o `GUNP3`.

```xml
.....
     <emulatorOptions>
        <!-- index to manage distinction between guns -->
        <option name="input_player1_mouse_index" value="GUNP1" />
        <option name="input_player2_mouse_index" value="GUNP2" />
        .....
```

```xml
        <gameList> <!-- games using justifier -->
            <emulatorOptions>
                <!-- lightguns always on port 2 -->
                <!-- For Justifier device (blue cross) -->
                <string name="input_libretro_device_p2" value="516" />
                <string name="input_player2_mouse_index" value="GUNP1" />
                <!-- Wiimote button B or Z -->
                <string name="input_player2_gun_trigger_mbtn" value="1" />
                <!-- For Justifier(P2) device (pink cross) -->
                <string name="input_libretro_device_p3" value="772" />
                <string name="input_player3_mouse_index" value="GUNP2" />
                <!-- Wiimote button B or Z -->
                <string name="input_player3_gun_trigger_mbtn" value="1" />
                <!-- Common inputs part replicated for this game using player 3 -->
                <string name="input_player3_gun_start" value="enter" />
                <string name="input_player3_gun_select" value="escape" />
                <!-- Common inputs part replicated to force select and start for consoles games -->
                <string name="input_player3_select" value="escape" />
                <string name="input_player3_start" value="enter" />
            </emulatorOptions>
            <game name="lethalenforcers" tested="true" />
        </gameList>
```

## De una forma sencilla...

En realidad, el fichero xml de configuración no se puede encontrar dentro de vuestro "share". Se encuentra únicamente accesible por SSH: `/recalbox/share_init/system/.emulationstation/lightgun.cfg.`

>Para modificar el fichero hay que montar la [partición sistema](./../../../tutorials/system/access/remount-partition-with-write-access) en modo lectura/escritura.
{.is-warning}

### ¿Cómo añado un juego/rom?

Basta con encontrar el sistema, ejemplo para la NES :

![](/basic-usage/features/lightgun/advanced-users/advancedusers2.png)

Y añadir dentro de la sección &lt;games> el tag &lt;game> y a continuación &lt;name> de esta forma:

```xml
           <game name="monjeu2" tested="true" />
```

>RECORDAD: Poned siempre el nombre del juego **sin espacios/caracteres especiales y en minúsculas**. Tened cuidado, es el nombre del juego en su forma más simple, no es el nombre de la rom ni el nombre del juego más su zona geográfica, etc... escribidlo lo más sencillamente posible para garantizar un buen "match". **Si lanzáis un scrapeado obtendréis el buen nombre en el fichero "gamelist.xml", lo que permitirá poder realizar el matching con el XML de configuración del sistema "lightgun".**
{.is-advertencia}

### Para comprobar que se tiene en cuenta

* Si el fichero está mal formateado, no podrás lanzar el juego y volveréis directamente a la lista de juegos.
* Si no se encuentra/reconoce el nombre del juego, no podréis activar el modo Lightgun.
* Si realmente os encontráis en modo Lightgun, debéis poder salir del juego usando el botón HOME del Wiimote.

### Para añadir configuraciones a un juego

Lo mejor que podéis hacer es observar lo que está hecho para otros juegos... En general, tendréis que cambiar las configuraciones de los botones de disparo primario y secundario, o incluso desactivar teclas (ver más abajo), pero esto puede ser aún más complicado... Si este es el caso, tendréis que consultar los manuales de los juegos de la época y pasar mucho tiempo navegando por los menús de RetroArch... ¡así que os aconsejamos bien dominar el arte de las sobrecargas!

```xml
        <game name="ninjaassault" tested="true">
            <emulatorOptions>
                <!-- for this game start p1 for P2 aux_c -->
                <string name="input_player1_gun_start" value="nul" />
                <string name="input_player1_gun_aux_b" value="enter" />
                <!-- for this game trigger -->
                <string name="input_player1_gun_trigger_mbtn" value="nul" />
                <string name="input_player1_gun_aux_a_mbtn" value="1" />
                <!-- for this game real offscreen reload -->
                <string name="input_player1_gun_offscreen_shot_mbtn" value="nul" />
            </emulatorOptions>
        </game>
```

### Para añadir un nuevo sistema

En este caso, hay que estar seguro del core que se debe utilizar, conocer bien los parámetros de RetroArch y configurar el "device" dentro de `&lt;emulatorOptions>` como también los parámetros "lightgun" del core en cuestión dentro de `&lt;coreOptions>`.

```xml
......
<system name="megadrive">
    <!-- MANDATORY: emulator and core selected for lightgun -->
    <emulatorList name="libretro">
        <emulator priority="1" name="libretro" core="genesisplusgx" />
    </emulatorList>
    <!-- MANDATORY: inputs common to this system -->
    <emulatorOptions>
        <!-- Wiimote button B or Z -->
        <option name="input_player2_gun_trigger_mbtn" value="1" />
        <!-- Wiimote button A or c - aux when it's necessary as jump, rocket, etc... -->
        <option name="input_player2_gun_aux_a_mbtn" value="2" />
        <!-- <option name="input_player2_gun_aux_a_mbtn" value="2" /> -->
        <!-- gamepad always on port 1-->
        <option name="input_libretro_device_p1" value="1" />
    </emulatorOptions>
    <!-- MANDATORY: options common to this system -->
    <coreOptions>
        <option name="genesis_plus_gx_gun_input" value="lightgun" />
        <option name="genesis_plus_gx_gun_cursor" value="enabled" />
    </coreOptions>
    .....
```

**Animo y buena suerte !!!**