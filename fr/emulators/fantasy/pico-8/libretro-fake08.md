---
title: Libretro Fake08
description: 
published: true
date: 2024-06-23T11:41:51.152Z
tags: libretro, 9.2+, fake08
editor: markdown
dateCreated: 2024-06-23T11:41:51.152Z
---



## ![](/emulators/license.svg) Licence

Ce core est sous licence [**MIT**](https://github.com/jtothebell/fake-08/blob/master/LICENSE.MD).

## ![](/emulators/compatibility.png) Compatibilité

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Fonctionnalités

| Fonctionnalité | Supporté |
| :---: | :---: |

## ![](/emulators/bios.svg) BIOS

>**Aucun bios n'est requis.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Extensions supportées

Les roms doivent avoir l'extension :

* .p8
* .png
* .zip
* .7z

### Emplacement

Placez les roms comme ceci : 

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 pico8
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.png**

## ![](/emulators/advanced-configuration.png) Configuration avancée de l'émulateur



## ![](/emulators/external-links.png) Liens externes

* **Code source utilisé** : [https://github.com/jtothebell/fake-08/](https://github.com/jtothebell/fake-08/)
* **Forum Pico-8** : [https://www.lexaloffle.com/bbs/?cat=7](https://www.lexaloffle.com/bbs/?cat=7)