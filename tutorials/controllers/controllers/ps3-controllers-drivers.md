---
title: Les pilotes des manettes PS3
description: 
published: true
date: 2021-09-04T22:48:47.362Z
tags: ps3, manettes, pilotes
editor: markdown
dateCreated: 2021-05-21T08:35:23.002Z
---

Il existe différentes versions de manettes PS3 _sixaxis_ et _dualshock3_ sur le marché.

Recalbox supporte par défaut les manettes officielles. Mais si vous possédez un clone GASIA ou Shanwan, vous devez modifier le pilote dans [recalbox.conf](./../../../basic-usage/getting-started/recalboxconf-file) en modifiant la ligne suivante par le contenu du tableau dessous :

```ini
controllers.ps3.driver=official
```

| Modèle | Pilote |
| :--- | :--- |
| Sixaxis | `official` |
| Dualshock3 | `official` |
| US Sixaxis | `shanwan` ?? |
| Shanwan | `shanwan` |
| Gasia | `gasia` |

Pour déterminer quel type de clone vous possédez (GASIA ou SHANWAN), vous pouvez :

* Brancher en USB votre manette
* Puis utilisez la commande suivante :

```shell
dmesg
```

Vous devriez alors voir le type de manette que vous possédez.