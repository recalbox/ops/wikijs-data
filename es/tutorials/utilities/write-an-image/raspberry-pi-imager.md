---
title: Raspberry Pi Imager
description: 
published: true
date: 2023-09-17T07:18:42.613Z
tags: pi, raspberry, imager
editor: markdown
dateCreated: 2023-09-17T07:18:42.613Z
---

El [Raspberry Pi Imager](https://www.raspberrypi.org/software/) (muy fácil de usar) os permite flashear un fichero imagen en cualquier medio de almacenamiento, como una tarjeta microSD. Flashear una imagen es rápido y sencillo.

* Visitad [Raspberry Pi Imager](https://www.raspberrypi.org/software/) para ir al enlace de descarga, a continuación, instaladlo en vuestro ordenador:

![](/tutorials/utilities/image-write/rpi-imager/rpi-imager1.png){.full-width}

* Una vez instalado, el programa se presenta de la siguiente forma:

![](/tutorials/utilities/image-write/rpi-imager/rpi-imager2.png){.full-width}

* Pulsad en "Elegir sistema operativo", seleccionad el fichero imagen que queréis flashear y, a continuación, pulsad en "Elegir almacenamiento" para iniciar el proceso.

>Os recomendamos encarecidamente conectar _**sólo el soporte de almacenamiento que queréis flashear**_ para evitar borrar datos de otro soporte por error.
{.is-warning}

![](/tutorials/utilities/image-write/rpi-imager/rpi-imager3.png){.full-width}

* A continuación, pulsad en 'Escribir'. Raspberry Pi Imager se encargará de todo, formatear, preparar particiones, copiar archivos, etc....

![](/tutorials/utilities/image-write/rpi-imager/rpi-imager4.png){.full-width}

* El programa os indica el progreso de la tarea.

![](/tutorials/utilities/image-write/rpi-imager/rpi-imager5.png){.full-width}

* A continuación verificará la imagen creada.

![](/tutorials/utilities/image-write/rpi-imager/rpi-imager6.png){.full-width}

* Y finalmente os informa del resultado de la operación.

![](/tutorials/utilities/image-write/rpi-imager/rpi-imager7.png){.full-width}

Como os indica el programa, podéis expulsar el medio de almacenamiento, insertar la tarjeta microSD recién flasheada en vuestra consola y ¡a jugar!