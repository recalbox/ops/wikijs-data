---
title: CHDMAN
description: 
published: true
date: 2024-10-17T16:49:18.382Z
tags: chd, chdman
editor: markdown
dateCreated: 2023-09-17T07:05:16.167Z
---

## Introducción

Vamos a explicar los diferentes formatos de fichero a adoptar para los CD y otros soportes ópticos.

En otras páginas hemos hablado de un formato, llamado CHD, creado a partir de varias investigaciones realizadas por el proyecto MAME. Este formato  representa estos soportes como una imagen comprimida, sin perder la integridad de los datos, aplicando un algoritmo que es reversible, y cuyo resultado final es scrapable.

El formato CHD (Compressed Hunks of Data) permite representar de forma comprimida las roms de las consolas SEGA CD, PS1, PC Engine....

Por desgracia, el usuario normal tendrá problemas para utilizar el conversor, que es un ejecutable de línea de comandos.

Así que aquí tenéis un fichero Zip, con scripts automatizados, para convertir de BIN+CUE (formato Redump) a CHD, y viceversa.

También hemos añadido un script para el formato GDI, que es un formato de roms de Dreamcast.

Para los juegos de PS1 protegidos por LibCrypt, se necesita poseer los ficheros SBI (Subchannel Information) asociados para poder jugar. Estos ficheros seguirán funcionando con vuestros CHDs.

Si alguna vez los perdéis o hacéis una manipulación incorrecta, estos ficheros SBI se encuentran en el proyecto **Redump**, junto a los ficheros CUE.

## Programa

## {.tabset}
### Windows

Podéis descargar el programa pulsado en **CHDMAN.zip** aquí abajo.

![](/tutorials/utilities/rom-conversion/chdman/archive.svg) [CHDMAN.zip](/tutorials/utilities/rom-conversion/chdman/chdman.zip)

Dentro del zip, encontraréis 6 ficheros:

| Nombre del fichero | Descripción |
| :--- | :--- |
| **chdman.exe** | El programa CHDMAN. |
| **CUE-GDI-ISO to CHD.bat** | Un fichero .bat para convertir vuestras roms de  `CUE` o `GDI` a `CHD`. |
| **Extract CHD to CUE.bat** | Un fichero .bat qui para convertir vuestras roms de `CHD` a `BIN`/`CUE`. |
| **Extract CHD to GDI.bat** | Un fichero .bat para convertir vuestras roms de `CHD` a `GDI`. |
| **README_EN.txt** |  |
| **README_FR.txt** |  |

#### Contenido del fichero Readme

* **CUE-GDI-ISO to CHD**

  Comprime todo tipo de fichero de disco BIN con cabecera CUE o GDI en formato CHD (v5).
  Busca en todas las subcarpetas y crea archivos CHD (v5) en la carpeta en la que se colocan los archivos con CHDMAN.

* **Extract CHD to CUE**

  Descomprime un archivo CHD (V5) en un archivo BIN+CUE. 
  El formato CUE es utilizado por los juegos en CD. En la Raspberry Pi, CHD es compatible con TurboGrafx-CD / PC Engine CD, Sega CD / Mega CD y Dreamcast.

* **Extract CHD to GDI**
  Descomprime un archivo CHD (V5) a GDI. (GDI es un formato de disco para Dreamcast).

### macOS

CHDMAN existe en macOS via Homebrew.

* Instalación de las herramientas Xcode

En primer lugar, hay que ejecutar este comando en el Terminal :

`xcode-select --install`

* Instalación de [**Homebrew**](https://brew.sh/index_fr)

A continuación, hay que ejecutar este comando en el Terminal :

`/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"`

* Si utilizas un Mac con un chip Apple Silicon, también tendrás que ejecutar este comando:

`echo 'eval $(/opt/homebrew/bin/brew shellenv)' >> ~/.zprofile`

* Instalación de [**rom-tools**](https://formulae.brew.sh/formula/rom-tools)

Una vez instalado Homebrew, hay que utilizar el siguiente comando en el Terminal:

`brew install rom-tools`

Como mínimo se require macOS 10.13 High Sierra.

Aquí tenéis nuestro Zip que permite automatizar las conversiones de ficheros al format CHD:

![](/tutorials/utilities/rom-conversion/chdman/archive.svg) [ChdScripts.zip](/tutorials/utilities/rom-conversion/chdman/chdscripts.zip)

Dentro del zip, encontraréis 6 ficheros:

| Nombre del fichero | Descripción |
| :--- | :--- |
| **<span>convertFromChdToCue.sh</span>** | Un fichero .sh para convertir vuestras roms de  `CHD` a `BIN`/`CUE`. |
| **<span>convertFromChdToGdi.sh</span>** | Un fichero .sh para convertir vuestras roms de `CHD` a `GDI`. |
| **<span>convertFromChdToIso.sh</span>** | Un fichero .sh para convertir vuestras roms de `CHD` a `ISO`. |
| **<span>convertFromCueToChd.sh</span>** | Un fichero .sh para convertir vuestras roms de `BIN`/`CUE` a `CHD`. |
| **<span>convertFromGdiToChd.sh</span>** | Un fichero .sh para convertir vuestras roms de `GDI` a `CUE`. |
| **<span>convertFromIsoToChd.sh</span>** | Un fichero .sh para convertir vuestras roms de `ISO` a `CUE`. |
| **README_EN.txt** |  |
| **README_FR.txt** |  |

#### Contenido del fichero Readme

* **<span>convertFromChdToCue.sh</span>**

  Décompresse un fichier CHD (V5) en fichier BIN+CUE.
  El formato CUE lo utilizan los juegos en CD. CHD es compatible con 3DO, Amiga CD32, Amiga CDTV, Dreamcast, Mega CD, Neo-Geo CD, PC Engine CD, PlayStation y Saturn.

* **<span>convertFromChdToGdi.sh</span>**
  Descomprime un archivo CHD (V5) en un archivo GDI.
  El formato GDI lo utilizan los juegos en disco para Dreamcast.

* **<span>convertFromChdToIso.sh</span>**
  Descomprime un archivo CHD (V5) en un archivo ISO.
  El formato ISO lo utilizan los juegos en disco para PlayStation 2.

* **<span>convertFromCueToChd.sh</span>**

  Comprime todo tipo de archivos de disco BIN con cabecera CUE al formato CHD (v5). Busca en todas las subcarpetas y crea archivos CHD (v5) en la carpeta en la que se colocan los archivos con CHDMAN.

* **<span>convertFromGdiToChd.sh</span>**

  Comprime todo tipo de archivos de disco BIN con cabecera GDI al formato CHD (v5). El formato GDI es el utilizado por los juegos en disco para Dreamcast.

* **<span>convertFromIsoToChd.sh</span>**

  Comprime todo tipo de archivos de disco ISO al formato CHD (v5). El formato ISO lo utilizan los juegos en disco para PlayStation 2.

### Linux

Algunas distribuciones de Linux pueden proporcionar este programa pasando por `aptitude` e instalando el paquete `mame-tools`. Otras distribuciones no tienen `mame-tools` y tenéis que compilar chdman:

* Id a https://github.com/libretro/mame
* Descargad los contenidos pulsando en el botón verde `Code` y, en el menú visible, pulsad en `Download ZIP`.

![](/tutorials/utilities/rom-conversion/chdman/mamedownload.png){.full-width}

* Descomprimid el fichero e id al directorio que se ha creado.
* Desde vuestro terminal, utilizad el siguiente comando:

```ssh
make tools
```

* Al final encontraréis el comando `chdman` dentro de la carpeta `build`.

Aquí tenéis nuestro Zip que permite automatizar las conversiones de ficheros al format CHD:

![](/tutorials/utilities/rom-conversion/chdman/archive.svg) [ChdScripts.zip](/tutorials/utilities/rom-conversion/chdman/chdscripts.zip)

Dentro del zip, encontraréis 6 ficheros:

| Nombre del fichero | Descripción |
| :--- | :--- |
| **<span>convertFromChdToCue.sh</span>** | Un fichero .sh para convertir vuestras roms de  `CHD` a `BIN`/`CUE`. |
| **<span>convertFromChdToGdi.sh</span>** | Un fichero .sh para convertir vuestras roms de `CHD` a `GDI`. |
| **<span>convertFromChdToIso.sh</span>** | Un fichero .sh para convertir vuestras roms de `CHD` a `ISO`. |
| **<span>convertFromCueToChd.sh</span>** | Un fichero .sh para convertir vuestras roms de `BIN`/`CUE` a `CHD`. |
| **<span>convertFromGdiToChd.sh</span>** | Un fichero .sh para convertir vuestras roms de `GDI` a `CUE`. |
| **<span>convertFromIsoToChd.sh</span>** | Un fichero .sh para convertir vuestras roms de `ISO` a `CUE`. |
| **README_EN.txt** |  |
| **README_FR.txt** |  |

#### Contenido del fichero Readme

* **<span>convertFromChdToCue.sh</span>**

  Décompresse un fichier CHD (V5) en fichier BIN+CUE.
  El formato CUE lo utilizan los juegos en CD. CHD es compatible con 3DO, Amiga CD32, Amiga CDTV, Dreamcast, Mega CD, Neo-Geo CD, PC Engine CD, PlayStation y Saturn.

* **<span>convertFromChdToGdi.sh</span>**
  Descomprime un archivo CHD (V5) en un archivo GDI.
  El formato GDI lo utilizan los juegos en disco para Dreamcast.

* **<span>convertFromChdToIso.sh</span>**
  Descomprime un archivo CHD (V5) en un archivo ISO.
  El formato ISO lo utilizan los juegos en disco para PlayStation 2.

* **<span>convertFromCueToChd.sh</span>**

  Comprime todo tipo de archivos de disco BIN con cabecera CUE al formato CHD (v5). Busca en todas las subcarpetas y crea archivos CHD (v5) en la carpeta en la que se colocan los archivos con CHDMAN.

* **<span>convertFromGdiToChd.sh</span>**

  Comprime todo tipo de archivos de disco BIN con cabecera GDI al formato CHD (v5). El formato GDI es el utilizado por los juegos en disco para Dreamcast.

* **<span>convertFromIsoToChd.sh</span>**

  Comprime todo tipo de archivos de disco ISO al formato CHD (v5). El formato ISO lo utilizan los juegos en disco para PlayStation 2.

## Utilización automatizada

## {.tabset}

### Windows
#### Convertir vuestro juego de `BIN`/`CUE` o `GDI` a `CHD`

* Colocad "**chdman.exe**" y "**CUE o GDI to CHD.bat**" en la carpeta que contiene su juego como se muestra a continuación.
  Ejemplo para el juego "Grandia (Francia)":

![](/tutorials/utilities/rom-conversion/chdman/chdman-win1.png){.full-width}

* Haced click en "**CUE or GDI to CHD.bat**" para lanzar la conversión.

![](/tutorials/utilities/rom-conversion/chdman/chdman-win2.png){.full-width}

* Cuando se cierra esta ventana, ha terminado la conversión.

![](/tutorials/utilities/rom-conversion/chdman/chdman-win3.png){.full-width}

* Podéis borrar "**chdman.exe**" y "**CUE or GDI to CHD.bat**", vuestra rom está lista.

![](/tutorials/utilities/rom-conversion/chdman/chdman-win4.png){.full-width}

>También podéis ejecutar el fichero .bat si queréis convertir varios juegos a la vez.
{.is-info}

#### Convertir vuestro juego de `CHD` a `BIN`/`CUE`

* Colocad "**chdman.exe**" y "**Extract CHD to CUE.bat**" en la carpeta que contiene el juego como se muestra a continuación.
  Ejemplo para el juego "Grandia (Francia)":



* Haced click en "**Extract CHD to CUE.bat**" para lanzar la conversión.



* Cuando se cierra esta ventana, ha terminado la conversión.



* Podéis borrar "**chdman.exe**" y "**Extract CHD to CUE.bat**", vuestra rom está lista.



>También podéis ejecutar el fichero .bat si queréis convertir varios juegos a la vez.
{.is-info}

#### Convertir vuestro juego de `CHD` a `GDI`

* Colocad "**chdman.exe**" y "**Extract CHD to GDI.bat**" en la carpeta que contiene el juego como se muestra a continuación.
  Ejemplo para el juego "Grandia (Francia)":



* Haced click en "**Extract CHD to GDI.bat**" para lanzar la conversión.



* Cuando se cierra esta ventana, ha terminado la conversión.



* Podéis borrar "**chdman.exe**" y "**Extract CHD to GDI.bat**", vuestra rom está lista.



>También podéis ejecutar el fichero .bat si queréis convertir varios juegos a la vez.
{.is-info}

### macOS

#### Convertir vuestro juego de `BIN`/`CUE` a `CHD`

* Coloca "**convertFromCueToChd.sh**" en la carpeta que contiene el juego como se muestra a continuación.
  Ejemplo para el juego "Grandia (Francia)":


* Regresad al directorio padre, haced clic derecho sobre el mismo y seleccionad `Servicios` > `Nuevo terminal en el directorio`. Una vez abierto, ejecutad el fichero `./convertFromCueToChd.sh` para lanzar la conversión.


* Cuando se cierra esta ventana, ha terminado la conversión.


* Podéis borrar el fichero "**convertFromCueToChd.sh**", la rom está lista.

#### Convertir vuestro juego de `GDI` a `CHD`

* Colocad "**convertFromGdiToChd.sh**" en la carpeta que contiene el juego como se muestra a continuación.
  Ejemplo para el juego "Resident Evil - Code - Veronica (France)" :


* Regresad al directorio padre, haced clic derecho sobre el mismo y seleccionad `Servicios` > `Nuevo terminal en el directorio`. Una vez abierto, ejecutad el fichero `./convertFromGdiToChd.sh` para lanzar la conversión.


* Cuando se cierra esta ventana, ha terminado la conversión.


* Podéis borrar "**convertFromGdiToChd.sh**", la rom está lista.

#### Convertir vuestro juego de `ISO` a `CHD`

* Colocad "**<span>convertFromIsoToChd.sh</span>**" en la carpeta que contiene el juego como se muestra a continuación.
  Ejemplo para el juego "Shadow of the Colossus (Europe, Australia) (En,Fr,De,Es,It)" :



* Regresad al directorio padre, haced clic derecho sobre el mismo y seleccionad `Servicios` > `Nuevo terminal en el directorio`. Una vez abierto, ejecutad el fichero `./convertFromIsoToChd.sh` para lanzar la conversión.



* Cuando se cierra esta ventana, ha terminado la conversión.



* Podéis borrar "**convertFromIsoToChd.sh**", la rom está lista.

#### Convertir vuestro juego de `CHD` a `BIN`/`CUE`

* Colocad "**convertFromChdToCue.sh**" en la carpeta que contiene el juego como se muestra a continuación.
  Ejemplo para el juego "Grandia (France)" :


* Regresad al directorio padre, haced clic derecho sobre el mismo y seleccionad `Servicios` > `Nuevo terminal en el directorio`. Una vez abierto, ejecutad el fichero `./convertFromChdToCue.sh` para lanzar la conversión.


* Cuando se cierra esta ventana, ha terminado la conversión.


* Podeís borrar "**convertFromChdToCue.sh**", la rom está lista.

#### Convertir vuestro juego de `CHD` a `GDI`

* Colocad "**<span>convertFromChdToGdi.sh</span>**" en la carpeta que contiene el juego como se muestra a continuación.
  Ejemplo para el juego "Resident Evil - Code - Veronica (France)" :


* Regresad al directorio padre, haced clic derecho sobre el mismo y seleccionad `Servicios` > `Nuevo terminal en el directorio`. Una vez abierto, ejecutad el fichero `./convertFromChdToGdi.sh` para lanzar la conversión.


* Cuando se cierra esta ventana, ha terminado la conversión.


* Podéis borrar "**convertFromChdToGdi.sh**", la rom está lista.

#### Convertir vuestro juego de `CHD` a `ISO`

* Colocad "**<span>convertFromChdToIso.sh</span>**"en la carpeta que contiene el juego como se muestra a continuación.
  Ejemplo para el juego "Shadow of the Colossus (Europe, Australia) (En,Fr,De,Es,It)" :



* Regresad al directorio padre, haced clic derecho sobre el mismo y seleccionad `Servicios` > `Nuevo terminal en el directorio`. Una vez abierto, ejecutad el fichero `./convertFromChdToIso.sh` para lanzar la conversión.



* Cuando se cierra esta ventana, ha terminado la conversión.



* Podéis borrar "**convertFromChdToIso.sh**", la rom está lista.

### Linux

#### Convertir vuestro juego de `BIN`/`CUE` a `CHD`

* Coloca "**convertFromCueToChd.sh**" en la carpeta que contiene el juego como se muestra a continuación.
  Ejemplo para el juego "Grandia (Francia)":

![](/tutorials/utilities/rom-conversion/chdman/chdman-unix1.png){.full-width}

* En el Terminal, situaros dentro de la carpeta donde se encuentra "**<span>convertFromCueToChd.sh</span>**" (sólo vosotros podéis saberlo) y ejecutad el fichero `./convertFromCueToChd.sh` para lanzar la conversión.

![](/tutorials/utilities/rom-conversion/chdman/chdman-unix2.png){.full-width}

* Cuando se cierra esta ventana, ha terminado la conversión.

![](/tutorials/utilities/rom-conversion/chdman/chdman-unix3.png){.full-width}

* Podéis borrar el fichero "**convertFromCueToChd.sh**", la rom está lista.

#### Convertir vuestro juego de `GDI` a `CHD`

* Colocad "**convertFromGdiToChd.sh**" en la carpeta que contiene el juego como se muestra a continuación.
  Ejemplo para el juego "Resident Evil - Code - Veronica (France)" :

![](/tutorials/utilities/rom-conversion/chdman/chdman-unix6.png){.full-width}

* En el Terminal, situaros dentro de la carpeta donde se encuentra "**<span>convertFromGdiToChd.sh</span>**" (sólo vosotros podéis saberlo) y ejecutad `./convertFromGdiToChd.sh` para lanzar la conversión.

![](/tutorials/utilities/rom-conversion/chdman/chdman-unix7.png){.full-width}

* Cuando se cierra esta ventana, ha terminado la conversión.

![](/tutorials/utilities/rom-conversion/chdman/chdman-unix8.png){.full-width}

* Podéis borrar "**convertFromGdiToChd.sh**", la rom está lista.

#### Convertir vuestro juego de `ISO` a `CHD`

* Colocad "**<span>convertFromIsoToChd.sh</span>**" en la carpeta que contiene el juego como se muestra a continuación.
  Ejemplo para el juego "Shadow of the Colossus (Europe, Australia) (En,Fr,De,Es,It)" :



* En el Terminal, situaros dentro de la carpeta donde se encuentra "**<span>convertFromIsoToChd.sh</span>**" (sólo vosotros podéis saberlo) y ejecutad el fichero `./convertFromIsoToChd.sh` para lanzar la conversión



* Cuando se cierra esta ventana, ha terminado la conversión.



* Podéis borrar "**convertFromIsoToChd.sh**", la rom está lista.

#### Convertir vuestro juego de `CHD` a `BIN`/`CUE`

* Colocad "**convertFromChdToCue.sh**" en la carpeta que contiene el juego como se muestra a continuación.
  Ejemplo para el juego "Grandia (France)" :

![](/tutorials/utilities/rom-conversion/chdman/chdman-unix4.png){.full-width}

* En el Terminal, situaros dentro de la carpeta donde se encuentra "**<span>convertFromChdToCue.sh</span>**" (sólo vosotros podéis saberlo) y ejecutad el fichero `./convertFromChdToCue.sh` para lanzar la conversión.

![](/tutorials/utilities/rom-conversion/chdman/chdman-unix5.png){.full-width}

* Cuando se cierra esta ventana, ha terminado la conversión.

![](/tutorials/utilities/rom-conversion/chdman/chdman-unix3.png){.full-width}

* Podeís borrar "**convertFromChdToCue.sh**", la rom está lista.

#### Convertir vuestro juego de `CHD` a `GDI`

* Colocad "**<span>convertFromChdToGdi.sh</span>**" en la carpeta que contiene el juego como se muestra a continuación.
  Ejemplo para el juego "Resident Evil - Code - Veronica (France)" :

![](/tutorials/utilities/rom-conversion/chdman/chdman-unix9.png){.full-width}

* En el Terminal, situaros dentro de la carpeta donde se encuentra "**<span>convertFromChdToGdi.sh</span>**" (sólo vosotros podéis saberlo) y ejecutad el fichero `./convertFromChdToGdi.sh` para lanzar la conversión

![](/tutorials/utilities/rom-conversion/chdman/chdman-unix10.png){.full-width}

* Cuando se cierra esta ventana, ha terminado la conversión.

![](/tutorials/utilities/rom-conversion/chdman/chdman-unix11.png){.full-width}

* Podéis borrar "**convertFromChdToGdi.sh**", la rom está lista.

#### Convertir vuestro juego de `CHD` a `ISO`

* Colocad "**<span>convertFromChdToIso.sh</span>**"en la carpeta que contiene el juego como se muestra a continuación.
  Ejemplo para el juego "Shadow of the Colossus (Europe, Australia) (En,Fr,De,Es,It)" :



* En el Terminal, situaros dentro de la carpeta donde se encuentra "**<span>convertFromChdToIso.sh</span>**" (sólo vosotros podéis saberlo) y ejecutad el fichero `./convertFromChdToIso.sh` para lanzar la conversión.



* Cuando se cierra esta ventana, ha terminado la conversión.



* Podéis borrar "**convertFromChdToIso.sh**", la rom está lista.

## Creación de ficheros `.M3U`

Cuando se trata de un juego con varios discos, una vez creados los CHD para cada disco, hay que crear un fichero `.M3U` para declararlos.

El fichero con formato `.M3U` no es más que un fichero de texto que contiene una lista de los diferentes CD que componen el juego, en el buen orden, lo que permite al emulador cambiar de un CD a otro simplemente utilizando la combinación de disk-swapping (`Hotkey` + `STICK G.` a la _IZQUIERDA_ o a la _DERECHA_).

Ejemplo para el juego "Grandia (France)" :

* Cread un fichero `M3U` usando Notepad++ llamado « Grandia (France).m3u ».
* Dentro del fichero, rellenad el nombre de los diferentes ficheros `CHD` que lo componen:

```text
Grandia (France) (Disc 1).chd
Grandia (France) (Disc 2).chd
```

* En Windows, hay que activar la vista de extensiones para poder crear este fichero:
  * Id al botón "Ver" en la parte superior de la ventana del Explorador de Windows.
  * Activad "Extensiones de nombre de archivo" en la parte superior derecha.

![](/tutorials/utilities/rom-conversion/chdman/chdman-m3u1.png){.full-width}

* Confirmad la modificación.

![](/tutorials/utilities/rom-conversion/chdman/chdman-m3u2.png){.full-width}