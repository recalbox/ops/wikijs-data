---
title: Rotary encoder via GPIO (digital volume control)
description: 
published: true
date: 2021-08-08T13:49:04.755Z
tags: gpio, rotary, encoder, digital, volume, control
editor: markdown
dateCreated: 2021-08-06T14:27:13.469Z
---

This short guide and python script is adapted from [Savetheclocktower's project](https://gist.github.com/savetheclocktower/9b5f67c20f6c04e65ed88f2e594d43c1) found on GitHub.

## Purpose

This script is for those who want a physical volume knob on a Recalbox project, like arcade machines. This script is useful for arcade machines with speakers that don't have their own physical volume knob, or would have difficulty moving the speaker control knob near the user.

This script uses a standard 5-pin rotary encoder and has been tested on the Adafruit encoder. Five wires are needed for this rotary encoder: three for the button part (A, B and ground), and two for the key part (common and ground). Here is a reference for the Raspberry Pi GPIO pins.

| Description | BCM # | Board # |
| :--- | :--- | :--- |
| button A | GPIO 26 | 37 |
| button B | GPIO 19 | 35 |
| ground button | ground pin below GPIO 26 | 39 |
| common button | GPIO 13 | 33 |
| ground button | ground pin opposite GPIO 13 | 34 |

You can use any pins you want; just update the `volume-monitor.sh` script if you change them. If you don't have a push button in your rotary encoder, leave the pins unoccupied. Any ground pin can be used, these pins are just suggested because of their proximity.

### Volume daemon

The script below works as follows: it uses the specified pins, and when the knob is turned one way or the other, it uses the states of pins A and B to know whether the knob has been turned left or right. Thus, it knows whether to raise or lower the system volume in response, which it does with the **Amixer** command line program.

1. To install the script in your Recalbox: you need to remount the [read-write](./../../tutorials/system/access/remount-partition-with-write-access) partition via SSH.
2. Create or modify your `volume-monitor.py` script in `/recalbox/scripts` via nano:

```shell
nano /recalbox/scripts/volume-monitor.py
```

3. Copy and paste the bottom script into the file, then save the file with the command `Ctrl` + `X`: 


```python
#!/usr/bin/env python2

"""
The daemon responsible for changing the volume in response to a turn or press
of the volume knob.
The volume knob is a rotary encoder. It turns infinitely in either direction.
Turning it to the right will increase the volume; turning it to the left will
decrease the volume. The knob can also be pressed like a button in order to
turn muting on or off.
The knob uses two GPIO pins and we need some extra logic to decode it. The
button we can just treat like an ordinary button. Rather than poll
constantly, we use threads and interrupts to listen on all three pins in one
script.
"""

import os
import signal
import subprocess
import sys
import threading

from RPi import GPIO
from multiprocessing import Queue

DEBUG = False

# SETTINGS
# ========

# The two pins that the encoder uses (BCM numbering).
GPIO_A = 26   
GPIO_B = 19

# The pin that the knob's button is hooked up to. If you have no button, set
# this to None.
GPIO_BUTTON = 13 

# The minimum and maximum volumes, as percentages.
#
# The default max is less than 100 to prevent distortion. The default min is
# greater than zero because if your system is like mine, sound gets
# completely inaudible _long_ before 0%. If you've got a hardware amp or
# serious speakers or something, your results will vary.
VOLUME_MIN = 60
VOLUME_MAX = 96

# The amount you want one click of the knob to increase or decrease the
# volume. I don't think that non-integer values work here, but you're welcome
# to try.
VOLUME_INCREMENT = 1

# (END SETTINGS)
# 


# When the knob is turned, the callback happens in a separate thread. If
# those turn callbacks fire erratically or out of order, we'll get confused
# about which direction the knob is being turned, so we'll use a queue to
# enforce FIFO. The callback will push onto a queue, and all the actual
# volume-changing will happen in the main thread.
QUEUE = Queue()

# When we put something in the queue, we'll use an event to signal to the
# main thread that there's something in there. Then the main thread will
# process the queue and reset the event. If the knob is turned very quickly,
# this event loop will fall behind, but that's OK because it consumes the
# queue completely each time through the loop, so it's guaranteed to catch up.
EVENT = threading.Event()

def debug(str):
  if not DEBUG:
    return
  print(str)

class RotaryEncoder:
  """
  A class to decode mechanical rotary encoder pulses.
  Ported to RPi.GPIO from the pigpio sample here: 
  http://abyz.co.uk/rpi/pigpio/examples.html
  """

  def __init__(self, gpioA, gpioB, callback=None, buttonPin=None, buttonCallback=None):
    """
    Instantiate the class. Takes three arguments: the two pin numbers to
    which the rotary encoder is connected, plus a callback to run when the
    switch is turned.

    The callback receives one argument: a `delta` that will be either 1 or -1.
    One of them means that the dial is being turned to the right; the other
    means that the dial is being turned to the left. I'll be damned if I know
    yet which one is which.
    """

    self.lastGpio = None
    self.gpioA    = gpioA
    self.gpioB    = gpioB
    self.callback = callback

    self.gpioButton     = buttonPin
    self.buttonCallback = buttonCallback

    self.levA = 0
    self.levB = 0

    GPIO.setmode(GPIO.BCM)
    GPIO.setup(self.gpioA, GPIO.IN, pull_up_down=GPIO.PUD_UP)
    GPIO.setup(self.gpioB, GPIO.IN, pull_up_down=GPIO.PUD_UP)

    GPIO.add_event_detect(self.gpioA, GPIO.BOTH, self._callback)
    GPIO.add_event_detect(self.gpioB, GPIO.BOTH, self._callback)

    if self.gpioButton:
      GPIO.setup(self.gpioButton, GPIO.IN, pull_up_down=GPIO.PUD_UP)
      GPIO.add_event_detect(self.gpioButton, GPIO.FALLING, self._buttonCallback, bouncetime=500)


  def destroy(self):
    GPIO.remove_event_detect(self.gpioA)
    GPIO.remove_event_detect(self.gpioB)
    GPIO.cleanup()

  def _buttonCallback(self, channel):
    self.buttonCallback(GPIO.input(channel))

  def _callback(self, channel):
    level = GPIO.input(channel)
    if channel == self.gpioA:
      self.levA = level
    else:
      self.levB = level

    # Debounce.
    if channel == self.lastGpio:
      return

    # When both inputs are at 1, we'll fire a callback. If A was the most
    # recent pin set high, it'll be forward, and if B was the most recent pin
    # set high, it'll be reverse.
    self.lastGpio = channel
    if channel == self.gpioA and level == 1:
      if self.levB == 1:
        self.callback(1)
    elif channel == self.gpioB and level == 1:
      if self.levA == 1:
        self.callback(-1)

class VolumeError(Exception):
  pass

class Volume:
  """
  A wrapper API for interacting with the volume settings on the RPi.
  """
  MIN = VOLUME_MIN
  MAX = VOLUME_MAX
  INCREMENT = VOLUME_INCREMENT

  def __init__(self):
    # Set an initial value for last_volume in case we're muted when we start.
    self.last_volume = self.MIN
    self._sync()

  def up(self):
    """
    Increases the volume by one increment.
    """
    return self.change(self.INCREMENT)

  def down(self):
    """
    Decreases the volume by one increment.
    """
    return self.change(-self.INCREMENT)

  def change(self, delta):
    v = self.volume + delta
    v = self._constrain(v)
    return self.set_volume(v)

  def set_volume(self, v):
    """
    Sets volume to a specific value.
    """
    self.volume = self._constrain(v)
    output = self.amixer("set 'PCM' unmute {}%".format(v))
    self._sync(output)
    return self.volume

  def toggle(self):
    """
    Toggles muting between on and off.
    """
    if self.is_muted:
      output = self.amixer("set 'PCM' unmute")
    else:
      # We're about to mute ourselves, so we should remember the last volume
      # value we had because we'll want to restore it later.
      self.last_volume = self.volume
      output = self.amixer("set 'PCM' mute")

    self._sync(output)
    if not self.is_muted:
      # If we just unmuted ourselves, we should restore whatever volume we
      # had previously.
      self.set_volume(self.last_volume)
    return self.is_muted

  def status(self):
    if self.is_muted:
      return "{}% (muted)".format(self.volume)
    return "{}%".format(self.volume)

  # Read the output of `amixer` to get the system volume and mute state.
  #
  # This is designed not to do much work because it'll get called with every
  # click of the knob in either direction, which is why we're doing simple
  # string scanning and not regular expressions.
  def _sync(self, output=None):
    if output is None:
      output = self.amixer("get 'PCM'")

    lines = output.readlines()
    if DEBUG:
      strings = [line.decode('utf8') for line in lines]
      debug("OUTPUT:")
      debug("".join(strings))
    last = lines[-1].decode('utf-8')

    # The last line of output will have two values in square brackets. The
    # first will be the volume (e.g., "[95%]") and the second will be the
    # mute state ("[off]" or "[on]").
    i1 = last.rindex('[') + 1
    i2 = last.rindex(']')

    self.is_muted = last[i1:i2] == 'off'

    i1 = last.index('[') + 1
    i2 = last.index('%')
    # In between these two will be the percentage value.
    pct = last[i1:i2]

    self.volume = int(pct)

  # Ensures the volume value is between our minimum and maximum.
  def _constrain(self, v):
    if v < self.MIN:
      return self.MIN
    if v > self.MAX:
      return self.MAX
    return v

  def amixer(self, cmd):
    p = subprocess.Popen("amixer {}".format(cmd), shell=True, stdout=subprocess.PIPE)
    code = p.wait()
    if code != 0:
      raise VolumeError("Unknown error")
      sys.exit(0)

    return p.stdout

if __name__ == "__main__":

  gpioA = GPIO_A
  gpioB = GPIO_B
  gpioButton = GPIO_BUTTON

  v = Volume()

  def on_press(value):
    v.toggle()
    print("Toggled mute to: {}".format(v.is_muted))
    EVENT.set()

  # This callback runs in the background thread. All it does is put turn
  # events into a queue and flag the main thread to process them. The
  # queueing ensures that we won't miss anything if the knob is turned
  # extremely quickly.
  def on_turn(delta):
    QUEUE.put(delta)
    EVENT.set()

  def consume_queue():
    while not QUEUE.empty():
      delta = QUEUE.get()
      handle_delta(delta)

  def handle_delta(delta):
    if v.is_muted:
      debug("Unmuting")
      v.toggle()
    if delta == 1:
      vol = v.up()
    else:
      vol = v.down()
    print("Set volume to: {}".format(vol))

  def on_exit(a, b):
    print("Exiting...")
    encoder.destroy()
    sys.exit(0)

  debug("Volume knob using pins {} and {}".format(gpioA, gpioB))

  if gpioButton != None:
    debug("Volume button using pin {}".format(gpioButton))

  debug("Initial volume: {}".format(v.volume))

  encoder = RotaryEncoder(GPIO_A, GPIO_B, callback=on_turn, buttonPin=GPIO_BUTTON, buttonCallback=on_press)
  signal.signal(signal.SIGINT, on_exit)

  while True:
    # This is the best way I could come up with to ensure that this script
    # runs indefinitely without wasting CPU by polling. The main thread will
    # block quietly while waiting for the event to get flagged. When the knob
    # is turned we're able to respond immediately, but when it's not being
    # turned we're not looping at all.
    # 
    # The 1200-second (20 minute) timeout is a hack; for some reason, if I
    # don't specify a timeout, I'm unable to get the SIGINT handler above to
    # work properly. But if there is a timeout set, even if it's a very long
    # timeout, then Ctrl-C works as intended. No idea why.
    EVENT.wait(1200)
    consume_queue()
EVENT.clear()
```

4. Mark the script as an executable file:

```shell
chmod +x /recalbox/scripts/volume-monitor.py
```

5. To get the volume control script to start with your system, do the following:

```shell
touch ~/custom.sh && chmod u+x ~/custom.sh
```

6. Open the custom.sh script in the nano editor:

```shell
nano ~/custom.sh
```

7. Finally, copy and paste the following into the custom.sh file and save with Ctrl+X :

```shell
python /recalbox/scripts/volume-monitor.py
```

8. Restart your Recalbox using the restart command:

```shell
reboot
```

9. Take advantage of the new volume control of your hardware.

### Script changes

These changes are all under the "Settings" heading of the `Volume-monitor.py` script.

>Please wait 3 seconds after the Emulation menu appears BEFORE touching the rotary encoder. Turning the encoder sooner may block the script and make the encoder unresponsive.
{.is-info}

If you want to change the two default pins used by the encoder, change the following lines in the script. Be sure to use the BCM numbering codes.

![](/tutorials/audio/gpio-rotative-coder.png)

`GPIO_A = 26`

`GPIO_B = 19`

If you want to change the pin to which the push button is attached, then change the corresponding line in the script below. Be sure to use the BCM numbering codes. If you don't have a button, set this parameter to None.

`GPIO_BUTTON = 13`

If you want to change the range (i.e.: min & max) that the script modulates the volume of the Raspberry Pi, then edit the corresponding line in the script below. The numbers are expressed as a percentage. The default max is less than 100 to avoid distortion. The default min is greater than zero because if your system is like mine, the sound becomes completely inaudible well before 0%. If you have a hardware amplifier or quality speakers or whatever, your results will vary.

`VOLUME_MIN = 60`.

`VOLUME_MAX = 96`

If you want the volume of your system to change faster and be more sensitive, change the corresponding line in the script below. The default setting is 1, change to 2 to double the rate of volume change.

`VOLUME_INCREMENT = 1`

### How to uninstall the script

Repeat steps 7 and 8 above, but remove the line added in step 8 or comment it out by adding a hash, for example the line reads as follows:

```shell
#python /recalbox/scripts/volume-monitor.py
```

## Acknowledgements

* **Substring** for his help in making this guide and the modification of Recalbox possible.  
* **All the other Recalbox developers** for making this wonderful project available.  
* **savetheclocktower** for the original project code and for his help in converting to Python2.