---
title: Dongles Wifi
description: 
published: true
date: 2025-03-11T10:07:29.686Z
tags: wifi, dongles, compatibilité
editor: markdown
dateCreated: 2021-05-21T15:39:28.559Z
---


| Périphérique | État | Chipset | Dernière version testée | Commentaires | Boutique |
| :---: | :---: | :---: | :---: | :---: | :---: |
| Belkin N300 F7D2102 | ✅ |  |  |  |  |
| CSL 300 Mbit/s WLAN Stick avec antenne | ❌ | RT 8191SU |  |  |  |
| Canakit Wifi Adapter | ✅ | RT5370 |  |  |  |
| Cudy WU650S AC | ✅  | RTL8821CU | 7.2.1, 8.0.1 |  |  |
| D-link dwl 123 | ❌ | RT5370 |  |  |  |
| D-link DWA 131 | ❌ |  |  |  |  |
| Edimax EW-7811UN | ✅ | RTL8188CUS | v3.3beta17 & v4.0.0-dev | Fonctionne mais crée des instabilités avec d'autres dongles ou périphériques USB. |  |
| Edimax EW-7811UTC - AC600 | ❌ | RT8812au |  |  |  |
| EDUP EP-N8508 USB Nano WiFi 150Mbps | ✅ |  |  |  |  |
| Hercules HWNUm-300 | ❌ |  |  |  |  |
| Inter-Tech DMG-04 | ✅ | RTL8811CU | 9.1 |  | [Amazon](https://www.amazon.fr/gp/product/B097CKTBLJ) |
| Lindy 52213 Dongle USB 3.0 WiFi Bluetooth | ✅ |  |  |  |  |
| Netgear WIFI USB N300 Nano WNA3100M | ✅ |  |  |  |  |
| Netgear WIFI USB AC1200 Nano Dual Band (A6150-100PES) | ✅ | Realtek RTL8812BU | 8.0 (x86_64) |  |  |
| Netis WF2122 USB WiFi 11n 300M 2 antenas 5dB | ✅ |  | v4.1 |  |  |
| Ourlink | ✅ | RTl8192cu |  |  |  |
| Ralink Wi-fi 802.11n with Antenna | ✅ | RT5370 |  |  |  |
| Tenda W311Ma Wireless N150 Mini High Gain USB Adapter | ✅ |  |  |  |  |
| TP-LINK TL-WN822N 300Mbps High Gain Wireless USB Adapter | ✅ |  | v4.0.0-dev |  |  |
| TP-LINK TL-WN823N 300Mbps Wireless Mini USB Adapter | ❌ |  | v4.0.2 |  |  |
| TP-Link TL-WN722N High Gain Wireless USB Adapter HW v1.0 | ✅ |  | v4.0.0 - rpi2B |  |  |
| TP-Link TL-WN722N High Gain Wireless USB Adapter HW v2.0 | ❌ |  | v4.1 - rpi3 |  |  |
| TP-Link TL-WN725N Nano WiFi N 150 Mbps | ❌ |  | v4.1 - rpi2B | Fonctionne correctement jusqu'à la version 7.1 mais crée des instabilités avec d'autres dongles ou périphériques USB. / Ne fonctionne plus depuis la version 8.0.1. |  |
| Wi-key 150 | ✅ |  |  |  |  |
| Wi-Pi Wifi Dongle | ✅ |  |  |  |  |
| Generic RTL8188EUS | ❌ | RTL8188EUS | 4.1 |  |  |
| Netis WF2116 Wireless N300 Long-Range USB Adapter, Supports Windows, Mac OS, Linux, 5dBi High Gain Antennas, Free USB Cradle | ✅ | rtl8192cu | 4.1 |  | [Amazon](https://www.amazon.com/gp/product/B006THNH7Q) |
| Ralink RT5370 Raspberry PI wifi adapter/ wifi dongle with soft AP function | ✅ | RT5370 | 4.1 |  | [Amazon](https://www.amazon.com/gp/product/B019XUDHFC) |
| OURLiNK 600Mbps AC600 Dual Band USB WiFi Dongle | ❌ | ????? | 4.1 |  | [Amazon](https://www.amazon.com/gp/product/B011T5IF06) |
