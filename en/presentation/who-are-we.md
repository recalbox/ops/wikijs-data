---
title: Who are we?
description: 
published: true
date: 2021-08-08T18:36:39.976Z
tags: project, who
editor: markdown
dateCreated: 2021-06-28T17:52:49.020Z
---

## Presentation

**Recalbox** is an open-source **retrogaming console** that allows you to play a variety of consoles and platforms in your living room, with ease!

**Recalbox** is the **operating system** of the Recalbox project, a **ready-to-use** emulation console based on the **Raspberry Pi** that is entirely **free and open source**, and has been designed specifically so that **you can create your own in no time**.

Recalbox offers you **a wide selection of consoles and systems**, from the first arcade terminals, to platforms such as the Playstation, the Nes and the Megadrive.

**Recalbox** uses several existing elements, such as [EmulationStation](https://github.com/Aloshi/EmulationStation) as an **interface**, [piFBA](https://github.com/digitalLumberjack/pifba) and [RetroArch](https://github.com/libretro/RetroArch) as **emulators**.

**Recalbox** can be described in one word: **EASY**.
We want to provide a system that is as user-friendly and intuitive as possible.  
No need to modify a multitude of configuration files or connect via SSH.
Just install the system and play.
The system configuration is done via a single file, named `recalbox.conf`.   
This file allows a fine configuration of all emulators!

But for **advanced users** wanting to have a **more advanced configuration**, we gave them the possibility to make a system to their liking with **overrides** (modify only if you are sure of what you are doing).

### Dozens of open-source software...... combined in one system.

**Recalbox** is based on the **GNU/Linux** system with Buildroot.  
It integrates hundreds of open-source software, from the smallest utility, to its emulators and its EmulationStation frontend.

Find the dozens of RetroArch/Libretro emulators, the famous Kodi media player, and the custom EmulationStation frontend!

![](/presentation/opensource.png)

### With homebrews and hacks, discover games never released on console!

Homebrews are games created from scratch by developers. Many of them are very good surprises, and will make you have a great time.

Hacks are modifications of existing games. They take all or part of the graphics and gameplay of a game, and differ on the story, difficulty, atmosphere.
Some are considered as full-fledged sequels of the original games, like Super Mario Bros 4 (hack of Super Mario World) or Legend Of Zelda - Parallels Worlds (hack of A Link To The Past).

![](/presentation/zeldaparallelsworlds.png)

### Media Center

Powered by **Kodi**, Recalbox is also a **Media Center**.  
By connecting it to the home network, you will be able to play videos from all your compatible devices (internet box, computer).

### Features

* System :
  * [Supported systems](./../hardware-compatibility/emulators-compatibility)
  * **Optimized FBA version with 4 player support**
  * This means that the base file system is only 150MB compressed.
  * **NOOBS-based rescue system:** Possibility to reinstall the system directly from the SD card, or by getting the latest version from the Internet.
  * **Supported system languages:** French, English, Spanish, German, Italian, Portuguese and maybe others to come if you participate.
  * **Interface:** Based on EmulationStation developed by Aloshi.
  * **Background music on the interface.**
  * **Favourites management.**
* Network:
  * **Access via network:** to roms directories, screenshots and game saves.
  * **Wifi and RJ45 support.**
  * **Online updates.**
* Controllers:
  * **Configuration of your game controller directly from the interface:** Once the configuration is done, it will be compatible with all emulators.
  * **Built-in support for PS3, Xbox360, 8BitDo and Bluetooth controllers:** Attach a controller and play!
  * **Presence of GPIO drivers:** To use arcade controls or your original Nes, Snes, Megadrive, PSX controllers.
  * **Support for Xin-Mo 2 player controllers.**
  * **Virtual Gamepad Support** [Miroof's Virtual Gamepad](https://github.com/miroof/node-virtual-gamepads): Use your phone as a controller.
* Feature:
  * **Kodi Media Center**
  * **Moonlight:** stream games over your local network or the Internet (requires Nvidia graphics card).
  * **Netplay:** Online games.
  * **Retroarch AI**: Translate text in games on the fly.
  * **Internal and external spraper:** insert a picture, a video and a description for each game.
  * **Snaps videos:** Video presentation of the game in the scrapes.

## Acknowledgements

Recalbox **would not exist** without the **open-source** emulators it uses.  
We would like to **thank all the people** who contribute to provide **open-source emulators,** thanks to which we had the chance to play, again, to **all these video game classics** 🤩

### Here is the list of software used in Recalbox

* Buildroot: [http://buildroot.uclibc.org/](http://buildroot.uclibc.org/)
* EmulationStation: [http://emulationstation.org/](http://emulationstation.org/)
* EmulationStation UI Design & original "simple" stock theme: Nils Bonenberger - [http://blog.nilsbyte.de/](http://blog.nilsbyte.de/)

### Emulators

* RetroArch and Libretro kernels: [http://www.libretro.com/](http://www.libretro.com/)
* PiFBA: [https://code.google.com/p/pifba/](https://code.google.com/p/pifba/)
* Mupen64: [https://github.com/mupen64plus/](https://github.com/mupen64plus/)
* Dolphin-emu: [https://fr.dolphin-emu.org/](https://fr.dolphin-emu.org/)
* PPSSPP: [https://www.ppsspp.org/](https://www.ppsspp.org/)

### Others

* Kodi media player: [http://kodi.tv/](http://kodi.tv/)
* Qtsixa: [http://qtsixa.sourceforge.net/](http://qtsixa.sourceforge.net/)