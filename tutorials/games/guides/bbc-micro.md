---
title: BBC Micro
description: 
published: true
date: 2021-11-15T09:49:53.451Z
tags: bbc micro, 8.0+
editor: markdown
dateCreated: 2021-10-16T11:03:37.813Z
---

If you have any doubts about using the BBC Micro system, you can find some useful information here.

Here are the tutorials available:

[FAQ](faq)