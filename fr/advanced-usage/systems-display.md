---
title: Affichage des systèmes
description: Fonctionnement et modifications de l'affichage des systèmes.
published: true
date: 2023-03-05T11:23:01.082Z
tags: emulationstation, affichage, systèmes
editor: markdown
dateCreated: 2021-05-21T07:49:42.597Z
---

## Fonctionnement de l'affichage

L’affichage des systèmes dans le menu de Recalbox est géré par le fichier `systemlist.xml`. Ce fichier se situe dans le dossier `/recalbox/share_init/system/.emulationstation/systemlist.xml`.

Il contient les noms des différents systèmes qui sont supportés par votre version de Recalbox. Il est construit de la manière suivante :

```xml
<?xml version="1.0"?>
<systemList>
  <defaults command="python /usr/bin/emulatorlauncher.pyc %CONTROLLERSCONFIG% -system %SYSTEM% -rom %ROM% -emulator %EMULATOR% -core %CORE% -ratio %RATIO% %NETPLAY%"/>
  <system uuid="b58b9072-0e44-4330-869d-96b72f53de1f" name="3do" fullname="Panasonic 3DO" platforms="3do">
    <descriptor path="%ROOT%/3do" theme="3do" extensions=".iso .chd .cue"/>
    <scraper screenscraper="29"/>
    <properties type="console" pad="mandatory" keyboard="no" mouse="no" lightgun="optional" releasedate="1993-10" retroachievements="1"/>
    <emulatorList>
      <emulator name="libretro">
        <core name="opera" priority="1" extensions=".chd .cue .iso" netplay="0" compatibility="high" speed="high"/>
      </emulator>
    </emulatorList>
  </system>
  <system>
      [. . .]
  </system>
  [. . .]
</systemList>
```

L’affichage des systèmes respecte l’ordre dans lequel ils sont listés dans ce fichier. Il contient aussi la configuration de ces systèmes.

## Modifier l'ordre d'affichage

>Il ne faut **PAS MODIFIER** le fichier`systemlist.xml` original (qui est dans le répertoire `share_init`). En cas de problème sur les modifications apportées plus tard, ce fichier reste la seule et unique source pour faire fonctionner correctement Recalbox.
{.is-warning}

La modification de l’ordre d’affichage doit se faire uniquement à partir du fichier `systemlist.xml` présent dans le répertoire` /recalbox/share/system/.emulationstation/systemlist.xml`

D'origine, ce fichier n’existe pas. Il faut soit copier le fichier original, soit créer un nouveau fichier. Une fois le nouveau fichier créé, il est possible de mettre les systèmes dans l'ordre que l’on désire. La configuration des systèmes sera toujours prise à partir du fichier `systemlist.xml` originel, mais l'ordre des systèmes sera celui défini dans le nouveau fichier.

Si, dans le nouveau fichier, un système est absent ou mal renseigné, la priorité est rendue au fichier original. Pour le nouveau fichier, il n’y a qu'une seule clé d’entrée qui est obligatoire : « **UUID** », toutes les autres sont optionnelles. Le fichier devra être construit au minimum de la manière suivante :

```xml
<?xml version="1.0"?>
<systemList>
  <system uuid="62f2cbed-5bcb-46d8-bca9-daaa36613c7a"/> <!-- nes -->
  <system uuid="0a7fd1d3-9673-44ab-bfef-5bb3c8a0d79a"/> <!-- fds -->
  <system uuid="3df92492-d69a-48f8-8e14-9a62bd9805a6"/> <!-- snes -->
  <system uuid="99e312bc-7602-4038-a871-e53c17fd1d76"/> <!-- satellaview -->
  [. . .]
</systemList>
```

## Ajouter un système « Custom »

>Il ne faut **PAS MODIFIER** le fichier`systemList` original (qui est dans le répertoire `share_init`). En cas de problème sur les modifications apportées plus tard, ce fichier reste la seule et unique source pour faire fonctionner correctement Recalbox.
{.is-warning}

Cette manipulation ne permet pas d'ajouter un nouvel émulateur à Recalbox mais d'ajouter une nouvelle entrée système dans le menu de sélection.

Il est possible de combiner la modification de l'ordre des systèmes et l'ajout d'un ou plusieurs systèmes customs.

Comme pour la modification de l'ordre des système, l'ajout d'un système « custom » doit se faire uniquement à partir du fichier `systemlist.xml` présent dans le répertoire suivant `/recalbox/share/system/.emulationstation/systemlist.xml`.

D'origine, ce fichier n’existe pas. Il faut soit copier le fichier original, soit créer un nouveau fichier. Une fois le nouveau fichier créé, il est maintenant possible d'y ajouter un nouveau système.

Si, dans le nouveau fichier, un système est mal renseigné, la priorité est rendue au fichier original. Pour le nouveau fichier, toutes les clés d’entrées sont obligatoires. Donc, pour créer un nouveau système, le plus simple est de partir d'un système existant (et correspondant aux roms que l'on veut y inclure) et de ne modifier que le stricte nécessaire :

- _**« fullname »**_ : Permet de donner le nom du nouveau système.
- _**« path »**_ : Permet d'indiquer le répertoire contenant les roms du nouveau système.
- _**« theme »**_ : Permet d'indiquer quel thème utiliser. Il faut, au préalable, créer ce nouveau thème (logo, fond, ...)

**Toutes les autres entrées ne doivent pas être modifiées.** 

Voici un exemple d'ajout pour un système basé sur la SNES pour n'y inclure que des roms traduites :

```xml
<?xml version="1.0"?>
<systemList>
  <system uuid="21b8873a-a93e-409c-ad0c-8bb6d682bef8" name="snes" fullname="Super Nintendo Fan Trad" platform="snes">
    <descriptor path="%ROOT%/snestrad" theme="snestrad" extensions=".smc .sfc .mgd .zip .7z"/>
    <scraper screenscraper=""/>
    <properties type="console" pad="mandatory" keyboard="no" mouse="optional" lightgun="optional" releasedate="1990-11" retroachievements="1"/>
    <emulatorList>
      <emulator name="libretro">
        <core>snes9x2005</core>
        <core>snes9x2010</core>
        <core>snes9x2002</core>
      </emulator>
    </emulatorList>
  </system>
</systemList>
```