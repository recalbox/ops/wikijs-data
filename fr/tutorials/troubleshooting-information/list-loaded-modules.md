---
title: Lister les modules chargés
---




>Vous pouvez **lister tout les modules chargé** en **SSH**.
{.is-info}

1. **Premièrement** [se connecter en root.](/fr/tutorials/system/access/root-access-terminal-cli)
2. **Exécutez** `lsmod`
3. Maintenant vous pouvez **copier/coller les informations nécessaires** dans le [forum](https://forum.recalbox.com/) ou une **"issue"** sur[ **gitlab**.](https://gitlab.com/recalbox/recalbox/)

## Exemple de "Output":

```text
     Module                  Size  Used by    Tainted: G
     mk_arcade_joystick_rpi     2835  0
     8192cu                509620  0
```

