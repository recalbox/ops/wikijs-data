---
title: Libretro Mednafen_SuperGrafx
description: 
published: true
date: 2024-07-14T17:15:44.939Z
tags: libretro, mednafen, supergrafx
editor: markdown
dateCreated: 2023-09-08T09:31:44.038Z
---



## ![](/emulators/license.svg) Licencia

El core posee la siguiente licencia  [**GPLv2**](https://github.com/libretro/beetle-supergrafx-libretro/blob/master/COPYING).

## ![](/emulators/compatibility.png) Compatibilidad

 | RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Funcionalidades

| Funcionalidad | ¿Soportada? |
| :---: | :---: |
| Reinicio | ✔ |
| Capturas de pantalla | ✔ |
| Grabaciones | ✔ |
| Grabaciones instantáneas | ✔ |
| Rebobinado | ✔ |
| Netplay | ✔ |
| Opciones del core | ✔ |
| RetroAchievements | ✔ |
| Cheats RetroArch | ✔ |
| Controles | ✔ |
| Remapping | ✔ |
| Multi-Ratón | ✔ |

## ![](/emulators/bios.svg) BIOS

### Lista de BIOS opcionales

| Nombre del fichero | Descripción | MD5 | ¿Preinstalada? |
| :---: | :---: | :---: | :---: |
| gexpress.pce | Game Express CD Card | 6d2cb14fc3e1f65ceb135633d1694122 | ❌ |

### Emplazamiento

Disponed vuestras bios como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 pcengine
┃ ┃ ┃ ┃ ┣ 🗒 **gexpress.pce**

## ![](/emulators/roms.png) Roms

### Extensiones de roms soportadas

Las roms deben tener las extensiones siguientes:

* .pce/.sgx
* .zip
* .7z

Este sistema soporta las roms comprimidas en formato .zip/.7z. No obstante, se trata sólo de un archivo.

Los ficheros contenidos en los .zip/.7z deben tener las extensiones citadas previamente.
Además, cada fichero .zip/.7z sólo debe contener un fichero rom al interior.

### Emplazamiento

Disponed vuestras roms como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 supergrafx
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.zip**

>Se aconsejan las roms del proyecto **No-Intro**.
{.is-success}

>Para más información sobre las roms, consultad [este tutorial](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

>Para conservar las configuraciones personalizadas cuando se produce una actualización del sistema Recalbox, os aconsejamos utilizar las [Sobrecargas de configuración](./../../../advanced-usage/configuration-override).
{.is-info}

### Acceder a las opciones

Podéis configurar las opciones del core de dos formas diferentes.

* Via el Menú RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via el fichero `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Opciones del core

| Opción | Valor de la opción | Variable (para la sobrecarga) | Valores posibles |
| --- | --- | --- | --- |
Palette de couleurs | `RVB` ✅ / `Composite` | `sgx_palette` | `RGB` / `Composite` |
 | Rapport d'aspect | `Auto` ✅ / `6:5` / `4:3` | `sgx_aspect_ratio` | `auto` / `6:5` / `4:3` |
 | Surbalayage horizontal (Mode largeur 352 uniquement) | `300` / `302` / `304` / `306` / `308` / `310` / `312` / `314` / `316` / `318` / `320` / `322` / `324` / `326` / `328` / `330` / `332` / `334` / `336` / `338` / `340` / `342` / `344` / `346` / `348` / `350` / `352 (par défaut)` ✅ | `sgx_hoverscan` | `300` / `302` / `304` / `306` / `308` / `310` / `312` / `314` / `316` / `318` / `320` / `322` / `324` / `326` / `328` / `330` / `332` / `334` / `336` / `338` / `340` / `342` / `344` / `346` / `348` / `350` / `352` |
 | Ligne de balayage initial | `0` / `1` / `2` / `3 (par défaut)` ✅ / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `21` / `22` / `23` / `24` / `25` / `26` / `27` / `28` / `29` / `30` / `31` / `32` / `33` / `34` / `35` / `36` / `37` / `38` / `39` / `40` | `sgx_initial_scanline` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `21` / `22` / `23` / `24` / `25` / `26` / `27` / `28` / `29` / `30` / `31` / `32` / `33` / `34` / `35` / `36` / `37` / `38` / `39` / `40` |
 | Dernière ligne de balayage | `208` / `209` / `210` / `211` / `212` / `213` / `214` / `215` / `216` / `217` / `218` / `219` / `220` / `221` / `222` / `223` / `224` / `225` / `226` / `227` / `228` / `229` / `230` / `231` / `232` / `233` / `234` / `235` / `236` / `237` / `238` / `239` / `240` / `241` / `242 (par défaut)` ✅ | `sgx_last_scanline` | `208` / `209` / `210` / `211` / `212` / `213` / `214` / `215` / `216` / `217` / `218` / `219` / `220` / `221` / `222` / `223` / `224` / `225` / `226` / `227` / `228` / `229` / `230` / `231` / `232` / `233` / `234` / `235` / `236` / `237` / `238` / `239` / `240` / `241` / `242` |
 | Sensibilité de la souris | `0.25` / `0.50` / `0.75` / `1.00` / `1.25` ✅ / `1.50` / `1.75` / `2.00` / `2.25` / `2.50` / `2.75` / `3.00` / `3.25` / `3.50` / `3.75` / `4.00` / `4.25` / `4.50` / `4.75` / `5.00` | `sgx_mouse_sensitivity` | `0.25` / `0.50` / `0.75` / `1.00` / `1.25` / `1.50` / `1.75` / `2.00` / `2.25` / `2.50` / `2.75` / `3.00` / `3.25` / `3.50` / `3.75` / `4.00` / `4.25` / `4.50` / `4.75` / `5.00` |
 | Autoriser les directions opposées | `Désactivé` ✅ / `Activé` | `sgx_up_down_allowed` | `disabled` / `enabled` |
 | Désactiver la réinitialisation logicielle (RUN+SELECT) | `Désactivé` ✅ / `Activé` | `sgx_disable_softreset` | `disabled` / `enabled` |
 | Contrôleur multitap à 5 ports | `Activé` ✅ / `Désactivé` | `sgx_multitap` | `enabled` / `disabled` |
 | Type de manette par défaut pour le joueur 1 | `2 touches` ✅ / `6 touches` | `sgx_default_joypad_type_p1` | `2 Buttons` / `6 Buttons` |
 | Type de manette par défaut pour le joueur 2 | `2 touches` ✅ / `6 touches` | `sgx_default_joypad_type_p2` | `2 Buttons` / `6 Buttons` |
 | Type de manette par défaut pour le joueur 3 | `2 touches` ✅ / `6 touches` | `sgx_default_joypad_type_p3` | `2 Buttons` / `6 Buttons` |
 | Type de manette par défaut pour le joueur 4 | `2 touches` ✅ / `6 touches` | `sgx_default_joypad_type_p4` | `2 Buttons` / `6 Buttons` |
 | Type de manette par défaut pour le joueur 5 | `2 touches` ✅ / `6 touches` | `sgx_default_joypad_type_p5` | `2 Buttons` / `6 Buttons` |
 | Mode des touches de raccourci du turbo | `Désactivé` ✅ / `On/Off Toggle` / `Dedicated Turbo Buttons` | `sgx_turbo_toggle` | `disabled` / `switch` / `dedicated` |
 | Touches de raccourci turbo alternatives | `X/Y` ✅ / `L3/R3` | `sgx_turbo_toggle_hotkey` | `disabled` / `enabled` |
 | Délai du turbo | `1` / `2` / `3` ✅ / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` | `sgx_turbo_delay` | `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` |
 | Mise en cache des images CD (Redémarrage requis) | `Désactivé` ✅ / `Activé` | `sgx_cdimagecache` | `disabled` / `enabled` |
 | BIOS CD (Redémarrage requis) | `System Card 3` ✅ / `Games Express` / `System Card 1` / `System Card 2` / `System Card 2 US` / `System Card 3 US` | `sgx_cdbios` | `System Card 3` / `Games Express` / `System Card 1` / `System Card 2` / `System Card 2 US` / `System Card 3 US` |
 | Détecter les CD de Games Express (Redémarrage requis) | `Activé` ✅ / `Désactivé` | `sgx_detect_gexpress` | `enabled` / `disabled` |
 | Vitesse du CD | `1` ✅ / `2` / `4` / `8` | `sgx_cdspeed` | `1` / `2` / `4` / `8` |
 | Volume ADPCM % | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ / `110` / `120` / `130` / `140` / `150` / `160` / `170` / `180` / `190` / `200` | `sgx_adpcmvolume` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` / `110` / `120` / `130` / `140` / `150` / `160` / `170` / `180` / `190` / `200` |
 | Volume CDDA % | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ / `110` / `120` / `130` / `140` / `150` / `160` / `170` / `180` / `190` / `200` | `sgx_cddavolume` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` / `110` / `120` / `130` / `140` / `150` / `160` / `170` / `180` / `190` / `200` |
 | Volume PSG % | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ / `110` / `120` / `130` / `140` / `150` / `160` / `170` / `180` / `190` / `200` | `sgx_cdpsgvolume` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` / `110` / `120` / `130` / `140` / `150` / `160` / `170` / `180` / `190` / `200` |
 | Forcer l'émulation de la SuperGrafx (Redémarrage requis) | `Désactivé` ✅ / `Activé` | `sgx_forcesgx` | `disabled` / `enabled` |
 | Aucune limite de sprites | `Désactivé` ✅ / `Activé` | `sgx_nospritelimit` | `disabled` / `enabled` |
 | Multiplicateur d'overclocking du processeur (Redémarrage requis) | `1` ✅ / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `20` / `30` / `40` / `50` | `sgx_ocmultiplier` | `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `20` / `30` / `40` / `50` |
 
## ![](/emulators/external-links.png) Enlaces externos

* **Código fuente utilizado** : [https://github.com/libretro/beetle-supergrafx-libretro/](https://github.com/libretro/beetle-supergrafx-libretro/)
* **Documentación Libretro** : [https://docs.libretro.com/library/beetle_sgx/](https://docs.libretro.com/library/beetle_sgx/)