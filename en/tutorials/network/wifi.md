---
title: Wifi
description: 
published: true
date: 2021-10-03T22:25:39.077Z
tags: wifi
editor: markdown
dateCreated: 2021-08-06T08:27:06.596Z
---

If you want to activate the wifi on your Recalbox or if you have a connection problem, you should find your happiness here.

Here are the available tutorials:

[Enable wifi](enable-wifi)
[Wifi country code](wifi-country-code)