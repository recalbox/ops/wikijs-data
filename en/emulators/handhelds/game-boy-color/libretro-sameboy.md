---
title: Libretro SameBoy
description: 
published: true
date: 2021-10-20T13:37:10.568Z
tags: libretro, color, gameboy, sameboy, gbc
editor: markdown
dateCreated: 2021-07-14T06:40:20.421Z
---

**Libretro SameBoy** is an extremely accurate open source Gameboy (DMG) and Gameboy Color (CGB) emulator written in C.

* Supports Game Boy (DMG); Game Boy Color (CGB) and GBC-Mode emulation; Game Boy Advance (AGB)
* Supports accurate high-level emulation of Super Game Boy (SGB; NTSC and PAL) and Super Game Boy 2 (SGB2)
* Allows you to choose the model you want to emulate regardless of ROM
* High quality 96 kHz audio
* Support for battery saving
* Save states
* Includes open source boot ROMs for all emulated models:

  * Full support for (and documentation of) all game-specific palettes in the CGB / AGB boot ROM, for accurate emulation of Game Boy games on a Game Boy Color
  * Supports manual palette selection with key combinations, with 4 additional new palettes (direction + A or B)
  * Supports palette selection in a CGB game, forcing it to run in "palletized" DMG mode, if the ROM allows it
  * Support for games with a non-Nintendo logo in the header
  * No long animation in the DMG boot ROM

* Four color correction settings
* Three high-pass audio filter settings
* Real-time clock emulation
* Turbo, rewind and slow motion modes
* Extremely high accuracy
* Link cable emulation

## ![](/emulators/license.svg) License

This core is under [**MIT**](https://github.com/libretro/SameBoy/blob/master/LICENSE) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Features

| Feature | Supported |
| :---: | :---: |
| Restart | ✔ |
| Screenshots | ✔ |
| Saves | ✔ |
| States | ✔ |
| Rewind | ✔ |
| Netplay | ✔ |
| Core Options | ✔ |
| RetroAchievements | ✔ |
| Controls | ✔ |
| Remapping | ✔ |
| Rumble | ✔ |
| Subsystem | ✔ |

## ![](/emulators/bios.svg) BIOS

### List of optional bios

| Filename | Description | MD5 | Provided |
| :---: | :---: | :---: | :---: |
| cgb_boot.bin | Game Boy Color BIOS | b560efd759d87634a03b243f22bba27a | ✅ |

### Location

Put your bios like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 🗒 **cgb_boot.bin**

## ![](/emulators/roms.png) Roms

### Supported extensions

Roms must have the extension:

* .bin
* .gbc
* .zip
* .7z

This system supports compressed roms in .zip/.7z format. However, be careful, it is only an archive.

The files contained in the .zip/.7z must correspond to the extensions mentioned above.
Moreover, each .zip/.7z file must contain only one compressed rom.

### Location

Put your roms like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 gbc
┃ ┃ ┃ ┃ ┣ 🗒 **game.zip**

>The roms using **No-Intro** format are highly recommended.
{.is-success}

>For more information about roms, go to [this tutorial](./../../../tutorials/games/generalities/isos-and-roms)!
{.is-info}

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Accessing to options

You can configure various options in two different ways.

* Via RetroArch menu:

┣ 📁 RetroArch Menu
┃ ┣ 📁 Core options
┃ ┃ ┣ 🧩 Name_option

* Via the file `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core options

Use this table for single player:

| Options | Options values | Variables (for configuration override) | Variables values |
| --- | --- | --- | --- |
| Color correction | `emulate hardware` ✅ / `preserve brightness` / `reduce contrast` / `off` / `correct curves` | `sameboy_color_correction_mode` | `emulate hardware` / `preserve brightness` / `reduce contrast` / `off` / `correct curves` |
| High-pass filter | `accurate` ✅ / `more dc offset` / `off` | `sameboy_high_pass_filter_mode` | `accurate` / `more dc offset` / `off` |
| Emulated model | `Auto` ✅ / `Game Boy` / `Game Boy Color` / `Game Boy Advance` / `Super Game Boy` / `Super Game Boy 2` | `sameboy_model` | `Auto` / `Game Boy` / `Game Boy Color` / `Game Boy Advance` / `Super Game Boy` / `Super Game Boy 2` |
| Display border | `Super Game Boy only` ✅ / `always` / `never` | `sameboy_border` | `Super Game Boy only` / `always` / `never` |
| Enable rumble | `rumble-enabled games` ✅ / `all games` / `never` | `sameboy_rumble` | `rumble-enabled games` / `all games` / `never` |

Use this table for linked players:

| Options | Options values | Variables (for configuration override) | Variables values |
| --- | --- | --- | --- |
| Link cable emulation | `Enabled` ✅ / `Disabled` | `sameboy_link` | `enabled` / `disabled` |
| Screen layout | `top-down` ✅ / `left-right` | `sameboy_screen_layout` | `top-down` / `left-right` |
| Audio output | `GameBoy #1` ✅ / `GameBoy #2` | `sameboy_audio_output` | `GameBoy #1` / `GameBoy #2` |
| Emulated model for GameBoy #1 | `Auto` ✅ / `Game Boy` / `Game Boy Color` / `Game Boy Advance` / `Super Game Boy` / `Super Game Boy 2` | `sameboy_model_1` | `Auto` / `Game Boy` / `Game Boy Color` / `Game Boy Advance` / `Super Game Boy` / `Super Game Boy 2` |
| Emulated model for GameBoy #2 | `Auto` ✅ / `Game Boy` / `Game Boy Color` / `Game Boy Advance` / `Super Game Boy` / `Super Game Boy 2` | `sameboy_model_2` | `Auto` / `Game Boy` / `Game Boy Color` / `Game Boy Advance` / `Super Game Boy` / `Super Game Boy 2` |
| Color correction for GameBoy #1 | `emulate hardware` ✅ / `preserve brightness` / `reduce contrast` / `off` / `correct curves` | `sameboy_color_correction_mode_1` | `emulate hardware` / `preserve brightness` / `reduce contrast` / `off` / `correct curves` |
| Color correction for GameBoy #2 | `emulate hardware` ✅ / `preserve brightness` / `reduce contrast` / `off` / `correct curves` | `sameboy_color_correction_mode_2` | `emulate hardware` / `preserve brightness` / `reduce contrast` / `off` / `correct curves` |
| High-pass filter for GameBoy #1 | `accurate` ✅ / `more dc offset` / `off` | `sameboy_high_pass_filter_mode_1` | `accurate` / `more dc offset` / `off` |
| High-pass filter for GameBoy #2 | `accurate` ✅ / `more dc offset` / `off` | `sameboy_high_pass_filter_mode_2` | `accurate` / `more dc offset` / `off` |
| Enable rumble for GameBoy #1 | `rumble-enabled games` ✅ / `all games` / `never` | `sameboy_rumble_1` | `rumble-enabled games` / `all games` / `never` |
| Enable rumble for GameBoy #2 | `rumble-enabled games` ✅ / `all games` / `never` | `sameboy_rumble_2` | `rumble-enabled games` / `all games` / `never` |

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/libretro/SameBoy/](https://github.com/libretro/SameBoy/)
* **Libretro documentation**: [https://docs.libretro.com/library/sameboy/](https://docs.libretro.com/library/sameboy/)
* **Official source code**: [https://github.com/LIJI32/SameBoy](https://github.com/LIJI32/SameBoy/)
* **Official website**: [https://sameboy.github.io](https://sameboy.github.io)