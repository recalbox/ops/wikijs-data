---
title: Wonderswan
description: Bandai Wonderswan
published: true
date: 2024-07-26T08:35:00.340Z
tags: bandai, wonderswan, portátiles
editor: markdown
dateCreated: 2023-09-04T12:23:49.439Z
---

![](/emulators/handheld/wonderswan.svg){.align-center}

## Detalles técnicos

* **Fabricante**: Bandai
* **Año de lanzamiento**: 1999
* **Unidades vendidas**: 3,5 millones
* **Procesador**: 16Bit NEC V20
* **RAM**: 512k vídeo
* **Resolución**: 224 x 144 píxeles
* **Sonido**: 4 canales PCM, 32 muestras de 4 bits por canal.

## Presentación

La **WonderSwan** salió a la venta en marzo de 1999 en Japón, donde tuvo un gran éxito. Era una consola monocroma que se vendió bien a pesar del lanzamiento de la Game Boy Color un año antes, gracias a una ventaja: su pequeño tamaño. Como resultado, tiene una gran autonomía, sin dejar de ser potente.

El inventor de esta consola es Gunpei Yokoi, el hombre detrás de la Game Boy.

La WonderSwan ha sufrido dos evoluciones: la WonderSwan Color, con pantalla en color, y la SwanCrystal, una versión de la WonderSwan Color con mejor pantalla.

Estos tres modelos vendieron 2.061.193 unidades en Japón, su único mercado.

## Emulador

[Libretro Mednafen_WSwan](libretro-mednafen-wswan)