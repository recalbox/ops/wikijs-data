---
title: Bem-vindos a Wiki do Recalbox!
description: 
published: true
date: 2024-06-02T16:54:33.770Z
tags: 
editor: markdown
dateCreated: 2021-06-23T23:52:12.807Z
---

![welcome.png](/home/welcome.png)

## O que é o Recalbox?

[Recalbox](https://www.recalbox.com) é o **derradeiro console de jogos retrô** que lhe permite jogar novamente todos os jogos dos consoles e computadores de sua infância.

Com [Recalbox](https://www.recalbox.com), (re)descubra e jogue(novamente) os títulos que moldaram o mundo dos videogames com facilidade!

O [Recalbox](https://www.recalbox.com) pode ser facilmente instalado em microcomputadores baratos como o Raspberry Pi, e até mesmo em seu PC (moderno ou não)!

## Eu quero instalar o Recalbox! Por onde eu começo?

Para instalar o Recalbox, vá para a página [Preparação e Instalação](./basic-usage/preparation-and-installation).

Para saber mais sobre os sistemas suportados, visite a seção [compatibilidade de hardware](./hardware-compatibility).

## Estou procurando uma página específica!

Use e abuse da função de busca no topo da página!
Você pode simplesmente preencher sua busca na caixa de texto...
![search-text.png](/home/search-text.png)
Ou, você pode pesquisar por *tag* (etiqueta).
![search-tags.png](/home/search-tags.png)

## Sou novato(a), por onde começo?

* Lhe encorajamos fortemente a olhar a documentação:
  * [Uso básico](./basic-usage), para entender o **funcionamento básico** do [Recalbox](https://www.recalbox.com).
  * [Compatibilidade dos emuladores](./hardware-compatibility/emulators-compatibility).
  * [Compatibilidade de dispositivos](./hardware-compatibility/compatible-devices).
  * Você pode consultar a seção [Emuladores](./emulators) para ter mais informações sobre os emuladores disponíveis no [Recalbox](https://www.recalbox.com).
  * Se alguns termos são muito técnicos para você, o [glossário](./basic-usage/glossary) pode ser uma ferramenta muito útil.
* Para uma utilização mais avançada do Recalbox:
  * Consulte os [tutoriais](./tutorials).
  * Visite a seção [Utilização avançada](./advanced-usage).
* Você também pode verificar estes links para obter informações adicionais: 
  * O [site oficial](https://www.recalbox.com) do Recalbox.
  * O [fórum](https://forum.recalbox.com/).
  * O [Discord](https://discord.gg/NbQFbGM).

## Algum conselho para um novo usuário?

[Recalbox](https://www.recalbox.com) é um projeto ***open-source***, ou seja, é **livre**!

As pessoas que trabalham nele o fazem em seu próprio tempo, portanto, o projeto e sua documentação evoluem tão rapidamente quanto as pessoas contribuem para ele.

O **melhor conselho** que podemos lhe dar é:

* Ser curioso(a).
* Ler a documentação.
* Ler os [tutoriais](./tutorials).
* Contribuir para a Wiki, seja criando conteúdo ou traduzindo (veja [Como contribuir](./contribute))
* Pesquisar no [fórum](https://forum.recalbox.com/).
* Sempre faça uma cópia de segurança (*backup*) da sua pasta **SHARE** antes de fazer qualquer coisa sobre a qual você não tenha certeza.