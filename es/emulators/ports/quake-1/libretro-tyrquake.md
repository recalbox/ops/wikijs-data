---
title: Libretro TyrQuake
description: 
published: true
date: 2024-07-25T22:57:48.385Z
tags: libretro, quake 1, tyrquake
editor: markdown
dateCreated: 2023-09-12T06:21:38.432Z
---

## ![](/emulators/license.svg) Licencia

El core posee la siguiente licencia [**GPLv2**](https://github.com/libretro/tyrquake/blob/master/gnu.txt).

## ![](/emulators/compatibility.png) Compatibilidad

 | RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | 
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | 

## ![](/emulators/features.png) Funcionalidades

| Funcionalidad | ¿Soportada? |
| :---: | :---: |
| Opciones del core | ✔ |

## ![](/emulators/bios.svg) BIOS

>**No necesita bios.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Extensiones de roms soportadas

Las roms deben tener las extensiones siguientes:

* .pak

### Emplazamiento

Disponed vuestras roms como en la siguiente imagen: 

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 ports
┃ ┃ ┃ ┃ ┣ 📁 Quake
┃ ┃ ┃ ┃ ┃ ┣ 📁 id1
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 **pak0.pak**

Esta rom se encuentra presente por defecto en vuestra Recalbox.

## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

>Para conservar las configuraciones personalizadas cuando se produce una actualización del sistema Recalbox, os aconsejamos utilizar las [Sobrecargas de configuración](./../../../advanced-usage/configuration-override).
{.is-info}

### Acceder a las opciones

Podéis configurar las opciones del core de dos formas diferentes.

* Via el Menú RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via el fichero `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Opciones del core

| Opción | Valor de la opción | Variable (para la sobrecarga) | Valores posibles |
| --- | --- | --- | --- |
| Internal resolution(restart) | `320x200` ✅ / `320x240` / `320x480` / `360x200` / `360x240` / `360x400` / `360x480` / `400x224` / `400x240` / `480x272` / `512x224` / `512x240` / `512x384` / `512x512` / `640x224` / `640x240` / `640x448` / `640x400` / `640x480` / `720x576` / `800x480` / `800x600` / `960x720` / `960x600` / `1024x768` / `1280x720` / `1280x800` / `1600x900` / `1600x1000` / `1600x1200` / `1920x1080` / `1920x1200` | `tyrquake_resolution` | `320x240` / `320x480` / `360x200` / `360x240` / `360x400` / `360x480` / `400x224` / `400x240` / `480x272` / `512x224` / `512x240` / `512x384` / `512x512` / `640x224` / `640x240` / `640x448` / `640x400` / `640x480` / `720x576` / `800x480` / `800x600` / `960x720` / `960x600` / `1024x768` / `1280x720` / `1280x800` / `1600x900` / `1600x1000` / `1600x1200` / `1920x1080` / `1920x1200` |
| Framerate (restart) | `Auto` ✅ / `10fps` / `15fps` / `20fps` / `25fps` / `30fps` / `40fps` / `50fps` / `60fps` / `72fps` / `75fps` / `90fps` / `100fps` / `119fps` / `120fps` / `144fps` / `155fps` / `160fps` / `165fps` / `180fps` / `200fps` / `240fps` / `244fps` / `300fps` / `360fps` | `tyrquake_framerate` | `auto` / `10` / `15` / `20` / `25` / `30` / `40` / `50` / `60` / `72` / `75` / `90` / `100` / `119` / `120` / `144` / `155` / `160` / `165` / `180` / `200` / `240` / `244` / `300` / `360` |
| Colored Lighting (restart) | `Désactivé` ✅ / `Activé` | `tyrquake_colored_lighting` | `disabled` / `enabled` |
| Rumble | `Désactivé` ✅ / `Activé` | `tyrquake_rumble` | `disabled` / `enabled` |
| Invert Y Axis | `Désactivé` ✅ / `Activé` | `tyrquake_invert_y_axis` | `disabled` / `enabled` |
| Analog Deadzone (percent) | `0` / `5` / `10` / `15` ✅ / `20` / `25` / `30` | `tyrquake_analog_deadzone` | `0` / `5` / `10` / `15` / `20` / `25` / `30` |

## ![](/emulators/external-links.png) Enlaces externos

* **Código fuente utilizado** : [https://github.com/libretro/tyrquake/](https://github.com/libretro/tyrquake/)
* **Documentación Libretro** : [https://docs.libretro.com/library/tyrquake/](https://docs.libretro.com/library/tyrquake/)