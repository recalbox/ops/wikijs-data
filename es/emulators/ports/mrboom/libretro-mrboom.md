---
title: Libretro MrBoom
description: 
published: true
date: 2024-07-25T22:52:59.599Z
tags: libretro, mrboom
editor: markdown
dateCreated: 2023-09-12T06:14:58.476Z
---

Libretro MrBoom es un core para ejecutar un clon de Bomberman para hasta 8 jugadores para LibRetro / RetroArch.

## ![](/emulators/license.svg) Licencia

El core posee la siguiente licencia [**MIT**](https://github.com/libretro/mrboom-libretro/blob/master/LICENSE).

## ![](/emulators/compatibility.png) Compatibilidad

 | RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | 
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | 

## ![](/emulators/features.png) Funcionalidades

| Funcionalidad | ¿Soportada? |
| :---: | :---: |
| Grabaciones instantáneas | ✔ |
| Rebobinado | ✔ |
| Opciones del core | ✔ |
| Netplay | ✔ |
| Controles | ✔ |

## ![](/emulators/bios.svg) BIOS

>**No necesita bios.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Extensiones de roms soportadas

El núcleo Mr.Boom no especifica ningun tipo de extensión. Basta con cargar y ejecutar el core.

### Emplazamiento

Disponed vuestras roms como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 ports
┃ ┃ ┃ ┃ ┣ 📁 mrboom
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **mrboom.game**

## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

>Para conservar las configuraciones personalizadas cuando se produce una actualización del sistema Recalbox, os aconsejamos utilizar las [Sobrecargas de configuración](./../../../advanced-usage/configuration-override).
{.is-info}

### Acceder a las opciones

Podéis configurar las opciones del core de dos formas diferentes.

* Via el Menú RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via el fichero `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Opciones del core

| Opción | Valor de la opción | Variable (para la sobrecarga) | Valores posibles |
| --- | --- | --- | --- |
| Team mode | `Selfie` ✅ / `Color` / `Sex` / `Skynet` | `mrboom-teammode` | `Selfie` / `Color` / `Sex` / `Skynet` |
| Monsters | `Activé` ✅ / `Désactivé` | `mrboom-nomonster` | `ON` / `OFF` |
| Level select | `Normal` ✅ / `Candy` / `Penguins` / `Pink` / `Jungle` / `Board` / `Soccer` / `Sky` / `Aliens` / `Random` | `mrboom-levelselect` | `Normal` / `Candy` / `Penguins` / `Pink` / `Jungle` / `Board` / `Soccer` / `Sky` / `Aliens` / `Random` |
| Aspect ratio | `Native` ✅ / `4:3` / `16:9` | `mrboom-aspect` | `Native` / `4:3` / `16:9` |
| Music volume | `0` / `5` / `10` / `15` / `20` / `25` / `30` / `35` / `40` / `45` / `50` / `55` / `60` / `65` / `70` / `75` / `80` / `85` / `90` / `95` / `100` ✅ | `mrboom-musicvolume` | `0` / `5` / `10` / `15` / `20` / `25` / `30` / `35` / `40` / `45` / `50` / `55` / `60` / `65` / `70` / `75` / `80` / `85` / `90` / `95` / `100` |
| Sfx volume | `0` / `5` / `10` / `15` / `20` / `25` / `30` / `35` / `40` / `45` / `50` ✅ / `55` / `60` / `65` / `70` / `75` / `80` / `85` / `90` / `95` / `100` | `mrboom-sfxvolume` | `0` / `5` / `10` / `15` / `20` / `25` / `30` / `35` / `40` / `45` / `50` / `55` / `60` / `65` / `70` / `75` / `80` / `85` / `90` / `95` / `100` |

## ![](/emulators/external-links.png) Enlaces externos

* **Código fuente utilizado** : [https://github.com/libretro/mrboom-libretro/](https://github.com/libretro/mrboom-libretro)
* **Página web oficial** : [http://mrboom.mumblecore.org/](http://mrboom.mumblecore.org/)