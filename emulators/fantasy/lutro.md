---
title: Lutro
description: Lua engine
published: true
date: 2022-09-06T09:50:45.371Z
tags: fantasy, lutro
editor: markdown
dateCreated: 2021-06-30T09:06:29.825Z
---

![](/emulators/fantasy/lutro.svg){.align-center}

## Technical data

* **Manufacturer**: -
* **Year of release**: -

## Presentation

Lutro is an experimental Lua game framework for Libretro following the LÖVE API.

Lutro is a renderer and implements only a subset of the LÖVE API. It targets portability via the libretro API and relies on dependencies.

## Emulators

[Libretro Lutro](libretro-lutro)