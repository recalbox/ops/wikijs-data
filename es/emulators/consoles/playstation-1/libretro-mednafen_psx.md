---
title: Libretro Mednafen_PSX
description: 
published: true
date: 2024-07-29T15:42:07.790Z
tags: libretro, ps1, mednafen, playstation 1, psx
editor: markdown
dateCreated: 2023-09-07T16:16:28.309Z
---

**Beetle PSX** es un fork del módulo PSX de [Mednafen](https://mednafen.github.io/) para Libretro, funciona actualmente bajo Linux, macOS y Windows.

## ![](/emulators/license.svg) Licencia

Este núcleo está bajo la licencia [**GPLv2**](https://github.com/libretro/beetle-psx-libretro/blob/master/COPYING).

## ![](/emulators/compatibility.png) Compatibilidad

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ❌ | ❌ | ✅ | ✅ | ❌ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Funcionalidades

| Funcionalidad | Soportado |
| :---: | :---: |
| Reinicio | ✔ |
| Captura de pantalla | ✔ |
| Guardados | ✔ |
| Guardados instantáneos | ✔ |
| Rebobinado | ✔ |
| Opciones del núcleo | ✔ |
| RetroAchievements | ✔ |
| Cheats RetroArch | ✔ |
| Controles | ✔ |
| Remapeo | ✔ |
| Multi-Mouse | ✔ |
| Vibración | ✔ |
| Control de disco | ✔ |

## ![](/emulators/bios.svg) BIOS

### Lista de BIOS obligatorios

| Nombre de archivo | Descripción | MD5 | Proporcionado |
| :---: | :---: | :---: | :---: |
| scph5500.bin | BIOS PS1 japonés - requerido para los juegos japoneses | 8dd7d5296a650fac7319bce665a6a53c | ❌ |
| scph5501.bin | BIOS PS1 americano - requerido para los juegos americanos | 490f666e1afb15b7362b406ed1cea246 | ❌ |
| scph5502.bin | BIOS PS1 europeo - requerido para los juegos europeos | 32736f17079d0b2b7024407c39bd3050 | ❌ |

### Ubicación

Coloque los BIOS de la siguiente manera:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 psx
┃ ┃ ┃ ┃ ┣ 🗒 **scph5500.bin**
┃ ┃ ┃ ┃ ┣ 🗒 **scph5501.bin**
┃ ┃ ┃ ┃ ┣ 🗒 **scph5502.bin**

## ![](/emulators/isos.svg) Isos

### Extensiones soportadas

Los isos deben tener las siguientes extensiones:

* .bin/.cue
* .bin/.toc
* .chd
* .exe
* .img/.ccd/.sub
* .m3u
* .pbp

### Ubicación

Coloque los isos de la siguiente manera:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 psx
┃ ┃ ┃ ┃ ┣ 🗒 **juego.bin**
┃ ┃ ┃ ┃ ┣ 🗒 **juego.cue**

>Los isos en formato **Redump** son vivamente recomendados.
{.is-success}

>Para más información sobre los isos, visite [este tutorial](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

>Para poder conservar sus configuraciones personales durante una actualización, le recomendamos utilizar nuestra funcionalidad [Sobrecargas de configuración](./../../../advanced-usage/configuration-override).
{.is-info}

### Acceder a las opciones

Puede configurar diversas opciones de dos maneras diferentes.

* A través del menú RetroArch:

┣ 📁 Menú RetroArch
┃ ┣ 📁 Opciones del núcleo
┃ ┃ ┣ 🧩 Name_option

* A través del archivo `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Opciones del núcleo

| Opciones | Valores de las opciones | Variable (para sobrecarga) | Valores de las variables |
| --- | --- | --- | --- |
| Resolución del procesador gráfico interno | `1x (nativo)` ✅ / `2x` / `4x` / `8x` / `16x` | `beetle_psx_internal_resolution` | `1x(nativo)` / `2x` / `4x` / `8x` / `16x` |
| Profundidad de colores interna | `16 bpp (nativo)` ✅ / `32 bpp` | `beetle_psx_depth` | `16bpp(nativo)` / `32 bpp` |
| Modo de tramado (dithering) | `1x (nativo)` ✅ / `Método interno` / `Método desactivado` | `beetle_psx_dither_mode` | `1x(nativo)` / `internal method` / `disabled` |
| Desplazamiento UV de la textura | `Activado` ✅ / `Desactivado` | `beetle_psx_scaled_uv_offset` | `enabled` / `disabled` |
| Filtro de texturas | `Au más próximo` ✅ / `SABR` / `xBR` / `Bilineal` / `3-punto` / `JINC2` | `beetle_psx_filter` | `nearest` / `SABR` / `xBR` / `bilinear` / `3-point` / `JINC2` |
| Excluir los sprites del filtro | `Desactivado` ✅ / `Solo sprite` / `Todos` | `beetle_psx_filter_exclude_sprite` | `disabled` / `sprite only` / `all` |
| Excluir los polígonos 2D del filtro | `Desactivado` ✅ / `Solo polígono` | `beetle_psx_filter_exclude_2d_polygon` | `disabled` / `sprite only` |
| Suavización adaptativa | `Desactivado` ✅ / `Activado` | `psx_adaptive_smoothing` | `disabled` / `enabled` |
| Super muestreo (muestreo a la resolución nativa) | `Desactivado` ✅ / `Activado` | `psx_super_sampling` | `disabled` / `enabled` |
| Anti-crénelación multi-muestreo | `1x (Por defecto)` ✅ / `2x` / `4x` / `8x` / `16x` | `psx_msaa` | `1x` / `2x` / `4x` / `8x` / `16x` |
| Filtro MDEC YUV chroma | `Desactivado` ✅ / `Activado` | `psx_mdec_yuv` | `disabled` / `enabled` |
| Sobrecargar las texturas | `Desactivado` ✅ / `Activado` | `beetle_psx_track_textures` | `disabled` / `enabled` |
| Importar las texturas | `Desactivado` ✅ / `Activado` | `beetle_psx_dump_textures` | `disabled` / `enabled` |
| Reemplazar las texturas | `Desactivado` ✅ / `Activado` | `beetle_psx_replace_textures` | `disabled` / `enabled` |
| Modo de línea de ferro (debugging) | `Desactivado` ✅ / `Activado` | `beetle_psx_wireframe` | `disabled` / `enabled` |
| Duplicación de imágenes (aceleración) | `Desactivado` ✅ / `Activado` | `psx_frame_duping` | `disabled` / `enabled` |
| Guardar las i/s internas | `Desactivado` ✅ / `Activado` | `beetle_psx_display_internal_fps` | `disabled` / `enabled` |
| Guardar la VRAM completa (debugging) | `Desactivado` ✅ / `Activado` | `beetle_psx_dispaly_vram` | `disabled` / `enabled` |
| Calibración analógica automática | `Desactivado` ✅ / `Activado` | `beetle_psx_analog_calibration` | `disabled` / `enabled` |
| Activar el modo analógico DualShock | `Desactivado` ✅ / `Activado` | `beetle_psx_analog_toggle` | `disabled` / `enabled` |
| Combinación de teclas del modo analógico DualShock | `L1 + L2 + R1 + R2 + Start + Select` / `L1 + R1 + Select` ✅ / `L1 + R1 + Start` / `L1 + R1 + L3` / `L1 + R1 + R3` / `L2 + R2 + Select` / `L2 + R2 + Start` / `L2 + R2 + L3` / `L2 + R2 + R3` / `L3 + R3` | `beetle_psx_analog_toggle_combo` | `l1+l2+r1+r2+start+select` / `l1+r1+select` / `l1+r1+start` / `l1+r1+l3` / `l1+r1+r3` / `l2+r2+select` / `l2+r2+start` / `l2+r2+l3` / `l2+r2+r3` / `l3+r3` |
| Retraso del mantenimiento de la combinación de teclas del modo analógico DualShock | `0 segundos` / `1 segundo` ✅ / `2 segundos` / `3 segundos` / `4 segundos` / `5 segundos` | `beetle_psx_analog_toggle_hold` | `0` / `1` / `2` / `3` / `4` / `5` |
| Puerto 1: Activar el multitap | `Desactivado` ✅ / `Activado` | `beetle_psx_enable_multitap_port1` | `disabled` / `enabled` |
| Puerto 2: Activar el multitap | `Desactivado` ✅ / `Activado` | `beetle_psx_enable_multitap_port2` | `disabled` / `enabled` |
| Modo de entrada del pistolet | `Pistolet` ✅ / `Pantalla táctil` | `beetle_psx_gun_input_mode` | `lightgun` / `touchscreen` |
| Visor del pistolet | `Cruz` ✅ / `Punto` / `Desactivado` | `beetle_psx_gun_cursor` | `cross` / `dot` / `off` |
| Puerto 1: Color del visor del pistolet | `Rojo` ✅ / `Azul` / `Verde` / `Naranja` / `Amarillo` / `Cian` / `Rosa` / `Violeta` / `Negro` / `Blanco` | `beetle_psx_crosshair_color_p1` | `red` / `blue` / `green` / `orange` / `yellow` / `cyan` / `pink` / `purple` / `black` / `white` |
| Puerto 2: Color del visor del pistolet | `Azul` ✅ / `Rojo` / `Verde` / `Naranja` / `Amarillo` / `Cian` / `Rosa` / `Violeta` / `Negro` / `Blanco` | `beetle_psx_crosshair_color_p2` | `blue` / `red` / `green` / `orange` / `yellow` / `cyan` / `pink` / `purple` / `black` / `white` |
| Sensibilidad del ratón | `5%` / `10%` / `15%` / `20%` / `25%` / `30%` / `35%` / `40%` / `45%` / `50%` / `55%` / `60%` / `65%` / `70%` / `75%` / `80%` / `85%` / `90%` / `95%` / `100%` ✅ / `105%` / `110%` / `115%` / `120%` / `125%` / `130%` / `135%` / `140%` / `145%` / `150%` / `155%` / `160%` / `165%` / `170%` / `175%` / `180%` / `185%` / `190%` / `195%` / `200%` | `beetle_psx_mouse_sensitivity` | `5%` / `10%` / `15%` / `20%` / `25%` / `30%` / `35%` / `40%` / `45%` / `50%` / `55%` / `60%` / `65%` / `70%` / `75%` / `80%` / `85%` / `90%` / `95%` / `100%` / `105%` / `110%` / `115%` / `120%` / `125%` / `130%` / `135%` / `140%` / `145%` / `150%` / `155%` / `160%` / `165%` / `170%` / `175%` / `180%` / `185%` / `190%` / `195%` / `200%` |
| Respuesta de torsión del neGcon | `Lineal` ✅ / `Cuadrático` / `Cúbico` | `beetle_psx_negcon_response` | `linear` / `quadratic` / `cubic` |
| Zona muerta de torsión del neGcon | `0%` ✅ / `5%` / `10%` / `15%` / `20%` / `25%` / `30%` | `beetle_psx_negcon_deadzone` | `5%` / `10%` / `15%` / `20%` / `25%` / `30%` |
| Método de la Memory Card 0 (Reinicio requerido) | `Libretro` ✅ / `Mednafen` | `beetle_psx_use_mednafen_memcard0_method` | `libretro` / `mednafen` |
| Activar la Memory Card 1 (Reinicio requerido) | `Activado` ✅ / `Desactivado` | `beetle_psx_enable_memcard1` | `enabled` / `disabled` |
| Memory Cards compartidas (Reinicio requerido) | `Activado` ✅ / `Desactivado` | `beetle_psx_shared_memory_cards` | `enabled` / `disabled` |
| Índice de la Memory Card izquierda | `0` ✅ / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `21` / `22` / `23` / `24` / `25` / `26` / `27` / `28` / `29` / `30` / `31` / `32` / `33` / `34` / `35` / `36` / `37` / `38` / `39` / `40` / `41` / `42` / `43` / `44` / `45` / `46` / `47` / `48` / `49` / `50` / `51` / `52` / `53` / `54` / `55` / `56` / `57` / `58` / `59` / `60` / `61` / `62` / `63` | `beetle_psx_memcard_left_index` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `21` / `22` / `23` / `24` / `25` / `26` / `27` / `28` / `29` / `30` / `31` / `32` / `33` / `34` / `35` / `36` / `37` / `38` / `39` / `40` / `41` / `42` / `43` / `44` / `45` / `46` / `47` / `48` / `49` / `50` / `51` / `52` / `53` / `54` / `55` / `56` / `57` / `58` / `59` / `60` / `61` / `62` / `63` |
| Índice de la Memory Card derecha | `0` / `1 (por defecto)` ✅ / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `21` / `22` / `23` / `24` / `25` / `26` / `27` / `28` / `29` / `30` / `31` / `32` / `33` / `34` / `35` / `36` / `37` / `38` / `39` / `40` / `41` / `42` / `43` / `44` / `45` / `46` / `47` / `48` / `49` / `50` / `51` / `52` / `53` / `54` / `55` / `56` / `57` / `58` / `59` / `60` / `61` / `62` / `63` | `beetle_psx_memcard_right_index` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `21` / `22` / `23` / `24` / `25` / `26` / `27` / `28` / `29` / `30` / `31` / `32` / `33` / `34` / `35` / `36` / `37` / `38` / `39` / `40` / `41` / `42` / `43` / `44` / `45` / `46` / `47` / `48` / `49` / `50` / `51` / `52` / `53` / `54` / `55` / `56` / `57` / `58` / `59` / `60` / `61` / `62` / `63` |
| Modo de operación de PGXP | `Desactivado` ✅ / `Memoria únicamente` / `Memoria + CPU` |`beetle_psx_pgxp_mode` | `disabled` / `memory only` / `memory + CPU` |
| Tolerancia a la geometría 2D de PGXP | `Desactivado` ✅ / `1px` / `2px` / `3px` / `4px` / `5px` / `6px` / `7px` / `8px` | `beetle_psx_pgxp_2d_tol` | `disabled` / `1px` / `2px` / `3px` / `4px` / `5px` / `6px` / `7px` / `8px` |
| Eliminación primitiva de las caras cachadas de PGXP | `Desactivado` ✅ / `Activado` | `beetle_psx_pgxp_nclip` | `disabled` / `enabled` |
| Caché de vértices de PGXP | `Desactivado` ✅ / `Activado` | `beetle_psx_pgxp_vertex` | `disabled` / `enabled` |
| Texturas correctas en perspectiva de PGXP | `Desactivado` ✅ / `Activado` | `beetle_psx_pgxp_texture` | `disabled` / `enabled` |
| Hack de líneas en cuadriláteros | `Por defecto` ✅ / `Agressivo` / `Desactivado` | `beetle_psx_line_render` | `default` / `aggressive` / `disabled` |
| Hack del modo de línea grande | `Desactivado` ✅ / `Activado` | `beetle_psx_widescreen_hack` | `disabled` / `enabled` |
| Relación de aspecto del hack del modo de línea grande | `16:10` / `16:9` ✅ / `18:9` / `19:9` / `20:9` / `21:9` / `32:9` | `beetle_psx_widescreen_hack_aspect_ratio` | `16:10` / `16:9` / `18:9` / `19:9` / `20:9` / `21:9` / `32:9` |
| Escalado de la frecuencia del procesador (overclock) | `50%` / `60%` / `70%` / `80%` / `90%` / `100% (Nativo) ✅` / `110%` / `120%` / `130%` / `140%` / `150%` / `160%` / `170%` / `180%` / `190%` / `200%` / `210%` / `220%` / `230%` / `240%` / `250%` / `260%` / `270%` / `280%` / `290%` / `300%` / `310%` / `320%` / `330%` / `340%` / `350%` / `360%` / `370%` / `380%` / `390%` / `400%` / `410%` / `420%` / `430%` / `440%` / `450%` / `460%` / `470%` / `480%` / `490%` / `500%` / `510%` / `520%` / `530%` / `540%` / `550%` / `560%` / `570%` / `580%` / `590%` / `600%` / `610%` / `620%` / `630%` / `640%` / `650%` / `660%` / `670%` / `680%` / `690%` / `700%` / `710%` / `720%` / `730%` / `740%` / `750%` | `beetle_psx_cpu_freq_scale` | `50%` / `60%` / `70%` / `80%` / `90%` / `100%(nativo)` / `110%` / `120%` / `130%` / `140%` / `150%` / `160%` / `170%` / `180%` / `190%` / `200%` / `210%` / `220%` / `230%` / `240%` / `250%` / `260%` / `270%` / `280%` / `290%` / `300%` / `310%` / `320%` / `330%` / `340%` / `350%` / `360%` / `370%` / `380%` / `390%` / `400%` / `410%` / `420%` / `430%` / `440%` / `450%` / `460%` / `470%` / `480%` / `490%` / `500%` / `510%` / `520%` / `530%` / `540%` / `550%` / `560%` / `570%` / `580%` / `590%` / `600%` / `610%` / `620%` / `630%` / `640%` / `650%` / `660%` / `670%` / `680%` / `690%` / `700%` / `710%` / `720%` / `730%` / `740%` / `750%` |
| Overclocking del GTE | `Desactivado` ✅ / `Activado` | `beetle_psx_gte_overclock` | `disabled` / `enabled` |
| Ignorar el BIOS | `Desactivado` ✅ / `Activado` | `beetle_psx_skip_bios` | `disabled` / `enabled` |
| Sobrecargar el BIOS (Reinicio requerido) | `Desactivado` ✅ / `BIOS PS1 de la PSP` / `BIOS PS1 de la PS3` | `beetle_psx_override_bios` | `disabled` / `psxonpsp` / `ps1_rom` |
| Método de renderizado (Reinicio requerido) | `Material (por defecto)` ✅ / `Material (OpenGL)` / `Material (Vulkan)` / `Lógico` | `beetle_psx_renderer` | `hardware` / `hardware_gl` / `hardware_vk` / `software` |
| Tampon de imágenes lógico | `Activado` ✅ / `Desactivado` | `beetle_psx_renderer` | `enabled` / `disabled` |
| Invalidation del código Dynarec | `Completo` ✅ / `DMA únicamente (método más rápido)` | `beetle_psx_dynarec_invalidate` | `full` / `dma` |
| Ciclos de eventos Dynarec DMA/procesador gráfico/MEC/Timer | `128 (por defecto)` ✅ / `256` / `384` / `512` / `640` / `768` / `896` / `1024` / `1152` / `1280` / `1408` / `1536` / `1664` / `1792` / `1920` / `2048` | `beetle_psx_dynarec_eventcycles` | `128` / `256` / `384` / `512` / `640` / `768` / `896` / `1024` / `1152` / `1280` / `1408` / `1536` / `1664` / `1792` / `1920` / `2048` |
| Muestras SPU Dynarec | `1 (por defecto)` ✅ / `4` / `16` | `beetle_psx_dynarec_spu_samples` | `1` / `4` / `16` |
| Cadencia de las i/s comunes por el núcleo | `Desactivado` ✅ / `Forzar el débit entrelazado` / `Autorizar el basamento automático` | `beetle_psx_core_timing_fps` | `force_progressive` / `force_interlaced` / `auto_toggle` |
| Sobrecargar la cadencia de vídeo PAL (europea) | `Desactivado` ✅ / `Activado` | `beetle_psx_pal_video_timing_override` | `disabled` / `enabled` |
| Recorte suplementario | `Desactivado` ✅ / `Activado` | `beetle_psx_crop_overscan` | `enabled` / `disabled` |
| Recorte suplementario | `Desactivado` ✅ / `1px` / `2px` / `3px` / `4px` / `5px` / `6px` / `7px` / `8px` / `9px` / `10px` / `11px` / `12px` / `13px` / `14px` / `15px` / `16px` / `17px` / `18px` / `19px` / `20px` | `psx_image_crop` | `disabled` / `1px` / `2px` / `3px` / `4px` / `5px` / `6px` / `7px` / `8px` / `9px` / `10px` / `11px` / `12px` / `13px` / `14px` / `15px` / `16px` / `17px` / `18px` / `19px` / `20px` |
| Desplazamiento de la imagen recortada | `-12px` / `-11px` / `-10px` / `-9px` / `-8px` / `-7px` / `-6px` / `-5px` / `-4px` / `-3px` / `-2px` / `-1px` / `0 (por defecto)` ✅ / `+1px` / `+2px` / `+3px` / `+4px` / `+5px` / `+6px` / `+7px` / `+8px` / `+9px` / `+10px` / `+11px` / `+12px` | `psx_image_offset` | `-12px` / `-11px` / `-10px` / `-9px` / `-8px` / `-7px` / `-6px` / `-5px` / `-4px` / `-3px` / `-2px` / `-1px` / `disabled` / `+1px` / `+2px` / `+3px` / `+4px` / `+5px` / `+6px` / `+7px` / `+8px` / `+9px` / `+10px` / `+11px` / `+12px` |
| Desplazamiento horizontal de la imagen (ciclos del procesador gráfico) | `-40` / `-39` / `-38` / `-37` / `-36` / `-35` / `-34` / `-33` / `-32` / `-31` / `-30` / `-29` / `-28` / `-27` / `-26` / `-25` / `-24` / `-23` / `-22` / `-21` / `-20` / `-19` / `-18` / `-17` / `-16` / `-15` / `-14` / `-13` / `-12` / `-11` / `-10` / `-9` / `-8` / `-7` / `-6` / `-5` / `-4` / `-3` / `-2` / `-1` / `0` ✅ / `+1` / `+2` / `+3` / `+4` / `+5` / `+6` / `+7` / `+8` / `+9` / `+10` / `+11` / `+12` / `+13` / `+14` / `+15` / `+16` / `+17` / `+18` / `+19` / `+20` / `+21` / `+22` / `+23` / `+24` / `+25` / `+26` / `+27` / `+28` / `+29` / `+30` / `+31` / `+32` / `+33` / `+34` / `+35` / `+36` / `+37` / `+38` / `+39` / `+40` | `beetle_psx_image_offset_cycles` | `-40` / `-39` / `-38` / `-37` / `-36` / `-35` / `-34` / `-33` / `-32` / `-31` / `-30` / `-29` / `-28` / `-27` / `-26` / `-25` / `-24` / `-23` / `-22` / `-21` / `-20` / `-19` / `-18` / `-17` / `-16` / `-15` / `-14` / `-13` / `-12` / `-11` / `-10` / `-9` / `-8` / `-7` / `-6` / `-5` / `-4` / `-3` / `-2` / `-1` / `0` / `+1` / `+2` / `+3` / `+4` / `+5` / `+6` / `+7` / `+8` / `+9` / `+10` / `+11` / `+12` / `+13` / `+14` / `+15` / `+16` / `+17` / `+18` / `+19` / `+20` / `+21` / `+22` / `+23` / `+24` / `+25` / `+26` / `+27` / `+28` / `+29` / `+30` / `+31` / `+32` / `+33` / `+34` / `+35` / `+36` / `+37` / `+38` / `+39` / `+40` |
| Overclocking del procesador gráfico interno | `1x (nativo)` ✅ / `2x` / `4x` / `8x` / `16x` | `beetle_psx_gpu_overclock` | `1x(nativo)` / `2x` / `4x` / `8x` / `16x` |
| Relación de aspecto del córreo | `Corregido` ✅ / `No corregido` / `Forzar 4:3` / `Forzar NTSC` | `beetle_psx_aspect_ratio` | `corrected` / `uncorrected` / `4:3` / `ntsc` |
| Línea de balayage inicial - NTSC | `0` ✅ / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `21` / `22` / `23` / `24` / `25` / `26` / `27` / `28` / `29` / `30` / `31` / `32` / `33` / `34` / `35` / `36` / `37` / `38` / `39` / `40` | `psx_initial_scanlines` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `21` / `22` / `23` / `24` / `25` / `26` / `27` / `28` / `29` / `30` / `31` / `32` / `33` / `34` / `35` / `36` / `37` / `38` / `39` / `40` |
| Última línea de balayage - NTSC | `210` / `211` / `212` / `213` / `214` / `215` / `216` / `217` / `218` / `219` / `220` / `221` / `222` / `223` / `224` / `225` / `226` / `227` / `228` / `229` / `230` / `231` / `232` / `233` / `234` / `235` / `236` / `237` / `238` / `239 (por defecto)` ✅ | `psx_last_scanlines` | `210` / `211` / `212` / `213` / `214` / `215` / `216` / `217` / `218` / `219` / `220` / `221` / `222` / `223` / `224` / `225` / `226` / `227` / `228` / `229` / `230` / `231` / `232` / `233` / `234` / `235` / `236` / `237` / `238` / `239` |
| Línea de balayage inicial - PAL | `0` ✅ / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `21` / `22` / `23` / `24` / `25` / `26` / `27` / `28` / `29` / `30` / `31` / `32` / `33` / `34` / `35` / `36` / `37` / `38` / `39` / `40` | `beetle_psx_initial_scanline_pal` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `21` / `22` / `23` / `24` / `25` / `26` / `27` / `28` / `29` / `30` / `31` / `32` / `33` / `34` / `35` / `36` / `37` / `38` / `39` / `40` |
| Última línea de balayage - PAL | `230` / `231` / `232` / `233` / `234` / `235` / `236` / `237` / `238` / `239` / `240` / `241` / `242` / `243` / `244` / `245` / `246` / `247` / `248` / `249` / `250` / `251` / `252` / `253` / `254` / `255` / `256` / `257` / `258` / `259` / `260` / `261` / `262` / `263` / `264` / `265` / `266` / `267` / `268` / `269` / `270` / `271` / `272` / `273` / `274` / `275` / `276` / `277` / `278` / `279` / `280` / `281` / `282` / `283` / `284` / `285` / `286` / `287 (por defecto)` ✅ | `beetle_psx_last_scanline_pal` | `230` / `231` / `232` / `233` / `234` / `235` / `236` / `237` / `238` / `239` / `240` / `241` / `242` / `243` / `244` / `245` / `246` / `247` / `248` / `249` / `250` / `251` / `252` / `253` / `254` / `255` / `256` / `257` / `258` / `259` / `260` / `261` / `262` / `263` / `264` / `265` / `266` / `267` / `268` / `269` / `270` / `271` / `272` / `273` / `274` / `275` / `276` / `277` / `278` / `279` / `280` / `281` / `282` / `283` / `284` / `285` / `286` / `287` |
| Método de acceso al CD (Reinicio requerido) | `Sincrónico` ✅ / `Asincrónico` / `Pré-caché` | `beetle_psx_cd_access_method` | `sync` / `async` / `precache` |
| Velocidad de carga del CD | `2x (nativo)` ✅ / `4x` / `6x` / `8x` / `10x` / `12x` / `14x` | `beetle_psx_cd_fastload` | `2x(nativo)` / `4x` / `6x` / `8x` / `10x` / `12x` / `14x` |

## ![](/emulators/external-links.png) Enlaces externos

* **Código fuente utilizado** : [https://github.com/libretro/beetle-psx-libretro/](https://github.com/libretro/beetle-psx-libretro/)
* **Documentación Libretro** : [https://docs.libretro.com/library/beetle_psx/](https://docs.libretro.com/library/beetle_psx/)
