---
title: Acesso
description: 
published: true
date: 2021-12-23T21:07:47.295Z
tags: acesso
editor: markdown
dateCreated: 2021-12-23T21:07:47.295Z
---

Você aprenderá como pode acessar o conteúdo do seu Recalbox.

Aqui estão os tutoriais disponíveis:

[Comandos de pastas e SSH](folders-and-ssh-commands)
[Acesso à rede via Cyberduck](network-access-cyberduck)
[Acesso à rede via MobaXTerm](network-access-mobaxterm)
[Acesso à rede via WinSCP](network-access-winscp)
[Recalbox Manager - Web Interface](recalbox-manager-web-interface)
[Remontar partição com acesso de gravação](remount-partition-with-write-access)
[Acesso root via Terminal CLI](root-access-terminal-cli)
