---
title: Libretro-cdi2015
description: 
published: true
date: 2024-06-22T23:42:36.505Z
tags: libretro, cdi, cdi2015, 8.1+
editor: markdown
dateCreated: 2022-02-03T17:06:32.246Z
---

**Libretro-cdi2015** est un core dérivé de MAME2015.

## ![](/emulators/license.svg) Licence

Ce core est sous licence **MAME non-commerciale**.

## ![](/emulators/compatibility.png) Compatibilité

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ❌ | ❌ | ✅ | ✅ | ❌ | ❌ | ✅ | ✅ |

## ![](/emulators/features.png) Fonctionnalités

| Fonctionnalité | Supporté |
| :---: | :---: |
| Redémarrage | ✔ |
| Captures d'écran | ✔ |
| Sauvegardes | ✔ |
| Sauvegardes instantanées | ✔ |
| Options du core | ✔ |

## ![](/emulators/bios.svg) BIOS

### Liste des bios obligatoires

| Nom de fichier | Description | MD5 | Fourni |
| :---: | :---: | :---: | :---: |
| cdimono1.zip | CD-i (Mono-I) (PAL) | ce590c4787460161359f2b680136cb60 bbe6636ac11d3744f0710e8e820187e7 | ❌ |

### Emplacement

Placez les bios comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 cdi
┃ ┃ ┃ ┃ ┣ 🗒 **cdimono1.zip**

## ![](/emulators/isos.svg) Isos

### Extensions supportées

Les isos doivent avoir les extensions suivantes :

* .chd
* .cmd

### Emplacement

Placez les isos comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 cdi
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.chd**

>Les isos au format **Redump** sont vivement conseillées.
{.is-success}

>Pour plus d'information sur les roms, rendez-vous sur [ce tutoriel](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuration avancée de l'émulateur

>Pour pouvoir conserver vos configurations personnalisées lors d'une mise à jour, nous vous conseillons d'utiliser notre fonctionnalité [Surcharges de configuration](./../../../advanced-usage/configuration-override).
{.is-info}

### Accéder aux options

Vous pouvez configurer diverses options de deux façons différentes.

* Via le menu RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via le fichier `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Options du core

| Options | Valeur des options | Variable (pour surcharge) | Valeurs des variables |
| --- | --- | --- | --- |
| Read configuration | `Désactivé` ✅ / `Activé` | `cdi2015_read_config` | `disabled` / `enabled` |
| Auto save/load states | `Désactivé` ✅ / `Activé` | `cdi2015_auto_save` | `disabled` / `enabled` |
| XY device (Restart) | `none` ✅ / `lightgun` / `mouse` | `cdi2015_mouse_mode` | `none` / `lightgun` / `mouse` |
| Enable throttle | `Désactivé` ✅ / `Activé` | `cdi2015_throttle` | `disabled` / `enabled` |
| Enable cheats | `Désactivé` ✅ / `Activé` | `cdi2015_cheats_enable` | `disabled` / `enabled` |
| Hide nag screen | `Désactivé` ✅ / `Activé` | `cdi2015_hide_nagscreen` | `disabled` / `enabled` |
| Hide gameinfo screen | `Désactivé` ✅ / `Activé` | `cdi2015_hide_infoscreen` | `disabled` / `enabled` |
| Hide warnings screen | `Désactivé` ✅ / `Activé` | `cdi2015_hide_warnings` | `disabled` / `enabled` |
| Alternate render method | `Désactivé` ✅ / `Activé` | `cdi2015_alternate_renderer` | `disabled` / `enabled` |

## ![](/emulators/external-links.png) Liens externes

* **Code source utilisé** : [https://github.com/libretro/mame2015-libretro/](https://github.com/libretro/mame2015-libretro/)