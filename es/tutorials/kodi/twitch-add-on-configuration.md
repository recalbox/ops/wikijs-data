---
title: Configurar la extensión Twitch
description: ¡Cómo ver los vídeos de Twich sin problemas!
published: true
date: 2025-02-22T23:57:25.368Z
tags: kodi, twitch, configuración, extensiones
editor: markdown
dateCreated: 2023-09-15T07:34:44.847Z
---

## Introducción

La extensión de Twitch ya está preinstalada en Kodi. Sin embargo, necesitas configurarla mediante identificación para acceder a vuestras informaciones (cuentas followed, etc.).

## Procedimiento

### Requisitos previos

- Hay que disponer de una cuenta de Twitch.

### Configuración

>Los siguientes pasos pueden diferir si estás iniciando Kodi o Twitch por primera vez.
>{.is-info}

1. Iniciad Kodi.

2. Id a `Extensiones` > `Twitch`.

3. Una ventana os indica que váis a generar un token OAuth.

4. Confirmad el mensaje et id a `Configuración` en la parte inferior.

5. En los ajustes, elegid `Connection` a la izquierda y en el centro, elegid la opción `Get OAuth token`.

6. Tras unos segundos, una ventana os pedira visitar una URL. Como es posible que ya no funcione, tendrá que visitar https://twitchaddon.page.link/1Sk5.

7. En esta dirección, obtendréis un token de OAuth automáticamente después de abrir una sesión en Twitch.

8. De vuelta en Kodi, seleccionad `Oauth Token`.

9. Copiad el token OAuth y validad.

10. Confirmad la ventana de configuración.

11. Salid de la extensión y volved a entrar. Si habéis copiado el token OAuth correctamente, ¡habréis iniciado sesión y podréis disfrutar de Twitch en Kodi sin publicidad!

