---
title: Libretro EmuSCV
description: 
published: true
date: 2024-07-24T18:58:09.337Z
tags: libretro, 7.2+, super cassette vision, emuscv
editor: markdown
dateCreated: 2021-05-21T08:27:04.688Z
---



## ![](/emulators/license.svg) Licence

Ce core est sous licence [**GPLv3**](https://gitlab.com/MaaaX-EmuSCV/libretro-emuscv/-/blob/master/licence.txt).

## ![](/emulators/compatibility.png) Compatibilité

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Fonctionnalités

| Fonctionnalité | Supporté |
| :---: | :---: |
| Redémarrage | ✔ |
| Captures d'écran | ✔ |
| Sauvegardes | - |
| Sauvegardes instantanées | - |
| Rembobinage | - |
| Contrôles | ✔ |

## ![](/emulators/bios.svg) BIOS

### Liste des bios obligatoires

| Nom de fichier | Description | MD5 | Fourni |
| :---: | :---: | :---: | :---: |
| upd7801g.s01 | BIOS - Requis | 635a978fd40db9a18ee44eff449fc126 | ❌ |

### Emplacement

Placez les BIOS comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 scv
┃ ┃ ┃ ┃ ┣ 🗒 **upd7801g.s01**

## ![](/emulators/roms.png) Roms

### Extensions supportées

Les roms doivent avoir les extensions suivantes :

* .0
* .bin
* .cart
* .rom
* .zip

Ce système supporte les roms compressées au format .zip. Attention toutefois, il ne s'agit que d'une archive.

Les fichiers contenus dans les .zip doivent correspondre aux extensions citées précédemment.
De plus, chaque fichier .zip ne doit contenir qu'une seule rom compressée.

### Emplacement

Placez les roms comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 scv
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.zip**

La particularité est que si on charge une ROM en `.bin`, `.rom` ou `.0` (`.1`, `.2`, etc.) connu, ça crée un fichier ROM au format `.CART` qui indique à l'émulateur le bon mappage des données (la cartouche pouvant contenir plusieurs ROMs, éventuellement de la RAM, voire de la SRAM sauvegardée par une pile). Pour les jeux où on pouvait sauvegarder, ça crée aussi un fichier `.SAVE` qui contient les données de la SRAM (sauvegarde quand on quitte).

Le `.CART` est créé dans le repertoire `roms`.

S'il ne peut pas créer le `.CART`, ça ne charge pas le jeux, ça démarre sur l'écran de test de la console comme quand la console n'arrive pas à lire une cartouche.
Je vais modifier le code pour que ça charge quand même et voir comment je peux mettre ça dans le répertoire des sauvegardes.

>Les roms au format **No-Intro** sont vivement conseillées.
{.is-success}

>Pour plus d'information sur les roms, rendez-vous sur [ce tutoriel](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuration avancée de l'émulateur

>Pour pouvoir conserver vos configurations personnalisées lors d'une mise à jour, nous vous conseillons d'utiliser notre fonctionnalité [Surcharges de configuration](./../../../advanced-usage/configuration-override).
{.is-info}

### Accéder aux options

Vous pouvez configurer diverses options de deux façons différentes.

* Via le menu RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via le fichier `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Options du core

| Options | Valeurs des options | Variable (pour surcharge) | Valeurs des variables |
| --- | --- | --- | --- |
| CONSOLE | `AUTO` ✅ / `EPOCH` / `YENO` / `EPOCHLADY` | `emuscv_console` | `AUTO` / `EPOCH` / `YENO` / `EPOCHLADY` |
| DISPLAY | `AUTO` ✅ / `EMUSCV` / `EPOCH` / `YENO` | `emuscv_display` | `AUTO` / `EMUSCV` / `EPOCH` / `YENO` |
| PIXELASPECT | `AUTO` ✅ / `RECTANGULAR` / `SQUARE` | `emuscv_pixelaspect` | `AUTO` / `RECTANGULAR` / `SQUARE` |
| RESOLUTION | `AUTO` ✅ / `LOW` / `MEDIUM` / `HIGH` | `emuscv_resolution` | `AUTO` / `LOW` / `MEDIUM` / `HIGH` |
| PALETTE | `AUTO` ✅ / `STANDARD` / `OLDNTSC` | `emuscv_palette` | `AUTO` / `STANDARD` / `OLDNTSC` |
| FPS | `AUTO` ✅ / `EPOCH60` / `YENO50` | `emuscv_fps` | `AUTO` / `EPOCH60` / `YENO50` |
| DISPLAYFULLMEMORY | `AUTO` ✅ / `YES` / `NO` | `emuscv_displayfullmemory` | `AUTO` / `YES` / `NO` |
| DISPLAYINPUTS | `AUTO` ✅ / `YES` / `NO` | `emuscv_displayinputs` | `AUTO` / `YES` / `NO` |
| LANGAGE | `AUTO` ✅ / `JP` / `FR` / `EN` | `emuscv_langage` | `AUTO` / `JP` / `FR` / `EN` |
| BIOS | `AUTO` ✅ / `YES` / `NO` | `emuscv_checkbios` | `AUTO` / `YES` / `NO` |

## ![](/emulators/external-links.png) Liens externes

* **Code source utilisé** : [https://gitlab.com/MaaaX-EmuSCV/libretro-emuscv/](https://gitlab.com/MaaaX-EmuSCV/libretro-emuscv/)
* **Documentation Libretro** : [https://docs.libretro.com/library/emuscv/](https://docs.libretro.com/library/emuscv/)