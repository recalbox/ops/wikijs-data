---
title: Tic-80
description: Libretro-tic80
published: true
date: 2021-10-23T14:58:32.077Z
tags: tic-80, fantasy
editor: markdown
dateCreated: 2021-10-23T14:58:32.077Z
---

![](/emulators/fantasy/tic80.svg){.align-center}

## Ficha técnica

* **Fabricante**: Vadim Grigoruk
* **Ano de lançamento**: 2017
* **Recursos incluídos**: editor de código, editor de sprite, editor de mapa de blocos, editor de SFX
* **Resolução**: 240x136 pixels, paleta de 16 cores
* **Sprites**: 256 sprites de primeiro plano 8x8, 256 blocos de fundo 8x8
* **Som**: 4 canais (com envelopes waveform modificáveis)

## Apresentação

O TIC-80 é um fantástico computador de CÓDIGO ABERTO e GRATUITO para criar, jogar e compartilhar pequenos jogos.

Com o TIC-80, você tem ferramentas de desenvolvimento integradas: código, sprites, mapas, editores de som e linha de comando, o que é suficiente para criar um minijogo retrô!

Os jogos são agrupados em um arquivo de cartucho, que pode ser facilmente distribuído. O TIC-80 funciona em todas as plataformas populares. Isso significa que seu cartucho pode ser lido em qualquer dispositivo.

Para criar um jogo de estilo retrô, todo o processo de criação e execução ocorre dentro de certos limites técnicos: exibição de 240x136 pixels, paleta de 16 cores, 256 sprites de 8x8 cores, som de 4 canais, etc.

## Emuladores

[Libretro Tic80](libretro-tic80)