---
title: Aumentar la corriente disponible en los puertos USB
description: 
published: true
date: 2023-09-14T07:11:56.276Z
tags: usb, aumentar, corriente
editor: markdown
dateCreated: 2023-09-14T07:11:56.276Z
---

>Esta sección está reservada a los Raspberry Pi 2.
{.is-danger}

Con la configuración por defecto, el Raspberry Pi 2 limita la **corriente de los puertos USB a 600mA para todos juntos**. La modificación que describimos a continuación va a aumentar este límite hasta 1200mA.

>* Esta modificación no es exclusiva de Recalbox.
>* La modificación es reversible.
>* Se recomienda utilizar una fuente de alimentación de 2000mA, poco importa el uso que le vais a dar al sistema.
>* La corriente máxima admisible por el Raspberry Pi 2 es de 2000mA.
{.is-warning}

## ¿Por qué debo aplicar esta modificacion?

Si vais a conectar varios periféricos USB a vuestro Raspberry Pi sin utilizar un hub con alimentación externa, es necesario aumentar los 600mA para que todos los periféricos dispongan de corriente suficiente. Por ejemplo si pensáis enchufar un disco dur de 2.5 pulgadas USB auto-alimentado, ya que estos discos pueden consumir hasta 500mA.

## Ejemplos donde aumentar este límite es necesario

* Utilización de un disco SSD USB auto-alimentado para almacenar las roms, las bios y las grabaciones.
* Llave USB wifi.
* Recargar / utilizar varios mandos USB al mismo tiempo.

## Procedimiento a seguir

* Abrid el fichero [recalbox-user-config.txt](./../../../tutorials/system/modification/configtxt-configuration)
* Dentro del fichero, buscad la presencia de una línea de este tipo `max_usb_current=...`
* Añadid la línea `max_usb_current=1` o, si ya existe, poned su valor a 1.
* Grabad el fichero `recalbox-user-config.txt`.
* Reiniciad vuestro Raspberry Pi 2.

>Este truco funciona también en el Rasberry Pi 3, pero la línea `max_usb_current=...` no existe dentro del fichero `recalbox-user-config.txt`. Hay que añadirla a la mano.
{.is-info}