---
title: Connectez votre Recalbox à un écran DVI
description: 
published: true
date: 2021-09-04T22:23:05.256Z
tags: dvi, écran
editor: markdown
dateCreated: 2021-05-21T08:43:53.287Z
---

Vous devez connecter votre Recalbox sur un écran DVI mais vous obtenez un écran noir ou une ligne rose sur le bord de l'écran ?

* Rendez la [partition de démarrage](./../../../tutorials/system/access/remount-partition-with-write-access) possible en écriture.
* Modifiez le [fichier recalbox-user-config.txt](./../../../tutorials/system/modification/configtxt-configuration), et commentez la ligne :

```ini
hdmi_drive=2
```

* Avec un # devant :

```ini
#hdmi_drive=2
```