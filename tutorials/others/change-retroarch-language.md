---
title: Change RetroArch language
description: English is great, but not for everybody!
published: true
date: 2021-11-17T16:31:12.336Z
tags: retroarch, language
editor: markdown
dateCreated: 2021-08-06T16:22:19.628Z
---

## Explanations

Did you know that you can change the language displayed in the RetroArch options? As example, you have French, of course, but also other languages.

## Setting up

* Launch a game using a Libretro core.
* Once the game is running, go to the RetroArch options with the `HOTKEY` + `B` buttons (see [this page](./../../basic-usage/getting-started/special-commands/in-game) to find out the mapping for your controller).

![](/tutorials/others/change-retroarch-language/ralanguage1.png){.full-width}

* When you have opened the RetroArch options, following the buttons displayed at the bottom right, press "Back" and go left once.

![](/tutorials/others/change-retroarch-language/ralanguage2.png){.full-width}

* When the cursor is on "Main Menu", scroll down to "Settings" and confirm.

![](/tutorials/others/change-retroarch-language/ralanguage3.png){.full-width}

* In the menu on the right, scroll down to "User" and validate.

![](/tutorials/others/change-retroarch-language/ralanguage4.png){.full-width}

* In the user options, select "Language" and change it to the language of your choice.

![](/tutorials/others/change-retroarch-language/ralanguage5.png){.full-width}

* RetroArch is now in the language of your choice.

![](/tutorials/others/change-retroarch-language/ralanguage6.png){.full-width}

* Press once « Retour », go up to the menu then go to « Configuration ».

![](/tutorials/others/change-retroarch-language/ralanguage7.png){.full-width}

* On the option « Sauvegarder la configuration en quittant », choose « Activé ».

![](/tutorials/others/change-retroarch-language/ralanguage8.png){.full-width}

* Quit completely the game and the saved language will be selected et each game.