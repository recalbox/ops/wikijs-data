---
title: Game & Watch
description: 
published: true
date: 2021-08-09T07:40:35.371Z
tags: nintendo, game, handhelds, game&watch, watch
editor: markdown
dateCreated: 2021-06-29T18:28:08.885Z
---

![](/emulators/handheld/gameandwatch.svg){.align-center}

## Technical data

* **Manufacturer**: Nintendo
* **Year of release**: 1980
* **Units sold**: 43,4 million
* **Best-selling game**: Donkey Kong

## Presentation

The **Game and Watch** are pocket electronic games, ancestor of the portable game console , produced by the Nintendo company from 1980 to 1991, they also act as watches and alarm clocks. Sold 43.4 million copies worldwide, these 59 games represent one of the biggest successes of the Kyōto firm.

The Game and Watch were imagined by Gunpei Yokoi, the creator of the Game Boy. He got the idea for Game and Watch during a shinkansen trip. In a seat next to his, a businessman was playing with his calculator to pass the time. He wanted to develop a toy that Japanese adults could use discreetly on public transport or during business trips, for example...

The very first Game and Watch was born on April 28, 1980, under the name of Ball. It was an immediate success, but Nintendo soon realized that the buyers were mainly children, which encouraged the firm to release new, more colorful games for children. This is how the most famous characters of the brand were introduced: Mario, Donkey Kong or Link.

The Game and Watch only allowed a limited number of games to be played per model (this was due to the liquid crystal screen of the machine, where the images of the game were pre-printed and successively illuminated during the game).

There are a total of 60 Game and Watch in ten series.

## Emulators

[Libretro GW](libretro-gw)