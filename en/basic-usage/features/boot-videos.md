---
title: Boot videos
description: 
published: true
date: 2021-08-08T19:01:04.040Z
tags: videos, boot
editor: markdown
dateCreated: 2021-06-29T11:50:18.037Z
---

In Recalbox, you have the possibility to add your own personal videos to be played at startup.

We will see:

* How to add videos
* How to set up their playback

## Add new boot videos

The addition of boot videos is done via the directory `/recalbox/share/bootvideos/`.

>Adding videos can only be done via the network!
>
>The totality of your videos must not exceed 500 Mb.
{.is-warning}

To connect via the network to your Recalbox, you have several possibilities:

* Via [WinSCP](./../../tutorials/system/access/network-access-winscp)
* Via [Cyberduck](./../../tutorials/system/access/network-access-cyberduck)
* Via [MobaXTerm](./../../tutorials/system/access/network-access-mobaxterm)
* Via your file explorer. To do this, you will need the [IP address](./../../tutorials/network/ip/discover-recalbox-ip) of your Recalbox and insert the address `\\your-ip-address\` in your File Explorer.

## Settings related to the boot videos

### Playback frequency

You can set the frequency at which you can view your boot video(s) via the `system.splash.select` parameter in the [recalbox.conf](./../getting-started/recalboxconf-file) file.

Here are the available options:

**all** : the system chooses a video provided by the original system or one of your videos
**custom** : the system chooses one of your videos
**recalbox** : the system chooses a video provided by origin

### Playback time

You can set the playback time for all videos (original or your own) via the `system.splash.length` parameter in the [recalbox.conf](./../getting-started/recalboxconf-file) file.

Here are the available options:

* **0**: video playback will be stopped when EmulationStation is ready
* **1**: the video will be played to the end before EmulationStation starts
* **Any value greater than 0**: video playback will last for the specified number of seconds before EmulationStation starts