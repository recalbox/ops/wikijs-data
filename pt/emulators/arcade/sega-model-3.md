---
title: Sega Model 3
description: 
published: true
date: 2021-10-14T23:24:12.917Z
tags: arcade, model-3
editor: markdown
dateCreated: 2021-10-14T23:24:12.917Z
---

![](/emulators/arcade/model3.svg){.align-center}

## Ficha técnica

* **Processador**: IBM 32bit RISC PowerPC 603ev 
  * 66 MHz (Versão 1.0)
  * 100 MHz (Versão 1.5)
  * 166 MHz (Versão 2.0 e 2.1)
* **Processador gráfico**: 2 × Lockheed Martin Real3D/Pro-1000
* **Renderização**: 60 milhões de pixels
* **Resolução**: 496 x 384
* **Processador de som**: Motorola 68EC000 clock a 12 MH, 2 × Yamaha SCSP/YM-292F 128 passos DSP
* **Interface MIDI**
  * 64 vozes, 4 canais, máximo 16,5 MB de ROM
  * 64 canais PCM
* **RAM**: 1 Mb
* **Placa de som opcional** :
  * **DSB1:**
    * **Processador de som**: Zilog Z80
    * **Chip de áudio** : NEC uD65654GF102
  * **DSB2:**
    * **CPU**: Motorola 68000
    * **Chip de áudio** : NEC uD65654GF102

## Apresentação

Com o **Model 3***, a Sega decidiu projetar um novo sistema a partir do zero. Mais uma vez, eles escolheram a empresa com a qual haviam estabelecido um bom relacionamento, agora denominada Real3D divisão da Lockheed Martin.

Eles foram trazidos para ajudar a desenvolver o novíssimo subsistema gráfico, que se tornou o duplo Chipset Real3D/Pro-1000, surpreendentemente poderoso para seu tempo.

## Emuladores

[Supermodel](supermodel)