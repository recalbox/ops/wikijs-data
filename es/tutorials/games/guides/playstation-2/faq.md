---
title: FAQ
description: 
published: true
date: 2023-09-21T13:07:59.527Z
tags: faq
editor: markdown
dateCreated: 2023-09-21T13:07:59.527Z
---

## ¿Existen una serie de combinaciones de teclas para PCSX2?

Sí, aquí tenéis una lista de estas combinaciones:

- HotKey + Arriba/Abajo: seleccionar ubicación de guardado
- HotKey + X/Y: carga/guardado rápido
- HotKey + Izquierda: cambia al modo de cámara lenta
- HotKey + Derecha: cambia a modo rápido
- HotKey + L1: cambiar la relación de pantalla (4:3, 16:9, pantalla completa ampliada)
- HotKey + R1: Captura de pantalla
- Select + Start: Salir de PCSX2.

## ¿Puedo visualizar el contador de FPS durante el juego?

Sí, tenéis que hacer lo siguiente:

- Iniciad cualquier juego.
- Cuando el juego se haya iniciado, pulsad Esc en vuestro teclado y accederéis a la interfaz gráfica de PCSX2.
- Pulsad en `Config` > `Graphics settings`.
- En la ventana, pulsad en `OSD` > `Enable monitor` y pulsad `OK`.
- Salid del emulador PCSX2.
- Volved a iniciad el juego.