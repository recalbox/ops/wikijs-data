---
title: GameCube / Wii
description: 
published: true
date: 2021-09-05T12:58:54.753Z
tags: nintendo, gamecube, wii, gc
editor: markdown
dateCreated: 2021-05-21T08:37:02.703Z
---

Vous allez pouvoir trouver ici des informations pour mieux jouer à vos jeux GameCube et Wii.

Voici les tutoriels disponibles :

[Définir la position de la Dolphin Bar](define-dolphinbar-position)
[Jouer avec de véritables Wiimotes](use-original-wiimotes-controllers)
[Jouer avec de véritables manettes Gamecube](use-original-gamecube-controllers)
[Jouer à plusieurs sur Wii](multiplayer-games-on-wii)
[Mettre les jeux Wii/GameCube multi-langues avec la langue de votre choix](set-multilingual-wii-gamecube-games-in-your-language)
[Packs de textures personnalisées](custom-hires-textures-packs)
[Utiliser les Wiimotes comme manettes](use-wiimotes-as-controllers)