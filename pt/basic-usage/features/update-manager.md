---
title: Gerenciador de atualização
description: 
published: true
date: 2021-07-26T23:12:50.707Z
tags: 
editor: markdown
dateCreated: 2021-07-26T23:12:50.707Z
---

![](/basic-usage/features/update.png){.full-width}

## Uma nova maneira de fazer atualizações!

Desde o Recalbox 7, o sistema de atualização foi completamente revisado e não funciona a partir das versões mais antigas.

* O novo sistema de atualização é muito mais rápido:
  * _**5 à 10min no máximo para uma atualização**_ (excluindo o tempo de download, claro). 
*  O novo sistema de atualização é muito mais robusto:
	* Acabaram-se as atualizações que forçam você a começar tudo de novo (mesmo que fosse raro, felizmente!) 
* Novas atualizações serão gerenciadas pelo EmulationStation: visualização do progresso, tratamento de erros, etc.
* Você pode atualizar offline de uma maneira muito simples: copiando a imagem da nova versão em uma pasta do cartão SD acessível por todos os sistemas operacionais: Windows, Linux e macOS! Uma pequena reinicialização, o sistema detecta a imagem, realiza a atualização e está pronto!
* Agora será possível colocar seus próprios vídeos de inicialização, em uma pasta facilmente acessível na rede (em `/share/bootvideos/`) e escolher se o player deve reproduzir vídeos apenas os do Recalbox, apenas os seus, ou entre tudo o que ele tem à disposição.
  * O que isso tem a ver com as atualizações, você pergunta? Nada, é só que esta nova possibilidade está ligada à nova estrutura do Recalbox para facilitar as atualizações! 
* Se você não gosta daqueles que roubam o Recalbox e dos seus clientes, _**as notificações de atualização não podem mais ser desativadas**_ no EmulationStation: somente a notificação popup intrusiva pode ser desativada. Mas você ainda verá um popup não-intrusivo notificando-o de que uma atualização está disponível.

## Como funciona

A atualização pode ser feita de duas maneiras: online ou offline.

## {.tabset}

### Método on-line

O método on-line é o mais fácil:

* No EmulationStation, pressione o botão `START` em seu controle.
* Vá para `ATUALIZAÇÕES`.
* Se uma atualização estiver disponível, você verá a linha `ATUALIZAÇÃO DISPONÍVEL` com `SIM` à direita. Se esse é o caso, vá para a linha `INICIAR ATUALIZAÇÃO` e confirme.
* A atualização começará a ser baixada.
* Uma vez baixada, seu Recalbox será automaticamente reiniciado e a atualização será realizada.

### Método off-line

Esta forma de atualização é um pouco mais complexa de se realizar.

* Vá para [esta página](https://download.recalbox.com/pt/#step3allimages) onde você encontrará todas as imagens da última versão do Recalbox.
* Baixe a imagem correspondente à sua arquitetura (RPi/Odroid/PC) e o arquivo `.SHA1` correspondente.
* Uma vez baixado, há 2 possibilidades:
  * Você pode usar software como [WinSCP](./../../../tutorials/system/access/network-access-winscp), [Cyberduck](./../../../tutorials/system/access/network-access-cyberduck) ou [MobaXTerm](./../../../tutorials/system/access/network-access-mobaxterm) para colocar os 2 arquivos no diretório `/boot/update/` (montando a partição [boot](./.../.../.../tutorials/system/access/remount-partition-with-write-access) com permissão para gravação).
  * Retirar o cartão SD do seu Recalbox desligado, colocar-lo em seu PC, abrir a unidade chamada `RECALBOX` e colocar os 2 arquivos na pasta `/update`.
* Uma vez feito isso, inicie ou reinicie seu Recalbox e a atualização será feita.

##

Durante a atualização, se todas as 7 barras ficarem imediatamente vermelhas ao invés de brancas à medida que você for avançando, observe quantas barras brancas estavam visíveis e contate o suporte ([Fórum](https://forum.recalbox.com) ou [Discord](https://discord.gg/NbQFbGM)).