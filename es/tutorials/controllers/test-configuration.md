---
title: Configuración/Test de mandos
description: 
published: true
date: 2023-09-26T07:03:22.586Z
tags: test, configuración, controladores, mandos
editor: markdown
dateCreated: 2023-09-18T11:40:02.197Z
---

En esta sección encontraréis cómo testear vuestros mandos en caso de tener problemas con ellos.

Aqui tenéis la lista de tutoriales disponibles:

[Testear vuestro joystick con sdl2-jstest](test-your-joystick-with-sdl2-jstest)