---
title: Ajuste del tamaño de la imagen mediante el overscan en TFT
description: 
published: false
date: 2024-11-17T18:07:49.274Z
tags: tft, overscan, imagen, tamaño
editor: markdown
dateCreated: 2023-09-14T12:03:46.260Z
---

La opción de overscna puede activarse si tienes bordes negros en la imagen o si tu imagen está recortada.

## Activación

* Menú EmulationStation (botón `START` de vuestro mando)
* `PARAMETROS AVANZADOS`
* Activad la opción `OVERSCAN`

>Debe reiniciar Recalbox después de modificar esta opción
{.is-warning}

## Configuración

* Si la configuración no corresponde a vuestra pantalla, podéis modificar los parámetros de cada borde dentro del fichero [recalbox-user-config.txt](./../../../tutorials/system/modification/configtxt-configuration) :

```ini
disable_overscan=0
overscan_left=24
overscan_right=24
overscan_top=24
overscan_bottom=24
```

El valor especifica el número de píxeles a ignorar en el borde de la pantalla. Aumentad este valor si la imagen se extiende más allá del borde de la pantalla; y disminuidlo si hay un borde negro entre el borde de la pantalla y vuestra imagen.

* Si estos ajustes no se aplican correctamente en emuladores o EmulationStation, probad a añadir esto:

```ini
overscan_scale=1
```