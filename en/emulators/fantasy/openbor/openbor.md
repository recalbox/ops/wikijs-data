---
title: OpenBOR
description: 
published: true
date: 2021-10-04T22:10:32.095Z
tags: openbor
editor: markdown
dateCreated: 2021-07-04T18:00:35.947Z
---



## ![](/emulators/license.svg) License

This core is under [**BSD-3**](https://github.com/DCurrent/openbor/blob/master/LICENSE) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Features



## ![](/emulators/bios.svg) BIOS

>**No bios is required.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Supported extensions

Roms must have the extension:

* .pak

### Location

Put your roms like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 openbor
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.pak**

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>**This core have no option.**
{.is-success}

## ![](/emulators/external-links.png) External links

* **Source code used** : [https://github.com/DCurrent/openbor/](https://github.com/DCurrent/openbor/)
* **Official Senile Team website** : [https://www.senileteam.com/](https://www.senileteam.com/)
* **Official community** : [http://www.chronocrash.com/forum/](http://www.chronocrash.com/forum/)