---
title: TI-99/4A
description: 
published: false
date: 2021-11-09T20:34:39.603Z
tags: computers, ti-99/4a, texas instruments, 8.0+
editor: markdown
dateCreated: 2021-09-12T08:35:58.527Z
---

![](/emulators/computers/ti994a.svg){.align-center}

## Technical data

* **Manufacturer**: Texas Instruments
* **Year of release**: 1979
* **OS** : Basic owner
* **CPU** : Texas Instruments TMS9900 @ 3.3 MHz
* **RAM** : 16/24 KB
* **Resolution** : max 256 x 192, 16 colours
* **Sound** : 3 voices on 5 octaves and noise channel

## Presentation

Produced by Texas Instruments, the **TI-99/4** is one of the first family computers (early 1980s). The version marketed in France soon became the **TI-99/4A**.

This computer has some similarities with the concept of video game console. Indeed, it can be connected to a television set via a SCART socket, can read cartridges and has a connector allowing to add two game controllers. It can also use audio cassettes to load or save programs using a standard tape recorder.

In June 1979, the American company Texas Instruments presented the TI-99/4, based on the TI-99/3 prototype. The first copies were sold in November for $1,150. The first games are Connect Four, Hangman, Yahtzee and Zero Zap.

A successor, the TI-99/7, is planned for the professional world but will never be released.

The TI-99/4A is presented to the general public in June 1981. The "A" refers to the new graphic processor: the TMS9918A chip. The latter, unlike the TMS9918, has a bitmap mode.

In December 1983, Texas Instruments officially announces that it stops the production of the TI-99/4A.

## Emulators

[TI-99/Sim](ti-99-sim)