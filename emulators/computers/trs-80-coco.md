---
title: TRS-80 Color Computer
description: 
published: true
date: 2021-11-16T07:49:26.203Z
tags: color, computers, trs-80, computer, trs-80-coco, tandy, 8.0+
editor: markdown
dateCreated: 2021-09-12T08:38:15.562Z
---

![](/emulators/computers/trs80coco.svg){.align-center}

## Technical data

* **Manufacturer**: Tandy Corporation
* **Year of release**: 1980
* **OS**: Microsoft Color Basic v1.0
* **CPU**: MC6809E @ 0.89 MHz
* **RAM**: 4KB - 16KB
* **Drive**: Cassette tape, Floppy disk 5.25"
* **Resolution**: 32 Column Display Ability (32 x 16 with 4 Colors / 256 x 192 with 2 Colors / 128 x 96 at 8 Colors / 128 x 192 with 4 Colors)

## Presentation

The RadioShack **TRS-80 Color Computer** (**Tandy Color Computer** or **CoCo**) was a personal computer based on Motorola's 6809 microprocessor and was part of the TRS-80 line.

The CoCo started its life as a reference system at Motorola and was intended to be used as a Videotext terminal. In fact, a simplified version of the CoCo was sold as a Videotext terminal with the same case and keyboard. The original CoCo came in 4K, 16K and 32K versions, although tinkerers soon discovered a way to turn 32K systems into 64K systems by activating the second RAM bank (which was disabled at manufacture). The gray case and "chiclet" keyboard of the CoCo I were abandoned in favor of the full-size white keyboard of the CoCo II and III.

## Emulators

[XRoar](xroar)