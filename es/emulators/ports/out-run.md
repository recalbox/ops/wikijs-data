---
title: Out Run
description: 
published: true
date: 2024-07-26T08:30:17.646Z
tags: ports, out run
editor: markdown
dateCreated: 2023-09-04T12:44:27.339Z
---

![](/emulators/ports/outrun.gif){.align-center}

## Detalles técnicos

* **Desarrollador**: Chris White
* **Año de lanzamiento**: 2014

## Presentación

**Out Run**, también escrito **OutRun** o **Outrun**, es un videojuego arcade de carreras de coches diseñado por Yū Suzuki y Sega-AM2 y lanzado por Sega en 1986 en terminales arcade. Fue portado a muchos otros soportes.

El juego tuvo un gran éxito entre los aficionados a las recreativas, hasta el punto de que a menudo se considera uno de los mejores juegos de carreras arcade. Destaca por su innovador hardware (incluida una cabina de juego móvil sobre un sistema hidráulico), sus innovadores gráficos y música, la posibilidad de elegir la banda sonora y el recorrido, y su fuerte temática de lujo y relajación.

En entrevistas retrospectivas sobre el juego, Yū Suzuki clasificó Out Run no como un juego de carreras, sino como un juego de "conducción", a pesar de admitir haberse inspirado en parte en la película de 1981 The Cannonball Crew.

## Emuladores

[Libretro Cannonball](libretro-cannonball)

