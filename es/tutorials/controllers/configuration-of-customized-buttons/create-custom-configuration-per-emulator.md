---
title: Crear una configuración personalizada para cada emulador
description: 
published: true
date: 2023-09-18T11:38:16.398Z
tags: configuración, emulador, personalizada
editor: markdown
dateCreated: 2023-09-18T11:38:16.398Z
---

Podéis crear vuestra propia configuración personalizada para cada emulador en Recalbox.

## RetroArch

La configuración de RetroArch puede ser creada a mano tomando como base el fichero [retroarch.cfg](https://github.com/libretro/RetroArch/blob/master/retroarch.cfg) o  modificando la configuración desde los menús de la interfaz de RetroArch. 
Una vez customizada vuestra configuración, habrá que configurar el fichero  `recalbox.conf` para que sea tenida en cuenta.

### Utilizando los menús de RetroArch

Tomaremos como ejemplo la creación de un fichero de configuración específico para la Neo-geo.

* Iniciad un juego en el emulador que queréis personalizar (en este caso neogeo).
* Entrad en el menú RetroArch (`Hotkey + B`).
* Modificad todos los ajustes que queráis.
* Volved a la primera entrada del menú RetroArch (`Main Menu`) e id hacia `Configuration Files` > `Save New Configuration` (el nombre del fichero de configuración debería ser `fbneo_libretro.cfg`, conteniendo el nombre del core que estáis utilizando).
* Conectandose via [SSH](./../../../tutorials/system/access/root-access-terminal-cli), cambiad el nombre del fichero de configuración a algo más fácil de recordar, por ejemplo :

```shell
mv /recalbox/share/system/configs/retroarch/fba_libretro.cfg /recalbox/share/system/configs/retroarch/inputs/neogeo_custom.cfg
```

* Finalmente, añadid la siguiente línea dentro del fichero `recalbox.conf` :

```ini
neogeo.configfile=/recalbox/share/system/configs/retroarch/inputs/neogeo_custom.cfg
```

### Inputs

#### Modificación manual

Podéis añadir archivos de configuración específicos para el sistema y/o el juego. Por supuesto, existe una prioridad si el mismo parámetro aparece en cada archivo de configuración. La prioridad es Juego > Sistema > RetroArch, lo que significa que una configuración existente en un fichero .cfg para un juego será la configuración aplicada y sobreescribirá los valores especificados en el sistema o en la configuración global (en otras palabras, la configuración del juego es la más prioritaria).

Estos son los ficheros que podéis modificar (o crear si no existen):

*  `~/configs/retroarch/<system>.cfg` donde `<système>` es el nombre del sistema tal cual aparece en la carpeta de `<roms>`. Por ejemplo: `~/configs/retroarch/snes.cfg`.
*  `~/configs/retroarch/<system>/<romname>.cfg` donde `<system>` es el nombre del sistema tal cual aparece en la carpeta de `roms` , y `<romname>` es el nombre exacto de la rom incluyendo su extensión. Por ejemplo: `~/configs/retroarch/snes/mario.zip.cfg`

## piFBA

>Esta parte necesita ser escrita. Si está familiarizado con el funcionamiento de piFBA, puede añadir su contenido aquí directamente.