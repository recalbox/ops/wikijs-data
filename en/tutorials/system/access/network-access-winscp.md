---
title: Network access via WinSCP
description: 
published: true
date: 2021-08-06T15:06:11.919Z
tags: winscp, network, access
editor: markdown
dateCreated: 2021-08-06T09:11:44.697Z
---

This tutorial is intended for Windows users. Please see [this tutorial](./../../../tutorials/system/access/network-access-cyberduck) if you are a macOS user or if you want to use Cyberduck on Windows.

## Prerequisites

* Make sure your Recalbox is running and connected to the network via an Ethernet cable or a Wifi dongle.

>You have a doubt and your Recalbox is connected? Go to [this tutorial](./../../../tutorials/network/ip/discover-recalbox-ip) to find the IP address of your Recalbox.
{.is-info}

* Download and install the [WinSCP](https://winscp.net/) software.

## Configuration

* Launch `WinSCP` and a window will appear to create your first connection.
* In this window, fill in the information as follows:
  * File protocol `SFTP`
  * Host name `recalbox` or the IP address you previously identified (the config will be changed each time you restart Recalbox if you entered the ip address and it is dynamic).
  * Port number `22`.
  * User name `root`.
  * Password `recalboxroot
* Click on `Save...`.
* Fill in the information as follows:
  * Save the session as: `Recalbox` for example.
  * Check the `Save Password (not recommended)` box, then click `OK`.
* Your software is now configured.

## Login

* In the opened window, select your created connection and click `Logon`.
* Another window will open informing you of the current connection.
* You are connected.

>Another window may open asking you to `Continue connecting to the unknown server and add the host key to the cache` Click `Yes` if this is the case.
{.is-warning}

## Show cached files

This is useful for accessing the `.emulationstation` folder to change the theme for example.

* Click `Options` > `Preferences`.
* Select `Panels` from the left hand side.
* Check the `Show hidden files` box and click `OK`.

## Usage

The use is quite intuitive. By default, you will find your computer on the left and on the right, the tree structure of your Recalbox.

**A few tips** :

* To return to the top folder or the root of your Recalbox, simply click on the first folder with a picto back up and identified as `..`.
* You can drop folders by simply dragging and dropping them into the right-hand column.
* You can change the permissions on a file by right clicking on it and then `Properties`.

## Add Notepad++

* Download [Notepad++](https://notepad-plus-plus.org/downloads/).
* In WinSCP, click on `Options` > `Preferences`.
* Select `Editors` on the left.
* Click `Add` and find the location where you installed Notepad++.

## Use Winscp to use the console

Click on the `Console` icon and type the command to write the partition.

![](/tutorials/system/access/winscp.png)