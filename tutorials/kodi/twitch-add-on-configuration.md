---
title: Twitch add-on configuration
description: Or how to watch Twitch videos without problems!
published: true
date: 2025-02-22T23:57:07.015Z
tags: configuration, kodi, twitch, add-ons
editor: markdown
dateCreated: 2022-12-27T23:54:14.916Z
---

## Introduction

In Kodi, the Twitch extension is already pre-installed. However, you need to configure it to be able to identify yourself and find all your account (live channels, etc.).

## Procedure

### Prerequisites

- Have a Twitch account.

### Configuration

>The following steps may differ if you are starting Kodi or Twitch for the first time.
>{.is-info}

1. Start Kodi.

2. Go to `Add-ons` > `Twitch`.



3. A window tells you to go generate an OAuth token.



4. Confirm the message and go to `Settings` at the bottom.



5. In the settings, choose `Login` on the left then in the middle, choose the option `Get OAuth token`.



6. After a few seconds, a window will tell you to go to a URL. As this may no longer be functional, you will need to visit https://twitchaddon.page.link/1Sk5.



7. At this address, you will get an OAuth token automatically if you are logged in on Twitch, in which case you will need to log in on Twitch if you are not already.



8. Back in Kodi, select `OAuth Token`.



9. Copy the OAuth token and validate.



10. Confirm the settings window.



11. Exit the add-on and go back in. If you copied the OAuth token correctly, you will be logged-in and can enjoy Twitch on Kodi without any ads!

