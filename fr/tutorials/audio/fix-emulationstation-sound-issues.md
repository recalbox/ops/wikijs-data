---
title: Corriger les problèmes de son dans l'interface
description: 
published: true
date: 2024-07-26T07:14:54.614Z
tags: emulationstation, son
editor: markdown
dateCreated: 2021-11-16T08:41:22.636Z
---

## Introduction

Dans certains cas, en fonction du matériel utilisé, le son peut paraître anormal dans l'interface, comme saccadé. Ce petit tutoriel pourrait vous aider si vous êtes dans le même cas.

## Utilisation

Pour cela, il faut se connecter via [SSH](./../../tutorials/system/access/root-access-terminal-cli) et exécuter les 2 lignes suivantes :

```shell
mount -o remount,rw /
echo 'default-sample-rate = 48000' >> /etc/pulse/daemon.conf
```

Redémarrez et le son devrait être correct.

>Vous devrez refaire la manipulation après chaque mise à jour du système.
{.is-info}