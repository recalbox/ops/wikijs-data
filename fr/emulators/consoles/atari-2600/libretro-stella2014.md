---
title: Libretro Stella2014
description: 
published: true
date: 2024-06-23T11:24:14.187Z
tags: libretro, atari-2600, atari, 2600, stella2014, 8.0+
editor: markdown
dateCreated: 2021-06-25T17:39:39.440Z
---

**Libretro Stella2014** est un émulateur Atari multiplateforme.

## ![](/emulators/license.svg) Licence

Ce core est sous licence [**GPLv2**](https://github.com/stella-emu/stella/blob/master/License.txt).

## ![](/emulators/compatibility.png) Compatibilité

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ❌ | ❌ | ❌ | ❌ | ❌ | ❌ | ❌ |

## ![](/emulators/features.png) Fonctionnalités

| Fonctionnalité | Supporté |
| :---: | :---: |
| Redémarrage | ✔ |
| Captures d'écran | ✔ |
| Sauvegardes instantanées | ✔ |
| Rembobinage | ✔ |
| Netplay | ✔ |
| RetroAchievements | ✔ |
| Controls | ✔ |
| Remapping | ✔ |

## ![](/emulators/bios.svg) BIOS

>**Aucun bios n'est requis.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Extensions supportées

Les roms doivent avoir les extensions suivantes :

* .a26
* .bin
* .zip
* .7z

Ce système supporte les roms compressées au format .zip/.7z. Attention toutefois, il ne s'agit que d'une archive.

Les fichiers contenus dans les .zip/.7z doivent correspondre aux extensions citées précédemment.
De plus, chaque fichier .zip/.7z ne doit contenir qu'une seule rom compressée.

### Emplacement

Placez les roms comme ceci : 

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 atari2600
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.zip**

>Les roms au format **No-Intro** sont vivement conseillées.
{.is-success}

>Pour plus d'information sur les roms, rendez-vous sur [ce tutoriel](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuration avancée de l'émulateur

>Pour pouvoir conserver vos configurations personnalisées lors d'une mise à jour, nous vous conseillons d'utiliser notre fonctionnalité [Surcharges de configuration](./../../../advanced-usage/configuration-override).
{.is-info}

### Accéder aux options

Vous pouvez configurer diverses options de deux façons différentes.

* Via le menu RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via le fichier `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Options du core

| Options | Valeur des options | Variable (pour surcharge) | Valeurs des variables |
| --- | --- | --- | --- |
| Color Depth (Restart) | `Thousands (16-bit)` ✅ / `Millions (24-bit)` | `stella2014_color_depth` | `16bit` / `24bit` |
| Interframe Blending | `Désactivé` ✅ / `Simple` / `Ghosting (65%)` / `Ghosting (75%)` / `Ghosting (85%)` / `Ghosting (95%)` | `stella2014_mix_frames` | `disabled` / `mix` / `ghost_65` / `ghost_75` / `ghost_85` / `ghost_95` |
| Audio Filter | `Désactivé` ✅ / `Activé` | `stella2014_low_pass_filter` | `disabled` / `enabled` |
| Audio Filter Level | `5%` / `10%` / `15%` / `20%` / `25%` / `30%` / `35%` / `40%` / `45%` / `50%` / `55%` / `60%` ✅ / `65%` / `70%` / `75%` / `80%` / `85%` / `90%` / `95%` | `stella2014_low_pass_range` | `5` / `10` / `15` / `20` / `25` / `30` / `35` / `40` / `45` / `50` / `55` / `60` / `65` / `70` / `75` / `80` / `85` / `90` / `95` |
| Gamepad: Paddle Sensitivity (Digital) | `10%` / `15%` / `20%` / `25%` / `30%` / `35%` / `40%` / `45%` / `50%` ✅ / `55%` / `60%` / `65%` / `70%` / `75%` / `80%` / `85%` / `90%` / `95%` / `100%` | `stella2014_paddle_digital_sensitivity` | `10` / `15` / `20` / `25` / `30` / `35` / `40` / `45` / `50` / `55` / `60` / `65` / `70` / `75` / `80` / `85` / `90` / `95` / `100` |
| Gamepad: Paddle Sensitivity (Analog) | `10%` / `15%` / `20%` / `25%` / `30%` / `35%` / `40%` / `45%` / `50%` ✅ / `55%` / `60%` / `65%` / `70%` / `75%` / `80%` / `85%` / `90%` / `95%` / `100%` / `105%` / `110%` / `115%` / `120%` / `125%` / `130%` / `135%` / `140%` / `145%` / `150%` | `stella2014_paddle_analog_sensitivity` | `10` / `15` / `20` / `25` / `30` / `35` / `40` / `45` / `50` / `55` / `60` / `65` / `70` / `75` / `80` / `85` / `90` / `95` / `100` / `105` / `110` / `115` / `120` / `125` / `130` / `135` / `140` / `145` / `150` |
| Gamepad: Paddle Response (Analog) | `Linear` ✅ / `Quadratic` | `stella2014_paddle_analog_response` | `linear` / `quadratic` |
| Gamepad: Paddle Deadzone (Analog) | `0%` / `3%` / `6%` / `9%` / `12%` / `15%` ✅ / `18%` / `21%` / `24%` / `37%` / `30%` | `stella2014_paddle_analog_deadzone` | `0` / `3` / `6` / `9` / `12` / `15` / `18` / `21` / `24` / `37` / `30` |
| Stelladaptor: Paddle Sensitivity | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` ✅ / `21` / `22` / `23` / `24` / `25` / `26` / `27` / `28` / `29` / `30` | `stella2014_stelladaptor_analog_sensitivity` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `21` / `22` / `23` / `24` / `25` / `26` / `27` / `28` / `29` / `30` |
| Stelladaptor: Paddle Centre Offset | `-10` / `-9` / `-8` / `-7` / `-6` / `-5` / `-4` / `-3` / `-2` / `-1` / `0` ✅ / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `21` / `22` / `23` / `24` / `25` / `26` / `27` / `28` / `29` / `30` | `stella2014_stelladaptor_analog_center` | `-10` / `-9` / `-8` / `-7` / `-6` / `-5` / `-4` / `-3` / `-2` / `-1` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `21` / `22` / `23` / `24` / `25` / `26` / `27` / `28` / `29` / `30` |

## ![](/emulators/external-links.png) Liens externes

* **Code source utilisé :** [https://github.com/libretro/stella2014-libretro/](https://github.com/libretro/stella2014-libretro/)
* **Documentation Libretro :** [https://docs.libretro.com/library/stella/](https://docs.libretro.com/library/stella/)