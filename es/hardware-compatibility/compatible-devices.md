---
title: Compatibilidad de periféricos
description: 
published: true
date: 2023-09-19T07:07:43.586Z
tags: periféricos, compatibilidad
editor: markdown
dateCreated: 2023-09-19T07:07:43.586Z
---

Este es un proyecto llevado a cabo por varios miembros de la comunidad Recalbox para listar todos los dispositivos que funcionan "plug and play" y también los dispositivos que no funcionan.

Sentíos libres de añadir o cambiar lo que queráis, ¡la idea es crear una base de datos de compatibilidades de periféricos en Recalbox!