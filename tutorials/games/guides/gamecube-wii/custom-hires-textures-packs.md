---
title: Packs de textures personnalisées
description: 
published: true
date: 2021-09-05T12:59:30.298Z
tags: dolphin, texture, pack, personnalisé
editor: markdown
dateCreated: 2021-06-27T19:43:49.968Z
---

Dolphin permet, en plus de faire tourner les jeux Gamecube, d'améliorer les textures ou encore pour certain les vidéos en HD, crée par des passionnés (merci à eux) pour encore plus embellir vos jeux préférés.

Pour l’instant il existe des packs textures pour les jeux suivants :

* Mario Kart Double Dash
* Luigi’s Mansion
* The Legend of Zelda : Ocarina of Time
* Resident Evil 2
* Resident Evil 3 Nemesis

_Cette liste n’est pas exhaustive, d’autres sont sûrement en préparation ou sont déjà sortis lorsque vous lirez ces lignes._

Ils sont disponible sur le [forum de Dolphin](https://forums.dolphin-emu.org/Forum-custom-texture-projects). Les formats supportés sont :

* BC7
* DDS

Placez vos pack de textures dans le dossier suivant : `/recalbox/share/saves/dolphin-emu/Load/Textures/`.

Toutes les textures doivent se trouver dans un dossier avec l'ID du jeu pour lequel vous installez le pack ou l'équivalent de 3 lettres sans région.

* `GM4`: id sans région
* `GM4P01`: id complet 

![Exemple pour Mario Kart Double Dash (Europe)](/emulators/consoles/gc-textures-packs.png)

On peut trouve cette information sous Dolphin Pour ce faire :

* Lancez n’importe quel jeu GameCube.
* Une fois le jeu démarré, appuyez simultanément sur `Hotkey` + `B`. 
* Le menu Dolphin apparaît avec une liste de vos jeux.
* Recherchez le jeu dont vous avez besoin de connaître l’ID, cette dernière est indiquée sur la quatrième colonne et elle est composée d’une combinaison de 6 lettres et chiffres.

>Si les textures ne se trouvent pas dans un dossier nommé correctement, alors le pack ne fonctionnera pas !
{.is-warning}