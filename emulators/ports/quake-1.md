---
title: Quake 1
description: 
published: true
date: 2021-09-16T04:52:04.841Z
tags: ports, quake 1
editor: markdown
dateCreated: 2021-05-21T08:05:07.597Z
---

![](/emulators/ports/quake.gif){.align-center}

## Fiche technique

* **Crée par :** id Software
* **Année de sortie :** 1996
* **Genre :** Tir à la première personne
* **Moteur :** Quake Engine
* **Dernière version :** v1.09
* **Système d'exploitation :** MS-DOS 5.0 (ou supérieur), Win95/98
* **Processeur :** Pentium minimum
* **Stockage :** 16 MB sur MS-DOS, 24MB sur Win95/98  + lecteur CD-Rom 2x (sans = pas de pistes sonores)
* **RAM :** 8 MB MS-DOS ou 16MB Win95/98 minimum
* **Résolution :** 320x200 pixels minimum
* **Son :** Sound Blaster et AdLib ou carte son 100% compatible
* **Développeur :** id Software
* **Éditeur :** GT Interactive
* **Designers :** John Romero, American McGee, Sandy Petersen, Tim Willits
* **Programmeurs :** John Carmack, Michael Abrash, John Cash
* **Artistes :** Adrian Carmack, Kevin Cloud, Paul Steed
* **Compositeurs :** Trent Reznor, Nine Inch Nails
* **PEGI** : 16

## Présentation

Quake prend place dans un monde où le gouvernement, après plusieurs années de recherche et développement, développe un prototype rendant possible la téléportation. Celui-ci est cependant corrompu par un ennemi, connu sous le nom de code Quake, qui utilise le prototype pour envoyer ses créatures cauchemardesques dans la dimension des humains, préparant ainsi une possible invasion de la Terre. Le gouvernement envoie alors une équipe de combattants à travers le portail afin d’éliminer cette menace. Le joueur incarne un soldat anonyme, seul survivant de l’opération. À partir d’une base encore sécurisée, celui-ci peut accéder, via des portails, à quatre dimensions contrôlées par l’ennemi. Chaque dimension permet au joueur d’acquérir une des quatre runes magiques de Shub-Niggurath, le lieutenant de Quake chargé d’envahir la Terre. Réunir les quatre runes permet d’accéder à la dimension où se cache le démon.

## Émulateurs

[Libretro TyrQuake](libretro-tyrquake)