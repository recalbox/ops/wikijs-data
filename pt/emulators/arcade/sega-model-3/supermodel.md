---
title: Supermodel
description: 
published: true
date: 2021-10-14T23:37:03.687Z
tags: model-3, supermodel
editor: markdown
dateCreated: 2021-10-14T23:37:03.687Z
---

O Supermodel emula a plataforma de arcade Sega Model 3.  
Ele usa **OpenGL 2.1** e **SDL**, e pode rodar em Windows, Linux e macOS.

Este é um lançamento público antecipado do Supermodel. Este é um documento muito preliminar, a versão alfa do software.
O desenvolvimento começou em janeiro de 2011 e tem se concentrado nos aspectos de engenharia reversa do ainda desconhecido Model 3.
Como resultado, muitas características importantes, tais como uma interface de usuário adequada, ainda não foram implementadas e a compatibilidade de jogos ainda é baixa.

## ![](/emulators/license.svg) Licença

Este núcleo é licenciado sob a [**GPLV3***](https://www.supermodel3.com/About.html).

## ![](/emulators/compatibility.png) Compatibilidade

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ❌ | ❌ | ❌ | ❌ | ❌ | ❌ | ✅ | ✅ |

## ![](/emulators/features.png) Recursos

| Funcionalidade | Apoiado |
| :---: | :---: |
| Pausar | ✔︎ |
| Savestate | ✔︎ |
| Captura de tela | ✔︎

## ![](/emulators/bios.svg) BIOS

>**Nenhuma BIOS é necessária**.
{.is-success}

## ![](/emulators/roms.png) Roms

O model3 é baseado no romset Mame 0.230.

> Você precisa passar seu romset MAME no ClrMamePro para ter um romset Model3 funcional.
{.is-info}

>Para classificar suas roms, o arquivo .dat está disponível na pasta `/recalbox/share/bios/supermodel/` ou abaixo para download.
{.is-success}

[model3-0.230_noclones_v1.0_barhi.dat.zip](/emulators/arcade/model3-0.230_noclones_v1.0_barhi.dat.zip)

### Local

Coloque as roms assim:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 model3
┃ ┃ ┃ ┃ ┣ 🗒 **jogo.zip**


## ![](/emulators/advanced-configuration.png) Configuração avançada do emulador

> Para poder manter suas configurações personalizadas durante uma atualização, aconselhamos que você use nosso recurso de [Sobrecarga de configuração](./../../../advanced-usage/configuration-override).
{.is-info}

### O arquivo de configuração do Supermodel:

```text
#
#  ####                                                      ###           ###
# ##  ##                                                      ##            ##
# ###     ##  ##  ## ###   ####   ## ###  ##  ##   ####       ##   ####     ##
#  ###    ##  ##   ##  ## ##  ##   ### ## ####### ##  ##   #####  ##  ##    ##
#    ###  ##  ##   ##  ## ######   ##  ## ####### ##  ##  ##  ##  ######    ##
# ##  ##  ##  ##   #####  ##       ##     ## # ## ##  ##  ##  ##  ##        ##
#  ####    ### ##  ##      ####   ####    ##   ##  ####    ### ##  ####    ####
#                 ####                                                            
#
#                       A Sega Model 3 Arcade Emulator.
#                Copyright 2011 Bart Trzynadlowski, Nik Henson


# ----------------------------- CONTROLLERS -------------------------------- #
# The rate at which analog control values increase/decrease
# when controlled by a key.  Valid range is 1-100, with 1
# being the least sensitive (slowest response) on default [25]
sensitivity=25

# Specifies the saturation, the position at which the joystick
# is interpreted as being in its most extreme position,
# as a % of the total range of the axis, from 0-200.
# A value of 200 means that the range will be halved, on default [100].
saturation=100

# Specifies the dead zone as a percentage,
# 0-99, of the total range of the axis.
# Within the dead zone, the joystick is inactive on default [2].
deadzone=2

# ------------------------------- AUDIO ------------------------------------- #

# Master Volume in %
sound-volume=100
# Set Music volume in %
music-volume=100

# Swap left and right audio channels
flip-stereo=0
# Relative front/rear balance in %
balance=0

# Disable sound board emulation sound effects
no-sound=0
# Disable Digital Sound Board MPEG music
no-dsb=0

# Select your sound engine:
# New SCSP engine based on MAME [0]
# Legacy SCSP engine [1]
sound-engine=0

# ------------------------------- VIDEO ------------------------------------- #

# set your graphics resolution automaticly with [auto]
# or set on this format [1280,1024] or [none] if needed
resolution=auto

# set on default ratio with [0]
# Expand 3D field of view to screen width [1]
# ignoring aspect ratio with [2]
screen-ratio=0
# Disable 60 Hz frame rate lock
no-throttle=0

# New 3D engine on default [0] enable 
# Legacy 3D engine only if no image only sound set [1] to enable 
3d-engine=0
# Use 8 texture maps for decoding
multi-texture=0
# Enable proper quad rendering
quad-rendering=0

# Crosshairs configuration for gun games
crosshairs=1

# -------------------------------- CORE ------------------------------------- #

# Run graphics rendering in main thread
no-gpu-thread=1
# Disable [0], Enable [1] multi-threading.
no-threads=0

# The PowerPC frequency in MHz.
# The default is [50].
ppc-frequency=100

# Enable [1] or Disable [0] menu services [L3=test, R3=service]
service-button=1

# Log level information in Supermodel.log
# Default value : info
# add value : error
log-level=info
```

**Localização do arquivo** : `/recalbox/share/system/configs/model3/ConfigModel3.ini`

### Opções do núcleo

## ![external-links.png](/emulators/external-links.png) Links externos

* **Fórum Oficial** : [https://www.supermodel3.com/](https://www.supermodel3.com/)
* **Código Fonte Oficial** : [https://sourceforge.net/projects/model3emu/](https://sourceforge.net/projects/model3emu/)