---
title: Gestion des scrapes
description: 
published: true
date: 2021-09-04T21:08:07.900Z
tags: gestion, scrapes
editor: markdown
dateCreated: 2021-05-21T08:11:00.734Z
---

Le scrape consiste à ajouter à vos jeux une multitude d'informations associés tels qu'un résumé de jeu, capture d'écran, vidéo, manuel de jeu, etc.

Liste des tutoriels disponibles :

[Liste des utilitaires de scrape](scraping-tools)
[Skraper - Scraper ses roms](scrape-your-roms-with-skraper)