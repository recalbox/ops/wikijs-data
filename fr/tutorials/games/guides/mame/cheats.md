---
title: Codes de triche
description: 
published: true
date: 2024-09-18T12:37:50.985Z
tags: mame, codes, triche
editor: markdown
dateCreated: 2024-09-18T12:33:00.142Z
---

## Introduction

Avec MAME, il est possible d'ajouter des codes de triche.

>Il est préférable d'utiliser une manette avec les boutons `Select` et `Hotkey` séparés si vous souhaitez conserver l'usage des sauvegardes instantanées.
{.is-info}

## Utilisation

* Téléchargez l'archive des [codes de triche](https://www.mamecheat.co.uk/mame_downloads.htm) correspondant à la version intégrée de MAME dans Recalbox.

>Si vous ne trouvez pas pour la version de MAME actuelle, prenez la première version précédente, elle fonctionnera à 100%.
{.is-info}

* Décompressez l'archive. Vous obtiendrez 3 fichiers : le seul fichier qui nous intéresse est `cheat.7z`.
* Placez le fichier `cheat.7z` dans le répertoire `/recalbox/share/bios/mame` **sans le décompresser !** et lancez votre jeu.

Pour accéder aux codes de triche, pendant le jeu :

* Allez dans le menu de RetroArch en faisant `Hotkey + B`.
* Allez dans `Options de cœur` > `System`.

![](/tutorials/games/guides/mame/cheatsfr1.png){.full-width}

* Tout en haut de l'écran des options, activez `Cheats` et quittez le jeu.

![](/tutorials/games/guides/mame/cheatsfr2.png){.full-width}

* Relancez votre jeu et allez dans le menu MAME en faisant `Select` + `Y` et allez dans `Cheat`.

![](/tutorials/games/guides/mame/cheatsfr3.png){.full-width}

* Vous aurez la liste des cheats pour votre jeu. Il ne vous reste qu'à sélectionner les cheats souhaités.

![](/tutorials/games/guides/mame/cheatsfr4.png){.full-width}