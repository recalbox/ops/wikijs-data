---
title: Preparación
description: 
published: true
date: 2025-02-06T23:52:12.073Z
tags: netplay, preparación
editor: markdown
dateCreated: 2023-09-23T20:03:42.444Z
---

## I - Preparación de roms

Debéis respetar las siguientes etapas para poder jugar con vuestras roms en modo Netplay.

### A - Core/Romsets Compatibles Netplay Libretro

#### 1. Arcades

| Sistema | Core | Romset |
| :---: | :---: | :---: |
| **FinalBurn Neo** | Libretro FbNeo | 1.0.0.03 |
| **Mame** |  |  |
|  | Libretro Mame 2003 | Mame 0.78 |
|  | Libretro Mame 2003Plus | Mélange Mame de 0.78 à 0.188 |
|  | Libretro Mame 2010 | Mame 0.139 |
| **Neo-geo** | Libretro FbNeo | 1.0.0.03 |

#### 2. Consolas Fantasy

| Sistema | Core | Romset |
| :---: | :---: | :---: |
| **TIC80** | Libretro Tic80 |  |

#### 3. Consolas de Salón

| Sistema | Core | Romset |
| :---: | :---: | :---: |
| **Atari 2600** | Libetro Stella | No Intro |
| **Famicom Disk System** |  | No-Intro |
|  | Libretro FCEUmm |  |
|  | Libretro Nestopia |  |
|  | Libretro Mesen |  |
| **Master System** |  | No-Intro |
|  | Libretro PicoDrive |  |
|  | Libretro Genesisplusgx |  |
| **Megadrive** |  | No-Intro |
|  | Libretro PicoDrive |  |
|  | Libretro Genesisplusgx |  |
| **Nintendo Entertainment System** |  | No-Intro |
|  | Libretro FCEUmm |  |
|  | Libretro FCEUNext |  |
|  | Libretro Nestopia |  |
|  | Libretro Quicknes |  |
|  | Libretro Mesen |  |
| **Super Nintendo** |  | No-Intro |
|  | Libretro Snes9x (activer l'option « multitap » |  |
|  | Libretro Snes9x_2002 |  |
|  | Libretro Snes9x_2005 |  |
|  | Libretro Snes9x_2010 |  |
| **Pc-Engine** |  | No-Intro |
|  | Libretro Mednafen_supergrafx |  |
|  | Libretro Mednafen_PCE_FAST |  |
| **Pc-Engine CD** |  | Redump |
|  | Libretro Mednafen_PCE_FAST |  |
|  | Libretro Mednafen_supergrafx |  |
| **PC-FX** | Libretro Mednafen_PCFX | Redump |
| **Sega 32x** | Libretro PicoDrive | No-Intro |
| **Sega CD** |  | Redump |
|  | Libretro PicoDrive |  |
|  | Libretro Genesisplusgx |  |
| **SG-1000** | Libretro blueMSX | No-Intro |
| **Supergrafx** | Libretro Mednafen_supergrafx | No-Intro |

#### 4. Port

| Sistema | Core | Romset |
| :---: | :---: | :---: |
| **MR BOOM** | Libretro MrBoom | ? |

### B - Core/Romsets Compatibles Netplay Standalone

#### Consolas de Salon

| Sistema | Core | Romset |
| :---: | :---: | :---: |
| **GameCube** | Dolphin-emu | Redump |
| **Wii** | Dolphin-emu | Redump |

### C - Ordenar y comprobar vuestros romsets

Existen diferentes formas de clasificación. ¿Cúal elegir?

#### 1 - ¿National o international?

* **National :** Si sólo queréis jugar con gente de vuestro propio país: quedaros con las roms de vuestro país **y utilizad las roms de EE.UU. y Japón para las que os falten **.
* **International :** Si queréis poder jugar con personas de otros países. Guardad una rom de cada país.

#### 2 - Obtención de un fichero dat para el software de filtro

Descargad los ficheros dat de las consolas que queréis clasificar (Link próximamente...?).

#### 3 - Programa de filtro

* Clrmamepro (**El mejor programa** pero necesita mucha prática para aprender a manejarlo bien, **es indispensable para las roms de arcade**)

[https://mamedev.emulab.it/clrmamepro/](https://mamedev.emulab.it/clrmamepro/)

* Romulus (Programa **mucho más simple de uso** pero no gestiona suficientemente bien el arcade)

[http://romulus.net63.net/#downloads](https://romulus.cc/#downloads)

Seguid este enlace para saber a qué corresponden [los códigos y etiquetas que se encuentran en los nombres de los ficheros de roms](./../../../tutorials/games/generalities/tags-used-in-rom-names).

### D - Scrapear vuestros romsets

Existen varias formas de scraper vuestras roms:

#### 1. Scrapear con la herramienta interna de Recalbox

* MENU EMULATIONSTATION (`START`)
* Scraper

#### 2. Scrapear vuestras roms con un programa dedicado para una mejor personalización (Ejemplo: Hack Region traducción)

* **Skraper**

[https://www.skraper.net/](https://www.skraper.net/) (compatible Windows/Linux y dentro de poco también con macOS).

**Tuto video de Skraper :**

[https://www.youtube.com/watch?v=vV8DxtdOCaU&index=15&list=PL2oNQ0AT7fx2ExiSNrfHUzga5GnogI4sh](https://www.youtube.com/watch?v=vV8DxtdOCaU&index=15&list=PL2oNQ0AT7fx2ExiSNrfHUzga5GnogI4sh)

### E - Hasher vuestras roms

El **HASH** es una huella digital única que sirve para obtener **un mejor reconocimiento** a la hora de comparar las roms (es una suma de comprobación CRC32 de la rom).

El **Hash** es obligatorio para que Netplay funcione correctamente**.

* Menu EmulationStation
* Optiones de juegos
* Optiones de Netplay
* Hash
* Lanzar

No obstante algunas roms no poseen hashes, es el caso de los siguientes sistemas de arcade:

* Fba Libretro
* Mame
* Neo-Geo

>Algunos programas, como Skraper, pueden hashear y scrapear al mismo tiempo!
{.is-info}

## II- Configuración del sistema Netplay

### A - Activación y configuración de Netplay

* Desde vuestra Recalbox :
  * Menú EmulationStation
  * Optiones de juegos
  * Optiones de Netplay

![](/basic-usage/features/netplay/netplaypreparation.png)

* Netplay (Red)
  * **ON/OFF**
* Nickname
  * Seleccionad para configurar vuestro pseudo.

>Por favor, no pongáis acentos en el pseudo, puede impedir que el Netplay funcione correctamente.
{.is-warning}

* Puerto
  * 55435 (Correspond au serveur de RetroArch)
* Netplay MITM Server gateway (ver más abajo para más explicaciones)
  * Ninguno
  * New-York
  * Madrid
* Contraseñas predefinidas
  * Seleccionad si queréis modificar la lista de contraseñas disponibles.
* Hashear las roms. Tenéis dos opciones.
  * Filtro
 		* Sólamente los hashs que faltan.
    * Todos los juegos.
  * Sistemas: lista de sistemas sobre los que efectuar el hasheado

### B - Configurar vuestro puerto (UPNP/Abrid el puerto/Netplay MITM)

>Algunos modems poseen el protocolo UPNP que permite realizar una configuración automática de los puertos.
{.is-info}

#### 1. Verificad que vuestra box gestiona el protocolo UPnP

Os aconsejamos utilizar la opción de UPnP si la tenéis disponible en vuestro router.

* Empiezad lanzado un juego de vuestra lista en modo Netplay.
* En vuestro PC, id a: [https://www.yougetsignal.com/tools/open-ports/](https://www.yougetsignal.com/tools/open-ports/)
* Introducid el puerto 55435 y pulsad Comprobar.
  * **a. Si la respuesta es "positiva":** No necesitáis abrir el puerto.
  * **b. Si la respuesta es "negativa":** Vuestra box no soporta el UPNP o hay que activar esta opción. Abrid **un puerto manualmente** (recomendado) o usad el **Netplay MITM**.

#### 2. Abrir un puerto (Conexión directa)

Necesitas abrir el puerto 55435 para el lobby de Libretro y el puerto 2626 para Dolphin en tu módem de Internet.

* **Abrid el puerto manualmente:** Dependiendo de vuestro ISP, tendréis que hacer cosas diferentes. Informaos en la página de vuestro operador o en los foros.
  * El nombre puede ser cualquiera, pero os ayudará si lo nombráis exactamente como el software que utilizará los puertos. Recordad que no puedéis utilizar un nombre ya en uso.
  * A continuación, eligid el Protocolo, TCP o UDP. Algunos routers te permiten elegir uno u otro, mientras que otros te dan una tercera opción para elegir ambos simultáneamente.
  * Necesitaréis uno u otro, o ambos. Si no se especifica, es preferible abrir los puertos utilizando ambos protocolos.
  * A continuación, aparecerá una lista de puertos. En esta sección, anotaremos el puerto o rango de puertos que queremos abrir.
  * Si sólo vas a abrir un puerto, escríbelo en la primera casilla y deja la otra en blanco.
  * Por ejemplo, si vas a abrir 100 puertos entre los puertos 1500 y 1600, escribid 1500 en la primera casilla y 1600 en la segunda.
  * Podéis ignorar todo los demás. Podéis añadir una definición o un tipo de aplicación si lo deseas, pero no es necesario.


  * Si el router no os permite elegir UDP y TCP al mismo tiempo, deberéis hacer 2 entradas separadas para el mismo puerto.
  * Al terminar de añadir vuesro puerto, pulsad en "Añadir una entrada" o "Aceptar" o cualquier otro botón que permita grabar los cambios realizados.
  * Algunos routers permiten crear reglas de excepción, pero no las asignan automáticamente a vuestro ordenador/dispositivo. Debería haber una opción para hacerlo.

#### 3. La opción Netplay MITM

La opción "Netplay MITM, Relay Server" permite saltar la apertura de puertos en el módem.  
Esto funciona mediante la utilización de un servidor intermedio donde se conectan todos los jugadores, en vez de utilizar una conexión directa.

Sin embargo, este modo de funcionamiento repercutirá en la velocidad a la que se sincronizan los datos y, por tanto, en la capacidad de respuesta del juego y en el retardo.

* **¿En que casos puedo utilizar?**

  * Vuestro router no soporta el protocolo UPnP (apertura y cierre automático de puertos).
  * No deseáis abrir los puertos necesarios en vuestro router.
  * No estáis seguros de haberlo hecho bien:

  En estos casos, podéis probar a activar la opción "Netplay MITM"

>Es aconsejable abrir el puerto manualmente o un router que gestione el UPnP, ya que el servidor intermedio MITM añadirá una gran latencia al juego.
>
>RetroArch no comprueba si vuestros puertos están abiertos, y tampoco lo hacen los sevidores intermedios de Libretro.  
>Verificad que vuestro puerto está abierto correctamente o habilitad el servidor intermedio, de lo contrario los usuarios no podrán conectarse a vuestra sesión y no podréis jugar en red.
{.is-warning}