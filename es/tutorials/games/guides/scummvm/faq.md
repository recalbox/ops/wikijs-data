---
title: FAQ
description: 
published: true
date: 2024-07-16T17:12:02.773Z
tags: faq
editor: markdown
dateCreated: 2023-09-21T13:16:48.528Z
---

## ¿Qué es SCUMM?

**Script Creation Utility for Maniac Mansion** (SCUMM) es un motor de videojuegos desarrollado por Lucasfilm Games, que posteriormente pasó a llamarse LucasArts, para facilitar el desarrollo de su primer juego de aventuras gráficas Maniac Mansion (1987).  
Posteriormente se reutilizó como motor para las siguientes aventuras gráficas de LucasArts.

Se sitúa entre el motor del juego y el lenguaje de programación, permitiendo a los diseñadores crear localizaciones, objetos y secuencias de diálogo sin escribir código.

También permite que el escenario y los archivos de datos sean independientes de la plataforma. SCUMM también alberga motores de juego integrados, como **Interactive MUsic Streaming Engine** (iMUSE), **Interactive Streaming ANimation Engine** (INSANE), **CYST** (el motor de animación del juego), **FLEM** (ubicaciones y nombres de objetos en una habitación) y **MMUCUS**.

**SCUMM** ha sido portado a las siguientes plataformas: 3DO, Amiga, Apple II, Atari ST, CDTV, Commodore 64, Fujitsu FM Towns & Marty, Apple Macintosh, Nintendo Entertainment System, DOS, Microsoft Windows, Sega CD (Mega-CD), y TurboGrafx-16/PC Engine.

## ¿Qué es ScummVM?

**ScummVM** es un programa que te permite jugar a ciertos juegos de aventuras gráficas point-and-click, siempre y cuando proporciones los ficheros del juego.

La idea detrás de esto es que ScummVM simplemente reemplaza los ejecutables del juego, permitiéndote jugarlo en sistemas para los que antes no estaba disponible.

### Añadir un juego ScummVM:

* Id a la página [https://www.scummvm.org/compatibility/2.7.0/] (https://www.scummvm.org/compatibility/2.7.0/) para encontrar vuestro juego y apuntad su nombre largo (`Game Long Name`) y su nombre corto (`Game Short Name`).
* Cread una carpeta con el nombre largo del juego seguido de la extensión `.scummvm` y copiad los ficheros del juego en ella. 
*En esta carpeta, tendréis que añadir un fichero de texto simple, llamado `[nombre corto].scummvm` con su nombre corto en una sola línea.
 
>Añadid el nombre largo como nombre de la carpeta `[nombre largo].scummvm`. Esto permitirá al scraper detectarlo más fácilmente.
{.is-success}

#### Ejemplo

Copiad el juego "Broken Sword" dentro de un directorio llamado `Broken Sword 1.scummvm` y a continuación colocad este directorio dentro de vuestro directorio de roms `scummvm`.  
En el directorio `Broken Sword 1.scummvm`, cread un fichero llamado `sword1.scummvm`.

```text
scummvm
|- Broken Sword 1.scummvm
|  |- sword1.scummvm
|  |- ... autres fichiers du jeu
```

El nombre de esta carpeta (sin la extensión) será el nombre del juego que mostrará EmulationStation dentro de la lista de juegos del sistema Scummvm.

## ¿Son compatibles todos los juegos de Scumm con el emulador?

Leed la [lista de compatibilidad](http://scummvm.org/compatibility/) de la página oficial de ScummVM.

>Contiene una lista actualizada de todos los juegos soportados y su nivel de compatibilidad.
{.is-info}

## ¿Cómo configuro la emulación Roland MT-32 para los juegos de ScummVM?

* Primero, hay que copiar las ROMs MT32 (`MT32_CONTROL.ROM` y `MT32_PCM.ROM`) a un directorio, por ejemplo `/recalbox/share/bios`. 
* A continuación, editad el fichero de configuración de ScummVM (`scummvm.ini` o `.scummvmrc`) y añadid las siguientes líneas a la sección `[scummvm]`:

```text
extrapath=/recalbox/share/bios
mt32_device=mt32
music_driver=mt32
```

**Solución alternativa:**
También podéis copiar los dos ficheros ROM dentro del directorio del juego.

De esta manera, no hay necesidad de modificar el fichero de configuración, ¡pero tendréis que duplicar estos ficheros para cada juego que necesite está música!

Debéis utilizar los ficheros de ROM soportados por ScummVM.

Es decir, los ficheros de ROM que poseen las siguientes firmas MD5 :

```text
5626206284b22c2734f3e9efefcd2675  MT32_CONTROL.ROM
89e42e386e82e0cacb4a2704a03706ca  MT32_PCM.ROM
```