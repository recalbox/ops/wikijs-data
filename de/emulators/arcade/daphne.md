---
title: Daphne
description: LaserDisx
published: true
date: 2023-02-22T12:37:42.362Z
tags: arcade, daphne, laserdisc
editor: markdown
dateCreated: 2023-02-22T12:37:42.361Z
---

![](/emulators/arcade/daphne.svg){.align-center}

## Technische Daten

* **Hersteller**: Matt Ownby (Daphne) and DirtBagXon (Hypseus)
* **Jahr der Veröffentlichung**: 1999
* **Website**: [http://www.daphne-emu.com/](http://www.daphne-emu.com/site3/index_hi.php)
* **Daphne** ist ein Programm, mit dem man die Originalversionen vieler Laserdisc-Arcade-Spiele spielen kann.

## Beschreibung

Die **LaserDisc** war das erste optische Speichermedium, zunächst für Video, das 1978 in Nordamerika zunächst unter dem Namen MCA DiscoVision auf den Markt kam.

Obwohl sie eine bessere Ton- und Bildqualität bot als die meisten anderen Medien aus der Zeit (insbesondere VHS- und Betamax-Bänder), war die LaserDisc nicht sehr erfolgreich, vor allem wegen des hohen Preises der Abspielgeräte und der Tatsache, dass mit ihr keine Fernsehprogramme aufgenommen werden konnten. Obwohl sie bei Heimkinobesitzern von Anfang an sehr beliebt war, wurde die LaserDisc in den 1990er Jahren nur in Asien (Hongkong, Malaysia und Singapur) in Privathaushalten eingesetzt.

Dennoch wurden auf der Grundlage der LaserDisc-Technologie verschiedene optische Speichermedien entwickelt, insbesondere die CD und die DVD, die sich als sehr erfolgreich erwiesen haben.

### Was ist eine Laserdisc?

Ein **LaserDisc**-Videospiel ist ein Spiel, das **voraufgezeichnete Videos** (Filme oder Animationen) abspielt, entweder für **alle** oder für **Teilbereiche der Grafik**.
**Einige Spiele** verwenden Videos, in denen **Sprites** (wie z.B. Autos, Schiffe, ...) **überlagert werden.**

Aber die **beliebtesten** Spiele waren **Filme oder interaktive Zeichentrickfilme**, in denen **der Spieler** eine bestimmte Taste **drücken** oder den Joystick **zur richtigen Zeit in die richtige Richtung bewegen** musste, um **zur nächsten Szene zu gelangen.**

### Was ist DAPHNE?

DAPHNE ist ein Emulator; oder eher ein 'Emulator' (weil er als Frau oder <<**F.E.M.A.L.E**>> erdacht wurde: _First Ever Multiple Arcade Laserdisc Emulator_), mit dem man **LaserDisc-Arcade-Spiele** (Marke LaserDisc) spielen kann, wie z.B. **Dragon's Lair** (der Name des Emulators basiert auf dem Namen der Prinzessin im Spiel), **Badlands**, **Cobra Command**, **Space Ace** und viele andere.

Es wurde von **Matt Ownby** im Jahr 1999 auf einem Z80-Prozessor-Emulator entwickelt. Das erste und zugleich berühmteste Spiel, das emuliert wurde, war Dragon's Lair! Zusammen mit anderen Kollegen und Freunden gelang es ihm, fast zwanzig Spiele zu emulieren. Aufgrund der Frustration der Emulator-Community, die ein professionelles Management eines Projekts erwartete, das im Grunde nur "zum Spaß" betrieben wurde, stellte Matt die Veröffentlichung und Aktualisierung im Jahr 2009 jedoch ein.


Matt Ownby arbeitet derzeit an einer Neufassung von DAPHNE und definiert ein echtes Geschäftsmodell, mit dem er in der Lage sein wird, seinen Nutzern Unterstützung in kommerzieller Qualität anzubieten.

## Emulatoren

[Hypseus](hypseus)