---
title: Add Disney+ add-on
description: 
published: true
date: 2024-09-19T09:03:11.519Z
tags: kodi, disney+, add-ons
editor: markdown
dateCreated: 2022-12-27T21:58:39.211Z
---

## Introduction

There is an add-on for Kodi that allows you to play Disney+ videos. This tutorial describes how to install it.

>The add-on is not currently compatible with the Raspberry Pi 4 and 5 because the WideVine video module for 64-bit ARM systems does not yet exist.
>{.is-info}

## Prerequisites

You must enable installation from a zip file in Kodi :

1. Go to `Settings` > `System` > `Add-ons`.

2. Enable the `Unknown sources` option. Confirm when prompted.

## Installation

1. Launch Kodi.

2. You must first install a zip file containing the repository information where the Disney+ add-on is located. Go to the following address and download the following file:

https://slyguy.uk/repository.slyguy.zip

3. Transfer the downloaded zip file to your Recalbox in the desired location in the share.

4. In Kodi, go to `Add-ons` > `Install from zip file` and select the zip file you downloaded. Confirm the confirmation messages if you have any.



5. Once installed, still in `Add-ons`, go to `Install from repository`.



6. Find the plugin in `SlyGuy Repository (slyguy.uk)` > `Video add-ons` > `Disney+`. Open the add-on to install it.



7. Once installed, the add-on should be visible in the list of installed add-ons.



## Usage

You will need to log in before you can use the add-on.

1. Open the add-on.

2. You will automatically be presented with the login screen. Insert your email address and password in the next window.



3. Once you are logged-in, you will show a window asking you which profile to use.



4. Now you can enjoy Disney+.