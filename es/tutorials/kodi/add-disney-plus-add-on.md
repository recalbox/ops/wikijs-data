---
title: Añadir la extensión Disney+
description: 
published: true
date: 2024-09-29T14:09:14.390Z
tags: kodi, disney+, extensiones
editor: markdown
dateCreated: 2023-09-15T07:23:47.711Z
---

## Introducción

Existe una extensión para Kodi que permite reproducir vídeos de Disney+. Este tutorial describe cómo instalarla.

>La extensión no es compatible actualmente con la Raspberry Pi 4 y Pi 5 porque aún no existe el módulo de vídeo WideVine para sistemas ARM de 64 bits.
>{.is-info}

## Requisitos previos

Hay que habilitar la instalación de ficheros zip en Kodi :

1. `Configuración` > `Sistema` > `Extensiones`.

2. Activad la opción `Fuentes desconocidas`. Confirmad.

## Instalación

1. Lanzad Kodi.

2. Lo primero que necesitas para instalar es el fichero zip que contiene información sobre el repositorio donde se encuentra la extensión Disney+. Descargad el siguiente fichero

https://slyguy.uk/repository.slyguy.zip

3. Copiad el zip dentro de vuestra Recalbox en la carpeta compartida `SHARE`.

4. 4. En Kodi, id a `Extensions` > `Install from Zip file` y seleccionad el fichero zip anterior. Confirmad las siguientes ventanas.

5. Una vez instalado, en `Extensiones`, id a `Instalar desde un repositorio`.

6. 6. Busca la extensión `SlyGuy Repository (slyguy.uk)` > `Video Extensions` > `Disney+`. Abridla para instalarla.

7. Una vez instalada, la extensión debería ser visible en la lista de extensiones instaladas.

## Utilización

Hay que iniciar sesión antes de poder utilizar la extensión.

1. Abrid la extensión.

2. Seleccionad "Iniciar sesión". Meted vuestra dirección de correo electrónico y la contraseña de Disney+ en la siguiente ventana.

3. Una vez que hayas iniciado sesión, aparecerá una ventana que os preguntará qué perfil utilizar.

4. Ya podéis disfrutar de Disney+.