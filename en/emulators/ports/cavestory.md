---
title: Cavestory
description: 
published: true
date: 2021-08-08T23:48:43.588Z
tags: ports, cavestory
editor: markdown
dateCreated: 2021-06-29T18:32:25.165Z
---

![](/emulators/ports/cavestory.svg){.align-center}

## Technical data

* **Developer**: Studio Pixel 
* **Editor**: Studio Pixel 
* **Designer**: Daisuke Amaya 
* **Scenario**: Daisuke Amaya 
* **Compositor**: Daisuke Amaya 
* **Plateform**: Windows 
* **Year of release**: 20/12/2004 
* **Genre**: Plateform - Adventure 
* **Mode**: Solo

## Presentation

Cave Story is a free adventure and platform video game released in 2004 for PC, developed in 5 years by **Daisuke "Pixel" Amaya** on his free time. The gameplay is played as a combination of various NES / Famicom games.

Several extra endings have been added, several boss battles, tons of items to collect, all sorts of extra secret bonuses and special challenges, various and considerably different ways of playing the game depending on your actions, competition ranking features and an extremely high replay value. For a freeware game, it's also quite long.

The smooth gameplay of this game plays like a combination of various NES / Famicom games with solid controls, a great cast of characters and dialogue, an interesting plot, good music and beautiful old-school 2D illustrations.

See [http://www.cavestory.org/](http://www.cavestory.org/) for more information.

## Emulators

[Libretro NXEngine](libretro-nxengine)