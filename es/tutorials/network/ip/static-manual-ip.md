---
title: Definir la dirección IP de la conexión Wi-Fi
description: ¿La dirección IP de vuestra Recalbox cambia todo el rato? 
published: true
date: 2023-09-13T06:41:22.403Z
tags: wifi, ip, red, dirección, administración
editor: markdown
dateCreated: 2023-09-13T06:41:22.403Z
---

Recalbox permite definir una dirección IP que se utilizará para la conexión Wi-Fi. Esto se configura en [el archivo de configuración recalbox.conf](./../../../basic-usage/getting-started/recalboxconf-file).

>Recalbox permite definir varios parámetros de red para satisfacer necesidades específicas; un error de configuración puede provocar un conflicto u otros problemas. Lo ideal es que esta configuración se realice a nivel de router. Utilizad este tutorial sólo si sabéis lo que estáis haciendo. 
{.is-warning} 

Abrid el archivo `recalbox.conf` y posicionaros al siguiente nivel:

```ini
## Wifi - static IP
## if you want a static IP address, you must set all 4 values (ip, netmask, gateway and nameservers)
## if any value is missing or all lines are commented out, it will fall back to the
## default of DHCP. For nameservers, you must set at least 1 nameserver.
;wifi.ip=manual ip address (ex: 192.168.1.2)
;wifi.netmask=network mask (ex: 255.255.255.0)
;wifi.gateway=ip address of gateway (ex: 192.168.1.1)
;wifi.nameservers=ip address of domain name servers, space separated (ex: 192.168.1.1 8.8.8.8)
```

Eliminad el carácter `;` al principio de las tres últimas líneas y ajustad los valores como consideréis oportuno. 

```ini
wifi.ip=192.168.1.10
wifi.netmask=255.255.255.0
wifi.gateway=192.168.1.254
wifi.nameservers=8.8.8.8 8.8.4.4
```

Reiniciad vuestra Recalbox para aplicar la nueva configuración utilizando el comando `reboot`. 