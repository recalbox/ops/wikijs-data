---
title: Libretro Flycast-Next
description: 
published: true
date: 2024-07-29T11:28:45.003Z
tags: libretro, naomi-gd, 9.2+, flycast-next
editor: markdown
dateCreated: 2024-07-29T11:28:45.003Z
---

**Libretro Flycast** es un emulador **Sega Dreamcast multiplataforma** capaz de emular la **Naomi GD-ROM.**

## ![](/emulators/license.svg) Licencia

Este núcleo está bajo la licencia [**GPLv2**](https://github.com/flyinghead/flycast/blob/master/LICENSE).

## ![](/emulators/compatibility.png) Compatibilidad

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ❌ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Funcionalidades

| Funcionalidad | Soportado |
| :---: | :---: |
| Reinicio | ✔ |
| Capturas de pantalla | ✔ |
| Guardado | ✔ |
| Opciones del núcleo | ✔ |
| Trucos RetroArch | ✔ |
| Controles | ✔ |
| Remapeo | ✔ |
| Vibración | ✔ |
| Control del disco | ✔ |

## ![](/emulators/bios.svg) BIOS

### Lista de BIOS obligatorios

| Nombre de archivo | Descripción | MD5 | Proporcionado |
| :---: | :---: | :---: | :---: |
| naomi.zip | Formato MAME a partir del Romset 0.154 | eb4099aeb42ef089cfe94f8fe95e51f6 | ❌ |

### Lista de BIOS opcionales

| Nombre de archivo | Descripción | MD5 | Proporcionado |
| :---: | :---: | :---: | :---: |
| airlbios.zip | Naomi Airline Pilots deluxe Bios from MAME | 3f348c88af99a40fbd11fa435f28c69d | ❌ |
| hod2bios.zip | Naomi The House of the Dead 2 Bios from MAME | 9c755171b222fb1f4e1439d5b709dbf1 | ❌ |

### Ubicación

Coloque los BIOS de la siguiente manera:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 dc
┃ ┃ ┃ ┃ ┣ 🗒 **naomi.zip**
┃ ┃ ┃ ┃ ┣ 🗒 **airlbios.zip**
┃ ┃ ┃ ┃ ┣ 🗒 **hod2bios.zip**

## ![](/emulators/roms.png) Roms

Flycast se basa en el romset de **Mame**.

### Romset Mame

¡Solo los roms Naomi **provenientes de un romset MAME 0.135 o superior** son compatibles!
Le recomendamos el último romset Mame 0.258 que traerá su lote de compatibilidad adicional.
Para más información sobre la versión del romset actual: [MameDev](https://www.mamedev.org/release.html).

En Naomi GD-Rom, los roms requieren un archivo `.chd` también proveniente del romset MAME.

>El archivo dat para ordenar tus roms arcade está disponible en la carpeta `/recalbox/share/bios/dc/`
{.is-info}

>Los juegos **Naomi GD-ROM** están en formato `.zip + .chd`
{.is-info}

Ejemplo para el juego **`cfield.zip`**:
Coloque el archivo del juego en la raíz de la carpeta NaomiGD
`/recalbox/share/roms/naomigd/cfield.zip`

Coloque el(los) archivo(s) .chd necesario(s) en una carpeta con el mismo nombre que el juego zip

`/recalbox/share/roms/naomigd/cfield/gdl-0025.chd`

### Ubicación

Coloque los roms de la siguiente manera:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 naomigd
┃ ┃ ┃ ┃ ┣ 🗒 **juego.zip**
┃ ┃ ┃ ┃ ┣ 📁 **juego**
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **juego.chd**

## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

>Para poder conservar sus configuraciones personalizadas durante una actualización, le recomendamos utilizar nuestra funcionalidad [Sobrecargas de configuración](./../../../advanced-usage/configuration-override).
{.is-info}

### Acceder a las opciones

Puede configurar diversas opciones de dos maneras diferentes.

* A través del Menú RetroArch:

┣ 📁 Menú RetroArch
┃ ┣ 📁 Opciones del núcleo
┃ ┃ ┣ 🧩 Nombre_opción

* A través del archivo `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Opciones del núcleo

| Opciones | Valor de las opciones | Variable (para sobrecarga) | Valores de las variables |
| --- | --- | --- | --- |
| Región | `Japón` / `Estados Unidos` ✅ / `Europa` / `Predeterminado` | `reicast_region` | `Japan` / `USA` / `Europe` / `Default` |
| Activar DSP | `Desactivado` / `Activado` ✅ | `reicast_enable_dsp` | `disabled` / `enabled` |
| Permitir botones de servicio NAOMI | `Desactivado` ✅ / `Activado` | `reicast_allow_service_buttons` | `disabled` / `enabled` |
| Definir juegos NAOMI en Free Play | `Desactivado` / `Activado` ✅ | `reicast_force_freeplay` | `disabled` / `enabled` |
| Resolución interna | `320x240` / `640x480` ✅ / `800x600` / `960x720` / `1024x768` / `1280x960` / `1440x1080` / `1600x1200` / `1920x1440` / `2560x1920` / `2880x2160` / `3200x2400` / `3840x2880` / `4480x3360` / `5120x3840` / `5760x4320` / `6400x4800` / `7040x5280` / `7680x5760` / `8320x6240` / `8960x6720` / `9600x7200` / `10240x7680` / `10880x8160` / `11520x8640` / `12160x9120` / `12800x9600` | `reicast_internal_resolution` | `320x240` / `640x480` / `800x600` / `960x720` / `1024x768` / `1280x960` / `1440x1080` / `1600x1200` / `1920x1440` / `2560x1920` / `2880x2160` / `3200x2400` / `3840x2880` / `4480x3360` / `5120x3840` / `5760x4320` / `6400x4800` / `7040x5280` / `7680x5760` / `8320x6240` / `8960x6720` / `9600x7200` / `10240x7680` / `10880x8160` / `11520x8640` / `12160x9120` / `12800x9600` |
| Orientación de la pantalla | `Horizontal` ✅ / `Vertical` | `reicast_screen_rotation` | `horizontal` / `vertical` |
| Ordenación alfa | `Por tira (rápida, menos precisa)` / `Por triángulo (normal)` ✅ / `Por píxel (precisa, pero más lenta)` | `reicast_alpha_sorting` | `per-strip (fast, least accurate)` / `per-triangle (normal)` / `per-pixel (accurate)` |
| Emulación completa del framebuffer | `Desactivado` ✅ / `Activado` | `reicast_emulate_framebuffer` | `disabled` / `enabled` |
| Activar el buffer de RTT (renderizado a textura) | `Desactivado` ✅ / `Activado` | `reicast_enable_rttb` | `disabled` / `enabled` |
| Mipmapping | `Desactivado` / `Activado` ✅ | `reicast_mipmapping` | `disabled` / `enabled` |
| Efectos de niebla | `Desactivado` / `Activado` ✅ | `reicast_fog` | `disabled` / `enabled` |
| Modificador de volumen | `Desactivado` / `Activado` ✅ | `reicast_volume_modifier_enable` | `disabled` / `enabled` |
| Filtrado anisotrópico | `Desactivado` / `2` / `4` ✅ / `8` / `16` | `reicast_anisotropic_filtering` | `off` / `2` / `4` / `8` / `16` |
| Filtrado de texturas | `Predeterminado` ✅ / `Forzar al más cercano` / `Forzar lineal` | `reicast_texture_filtering` | `0` / `1` / `2` |
| Retrasar el intercambio de cuadros | `Desactivado` ✅ / `Activado` | `reicast_delay_frame_swapping` | `disabled` / `enabled` |
| Detectar cambios en el intervalo de sincronización vertical | `Desactivado` ✅ / `Activado` | `reicast_detect_vsync_swap_interval` | `disabled` / `enabled` |
| Filtro de posprocesamiento PowerVR2 | `Desactivado` ✅ / `Activado` | `reicast_pvr2_filtering` | `disabled` / `enabled` |
| Ampliación de texturas (xBRZ) | `Desactivado` ✅ / `x2` / `x4` / `x6` | `reicast_texupscale` | `disabled` / `2` / `4` / `6` |
| Factor máximo del filtro de ampliación de texturas | `256` / `512` / `1024` | `reicast_` | `256` / `512` / `1024` |
| Interpolación de profundidad nativa | `Desactivado` ✅ / `Activado` | `reicast_native_depth_interpolation` | `disabled` / `enabled` |
| Renderizado en múltiples hilos | `Desactivado` / `Activado` ✅ | `reicast_thraded_rendering` | `disabled` / `enabled` |
| Salto de cuadros automático | `Desactivado` ✅ / `Normal` / `Máximo` | `reicast_auto_skip_frame` | `disabled` / `some` / `more` |
| Salto de cuadros | `Desactivado` ✅ / `1` / `2` / `3` / `4` / `5` / `6` | `reicast_frame_skipping` | `disabled` / `1` / `2` / `3` / `4` / `5` / `6` |
| Trucos de pantalla ancha (Reinicio requerido) | `Desactivado` ✅ / `Activado` | `reicast_widescreen_cheats` | `disabled` / `enabled` |
| Hack de pantalla ancha | `Desactivado` ✅ / `Activado` | `reicast_widescreen_hack` | `disabled` / `enabled` |
| SH4 CPU under/overclock | `100 MHz` / `110 MHz` / `120 MHz` / `130 MHz` / `140 MHz` / `150 MHz` / `160 MHz` / `170 MHz` / `180 MHz` / `190 MHz` / `200 MHz` / `210 MHz` / `220 MHz` / `230 MHz` / `240 MHz` / `250 MHz` / `260 MHz` / `270 MHz` / `280 MHz` / `290 MHz` / `300 MHz` | `reicast_sh4clock` | `100` / `110` / `120` / `130` / `140` / `150` / `160` / `170` / `180` / `190` / `200` / `210` / `220` / `230` / `240` / `250` / `260` / `270` / `280` / `290` / `300`  |
| Cargar texturas personalizadas | `Desactivado` ✅ / `Activado` | `reicast_custom_textures` | `disabled` / `enabled` |
| Importar texturas | `Desactivado` ✅ / `Activado` | `reicast_dump_textures` | `disabled` / `enabled` |
| Zona muerta del stick analógico | `0%` / `5%` / `10%` / `15%` ✅ / `20%` / `25%` / `30%` | `reicast_analog_stick_deadzone` | `0%` / `5%` / `10%` / `15%` / `20%` / `25%` / `30%` |
| Zona muerta de los gatillos | `0%` ✅ / `5%` / `10%` / `15%` / `20%` / `25%` / `30%` | `reicast_trigger_deadzone` | `0%` / `5%` / `10%` / `15%` / `20%` / `25%` / `30%` |
| Gatillos digitales | `Desactivado` ✅ / `Activado` | `reicast_digital_triggers` | `disabled` / `enabled` |
| Difundir salidas digitales | `Desactivado` ✅ / `Activado` | `reicast_network_output` | `disabled` / `enabled` |
| Mostrar la mira de la pistola 1 | `Desactivado` ✅ / `Blanco` / `Rojo` / `Verde` / `Azul` | `reicast_lightgun1_crosshair` | `disabled` / `White` / `Red` / `Green` / `Blue` |
| Mostrar la mira de la pistola 2 | `Desactivado` ✅ / `Blanco` / `Rojo` / `Verde` / `Azul` | `reicast_lightgun2_crosshair` | `disabled` / `White` / `Red` / `Green` / `Blue` |
| Mostrar la mira de la pistola 3 | `Desactivado` ✅ / `Blanco` / `Rojo` / `Verde` / `Azul` | `reicast_lightgun3_crosshair` | `disabled` / `White` / `Red` / `Green` / `Blue` |
| Mostrar la mira de la pistola 4 | `Desactivado` ✅ / `Blanco` / `Rojo` / `Verde` / `Azul` | `reicast_lightgun4_crosshair` | `disabled` / `White` / `Red` / `Green` / `Blue` |
| VMU por juego | `Desactivado` ✅ / `VMU A1` / `Todos los VMU` | `reicast_per_content_vmus` | `disabled` / `VMU A1` / `All VMUs` |
| VMU Sounds | `Desactivado` ✅ / `Activado` | `reicast_vmu_sound` | `disabled` / `enabled` |

## ![](/emulators/external-links.png) Enlaces externos

* **Código fuente utilizado**: [https://github.com/flyinghead/flycast](https://github.com/flyinghead/flycast)
