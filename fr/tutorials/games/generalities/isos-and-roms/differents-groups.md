---
title: Les différents groupes
description: 
published: true
date: 2022-01-05T00:13:53.958Z
tags: groupes
editor: markdown
dateCreated: 2021-05-21T08:48:25.290Z
---

Pour savoir quelles roms il vous faut pour une console, vous devez connaître les différents groupes qui les référencent.

La différence entre ces groupes est simple, c'est qu'ils sont chacun spécialisés dans un type de support (disquette, cartouche et CD-ROM).

## FinalBurn Neo (⭐⭐⭐⭐)

### Les fichiers dat

Vous pourrez télécharger ces fichiers sur [ce site](https://github.com/libretro/FBNeo/tree/master/dats).

## Mame (⭐⭐⭐⭐)

### Les fichiers dat

Vous pourrez télécharger ces fichiers sur [ce site](https://www.progettosnaps.net/dats/MAME/).

## No-intro (⭐⭐⭐⭐)

No-Intro se concentre uniquement sur les jeux édités sur **supports cartouches** (Master-System, SNES, N64, Gameboy, etc) .

No-Intro ne garde que les meilleures roms, sans erreur ni changement, et qui sont les plus proches possible des cartouches originales.

No-Intro ne contient généralement qu’une seule version fonctionnelle de chaque jeu pour un pays donné (US, Eur, Jp, Fr, De, ...) ainsi que toutes les mises à jour de ces jeux (Rev1, Rev2, Beta, ...).

>Ces sets sont par conséquent les meilleures roms pour ceux qui veulent utiliser la fonctionnalité Netplay.
{.is-success}

### Les fichiers dat

Vous pourrez télécharger ces fichiers sur ce [site](https://datomatic.no-intro.org/?page=download).

## Redump (⭐⭐⭐⭐)

Redump se concentre uniquement sur les jeux édités sur **supports optiques** (CD, DVD, ...) (PlayStation, Sega Saturn, GameCube, Dreamcast, etc).

Comme No-Intro, Redump ne garde que les meilleures isos, sans erreur ni changement, et qui sont les plus proches possible des supports originaux.

Les Isos Redump sont plus volumineux que les Isos Tosec ou Trurip, mais ils possèdent une meilleure qualité, ce qui influe directement sur les comportement lors de l'émulation.

>Vous trouverez [ici](http://wiki.redump.org/index.php?title=Discs_not_yet_dumped) la liste des disques non dumpés.
{.is-info}

### Les fichiers dat

Vous pourrez télécharger ces fichiers sur ce [site](http://redump.org/downloads/).

## Tosec (⭐⭐⭐)

TOSEC a pour but de lister tout le contenu possible pour chaque console ou ordinateur (Disquettes, Cartouches et CD mais aussi les magazines, manuels et vidéos - Atari-ST, Amiga, Commodore, Dreamcast, Master-System, N64, etc).

TOSEC est un système de catalogage / nommage. Leur mission est d’identifier et de nommer toutes les ROMs trouvées dans la nature.

Ce qui parait louable à première vue mais au final devient extrêmement chaotique.

On obtient les bonnes mais aussi les mauvaises Roms, des fichiers défectueux, des fichiers de sauvegarde supplémentaires, des fichiers de remplacement alternatifs, des roms hack, des faux, des homebrews, des roms modifiées, etc...

On peut ainsi avoir 27 versions différentes d'un même jeu dont 3 seulement sont des roms qui fonctionnent correctement.

Exemple, le Set _TOSEC 2012-12-28_ contient 346 versions différentes de Super Mario World...

>TOSEC est **excellent pour l’archivage**, mais c’est inexploitable pour un usage personnel. C'est pourquoi nous vous conseillons d'utiliser seulement un set Tosec s'il n'existe pas d'alternative en No-Intro ou Redump, prenez bien le temps de le trier soigneusement.
{.is-warning}

### Les fichiers dat

Vous pourrez télécharger ces fichiers sur ce [site](https://www.tosecdev.org/).

## Trurip (⭐⭐)

TruRip se concentre uniquement sur les **jeux basées sur CD**.

TruRip a pour mission d’avoir le duplicata le plus exact du média original, même si cette affirmation peut sembler très similaire aux objectifs de No-Intro et Redump, ce n'est pas le cas.

C'est un autre niveau, ce qui signifie qu'il n'y a pas d'en-têtes ou de réglages pour garantir que les jeux fonctionnent correctement dans les émulateurs.

TruRip ne peut pas faire une copie exacte dans tous les cas, il arrive parfois que des éléments tels que le sous-code manquent encore. C'est à coup sûr le puriste de tout ce qui se passe en ce moment.

TruRip a essayé d'être un groupe privé et a échoué. Il est difficile d'obtenir des informations à ce sujet en ligne.

>Ces sets présentent beaucoup de jeux manquants sur certaines plateformes.
{.is-info}

### Les fichiers dat

Vous pourrez télécharger ces fichiers sur ce [site](http://database.trurip.org/).

## Goodsets/Goodtools (⭐⭐)

>GoodSets se concentre uniquement sur les jeux basés sur certains systèmes à cartouches.
{.is-info}

Le but de GoodSets/GoodTools est très similaire à TOSEC mais légèrement plus raffinée puisqu'ils sont juste un système de catalogage / nommage et non un système de purification comme No-Intro, Redump et TruRip.

Les sets GoodTools sont destinés à être l’ensemble idéal "d’où leurs noms", ils essaient d'avoir chaque jeu et chaque version du jeu.

En conséquence, vous obtiendrez 20 versions de Super Mario (USA, JAP, EUR, USA version piratée V1, JAP Alternate V1, etc) de manière à ce que chaque jeu dispose de toutes les versions des roms.

Ils identifient généralement les roms (les parenthèses, les crochets, etc.. qui sont ajoutés au nom de la ROM, voir [ici](./../tags-used-in-rom-names)).

>Ces sets sont par conséquent un mélange de roms venant de plusieurs groupes. Mais pas forcément la meilleure (pour ceux souhaitant utiliser la fonctionnalité Netplay).
{.is-info}

### Les fichiers dat

Vous pourrez télécharger ces fichiers sur [ce site](https://cowering.blogspot.com/).