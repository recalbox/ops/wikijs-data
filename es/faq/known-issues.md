---
title: Problemas conocidos
description: Cada problema tiene su solución
published: true
date: 2023-09-23T14:23:26.623Z
tags: problemas, conocidos, existentes
editor: markdown
dateCreated: 2023-09-23T14:23:26.623Z
---

En esta página enumeramos los casos de uso que pueden causar problemas, y las respuestas y soluciones aportadas por el equipo de desarrollo. 

## Gestor Web: el editor de roms no funciona más

El gestor de roms del Web Manager es una funcionalidad antigua que ha sido sustituida por el editor de roms implementado directamente en la interfaz de Recalbox.

Aunque la interfaz del Web Manager sigue disponible, no funciona bien (no se muestran las imágenes, el editor no muestra los detalles de los juegos, etc.). 

En lugar del Web Manager, ahora recomendamos utilizar la interfaz de EmulationStation de Recalbox para editar vuestras roms. 