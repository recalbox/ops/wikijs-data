---
title: Softpatching
description: Jouer avec des jeux patchés à la volée
published: true
date: 2024-11-11T05:38:25.390Z
tags: 8.1+, softpatching
editor: markdown
dateCreated: 2022-05-19T22:48:02.143Z
---

## Qu'est-ce que le softpatching ?

Le « softpatching » consiste à appliquer sur un jeu un patch sans créer de nouvelle rom. Vous pouvez jouer à un jeu avec ou sans le patch en fonction de son action.

Les patchs vont de la petite triche jusqu'à la traduction complète de jeux japonais en anglais ou en français.

> Vous ne pouvez utiliser plusieurs patchs en même temps par jeu.
{.is-info}

## Fonctionnement

Son utilisation est très simple et se fait en 2 temps :

### Mise en place du patch

Tout d'abord, vous devez mettre votre patch dans la même répertoire que la rom à patcher. Une fois fait, le patch doit avoir le même nom que la rom excepté son extension de fichier.

> Un patch peut avoir l'extension `.UPS`, `.IPS` ou `.BPS`.
{.is-info}

### Activation du patch

Le patch est mis en place et est renommé ? Vous devez maintenant définir quoi faire avec ce patch. Souhaitez-vous qu'il s'applique à chaque lancement automatiquement ou selon votre choix du moment ? Ou souhaitez-vous ne pas l'appliquer pour le moment ?

Pour cela, dans EmulationStation, dans la liste des systèmes, appuyez sur `Start` et allez dans `OPTIONS DES JEUX`. Vous aurez une option nommée `SOFTPATCHING` avec 3 choix possibles :

- `AUTO` : appliquera le patch tout le temps automatiquement
- `SÉLECTIONNER` : vous demandera d'appliquer le patch si vous le souhaitez
- `DÉSACTIVÉ` : ignorera complètement le patch si il en existe un

Une fois votre choix fait, il suffit de lancer le jeu à patcher.

### Activation des patchs

Depuis Recalbox 9, vous pouvez avoir plusieurs patchs pour un même jeu !

Dans le même dossier que le jeu qui a plusieurs patchs, créez un dossier ayant le nom du jeu suivi de « -patches ». Exemple :

┣ 📁 nes
┃ ┣ 📁 Legend of Zelda, The (USA) (Rev 1)-patches
┃ ┃ ┣ 🗒 ZeldaC.ips
┃ ┃ ┣ 🗒 Zelda - The Legend of Link (v3-12-20).ips
┃ ┃ ┣ 🗒 Un autre patch.ups
┃ ┣ 🗒 Legend of Zelda, The (USA) (Rev 1).zip

Une fois cela fait et à condition d'avoir choisi l'option `SÉLECTIONNER`, au démarrage du jeu, vous aurez un message vous demandant le(s) patch(s) à appliquer sur votre jeu !

## Utilisation de plusieurs patchs en même temps

Vous avez la possibilité d'utiliser plusieurs patchs en même temps. Pour cela, vous devez nommer les patchs correspondants d'une certaine façon :

- Les patchs qui doivent s'appliquer en même temps doivent avoir le même nom
- Les extensions de chaque patch ont un chiffre à la fin, indiquant l'ordre d'application

Par exemple, vous avez le jeu `rom.bin` auquel vous avez plusieurs patchs à appliquer. Vous pouvez les nommer comme ceci :

- rom.ips
- rom.ips1
- rom.ips2

Ici, `rom.ips` sera le premier patch à être appliqué, `rom.ips1` sera le second patch à être appliqé, etc.

Aussi, pour que ceci soit le plus automatique possible, dans le menu de l'interface, faites `START`, allez dans `OPTIONS DES JEUX` et mettez l'option `SOFTPATCHING` sur `AUTO`.

## Application d'un patch unique automatiquement à chaque démarrage

Vous avez aussi la possibilité de vouloir toujours lancer un jeu avec le même patch. Pour cela, il y a deux choses à effectuer :

- Dans l'interface, faites `START`, allez dans `OPTIONS DES JEUX` et pour l'option `SOFTPATCHING`, choisissez `LAUNCH LAST`.
- Toujours dans l'interface, sur le jeu à appliquer tout le temps le patch, faites `START` puis `MODIFIER LE JEU` et sur l'option `SOFTPATCHING`, choisissez le patch à appliquer.

Avec ceci, vous n'aurez pas la fenêtre de choix si vous souhaitez appliquer un patch ou non et ce jeu se lancera toujours patché.

## Liste d'émulateurs compatibles

Voici la liste des émulateurs compatibles avec les patchs :

- ColecoVision
  - Libretro GearColeco
- Famicom Disk System
  - Libretro FCEUmm
- Game Boy / Game Boy Color
  - Libretro bsnes
  - Libretro Gambatte
  - Libretro Mesen_S
  - Libretro mGBA
  - Libretro TGB Dual
- Game Boy Advance
  - Libretro mGBA
  - Libretro Meteor
- Master System
  - Libretro Gearsystem
- Megadrive
  - Libretro GenesisPlus GX
  - Libretro GenesisPlus GX Wide
  - Libretro Picodrive
- Neo Geo Pocket / Neo Geo Pocket Color
  - Libretro Mednafen_NGP
- NES
  - Libretro FCEUmm
  - Libretro FCEUNext
  - Libretro Mesen
  - Libretro Nestopia
  - Libretro QuickNES
- Nintendo 64
  - Libretro Mupen64Plus NX
- Satellaview
  - Libretro bsnes
  - Libretro Snes9x
- SNES
  - Libretro bsnes
  - Libretro bsnes HD
  - Libretro Mesen_S
  - Libretro Snes9x
  - Libretro Snes9x 2002
  - Libretro Snes9x 2005
  - Libretro Snes9x 2010
- WonderSwan/Color
  - Libretro Mednafen_WSWAN

## Adresses de patchs

GohanSenior nous a préparé une sélection de près d'un millier de patchs de traduction française des jeux 8 et 16 bits !

Pour en profiter, il vous faudra : 
- utiliser les romsets no-intro
- télécharger le dossier des patchs a l'adresse https://github.com/GohanSenior/Pack-TradFr-Sets-No-Intro-Softpatching-Recalbox/archive/refs/heads/main.zip (lien vers le github: https://github.com/GohanSenior/Pack-TradFr-Sets-No-Intro-Softpatching-Recalbox/
- décompresser le tout dans votre dossier roms
- activer le softpatching dans les options Recalbox
- Sélectionner le patch lorsque Recalbox vous demande comment lancer votre jeu

Et vous voila avec un millier de jeux traduits en français !

N'hésitez pas à le contacter sur notre discord pour le remercier ou pour remonter des problèmes que vous pourriez rencontrer !
Vous trouverez d'autres patchs utilisables sur les sites suivants :

- https://traf.romhack.org/
- https://terminus.romhack.net/