---
title: Libretro Mesen
description: 
published: true
date: 2024-06-22T23:48:02.366Z
tags: libretro, famicom, mesen, family computer disk system
editor: markdown
dateCreated: 2021-05-21T08:20:35.807Z
---

Libretro Mesen est un émulateur NES et Famicom de haute précision et un lecteur NSF.

## ![](/emulators/license.svg) Licence

Ce core est sous licence [**GPLv3**](https://github.com/SourMesen/Mesen/blob/master/README.md).

## ![](/emulators/compatibility.png) Compatibilité

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ❌ | ❌ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Fonctionnalités

| Fonctionnalité | Supporté |
| :---: | :---: |
| Redémarrage | ✔ |
| Captures d'écran | ✔ |
| Sauvegardes | ✔ |
| Sauvegardes instantanées | ✔ |
| Rembobinage | ✔ |
| Netplay | ✔ |
| Options du core | ✔ |
| RetroAchievements | ✔ |
| Cheats RetroArch | ✔ |
| Controls | ✔ |
| Remapping | ✔ |
| Softpatching | ✔ |

## ![](/emulators/bios.svg) BIOS

### Liste des bios obligatoires

| Nom de fichier | Description | MD5 | Fourni |
| :---: | :---: | :---: | :---: |
| disksys.rom | BIOS Family Computer Disk System | ca30b50f880eb660a320674ed365ef7a | ❌  |

### Emplacement

Placez les bios comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 🗒 **disksys.rom**

## ![](/emulators/roms.png) Roms

### Extensions supportées

Les roms doivent avoir les extensions suivantes :

* .fds
* .zip
* .7z

Ce système supporte les roms compressées au format .zip/.7z. Attention toutefois, il ne s'agit que d'une archive.

Les fichiers contenus dans les .zip/.7z doivent correspondre aux extensions citées précédemment.
De plus, chaque fichier .zip/.7z ne doit contenir qu'une seule rom compressée.

### Emplacement

Placez les roms comme ceci : 

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 fds
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.zip**

>Les roms au format **No-Intro** sont vivement conseillées.
{.is-success}

>Pour plus d'information sur les roms, rendez-vous sur [ce tutoriel](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuration avancée de l'émulateur

>Pour pouvoir conserver vos configurations personnalisées lors d'une mise à jour, nous vous conseillons d'utiliser notre fonctionnalité [Surcharges de configuration](./../../../advanced-usage/configuration-override).
{.is-info}

### Accéder aux options

Vous pouvez configurer diverses options de deux façons différentes.

* Via le menu RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via le fichier `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Options du core

| Options | Valeur des options | Variable (pour surcharge) | Valeurs des variables |
| --- | --- | --- | --- |
| NTSC filter | `Désactivé` ✅ / `Composite (Blargg)` / `S-Video (Blargg)` / `RGB (Blargg)` / `Monochrome (Blargg)` / `Bisqwit 2x` / `Bisqwit 4x` / `Bisqwit 8x` | `mesen_ntsc_filter` | `Disabled` / `Composite (Blargg)` / `S-Video (Blargg)` / `RGB (Blargg)` / `Monochrome (Blargg)` / `Bisqwit 2x` / `Bisqwit 4x` / `Bisqwit 8x` |
| Palette | `Default` ✅ / `Composite Direct (by FirebrandX)` / `Nes Classic` / `Nestopia (RGB)` / `Original Hardware (by FirebrandX)` / `PVM Style (by FirebrandX)` / `Sony CXA2025AS` / `Unsaturated v6 (by FirebrandX)` / `YUV v3 (by FirebrandX)` / `Wavebeam (by nakedarthur)` / `Custom` / `Raw` | `mesen_palette` | `Default` / `Composite Direct (by FirebrandX)` / `Nes Classic` / `Nestopia (RGB)` / `Original Hardware (by FirebrandX)` / `PVM Style (by FirebrandX)` / `Sony CXA2025AS` / `Unsaturated v6 (by FirebrandX)` / `YUV v3 (by FirebrandX)` / `Wavebeam (by nakedarthur)` / `Custom` / `Raw` |
| Overclock | `None` ✅ / `Low` / `Medium` / `High` / `Very High` | `mesen_overclock` | `None` / `Low` / `Medium` / `High` / `Very High` |
| Overclock Type | `Before NMI (Recommended)` ✅ / `After NMI` | `mesen_overclock_type` | `Before NMI (Recommended)` / `After NMI` |
| Region | `Auto` ✅ / `NTSC` / `PAL` / `Dendy` | `mesen_region` | `Auto` / `NTSC` / `PAL` / `Dendy` |
| Left Overscan | `None` ✅ / `4px` / `8px` / `12px` / `16px` | `mesen_overscan_left` | `None` / `4px` / `8px` / `12px` / `16px` |
| Right Overscan | `None` ✅ / `4px` / `8px` / `12px` / `16px` | `mesen_overscan_right` | `None` / `4px` / `8px` / `12px` / `16px` |
| Top Overscan | `None` ✅ / `4px` / `8px` / `12px` / `16px` | `mesen_overscan_top` | `None` / `4px` / `8px` / `12px` / `16px` |
| Bottom Overscan | `None` ✅ / `4px` / `8px` / `12px` / `16px` | `mesen_overscan_bottom` | `None` / `4px` / `8px` / `12px` / `16px` |
| Aspect Ratio | `Auto` ✅ / `No Stretching` / `NTSC` / `PAL` / `4:3` / `16:9` | `mesen_aspect_ratio` | `Auto` / `No Stretching` / `NTSC` / `PAL` / `4:3` / `16:9` |
| Controller Turbo Speed | `Fast` ✅ / `Very Fast` / `Désactivé` / `Slow` / `Normal` | `mesen_controllerturbospeed` | `Fast` / `Very Fast` / `Disabled` / `Slow` / `Normal` |
| Shift A/B/X/Y clockwise | `Désactivé` ✅ / `Activé` | `mesen_shift_buttons_clockwise` | `disabled` / `enabled` |
| Enable HD Packs | `Activé` ✅ / `Désactivé` | `mesen_hd_packs` | `enabled` / `disabled` |
| Remove sprite limit | `Désactivé` ✅ / `Activé` | `mesen_no_sprite_limit` | `disabled` / `enabled` |
| Enable fake stereo effect | `Désactivé` ✅ / `Activé` | `mesen_fake_stereo` | `disabled` / `enabled` |
| Reduce popping on Triangle channel | `Activé` ✅ / `Désactivé` | `mesen_mute_triangle_ultrasonic` | `enabled` / `disabled` |
| Reduce popping on DMC channel | `Activé` ✅ / `Désactivé` | `mesen_reduce_dmc_popping` | `enabled` / `disabled` |
| Swap Square channel duty cycles | `Désactivé` ✅ / `Activé` | `mesen_swap_duty_cycle` | `disabled` / `enabled` |
| Disable Noise channel mode flag | `Désactivé` ✅ / `Activé` | `mesen_disable_noise_mode_flag` | `disabled` / `enabled` |
| Screen rotation | `None` / `90 degrees` ✅ / `180 degrees` / `270 degrees` | `mesen_screenrotation` | `None` / `90 degrees` / `180 degrees` / `270 degrees` |
| Default power-on state for RAM | `All 0s (Default)` ✅ / `All 1s` / `Random Values` | `mesen_ramstate` | `All 0s (Default)` / `All 1s` / `Random Values` |
| FDS: Automatically insert disks | `Désactivé` ✅ / `Activé` | `mesen_fdsautoselectdisk` | `disabled` / `enabled` |
| FDS: Fast forward while loading | `Désactivé` ✅ / `Activé` | `mesen_fdsfastforwardload` | `disabled` / `enabled` |
| Sound Output Sample Rate | `96000` ✅ / `192000` / `384000` / `11025` / `22050` / `44100` / `48000` | `mesen_audio_sample_rate` | `96000` / `192000` / `384000` / `11025` / `22050` / `44100` / `48000` |

## ![](/emulators/external-links.png) Liens externes

* **Code source utilisé** : [https://github.com/libretro/Mesen/](https://github.com/libretro/Mesen/)
* **Documentation Libretro** : [https://docs.libretro.com/library/mesen/](https://docs.libretro.com/library/mesen/)