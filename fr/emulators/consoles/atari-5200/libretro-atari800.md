---
title: Libretro Atari800
description: 
published: true
date: 2024-07-24T09:44:00.108Z
tags: libretro, atari, atari800, 5200, atari-5200
editor: markdown
dateCreated: 2021-05-21T08:19:34.621Z
---



## ![](/emulators/license.svg) Licence

Ce core est sous licence [**GPLv2**](https://github.com/atari800/atari800/blob/master/COPYING).

## ![](/emulators/compatibility.png) Compatibilité

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Fonctionnalités

| Fonctionnalité | Supporté |
 | :---: | :---: |
 | Captures d'écran | ✔ |
 | Sauvegardes | - |
 | Sauvegardes instantanées | - |
 | Options du core | ✔ |
 | Native Cheats | - |
 | Controls | ✔ |
 | Remapping | ✔ |

## ![](/emulators/bios.svg) BIOS

### Liste des bios obligatoires

| Nom de fichier | Description | MD5 | Fourni |
| :---: | :---: | :---: | :---: |
| 5200.rom | BIOS 5200 | 281f20ea4320404ec820fb7ec0693b38 | ❌ |

### Emplacement

Placez les bios comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 atari5200
┃ ┃ ┃ ┃ ┣ 🗒 **5200.rom**

## ![](/emulators/roms.png) Roms

### Extensions supportées

Les roms doivent avoir les extensions suivantes :

* .a52
* .zip

Ce système supporte les roms compressées au format .zip. Attention toutefois, il ne s'agit que d'une archive.

Les fichiers contenus dans les .zip doivent correspondre aux extensions citées précédemment.
De plus, chaque fichier .zip ne doit contenir qu'une seule rom compressée.

### Emplacement

Placez les roms comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 atari5200
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.zip**

>Les roms au format **TOSEC** sont vivement conseillées.
{.is-success}

>Pour plus d'information sur les roms, rendez-vous sur [ce tutoriel](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuration avancée de l'émulateur

>Pour pouvoir conserver vos configurations personnalisées lors d'une mise à jour, nous vous conseillons d'utiliser notre fonctionnalité [Surcharges de configuration](./../../../advanced-usage/configuration-override).
{.is-info}

### Accéder aux options

Vous pouvez configurer diverses options de deux façons différentes.

* Via le menu RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via le fichier `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Options du core

| Options | Valeur des options | Variable (pour surcharge) | Valeurs des variables |
| --- | --- | --- | --- |
| Video Standard | `NTSC` ✅ / `PAL` | `atari800_ntscpal` | `NTSC` / `PAL` |
| Hi-Res Artifacting | `None` ✅ / `blue/brown 1` / `blue/brown 2` / `GTIA` / `CTIA` | `atari800_artifacting` | `none` / `blue/brown 1` / `blue/brown 2` / `GTIA` / `CTIA` |
| Internal resolution | `336x240` ✅ / `320x240` / `384x240` / `384x272` / `384x288` / `400x300` | `atari800_resolution` | `336x240` / `320x240` / `384x240` / `384x272` / `384x288` / `400x300` |
| Controller Hacks | `none` ✅ / `Dual Stick` / `Swap Ports` | `atari800_opt2` | `none` / `enabled` / `Swap Ports` |
| Activate Paddle Mode | `Désactivé` ✅ /  `Activé` | `paddle_active` | `disabled` / `enabled` |
| Paddle Movement Speed | `1` / `2` / `3` ✅ / `4` / `5` / `6` / `7` / `8` / `9` | `paddle_movement_speed` | `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` |
| Digital Joystick Sensitivity | `5%` / `10%` / `15%` / `20%` / `25%` / `30%` / `35%` / `40%` / `45%` / `50%` / `55%` / `60%` / `65%` / `70%` / `75%` / `80%` / `85%` / `90%` / `95%` / `100%` ✅ | `pot_digital_sensitivity` | `5` / `10` / `15` / `20` / `25` / `30` / `35` / `40` / `45` / `50` / `55` / `60` / `65` / `70` / `75` / `80` / `85` / `90` / `95` / `100` |
| Analog Joystick Sensitivity | `5%` / `10%` / `15%` / `20%` / `25%` / `30%` / `35%` / `40%` / `45%` / `50%` / `55%` / `60%` / `65%` / `70%` / `75%` / `80%` / `85%` / `90%` / `95%` / `100%` ✅ | `pot_analog_sensitivity` | `5` / `10` / `15` / `20` / `25` / `30` / `35` / `40` / `45` / `50` / `55` / `60` / `65` / `70` / `75` / `80` / `85` / `90` / `95` / `100` |
| Analog Joystick Deadzone | `0%` / `3%` / `5%` / `7%` / `10%` / `13%` / `15%` ✅ / `17%` / `20%` / `23%` / `25%` / `27%` / `30%` | `pot_analog_deadzone` | `0` / `3` / `5` / `7` / `10` / `13` / `15` / `17` / `20` / `23` / `25` / `27` / `30` |
| Retroarch Keyboard type | `poll` ✅ / `callback` | `atari800_keyboard` | `poll` / `callback` |
| Atari Keyboard Defines | `informational` ✅ | `keyboard_defines` | `informational` |
| Atari System | `Atari 400/800 (OS B)` ✅ / `Atari 800XL (64K)` / `Atari 130XE (128K)` / `Modern XL/XE(320K Comby Shop)` / `Modern XL/XE(576K)` / `Modern XL/XE(1088K)` / `Atari 5200 Super System` | `atari800_system` | `400/800 (OS B)` / `800XL (64K)` / `130XE (128K)` / `Modern XL/XE(320K CS)` / `Modern XL/XE(576K)` / `Modern XL/XE(1088K)` / `5200` |
| Internal BASIC (hold OPTION on boot) | `Désactivé` ✅ / `Activé` | `atari800_internalbasic` | `disabled` / `enabled` |
| SIO Acceleration | `Désactivé` / `Activé` ✅ | `atari800_sioaccel` | `disabled` / `enabled` |
| Boot from Cassette (Reboot) | `Désactivé` ✅ / `Activé` | `atari800_cassboot` | `disabled` / `enabled` |
| Autodetect Atari Cartridge Type (Restart) | `Désactivé` ✅ / `Activé` | `atari800_opt1` | `disabled` / `enabled` |

## ![](/emulators/external-links.png) Liens externes

* **Code source utilisé** : [https://github.com/atari800/atari800/](https://github.com/atari800/atari800/)
* **Documentation Libretro** : [https://docs.libretro.com/library/atari800/](https://docs.libretro.com/library/atari800/)