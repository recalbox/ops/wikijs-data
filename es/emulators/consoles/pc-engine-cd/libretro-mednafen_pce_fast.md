---
title: Libretro Mednafen_PCE_FAST
description: 
published: true
date: 2024-07-29T15:21:29.026Z
tags: libretro, mednafen, pce fast, pc-engine cd
editor: markdown
dateCreated: 2023-09-07T07:19:27.519Z
---

**Libretro Mednafen_PCE_FAST** es un port libretro de Mednafen PCE Fast con el módulo PC Engine SuperGrafx retirado.

## ![](/emulators/license.svg) Licencia

Este núcleo está bajo la licencia [**GPLv2**](https://github.com/libretro/beetle-pce-fast-libretro/blob/master/COPYING).

## ![](/emulators/compatibility.png) Compatibilidad

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Funcionalidades

| Funcionalidad | Soportado |
| :---: | :---: |
| Reinicio | ✔ |
| Guardados | ✔ |
| Guardados instantáneos | ✔ |
| Rembobinado | ✔ |
| Netplay | ✔ |
| Opciones del núcleo | ✔ |
| RetroAchievements | ✔ |
| Controles | ✔ |
| Remapeo | ✔ |

## ![](/emulators/bios.svg) BIOS

### Lista de BIOS obligatorios

| Nombre de archivo | Descripción | MD5 | Proporcionado |
| :---: | :---: | :---: | :---: |
| syscard3.pce | PC-Engine Super CD-ROM^2 System Card v3.0 | ff1a674273fe3540ccef576376407d1d 1e47a1780aa7e277a3896c1ba00e317 38179df8f4ac870017db21ebcbf53114 | ❌ |

### Ubicación

Coloque los BIOS de la siguiente manera:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 pcenginecd
┃ ┃ ┃ ┃ ┣ 🗒 **syscard3.pce**

### Lista de BIOS opcionales

| Nombre de archivo | Descripción | MD5 | Proporcionado |
| :---: | :---: | :---: | :---: |
| syscard1.pce | PC-Engine CD-ROM System Card v1.0 | 9f770275393b8627cf9d24e5c56d2ab9 | ❌ |
| syscard2.pce | PC-Engine CD-ROM^2 System Card v2.0 | 424f1d6bf93259bf255afa7d1dc9f721 | ❌ |
| syscard2u.pce | TurboGrafx-CD System Card | 94279f315e8b52904f65ab3108542afe | ❌ |
| syscard3u.pce | TurboGrafx-CD Super System Card | 0754f903b52e3b3342202bdafb13efa5 | ❌ |
| gexpress.pce | Game Express CD Card | 6d2cb14fc3e1f65ceb135633d1694122 | ❌ |

### Ubicación

Coloque los BIOS de la siguiente manera:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 pcenginecd
┃ ┃ ┃ ┃ ┣ 🗒 **syscard1.pce**
┃ ┃ ┃ ┃ ┣ 🗒 **syscard2.pce**
┃ ┃ ┃ ┃ ┣ 🗒 **syscard2u.pce**
┃ ┃ ┃ ┃ ┣ 🗒 **syscard3u.pce**
┃ ┃ ┃ ┣ 📁 pcengine
┃ ┃ ┃ ┃ ┣ 🗒 **gexpress.pce**

## ![](/emulators/isos.svg) Isos

### Extensiones soportadas

Los isos deben tener las siguientes extensiones:

* .cue/.bin
* .ccd/.img
* .chd
* .m3u
* .toc

### Ubicación

Coloque los isos de la siguiente manera:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 pcenginecd
┃ ┃ ┃ ┃ ┣ 🗒 **juego.cue**
┃ ┃ ┃ ┃ ┣ 🗒 **juego.bin**

>Los isos en formato **Redump** son vivamente recomendados.
{.is-success}

>Para más información sobre los isos, visite [este tutorial](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

>Para poder conservar sus configuraciones personalizadas durante una actualización, le recomendamos utilizar nuestra funcionalidad [Sobrecargas de configuración](./../../../advanced-usage/configuration-override).
{.is-info}

### Acceder a las opciones

Puede configurar diversas opciones de dos maneras diferentes.

* A través del menú RetroArch:

┣ 📁 Menú RetroArch
┃ ┣ 📁 Opciones del núcleo
┃ ┃ ┣ 🧩 Name_option

* A través del archivo `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Opciones del núcleo

| Opciones | Valor de las opciones | Variable (para sobrecarga) | Valores de las variables |
| --- | --- | --- | --- |
| Paleta de colores | `RVB` ✅ / `Composite` | `pce_color_palette` | `RGB` / `Composite` |
| Salto de imágenes | `Desactivado` ✅ / `Auto` / `Manual` | `pce_fast_frameskip` | `disabled` / `auto` / `manual` |
| Umbral de salto de imágenes (%) | `15` / `18` / `21` / `24` / `27` / `30` / `33 (por defecto)` ✅ / `36` / `39` / `42` / `45` / `48` / `51` / `54` / `57` / `60` | `pce_fast_frameskip_threshold` | `15` / `18` / `21` / `24` / `27` / `30` / `33` / `36` / `39` / `42` / `45` / `48` / `51` / `54` / `57` / `60` |
| Sobrecarga horizontal (modo de línea grande 352 únicamente) | `300` / `302` / `304` / `306` / `308` / `310` / `312` / `314` / `316` / `318` / `320` / `322` / `324` / `326` / `328` / `330` / `332` / `334` / `336` / `338` / `340` / `342` / `344` / `346` / `348` / `350` / `352 (por defecto)` ✅ | `pce_hoverscan` | `300` / `302` / `304` / `306` / `308` / `310` / `312` / `314` / `316` / `318` / `320` / `322` / `324` / `326` / `328` / `330` / `332` / `334` / `336` / `338` / `340` / `342` / `344` / `346` / `348` / `350` / `352` |
| Línea de escaneo inicial | `0` / `1` / `2` / `3 (por defecto)` ✅ / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `21` / `22` / `23` / `24` / `25` / `26` / `27` / `28` / `29` / `30` / `31` / `32` / `33` / `34` / `35` / `36` / `37` / `38` / `39` / `40` | `pce_initial_scanline` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `21` / `22` / `23` / `24` / `25` / `26` / `27` / `28` / `29` / `30` / `31` / `32` / `33` / `34` / `35` / `36` / `37` / `38` / `39` / `40` |
| Última línea de escaneo | `208` / `209` / `210` / `211` / `212` / `213` / `214` / `215` / `216` / `217` / `218` / `219` / `220` / `221` / `222` / `223` / `224` / `225` / `226` / `227` / `228` / `229` / `230` / `231` / `232` / `233` / `234` / `235` / `236` / `237` / `238` / `239` / `240` / `241` / `242 (por defecto)` ✅ | `pce_last_scanline` | `208` / `209` / `210` / `211` / `212` / `213` / `214` / `215` / `216` / `217` / `218` / `219` / `220` / `221` / `222` / `223` / `224` / `225` / `226` / `227` / `228` / `229` / `230` / `231` / `232` / `233` / `234` / `235` / `236` / `237` / `238` / `239` / `240` / `241` / `242` |
| Volumen del canal de audio PSG 0 (%) | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `pce_fast_sound_channel_0_volume` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
| Volumen del canal de audio PSG 1 (%) | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `pce_fast_sound_channel_1_volume` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
| Volumen del canal de audio PSG 2 (%) | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `pce_fast_sound_channel_2_volume` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
| Volumen del canal de audio PSG 3 (%) | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `pce_fast_sound_channel_3_volume` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
| Volumen del canal de audio PSG 4 (%) | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `pce_fast_sound_channel_4_volume` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
| Volumen del canal de audio PSG 5 (%) | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `pce_fast_sound_channel_5_volume` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
| Sensibilidad del ratón | `0.25` / `0.50` / `0.75` / `1.00` / `1.25` ✅ / `1.50` / `1.75` / `2.00` / `2.25` / `2.50` / `2.75` / `3.00` / `3.25` / `3.50` / `3.75` / `4.00` / `4.25` / `4.50` / `4.75` / `5.00` | `pce_mouse_sensitivity` | `0.25` / `0.50` / `0.75` / `1.00` / `1.25` / `1.50` / `1.75` / `2.00` / `2.25` / `2.50` / `2.75` / `3.00` / `3.25` / `3.50` / `3.75` / `4.00` / `4.25` / `4.50` / `4.75` / `5.00` |
| Desactivar el reinicio lógico (RUN+SELECT) | `Desactivado` ✅ / `Activado` | `pce_disable_softreset` | `disabled` / `enabled` |
| Tipo de joypad por defecto para el jugador 1 | `2 botones` ✅ / `6 botones` | `pce_fast_default_joypad_type_p1` | `2 Buttons` / `6 Buttons` |
| Tipo de joypad por defecto para el jugador 2 | `2 botones` ✅ / `6 botones` | `pce_fast_default_joypad_type_p2` | `2 Buttons` / `6 Buttons` |
| Tipo de joypad por defecto para el jugador 3 | `2 botones` ✅ / `6 botones` | `pce_fast_default_joypad_type_p3` | `2 Buttons` / `6 Buttons` |
| Tipo de joypad por defecto para el jugador 4 | `2 botones` ✅ / `6 botones` | `pce_fast_default_joypad_type_p4` | `2 Buttons` / `6 Buttons` |
| Tipo de joypad por defecto para el jugador 5 | `2 botones` ✅ / `6 botones` | `pce_fast_default_joypad_type_p5` | `2 Buttons` / `6 Buttons` |
| Activar/desactivar el turbo | `Desactivado` ✅ / `Activado` | `pce_turbo_toggling` | `disabled` / `enabled` |
| Tecla de activación alternativa del turbo | `Desactivado` ✅ / `Activado` | `pce_turbo_toggle_hotkey` | `disabled` / `enabled` |
| Retraso del turbo | `1` / `2` / `3 (por defecto)` ✅ / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` | `pce_turbo_delay` | `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` |
| Caché de imágenes CD (Reinicio requerido) | `Desactivado` ✅ / `Activado` | `pce_fast_cdimagecache` | `disabled` / `enabled` |
| BIOS CD (Reinicio requerido) | `Game Express` / `System Card 1` / `System Card 2` / `System Card 3` ✅ / `System Card 2 US` / `System Card 3 US` | `pce_fast_cdbios` | `Game Express` / `System Card 1` / `System Card 2` / `System Card 3` / `System Card 2 US` / `System Card 3 US` |
| Velocidad del CD | `x1` ✅ / `x2` / `x4` / `x8` | `pce_cdspeed` | `1` / `2` / `4` / `8` |
| Volumen ADPCM (%) | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ / `110` / `120` / `130` / `140` / `150` / `160` / `170` / `180` / `190` / `200` | `pce_adpcmvolume` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` / `110` / `120` / `130` / `140` / `150` / `160` / `170` / `180` / `190` / `200` |
| Volumen CDDA (%) | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ / `110` / `120` / `130` / `140` / `150` / `160` / `170` / `180` / `190` / `200` | `pce_cddavolume` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` / `110` / `120` / `130` / `140` / `150` / `160` / `170` / `180` / `190` / `200` |
| Volumen PSG (%) | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ / `110` / `120` / `130` / `140` / `150` / `160` / `170` / `180` / `190` / `200` | `pce_cdpsgvolume` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` / `110` / `120` / `130` / `140` / `150` / `160` / `170` / `180` / `190` / `200` |
| Sin límite de sprites | `Desactivado` ✅ / `Activado` | `pce_nospritelimit` | `disabled` / `enabled` |
| Multiplicador de overclocking del procesador (Reinicio requerido) | `1` ✅ / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `20` / `30` / `40` / `50` | `pce_ocmultiplier` | `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `20` / `30` / `40` / `50` |

## ![](/emulators/external-links.png) Enlaces externos

* **Código fuente utilizado** : [https://github.com/libretro/beetle-pce-fast-libretro/](https://github.com/libretro/beetle-pce-fast-libretro/)
* **Documentación Libretro** : [https://docs.libretro.com/library/beetle_pce_fast/](https://docs.libretro.com/library/beetle_pce_fast/)
