---
title: Netplay (Jeux en Ligne)
description: 
published: true
date: 2025-02-26T18:53:34.737Z
tags: netplay, jeux, en ligne
editor: markdown
dateCreated: 2021-05-21T07:53:37.491Z
---

![](/basic-usage/features/netplay/logo-recalbox_netplay.png)

## Introduction

Le Netplay est une fonctionnalité permettant le mode multijoueur en réseau avec certains émulateurs via la mise en réseau peer-to-peer (P2P) pour réduire la latence du réseau, afin de garantir la meilleure expérience possible en synchronisant en continu plusieurs instances exécutant le même noyau d'émulation et le même contenu.
Vous avez également la possibilité d’avoir aussi des personnes pouvant rejoindre en mode spectateur (latence supplémentaire).
Du jeu en ligne sur nos vieilles consoles qui ne connaissait même pas encore cette faculté !

## Recommandations

### Connexion

* Connectez votre machine en Câble Ethernet RJ45 (une connexion wifi étant bien moins stable).
* Avoir une box qui gère l'UPNP ou Ouvrir son port manuellement.
* Si la Recalbox est derrière le routeur, tout est OK (simple NAT).
* Si la Recalbox est derriere 2 routeurs alors le Netplay ne passe pas (Double NAT).

### Roms

* **Pour les Consoles Cartouches :**
  * Utilisez des roms issues de romsets **No-Intro** (dumps plus proche des jeux originaux)
* **Pour les Consoles CD :**
  * Utilisez des roms issues de romsets **Redump** (dumps plus proche des jeux originaux)

### Système

* Si vous utilisez un Raspberry Pi0, Pi1, Pi2 , Pi3-b et Pi3-b+, un **overclock** (À 1300 MHZ) est **conseillé voire obligatoire**.
* Pour les versions Raspberry Pi4, les PC x86_64 (64bit), x86 (32bit) et l’Odroid, l’overclock n’est pas nécessaire.

## Message d'erreur Netplay

### En tant que client : "Failed to Initialize Netplay" ==> « Échec de l'initialisation de Netplay »

« Échec de l'initialisation de Netplay » signifie souvent que vous n'avez pas pu vous connecter à l'hôte. Confirmez que vous disposez de l'adresse IP correcte pour l'hôte et que l'hôte a commencé à héberger une session NetPlay. Dites à l'hôte de vérifier si son pare-feu basé sur l'hôte autorise RetroArch à accepter les connexions et confirmez que la redirection de port fonctionne.

### En tant qu'hôte : "Port Mapping Failed" ==> « Échec du mappage de port »

« Échec du mappage de port » indique probablement un problème de transfert de port UPnP.  
Si vous pouvez configurer manuellement votre passerelle réseau pour transférer le port TCP 55435 à l'adresse IP du réseau local de votre appareil RetroArch, vous pouvez le faire. Vous pouvez également activer l'utilisation d'un serveur relais (rajoute une latence).

Attention : Pour le moment, il est nécessaire d'avoir un IPV4 complètement fonctionnel. Si le réseau fourni par votre FAI est de type IPV4+CGNAT avec IPV6, cela risque de provoquer cette erreur. Voir avec votre FAI pour revenir en full IPV4.

### "Content not found, try loading content manually" ==> « Contenu non trouvé, essayez de charger le contenu manuellement »

* Chargez le contenu manuellement, placez le contenu dans votre liste d'historique récent ou scannez votre contenu vers une liste de lecture.

### En tant que client : "A netplay peer is running different core. Cannot connect." ==> « La partie de netplay utilise un core différent. Impossible d'effectuer la connexion »

* Vérifiez que vous utilisez le même core que l'hôte
* Désactivez les modes HD et écran large, ceux-ci forcent des cores qui peuvent être différents de ce que vous avez choisi.

## FAQ

### 1 - Pourquoi je ne peux pas héberger ?

Il y a 2 possibilités :

* Connexion internet insuffisante

  * **Essayez en étant client.**

* Votre port ne s'ouvre pas automatiquement

  * **Activez l'option UPNP de votre box internet**

  ou

  * **Ouvrez votre port manuellement (Recommandé)**

  ou en dernier recours

  * **Essayez en utilisant la fonction NETPLAY MITM**

### 2 - Si je lance l'hébergement, le jeu se coupe et revient sur le menu Recalbox

Vérifiez que vos cores soient en adéquation avec votre machine (PC, Raspberry Pi ou autre). De plus, certaines machines ne supportent que la fonction client.

### 3 - Est-ce que le Netplay requiert l’ouverture de port pour fonctionner ?

Oui, l'hôte et le client doivent ouvrir les ports correctement.

Il existe une option de secours qui peut être utilisée par ceux qui ne veulent pas ouvrir leurs ports (Fonction NETPLAY MITM).

### 4 - De quoi avez-vous besoin pour que le netplay fonctionne correctement ?

* Même version de Recalbox (si la version de RetroArch ou Dolphin est différente).
* Même version de Retroarch ou Dolphin.
* Même core pour l’émulateur RetroArch.
* Même rom (nommée à la lettre et à la ponctuation prés et signature de rom identique).
* Être connecté en RJ45 (vivement recommandé).

### 5 - Le netplay PSX / N64 / Playstation / PSP / Dreamcast / DS fonctionne-t-il ?

Non, les exigences de performances de ces consoles rendent difficile le netplay.

### 6 - Puis-je jouer à des jeux GB / GBC / GBA / PSP / DS avec plusieurs personnes via Netplay ?

Non, le netplay de RetroArch n’est pas une émulation de câble de liaison.  
Les GB, GBC, GBA et PSP ne sont actuellement pas possibles avec notre implémentation.  
Mais, une exception notable est le même jeu GB / GBC (Netplay via les cores TGB-Dual et Sameboy).  
Les échanges de Pokémons et autres sont donc impossibles.

### 7 - Peut-on jouer à des jeux 4 joueurs sur 2 recalbox via Netplay (2 joueurs par recalbox) ?

Oui. Pour cela, il faut :

* Lancer le jeu concerné jouable à 4 (par exemple TMNT)
* En jeu, exécuter RetroArch via la combinaison de boutons Hotkey+B.
* Aller dans le menu "Réseau"
* Sur la première Recalbox, paramétrer les lignes suivantes comme suit : 
"Demander le périphérique 1" --> Oui
"Demander le périphérique 3" --> Oui

* Sur la deuxième Recalbox, paramétrer les lignes suivantes comme suit : 
"Demander le périphérique 2" --> Oui
"Demander le périphérique 4" --> Oui

* Sauvegarder la configuration 

* Relancer le jeu en Netplay et amusez-vous !

Il est aussi possible de jouer à 4 en Netplay localement (avec un port spécifique par recalbox, par exemple 55435 et 55436).

