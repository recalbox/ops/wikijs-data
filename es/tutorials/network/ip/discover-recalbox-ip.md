---
title: Encontrar la dirección IP de vuestra Recalbox
description: Todos los consejos que necesitas para encontrar la dirección de tu Recalbox
published: true
date: 2023-09-13T06:48:16.954Z
tags: ip, red, dirección, administración
editor: markdown
dateCreated: 2023-09-13T06:48:16.954Z
---

## Comprobad la conectividad

En primer lugar, Recalbox debe estar conectada a la red, ya sea mediante un cable Ethernet o un dongle wifi.

vuestra Recalbox debería estar conectada, pero...¿realmente no sabes si la red funciona? 

Para averiguarlo, pulsad `START`, seleccionad `ACTUALIZACIONES` y finalmente `LANZAR LA ACTUALIZACIÓN` :
- Si aparece un mensaje que dice "Hay una actualización disponible" o "No hay actualizaciones disponibles": vuestra Recalbox está conectada a Internet y por lo tanto a la red. 
- Si aparece el mensaje `Por favor conecte un cable de red`: vuestra Recalbox no está conectada a la red, esto puede ser debido a un problema de configuración de su router, un cable defectuoso, el dongle wifi que no funcione, etc.

>Generalmente, la dirección IP no es fija y puede cambiar en cada reinicio.   
>Para evitar esto, podéis establecer una dirección IP fija en vuestro router, también podéis [establecer una dirección IP definiendo manualmente los parámetros de red](./../../../tutorials/network/ip/static-manual-ip).
{.is-warning}

## Identificar la dirección IP de Recalbox

Pulsad `START` e id a `OPCIONES DE RED`, la dirección IP de vuestra Recalbox se mostrará aquí, normalmente en la forma `192.168.0.XX`.

## Desde tu ordenador

## {.tabset}

### Windows

* Hay un cuadro de búsqueda en la barra de tareas. Escribid `cmd` y pulsad `Enter`.
* Se abrirá el terminal, escribid `ping recalbox` (o `ping recalbox.local`) y pulsad `Enter`.
* Aparecerá el siguiente mensaje, haciendo referencia a la dirección IP de vuestra Recalbox (192.168.0.XX):

```dos
> Envoi d'une requête 'ping' sur RECALBOX avec 32 octets de données :  
> Réponse de 192.168.0.XX : octets=32 temps=1 ms TTL=128  
> Réponse de 192.168.0.XX : octets=32 temps=1 ms TTL=128  
> Réponse de 192.168.0.XX : octets=32 temps=1 ms TTL=128  
> Statistiques Ping pour [...]
```

### macOS

* Id a `Aplicaciones/Utilidades/Terminal`.
* Aparece una ventana de terminal, escribid `arp -a` y pulsad `Enter`.
* Aparece la lista de direcciones IP utilizadas por los dispositivos conectados a la red, y encontraréis la dirección IP de Recalbox (que debe ser de la forma `192.168.0.XX`).

## Otras opciones

* Las aplicaciones gratuitas para smartphone pueden escanear la red local para identificar los dispositivos conectados. Id a vuestra store y realizad una búsqueda.
* Id al panel de administración de la box o del router. Estas herramientas suelen tener una interfaz que enumera todos los dispositivos conectados a la red.