---
title: Accès root via Terminal
description: 
published: true
date: 2024-09-18T15:20:44.394Z
tags: root, terminal, ssh, cli, accès
editor: markdown
dateCreated: 2021-05-21T08:40:21.328Z
---

Afin d'obtenir un terminal avec un accès root sur Recalbox, vous avez deux options :

>N'exposez jamais votre Recalbox à l'internet. En particulier le port 22. Tout le monde connaît le mot de passe root par défaut et vous pourriez être piraté.
{.is-danger}

## SSH

Se connecter via ssh à Recalbox, avec les informations d'identification suivantes :

* Adresse IP de votre Recalbox : `192.168.x.x`
* Identifiant : `root`
* Mot de passe : `recalboxroot`

## {.tabset}
### Windows

Vous pouvez utiliser ces logiciels :

* [Putty](https://www.chiark.greenend.org.uk/~sgtatham/putty/latest.html)
* [MobaXterm](https://mobaxterm.mobatek.net/)
* PowerShell

### macOS

Le Terminal est fourni d'origine dans le système. Ouvrez votre dossier `Applications` et allez dans le répertoire `Utilitaires` pour le trouver.

Ligne de commande à exécuter : `ssh root@adresse-ip-de-votre-Recalbox`

### Linux

Le Terminal est fourni d'origine dans le système. Son emplacement dépend de la distribution Linux que vous utilisez.

Ligne de commande à exécuter : `ssh root@adresse-ip-de-votre-Recalbox`

## Accès direct

Dans l'interface avec un clavier :

* Appuyez sur `Alt + F2` pour obtenir un terminal. Celui-ci ne sera pas visible de suite.
* Appuyez sur `Alt + F4` pour quitter l'interface et accéder au terminal.
* Utilisez les mêmes identifiants que ci-dessus.

## Modifier le mot de passe par défaut

Vous pouvez modifier le mot de passe par défaut avec:

```bash
mount -o remount,rw /
passwd
```

>La modification du mot de passe ne survivra pas aux mises à jour.
{.is-warning}