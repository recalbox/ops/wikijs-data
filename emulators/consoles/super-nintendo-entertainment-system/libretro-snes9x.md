---
title: Libretro Snes9X
description: 
published: true
date: 2024-07-24T20:17:24.433Z
tags: libretro, snes, super nintendo, snes9x
editor: markdown
dateCreated: 2021-08-03T19:22:40.844Z
---

**Libretro Snes9X** is an upstream port of Snes9x, a portable Super Nintendo Entertainment System emulator to Libretro.

## ![](/emulators/license.svg) License

This core is under [**non-commercial**](https://github.com/snes9xgit/snes9x/blob/master/LICENSE) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Features

| Feature | Supported |
| :---: | :---: |
| Restart | ✔ |
| Saves | ✔ |
| States | ✔ |
| Rewind | ✔ |
| Netplay | ✔ |
| Core Options | ✔ |
| RetroAchievements | ✔ |
| RetroArch Cheats | ✔ |
| Native Cheats | ✔ |
| Controls | ✔ |
| Remapping | ✔ |
| Multi-Mouse | ✔ |
| Subsystems | ✔ |
| Softpatching | ✔ |

## ![](/emulators/bios.svg) BIOS

>**No bios is required.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Supported extensions

Roms must have the extension:

* .fig
* .gd3
* .sfc
* .smc
* .swc
* .zip
* .7z

This system supports compressed roms in .zip/.7z format. However, be careful, it is only an archive.

The files contained in the .zip/.7z must correspond to the extensions mentioned above.
Moreover, each .zip/.7z file must contain only one compressed rom.

### Location

Put the roms like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 snes
┃ ┃ ┃ ┃ ┣ 🗒 **game.zip**

>The roms using **No-Intro** format are highly recommended.
{.is-success}

>For more information about roms, go to [this tutorial](./../../../tutorials/games/generalities/isos-and-roms)!
{.is-info}

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Accessing to options

You can configure various options in two different ways.

* Via RetroArch menu:

┣ 📁 RetroArch Menu
┃ ┣ 📁 Core options
┃ ┃ ┣ 🧩 Name_option

* Via the file `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core options

| Options | Options values | Variables (for configuration override) | Variables values |
| --- | --- | --- | --- |
| Console Region (Reload Core) | `Auto` ✅ / `NTSC` / `PAL` | `snes9x_region` | `auto` / `ntsc` / `pal` |
| Preferred Aspect Ratio | `4:3` ✅ / `Uncorrected` / `Auto` / `NTSC` / `PAL` | `snes9x_aspect` | `4:3` / `uncorrected` / `auto` / `ntsc` / `pal` |
| Crop Overscan | `Enabled` ✅ / `Disabled` | `snes9x_overscan` | `enabled` / `disabled` |
| Hi-Res Blending | `Disabled` ✅ / `Merge` / `Blur` | `snes9x_hires_blend` | `disabled` / `merge` / `blur` |
| Blargg NTSC Filter | `Disabled` ✅ / `Monochrome` / `RF` / `Composite` / `S-Video` / `RGB` | `snes9x_blargg` | `disabled` / `monochrome` / `rf` / `composite` / `s-video` / `rgb` |
| Audio Interpolation | `Gaussian` ✅ / `Cubic` / `Sinc` / `None` / `Linear` | `snes9x_audio_interpolation` | `gaussian` / `cubic` / `sinc` / `none` / `linear` |
| Allow Opposing Directions | `Disabled` ✅ / `Enabled` | `snes9x_up_down_allowed` | `disabled` / `enabled` |
| SuperFX Overclocking | `50%` / `60%` / `70%` / `80%` / `90%` / `100%` ✅ / `150%` / `200%` / `250%` / `300%` / `350%` / `400%` / `450%` / `500%` | `snes9x_overclock_superfx` | `50%` / `60%` / `70%` / `80%` / `90%` / `100%` / `150%` / `200%` / `250%` / `300%` / `350%` / `400%` / `450%` / `500%` |
| Reduce Slowdown (Unsafe) | `Disabled` ✅ / `Light` / `Compatible` / `Max` | `snes9x_overclock_cycles` | `disabled` / `light` / `compatible` / `max` |
| Reduce Flickering (Unsafe) | `Disabled` ✅ / `Enabled` | `snes9x_reduce_sprite_flicker` | `disabled` / `enabled` |
| Randomize Memory (Unsafe) | `Disabled` ✅ / `Enabled` | `snes9x_randomize_memory` | `disabled` / `enabled` |
| Block Invalid VRAM Access | `Enabled` ✅ / `Disabled` | `snes9x_block_invalid_vram_access` | `enabled` / `disabled` |
| Echo Buffer Hack (Unsafe, only enable for old addmusic hacks) | `Disabled` ✅ / `Enabled` | `snes9x_echo_buffer_hack` | `disabled` / `enabled` |
| Light Gun Mode | `Light Gun` / `Touchscreen` | `snes9x_lightgun_mode` | `Lightgun` / `Touchscreen` |
| Super Scope Reverse Trigger Buttons | `Disabled` ✅ / `Enabled` | `snes9x_superscope_reverse_buttons` | `disabled` / `enabled` |
| Super Scope Crosshair | `0` / `1` / `2` ✅ / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` | `snes9x_superscope_crosshair` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` |
| Super Scope Color | `White` ✅ / `White (blend)` / `Red` / `Red (blend)` / `Orange` / `Orange (blend)` / `Yellow` / `Yellow (blend)` / `Green` / `Green (blend)` / `Cyan` / `Cyan (blend)` / `Sky` / `Sky (blend)` / `Blue` / `Blue (blend)` / `Violet` / `Violet (blend)` / `Pink` / `Pink (blend)` / `Purple` / `Purple (blend)` / `Black` / `Black (blend)` / `25% Grey` / `25% Grey (blend)` / `50% Grey` / `50% Grey (blend)` / `75% Grey` / `75% Grey (blend)` | `snes9x_superscope_color` | `White` / `White (blend)` / `Red` / `Red (blend)` / `Orange` / `Orange (blend)` / `Yellow` / `Yellow (blend)` / `Green` / `Green (blend)` / `Cyan` / `Cyan (blend)` / `Sky` / `Sky (blend)` / `Blue` / `Blue (blend)` / `Violet` / `Violet (blend)` / `Pink` / `Pink (blend)` / `Purple` / `Purple (blend)` / `Black` / `Black (blend)` / `25% Grey` / `25% Grey (blend)` / `50% Grey` / `50% Grey (blend)` / `75% Grey` / `75% Grey (blend)` |
| Justifier 1 Crosshair | `0` / `1` / `2` / `3` / `4` ✅ / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` | `snes9x_justifier1_crosshair` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` |
| Justifier 1 Color | `White` / `White (blend)` / `Red` / `Red (blend)` / `Orange` / `Orange (blend)` / `Yellow` / `Yellow (blend)` / `Green` / `Green (blend)` / `Cyan` / `Cyan (blend)` / `Sky` / `Sky (blend)` / `Blue` ✅ / `Blue (blend)` / `Violet` / `Violet (blend)` / `Pink` / `Pink (blend)` / `Purple` / `Purple (blend)` / `Black` / `Black (blend)` / `25% Grey` / `25% Grey (blend)` / `50% Grey` / `50% Grey (blend)` / `75% Grey` / `75% Grey (blend)` | `snes9x_justifier1_color` | `White` / `White (blend)` / `Red` / `Red (blend)` / `Orange` / `Orange (blend)` / `Yellow` / `Yellow (blend)` / `Green` / `Green (blend)` / `Cyan` / `Cyan (blend)` / `Sky` / `Sky (blend)` / `Blue` / `Blue (blend)` / `Violet` / `Violet (blend)` / `Pink` / `Pink (blend)` / `Purple` / `Purple (blend)` / `Black` / `Black (blend)` / `25% Grey` / `25% Grey (blend)` / `50% Grey` / `50% Grey (blend)` / `75% Grey` / `75% Grey (blend)` |
| Justifier 2 Crosshair | `0` / `1` / `2` / `3` / `4` ✅ / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` | `snes9x_justifier2_crosshair` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` |
| Justifier 2 Color | `White` / `White (blend)` / `Red` / `Red (blend)` / `Orange` / `Orange (blend)` / `Yellow` / `Yellow (blend)` / `Green` / `Green (blend)` / `Cyan` / `Cyan (blend)` / `Sky` / `Sky (blend)` / `Blue` / `Blue (blend)` / `Violet` / `Violet (blend)` / `Pink` ✅ / `Pink (blend)` / `Purple` / `Purple (blend)` / `Black` / `Black (blend)` / `25% Grey` / `25% Grey (blend)` / `50% Grey` / `50% Grey (blend)` / `75% Grey` / `75% Grey (blend)` | `snes9x_justifier2_color` | `White` / `White (blend)` / `Red` / `Red (blend)` / `Orange` / `Orange (blend)` / `Yellow` / `Yellow (blend)` / `Green` / `Green (blend)` / `Cyan` / `Cyan (blend)` / `Sky` / `Sky (blend)` / `Blue` / `Blue (blend)` / `Violet` / `Violet (blend)` / `Pink` / `Pink (blend)` / `Purple` / `Purple (blend)` / `Black` / `Black (blend)` / `25% Grey` / `25% Grey (blend)` / `50% Grey` / `50% Grey (blend)` / `75% Grey` / `75% Grey (blend)` |
| M.A.C.S. Rifle Crosshair | `0` / `1` / `2` ✅ / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` | `snes9x_rifle_crosshair` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` |
| M.A.C.S. Rifle Color | `White` ✅ / `White (blend)` / `Red` / `Red (blend)` / `Orange` / `Orange (blend)` / `Yellow` / `Yellow (blend)` / `Green` / `Green (blend)` / `Cyan` / `Cyan (blend)` / `Sky` / `Sky (blend)` / `Blue` / `Blue (blend)` / `Violet` / `Violet (blend)` / `Pink` / `Pink (blend)` / `Purple` / `Purple (blend)` / `Black` / `Black (blend)` / `25% Grey` / `25% Grey (blend)` / `50% Grey` / `50% Grey (blend)` / `75% Grey` / `75% Grey (blend)` | `snes9x_rifle_color` | `White` / `White (blend)` / `Red` / `Red (blend)` / `Orange` / `Orange (blend)` / `Yellow` / `Yellow (blend)` / `Green` / `Green (blend)` / `Cyan` / `Cyan (blend)` / `Sky` / `Sky (blend)` / `Blue` / `Blue (blend)` / `Violet` / `Violet (blend)` / `Pink` / `Pink (blend)` / `Purple` / `Purple (blend)` / `Black` / `Black (blend)` / `25% Grey` / `25% Grey (blend)` / `50% Grey` / `50% Grey (blend)` / `75% Grey` / `75% Grey (blend)` |
| Show Layer 1 | `Enabled` ✅ / `Disabled` | `snes9x_layer_1` | `enabled` / `disabled` |
| Show Layer 2 | `Enabled` ✅ / `Disabled` | `snes9x_layer_2` | `enabled` / `disabled` |
| Show Layer 3 | `Enabled` ✅ / `Disabled` | `snes9x_layer_3` | `enabled` / `disabled` |
| Show Layer 4 | `Enabled` ✅ / `Disabled` | `snes9x_layer_4` | `enabled` / `disabled` |
| Show Sprite Layer | `Enabled` ✅ / `Disabled` | `snes9x_layer_5` | `enabled` / `disabled` |
| Enable Graphic Clip Windows | `Enabled` ✅ / `Disabled` | `snes9x_gfx_clip` | `enabled` / `disabled` |
| Enable Transparency Effects | `Enabled` ✅ / `Disabled` | `snes9x_gfx_transp` | `enabled` / `disabled` |
| Enable Sound Channel 1 | `Enabled` ✅ / `Disabled` | `snes9x_sndchan_1` | `enabled` / `disabled` |
| Enable Sound Channel 2 | `Enabled` ✅ / `Disabled` | `snes9x_sndchan_2` | `enabled` / `disabled` |
| Enable Sound Channel 3 | `Enabled` ✅ / `Disabled` | `snes9x_sndchan_3` | `enabled` / `disabled` |
| Enable Sound Channel 4 | `Enabled` ✅ / `Disabled` | `snes9x_sndchan_4` | `enabled` / `disabled` |
| Enable Sound Channel 5 | `Enabled` ✅ / `Disabled` | `snes9x_sndchan_5` | `enabled` / `disabled` |
| Enable Sound Channel 6 | `Enabled` ✅ / `Disabled` | `snes9x_sndchan_6` | `enabled` / `disabled` |
| Enable Sound Channel 7 | `Enabled` ✅ / `Disabled` | `snes9x_sndchan_7` | `enabled` / `disabled` |
| Enable Sound Channel 8 | `Enabled` ✅ / `Disabled` | `snes9x_sndchan_8` | `enabled` / `disabled` |

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/libretro/snes9x/](https://github.com/libretro/snes9x/)
* **Libretro documentation**: [https://docs.libretro.com/library/snes9x/](https://docs.libretro.com/library/snes9x/)