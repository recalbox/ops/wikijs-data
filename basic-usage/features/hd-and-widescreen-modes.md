---
title: HD and widescreen modes
description: 
published: true
date: 2024-11-11T06:10:58.027Z
tags: hd, widescreen
editor: markdown
dateCreated: 2024-11-11T06:10:58.027Z
---

## Presentation

HD and widescreen modes are available on certain systems and enable you to get a better quality image and enlarge it across the entire surface of your screen.

>Widescreen mode is available on Raspberry Pi 4 / 400 / 5 and PC. HD mode is available on Raspberry Pi 5 and PC.
{.is-info}

## Configuration

In the interface, press `START` then go to `GAME OPTIONS`. Depending on your board, you'll have one or both of these options.

## Notes about these options

These options **change the emulator/core** used! Here are the cores used according to these options:

| System | Used core | Widescreen | HD |
| :---: | :---: | :---: | :---: |
| Atomiswave | Libretro-Flycast | ✅ | ✅ |
| Dreamcast | Libretro-Flycast | ✅ | ✅ |
| Megadrive | Libretro-GenesisPlusGX-Wide | ✅ | ❌ |
| Naomi | Libretro-Flycast | ✅ | ✅ |
| Nintendo 64 | Libretro-Parallel_n64 | ❌ | ✅ |
| Playstation | Libretro-PCSX_reARMed | ❌ | ✅ |
| Saturn | Libretro-Yabasanshiro | ❌ | ✅ |
| Super Nintendo | Libretro-bsneshd | ✅ | ❌ |