---
title: Thomson MO/TO
description: 
published: true
date: 2021-08-09T15:04:43.447Z
tags: thomson, mo5, to8, computers
editor: markdown
dateCreated: 2021-07-28T21:36:03.956Z
---

![Modèle TO8](/emulators/computers/thomson.svg){.align-center}

## Technical data

* **Manufacturer**: Thomson
* **Year of release**: 1986
* **CPU**: Motorola 6809E @ 1 MHz
* **RAM**: 256kB
* **ROM**: 80kB
* **Resolution**: 8 display modes, palette of 4096 colors
  * **Low résolutions**: 160 × 200 (16, 5 or 2 colors)
  * **Medium résolutions**: 320 × 200 (16, 4, 3 or 2 colors)
  * **High résolutions**: 640 × 200 (2 colors)
* **Media**: Cartridge, external 3½" floppy disk and tape drives

## Presentation

The Thomson MO/TO range is a range of 8-bit microcomputers: MO5, MO6, TO7, TO7/70, TO8, TO8D, TO9, TO9+.

## Emulators

[Libretro Theodore](libretro-theodore)