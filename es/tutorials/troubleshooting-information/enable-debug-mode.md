---
title: Activar el modo debug
description: 
published: true
date: 2024-06-29T17:24:26.326Z
tags: debug, activar, modo, depuración
editor: markdown
dateCreated: 2023-09-17T19:44:18.198Z
---

Con el fin de solucionar problemas de vuestra Recalbox, puede que os pidamos que activéis el modo de depuración (también conocido como "debug logs"). 

El modo debug permite registrar múltiples informaciones detalladas sobre la actividad del sistema (en ficheros \*.log) para solucionar problemas. 



> Tened en cuenta que el modo debug no es una solución en sí misma, pero es extremadamente útil para el diagnóstico. 
{.is-info}

## Procedimiento

- Abrid el fichero [recalbox.conf](../../basic-usage/getting-started/recalboxconf-file).
- En una nueva línea, añadid lo siguiente:

```ini
emulationstation.debuglogs=1
```

- Grabad la modificación y  **reiniciad** Recalbox.
- (Opcional) Depués del reinicio, intentad reproducir el problema que tenéis y una vez hecho esto, dirigiros dentro del directorio `/recalbox/share/system/logs/` para recuperar el fichero de debug `es_log.txt`. 

> ¡No olvides borrar o comentar esta línea con un `;` delante para detener el modo depuración una vez finalizado todo el proceso!
{.is-info}

> Comprobad que el modo debug está desactivado cuand no lo necesitéis más, ya que puede degradar el rendimiento de ciertos emuladores y la experiencia general durante los juegos, ¡en especial sobre los juegos de MAME!
{.is-info}