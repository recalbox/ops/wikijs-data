---
title: Compatibilité des roms avec les cores
description: Il en va de la compatibilité et de la satisfaction des utilisateurs, puisse le sort vous être favorable.
published: true
date: 2024-05-05T18:04:26.856Z
tags: core n64, roms n64, core, compatibilité, compatibilité n64
editor: markdown
dateCreated: 2021-06-16T15:16:53.124Z
---

#### Version 9.1-Pulstar

Si vous ne trouvez pas votre jeu dans le tableau, cela signifie qu'il n'a pas encore été testé.
Soyez patiente et patient, nous travaillons dessus, vous pouvez également rejoindre l'équipe de test si vous avez envie de donner un peu de votre temps pour nous aider.
Contactez m.r.b.i sur le Discord de Recalbox pour plus d'infos :wink: 

## Légende

 :star2: Haute performance. Vers l'infini et au-delà !
 :star: Moyenne performance. Bon équilibre, mais plus lent avec des bugs minimes.
 :anger: Basse performance. Lent et bugué. _**Non recommandé**_.
 :boom: Ne fonctionne pas. Crash, Lags, Freeze, retour EmulationStation, bugs. _**Déconseillé**_.

## CORE

### Raspberry Pi3 b 

| JEUX  | Extension | Gliden 64 | GLES2 | Rice_GLES2 | Rice | Parallel 64 | Plus_Next |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | 
| **1080** | .z64 | :star2: | :boom: | :star: | :anger: |:anger:  | :boom: | 
| **Aidyn** | .z64 |  :star: | :anger: | :anger: |  |  |  | 
| **Banjo-Kazooie** | .z64 | :star2: | :boom: | :star2: | :star: |  |  |
| **Banjo-Tooie** | .z64 | :boom: | :boom: | :star: | :anger: |  |  |
| **Diddy Kong Racing** | .n64 | :boom: | :star: | :star: | :anger: | :boom: | :anger: | 
| **Dr.Mario 64** | .z64 | :anger: | :boom: | :anger: | :anger: | | |
| **F Zéro X**  | .n64 | :star: | :star: | :anger: | :anger: | :anger:  | :boom:|
| **Kirby 64: The Crystal Shards** | .z64 | :star2: | :anger: | :star: | :anger: |  |  |
| **Lylat Wars** | .n64 | :star2: | :star: | :star: | :star: | :star: | :boom: |
| **Mario Kart 64** | .n64 | :star2: | :star2: | :star2: | :anger: | :star2: | :boom: |
| **Mario Tennis** | .z64 |  |  |  |  |  |  |  
| **Micro Machines 64 Turbo** | .z64 | :star2: | :anger: | :star: | :anger: |  |  |
| **Paperboy** | .z64 | :boom: | :boom: | :boom: | :boom: | :boom: | :boom: |  
| **Paper Mario**  | .n64 | :star2: |:boom:  | :anger: | :boom: |  |  |
| **Pokemon Snap** | .z64 | :boom: | :boom: | :boom: | :boom: | :boom: |:boom:
| **Pokémon Stadium** | .n64 | :boom: | :boom: | :anger: | :anger: | :anger: | :boom: |
| **Pokémon Stadium 2** | .n64 | :boom: | :boom: | :anger: | :anger: | :star: | :boom: |
| **Star Wars Episode I Battle for Naboo** | .n64 | :boom: | :boom: | :boom: | :boom: | :boom: | :boom: |
| **Star Wars Episode I Racer** | .n64 | :star: | :boom: | :anger: | :boom: | :boom: |:boom: |
| **Star Wars Rogue Squadron** | .n64 | :boom: | :boom: | :boom: | :boom: | :boom: |:boom: |
| **Star Wars Shadows of the Empire** | .n64 | :boom: | :boom: | :anger: | :anger: | :boom: | :boom: |
| **Super Mario 64** | .n64 | :star2: | :star: | :star2: | :star: | :star2: | :anger: |
| **The New Tetris**  | .z64 | :star: |  |  |  |  |  |  
| **Wave Race** | .n64 | :star: | :anger: | :boom: | :boom: | :anger: | :boom: |
| **Wipeout 64** | .z64 | :star2: | :boom: | :star: | :anger: |  |  |
| **Wipeout 64** | .n64 | :star2: | :boom: | :star: | :anger: | :star2: | :boom: |
| **Zelda Majora Mask**  | .n64  | :boom: | :boom: | :star: | :boom: | :anger: | :boom:

### Raspberry Pi 4

| Jeux | Extension | GlideMK2 | RICE | Plus_Next | ParaLLEl_N64 |
| :---: | :---: | :---: | :---: | :---: | :---: |
|  |  |  |  |  |  |

### PC x86_64

| JEUX  | Extension | Gliden 64 | GLES2 | Rice_GLES2 | Rice | Parallel 64 | Plus_Next |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | 
| **A Bug's Life** | .z64 |  |  |  |  |:anger:|  |
| **GoldenEye 007** | .z64 | :star: |  |  | :star2: |:anger:|:star2:|
| **007 : Le Monde Ne Suffit Pas** | .z64 | :star2: |  |  | :star2: |:star2:|:star2:|