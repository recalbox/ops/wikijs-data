---
title: Licença
description: Este espaço contém explicações sobre aspectos legais e outras coisas enfadonhas, mas importantes
published: true
date: 2024-06-02T17:23:44.460Z
tags: 
editor: markdown
dateCreated: 2021-08-10T23:49:10.314Z
---

O Recalbox usa vários pacotes sob certos tipos de licença (GPL, BSD, MIT, etc.) e o próprio Recalbox usa uma licença gratuita.

Aqui está a descrição da licença usada pelo Recalbox:

> A redistribuição e uso do código RECALBOX ou qualquer trabalho derivado é autorizada desde que as seguintes condições sejam respeitadas/cumpridas:
>
> As redistribuições não podem ser vendidas sem autorização, nem utilizadas em qualquer produto ou atividade comercial.
> As redistribuições que são modificações das fontes originais devem incluir o código-fonte completo, incluindo o código-fonte de todos os componentes usados por uma distribuição binária construída a partir das fontes modificadas.
> As redistribuições devem reproduzir o aviso de direitos autorais acima, esta lista de condições e a seguinte isenção de responsabilidade na documentação e/ou outrosdocumentos fornecidos com a distribuição.
> ESTE SOFTWARE É FORNECIDO PELOS PROPRIETÁRIOS DOS DIREITOS AUTORAIS E CONTRIBUIDORES "NO ESTADO EM QUE SE ENCONTRAM" E TODAS AS GARANTIAS, EXPRESSAS OU IMPLÍCITAS, INCLUINDO, MAS NÃO SE LIMITANDO A, GARANTIAS IMPLÍCITAS DE COMERCIALIZAÇÃO E ADEQUAÇÃO A UM PROPÓSITO INDIVIDUAL, SÃO RENUNCIADAS. EM NENHUMA HIPÓTESE O PROPRIETÁRIO DOS DIREITOS AUTORAIS OU OS CONTRIBUIDORES SERÃO RESPONSÁVEIS POR QUAISQUER DANOS DIRETOS, INDIRETOS, INCIDENTAIS, ESPECIAIS, EXEMPLARES OU CONSEQÜENTES (INCLUINDO, MAS NÃO SE LIMITANDO A, AQUISIÇÃO DE BENS OU SERVIÇOS SUBSTITUTOS; PERDA DE USO, DADOS OU LUCROS; OU INTERRUPÇÃO DE NEGÓCIOS), SEJA A CAUSA E QUALQUER TEORIA DE RESPONSABILIDADE, SEJA RESPONSABILIDADE CONTRATUAL, ESTRITA OU ATO ILÍCITO (INCLUINDO NEGLIGÊNCIA OU DE OUTRA FORMA) DECORRENTE DE QUALQUER FORMA DO USO DESTE SOFTWARE, MESMO SE AVISADO DA POSSIBILIDADE DE TAIS DANOS.

> Embora esta não seja uma obrigação legal enquadrada pela licença, pedimos a qualquer pessoa que deseje criar trabalhos derivados, versões modificadas ou "forks" do Recalbox que não use código que ainda não tenha alcançado o estágio `master`.
{.is-info}