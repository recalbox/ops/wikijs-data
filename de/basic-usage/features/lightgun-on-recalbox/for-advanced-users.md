---
title: Für erfahrene Benutzer
description: WIe man selbst Lightgun-Spiele hinzufügt
published: true
date: 2022-06-04T13:27:41.345Z
tags: lightgun, 7.2+, erfahrene benutzer
editor: markdown
dateCreated: 2022-06-04T13:27:41.345Z
---

## Wie geht man noch weiter?

### Die Grundidee...

Das Prinzip ist, eine einzige Konfigurationsdatei zu haben, die alle notwendigen Informationen für die "Lightgun"-Spiele aller Systeme und Emulatoren enthält. 

Auf diese Weise können wir Vererbungen vornehmen, Spiele gruppieren, aber auch Besonderheiten verwalten, ohne eine Datei pro Spiel oder System haben zu müssen, was wir bereits können, aber ohne in der Lage zu sein, z. B. Systeme zu gruppieren. Dies ist mit dieser einzigartigen XML möglich. 

Außerdem werden wir in den nächsten Versionen in der Lage sein, Online-Hilfe, Meldungen beim Laden von Spielen usw. hinzuzufügen.

Ein weiterer Vorteil gegenüber den klassischen Overloads: wir können Funktionen in Configgen Python hinzufügen, um zu erkennen, ob eine bestimmte Hardware vorhanden ist, wie z.B. die Mayflash Delphin Bar oder um zu wissen, auf welchem Board/Computer wir uns befinden. Du kannst 1 bis 3 Lightgun-Spieler auf diese Weise konfigurieren.

>Zur Erinnerung, dies ist im Moment nur für Libretro möglich. Dennoch können wir dies auch für andere Emulatoren mit der XML-Datei tun, die wir bereits haben.
{.is-info}

### Die XML-Konfigurationsdatei

Die Konfiguration der Spiele befindet sich in einer XML-Datei (zur Info: es handelt sich um eine ".cfg").

Die Datei ist wie folgt strukturiert *(die untenstehende ist leer, ohne Konfigurationen, um die vollständige XML-Struktur zu zeigen)* :

```xml
<?xml version="1.0"?>
<root>
	<common_inputs>
 	</common_inputs>
  <common_options>
  </common_options>
  <system>
 		<platform></platform>
		<emulator>
			<core></core>
		</emulator>
    <inputs>
    </inputs>
    <options>
    </options>
    <games>
      <inputs>
      </inputs>
      <options>
      </options>
      <game>
        <name></name> 
        <inputs>
        </inputs>
        <options>
        </options>
      </game>
    </games>
  </system>
</root>
```

### XML Tags

**&lt;root>**: dies ist die Basis der XML-Datei, in der alles zu finden ist, zusammen mit einer Versionsnummer, um die Entwicklung dieser Datei zu verfolgen

```xml
<?xml version="1.0"?>
<root>
	<!-- internal version of the file to help follow-up of update, same version until release -->
	<version>1.0 - Beta8 - 20-10-2020</version>
</root>
```

**<common_inputs>**: dies ist der Teil in &lt;root> und die allgemeine Überschreibung von `retroarchcustom.cfg`, die wir für alle Systeme haben werden. Im Fall der Lightgun werden wir die grundlegenden Tasten wie *Select/Exit/Start/Trigger* dort unterbringen.

```xml
	<!--MANDATORY inputs (for retroarchcustom.cfg): comon part to use for all systems (using dolphinbar and until 2 wiimotes) -->
	<common_inputs>
        <string name="input_player1_gun_start" value="enter" />
        <string name="input_player2_gun_start" value="enter" />
        <string name="input_player1_gun_select" value="escape" />
        <string name="input_player2_gun_select" value="escape" />
        <!-- force selecte and start for consoles games -->
        <string name="input_player1_select" value="escape" />
        <string name="input_player2_select" value="escape" />
        <string name="input_player1_start" value="enter" />
        <string name="input_player2_start" value="enter" />
        <!--for many it's necessary to move in menu or else -->
        <string name="input_player1_gun_dpad_up" value="up" />
        <string name="input_player1_gun_dpad_down" value="down" />
        <string name="input_player1_gun_dpad_right" value="right" />
        <string name="input_player1_gun_dpad_left" value="left" />
        <string name="input_player2_gun_dpad_up" value="up" />
        <string name="input_player2_gun_dpad_down" value="down" />
        <string name="input_player2_gun_dpad_right" value="right" />
        <string name="input_player2_gun_dpad_left" value="left" />
        <!-- index to manage distinction between guns -->
        <string name="input_player1_mouse_index" value="GUNP1" />
        <string name="input_player2_mouse_index" value="GUNP2" />
        <!-- wiimotes home button for exit game -->
		    <string name="input_exit_emulator" value="lsuper" />
	  	  <string name="input_enable_hotkey" value="lsuper" />
        <!-- set on fullscreen for don't see cursor mouse on x86(-64) -->
	  	  <string name="video_fullscreen" value="true" />
        <!-- to have help using overlay switch and finally ask to press 2 times home to exit -->
        <string name="input_overlay_next" value="lsuper" />
        <string name="quit_press_twice" value="true" />
 	</common_inputs>
```

**<common_options>**: dies ist der Teil in &lt;root> und allgemeine Überschreibung von `retroarch-core-options.cfg`, den wir haben werden. In unserem Fall ist es nicht in einer gemeinsamen Weise verwendet, aber es wäre möglich.

```xml
    <!--OPTIONAL options (for retroarch-core-options.cfg): comon part to use for all systems (using dolphinbar and until 2 wiimotes) -->
  	<common_options>
        <!-- only for more log on test -->
        <!-- <string name="libretro_log_level" value="2" />
        <string name="log_verbosity" value="true" /> -->
  	</common_options>
```

**&lt;string>:** dies sind die grundlegenden Konfigurationswerte, die immer aus den 2 Attributen *"name"* und *"value"* bestehen :

```xml
<string name="input_player1_gun_start" value="enter" />
```

**&lt;system>:** Dies ist der Teil, der die gesamte Konfiguration eines Systems zusammenfasst. Du findest hier die Tags *&lt;platform>, &lt;emulator>, &lt;inputs>, &lt;options>, &lt;games>* und *&lt;game>*.
```xml
  <!-- ********************************************************* Nintendo Entertainment System ************************************************************** -->
	<system>
    <!-- MANDATORY: name of the platform as in roms directory -->
		<platform>nes</platform>
		<!-- MANDATORY: emulator and core selected for lightgun-->
		<emulator name="libretro">
			<core>fceumm</core>
		</emulator>
    <!--MANDATORY: inputs common to this system -->
		<inputs>
      	<!-- Wiimote button B -->
		  	<string name="input_player2_gun_trigger_mbtn" value="1" />
      	<!-- gamepad -->
      	<string name="input_libretro_device_p1" value="513" />
      	<!-- lightgun -->
      	<string name="input_libretro_device_p2" value="258" />
        <!-- Set index to GUNP1 on player 2 to have first wiimote as player 1 -->
        <string name="input_player2_mouse_index" value="GUNP1" />
		</inputs>
		<!--MANDATORY: options common to this system-->
		<options>
		  	<string name="fceumm_zapper_mode" value="lightgun" />
		  	<string name="fceumm_zapper_tolerance" value="6" />
      	<string name="fceumm_show_crosshair" value="enabled" />
		</options>
        <games>
            <!--list of games using lightgun-->
            <!-- see 'duckhunt' game to see more explainations to know how to setup game part -->
            <game tested="no">
                <name>2in1</name>
            </game>
```

**&lt;platform>**: dies ist der Tag, der dem Namen des Systems entspricht, aber wir können mehrere Plattformen im selben System haben, wie zum Beispiel für die Systeme, die auf dem Emulator *flycast* basieren.

```xml
  	<!-- ******************************* Sega Naomi & Sega NaomiGd & Sega Atomiswave ********************************************************* -->
  	<system>
    <!-- MANDATORY: name of the platform as in roms directory -->
        <platform>atomiswave</platform>
        <platform>naomi</platform>
        <platform>naomigd</platform>
    		<!-- MANDATORY: emulator and core selected for lightgun-->
    		<emulator name="libretro">
      			<core>flycast</core>
    		</emulator>
```

>Du kannst mehrere unterschiedliche Systeme mit der gleichen Plattform haben, um Spiele mit verschiedenen Emulatoren und/oder Cores zu haben. Dies ist z. B. bei der MAME-Plattform der Fall, die einen "MAME" Core (auf PC) oder "mame2003plus" (auf Platinen) verwendet..
{.is-warning}

**&lt;emulator>**: dieser Tag hat als Attribut den Namen des Emulators, der für das betreffende System zu verwenden ist:

```xml
    	<!-- MANDATORY: emulator and core selected for lightgun-->
    	<emulator name="libretro">
      		<core>flycast</core>
    	</emulator>
```

**&lt;core>**: dieser tag hat als Wert den Namen des zu verwendenden Emulators. Er steht immer in *&lt;emulator>* wie oben.

```xml
<core>flycast</core>
```

**&lt;inputs>**: dieser Tag erlaubt es, _retroarchcustom.cfg_ zusätzlich zum *&lt;common_inputs>* Teil auf der Ebene von *&lt;system>, &lt;games>* und/oder *&lt;game>* zu überschreiben

```xml
<system>
.....
		<inputs>
      	<!-- Wiimote button B -->
		  	<string name="input_player2_gun_trigger_mbtn" value="1" />
      	<!-- gamepad -->
      	<string name="input_libretro_device_p1" value="513" />
      	<!-- lightgun -->
      	<string name="input_libretro_device_p2" value="258" />
        <!-- Set index to GUNP1 on player 2 to have first wiimote as player 1 -->
        <string name="input_player2_mouse_index" value="GUNP1" />
.....
```

```xml
.....
        <games>
            <inputs>
                <!-- no reload, this game is a free fire but need secondary input fo grenade and more -->
                <string name="input_player1_a_mbtn" value="2" />
                <string name="input_player2_a_mbtn" value="2" />     
            </inputs>
            <game tested="ok">
                <name>alien3thegun</name>
            </game>
.....
```

```xml
.....            
            <game tested="ok">
                <name>gunbuster</name>
                <inputs>
                    <!-- no reload free fire game but ned secondary input fo grenade and more -->
                    <!-- move players with dpad on wiimote  -->
                    <string name="input_player1_a" value="2" />
                    <string name="input_player1_gun_offscreen_shot_mbtn" value="nul" />
                    <string name="input_player2_a" value="2" />
                    <string name="input_player2_gun_offscreen_shot_mbtn" value="nul" />
                </inputs>
            </game>
.....
```

**&lt;options>**: dieser Tag erlaubt es, *retroarch-core-options.cfg* zusätzlich zum *&lt;common_options>*-Teil auf der Ebene von *&lt;system>, &lt;games>* und/oder *&lt;game> zu überschreiben.* 

```xml
        <!-- options common to this system-->
        <options>
            <string name="genesis_plus_gx_gun_input" value="lightgun" />
            <string name="genesis_plus_gx_gun_cursor" value="enabled" />
        </options>
```

**&lt;games>**: dieser Tag ist die Basis für Spielegruppen. Er ermöglicht eine eigene Konfiguration für einen Teil der Spiele im selben System, so dass Du mehrere *&lt;games>*-Bereiche im selben System haben kannst.

```xml
        <games>
            <inputs>
                <!-- no reload, these games have a free fire but need secondary input for grenade and more -->
                <string name="input_player1_a_mbtn" value="2" />
                <string name="input_player2_a_mbtn" value="2" />     
            </inputs>
            <game tested="ok">
                <name>alien3thegun</name>
            </game>
            <game tested="ok">
                <name>beastbusters</name>
            </game>
            <game tested="ok">
                <name>dragongun</name>
            </game>
        </games>

```

**&lt;game>**: dieser Tag, der sich in *&lt;games>* befindet, dient dazu, die Konfiguration des jeweiligen Spiels festzulegen.

```xml
            <game tested="ok">
                <name>gunbuster</name>
                <inputs>
                    <!-- no reload free fire game but ned secondary input fo grenade and more -->
                    <!-- move players with dpad on wiimote  -->
                    <string name="input_player1_a" value="2" />
                    <string name="input_player1_gun_offscreen_shot_mbtn" value="nul" />
                    <string name="input_player2_a" value="2" />
                    <string name="input_player2_gun_offscreen_shot_mbtn" value="nul" />
                </inputs>
            </game>
```

>**&lt;game>** hat das Attribut *"tested"*, um zu bestimmen, ob diese bestimmte Rom bereits getestet wurde und welchen Status sie hat (3 mögliche Werte):
>
>* **no** → nicht getestet, wird aber bei der Standard-Systemkonfiguration berücksichtigt.
>* **ok** → mit der festgelegten Konfiguration getestet.
>* **nok** → getestet, aber nicht funktionsfähig, wird also im Moment nicht für den Lightgun-Modus konfiguriert werden können (wir haben aber Hoffnung).
{.is-warning}

**&lt;name>**: dieser Tag in *&lt;game>* wird verwendet, um den Namen anzugeben, der zur Identifizierung verwendet wird, um die Lightgun-Funktion von recalbox beim Start zu aktivieren.

```xml
.....
<name>duckhunt</name>
.....
<name>mobilsuitgundamfinalshooting</name>
.....
```

>Du musst immer den Namen **ohne Leerzeichen/Sonderzeichen und in Kleinbuchstaben** für das Parsen der "Gamelist" eines Systems durch Recalbox angeben. Sei vorsichtig, denn dies ist der Name des Spiels in seiner einfachsten Form. Es ist weder der Name der Datei des ROMs noch der Name mit der geographischen Zone, usw... es ist notwendig, so einfach wie möglich zu bleiben, damit die Zuordnung sichergestellt ist. **Scraping wird sicherstellen, dass Du ein Spiel mit einem identifizierbaren Namen hast**.
{.is-warning}

### Vererbung

In diesem Abschnitt erklären wir dir, dass die Einrichtung der Reihe nach wie folgt abläuft: 

***common → system → games → game***

Wir überlassen es Dir, uns zu sagen, wie input_player2_gun_trigger_mbtn im folgenden Beispiel aussehen wird

```xml
<?xml version="1.0"?>
<root>
	<common_inputs>
	  <string name="input_player2_gun_trigger_mbtn" value="1" />
 	</common_inputs>
  <common_options>
  </common_options>
  <system>
 		<platform></platform>
		<emulator>
			<core></core>
		</emulator>
    <inputs>
      <string name="input_player2_gun_trigger_mbtn" value="2" />
    </inputs>
    <options>
    </options>
    <games>
      <inputs>
        <string name="input_player2_gun_trigger_mbtn" value="1" />
      </inputs>
      <options>
      </options>
      <game>
        <name></name> 
        <inputs>
          <string name="input_player2_gun_trigger_mbtn" value="2" />
        </inputs>
        <options>
        </options>
      </game>
    </games>
  </system>
</root>
```

### Indexverwaltung für Multiplayer

Wir mussten die Maus-Indizes verwalten und sie auf eine einheitliche Weise oder genauer gesagt pro Spiel angeben. Zum Beispiel, wenn Spieler 1 auf Port 2 ist, und Spieler 2 auf Port 3. Wir haben also Schlüsselwörter wie **GUNP1, GUNP2** oder **GUNP3**.

```xml
.....
     <common_inputs>
        <!-- index to manage distinction between guns -->
        <string name="input_player1_mouse_index" value="GUNP1" />
        <string name="input_player2_mouse_index" value="GUNP2" />
        .....
```

```xml
        <games> <!-- games using justifier -->
            <inputs>
                <!-- lightguns always on port 2 -->
                <!-- For Justifier device (blue cross) -->
                <string name="input_libretro_device_p2" value="516" />
                <string name="input_player2_mouse_index" value="GUNP1" />
                <!-- Wiimote button B or Z -->
                <string name="input_player2_gun_trigger_mbtn" value="1" />
                <!-- For Justifier(P2) device (pink cross) -->
                <string name="input_libretro_device_p3" value="772" />
                <string name="input_player3_mouse_index" value="GUNP2" />
                <!-- Wiimote button B or Z -->
                <string name="input_player3_gun_trigger_mbtn" value="1" />
                <!-- Common inputs part replicated for this game using player 3 -->
                <string name="input_player3_gun_start" value="enter" />
                <string name="input_player3_gun_select" value="escape" />
                <!-- Common inputs part replicated to force select and start for consoles games -->
                <string name="input_player3_select" value="escape" />
                <string name="input_player3_start" value="enter" />
            </inputs>
            <game tested="ok">
                <name>lethalenforcers</name>
            </game>
        </games>
```

## Enfacher gesagt...

Tatsächlich ist die XML Konfigurationsdatei nicht in deinem "share" zu finden, sondern befindet sich im System und ist nur über SSH zugänglich unter: `/recalbox/share_init/system/.emulationstation`

unter dem folgenden Namen: `lightgun.cfg`.

So sieht die Anzeige in einer SSH-Konsole aus:

```shell
# cd /recalbox/share_init/system/.emulationstation
# ls -l
total 252
-rwxr-xr-x    1 root     root         51359 Oct 22 19:13 es_bios.xml
-rwxr-xr-x    1 root     root          1254 Oct 22 19:13 es_bios.xsd
-rwxr-xr-x    1 root     root         70383 Oct 22 19:13 es_input.cfg
-rwxr-xr-x    1 root     root          2979 Oct 22 19:13 es_settings.cfg
-rw-r--r--    1 root     root         61973 Oct 23 02:31 es_systems.cfg
-rw-r--r--    1 root     root         68513 Oct 22 14:19 lightgun.cfg
drwxr-xr-x    3 root     root            36 Oct 23 02:28 themes

```

>Um diese Datei ändern zu können, muss die [Systempartition](./../../../tutorials/system/access/remount-partition-with-write-access) mit Lese- und Schreibrechten eingerichtet werden.
{.is-warning}

Anfang der Datei beim Öffnen mit einem Texteditor:
![](/basic-usage/features/lightgun/advanced-users/advancedusers1.png){.full-width}

### Wie kann ich ein Spiel/ROM hinzufügen??

Du musst nur das System finden, z. B. für NES:
![](/basic-usage/features/lightgun/advanced-users/advancedusers2.png){.full-width}

Und im Abschnitt *&lt;games>* den Tag *&lt;game>* und dann *&lt;name>* wie folgt hinzufügen:

```xml
           <game tested="ok">
                <name>monjeu2</name>
            </game>
```

>ERINNERUNG: Du musst immer den Namen **ohne Leerzeichen/Sonderzeichen und in Kleinbuchstaben** für das Parsen der "Gamelist" eines Systems durch Recalbox angeben. Sei vorsichtig, denn dies ist der Name des Spiels in seiner einfachsten Form. Es ist weder der Name der Datei des ROMs noch der Name mit der geographischen Zone, usw... es ist notwendig, so einfach wie möglich zu bleiben, damit die Zuordnung sichergestellt ist. **Scraping wird sicherstellen, dass Du ein Spiel mit einem identifizierbaren Namen hast**
{.is-warning}

### Um zu überprüfen, ob dies berücksichtigt wurde

* Wenn die Datei schlecht formatiert ist, kehrt man zur Liste der Spiele zurück.
* Wenn der Name nicht gefunden/erkannt wird, lässt sich der Lightgun-Modus nicht aktivieren..
* Wenn du wirklich im Lightgun-Modus bist, solltest du das Spiel mit der HOME-Taste der Wiimote beenden können.

### Einem Spiel eine Konfiguration hinzufügen

Am besten schaut man sich an, was für andere Spiele getan wurde... Im Allgemeinen musst Du die Einstellungen für die primären und sekundären Feuertasten ändern oder sogar einige Tasten deaktivieren (siehe unten), aber es kann noch komplizierter sein... In diesem Fall musst Du in den Handbüchern der damaligen Spiele nachschauen und viel Zeit in den Menüs von RetroArch verbringen... also lerne und beherrsche die Kunst des "Überladens"!

```xml
        <game tested="ok">
            <name>ninjaassault</name>
            <inputs>
                <!--for this game start p1 for P2 aux_c -->
                <string name="input_player1_gun_start" value="nul" />
                <string name="input_player1_gun_aux_b" value="enter" />
                <!--for this game trigger -->
                <string name="input_player1_gun_trigger_mbtn" value="nul" />
                <string name="input_player1_gun_aux_a_mbtn" value="1" />
                <!--for this game real offscreen reload -->
                <string name="input_player1_gun_offscreen_shot_mbtn" value="nul" />
            </inputs>
        </game>
```

### Ein neues System hinzufügen

Um das zu tun, musst Du den zu verwendenden Core und die RetroArch-Parameter kennen und im Allgemeinen auch das "Gerät" in *&lt;inputs>* sowie die "Lightgun"-Parameter des Core in *&lt;options>* konfigurieren.

```xml
......
<system>
<!-- MANDATORY: name of the platform as in roms directory -->
    <platform>megadrive</platform>
    <!-- MANDATORY: emulator and core selected for lightgun-->
    <emulator name="libretro">
        <core>genesisplusgx</core>
    </emulator>
    <!-- MANDATORY: inputs common to this system-->
    <inputs>
        <!-- Wiimote button B or Z -->
        <string name="input_player2_gun_trigger_mbtn" value="1" />
        <!-- Wiimote button A or c - aux when it's necessary as jump, rocket, etc... -->
        <string name="input_player2_gun_aux_a_mbtn" value="2" />
        <!-- <string name="input_player2_gun_aux_a_mbtn" value="2" /> -->
        <!-- gamepad always on port 1-->
        <string name="input_libretro_device_p1" value="1" />
    </inputs>
    <!--MANDATORY: options common to this system-->
    <options>
        <string name="genesis_plus_gx_gun_input" value="lightgun" />
        <string name="genesis_plus_gx_gun_cursor" value="enabled" />
    </options>
    .....
```

**Viel Erfolg!**