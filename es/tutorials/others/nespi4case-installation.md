---
title: Instalación del NesPi4Case
description: 
published: true
date: 2023-09-21T14:56:39.374Z
tags: nespi4case, instalación
editor: markdown
dateCreated: 2023-09-18T10:30:53.461Z
---

## Introducción

En la NesPi4Case, resulta que el puerto SATA del disco duro causa varios problemas dependiendo de la configuración utilizada. En pocas palabras, si el puerto SATA (integrado en la parte superior de la caja) está conectado a través del cable USB3 a la parte inferior, pero ningún disco (disco duro mecánico o SSD) está conectado a él, entonces Recalbox se encuentra con varios problemas:

* Problema de instalación durante una instalación nueva ;
* Latencia de arranque (que puede ser de varios minutos);
* Problema de reconocimiento de la caja;
* Problema de apagado de la fuente de alimentación y del ventilador de la Nespi4Case cuando se pulsa directamente el botón de encendido.

Para resolver estos problemas y conseguir que la caja funcione correctamente mientras esperas una solución por parte de los desarrolladores, te ofrecemos este breve tutorial para ayudarte a sacar el máximo partido a tu caja. Ten en cuenta que se trata de una medida temporal para poder utilizar la caja mientras esperamos una solución estable.

Por lo tanto, distinguiremos dos casos:

* Uso con disco duro integrado en el cartucho y conectado a la caja;
* Utilización sin disco duro.

>Asegúrese de que los cables a conectar siguen exactamente el esquema de montaje recomendado por RETROFLAG. No invierta los cables USB2/USB3.
{.is-warning}

En todos los casos, suponemos que el Pi4 está instalado en la caja.

## Instalación

### Uso del Nespi4Case con un disco duro

Este es el caso más sencillo.

#### Instalación
Primero, formatea tu disco duro en formato exFAT. Flashea tu tarjeta µSD con tu software favorito.

A continuación, asegúrate de que el botón de apagado seguro de la caja está activado (consulta el manual de la caja) para utilizar el modo de apagado seguro.

En segundo lugar, en la caja, inserta tu tarjeta µSD en su ranura y conecta el disco duro al puerto SATA. El disco duro _**DEBE**_ estar conectado.

![](/tutorials/others/nespi4case-installation/nespi4case1.jpg)

* Encienda su terminal. La instalación se iniciará rápidamente.
* Al finalizar el arranque, la instalación debería haberse realizado correctamente, con reconocimiento automático de la caja, apagado seguro y botones.
* Pulsando el botón de encendido de la caja, el sistema se apagará limpiamente, la fuente de alimentación se cortará y el ventilador se detendrá.

A continuación, puede proceder a la instalación en su disco duro, de la manera clásica:

* Encienda de nuevo su caja hasta que esté completamente cargada.
* Con su mando, pulse el botón `START` y vaya a `SYSTEM SETTINGS` > `STORAGE MEDIA` y elija el disco duro. La instalación en el disco duro también reconoce la caja, por lo que la configuración es automática.

No hay absolutamente nada más que hacer, ni scripts que instalar aparte.

#### Utilización

Utiliza tu Recalbox de la forma clásica. El botón `Power` apaga limpiamente, como se ha descrito anteriormente. `Reset` hace un reinicio limpio.

### Usando el Nespi4Case sin disco duro (para una conexión de red, por ejemplo)

En este caso, si el cable USB conectado al SATA está enchufado, se pueden producir los síntomas detallados al principio de este tutorial.

#### Instalación

En primer lugar, flashea tu tarjeta µSD utilizando tu software favorito.

A continuación, asegúrese de que el botón safeshutdown de la carcasa está en ON (consulte el manual de la carcasa) para utilizar el modo safeshutdown. Y asegúrese de desconectar el cable USB3 de la parte superior de la carcasa (conectado al puerto SATA) del puerto USB3 del Pi4. La parte superior de la carcasa no debe estar conectada a la parte inferior.

A continuación, inserta tu tarjeta µSD en la ranura prevista para ello en la carcasa.

![](/tutorials/others/nespi4case-installation/nespi4case2.jpg)

* Encended. la instalación comienza rápidamente.
* Cuando el inicio termina, la instalación ha sido completada, pero nos queda la parte del reconocimiento del nespi4case.

#### Comprobación de la instalación

Queda por realizar una pequeña manipulación.

* Conectad vuestra Recalbox a internet, conectaros vía SSH  y autorizad la lectura-escritura en vuestra [partición de arranque](./../system/access/remount-partition-with-write-access).
* Con la ayuda de vuestro programa favorito ([WinSCP](./../system/access/network-access-winscp) / [Cyberduck](./../system/access/network-access-cyberduck) / [MobaXTerm](./../system/access/network-access-mobaxterm)), buscad el fichero  `/boot/recalbox-boot.conf` y abridlo.
* Buscad la siguiente línea y borradla:

```ini
case=none:1
```

* Guardad y apagad el sistema correctamente.
* Conectad el cable USB3 de la parte superior de la caja al puerto USB3 disponible en el Pi4 y encended vuestra Recalbox.

Esto es un poco aleatorio. No hay una explicación lógica pero a veces la caja es reconocida, a veces no. Para comprobarlo, cuando todo el sistema está funcionando:

* Conectaros vía [SSH](./../system/access/root-access-terminal-cli) y leer el fichero `/boot/recalbox-boot.conf`.

![](/tutorials/others/nespi4case-installation/nespi4case3.jpeg)

Si os encontráis con esta lína:

```ini
case=none:1
```

Significa que la caja no ha sido bien reconocida. En este caso, volved a hacer la instalación.

![](/tutorials/others/nespi4case-installation/nespi4case4.jpeg)

Si os encontráis con esta otra línea:

```ini
case=NESPi4:1
```

Esto significa que la caja ha sido detectada. En este momento, puede apagar y desconectar el cable USB3 de la caja Pi4 (dado que no se está utilizando ningún disco duro, esto evitará los problemas descritos anteriormente) manteniendo la funcionalidad de la caja activa ya que ha sido detectada.

![](/tutorials/others/nespi4case-installation/nespi4case5.jpeg)

>Si tenéis un disco duro pero no queréis utilizarlo para la nespi4case, se aconseja proceder a la instalación en el disco duro, sin pasar por los pasos para definir el disco duro como medio de almacenamiento. Y cuando la caja sea reconocida, retirad el cable UB3 del Pi4 de la misma manera.
{.is-info}

## Créditos

Este tutorial se encuentra en el foro en [este thread](https://forum.recalbox.com/topic/24390/recalbox-v7-2-1-tutoriel-d-installation-du-boitier-nespi4case). Podéis participar si tenéis problemas después de haber seguido paso a paso este tutorial.