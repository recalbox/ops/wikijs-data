---
title: XRoar
description: 
published: true
date: 2024-07-24T22:02:49.188Z
tags: dragon32, dragon64, xroar, 8.0+
editor: markdown
dateCreated: 2021-08-22T17:29:03.111Z
---



## ![](/emulators/license.svg) Licence

Ce core est sous licence [**GPL-3.0**](https://gitlab.com/recalbox/packages/standalone/xroar/-/blob/master/COPYING.GPL) et [**LGPL-3.0**](https://gitlab.com/recalbox/packages/standalone/xroar/-/blob/master/COPYING.LGPL).

## ![](/emulators/compatibility.png) Compatibilité

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Fonctionnalités

| Fonctionnalité | Supporté |
| :---: | :---: |


## ![](/emulators/bios.svg) BIOS

### Liste des bios obligatoires

| Nom de fichier | Description | MD5 | Fourni |
| :---: | :---: | :---: | :---: |
| d32.rom | Dragon32 system rom | 3420b96031078a4ef408cad7bf21a33f | ❌ |
| d64rom1.rom | Dragon64 system rom #1 | 5f0bee59710e55f5880e74890912ed78 | ❌ |
| d64rom2.rom | Dragon64 system rom #2 | fd91edce7be5e7c2d88e46b76956a8aa | ❌ |
| d64tano.rom | Tano system rom #1 | be9bc86ee5eb401d0a40d0377f65fefa | ❌ |
| d64tano2.rom | Tano system rom #2 | fd91edce7be5e7c2d88e46b76956a8aa | ❌ |

### Emplacement

Placez les bios comme ceci : 

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 dragon
┃ ┃ ┃ ┃ ┣ 🗒 **d32.rom**
┃ ┃ ┃ ┃ ┣ 🗒 **d64rom1.rom**
┃ ┃ ┃ ┃ ┣ 🗒 **d64rom2.rom**
┃ ┃ ┃ ┃ ┣ 🗒 **d64tano.rom**
┃ ┃ ┃ ┃ ┣ 🗒 **d64tano2.rom**

### Liste des bios optionnels

| Nom de fichier | Description | MD5 | Fourni |
| :---: | :---: | :---: | :---: |
| d200rom1.rom | Dragon 200e system rom #1 | be9bc86ee5eb401d0a40d0377f65fefa | ❌ |
| d200rom2.rom | Dragon 200e system rom #2 | fd91edce7be5e7c2d88e46b76956a8aa | ❌ |
| ddos10.rom | Dragon DOS 1.0 | 1c965da49b6c5459b8353630aa1482e7 | ❌ |
| ddos11c.rom | Dragon DOS 1.1 | d8429af1a12f7438a4bf88a5b934cb3a | ❌ |
| ddos12a.rom | Dragon DOS 1.2 | 55e2535dbbed7f1a26b5f263d7c72c63 | ❌ |
| ddos40.rom | Dragon DOS 4.0 | 9ddc388632cd3c376b164ba5cfc64329 | ❌ |
| ddos42.rom | Dragon DOS 4.2 | c956a854cbc4b9d1e69c000f78368668 | ❌ |
| deltados.rom | Delta DOS | 024eac3db20f1b5cf98c30a0e4743201 | ❌ |
| dplus48.rom | DOS Plus 4.8 | ee6f24d893a52b8efea9f787855456b5 | ❌ |
| dplus49b.rom | DOS Plus 4.9 | 56f1b97314e4ca82491c465bb887059e | ❌ |
| dplus50.rom | DOS Plus 5.0 | 35de5d28da507ebb213a26e04241d940 | ❌ |
| sdose6.rom | Super DOS E6 | 9d85e6b7133f915c021156f4b9cdb512 | ❌ |
| sdose8.rom | Super DOS E8 | 167f409b7a4b992faabb784b061ab4c6 | ❌ |

### Emplacement

Placez les bios comme ceci : 

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 dragon
┃ ┃ ┃ ┃ ┣ 🗒 **d200rom1.rom**
┃ ┃ ┃ ┃ ┣ 🗒 **d200rom2.rom**
┃ ┃ ┃ ┃ ┣ 🗒 **ddos10.rom**
┃ ┃ ┃ ┃ ┣ 🗒 **ddos11c.rom**
┃ ┃ ┃ ┃ ┣ 🗒 **ddos12a.rom**
┃ ┃ ┃ ┃ ┣ 🗒 **ddos40.rom**
┃ ┃ ┃ ┃ ┣ 🗒 **ddos42.rom**
┃ ┃ ┃ ┃ ┣ 🗒 **deltados.rom**
┃ ┃ ┃ ┃ ┣ 🗒 **dplus48.rom**
┃ ┃ ┃ ┃ ┣ 🗒 **dplus49b.rom**
┃ ┃ ┃ ┃ ┣ 🗒 **dplus50.rom**
┃ ┃ ┃ ┃ ┣ 🗒 **sdose6.rom**
┃ ┃ ┃ ┃ ┣ 🗒 **sdose8.rom**

## ![](/emulators/roms.png) Roms

### Extensions supportées

La rom doit avoir l'extension :

* .cas
* .wav
* .bas
* .asc
* .dmk
* .jvc
* .os9
* .dsk
* .vdk
* .rom
* .ccc

### Emplacement

Placez les roms comme ceci : 

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 dragon
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.dsk**

>Pour plus d'information sur les roms, rendez-vous sur [ce tutoriel](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuration avancée de l'émulateur

>Pour pouvoir conserver vos configurations personnalisées lors d'une mise à jour, nous vous conseillons d'utiliser notre fonctionnalité [Surcharges de configuration](./../../../advanced-usage/configuration-override).
{.is-info}

## ![](/emulators/external-links.png) Liens externes

* **Code source utilisé** : [https://gitlab.com/recalbox/packages/standalone/xroar/](https://gitlab.com/recalbox/packages/standalone/xroar/)
* **Site web officiel** : [https://www.6809.org.uk/xroar/](https://www.6809.org.uk/xroar/)