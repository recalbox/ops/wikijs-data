---
title: Aviso Netplay Dolphin
description: Jugar online en Gamecube y Wii
published: true
date: 2023-08-15T22:26:56.014Z
tags: dolphin, netplay
editor: markdown
dateCreated: 2023-08-15T22:26:56.014Z
---

> Tened en cuenta que para jugar online con la Gamecube, se necesita una configuración especial además de un teclado y un ratón.
{.is-info}

## Acceder al menu Netplay

* Lanzad cualquier juego Gamecube o Wii.
* Con un teclado una vez dentro del juego, presionad `ALT` + `F4` par aacceder al menu de Dolphin.
* Dentro del menu, utilizando el ratón, abrid `Tools`.

![](/basic-usage/features/netplay/dolphin-netplay-tools-menu.png){.full-width}

### Creación de una sesión Netplay como anfitrión

* Click en `Start Netplay...`

![](/basic-usage/features/netplay/dolphin-host-netplay.png){.full-width}

* Haz click en la pestaña `Host`, selcciona una región en el menu desplegable sobre la lista de juegos, selecciona tu juego en la lista y haz click en el botón ´Host´.

>En esta ventana puedes definir una contraseña para la sala de Netplay además de un puerto en caso que necesites cambiar el puerto por defecto.
{.is-info}

![](/basic-usage/features/netplay/dolphin-netplay-host-setting.png){.full-width}

* Una ventana de chat aparecerá y tu sesión de Netplay estará lista!.

![](/basic-usage/features/netplay/dolphin-netplay-host-chat-room.png){.full-width}

>En la mayoría de los juegos, añade alrededor de 1 Buffer por cada 15 ms de latencia por cliente.
{.is-warning}

>Dos jugadores con 50 ms de latencia son entre 3 y 4 buffers, mientras que tres jugadores entre 50 y 65 ms de latencia necesitan alrededor de 7 buffers.
{.is-success}

* Para empezar el juego, click `Start` !

### Unirse a una sesión de Netplay (cliente)

* Click en `Browse NetPlay Sessions....`.

![](/basic-usage/features/netplay/dolphin-netplay-client-en.png){.full-width}

* En la sala de Netplay de Dolphin, puedes observar los juegos online disponibles en tiempo real.

![](/basic-usage/features/netplay/dolphin-netplay-client-lobby-en.png){.full-width}

* Haz doble click en la sesión que te interesa para tener acceso a la ventana de chat.

>Si se ha configurado una contraseña, se te pedirá en este momento.
{.is-info}

* Espera hastat que el juego sea lanzado por el anfitrión de la sesión de netplay!

![](/basic-usage/features/netplay/dolphin-netplay-client-chat-room-en.png){.full-width}