---
title: Libretro Tic80
description: 
published: true
date: 2024-07-14T19:19:00.714Z
tags: libretro, tic-80, tic80
editor: markdown
dateCreated: 2023-09-11T06:21:50.221Z
---

Libretro TIC-80 es un emulador que te permite emular un ordenador virtual, para crear, jugar y compartir pequeños juegos.

## ![](/emulators/license.svg) Licencia

El core posee la siguiente licencia [**MIT**](https://github.com/libretro/TIC-80/blob/master/LICENSE).

## ![](/emulators/compatibility.png) Compatibilidad

 | RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | 
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Funcionalidades

| Funcionalidad | ¿Soportada? |
| :---: | :---: |
| Reinicio | ✔ |
| Grabaciones | ✔ |
| Opciones del core | ✔ |
| Netplay | ✔ |
| Cheats nativos| ✔ |
| Controles | ✔ |

## ![](/emulators/bios.svg) BIOS

>**No necesita bios.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Extensiones de roms soportadas

Las roms deben tener las extensiones siguientes:

* .fd
* .sap
* .k7
* .m7
* .rom
* .zip
* .7z

Este sistema soporta las roms comprimidas en formato .zip/.7z. No obstante, se trata sólo de un archivo.

Los ficheros contenidos en los .zip/.7z deben tener las extensiones citadas previamente.
Además, cada fichero .zip/.7z sólo debe contener un fichero rom al interior.

### Emplazamiento

Disponed vuestras roms como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 tic80
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.zip**

## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

>Para conservar las configuraciones personalizadas cuando se produce una actualización del sistema Recalbox, os aconsejamos utilizar las [Sobrecargas de configuración](./../../../advanced-usage/configuration-override).
{.is-info}

### Acceder a las opciones

Podéis configurar las opciones del core de dos formas diferentes.

* Via el Menú RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via el fichero `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Opciones del core

| Opción | Valor de la opción | Variable (para la sobrecarga) | Valores posibles |
| --- | --- | --- | --- |
| Crop Border | `Désactivé` ✅ / `Activé` | `tic80_crop_border` | `disabled` / `enabled` |
| Pointer Device | `Mouse` ✅ / `Touchscreen (Pointer API)` / `Left Analog` / `Right Analog` / `D-Pad` | `tic80_pointer_device` | `mouse` / `touchscreen` / `left_analog` / `right_analog` / `dpad` |
| Pointer Speed | `50%` / `60%` / `70%` / `80%` / `90%` / `100%` ✅ / `110%` / `120%` / `130%` / `140%` / `150%` / `160%` / `170%` / `180%` / `190%` / `200%` / `210%` / `220%` / `230%` / `240%` / `250%` / `260%` / `270%` / `280%` / `290%` / `300%` | `tic80_pointer_speed` | `50` / `60` / `70` / `80` / `90` / `100` / `110` / `120` / `130` / `140` / `150` / `160` / `170` / `180` / `190` / `200` / `210` / `220` / `230` / `240` / `250` / `260` / `270` / `280` / `290` / `300` |
| Mouse Cursor | `Désactivé` ✅ / `Dot` / `Cross` / `Arrow` | `tic80_mouse_cursor` | `disabled` / `dot` / `cross` / `arrow` |
| Mouse Cursor Color | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` ✅ | `tic80_mouse_cursor_color` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` |
| Mouse Cursor Hide Delay | `Disabled` / `1` / `2` / `3` / `4` / `5` ✅ / `6` / `7` / `8` / `9` / `10` | `tic80_mouse_hide_delay` | `disabled` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` |
| Gamepad Analog Deadzone | `0%` / `3%` / `6%` / `9%` / `12%` / `15%` ✅ / `18%` / `21%` / `24%` / `27%` / `30%` | `tic80_analog_deadzone` | `0` / `3` / `6` / `9` / `12` / `15` / `18` / `21` / `24` / `27` / `30` |

## ![](/emulators/external-links.png) Enlaces externos

* **Código fuente utilizadoé** : [https://github.com/libretro/TIC-80/](https://github.com/libretro/TIC-80)
* **Código fuente oficial** : [https://github.com/nesbox/TIC-80/](https://github.com/nesbox/TIC-80/)
* **Página oficial** : [https://tic.computer/](https://tic.computer/)
* **Wiki fuente oficial** : [https://github.com/nesbox/TIC-80/wiki](https://github.com/nesbox/TIC-80/wiki)
* **Base de datos Libretro** : [Libretro TIC-80 Database](https://github.com/libretro/libretro-database/blob/master/rdb/TIC-80.rdb)
* **Códigos de trucos Libretro Tic-80** : [https://github.com/libretro/libretro-database/tree/master/cht/TIC-80/](https://github.com/libretro/libretro-database/tree/master/cht/TIC-80)