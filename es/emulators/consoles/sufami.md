---
title: SuFami Turbo
description: Bandai SuFami Turbo
published: true
date: 2024-07-26T08:06:00.430Z
tags: bandai, sufami turbo, consolas
editor: markdown
dateCreated: 2023-09-03T20:20:49.547Z
---

![](/emulators/consoles/sufamiturbo.svg){.align-center}

## Detalles técnicos

* **Fabricante**: Bandai
* **Año de lanzamiento**: 1996
* **Soportes**: Aplicación en cartucho SuFami Turbo + cartuchos de juego

## Presentación

El **SuFami Turbo**, a veces comparado con el Aladdin Deck Enhancer, es un accesorio suministrado por Bandai para la Super Famicom de Nintendo y fue lanzado en 1996.

El dispositivo está diseñado para encajarse en la parte superior de la Super Famicom y contiene 2 ranuras para cartuchos. El objetivo es que los juegos puedan producirse a menor coste, así como reducir el tiempo de desarrollo, al no depender de la producción de cartuchos de Nintendo. Al igual que el Aladdin Deck Enhancer, este dispositivo ha sido aprobado oficialmente por Nintendo con la condición de que Bandai produzca todo el hardware por sí misma.

Las 2 ranuras para cartuchos están diseñadas para compartir datos entre cartuchos. El cartucho de la primera ranura es el juego que se está jugando, mientras que el cartucho de la segunda ranura proporciona datos adicionales para su uso en el juego principal. De los trece juegos disponibles, 9 de ellos se pueden enlazar, entre cada serie de juegos.

Los juegos que pueden enlazarse se identifican mediante un diagrama amarillo que muestra un SuFami Turbo con uno o dos cartuchos en la esquina inferior derecha de la caja del juego. Si la imagen tiene 1 cartucho insertado en el SuFami Turbo, el juego no puede vincularse. Si la imagen tiene 2 cartuchos insertados en el SuFami Turbo, el juego puede enlazarse con la lista de juegos que aparece en la caja.

El SuFami Turbo estaba disponible solo o en un pack con un juego.

## Emuladores

[Libretro Snes9x](libretro-snes9x)
