---
title: Amiberry
description: 
published: true
date: 2024-06-23T00:17:50.855Z
tags: amiga-cdtv, amiga, amiberry, cdtv, 8.0+
editor: markdown
dateCreated: 2021-09-12T21:21:17.375Z
---

**Amiberry** est un core pour ARM optimisé pour Amiga.

## ![](/emulators/license.svg) Licence

Ce core est sous licence [**GPLv3**](https://github.com/midwan/amiberry/blob/master/COPYING).

## ![](/emulators/compatibility.png) Compatibilité

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ❌ | ✅ | ✅ | ✅ | ✅ | ✅  🐌 | ❌ | ✅ |

🐌  Basses performances mais jouable

## ![](/emulators/features.png) Fonctionnalités



## ![](/emulators/bios.svg) BIOS

### Liste des bios obligatoires

Vous trouverez les bios avec le nom indiqué dans la colonne **Description** qu'il vous faudra renommer avec le nom indiqué dans la colonne **Nom de fichier**.

| Nom de fichier | Description | MD5 | Fourni |
| :---: | :---: | :---: | :---: |
| kick34005.CDTV | CDTV Extended-ROM v1.0 (1991)(Commodore)(CDTV)\[!\] | 89da1838a24460e4b93f4f0c5d92d48d d98112f18792ee3714df16a6eb421b89 d1145ab3a0f89340f94c9e734762c198 | ❌ |

### Emplacement

Placez les BIOS comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 🗒 **kick34005.CDTV**

## ![](/emulators/isos.svg) Isos

### Extensions supportées

Les isos doivent avoir les extensions suivantes :

* .bin/.cue
* .iso
* .img/.ccd

### Emplacement

Placez les isos comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 amigacdtv
┃ ┃ ┃ ┃ ┣ 🗒  **jeu.cue**
┃ ┃ ┃ ┃ ┣ 🗒  **jeu.bin**

>Les isos au format **Redump** sont vivement conseillés.
{.is-success}

>Pour plus d'information sur les isos, rendez-vous sur [ce tutoriel](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuration avancée de l'émulateur

>Pour pouvoir conserver vos configurations personnalisées lors d'une mise à jour, nous vous conseillons d'utiliser notre fonctionnalité [Surcharges de configuration](./../../../advanced-usage/configuration-override).
{.is-info}

## ![](/emulators/external-links.png) Liens externes

* **Code source utilisé** : [https://github.com/midwan/amiberry/](https://github.com/midwan/amiberry/)
* **Documentation** : [https://blitterstudio.com/amiberry/](https://blitterstudio.com/amiberry/)