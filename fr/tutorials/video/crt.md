---
title: CRT
description: 
published: true
date: 2023-09-19T20:36:07.248Z
tags: crt
editor: markdown
dateCreated: 2021-05-21T08:11:30.047Z
---

Vous y trouverez ici des tutoriaux concernant les branchements de votre Recalbox sur un écran CRT.

Voici les tutoriels disponibles :

[Configurez votre écran cathodique sur Raspberry Pi 4 / 400 / 3](recalbox-on-crt-with-scart-dac)
[Configurez votre écran cathodique sur le bus DPI (VGA666 / PiScart / RGBPi)](crt-screen-dpi-vga666-piscart-rgbpi)
[Connectez votre Recalbox à un CRT avec composite](crt-screen-with-composite)
[Connectez votre Recalbox à un écran cathodique avec HDMI et SCART](crt-screen-with-hdmi-and-scart)
[Connectez votre Recalbox à un écran CRT en utilisant la sortie HDMI](crt-screen-with-hdmi)