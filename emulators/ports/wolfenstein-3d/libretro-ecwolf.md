---
title: Libretro ECWolf
description: 
published: true
date: 2025-01-04T06:15:47.582Z
tags: libretro, wolfenstein3d, ecwolf
editor: markdown
dateCreated: 2021-06-29T22:55:58.045Z
---

**Free ECWolf** is a source port game engine for Wolfenstein 3D. This port is courtesy of phcoder.

ECWolf is a port of the Wolfenstein 3D engine based on Wolf4SDL. It combines the original Wolfenstein 3D engine with the ZDoom user experience to create the most user and modifier friendly Wolf3D source port.

Like ZDoom, ECWolf aims to support all games that use the Wolfenstein 3D engine, including Blake Stone (coming in ECWolf 3.0), Corridor 7, Operation Body Count, Rise of the Triad and Super 3D Noah's Ark. ECWolf will also support Macintosh Wolfenstein 3D with all its user-created missions (coming in ECWolf 2.0).

* A single binary runs all supported games. (Wolfenstein 3D, Spear of Destiny, ...)
* Full support for high resolution modes with aspect ratio correction, including widescreen support.
* Modern control schemes (WASD + mouse).
* Mac Wolf / S3DNA / ROTT style automap.
* Unlimited backup locations.
* This is actually based on the Wolf3D engine instead of recreating or forcing in a more modern engine.
* Software rendered using the same 8-bit raytracing.

ECWolf can run the following content:

* Wolfenstein 3D
* Spear of Destiny
* Super 3D Noah's Ark

## ![](/emulators/license.svg) License

This core is under [**GPLv2**](https://github.com/libretro/ecwolf/blob/master/docs/license-gpl.txt) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Features



## ![](/emulators/bios.svg) BIOS

>**No bios is required.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Supported extensions

Roms must have the extension:

* .exe
* .pk3
* .zip
* .7z

### Location

Put your roms like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 ports
┃ ┃ ┃ ┃ ┣ 📁 Wolfenstein 3D
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **ecwolf.pk3**

This rom is included in your Recalbox.

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Accessing to options

You can configure various options in two different ways.

* Via RetroArch menu:

┣ 📁 RetroArch Menu
┃ ┣ 📁 Core options
┃ ┃ ┣ 🧩 Name_option

* Via the file `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core options

| Options | Options values | Variables (for configuration override) | Variables values |
| --- | --- | --- | --- |
| Internal resolution | `320x200` ✅ / `320x240` / `400x240` / `420x240` / `480x270` / `640x360` / `640x400` / `640x480` / `800x500` / `960x540` / `960x600` / `1024x768` / `1280x720` / `1280x800` / `1600x1000` / `1920x1200` / `2240x1400` / `2560x1600` | `ecwolf-resolution` | `320x200` / `320x240` / `400x240` / `420x240` / `480x270` / `640x360` / `640x400` / `640x480` / `800x500` / `960x540` / `960x600` / `1024x768` / `1280x720` / `1280x800` / `1600x1000` / `1920x1200` / `2240x1400` / `2560x1600` |
| Analog deadzone | `0%` / `5%` / `10%` / `15%` ✅ / `20%` / `25%` / `30%` | `ecwolf-analog-deadzone` | `0` / `5` / `10` / `15` / `20` / `25` / `30` |
| Refresh rate (FPS) | `7` / `7.8` / `8.8` / `10` / `14` / `17.5` / `25` / `30` / `35` ✅ / `50` / `60` / `70` / `72` / `75` / `90` / `100` / `119` / `120` / `140` / `144` / `240` / `244` / `300` / `360` | `ecwolf-fps` | `7` / `7.8` / `8.8` / `10` / `14` / `17.5` / `25` / `30` / `35` / `50` / `60` / `70` / `72` / `75` / `90` / `100` / `119` / `120` / `140` / `144` / `240` / `244` / `300` / `360` |
| Preferred palette format (Restart) | `RGB565 (16-bit)` ✅ / `XRGB8888 (24-bit)` | `ecwolf-palette` | `rgb565` / `xrgb8888` |
| Always run | `Disabled` ✅ / `Enabled` | `ecwolf-alwaysrun` | `disabled` / `enabled` |
| Screen size | `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `Largest with statusbar` ✅ / `Without statusbar` | `ecwolf-viewsize` | `4` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `21` |
| Show map as overlay | `Disabled` ✅ / `Enabled` / `On + Normal` | `ecwolf-am-overlay` | `off` / `on` / `both` |
| Rotate map | `Disabled` ✅ / `Enabled` / `Overlay only` | `ecwolf-am-rotate` | `off` / `on` / `overlay_only` |
| Textures walls in automap | `Disabled` ✅ / `Enabled` | `ecwolf-am-drawtexturedwalls` | `disabled` / `enabled` |
| Textured floors in automap | `Disabled` ✅ / `Enabled` | `ecwolf-am-drawtexturedfloors` | `disabled` / `enabled` |
| Textured Overlay in automap | `Disabled` ✅ / `Enabled` | `ecwolf-am-texturedoverlay` | `disabled` / `enabled` |
| Show level ratios in automap | `Disabled` ✅ / `Enabled` | `ecwolf-am-showratios` | `disabled` / `enabled` |
| Pause game in automap | `Disabled` ✅ / `Enabled` | `ecwolf-am-pause` | `disabled` / `enabled` |
| Volume of music | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` ✅ | `ecwolf-music-volume` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` |
| Volume of digitized sound effects | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` ✅ | `ecwolf-digi-volume` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` |
| Volume of Adlib sound effects | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` ✅ | `ecwolf-adlib-volume` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` |
| Volume of Speaker sound effects | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` ✅ | `ecwolf-speaker-volume` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` |
| Analog move and strafe sensitivity | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` ✅ | `ecwolf-analog-move-sensitivity` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` |
| Analog turn sensitivity | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` ✅ | `ecwolf-analog-turn-sensitivity` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` |
| Order of lookup for effects | `Digitized, Adlib, Speaker` ✅ / `Digitized, Adlib` / `Digitized only` / `Adlib only` / `Speaker only` | `ecwolf-effects-priority` | `digi-adlib-speaker` / `digi-adlib` / `digi-speaker` / `digi` / `adlib` / `speaker` |
| Aspect | `Auto` ✅ / `16:9` / `4:3` / `16:10` / `17:10` / `5:4` / `21:9` | `ecwolf-aspect` | `auto` / `16:9` / `4:3` / `16:10` / `17:10` / `5:4` / `21:9` |
| Invulnerability | `Disabled` ✅ / `Enabled` | `ecwolf-invulnerability` | `disabled` / `enabled` |
| Dynamic FPS | `Disabled` ✅ / `Enabled` | `ecwolf-dynamic-fps` | `disabled` / `enabled` |
| Store files in memory | `Disabled` ✅ / `Enabled` | `ecwolf-memstore` | `disabled` / `enabled` |
| Horizontal panning speed in automap | `0` / `1` / `2` / `3` / `4` / `5` ✅ / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` | `ecwolf-panx-adjustment` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` |
| Vertical panning speed in automap | `0` / `1` / `2` / `3` / `4` / `5` ✅ / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` | `ecwolf-paxy-adjustment` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` |

## Creating mods

* Create mods without working with the source code!
  * Mods work with all supported platforms, including Windows, macOS and Linux.
* Support for arbitrary high resolution texture, flat and sprite.
* Unlimited simultaneous mobile pushwalls.
* Unlimited things.
* Unlimited map size (although technical limitations limit GAMEMAPS to 181x181).
* Uses scripting and data formats similar to ZDoom.
  * Doom editing utilities work with ECWolf (except for levels).

Note that until ECWolf 2.0, although radical changes are not excluded, backward compatibility of mods will not necessarily be maintained. See [the wiki](http://maniacsvault.net/ecwolf/wiki/Version_compatibility) for more information.

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/libretro/ecwolf/](https://github.com/libretro/ecwolf)
* **Official website**: [http://maniacsvault.net/ecwolf/](http://maniacsvault.net/ecwolf/)
* **Official forum**: [https://forum.drdteam.org/viewforum.php?f=174](https://forum.drdteam.org/viewforum.php?f=174)
* **Official documentation**: [http://maniacsvault.net/ecwolf/wiki/Main_Page](http://maniacsvault.net/ecwolf/wiki/Main_Page)