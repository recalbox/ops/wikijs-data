---
title: Fix sound issues on frontend
description: 
published: true
date: 2024-07-26T07:14:33.199Z
tags: emulationstation, sound
editor: markdown
dateCreated: 2021-11-16T08:46:31.737Z
---

## Introduction

In some cases, depending on the hardware used, the sound in fontend may sound abnormal, like jerky. This short tutorial might help you if you are in the same case.

## Usage

To do this, you need to connect via [SSH](./../../tutorials/system/access/root-access-terminal-cli) and execute the following 2 lines:

```shell
mount -o remount,rw /
echo 'default-sample-rate = 48000' >> /etc/pulse/daemon.conf
```

Reboot and the sound should be correct.

>You will need to do this again after each system update.
{.is-info}