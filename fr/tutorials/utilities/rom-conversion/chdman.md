---
title: CHDMAN
description: Convertir vos roms CD en un format compressé lisible.
published: true
date: 2024-10-17T16:49:02.548Z
tags: chd, chdman
editor: markdown
dateCreated: 2021-05-21T08:42:11.451Z
---

## Introduction

Les formats de fichiers à adopter pour les CDs et autres supports optiques.

Nous avons parlé d'un format, dérivé de recherches dont nous avons eus sur MAME, afin de pouvoir représenter au mieux ces supports, de manière compressée, sans perdre l'intégrité des données et que celles-ci puissent rester scrappables.

Ce format est le CHD (Compressed Hunks of Data), datas qui pourraient être pratiques pour les consoles SEGA CD, PS1, PC Engine...

Malheureusement, l'utilisateur classique aura du mal à utiliser le convertisseur, qui est un exécutable en ligne de commandes.

Voici donc un fichier Zip, avec des scripts automatisés, pour passer du format BIN+CUE (format Redump) vers CHD, et inversement.

Il a été ajouté aussi pour le format GDI, qui est pour la Dreamcast.

Pour les jeux PS1 protégés par LibCrypt, normalement vous avez des fichiers SBI (Subchannel Information), vous les conservez et les mettre avec les CHDs, sinon vos jeux ne passeront pas.

Si jamais vous lez perdez ou vous effectuez une mauvaise manipulation, ces fichiers sont disponibles sur les fiches de chaque disque correspondant chez Redump, à côté des fichiers CUE retéléchargeables.

## Logiciel

## {.tabset}
### Windows

Vous pouvez télécharger le logiciel en cliquant sur **CHDMAN.zip** ci-dessous.

![](/tutorials/utilities/rom-conversion/chdman/archive.svg) [CHDMAN.zip](/tutorials/utilities/rom-conversion/chdman/chdman.zip)

Cette version du logiciel utilise la version 0.257 de CHDMan. Vous pouvez télécharger une version plus récente de **chdman.exe** (sans les scripts de conversion, qui restent inchangés) sur le site officiel de MAME en récupérant l'exécutable de la dernière version disponible : https://www.mamedev.org/release.html.

**Exemple de compression d'une ROM Mega-CD classique avec 10 fichiers .CUE + .BIN --> .CHD :**

Version 0.257 : 1 minute et 7 secondes
Version 0.269 : 35 secondes



Dans ce zip, vous trouverez 6 fichiers :

| Nom de fichier | Description |
| :--- | :--- |
| **chdman.exe** | Le logiciel CHDMAN. |
| **CUE-GDI-ISO to CHD.bat** | Un fichier .bat qui vous permet de convertir vos roms aux formats `CUE` ou `GDI` en `CHD`. |
| **Extract CHD to CUE.bat** | Un fichier .bat qui vous permet de convertir vos roms au format `CHD` en `BIN`/`CUE`. |
| **Extract CHD to GDI.bat** | Un fichier .bat qui vous permet de convertir vos rom au format `CHD` en `GDI`. |
| **README_EN.txt** |  |
| **README_FR.txt** |  |

#### Contenu du fichier Readme

* **CUE-GDI-ISO to CHD**

  Compresse tous types de fichiers disques BIN avec un entête CUE ou GDI vers le format CHD (v5).
  Recherche tous les sous-dossiers et crée des fichiers CHD (v5) dans le dossier dans lequel les fichiers sont placés avec CHDMAN.

* **Extract CHD to CUE**

  Décompresse un fichier CHD (V5) en fichier BIN+CUE. 
  Le format CUE est utilisé par les jeux sur CD. Sur le Raspberry Pi, CHD est pris en charge par TurboGrafx-CD / PC Engine CD, Sega CD / Mega CD et Dreamcast.

* **Extract CHD to GDI**
  Décompresse un fichier CHD (V5) vers GDI. (GDI est un format disque pour Dreamcast).

### macOS

CHDMAN existe pour macOS via Homebrew.

* Installation des outils Xcode

En premier, vous devez exécuter cette commande dans le Terminal :

`xcode-select --install`

* Installation de [**Homebrew**](https://brew.sh/index_fr)

Ensuite, vous devez utiliser cette commande dans le Terminal :

`/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"`

* Si vous êtes sur un Mac avec puce Apple Silicon, vous avez cette commande en plus à exécuter :

`echo 'eval $(/opt/homebrew/bin/brew shellenv)' >> ~/.zprofile`

* Installation de [**rom-tools**](https://formulae.brew.sh/formula/rom-tools)

Vous devez utiliser la commande suivante dans le Terminal un fois Homebrew installé :

`brew install rom-tools`

macOS 10.13 High Sierra minimum est requis.

Voici un lien pour télécharger des fichiers pour automatiser les conversions de format :

![](/tutorials/utilities/rom-conversion/chdman/archive.svg) [ChdScripts.zip](/tutorials/utilities/rom-conversion/chdman/chdscripts.zip)

Dans ce zip, vous trouverez 6 fichiers :

| Nom de fichier | Description |
| :--- | :--- |
| **<span>convertFromChdToCue.sh</span>** | Un fichier .sh qui vous permet de convertir vos roms au format `CHD` en `BIN`/`CUE`. |
| **<span>convertFromChdToGdi.sh</span>** | Un fichier .sh qui vous permet de convertir vos roms au format `CHD` en `GDI`. |
| **<span>convertFromChdToIso.sh</span>** | Un fichier .sh qui vous permet de convertir vos roms au format `CHD` en `ISO`. |
| **<span>convertFromCueToChd.sh</span>** | Un fichier .sh qui vous permet de convertir vos roms au format `BIN`/`CUE` en `CHD`. |
| **<span>convertFromGdiToChd.sh</span>** | Un fichier .sh qui vous permet de convertir vos roms au format `GDI` en `CUE`. |
| **<span>convertFromIsoToChd.sh</span>** | Un fichier .sh qui vous permet de convertir vos roms au format `ISO` en `CUE`. |
| **README_EN.txt** |  |
| **README_FR.txt** |  |

#### Contenu du fichier Readme

* **<span>convertFromChdToCue.sh</span>**

  Décompresse un fichier CHD (V5) en fichier BIN+CUE.
  Le format CUE est utilisé par les jeux sur CD. CHD est pris en charge par 3DO, Amiga CD32, Amiga CDTV, Dreamcast, Mega CD, Neo-Geo CD, PC Engine CD, PlayStation et Saturn.

* **<span>convertFromChdToGdi.sh</span>**
  Décompresse un fichier CHD (V5) en fichier GDI.
  Le format GDI est utilisé par les jeux sur disque pour Dreamcast.

* **<span>convertFromChdToIso.sh</span>**
  Décompresse un fichier CHD (V5) en fichier ISO.
  Le format ISO est utilisé par les jeux sur disque pour PlayStation 2.

* **<span>convertFromCueToChd.sh</span>**

  Compresse tout type de fichiers disques BIN avec un entête CUE vers le format CHD (v5). Recherche tous les sous-dossiers et crée des fichiers CHD (v5) dans le dossier dans lequel les fichiers sont placés avec CHDMAN.

* **<span>convertFromGdiToChd.sh</span>**

  Compresse tout type de fichiers disques BIN avec un entête GDI vers le format CHD (v5). Le format GDI est utilisé par les jeux sur disque pour Dreamcast.

* **<span>convertFromIsoToChd.sh</span>**

  Compresse tout type de fichiers disques ISO vers le format CHD (v5). Le format ISO est utilisé par les jeux sur disque pour PlayStation 2.

### Linux

Certaines distributions de Linux peuvent vous le fournir en passant par `aptitude` et en installant le paquetage `mame-tools`. D'autres distributions n'ont pas de `mame-tools` et vous devez builder votre chdman :

* Rendez-vous sur la page https://github.com/libretro/mame
* Téléchargez le contenu en cliquant sur le bouton vert `Code` et, dans le menu visible, cliquez sur `Download ZIP`

![](/tutorials/utilities/rom-conversion/chdman/mamedownload.png){.full-width}

* Décompressez l'archive obtenu et allez dans le répertoire crée. Vous devez retrouver le contenu du lien donné en première étape.
* Depuis votre terminal, utilisez la commande suivante :

```ssh
make tools
```

* À la fin, vous devriez trouver chdman dans le répertoire `build`.

Voici un lien pour télécharger des fichiers pour automatiser les conversions de format :

![](/tutorials/utilities/rom-conversion/chdman/archive.svg) [ChdScripts.zip](/tutorials/utilities/rom-conversion/chdman/chdscripts.zip)

Dans ce zip, vous trouverez 6 fichiers :

| Nom de fichier | Description |
| :--- | :--- |
| **<span>convertFromChdToCue.sh</span>** | Un fichier .sh qui vous permet de convertir vos roms au format `CHD` en `BIN`/`CUE`. |
| **<span>convertFromChdToGdi.sh</span>** | Un fichier .sh qui vous permet de convertir vos roms au format `CHD` en `GDI`. |
| **<span>convertFromChdToIso.sh</span>** | Un fichier .sh qui vous permet de convertir vos roms au format `CHD` en `ISO`. |
| **<span>convertFromCueToChd.sh</span>** | Un fichier .sh qui vous permet de convertir vos roms au format `BIN`/`CUE` en `CHD`. |
| **<span>convertFromGdiToChd.sh</span>** | Un fichier .sh qui vous permet de convertir vos roms au format `GDI` en `CUE`. |
| **<span>convertFromIsoToChd.sh</span>** | Un fichier .sh qui vous permet de convertir vos roms au format `ISO` en `CUE`. |
| **README_EN.txt** |  |
| **README_FR.txt** |  |

#### Contenu du fichier Readme

* **<span>convertFromChdToCue.sh</span>**

  Décompresse un fichier CHD (V5) en fichier BIN+CUE.
  Le format CUE est utilisé par les jeux sur CD. CHD est pris en charge par 3DO, Amiga CD32, Amiga CDTV, Dreamcast, Mega CD, Neo-Geo CD, PC Engine CD, PlayStation et Saturn.

* **<span>convertFromChdToGdi.sh</span>**
  Décompresse un fichier CHD (V5) en fichier GDI.
  Le format GDI est utilisé par les jeux sur disque pour Dreamcast.

* **<span>convertFromChdToIso.sh</span>**
  Décompresse un fichier CHD (V5) en fichier ISO.
  Le format ISO est utilisé par les jeux sur disque pour PlayStation 2.

* **<span>convertFromCueToChd.sh</span>**

  Compresse tout type de fichiers disques BIN avec un entête CUE vers le format CHD (v5). Recherche tous les sous-dossiers et crée des fichiers CHD (v5) dans le dossier dans lequel les fichiers sont placés avec CHDMAN.

* **<span>convertFromGdiToChd.sh</span>**

  Compresse tout type de fichiers disques BIN avec un entête GDI vers le format CHD (v5). Le format GDI est utilisé par les jeux sur disque pour Dreamcast.

* **<span>convertFromIsoToChd.sh</span>**

  Compresse tout type de fichiers disques ISO vers le format CHD (v5). Le format ISO est utilisé par les jeux sur disque pour PlayStation 2.

## Utilisation automatisé

## {.tabset}
### Windows
#### Convertir votre jeu depuis `BIN`/`CUE` ou `GDI` en `CHD`

* Placez "**chdman.exe**" et "**CUE or GDI to CHD.bat**" dans le dossier contenant votre jeu comme ci-dessous.
  Exemple pour le jeu "Grandia (France)" :

![](/tutorials/utilities/rom-conversion/chdman/chdman-win1.png){.full-width}

* Cliquez sur le fichier "**CUE or GDI to CHD.bat**" pour lancer la conversion.

![](/tutorials/utilities/rom-conversion/chdman/chdman-win2.png){.full-width}

* Une fois que la fenêtre CMD ci-dessus s'est fermé automatiquement, votre conversion est terminé.

![](/tutorials/utilities/rom-conversion/chdman/chdman-win3.png){.full-width}

* Vous pouvez supprimer "**chdman.exe**" et "**CUE or GDI to CHD.bat**", votre rom est prête.

![](/tutorials/utilities/rom-conversion/chdman/chdman-win4.png){.full-width}

>Vous pouvez également lancer le fichier .bat pour lancer la conversion de plusieurs jeux d'un coup.
{.is-info}

#### Convertir votre jeu depuis `CHD` en `BIN`/`CUE`

* Placez "**chdman.exe**" et "**Extract CHD to CUE.bat**" dans le dossier contenant votre jeu comme ci-dessous.
  Exemple pour le jeu "Grandia (France)" :



* Cliquez sur le fichier "**Extract CHD to CUE.bat**" pour lancer la conversion.



* Une fois que la fenêtre CMD ci-dessus s'est fermé automatiquement, votre conversion est terminé.



* Vous pouvez supprimer "**chdman.exe**" et "**Extract CHD to CUE.bat**", votre rom est prête.



>Vous pouvez également lancer le fichier .bat pour lancer la conversion de plusieurs jeux d'un coup.
{.is-info}

#### Convertir votre jeu depuis `CHD` en `GDI`

* Placez "**chdman.exe**" et "**Extract CHD to GDI.bat**" dans le dossier contenant votre jeu comme ci-dessous.
  Exemple pour le jeu "Grandia (France)" :



* Cliquez sur le fichier "**Extract CHD to GDI.bat**" pour lancer la conversion.



* Une fois que la fenêtre CMD ci-dessus s'est fermé automatiquement, votre conversion est terminé.



* Vous pouvez supprimer "**chdman.exe**" et "**Extract CHD to GDI.bat**", votre rom est prête.



>Vous pouvez également lancer le fichier .bat pour lancer la conversion de plusieurs jeux d'un coup.
{.is-info}

### macOS

#### Convertir votre jeu depuis `BIN`/`CUE` en `CHD`

* Placez "**<span>convertFromCueToChd.sh</span>**" dans le dossier contenant votre jeu comme ci-dessous.
  Exemple pour le jeu "Grandia (France)" :

![](/tutorials/utilities/rom-conversion/chdman/chdman-unix1.png){.full-width}

* Revenez dans le dossier parent, faites un clic droit dessus et choisissez `Services` > `Nouveau terminal au dossier`. Une fois ouvert, lancez le fichier avec `./convertFromCueToChd.sh` pour lancer la conversion.

![](/tutorials/utilities/rom-conversion/chdman/chdman-unix2.png){.full-width}

* Une fois que la fenêtre du Terminal ci-dessus a terminé, votre conversion est terminée.

![](/tutorials/utilities/rom-conversion/chdman/chdman-unix3.png){.full-width}

* Vous pouvez supprimer "**<span>convertFromCueToChd.sh</span>**", votre rom est prête.

#### Convertir votre jeu depuis `GDI` en `CHD`

* Placez "**<span>convertFromGdiToChd.sh</span>**" dans le dossier contenant votre jeu comme ci-dessous.
  Exemple pour le jeu "Resident Evil - Code - Veronica (France)" :

![](/tutorials/utilities/rom-conversion/chdman/chdman-unix6.png){.full-width}

* Revenez dans le dossier parent, faites un clic droit dessus et choisissez `Services` > `Nouveau terminal au dossier`. Une fois ouvert, lancez le fichier avec `./convertFromGdiToChd.sh` pour lancer la conversion.

![](/tutorials/utilities/rom-conversion/chdman/chdman-unix7.png){.full-width}

* Une fois que la fenêtre du Terminal ci-dessus a terminé, votre conversion est terminée.

![](/tutorials/utilities/rom-conversion/chdman/chdman-unix8.png){.full-width}

* Vous pouvez supprimer "**<span>convertFromGdiToChd.sh</span>**", votre rom est prête.

#### Convertir votre jeu depuis `ISO` en `CHD`

* Placez "**<span>convertFromIsoToChd.sh</span>**" dans le dossier contenant votre jeu comme ci-dessous.
  Exemple pour le jeu "Shadow of the Colossus (Europe, Australia) (En,Fr,De,Es,It)" :



* Revenez dans le dossier parent, faites un clic droit dessus et choisissez `Services` > `Nouveau terminal au dossier`. Une fois ouvert, lancez le fichier avec `./convertFromIsoToChd.sh` pour lancer la conversion.



* Une fois que la fenêtre du Terminal ci-dessus a terminé, votre conversion est terminée.



* Vous pouvez supprimer "**<span>convertFromIsoToChd.sh</span>**", votre rom est prête.

#### Convertir votre jeu depuis `CHD` en `BIN`/`CUE`

* Placez "**<span>convertFromChdToCue.sh</span>**" dans le dossier contenant votre jeu comme ci-dessous.
  Exemple pour le jeu "Grandia (France)" :

![](/tutorials/utilities/rom-conversion/chdman/chdman-unix4.png){.full-width}

* Revenez dans le dossier parent, faites un clic droit dessus et choisissez `Services` > `Nouveau terminal au dossier`. Une fois ouvert, lancez le fichier avec `./convertFromChdToCue.sh` pour lancer la conversion.

![](/tutorials/utilities/rom-conversion/chdman/chdman-unix5.png){.full-width}

* Une fois que la fenêtre du Terminal ci-dessus a terminé, votre conversion est terminée.

![](/tutorials/utilities/rom-conversion/chdman/chdman-unix3.png){.full-width}

* Vous pouvez supprimer "**<span>convertFromChdToCue.sh</span>**", votre rom est prête.

#### Convertir votre jeu depuis `CHD` en `GDI`

* Placez "**<span>convertFromChdToGdi.sh</span>**" dans le dossier contenant votre jeu comme ci-dessous.
  Exemple pour le jeu "Resident Evil - Code - Veronica (France)" :

![](/tutorials/utilities/rom-conversion/chdman/chdman-unix9.png){.full-width}

* Revenez dans le dossier parent, faites un clic droit dessus et choisissez `Services` > `Nouveau terminal au dossier`. Une fois ouvert, lancez le fichier avec `./convertFromChdToGdi.sh` pour lancer la conversion.

![](/tutorials/utilities/rom-conversion/chdman/chdman-unix10.png){.full-width}

* Une fois que la fenêtre du Terminal ci-dessus a terminé, votre conversion est terminée.

![](/tutorials/utilities/rom-conversion/chdman/chdman-unix11.png){.full-width}

* Vous pouvez supprimer "**<span>convertFromChdToGdi.sh</span>**", votre rom est prête.

#### Convertir votre jeu depuis `CHD` en `ISO`

* Placez "**<span>convertFromChdToIso.sh</span>**" dans le dossier contenant votre jeu comme ci-dessous.
  Exemple pour le jeu "Shadow of the Colossus (Europe, Australia) (En,Fr,De,Es,It)" :



* Dans votre Terminal, allez dans le dossier où se trouve "**<span>convertFromCueToChd.sh</span>**" (seul vous pouvez le savoir) et lancez le fichier avec `./convertFromChdToIso.sh` pour lancer la conversion.



* Une fois que la fenêtre du Terminal ci-dessus a terminé, votre conversion est terminée.



* Vous pouvez supprimer "**<span>convertFromChdToIso.sh</span>**", votre rom est prête.
### Linux

#### Convertir votre jeu depuis `BIN`/`CUE` en `CHD`

* Placez "**<span>convertFromCueToChd.sh</span>**" dans le dossier contenant votre jeu comme ci-dessous.
  Exemple pour le jeu "Grandia (France)" :



* Ouvrez le Terminal et lancez le fichier avec `./convertFromCueToChd.sh` pour lancer la conversion.



* Une fois que la fenêtre du Terminal ci-dessus a terminé, votre conversion est terminée.



* Vous pouvez supprimer "**<span>convertFromCueToChd.sh</span>**", votre rom est prête.

#### Convertir votre jeu depuis `GDI` en `CHD`

* Placez "**<span>convertFromGdiToChd.sh</span>**" dans le dossier contenant votre jeu comme ci-dessous.
  Exemple pour le jeu "Resident Evil - Code - Veronica (France)" :



* Ouvrez le Terminal et lancez le fichier avec `./convertFromGdiToChd.sh` pour lancer la conversion.



* Une fois que la fenêtre du Terminal ci-dessus a terminé, votre conversion est terminée.



* Vous pouvez supprimer "**<span>convertFromGdiToChd.sh</span>**", votre rom est prête.

#### Convertir votre jeu depuis `ISO` en `CHD`

* Placez "**<span>convertFromIsoToChd.sh</span>**" dans le dossier contenant votre jeu comme ci-dessous.
  Exemple pour le jeu "Shadow of the Colossus (Europe, Australia) (En,Fr,De,Es,It)" :



* Ouvrez le Terminal et lancez le fichier avec `./convertFromIsoToChd.sh` pour lancer la conversion.



* Une fois que la fenêtre du Terminal ci-dessus a terminé, votre conversion est terminée.



* Vous pouvez supprimer "**<span>convertFromIsoToChd.sh</span>**", votre rom est prête.

#### Convertir votre jeu depuis `CHD` en `BIN`/`CUE`

* Placez "**<span>convertFromChdToCue.sh</span>**" dans le dossier contenant votre jeu comme ci-dessous.
  Exemple pour le jeu "Grandia (France)" :



* Ouvrez le Terminal et lancez le fichier avec `./convertFromChdToCue.sh` pour lancer la conversion.



* Une fois que la fenêtre du Terminal ci-dessus a terminé, votre conversion est terminée.



* Vous pouvez supprimer "**<span>convertFromChdToCue.sh</span>**", votre rom est prête.

#### Convertir votre jeu depuis `CHD` en `GDI`

* Placez "**<span>convertFromChdToGdi.sh</span>**" dans le dossier contenant votre jeu comme ci-dessous.
  Exemple pour le jeu "Resident Evil - Code - Veronica (France)" :



* Ouvrez le Terminal et lancez le fichier avec `./convertFromChdToGdi.sh` pour lancer la conversion.



* Une fois que la fenêtre du Terminal ci-dessus a terminé, votre conversion est terminée.



* Vous pouvez supprimer "**<span>convertFromChdToGdi.sh</span>**", votre rom est prête.

#### Convertir votre jeu depuis `CHD` en `ISO`

* Placez "**<span>convertFromChdToIso.sh</span>**" dans le dossier contenant votre jeu comme ci-dessous.
  Exemple pour le jeu "Shadow of the Colossus (Europe, Australia) (En,Fr,De,Es,It)" :



* Ouvrez le Terminal et lancez le fichier avec `./convertFromChdToIso.sh` pour lancer la conversion.



* Une fois que la fenêtre du Terminal ci-dessus a terminé, votre conversion est terminée.



* Vous pouvez supprimer "**<span>convertFromChdToIso.sh</span>**", votre rom est prête.

## Créer le fichier `.M3U`

En conversion CHD pour les jeux multi-disques, il faut faire un fichier **M3U** pour déclarer tous les disques.

Le fichier au format `.M3U` est une liste des différents CD pour un même jeu qui permet de passer d'un cd à un autre de façon simple en utilisant la combinaison de changement du disque (`Hotkey` + `STICK G.` vers la _GAUCHE_ ou vers la _DROITE_).

Exemple pour le jeu "Grandia (France)" :

* Créez un fichier `M3U` via Notepad++ nommé « Grandia (France).m3u ».
* Dans le fichier, renseignez les fichiers `CHD` du jeu :

```text
Grandia (France) (Disc 1).chd
Grandia (France) (Disc 2).chd
```

* Sur Windows, vous devez activer la vue sur extension pour pouvoir créer ce fichier :
  * Allez dans `Affichage` en haut de la fenêtre de l'Explorateur Windows.
  * Cochez « Extensions de noms de fichiers » en haut à droite.

![](/tutorials/utilities/rom-conversion/chdman/chdman-m3u1.png){.full-width}

* Confirmez la modification de l'extension

![](/tutorials/utilities/rom-conversion/chdman/chdman-m3u2.png){.full-width}