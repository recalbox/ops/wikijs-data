---
title: Libretro LowResNX
description: 
published: true
date: 2024-06-22T10:08:14.814Z
tags: libretro, lowresnx, 8.0+
editor: markdown
dateCreated: 2021-07-09T13:03:41.126Z
---

**Libretro LowResNX** est un port de l'émulateur **LowRes NX**.

## ![](/emulators/license.svg) Licence

Ce core est sous licence [**Zlib**](https://github.com/timoinutilis/lowres-nx/blob/master/LICENSE).

## ![](/emulators/compatibility.png) Compatibilité

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Fonctionnalités

| Fonctionnalité | Supporté |
| :---: | :---: |

## ![](/emulators/bios.svg) BIOS

>**Aucun bios n'est requis.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Extensions supportées

Les roms doivent avoir l'extension :

* .nx
* .zip
* .7z

Les fichiers contenus dans les .zip/.7z doivent correspondre aux extensions citées précédemment.

### Emplacement

Placez les roms comme ceci : 

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 lowresnx
┃ ┃ ┃ ┃ ┣ 🗒  **jeu.nx**

## ![](/emulators/advanced-configuration.png) Configuration avancée de l'émulateur

>**Ce core n'a pas d'option.**
{.is-success}

## ![](/emulators/external-links.png) Liens externes

* **Code source utilisé** : [https://github.com/timoinutilis/lowres-nx/](https://github.com/timoinutilis/lowres-nx/)
* **Site officiel** : [https://lowresnx.inutilis.com/](https://lowresnx.inutilis.com/)
* **Documentation officielle** : [https://lowresnx.inutilis.com/help.php](https://lowresnx.inutilis.com/help.php)