---
title: Aktualisieren
description: 
published: true
date: 2024-06-02T20:06:01.917Z
tags: 
editor: markdown
dateCreated: 2021-07-01T09:28:09.574Z
---

## Aktualisierungsprozess

Das sind die **Update-Optionen** für Recalbox.  Wir hoffen, dass diese Zusammenfassung hilfreich ist und dir die gesuchten Antworten liefert.

## Je nach Situation...

### Ist es möglich, ein Upgrade von Recalbox x.x auf Recalbox 7 durchzuführen?

_**Nein**, es ist nicht möglich, auf Version 7 zu aktualisieren. Eine Neuinstallation ist erforderlich. Siehe unten für Anweisungen._

### Was ist, wenn ich ein anderes als das standardmäßig bereitgestellte Theme verwende?

* Stelle sicher, dass die Systemeinträge und Konfigurationsdateien deines Themes auf dem neuesten Stand sind.
* Kontaktiere den Autor des Themes, um zu sehen, ob eine neue, aktualisierte Version verfügbar ist.
* Verwende ein benutzerdefiniertes Theme in 720p auf dem Raspberry Pi. Mit Themes in 1080p hat der Raspberry Pi zu kämpfen.
* Wir bieten keinen Support für inoffizielle benutzerdefinierte Recalbox-Themen. Kontaktiere immer die Theme-Ersteller, um Support zu erhalten.
* Bitte verwende das Standard-Recalbox-Theme, um eventuelle Bugs zu überprüfen, bevor du dein Problem im Forum postest.

### Was ist, wenn ich OVERLAYS verwende?

Wenn du deine eigenen Overlays erstellt hast, beachte, dass RetroArch ein neues Seitenverhältnis hinzugefügt hat, das einige Overlays verschiebt:

* Teste 22 oder 23 in dieser Zeile in den Overlay-Konfigurationen:

```ini
aspect_ratio_index = 21
```

### Arcade

Wir haben die Cores der Arcade-Emulatoren MAME und FinalBurn Neo aktualisiert und weitere Emulatoren hinzugefügt.

Deswegen solltest Du deine ROMsets anpassen:

* Die entsprechenden dat-Dateien befinden sich jetzt in den folgenden Verzeichnissen:

  * /recalbox/share/bios/fba
  * /recalbox/share/bios/fba
  * /recalbox/share/bios/mame
  * /recalbox/share/bios/mame2000
  * /recalbox/share/bios/mame2003
  * /recalbox/share/bios/mame2003-plus
  * /recalbox/share/bios/mame2010
  * /recalbox/share/bios/mame2015

* Um eine "parent-only" dat, oder eine "neogeo parent only" dat, etc. zu erstellen, kannst Du dieses [datutil tutorial](./tutorials/utilities/dat-management/datutil) verwenden.

Hier sind die verwendeten Versionen:

* advanceMame : romset mame 0.106 (nur für erfahrene Anwender).
* FBA : romset 0.2.97.44
* FBNeo : romset 1.0.0.01
* Mame 2000 : romset mame 0.37b5
* Mame 2003 : romset mame 0.78
* Mame 2003-plus : romset mame 0.78-0.188.
* Mame 2010 : romset mame 0.139
* Mame 2015 : romset mame 0.160
* Mame : romset mame 0.230
* Beachte bitte auch die Arcade-Dokumentation

### Dein Raspberry Pi in einem Gehäuse.

* Stelle sicher, dass dein Pi eine gute Kühlung hat.
* Eine geeignete Stromversorgung ist unumgänglich.
* Wenn du irgendwelche Probleme mit deinem Pi hast, nimm ihn aus dem Gehäuse und teste Recalbox 7 bare metal (ohne Gehäuse).

### Unser Rat:

* Lege eine Sicherungskopie deines Share-Verzeichnisses auf deinem PC oder einer externen Festplatte an.
* Verwende einen Wechseldatenträger (USB-Stick oder externe Festplatte). So gehen eventuelle Neuinstallationen sehr viel schneller.
* Benutze einen externen Scraper
* Habe immer 2 MicroSD-Karten zur Hand (Eine für Stable Versionen und eine für Tests). 8GB sind ausreichend für das System. Deine ROMs sollten sich dann einem externen Datenträger befinden.
* Installiere Kühlkörper oder Lüfter:
  * Wenn du deinen Raspberry Pi übertaktest
  * Wenn dein Raspberry Pi sich in einem Gehäuse befindet

## Update starten

>Die **Update-Funktion** wird durch drücken von `START` > `UPDATES` aufgerufen.
{.is-info}

* Richte vorher dein WLAN ein, oder schließe ein Netzwerkkabel an die Recalbox an.
* Wähle dann `UPDATES`.
* Anschließend `UPDATE STARTEN`. 

Das System startet automatisch neu, nachdem das Update heruntergeladen wurde.