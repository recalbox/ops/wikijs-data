---
title: WASM-4
description: 
published: true
date: 2024-07-26T08:23:29.655Z
tags: fantasy, 9.1+, wasm4
editor: markdown
dateCreated: 2023-06-17T22:13:13.729Z
---

![](/emulators/fantasy/wasm4.svg){.align-center}

## Fiche technique

* **Développeur** : Bruno Garcia
* **Année de sortie** : 2021
* **Affichage** : 160x160 pixels, 4 couleurs personnalisables à 60 Hz
* **Mémoire** : 64 KB RAM linéaire
* **Taille limite des cartouches** : 64 KB
* **Entrées** : clavier, souris, tactile, jusqu'à 4 manettes
* **Audio** : 2 canaux pulse wave, 1 canal triangle wave, 1 canal noise
* **Stockage** : 1024 bytes

## Présentation

WebAssembly, abrégé wasm, est un standard du World Wide Web pour le développement d’applications. Il est conçu pour remplacer JavaScript avec des performances supérieures. Le standard consiste en un bytecode, sa représentation textuelle et un environnement d'exécution dans un bac à sable compatible avec JavaScript. Il peut être exécuté dans un navigateur Web et en dehors. WebAssembly est standardisé dans le cadre du World Wide Web Consortium.

Comme WebAssembly ne spécifie qu'un langage de bas niveau, le bytecode est généralement produit en compilant un langage de plus haut niveau. Parmi les premiers langages supportés figurent Rust avec le projet/module (crate) wasm-bindgen ainsi que le C et C++, compilés avec Emscripten (basé sur LLVM). De nombreux autres langages de programmation possèdent aujourd'hui un compilateur WebAssembly, parmi lesquels : C#, Go, Java, Lua, Python, Ruby, Fortran ou Pascal.

Les navigateurs Web compilent le bytecode wasm dans le langage machine de l'hôte sur lequel ils sont utilisés avant de l'exécuter.

## Émulateurs

[Libretro Wasm4](libretro-wasm4)