---
title: Configuración de la pantalla
description: 
published: true
date: 2024-07-25T19:12:42.301Z
tags: configuración, visualización, pantalla, presentación
editor: markdown
dateCreated: 2023-09-14T07:21:15.191Z
---

En esta sección se encuentran varios tutoriales relacionados con la configuración de la pantalla.

Aquí tenéis la lista de tutoriales disponibles:

[Configurar una pantalla externa](external-screen-configuration)
[Guía completa sobre la configuración vídeo](complete-video-configuration-guide)