---
title: Libretro Snes9X 2010
description: 
published: true
date: 2024-07-13T13:55:49.663Z
tags: libretro, snes, super nintendo, snes9x 2010
editor: markdown
dateCreated: 2023-09-08T09:13:44.351Z
---



## ![](/emulators/license.svg) Licencia

El core posee la siguiente licencia [**non commerciale**](https://github.com/libretro/snes9x/blob/master/LICENSE).

## ![](/emulators/compatibility.png) Compatibilidad

 | RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | 
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | 

## ![](/emulators/features.png) Funcionalidades

| Funcionalidad | ¿Soportada? |
| :---: | :---: |
| Reinicio | ✔ |
| Capturas de pantalla | ✔ |
| Grabaciones | ✔ |
| Grabaciones instantáneas | ✔ |
| Rebobinado | ✔ |
| Netplay | ✔ |
| Opciones del core | ✔ |
| RetroAchievements | ✔ |
| Cheats RetroArch | ✔ |
| Controles | ✔ |
| Remapping | ✔ |
| Multi-Ratón | ✔ |
| Softpatching | ✔ |
| Crop Overscan | ✔ |

## ![](/emulators/bios.svg) BIOS

>**No se necesitan bios.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Extensiones de roms soportadas

Las roms deben tener las extensiones siguientes:

* .fig
* .gd3
* .sfc
* .smc
* .swc
* .zip
* .7z

Este sistema soporta las roms comprimidas en formato .zip/.7z. No obstante, se trata sólo de un archivo.

Los ficheros contenidos en los .zip/.7z deben tener las extensiones citadas previamente.
Además, cada fichero .zip/.7z sólo debe contener un fichero rom al interior.

### Emplazamiento

Disponed vuestras roms como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 snes
┃ ┃ ┃ ┃ ┣ 🗒 **juego.zip**

>Se aconsejan las roms del proyecto **No-Intro**.
{.is-success}

>Para más información sobre las roms, consultad [este tutorial](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

>Para conservar las configuraciones personalizadas cuando se produce una actualización del sistema Recalbox, os aconsejamos utilizar las [Sobrecargas de configuración](./../../../advanced-usage/configuration-override).
{.is-info}

### Acceder a las opciones

Podéis configurar las opciones del core de dos formas diferentes.

* Via el Menú RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via el fichero `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Opciones del core

| Opción | Valor de la opción | Variable (para la sobrecarga) | Valores posibles |
| --- | --- | --- | --- |
 | Console Region (Restart) | `Auto` ✅ / `NTSC` / `PAL` | `snes9x_2010_region` | `auto` / `ntsc` / `pal` |
 | Preferred Aspect Ratio | `4:3` / `Uncorrected` / `Auto` ✅ / `NTSC` / `PAL` | `snes9x_2010_aspect` | `4:3` / `uncorrected` / `auto` / `ntsc` / `pal` |
 | Set Autofire Pulse | `Medium` ✅ / `Fast` / `Slow` | `snes9x_2010_turbodelay` | `medium` / `fast` / `slow` |
 | Blargg NTSC Filter | `Désactivé` ✅ / `Monochrome` / `RF` / `Composite` / `S-Video` / `RGB` | `snes9x_2010_blargg` | `disabled` / `monochrome` / `rf` / `composite` / `s-video` / `rgb` |
 | Frameskip | `Désactivé` ✅ / `Auto` / `Manual` | `snes9x_2010_frameskip` | `disabled` / `auto` / `manual` |
 | Frameskip Threshold (%) | `15` / `18` / `21` / `24` / `27` / `30` / `33` ✅ / `36` / `39` / `42` / `45` / `48` / `51` / `54` / `57` / `60` | `snes9x_2010_frameskip_threshold` | `15` / `18` / `21` / `24` / `27` / `30` / `33` / `36` / `39` / `42` / `45` / `48` / `51` / `54` / `57` / `60` |
| SuperFX Overclock | `5 MHz (Underclock)` / `8 MHz (Underclock)` / `9 MHz (Underclock)` / `10 MHz (Default)` ✅ / `11 MHz` / `12 MHz` / `13 MHz` / `14 MHz` / `15 MHz` / `20 MHz` / `30 MHz` / `40 MHz` | `snes9x_2010_overclock` | `5 MHz (Underclock)` / `8 MHz (Underclock)` / `9 MHz (Underclock)` / `10 MHz (Default)` / `11 MHz` / `12 MHz` / `13 MHz` / `14 MHz` / `15 MHz` / `20 MHz` / `30 MHz` / `40 MHz` |
| Reduce Slowdown (Hack, Unsafe) | `Disabled` ✅ / `Light` / `Compatible` / `Max` | `snes9x_2010_overclock_cycles` | `disabled` / `light` / `compatible` / `max` |
| Reduce Flickering (Hack, Unsafe) | `Disabled` ✅ / `Enabled` | `snes9x_2010_reduce_sprite_flicker` | `disabled` / `enabled` |
 | Block Invalid VRAM Access (Restart) | `Activé` ✅ / `Désactivé` | `snes9x_2010_block_invalid_vram_access` | `enabled` / `disabled` |

## ![](/emulators/external-links.png) Enlaces externos

* **Código fuente utilizado** : [https://github.com/libretro/snes9x2010/](https://github.com/libretro/snes9x2010/)
* **Documentación Libretro** : [https://docs.libretro.com/library/snes9x_2010/](https://docs.libretro.com/library/snes9x_2010/)