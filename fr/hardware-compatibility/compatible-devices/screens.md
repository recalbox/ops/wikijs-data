---
title: Écrans
description: 
published: true
date: 2022-01-22T23:00:54.215Z
tags: compatibilité, écrans
editor: markdown
dateCreated: 2021-05-21T15:48:08.996Z
---

### Écran TFT HDMI

| Périphérique | Taille | Résolution | État | Commentaires |
| :---: | :---: | :---: | :---: | :---: |
|  | 5" | 800x480 | ✅ | [banggood](https://www.banggood.com/5-Inch-800-x-480-HDMI-TFT-LCD-Touch-Screen-For-Raspberry-PI-2-Model-B-B-A-B-p-1023438.html) |
| PI-TOP CEED | 14" | 1366×768 | ✅ | Fonctionne mais pas de son. Besoin du paquetage "pt-speaker" [Source](https://www.neozone.org/tests/test-pi-top-ceed-lordi-ideal-pour-les-makers-en-herbe/) |

### Écran TFT SPI Bus

| Périphérique | Taille | Résolution | État | Commentaires |
| :---: | :---: | :---: | :---: | :---: |
|  |  |  |  |  |

### Écran TFT DPI Bus

| Périphérique | Taille | Résolution | État | Commentaires |
| :---: | :---: | :---: | :---: | :---: |
|  |  |  |  |  |

### Écran TFT avec panneau de contrôle

| Périphérique | Taille | Résolution | État | Commentaires |
| :---: | :---: | :---: | :---: | :---: |
| Assus N71Jq Laptop panel | 17.3" | 1600x900 | ✅ | Écran LCD recyclé Asus N71JQ connecté à un contrôleur d'affichage LCD (*panneau de contrôle d'affichage LCD HDMI VGA DVI Audio pour écran LCD LP173WD1 N173FGE B173RW01 15.6" 17.3" 1600x900 LED*) utilisant un connecteur LVDS à 30 pins |