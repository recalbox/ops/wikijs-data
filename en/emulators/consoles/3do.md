---
title: 3DO
description: Panasonic 3DO
published: true
date: 2021-08-09T16:18:55.079Z
tags: consoles, panasonic, 3do
editor: markdown
dateCreated: 2021-07-27T16:40:38.016Z
---

![](/emulators/consoles/3do.svg){.align-center}

## Technical data

* **Manufacturer**: Panasonic, Sanyo, GoldStar
* **Year of release**: 1993
* **OS**: Multitasking 32-bit OS 
* **CPU**: 32-bit RISC (ARM60) @ 12.5 MHz
* **RAM**: 2 MB
* **VRAM**: 1 MB
* **ROM**: 1 MB
* **GPU**: 2 Accelerated Video Co-Processors @ 25MHz
* **Sound chip**: 16-bit Stereo Sound with 44.1 KHz Sound Sampling Rate
* **Resolution**: 640x480 (16.7 millions colors)

## Presentation

The **3DO Interactive Multiplayer** was originally the name given to the first video game console developed by Panasonic that met the **3DO** standard established by The 3DO Company. This model and others that followed are commonly abbreviated as **3DO**.

In 1993, Trip Hawkins left Electronic Arts to found 3DO (Three Dimensional Objects). His goal was to create the technology for a 32-bit console and to license it to various manufacturers, thus imposing a standard.

The 3DO technology was conceived on paper by Dave Needle and Robert J. Mical, who had been part of the development teams for the Commodore Amiga and the Atari Lynx: due to the large number of former Commodore engineers, the company was nicknamed The Ex-Commodore West in the early days in the professional computer community.

The first to sign for the license were Matsushita and Goldstar, followed by Samsung, AT&T and Toshiba. The first 3DO to be marketed was the Panasonic FZ-1, in April 1993, followed by the Goldstar GDO and then the Sanyo model. An ISA card for PCs was produced by the Creative company: this card, called 3DO Blaster, made it possible to use 3DO media on a PC running Windows 3.1. Of the AT&T 3DO, only a prototype was shown at the Winter CES at the end of 1993.

Because of the videogame period, when the fashion was for "multimedia", numerous and very varied titles were created for this console. In addition to games, there were educational titles, encyclopaedic CD-ROMs, pornographic titles, etc.

In 1994, the sales of the various models did not reach the objectives and Trip Hawkins sold his shares in the company to Matsushita. The 3DO system was abandoned in 1995. The 64-bit 3DO M2 system, which was supposed to replace the 3DO standard, was not released, despite expectations. The 3DO Company sold all the rights to the M2 standard to Matsushita, who used it for arcade systems and interactive display stands.

After the failure of its hardware, 3DO recycles itself as a game publisher. Despite some successes such as Meridian 59 and Army Men, bad choices impacted production and during 2002, although 3DO's turnover rose to 53.7 million dollars, the company made a loss of 47.3 million and filed for bankruptcy.

## Emulators

[Libretro Opera](libretro-opera)