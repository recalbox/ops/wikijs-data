---
title: Root access via Terminal CLI
description: 
published: true
date: 2024-09-18T15:20:58.089Z
tags: 
editor: markdown
dateCreated: 2021-08-06T08:12:44.941Z
---

To get a terminal with root access on Recalbox, you have two options:

>Never expose your Recalbox to the internet. In particular port 22. Everyone knows the default root password and you could be hacked.
{.is-danger}

## SSH

Connect via ssh to Recalbox, with the following credentials:

* IP address of your Recalbox: `192.168.x.x`
* User ID: `root`
* Password: `recalboxroot`

## {.tabset}
### Windows

You can use these programs:

* [Putty](https://www.chiark.greenend.org.uk/~sgtatham/putty/latest.html)
* [MobaXterm](https://mobaxterm.mobatek.net/)
* PowerShell

### macOS

The Terminal is included in the system. Open your `Applications` folder and go to the `Utilities` directory to find it.

Command line to run: `ssh root@ip-address-of-your-Recalbox`

### Linux

The Terminal is provided as standard in the system. Its location depends on the Linux distribution you are using.

Command line to run: `ssh root@ip-address-of-your-Recalbox`

## Direct access

In the interface with a keyboard:

* Press `Alt + F2` to get a terminal. You will not see it yet.
* Press `Alt + F4` to exit the frontend. Once left, you will get the terminal.
* Use the same credentials as above.

## Change the default password

You can change the default password with:

```bash
mount -o remount,rw /
passwd
```

>The password change will not survive updates.
{.is-warning}