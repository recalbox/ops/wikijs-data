---
title: Définir la position de la Dolphin Bar
description: 
published: true
date: 2021-10-16T11:45:35.284Z
tags: wii, dolphin, bar
editor: markdown
dateCreated: 2021-06-28T07:45:55.378Z
---

* Ouvrez le fichier `/recalbox/share/system/recalbox.conf`
* Dans la section "D3 - Dolphin Controllers" cherche la ligne :

```ini
wii.sensorbar.position
```
* Si vous placez votre Dolphin Bar au-dessus de votre écran,, remplacez par ceci :

```ini
wii.sensorbar.position=1
```
* Si vous placez votre Dolphin Bar en dessous de votre écran, remplacez par ceci :

```ini
wii.sensorbar.position=0
```