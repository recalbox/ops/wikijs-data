---
title: Where to find 100% legal roms
description: 
published: true
date: 2021-08-07T15:47:28.891Z
tags: roms, find, legal
editor: markdown
dateCreated: 2021-08-07T15:47:28.891Z
---

## Introduction
This page aims to list the different "100% legal" ways to get roms files for the different platforms supported by Recalbox.

## Atari2600

* On Steam, Atari 2600 roms can be extracted from the **Atari Vault** game. There are 100 Atari 2600 games in this pack which costs **9,99€**. Here is the [link to the game's Steam page](https://store.steampowered.com/app/400020/Atari_Vault/).

## Daphne

* Some games can be directly and legally downloaded by the **DaphneLoader** tool of the Daphne Windows distribution.
* For others (Dragon's Lair, Dragon's Lair II, Space Ace, ...), you must prove that you have a valid license of the game (DVD version for example).

## DOS

* On GOG.com (Good Old Games, a platform for downloading PC games without DRM), it is possible to buy MS-DOS games like Disney's Aladdin, Disney's Lion King, Disney's Jungle Book, ...  These games run on dosbox and are therefore compatible with Recalbox.
* On the " Legacy " site of 3D Realms (formerly Apogee), it is possible to download some of their old titles. Most of them are offered as freeware and some as shareware. There are some quite interesting titles like **Alien Carnage (Halloween Harry)**. The list of downloadable games can be found [here](http://legacy.3drealms.com/downloads.html). These games run on DOSbox and are therefore compatible with Recalbox. 
* Blizzard offers the game **The Lost Vikings (USA)** for free download on [their site](https://us.battle.net/account/download/?show=classic).

## Genesis

* On Steam, it is possible to buy the game **SEGA Mega Drive & Genesis Classics**. During the installation, a `uncompressed roms` directory is created. You just have to change the extension of the roms to .md or .bin so that they are recognized by Recalbox. The game costs **29,99€** and contains 58 Megadrive titles + Sonic CD. The list of included games is available on the [Steam page of the game](https://store.steampowered.com/sub/102625/?l=french), on the 67 Roms present, 61 correspond to roms referenced by No-intro.
* There are "homebrew" games created by the Shiru and available for free download on [his site](https://shiru.untergrund.net/software.shtml#genesis).

## Master System

* On Steam, it is possible to buy the remake **"Wonder Boy: The Dragon's trap".** Once installed, the game directory contains a sub-folder `bin_pc`, then another sub-folder `rom`, this last one contains a `wb3.sms` file which is a rom corresponding to the Master System game **Wonder boy III: The Dragon's Trap** (it doesn't correspond to a game referenced by No-intro though)

## NeoGeo

* On GOG.com (Good Old Games, a DRM-free PC game download platform), it is possible to buy **24 Neo Geo games at 5,19€ per title**. Here is the [link to all SNK titles available on GOG.com](https://www.gog.com/games?sort=popularity&search=snk&page=1). 
* On Humble Bundle, it is possible to get a compilation of 23 games for **34,99€**. The games are also DRM free. The list of available games is on the [bundle page](https://www.humblebundle.com/store/neogeo-classic-complete-collection). It is also possible to buy the titles **individually for 5,99€**. The pack contains the same games as on GOG.com except for The King of Fighters '98 which must be purchased separately with the "Unlimited Match" pack.

## NES

* There are homebrew games created by the Shiru and available for free download on [his site](https://shiru.untergrund.net/software.shtml#snes).
* There is also the possibility to buy Capcom bundles on Steam (directly or through Humble Bundle).

From these bundles, it is possible to extract the .nes roms thanks to the utility [romextract](https://gitlab.com/vaiski/romextract/tree/master). Among the bundles proposed by Capcom from which it is possible to extract the NES roms, there are [Disney Afternoon Collection](https://www.humblebundle.com/store/the-disney-afternoon-collection) and [Megaman Legacy Collection (the first pack only)](https://www.humblebundle.com/store/mega-man-legacy-collection).

## SNES

* There are "homebrew" games created by the Shiru and available for free download on [its site](https://shiru.untergrund.net/software.shtml#nes).
* Blizzard offers free download of the Rock 'N Roll Racing (USA) game rom on [its site](https://us.battle.net/account/download/?show=classic).
* On the commercial side, on Steam, the publisher [Piko Interactive](https://store.steampowered.com/publisher/Piko) offers some titles from the SNES catalog. These games are presented in the form of an emulator with the game's rom. These roms are also available in [bundle](https://store.steampowered.com/bundle/5256/RETRO_Action_SNES_Volume_1/).
* Just like for the NES, Capcom published the [Megaman X Legacy Collection](https://www.humblebundle.com/store/mega-man-x-legacy-collection) bundle which can be bought on Steam (directly or through Humble Bundle) and contain the SNES roms. These roms can be extracted with the utility [romextract](https://gitlab.com/vaiski/romextract/tree/master). Only the roms from the first pack can be extracted.