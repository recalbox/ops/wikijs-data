---
title: Balena Etcher
description: 
published: true
date: 2023-09-17T07:13:14.417Z
tags: balena, etcher
editor: markdown
dateCreated: 2023-09-17T07:13:14.417Z
---

> ¡Utilizad Balena Etcher como último recurso, ya que flashea Recalbox incorrectamente y puede dejar vuestra instalación inoperativa!
>
> Se recomienda usar [Raspberry Pi Imager](./../../../tutorials/utilities/write-an-image/raspberry-pi-imager) para instalar Recalbox en cualquier sistema (no sólo para los Raspberry Pis)
{.is-danger}

[BalenaEtcher](https://www.balena.io/etcher/) es un programa fácil de usar. Puedes flashear el fichero imagen en cualquier medio de almacenamiento externo (por ejemplo, una tarjeta microSD para una Raspberry Pi). Sólo se tarda unos segundos.

* Elegid la imagen ISO a flashear:

  * Pulsad en `Select image` y buscad el fichero que queréis flashear. El nombre de la imagen aparecerá bajo el primer icono, y un pequeño cambio os permitirá modificar esta elección.

* Enchufad un medio de almacenamiento:

	* Se enciende el segundo icono de almacenamiento externo.
  * Seleccionad un soporte de almacenamiento.

Etcher valida automáticamente el soporte. Puedes cambiar el destino pulsando el pequeño botón `Change`. El programa indica la capacidad del dispositivo y avisa si es demasiado pequeño para aceptar la imagen.

>Para la instalación en un disco duro:
>
>* Id a `Configuración` (Rueda de desplazamiento arriba a la derecha).
>* Desactivad la función "modo inseguro".
>* Volved al menú Inicio.
>* Seleccionad un disco duro.
{.is-warning}

![Etcher](/tutorials/utilities/image-write/balena-etcher/balena1.jpg)

* Pulsad el botón `Start o Flash` para iniciar el proceso. Etcher se encarga de todo, formatear, preparar particiones, copiar archivos, etc.

![](/tutorials/utilities/image-write/balena-etcher/balena2.png)

* El programa indica el trabajo en curso, el tiempo necesario para completar la operación y la velocidad de transferencia de datos.

![](/tutorials/utilities/image-write/balena-etcher/balena3.png)

* Valida la imagen.

![](/tutorials/utilities/image-write/balena-etcher/balena4.png)

* Y, por último, informa de si la operación se ha realizado correctamente o no. Os da una clave para comprobar la integridad del fichero y os sugiere repetir la operación, ya sea con la misma imagen en un solo clic, o con una imagen diferente.

![](/tutorials/utilities/image-write/balena-etcher/balena5.png)

* Podéis extraer el soporte de almacenamiento externo.