---
title: Hyperion sur Recalbox
description: 
published: true
date: 2023-09-18T21:03:05.709Z
tags: hyperion
editor: markdown
dateCreated: 2021-05-21T08:09:29.397Z
---

>Ce tutoriel ne fonctionne plus à partir de Recalbox 8.0 !
{.is-warning}

## Faites briller votre Recalbox avec Hyperion !

[Hyperion](https://github.com/hyperion-project/hyperion) est un projet libre et open source pour transformer votre Raspberry Pi en système **Ambilight** !

Avec Hyperion et Recalbox, vous allez transformer votre système de rétrogaming et système multimédia en une magnifique experience immersive !

## Description

Tout d’abord, nous ne parlons pas d’utiliser Hyperion avec une partie de votre Recalbox comme Kodi.  
Vous serez capable d’utiliser les fonctions d’Hyperion dans CHACUN DES JEUX RETRO auquel vous jouez sur Recalbox !

![Hyperion avec Recalbox](/tutorials/others/hyperion/pukerainbow.gif)

L’image que vous voyez sur votre TV (LCD ou CRT) sera dynamiquement étendu sur le mur avec des LEDs RGB.

A quoi ça ressemble ? Regardez avec Sonic 3 sur Sega Megadrive/Genesis :

![](/tutorials/others/hyperion/sonic1.jpg)

![](/tutorials/others/hyperion/sonic2.jpg)

![](/tutorials/others/hyperion/sonic3.jpg)

Et en vidéo :

[https://youtu.be/z1QblkdO6bs](https://youtu.be/z1QblkdO6bs)

## Matériel

Hyperion supporte de nombreuses bandes de LED (voir [cette page](https://docs.hyperion-project.org/en/user/leddevices/)) avec différents protocoles :

* **SPI** : LPD6803, LPD8806, APA102, WS2812b et WS2801
* **USB** : AdaLight et SEDU
* **PWM** : WS2812b

Les tests ont été faits sur WS2812b avec PWM et SPI. Je n’ai jamais réussi à faire fonctionner PWM correctement, nous allons donc décrire comment faire votre installation Recalbox / Hyperion avec le mode WS2812b en SPI.  
Cette section est absente de la documentation Hyperion, nous allons donc expliquer comment l’utiliser ici.

La prise en charge de WS2812b avec un fil sur SPI a été ajoutée par [penfold42](https://github.com/penfold42) avec [ce commit](https://github.com/hyperion-project/hyperion/commit/a960894d140405e29edb413685e700d2f1714212). Merci.

Le tutoriel suivant utilise un fer à souder. Si vous en possédez déjà un, le reste du matériel est plutôt bon marché. Mais si vous souhaitez faire quelque chose sans soudure, voir le tutoriel [ici](https://hyperion-project.org/forum/index.php?thread/77).

### Les éléments nécessaires

Vous aurez besoin de:

* Votre Recalbox.
* Une bande de LED WS2812b.
* Une résistance de 330 Ohms à mettre sur le pin data.
* Quelque chose pour couper la bande.
* Un fer à souder.
* Des fils ou connecteurs pour connecter la bande de LEDs.
* Un level shifter pour passer le voltage de data à 5V , ou un régulateur de tension pour réduire le VCC des LEDs.
* Des jumpers DuPont pour connecter facilement les fils sur le Raspberry Pi.
* Une alimentation de 4A \(min\) 5v.

N’hésitez pas à visiter le site [kubii.fr](https://kubii.fr/) pour trouver tout ce dont vous avez besoin.

Bien sûr, si vous voulez économiser vous pouvez :

* Souder directement (pas de connecteurs ni DuPont)
* Utilisez une alimentation 3A.

J’ai testé ma bande de 220 LEDs Hyperion avec une Aukru 5V 3A et ça fonctionnait presque bien, mais quand toutes les LEDs sont blanches, la fin de la bande est un peu jaune…

## Câblage

### LEDs

Mesurez les bords de votre TV et coupez 1 bande par bord selon cette mesure.

Soudez les bandes ensemble OU soudez des connecteurs sur les bandes. Suivez les flèches sur la bande pour savoir quel connecteur utiliser (IN ou OUT).

Voici un aperçu du résultat :

![Connecteurs](/tutorials/others/hyperion/connectors.jpg)

![Soudure](/tutorials/others/hyperion/solder.jpg)

### Le système

Pour autoriser le Raspberry Pi à envoyer des données aux LEDs, vous avez le choix :

* Baisser le voltage LED à 4.7V avec un convertisseur
* Redresser le signal du GPIO à 5V

#### Régulateur de tension

![wiring-regulator.png](/tutorials/others/hyperion/wiring-regulator.png)

Si vous êtes sur Raspberry Pi 2 ou 3, vous avez plus de GPIO mais le placement reste le même. Le Raspberry Pi est connecté sur sa propre alimentation et sur la masse de l’alimentation des LEDs. Le Raspberry Pi envoie les données à travers le SPI MOSI GPIO vers le câble données des LEDs. Le régulateur est connecté entre les LEDs et leur alimentation.

#### Level shifter (non testé)

Si vous choisissez de réguler le voltage des GPIO, utilisez le schéma au dessus, enlevez le régulateur et connectez le level shifter entre le GPIO MOSI et le câble données des LEDs.

## Configuration

**Hypercon** est une interface graphique qui vous permet de configurer votre installation Hyperion et créer le fichier de configuration a placer sur la Recalbox.

![](/tutorials/others/hyperion/hypercon.jpg)

### Téléchargement et démarrage

Téléchargez et démarrez Hypercon en suivant le [tutoriel officiel](https://docs.hyperion-project.org/en/user/Installation.html).

![](https://gblobscdn.gitbook.com/assets%2F-LdKTX4ollh_G72-pO8z%2F-MJOZB8uJ0fcgdF-EvJd%2F-MJO_1HkQUXpWc1uWUwn%2Fimage.png?alt=media&token=7c07bd76-2497-45fd-b8f1-399a1fd4c403)

Vous trouverez de nombreuses ressources sur la configuration d’Hypercon sur la [documentation d'Hyperion](https://docs.hyperion-project.org/en/user/Configuration.html).

### Configuration pour Recalbox

Voici comment configurer Hypercon pour votre Recalbox et le WS2812b en mode SPI :

* **Hardware**
  * Type -> WS281X-SPI
  * RGB Byte Order -> GRB
  * Construction and Image Process -> configure depending of your installation
  * Blackborder Detection -> Enabled
    * Threshold -> 5
    * mode -> osd
* **Process**
  * _Smoothing_
    * Enabled -> True
    * Time -> 200ms
    * Update Freq -> 20
    * Update Delay -> 0
* **Grabber**
  * _Internal Frame Grabber_
    * Enabled -> True
    * Width -> 64
    * Height -> 64
    * Interval -> 200
    * Priority Channel -> 890
  * _GrabberV4L2_
    * Enabled -> False
* External
  * Booteffect / Static Color -> configure the boot effect you like here!

Cliquez ensuite sur le bouton Create Hyperion Configuration. Sauvegardez le fichier json en `hyperion.config.json`.

Copiez ce fichier sur votre recalbox dans à l'emplacement `/recalbox/share/system/config/hyperion/
`

#### Activer le service Hyperion dans recalbox.conf

* Activez simplement Hyperion dans [recalbox.conf](./../../basic-usage/getting-started/recalboxconf-file) en changeant la ligne `hyperion.enabled=0` en `hyperion.enabled=1`.
* Modifiez le fichier `recalbox-user-config.txt` sur la [partition de démarrage](./../../tutorials/system/access/remount-partition-with-write-access) et ajoutez la ligne `dtparam=spi=on` :

```shell
echo 'dtparam=spi=on' >> /boot/recalbox-user-config.txt
```

* Redémarrez votre Recalbox.

## Profitez de l’arc en ciel

Vous avez un système Ambilight totalement fonctionnel ! Testez des jeux retro, des films et animes avec Kodi et laissez la beauté des couleurs améliorer votre expérience multimédia !

![](/tutorials/others/hyperion/nyan.jpg)