---
title: HDMI
description: 
published: true
date: 2023-09-26T07:02:02.527Z
tags: hdmi
editor: markdown
dateCreated: 2023-09-14T07:23:01.330Z
---

En esta sección se presentan varios tutoriales para gestionar diferentes aspectos de la interfaz HDMI.

Aquí tenéis la lista de tutoriales disponibles:

[Ignorar el HDMI CEC en la Odroid XU4](ignore-hdmi-cec-odroidxu4)