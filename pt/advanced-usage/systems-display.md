---
title: Visualização dos sistemas
description: Funcionamento e modificações na visualização dos sistemas.
published: true
date: 2021-11-28T13:45:19.750Z
tags: 6.1+
editor: markdown
dateCreated: 2021-08-17T23:39:22.731Z
---

## Funcionamento da visualização

A exibição dos sistemas no menu do Recalbox é gerenciada pelo arquivo `systemlist.xml`. Este arquivo está localizado na pasta `/recalbox/share_init/system/.emulationstation/systemlist.xml`.

Ele contém os nomes dos vários sistemas que são suportados por sua versão do Recalbox. Ele é construído da seguinte forma:

```xml
<?xml version="1.0"?>
<systemList>
  <defaults command="python /usr/bin/emulatorlauncher.pyc %CONTROLLERSCONFIG% -system %SYSTEM% -rom %ROM% -emulator %EMULATOR% -core %CORE% -ratio %RATIO% %NETPLAY%"/>
  <system uuid="b58b9072-0e44-4330-869d-96b72f53de1f" name="3do" fullname="Panasonic 3DO" platforms="3do">
    <descriptor path="%ROOT%/3do" theme="3do" extensions=".iso .chd .cue"/>
    <scraper screenscraper="29"/>
    <properties type="console" pad="mandatory" keyboard="no" mouse="no" lightgun="optional" releasedate="1993-10" retroachievements="1"/>
    <emulatorList>
      <emulator name="libretro">
        <core name="opera" priority="1" extensions=".chd .cue .iso" netplay="0" compatibility="high" speed="high"/>
      </emulator>
    </emulatorList>
  </system>
  <system>
      [. . .]
  </system>
  [. . .]
</systemList>
```

Os sistemas são exibidos na ordem em que estão listados neste arquivo. Ele também contém a configuração desses sistemas.

## Alterar a ordem de exibição

> Você **NÃO DEVE ALTERAR** arquivo original `systemlist.xml` (que está na pasta `share_init`). Se houver algum problema com as mudanças feitas posteriormente, este arquivo será a fonte para que o Recalbox funcione corretamente.
{.is-warning}

A alteração da ordem de exibição só deve ser feita a partir do arquivo `systemlist.xml` na pasta `/recalbox/share/system/.emulationstation/systemlist.xml`.

Originalmente, este arquivo não existe. Você deve copiar o arquivo original ou criar um novo arquivo. Uma vez que o novo arquivo tenha sido criado, é possível colocar os sistemas em qualquer ordem que desejar. A configuração dos sistemas ainda será extraída do arquivo original `systemlist.xml`, mas a ordem dos sistemas será a definida no novo arquivo.

Se, no novo arquivo, um sistema estiver faltando ou incorretamente preenchido, a prioridade é dada ao arquivo original. Para o novo arquivo, há apenas uma chave de entrada que é necessária: "**UUID**", todas as outras são opcionais. O arquivo deve ser construído pelo menos da seguinte forma:

```xml
<?xml version="1.0"?>
<systemList>
  <system uuid="62f2cbed-5bcb-46d8-bca9-daaa36613c7a"/> <!-- nes -->
  <system uuid="0a7fd1d3-9673-44ab-bfef-5bb3c8a0d79a"/> <!-- fds -->
  <system uuid="3df92492-d69a-48f8-8e14-9a62bd9805a6"/> <!-- snes -->
  <system uuid="99e312bc-7602-4038-a871-e53c17fd1d76"/> <!-- satellaview -->
  [. . .]
</systemList>
```

## Adicionar um sistema "Personalizado"

> Você **NÃO DEVE ALTERAR** arquivo original `systemlist.xml` (que está na pasta `share_init`). Se houver algum problema com as mudanças feitas posteriormente, este arquivo será a fonte para que o Recalbox funcione corretamente.
{.is-warning}

Esta modificação não adicionará um novo emulador ao Recalbox, acrescentará apenas uma nova entrada de sistema ao menu de seleção.

É possível combinar a alteração da ordem dos sistemas com a adição de um ou mais sistemas personalizados.

Assim como na mudança da ordem do sistema, a adição de um sistema personalizado só deve ser feita a partir do arquivo `systemlist.xml` na seguinte pasta `/recalbox/share/system/.emulationstation/systemlist.xml`.

Originalmente, este arquivo não existe. Você deve copiar o arquivo original ou criar um novo arquivo. Uma vez criado o novo arquivo, agora é possível acrescentar um novo sistema a ele.

Se, no novo arquivo, um sistema estiver faltando ou incorretamente preenchido, a prioridade é dada ao arquivo original. 

Para o novo arquivo, todas as chaves de entrada são obrigatórias. Assim, para criar um novo sistema, a maneira mais fácil é partir de um sistema existente (e correspondente as roms que você deseja incluir) e modificar apenas o que é estritamente necessário:

- _**fullname**_ : Permite dar o nome do novo sistema.
- _**path**_ : Permite indicar a pasta que contém as roms do novo sistema.
- _**theme**_ : Permite indicar o tema a ser utilizado. É necessário, com antecedência, criar este novo tema (logotipo, fundo, ...)

**Todas as outras entradas não devem ser modificadas.**

Aqui está um exemplo de adição de um sistema baseado no SNES para incluir apenas as roms traduzidas:

```xml
<?xml version="1.0"?>
<systemList>
  <system uuid="21b8873a-a93e-409c-ad0c-8bb6d682bef8" name="snes" fullname="Super Nintendo Traduzidos" platform="snes">
    <descriptor path="%ROOT%/snestrad" theme="snestrad" extensions=".smc .sfc .mgd .zip .7z"/>
    <scraper screenscraper=""/>
    <properties type="console" pad="mandatory" keyboard="no" mouse="optional" lightgun="optional" releasedate="1990-11" retroachievements="1"/>
    <emulatorList>
      <emulator name="libretro">
        <core>snes9x2005</core>
        <core>snes9x2010</core>
        <core>snes9x2002</core>
      </emulator>
    </emulatorList>
  </system>
</systemList>
```