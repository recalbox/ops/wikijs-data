---
title: Dossiers et commandes SSH
description: Quelques commandes bien utiles...
published: true
date: 2021-09-04T21:45:36.191Z
tags: ssh, commandes, dossiers
editor: markdown
dateCreated: 2021-05-21T08:39:55.442Z
---

Vous trouverez ci-dessous quelques commandes vous permettant d'effectuer quelques actions spécifiques si cela vous est nécessaire.

## Commandes SSH

Vous connecter à votre Recalbox :

```shell
ssh root@[adresse IP address de votre Recalbox]
```

Monter la partition du système en mode lecture/écriture :

```shell
mount -o remount,rw / 
```

Monter la partition de démarrage en lecture/écriture :

```shell
mount -o remount,rw /boot
```

Arrêter EmulationStation :

```shell
es stop
```

Démarrer EmulationStation :

```shell
es start
```

Redémarrer EmulationStation :

```shell
es restart
```

Changer le mot de passe utilisateur :

```shell
passwd
```

Redémarrer votre Recalbox :

```shell
reboot
```

Éteindre votre Recalbo :

```shell
poweroff
```

Faire une capture d'écran :

```shell
raspi2png
```

Il prendra une capture d'écran et créera un fichier sous `/recalbox/share/screenshots/` sous le nom `screenshot-date-du-screenTheure-du-screen.png`.

Vous pouvez également modifier en un nom personnalisé votre capture avec `-p nomdefichier.png`

Informations sur le réseau :

```shell
ifconfig
```

Vérifiez la température du CPU :

```shell
cat /sys/class/thermal/thermal_zone0/temp  
```

Ensuite, divisez le résultat par 1000 pour obtenir la température en Celsius (par exemple : 54034 devient 54°C).

## Fichiers

Configuration spécifique au Raspberry Pi, comme les paramètres d'overscan, l'overclocking, le mode vidéo par défaut, etc... :

```shell
nano /boot/recalbox-user-config.txt  
```

Fichier de configuration Recalbox :

```shell
nano /recalbox/share/system/recalbox.conf
```