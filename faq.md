---
title: 6. 💡 FAQ
description: 
published: true
date: 2024-11-28T13:39:00.876Z
tags: faq
editor: markdown
dateCreated: 2021-05-28T22:31:34.313Z
---

## General
In this page, you will find answers to frequently asked questions regarding.
In case of question about a specific release go to dedicated pages :
- [FAQ Recalbox 8](/en/faq/recalbox-8)
- [FAQ Recalbox 9](/en/faq/recalbox-9)

In addition of previous pages, you should get a look the [known issues page](/en/faq/known-issues). 

### Which device can I use to install Recalbox ?

You can install Recalbox on the following devices:

- **Raspberry Pi 5**
- Raspberry Pi 4
- Raspberry Pi 400
- Raspberry Pi 3 (B/B+)
- Raspberry Pi 2
- Raspberry Pi 1 (B/B+)
- Raspberry Pi Zero
- Raspberry Pi Zero 2
- Odroid XU4
- Odroid Go Advance (Portable Console)
- Odroid Go Super (Laptop)
- PC x86 (32bits)
- PC x86_64 (64bits)
- Anbernic RG353(M/V/P)
- Anbernic RG351

### How can I contribute?

- You can participate on our [Patreon](https://patreon.com/recalbox)
- You can order your hardware on [Kubii](https://kubii.fr/221-recalbox)
- You can report bugs or improvement ideas on our [Gitlab](https://gitlab.com/recalbox/recalbox/issues)
- You can participate on the [Recalbox forums](https://forum.recalbox.com) and on our [Discord](https://discord.gg/HKg3mZG)
- You can [contribute to this Wiki](./contribute)

### How following developments in progress? 

On the [recalbox gitlab](https://gitlab.com/recalbox). You can access:
- [issues](https://gitlab.com/recalbox/recalbox/issues) which are the current tasks.  
- the [milestones](https://gitlab.com/recalbox/recalbox/-/milestones) where you can see the tasks that are still to be developed, and those that are finished.

### How many games already installed on Recalbox?

We're trying to add as many free games as possible to the distribution, so you can enjoy the Recalbox experience from the first launch!
Go to [this page](https://forum.recalbox.com/topic/52/homebrews-sur-la-recalbox) to submit your homebrews!

### Can I use Recalbox on a kiosk / bartop?

Yes of course, Recalbox is designed to be used  on kiosks and bartops:

- You can connect the buttons and joysticks directly to the GPIOs of the Raspberry.
- You can disable the menus to avoid any misuse.
- It is also possible to use the HDMI / DVI / VGA outputs (with adapter) or RCA.
You can find more information on [the dedicated page](./tutorials/system/installation/bartop-arcade-configuration).

### Recalbox supports many systems but I only see few of them, what should I do?

Only systems with available games are displayed. Try to add your roms via the network and restart the Recalbox to see the new systems appear.

### Roms are installed but the video game system doesn't show up! 

Recalbox only displays systems supported by your hardware. 

For example, the Sega Saturn emulator is not compliant with Raspberry Pi3 and olders, it only works with Raspberry Pi4 and stronger devices. PS2 and Gamecube emulators are only on supported on PC…

Read the [emulators compatibility table](/en/hardware-compatibility/emulators-compatibility) and check that your device can run the expected emulator. 

### Do I need to overclock my Raspberry Pi?

On Raspberry Pi 1 (including zero), it is highly recommended to overclock the Raspberry in EXTREM mode.

## EmulationStation

### How do I change the look of the EmulationStation theme?

Press `Start` on the controller → `INTERFACE SETTINGS` → `THEME`

### How to turn off the background music?

Press `Start` on the controller → `SOUND OPTIONS` → Set to Off

### Where is the EmulationStation configuration file?

```ini
/recalbox/share/system/recalbox.conf
```

### Where are EmulationStation's themes located?

```ini
/recalbox/share/themes
```

## Emulation

### Why are there multiple emulators for each system?

Emulation is not a flawless operation, there is a balance between performance, functionality and fidelity.
Since emulation is not perfect, it is better to have several options to handle it, sometimes a faster emulator will be better but may have unexpected problems, while a slower one may have higher accuracy but does not support some specific features.

### How do I change the default emulator for a given system?

By pressing `START` on the gamepad, then going to the advanced settings, and finally to the advanced emulator configuration. You can select the desired system, you can change the emulator and kernel settings (note that the basic setting will not change if the emulator setting says Default).

## System



### Why on some systems like the GBA, my games show a black screen and then return under EmulationStation?

Some systems require bios to work. Maybe you passed a warning at game launch asking for the bios.

## General Troubleshooting

### My Recalbox just crashed, should I unplug it and plug it back in?

No, in most cases, the system did not completely crash, only EmulationStation did.

There are several ways to handle this situation, the easiest one is to use the debugging features of Webmanager to restart EmulationStation or to shut down Recalbox properly.

Not doing so can corrupt your data, especially if you are using a Raspberry Pi with an SD card.

### My controller doesn't work

You must check that your device is [officially supported](/en/tutorials/controllers/controllers). 

We strongly recommend to DO NOT use unofficial devices, despite of our efforts Recalbox can not support all devices and you will have many issues using cheap devices. 

Otherwise if you use a proper device, you should take care to use the right firmware update on you controller. 

For example 8Bitdo controllers are usually supported but latest 8Bitdo firmware update brake Recalbox support. Updating to the latest firmware is not always a good idea! 

### Can I use a Recalbox hard drive (SSD,NVMe, etc.) on other device? 

No, Recalbox is not designed to work with other devices than the device used to make the system initialization. If you want to reuse a hard drive, you must re-install the system following usual instructions.

### I can't launch any game and I get an error message about bios. What can I do?

Did you have your Recalbox working and now it's not?

![Error message](/faq/error-en.png){.align-center}

In order to get help with this error, have you :

- Checked that your bios for the system concerned are good (if that system has bios)?
- Reconfigured your controllers?
- Check that your game is correct and well recognized [by preservation groups](./tutorials/games/generalities/isos-and-roms/differents-groups)?

Once all these checks have been carried out and the same problem persists, please follow these steps.

#### Enable debug mode

First, you need to enable debug mode. This will allow us to see more clearly when your problem occurs.

To do this, follow these steps:

- Switch off your Recalbox.
- Put your storage media on your computer. This depends on what you use to store your games.
- Once you've put the storage media on your computer, you should have a drive named `SHARE`. Open it up.
- Inside, you'll have a directory named `system`. Inside, you'll find a file called `recalbox.conf`. Open this file.
- At the bottom of this file, on an empty line, add the following line:

```ini
emulationstation.debuglogs=1
```

- Once you've added this line, save the file and exit the software that allowed you to modify it.
- Reconnect your storage device and start your Recalbox.

#### Creation of a support archive

Next, you need to create a support archive. Here's how to do it:

- Once you've activated the debug mode described above and started your Recalbox, reproduce the error.
- Now go to the [web manager](./basic-usage/features/webmanager) on your browser.
- Once you're on it, at the bottom right of your screen, you'll see a little cogwheel. Click on it and in the menu that opens, click on `Support Archive`.
- After a while, you should have a link to your support archive. Download it.

>If you get an error at the end of the script execution, you'll be able to find your support archive on your storage medium, in the `saves` folder. Its name starts with `recalbox-support-` and ends with `.tar.gz`.
>
>The `saves` folder is located next to the `system` folder mentioned above.
{.is-info}

Once you've downloaded your support archive, you can provide it directly to the support person who asked for it, either on the [forum](https://forum.recalbox.com) or on [Discord](https://discord.gg/NbQFbGM).