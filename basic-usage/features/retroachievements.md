---
title: Retroachievements
description: 
published: true
date: 2023-07-06T11:22:36.693Z
tags: retroachievements
editor: markdown
dateCreated: 2021-06-29T09:59:06.891Z
---

## Introducing retroachievements

The retrogaming community has developed a platform [retroachievements.org](http://www.retroachievements.org/)
for players who want to rise challenges on their favorite games.

This platform supports the great part of old video-game systems (NES, SNES, GB, GBC, GBA, Genesis, PC engine, etc.) and offers challenges for most of games.

Recalbox allows you to connect a [retroachievements.org](http://www.retroachievements.org/) account, rising to challenges and being in the competition of best players. 

## Account creation 

* Rendez-vous on [retroachievements.org](http://www.retroachievements.org/) and [create an account](http://retroachievements.org/createaccount.php)

* Keep in mind your login and password, they will be used to set up the feature in Recalboxtp://www.retroachievements.org/)
* Keep in mind your login and password, they will be used to setup the feature in Recalbox.

## Account activation in Recalbox

To be done in EmulationStation settings. 

* Open EmulationStation settings with the `START` button. 
* Go to `GAME SETTINGS` > `RETROACHIEVEMENTS SETTINGS`.
* Filling fields :

  * `RETROACHIEVEMENTS`: ON
  * `USER NAME`: your login
  * `PASSWORD`: your password

- In addition you can enable **the hardcore mode**, this options allows to play the original game format removing emulators features (save states, rewind, using cheats, etc.).

## Emulators support

The retroachievements feature supports a great part of Recalbox emulators but some of them are not supported as well. For example Game Cube and Wii emulators are not supported at all, some other games consoles are supported depending the core.

To check the compatibility of a core, please [read the official retroachievements support table](https://docs.retroachievements.org/Emulator-Support-and-Issues/).

## Games and roms support

Most popular games are supported,read [the official game list](http://retroachievements.org/gameList.php) to be sur that your game is supported. 

For every supported game only some Roms are handled. Because Retroachievements.org is an American project there is a  better coverage with US and "No-Intro" Roms.. but some European Roms are also compliants. 

You can check the compatibility of your game file doing the comparison between your hash file and the "The list of Supported Game Files" under the game description page. 

## Viewing achievements in progress

To get a look on your achievements, rendez-vous in the RetroArch and with keys `Hotkey`+`B` and after go to `Achievements`.

To go back in-game, push `A` to go in the main Retroarch menu and select `Resume` with the key`B` to exit the menu.

