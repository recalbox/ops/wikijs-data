---
title: 6. 💡 FAQ
description: 
published: true
date: 2024-11-28T13:38:48.327Z
tags: faq
editor: markdown
dateCreated: 2021-05-21T07:48:39.876Z
---

## Général

Dans ce cette page, nous répondons aux questions fréquemment posées par les utilisateurs de Recalbox. Si votre question porte sur une version particulière, rendez-vous sur les pages dédiées :
- [FAQ Recalbox 8](/fr/faq/recalbox-8)
- [FAQ Recalbox 9](/fr/faq/recalbox-9)

En complément des pages précédentes, nous recommandons la lecture de [la page des problèmes connus](/fr/faq/known-issues). 

### Sur quoi puis-je installer Recalbox ?

Vous pouvez installer Recalbox sur les appareils suivants :

- **Raspberry Pi 5**
- Raspberry Pi 4
- Raspberry Pi 400
- Raspberry Pi 3 (B/B+)
- Raspberry Pi 2
- Raspberry Pi 1 (B/B+)
- Raspberry Pi Zero
- Raspberry Pi Zero 2
- Odroid XU4
- Odroid Go Advance (Console Portable)
- Odroid Go Super (Console Portable)
- PC x86 (32bits)
- PC x86_64 (64bits)
- Anbernic RG353(M/V/P)
- Anbernic RG351

### Comment puis-je participer ?

- Vous pouvez participer à notre [Patreon](https://patreon.com/recalbox)
- Vous pouvez commandez votre matériel sur [Kubii](https://kubii.fr/221-recalbox)
- Vous pouvez remonter des bugs ou des idées d'amélioration sur notre [Gitlab](https://gitlab.com/recalbox/recalbox/issues)
- Vous pouvez participer sur les [forums Recalbox](https://forum.recalbox.com) et sur notre [Discord](https://discord.gg/HKg3mZG)
- Vous pouvez [contribuer à ce Wiki](./contribute)

### Où puis-je voir l'avancée du développement de la Recalbox ?

Sur le [gitlab de Recalbox](https://gitlab.com/recalbox). Vous pouvez accéder :  
- aux [issues](https://gitlab.com/recalbox/recalbox/issues) qui sont les tâches en cours.  
- aux [milestones](https://gitlab.com/recalbox/recalbox/-/milestones) sur lesquelles vous pourrez voir les tâches qui sont encore à développer, et celles qui sont terminées.

### Recalbox contient combien de jeux ?

Nous essayons d'ajouter un maximum de jeux libres dans la distribution, pour que vous puissiez profiter de l'expérience Recalbox dès le premier lancement !
Rendez-vous sur [cette page](https://forum.recalbox.com/topic/52/homebrews-sur-la-recalbox) pour proposer vos homebrews !

### Puis-je utiliser Recalbox dans une borne / bartop ?

Oui. Le système Recalbox a été aussi pensé pour l'intégration dans les bornes et bartops.
- Vous pouvez brancher les boutons et joysticks directement sur les GPIO du Raspberry.
- Vous pouvez désactiver les menus pour éviter toute mauvaise manipulation.
- Il est aussi possible d'utiliser les sorties HDMI / DVI /VGA (avec adaptateur) ou RCA.
Vous trouverez plus d'informations sur [la page dédiée](./tutorials/system/installation/bartop-arcade-configuration).

### La Recalbox supporte beaucoup de systèmes mais je n'en vois que quelques uns dans l'interface, que faire?

Seuls les systèmes ayant des jeux disponibles s'affichent. Essayez d'ajouter vos roms via le réseau et redémarrez la Recalbox pour voir apparaître les nouveaux systèmes.

### J'ai ajouté des roms mais la console ne s'affiche toujours pas

Recalbox affiche uniquement les consoles compatibles avec votre matériel. 

Par exemple l'émulation Sega Saturn n'est pas prise en charge sur Raspberry Pi3 ou plus anciens, il faut au minimum un Raspberry Pi4. Les émulateurs PS2 et Gamecube fonctionnent uniquement sur PC…

Regardez [la grille de compatibilité des émulateurs ](/fr/hardware-compatibility/emulators-compatibility) pour vérifier que le matériel utilisé est compatible avec l'émulateur  visé. 


### Dois-je overclocker mon Raspberry Pi ?

Sur Raspberry Pi 1 (y compris zero), il est vivement conseillé d'overclocker le Raspberry en mode EXTREM.

## EmulationStation

### Comment changer l'apparence du thème d'EmulationStation ?

Appuyez sur `Start` sur la manette → `PARAMÈTRES D'INTERFACE` → `THEME`

### Comment couper la musique de fond ?

Appuez sur `Start` sur la manette → `OPTIONS DU SON` → Mettre sur Off

### Où se situent les fichiers de configuration d'EmulationStation dans le système de fichiers ?

```ini
/recalbox/share/system/recalbox.conf
```

### Où sont situés les thèmes d'EmulationStation ?

```ini
/recalbox/share/themes
```

## Émulation

### Pourquoi y a-t-il plusieurs émulateurs pour chaque système ?

L'émulation n'est pas une opération sans faille, il y a un équilibre entre performances, fonctionnalités et fidélité.
Étant donné que l'émulation n'est pas parfaite, il est préférable d'avoir plusieurs options pour la gérer, parfois un émulateur plus rapide sera meilleur mais peut avoir des problèmes inattendus, tandis qu'un plus lent peut avoir une précision plus élevée mais ne prend pas en charge certaines fonctionnalités spécifiques.

### Comment changer l'émulateur par défaut pour un système donné ?

En appuyant sur `START` sur la manette de jeu, puis en accédant aux paramètres avancés, et enfin dans la configuration avancée des émulateurs. Vous pouvez sélectionner le système souhaité, vous pouvez changer les paramètres de l'émulateur et du noyau (notez que le paramètre de base ne changera pas si le paramètre de l'émulateur indique Défaut).

## Système

### Pourquoi, sur certains systèmes comme la GBA, mes jeux affichent un écran noir puis un retour sous EmulationStation ?

Certains systèmes nécessitent des bios pour fonctionner. Vous avez peut-être ignoré le message d'avertissement avant de lancer le jeu.

Si, malgré tout, le problème continue, contactez l'équipe sur les forums ou Discord.

## Dépannage général

### Ma Recalbox vient de planter, dois-je la débrancher et la rebrancher ?

Non, dans la plupart des cas, le système n'a pas complètement planté, seule EmulationStation l'a fait.

Il existe plusieurs façons de gérer cette situation, la plus simple étant l'utilisation des fonctionnalités de débogage du Webmanager pour redémarrer EmulationStation ou pour arrêter correctement Recalbox.

Ne pas le faire peut corrompre vos données, surtout si vous utilisez un Raspberry Pi avec une carte SD.

### Ma manette ne fonctionne pas

Vous devriez vérifier que votre manette est [officiellement supportée](/fr/tutorials/controllers/controllers). 

Il est vivement recommandé de NE PAS utiliser de matériel générique. 

En dépit des efforts de développement, Recalbox ne peut supporter tous les périphériques et vous aurez à coups sûr des difficultés en utilisant une manette bon marché. 

Si vous avez un modèle supporté, soyez attentif au firmware de la manette. 

Par exemple en règle générale les manettes 8Bitdo sont supportées mais les derniers firmwares viennent casser la compatibilité Recalbox. 

Si votre manette fonctionne, mettre à jour le firmware n'est pas recommandé. 

### Peut-on réutiliser un disque dur Recalbox (SSD, NVMe, etc.) sur une autre machine ? 

Non, Recalbox n'est pas conçu pour fonctionner sur une autre machine que celle utilisée pour initialiser le système. Si vous voulez réutiliser le disque sur une machine différente, il faut réinstaller Recalbox en suivant la procédure habituelle.

### Je n'arrive plus à lancer n'importe quel jeu et j'ai un message d'erreur parlant de bios. Que puis-je faire ?

Vous aviez votre Recalbox qui fonctionnait et qui ne fonctionne plus avec ce message ?

![Message d'erreur](/faq/error-fr.png){.align-center}

Afin de pouvoir obtenir de l'aide face à cette erreur, avez-vous :

- Vérifié que vos bios pour le système concerné sont bons (si ce système a des bios) ?
- Reconfiguré vos manettes ?
- Votre jeu est correct et est bien reconnu [chez les groupes de préservation](./tutorials/games/generalities/isos-and-roms/differents-groups) ?

Une fois toutes ces vérifications effectuées et que le même problème continue, veuillez suivre ces étapes.

### Activation du mode debug

Vous devez, dans un premier temps, activer le mode debug. Celui-ci nous permettra d'y voir plus clair quand votre problème survient.

Pour cela, suivez les étapes suivantes :

- Éteignez votre Recalbox.
- Mettez le support qui vous sert de média de stockage sur votre ordinateur. Cela dépend de ce que vous utilisez pour mettre vos jeux.
- Une fois ce support de stockage mis sur votre ordinateur, vous devez avoir un lecteur qui doit se nommer `SHARE`. Ouvrez-le.
- Dedans, vous aurez un répertoire nommé `system`. Dedans, vous aurez un fichier nommé `recalbox.conf`. Ouvrez ce fichier.
- Tout en bas de ce fichier, sur une ligne vide, ajoutez la ligne suivante :

```ini
emulationstation.debuglogs=1
```

- Une fois cette ligne ajoutée, sauvegardez le fichier et quittez le logiciel qui vous a permis de modifier ce fichier.
- Rebranchez votre support de stockage et démarrez votre Recalbox.

### Création d'une archive de support

Vous devez maintenant, dans un second temps, créer une archive de support. Voici comment faire :

- Une fois le mode debug vu au-dessus activé et votre Recalbox démarré, reproduisez l'erreur.
- Maintenant, vous devez allez dans le [web manager](./basic-usage/features/webmanager) sur votre navigateur.
- Une fois dessus, en bas à droite de votre écran, vous aurez une petite roue crantée. Cliquez dessus et dans le menu qui s'ouvre, cliquez sur `Archive de support`.
- Au bout d'un moment, vous devriez avoir un lien vers votre archive de support. Téléchargez-là.

>Si vous avez une erreur pendant la génération de l'archive de support, vous pourrez retrouver votre archive de support sur votre support de stockage, dans le dossier `saves`. Celle-ci a son nom qui commence par `recalbox-support-` et se termine par `.tar.gz`.
>
>Le dossier `saves` se trouve à côté du dossier `system` vu plus haut.
{.is-info}

Une fois que vous avez récupéré votre archive de support, vous pouvez la fournir directement à la personne du support qui vous l'a demandé, que ce soit sur le [forum](https://forum.recalbox.com) ou sur [Discord](https://discord.gg/NbQFbGM).