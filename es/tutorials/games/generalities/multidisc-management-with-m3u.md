---
title: Gestión de juegos multi-discos con el formato .M3U
description: 
published: true
date: 2024-07-03T14:52:26.779Z
tags: m3u, gestión, multi-discos
editor: markdown
dateCreated: 2023-09-08T09:59:01.893Z
---

## Introducción

Se pueden cargar simultáneamente varios CDs del mismo juego desde EmulationStation en RetroArch creando un fichero `.M3U` (texto plano `.TXT` con extensión `.M3U`).

Una lista de reproducción `.M3U` se puede utilizar con cualquier extension del juego:
- `.BIN` / `.IMG` / `.CCD` / `.SUB`,
- `.BIN` / `.GDI` / `.RAW`,
- `.CHD`,
- `.CDI`,
- etc.

## Utilización

Aquí se os presentan varios ejemplos de uso. Este formato de fichero se puede aplicar en todos los sistemas que utilizan varios discos, cartuchos o disquetes y cuyos emuladores soportan el formato `.M3U`.

## {.tabset}

### Formato `.BIN` / `.CUE`.

* Cred un nuevo fichero de texto llamado `Final Fantasy VII (France).m3u` que contenga la siguiente información:

```text
Final Fantasy VII (Francia) (Disco 1).cue
Final Fantasy VII (Francia) (Disco 2).cue
Final Fantasy VII (Francia) (Disc 3).cue
```

### Formato `.CHD`

* Cread un nuevo fichero de texto llamado `Final Fantasy VII (France).m3u` que contenga la siguiente información:

```text
Final Fantasy VII (Francia) (Disco 1).chd
Final Fantasy VII (Francia) (Disco 2).chd
Final Fantasy VII (Francia) (Disco 3).chd
```

### Formato `.BIN` / `.IMG` / `.CCD` / `.SUB`

* Cread un nuevo fichero de texto llamado `Final Fantasy VII (France).m3u` que contenga la siguiente información:

```texto
Final Fantasy VII (Francia) (Disco 1).ccd
Final Fantasy VII (Francia) (Disco 2).ccd
Final Fantasy VII (Francia) (Disco 3).ccd
```

### Formato `.BIN` / `.GDI` / `.RAW` 

* Cread un nuevo fichero de texto llamado `Final Fantasy VII (France).m3u` que contenga la siguiente información:

```texto
Final Fantasy VII (Francia) (Disco 1).gdi
Final Fantasy VII (Francia) (Disco 2).gdi
Final Fantasy VII (Francia) (Disco 3).gdi
```

### Format `.CDI`
 
* Créez un nouveau fichier texte que l'on renomme `Grandia II (USA)[Zorlon].m3u` et qui contient les informations suivantes :
 
```text
Grandia II (USA)(CD 1 of 3)[Zorlon].cdi
Grandia II (USA)(CD 2 of 3)[Zorlon].cdi
Grandia II (USA)(CD 3 of 3)[Zorlon].cdi
```

## Cambiar de disco durante una partida

Si quereis cambiar de disco mientras el juego está en marcha, hay que hacer lo siguiente:

* Abrid la tapa "virtual" del CD: tecla `HK` + `Joystick izquierdo` (Dirección Arriba)
* Cambiad el disco: Tecla `HK` + `Joystick izquierdo` (dirección Derecha o Izquierda).
* Cerrad la tapa "virtual" de CDs: tecla `HK` + `Joystick izquierdo` (dirección Arriba)

## El caso particular de Dolphin

En function del sistema operativo que utilizáis (en la mayoria de los casos Windows), podéis encontraros con un mensaje de error como este:

![Message d'erreur du fichier M3U avec Dolphin](/tutorials/games/generalities/m3u-dolphin-fr.png){.center}

En este caso, abrid Notepad++ ([instaladlo antes si no lo habéis hecho nunca](https://notepad-plus-plus.org/downloads/)) y abrid el fichero `.M3U`. En los menús de Notepad++, entrad en `Edición` > `Convertir los saltos de línea` y seleccionad `Convertir al formato UNIX (LF)`. Una vez hecho esto, grabad vuestro fichero para que pueda ser leído correctamente por Dolphin.