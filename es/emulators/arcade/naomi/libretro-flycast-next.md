---
title: Libretro Flycast-Next
description: 
published: true
date: 2024-07-29T11:18:36.806Z
tags: libretro, naomi, 9.2+, flycast-next
editor: markdown
dateCreated: 2024-07-29T11:18:36.806Z
---

**Libretro Flycast** es un emulador **Sega Dreamcast multiplataforma** capaz de emular los siguientes sistemas:

* **NAOMI M1**
* **NAOMI M2**
* **NAOMI M4**

## ![](/emulators/license.svg) Licencia

Este core está bajo licencia [**GPLv2**](https://github.com/flyinghead/flycast/blob/master/LICENSE).

## ![](/emulators/compatibility.png) Compatibilidad

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ❌ | ❌ | ✅ | ✅ | ✅ | ✅ | ❌ | ✅ | ✅ |

## ![](/emulators/features.png) Funcionalidades

| Funcionalidad | Soportado |
| :---: | :---: |
| Reinicio | ✔ |
| Capturas de pantalla | ✔ |
| Guardados | ✔ |
| Opciones del core | ✔ |
| Trucos RetroArch | ✔ |
| Controles | ✔ |
| Remapeo | ✔ |
| Vibración | ✔ |
| Control del disco | ✔ |

## ![](/emulators/bios.svg) BIOS

### Lista de BIOS obligatorios

| Nombre de archivo | Descripción | MD5 | Proporcionado |
| :---: | :---: | :---: | :---: |
| naomi.zip | Formato MAME a partir del Romset MAME 0.154 | eb4099aeb42ef089cfe94f8fe95e51f6 | ❌ |

### Lista de BIOS opcionales

| Nombre de archivo BIOS | Descripción | MD5 | Proporcionado |
| :---: | :---: | :---: | :---: |
| airlbios.zip | BIOS Naomi para el juego «Airline Pilots Deluxe» de MAME | 3f348c88af99a40fbd11fa435f28c69d | ❌ |
| hod2bios.zip | BIOS Naomi para el juego «The House of the Dead 2» de MAME | 9c755171b222fb1f4e1439d5b709dbf1 | ❌ |

### Ubicación

Coloca los BIOS de la siguiente manera:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 dc
┃ ┃ ┃ ┃ ┣ 🗒 **naomi.zip**
┃ ┃ ┃ ┃ ┣ 🗒 **airlbios.zip**
┃ ┃ ┃ ┃ ┣ 🗒 **hod2bios.zip**

## ![](/emulators/roms.png) Roms

Flycast se basa en el Romset de **Mame** pero también en el formato **nullDC** para su parte Naomi.

### Romset Mame

Solo los roms Naomi **provenientes de un romset MAME 0.135 o superior** son compatibles.
Te recomendamos el último romset 0.258 que aportará su lote de compatibilidad adicional.
Para más información sobre la versión del Romset en curso: [MameDev](https://www.mamedev.org/release.html).

>El archivo dat para ordenar tus roms arcade está disponible en el directorio `/recalbox/share/bios/dc/`
{.is-info}

### Romset NullDC

Estos roms son compatibles con Flycast pero menos fiables que los roms provenientes de un Romset Mame.

>Los Roms **NullDC** están en formato: _`.bin + .lst`_
{.is-info}

Ejemplo para el juego `cfield.zip`:

* `/recalbox/share/roms/naomi/cfield/...`
* `/recalbox/share/roms/naomi/cfield/cfield.bin`
* `/recalbox/share/roms/naomi/cfield/cfield.lst`

### Ubicación

Coloca los roms de la siguiente manera:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 naomi
┃ ┃ ┃ ┃ ┣ 🗒 **juego.zip**

## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

>Para poder conservar tus configuraciones personalizadas durante una actualización, te recomendamos utilizar nuestra funcionalidad [Sobrecargas de configuración](./../../../advanced-usage/configuration-override).
{.is-info}

### Acceder a las opciones

Puedes configurar diversas opciones de dos maneras diferentes.

* A través del Menú RetroArch:

┣ 📁 Menú RetroArch
┃ ┣ 📁 Opciones del core
┃ ┃ ┣ 🧩 Name_option

* A través del archivo `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Opciones del core

| Opciones | Valor de las opciones | Variable (para sobrecarga) | Valores de las variables |
| --- | --- | --- | --- |
| Región | `Japón` / `Estados Unidos` ✅ / `Europa` / `Por defecto` | `reicast_region` | `Japan` / `USA` / `Europe` / `Default` |
| Activar el DSP | `Desactivado` / `Activado` ✅ | `reicast_enable_dsp` | `disabled` / `enabled` |
| Forzar el modo Windows CE | `Desactivado` ✅ / `Activado` | `reicast_force_wince` | `disabled` / `enabled` |
| Permitir los botones de servicio NAOMI | `Desactivado` ✅ / `Activado` | `reicast_allow_service_buttons` | `disabled` / `enabled` |
| Definir los juegos NAOMI en Free Play | `Desactivado` / `Activado` ✅ | `reicast_force_freeplay` | `disabled` / `enabled` |
| Resolución interna | `320x240` / `640x480` ✅ / `800x600` / `960x720` / `1024x768` / `1280x960` / `1440x1080` / `1600x1200` / `1920x1440` / `2560x1920` / `2880x2160` / `3200x2400` / `3840x2880` / `4480x3360` / `5120x3840` / `5760x4320` / `6400x4800` / `7040x5280` / `7680x5760` / `8320x6240` / `8960x6720` / `9600x7200` / `10240x7680` / `10880x8160` / `11520x8640` / `12160x9120` / `12800x9600` | `reicast_internal_resolution` | `320x240` / `640x480` / `800x600` / `960x720` / `1024x768` / `1280x960` / `1440x1080` / `1600x1200` / `1920x1440` / `2560x1920` / `2880x2160` / `3200x2400` / `3840x2880` / `4480x3360` / `5120x3840` / `5760x4320` / `6400x4800` / `7040x5280` / `7680x5760` / `8320x6240` / `8960x6720` / `9600x7200` / `10240x7680` / `10880x8160` / `11520x8640` / `12160x9120` / `12800x9600` |
| Orientación de la pantalla | `Horizontal` ✅ / `Vertical` | `reicast_screen_rotation` | `horizontal` / `vertical` |
| Ordenación alfa | `Por tira (rápido, menos preciso)` / `Por triángulo (normal)` ✅ / `Por píxel (preciso, pero más lento)` | `reicast_alpha_sorting` | `per-strip (fast, least accurate)` / `per-triangle (normal)` / `per-pixel (accurate)` |
| Emulación completa del framebuffer | `Desactivado` ✅ / `Activado` | `reicast_emulate_framebuffer` | `disabled` / `enabled` |
| Activar el búfer RTT (renderizado a textura) | `Desactivado` ✅ / `Activado` | `reicast_enable_rttb` | `disabled` / `enabled` |
| Mipmapping | `Desactivado` / `Activado` ✅ | `reicast_mipmapping` | `disabled` / `enabled` |
| Efectos de niebla | `Desactivado` / `Activado` ✅ | `reicast_fog` | `disabled` / `enabled` |
| Modificador del volumen | `Desactivado` / `Activado` ✅ | `reicast_volume_modifier_enable` | `disabled` / `enabled` |
| Filtrado anisotrópico | `Desactivado` / `2` / `4` ✅ / `8` / `16` | `reicast_anisotropic_filtering` | `off` / `2` / `4` / `8` / `16` |
| Filtrado de texturas | `Por defecto` ✅ / `Forzar al más cercano` / `Forzar lineal` | `reicast_texture_filtering` | `0` / `1` / `2` |
| Retrasar el intercambio de fotogramas | `Desactivado` ✅ / `Activado` | `reicast_delay_frame_swapping` | `disabled` / `enabled` |
| Detectar cambios en el intervalo de sincronización vertical | `Desactivado` ✅ / `Activado` | `reicast_detect_vsync_swap_interval` | `disabled` / `enabled` |
| Filtro de postprocesamiento PowerVR2 | `Desactivado` ✅ / `Activado` | `reicast_pvr2_filtering` | `disabled` / `enabled` |
| Ampliación de texturas (xBRZ) | `Desactivado` ✅ / `x2` / `x4` / `x6` | `reicast_texupscale` | `disabled` / `2` / `4` / `6` |
| Factor máximo del filtro de ampliación de texturas | `256` / `512` / `1024` | `reicast_` | `256` / `512` / `1024` |
| Interpolación de profundidad nativa | `Desactivado` ✅ / `Activado` | `reicast_native_depth_interpolation` | `disabled` / `enabled` |
| Renderizado en múltiples hilos | `Desactivado` / `Activado` ✅ | `reicast_thraded_rendering` | `disabled` / `enabled` |
| Salto automático de fotogramas | `Desactivado` ✅ / `Normal` / `Máximo` | `reicast_auto_skip_frame` | `disabled` / `some` / `more` |
| Salto de fotogramas | `Desactivado` ✅ / `1` / `2` / `3` / `4` / `5` / `6` | `reicast_frame_skipping` | `disabled` / `1` / `2` / `3` / `4` / `5` / `6` |
| Trucos de pantalla ancha (Reinicio requerido) | `Desactivado` ✅ / `Activado` | `reicast_widescreen_cheats` | `disabled` / `enabled` |
| Hack de pantalla ancha | `Desactivado` ✅ / `Activado` | `reicast_widescreen_hack` | `disabled` / `enabled` |
| SH4 CPU under/overclock | `100 MHz` / `110 MHz` / `120 MHz` / `130 MHz` / `140 MHz` / `150 MHz` / `160 MHz` / `170 MHz` / `180 MHz` / `190 MHz` / `200 MHz` / `210 MHz` / `220 MHz` / `230 MHz` / `240 MHz` / `250 MHz` / `260 MHz` / `270 MHz` / `280 MHz` / `290 MHz` / `300 MHz` | `reicast_sh4clock` | `100` / `110` / `120` / `130` / `140` / `150` / `160` / `170` / `180` / `190` / `200` / `210` / `220` / `230` / `240` / `250` / `260` / `270` / `280` / `290` / `300`  |
| Cargar texturas personalizadas | `Desactivado` ✅ / `Activado` | `reicast_custom_textures` | `disabled` / `enabled` |
| Importar texturas | `Desactivado` ✅ / `Activado` | `reicast_dump_textures` | `disabled` / `enabled` |
| Zona muerta del stick analógico | `0%` / `5%` / `10%` / `15%` ✅ / `20%` / `25%` / `30%` | `reicast_analog_stick_deadzone` | `0%` / `5%` / `10%` / `15%` / `20%` / `25%` / `30%` |
| Zona muerta de los gatillos | `0%` ✅ / `5%` / `10%` / `15%` / `20%` / `25%` / `30%` | `reicast_trigger_deadzone` | `0%` / `5%` / `10%` / `15%` / `20%` / `25%` / `30%` |
| Gatillos digitales | `Desactivado` ✅ / `Activado` | `reicast_digital_triggers` | `disabled` / `enabled` |
| Difundir salidas digitales | `Desactivado` ✅ / `Activado` | `reicast_network_output` | `disabled` / `enabled` |
| Mostrar la mira de la pistola 1 | `Desactivado` ✅ / `Blanco` / `Rojo` / `Verde` / `Azul` | `reicast_lightgun1_crosshair` | `disabled` / `White` / `Red` / `Green` / `Blue` |
| Mostrar la mira de la pistola 2 | `Desactivado` ✅ / `Blanco` / `Rojo` / `Verde` / `Azul` | `reicast_lightgun2_crosshair` | `disabled` / `White` / `Red` / `Green` / `Blue` |
| Mostrar la mira de la pistola 3 | `Desactivado` ✅ / `Blanco` / `Rojo` / `Verde` / `Azul` | `reicast_lightgun3_crosshair` | `disabled` / `White` / `Red` / `Green` / `Blue` |
| Mostrar la mira de la pistola 4 | `Desactivado` ✅ / `Blanco` / `Rojo` / `Verde` / `Azul` | `reicast_lightgun4_crosshair` | `disabled` / `White` / `Red` / `Green` / `Blue` |
| Sonidos VMU | `Desactivado` ✅ / `Activado` | `reicast_vmu_sound` | `disabled` / `enabled` |

## ![](/emulators/external-links.png) Enlaces externos

* **Código fuente utilizado**: [https://github.com/flyinghead/flycast/](https://github.com/flyinghead/flycast)
