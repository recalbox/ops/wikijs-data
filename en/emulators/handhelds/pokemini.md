---
title: Pokémon mini
description: 
published: true
date: 2021-08-09T13:07:52.123Z
tags: nintendo, handhelds, pokémon, mini, pokémini
editor: markdown
dateCreated: 2021-07-06T09:32:45.409Z
---

![](/emulators/handheld/pokemini.svg){.align-center}

## Technical data

* **Manufacturer**: Nintendo
* **Year of release**: 2001
* **Best-selling game**: Pokémon Party mini
* **CPU** : S1C88 @ 4 MHz
* **RAM**: 4 kB (shared with video subsystem)
* **Display**: 96 x 64 pixels monochrome LCD
* **Sound**: Single channel PWM sound with three volume levels
* **Cart size**: 512 kB

## Presentation

**Pokémon mini** is a portable video game system designed and distributed by Nintendo and based on the Pokémon franchise. It is the smallest cartridge-based game console produced by Nintendo. It has a black and white screen, and features a clock, a vibrator and a shock sensor. It also has an infrared connection for multiplayer games.

The console is designed for young children. The official games are simple and short, and all have a Pokémon theme. Games outside the Pokémon franchise have been announced by Nintendo, especially Game and Watch adaptations, but have never been released.

The console is retroengineered in order to develop amateur games. On this console, only ten games are available. Japan is the only country to have all the games, Europe has only five and the United States has only four.

## Emulators

[Libretro Pokémini](libretro-pokemini)