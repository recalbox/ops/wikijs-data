---
title: Pong
description: 
published: true
date: 2024-07-26T08:30:29.761Z
tags: ports, gong, 8.0+
editor: markdown
dateCreated: 2023-09-04T12:39:23.134Z
---

![](/emulators/ports/pong.svg){.align-center}

## Detalles técnicos

* **Desarrollador**: Dan Zaidan
* **Fecha de lanzamiento**: 2018

## Presentación

Pong es uno de los primeros videojuegos arcade y el primer videojuego arcade de deportes. Fue ideado por el estadounidense Nolan Bushnell y desarrollado por Allan Alcorn, y fue comercializado por Atari a partir de noviembre de 1972. Aunque ya se habían inventado otros videojuegos anteriormente, como Computer Space, Pong fue el primero en popularizarse.

El juego está inspirado en el tenis de mesa en vista cenital, y cada jugador compite moviendo la raqueta virtual arriba y abajo, mediante un mando giratorio, para mantener la pelota en el campo de juego. El jugador puede cambiar la dirección de la pelota dependiendo de dónde golpee la raqueta, mientras que su velocidad aumenta gradualmente en el transcurso de la ronda. Se muestra una puntuación para el juego en curso y los efectos de sonido acompañan a la pelota que golpea las raquetas.

Pong fue originalmente un ejercicio que Bushnell pidió a Alcorn como entrenamiento. Ralph Baer ya había creado una versión similar para la consola de juegos Odyssey de Magnavox, pero poco se sabe de su existencia. Sorprendidos por la calidad del resultado, Bushnell y Ted Dabney, fundadores de Atari, decidieron comercializar el juego en 1972. El hecho de que Magnavox copiara el concepto dio lugar a una acción legal contra Atari por infracción de patente en 1976.

Lanzado a finales de 1972, el terminal arcade fue un éxito, vendiendo casi 8.000 unidades al año siguiente y generando unas ventas de hasta 40 millones de dólares en 1975, superando todas las predicciones realizadas por Bushnell y Dabney. Este éxito animó a varias empresas a copiar el concepto para sus propios videojuegos, en particular las consolas domésticas. Tras el éxito de la máquina recreativa y la locura generada por la competencia por los juegos domésticos, Pong se llevó a una consola doméstica dedicada con el nombre de Home Pong a partir de 1975, comercializada por Sears y, un año más tarde, directamente por Atari. Este doble éxito se considera el acontecimiento precursor de la industria del videojuego, con un fuerte aumento de la oferta y la adopción del concepto por cientos de consolas domésticas.

## Emuladores

[Libretro Gong](libretro-gong)