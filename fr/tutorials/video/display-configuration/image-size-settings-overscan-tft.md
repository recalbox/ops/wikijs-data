---
title: Réglage de la taille de l'image en utilisant l'overscan sous TFT
description: 
published: false
date: 2024-07-25T19:11:26.409Z
tags: tft, image, overscan, taille
editor: markdown
dateCreated: 2021-05-21T08:43:42.170Z
---

L'option overscan peut être activée si vous avez des bordures noires sur l'image ou si votre image est recadrée.

## Activation

* Menu EmulationStation (bouton `START` de votre manette)
* `PARAMÈTRES AVANCÉES`
* Activer l'option `OVERSCAN`

>Vous devez redémarrer votre Raspberry Pi après avoir modifié cette option.
{.is-warning}

## Configuration

* Si elle ne correspond pas à votre écran, vous pouvez modifier les paramètres de chaque bordure dans votre fichier [recalbox-user-config.txt](./../../../tutorials/system/modification/configtxt-configuration) :

```ini
disable_overscan=0
overscan_left=24
overscan_right=24
overscan_top=24
overscan_bottom=24
```

La valeur précise le nombre de pixels à ignorer sur le bord de l'écran. Augmentez cette valeur si l'image sort du bord de l'écran ; et diminuez-la s'il y a une bordure noire entre le bord de l'écran et l'image.

* Si ces paramètres ne sont pas appliqués correctement dans les émulateurs ou EmulationStation, essayez d'ajouter ceci :

```ini
overscan_scale=1
```