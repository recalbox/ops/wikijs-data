---
title: Anbernic
description: 
published: true
date: 2023-06-18T17:04:30.468Z
tags: anbernic
editor: markdown
dateCreated: 2023-06-18T17:01:20.795Z
---

## Anbernic RG353

![anbernic-rg353m.png](/compatibility/anbernic-rg353m.png)

The retro game console based on RK3566 CPU has dual operating system (Android and Linux) and supports dozens of classic game consoles from 1970s to 2000s. It is equipped with WiFi for multiplayer games and an HDMI output for playing on the big screen.

### Features :

* **CPU:** 
  * RK3566, Quad-Core, Cortex-A55 64 bits, fréquence jusqu'à 1,8 Ghz
* **GPU:**
  * Mali™ G52 3D Accelerator
* **RAM:**
  * LPDDR4 2 GB
* **Connectivity available:** 
  * 2 X USBC
  * 1 X mini HDMI
  * 2 X MicroSD slots (up to 512 GB)
  * 1 X Jack
* **Network:**
  * Wi-Fi 2.4G/5G 802.11 a/b/g/n/ac
  * Bluetooth v4.2
* **Battery:**
  * Li-Po, 3200 mAh
* **Power supply:**
  * 5w or 10W phone power supply
* **Dimensions:**
  * 14,5 x 7,05 x 1,59 cm
* **Weight:**
  * 232 grammes