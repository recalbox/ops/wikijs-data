---
title: Nintendo 64 DD
description: Nintendo 64 Disk Drive
published: true
date: 2021-08-09T20:26:33.879Z
tags: consoles, nintendo, 64dd, n64dd
editor: markdown
dateCreated: 2021-07-27T16:35:06.734Z
---

![](/emulators/consoles/nintendo64dd.svg){.align-center}

## Technical data

* **Manufacturer**: Nintendo
* **Year of release**: 1999
* **Units sold**: 15000
* **CPU**: 64-bit NEC VR4300 @ 93.75 MHz
* **Co-CPU**: 64-bit MIPS RISC "Reality Immersion" RCP @ 62.5 MHz
* **64DD-CPU**: 32-bit co-processor for reading/transferring disk data
* **RAM**: 4 MB RAMBUS RDRAM (expandable up to 8 MB)
* **Sound**: Stereo 16-Bit and 48 kHz

## Presentation

The **Nintendo 64DD** (Disk Drive) is a peripheral for the Nintendo 64 system released in Japan on December 1st 1999.

The 64DD connects to the Nintendo 64 via the expansion port on the bottom of the system. It allows the Nintendo 64 to read magnetic disks.   
A modem was also used to connect to the Randnet network, which offered various options such as online gaming, a messaging service or Internet browsing.

The 64DD offers the possibility of having two games communicate, one on the Nintendo 64 in cartridge format and the other on the 64DD in disk format, thus being able to accommodate more ambitious projects or extensions.

## Emulators

[Libretro ParaLLEl_n64](libretro-parallel_n64)
[Libretro Mupen64plus_Next](libretro-mupen64plus_next)