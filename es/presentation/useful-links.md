---
title: Enlaces útiles
description: 
published: true
date: 2023-09-24T20:41:32.474Z
tags: discord, twitch, youtube, utiles, changelog, twitter, enlaces, facebook, instagram
editor: markdown
dateCreated: 2021-08-02T18:00:16.865Z
---

## Changelog

* [Notas de la versión](https://gitlab.com/recalbox/recalbox/raw/master/RELEASE-NOTES.md)

## Descargar

* [Última imagen de Recalbox](https://download.recalbox.com/)

## Nuestros sitios

* [www.recalbox.com](https://www.recalbox.com/fr/)
* [Gitlab](https://gitlab.com/recalbox/recalbox/)

## Nuestras redes de apoyo

* [Discord](https://discord.gg/NbQFbGM)
* [Foro](https://forum.recalbox.com/)

## Nuestras redes sociales

* [Facebook](https://www.facebook.com/recalbox)
* [Facebook Francia](https://www.facebook.com/recalbox.fr)
* [Instagram](https://instagram.com/recalbox/)
* [Twitter](https://twitter.com/recalbox)

## Nuestros streams

* [Twitch](https://www.twitch.tv/recalbox)

## Nuestro canal de youtube

* [Youtube](https://www.youtube.com/c/RecalboxOfficial)