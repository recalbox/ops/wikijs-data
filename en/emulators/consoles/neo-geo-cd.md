---
title: Neo-Geo CD
description: 
published: true
date: 2021-08-09T20:18:26.742Z
tags: consoles, snk, neo-geo cd
editor: markdown
dateCreated: 2021-07-28T05:26:47.076Z
---

![](/emulators/consoles/neogeocd.svg){.align-center}

## Technical data

* **Manufacturer**: SNK Corporation
* **Year of release**: 1990
* **Units sold**: 570000
* **CPU**: 16-bit Motorola 68000 @ 12MHz, 8-bit Zilog Z80 @ 4MH
* **RAM**: 7 MB
* **SRAM**: 2 KB
* **Video RAM**: 512 KB
* **Sound chip**: Yamaha YM2610 with 13 channels
* **Display**: 304x224, 4096 colors out of 65536

## Presentation

The **Neo-Geo CD** is a video game console launched on September 9th 1994 in Japan by SNK. It is based on the architecture of the Neo-Geo AES and Neo-Geo MVS, but uses CD-ROM instead of cartridges, in order to reduce the selling price of the games.

There are three versions of the Neo-Geo CD:

* the original model, with a front-mounted CD loader, was released at the end of 1994 and is exclusively distributed in Japan,
* the model with a CD changer on the top, which replaces the first model and is distributed in Europe and the United States,
* the Neo-Geo CDZ, released on December 29, 1995, with a double speed CD player.

The CD-ROM drive of the Neo-Geo CD is both its best asset (the price of the games is much lower than their counterparts in cartridge format) and its main drawback. Indeed, its transfer speed of 150 kio/s (single speed) associated with the low amount of RAM available, impose important loading times during the game. If the oldest titles only suffer moderately from this, the increase in size of SNK productions has quickly forced the player to wait several seconds before and during each game.

Furthermore, due to the limited D-RAM of 56 Mio, some of the latest games have been modified. Mostly, it is about animation steps or effects that have been reduced like The Last Blade and The Last Blade 2, only the game Art of Fighting 3: The Path of the Warrior has been clearly modified with smaller sprites than in the MVS/AES version.

## Emulators

[Libretro FBNeo](libretro-fbneo)
[Libretro NeoCD](libretro-neocd)