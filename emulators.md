---
title: 5. 🛠️ EMULATORS
description: 
published: true
date: 2023-10-02T18:56:52.467Z
tags: emulators
editor: markdown
dateCreated: 2021-05-28T22:21:41.354Z
---

You are now in the more sophisticated part of the whole documentation, concerning all the game systems included in Recalbox:

* [Arcade](arcade)
* [Computers](computers)
* [Consoles](consoles)
* [Fantasy](fantasy)
* [Game engines](engines)
* [Handhelds](handhelds)
* [Ports](ports)

This is the [default game list](included-games) included in the latest Recalbox release.