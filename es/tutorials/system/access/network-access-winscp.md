---
title: Acceso en red via WinSCP
description: 
published: true
date: 2023-09-13T20:11:16.860Z
tags: winscp, red, acceso
editor: markdown
dateCreated: 2023-09-13T20:11:16.860Z
---

Este tutorial se dirige a los usuarios de Windows. Consultad [este tutorial](./../../../tutorials/system/access/network-access-cyberduck) si sois usuarios de macOS o si preferís utilizar Cyberduck en Windows.

## Requisitos previos

* Aseguráos de que vuestra Recalbox está funcionando y que se conecta a la red mediante un cable Ethernet o un dongle WiFi correctamente.

>¿No sabéis si vuestra Recalbox está conectada a la red? Consultad [este tutorial](./../../../tutorials/network/ip/discover-recalbox-ip) para encontrar la dirección IP de vuestra Recalbox.
{.is-info}

* Descargad e instalad el programa [WinSCP](https://winscp.net/).

## Configuración

* Lanzad `WinSCP` y aparecerá una ventana para crear vuestra primera conexión.
* En esta ventana, rellenad las siguientes informaciones:
  * Protocolo de fichero `SFTP`
  * Nombre de host `recalbox` o la dirección IP de vuestra Recalbox.
  * Numéro de puerto `22`
  * Usuario `root`
  * Contraseña `recalboxroot`
* Pulsad `Grabar...`.
* Rellenad las siguientes informaciones:
  * Grabar la sesión como: `Recalbox` por ejemplo.
  * Activad la opción `Grabar la contraseña (no aconsejado)` y pulsad `OK`.
* Vuestro programa está configurado y listo para conectarse a Recalbox.

## Connexión

* En la ventana que está abierta, seleccionad vuestra conexión recién creada y pulsad `Connexión`.
* Se abrirá otra ventana que os informa que la conexión está en curso.
* Finalmente, os conectáis.

>Puede que se abra otra ventana que os pedirá `¿Continuar con la conexión al servidor desconocido y añadir su llave al cache?` Pulsad `Oui` si esto ocurre.
{.is-warning}

## Mostrar archivos ocultos

Esto es útil para acceder a la carpeta `.emulationstation` para cambiar el theme, por ejemplo.

* Pulsad en `Opciones` > `Preferencias`.
* Seleccionad "Paneles" a la izquierda.
* Marcad la casilla `Mostrar archivos ocultos` y pulsad `Aceptar`.

## Utilización

El uso es bastante intuitivo. Por defecto, encontraréis vuestro ordenador a la izquierda y el árbol de carpetas de Recalbox a la derecha.

**Algunos consejos** :

* Para volver a la carpeta superior o a la raíz de tu Recalbox, pulsad en la primera carpeta con un icono de volver al principio identificado como `..`.
* Podéis arrastrar y soltar carpetas en la columna de la derecha o en la columna de la derecha.
* Podéis cambiar los permisos de un fichero haciendo clic con el botón derecho sobre él y seleccionando `Propiedades`.

## Añadir Notepad++

* Descargad [Notepad++](https://notepad-plus-plus.org/downloads/).
* En WinSCP, pulsad `Opciones` > `Preferencias`.
* Seleccionad "Editores" a la izquierda.
* Pulsad `Add` y buscad el directorio de instalación de Notepad++.

## Utilizar Winscp para abrir una consola

Pulsad en el icono `Consola` y escribid el comando que cambia la partición de lectura-sola a lectura-escritura.

![](/tutorials/system/access/winscp.png)