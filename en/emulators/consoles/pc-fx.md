---
title: PC-FX
description: 
published: true
date: 2021-08-09T20:36:47.193Z
tags: consoles, nec, pc-fx
editor: markdown
dateCreated: 2021-07-28T05:53:22.307Z
---

![](/emulators/consoles/pcfx.svg){.align-center}

## Technical data

* **Manufacturer**: NEC Home Electronics
* **Year of release**: 1994
* **Units sold**: 400000
* **CPU**: 32-bit NEC V810 RISC @ 21.5 MHz
* **RAM**: 2 MB (32 KB backup)
* **VRAM**: 1.25 MB
* **Sound chip**: 16-bit stereo with ADPCM
* **Resolution**: 640x480 pixels
* **Media**: CD-ROM

## Presentation

The **PC-FX** is a 32 bits video game console, developed by NEC. It was released on December 23, 1994, a few weeks after the Sony PlayStation and one month after the Sega Saturn. Designed to succeed the PC-Engine and the SuperGrafX, its games are in CD-ROM format.

Unlike its predecessors, the PC-FX was only released in Japan. The console has the shape of a vertical tower like the tower cases of some PCs, and was meant to be as upgradeable as the latter. However, the PC-FX did not have a graphics chip capable of handling 3D, as instead, NEC focused on full motion video and 2D gaming capabilities. Ultimately, this choice made the FX less powerful than its competitors. It was also expensive and lacked support from game developers and publishers. All of this combined meant that it failed to compete with other fifth-generation consoles. The PC-FX was NEC's last video game console and production was stopped in February 1998, when only 400,000 units had been sold in 4 years.

## Emulators

[Libretro Mednafen_PCFX](libretro-mednafen_pcfx)