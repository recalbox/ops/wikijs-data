---
title: Libretro Virtualjaguar
description: 
published: true
date: 2024-06-23T11:26:17.381Z
tags: libretro, atari, jaguar, virtualjaguar
editor: markdown
dateCreated: 2021-05-21T08:19:46.845Z
---

**Libretro Virtualjaguar** est un fork du core du même nom et développé pour l'Atari Jaguar.

## ![](/emulators/license.svg) Licence

Ce core est sous licence [**GPLv3**](https://github.com/libretro/virtualjaguar-libretro/blob/master/docs/GPLv3).

## ![](/emulators/compatibility.png) Compatibilité

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | ODROID GO | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ❌ | ❌ | ✅ | ✅ | ✅ | ❌ | ✅ | ✅ |

## ![](/emulators/features.png) Fonctionnalités

| Fonctionnalité | Supporté |
| :---: | :---: |
| Redémarrage | ✔ |
| Captures d'écran | ✔ |
| Sauvegardes | ✔ |
| Options du core | ✔ |
| RetroAchievements | ✔ |
| Controls | ✔ |
| Remapping | ✔ |

## ![](/emulators/bios.svg) BIOS

>**Aucun bios n'est requis.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Extensions supportées

Les roms doivent avoir les extensions suivantes :

* .j64
* .jag
* .rom
* .abs
* .cof
* .bin
* .prg
* .zip
* .7z

Ce système supporte les roms compressées au format .zip/.7z. Attention toutefois, il ne s'agit que d'une archive.

Les fichiers contenus dans les .zip/.7z doivent correspondre aux extensions citées précédemment.
De plus, chaque fichier .zip/.7z ne doit contenir qu'une seule rom compressée.

### Emplacement

Placez les roms comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 jaguar
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.zip**

>Les roms au format **No-Intro** sont vivement conseillées.
{.is-success}

>Pour plus d'information sur les roms, rendez-vous sur [ce tutoriel](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuration avancée de l'émulateur

>Pour pouvoir conserver vos configurations personnalisées lors d'une mise à jour, nous vous conseillons d'utiliser notre fonctionnalité [Surcharges de configuration](./../../../advanced-usage/configuration-override).
{.is-info}

### Accéder aux options

Vous pouvez configurer diverses options de deux façons différentes.

* Via le menu RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via le fichier `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Options du core

| Options | Valeur des options | Variable (pour surcharge) | Valeurs des variables |
| --- | --- | --- | --- |
| Fast Blitter | `Désactivé` ✅ / `Activé` | `virtualjaguar_usefastblitter` | `disabled` / `enabled` |
| Doom Res Hack | `Désactivé` ✅ / `Activé` | `virtualjaguar_doom_res_hack` | `disabled` / `enabled` |
| Bios | `Désactivé` ✅ / `Activé` | `virtualjaguar_bios` | `disabled` / `enabled` |
| Pal (Restart) | `Désactivé` ✅ / `Activé` | `virtualjaguar_pal` | `disabled` / `enabled` |
| Enable Core Options Remapping | `Désactivé` ✅ / `Activé` | `virtualjaguar_alt_inputs` | `disabled` / `enabled` |

## ![](/emulators/external-links.png) Liens externes

* **Code source utilisé** : [https://github.com/libretro/virtualjaguar-libretro/](https://github.com/libretro/virtualjaguar-libretro/)
* **Documentation Libretro** : [https://docs.libretro.com/library/virtual_jaguar/](https://docs.libretro.com/library/virtual_jaguar/)