---
title: Configuration pour le GameHat
description: 
published: true
date: 2021-09-04T18:02:05.381Z
tags: configuration, gamehat
editor: markdown
dateCreated: 2021-05-21T08:09:23.541Z
---

## Configuration

Pour tous ceux qui veulent utiliser Recalbox avec le GameHat, voici comment faire :

* Flashez l'image de Recalbox sur votre carte SD.
* Depuis un PC/Mac :
  * Branchez la carte SD
  * Ajoutez ces paramètres au fichier `recalbox-user-config.txt` à la racine de la carte :

```ini
hdmi_force_hotplug=1
avoid_warnings=1
max_usb_current=1
hdmi_group=2
hdmi_mode=1
hdmi_mode=87
hdmi_cvt 640 480 60 6 0 0 0
hdmi_drive=2
display_rotate=0
```

* Connectez une manette de jeu (filaire) et renseignez les paramètres wifi.
* Lorsque vous êtes connecté sur votre réseau, via l'interface web (ou SSH)
  * Modifiez le fichier `recalbox.conf` comme ceci :
    * Changez la ligne `controllers.gpio.enabled=0` par `controllers.gpio.enabled=1`
    * Changez la ligne `controllers.gpio.args=map=1,2` par `controllers.gpio.args=map=5 gpio=5,6,13,19,21,4,26,12,23,20,16,18,-1`
* Débranchez la manette et redémarrez votre Recalbox.
* Après le redémarrage, appuyez sur un bouton et configurez !

## Liens externes

* Lien vers le Wiki du GameHat : [https://www.waveshare.com/wiki/Game\_HAT](https://www.waveshare.com/wiki/Game_HAT) (Anglais)