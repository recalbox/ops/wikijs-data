---
title: HDMI
description: 
published: true
date: 2021-08-06T23:11:02.051Z
tags: hdmi
editor: markdown
dateCreated: 2021-08-06T17:09:02.539Z
---

You can manage some aspects of HDMI outputs, to which you can find related tutorials here.

List of available tutorials :

[Ignore HDMI CEC on Odroid XU4](ignore-hdmi-cec-odroidxu4)