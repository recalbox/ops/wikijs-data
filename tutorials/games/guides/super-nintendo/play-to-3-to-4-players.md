---
title: Play to 3 to 4 players
description: 
published: true
date: 2024-01-07T23:28:58.573Z
tags: nintendo, super, multplayer
editor: markdown
dateCreated: 2021-08-08T09:26:17.768Z
---

It is possible to play up to 4 players on the SNES emulator on Recalbox.

If you want to enjoy multiplayer on SNES, you will need one controller per player, properly configured.

* Launch an SNES game, like Micro Machines for example.
* When the game starts, go to the RetroArch menu by pressing `Hotkey + B`.
* Press `Back` twice and go to `Settings` > `Configuration`.

![](/tutorials/games/guides/snes/multitap/snesmultitap1-en.png){.full-width}

* Enable the `Save configuration on exit` setting.

![](/tutorials/games/guides/snes/multitap/snesmultitap2-en.png){.full-width}

* Press `Return` twice and go to `Main Menu` > `Quick Menu` > `Controls` > `Port 2 Controls`.

![](/tutorials/games/guides/snes/multitap/snesmultitap3-en.png){.full-width}

* On the `Device Type` option, scroll down until you have the text `Multitap` displayed.

![](/tutorials/games/guides/snes/multitap/snesmultitap4-en.png){.full-width}

* Go back once and go to `Manage Remap Files`. Here, select for which case you would like to save the multitap option (for the game, for the content directory, etc.).

* Go back twice and select `Restart`.

And that's it. The game works, and you are the king of the party!