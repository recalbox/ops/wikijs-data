---
title: Supermodel
description: 
published: true
date: 2024-11-10T18:04:18.443Z
tags: model-3, supermodel
editor: markdown
dateCreated: 2023-09-06T06:50:38.930Z
---

Supermodel emula la plataforma arcade Sega Model 3.  
Utiliza **OpenGL 2.1** y **SDL**, y puede ejecutarse en Windows, Linux y macOS.

Esta es una versión pública temprana de Supermodel. Se trata de un documento muy preliminar, la versión alfa del software.
El desarrollo comenzó en enero de 2011 y se ha centrado en aspectos de ingeniería inversa del aún desconocido Model 3.
Como resultado, muchas características importantes, como una interfaz de usuario adecuada, aún no se han implementado y la compatibilidad con los juegos es todavía baja.

## ![](/emulators/license.svg) Licencia

El core posee la siguiente licencia [**GPLV3**](https://www.supermodel3.com/About.html).

## ![](/emulators/compatibility.png) Compatibilidad

 | RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
 | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
 | ❌ | ❌ | ❌ | ❌ | ❌ | ❌ | ✅ | ❌ |

## ![](/emulators/features.png) Funcionalidades

| Funcionalidad | ¿Soportada? |
| :---: | :---: |
| Pause | ✔︎ |
| Grabaciones instantáneas | ✔︎ |
| Capturas de pantalla | ✔︎ |

## ![](/emulators/bios.svg) BIOS

>**No se necesita ninguna bios.**
{.is-success}

## ![](/emulators/roms.png) Roms

Model3 se basa en el romset de Mame que se muestra [aquí](../).

>Debéis filtrar obligatoriamente vuestro romset MAMA con ClrMamePro para obtener un romset Model3 funcional.
{.is-info}

>Para poder filtrar vuestras roms arcade, el fichero dat se encuentra disponible en Recalbox dentro del directorio `/recalbox/share/bios/supermodel/`.
{.is-success}

### Emplazamiento

Disponed vuestros juegos como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 model3
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.zip**

## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

>Para conservar las configuraciones personalizadas cuando se produce una actualización del sistema Recalbox, os aconsejamos utilizar las [Sobrecargas de configuración](./../../../advanced-usage/configuration-override).
{.is-info}

### El fichero de configuración de SuperModel:

```text
#
#  ####                                                      ###           ###
# ##  ##                                                      ##            ##
# ###     ##  ##  ## ###   ####   ## ###  ##  ##   ####       ##   ####     ##
#  ###    ##  ##   ##  ## ##  ##   ### ## ####### ##  ##   #####  ##  ##    ##
#    ###  ##  ##   ##  ## ######   ##  ## ####### ##  ##  ##  ##  ######    ##
# ##  ##  ##  ##   #####  ##       ##     ## # ## ##  ##  ##  ##  ##        ##
#  ####    ### ##  ##      ####   ####    ##   ##  ####    ### ##  ####    ####
#                 ####
#
#                       A Sega Model 3 Arcade Emulator.
#                Copyright 2011 Bart Trzynadlowski, Nik Henson


# ----------------------------- CONTROLLERS -------------------------------- #
# The rate at which analog control values increase/decrease
# when controlled by a key.  Valid range is 1-100, with 1
# being the least sensitive (slowest response) on default [25]
sensitivity=25

# Specifies the saturation, the position at which the joystick
# is interpreted as being in its most extreme position,
# as a % of the total range of the axis, from 0-200.
# A value of 200 means that the range will be halved, on default [100].
saturation=100

# Specifies the dead zone as a percentage,
# 0-99, of the total range of the axis.
# Within the dead zone, the joystick is inactive on default [2].
deadzone=2

# ------------------------------- AUDIO ------------------------------------- #

# Master Volume in %
sound-volume=100

# Set Music volume in %
music-volume=100

# Swap left and right audio channels
# [1] enable
# [0] disable
flip-stereo=0

# Relative front/rear balance in %
balance=0

# Disable sound board emulation sound effects
# [1] enable
# [0] disable
no-sound=0

# Disable Digital Sound Board MPEG music
# [1] enable
# [0] disable
no-dsb=0

# Select your sound engine:
# [0] New SCSP engine based on MAME [Default]
# [1] Legacy SCSP engine
sound-engine=0

# ------------------------------- VIDEO ------------------------------------- #

# set your graphics resolution automatically with [auto]
# or set on this format [1280,1024] or [none] if needed
resolution=auto

# Disable 60 Hz frame rate lock
# [1] disable
# [0] enable
no-throttle=0

# Use 8 texture maps for decoding
# [1] enable
# [0] disable
multi-texture=0

# Enable proper quad rendering
# [1] enable
# [0] disable
quad-rendering=0

# Show Crosshairs for gun games
# [0] disable
# [1] enable for 1 player
# [2] enable for 2 players
crosshairs=1

# -------------------------------- CORE ------------------------------------- #

# Set graphics multi threading in gpu or cpu
# [0] gpu-multi-threaded > Run graphics rendering in separate thread [Default]
# [1] no-gpu-thread > Run graphics rendering in main thread
# [2] no-threads > Disable multi-threading entirely
multi-threading=0

# The PowerPC frequency in MHz.
# The default is value [50] but [70] for best performance.
ppc-frequency=70

# Enable menu services on games [L3=test, R3=service]
# [1] enable
# [0] disable
service-button=1

# Log level information in Supermodel.log
# Default value : info
# add value : error
log-level=info
```

**Emplazamiento del fichero** : `/recalbox/share/system/configs/model3/ConfigModel3.ini`

### Opciones del core

## ![](/emulators/external-links.png) Enlaces externos

* **Foro Oficial** : [https://www.supermodel3.com/](https://www.supermodel3.com/)
* **Source Forge Oficial** : [https://sourceforge.net/projects/model3emu/](https://sourceforge.net/projects/model3emu/)