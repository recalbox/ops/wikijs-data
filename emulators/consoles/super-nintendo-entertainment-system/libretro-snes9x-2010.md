---
title: Libretro Snes9X 2010
description: 
published: true
date: 2024-07-24T20:20:35.272Z
tags: libretro, snes, super nintendo, snes9x 2010
editor: markdown
dateCreated: 2021-08-03T19:31:18.804Z
---

**Libretro Snes9X** is an upstream port of Snes9x, a portable Super Nintendo Entertainment System emulator to Libretro.

## ![](/emulators/license.svg) License

This core is under [**non-commercial**](https://github.com/libretro/snes9x/blob/master/LICENSE) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Features

| Feature | Supported |
| :---: | :---: |
| Restart | ✔ |
| Screenshots | ✔ |
| Saves | ✔ |
| States | ✔ |
| Rewind | ✔ |
| Netplay | ✔ |
| Core Options | ✔ |
| RetroAchievements | ✔ |
| RetroArch Cheats | ✔ |
| Controls | ✔ |
| Remapping | ✔ |
| Multi-Mouse | ✔ |
| Softpatching | ✔ |
| Crop Overscan | ✔ |

## ![](/emulators/bios.svg) BIOS

>**No bios is required.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Supported extensions

Roms must have the extension:

* .fig
* .gd3
* .sfc
* .smc
* .swc
* .zip
* .7z

This system supports compressed roms in .zip/.7z format. However, be careful, it is only an archive.

The files contained in the .zip/.7z must correspond to the extensions mentioned above.
Moreover, each .zip/.7z file must contain only one compressed rom.

### Location

Put the roms like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 snes
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.zip**

>The roms using **No-Intro** format are highly recommended.
{.is-success}

>For more information about roms, go to [this tutorial](./../../../tutorials/games/generalities/isos-and-roms)!
{.is-info}

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Accessing to options

You can configure various options in two different ways.

* Via RetroArch menu:

┣ 📁 RetroArch Menu
┃ ┣ 📁 Core options
┃ ┃ ┣ 🧩 Name_option

* Via the file `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core options

| Options | Options values | Variables (for configuration override) | Variables values |
| --- | --- | --- | --- |
| Console Region (Restart) | `Auto` ✅ / `NTSC` / `PAL` | `snes9x_2010_region` | `auto` / `ntsc` / `pal` |
| Preferred Aspect Ratio | `4:3` / `Uncorrected` / `Auto` ✅ / `NTSC` / `PAL` | `snes9x_2010_aspect` | `4:3` / `uncorrected` / `auto` / `ntsc` / `pal` |
| Set Autofire Pulse | `Medium` ✅ / `Fast` / `Slow` | `snes9x_2010_turbodelay` | `medium` / `fast` / `slow` |
| Blargg NTSC Filter | `Disabled` ✅ / `Monochrome` / `RF` / `Composite` / `S-Video` / `RGB` | `snes9x_2010_blargg` | `disabled` / `monochrome` / `rf` / `composite` / `s-video` / `rgb` |
| Frameskip | `Disabled` ✅ / `Auto` / `Manual` | `snes9x_2010_frameskip` | `disabled` / `auto` / `manual` |
| Frameskip Threshold (%) | `15` / `18` / `21` / `24` / `27` / `30` / `33` ✅ / `36` / `39` / `42` / `45` / `48` / `51` / `54` / `57` / `60` | `snes9x_2010_frameskip_threshold` | `15` / `18` / `21` / `24` / `27` / `30` / `33` / `36` / `39` / `42` / `45` / `48` / `51` / `54` / `57` / `60` |
| SuperFX Overclock | `5 MHz (Underclock)` / `8 MHz (Underclock)` / `9 MHz (Underclock)` / `10 MHz (Default)` ✅ / `11 MHz` / `12 MHz` / `13 MHz` / `14 MHz` / `15 MHz` / `20 MHz` / `30 MHz` / `40 MHz` | `snes9x_2010_overclock` | `5 MHz (Underclock)` / `8 MHz (Underclock)` / `9 MHz (Underclock)` / `10 MHz (Default)` / `11 MHz` / `12 MHz` / `13 MHz` / `14 MHz` / `15 MHz` / `20 MHz` / `30 MHz` / `40 MHz` |
| Reduce Slowdown (Hack, Unsafe) | `Disabled` ✅ / `Light` / `Compatible` / `Max` | `snes9x_2010_overclock_cycles` | `disabled` / `light` / `compatible` / `max` |
| Reduce Flickering (Hack, Unsafe) | `Disabled` ✅ / `Enabled` | `snes9x_2010_reduce_sprite_flicker` | `disabled` / `enabled` |
| Block Invalid VRAM Access (Restart) | `Enabled` ✅ / `Disabled` | `snes9x_2010_block_invalid_vram_access` | `enabled` / `disabled` |

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/libretro/snes9x2010/](https://github.com/libretro/snes9x2010/)
* **Libretro documentation**: [https://docs.libretro.com/library/snes9x_2010/](https://docs.libretro.com/library/snes9x_2010/)