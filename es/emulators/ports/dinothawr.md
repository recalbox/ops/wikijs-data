---
title: DinoThawr
description: 
published: true
date: 2024-07-26T08:28:49.874Z
tags: ports, dinothawr
editor: markdown
dateCreated: 2023-09-04T12:35:32.074Z
---

![](/emulators/ports/dinothawr.png){.align-center}

## Ficha técnica

* **Autor :** Themaister, Agnes Heyer 
* **Diseño de niveles :** Agnes Heyer 
* **Programación:** Hans-Kristian Arntzen 
* **Música:** Hans-Kristian Arntzen 
* **Diseño de algunos niveles:** Hans-Kristian Arntzen 
* **Género :** Puzzle

## Presentación 

**Dinothawr** es un juego de puzzle que consiste en empujar bloques a través de superficies deslizantes.

Nuestro héroe es un dinosaurio cuyos amigos están atrapados en el hielo. A través de puzzles, tu tarea es liberar a los dinosaurios de su prisión de hielo.

## Emuladores

[Libretro Dinothawr](libretro-dinothawr)