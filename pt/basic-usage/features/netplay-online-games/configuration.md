---
title: Preparação
description: 
published: true
date: 2025-02-06T23:53:14.324Z
tags: 
editor: markdown
dateCreated: 2021-07-21T00:32:01.862Z
---

## I - Preparando suas ROMs

Você deve seguir estes passos para ter suas ROMs prontas para o Netplay.

### A - Núcleo/Romsets compatíveis com Netplay Libretro

#### 1. Arcades

| Sistemas | Núcleo | Romset |
| :---: | :---: | :---: |
| **FinalBurn Neo** | Libretro FbNeo | 1.0.0.03 |
| **Mame** |  |  |
|  | Libretro Mame 2003 | Mame 0.78 |
|  | Libretro Mame 2003Plus | Mix de Mame 0.78 à 0.188 |
|  | Libretro Mame 2010 | Mame 0.139 |
| **Neo-geo** | Libretro FbNeo | 1.0.0.03 |

#### 2. Console de Fantasia

| Sistemas | Núcleo | Romset |
| :---: | :---: | :---: |
| **TIC80** | Libretro Tic80 |  |

#### 3. Consoles Domésticos

| Sistemas | Núcleo | Romset |
| :---: | :---: | :---: |
| **Atari 2600** | Libetro Stella | No Intro |
| **Famicom Disk System** |  | No-Intro |
|  | Libretro FCEUmm |  |
|  | Libretro Nestopia |  |
|  | Libretro Mesen |  |
| **Master System** |  | No-Intro |
|  | Libretro PicoDrive |  |
|  | Libretro Genesisplusgx |  |
| **Megadrive** |  | No-Intro |
|  | Libretro PicoDrive |  |
|  | Libretro Genesisplusgx |  |
| **Nintendo Entertainment System** |  | No-Intro |
|  | Libretro FCEUmm |  |
|  | Libretro FCEUNext |  |
|  | Libretro Nestopia |  |
|  | Libretro Quicknes |  |
|  | Libretro Mesen |  |
| **Super Nintendo** |  | No-Intro |
|  | Libretro Snes9x (activer l'option « multitap » |  |
|  | Libretro Snes9x_2002 |  |
|  | Libretro Snes9x_2005 |  |
|  | Libretro Snes9x_2010 |  |
| **Pc-Engine** |  | No-Intro |
|  | Libretro Mednafen_supergrafx |  |
|  | Libretro Mednafen_PCE_FAST |  |
| **Pc-Engine CD** |  | Redump |
|  | Libretro Mednafen_PCE_FAST |  |
|  | Libretro Mednafen_supergrafx |  |
| **PC-FX** | Libretro Mednafen_PCFX | Redump |
| **Sega 32x** | Libretro PicoDrive | No-Intro |
| **Sega CD** |  | Redump |
|  | Libretro PicoDrive |  |
|  | Libretro Genesisplusgx |  |
| **SG-1000** | Libretro blueMSX | No-Intro |
| **Supergrafx** | Libretro Mednafen_supergrafx | No-Intro |

#### 4. Ports

| Sistemas | Núcleo | Romset |
| :---: | :---: | :---: |
| **MR BOOM** | Libretro MrBoom | ? |

### B - Núcleo/Romsets compatíveis com Netplay Standalone (Autônomo)

#### Consoles Domésticos

| Sistemas | Núcleo | Romset |
| :---: | :---: | :---: |
| **GameCube** | Dolphin-emu | Redump |
| **Wii** | Dolphin-emu | Redump |

### C - Classificar e verificar seus romsets

As diferentes preferências de ordenação. O que escolher?

#### 1 - Nacional ou internacional?

* **Nacional:** Se você quiser jogar somente com pessoas do mesmo país que você: mantenha somente as ROMs de seu país **então, escolha as ROMs dos EUA e Japão para as que faltam.**
* **International :** Se você quiser jogar com pessoas de outros países: mantenha uma ROM de cada país.

#### 2 - Obtenção de um arquivo dat para o software de classificação

Baixe os arquivos dats de seus consoles para classificar (Link a vir em breve... ?).

#### 3 - O software de classificação

* Clrmamepro (**O melhor software*** mas requer prática antes da maestria, **indispensável para o arcade***)

[https://mamedev.emulab.it/clrmamepro/](https://mamedev.emulab.it/clrmamepro/)

* Romulus (**muito mais fácil de usar** mas não lida bem com o arcade)

[http://romulus.net63.net/#downloads](https://romulus.cc/#downloads)

Para descobrir o que significam os [códigos e tags nas ROMs](./../../../tutorials/games/generalities/tags-used-in-rom-names).

#### D - Fazendo Scrape em seus romsets

Há várias maneiras de fazer Scrape em seus romsets:

#### 1. Fazer Scrape com a ferramenta integrada do Recalbox

* Menu do EmulationStation (`START`)
* Sraper

#### 2. Fazer Scrape de suas ROMs com um software dedicado, para melhor personalização (Exemplo de Metadados: Hack de tradução da Região)

* **Skraper**

[https://www.skraper.net/](https://www.skraper.net/) (compatível com Windows/Linux e em breve macOS).

**Tutorial em vídeo do Skraper:**

[https://www.youtube.com/watch?v=vV8DxtdOCaU&index=15&list=PL2oNQ0AT7fx2ExiSNrfHUzga5GnogI4sh](https://www.youtube.com/watch?v=vV8DxtdOCaU&index=15&list=PL2oNQ0AT7fx2ExiSNrfHUzga5GnogI4sh)

#### E - Faça a verificação de Hash das suas ROMs

O **HASH** é uma impressão digital única para o **melhor reconhecimento** ao escolher um jogo (é um checksum CRC32 da ROM).

**O Hash é, portanto, obrigatório para o bom funcionamento do Netplay.**

* Menu do EmulationStation
* Configurações dos Jogos
* Configurações de Jogo em Rede
* Faça Hash das ROMs
* Iniciar

Entretanto, algumas ROMs não terão um Hash, este é o caso dos sistemas Arcade:

* Fba Libretro
* Mame
* Neo-Geo

>Alguns programas podem fazer hash e fazer scrape ao mesmo tempo, por exemplo o Skraper!
{.is-info}

## II- Configurando o sistema Netplay

### A - Ativação e configuração do Netplay

* No seu Recalbox :
  * Menu do EmulationStation
	* Configurações dos Jogos
	* Configurações de Jogo em Rede

![](/basic-usage/features/netplay/netplaypreparation-pt.png)

* Jogo em Rede
  * **ON/OFF**
* Apelido
  * Selecione para definir seu apelido

> Tenha cuidado para não colocar um acento no apelido, isso impedirá que o Netplay funcione.
{.is-warning}

* Porta
  * 55435 (Corresponde ao servidor da RetroArch)
* Servidor Relé Netplay (veja abaixo para mais explicações)
  * Nenhum
  * Nova York
  * Madri
* Senhas pré-definidas
  * Selecione para alterar as senhas pré-definidas.
* Faça Hash das ROMs - Você tem duas opções:
  * Filtro
    * Apenas hashs faltando
    * Todos os jogos
  * Sistemas
  	* Lista de sistemas para fazer hash

### B - Configure sua porta (UPNP/Abrir Porta/Servidor Relé Netplay)

>Alguns modems têm UPNP que permite a abertura/fechamento automático de portas.
{.is-info}

#### 1. Verifique se sua porta suporta UPnP

Recomendamos fortemente que você ative a opção UPNP.

* Comece a hospedar um jogo a partir de seu Recalbox.
* Em seu PC, vá para : [https://www.yougetsignal.com/tools/open-ports/](https://www.yougetsignal.com/tools/open-ports/)
* Entre na porta 55435 e pressione Verificar.
  * **a. A resposta é "positiva":** Você não precisa abrir a porta, vá para o passo B.
  * **b. A resposta é "negativa":** Ou seu modem não suporta UPNP ou esta opção precisa ser habilitada. Abra **uma porta manualmente** (recomendado) ou use um **Servidor Relé Netplay***.

#### 2. Abra sua porta (conexão direta)

É necessário abrir a porta 55435 para o lobby do Libretro e a porta 2626 para o Dolphin em seu modem de internet.

* Dependendo de seu provedor, a operação a ser feita é diferente. Verifique o site ou fóruns do seu provedor para obter mais informações.
  * O nome pode ser o que você quiser, mas ajuda se você o nomear exatamente como o software cujas portoas você está encaminhando. Lembre-se que você não pode usar um nome já usado por outra regra de exceção.
  * Em seguida, você escolherá o Protocolo, TCP ou UDP. Alguns roteadores permitem que você escolha um ou outro, enquanto outros lhe dão uma terceira opção para escolher os dois simultaneamente.
  * Você precisará de um ou do outro, ou de ambos. Se não for especificado, é melhor abrir as portas usando ambos os protocolos.
  * A seguir, você tem uma gama de portas. Nesta seção, observaremos a porta ou intervalo de portas que vamos abrir.
  * Se você estiver abrindo apenas uma porta, anote-a apenas na primeira caixa e deixe a outra em branco.
  * Se você estiver abrindo, por exemplo, 100 portas entre as portas 1500 e 1600, você escreveria 1500 na primeira caixa e 1600 na segunda.
  * Todo o resto pode ser ignorado com segurança. Você pode adicionar uma definição ou um tipo de aplicação, se desejar, mas não é necessário.
  * Se o roteador não permitir que você escolha ambos UDP e TCP ao mesmo tempo, você terá que fazer 2 entradas separadas para a mesma porta.
  * Quando terminar de adicionar sua porta, clique em Adicionar Definição ou Aceitar ou o que quer que seja que lhe permita salvar suas mudanças.
  * Alguns roteadores permitem que você crie regras de exceção, mas não as atribui automaticamente ao seu computador/dispositivo. Deve haver uma opção para fazer isso.

#### 3. A opção Servidor Relé Netplay

A opção Servidor Relé Netplay, permite que você não faça a abertura da porta do seu modem.  
Isto permite que ambos os lados da conexão sejam roteados através de um servidor intermediário.

Entretanto, isto terá um impacto na velocidade de sincronização dos dados e, portanto, na capacidade de resposta do jogo.

* Quando utilizá-lo

  * Seu roteador não suporta o protocolo UPnP (abertura e fechamento automático das portas).
  * Você não quer abrir suas portas.
  * Você tem dúvidas de como fazer:

  Ative a opção Servidor Relé Netplay.

> Recomenda-se abrir sua porta manualmente ou ter um modem que suporte UPnP, pois o servidor de relé adicionará latência extra.


>RetroArch não verifica se suas portas estão abertos, nem o servidor do lobby Libretro.  
>Portanto, certifique-se de abrir sua porta corretamente ou ative o servidor de relé, caso contrário, os usuários não poderão se conectar à sua sessão.
{.is-warning}



