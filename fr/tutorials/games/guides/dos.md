---
title: DOS
description: 
published: true
date: 2023-02-26T17:48:16.322Z
tags: dos, msdos, pc
editor: markdown
dateCreated: 2021-06-15T23:23:57.261Z
---

Vous aurez ici quelques tutoriels concernant DOS.

Voici les tutoriels disponibles :

[DOSbox pour émuler des jeux DOS](dosbox-to-play-with-dos-games)
[Jouer aux jeux Windows 95 & 98 avec Dosbox Pure](play-win-95-98-games-dosbox-pure)
[Utilisation de la souris avec la manette dans DOSBox Pure](mouse-use-with-controller-in-dosbox-pure)