---
title: Libretro Mednafen_Lynx
description: 
published: true
date: 2024-06-22T11:10:44.362Z
tags: libretro, atari, lynx, mednafen
editor: markdown
dateCreated: 2021-05-21T08:29:08.245Z
---

**Libretro Mednafen\_lynx** est un émulateur de système de jeu vidéo Atari Lynx.
Plus précisément, c'est un port de Mednafen Lynx qui est un fork de Handy.

## ![](/emulators/license.svg) Licence

Ce core est sous licence [**zlib**](https://github.com/libretro/beetle-lynx-libretro/blob/master/mednafen/lynx/license.txt) et [**GPLv2**](https://github.com/libretro/beetle-lynx-libretro/blob/master/COPYING).

## ![](/emulators/compatibility.png) Compatibilité

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Fonctionnalités

| Fonctionnalité | Supporté |
| :---: | :---: |
| Redémarrage | ✔ |
| Captures d'écran | ✔ |
| Sauvegardes instantanées | ✔ |
| Rembobinage | ✔ |
| Netplay | ✔ (pas d'émulation de câble link) |
| Options du core | ✔ |
| RetroAchievements | ✔ |
| Cheats (Menu de cheats) | ✔ |
| Controls | ✔ |
| Remapping | ✔ |
| Softpatching | ✔ |
| Crop Overscan | ✔ |

## ![](/emulators/bios.svg) BIOS

### Liste des bios obligatoires

| Nom de fichier | Description | MD5 | Fourni |
| :---: | :---: | :---: | :---: |
| lynxboot.img | Image Boot Lynx | fcd403db69f54290b51035d82f835e7b | ❌  |

### Emplacement

Placez le BIOS comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 lynx
┃ ┃ ┃ ┃ ┣ 🗒 **lynxboot.img**

## ![](/emulators/roms.png) Roms

### Extensions supportées

La rom doit avoir l'extension :

* .lnx
* .o
* .zip
* .7z

Ce système supporte les roms compressées au format .zip/.7z. Attention toutefois, il ne s'agit que d'une archive.

Les fichiers contenus dans les .zip/.7z doivent correspondre aux extensions citées précédemment.
De plus, chaque fichier .zip/.7z ne doit contenir qu'une seule rom compressée.

### Emplacement

Placez les roms comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 lynx
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.zip**

>Les roms au format **No-Intro** sont vivement conseillées.
{.is-success}

>Pour plus d'information sur les roms, rendez-vous sur [ce tutoriel](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuration avancée de l'émulateur

>Pour pouvoir conserver vos configurations personnalisées lors d'une mise à jour, nous vous conseillons d'utiliser notre fonctionnalité [Surcharges de configuration](./../../../advanced-usage/configuration-override).
{.is-info}

### Accéder aux options

Vous pouvez configurer diverses options de deux façons différentes.

* Via le menu RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via le fichier `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Options du core

| Options | Valeurs des options | Variable (pour surcharge) | Valeurs des variables |
| --- | --- | --- | --- |
| Display rotation | `Activé` ✅ / `Désactivé` | `lynx_rot_screen` | `enabled` / `disabled` |
| Color Format (Restart Required) | `16-Bit (RGB565)` ✅ / `32-Bit (RGB8888)` | `lynx_pix_format` | `16` / `32` |
| Force 60Hz | `Désactivé` ✅ / `Activé` | `lynx_force_60hz` | `disabled` / `enabled` |

## ![](/emulators/external-links.png) Liens externes

* **Code source utilisé** : [https://github.com/libretro/beetle-lynx-libretro/](https://github.com/libretro/beetle-lynx-libretro/)
* **Documentation Libretro** : [https://docs.libretro.com/library/beetle\_lynx/](https://docs.libretro.com/library/beetle_lynx/)
* **Code source officiel** : [https://mednafen.github.io/releases/](https://mednafen.github.io/releases/)
* **Site officiel** : [https://mednafen.github.io/](https://mednafen.github.io/)