---
title: Screens
description: 
published: true
date: 2022-01-22T22:58:40.707Z
tags: compatibility, screens
editor: markdown
dateCreated: 2021-08-05T22:01:25.700Z
---

### TFT HDMI screen

| Device | Size | Resolution | Status | Comments |
| :---: | :---: | :---: | :---: | :---: |
|  | 5" | 800x480 | ✅ | [banggood](https://www.banggood.com/5-Inch-800-x-480-HDMI-TFT-LCD-Touch-Screen-For-Raspberry-PI-2-Model-B-B-A-B-p-1023438.html) |
| PI-TOP CEED | 14" | 1366×768 | ✅ | Works but no sound. Need to install "pt-speaker" package [Source](https://www.neozone.org/tests/test-pi-top-ceed-lordi-ideal-pour-les-makers-en-herbe/) |

### TFT SPI Bus screen

| Device | Size | Resolution | Status | Comments |
| :---: | :---: | :---: | :---: | :---: |
|  |  |  |  |  |

### TFT DPI Bus screen

| Device | Size | Resolution | Status | Comments |
| :---: | :---: | :---: | :---: | :---: |
|  |  |  |  |  |

### TFT panel with Display Controller

| Device | Size | Resolution | Status | Comments |
| :---: | :---: | :---: | :---: | :---: |
| Assus N71Jq Laptop panel | 17.3" | 1600x900 | ✅ | Recycled Asus N71JQ LCD panel connected on LCD Display Controller (*HDMI VGA DVI Audio LCD Display Controller Board for LP173WD1 N173FGE B173RW01 15.6" 17.3" 1600x900 LED LCD Screen*) using a 30pins LVDS connector   |