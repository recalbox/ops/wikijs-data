---
title: FAQ
description: 
published: true
date: 2023-09-19T13:59:23.001Z
tags: faq
editor: markdown
dateCreated: 2023-09-19T13:59:23.001Z
---

## ¿Dónde puede encontrar los juegos?

* Algunos juegos pueden descargarse directa y legalmente utilizando la herramienta DaphneLoader de la [distribución Windows de Daphne](http://www.daphne-emu.com/site3/index_hi.php).
* Para otros (Dragon's Lair, Dragon's Lair II, Space Ace, etc.), es necesario demostrar que disponéis de una licencia válida (versión DVD, por ejemplo).

Una vez descargados, copiad los ficheros necesarios (las ROM y los ficheros de imagen de laserdisc) en vuestra Recalbox, como se explica en las 2 páginas siguientes:

* [Jugar a los juegos Daphné](./play-daphne-games)
* [Jugar a los juegos Singe](./play-singe-games)

## ¿Cómo salgo del juego?

Para salir del juego, sólo tienes que utilizar el botón `Hotkey`.

Sin embargo, como se trata a la base de un sistema arcade, hay que utilizar el botón `Select` para añadir créditos. ¿Qué pasa entonces si el botón `Hotkey` está en `Select`?

En los mandos sin botón "Hotkey" dedicado, podéis utilizar el botón "R1" para salir del juego y seguir usando el botón "Select" para añadir créditos.

## Configuración de base

Configuración predeterminada requerida para el buen funcionamiento de los juegos.

### ¿Qué es el fichero `.commands`?

Como se ha explicado anteriormente, hay que crear este fichero en el directorio del juego, con el mismo nombre que el archivo ROM pero con la extensión `.commands`. Permite pasar parámetros adicionales al emulador.

La mayoría de las veces, contiene la opción `-bank`, que define la configuración del juego (también conocida como interruptor DIP):

* Número de créditos para una partida
* Número de vidas por crédito
* Nivel de dificultad
* Nivel de dificultad

>Los valores recomendados para los juegos más comunes se describen a continuación, pero también podéis consultar el sitio [LaserDisc Game Tech Center] (http://www.dragons-lair-project.com/tech/) para obtener información más detallada.
>
>Tened en cuenta que, para un mismo juego, los interruptores DIP pueden variar de una versión de ROM a la otra. También son posibles otros ajustes. Consultad [Daphne's wiki](http://www.daphne-emu.com/mediawiki/index.php/CmdLine) para más detalles.
{.is-info}

Ejemplo para Dragon's Lair (DLE 2.x):
  
`-bank 1 00110111 -bank 0 10011000`   
  
[Esta página](http://www.dragons-lair-project.com/tech/dips/dle20.asp) nos indica que:

* El sonido está siempre presente, con la voz del narrador (incluso fuera de un juego)
* 1 moneda = 1 crédito
* 1 crédito = 5 vidas
* Sin pruebas ni diagnósticos
* Modo de juego estándar

### Juegos compatibles y configuración recomendada

Para cada juego, la siguiente tabla describe:

* El nivel de compatibilidad con el emulador. 
* El fichero ROM recomendado para obtener la mejor experiencia. 
* El contenido recomendado del fichero `.commands`: para configurar una dificultad normal, 5 vidas, crédito mínimo necesario, etc...

| Nombre del juego | Compatibilidad | ROM recomendada | commands |
| :--- | :--- | :--- | :---: |
| Astron Belt | ⭐️⭐️ | astron.zip | `-bank 0 00000000 -bank 1 01000000` |
| Badlands | ⭐️ | badlands.zip | `-bank 0 00000000 -bank 1 10000010` |
| Bega's Battle | ⭐️ | bega.zip | `-bank 0 00000000 -bank 1 00000001` |
| Cliff Hanger |  | cliff.zip | `-bank 0 00000000 -bank 1 01000000 -bank 2 00000000 -bank 3 01111000` |
| Cobra Command | ⭐️⭐️ | cobraconv.zip | `-bank 0 11010000 -bank 1 00010001` |
| Dragon's Lair | ⭐️ | dle21.zip | `-bank 1 00110111 -bank 0 10011000` |
| Dragon's Lair II | ⭐️⭐️ | lair2.zip |  |
| Esh's Aurunmilla | ⭐️⭐️ | esh.zip |  |
| Galaxy Ranger | ⭐️ | galaxy.zip | `-bank 0 00000000 -bank 1 00000000` |
| GP World | ⭐️ | gpworld.zip |  |
| Interstellar Laser Fantasy | ⭐️ | interstellar.zip | `-bank 0 00100001 -bank 1 00000000` |
| M.A.C.H. 3 | ⭐️ | mach3.zip | `-bank 0 01000100` |
| Road Blaster | ⭐️ | roadblaster.zip | `-bank 0 00000000 -bank 1 00000000` |
| Space Ace | ⭐️⭐️ | sae.zip | `-bank 1 01100111 -bank 0 10011000` |
| Super Don Quix-Ote | ⭐️ | sdq.zip | `-bank 0 00100001 -bank 1 00000000` |
| Thayer's Quest |  | tq.zip | `-bank 0 00010000` |
| Us Vs Them | ⭐️ | uvt.zip | `-bank 0 00000000` |

## Botones del joystick

>El emulador sólo soporta un joystick, para el jugador 1.
{.is-info}

* Utilizad el stick izquierdo para moveros y los botones estándar para realizar acciones.

> En la mayoría de los juegos, sólo se utiliza el botón B (A en los mandos de Xbox, X en los de Playstation).
{.is-info}

## Overlays

* ¿Cómo puedo añadir overlays?

Desde la versión de Recalbox 9.1, se pueden añadir overlays a los juegos Daphne. Cread un directorio llamado `bezels` en vuestra carpeta de juegos Daphne y colocad en él los correspondientes overlays con el mismo nombre que el juego. Por ejemplo, el juego llamado `lair.daphne` tendrá su overlay llamado `lair.png`.

![](/tutorials/games/guides/daphne/ace.png)

![](/tutorials/games/guides/daphne/lair.png)