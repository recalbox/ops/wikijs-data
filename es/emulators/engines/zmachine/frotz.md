---
title: Frotz
description: 
published: true
date: 2024-07-14T19:22:50.207Z
tags: 9.0+, z-machine, frotz
editor: markdown
dateCreated: 2023-09-11T06:41:03.823Z
---

**Frotz** es un intérprete de juegos Infocom y de otros juegos desarrolados para la **Z-máquina**.

## ![](/emulators/license.svg) Licencia

El core posee la siguiente licencia [**GPL-2.0+**](https://gitlab.com/DavidGriffith/frotz/-/blob/master/COPYING).

## ![](/emulators/compatibility.png) Compatibilidad

 | RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | 
 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Funcionalidades

| Funcionalidad | ¿Soportada? |
| :---: | :---: |
| Grabaciones | ✔ |

## ![](/emulators/bios.svg) BIOS

>**No necesita bios.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Extensiones de roms soportadas

Las roms deben tener las extensiones siguientes:

* .dat
* .zip
* .z1
* .z2
* .z3
* .z4
* .z5
* .z6
* .z7
* .z8

### Emplazamiento

Disponed vuestras roms como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 zmachine
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.zip**

## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

> **Este core no tiene opciones.**
{.is-success}

## ![](/emulators/external-links.png) Enlaces externos

* **Código fuente utilizado** : [https://gitlab.com/DavidGriffith/frotz/](https://gitlab.com/DavidGriffith/frotz/)