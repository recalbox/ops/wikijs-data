---
title: Libretro MAME 2000
description: 
published: true
date: 2024-07-24T06:56:28.060Z
tags: libretro, mame, mame2000
editor: markdown
dateCreated: 2021-07-05T23:16:08.252Z
---



## ![](/emulators/license.svg) License

This core is under **MAME non-commercial** license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ❌ | ❌ | ❌ | ❌ | ❌ | ❌ |

## ![](/emulators/features.png) Features

| Feature | Supported |
| :---: | :---: |
| Saves | ✔ |
| States | Game-dependent |
| Rewind | ✔ |
| Netplay | ✔ |
| Native Cheats | ✔ |
| Controllers | ✔ |
| Multi-Mouse | ✔ |

## ![](/emulators/bios.svg) BIOS

BIOS romsets are not required when using "Full Non-Merged" arcade romsets. For "Split" and "Non-Merged" romsets, place the necessary BIOS in the same directory as the game romset.

>**Note**:
>Place the bios in the directory: `/recalbox/share/roms/mame/`   
>or in its subdirectory if you use several Mame cores, for example: `/recalbox/share/roms/mame/Mame2000/`
>Games and their bios must be in the same directory or sub-directory.
{.is-warning}

## ![](/emulators/roms.png) Roms

* Based on romset: MAME 0.37b5 (July 2000)
* DatFile: You can find the dat file in your recalbox at location `/recalbox/share/arcade/libretro/mame2000.dat`.

### Location

Put your roms like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 mame
┃ ┃ ┃ ┃ ┣ 🗒 **game.zip**
┃ ┃ ┃ ┃ ┣ 📁 **game**
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **game.chd**

You can also opt for a subdirectory (useful if you want to have a mame set for another core).

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 mame
┃ ┃ ┃ ┃ ┣ 📁 Mame 2000
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **game.zip**
┃ ┃ ┃ ┃ ┃ ┣ 📁 **game**
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 **game.chd**

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Accessing to options

You can configure various options in two different ways.

* Via RetroArch menu:

┣ 📁 RetroArch Menu
┃ ┣ 📁 Core options
┃ ┃ ┣ 🧩 Name_option

* Via the file `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core options

| Options | Options values | Variables (for configuration override) | Variables values |
| --- | --- | --- | --- |
| Frameskip | `Disabled` ✅ / `auto` / `threshold` | `mame2000-frameskip` | `disabled` / `auto` / `threshold` |
| Frameskip Threshold (%) | `30` ✅ / `40` / `50` / `60` | `mame2000-frameskip_threshold` | `30` / `40` / `50` / `60` |
| Frameskip Interval | `1` ✅ / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` | `mame2000-frameskip_interval` | `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` |
| Skip Disclaimer | `Enabled` ✅ / `Disabled` | `mame2000-skip_disclaimer` | `enabled` / `disabled` |
| Show Game Information | `Disabled` ✅ / `Enabled` | `mame2000-show_gameinfo` | `disabled` / `enabled` |
| Audio Rate (Restart) | `11025` / `22050` ✅ / `32000` / `44100` | `mame2000-sample_rate` | `11025` / `22050` / `32000` / `44100` |
| Stereo (Restart) | `Enabled` ✅ / `Disabled` | `mame2000-stereo` | `enabled` / `disabled` |

## ![](/emulators/external-links.png) External links

* **Progettosnaps**: [https://www.progettosnaps.net/](https://www.progettosnaps.net/)
* **Source code used**: [https://github.com/libretro/mame2000-libretro/](https://github.com/libretro/mame2000-libretro/)