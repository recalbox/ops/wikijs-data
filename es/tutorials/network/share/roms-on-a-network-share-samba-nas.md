---
title: Cargar roms a través de una carpeta compartida con Samba (NAS)
description: 
published: true
date: 2023-09-13T14:14:48.121Z
tags: nas, red, compartidas, carpetas
editor: markdown
dateCreated: 2023-09-13T14:13:34.003Z
---

Con Recalbox, podéis almacenar vuestros ficheros como roms, bios, saves, themes, etc. en un servidor de almacenamiento en red, también conocido como NAS (Network Attached System).

A continuación os mostramos cómo configurar el sistema desde el NAS y desde Recalbox, todo ello utilizando el protocolo Samba.

>¡El NAS y vuestra Recalbox deben estar conectados vía Ethernet para una mejor estabilidad y rendimiento!
{.is-warning}

## En el NAS

El procedimiento que sigue ha sido probado en un servidor Synology, pero el funcionamiento será equivalente para todos los tipos de servidor.

En el NAS:

* Cread una carpeta compartida (llamémosla « /roms/ »).
* Cread un usuario con los derechos de lectura/escritura sobre esta carpeta (o reutilizad un usuario existente asignándole los permisos adecuados).
* Recread la misma jerarquía de subcarpetas como las que existen en vuestra Recalbox (a través de la red, vía FTP o a la mano. Lo importante es tener la misma jerarquía, es decir un directorio por cada máquina emulada).
  
Es posible utilizar un PC con windows para hacer exactamente lo mismo:

* Podéis crear una carpeta compartida.
* Podéis montarla de igual forma como se explicará más adelante.

El único inconveniente es que el PC que sirve de NAS deberá estar siempre encendido para que se pueda acceder à el desde vuestra Recalbox.

## En vuestra Recalbox

* Montad la partición de inicio en modo [lectura/escritura](./../../system/access/remount-partition-with-write-access).
* Modificad el fichero `recalbox-boot.conf` usando vuestro editor de texto favorito. Se puede acceder a este fichero de varias formas:
  * Si vuestra carta SD se encuentra montada en vuestro PC, encontraréis un lector llamado `RECALBOX`
  * Si utilizáis [SSH](./../../system/access/root-access-terminal-cli#ssh), el fichero se encuentra dentro del directorio '/boot'.
* Reemplazad la siguiente línea:

```ini
;sharedevice=INTERNAL
```

* Por esta otra :

```ini
sharedevice=NETWORK
```

Esto indica a vuestra Recalbox que el contenido de la carpeta `/recalbox/share` se encuentra en una carpeta compartida en red (y no en la tarjeta SD).

>Vérificad que habéis suprimido el `;` al principio de la línea para activarla.
>{.is-info}

### Especificación de los directorios remotos

Es necesario especificar, dentro del fichero `recalbox-boot.conf`, las informaciones de conexión 
al servidor de almacenamiento en red (NAS). Para ello, hay dos opciones:

### {.tabset}

#### El comando que engloba todo

```ini
sharenetwork_<nfs|smb><[0-9]>=<SHARE|ROMS|SAVES|BIOS>@<NAS>:<répertoire partagé>:<options>
```

##### Ejemplo

```ini
sharenetwork_smb1=ROMS@192.168.0.1:recalbox/roms:username=recalbox,password=recalbox,vers=2.0
sharenetwork_smb2=SAVES@192.168.0.1:recalbox/saves:username=recalbox,password=recalbox,vers=2.0
```

La lista de carpetas que pueden especificarse se encuentra al final de esta página.

#### El comando para cada directorio

```ini
sharenetwork_cmd<[0-9]>=<commande à exécuter>
```

##### Ejemplo

```ini
sharenetwork_cmd1=mount -o port=2049,nolock,proto=tcp 192.168.0.1:/Documents/recalbox /recalbox/share
```

La lista de carpetas que pueden especificarse se encuentra al final de esta página.

## Configuración con SMB v2 o superior

Con SMB version 2 o superior, hace falta precisar la versión del protocolo que queréis utilizar. Por ejemplo para la versión 2.0:

```ini
sharenetwork_smb1=ROMS@192.168.0.1:recalbox/roms:username=recalbox,password=recalbox,vers=2.0
sharenetwork_smb2=SAVES@192.168.0.1:recalbox/saves:username=recalbox,password=recalbox,vers=2.0
```

### Versiones de Windows mínimas requeridas para Samba

* 1.0: Win 2k/XP/Server 2003/Server 2003 R2
* 2.0: Vista SP1/Server 2008
* 2.1: Win7/Server 2008 R2
* 3.0: Win8/Server 2012
* 3.02: Win8.1/Server 2012
* 3.11: Win10/Server 2016

### Algunos ejemplos

Aquí tenéis los montajes posibles:

### {.tabset}

#### El comando que engloba todo

```ini
sharenetwork_<nfs|smb><[0-9]>=<SHARE|ROMS|SAVES|BIOS|MUSIC|OVERLAYS|SCREENSHOTS|SHADERS|SCRIPTS>@<NAS>:<répertoire partagé>:<options>
```

#### El comando para cada directorio

```ini
sharedevice=NETWORK
sharewait=30
# TOUT LE DOSSIER SHARE EN ENTIER
sharenetwork_smb1=SHARE@<IP_DU_NAS>:<CHEMIN_NAS/SHARE>:username=<ICI_LOGIN>,password=<ICI_PASSWORD>,vers=3.0
```

## Configurar sólo determinadas carpetas

Como habéis visto más arriba, es posible seleccionar una o varias (o todas) de las siguientes carpetas:

* BIOS
* MUSIC
* OVERLAYS
* ROMS
* SAVES
* SCREENSHOTS
* SHADERS
* SCRIPTS

### Ejemplo

```ini
sharedevice=NETWORK
sharewait=30
#UNIQUEMENT UNE SELECTION DES DOSSIER
sharenetwork_smb1=ROMS@<IP_DU_NAS>:<CHEMIN_NAS/ROMS>:username=<ICI_LOGIN>,password=<ICI_PASSWORD>,vers=3.0
sharenetwork_smb2=SAVES@<IP_DU_NAS>:<CHEMIN_NAS/SAVES>:username=<ICI_LOGIN>,password=<ICI_PASSWORD>,vers=3.0
sharenetwork_smb3=BIOS@<IP_DU_NAS>:<CHEMIN_NAS/BIOS>:username=<ICI_LOGIN>,password=<ICI_PASSWORD>,vers=3.0
sharenetwork_smb4=MUSIC@<IP_DU_NAS>:<CHEMIN_NAS/MUSIC>:username=<ICI_LOGIN>,password=<ICI_PASSWORD>,vers=3.0
sharenetwork_smb5=OVERLAYS@<IP_DU_NAS>:<CHEMIN_NAS/OVERLAYS>:username=<ICI_LOGIN>,password=<ICI_PASSWORD>,vers=3.0
sharenetwork_smb6=SCREENSHOTS@<IP_DU_NAS>:<CHEMIN_NAS/SCREENSHOTS>:username=<ICI_LOGIN>,password=<ICI_PASSWORD>,vers=3.0
sharenetwork_smb7=SHADERS@<IP_DU_NAS>:<CHEMIN_NAS/SHADERS>:username=<ICI_LOGIN>,password=<ICI_PASSWORD>,vers=3.0
sharenetwork_smb8=SCRIPTS@<IP_DU_NAS>:<CHEMIN_NAS/SCRIPTS>:username=<ICI_LOGIN>,password=<ICI_PASSWORD>,vers=3.0
```

Cada punto de montaje puede apuntar a un NAS diferente, si lo necesitáis.

>
>Para los overlays, hay que elegirlos en función de vuestra resolución de pantalla. 
{.is-info}

```ini
sharedevice=NETWORK
sharewait=30

#FULL ROMS
sharenetwork_smb1=ROMS@192.168.1.112:C/RCB/romsfull:username=USER,password=PASS,vers=3.0
#ROMS SELECTION
#sharenetwork_smb1=ROMS@192.168.1.112:C/RCB/roms:username=USER,password=PASS,vers=3.0
#ROMS POUR TESTS
#sharenetwork_smb1=ROMS@192.168.1.110:TEST/roms:username=USER,password=PASS+,vers=3.0

sharenetwork_smb2=SAVES@192.168.1.112:C/RCB/saves:username=USER,password=PASS,vers=3.0
sharenetwork_smb3=BIOS@192.168.1.112:C/RCB/bios:username=USER,password=PASS,vers=3.0
sharenetwork_smb4=SCREENSHOTS@192.168.1.112:C/RCB/screenshots:username=USER,password=PASS,vers=3.0

#720p pour Pi0 Pi2 Pi3
#sharenetwork_smb5=OVERLAYS@192.168.1.112:C/RCB/overlays720:username=USER,password=PASS,vers=3.0
#1080p pour Pi4 XU4 et PC en 1080p
sharenetwork_smb5=OVERLAYS@192.168.1.112:C/RCB/overlays1080:username=USER,password=PASS,vers=3.0
```

>Por motivos de seguridad y de obsolescencia, se aconseja desactivar el soporte de SMB v1 en vuestro ordenador Windows !
{.is-warning}

### Enlaces externos

* [http://www.zdnet.com/article/windows-10-tip-stop-using-the-horribly-insecure-smbv1-protocol/](http://www.zdnet.com/article/windows-10-tip-stop-using-the-horribly-insecure-smbv1-protocol/)
* [https://www.howtogeek.com/321072/how-to-disable-smbv1-and-protect-your-windows-pc-from-attack/](https://www.howtogeek.com/321072/how-to-disable-smbv1-and-protect-your-windows-pc-from-attack/)
* [https://www.spamtitan.com/blog/stop-smbv1-ransomware-attacks/](https://www.titanhq.fr/blog/comment-arreter-attaques-ransomware-smbv1/)
* [https://www.bostonhelpdesk.com/disabling-smbv1-one-defense-against-wanna-cry-ransomware/](https://www.bostonhelpdesk.com/disabling-smbv1-one-defense-against-wanna-cry-ransomware/)