---
title: First use and configuration
description: To configure your Recalbox system for the first time
published: true
date: 2024-04-19T16:41:56.276Z
tags: configuration, first use
editor: markdown
dateCreated: 2021-06-28T21:05:54.010Z
---

## First steps

### First start

After installation, the first thing to do is to connect your box with Recalbox to your TV via the HDMI cable. To power and start your Recalbox, simply plug in the micro USB or USB-C power cable.

>Many controllers work directly when you first use Recalbox. However, if you want to configure a USB controller directly, connect a USB keyboard, ... please see the [Configuring a controller](#configuring-a-controller) section.
{.is-info}

### Set Recalbox to your language

On the system selection screen, press `START`. Choose `SYSTEM SETTINGS` > `LANGUAGE`, select your language and go back. Recalbox will tell you that it must restart. Once it has finished booting up, the menus will be completely in your language.

### Stop Recalbox

On the system selection screen, press `SELECT` and choose `SHUTDOWN SYSTEM`. Wait until the Pi's green/orange LED stops flashing so as not to damage the microSD card, and once it stops flashing, unplug the power cable.

### Default games

Recalbox comes with a few royalty free games (homebrew) and/or demos for several systems.

## Configuring a controller

### Add and configure an USB controller

You can add USB controllers to Recalbox. Most models are compatible (see the [compatibility list](./../hardware-compatibility/compatible-devices/controllers)).

After connecting your USB controller (or pairing your Bluetooth controller), press `START` with a previously configured controller. Select `CONTROLLER SETTINGS` and follow the instructions.

![](/basic-usage/first-use-and-configuration/padconfig1.png)

Select `CONFIGURE A CONTROLLER` and follow the instructions.

![](/basic-usage/first-use-and-configuration/padconfig2.png)

Press a key to start the configuration.

![](/basic-usage/first-use-and-configuration/padconfig3.png)

Hold down any button on the controller to configure it.

![](/basic-usage/first-use-and-configuration/padconfig4.png)

Now you can assign buttons to your controller. The names used to assign the buttons are those of the Super Nintendo controller:

![Super Nintendo (SNES) controller](/basic-usage/first-use-and-configuration/snespad.jpg)

>The L1, R1, L2, R2, L3 and R3 buttons are based on the Playstation controller.
{.is-info}

>**HOTKEY**
>
>The last button to configure, **HOTKEY**, is the one that will be used to activate the [special-commands](./getting-started/special-commands) when you are in an emulator:
>
>For Xbox 360 and Xbox One controllers, the **Hotkey** button is assigned to the `HOME` button.
>For PS3 and PS4 controllers, the **Hotkey** button is assigned to the `PS` button.
>
>If there is no dedicated button or one has not yet been assigned, it is recommended that you assign **Hotkey** to the `Select` button.
{.is-warning}

>To skip any button (except **Hotkey**), press the `Down` key to move to the next button.
{.is-info}

>For 6-button controllers (SNES, arcade, ...), the buttons are assigned in correspondence with the SNES controller (see above).
>For 2-button controllers (NES, PC Engine, Gameboy, ...), the assigned buttons are `A` and `B`.
{.is-warning}

![](/basic-usage/first-use-and-configuration/padconfig5.png)

![](/basic-usage/first-use-and-configuration/padconfig6.png)

### Assigning the controller

* Back on the setup screen, you can assign the controller to a player.
* Select `INPUT P1` to assign a controller.

![](/basic-usage/first-use-and-configuration/padassign1.png)

* Select a controller.

![](/basic-usage/first-use-and-configuration/padassign2.png)

* Your controller is now configured.

![](/basic-usage/first-use-and-configuration/padassign3.png)

### Using a keyboard

If you can't configure your controller, you can connect a wired USB keyboard to your Recalbox to configure it. You can find all the key combinations of the keyboard [here](./../tutorials/others/emulationstation-use-with-keyboard-and-mouse).

### PS3 Controller

To **link** a PS3 controller:

* First connect the controller to the USB port (via a suitable cable) and wait 10 seconds.
* After that, you can disconnect the controller and press the **PS** button to start the wireless connection.

>**Note:**  
>For Asian copies of PS3 Dualshock 3 (like GASIA or SHANWAN), you will need to adjust [some settings](./../tutorials/controllers/controllers/ps3-controllers-drivers).
{.is-warning}

>**Warning :**  
>If you have any doubts about the power supply and power consumption, it is better to avoid loading the PS3 controller on the Raspberry Pi because it can cause stability problems.
>
>Please connect the controller to the Raspberry Pi only to associate your controller with your Recalbox.
{.is-danger}

If you understand the settings involved or want to use your controller with a USB connection, you need to disable the PS3 bluetooth driver in recalbox.conf by finding and editing the line `controllers.ps3.enabled=0`.

Remember that the controller configuration in Recalbox is based on the SNES button configuration:

| PS3 controller | SNES controller |
| :---: | :---: |
| X | B |
| ◯ | A |
| ⬜ | Y |
| △ | X |

>**Information:**  
>By default, **HOTKEY** is associated with the **PS** button (the one in the middle of the joystick).  
>For more information on **HOTKEY**, see the [special-commands](./getting-started/special-commands) page.
{.is-info}

### Xbox 360 controller 

>**Note:**  
>Xbox 360 wireless controllers need a specific wireless receiver dongle.
{.is-warning}

Remember that the controller configuration in Recalbox is based on the SNES button configuration:

| Xbox 360 Controller | SNES Controller |
| :---: | :---: |
| A | B |
| B | A |
| X | Y |
| Y | X |

>**Information :**  
>By default, **HOTKEY** is associated with the **HOME** button (the one in the middle of the joystick).  
>For more information on **HOTKEY,** see the [special-commands](./getting-started/special-commands) page.
{.is-info}

### Add a Bluetooth controller

To add a Bluetooth controller:

* Put your controller in _pairing_ mode.
* Then press `START` and choose `CONTROLLER SETTINGS`.
* Then `PAIR A BLUETOOTH CONTROLLER`.

A **list of detected handsets** appears!

* Just select yours and your controller is paired.
* You can now proceed to the configuration (if the controller is not already supported by Recalbox).

>**Information:**  
>For **8bitdo** controllers, please see the [**8bitdo on Recalbox**](./../hardware-compatibility/compatible-devices/8bitdo-on-recalbox) page.
{.is-info}

### GPIO controller

You can connect your arcade buttons and joysticks directly to the Raspberry Pi's GPIOs (see the page [**GPI controllers**](./../tutorials/controllers/gpio/gpio-controllers)). You can also connect original controllers from PSOne, Nes, Snes, Megradrive, ... (see **DB9 controllers** and **Gamecon controllers**).

### Virtual controllers

>Virtuals controllers are not accessible on Raspberry Pi0 et Pi1 by default.
{.is-info}

With the Miroof Virtual Gamepads project, you can add up to 4 controllers with your smartphones/tablets !

To do this, start your smartphone's web browser and type the IP address of your Recalbox followed by the communication port (port `:8080`). 

>**Information:**  
>You can find the IP address of your Recalbox in the `NETWORK OPTIONS` menu.
{.is-info}

![Virtual Controller Touch Zones](/basic-usage/first-use-and-configuration/virutalgamepad_touch_zones.png)

## Using external storage

>**Information:**  
>You can very easily **use a USB storage device** (USB key, external self-powered hard drive etc.) to store your roms, personal files etc...
>
>**With this method**, the **system** (on the SD card) and the **share partition** (on the device) **are separated**.
>  
>So **if you were to reinstall your system**, you would **keep all your user data**.
>All you have to do then is **connect your device** to your Recalbox, then **select it in the system, and play.**
{.is-info}

### The format of your media

First, you must use **a device that uses the following file system**: **FAT32, EXFAT, EXT4 or NTFS**.

>**Note:**
>
>It is highly recommended to use the **EXFAT** file system.
{.is-info}

>**Caution:**
>
>Transfer rates can be slow** in the case of **NTFS**.
>Also be sure that the file system you choose is compatible with your PC's operating system.
{.is-danger}

| Formats | Maximum Volume Size | Maximum File Size | Maximum Number of Files | Read/Write Mode
| :---: | :---: | :---: | :---: | :---:|
| Fat | 2Tio | 4Gio (4.2949GB) | Greater than **250 million** | Windows: ✅ Linux: ✅ |
| NTFS | 256Tb (2,815.8492TB) | 16Tb (17.5921TB) | 4,294,967,295 | Windows: ✅ Linux: ✅ |
| exFAT | 128Pio (144,115,1880TB) | 128Pio (144,115,1880TB) | 2,796,202 per directory | Windows: ✅ Linux: ✅ |
| Ext4 | 1 Eio (1,152,921.5046TB - limited to 16Tio per e2fsprogs) | 16Tio (17,5921TB) | 4 billion | Windows : ❌ Read/Write via ext2fsd Linux : ✅ |

>**Caution
>
>Recalbox will not format your device, it will only create new files on it.
{.is-danger}

### Configuration

To configure Recalbox to use a USB storage device

* Connect your device to your Recalbox, then turn it on.
* Once under the Recalbox interface, press the `START' button on your controller, go to `SYSTEM SETTINGS` and then to `STORAGE DEVICE`.
* Now select your device from the list, then click and wait for the system to reboot.

>**Information:**
>
>**During this reboot phase**, Recalbox will **create at the root** of your device **a new directory** named **"recalbox "** which will contain the whole tree of your **"share" partition.**
{.is-info}

### Troubleshooting

#### Invisible hard disk

What to do if after reboot, Recalbox still does not see the hard disk?

>**Information:** 
>
>Sometimes, after selecting the device in EmulationStation and rebooting, Recalbox fails to create the file system on it. It continues to use the files on the SD card. This happens especially with some hard drives that are slow to initialize.
{.is-info}

What you can do:

* Connect via [SSH](./../tutorials/system/access/root-access-terminal-cli)
* Mount the [boot partition](./../tutorials/system/access/remount-partition-with-write-access) as read/write.
* Type the following commands: `cd /boot` and `nano recalbox-boot.conf`.
* Add this line `sharewait=30`. This value is in seconds.
* Save with `Ctrl+X`,`Y`,`Enter`. 
* Type `reboot` and confirm to restart Recalbox.

## Network

### Configure via RJ45 cable

* Connect your RJ45 cable to your Recalbox and that's it, your Recalbox is configured.

### Configure Wifi

* In the general interface, press `Start` > `NETWORK OPTIONS`. To enable, set the `ENABLE WIFI` option to **On**.
* You must now configure the wifi via WPS. On your Internet box, you must have a button on the modem or in the interface of your modem to activate the WPS function. Once pressed, it remains activated for 2 minutes.
* Still in Recalbox, select `WPS CONNECTION` at the bottom of the menu. This will attempt to find all existing and activated WPS connections around it.
* Once your connection is found, it will be automatically saved in your Recalbox.