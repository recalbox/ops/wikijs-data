---
title: Solarus
description: Arpg Game Engine
published: true
date: 2021-08-09T00:04:37.105Z
tags: fantasy, solarus
editor: markdown
dateCreated: 2021-06-30T09:11:39.896Z
---

![](/emulators/fantasy/solarus.svg){.align-center}

## Technical data

* **Manufacturer**: Solarus
* **Date of release**: September 19, 2020

## Presentation

**Solarus** is a lightweight, free and open-source 2D Action-RPG game engine specifically designed with the cult Action-RPGs of the 2D era in mind, such as **The Legend of Zelda: A Link to the Past** and **Secret of Mana** on Super Nintendo, or **Sunshine** on Sega Megadrive/Genesis.

## Emulators

[Solarus](solarus)