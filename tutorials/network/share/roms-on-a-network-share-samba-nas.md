---
title: Load roms on a network share with Samba (NAS)
description: 
published: true
date: 2023-10-23T19:22:15.570Z
tags: nas, share
editor: markdown
dateCreated: 2021-08-06T08:43:01.991Z
---

With Recalbox, you can store your data such as roms, bios, saves, themes, etc. from a network storage server, also known as NAS (Network Attached System).

We will see below how to configure the whole system on the network storage server side and on the Recalbox side, all with the Samba protocol.

>The NAS and your Recalbox must be connected via Ethernet for better stability and throughput!
{.is-warning}

## On the network storage server

This has been tested on a Synology, but it works the same.

On the network storage server:

* Create a shared directory (let's call it "/roms/").
* Create a user with read/write permissions on this directory (or reuse an existing user with these permissions).
* Recreate the architecture of the roms directory of your Recalbox (via network share, via FTP, or by hand if you want. The important thing is to have the same architecture, i.e. one directory per emulated machine).
  
It is quite possible to do the same thing from the hard disk of your PC. From Windows :

* You can create a shared directory.
* Mount it on your Recalbox in the same way.

The only drawback is that the PC serving as network storage server must be switched on permanently to access the roms from your Recalbox.

## In your Recalbox

* Mount the boot partition in [read/write](./../../../tutorials/system/access/remount-partition-with-write-access) mode.
* Edit the `/boot/recalbox-boot.conf` file with your favorite editor (nano, vi)
* Replace the following line:

``ini
;sharedevice=INTERNAL
```

* With the following line:

``ini
sharedevice=NETWORK
```

This is used to tell your Recalbox that the contents of the `/recalbox/share` folder should be mounted from a network share (not from the SD card).

>Note the removal of the `;` at the start of the line to make it active.
>{.is-info}

### Specifying remote directories

It is now necessary to specify, again in the `recalbox-boot.conf` file, the connection information to the network storage server. For this you have two options:

### {.tabset}

#### The command that encompasses everything at once

```ini
sharenetwork_<nfs|smb><[0-9]>=<SHARE|ROMS|SAVES|BIOS>@<NAS>:<shared directory>:<options>
```

##### Example:

```ini
sharenetwork_smb1=ROMS@192.168.0.1:recalbox/roms:username=recalbox,password=recalbox,vers=2.0
sharenetwork_smb2=SAVES@192.168.0.1:recalbox/saves:username=recalbox,password=recalbox,vers=2.0
```

The list of specifiable folders is at the bottom of this page.

#### The command for each directory

```ini
sharenetwork_cmd<[0-9]>=<command to execute>
```

##### Example

```ini
sharenetwork_cmd1=mount -o port=2049,nolock,proto=tcp 192.168.0.1:/Documents/recalbox /recalbox/share
```

The list of folders that can be specified is at the bottom of this page.

## Setup with SMB v2 or higher

With SMB version 2 or higher, you must specify the protocol version to use. For example in version 2.0:

```ini
sharenetwork_smb1=ROMS@192.168.0.1:recalbox/roms:username=recalbox,password=recalbox,vers=2.0
sharenetwork_smb2=SAVES@192.168.0.1:recalbox/saves:username=recalbox,password=recalbox,vers=2.0
```

### Minimum Windows versions per Samba version

* 1.0: Win 2k/XP/Server 2003/Server 2003 R2
* 2.0: Vista SP1/Server 2008
* 2.1: Win7/Server 2008 R2
* 3.0: Win8/Server 2012
* 3.02: Win8.1/Server 2012
* 3.11: Win10/Server 2016

### Some examples

Here are the possible assemblies :

### {.tabset}

#### Command that encompasses everything at once

```ini
sharenetwork_<nfs|smb><[0-9]>=<SHARE|ROMS|SAVES|BIOS|MUSIC|OVERLAYS|SCREENSHOTS|SHADERS|SCRIPTS>@<NAS>:<shared directory>:<options>
```

#### Command for each directory

```ini
sharedevice=NETWORK
sharewait=30
# THE ENTIRE SHARE FOLDER
sharenetwork_smb1=SHARE@<IP_DU_NAS>:<CHEMIN_NAS/SHARE>:username=<ICI_LOGIN>,password=<ICI_PASSWORD>,vers=3.0
```

## Set up with some folder(s) only

As seen above, it is possible to select only the folder(s) from the list below:

* BIOS
* MUSIC
* OVERLAYS
* ROMS
* SAVES
* SCREENSHOTS
* SHADERS
* SCRIPTS

### Example

```ini
sharedevice=NETWORK
sharewait=30
#UNIQUEMENT UNE SELECTION DES DOSSIER
sharenetwork_smb1=ROMS@<IP_DU_NAS>:<CHEMIN_NAS/ROMS>:username=<ICI_LOGIN>,password=<ICI_PASSWORD>,vers=3.0
sharenetwork_smb2=SAVES@<IP_DU_NAS>:<CHEMIN_NAS/SAVES>:username=<ICI_LOGIN>,password=<ICI_PASSWORD>,vers=3.0
sharenetwork_smb3=BIOS@<IP_DU_NAS>:<CHEMIN_NAS/BIOS>:username=<ICI_LOGIN>,password=<ICI_PASSWORD>,vers=3.0
sharenetwork_smb4=MUSIC@<IP_DU_NAS>:<CHEMIN_NAS/MUSIC>:username=<ICI_LOGIN>,password=<ICI_PASSWORD>,vers=3.0
sharenetwork_smb5=OVERLAYS@<IP_DU_NAS>:<CHEMIN_NAS/OVERLAYS>:username=<ICI_LOGIN>,password=<ICI_PASSWORD>,vers=3.0
sharenetwork_smb6=SCREENSHOTS@<IP_DU_NAS>:<CHEMIN_NAS/SCREENSHOTS>:username=<ICI_LOGIN>,password=<ICI_PASSWORD>,vers=3.0
sharenetwork_smb7=SHADERS@<IP_DU_NAS>:<CHEMIN_NAS/SHADERS>:username=<ICI_LOGIN>,password=<ICI_PASSWORD>,vers=3.0
sharenetwork_smb8=SCRIPTS@<IP_DU_NAS>:<CHEMIN_NAS/SCRIPTS>:username=<ICI_LOGIN>,password=<ICI_PASSWORD>,vers=3.0
```

If needed, here is another example. Each mount point can point to a specific network storage system.

>For overlays, you have to choose according to the resolution of your screen.
{.is-info}

```ini
sharedevice=NETWORK
sharewait=30

#FULL ROMS
sharenetwork_smb1=ROMS@192.168.1.112:C/RCB/romsfull:username=USER,password=PASS,vers=3.0
#ROMS SELECTION
#sharenetwork_smb1=ROMS@192.168.1.112:C/RCB/roms:username=USER,password=PASS,vers=3.0
#ROMS POUR TESTS
#sharenetwork_smb1=ROMS@192.168.1.110:TEST/roms:username=USER,password=PASS+,vers=3.0

sharenetwork_smb2=SAVES@192.168.1.112:C/RCB/saves:username=USER,password=PASS,vers=3.0
sharenetwork_smb3=BIOS@192.168.1.112:C/RCB/bios:username=USER,password=PASS,vers=3.0
sharenetwork_smb4=SCREENSHOTS@192.168.1.112:C/RCB/screenshots:username=USER,password=PASS,vers=3.0

#720p pour Pi0 Pi2 Pi3
#sharenetwork_smb5=OVERLAYS@192.168.1.112:C/RCB/overlays720:username=USER,password=PASS,vers=3.0
#1080p pour Pi4 XU4 et PC en 1080p
sharenetwork_smb5=OVERLAYS@192.168.1.112:C/RCB/overlays1080:username=USER,password=PASS,vers=3.0
```

>It is strongly recommended to disable SMB v1 support on your Windows machine!
{.is-warning}

### External links

* [http://www.zdnet.com/article/windows-10-tip-stop-using-the-horribly-insecure-smbv1-protocol/](http://www.zdnet.com/article/windows-10-tip-stop-using-the-horribly-insecure-smbv1-protocol/)
* [https://www.howtogeek.com/321072/how-to-disable-smbv1-and-protect-your-windows-pc-from-attack/](https://www.howtogeek.com/321072/how-to-disable-smbv1-and-protect-your-windows-pc-from-attack/)
* [https://www.spamtitan.com/blog/stop-smbv1-ransomware-attacks/](https://www.titanhq.fr/blog/comment-arreter-attaques-ransomware-smbv1/)
* [https://www.bostonhelpdesk.com/disabling-smbv1-one-defense-against-wanna-cry-ransomware/](https://www.bostonhelpdesk.com/disabling-smbv1-one-defense-against-wanna-cry-ransomware/)