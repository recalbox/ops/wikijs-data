---
title: Prince of Persia
description: 
published: true
date: 2024-07-25T08:26:48.848Z
tags: ports, prince of persia
editor: markdown
dateCreated: 2024-07-25T08:26:48.848Z
---

![](/emulators/ports/prince-of-persia.svg){.align-center}

## Technical data

* **Created by**: Brøderbund
* **Year of release**: 1989
* **Genre**: Platform, Die and retry
* **Operating system**: Amiga 500, Amstrad CPC, Atari ST, Commodore 64, DOS and ZX Spectrum.
* **Developer**: Jordan Mechner
* **Publisher**: Brøderbund
* **PEGI**: 3

## Presentation

Prince of Persia is a video game franchise created by Jordan Mechner. It is centered around a series of action-adventure games focused on various incarnations of the eponymous Prince, set in ancient and medieval Persia.

The first two games in the series, Prince of Persia (1989) and Prince of Persia 2: The Shadow and the Flame (1993), were published by Brøderbund. Prince of Persia 3D (1999), named for being the first installment to use 3D computer graphics, was developed by Red Orb Entertainment and published by The Learning Company on PC; the Dreamcast version was developed by Avalanche Software and published by Mattel Interactive. Ubisoft bought the rights to the franchise in 2001 and rebooted it with Prince of Persia: The Sands of Time (2003). Ubisoft has since developed and published five additional entries in the series: Prince of Persia: Warrior Within (2004), Prince of Persia: The Two Thrones (2005), Prince of Persia (2008), Prince of Persia: The Forgotten Sands (2010), and Prince of Persia: The Lost Crown (2024), as well as a number of spin-offs and games for mobile devices.

Outside of the games, the franchise includes a film adaptation based on The Sands of Time, written in part by Mechner, and released by Walt Disney Pictures in 2010; a graphic novel; and the Lego Prince of Persia toyline. Ubisoft's Assassin's Creed franchise is considered to be the spiritual successor to the series.

## Emulators

[SDLPoP](sdlpop)