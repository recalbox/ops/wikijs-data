---
title: FAQ
description: 
published: true
date: 2023-09-21T13:36:44.405Z
tags: faq
editor: markdown
dateCreated: 2023-09-21T13:36:44.405Z
---

## Herramienta para descargar los juegos

### Introducción

Los juegos de Tic-80 están libres de derechos de autor. Recalbox incluye un script que os permite descargar los juegos automáticamente a partir de https://tic80.com/.

### Utilización

A través de [SSH](./../../../system/access/root-access-terminal-cli), en la carpeta `/recalbox/share/roms/tic80`, escribid `bash recalbox-tic80` para descargar los juegos de TIC-80. Podéis elegir una categoría de juegos (demos, otros...) o las 5 categorías propuestas.