---
title: Fantasy
description: Consolas virtuales o imaginarias
published: true
date: 2023-09-04T06:55:36.480Z
tags: virtual, fantasy, consolas
editor: markdown
dateCreated: 2023-09-04T06:55:36.480Z
---

Las consolas Fantasy son consolas virtuales, tambien llamadas consolas imaginarias, porque no ha salido ninguna máquina fisica a la venta. Son emuladores que simulan un ordenador con pocos recursos.

Existen motores de juegos específicos para este tipo de consolas, que os detallamos en las siguientes páginas de esta sección.