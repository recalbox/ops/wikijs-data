---
title: High-resolution textures
description: 
published: true
date: 2021-08-08T13:46:28.305Z
tags: n64, texture, pack
editor: markdown
dateCreated: 2021-08-08T13:46:28.305Z
---

## What are Hi-res textures?

High-resolution textures (Hi-res) are fan-made packages designed to enhance the default textures of a game.

**Rice**, developer of the **Rice Video** plugin, was the first to make it possible to extract textures from a game, modify them and re-import them in a higher resolution. Now it is possible to replace blurred textures in games with high definition versions or a completely different version. Excited by this new possibility, many graphic designers have started to create substitute textures for their favorite games.

>When you download a high-resolution texture pack (Google is your friend), look in the readme file for the description, forum posts or anything else. Many of these packs only work with a specific rom file. For example, rom (U) works, rom (E) does not. Before you start, you should know this.
{.is-warning}

## Configuration

### Copy the high resolution textures to your Recalbox

Put the high resolution textures folder of each of your games in the following directory:

```text
/recalbox/share/bios/Mupen64plus/hires_texture/game_name/
```

### Activating the Libretro Mupen64Plus_Next emulator:

You need to tell the game in question to start it with the correct emulator.

* After rebooting, go to EmulationStation on your game and press `START`.
* Now go to `MODIFY GAME` > `START WITH`.
* Change the emulator to `LIBRETRO MUPEN64PLUS_NEXT`.
* Close the menus.

### Enabling high resolution textures

Now you have to activate the high resolution textures in the emulator.

* Launch a game.
* When the game starts, go to the RetroArch menu by pressing `Hotkey + B.`
* Press `Back` twice, then go to `Settings` > `Configuration`.

![](/tutorials/games/guides/nintendo-64/hires-textures/hirestextures1-en.png){.full-width}

* Enable the `Save configuration on exit` setting.

![](/tutorials/games/guides/nintendo-64/hires-textures/hirestextures2-en.png){.full-width}

* Press "Back" twice and go to `Main Menu` > `Quick Menu` > `Options`.

![](/tutorials/games/guides/nintendo-64/hires-textures/hirestextures3-en.png){.full-width}

* Enable the `Use High-Res textures` parameter.

![](/tutorials/games/guides/nintendo-64/hires-textures/hirestextures4-en.png){.full-width}

* Do "Back" twice and go to "Resume".
* Quit the game and start the game that needs to load the high resolution textures.

When the high resolution texture pack and the rom are right, the game will start with the high resolution texture pack.

## Troubleshooting

### You have followed the above instructions and it still doesn't work ?

Maybe it's a name problem...  
Many of these texture packs are provided with the right folder name but not all of them! The folder name must be exactly the internal name of your rom.

>The file name is `Mario Kart 64 (USA).v64` but the internal name of the rom is `MARIOKART64`.
{.is-warning}

### How do I know the internal name of my rom?

On Windows, you can use **Tool64** (Google is also your friend here).

* Start **Tool64**;
* `File` -> `Open`
* Select your rom folder.

![](/tutorials/games/guides/nintendo-64/hires-textures/hirestextures5.png){.full-width}

* Right click on a rom and then click `Rom Properties...`.

![](/tutorials/games/guides/nintendo-64/hires-textures/hirestextures6.png){.full-width}

Look at the `Rom Name` line to get the internal name of the rom.