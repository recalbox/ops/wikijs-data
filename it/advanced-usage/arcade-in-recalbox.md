---
title: L'acade in Recalbox
description: 
published: true
date: 2024-06-22T06:13:55.062Z
tags: 
editor: markdown
dateCreated: 2024-06-22T05:45:27.338Z
---

![](/advanced-usage/arcade/arcade-recalbox.png)

## I - IL CABINET ARCADE

Gli anni '70 segnano l'inizio della storia degli arcade!  
Ogni cabinet utilizza un hardware specifico e unico, spesso con più processori, chip sonori e video molto speciali e sempre la migliore tecnologia informatica di visualizzazione del momento.  
I primi cabinet arcade erano dedicati e utilizzabili solo per il gioco con cui erano stati consegnati (Pac-man, Space Invaders, Pong, ecc.). Quando un gioco giungeva alla fine della sua vita commerciale, era difficile riutilizzare il mobile.

I sistemi arcade nel 1986 evolvono e sono spesso basati su una scheda madre accoppiata con una cartuccia di gioco o un [**PCB**](https://it.wikipedia.org/wiki/Circuito_stampato) (connettività [**JAMMA**](https://it.wikipedia.org/wiki/JAMMA)).  
I cabinet arcade diventano quindi generici e l'hardware si avvicina a una console per videogiochi domestica o a un computer di fascia alta.  
Ciò ha permesso di ridurre i costi di produzione, i tempi di creazione e ha facilitato la vita degli operatori di cabinet e sale giochi.

Tuttavia, ci sono ancora molti sistemi arcade che utilizzano ancora hardware specifico e ottimizzato per un gioco unico: giochi di guida, giochi con pistola ottica, giochi di danza...

## II - MAME

[**MAME**](https://it.wikipedia.org/wiki/MAME) (Multiple Arcade Machine Emulator) è un sistema di emulazione versatile.

L'obiettivo di MAME è preservare decenni di storia del software.  
Mentre la tecnologia elettronica continua ad avanzare, MAME impedisce che questo importante software "vintage" venga perso e dimenticato.  
Questo viene realizzato documentando l'hardware e il suo funzionamento.

### 1 - La documentazione MAME

Il fatto che la documentazione sia utilizzabile serve principalmente a convalidare l'accuratezza della stessa (come dimostrare altrimenti che hai ricreato fedelmente l'hardware?).  
Nel corso del tempo, MAME ha assorbito il progetto gemello [**MESS**](https://it.wikipedia.org/wiki/MESS) nel 2015 (Multi Emulator Super System). Così, MAME documenta ora una grande varietà di computer (principalmente retrò), console per videogiochi, calcolatrici, oltre ai videogiochi arcade che erano il suo obiettivo iniziale.

La prima versione pubblica (0.1) di MAME, di Nicola SALMORIA, è uscita il 5 febbraio 1997.  
Il 21 ottobre 2010, la versione 0.140 dell'emulatore supporta più di 4.510 giochi.  
Il 4 aprile 2020, la versione 0.220 supporta più di 6.000 giochi e computer.

Essendo MAME un emulatore in continua evoluzione, è necessario far evolvere il proprio romset parallelamente alla versione dell'emulatore.

### 2 - Principio di documentazione MAME

MAME documenta questa favolosa biblioteca di macchine tramite il principio del [**DUMP**](https://it.wikipedia.org/wiki/Dump).  
Il dump consiste nel trasferire i dati contenuti nei vari componenti (scheda madre, PCB, cartucce) in un formato digitale per essere utilizzati con gli emulatori.

![](/advanced-usage/arcade/board-to-zip.png)

Per alcune macchine più o meno recenti questa documentazione è accompagnata da un supporto CD-ROM, DVD o anche cassetta chiamato CHD (Compressed Hunks of Data o pezzi di dati compressi).  
Senza questo CHD l'emulazione sarà imperfetta o non si avvierà affatto.

![](/advanced-usage/arcade/rom-and-chd.png)

Questa documentazione non è sempre perfetta e ciò dà versioni "BAD DUMP" di alcuni giochi purtroppo e quindi un'emulazione imperfetta o impossibile.

## III - I ROMSET

### 1 - Contenuto di un romset

Come spiegato sopra, nei tuoi romset c'è di tutto.  
Infatti, il fatto di voler documentare il “vintage” e l'insieme delle memorie morte del pianeta ci porta a chiederci: cosa c'è in un ROMSET?

Grazie al software [**CLRMAMEPRO**](./../tutorials/utilities/rom-management/clrmamepro) è possibile leggere l'intero contenuto di un romset avendo preventivamente un riferimento: il file [**.dat**](https://it.wikipedia.org/wiki/Index.dat).  
Questo file raccoglie l'elenco completo dei dati che costituiscono il romset rispetto all'emulatore.  
_**È importante confrontare il proprio romset con questo file .dat tramite clrmamepro ed evitare di prendere ROM a caso per problemi di compatibilità!**_

In MAME troveremo le seguenti famiglie:

* **STANDARD:** è la parte più grande del romset (55Go). Troveremo giochi arcade, flipper, console, slot machine, calcolatrici, persino tamagotchi...
* **MECHANICAL:** traduzione "elettromeccanico" sono macchine difficilmente emulabili perché semi-meccaniche/semi-digitali.
* **DEVICE:** traduzione "hardware" sono file di configurazione che accompagnano alcune macchine per l'emulazione.
* **BIOS:** alcuni sistemi sono classificati per BIOS (circa 70).

### 2 - I BIOS / i Drivers

Alcune ROM di giochi di un romset possono richiedere file BIOS o un driver (driver), il caso più noto sono i giochi [**Neo-Geo**](https://it.wikipedia.org/wiki/Neo-Geo_MVS).

* Esempio **Neo-Geo:** se vuoi utilizzare giochi Neo-Geo, dovrai copiare il file “BIOS” (in questo caso neogeo.zip) nella stessa directory del gioco.
* Esempio [**CPS**](https://it.wikipedia.org/wiki/CP_System): se vuoi utilizzare alcuni giochi CPS, dovrai copiare il file “driver” (in questo caso qsound_hle.zip) nella stessa directory del gioco.

Ovviamente se utilizzi sottocartelle per i tuoi giochi (genere o hardware ad esempio), dovrai copiare i file BIOS e driver in ogni directory contenente giochi che li richiedono.  
Dato che questi file BIOS e driver sono piuttosto piccoli in dimensione, è più semplice copiarli tutti in ciascuna delle tue sottocartelle.

Dove trovare questi file BIOS e driver ti chiedi?
Ebbene è piuttosto semplice: nel tuo romset!

### 3 - I CHD

Come per un BIOS o un DRIVER, il CHD deve accompagnare la sua ROM ma attenzione perché il formato e la disposizione di questo è molto specifico.

Il file CHD (non compresso) non porta lo stesso nome della ROM ma deve essere collocato in una cartella (non compressa) con lo stesso nome esatto di questa ROM per funzionare.  
Questi file CHD sono raramente presenti nei romset a causa delle loro dimensioni.

**Esempio ROM+CHD: sfiii3**

![](/advanced-usage/arcade/roms-chd.png)

### 4 - Tipi di file in un romset

* **PARENT:** la ROM del gioco originale che il team di MAME ha deciso di chiamare "versione originale". Tranne che per i file ROM BIOS, i file ROM per questi giochi contengono tutto il necessario per il funzionamento del gioco. L'insieme originale è considerato come la revisione più recente e se sono disponibili più versioni regionali, viene utilizzata la revisione world (internazionale) o US (americana).
* **CLONES:** la ROM è una versione o variante diversa dello stesso gioco (ad esempio, Street Fighter II Turbo è considerato una variante di Street Fighter II Champion Edition). Troveremo anche le versioni: Bootleg / homebrew / hack...
* **BIOS:** la ROM comune utilizzata da tutti i giochi di una stessa macchina (come Neo-Geo MVS). Sono i programmi utilizzati all'avvio della macchina, che consentono di eseguire operazioni di base durante l'accensione.

### 5 - Tipo di romset: split / merged / non-merged

**Esistono tre tipi di romset:**

* **Split:** [diviso] i file comuni tra genitori e cloni sono solo nello zip del genitore, per utilizzare un clone è necessario avere entrambi i ROM genitore e clone.
* **Non-merged:** [non fuso] tutti i file necessari per il clone sono nello zip di quest'ultimo.
* **Merged:** [fuso] genitori e cloni sono fusi nello stesso zip.

**Di seguito un esempio ROM nei diversi tipi di romset:** in rosso gli elementi del genitore, in giallo gli elementi del clone e in bianco gli elementi necessari per il funzionamento del gioco.  
Si noterà che nel caso del romset **split** la cartella **clone** non può funzionare senza la cartella **parent**.

![](/advanced-usage/arcade/rom-types.jpg){.full-width}

## IV - L'ARCADE E RECALBOX

I diversi emulatori in **Recalbox** fanno riferimento a **diversi romset MAME** supportati da questi.

_**È importante confrontare il tuo romset con un file .dat tramite clrmamepro e evitare di raccogliere rom qua e là per problemi di compatibilità.**_

Troverai tutte le versioni di tutti i romset su [questa pagina](./../emulators/arcade).

## V - GLOSSARIO

**A**

* **AtomisWave:** sistema arcade creato da Sammy la cui architettura è simile a quella della Naomi di Sega.

**B**

* **Boot:** si riferisce alla fase di avvio di un sistema, come ad esempio quello di una scheda arcade o di una console.
* **Bootleg:** copie non ufficiali di giochi su PCB o cartuccia, generalmente vendute a prezzi molto inferiori rispetto agli originali, riconoscibili abbastanza facilmente poiché quasi mai riportano il marchio dell'editore del gioco sul PCB o sulla cartuccia, contrariamente agli originali. È comune che i bootleg presentino vari bug, musiche mancanti, effetti sonori mancanti, freeze casuali...

**C**

* **Scheda madre:** spesso si riferisce al supporto che contiene tutti i componenti di calcolo e che è necessario per far funzionare i giochi che devono essere installati su di essa. Esempio con l'MVS o altri sistemi a cartuccia o disco: le unità di calcolo, i processori sonori o grafici, ecc. (chiamato anche hardware) sono concentrati nella scheda madre mentre il gioco (software) è contenuto nella cartuccia o nel disco.
* **Chihiro:** nome di un sistema arcade rilasciato da SEGA nel 2002. L'architettura di questa piattaforma è basata su quella della console Xbox di Microsoft. Tra i giochi rilasciati, i più popolari sono: Outrun 2, House of the Dead 3 e Virtua Cop 3.
* **CPS 1:** acronimo di Capcom Play System 1. Nome di un sistema arcade creato da Capcom e rilasciato nel 1988. Tra i giochi rilasciati su questo sistema, Street Fighters II è uno dei più famosi!
* **CPS 2:** acronimo di Capcom Play System 2. Nome di un sistema arcade creato da Capcom e rilasciato nel 1993. Tra i giochi rilasciati si trovano: Gigawing, Marvel Vs Capcom, la serie di Street Fighters Zero o Alpha, Vampire Savior o Progear no Arashi.
* **CPS 3:** acronimo di Capcom Play System 3. Nome di un sistema arcade creato da Capcom e rilasciato nel 1996. Questo sistema è molto potente nella gestione della grafica 2D. Sono usciti su questo sistema: Jojo’s Bizarre Adventure, Warzard, e la serie di Street Fighters III.
* **Crystal System:** [sistema arcade](https://fr.wikipedia.org/wiki/Syst%C3%A8me_d%27arcade) in formato JAMMA rilasciato nel 2001 prodotto e commercializzato dalla società BrezzaSoft. Il Crystal System è stato creato per sostituire il sistema Neo-Geo nel mercato dei giochi arcade economici. Ricordiamo che quasi tutti i dipendenti che lavorano presso BrezzaSoft sono ex membri di SNK, che hanno lasciato la società appena fallita per creare BrezzaSoft. Questo sistema offre giochi con una grafica 2D molto avanzata e piacevole. Il Crystal System non avrà il successo sperato e sarà un disastro commerciale, con una vita molto breve. Pochissimi giochi saranno rilasciati su questo nuovo sistema, ma si possono citare giochi come Evolution Soccer, un gioco di calcio copiato su Super Sidekicks, e Crystal of the Kings, un Beat 'em up di tipo medievale sulla falsariga di Golden Axe o Knights of the Round.

**D**

* **DECO Cassette System:** [cabina arcade](https://fr.wikipedia.org/wiki/Borne_d%27arcade) sviluppata da Data East. Rilasciata nel 1980, è stato il primo sistema arcade che permetteva al proprietario della cabina di cambiare giochi. L'acquirente doveva acquistare la cabina arcade, mentre i giochi erano disponibili su cassette audio. Era sufficiente inserire nella cabina la cassetta e un dongle di sicurezza (una delle prime forme di gestione digitale dei diritti, che impediva la copia).
* **Dump / Dumping / Dumper:** tecnica che consiste nel trasferire i dati contenuti nelle ROM di un gioco arcade sul disco rigido di un computer (immagine della ROM) per poi essere messa a disposizione su internet e utilizzata negli emulatori come MAME.

**E**

* **Emulazione:** processo che consente di riprodurre su un computer o una console il comportamento di un altro dispositivo, grazie a ciò che viene chiamato emulatore. Ad esempio, l'emulatore Kawaks permette di giocare alla Neo.Geo sul proprio PC.

**H**

* **Hyper Neo-Geo 64:** creato dalla società SNK e commercializzato nel settembre 1997, per succedere al sistema Neo-Geo MVS che all'epoca aveva sette anni. Tecnicamente, come suggerisce il nome, l'Hyper Neo-Geo 64 è dotato di un microprocessore a 64 bit che consente di eseguire giochi in 3D, visualizzando 4096 colori simultaneamente tra una gamma di 16,7 milioni di colori, con una risoluzione di 640 x 480 pixel. Tra i 7 giochi rilasciati: Fatal Fury, Wild Ambition, Samurai Shodown 64, Samurai Shodown 64, Warriors Rage...

**I**

* **Immagine di ROM:** copia delle ROM (Read Only Memory), i componenti elettronici che si trovano nelle cartucce e nelle schede arcade e che contengono il gioco. Copiare un gioco arcade sul disco rigido di un PC si chiama "dumpare" un gioco e richiede attrezzature speciali. Le copie/immagini delle ROM vengono poi messe su internet sotto forma di file generalmente compressi in .zip leggibili dagli emulatori, e poi scaricate da te e da me. Per semplicità, le immagini delle ROM sono semplicemente chiamate "rom" dalla maggior parte dei siti di emulazione.

**J**

* **JAMMA:** standard di connessione per tutti i giochi arcade affinché tutti i giochi arcade funzionino su tutte le cabine, creato a metà degli anni '80 dalla "Japanese Amusement Machine Manufacturers Association". Tecnicamente, lo standard definisce le connessioni per: due joystick a otto direzioni, due pulsanti start, e sei pulsanti di controllo (tre per giocatore), oltre al supporto del suono (mono).

**K**

* **Konami System 573:** il System 573 è una serie di schede di sistema arcade di Konami basate sulla Playstation originale. L'hardware è stato utilizzato principalmente per le serie di giochi video musicali Benami di Konami, più comunemente la serie Dance Dance Revolution introdotta nel 1998.
* **Konami Viper:** Konami lancia la Konami Viper nel 1998 per le sale giochi. Questo arcade utilizza il sistema JAMMA+PCB. Tra i 13 giochi rilasciati: Jurassic Park 3, GTI club 2, Silent Scope EX, Thrill Drive II…

**L**

* **Lindbergh:** nome di un sistema arcade creato da SEGA rilasciato nel 2006. L'architettura e i componenti di questo sistema sono molto simili a quelli di un PC, il che rappresenta una rottura rispetto agli altri sistemi creati da SEGA in precedenza. Tra i giochi rilasciati su Lindbergh ci sono Virtua Fighters 5, Virtua Tennis 3, House of The Dead 4, After Burner Climax… Lindbergh è anche il nome di uno dei pionieri dell'aviazione, l'americano Charles Lindbergh (1902-1974). È la prima persona ad aver effettuato la traversata in solitaria dell'oceano Atlantico in aereo.

**M**

* **MAME:** famoso emulatore arcade che emula oltre 6000 sistemi arcade. MAME è anche considerato un vero e proprio museo virtuale del gioco arcade.
* **Model 1:** nome di un sistema arcade creato da SEGA rilasciato nel 1992. Un vero e proprio avanzamento tecnologico, è su questo sistema che usciranno Virtua Racing e Virtua Fighter 1.
* **Model 2:** nome di un sistema arcade creato da SEGA rilasciato nel 1993. Alcuni dei giochi più popolari rilasciati su questo sistema sono: Daytona USA, House of the Dead, Sega Rally Championship e Virtua Fighter 2.
* **Model 3:** nome di un sistema arcade creato da SEGA rilasciato nel 1996 che diventa allora il sistema più potente della sua epoca. Alcuni dei giochi più popolari rilasciati su questo sistema sono: Daytona USA 2, Scud Race, Sega Rally 2 e Virtua Fighter 3.

**N**

* **Namco System 246:** il System 246 è un sistema di giochi video destinato alle sale giochi, basato sulla Playstation 2. È stato creato dalla società Namco nel 2001 e ha conosciuto diverse evoluzioni: il System 256, il Super System 256 e il System 147.
* **Naomi e Naomi GD:** sistema di gioco video a cartuccia e GD-ROM per cabina arcade, rilasciato nel 1998 come successore del sistema Sega Model 3. L'acronimo sta per New Arcade Operation Machine Idea (letteralmente "nuova idea di macchina arcade"), Naomi si traduce anche in "bellezza" in giapponese.
* **Neo-geo MVS:** (Multi Video System) è un sistema di giochi video per cabina arcade compatibile JAMMA destinato alle sale giochi e creato dalla società giapponese SNK nel 1990.
* **Nintendo Playchoice 10:** basato sul sistema Nintendo Entertainment (NES), la scheda madre Playchoice 10 ha permesso di installare fino a 10 cartucce ROM contemporaneamente. A differenza della maggior parte degli altri giochi arcade, le monete aggiungono tempo anziché crediti, permettendoti di cambiare gioco al volo quando lo desideri.

**P**

* **PCB:** iniziali di "Printed Circuit Board" ("Circuito stampato" in italiano). Piastra destinata a raggruppare componenti elettronici. In arcade un PCB è un circuito stampato che contiene un gioco e che si collega alla cabina arcade tramite il connettore JAMMA.
* **PGM:** iniziali di "Poly Game Master" sviluppato da IGS (International Games System). Questo sistema arcade è simile all'MVS nelle prestazioni, nell'aspetto e nell'utilizzo.
* **Pinball:** flipper.

**R**

* **ROM:** sigla di "Read Only Memory" chiamata anche "memoria non volatile".

**S**

* **Sega Hikaru:** nome di un sistema creato dalla società Sega nel 1999. L'Hikaru è pensato originariamente per ospitare il gioco Brave Fire Fighters. Ha la capacità di visualizzare su schermo grafica complessa rappresentante movimenti di fuoco o acqua. È stato il primo sistema arcade a proporre l'ombreggiatura Phong su schermo. Questo sistema ha conosciuto solo sei giochi ed è stato rapidamente abbandonato, poiché la sua gestione era troppo costosa rispetto alla Naomi2.
* **Sega Mega Play:** sistema arcade basato sulla console domestica Sega Megadrive. Quando inserisci le monete, acquisti crediti come un normale gioco arcade.
* **Sega Mega-Tech:** sistema arcade basato sulla console domestica Sega Megadrive. Quando inserisci una moneta, acquisti il tempo, il gioco termina quando il tuo tempo è scaduto, puoi aggiungere monete durante il gioco per ottenere tempo extra.
* **Set:** nel linguaggio arcade un set=gioco o un BIOS composto da più memorie non volatili o ROM.
* **ST-V Titan** (acronimo di Sega Titan Video): sistema arcade di Sega lanciato nel 1994, con un'architettura identica a quella della console Sega Saturn (con la differenza che l'ST-V utilizzava cartucce). Uno degli scopi era di facilitare la vita dei programmatori, di fare conversioni arcade verso console molto più rapide, e di ottenere conversioni il più fedeli possibile.

**T**

* **Triforce:** nome di un sistema arcade sviluppato congiuntamente da Nintendo, SEGA e Namco e rilasciato nel 2002. L'architettura di questo sistema è basata su quella della console GameCube di Nintendo. Tra i giochi rilasciati, i più popolari sono: F-Zero AX, Virtua Striker 4 e Mario Kart Arcade GP. Nota: il nome Triforce rappresenta l'alleanza tra le 3 società ma è anche un riferimento alla serie The Legend Of Zelda.

## VI - Fonti:

* [http://www.system16.com/](http://www.system16.com/)
* [https://fr.wikipedia.org/wiki/Wikip%C3%A9dia:Accueil_principal](https://fr.wikipedia.org/wiki/Wikip%C3%A9dia:Accueil_principal)
