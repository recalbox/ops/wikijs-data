---
title: FAQ
description: 
published: true
date: 2021-08-08T13:53:01.475Z
tags: faq
editor: markdown
dateCreated: 2021-08-08T06:17:49.120Z
---

## Extensions

Here is a list with explanations of the different file formats for Amiga systems:

* Disks _**AFD**_ (*.adf), the **most popular** disk format. **Cannot integrate** disk protections (**can be zipped or 7-zipped**). 
* _**IPF**_ (*.ipf) disks, most commonly **used by non-intro game copies** (_may_ incorporate disk protections). **Can be zipped. 
* _**WHD**_ (folders, *.lha/lzh/lzx, *.zip), the well-known format for hard disk games. WHD folders are recognized using the *.slave file inside the folder, but it is _strongly_ recommended to keep them packaged as lha or zip. Unpacked WHDs are not faster and even worse, they can make EmulationStation super slow. 
* _**HDD FS**_, "Hard drive on filesystem". Must be a folder ending with ".hd" extension. Rarely used. For true Amiga fans who have backed up their machine's hard drive. Can be zipped, but not recommended. 
* _**HDF**_ (*.hdf), Disk image in a single file. Can be zipped (Read only!) 
* _**RP9**_ (*.rp9), Amiga Forever's all-in-one packages

When you play from zipped/7-zipped files, our configurator tries to identify the type of rom by looking for specific extensions inside the archive.

* ADF/IPF/BIN/ISO extensions are fast to identify. 
* WHD is fast, but LHA should be the preferred option. 
* HDDFS may take longer to identify and may result in false interpretations. If you use HDDFS, leave it as normal files and folders and end the root folder name with `.hd` for easy identification.

## How it works

### ADF games

**Copy them** into your Amiga **roms folder**.  
Multiple **disks** are **automatically** loaded, **up to 4** for games using **the correct nomenclature** "(Disk 1 of X)".

### WHD games

**WHD** games are **a bit more difficult** but nothing too complicated.

* **Installation:**

Unzip your WHDLoad game, delete the `.info` file at the same level as the folder, and copy only this folder to your roms folder (you can use subfolders if you wish -&gt; e.g. `/recalbox/share/roms/amiga1200/ShootEmUp`).

* **Use:**

You will need a `.uae` file at the same level as the folder of the same name, so two solutions:

* Create an empty one. 
* Use a custom file that will allow you to modify the emulator configuration (see next paragraph).

**By default I recommend to first try the amiga1200 folder for any WHDL game. If some are too fast, then try them in the amiga600 folder.**

You can use the following script to generate massive uae files: [UaeGenerator](http://www89.zippyshare.com/v/80w859p5/file.html)

* **Modifying WHDL games:**

### Editing .UAE files

In the uae file, you can define `;hardware`, `;controls` and `;graphics` blocks to replace the standard emulator configuration.

**The custom parts of the configuration in the .uae file will only be used if they start with the correct block name:** `;hardware`**,** `;controls` **or** `;graphics`

This can allow you to use two joysticks for example, have a very specific hardware configuration for some games that are a bit special, or define a custom resolution.

You can also delete everything in the uae file and the basic emulator configuration will be used.

Example of fully customized uae files for the amiga600 and amiga1200 duo : [Custom UAEs](https://www100.zippyshare.com/v/1ttgYgks/file.html).

Copy this one (corresponding to the Amiga model you want to use) next to the WHDL game folder and rename it exactly like the game folder (keep the `.uae` extension) e.g. if your game has the folder name `Rick Dangerous`, your uae file should be named `Rick Dangerous.uae` and be at the same level as the folder.

### Changing the startup sequence (experts only)

The standard startup sequence generated for WHDL games is `WHDload game.slave Preload` (in `WHDL/S/Startup-Sequence`).

But a few WHDL games (like History Line: 1914-1918) may ask for additional parameters at startup or they will simply crash.

You can add a second (completely optional) file containing these additional settings, so you will have, taking the example of History Line :

* HistoryLine (WHDL game folder) 
* HistoryLine.uae (which can be empty or custom) 
* HistoryLine.whdl (optional, containing additional parameters `CUSTOM1=1`)

The game will then launch.

Here is a small list of games that require additional parameters.

| Game | Extra parameter(s) / File content |
| :--- | :--- |
| HistoryLine 1914-1918 | `CUSTOM1=1` for the intro, `CUSTOM1=2` for the game |
| The Settlers / Die Siedlers | `CUSTOM1=1` passes the intro |