---
title: Os Overlays
description: Adicione ilustrações ao redor da imagem do jogo.
published: true
date: 2021-08-01T22:19:20.511Z
tags: 
editor: markdown
dateCreated: 2021-07-27T23:14:58.216Z
---

## Apresentação

Os **overlays** (também chamados de *bezels*) são ilustrações que "decoram" sua tela durante o jogo. Estas ilustrações são frequentemente colocadas nas áreas pretas de sua tela 16/9, não utilizadas pelo jogo que é exibido em 4/3.

**Street of Rage 3** com um Overlay do Megadrive, em uma tela de 16:9:
![overlays.png](/basic-usage/features/overlays/overlays.png)


**Street of Rage 3** sem Overlay, em uma tela de 16/9:
![43.png](/basic-usage/features/overlays/43.png)

**Street of Rage 3** sem Overlay, em uma tela de 16:9, com a imagem esticada:
![169.png](/basic-usage/features/overlays/169.png)


## Overlays do Recalbox

O Recalbox tem incorporado **Overlays do Recalbox** que são exibidos por padrão em telas compatíveis.

Você pode ativar ou desativar os **overlays** no menu **CONFIGURAÇÕES DOS JOGOS** do EmulationStation:

![overlays-set-option-es.png](/basic-usage/features/overlays/overlays-set-option-es-pt.png)

### Ativação por sistema

No `recalbox.conf` é possível ativar ou desativar os Overlays do Recalbox de forma geral:
```ini
global.recalboxoverlays=1
```

Ou por sistema:
```ini
snes.recalboxoverlays=0
```

## Adicione seus próprios Overlays

Há diferentes pacotes de Overlays, alguns dos quais oferecem um Overlays para cada jogo!

Quando você tiver baixado seu pacote de overlays, copie todas as pastas que ele contém para a pasta 'overlays' da sua partição 'SHARE':

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 overlays
┃ ┃ ┃ ┣ 📁 megadrive
┃ ┃ ┃ ┃ ┣ 🗒 megadrive.cfg
┃ ┃ ┃ ┃ ┣ 🗒 megadrive_overlay.cfg
┃ ┃ ┃ ┃ ┣ 🗒 megadrive.png
┃ ┃ ┃ ┣ 📁 snes
┃ ┃ ┃ ┃ ┣ 🗒 .....

> Overlays personalizados têm prioridade sobre os Overlays automáticos. 
> {.is-info}