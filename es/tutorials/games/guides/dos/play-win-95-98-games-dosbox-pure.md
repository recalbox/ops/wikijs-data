---
title: Jugar a los juegos de Windows 95 & 98 con Dosbox Pure
description: Jugad a vuestros antiguos juegos de Windows !
published: true
date: 2025-03-13T19:30:20.656Z
tags: dosbox pure, 9.0+, windows95, win95, win98, windows98
editor: markdown
dateCreated: 2023-08-15T22:51:25.952Z
---

## Introducción

A partir de Recalbox 9.0, se puede jugar a los juegos de Windows 95 y de Windows 98 !. Este tutorial describe los pasos a seguir.

Antes de nada, hay que cumplir los siguientes prerequisitos :

- Posesión de un Windows 95/98 en formato image CD
- 1 teclado
- 1 ratón
- Posesión del juego que quereis lanzar en formato comprimido  .zip ou .7z

## Instalación

En la siguiente demostración, vamos a utilizar un Windows 95 OSR 2.5 (el cual soporta USB de forma nativa) junto con un CD de Wolfenstein comprimido.

La instalación se realiza en dos partes. La primera parte consiste en la instalación de Windows, mientras que la segunda parte consiste en el lanzamiento del juego dentro de vuestra instalación Windows.

### Instalación de Windows

- Depositad el .zip de la imagen CD de Windows y el .zip de vuestro juego dentro del directorio `dos` de vuestra Recalbox y actualizad la lista de juegos.
- Lanzad vuestro Windows con el core `Libretro-DOSbox_Pure`
- A continuación, elegid la opción `[ Boot and Install New Operating System ]`

![](/tutorials/games/guides/dos/w95-1-fr.png)

- El sistema os propone crear una instalación dentro de una imagen que se créara dentro de vuestra carpeta de bios. Sólo tenéis que elegid el tamaño de vustro disco de instalación de Windows.

![](/tutorials/games/guides/dos/w95-2-fr.png)

>Tened cuidado con el tamaño que elegís, el disco virtual debe ser suficientemente grande como para almacenar los juegos que queréis instalar (si deben ser instalados)!.
{.is-warning}

>No pierda de vista la pantalla, ya que una vez finalizada la creación del espacio en disco definido para esta instalación, el ejecutable de instalación de Windows toma el relevo sin demora y espera a que realice una elección (arranque desde CD-ROM, elección / opción 2) en un plazo máximo de 5 a 10 segundos.
{.is-warning}

- Una vez elegido un tamaño, pasaran varios segundos sin que pase nada y finalmente vuestro Windows arrancará.

![](/tutorials/games/guides/dos/w95-3-fr.png)

- Continuad con la instalación típica de Windows que necesita varios reinicios hasta tener acceso al escritorio de Windows que indica el final de la instalción.

> No olvides que para instalar Windows se necesita tener una clave de licencia !
{.is-info}

![](/tutorials/games/guides/dos/w95-4-fr.png)

- Una vez que Windows ha sido correctamente instalado, podéis apagarlo con el botón `Inicio` > `Apagar`. Cuando el sistema os indica que podéis apagar el ordenador, pulsad `HOTKEY` + `START` en vuestro mando para volver a EmulationStation.

> Si pulsáis `HOTKEY` + `START` mientras que Windows no se ha apagado correctamente, tendrá que ser reparado en el siguiente lanzamiento !
{.is-info}

### Instalación de vuestro juego

- En la lista de juegos DOS, lanzad el juego que queréis instalar/ejecutar dentro de vuestra imagen virtual de Windows creada anteriormente.
- Una vez lanzada, seleccionad `[ Run Installed Operating System ]`.

![](/tutorials/games/guides/dos/w95-5-fr.png)

- La lista de sistemas virtuales Windows disponibles se mostrará en una lista. Elegid el que acabais de instalar y confirmad.

![](/tutorials/games/guides/dos/w95-6-fr.png)

- Windows se lanza mostrando el juego como si fuera un CD dentro del explorador de Windows

![](/tutorials/games/guides/dos/w95-7-fr.png)

- Simplemente lanzad el programa de instalación de vuestro juego para poder jugar.

![](/tutorials/games/guides/dos/w95-8-fr.png)