---
title: Touches du clavier via GPIO
description: 
published: true
date: 2021-09-04T22:43:17.594Z
tags: gpio, touches, clavier
editor: markdown
dateCreated: 2021-05-21T08:36:00.731Z
---

Recalbox comporte le module `retrogame` sous le nom de `recalbox-retrogame`. Ce module va permettre d'utiliser les GPIO afin de reproduire le comportement d'un vrai clavier USB sous Recalbox.

>Ne fonctionne que si on retrouve la ligne `controllers.gpio.enabled=0` dans recalbox.conf.
{.is-warning}

* Si vous voulez intégrer un Raspberry Pi dans une vieille console et réutiliser les boutons de la façade (ex: `PAUSE/SELECT` d'une Atari 7800) :
   * Câbler (sur une borne d'arcade) un bouton qui aurait le même comportement que la touche Echap d'un clavier.
   * Ou tout simplement réussir à se passer d'un clavier.

## Installation

### Câblage

Il faut utiliser des boutons poussoir (qui remonte après un appui) et relier une borne à une masse (GROUND) et l'autre borne à un GPIO.

### Script

Afin de lancer automatiquement le module au démarrage de Recalbox, il est important d'ajouter un script.

* Créez un ficher texte `S99retrogame` sans extension de fichier.
* Ajoutez les deux lignes de codes suivantes : 

```ini
/usr/bin/recalbox-retrogame
exit 0
```

* Donnez les droits correspondants :

```shell
chmod 775 /etc/init.d/S99retrogame
```

* Placez-le dans le répertoire `/etc/init.d/`.
* Redémarrez et vérifiez via [SSH](./../../../tutorials/system/access/root-access-terminal-cli) si le module s'est bien lancé grâce à la commande :

```shell
ps aux | grep recalbox-retrogame
```

Si la commande retourne un identifiant pour une autre ligne que celle-ci, c'est que vous avez réussi.