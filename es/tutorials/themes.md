---
title: 🎨Themes
description: Manual detallado para la creación de themes
published: true
date: 2024-04-07T13:28:37.176Z
tags: 9.2+, temas
editor: markdown
dateCreated: 2024-04-07T12:01:10.554Z
---

>Esta sección de tutoriales está en curso de redacción.
>
>Todo lo que se describe en las siguientes secciones se aplica **à partir de Recalbox 9.2** !
{.is-warning}

En los siguientes apartados se detalla el funcionamiento de los themes de Recalbox: cómo funcionan, qué es lo que se encuentra disponible y lo que podéis modificar, etc.

Aquí tenéis la liste de tutorialies disponibles:

[1. Arborescencia](tree)
[2. Las variables](variables)
[3. Las condiciones](conditions)
[4. Los objetos](objects)