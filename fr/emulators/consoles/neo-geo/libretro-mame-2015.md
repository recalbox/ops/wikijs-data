---
title: Libretro MAME 2015
description: 
published: true
date: 2024-06-22T10:48:11.249Z
tags: libretro, neo-geo, mame2015
editor: markdown
dateCreated: 2021-05-21T08:22:53.345Z
---



## ![](/emulators/license.svg) Licence

Ce core est sous licence **MAME non-commerciale**.

## ![](/emulators/compatibility.png) Compatibilité

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ❌ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Fonctionnalités



## ![](/emulators/bios.svg) BIOS

### Liste des bios obligatoires

| Nom de fichier | Description | MD5 | Fourni |
| :---: | :---: | :---: | :---: |
| neogeo.zip | BIOS Neo-Geo | e023d0a4b5249fdff2a4620c28963944 8cbdff53661f41ddbe486a98915e1ec9 42e359e394f0be1d61b94ddca0efbe6c c1e0a738d266926f604186aa9d63e4db d12bba59121229ff13702878b415cb7c 2b4ffdf24d5d4f24c4b116c50c4bafc3 005b843b6e70b939c2cca41887cbc371 42e359e394f0be1d61b94ddca0efbe6c 4b9432a4571d03cc8bae9d24d8d6eb40 4cd64313e26537c04cdb561b8f84b0e4 50bbfd6bf297038b085e29265c65723a 76ee7390ccd537cddea1018025ca29a8 84d6c35aad790a77ba3cfc1b319e1061 912c0a56bafe0fba39d0c668b139b250 94748719c0ef31e7375b0c357d82fc89 ab25b9e25052a6064901a7a221037eb6 ae9e5825cde82c58e39dfd48534f7060 b18f83feef474b8a1306ef199cd810a2 b4519df27a352c2a6e42e06d31330d91 bf00272e7150b31156a8995d60bf185d ca66fd463ef7dcab492a6de8ce5f45eb f4a125538dfd7a8044e025fc5188fb88 a3b2c9a69c28ad0bd4ea07877b744bbe | ❌  |

### Emplacement

Placez les bios comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 neogeo
┃ ┃ ┃ ┃ ┣ 🗒 **neogeo.zip**

## ![](/emulators/roms.png) Roms

### Extensions supportées

La rom doit avoir l'extension :

* .zip
* .7z

### Emplacement

Placez vos roms comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 neogeo
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.zip**

>Pour plus d'information sur les roms, rendez-vous sur [ce tutoriel](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuration avancée de l'émulateur

>Pour pouvoir conserver vos configurations personnalisées lors d'une mise à jour, nous vous conseillons d'utiliser notre fonctionnalité [Surcharges de configuration](./../../../advanced-usage/configuration-override).
{.is-info}

### Accéder aux options

Vous pouvez configurer diverses options de deux façons différentes.

* Via le menu RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via le fichier `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Options du core

| Options | Valeur des options | Variable (pour surcharge) | Valeurs des variables |
| --- | --- | --- | --- |
| Read configuration | `Désactivé` ✅ / `Activé` | `mame2015_read_config` | `disabled` / `enabled` |
| Auto save/load states | `Désactivé` ✅ / `Activé` | `mame2015_auto_save` | `disabled` / `enabled` |
| XY device (Restart) | `none` ✅ / `lightgun` / `mouse` | `mame2015_mouse_mode` | `none` / `lightgun` / `mouse` |
| Enable throttle | `Désactivé` ✅ / `Activé` | `mame2015_throttle` | `disabled` / `enabled` |
| Enable cheats | `Désactivé` ✅ / `Activé` | `mame2015_cheats_enable` | `disabled` / `enabled` |
| Hide nag screen | `Désactivé` ✅ / `Activé` | `mame2015_hide_nagscreen` | `disabled` / `enabled` |
| Hide gameinfo screen | `Désactivé` ✅ / `Activé` | `mame2015_hide_infoscreen` | `disabled` / `enabled` |
| Hide warnings screen | `Désactivé` ✅ / `Activé` | `mame2015_hide_warnings` | `disabled` / `enabled` |
| Alternate render method | `Désactivé` ✅ / `Activé` | `mame2015_alternate_renderer` | `disabled` / `enabled` |
| Boot to OSD | `Désactivé` ✅ / `Activé` | `mame2015_boot_to_osd` | `disabled` / `enabled` |
| Boot from CLI | `Désactivé` ✅ / `Activé` | `mame2015_boot_from_cli` | `disabled` / `enabled` |

## ![](/emulators/external-links.png) Liens externes

* **Code source utilisé** : [https://github.com/libretro/mame2015-libretro/](https://github.com/libretro/mame2015-libretro/)