---
title: Compatibles PC
description: 
published: true
date: 2021-08-08T18:31:42.264Z
tags: compatibility, pc
editor: markdown
dateCreated: 2021-08-05T23:21:16.331Z
---

>**Information**:
>
>If you **don't see your PC** in the tables below, it **has not been tested yet**.
>You can also test the hardware with the latest version of Recalbox if it has not been done yet, the information can be updated (please let us know if the tested hardware is compatible).
>
>* Specify the **Manufacturer** and **Model**.
>* If it is an assembled PC, specify the CPU and GPU.
>* Feel free to **add details about the performance** of your configuration. 
>* If you have any problems, please let us know.
{.is-info}

## PC Models

### Dell

| Name | Status | Recalbox version | Notes |
| :---: | :---: | :---: | :---: |
| Dell Optiplex 390 i5-2400 3.10GHz | ✅ | 6.0 Rc3 | 100% Compatible |
| Dell Optiplex 990 i3 2100 | ✅ | 13.07.18 | 100% Compatible |
| Dell Optiplex 990 USFF i5 2400S 2.5GHZ | ✅ | 6.0.1 | 100% Compatible |
| Dell Optiplex 3020 micro i3 4150T | ✅ | 6.0 Rc3 | 100% Compatible |

### Intel

| Name | Status | Recalbox version | Notes |
| :---: | :---: | :---: | :---: |
| Intel NUC7i3BNK | ✅ | 4.1 | i3-7100U / HD Graphics 620 / BIOS 0036:OK / BIOS 0052:No temp probe |
| Intel NUC7i5BNH | ✅ | 6.1 | 100% compatible, around 60 FPS for all systems, CPU Core i5-7260U, GPU Intel IRIS Plus 640 |
| Intel NUC6I7KYK | ✅ | 4.1 | Fully compatible, Around 60 FPS for all consoles, latest bios 0050, all hardwares are ok: CPU Core i7-6770HQ, graphics Intel IRIS PRO 580 fully supported, wifi, bluetooth, sound, usbs, SSD SATA Samsung 1TB |
| Intel NUC8i7BEH | ✅ | 6.1 | 100% compatible, around 60 FPS for all systems, CPU Core i7-8559U, GPU Intel IRIS Plus 655 |

### Divers

| Name | Status | Recalbox version | Notes |
| :---: | :---: | :---: | :---: |
| HP Slim Desktop 270-P026 i3-7100T | ✅ | 18.07.13 | 100% Compatible |
| Laptop 17.3" MSI GP72M 7RDX-871XFR | ✅ | 4.1 | Around 60 FPS for all consoles on x64 version with defaults parameters, latest bios flashed, all hardwares are ok: CPU Core i7-7700HQ, graphics Intel HD Graphics 630 fully supported, wifi, bluetooth, sound (on laptop speakers+headphones+HDMI), usbs, SSD SATA Samsung 1TB, all ok! Since it have a battery, games are no longer interrupted ! (only drawback is that it doesn't support the GPU Nvidia GTX 1050 yet due to the current used linux kernel) |
| Taichi  Z270 + I5 7500 | ✅ | 4.1 | Around 60 FPS for all consoles on x64 |
| ZOTAC  ZBOX Nano AD10 | ✅ | 4.1 | Wii/NGC ok but not playable |

### Apple Mac

| Name | Status | Recalbox version | Notes |
| :---: | :---: | :---: | :---: |
| MacBook Pro (13 pouces, début 2011) | ✅ | 4.1 | 100% compatible |
| iMac (21.5 pouces, Mid 2011) | ✅ | 4.1 | 100% Compatible, some minor graphic bugs on the menu |
| Mac Pro (fin 2013) | ❌ | 4.1 | Not Working. Can't even boot |
| MacBook (13 pouces, fin 2009) | ✅ | 7.2.1 | 100% compatible |
| MacBook Pro (15 pouces, 2018) | ✅ | 7.3 | No sound, keyboard not working. [Disable the T2 secure chip to boot](./../tutorials/others/disable-mac-t2-chip) |

## CPU

### Intel

| Name | Status | Recalbox version | Notes |
| :---: | :---: | :---: | :---: |
| Intel i3-4150T 3.00GHz | ✅ | 6.0 Rc3 | Around 60 FPS for all consoles on x64 version with default parameters dolphin work great with default parameters |
| Intel Core i3-7100T | ✅ | 18.07.13 | Around 60 FPS for all consoles on x64 version with default parameters |
| Intel Core i3-7100U | ✅ | 4.1 |  |
| Intel Core i5-2400 3.10 GHz | ✅ | 4.1 | Working fine for all emulators except for gamecube's and wii's emulators (best perfomance with graphics card) |
| Intel Core i5-2400S 2.5 GHZ | ✅ | 7.0.1 |  |
| Intel Core i5-3570K 3.40 GHz | ✅ | 6.0 Rc3 | Around 60 FPS for all consoles on x64 version with default parameters |
| Intel Core i5-4570S | ✅ | 4.1 |  |
| Intel Core i5-6500 | ✅ | 4.1 |  |
| Intel Core i5-7500 | ✅ | 4.1 |  |
| Intel Core i7-2600K | ✅ | 4.1 | Around 60 FPS for all consoles on x64 version with default parameters |
| Intel Core i7-6770HQ | ✅ | 4.1 | Around 60 FPS for all consoles on x64 version with default parameters |
| Intel Core i7-7700HQ | ✅ | 4.1 | Around 60 FPS for all consoles on x64 version with default parameters |
| Intel Pentium G2130 3.20GHZ | ✅ | 4.1 |  |
| Intel G4600 | ✅ | 18.04.20 | Around 60 FPS for all consoles on x64 version with default parameters |

### AMD

| Name | Status | Recalbox version | Notes |
| :---: | :---: | :---: | :---: |
| AMD X2 4850B | ✅ | 4.1 |  |

## GPU

>**Attention**:  
>Please keep in mind that the graphics card is not everything, the Linux kernel contained in Recalbox must also be able to handle the device.
{.is-info}

### New

Nouveau is a project of the X.Org foundation and Freedesktop.org to provide royalty-free Nvidia graphics drivers by reverse engineering.

Nouveau is based on the free (but obscure) nv drivers maintained by Nvidia for 2D support. 

To support OpenGL, the project uses Mesa 3D, but changed in February 2018 by its successor Gallium3D4.5. OpenCL support is also done through Gallium3D. 

#### Process used

The project uses the technique known as reverse engineering on Nvidia graphics cards by studying how the 3D drivers for Linux, provided by the manufacturer (under a proprietary license), work without touching the drivers. This way of doing things allows the project to avoid conflicts with the Nvidia license.

* According to the [features page again](https://nouveau.freedesktop.org/wiki/FeatureMatrix/)
* According to the compatibility with NVIDIA ([NVidia codenames](https://nouveau.freedesktop.org/wiki/CodeNames/))
* The New driver in Recalbox (Version 1.0.15)

>**Note:**  
>New drivers are used by default when an Nvidia graphics card is detected but not compatible with the official drivers.
{.is-warning}

### NVIDIA

Nvidia graphics cards are compatible with the **official driver** in its version 460.67 which you can find the list of compatible graphics cards [here](https://gitlab.com/recalbox/recalbox-hardware/blob/master/videocard/nvidiacheckcompatibility-460.txt).

### Intel

List of graphics cards integrated to the processor.

Their [source code](https://cgit.freedesktop.org/xorg/driver/xf86-video-intel/tree/?id=b57abe20e81f4b8e4dd203b6a9eda7ff441bc8ce) is not very explicit about the supported graphics cards.

| Name | Status | Recalbox version | Notes |
| :---: | :---: | :---: | :---: |
| Intel® HD Graphics 630 | ✅ | 4.1 | Around 60 FPS for all consoles on x64 version with default parameters |
| Intel® HD Graphics 3000 | ✅ | 4.1 | Around 60 FPS for all consoles on x64 version with default parameters |
| Intel® Iris™ Plus Graphics 620 | ✅ | 4.1 |  |
| Intel® Iris™ Pro Graphics 580 | ✅ | 4.1 | Around 60 FPS for all consoles on x64 version with default parameters |
| Intel® HD Graphics 4600 | ✅ | 4.1 | Around 60 FPS for all consoles on x64 version with default parameters |
| Intel® HD Graphics 4400 | ✅ | 6.0 rc3 | Around 60 FPS for all consoles on x64 version with default parameters |
| Intel® HD Graphics 4850 | ✅ | 6.0 DragonBlaze | Around 60 FPS for all consoles on x64 version with default parameters |

### AMD

Their [source code](https://cgit.freedesktop.org/xorg/driver/xf86-video-amdgpu/tree/?h=xf86-video-amdgpu-1.4.0) is not very explicit about the supported graphics cards.

| Name | Status | Recalbox version | Notes |
| :--- | :---: | :---: | :---: |
| AMD Radeon HD 4850 512mo | ✅ | 6.0 Rc3 | work with CPU intel i5-3570K |
| AMD Radeon HD 4890 1Go | ✅ | 4.1 |  |
| AMD Radeon HD 6750M | ✅ | 4.1 | some minor graphic bugs on the menu |
| AMD Radeon HD 7970 3G | ❌ | 18.04.20 | Not working (menu is very slow and unplayable) |
| AMD Radeon HD 8280E | ❌ | 18.07.13 | Not working (menu is very slow and unplayable) |
| AMD Radeon RX 480 8gb | ❌ | 6.0 RC2 | Not working (menu is very slow and unplayable) |