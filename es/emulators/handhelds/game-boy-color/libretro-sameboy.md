---
title: Libretro SameBoy
description: 
published: true
date: 2024-07-25T22:18:27.624Z
tags: libretro, color, gameboy, sameboy, gbc
editor: markdown
dateCreated: 2023-09-11T13:14:16.608Z
---

**SameBoy** es un emulador de código abierto extremadamente preciso para Gameboy (DMG) y Gameboy Color (CGB) escrito en C.

* Soporta la emulación de Game Boy (DMG); Game Boy Color (CGB) y GBC-Mode; Game Boy Advance (AGB).
* Soporta emulación precisa y de alto nivel de Super Game Boy (SGB; NTSC y PAL) y Super Game Boy 2 (SGB2).
* Permite elegir el modelo que se desea emular desde cualquier ROM
* Audio de 96 kHz de alta calidad
* Soporta ahorro de batería
* Estado de guardado
* Incluye ROMs de arranque de código abierto para todos los modelos emulados:

  * Soporte completo para (y documentación de) todas las paletas específicas del juego en la ROM de arranque CGB / AGB, para una emulación precisa de los juegos de Game Boy en una Game Boy Color.
  * Soporte para la selección manual de paletas usando combinaciones de teclas, con 4 nuevas paletas adicionales (dirección A + B +)
  * Soporta selección de paleta en un juego CGB, forzándolo a ejecutarse en modo DMG "paletizado", si la ROM lo permite.
  * Soporta juegos con un logo que no sea de Nintendo en la cabecera.
  * No hay animación larga en la ROM de arranque DMG

* Cuatro ajustes de corrección de color
* Tres ajustes de filtro de audio de paso alto
* Emulación de reloj en tiempo real
* Modos turbo, rebobinado y cámara lenta
* Precisión extremadamente alta
* Emulación de cable de enlace

## ![](/emulators/license.svg) Licencia

El core posee la siguiente licencia [**MIT**](https://github.com/libretro/SameBoy/blob/master/LICENSE).

## ![](/emulators/compatibility.png) Compatibilidad

 | RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | 
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Funcionalidades

| Funcionalidad | ¿Soportada? |
| :---: | :---: |
| Reinicio | ✔ |
| Capturas de pantalla | ✔ |
| Grabaciones | ✔ |
| Grabaciones instantáneas | ✔ |
| Rebobinado | ✔ |
| Netplay | ✔ |
| Opciones del core | ✔ |
| RetroAchievements | ✔ |
| Controles | ✔ |
| Remapping | ✔ |
| Vibración | ✔ |
| Subsystem | ✔ |

## ![](/emulators/bios.svg) BIOS

### Lista de BIOS opcionales

| Nombre del fichero | Descripción | MD5 | ¿Preinstalada? |
| :---: | :---: | :---: | :---: |
| cgb_boot.bin | BIOS Game Boy Color | b560efd759d87634a03b243f22bba27a | ✅ |

### Emplazamiento

Disponed vuestras bios como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 gbc
┃ ┃ ┃ ┃ ┣ 🗒 **cgb_boot.bin**

## ![](/emulators/roms.png) Roms

### Extensiones de roms soportadas

Las roms deben tener las extensiones siguientes:

* .bin
* .gbc
* .zip
* .7z

Este sistema soporta las roms comprimidas en formato .zip/.7z. No obstante, se trata sólo de un archivo.

Los ficheros contenidos en los .zip/.7z deben tener las extensiones citadas previamente.
Además, cada fichero .zip/.7z sólo debe contener un fichero rom al interior.

### Emplazamiento

Disponed vuestras roms como en la siguiente imagen: 

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 gbc
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.zip**

>Se aconsejan las roms del proyecto **No-Intro**.
{.is-success}

>Para más información sobre las roms, consultad [este tutorial](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

>Para conservar las configuraciones personalizadas cuando se produce una actualización del sistema Recalbox, os aconsejamos utilizar las [Sobrecargas de configuración](./../../../advanced-usage/configuration-override).
{.is-info}

### Acceder a las opciones

Podéis configurar las opciones del core de dos formas diferentes.

* Via el Menú RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via el fichero `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Opciones del core

Utilizad este cuadro para el modo 1 jugador:

| Opción | Valor de la opción | Variable (para la sobrecarga) | Valores posibles |
| --- | --- | --- | --- |
| Emulated Model (Requires Restart) | `Auto Detect DMG/CGB` ✅ / `Auto Detect DMG/SGB/CGB` / `Game Boy (DMG-CPU B)` / `Game Boy Color (CPU-CGB C) (Experimental)` / `Game Boy Color (CPU-CGB E)` / `Game Boy Advance` / `Super Game Boy NTSC` / `Super Game Boy PAL` /`Super Game Boy 2` | `sameboy_model` | `Auto` / `Auto (SGB)` / `Game Boy` / `Game Boy Color C` / `Game Boy Color` / `Game Boy Advance` / `Super Game Boy` / `Super Game Boy PAL` / `Super Game Boy 2` |
| Auto Detected SGB Model (Requires Restart) | `Super Game Boy NTSC` ✅ / `Super Game Boy PAL` / `Super Game Boy 2` | `sameboy_auto_sgb_model` | `Super Game Boy` / `Super Game Boy PAL` / `Super Game Boy 2` |
| Real Time Clock Emulation | `Sync to System Clock` ✅ / `Accurate` | `sameboy_rtc` | `sync to system clock` / `accurate` |
| GB Mono Palette | `Greyscale` ✅ / `Lime (Game Boy)` / `Olive (Game Boy Pocket)` / `Teal (Game Boy Light)` | `sameboy_mono_palette` | `greyscale` / `lime` / `olive` / `teal` |
 | GBC Color Correction | `Modern - Balanced` ✅ / `Modern - Accurate` / `Modern - Boost Contrast` / `Reduce Contrast` / `Correct Color Curves` / `Harsh Reality (Low Contrast)` / `Désactivé` | `sameboy_color_correction_mode` | `emulate hardware` / `accurate` / `preserve brightness` / `reduce contrast` / `correct curves` / `harsh reality` / `off` |
| Ambient Light Temperature | `1000K (Warmest)` / `1550K` / `2100K` / `2650K` / `3200K` / `3750K` / `4300K` / `4850K` / `5400K` / `5950K` / `6500K (Neutral White)` ✅ / `7050K` / `7600K` / `8150K` / `8700K` / `9250K` / `9800K` / `10350K` / `10900K` / `11450K` / `12000K (Coolest)` | `sameboy_light_temperature` | `1.0` / `0.9` / `0.8` / `0.7` / `0.6` / `0.5` / `0.4` / `0.3` / `0.2` / `0.1` / `0` / `-0.1` / `-0.2` / `-0.3` / `-0.4` / `-0.5` / `-0.6` / `-0.7` / `-0.8` / `-0.9` / `-1.0` |
| Display border | `Always` / `Only for Super Game Boy` ✅ / `Désactivé` | `sameboy_border` | `always` / `Super Game Boy only` / `never` |
| Highpass Filter | `Accurate` ✅ / `Preserve Waveform` / `Désactivé` | `sameboy_high_pass_filter_mode` | `accurate` / `remove dc offset` / `off` |
| Interference Volume | `0%` ✅ / `5%` / `10%` / `15%` / `20%` / `25%` / `30%` / `35%` / `40%` / `45%` / `50%` / `55%` / `60%` / `65%` / `70%` / `75%` / `80%` / `85%` / `90%` / `95%` / `100%` | `sameboy_audio_interference` | `0` / `5` / `10` / `15` / `20` / `25` / `30` / `35` / `40` / `45` / `50` / `55` / `60` / `65` / `70` / `75` / `80` / `85` / `90` / `95` / `100` |
| Rumble Mode | `Always` / `Only for rumble-enabled games` ✅ / `Never` | `sameboy_rumble` | `all games` / `rumble-enabled games` / `never` |

Utilizad este cuadro para los modos multi-jugadores:

| Opción | Valor de la opción | Variable (para la sobrecarga) | Valores posibles |
| --- | --- | --- | --- |
| Link cable emulation | `Activé` ✅ / `Désactivé` | `sameboy_link` | `enabled` / `disabled` |
| Screen Layout | `Top-Down` ✅ / `Left-Right` | `sameboy_screen_layout` | `top-down` / `left-right` |
| Audio Output | `Game Boy #1` ✅ / `Game Boy #2` | `sameboy_audio_output` | `GameBoy #1` / `GameBoy #2` |
| Emulated model for Game Boy #1 | `Auto Detect DMG/CGB` ✅ / `Auto Detect DMG/SGB/CGB` / `Game Boy (DMG-CPU B)` / `Game Boy Color (CPU-CGB C) (Experimental)` / `Game Boy Color (CPU-CGB E)` / `Game Boy Advance` / `Super Game Boy NTSC` / `Super Game Boy PAL` /`Super Game Boy 2` | `sameboy_model_1` | `Auto` / `Auto (SGB)` / `Game Boy` / `Game Boy Color C` / `Game Boy Color` / `Game Boy Advance` / `Super Game Boy` / `Super Game Boy PAL` / `Super Game Boy 2` |
| Auto Detected SGB Model for Game Boy #1 (Requires Restart) | `Super Game Boy NTSC` ✅ / `Super Game Boy PAL` / `Super Game Boy 2` | `sameboy_auto_sgb_model_1` | `Super Game Boy` / `Super Game Boy PAL` / `Super Game Boy 2` |
| Emulated model for Game Boy #2 | `Auto Detect DMG/CGB` ✅ / `Auto Detect DMG/SGB/CGB` / `Game Boy (DMG-CPU B)` / `Game Boy Color (CPU-CGB C) (Experimental)` / `Game Boy Color (CPU-CGB E)` / `Game Boy Advance` / `Super Game Boy NTSC` / `Super Game Boy PAL` /`Super Game Boy 2` | `sameboy_model_2` | `Auto` / `Auto (SGB)` / `Game Boy` / `Game Boy Color C` / `Game Boy Color` / `Game Boy Advance` / `Super Game Boy` / `Super Game Boy PAL` / `Super Game Boy 2` |
| Auto Detected SGB Model for Game Boy #2 (Requires Restart) | `Super Game Boy NTSC` ✅ / `Super Game Boy PAL` / `Super Game Boy 2` | `sameboy_auto_sgb_model_2` | `Super Game Boy` / `Super Game Boy PAL` / `Super Game Boy 2` |
| GB Mono Palette for Game Boy #1 | `Greyscale` ✅ / `Lime (Game Boy)` / `Olive (Game Boy Pocket)` / `Teal (Game Boy Light)` | `sameboy_mono_palette_1` | `greyscale` / `lime` / `olive` / `teal` |
| GB Mono Palette for Game Boy #2 | `Greyscale` ✅ / `Lime (Game Boy)` / `Olive (Game Boy Pocket)` / `Teal (Game Boy Light)` | `sameboy_mono_palette_2` | `greyscale` / `lime` / `olive` / `teal` |
 | GBC Color Correction for Game Boy #1 | `Modern - Balanced` ✅ / `Modern - Accurate` / `Modern - Boost Contrast` / `Reduce Contrast` / `Correct color Curves` / `Harsh Reality (Low Contrast)` / `Désactivé` | `sameboy_color_correction_mode_1` | `emulate hardware` / `accurate` / `preserve brightness` / `reduce contrast` / `correct curves` / `harsh reality` / `off` |
 | GBC Color Correction for Game Boy #2 | `Modern - Balanced` ✅ / `Modern - Accurate` / `Modern - Boost Contrast` / `Reduce Contrast` / `Correct color Curves` / `Harsh Reality (Low Contrast)` / `Désactivé` | `sameboy_color_correction_mode_2` | `emulate hardware` / `accurate` / `preserve brightness` / `reduce contrast` / `correct curves` / `harsh reality` / `off` |
| Ambient Light Temperature for Game Boy #1 | `1000K (Warmest)` / `1550K` / `2100K` / `2650K` / `3200K` / `3750K` / `4300K` / `4850K` / `5400K` / `5950K` / `6500K (Neutral White)` ✅ / `7050K` / `7600K` / `8150K` / `8700K` / `9250K` / `9800K` / `10350K` / `10900K` / `11450K` / `12000K (Coolest)` | `sameboy_light_temperature_1` | `1.0` / `0.9` / `0.8` / `0.7` / `0.6` / `0.5` / `0.4` / `0.3` / `0.2` / `0.1` / `0` / `-0.1` / `-0.2` / `-0.3` / `-0.4` / `-0.5` / `-0.6` / `-0.7` / `-0.8` / `-0.9` / `-1.0` |
| Ambient Light Temperature for Game Boy #2 | `1000K (Warmest)` / `1550K` / `2100K` / `2650K` / `3200K` / `3750K` / `4300K` / `4850K` / `5400K` / `5950K` / `6500K (Neutral White)` ✅ / `7050K` / `7600K` / `8150K` / `8700K` / `9250K` / `9800K` / `10350K` / `10900K` / `11450K` / `12000K (Coolest)` | `sameboy_light_temperature_2` | `1.0` / `0.9` / `0.8` / `0.7` / `0.6` / `0.5` / `0.4` / `0.3` / `0.2` / `0.1` / `0` / `-0.1` / `-0.2` / `-0.3` / `-0.4` / `-0.5` / `-0.6` / `-0.7` / `-0.8` / `-0.9` / `-1.0` |
| Highpass Filter for Game Boy #1 | `Accurate` ✅ / `Preserve Waveform` / `Désactivé` | `sameboy_high_pass_filter_mode_1` | `accurate` / `remove dc offset` / `off` |
| Highpass Filter for Game Boy #2 | `Accurate` ✅ / `Preserve Waveform` / `Désactivé` | `sameboy_high_pass_filter_mode_2` | `accurate` / `remove dc offset` / `off` |
| Interference Volume for Game Boy #1 | `0%` ✅ / `5%` / `10%` / `15%` / `20%` / `25%` / `30%` / `35%` / `40%` / `45%` / `50%` / `55%` / `60%` / `65%` / `70%` / `75%` / `80%` / `85%` / `90%` / `95%` / `100%` | `sameboy_audio_interference_1` | `0` / `5` / `10` / `15` / `20` / `25` / `30` / `35` / `40` / `45` / `50` / `55` / `60` / `65` / `70` / `75` / `80` / `85` / `90` / `95` / `100` |
| Interference Volume for Game Boy #2 | `0%` ✅ / `5%` / `10%` / `15%` / `20%` / `25%` / `30%` / `35%` / `40%` / `45%` / `50%` / `55%` / `60%` / `65%` / `70%` / `75%` / `80%` / `85%` / `90%` / `95%` / `100%` | `sameboy_audio_interference_2` | `0` / `5` / `10` / `15` / `20` / `25` / `30` / `35` / `40` / `45` / `50` / `55` / `60` / `65` / `70` / `75` / `80` / `85` / `90` / `95` / `100` |
| Rumble Mode for Game Boy #1 | `Always` / `Only for rumble-enabled games` ✅ / `Never` | `sameboy_rumble_1` | `rumble-enabled games` / `all games` / `never` |
| Rumble Mode for Game Boy #2 | `Always` / `Only for rumble-enabled games` ✅ / `Never` | `sameboy_rumble_2` | `rumble-enabled games` / `all games` / `never` |

## ![](/emulators/external-links.png) Enlaces externos

* **Código fuente utilizado** : [https://github.com/libretro/SameBoy/](https://github.com/libretro/SameBoy/)
* **Documentación Libretro** : [https://docs.libretro.com/library/sameboy/](https://docs.libretro.com/library/sameboy/)
* **Código fuente oficial** : [https://github.com/LIJI32/SameBoy](https://github.com/LIJI32/SameBoy/)
* **Site officiel** : [https://sameboy.github.io](https://sameboy.github.io)