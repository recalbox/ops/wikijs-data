---
title: Libretro Wasm4
description: 
published: true
date: 2024-07-24T20:57:18.112Z
tags: libretro, 9.1+, wasm4
editor: markdown
dateCreated: 2023-09-27T06:36:51.594Z
---

**Libretro Wasm4** is a fantasy console emulator using **WebAssembly**.

## ![](/emulators/license.svg) License

This core is under [**ISC**](https://github.com/aduros/wasm4/blob/main/LICENSE.txt) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Features

| Feature | Supported |
| :---: | :---: |

## ![](/emulators/bios.svg) BIOS

>**No bios is required.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Supported extensions

Roms must have the extension:

* .wasm

### Location

Put the roms like this: 

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 wasm4
┃ ┃ ┃ ┃ ┣ 🗒  **jeu.wasm**

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>In order to keep your customized configurations during an upgrade, we recommend you use our [Configuration overrides](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Accessing options

You can configure various options in two different ways.

* Via the RetroArch Menu:

┣ 📁 RetroArch Menu
┃ ┣ 📁 Core options
┃ ┃ ┣ 🧩 Name_option

* Via the `retroarch-core-options.cfg` file:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core options

| Options | Option values | Variable (for override) | Variable values |
| --- | --- | --- | --- |
| Pixel type | `xrgb8888` ✅ / `rgb565` | `wasm4_pixel_type` | `xrgb8888` / `rgb565` |
| Audio type | `callback` ✅ / `normal` | `wasm4_audio_type` | `callback` / `normal` |
| Frames to hold touch value | `5` / `10` ✅ / `20` / `30` | `wasm4_touchscreen_hold_frames` | `5` / `10` / `20` / `30` |

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/aduros/wasm4/](https://github.com/aduros/wasm4/)