---
title: Roms and isos
description: 
published: true
date: 2021-08-08T13:51:05.018Z
tags: roms, isos
editor: markdown
dateCreated: 2021-08-07T15:22:42.331Z
---

## What is a rom / an iso

The emulation of a game consists in reproducing on a machine the behavior of a game which was programmed for another type of machine.

On the appropriate machine (console or arcade), the program is not emulated, we say that the game works in _**native**_.   
It is usually stored in read-only memory (on the motherboard or in plug-in cartridges); their content is thus called ROM (read only memory) in common usage.

An emulator works in the same way as this machine, by loading this ROM and executing the program (game) stored in it, adapting to the available resources (input/output devices, execution speed, etc.).

More recently, the content of games stored on CD-ROM or DVD can be copied into an image file called ISO; on the same principle as ROM emulation, one can emulate a game stored in an ISO file.

There are often several versions of the same dump (backup) of a ROM, but most of them are obsolete. Some versions may be incorrect dumps, hacks, fakes, overdumps, underdumps, etc... 

A rom set is more often called **romset**.

### Dat file

A dat file is a file containing a lot of information about a given domain. In our case, it is a file that lists all the known sets per system.

This dat file contains basic information and a whole bunch of sets with their signatures in MD5, SHA1 and/or MD5 format.

These files can be used with a specific utility such as [clrmamepro](./../../utilities/rom-management/clrmamepro), [Romulus Rom Manager](./../../utilities/rom-management/romulus-rom-manager), Romcenter, Romvault, etc.

### Header file

A header file is the first 16 bytes of a ROM that can be read by an emulator. It tells the emulator the size of the ROM, the region of the game, the mapper it uses, the type of mirroring it has, the console it is intended for (main console or VS. system), etc. Some emulators will not allow you to run the game if it does not have a header.

On SNES, these were the headers created by the copiers of the time. They are useless nowadays and were only used to "sign" and lock the ROMs on the copier. For users, the headered ROMs correspond to ROMs that were extracted with old copiers such as Super Wild Card (SWC extension), Super Magic Card (SMC format). These ROMs are no longer relevant today and no longer correspond to a reality, but it is sometimes necessary for hacks made at that time (late 90s to mid-2000s). In order to keep some compatibility for old emulators like ZSNES or old versions of Snes9X. The unheadered versions don't have this information from the copiers and correspond to the content of the chips on the PCBs. 

For the NES, it's different, we are talking about iNES headers, which is a format that has been designed to schematize the different types of PCBs and revisions of existing cartridges, especially co-processors (like MMC chips) or sound processors (like Konami VRCx). Emulators need them to describe the ROMs and how to run them. But newer emulators see it differently, it is not the ROM that should have this information, but a database in the emulator (or next to it), containing this additional information. The ROM should only contain the code from the physical chips, nothing else. Emulators like Nestopia or Mesen, have these databases, and use CRC32 calculations to find the right ROM with the right descriptor in the database.