---
title: 4. 🔧 HARDWARE KOMPATIBILITÄT
description: 
published: true
date: 2023-07-18T11:57:42.632Z
tags: 
editor: markdown
dateCreated: 2021-06-21T11:45:56.935Z
---

Auf den folgenden Seiten kannst du überprüfen, ob deine Hardware getestet wurde und unterstützt wird, sowie überprüfen, ob ein bestimmtes System oder ein Emulator mit deiner Hardware kompatibel ist.