---
title: FAQ
description: 
published: true
date: 2024-07-25T21:47:32.506Z
tags: faq
editor: markdown
dateCreated: 2021-08-08T06:17:49.120Z
---

## Systems

Amiga machines have been separated into 4 systems:

* Amiga 600: all `OCS`/`ECS` machines of Amiga 1000 unitl the last Amiga 600.
* Amiga 1200: all `AGA` machines, starting from Amiga 1200.
* Amiga CD32: Amiga CD32 console.
* Amiga CDTV: Amiga CDTV console.

## Extensions

Here is a list with explanations of the differents file formats for Amiga systems:

* Amiga 600 and Amiga 1200:
  * **ADF** discs (`*.adf`), the most popular disk format (cannot integrate disk protections). Can be zipped or 7-zipped.
  * **IPF** discs (`*.ipf`), most commonly used by non-intro game copies (may incorporate disk protections). Can be zipped.
  * **WHD** (folders, `*.lha/lzh/lzx`, `*.zip`), the well-known format for hard disk games. WHD folders are recognized using the `*.slave` file inside the folder, but it is strongly recommended to keep them packaged as `lha` or `zip`. Unpacked WHDs are not faster and even worse, they can make the frontend super slow.
  * **HDD FS**, hard drive on filesystem. Must be a folder ending with `.hd` extension. Rarely used. For true Amiga fans who have backed up their machine's hard drive. Can be zipped, but not recommended. 
  * **HDF** (`*.hdf`), Disk image in a single file. Can be zipped.
  * **RP9** (`*.rp9`), Amiga Forever's all-in-one packages.
* Amiga CD32 and Amiga CDTV:
  * **ISO** (`*.iso`), CD copies.
  * **BIN/CUE** (`*.bin` / `*.cue`), CD copies. Can have 1 `cue` file and one or multiple `bin` files.

When you play from zipped/7-zipped files, the system tries to identify the type of rom by looking for specific extensions inside the archive.

* `ADF`/`IPF`/`BIN`/`ISO` extensions are fast to identify.
* `WHD` is fast, but LHA should be the preferred option.
* `HDDFS` may take longer to identify and may result in false interpretations. If you use `HDDFS`, leave it as normal files and folders and end the root folder name with `.hd` for easy identification.

## Special commands with Amiberry

### Keyboard

* Enter into the emulator interface: `F12`.
* Quit: `F12`, go down to `Quit` and validate with `ENTER`.

### Controller

* Enter into the emulator interface: `HK` + `B`.
* Quit: `HK` + `START`.

## Configuration overrides

>The configuration overrides on Libretro cores are [configuration overrides](../../../../advanced-usage/configuration-override). Here, only Amiberry will be dealt with.
{.is-info}

As seen above, a huge amount of work has been done to automatically configure emulators according to what you want to boot.

Recalbox generates one configuration per system. However, in certain situations, you can override these configurations.

The configuration files are generated in `/tmp/amiga/conf`. The most important is the `uaeconfig.uae` file in which you will find all the keys defining the machine, the hard disks, etc.

If you are curious, you can have a look at the Amiberry documentation.

You have 2 ways of overriding the configurations with an example for each case:

### By system

To do this, you need to create a file in the `/recalbox/share/system/configs/amiberry/` folder. This file will be named according to the system:

* `amiga600.uae` to override the Amiga 600
* `amiga1200.uae` to override the Amiga 1200
* `amigacd32.uae` to override the Amiga CD32
* `amigacdtv.uae` to override the Amiga CDTV

For example, to disable the LED display in the bottom right-hand corner, you can use this line:

```txt
show_leds=false
```

### Per game

To do this, you need to create a file in the roms folder where the game to be overrided is located, ending with the `.uae` extension.

For example, in your `/recalbox/share/roms/amiga1200` folder, for the game `Aladdin (AGA).ipf`, you can create the file `Aladdin (AGA).uae` and place your overrides in it.

Overrides are handled as follows:

* First, the base configuration is generated.
* Then, if you have an override for the system, the keys found in the override file will overwrite those in the base configuration.
* Finally, if you have an override for the game you are running, the keys found in the override file will overwrite those in the base configuration AND the system override.

## How ADF games work

Copy them to your Amiga roms folder corresponding to the system.

Multiple discs are automatically loaded, up to 4 discs for games using the correct nomenclature such as `(Disk 1 of X)`.

## WHow HD games work

WHD games are a little more difficult to use, but nothing too complicated.

### Installation

Unzip your WHDLoad game, delete the `.info` file at the same level as the folder, and copy only this folder to your roms folder (you can use subfolders if you wish, for example `/recalbox/share/roms/amiga1200/ShootEmUp`).

### Usage

You will need a `.uae` file at the same level as the folder of the same name. There are two ways of doing this:

* Create an empty one.
* Use a custom file that will allow you to modify the emulator's configuration.

For the second method, there is a little utility called [UCG](https://github.com/solarmon/UCG). This will allow you to generate a UAE configuration.

### Modifying WHDL games

#### Modifying .UAE files

In the uae file, you can define `;hardware`, `;controls` and `;graphics` blocks to replace the standard emulator configuration.

The customised parts of the configuration in the `.uae` file will only be used if they start with the correct block name: `;hardware`, `;controls` or `;graphics`.

This can allow you, for example, to use two joysticks, to have a very specific hardware configuration for certain games that are a bit special/capricious or to define a custom resolution.

You can also delete everything in the `.uae` file and the basic emulator configuration will then be used.

Copy this (corresponding to the Amiga model you wish to use) next to the WHDL game folder and rename it exactly as the game folder (keeping the `.uae` extension). For example, if your game's folder name is `Rick Dangerous`, your uae file should be named `Rick Dangerous.uae` and be at the same level as the folder.

#### Modifying the start-up sequence (expert level)

The standard startup sequence generated for WHDL games is `WHDload game.slave Preload` (in `WHDL/S/Startup-Sequence`). But a few WHDL games (such as `History Line: 1914-1918`) may require additional parameters at startup or they will crash.

You can add a second file (completely optional) containing these additional parameters. Taking History Line as an example, you would have :

* HistoryLine (WHDL game folder)
* HistoryLine.uae (which can be empty or custom)
* HistoryLine.whdl (optional, containing additional parameters `CUSTOM1=1`)

The game will be launched at this point.

Here is a small list of games that require additional parameters.

| Game | Extra parameter(s) / File contents |
| :--- | :- |
| HistoryLine 1914-1918 | `CUSTOM1=1` for the intro, `CUSTOM1=2` for the game. |
| The Settlers / Die Siedlers | `CUSTOM1=1` to skip the intro. |