---
title: Le "Lightgun" de Recalbox
description: Le lightgun pour tous ! ;-)
published: true
date: 2021-09-04T17:51:57.792Z
tags: lightgun, 7.2+
editor: markdown
dateCreated: 2021-05-21T07:53:31.272Z
---

![](/basic-usage/features/lightgun/lightgun.png)

## Quel est le principe ?

Le principe est de configurer automatiquement les jeux compatible lightgun et cela pour la plupart des systèmes supportés par Recalbox. Pour des raisons de coût et de disponibilité, on veut pouvoir jouer avec une Wiimote (ou deux pour les jeux 2 joueurs). On pourra aussi brancher un Nunchunk pour déporter le tir primaire et secondaire selon notre préférence. Ce mode lightgun s'active automatiquement dès lors que l'on a une Mayflash dolphin bar en mode 2 et uniquement pour les jeux supportant ce mode.

La fonctionnalité Lightgun de Recalbox vous permet, enfin, de rejouer aux jeux de pistolets bazooka et autres dispositifs de pointages rétros !

**Systèmes supportant déjà cette fonctionnalité dans Recalbox :**

* 3DO
* Atomiswave
* Dreamcast
* Fbneo
* Mame
* Master System
* Megadrive
* Mega-CD
* Naomi
* Naomi GD ROM System
* NES
* Playstation 1
* Saturn
* SNES

>Les cores de l'émulateur libretro sont supportés à ce jour, pas encore d'émulateur ‘standalone’, cela viendra peut être par la suite.
{.is-info}

>ATTENTION : cela ne marchera que si les jeux sont scrappés avec des noms bien identifiables comme `duck hunt (world) v1.1` et pas `dhnes` !!! On doit pouvoir retrouver le terme "duck hunt" dans le nom.
{.is-warning}

## Quels sont les pré-requis matériel ?

![Une à deux Nintendo Wiimotes (ou copie compatible)](/basic-usage/features/lightgun/wiimote.png)

![Une à deux Mayflash dolphin bar](/basic-usage/features/lightgun/dolphinbar.png)

>**Il faudra une Mayflash dolphin bar par Wiimote en mode lightgun (c'est comme si la Wiimote devenait une souris USB)**
{.is-warning}

![Optionnel : Nunchuck si on veut tirer avec "C" & "Z"](/basic-usage/features/lightgun/nunchuck.png)

![](/basic-usage/features/lightgun/gun1.png)![](/basic-usage/features/lightgun/gun2.png)

![Optionnel : on peut aussi s'équiper pour être plus confortable](/basic-usage/features/lightgun/crossbow.png)

## Comment installer ?

### 1. Brancher la ou les Mayflash dolphin bar en usb sur votre Recalbox

![](/basic-usage/features/lightgun/usbports.png)

### 2. Mettre en mode 2 sur la ou les Mayflash dolphin bar

![Appuyer sur le bouton mode jusqu'à avoir la led bleu en face du chiffre 2](/basic-usage/features/lightgun/dolphinbarmode2.png)

### 3. Synchroniser la Wiimote avec la Mayflash dolphin bar

![Appuyer sur le bouton sync et une led bleu clignote](/basic-usage/features/lightgun/dolphinbarsync.png)

![Ensuite il faut appuyer sur le bouton Sync de la Wiimote](/basic-usage/features/lightgun/wiimotesync.png)

>Si vous voulez jouer à 2, il faudra lier une Wiimote à chaque Mayflash dolphin bar de la même manière qu'expliqué précédemment.
{.is-info}

## Comment l'activer ?

Ce mode lightgun s'active automatiquement dès lors que l'on a une Mayflash dolphin bar en mode 2 et uniquement pour les jeux supportant ce mode.

>Ce mode 'Lightgun' est donc activable/désactivable juste en allumant/éteignant la Mayflash dolphin bar mais honnêtement, ce n'est pas nécessaire de changer à moins de vouloir alterner mode lightgun et mode manette pour un même jeu.
{.is-info}

## Et finalement… comment jouer ?

### Les commandes par défaut de la Wiimote

Start : ![](/basic-usage/features/lightgun/button_plus.png) pour démarrer un mode de jeu / mettre en pause dans certains jeux

Select : ![](/basic-usage/features/lightgun/button_minus.png) pour sélectionner un mode de jeu / mettre des crédits

Home : ![](/basic-usage/features/lightgun/button_home.png) pour sortir du jeu et revenir sur le menu de sélection des jeux

Tir principal : ![](/basic-usage/features/lightgun/button_z.png) sur le Nunchuk

Tir secondaire :  ![](/basic-usage/features/lightgun/button_c.png) sur le Nunchuk 

### Remarques sur les boutons de la Wiimote

>* D'autres touches peuvent être configurées pour certains systèmes sur la croix directionnelles par exemple (même si on essai d'éviter ces cas)
>* La touche ![](/basic-usage/features/lightgun/button_a.png) pour les crédits.
>* De plus certains jeux peuvent se lancer avec ![](/basic-usage/features/lightgun/button_a.png) par exemple.
>* Des jeux demanderont aussi d'appuyer sur 2 touches en même temps pour entrer en jeu ou pour sortir/rentrer dans un menu.
>* Les touches directionnels gauche ![](/basic-usage/features/lightgun/stick.png) se comportera comme la croix directionnelle de la Wiimote.
>* La touche "power" ![](/basic-usage/features/lightgun/button_power.png) peut servir à éteindre la Wiimote par un appuie long.
>* Les boutons suivants ne sont pas fonctionnels en mode lightgun : ![](/basic-usage/features/lightgun/button_1.png) et ![](/basic-usage/features/lightgun/button_2.png)
>* Si le Nunchuck est branché, les boutons ![](/basic-usage/features/lightgun/button_a.png) et ![](/basic-usage/features/lightgun/button_b.png) sont inversés sur la Wiimote.
{.is-info}

## Le système virtuel Lightgun

Vous pouvez activer un système virtuel Lightgun pour rassembler tout ces jeux.  
Pour l'activer, [regardez ici](./../../basic-usage/features/virtual-systems).

![ENJOY !!!](/basic-usage/features/lightgun/duckhunt.png)