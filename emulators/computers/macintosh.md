---
title: Macintosh
description: 
published: true
date: 2023-02-26T18:05:09.795Z
tags: computers, apple, macintosh, 9.0+
editor: markdown
dateCreated: 2023-02-26T18:03:04.387Z
---

![](/emulators/computers/macintosh.svg){.align-center}

## Technical data

* **Manufacturer** : Apple Computer, Inc.
* **Year of release** : 1984
* **OS** : System Software 1.0
* **CPU** : Motorola 68000 @ 7.8336 MHz
* **RAM** : 128 KB
* **ROM** : 64 KB
* **Sound** : 8 TTL chips implementing a video and sound DMA controller
* **Display** : 9" Monochrome built-in (512×342 pixels)

## Presentation

The Mac (known as Macintosh until 1999) is a family of personal computers designed and marketed by Apple Inc. Macs are known for their ease of use and minimalist designs, and are popular among students, creative professionals, and software engineers. The current lineup includes the MacBook Air and MacBook Pro laptops, as well as the iMac, Mac Mini, Mac Studio and Mac Pro desktops. Macs run the macOS operating system.

The first Mac was released in 1984, and was advertised with the highly-acclaimed "1984" ad. After a period of initial success, the Mac languished in the 1990s, until co-founder Steve Jobs returned to Apple in 1997. Jobs oversaw the release of many successful products, unveiled the modern Mac OS X, completed the 2005-06 Intel transition, and brought features from the iPhone back to the Mac. During Tim Cook's tenure as CEO, the Mac underwent a period of neglect, but was later reinvigorated with the introduction of popular high-end Macs and the ongoing Apple silicon transition, which brought the Mac to the same ARM architecture as iOS devices.

## Emulators

[Libretro MinivMac](libretro-minivmac)