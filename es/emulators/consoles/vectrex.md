---
title: Vectrex
description: Milton Bradley Vectrex
published: true
date: 2024-07-26T08:06:54.635Z
tags: gce, vectrex, mb, consolas
editor: markdown
dateCreated: 2023-09-03T20:29:18.584Z
---

![](/emulators/consoles/vectrex.svg){.align-center}

## Datos técnicos

* **Fabricante**: General Consumer Electronics / MB
* **Año de lanzamiento**: 1982
* **Procesador**: Motorola 68A09 a 1,6 MHz
* **Pantalla**: Samsung 240RB40 de 9" en blanco y negro.
* **RAM**: 1 KB (dos chips 2114 de 4 bits)
* **ROM**: 8 KB (un chip 2363 de 8 bits)
* **Chip de sonido**: 3 canales mediante General Instrument AY-3-8912
* **Entradas / salidas**: MOS Technology 6522
* **Medios**: cartucho de 32 KB

## Presentación

La **Vectrex** es una videoconsola de 8 bits desarrollada por Smith Engineering y distribuida por General Consumer Electronics, y más tarde por MB (Milton Bradley Company) tras la adquisición de GCE.
Salió al mercado a finales de 1982 y dejó de fabricarse en 1984, tras la crisis de los videojuegos de 1983.

Desarrollada por Jay Smith, de Western Technology & Smith Engineering, la Vectrex era una consola de 8 bits con pantalla propia. Ofrecida inicialmente con una pantalla de 5" a varios distribuidores, sólo GCE se interesó a condición de que la pantalla se ampliara a 9".

La Vectrex salió a la venta en noviembre de 1982 a un precio de 199 dólares en EE.UU., frente a las Atari 2600 y 5200 y la Intellivision, que eran consolas que podían conectarse a un televisor. La Vectrex, sin embargo, tenía su propia pantalla sin tener que ocupar espacio bajo el televisor familiar, ya que fue diseñada desde el principio como una miniconsola arcade.

A principios de 1983, MB compra GCE y se convierte en distribuidor de la consola. Frente al éxito de Mattel y su Intellivision, MB lanzó la Vectrex en Europa.

## Emuladores

[Libretro-vecx](libretro-vecx)