---
title: Utilización de interfaz con el teclado / ratón
description: 
published: true
date: 2024-07-26T07:11:49.998Z
tags: emulationstation, teclado, ratón
editor: markdown
dateCreated: 2023-09-18T09:52:16.094Z
---

Si vuestro mando se estropea y no podéis navegar por los menús de interfaz, que sepáis que también se puede navegar utilizando el teclado y un ratón.

## Comandos del ratón

Con un ratón podéis realizar lo siguiente:

* A : clic derecho
* B : clic izquierdo

## Comandos del teclado

Con un teclado enchufado a vuestra Recalbox podéis enviar los siguientes comandos a EmulationStation:

| Pulsación del mando | Tecla equivalente del teclado |
| ------------------- | ----------------------------- |
| A | S |
| B | A |
| X | P |
| Y | L |
| Start | ENTER |
| Select | SPACE |
| L1 | Page Up (⇞) |
| R1 | Page Down (⇟) |
| Hotkey | ESC |



