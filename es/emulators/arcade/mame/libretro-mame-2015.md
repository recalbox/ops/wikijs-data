---
title: Libretro MAME 2015
description: 
published: true
date: 2024-06-29T17:37:21.478Z
tags: libretro, mame, mame2015
editor: markdown
dateCreated: 2023-09-05T15:24:55.610Z
---



## ![](/emulators/license.svg) Licencia

El core posee la licencia **MAME no-commercial**

## ![](/emulators/compatibility.png) Compatibilidad

 | RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
 | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
 | ❌ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Funcionalidades

| Funcionalidad | ¿Soportada? |
| :---: | :---: |
| Saves |  |
| States |  |
| Rewind |  |
| Netplay |  |
| RetroAchievements |  |
| RetroArch Cheats |  |
| Native Cheats |  |
| Controllers |  |
| Multi-Mouse |  |
| Rumble |  |
| Sensors |  |
| Camera |  |
| Location |  |
| Subsystem |  |

## ![](/emulators/bios.svg) BIOS

Las romsets de BIOS no son necesarias si utilizáis las romsets de arcade "Full Non-Merged". Para las romset de tipo "Split" y "Non-Merged", disponed las BIOS necesarias en la misma carpeta que la romset del juego.

>**Aviso** :
>Poned las bios dentro del directorio : `/recalbox/share/roms/mame/`   
>o dentro de una subcarpeta si utilizáis un directorio diferente por core Mame, por ejemplo: `/recalbox/share/roms/mame/Mame2015/`
Los juegos junto con sus bios deben encontrarse dentro del mismo directorio o subdirectorio.
{.is-warning}

## ![](/emulators/roms.png) Roms

* Basado en el romset: MAME 0.160 (Mars 2015)
* Fichero dat : el fichero dat se encuentra en vuestra Recalbox dentro del directorio `/recalbox/share/arcade/libretro/mame2015.dat`.

### Emplazamiento

Disponed vuestras roms como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 mame
┃ ┃ ┃ ┃ ┣ 🗒 **juego.zip**
┃ ┃ ┃ ┃ ┣ 📁 **juego**
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **juego.chd**

También podéis utilizar el subdirectorio mame2015, si queréis clasificar vuestros fullsets por el core que las hace funcionar. Si utilizáis este subdirectorio vuestros juegos se lanzarán automáticamente bajo el emulador mame2015.


┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 mame
┃ ┃ ┃ ┃ ┣ 📁 mame2015
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **jeu.zip**
┃ ┃ ┃ ┃ ┃ ┣ 📁 **jeu**
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 **jeu.chd**

## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

>Para conservar las configuraciones personalizadas cuando se produce una actualización del sistema Recalbox, os aconsejamos utilizar las [Sobrecargas de configuración](./../../../advanced-usage/configuration-override).
{.is-info}

### Acceder a las opciones

Podéis configurar las opciones del core de dos formas diferentes.

* Via el Menú RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via el fichero `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Opciones del core

| Opción | Valor de la opción | Variable (para la sobrecarga) | Valores posibles |
| --- | --- | --- | --- |
| Read configuration | `Désactivé` ✅ / `Activé` | `mame2015_read_config` | `disabled` / `enabled` |
| Auto save/load states | `Désactivé` ✅ / `Activé` | `mame2015_auto_save` | `disabled` / `enabled` |
 | XY device (Restart) | `none` ✅ / `lightgun` / `mouse` | `mame2015_mouse_mode` | `none` / `lightgun` / `mouse` |
| Enable throttle | `Désactivé` ✅ / `Activé` | `mame2015_throttle` | `disabled` / `enabled` |
| Enable cheats | `Désactivé` ✅ / `Activé` | `mame2015_cheats_enable` | `disabled` / `enabled` |
| Hide nag screen | `Désactivé` ✅ / `Activé` | `mame2015_hide_nagscreen` | `disabled` / `enabled` |
| Hide gameinfo screen | `Désactivé` ✅ / `Activé` | `mame2015_hide_infoscreen` | `disabled` / `enabled` |
| Hide warnings screen | `Désactivé` ✅ / `Activé` | `mame2015_hide_warnings` | `disabled` / `enabled` |
| Alternate render method | `Désactivé` ✅ / `Activé` | `mame2015_alternate_renderer` | `disabled` / `enabled` |
| Boot to OSD | `Désactivé` ✅ / `Activé` | `mame2015_boot_to_osd` | `disabled` / `enabled` |
| Boot from CLI | `Désactivé` ✅ / `Activé` | `mame2015_boot_from_cli` | `disabled` / `enabled` |

## ![](/emulators/external-links.png) Enlaces externos

* **Progettosnaps** : [https://www.progettosnaps.net/](https://www.progettosnaps.net/)
* **Código fuente utilizado** : [https://github.com/libretro/mame2015-libretro/](https://github.com/libretro/mame2015-libretro)