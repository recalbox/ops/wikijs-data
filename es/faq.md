---
title: 6. 💡 FAQ
description: 
published: true
date: 2024-11-28T13:39:12.480Z
tags: 
editor: markdown
dateCreated: 2021-08-02T17:19:35.360Z
---

## Général

En esta página respondemos a las preguntas más frecuentes de los usuarios de Recalbox. Si necesitáis ayuda, consultad la página que corresponde a vuestra versión de Recalbox:

- [FAQ Recalbox 8](recalbox-8)
- [FAQ Recalbox 9](recalbox-9)

Además de las páginas précedentes, también os recomendamos leer [la página sobre problemas existentes](known-issues). 

### ¿En qué puedo instalar Recalbox?

Puedes instalar Recalbox en los siguientes dispositivos:

- **Raspberry Pi 5**
- Raspberry Pi 4
- Raspberry Pi 400
- Raspberry Pi Zero
- Raspberry Pi 1 (B/B+)
- Raspberry Pi 2
- Raspberry Pi 3 (B/B+)
- Odroid XU4
- Odroid Go Advance (consola portátil)
- Odroid Go Super (consola portátil)
- PC x86 (32 bits)
- PC x86_64 (64 bits)
- Anbernic RG353(M/V/P)
- Anbernic RG351

### ¿Cómo puedo participar?

- Podéis participar en nuestro [Patreon](https://patreon.com/recalbox)
- Podéis pedir vuestro material en kubii.fr/221-recalbox
- Podéis informar de errores o ideas de mejora en nuestro [Gitlab](https://gitlab.com/recalbox/recalbox/issues)
- Podéis participar en los [foros de recalbox](http://forum.recalbox.com)
- Podéis [contribuir a esta misma wiki](./contribute)

### ¿Dónde puedo ver el progreso del desarrollo de Recalbox?

En el [Recalbox gitlab](https://gitlab.com/recalbox). Puede acceder a :  
- los [temas](https://gitlab.com/recalbox/recalbox/issues) que son las tareas actuales.  
- los [milestones](https://gitlab.com/recalbox/recalbox/-/milestones) donde se pueden ver las tareas que aún están por desarrollar, y las que están terminadas.

### ¿Cuántos juegos contiene Recalbox?

Estamos intentando añadir el mayor número posible de juegos gratuitos a la distribución, para que puedas disfrutar de la experiencia Recalbox desde el primer lanzamiento.
Vaya a [esta página](https://forum.recalbox.com/topic/52/homebrews-sur-la-recalbox) para enviar sus homebrews!

### ¿Puedo utilizar Recalbox en un quiosco / bartop?

Sí, el sistema Recalbox también ha sido diseñado para ser integrado en quioscos y bartops.
- Puedes conectar los botones y joysticks directamente a los GPIOs de la Raspberry.
- Puedes desactivar los menús para evitar cualquier mal uso.
- También es posible utilizar las salidas HDMI / DVI /VGA (con adaptador) o RCA.
Puede encontrar más información en [la página dedicada](./tutorials/system/installation/bartop-arcade-configuration).

### Recalbox soporta muchos sistemas pero sólo veo algunos en la interfaz, ¿qué puedo hacer?

Sólo se muestran los sistemas con juegos disponibles. Prueba a añadir tus roms a través de la red y reinicia Recalbox para que aparezcan los nuevos sistemas. Además, algunos equipos no son compatibles con todas las consolas.

### ¿Debería overclockear mi Raspberry Pi?

En la Raspberry Pi 1 (incluida la Zero), se aconseja encarecidamente overclockearla en modo EXTREMO.

## EmulationStation

### ¿Cómo puedo cambiar el aspecto del tema de EmulationStation?

Pulse `Start` en el mando → `CONFIGURACION DE INTERFACE` → `TEMA`.

### ¿Cómo puedo silenciar la música de fondo?

Pulse `Start` en el mando → `CONFIGURACION DE AUDIO` → `MODO DE AUDIO` → SIN SONIDO

### ¿Dónde se encuentran los archivos de configuración de EmulationStation en el sistema de archivos?

```
/recalbox/share/system/recalbox.conf
```

### ¿Dónde se encuentran los temas de EmulationStation?

```
/recalbox/share/themes
```

## Emulación

### ¿Por qué hay varios emuladores para cada sistema?

La emulación no es una operación impecable, hay un equilibrio entre rendimiento, funcionalidad y fidelidad.
Como la emulación no es perfecta, es mejor tener varias opciones para manejarla, a veces un emulador más rápido será mejor pero puede tener problemas inesperados, mientras que uno más lento puede tener mayor precisión pero no soportar características específicas.

### ¿Cómo puedo cambiar el emulador por defecto para un sistema determinado?

Pulsando `START` en el mando, debes de ir a opciones avanzadas, y finalmente a la configuración del emulador avanzada. Puedes seleccionar el sistema deseado, puedes cambiar la configuración del emulador y del kernel (ten en cuenta que la configuración de la base no cambiará si la configuración del emulador dice Default).

## Sistema

### Recalbox soporta alrededor de 100 consolas, pero sólo veo algunas, ya sea en la interfaz de EmulationStation o en la carpeta compartida.

Dependiendo de la placa madre/pc que utilices tendrás emuladores compatibles con ella.  
Consulte la lista de compatibilidad por placa/pc [aquí](./hardware-compatibility/).

### ¿Por qué en algunos sistemas como la GBA mis juegos muestran una pantalla negra y luego vuelven en EmulationStation?

Algunos sistemas requieren bios para funcionar. A lo mejor no os habéis dado cuenta del mensaje de aviso que aparece al lanzar el juego.

Si, a pesar de todo, el problema continúa, contactad con el equipo Recalbox a través del forum o en el Discord.

## Solución de problemas generales

### Mi Recalbox se ha quedado colgada, ¿debo desconectarla y volver a conectarla?

No, en la mayoría de los casos, el sistema no se bloqueó por completo, sólo lo hizo EmulationStation.

Hay varias formas de manejar esta situación, la más fácil es utilizar las funciones de depuración del Webmanager para reiniciar EmulationStation o cerrar Recalbox correctamente.

No hacerlo puede corromper tus datos, especialmente si estás usando una Raspberry Pi con una tarjeta SD.

### Mi mando no funciona

Comprobad que vuestro mando está [oficialmente soportado por Recalbox](/tutorials/controllers/controllers).

Se recomienda no utilizar productos chinos o clones baratos.

A pesar de nuestros esfuerzos de desarrollo y tests, no podemos hacer funcionar todos los mandos del mundo, y utilizando productos no oficiales seguramente os encontraréis con problemas.

Si vuestro mando está soportado, vérificad y actualizad su firmware. Si vuestro mando funciona ya, no se recomienda actualizar su firmware porque podría dejar de funcionar.

### ¿Puedo utilizar un disco duro con una instalación de Recalbox (SDD, NVMe, etc) en otro PC o Raspberry Pi?

No, Recalbox no funcionará en otros ordenadores diferentes del que ha sido utilizado para la primera instalación del sistema. Si queréis reutilizar el disco en otro ordenador (otro PC o otro modelo diferente de Rasperry PI), debéis reinstallar Recalbox siguiendo el proceso habitual (descargar la imagen, grabarla en el disco, realizar el primer arranque, etc).

### No consigo más lanzar ningún juego y aparece un mensaje de error relacionado con la bios. ¿Qué puedo hacer?

¿Vuestra Recalbox funcionaba antes y ahora muestra este mensaje?

![Mensaje de error](/faq/error-fr.png){.align-center}

Para poder ayudaros cara a este error, comprobad que:

- Tenéis las buenas bios correspondientes a la version del emulador embarcada por Recalbox (en caso de que el sistema en cuestión necesite bios)
- ¿Intentad reconfigurar vuestros mandos?
- ¿Vuestra rom es la correcta y se reconoce correctamente [en los grupos de preservación de roms](./tutorials/games/generalities/isos-and-roms/differents-groups) ?

Una vez efectuadas estas verificaciones, si el problema continúa, seguid las siguientes etapas:

#### Activación del modo debug

En primer lugar, debéis activar el modo debug de Recalbox. Esto permite añadir más información durante la ejecución del sistema.

Para ello, seguid las siguiente etapas:

- Apagad vuestra Recalbox.
- Insertad el soporte que os sirve como medio de almacenamiento de juegos en vuestro ordenador.
- Una vez insertado, en Windows debería aparecer un lector llamado `SHARE`. Abridlo.
- Dentro, existe una carpeta llamada `system`. Dentro de ella existe un fichero llamado `recalbox.conf`. Abrid este fichero.
- Al final del todo, en una nueva línea, añadid el siguiente texto:

```ini
emulationstation.debuglogs=1
```
- Grabad el fichero y salid del programa con el que habéis editado el mismo.
- Enchufad de nuevo vuestro medio de almacenamiento a Recalbox y arrancad.

#### Creación de un archivo de soporte

En segundo lugar, hay que crear un archivo de soporte. Aquí tenéis cómo hacer:

- Una vez activado el modo debug como se ha explicado más arriba y con vuestra Recalbox arrancada, intentad reproducir vuestro problema.
- A continuación, dirigíros dentro del [web manager](./basic-usage/features/webmanager) usando un navegador web.
- Una vez dentro, en la zona de la pantalla de abajo a la derecha, existe un pequeño icono con una tuerca. Haced clic encima y en el menú que se abre, haced clic en `Solución de problemas`.
- Al cabo de unos instantes, deberá aparecer un enlace con vuestro archivo de soporte. Descargadlo.

>Si obtenéis un error durante la generación del fichero, siempre podéis recuperarlo desde dentro de vuestro dispositivo de almacenamiento, en la carpeta `saves`. Su nombre comienza con `recalbox-support-` y termina con `.tar.gz`.
>
>La carpeta `saves` está al lado de la carpeta `system` vista anteriormente.
{.is-info}

Una vez recuperado el archivo de soporte, podéis proporcionárselo directamente a la persona del soporte tecnico que os lo pidió, ya sea en el [foro](https://forum.recalbox.com) o en nuestro [Discord](https://discord.gg/NbQFbGM).

