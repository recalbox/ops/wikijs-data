---
title: Convertir les images disque en .BIN + .CUE
description: 
published: true
date: 2021-09-08T12:59:33.151Z
tags: ps1, cue, convertir, cin
editor: markdown
dateCreated: 2021-05-21T08:47:42.296Z
---

## Informations

Les jeux SegaCD ou PSX peuvent utiliser différents formats :

* `.ISO` + `.CUE` accompagné par des fichiers pistes au format `.WAV`, `.BIN`
* `.ISO`
* `.BIN` + `.CUE`
* `.CCD` + `.IMG` + `.SUB`
* `.BIN` seul

**Exemple :**

```text
Wonder Dog (1993)(Sega)(PAL)(Track 01 of 21)[!].iso
Wonder Dog (1993)(Sega)(PAL)(Track 02 of 21)[!].wav
Wonder Dog (1993)(Sega)(PAL)(Track 03 of 21)[!].wav
Wonder Dog (1993)(Sega)(PAL)(Track 04 of 21)[!].wav
Wonder Dog (1993)(Sega)(PAL)(Track 05 of 21)[!].wav
Wonder Dog (1993)(Sega)(PAL)(Track 06 of 21)[!].wav
Wonder Dog (1993)(Sega)(PAL)(Track 07 of 21)[!].wav
Wonder Dog (1993)(Sega)(PAL)(Track 08 of 21)[!].wav
Wonder Dog (1993)(Sega)(PAL)(Track 09 of 21)[!].wav
Wonder Dog (1993)(Sega)(PAL)(Track 10 of 21)[!].wav
Wonder Dog (1993)(Sega)(PAL)(Track 11 of 21)[!].wav
Wonder Dog (1993)(Sega)(PAL)(Track 12 of 21)[!].wav
Wonder Dog (1993)(Sega)(PAL)(Track 13 of 21)[!].wav
Wonder Dog (1993)(Sega)(PAL)(Track 14 of 21)[!].wav
Wonder Dog (1993)(Sega)(PAL)(Track 15 of 21)[!].wav
Wonder Dog (1993)(Sega)(PAL)(Track 16 of 21)[!].wav
Wonder Dog (1993)(Sega)(PAL)(Track 17 of 21)[!].wav
Wonder Dog (1993)(Sega)(PAL)(Track 18 of 21)[!].wav
Wonder Dog (1993)(Sega)(PAL)(Track 19 of 21)[!].wav
Wonder Dog (1993)(Sega)(PAL)(Track 20 of 21)[!].wav
Wonder Dog (1993)(Sega)(PAL)(Track 21 of 21)[!].wav
Wonder Dog (1993)(Sega)(PAL)[!].cue   

Rockman 8 - Metal Heroes (Japan) (Track 1).bin   
Rockman 8 - Metal Heroes (Japan) (Track 2).bin   
Rockman 8 - Metal Heroes (Japan) (Track 3).bin   
Rockman 8 - Metal Heroes (Japan) (Track 4).bin   
Rockman 8 - Metal Heroes (Japan).cue
```

## Convertir une image disque en `.BIN` + `.CUE`

Voici une technique pour convertir les `.ISO` + `.CUE` avec fichiers `.WAV` pour ceux qui ne veulent pas créer de dossiers pour contenir tous les fichiers d'un jeu.

>Cette technique est valable pour PSX, SEGACD.
{.is-info}

* Téléchargez [Daemons Tools Lite](https://www.daemon-tools.cc/fra/products/dtLite).
* Installez le logiciel.
* Choisissez les images disques que vous souhaiter convertir puis cliquer le signe `+`
  * Sélectionnez le fichier `*.CUE` puis appuyer sur `Entrée`. OU
  * Faites un clic droit et choisissez `Monter` dans le menu contextuel pour créer le lecteur virtuel.
* Téléchargez [ImgBurn](http://imgburn.com/).
* Téléchargez le fichier de langue en français en cliquant sur [le lien](http://download.imgburn.com/translations/french.zip).
* Installez le logiciel.
* Dézippez le fichier de langue et placez-le dans le dossier `C:\Program Files(x86)\imgburn\languages`.
* Ouvrez le logiciel et vous l'aurez en français.
* Cliquez sur le bouton `Créer une image à partir du disque`.
* Choisissez le lecteur virtuel comme source.
* Choisissez un dossier de destination pour la création des fichiers BIN et CUE.
* Cliquez sur le bouton `Lire` et patientez.

Une fois la conversion terminée, vous pouvez démonter le lecteur virtuel (clic droit sur l’icône dans **Daemon Tools** puis cliquez sur `Démonter`).

## Créer un fichier `.CUE` manquant pour votre unique fichier `.BIN`

Exemple avec le fichier `r-type.bin`.

* Ouvrez [Notepad++](https://notepad-plus-plus.org/downloads/) sur un nouveau fichier vide.
* Vous allez devoir écrire 3 lignes dans ce nouveau fichier :
  * La première ligne doit contenir le **BINARY**
  * La seconde ligne doit contenir les pistes toujours en **MODE2/2352**
  * La troisième ligne doit contenir l'index **01**
* Vous obtenez quelque chose comme ceci qui devrait fonctionner :

```text
FILE "r-type.bin" BINARY
TRACK 01 MODE2/2352
INDEX 01 00:00:00
```

* Enregistrez le fichier avec l'extension `.CUE` : `r-type.cue`.
* Il reste plus qu'à transférer votre fichier `.bin` et `.cue`.

>Si vous avez plusieurs jeux auquel le fichier `.CUE` est manquant, vous pouvez aller sur le [site de Redump](http://redump.org/downloads/) pour récupérer tous les fichiers .CUE par système.
{.is-info}