---
title: Utilizar vuestras grabaciones ePSXe (.MCR) en RetroArch (.SRM)
description: 
published: true
date: 2023-09-21T12:21:47.917Z
tags: ps1, srm, mcr, grabaciones
editor: markdown
dateCreated: 2023-09-21T12:21:47.917Z
---

Podéis utilizar vuestras copias de seguridad ePsxe utilizando el siguiente método:

* Recuperad vuestro fichero de grabación en ePSXe (Epsxe/memcards/Votre_Sauvegarde.mcr).
* Identificad el **nombre exacto** de vuestra rom.
* Cambied el fichero `Vuestra_Grabacion.mcr` a `NombreDeVuestraRom.srm`.
* Desplazad vuestro fichero recién renombrado dentro del directorio `/recalbox/share/saves/psx` de vuestra Recalbox.