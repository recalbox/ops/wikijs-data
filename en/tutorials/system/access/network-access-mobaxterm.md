---
title: Network access via MobaXTerm
description: 
published: true
date: 2021-08-06T15:05:51.501Z
tags: mobaxterm, network, access
editor: markdown
dateCreated: 2021-08-06T09:05:51.709Z
---

With MobaXTerm, you can:

* Make an SFTP connection
* Use SSH command lines

## Prerequisites

You must download [MobaXTerm](https://mobaxterm.mobatek.net/). You don't have to install it, there is a portable version.

## Configuration

### Connection via SFTP

* Launch MobaXTerm and you will see this window.

![](/tutorials/system/access/mobaxterm1.png){.full-width}

* Click on `Session` at the top of the window.
* In this window, click on `SFTP` to be presented with this window.

![](/tutorials/system/access/mobaxterm2.png){.full-width}

* In the `Remote host` field, enter [the IP address](./../../../tutorials/network/ip/discover-recalbox-ip) of your Recalbox, and in the `Username` field, enter `root` and click OK at the bottom.
* You will get a window asking you for the password to log in.

![](/tutorials/system/access/mobaxterm3.png){.full-width}

* In the visible field, enter `recalboxroot` and click OK. In the next window, you can ask to save the password to connect to this session.

>You will get a window asking you to create a master password. This will allow you to save all the connections made in MobaXTerm.
{.is-info}

* Once all the windows are validated, you will be connected.

![](/tutorials/system/access/mobaxterm4.png){.full-width}

>Once you are logged in once, the connection will be saved so you can easily log in later.
{.is-success}

### Connecting via SSH

Once your connection is saved, you can create another connection to connect via SSH.

* On the left hand side, right click on your connection and select `Duplicate session`.
* On the duplicate connection, right click on it and choose `Edit session`.
* At the top of the new window, click on `SSH`.
* Fill in the same IP address, check the `Specify username` box and enter the same username (i.e. `root`), then click OK.
* Your SSH connection is created.

![](/tutorials/system/access/mobaxterm5.png){.full-width}

## Usage

When you are connected via SFTP, you can see :

* The files on your PC on one side,
* The files on your Recalbox on the other side.

By default, you will be connected to the location `/recalbox/share/system`.