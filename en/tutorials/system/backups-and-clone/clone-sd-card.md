---
title: Clone your SD card
description: 
published: true
date: 2021-08-06T09:24:28.904Z
tags: sd, card, clone
editor: markdown
dateCreated: 2021-08-06T09:24:28.904Z
---

>If you want to clone to another SD card, two SD cards from different manufacturers, supposedly of the same capacity, will not be exactly the same size. So you may not be able to restore your backup on a different brand of SD card, unless it has a larger size than the original one.
{.is-warning}

* To clone your SD card and have a complete backup of your Recalbox, you can use these programs:

## {.tabset}
### Windows

Software :

* [Win32 Disk Imager](http://sourceforge.net/projects/win32diskimager/) with the READ function.

### macOS

Software:

* [ApplePi-Baker](https://www.tweaking4all.com/software/macosx-software/applepi-baker-v2/)
* Carbon Copy Cloner](https://bombich.com/fr)

You can also use the command `dd` in a terminal:

#### Backup

* Plug your SD card into your PC running Linux.
* Determine which device is your SD card:

``shell
sudo fdisk -l
```

* Once in your backup folder, you need to extract and compress your SD card with this command (the letter X must be replaced by the letter determined in the first step):

```shell
sudo dd if=/dev/sdX | gzip -9 > ./recalbox-20150411-sdb.img.gz
```

#### Restore

* To restore your backup, plug your SD card into your PC and use this command (where `/dev/sdX` is your SD card):

```shell
gunzip ./recalbox-20150411-sdb.img.gz | sudo dd of=/dev/sdX
```
 
To follow the progress of the `dd` process, open a new terminal and type the following command:

```shell
watch -n 5 sudo pkill -USR1 -n -x dd
```

### Linux

Software:

* GParted

You can also use the command `dd` in a terminal:

#### Backup

* Plug your SD card into your PC running Linux.
* Determine which device is your SD card:

```shell
sudo fdisk -l
```

* Once in your backup folder, you need to extract and compress your SD card with this command (the letter X must be replaced by the letter determined in the first step):

```shell
sudo dd if=/dev/sdX | gzip -9 > ./recalbox-20150411-sdb.img.gz
```

#### Restore

* To restore your backup, plug your SD card into your PC and use this command (where `/dev/sdX` is your SD card):

```shell
gunzip ./recalbox-20150411-sdb.img.gz | sudo dd of=/dev/sdX
```
 
To follow the progress of the `dd` process, open a new terminal and type the following command:

```shell
watch -n 5 sudo pkill -USR1 -n -x dd
```