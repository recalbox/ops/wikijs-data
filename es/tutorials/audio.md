---
title: 🔊 Sonido
description: 
published: true
date: 2024-07-26T07:15:10.208Z
tags: tutorial, audio, sonido
editor: markdown
dateCreated: 2023-09-01T09:29:14.049Z
---

Aquí se encuentran los tutoriales relacionados con las salidas de audio de vuestros sistemas.

Lista de tutoriales disponibles:

[Audio HiFiBerry](hifiberry-audio)
[Codificador giratorio a través de los GPIO (control de volumen digital)](rotary-encoder-via-gpio-digital-volume-control)
[Corregir los problemas de sonido de interfaz](fix-emulationstation-sound-issues)
[Salida audio analógica en el Pi Zero](analog-audio-output-on-pi-zero)