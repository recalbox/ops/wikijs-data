---
title: Atari Jaguar
description: 
published: true
date: 2024-07-26T07:46:03.014Z
tags: atari, jaguar, consolas, atari-jaguar
editor: markdown
dateCreated: 2023-09-03T15:45:40.447Z
---

![](/emulators/consoles/atarijaguar.svg){.align-center}

## Especificaciones técnicas

* **Fabricante:** Atari Corporation
* **Año de lanzamiento**: 1993
* **Cantidades vendidas** : menos de 250000
* **Procesadores:** 5 procesadores principales en 3 chips:
  * "Tom": chip a 26,59 MHz, procesador gráfico (RISC de 32 bits), Object (RISC de 64 bits), Blitter (RISC de 64 bits)
  * "Jerry": chip a 26,59 MHz, procesador de señales digitales (32 bits)
  * Motorola 68000 (16 bits a 13,295 MHz)
* **RAM :** 2 MB
* **Resolución máxima** 800×576 a 24 bits de color "verdadero"

## Presentación

La **Jaguar** es una videoconsola doméstica de cartuchos que data de 1993, con una biblioteca de juegos limitada, y que no tuvo un gran éxito a pesar de su tecnología innovadora. Fue la penúltima consola producida por Atari Corporation.

## Emuladores

[Libretro Virtualjaguar](libretro-virtualjaguar)