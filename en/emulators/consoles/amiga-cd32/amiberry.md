---
title: Amiberry
description: 
published: true
date: 2021-10-11T17:15:37.846Z
tags: amiga-cd32, amiga, amiberry, cd32
editor: markdown
dateCreated: 2021-07-30T11:39:36.697Z
---

**Amiberry** is a core for ARM optimized for Amiga.

## ![](/emulators/license.svg) License

This core is under [**GPLv3**](https://github.com/midwan/amiberry/blob/master/COPYING) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ❌ | ✅ | ✅ | ✅ | ✅ | ✅  🐌 | ❌ | ❌ |

🐌  Low performances but playable

## ![](/emulators/features.png) Features



## ![](/emulators/bios.svg) BIOS

### List of mandatory bios

You will find the bios with the name indicated in the column **Description** that you will have to rename with the name indicated in the column **Filename**.

| Filename | Description | MD5 | Provided |
| :---: | :---: | :---: | :---: |
| kick40060.CD32 | Kickstart v3.1 r40.060 (1993-05)(Commodore)(CD32)\[!\].rom | f2f241bf094168cfb9e7805dc2856433 5f8924d013dd57a89cf349f4cdedc6b1 | ❌ |
| kick40060.CD32.ext | CD32 Extended-ROM r40.60 (1993)(Commodore)(CD32).rom | bb72565701b1b6faece07d68ea5da639 | ❌ |

### Location

Put the bios like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 🗒 **kick40060.CD32.rom**
┃ ┃ ┃ ┣ 🗒 **kick40060.CD32.ext.rom**

## ![](/emulators/isos.svg) Isos

### Suported extensions

Isos must have the extension:

* .bin/.cue
* .iso
* .mds/.mdf
* .img/.ccd

### Location

Put the isos like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 cd32
┃ ┃ ┃ ┃ ┣ 🗒 **game.cue**
┃ ┃ ┃ ┃ ┣ 🗒 **game.bin**

>The isos using **Redump** format are highly recommended.
{.is-success}

>For more information about isos, go to [this tutorial](./../../../tutorials/games/generalities/isos-and-roms)!
{.is-info}

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/midwan/amiberry/](https://github.com/midwan/amiberry/)
* **Libretro ocumentation**: [https://blitterstudio.com/amiberry/](https://blitterstudio.com/amiberry/)