---
title: Libretro Dolphin
description: 
published: true
date: 2024-06-22T14:28:01.565Z
tags: libretro, wii, dolphin, 9.2+
editor: markdown
dateCreated: 2024-06-22T14:28:01.565Z
---



## ![](/emulators/license.svg) Licencia

El core posee la siguiente licencia [**GPLv2**](https://github.com/libretro/dolphin/blob/master/license.txt).

## ![](/emulators/compatibility.png) Compatibilidad

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ❌ | ❌ | ❌ | ❌ | ❌ | ❌ | ✅ | ❌ |

## ![](/emulators/features.png) Funcionalidades

| Funcionalidad | ¿Soportada? |
| :---: | :---: |

## ![](/emulators/bios.svg) BIOS

>**No necesita bios.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Extensiones de roms soportadas

Las roms deben tener las extensiones siguientes:

* .ciso
* .gc
* .gcm
* .gcz
* .iso
* .rvz

### Emplazamiento

Disponed vuestras isos como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 wii
┃ ┃ ┃ ┃ ┣ 🗒 **juego.rvz**

## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

>Para conservar las configuraciones personalizadas cuando se produce una actualización del sistema Recalbox, os aconsejamos utilizar las [Sobrecargas de configuración](./../../../advanced-usage/configuration-override).
{.is-info}

### Acceder a las opciones

Podéis configurar las opciones del core de dos formas diferentes.

* Via el Menú RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via el fichero `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**


### Opciones del core

| Opción | Valor de la opción | Variable (para la sobrecarga) | Valores posibles |
| --- | --- | --- | --- |
| Renderer | `Hardware` ✅ / `Software` | `dolphin_renderer` | `Hardware` / `Software` |
| Internal Resolution | `x1 (640 x 528)` ✅ / `x2 (1280 x 1056)` / `x3 (1920 x 1584)` / `x4 (2560 x 2112)` / `x5 (3200 x 2640)` / `x6 (3840 x 3168)` | `dolphin_efb_scale` | `x1 (640 x 528)` / `x2 (1280 x 1056)` / `x3 (1920 x 1584)` / `x4 (2560 x 2112)` / `x5 (3200 x 2640)` / `x6 (3840 x 3168)` |
| Widescreen (Wii) | `ON` ✅ / `OFF` | `dolphin_widescreen` | `true` / `false` |
| WideScreen Hack | `ON` / `OFF` ✅ | `dolphin_widescreen_hack` | `true` / `false` |
| Shader Compilation Mode | `sync` ✅ / `a-sync Skip Rendering` / `sync UberShaders` / `a-sync UberShaders` | `dolphin_shader_compilation_mode` | `sync` / `a-sync Skip Rendering` / `sync UberShaders` / `a-sync UberShaders` |
| Wait for Shaders before Starting | `ON` / `OFF` ✅ | `dolphin_wait_for_shaders` | `true` / `false` |
| Progressive Scan | `ON` ✅ / `OFF` | `dolphin_progressive_scan` | `true` / `false` |
| PAL60 | `ON` ✅ / `OFF` | `dolphin_pal60` | `true` / `false` |
| Anti-Aliasing | `None` ✅ / `2x MSAA` / `4x MSAA` / `8x MSAA` / `2x SSAA` / `4x SSAA` / `8x SSAA` | `dolphin_anti-aliasing` | `None` / `2x MSAA` / `4x MSAA` / `8x MSAA` / `2x SSAA` / `4x SSAA` / `8x SSAA` |
| Max Anisotropy | `1x` ✅ / `2x` / `4x` / `8x` / `16x` | `dolphin_max_anisotropy` | `1x` / `2x` / `4x` / `8x` / `16x` |
| Skip Presenting Duplicate Frames | `ON` ✅ / `OFF` | `dolphin_skip_dupe_frames` | `true` / `false` |
| Immediate XFB | `ON` / `OFF` ✅ | `dolphin_immediate_xfb` | `true` / `false` |
| Scaled EFB Copy | `ON` ✅ / `OFF` | `dolphin_efb_scaled_copy` | `true` / `false` |
| Force Texture Filtering | `ON` / `OFF` ✅ | `dolphin_force_texture_filtering` | `true` / `false` |
| Store EFB Copies on GPU | `ON` ✅ / `OFF` | `dolphin_efb_to_textures` | `true` / `false` |
| Texture Cache Accuracy | `Fast` ✅ / `Middle` / `Safe` | `dolphin_texture_cache_accuracy` | `Fast` / `Middle` / `Safe` |
| GPU Texture Decoding | `ON` / `OFF` ✅ | `dolphin_gpu_texture_decoding` | `true` / `false` |
| Fast Depth Calculation | `ON` ✅ / `OFF` | `dolphin_fast_depth_calculation` | `true` / `false` |
| Bounding Box Emulation | `ON` / `OFF` ✅ | `dolphin_bbox_enabled` | `true` / `false` |
| Disable EFB to VRAM | `ON` / `OFF` ✅ | `dolphin_efb_to_vram` | `true` / `false` |
| Load Custom Textures | `ON` / `OFF` ✅ | `dolphin_load_custom_textures` | `true` / `false` |
| Prefetch Custom Textures | `ON` / `OFF` ✅ | `dolphin_cache_custom_textures` | `true` / `false` |
| CPU Core | `JIT64` ✅ / `Interpreter` / `Cached Interpreter` | `dolphin_cpu_core` | `JIT64` / `Interpreter` / `Cached Interpreter` |
| CPU Clock Rate | `5%` / `10%` / `20%` / `30%` / `40%` / `50%` / `60%` / `70%` / `80%` / `90%` / `100%` ✅ / `150%` / `200%` / `250%` / `300%` | `dolphin_cpu_clock_rate` | `0.05` / `0.1` / `0.2` / `0.3` / `0.4` / `0.5` / `0.6` / `0.7` / `0.8` / `0.9` / `1.0` / `1.5` / `2.0` / `2.5` / `3.0` |
| Emulation Speed | `unlimited` ✅ / `100%` | `dolphin_emulation_speed` | `0.0` / `1.0` |
| Fastmem | `ON` ✅ / `OFF` | `dolphin_fastmem` | `true` / `false` |
| Speed Up Disc Transfer Rate | `ON` / `OFF` ✅ | `dolphin_fast_disc_speed` | `true` / `false` |
| Wiimote IR Mode | `Right Stick controls pointer (relative)` ✅ / `Right stick controls pointer (absolute)` / `Mouse controls pointer` | `dolphin_ir_mode` | `Right Stick controls pointer (relative)` / `Right stick controls pointer (absolute)` / `Mouse controls pointer` |
| Wiimote IR Vertical Offset | `-50` / `-49` / `-48` / `-47` / `-46` / `-45` / `-44` / `-43` / `-42` / `-41` / `-40` / `-39` / `-38` / `-37` / `-36` / `-35` / `-34` / `-33` / `-32` / `-31` / `-30` / `-29` / `-28` / `-27` / `-26` / `-25` / `-24` / `-23` / `-22` / `-21` / `-20` / `-19` / `-18` / `-17` / `-16` / `-15` / `-14` / `-13` / `-12` / `-11` / `-10` / `-9` / `-8` / `-7` / `-6` / `-5` / `-4` / `-3` / `-2` / `-1` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` ✅ / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `21` / `22` / `23` / `24` / `25` / `26` / `27` / `28` / `29` / `30` / `31` / `32` / `33` / `34` / `35` / `36` / `37` / `38` / `39` / `40` / `41` / `42` / `43` / `44` / `45` / `46` / `47` / `48` / `49` / `50` | `dolphin_ir_offset` | `-50` / `-49` / `-48` / `-47` / `-46` / `-45` / `-44` / `-43` / `-42` / `-41` / `-40` / `-39` / `-38` / `-37` / `-36` / `-35` / `-34` / `-33` / `-32` / `-31` / `-30` / `-29` / `-28` / `-27` / `-26` / `-25` / `-24` / `-23` / `-22` / `-21` / `-20` / `-19` / `-18` / `-17` / `-16` / `-15` / `-14` / `-13` / `-12` / `-11` / `-10` / `-9` / `-8` / `-7` / `-6` / `-5` / `-4` / `-3` / `-2` / `-1` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `21` / `22` / `23` / `24` / `25` / `26` / `27` / `28` / `29` / `30` / `31` / `32` / `33` / `34` / `35` / `36` / `37` / `38` / `39` / `40` / `41` / `42` / `43` / `44` / `45` / `46` / `47` / `48` / `49` / `50` |
| Wiimote IR Total Yaw | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` ✅ / `16` / `17` / `18` / `19` / `20` / `21` / `22` / `23` / `24` / `25` / `26` / `27` / `28` / `29` / `30` / `31` / `32` / `33` / `34` / `35` / `36` / `37` / `38` / `39` / `40` / `41` / `42` / `43` / `44` / `45` / `46` / `47` / `48` / `49` / `50` / `51` / `52` / `53` / `54` / `55` / `56` / `57` / `58` / `59` / `60` / `61` / `62` / `63` / `64` / `65` / `66` / `67` / `68` / `69` / `70` / `71` / `72` / `73` / `74` / `75` / `76` / `77` / `78` / `79` / `80` / `81` / `82` / `83` / `84` / `85` / `86` / `87` / `88` / `89` / `90` / `91` / `92` / `93` / `94` / `95` / `96` / `97` / `98` / `99` / `100` | `dolphin_ir_yaw` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `21` / `22` / `23` / `24` / `25` / `26` / `27` / `28` / `29` / `30` / `31` / `32` / `33` / `34` / `35` / `36` / `37` / `38` / `39` / `40` / `41` / `42` / `43` / `44` / `45` / `46` / `47` / `48` / `49` / `50` / `51` / `52` / `53` / `54` / `55` / `56` / `57` / `58` / `59` / `60` / `61` / `62` / `63` / `64` / `65` / `66` / `67` / `68` / `69` / `70` / `71` / `72` / `73` / `74` / `75` / `76` / `77` / `78` / `79` / `80` / `81` / `82` / `83` / `84` / `85` / `86` / `87` / `88` / `89` / `90` / `91` / `92` / `93` / `94` / `95` / `96` / `97` / `98` / `99` / `100` |
| Wiimote IR Total Pitch | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` ✅ / `16` / `17` / `18` / `19` / `20` / `21` / `22` / `23` / `24` / `25` / `26` / `27` / `28` / `29` / `30` / `31` / `32` / `33` / `34` / `35` / `36` / `37` / `38` / `39` / `40` / `41` / `42` / `43` / `44` / `45` / `46` / `47` / `48` / `49` / `50` / `51` / `52` / `53` / `54` / `55` / `56` / `57` / `58` / `59` / `60` / `61` / `62` / `63` / `64` / `65` / `66` / `67` / `68` / `69` / `70` / `71` / `72` / `73` / `74` / `75` / `76` / `77` / `78` / `79` / `80` / `81` / `82` / `83` / `84` / `85` / `86` / `87` / `88` / `89` / `90` / `91` / `92` / `93` / `94` / `95` / `96` / `97` / `98` / `99` / `100` | `dolphin_ir_pitch` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `21` / `22` / `23` / `24` / `25` / `26` / `27` / `28` / `29` / `30` / `31` / `32` / `33` / `34` / `35` / `36` / `37` / `38` / `39` / `40` / `41` / `42` / `43` / `44` / `45` / `46` / `47` / `48` / `49` / `50` / `51` / `52` / `53` / `54` / `55` / `56` / `57` / `58` / `59` / `60` / `61` / `62` / `63` / `64` / `65` / `66` / `67` / `68` / `69` / `70` / `71` / `72` / `73` / `74` / `75` / `76` / `77` / `78` / `79` / `80` / `81` / `82` / `83` / `84` / `85` / `86` / `87` / `88` / `89` / `90` / `91` / `92` / `93` / `94` / `95` / `96` / `97` / `98` / `99` / `100` |
| Rumble | `ON` ✅ / `OFF` | `dolphin_enable_rumble` | `true` / `false` |
| Sensor Bar Position | `Bottom` ✅ / `Top` | `dolphin_sensor_bar_position` | `Bottom` / `Top` |
| Wiimote Continuous Scanning | `ON` / `OFF` ✅ | `dolphin_wiimote_continuous_scanning` | `true` / `false` |
| Use ports 5-8 for GameCube controllers in Wii mode | `ON` / `OFF` ✅ | `dolphin_alt_gc_ports_on_wii` | `true` / `false` |
| Audio Mixer Rate | `32000` / `48000` | `dolphin_mixer_rate` | `32000u` / `48000u` |
| DSP HLE | `ON` ✅ / `OFF` | `dolphin_dsp_hle` | `true` / `false` |
| DSP Enable JIT | `ON` ✅ / `OFF` | `dolphin_dsp_jit` | `true` / `false` |
| Language | `English` ✅ / `Japanese` / `German` / `French` / `Spanish` / `Italian` / `Dutch` / `Simplified Chinese` / `Traditional Chinese` / `Korean` | `dolphin_language` | `English` / `Japanese` / `German` / `French` / `Spanish` / `Italian` / `Dutch` / `Simplified Chinese` / `Traditional Chinese` / `Korean` |
| Internal Cheats Enabled | `ON` / `OFF` ✅ | `dolphin_cheats_enabled` | `true` / `false` |
| OSD Enabled | `ON` ✅ / `OFF` | `dolphin_osb_enabled` | `true` / `false` |
| Log Level | `Info` ✅ / `Notice` / `Error` / `Warning` | `dolphin_log_level` | `Info` / `Notice` / `Error` / `Warning` |

## ![](/emulators/external-links.png) Enlaces externos

* **Código fuente utilizado** : [https://github.com/libretro/dolphin/](https://github.com/libretro/dolphin/)
* **Documentación Libretro** :