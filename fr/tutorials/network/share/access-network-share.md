---
title: Impossible d'accéder à Recalbox depuis le partage réseau
description: 
published: true
date: 2024-11-25T21:34:46.126Z
tags: accès, réseau, partage
editor: markdown
dateCreated: 2021-05-21T08:39:04.904Z
---

Il peut arriver que Windows ne veuille pas afficher Recalbox sur votre réseau domestique.

## Windows 10

### Aucun ordinateur n'est visible

Si vous ne voyez aucun ordinateur incluant Recalbox depuis le réseau de Windows 10, plusieurs choses peuvent en être la cause :

* La découverte du réseau est désactivée par défaut et la configuration du réseau est positionnée sur « publique » 
* La fonction « Rendre ce PC détectable » est défini sur « Désactivé ».

Vous devez aller dans les paramètres réseau et mettre votre réseau en mode « privé », ou mettre « Rendre ce PC détectable » sur « activé ».

### Recalbox n'est plus accessible depuis la mise à jour n°1709 de Windows 10

Depuis la mise à jour 1709 de Windows 10, vous pouvez voir votre Recalbox dans votre réseau, mais vous ne pouvez plus y accéder.

Windows vous évite d'accéder aux partages réseaux avec l'accès invité activé. Et il n'y a aucune référence au protocole SMBv1 qui a été désactivé.

Pour y remédier, vous pouvez télécharger et appliquer [ce patch](/tutorials/network/win10_allow_recalbox.reg). Ce patch va désactiver la sécurité sur les accès réseaux anonymes dans Windows, pour rétablir l'accès à votre Recalbox.

* [Source 1](https://tech.nicolonsky.ch/windows-10-1709-cannot-access-smb2-share-guest-access/)
* [Source 2](https://support.microsoft.com/fr-fr/help/4046019/guest-access-in-smb2-disabled-by-default-in-windows-10-and-windows-server-2016)

### Autres possibilités

* Ouvrez le menu Démarrer et allez dans `Paramètres` > `Applications` > `Applications et fonctionnalités` > `Activer ou désactiver des fonctionnalités Windows`
* Activez en cochant l'option `support de partage de fichiers SMB 1.0/CIFS / client SMB1.0/CIFS...`
* Redémarrez Windows.

## Windows 11

### Recalbox n'est plus accessible depuis la mise à jour 24H2 de Windows 11

Depuis la mise à jour 24H2 de Windows 11, vous pouvez voir votre Recalbox dans votre réseau, mais vous ne pouvez plus y accéder sans devoir y insérer des identifiants.

Pour y remédier, vous pouvez télécharger et appliquer [ce patch](/tutorials/network/win11_allow_recalbox.reg). Ce patch va désactiver la sécurité sur les accès réseaux anonymes dans Windows, pour rétablir l'accès à votre Recalbox.