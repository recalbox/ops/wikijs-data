---
title: FAQ
description: 
published: true
date: 2024-07-25T21:47:29.609Z
tags: faq
editor: markdown
dateCreated: 2021-06-27T07:51:06.931Z
---

## Systèmes

Les machines Amiga ont été divisés en 4 systèmes :

* Amiga 600 : toutes les machines `OCS`/`ECS` de l'Amiga 1000 jusqu'à la dernière Amiga 600.
* Amiga 1200 : toutes les machines `AGA`, à partir de l'Amiga 1200.
* Amiga CD32 : la console Amiga CD32.
* Amiga CDTV : la console Amiga CDTV.

## Extensions

Voici une liste avec explications des différents formats de fichiers pour les systèmes Amiga :

* Amiga 600 et Amiga 1200 :
  * Disques **ADF** (`*.adf`), le format de disque le plus populaire (ne peut pas intégrer les protections de disque). Peut être zippé ou 7-zippé.
  * Disques **IPF** (`*.ipf`), plus généralement utilisés par les copies de jeu sans intro (peut intégrer les protections de disque). Peut être zippé.
  * **WHD** (dossiers, `*.lha/lzh/lzx`, `*.zip`), le format bien connu pour les jeux sur disque dur. Les dossiers WHD sont reconnus en utilisant le fichier `*.slave` à l'intérieur du dossier, mais il est fortement recommandé de les garder empaquetés en `lha` ou en `zip`. Les WHD dépaquetés ne sont pas plus rapides et encore pire, ils peuvent rendre l'interface très lente.
  * **HDD FS**, disque dur sur système de fichiers. Les dossiers doivent se terminer par des extensions de type `.hd`. Rarement utilisé. Pour les vrais amateurs d'Amiga qui ont sauvegardé le disque dur de leur machine. Peut être zippé, mais pas recommandé.
  * **HDF** (`*.hdf`), images de disque dur dans un seul fichier. Peut être zippé.
  * **RP9** (`*.rp9`), paquets tout-en-un d'Amiga Forever.
* Amiga CD32 et Amiga CDTV :
  * **ISO** (`*.iso`), copies de CD.
  * **BIN/CUE** (`*.bin` / `*.cue`), copies de CD. Peut être composé de 1 `cue` et de un ou plusieurs `bin`.

Quand vous jouez à partir de fichiers zippés/7-zippés, le système essaye d'identifier le type de rom en cherchant des extensions spécifiques à l'intérieur de l'archive.

* Les extensions `ADF`/`IPF`/`BIN`/`ISO` sont rapides à identifier. 
* L'extension `WHD` est rapide, mais LHA devrait être l'option préférée. 
* L'extension `HDDFS` peut prendre plus longtemps à identifier et peut donner lieu à des interprétations fausses. Si vous utilisez le `HDDFS`, laissez-le en tant que fichiers et dossiers normaux et finissez le nom du dossier racine avec `.hd` pour une identification facile.

## Commandes spéciales avec Amiberry

### Au clavier

* Entrer dans l'interface : `F12`.
* Quitter : `F12`, descendre sur `Quit` et valider avec `ENTER`.

### À la manette

* Entrer dans l'interface : `HK` + `B`.
* Quitter : `HK` + `Start`.

## Surcharges de configuration

>Les surcharges de configuration sur les cores Libretro sont des [surcharges de configurations](../../../../advanced-usage/configuration-override). Ici, seul Amiberry sera traité.
{.is-info}

Comme vu dessus, un énorme travail a été fait pour configurer automatiquement les émulateurs en fonction de ce que vous souhaitez démarrer.

Recalbox génère une configuration par système. Cependant, dans certaines situations, vous pouvez surcharger ces configurations.

La génération des fichiers de configuration est effectuée dans `/tmp/amiga/conf`. Le plus important est le fichier `uaeconfig.uae` dans lequel vous allez trouver toutes les clés définissant la machine, les disques durs, etc.

Si vous êtes curieux, vous pouvez jeter un œil à la documentation d'Amiberry.

Vous avez 2 façons de surcharger les configurations avec un exemple pour chaque cas :

### Par système

Pour cela, vous devez créer un fichier dans le dossier `/recalbox/share/system/configs/amiberry/`. Ce fichier sera nommé en fonction du système :

* `amiga600.uae` pour surcharger l'Amiga 600
* `amiga1200.uae` pour surcharger l'Amiga 1200
* `amigacd32.uae` pour surcharger l'Amiga CD32
* `amigacdtv.uae` pour surcharger l'Amiga CDTV

Par exemple, pour désactiver l'affichage des LEDs dans le coin inférieur droit, vous pouvez utiliser cette ligne :

```txt
show_leds=false
```

### Par jeu

Pour cela, vous devez créer un fichier dans le dossier des roms où se trouve le jeu à surcharger, se finissant par l'extension `.uae`.

Par exemple, dans votre dossier `/recalbox/share/roms/amiga1200`, pour le jeu `Aladdin (AGA).ipf`, vous pouvez créer le fichier `Aladdin (AGA).uae` et placer vos surcharges dedans.

Les surcharges sont gérés de la manière suivante :

* Premièrement, la configuration de base est généré.
* Ensuite, si vous avez une surcharge pour le système, les clés trouvés dans le fichier de la surcharge écraseront ceux de la configuration de base.
* Enfin, si vous avez une surcharge pour le jeu que vous lancez, les clés trouvés dans le fichier de la surcharge écraseront ceux de la configuration de base ET de la surcharge par système.

## Fonctionnement des jeux ADF

Copiez-les dans votre dossier de roms Amiga correspondant au système.

Les disques multiples sont automatiquement chargés, et ce jusqu'à 4 disques pour les jeux utilisant la bonne nomenclature tel que `(Disk 1 of X)`.

## Fonctionnement des jeux WHD

Les jeux WHD sont un peu plus difficiles à utiliser mais rien de bien compliqué.

### Installation

Décompressez votre jeu WHDLoad, supprimez le fichier `.info` au même niveau que le dossier, et copiez seulement ce dossier vers votre dossier de roms (vous pouvez utiliser des sous-dossiers si vous le souhaitez, par exemple `/recalbox/share/roms/amiga1200/ShootEmUp`).

### Utilisation

Vous aurez besoin d'un ficher `.uae` au même niveau que le dossier du même nom. Il existe deux façons de faire :

* En créer un vide.
* Utiliser un fichier personnalisé qui va vous permettre de modifier la configuration de l'émulateur.

Pour la seconde façon de faire, il existe un petit utilitaire nommé [UCG](https://github.com/solarmon/UCG). Celui-ci va vous permettre de générer une configuration UAE.

### Modification des jeux WHDL

#### Modification de fichiers .UAE

Dans le fichier uae, vous pouvez définir des blocs `;hardware`, `;controls` et `;graphics` pour remplacer la configuration standard de l'émulateur.

Les parties personnalisées de la configuration dans le fichier `.uae` vont uniquement être utilisées si elles commencent avec le bon nom de bloc : `;hardware`, `;controls` ou `;graphics`.

Cela peut vous permettre par exemple d'utiliser deux joysticks, d'avoir une configuration matérielle très spécifique pour certains jeux qui sont un peu spéciaux/capricieux ou définir une résolution personnalisée.

Vous pouvez aussi tout supprimer dans le fichier `.uae` et la configuration de base de l'émulateur sera alors utilisée.

Copiez celui-ci (correspondant au modèle d'Amiga que vous souhaitez utiliser) à côté du dossier du jeu WHDL et renommez-le exactement comme le dossier du jeu (tout en gardant l'extension `.uae`). Par exemple, si votre jeu à pour nom de dossier `Rick Dangerous`, votre fichier uae devrait être nommé `Rick Dangerous.uae` et être au même niveau que le dossier.

#### Modification de la séquence de démarrage (niveau expert)

La séquence de démarrage standard générée pour les jeux WHDL est `WHDload game.slave Preload` (dans `WHDL/S/Startup-Sequence`). Mais quelques jeux WHDL peu nombreux (comme `History Line : 1914-1918`) peuvent demander des paramètres supplémentaires au démarrage sous peine de plantage.

Vous pouvez ajouter un deuxième fichier (complètement optionnel) contenant ces paramètres supplémentaires. En prenant l'exemple d'History Line, vous aurez :

* HistoryLine (dossier du jeu WHDL)
* HistoryLine.uae (qui peut être vide ou personnalisé)
* HistoryLine.whdl (optionnel, contenant des paramètres en plus `CUSTOM1=1`)

Le jeu se lancera à ce moment-là.

Voici une petite liste des jeux qui requiert des paramètres additionnels.

| Jeu | Paramètre(s) extra / Contenu du fichier |
| :--- | :--- |
| HistoryLine 1914-1918 | `CUSTOM1=1` pour l'intro, `CUSTOM1=2` pour le jeu. |
| The Settlers / Die Siedlers | `CUSTOM1=1` pour passer l'intro. |