---
title: Mouse use with controller in DOSBox Pure
description: 
published: true
date: 2021-09-04T23:26:56.840Z
tags: mouse, dosbox pure, 7.2+
editor: markdown
dateCreated: 2021-08-08T06:54:12.175Z
---

## Introduction

With the Libretro DOSBox Pure core, you can activate the mouse control to use it with one of the sticks of your controller.

## Use

To use the mouse on a joystick of your controller, there is a manipulation to do in the options of RetroArch. We will see below how to use the mouse on the right joystick.

* Launch your game
* Once launched, go to the RetroArch options and go down to **Controls**.
* In the key configuration, go to **Port 1 Controls**.
* Move to the **Device Type** option and use the right arrow to select `Mouse with Right Analog Stick`.
* Go back to the game and enjoy! You will have mouse clicks on L1 and R1 (left click and right click).