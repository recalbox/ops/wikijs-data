---
title: Libretro Yabause
description: 
published: true
date: 2024-07-13T08:10:17.764Z
tags: libretro, saturn, yabause
editor: markdown
dateCreated: 2023-09-07T20:47:32.121Z
---

**Libretro Yabause** es una adaptación del emulador **Yabause** para el sistema Libretro que emula la Sega Saturn.

## ![](/emulators/license.svg) Licencia

El core posee la siguiente licencia [**GPLv2**](https://github.com/libretro/yabause/blob/master/yabause/COPYING).

## ![](/emulators/compatibility.png) Compatibilidad

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | 
| ❌ | ❌ | ❌ | ❌ | ❌ | ❌ | ✅ | ❌ |

## ![](/emulators/features.png) Funcionalidades

| Funcionalidad | ¿Soportada? |
| :---: | :---: |
| Reinicio | ✔ |
| Capturas de pantalla | ✔ |
| Grabaciones | ✔ |
| Grabaciones instantáneas | ✔ |
| Rebobinado | ✔ |
| Opciones del core | ✔ |
| RetroAchievements | ✔ |
| Cheats RetroArch | ✔ |
| Controles | ✔ |
| Remapping | ✔ |
| Control de disco | ✔ |

## ![](/emulators/bios.svg) BIOS

### Lista de BIOS obligatorias

| Nombre del fichero | Descripción | MD5 | ¿Preinstalada? |
| :---: | :---: | :---: | :---: |
| saturn_bios.bin | BIOS Saturn | af5828fdff51384f99b3c4926be27762 | ❌ |

### Emplazamiento

Disponed vuestras bios como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 saturn
┃ ┃ ┃ ┃ ┣ 🗒 **saturn_bios.bin**

## ![](/emulators/isos.svg) Isos

### Extensiones de roms soportadas

Las isos deben tener las extensiones siguientes:

* .cue/.bin
* .m3u
* .img/.ccd
* .iso
* .mds/.mdf
* .chd

### Emplazamiento

Disponed vuestras roms como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 saturn
┃ ┃ ┃ ┃ ┣ 🗒 **juego.bin**
┃ ┃ ┃ ┃ ┣ 🗒 **juego.cue**

>Se aconsejan las isos del proyecto **Redump**.
{.is-success}

>Para más información sobre las isos, consultad [este tutorial](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

>Para conservar las configuraciones personalizadas cuando se produce una actualización del sistema Recalbox, os aconsejamos utilizar las [Sobrecargas de configuración](./../../../advanced-usage/configuration-override).
{.is-info}

### Acceder a las opciones

Podéis configurar las opciones del core de dos formas diferentes.

* Via el Menú RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via el fichero `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Opciones del core

| Opción | Valor de la opción | Variable (para la sobrecarga) | Valores posibles |
| --- | --- | --- | --- |
 | Saut d'images | `Désactivé` ✅ / `Activé` | `yabause_frameskip` | `disabled` / `enabled` |
 | Forcer le BIOS HLE (Redémarrage requis) | `Désactivé` ✅ / `Activé` | `yabause_force_hle_bios` | `disabled` / `enabled` |
 | Cartouche de mémoire supplémentaire (Redémarrage requis) | `None` ✅ / `1 Mo de RAM` / `4 Mo de RAM` | `yabause_addon_cartridge` | `None` / `1M_ram` / `4M_ram` |
 | Adaptateur 6 joueurs sur le port 1 | `Désactivé` ✅ / `Activé` | `yabause_multitap_port1` | `disabled` / `enabled` |
 | Adaptateur 6 joueurs sur le port 2 | `Désactivé` ✅ / `Activé` | `yabause_multitap_port2` | `disabled` / `enabled` |
 | Nombre de fils d'exécution (Redémarrage requis) | `1` / `2` / `4` ✅ / `8` / `16` / `32` | `yabause_numthreads` | `1` / `2` / `4` / `8` / `16` / `32` |

## ![](/emulators/external-links.png) Enlaces externos

* **Código fuente utilizado** : [https://github.com/libretro/yabause/](https://github.com/libretro/yabause/)
* **Documentación Libretro** : [https://docs.libretro.com/library/yabause/](https://docs.libretro.com/library/yabause/)