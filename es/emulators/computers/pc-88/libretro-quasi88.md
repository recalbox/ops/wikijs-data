---
title: Libretro QUASI88
description: 
published: true
date: 2024-07-17T21:18:56.596Z
tags: libretro, pc-88, quasi88
editor: markdown
dateCreated: 2023-09-11T11:35:21.971Z
---



## ![](/emulators/license.svg) Licencia

El core posee la siguiente licencia [**BSD 3-Clause**](https://github.com/libretro/quasi88-libretro/blob/master/LICENSE).

## ![](/emulators/compatibility.png) Compatibilidad

 | RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | 
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Funcionalidades

| Funcionalidad | ¿Soportada? |
| :---: | :---: |
| Reinicio | ✔ |
| Capturas de pantalla | ✔ |
| Grabaciones | ✔ |
| Grabaciones instantáneas | ✔ |
| Rebobinado | ✔ |
| Netplay | ✔ |
| Opciones del core | ✔ |
| RetroAchievements | ✔ |
| Cheats RetroArch | ✔ |
| Controles | ✔ |
| Remapping | ✔ |
| Vibración | ✔ |
| Subsistemas | ✔ |

## ![](/emulators/bios.svg) BIOS

### Lista de BIOS obligatorias

| Nombre del fichero | Descripción | MD5 | ¿Preinstalada? |
| :---: | :---: | :---: | :---: |
| n88.rom | BASIC rom | 22be239bc0c4298bc0561252eed98633 f7cba6a308c2718dbe97e60e46ddd66a 4f984e04a99d56c4cfe36115415d6eb8 | ❌ |
| n88_0.rom | BASIC rom #1 | e28fe3f520bea594350ea8fb00395370 c254685f00ca9c31b97203d6ef19f5e2 d675a2ca186c6efcd6277b835de4c7e5 | ❌ |
| n88_1.rom | BASIC rom #2 | e844534dfe5744b381444dbe61ef1b66 a8e298da7ac947669bcb1ff25cee0a83 | ❌ |
| n88_2.rom | BASIC rom #3 | 6548fa45061274dee1ea8ae1e9e93910 9d03154fd9abfc28c4e6d4dc705e6e23 | ❌ |
| n88_3.rom | BASIC rom #4 | fc4b76a402ba501e6ba6de4b3e8b4273 e1791f8154f1cdf22b576a1a365b6e1f | ❌ |
| n88n.rom | Emulation de la série PC-8000 | 5d6854624dd01cd791f58727fc43a525 93cd1d78b7b9c50b80041ed330332ece 2ff07b8769367321128e03924af668a0 | ❌ |
| disk.rom | Chargement des images disque | 793f86784e5608352a5d7f03f03e0858 01b1af474fcabe93c40d779b234a3825 | ❌ |

### Emplazamiento

Disponed vuestras bios como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 quasi88
┃ ┃ ┃ ┃ ┣ 🗒 **n88.rom**
┃ ┃ ┃ ┃ ┣ 🗒 **n88_0.rom**
┃ ┃ ┃ ┃ ┣ 🗒 **n88_1.rom**
┃ ┃ ┃ ┃ ┣ 🗒 **n88_2.rom**
┃ ┃ ┃ ┃ ┣ 🗒 **n88_3.rom**
┃ ┃ ┃ ┃ ┣ 🗒 **n88n.rom**
┃ ┃ ┃ ┃ ┣ 🗒 **disk.rom**

### Lista de BIOS opcionales

| Nombre del fichero | Descripción | MD5 | ¿Preinstalada? |
| :---: | :---: | :---: | :---: |
| n88knj1.rom | Affichage des kanjis. | d81c6d5d7ad1a4bbbd6ae22a01257603 | ❌ |

### Emplazamiento

Disponed vuestras bios como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 quasi88
┃ ┃ ┃ ┃ ┣ 🗒 **n88knj1.rom**

## ![](/emulators/roms.png) Roms

### Extensiones de roms soportadas

Las roms deben tener las extensiones siguientes:

* .d88
* .t88
* .cmt
* .m3u
* .zip
* .7z

Este sistema soporta las roms comprimidas en formato .zip/.7z. No obstante, se trata sólo de un archivo.

Los ficheros contenidos en los .zip/.7z deben tener las extensiones citadas previamente.
Además, cada fichero .zip/.7z sólo debe contener un fichero rom al interior.

### Emplazamiento

Disponed vuestras roms como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 pc88
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.zip**

>Se aconsejan las roms del proyecto **TOSEC**.
{.is-success}

>Para más información sobre las roms, consultad [este tutorial](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}


## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

>Para conservar las configuraciones personalizadas cuando se produce una actualización del sistema Recalbox, os aconsejamos utilizar las [Sobrecargas de configuración](./../../../advanced-usage/configuration-override).
{.is-info}

### Acceder a las opciones

Podéis configurar las opciones del core de dos formas diferentes.

* Via el Menú RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via el fichero `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Opciones del core

| Opción | Valor de la opción | Variable (para la sobrecarga) | Valores posibles |
| --- | --- | --- | --- |
| Mode BASIC | `N88-BASIC V2` ✅ / `N88-BASIC V1H` / `N88-BASIC V1S` / `N-BASIC` | `q88_basic_mode` | `N88 V2` / `N88 V1H` / `N88 V1S` / `N` |
| Mode du sous-processeur | `Faire tourner le sous-processeur uniquement lors de l'accès au disque` ✅ / `Faire tourner les deux processeurs lors de l'accès au disque` / `Toujours utiliser les deux processeurs` | `q88_sub_cpu_mode` | `0` / `1` / `2` |
| Horloge processeur | `1 MHz (underclock)` / `2 MHz (underclock)` / `4 MHz (NEC µPD780)` ✅ / `8 MHz (NEC µPD70008)` / `16 MHz (overclock)` / `32 MHz (overclock)` / `64 MHz (overclock)` | `q88_cpu_clock` | `1` / `2` / `4` / `8` / `16` / `32` / `64` |
| Carte son | `OPN (Yamaha YM2203)` / `OPNA (Yamaha YM2608)` ✅ | `q88_sound_board` | `OPN` / `OPNA` |
| Attente du FDC | `Activé` ✅ / `Désactivé` | `q88_use_fdc_wait` | `enabled` / `disabled` |
| Utiliser PCG-8100 | `Désactivé` ✅ / `Activé` | `q88_use_pcg-8100` | `disabled` / `enabled` |
| Sauvegarder vers une image disque | `Désactivé` ✅ / `Activé` | `q88_save_to_disk_image` | `disabled` / `enabled` |
| Vibration lors de l'accès au disque | `Activé` ✅ / `Désactivé` | `q88_rumble` | `enabled` / `disabled` |
| Taille de l'écran | `Pleine résolution (640x400)` ✅ / `Demi résolution (320x200)` | `q88_screen_size` | `full` / `half` |

## ![](/emulators/external-links.png) Enlaces externos

* **Código fuente utilizado** : [https://github.com/libretro/quasi88-libretro/](https://github.com/libretro/quasi88-libretro/)
* **Documentación Libretro** : [https://docs.libretro.com/library/quasi88/](https://docs.libretro.com/library/quasi88/)