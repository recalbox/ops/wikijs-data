---
title: Unibios
description: 
published: true
date: 2023-08-13T23:38:42.104Z
tags: neo-geo, unibios
editor: markdown
dateCreated: 2021-05-21T08:46:59.063Z
---

## Que permet Unibios

L'UNIBIOS permet entre-autres :

* De modifier la région (Europe/USA/Japan) et le mode (AES/MVS).
* D'accéder au « soft dip » pour modifier la difficulté/vie/temps/etc. du jeu.
* D'utiliser la base de données de cheats code.
* D'accéder au jukebox... et bien d'autres encore !

## FinalBurn Neo

Le dernier UNIBIOS est fourni dans le romset FinalBurn Neo à jour.

### Installation

* Lancez un jeu et aller dans le menu de RetroArch avec `Hotkey + B` et allez dans `Options`.

![](/tutorials/games/guides/neo-geo/unibios/unibios1.png){.full-width}

* Dans les options, descendez sur le paramètre `Neo-Geo mode`.

![](/tutorials/games/guides/neo-geo/unibios/unibios2.png){.full-width}

* Sur cette option, choisissez `Use UNIBIOS bios`.

![](/tutorials/games/guides/neo-geo/unibios/unibios3.png){.full-width}

* Et fermez le menu RetroArch (retournez en jeu). Le jeu va redémarrer avec l'UNIBIOS activé automatiquement.

![](/tutorials/games/guides/neo-geo/unibios/unibios4.png){.full-width}

### Utilisation

* Démarrez n'importe quel jeu Neo-Geo, vous verrez alors le nouvel écran de boot UNIVERSE BIOS.
* Pendant cet écran de boot, pressez et maintenez les boutons `Y + B + A` (sur la manette originale, les boutons correspondent respectivement à `C + A + B`).

![Manette Neo-Geo](/tutorials/games/guides/neo-geo/unibios/neogeo-controller.jpg){.full-width}

![](/tutorials/games/guides/neo-geo/unibios/unibios5.png){.full-width}

* Pour quitter, utilisez le bouton `X` (bouton `C` de la manette originale).

* Pour accéder en jeu au menu, vous devez presser et maintenir les boutons `X + A + B + START`.

## Dip Switch

Pour accéder au menu « dip switch » pendant le démarrage de l'écran de boot UNIBIOS, maintenez `Y + X + A` (sur la manette originale, les boutons correspondent respectivement à `C + D + B`).

### Réglage du Dip Switch

Pour afficher le sang dans Metal Slug, une fois dans le menu du « dip switch » :

* Allez sur `SETTING UP THE SOFT DIP`.

![](/tutorials/games/guides/neo-geo/unibios/unibios6.png){.full-width}

* Allez dans `SLOT1 METAL SLUG`.

![](/tutorials/games/guides/neo-geo/unibios/unibios7.png){.full-width}

* Mettez-vous sur `BLOOD` et changez de `OFF` à `ON`.

![](/tutorials/games/guides/neo-geo/unibios/unibios8.png){.full-width}

Pour naviguer dans les menus, validez avec `A` et revenez en arrière avec `C`. Modifiez les valeurs souhaitées avec `A` et `B`.

## Contenu du BIOS

La Neo-Geo nécessite un fichier BIOS nommé `neogeo.zip`.

* Il sera placé avec vos ROMs dans :

```text
/recalbox/share/roms/neogeo
```
ou

```text
/recalbox/share/bios
```

* Voici le contenu d'un fichier BIOS neogeo.zip certifié fonctionnel

```text
000-lo.lo
japan-j3.bin
neo-epo.bin
neo-po.bin
neodebug.bin
neopen.sp1
sfix.sfix
sm1.sm1
sp-1v1_3db8c.bin
sp-45.sp1
sp-e.sp1
sp-j2.sp1
sp-j3.sp1
sp-s.sp1
sp-s2.sp1
sp-s3.sp1
sp-u2.sp1
sp1-j3.bin
sp1-u2
sp1-u3.bin
sp1-u4.bin
sp1.jipan.1024
uni-bios_1_0.rom
uni-bios_1_1.rom
uni-bios_1_2.rom
uni-bios_1_2o.rom
uni-bios_1_3.rom
uni-bios_2_0.rom
uni-bios_2_1.rom
uni-bios_2_2.rom
uni-bios_2_3.rom
uni-bios_2_3o.rom
uni-bios_3_0.rom
uni-bios_3_1.rom
uni-bios_3_2.rom
uni-bios_3_3.rom
uni-bios_4_0.rom
vs-bios.rom
```