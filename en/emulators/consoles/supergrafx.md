---
title: SuperGrafX
description: 
published: true
date: 2021-08-09T22:03:32.403Z
tags: consoles, nec, supergrafx
editor: markdown
dateCreated: 2021-07-28T17:59:32.990Z
---

![](/emulators/consoles/supergrafx.svg){.align-center}

## Technical data

* **Manufacturer**: NEC
* **Year of release**: 1989
* **Units sold**: 7,5 million
* **Best-selling game**: Ghouls 'n Ghosts
* **CPU**: HuC6280 8-bit 7,16 Mhz
* **RAM**: 8 Kbyte
* **Picture Processor Unit**: 16-bit
* **Video RAM**: 64 Kbyte
* **Media**: HU-Card, SHU-Card, CD-ROM, Super-CD-ROM

## Presentation

The **SuperGrafx** is a video game console manufactured by NEC Corporation and launched in December 1989 in Japan.

It is an improved version of the PC-Engine (or CoreGrafX). Compared to the latter, it has four times more memory for the processor, a second video chip with its own memory, and a priority controller that allowed to combine the output of the two video chips in different ways.

The console was released in France in May 1990 at a price of 2490 FF without games. In November 1990, the price was reduced to 1990 FF with a game, then in June 1991 to 1490 FF with a game.

The SuperGrafx was largely a failure due to a lack of software using its capabilities: only 5 titles were produced. However, it was compatible with many PC-Engine games. No CD, Super CD or Arcade CD game took advantage of the platform.

Moreover, its simple 8-bit processor had a rather heavy workload to control the additional video hardware

## Emulators

[Libretro Mednafen_SuperGrafx](libretro-mednafen_supergrafx)
[Libretro FBNeo](libretro-fbneo)