---
title: Quake 2
description: 
published: true
date: 2022-07-22T10:35:09.394Z
tags: ports, quake 2, vitaquake, 8.1+
editor: markdown
dateCreated: 2022-02-06T18:20:41.083Z
---

![](/emulators/ports/nofile.svg){.align-center}

## Fiche technique

* **Crée par :** id Software
* **Année de sortie :** 1997
* **Genre :** Tir à la première personne
* **Moteur :** Quake Engine
* **Dernière version :** Quake Champions
* **Système d'exploitation :** Windows 95/98/ME/NT 4.0/2000
* **Processeur :** Pentium 90 MHz ou supérieur ou Athlon
* **Stockage :** 25 MB d'espace disque + 45 MB pour le fichier swap Windows
* **RAM :** 16 MB RAM minimum avec Windows; 24 MB minimum sur les autres systèmes
* **Résolution :** 320x200 pixels
* **Son :** Sound Blaster et AdLib ou carte son 100% compatible

## Présentation

Un générateur de trou noir menace directement l'humanité en permettant aux troupes Stroggs l'accès à la Terre. Les chefs du monde planifient une Operation Alien Overlord calquée sur le débarquement de Normandie de 1944, à savoir débarquer de nombreuses troupes sur Stroggos, la capitale Stroggs où se trouve le fameux Big Gun qui empêche un largage à plus grande échelle (au lancement de la partie s'affiche à l'écran un ordre de mission très similaire à celui distribué aux soldats américains en 1944, à ceci près qu'il se termine par la phrase: « So go forth and kick ass soldiers ! »).

De gros navires porteurs assaillent donc Stroggos, par ce qui semble être un vortex géant, et débarquent de nombreuses petites navettes personnelles. Le joueur est à bord d'une de ces navettes, et dans la cinématique d'introduction, survole la ville, décrit un large cercle autour du monstrueux Big Gun, avant de s'écraser dans une banlieue reculée, mais tout de même solidement gardée.

## Émulateurs

[Libretro VitaQuake2](libretro-vitaquake2)