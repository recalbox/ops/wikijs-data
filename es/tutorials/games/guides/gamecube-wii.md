---
title: GameCube / Wii
description: 
published: true
date: 2024-07-26T07:02:54.398Z
tags: nintendo, gamecube, wii, gc, juegos
editor: markdown
dateCreated: 2023-09-14T07:49:40.757Z
---

En esta sección encontraréis toda la información necesaria para jugar mejor a vuestros juegos de GameCube y Wii.

Aqui tenéis la lista de tutoriales disponibles:

[Definir la posición del Dolphin Bar](define-dolphinbar-position)
[Jugar a varios en la Wii](multiplayer-games-on-wii)
[Jugar con los verdaderos mandos Gamecube](use-original-gamecube-controllers)
[Jugar con los verdaderos mandos Wiimotes](use-original-wiimotes-controllers)
[Packs de texturas personnalizadas](custom-hires-textures-packs)
[Poner los juegos Wii/GameCube multi-lenguajes con el idioma que queréis](set-multilingual-wii-gamecube-games-in-your-language)
[Utilizar los Wiimotes como mandos](use-wiimotes-as-controllers)