---
title: Libretro GenesisPlusGX
description: 
published: true
date: 2023-09-07T20:14:24.667Z
tags: libretro, genesisplusgx, sg-1000
editor: markdown
dateCreated: 2023-09-07T20:14:24.667Z
---

**Libretro GenesisPlusGX** es un emulador Sega 8/16 bits open-source centrado en la precisión y en la portabilidad.

## ![](/emulators/license.svg) Licencia

El core posee la siguiente licencia [**non-commerciale**](https://github.com/libretro/Genesis-Plus-GX/blob/master/LICENSE.txt).

## ![](/emulators/compatibility.png) Compatibilidad

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Funcionalidades

| Funcionalidad | ¿Soportada? |
| :---: | :---: |
| Reinicio | ✔ |
| Capturas de pantalla | ✔ |
| Grabaciones | ✔ |
| Grabaciones instantáneas | ✔ |
| Rebobinado | ✔ |
| Netplay | ✔ |
| Opciones del core | ✔ |
| RetroAchievements | ✔ |
| Cheats RetroArch | ✔ |
| Controles | ✔ |
| Remapping | ✔ |
| Multi-Ratón | ✔ |

## ![](/emulators/bios.svg) BIOS

>**No se necesitan bios.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Extensiones de roms soportadas

Las roms deben tener las extensiones siguientes:

* .sg
* .zip
* .7z

Este sistema soporta las roms comprimidas en formato .zip/.7z. No obstante, se trata sólo de un archivo.

Los ficheros contenidos en los .zip/.7z deben tener las extensiones citadas previamente.
Además, cada fichero .zip/.7z sólo debe contener un fichero rom al interior.

### Emplazamiento

Disponed vuestras roms como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 sg1000
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.zip**

>Se aconsejan las roms del proyecto **No-Intro**.
{.is-success}

>Para más información sobre las roms, consultad [este tutorial](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

>Para conservar las configuraciones personalizadas cuando se produce una actualización del sistema Recalbox, os aconsejamos utilizar las [Sobrecargas de configuración](./../../../advanced-usage/configuration-override).
{.is-info}

### Acceder a las opciones

Podéis configurar las opciones del core de dos formas diferentes.

* Via el Menú RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via el fichero `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Opciones del core

| Opción | Valor de la opción | Variable (para la sobrecarga) | Valores posibles |
| --- | --- | --- | --- |
| System Hardware | `Auto` ✅ / `SG-1000` / `SG-1000 II` / `Mark III` / `Master System` / `Master System II` / `Game Gear` / `Mega Drive/Genesis` | `genesis_plus_gx_system_hw` | `auto` / `sg-1000` / `sg-1000 II` / `mark-III` / `master system` / `master system II` / `game gear` / `mega drive / genesis` |
| System Region | `Auto` ✅ / `NTSC-U` / `PAL` / `NTSC-J` | `genesis_plus_gx_region_detect` | `auto` / `ntsc-u` / `pal` / `ntsc-j` |
| System Boot ROM | `Désactivé` ✅ / `Activé` | `genesis_plus_gx_bios` | `disabled` / `enabled` |
| CD System BRAM | `Per-BIOS` ✅ / `Per-Game` | `genesis_plus_gx_bram` | `per bios` / `per game` |
| CD add-on (MD mode) (Requires Restart) | `Auto` ✅ / `Sega/Mega CD` / `MegaSD` / `None` | `genesis_plus_gx_add_on` | `auto` / `sega/mega cd` / `megasd` / `none` |
| Cartridge Lock-On | `Désactivé` ✅ / `Game Genie` / `Action Replay (Pro)` / `Sonic & Knuckles` | `genesis_plus_gx_lock_on` | `disabled` / `game genie` / `action replay (pro)` / `sonic & knuckles` |
| Core-Provided Aspect Ratio | `Auto` ✅ / `NTSC PAR` / `PAL PAR` | `genesis_plus_gx_aspect_ratio` | `auto` / `NTSC PAR` / `PAL PAR` |
| Borders | `Désactivé` ✅ / `Top/Bottom` / `Left/Right` / `Full` | `genesis_plus_gx_overscan` | `disabled` / `top/bottom` / `left/right` / `full` |
| Hide Master System Side Borders | `Désactivé` ✅ / `Activé` | `genesis_plus_gx_left_border` | `disabled` / `enabled` |
| Game Gear Extended Screen | `Désactivé` ✅ / `Activé` | `genesis_plus_gx_gg_extra` | `disabled` / `enabled` |
| Blargg NTSC Filter | `Désactivé` ✅ / `Monochrome` / `Composite` / `S-Video` / `RGB` | `genesis_plus_gx_blargg_ntsc_filter` | `disabled` / `monochrome` / `composite` / `svideo` / `rgb` |
| LCD Ghosting Filter | `Désactivé` ✅ / `Activé` | `genesis_plus_gx_lcd_filter` | `disabled` / `enabled` |
| Interlaced Mode 2 Output | `Single Field` ✅ / `Double Field` | `genesis_plus_gx_render` | `single field` / `double field` |
| Frameskip | `Désactivé` ✅ / `Auto` / `Manual` | `genesis_plus_gx_frameskip` | `disabled` / `auto` / `manual` |
| Frameskip Threshold (%) | `15` / `18` / `21` / `24` / `27` / `30` / `33` ✅ / `36` / `39` / `42` / `45` / `48` / `51` / `54` / `57` / `60` | `genesis_plus_gx_frameskip_threshold` | `15` / `18` / `21` / `24` / `27` / `30` / `33` / `36` / `39` / `42` / `45` / `48` / `51` / `54` / `57` / `60` |
| Master System FM (YM2413) | `Auto` ✅ / `Désactivé` / `Activé` | `genesis_plus_gx_ym2413` | `auto` / `disabled` / `enabled` |
| Master System FM (YM2413) Core | `MAME` ✅ / `Nuked` | `genesis_plus_gx_ym2413_core` | `mame` / `nuked` |
| Mega Drive / Genesis FM | `MAME (YM2612)` ✅ / `MAME (ASIC YM3438)` / `MAME (Enhanced YM3438)` / `Nuked (YM2612)` / `Nuked (YM3438)` | `genesis_plus_gx_ym2612` | `mame (ym2612)` / `mame (asic ym3438)` / `mame (enhanced ym3438)` / `nuked (ym2612)` / `nuked (ym3438)` |
| Sound Output | `Stereo` ✅ / `Mono` | `genesis_plus_gx_sound_output` | `stereo` / `mono` |
| Audio Filter | `Désactivé` ✅ / `Low-Pass` | `genesis_plus_gx_audio_filter` | `disabled` / `low-pass` |
| Low-Pass Filter % | `5` / `10` / `15` / `20` / `25` / `30` / `35` / `40` / `45` / `50` / `55` / `60` ✅ / `65` / `70` / `75` / `80` / `85` / `90` / `95` | `genesis_plus_gx_lowpass_range` | `5` / `10` / `15` / `20` / `25` / `30` / `35` / `40` / `45` / `50` / `55` / `60` / `65` / `70` / `75` / `80` / `85` / `90` / `95` |
| PSG Preamp Level | `0` / `5` / `10` / `15` / `20` / `25` / `30` / `35` / `40` / `45` / `50` / `55` / `60` / `65` / `70` / `75` / `80` / `85` / `90` / `95` / `100` / `105` / `110` / `115` / `120` / `125` / `130` / `135` / `140` / `145` / `150` ✅ / `155` / `160` / `165` / `170` / `175` / `180` / `185` / `190` / `195` / `200` | `genesis_plus_gx_psg_preamp` | `0` / `5` / `10` / `15` / `20` / `25` / `30` / `35` / `40` / `45` / `50` / `55` / `60` / `65` / `70` / `75` / `80` / `85` / `90` / `95` / `100` / `105` / `110` / `115` / `120` / `125` / `130` / `135` / `140` / `145` / `150` / `155` / `160` / `165` / `170` / `175` / `180` / `185` / `190` / `195` / `200` |
| FM Preamp Level | `0` / `5` / `10` / `15` / `20` / `25` / `30` / `35` / `40` / `45` / `50` / `55` / `60` / `65` / `70` / `75` / `80` / `85` / `90` / `95` / `100` ✅ / `105` / `110` / `115` / `120` / `125` / `130` / `135` / `140` / `145` / `150` / `155` / `160` / `165` / `170` / `175` / `180` / `185` / `190` / `195` / `200` | `genesis_plus_gx_fm_preamp` | `0` / `5` / `10` / `15` / `20` / `25` / `30` / `35` / `40` / `45` / `50` / `55` / `60` / `65` / `70` / `75` / `80` / `85` / `90` / `95` / `100` / `105` / `110` / `115` / `120` / `125` / `130` / `135` / `140` / `145` / `150` / `155` / `160` / `165` / `170` / `175` / `180` / `185` / `190` / `195` / `200` |
| CD-DA Volume | `0` / `5` / `10` / `15` / `20` / `25` / `30` / `35` / `40` / `45` / `50` / `55` / `60` / `65` / `70` / `75` / `80` / `85` / `90` / `95` / `100` ✅ | `genesis_plus_gx_cdda_volume` | `0` / `5` / `10` / `15` / `20` / `25` / `30` / `35` / `40` / `45` / `50` / `55` / `60` / `65` / `70` / `75` / `80` / `85` / `90` / `95` / `100` |
| PCM Volume | `0` / `5` / `10` / `15` / `20` / `25` / `30` / `35` / `40` / `45` / `50` / `55` / `60` / `65` / `70` / `75` / `80` / `85` / `90` / `95` / `100` ✅ | `genesis_plus_gx_pcm_volume` | `0` / `5` / `10` / `15` / `20` / `25` / `30` / `35` / `40` / `45` / `50` / `55` / `60` / `65` / `70` / `75` / `80` / `85` / `90` / `95` / `100` |
| Light Gun Input | `Light Gun` ✅ / `Touchscreen` | `genesis_plus_gx_gun_input` | `lightgun` / `touchscreen` |
| Show Light Gun Crosshair | `Désactivé` ✅ / `Activé` | `genesis_plus_gx_gun_cursor` | `disabled` / `enabled` |
| Invert Mouse Y-Axis | `Désactivé` ✅ / `Activé` | `genesis_plus_gx_invert_mouse` | `disabled` / `enabled` |
| Remove Per-Line Sprite Limit | `Désactivé` ✅ / `Activé` | `genesis_plus_gx_no_sprite_limit` | `disabled` / `enabled` |
| CPU Speed | `100%` ✅ / `125%` / `150%` / `175%` / `200%` | `genesis_plus_gx_overclock` | `100%` / `125%` / `150%` / `175%` / `200%` |
| System Lock-Ups | `Activé` ✅ / `Désactivé` | `genesis_plus_gx_force_dtack` | `enabled` / `disabled` |
| 68K Address Error | `Activé` ✅ / `Désactivé` | `genesis_plus_gx_addr_error` | `enabled` / `disabled` |
| PSG Tone Channel 0 Volume % | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `genesis_plus_gx_psg_channel_0_volume` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
| PSG Tone Channel 1 Volume % | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `genesis_plus_gx_psg_channel_1_volume` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
| PSG Tone Channel 2 Volume % | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `genesis_plus_gx_psg_channel_2_volume` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
| PSG Tone Channel 3 Volume % | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `genesis_plus_gx_psg_channel_3_volume` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
| Mega Drive / Genesis FM Channel 0 Volume % | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `genesis_plus_gx_md_channel_0_volume` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
| Mega Drive / Genesis FM Channel 1 Volume % | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `genesis_plus_gx_md_channel_1_volume` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
| Mega Drive / Genesis FM Channel 2 Volume % | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `genesis_plus_gx_md_channel_2_volume` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
| Mega Drive / Genesis FM Channel 3 Volume % | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `genesis_plus_gx_md_channel_3_volume` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
| Mega Drive / Genesis FM Channel 4 Volume % | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `genesis_plus_gx_md_channel_4_volume` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
| Mega Drive / Genesis FM Channel 5 Volume % | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `genesis_plus_gx_md_channel_5_volume` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
| Master System FM (YM2413) Channel 0 Volume % | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `genesis_plus_gx_sms_fm_channel_0_volume` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
| Master System FM (YM2413) Channel 1 Volume % | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `genesis_plus_gx_sms_fm_channel_1_volume` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
| Master System FM (YM2413) Channel 2 Volume % | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `genesis_plus_gx_sms_fm_channel_2_volume` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
| Master System FM (YM2413) Channel 3 Volume % | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `genesis_plus_gx_sms_fm_channel_3_volume` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
| Master System FM (YM2413) Channel 4 Volume % | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `genesis_plus_gx_sms_fm_channel_4_volume` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
| Master System FM (YM2413) Channel 5 Volume % | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `genesis_plus_gx_sms_fm_channel_5_volume` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
| Master System FM (YM2413) Channel 6 Volume % | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `genesis_plus_gx_sms_fm_channel_6_volume` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
| Master System FM (YM2413) Channel 7 Volume % | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `genesis_plus_gx_sms_fm_channel_7_volume` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
| Master System FM (YM2413) Channel 8 Volume % | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `genesis_plus_gx_sms_fm_channel_8_volume` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |

## ![](/emulators/external-links.png) Enlaces externos

* **Código fuente utilizado** : [https://github.com/libretro/Genesis-Plus-GX/](https://github.com/libretro/Genesis-Plus-GX/)
* **Documentación Libretro** : [https://docs.libretro.com/library/genesis_plus_gx/](https://docs.libretro.com/library/genesis_plus_gx/)