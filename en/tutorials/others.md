---
title: 📌 Others
description: 
published: true
date: 2021-08-06T16:57:55.708Z
tags: tutorial, others
editor: markdown
dateCreated: 2021-08-06T08:01:08.580Z
---

Here are the "unclassifiable" tutorials that can be useful.

Here are the available tutorials:

[Add Mausberry switch on/off button](add-mausberry-switch-on-off-button)
[Add on/off button to your Recalbox](add-on-off-button-to-your-recalbox)
[Change RetroArch language](change-retroarch-language)
[Disable Mac T2 chip](disable-mac-t2-chip)
[EmulationStation use with keyboard and mouse](emulationstation-use-with-keyboard-and-mouse)
[GameHat configuration](gamehat-configuration)
[NesPi4Case installation](nespi4case-installation)
[Useful Linux commands](useful-linux-commands)