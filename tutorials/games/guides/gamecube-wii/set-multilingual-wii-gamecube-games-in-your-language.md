---
title: Mettre les jeux Wii/GameCube multi-langues avec la langue de votre choix
description: 
published: true
date: 2021-09-05T13:01:44.002Z
tags: gamecube, wii, jeux, langue, multi-langues
editor: markdown
dateCreated: 2021-05-21T08:46:21.040Z
---

## Introduction

Sur les jeux avec plusieurs langues, vous ne pouvez pas la choisir au démarrage d'un jeu puisque c'est dans le système que la langue est définie.

## Utilisation générale

Pour passer les jeux dans la langue de votre choix, voici la manipulation à effectuer :

* Lancez un jeu GameCube ou Wii.
* Avec un clavier, faites `Alt + F4`.
* Avec la souris, allez dans `Configurer`.

![](/tutorials/games/guides/gamecube-wii/change-games-language/dolphinlanguage1.png)

## {.tabset}

### Pour un jeu GameCube

* Dans la nouvelle fenêtre, cliquez sur `GameCube` tout en haut.
* Modifiez l'option `Langue du système`.

![](/tutorials/games/guides/gamecube-wii/change-games-language/dolphinlanguage2.png){.full-width}

* Validez et quittez l'émulateur pour le relancer.

### Pour un jeu Wii

* Dans la nouvelle fenêtre, cliquez sur `Wii` tout en haut.
* Modifiez l'option `Langue du système`.

![](/tutorials/games/guides/gamecube-wii/change-games-language/dolphinlanguage3.png){.full-width}

* Validez et quittez l'émulateur pour le relancer.