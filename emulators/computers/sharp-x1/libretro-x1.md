---
title: Libretro x1
description: 
published: true
date: 2024-07-24T22:14:52.491Z
tags: libretro, x1
editor: markdown
dateCreated: 2021-07-29T17:31:38.862Z
---



## ![](/emulators/license.svg) License

This core is under [**BSD-3-Clause**](https://github.com/libretro/xmil-libretro/blob/master/LICENSE) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Features



## ![](/emulators/bios.svg) BIOS

### List of mandatory bios

| Filename | Description | MD5 | Provided |
| :---: | :---: | :---: | :---: |
| IPLROM.X1 | - | eeeea1cd29c6e0e8b094790ae969bfa7 59074727a953fe965109b7dbe3298e30 | ❌ |
| IPLROM.X1T | - | 56c28adcf1f3a2f87cf3d57c378013f5 | ❌ |

### Location

Put the bios like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 xmil
┃ ┃ ┃ ┃ ┣ 🗒 **IPLROM.X1**
┃ ┃ ┃ ┃ ┣ 🗒 **IPLROM.X1T**

## ![](/emulators/roms.png) Roms

### Supported extensions

Roms must have the extension:

* .dx1
* .2d
* .2hd
* .tfd
* .d88
* .88d
* .hdm
* .xdf
* .dup
* .cmd
* .m3u
* .zip
* .7z

This system supports compressed roms in .zip/.7z format. However, be careful, it is only an archive.

The files contained in the .zip/.7z must correspond to the extensions mentioned above.
Moreover, each .zip/.7z file must contain only one compressed rom.

### Location

Put the roms like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 x1
┃ ┃ ┃ ┃ ┣ 🗒 **game.zip**

>The roms using **TOSEC** format are highly recommended.
{.is-success}

>For more information about roms, go to [this tutorial](./../../../tutorials/games/generalities/isos-and-roms)!
{.is-info}

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Accessing to options

You can configure various options in two different ways.

* Via RetroArch menu:

┣ 📁 RetroArch Menu
┃ ┣ 📁 Core options
┃ ┃ ┣ 🧩 Name_option

* Via the file `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core options

| Options | Options values | Variables (for configuration override) | Variables values |
| --- | --- | --- | --- |
| Resolution | `LOW` ✅ / `HIGH` | `X1_RESOLUTE` | `LOW` / `HIGH` |
| Boot media | `2HD` ✅ / `2D` | `X1_BOOTMEDIA` | `2HD` / `2D` |
| ROM type | `X1` ✅ / `TURBO` / `TURBOZ` | `X1_ROMTYPE` | `X1` / `TURBO` / `TURBOZ` |
| FPS | `AUTO` ✅ / `60` / `30` / `20` / `15` | `X1_FPS` | `AUTO` / `60` / `30` / `20` / `15` |
| Disp Vsync | `Disabled` ✅ / `Enabled` | `X1_DISPSYNC` | `OFF` / `ON` |
| Raster | `Disabled` ✅ / `Enabled` | `X1_RASTER` | `OFF` / `ON` |
| No wait | `Disabled` ✅ / `Enabled` | `X1_NOWAIT` | `OFF` / `ON` |
| Joy Reverse | `Disabled` ✅ / `Enabled` | `X1_BTN_MODE` | `OFF` / `ON` |
| Joy Rapid | `Disabled` ✅ / `Enabled` | `X1_BTN_RAPID` | `OFF` / `ON` |
| Audio sampling rate | `44100` ✅ / `22050` / `11025` | `X1_AUDIO_RATE` | `44100` / `22050` / `11025` |
| Seek Sound | `Disabled` ✅ / `Enabled` | `X1_SEEKSND` | `OFF` / `ON` |
| Scanlines in low-res | `Disabled` ✅ / `Enabled` | `X1_SCANLINES` | `OFF` / `ON` |
| Key mode | `Keyboard` ✅ / `JoyKey-1` / `JoyKey-2` / `Mouse-Key` | `X1_KEY_MODE` | `Keyboard` / `JoyKey-1` / `JoyKey-2` / `Mouse-Key` |
| FM Board | `Disabled` / `Enabled` ✅ | `X1_FMBOARD` | `OFF` / `ON` |
| Audio buffer (ms) | `100` / `150` / `200` / `250` ✅ / `300` / `350` / `500` / `750` / `1000` | `X1_AUDIO_DELAYMS` | `100` / `150` / `200` / `250` / `300` / `350` / `500` / `750` / `1000` |
| Cpu clock (MHz) | `1` / `2` / `3` / `4` ✅ / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` | `X1_CPU_CLOCK` | `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` |

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/libretro/xmil-libretro/](https://github.com/libretro/xmil-libretro/)