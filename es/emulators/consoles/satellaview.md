---
title: Satellaview
description: Nintendo Satellaview
published: true
date: 2024-07-26T08:05:14.534Z
tags: nintendo, satellaview, bs-x, consolas
editor: markdown
dateCreated: 2023-09-03T20:16:37.502Z
---

![](/emulators/consoles/satellaview.svg){.align-center}

## Detalles técnicos

* **Fabricante**: Nintendo
* **Año de lanzamiento**: 1995
* **Cantidad vendida**: 2 millones
* **ROM**: 1024 KB
* **RAM**: 512 KB
* **Memoria interna**: 256 KB de memoria flash
* **Soportes**: cartucho de aplicación BS-X + memoria opcional de 8MB

## Presentación

El **Satellaview** es una extensión de la Super Famicom, comercializada únicamente en Japón a partir de 1995.

También conocido por las iniciales **BS-X**, este sistema de juegos, suministrado con un cartucho de tipo "BS-X" y una tarjeta de memoria de 8 Mbit, aportaba a la Super Famicom una serie de capacidades técnicas suplementarias, así como un servicio de descarga de juegos, códigos e información. Todos los datos pasaban a través de una estación de televisión por satélite llamada St.GIGA.

Satellaview se lanzó oficialmente el 23 de abril de 1995. El primer juego emitido fue BS Zelda. Como su nombre indica, era un remake del primer juego de la serie The Legend of Zelda, actualizado con muchos añadidos y cambios.

Durante el primer año de funcionamiento de Satellaview, era posible consultar información "en línea", así como reportajes de audio y vídeo. Los contenidos se actualizaban con regularidad, y en los juegos episódicos se añadía una nueva entrega cada 7 días.  
Pero en 1996, la generación de consolas de 32 bits ya estaba bien asentada (se habían vendido más de un millón de unidades de PlayStation). Incluso entonces, las ventas de Satellaview empezaban a considerarse insuficientes. El servicio prestado por St.GIGA se resintió, con un gran número de repeticiones que sustituían a los nuevos contenidos. En 1997, los programas se redujeron en dos horas. En 1998, los propietarios de Satellaview se encontraron con otra hora menos.

En abril de 1999, a raíz de desacuerdos financieros entre Nintendo y St.GIGA, en particular sobre la reestructuración de la considerable deuda acumulada por St.GIGA, Nintendo dejó definitivamente de suministrar nuevos programas al servicio y se retiró del proyecto. A falta de nuevos programas, St.GIGA mantuvo el servicio hasta junio de 2000, pero debido a la falta de nuevos contenidos, sólo pudo ofrecer reposiciones. Finalmente, el servicio se cerró la noche del 30 de junio de 2000.

## Emuladores

[Libretro Mesen_S](libretro-mesen_s)
[Libretro SNES9x](libretro-snes9x)
[Libretro bsnes](libretro-bsnes)
[Libretro bsnes HD](libretro-bsnes-hd)
