---
title: Liste des jeux compatibles
description: 
published: true
date: 2025-02-23T15:57:42.482Z
tags: lightgun, 7.2+, jeux
editor: markdown
dateCreated: 2021-08-17T23:46:57.376Z
---

## Introduction

Ici, vous pouvez avoir la liste des jeux reconnues comme lightgun par système.

Cette liste comporte tous les jeux reconnus actuellement comme utilisant le lightgun. Seuls les jeux avec la compatibilité ✅ sont listés dans le système virtuel LightGun dans EmulationStation. Les jeux marqués d'un ❌ ne fonctionnent pas ou peut-être mal et ceux marqués d'un ❔ n'ont pas été testés pour diverses raisons (fonctionnent que partiellement et pas au début du jeu, etc.).

La liste de compatibilité du core ne prend pas en compte si c'est une version portable ou non, elle indique juste si l'émulateur nécessaire est inclus ou non.

Cette liste sera mise à jour pour chaque nouvelle version si nécessaire.

## Nintendo Entertainment System

### Emulateur

L'émulateur requis est le core `Libretro FCEUmm`.

### Compatibilité

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

### Jeux

| Nom du jeu | Compatibilité | Notes |
| :--- | :--- | :--- |
| 3 In 1 Supergun | ✅ | - |
| Adventures of Bayou Billy | ✅ | - |
| Asder - 20 In 1 | ✅ | - |
| Baby Boomer | ✅ | - |
| Barker Bill's Trick Shooting | ✅ | - |
| Chiller | ✅ | - |
| Cobra Mission | ✅ | - |
| Crime Busters | ✅ | - |
| Day Dreamin' Davey | ✅ | - |
| Duck Hunt | ✅ | - |
| Freedom Force | ✅ | - |
| Gotcha! The Sport! | ✅ | - |
| Gumshoe | ✅ | - |
| Hit Marmot | ✅ | - |
| Hogan's Alley | ✅ | - |
| Laser Invasion | ❌ | La visée ne bouge pas en jeu. |
| Lethal Weapon | ❌ | Ne semble pas être un jeu lightgun. |
| Liesha Zhuluoji - Blood of Jurassic | ✅ | - |
| Lightgun Games 2 in 1 - Cosmocop + Cyber Monster | ✅ | - |
| Lightgun Games 2 in 1 - Tough Cop + Super Tough Cop | ✅ | - |
| Lone Ranger, The | ✅ | Certaines parties sont jouables au lightgun, le reste se joue à la manette. |
| Master Shooter | ✅ | - |
| Mechanized Attack | ✅ | - |
| Operation Wolf | ✅ | - |
| Russian Roulette | ✅ | Uniquement pour répliquer le mouvement de mettre le lightgun sur la tempe. |
| Shooting Range | ✅ | - |
| Space Shadow | ✅ | - |
| Strike Wolf | ✅ | - |
| Super Russian Roulette | ❌ | Echec du chargement du contenu. |
| To The Earth | ✅ | - |
| Track & Field II | ✅ | Uniquement sur un mini-jeu difficile à débloquer. |
| Wild Gunman | ✅ | - |

## Super Nintendo

### Emulateur

L'émulateur requis est le core `Libretro snes9x`.

### Compatibilité

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

### Jeux

| Nom du jeu | Compatibilité | Notes |
| :--- | :--- | :--- |
| Battle Clash | ✅ | - |
| Bazooka Blitzkrieg | ✅ | - |
| Lamborghini American Challenge | ✅ | - |
| Lethal Enforcers | ✅ | - |
| Metal Combat - Falcon's Revenge | ✅ | - |
| Nintendo Scope 6 | ✅ | - |
| Operation Thunderbolt | ✅ | - |
| Revolution X | ❌ | Viseur décalé, pas de calibrage en jeu, tir sur bouton L de manette. |
| Super Scope 6 | ✅ | - |
| T2 - The Arcade Game | ✅ | - |
| Tin Star | ✅ | - |
| Yoshi's Safari | ✅ | - |
| X Zone | ✅ | - |

## Megadrive

### Emulateur

L'émulateur requis est le core `Libretro GenesisPlusGX`.

### Compatibilité

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

### Jeux

| Nom du jeu | Compatibilité | Notes |
| :--- | :--- | :--- |
| Body Count | ❌ | Impossible de sélectionner le Menacer au début du jeu. |
| Lethal Enforcers | ✅ | - |
| Lethal Enforcers II - Gun Fighters | ✅ | - |
| Menacer - 6-Game Cartridge | ✅ | - |
| T2 - The Arcade Game | ❌ | Impossible de sélectionner le Menacer au début du jeu. |

## Mega-CD

### Emulateur

L'émulateur requis est le core `Libretro GenesisPlusGX`.

### Compatibilité

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

### Jeux

| Nom du jeu | Compatibilité | Notes |
| :--- | :--- | :--- |
| Corpse Killer | ✅ | Refus du calibrage dans les options. |
| Crime Patrol | ✅ | - |
| Ground Zero Texas | ❌ | - |
| Mad Dog McCree | ✅ | Le calibrage ne se fait pas correctement mais peut rester jouable. |
| Mad Dog II: The Lost Gold | ✅ | - |
| Snatcher | ❌ | Quelques légères phases de jeu jouables au lightgun mais pas le jeu entier. |
| Who Shot Johnny Rock ? | ✅ | Le calibrage ne se fait pas correctement mais peut rester jouable. |

## Master System

### Emulateur

L'émulateur requis est le core `Libretro GenesisPlusGX`.

### Compatibilité

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

### Jeux

| Nom du jeu | Compatibilité | Notes |
| :--- | :--- | :--- |
| Assault City (Light Phaser) | ✅ | - |
| Bank Panic | ❌ | Le tir ne suit pas la visée. |
| Gangster Town | ✅ | - |
| Laser Ghost | ✅ | Besoin d'appuyer sur 1 ou 2 pour démarrer le mode Light Phaser depuis la manette 2. |
| Marksman Shooting & Trap Shooting & Safari Hunt | ✅ | - |
| Missile Defense 3-D | ✅ | - |
| Operation Wolf | ✅ | - |
| Rambo III | ✅ | - |
| Rescue Mission | ✅ | - |
| Shooting Gallery ~ Shooting G. | ✅ | - |
| Space Gun | ✅ | - |
| Wanted | ✅ | - |

## Dreamcast

### Emulateur

L'émulateur requis est le core `Libretro Flycast`.

### Compatibilité

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ❌ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

### Jeux

| Nom du jeu | Compatibilité | Notes |
| :--- | :--- | :--- |
| Confidential Mission | ✅ | - |
| Death Crimson 2 | ✅ | - |
| Death Crimson OX | ✅ | - |
| House of the Dead 2 | ✅ | - |
| Virtua Cop 2 | ✅ | - |

## Playstation 1

### Emulateur

L'émulateur requis est le core `Libretro Mednafen_PSX`.

### Compatibilité

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ❌ | ❌ | ✅ | ✅ | ❌ | ✅ | ✅ | ✅ |

### Jeux

| Nom du jeu | Compatibilité | Notes |
| :--- | :--- | :--- |
| Area 51 | ✅ | - |
| Crypt Killer | ✅ | - |
| Die Hard Trilogy | ✅ | - |
| Die Hard Trilogy 2 | ✅ | - |
| Elemental Gearbolt | ✅ | - |
| Extreme Ghostbusters: The Ultimate Invasion | ✅ | Besoin de presser - et + sur la wiimote pour démarrer/mettre en pause le jeu (correspond aux boutons A et B du Gcon45). Presser - pour recharger. |
| Ghoul Panic | ✅ | - |
| Gumbare! Game Tengoku - The Game Paradise 2 | ✅ | - |
| Gunfighter: The Legend of Jesse James | ✅ | - |
| Guntu: Western Front June, 1944: Tetsu no Kioku | ✅ | - |
| Judge Dredd | ✅ | - |
| Lethal Enforcers I & II | ✅ | - |
| Maximum Force | ✅ | - |
| Mighty Hits Special | ✅ | - |
| Moorhen 3: Chicken Chase | ✅ | - |
| Moorhuhn 2: Die Jagd Geht Weiter | ✅ | - |
| Moorhuhn 3 - Chasse aux poulets !!! | ❌ | Semble inexistant en dump, impossible de tester. |
| Moorhuhn X | ✅ | - |
| Omega Assault | ❌ | Impossible de passer l'écran titre. |
| Point Blank | ✅ | - |
| Point Blank 2 | ✅ | - |
| Point Blank 3 | ✅ | - |
| Project: Horned Owl | ❌ | - |
| Puffy no P.S. I Love You | ❌ | Pas de commande. |
| Rescue Shot | ❔ | - |
| Rescue Shot Bubibo | ❔ | - |
| Rescue Shot Bubibo & Biohazard: Gun Survivor: GunCon Taiou Shooting Taikenban | ❔ | - |
| Resident Evil: Survivor | ✅ | - |
| Revolution X | ❌ | Pas de commande. |
| Serofans | ❌ | Ne lance pas le jeu. |
| Simple 1500 Series Vol. 24: The Gun Shooting | ✅ | - |
| Simple 1500 Series Vol. 63: The Gun Shooting 2 | ✅ | - |
| Time Crisis | ✅ | - |
| Time Crisis: Project Titan | ✅ | - |

## Saturn

### Emulateur

L'émulateur requis est le core `Libretro Mednafen_Saturn`.

### Compatibilité

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ❌ | ❌ | ❌ | ✅ | ❌ | ❌ | ✅ | ❌ |

### Jeux

| Nom du jeu | Compatibilité | Notes |
| :--- | :--- | :--- |
| Area 51 | ✅ | - |
| Chaos Control | ✅ | - |
| Corpse Killer: Graveyard Edition | ❌ | Aucune réaction à l'écran titre. |
| Crypt Killer | ✅ | - |
| Death Crimson | ❌ | Aucun tir en jeu. |
| Die Hard Trilogy | ❔ | - |
| House of the Dead | ✅ | - |
| Maximum Force | ✅ | - |
| Mechanical Violator Hakaider: Last Judgement | ❌ | - |
| Mighty Hits | ✅ | - |
| Policenauts | ❌ | Point & click |
| Revolution X | ❌ | Ne se lance pas. |
| Scud: The Disposable Assassin | ✅ | Impossible de calibrer. |
| Virtua Cop | ✅ | - |
| Virtua Cop 2 | ✅ | - |

## 3DO

### Emulateur

L'émulateur requis est le core `Libretro Opera`.

### Compatibilité

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ❌ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

### Jeux

| Nom du jeu | Compatibilité | Notes |
| :--- | :--- | :--- |
| Burning Soldier | ❌ | Plante pendant l'intro. |
| Corpse Killer | ✅ | - |
| Creature Shock | ❌ | Pas de réaction sur l'écran titre. |
| Crime Patrol | ✅ | - |
| CyberDillo | ❌ | Pas de réaction sur l'écran titre. |
| Demolition Man | ❌ | Pas de réaction sur l'écran titre. |
| Drug Wars | ✅ | - |
| Killing Time | ❌ | Plante pendant l'intro. |
| Last Bounty Hunter, The | ✅ | - |
| Mad Dog McCree | ✅ | - |
| Mad Dog II: The Lost Gold | ✅ | - |
| Shootout at Old Tucson | ❌ | Jeu non trouvé au lancement. |
| Space Pirates | ✅ | - |
| Who Shot Johnny Rock ? | ✅ | - |

## Atomiswave

### Emulateur

L'émulateur requis est le core `Libretro Flycast`.

### Compatibilité

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ❌ | ❌ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

### Jeux

| Nom du jeu | Compatibilité | Notes |
| :--- | :--- | :--- |
| Extreme Hunting | ✅ | - |
| Extreme Hunting 2 | ❌ | Impossible de reload. |
| Ranger Mission | ✅ | - |
| Sega Clay Challenge | ✅ | - |
| Sports Shooting USA | ✅ | - |

## Naomi

### Emulateur

L'émulateur requis est le core `Libretro Flycast`.

### Compatibilité

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ❌ | ❌ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

### Jeux

| Nom du jeu | Compatibilité | Notes |
| :--- | :--- | :--- |
| Death Crimson OX | ✅ | - |
| House of the Dead 2 | ✅ | - |
| Gun Survivor 2 Biohazard Code: Veronica | ❌ | Pas de viseur, aucune réponse des armes. |
| House of the Dead 2, The | ✅ | - |
| Mazan: Flash of the Blade | ✅ | - |
| Ninja Assault | ❌ | Aucune commande ne fonctionne. |

## Naomi GD

### Emulateur

L'émulateur requis est le core `Libretro Flycast`.

### Compatibilité

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ❌ | ❌ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

### Jeux

| Nom du jeu | Compatibilité | Notes |
| :--- | :--- | :--- |
| Confidential Mission | ✅ | - |
| Lupin The Third - The Shooting | ✅ | - |
| Maze of the King, The | ✅ | - |

## Mame

### Emulateur

L'émulateur requis est le core `Libretro MAME`.

### Compatibilité

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ❌ | ❌ | ✅ | ✅ | ❌ | ❌ | ✅ | ❌ |

### Jeux

| Nom du jeu | Compatibilité | Notes |
| :--- | :--- | :--- |
| Alien3: The Gun | ✅ | - |
| Area 51 (R3000) | ✅ | - |
| Area 51 / Maximum Force Duo v2.0 | ✅ | - |
| Area51: Site 4 | ✅ | - |
| Bang! | ✅ | Ajout de crédit via la manette. Plante si ajout via wiimote. |
| Bank Panic | ❌ | Pas de viseur. |
| Beast Busters | ✅ | - |
| Beast Busters: Second Nightmare | ✅ | - |
| Born to Fight | ✅ | - |
| Bubble Trouble | ✅ | - |
| CarnEvil | ❌ | Ne démarre pas. |
| Catch-22 | ✅ | - |
| Cheyenne | ✅ | - |
| Chiller | ✅ | - |
| Clay Pigeon | ✅ | - |
| Claybuster | ✅ | - |
| Combat Hawk | ❔ | - |
| Combat School | ❔ | - |
| Crackshot (version 2.0) | ✅ | - |
| Critter Crusher | ✅ | - |
| Crossbow | ✅ | - |
| Crypt Killer | ✅ | - |
| Cycle Shooting | ❌ | Appuyer sur Start réinitialise le jeu. |
| Deer Hunting USA | ✅ | - |
| Desert Gun | ✅ | - |
| Dragon Gun | ✅ | - |
| Egg Venture | ✅ | - |
| Egg Venture Deluxe | ✅ | - |
| Freedom Force | ❔ | - |
| Ghost Hunter | ✅ | - |
| Golgo 13 (Japan, GLG1/VER.A) | ✅ | - |
| Golgo 13 Kiseki no Dandou | ✅ | - |
| Golly! Ghost! | ✅ | - |
| Great Guns | ✅ | - |
| Gun Bullet | ✅ | - |
| Gun Gabacho | ✅ | - |
| Gunbuster | ✅ | - |
| Hit'n Miss | ✅ | - |
| Hit'n Miss 2 | ✅ | - |
| Invasion - The Abductors | ✅ | - |
| Jurassic Park | ✅ | - |
| Jurassic Park III | ❌ | - |
| Laser Ghost | ❌ | Ne passe pas la fenêtre de calibrage. |
| Lethal Enforcers | ✅ | - |
| Lethal Enforcers II: Gun Fighters | ✅ | - |
| Lethal Justice | ✅ | - |
| Line of Fire / Bakudan Yarou | ❌ | Ne passe pas la fenêtre de calibrage. |
| Lord of Gun | ❌ | Pas de reload. |
| Mallet Madnes | ✅ | - |
| Maximum Force | ✅ | - |
| Mechanized Attack | ✅ | - |
| Mobile Suit Gundam Final Shooting | ✅ | - |
| Night Stocker | ✅ | - |
| N.Y. Captor | ✅ | - |
| Oh! Bakyuuun | ✅ | Ajout de crédit via la manette. Plante si ajout via wiimote. |
| One Shot One Kill | ✅ | - |
| Operation Thunderbolt | ✅ | - |
| Operation Wolf | ✅ | - |
| Operation Wolf 3 | ❌ | Ne démarre pas. |
| Point Blank | ✅ | - |
| Point Blank 2 | ✅ | - |
| Point Blank 3 | ✅ | - |
| Police Trainer | ✅ | - |
| Police Trainer 2 | ❌ | "No screens attached to the system". |
| Rail Chase | ✅ | - |
| Rapid Fire | ✅ | - |
| Revolution X | ❌ | N'utilise pas 100% de l'écran pour le viseur. |
| Sharpshooter | ✅ | - |
| Shooting Gallery | ✅ | - |
| Space Gun | ✅ | - |
| Steel Gunner | ✅ | - |
| Steel Gunner 2 | ✅ | - |
| Target Hits | ✅ | - |
| Terminator 2 - Judgment Day | ❌ | N'utilise pas 100% de l'écran pour le viseur. |
| Tickee Tickats | ✅ | - |
| Time Crisis | ✅ | - |
| Time Crisis II | ✅ | - |
| Time Crisis 3 | ❌ | Aucune réaction. |
| Time Crisis 4 | ❌ | Aucune réaction. |
| Trophy Hunting - Bear & Moose | ❔ | - |
| Tut's Tomb | ✅ | - |
| Under Fire | ✅ | - |
| Virtua Cop | ✅ | - |
| Virtua Cop 2 | ✅ | - |
| Virtua Cop 3 | ❌ | Ne démarre pas. |
| Vs. Duck Hunt | ✅ | - |
| Vs. Freedom Force | ✅ | Ajout de crédit via la manette. Plante si ajout via wiimote. |
| Vs. Hogan's Alley | ✅ | - |
| Who Dunit | ✅ | - |
| Wild Gunman | ❌ | - |
| Wild Pilot | ✅ | - |
| Wing Shooting Championship | ❔ | - |
| Zero Point | ✅ | - |
| Zero Point 2 | ✅ | - |
| Zombie Raid | ✅ | - |

## Mame 2003-Plus

### Emulateur

L'émulateur requis est le core `Libretro MAME 2003 Plus`.

### Compatibilité

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

### Jeux

| Nom du jeu | Compatibilité | Notes |
| :--- | :--- | :--- |
| Alien 3: The Gun | ✅ | Ajout de crédit via la manette. Plante si ajout via wiimote. |
| Area 51 | ✅ | Ajout de crédit via la manette. Plante si ajout via wiimote. |
| Area 51 / Maximum Force Duo | ✅ | Ajout de crédit via la manette. Plante si ajout via wiimote. |
| Bang! | ✅ | Ajout de crédit via la manette. Plante si ajout via wiimote. |
| Beast Busters | ✅ | - |
| Born To Fight | ✅ | - |
| Bubble Trouble - Golly Ghost 2 | ❔ | - |
| Cheyenne | ✅ | - |
| Chiller | ✅ | - |
| Clay Pigeon | ✅ | - |
| Crossbow | ✅ | - |
| Crypt Killer | ✅ | - |
| Deer Hunting USA | ✅ | Calibrage foireux. |
| Dragon Gun | ✅ | - |
| Egg Venture | ✅ | - |
| Egg Venture Venture | ✅ | - |
| Ghost Hunter | ❔ | - |
| Golgo 13 | ❌ | Reste bloqué après le warning. |
| Golgo 13 Kiseki no Dandou | ❔ | - |
| Golly! Ghost! | ✅ | - |
| Great Guns | ❌ | Injouable. |
| Gunbuster | ✅ | - |
| Hit'n Miss | ✅ | - |
| Hit'n Miss 2 | ✅ | - |
| Invasion - The Abductors | ❔ | - |
| Jurassic Park | ✅ | - |
| Lethal Enforcers | ✅ | - |
| Lethal Enforcers II: Gun Fighters | ✅ | - |
| Lethal Justice | ✅ | - |
| Mallet Madnes | ❔ | - |
| Maximum Force | ✅ | - |
| N.Y. Captor | ✅ | - |
| Oh! Bakyuuun | ❔ | - |
| One Shot One Kill | ✅ | - |
| Operation Wolf | ✅ | - |
| Operation Wolf 3 | ❌ | Pas de viseur ni de tir possible. |
| Point Blank | ✅ | - |
| Rail Chase | ❔ | - |
| Rapid Fire v1.1 | ❔ | - |
| Steel Gunner | ✅ | - |
| Steel Gunner 2 | ✅ | - |
| Target Hits | ❌ | Le viseur ne suit pas la wiimote et reste au centre. |
| Tickee Tickats | ✅ | - |
| Time Crisis | ❌ | Mauvaise initialisation. |
| Trophy Hunting - Bear & Moose v1.0 | ✅ | - |
| Tut's Tomb | ❔ | - |
| Under Fire | ✅ | - |
| Virtua Cop 2 | ❔ | - |
| Vs. Duck Hunt | ✅ | - |
| Vs. Freedom | ✅ | - |
| Vs. Hogan's Alley | ✅ | - |
| Who Dunit | ✅ | - |
| Wild Pilot | ❔ | - |
| Wing Shooting Championship v2.00 | ✅ | - |
| Zero Point | ✅ | - |
| Zero Point 2 | ✅ | - |
| Zombie Raid | ✅ | - |

## FinalBurn Neo

### Emulateur

L'émulateur requis est le core `Libretro FBNeo`.

### Compatibilité

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

### Jeux

| Nom du jeu | Compatibilité | Notes |
| :--- | :--- | :--- |
| Alien3: The Gun | ✅ | - |
| Area 51 (R-3000) | ❔ | - |
| Area 51 / Maximum Force Duo v2.0 | ❔ | - |
| Bang! | ❌ | L'ajout de crédit quitte le jeu. |
| Bank Panic | ❌ | L'ajout de crédit quitte le jeu. |
| Bubble Trouble - Golly! Ghost! 2 | ❌ | Le parent ne fonctionne pas. |
| Beast Busters | ❔ | - |
| Born to Fight | ❔ | - |
| Cheyenne | ✅ | - |
| Chiller | ✅ | - |
| Clay Pigeon | ✅ | - |
| Crossbow | ✅ | - |
| Crypt Killer | ❔ | - |
| Deer Hunting USA | ✅ | - |
| Dragon Gun | ✅ | - |
| Egg Venture | ❔ | - |
| Ghost Hunter | ❔ | - |
| Golgo 13 | ❔ | - |
| Golgo 13 Kiseki no Dandou | ❔ | - |
| Golly! Ghost! | ❌ | L'ajout de crédit quitte le jeu. |
| Great Guns | ❔ | - |
| Gunbuster | ❔ | - |
| Hit'n Miss 1 | ✅ | - |
| Hit'n Miss 2 | ✅ | - |
| Invasion - The Abductors | ❔ | - |
| Jurassic Park | ✅ | - |
| Lethal Enforcers II: Gun Fighters | ✅ | - |
| Lethal Justice | ❔ | - |
| Line of Fire / Bakudan Yarou | ❔ | - |
| Lord of Gun | ✅ | - |
| Mechanized Attack | ✅ | - |
| Mobile Suit Gundam Final Shooting | ❌ | Aucune visée par lightgun, uniquement le dpad de la wiimote. |
| Operation Thunderbolt | ✅ | - |
| Operation Wolf | ✅ | - |
| Operation Wolf 3 | ✅ | - |
| Point Blank | ✅ | - |
| Rail Chase | ✅ | - |
| Space Gun | ✅ | - |
| Steel Gunner | ✅ | - |
| Steel Gunner 2 | ✅ | - |
| Trophy Hunting - Bear & Moose v1.0 | ✅ | - |
| Turkey Hunting USA v1.0 | ✅ | - |
| Wing Shooting Championship v2.00 | ✅ | - |
| Zero Point | ✅ | - |
| Zero Point 2 | ✅ | - |
| Zombie Raid | ✅ | - |