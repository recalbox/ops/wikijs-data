---
title: Lectura de vídeos en 3D
description: 
published: true
date: 2023-09-15T08:06:55.988Z
tags: kodi, videos, 3d
editor: markdown
dateCreated: 2023-09-15T08:06:55.988Z
---

## Introducción

Entre otras muchas funciones, la versión 13 de Kodi permite reproducir vídeos 3D en televisores compatibles con 3D utilizando las gafas adecuadas suministradas por el fabricante del televisor.

Los vídeos 3D compatibles son los siguientes:

* Lado a lado (SBS)
* Arriba y abajo (TAB)

Las películas 3D anaglifo no tienen ninguna configuración especial y pueden reproducirse como cualquier otra película convencional.

Cada marca de televisor tiene un determinado nivel de compatibilidad en términos de reproducción 3D. Puede probar los contenidos que figuran a continuación utilizando los vídeos del sitio web https://www.demolandia.net/3d-video/brands.html.

## Utilización

### Cambiar el nombre de las películas

Para poder utilizar Kodi para ver películas en 3D, primero hay que saber si se trata de una película en 3D con las imágenes una al lado de la otra (SBS) o arriba y abajo (TAB). Dependiendo de esta característica, hay que renombrar la película como se muestra en el siguiente ejemplo:

```text
votrefilm.3d.sbs.mp4
```

La parte `3d.sbs` es importante, es esta parte que decide la forma de presentación de vuestra película en el modo 3D.

Aquí tenéis un cuadro con algunos ejemplos de las configuraciones posibles:

| 3D | Lado a lado | Arriba / Abajo |
| :---: | :---: | :---: |
| .3D. | .sbs. | .tab. |
| -3d- | .SBS. | .TAB. |
| 3d | .HSBS. | .HTAB. |
| \_3d_ | -hsbs- | -htab- |
| .3D- | \_sbs_ | \_tab_ |
| 3D. | SBS | TAB |
| .3D | sbs | tab |
|  | -SBS | -TAB |
|  | HSBS_ | HTAB_ |

Ejemplos de nombres de películas:

* Mon film 3D.SBS.mp4
* Mon filmd 3d.sbs.mp4
* Mon film 3D-TAB.mp4

### Reproducción de películas

Una vez renombrada vuestra película, buscadla en Kodi en la sección 'Películas'. Una vez encontrada, lanzadla y os aparecerá un menú preguntándoos qué tipo de vídeo 3D queréis aplicar:

* Modo preferido (Como la película)
* Monoscópico / 2D

Dependiendo de cómo pongáis el nombre de vuestro fichero y de la selección anterior, vuestra película se verá en 3D con el par de gafas adecuado (las gafas no duplican las imágenes).