---
title: PiFBA
description: 
published: true
date: 2021-08-09T07:29:58.395Z
tags: finalburn, neo, pifba
editor: markdown
dateCreated: 2021-07-05T21:59:28.101Z
---

**PiFBA** is Final Burn Alpha 2x for the Raspberry Pi ported by Squid.  
It emulates old arcade games using MAME based roms for CPS1, CPS2, Neogeo, Toaplan and many other games.

It should support the most recent versions of the MAME versions of these ROMs, but unfortunately there is no definitive version to use. A full list of supported games can be found in the front end program.

A number of games will not work properly, some are so large that they seem to run out of memory.

>Use only if you have a Pi0/1 or if you want to improve the performance of a particular game running on FBNeo.
>piFBA is the best optimized FBA emulator on the Recalbox for the Pi0/1 but is compatible with far fewer games than FBNeo.
{.is-warning}

## ![](/emulators/license.svg) License

This core is under **GPLv2** license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ❌ | ❌ | ❌ | ❌ | ❌ |

## ![](/emulators/features.png) Features

| Feature | Supported |
| :---: | :---: |
| Saves | ✔ |
| Netplay | ✔ |
| RetroAchievements | ✔ |

## ![](/emulators/bios.svg) Bios

### Bios list

Depending on the games, some bios will be necessary and will be placed in the same directory as the games.

### Location

Put your bios like this:

┣ 📁 recalbox  
┃ ┣ 📁 share  
┃ ┃ ┣ 📁 roms  
┃ ┃ ┃ ┣ 📁 fba  
┃ ┃ ┃ ┃ ┣ 🗒 bios.zip  

## ![](/emulators/roms.png) Roms

* Based on the MAME romset: **FBA 0.2.96.71** (April 2007) itself based on the MAME 0.114 set
* Weight: 3.62GB
* Emulated romsets: 684 (No clone in this set)
* Compatibility list: [/recalbox/share/roms/fba/clrmamepro/piFBA_gamelist.txt](https://gitlab.com/recalbox/recalbox/-/raw/master/board/recalbox/fsoverlay/recalbox/share_init/bios/fba/piFBA_gamelist.txt)
* Dat file : [/recalbox/share/roms/fba/clrmamepro/fba_029671_od_release_10_working_roms.dat](https://gitlab.com/recalbox/recalbox/-/raw/master/board/recalbox/fsoverlay/recalbox/share_init/bios/fba/fba_029671_od_release_10_working_roms.dat)

Some important roms may need to be converted to '.fba' format before they work.
FBACache_windows.zip is included for this purpose, which works under Windows only.

A clrmamepro / romcenter DAT file is included 'fba_029671_clrmame_dat.zip' which supports most working ROMs and can generate compatible ROMs from recent MAME versions.

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/recalbox/pifba/](https://github.com/recalbox/pifba)
* **Original source code**: [https://github.com/squidrpi/pifba/](https://github.com/squidrpi/pifba)