---
title: Compatibilidad de roms con los cores
description: La compatibilidad y la satisfacción de los usuarios están en juego, así que... que la suerte os acompañe.
published: true
date: 2023-09-20T20:39:54.530Z
tags: core n64, core, compatibilidad, rom n64, compatibilidad n64
editor: markdown
dateCreated: 2023-09-20T20:39:54.530Z
---

#### Versión 7.2.2 Reloaded

Si vuestro juego no se encuentra en el siguiente cuadro, significa que aún no ha sido probado.
Por favor, tened paciencia, estamos trabajando en ello. También podéis uniros al equipo de pruebas si queréis participar.
Contactad con m.r.b.i en el Discord de Recalbox para más información :wink: 

## :caption

 :star2: Alto rendimiento. Hasta el infinito y más allá.
 :star: Rendimiento medio. Buen equilibrio, pero más lento con bugs mínimos.
 :ira: Bajo rendimiento. Lento y con errores. No recomendado.
 :boom: No funciona. Crash, Lags, Freeze, vuelve a EmulationStation, bugs. No recomendado**_.

## CORE

### Raspberry Pi3 b 

| Juego  | Extensión | Gliden 64 | GLES2 | Rice_GLES2 | Rice | Parallel 64 | Plus_Next |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | 
| **F Zéro X**  | .n64 | :star: | :star: | :anger: | :anger: | :anger:  | :boom:
| **Zelda Majora Mask**  | .n64  | :boom: | :boom: | :star: | :boom: | :anger: | :boom:
| **Paper Mario**  | .n64 | :star2: |:boom:  | :anger: | :boom: |  |  |
| **The New Tetris**  | .z64 | :star: |  |  |  |  |  |  
| **Paperboy** | .z64 | :boom: | :boom: | :boom: | :boom: | :boom: | :boom: |  
| **Mario Tennis** | .z64 |  |  |  |  |  |  |  
| **Pokemon Snap** | .z64 | :boom: | :boom: | :boom: | :boom: | :boom: |:boom:
| **1080** | .z64 | :star2: | :boom: | :star: | :anger: |:anger:  | :boom: | 
| **Aidyn** | .z64 |  :star: | :anger: | :anger: |  |  |  | 
| **Diddy Kong Racing** | .n64 | :boom: | :star: | :star: | :anger: | :boom: | :anger: | 
| **Super Mario 64** | .n64 | :star2: | :star: | :star2: | :star: | :star2: | :anger: |
| **Micro Machines 64 Turbo** | .z64 | :star2: | :anger: | :star: | :anger: |  |  |
| **Wipeout 64** | .z64 | :star2: | :boom: | :star: | :anger: |  |  |
| **Wipeout 64** | .n64 | :star2: | :boom: | :star: | :anger: | :star2: | :boom: |
| **Dr.Mario 64** | .z64 | :anger: | :boom: | :anger: | :anger: | | |
| **Wave Race** | .n64 | :star: | :anger: | :boom: | :boom: | :anger: | :boom: |
| **Star Wars Episode I Racer** | .n64 | :star: | :boom: | :anger: | :boom: | :boom: |:boom: |
| **Star Wars Rogue Squadron** | .n64 | :boom: | :boom: | :boom: | :boom: | :boom: |:boom: |
| **Star Wars Shadows of the Empire** | .n64 | :boom: | :boom: | :anger: | :anger: | :boom: | :boom: |
| **Star Wars Episode I Battle for Naboo** | .n64 | :boom: | :boom: | :boom: | :boom: | :boom: | :boom: |
| **Pokémon Stadium** | .n64 | :boom: | :boom: | :anger: | :anger: | :anger: | :boom: |
| **Pokémon Stadium 2** | .n64 | :boom: | :boom: | :anger: | :anger: | :star: | :boom: |
| **Mario Kart 64** | .n64 | :star2: | :star2: | :star2: | :anger: | :star2: | :boom: |
| **Lylat Wars** | .n64 | :star2: | :star: | :star: | :star: | :star: | :boom: |
| **Banjo-Kazooie** | .z64 | :star2: | :boom: | :star2: | :star: |  |  |
| **Banjo-Tooie** | .z64 | :boom: | :boom: | :star: | :anger: |  |  |
| **Kirby 64: The Crystal Shards** | .z64 | :star2: | :anger: | :star: | :anger: |  |  |

### Raspberry Pi 4

| Juego | Extensión | GlideMK2 | RICE | Plus_Next | ParaLLEl_N64 |
| :---: | :---: | :---: | :---: | :---: | :---: |
|  |  |  |  |  |  |
