---
title: Sobrecargas RetroArch
description: Ejemplos de claves para sobrecargar la configuración RetroArch
published: true
date: 2023-09-01T08:45:39.696Z
tags: retroarch, sobrecarga
editor: markdown
dateCreated: 2023-09-01T08:45:39.696Z
---

A continuación se presenta una lista no-exhaustiva de claves RetroArch que pueden utilizarse en los ficheros de sobrecarga de tipo `.retroarch.cfg`. Evidentemente, estas configuraciones sólo se aplican a los emuladores que utilizan un core RetroArch.

>Un \* después de un valor significa que el parámetro puede sobrecargarse igualmente desde un fichero .recalbox.conf
{.is-info}

## Audio

* `audio_enable = "true"` : activa o desactiva el sonido
* `audio_volume = "0.000000"` : ganancia del volumen, 0 = volumen por defecto.

## Menu RetroArch

* `quick_menu_show_save_content_dir_overrides = "false"` : muestra o esconde la opción "Reemplazamiento de la configuración para el directorio".
* `quick_menu_show_save_core_overrides = "false"` : muestra o esconde la opción "Reemplazamiento de la configuración para el core".
* `quick_menu_show_save_game_overrides = "false"` : muestra o esconde la opción "Reemplazamiento de la configuración para el juego".

>Los Reemplazamiento de configuración son una funcionalidad de RetroArch, similar a las sobrecargas. Se aconseja no utilizar esta funcionalidad y utilizar las sobrecargas del sistema de Recalbox en su lugar, siempre y cuando sea posible.
{.is-info}

## Debug

* `fps_show = "true"` : mostrar los FPS durante el juego.
* `menu_driver = "ozone"` : sección del menu retroarch, de base ozone, salvo en GPi Case, donde se utiliza rgui.
* `menu_enable_widgets = "true"` : activa los popups durante el juego, si se pone a false, mostrará las notificaciones como un texto amarillo.

## Directorios

* `recording_output_directory = ""` : directorio para las grabaciones de vídeos.
* `savefile_directory = ""` : directorio para las grabaciones de los juegos.
* `savestate_directory = ""` : directorio para las grabaciones instantáneas de RA.
* `screenshot_directory = ""` : directorio para las capturas de pantalla.

## Servicio de traducción

* `ai_service_enable = "true"` : \* activa o desactiva el servicio de traducción.
* `ai_service_mode = "0"` : _modo del servicio de traducción, 0: imagen, 1: voz.
* `ai_service_source_lang = "0"` : \* idioma origen, 0 = sin especificar.
* `ai_service_target_lang = "1"` : \* idioma destino, 1= Inglés, 3= Francés.
* `ai_service_url =` : \* URL del servicio de traducción a utilizar.

## Overlays

* `aspect_ratio_index = "23"` : \* índice de ratio, 23 = Custom.
* `input_overlay = ""` : enlace al fichero de configuración del overlay.
* `input_overlay_enable = "true"` :  activación del overlay.
* `input_overlay_hide_in_menu = "true"` : overlay escondido en el menú RetroArch.

### Coordenadas a definir para los overlays

* `custom_viewport_height = ""`
* `custom_viewport_width = ""`
* `custom_viewport_x = ""`
* `custom_viewport_y = ""`
* `video_message_pos_x = "0.050000"`
* `video_message_pos_y = "0.050000"`

## Netplay

* `netplay_nickname = ""` : \* pseudo Netplay.

## Rotación de la pantalla

* `video_rotation = "1"` : pivota el vídeo, 0= normal, 1=90°, 2=180°, 3=270°, cuidado con el aspect-ratio.

## Joystick y Pad direccional

Para asociar/disociar el Pad direccional a uno de los joystics:

* `input_player1_analog_dpad_mode = "0"` : disocia.
* `input_player1_analog_dpad_mode = "1"` : asocia al joystick izquierdo.
* `input_player1_analog_dpad_mode = "2"` : asocia al joystick derecho.
* `input_player1_analog_dpad_mode = "3"` : fuerza la asociación al joystick izquierdo
* `input_player1_analog_dpad_mode = "4"` : fuerza la asociación al joystick derecho

## Remapping de hotkeys

>Las configuraciones para cambiar las hotkeys de RetroArch dependen del mapping del mando realizado en Recalbox. Si el mando cambia, puede ser que las configuraciones realizadas en este apartado dejen de funcionar.
{.is-warning}

Para obtener el valor numérico de cada tecla de vuestro mando de juegos, debéis mirar dentro del fichero `/recalbox/share/system/configs/retroarch/retroarchcustom.cfg` y obtener el valor de la clave que buscáis siguiendo esta tabla:

| Nombre de la tecla | Clave correspondiente de donde hay que coger el valor |
| :--- | :--- |
| A | input_player1_a_btn |
| B | input_player1_b_btn |
| X | input_player1_x_btn |
| Y | input_player1_y_btn |
| Arriba | input_player1_up_btn |
| Abajo | input_player1_down_btn |
| Izquierda | input_player1_left_btn |
| Derecha | input_player1_right_btn |
| Select | input_player1_select_btn |
| Start | input_player1_start_btn |
| L | input_player1_l_btn |
| L2 | input_player1_l2_btn |
| L3 | input_player1_l3_btn |
| R | input_player1_r_btn |
| R2 | input_player1_r2_btn |
| R3 | input_player1_r3_btn |
| Stick Izquierdo Arriba | input_player1_l_y_minus_axis |
| Stick Izquierdo Abajo | input_player1_l_y_plus_axis |
| Stick Izquierdo Izquierda | input_player1_l_x_minus_axis |
| Stick Izquierdo Derecha | input_player1_l_x_plus_axis |
| Stick Derecho Arriba | input_player1_r_y_minus_axis |
| Stick Derecho Abajo | input_player1_r_y_plus_axis |
| Stick Derecho Izquierda | input_player1_r_x_minus_axis |
| Stick Derecho Derecha | input_player1_r_x_plus_axis |

>Las modificaciones que siguen se deben realizar en el fichero de sobrecargas.
>
>El fichero anterior sólo siver para observar las correspondencias.
{.is-warning}

* `input_enable_hotkey_btn =` :  Tecla Hotkey.
* `input_screenshot_btn =` : Tecla para realizar una captura de pantalla.
* `input_exit_emulator_btn =` : Tecla para salir del juego.
* `input_load_state_btn =` : Tecla para cargar un savestate.
* `input_save_state_btn =` : Tecla para realizar un savestate (grabación instantánea).
* `input_menu_toggle_btn =` : Tecla para acceder al menú RetroArch.
* `input_reset_btn =` : Tecla para reiniciar el juego.
* `input_ai_service_btn =` : Tecla para traducir la pantalla actual.

Por ejemplo, `The legend of Zelda: Link's Awakening` en Game Boy necesita utilizar la combinación `Start+Select+A+B` para grabar. Si vuestro mando no tiene botón home y que la hotkey se encuentra en `Select`, no se puede realizar una grabación natural del juego. Sin embargo, grcias a los ficheros de sobrecarga RetroArch, se puede configurar la tecla  `input_enable_hotkey_btn` en la tecla R de vuestro mando, únicamente para este juego. Si el valor de la clave `input_player1_r_btn` es 4 para la tecla R, entonces hay que añadir  `input_enable_hotkey_btn = 4` a vuestro fichero de sobrecarga para el juego.
