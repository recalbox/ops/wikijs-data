---
title: 4. Los objetos
description: 
published: true
date: 2024-04-07T14:41:47.963Z
tags: 9.2+, temas
editor: markdown
dateCreated: 2024-04-07T13:28:05.015Z
---

## Introducción

El contenido de los archivos de su tema debe seguir un guión determinado. Cada objeto debe colocarse en las etiquetas `<view></view>`.

## Etiqueta `<view>`

Las pantallas de la interfaz se gestionan a través de lo que se conoce como vistas. Estas vistas pueden tener varios atributos.

#### Atributo `name`

- Obligatorio: yes

El atributo `name` se utiliza para indicar en qué pantalla debe actuar su contenido. Puede tener al mismo tiempo uno de estos valores

- `system`: corresponde a la lista de sistemas.
- `detailed`: corresponde a la lista de juegos.
- `menu`: corresponde a los menús de opciones.
- `gameclip`: corresponde al salvapantallas "game clips".

Este atributo puede tener uno o varios valores al mismo tiempo:

```xml
<view name="system">
  ...
</view>
```

```xml
<view name="system, detailed">
  ...
</view>
```

Utilizar varios valores significa que utilizas los mismos datos en la etiqueta `<view>` en diferentes lugares (en el segundo ejemplo anterior, en la vista del sistema y en la vista de la lista de juegos).

## Objeto `<image>`

Los objetos `<image>` se utilizan para mostrar una imagen en los lugares deseados. Este objeto puede tener varios atributos:

#### Atributo `name`

- Obligatorio: yes

El atributo `name` se utiliza para indicar sobre qué elemento visual debe actuar el contenido. Puede tener uno o varios de estos valores al mismo tiempo.

Lista de valores que pueden utilizarse en la lista de sistemas (`<view name="system">`):

- `logo`: la imagen del logotipo del sistema.

Lista de valores que se pueden utilizar en la lista de juegos (`<view name="detailed">`):

- `console`: la imagen que muestra la consola del sistema.
- `logo`: la imagen del logotipo del sistema.
- `md_folder_image_0`: en una carpeta, la primera miniatura.
- `md_folder_image_1`: en una carpeta, la segunda miniatura.
- `md_folder_image_2`: en una carpeta, la tercera miniatura.
- `md_folder_image_3`: en una carpeta, cuarta miniatura.
- `md_folder_image_4`: en una carpeta, quinta miniatura.
- `md_folder_image_5`: en una carpeta, sexta miniatura.
- `md_folder_image_6`: en una carpeta, séptima miniatura.
- `md_folder_image_7`: en una carpeta, octava miniatura.
- `md_folder_image_8`: en una carpeta, la novena miniatura.
- `md_image`: ubicación de la imagen en miniatura de la chatarra.
- `md_region1`: primera región del juego seleccionado.
- `md_region2`: segunda región del juego seleccionado.
- `md_region3`: tercera región del juego seleccionado.
- `md_region4`: cuarta región del juego seleccionado.

Lista de valores posibles dentro de los clips de juegos (`<view name="gameclip">`) :

- `favoriteIcon` : icono de favoritos.
- `logo` : La imagen del logo del sistema.
- `md_image` : emplazamiento de la imagen de la miniatura del scrap.
- `md_thumbnail` : miniatura del scrap.
- `recalboxLogo` : logo de Recalbox.

```xml
<image name="logo">
  ...
</image>
```

#### Atributo `extra`

Podéis crear vuestro propio valor para el atributo `name` fuera de la lista anterior, pero tendréis que añadir el atributo `extra="true"` para que se tenga en cuenta.

```xml
<image name="background" extra="true">
  ...
</image>
```

#### Atributo `region`

Si especificáis una imagen para una región en concreto, podéis utilizar el atributo `region`.

```xml
<image name="logo" region="us">
  ...
</image>
```

El objeto `<image>` posee varias propiedades:

#### Propiedad `<pos>`

La propiedad `<pos>` indica la posición del objeto padre. La posición a indicar corresponde al punto más alto y más a la izquierda del objeto. Esta propiedad posee dos valores, el primero corresponde al posicionamiento desde la izquierda y el segundo al posicionamiento desde arriba.
Los varlores son porcentajes comprendidos entre 0 (0%) y 1 (100%).

```xml
<pos>0.33 0.25</pos>
```

En el ejemplo anterior, la posición indica que el objeto padre debe situarse a un 33% de la izquierda y a un 25% de la parte superior.

#### Propiedad `<size>`

La propiedad `<size>` define la talla del elemento que contiene el objeto padre.

```xml
<size>1 1</size>
```

#### Propiedad `<maxSize>`

>Esta propiedad está obsoleta, y se mantiene por compatibilidad con temas existentes. Será eliminada en una futura versión de Recalbox.
{.is-warning}

La propiedad `<maxSize>` evita que el elemento del objeto padre sea sobredimensionado en un eje. Por defecto, la imagen se redimensionará tanto como sea posible para que se adapte a este tamaño y conserve su relación de aspecto.
Los valores son porcentajes entre 0 (0%) y 1 (100%).

```xml
<maxSize>0.35 0.1</maxSize>
```

#### Propiedad `<keepratio>` ![](/tutorials/newicon-24.png)

La propiedad `<keepratio>` conserva el ratio del elemento padre. Se utiliza junto con `<size>`.

```xml
<keepratio>true</keepratio>
```

#### Propiedad `<origin>`

La propiedad `<origin>` define un nuevo punto de partida de origen para todas las tallas del objeto padre.
Los valores son porcentajes entre 0 (0%) y 1 (100%).

```xml
<origin>0.5 0.5</origin>
```

#### Propiedad `<rotation>`

La propiedad `<rotation>` se utiliza para especificar un ángulo de rotación para el objeto padre. Los valores positivos rotarán en el sentido de las agujas del reloj y los negativos en sentido contrario.

```xml
<rotation>90</rotation>
```

#### Propiedad `<rotationOrigin>`

La propiedad `<rotationOrigin>` se utiliza para definir un nuevo punto de partida de origen para todos los tamaños del objeto padre sólo si se va a realizar una rotación. 
Los valores son porcentajes entre 0 (0%) y 1 (100%).

```xml
<rotationOrigin>0.5 0.5</rotationOrigin>
```

#### Propiedad `<path>`

La propiedad `<path>` se utiliza para cargar un archivo de texto en lugar de introducir el texto manualmente.

```xml
<path>./camino/hacia/fichero/texto.txt</path>
```

#### Propiedad `<tile>`

La propiedad `<tile>` se utiliza para definir si la imagen se muestra como un mosaico en lugar de ser estirada para ajustarse al tamaño del objeto. Esto es útil para los fondos.

```xml
<tile>false</tile>
```

#### Propiedad `<color>`

La propiedad `<color>` se utiliza para especificar el color del texto del objeto padre. Los colores pueden estar en formato RRVVBB o RRVVBBAA (AA para transparencia, 00 = transparente y ff = opaco).

```xml
<color>666666</color>
```

#### Propiedad `<colorTop>` ![](/tutorials/newicon-24.png)

La propiedad `<colorTop>` se utiliza para indicar el color de la parte superior del objeto padre. Los colores pueden estar en formato RRVVBB o RRVVBBAA (AA para transparencia, 00 = transparente y ff = opaco).

```xml
<colorTop>666666</colorTop>
```

#### Propiedad `<colorBottom>` ![](/tutorials/newicon-24.png)

La propiedad `<colorBottom>` se utiliza para indicar el color del fondo del objeto padre. Los colores pueden estar en formato RRVVBB o RRVVBBAA (AA para transparencia, 00 = transparente y ff = opaco).

```xml
<colorBottom>666666</colorBottom>
```

#### Propiedad `<colorLeft>` ![](/tutorials/newicon-24.png)

La propiedad `<colorLeft>` se utiliza para indicar el color izquierdo del objeto padre. Los colores pueden estar en formato RRVVBB o RRVVBBAA (AA para transparencia, 00 = transparente y ff = opaco).

```xml
<colorLeft>666666</colorLeft>
```

#### Propiedad `<colorRight>` ![](/tutorials/newicon-24.png)

La propiedad `<colorRight>` se utiliza para indicar el color derecho del objeto padre. Los colores pueden estar en formato RRVVBB o RRVVBBAA (AA para transparencia, 00 = transparente y ff = opaco).

```xml
<colorRight>666666</colorRight>
```

#### Propiedad `<colorTopLeft>` ![](/tutorials/newicon-24.png)

La propiedad `<colorTopLeft>` se utiliza para indicar el color en la parte superior izquierda del objeto padre. Los colores pueden estar en formato RRVVBB o RRVVBBAA (AA para transparencia, 00 = transparente y ff = opaco).

```xml
<colorTopLeft>666666</colorTopLeft>
```

#### Propiedad `<colorTopRight>` ![](/tutorials/newicon-24.png)

La propiedad `<colorTopRight>` se utiliza para indicar el color de la parte superior derecha del objeto padre. Los colores pueden estar en formato RRVVBB o RRVVBBAA (AA para transparencia, 00 = transparente y ff = opaco).

```xml
<colorTopRight>666666</colorTopRight>
```

#### Propiedad `<colorBottomLeft>` ![](/tutorials/newicon-24.png)

La propiedad `<colorBottomLeft>` se utiliza para indicar el color de la parte inferior izquierda del objeto padre. Los colores pueden estar en formato RRVVBB o RRVVBBAA (AA para transparencia, 00 = transparente y ff = opaco).

```xml
<colorBottomLeft>666666</colorBottomLeft>
```

#### Propiedad `<colorBottomRight>` ![](/tutorials/newicon-24.png)

La propiedad `<colorBottomRight>` se utiliza para indicar el color de la parte inferior derecha del objeto padre. Los colores pueden estar en formato RRVVBB o RRVVBBAA (AA para transparencia, 00 = transparente y ff = opaco).

```xml
<colorBottomRight>666666</colorBottomRight>
```

#### Propiedad `<zIndex>`

La propiedad `<zIndex>` se utiliza para ordenar los elementos por profundidad / capa. Cuanto mayor sea su valor, más estará en primer plano. Puede ser negativo.

```xml
<zIndex>10</zIndex>
```

#### Propiedad `<disabled>`

La propiedad `<disabled>` desactiva el objeto padre.

```xml
<disabled>true</disabled>
```

#### Propiedad `<reflection>` ![](/tutorials/newicon-24.png)

Propiedad `<reflection>` se utiliza para crear efectos de reflexión por debajo del objeto padre.

El primer valor corresponde a la parte superior del reflejo, el segundo valor corresponde a la parte inferior del reflejo.

Los valores pueden oscilar entre 0.0 (transparente) y 1.0 (opaco).

```xml
<reflection>0.5 0.5</reflection>
```

## Objeto `<box>` ![](/tutorials/newicon.png)

Los objetos `<box>` se pueden utilizar para crear zonas de color decorativas en los lugares deseados. Este objeto puede tener varios atributos.

#### Atributo `name`

- Obligatorio: si

El Atributo `name` se utiliza para indicar sobre qué elemento de texto debe actuar el contenido. Puede tener uno o más de estos valores al mismo tiempo.

No hay ninguna lista que proporcionar; este contenido es específico para usted.

El objeto `<box>` puede contener varios otras propiedades.

#### Propiedad `<pos>`

La propiedad `<pos>` se utiliza para indicar la posición del objeto padre. La posición a indicar corresponde al punto más alto a la izquierda del objeto. Contiene 2 valores. El primer valor corresponde al posicionamiento desde la izquierda y el segundo valor al posicionamiento desde arriba.

Los valores son porcentajes entre 0 (0%) y 1 (100%).

```xml
<pos>0.33 0.25</pos>
```

En el ejemplo anterior, la posición indica que el objeto padre debe situarse a un 33% de la izquierda y a un 25% de la parte superior.

#### Propiedad `<size>`

La propiedad `<size>` indica la talla del elemento que contiene el objeto padre.

```xml
<size>1 1</size>
```

#### Propiedad `<origin>`

La propiedad `<origin>`se utiliza para definir un nuevo punto de partida de origen para todos los tamaños del objeto padre.

Los valores son porcentajes entre 0 (0%) y 1 (100%).

```xml
<origin>0.5 0.5</origin>
```

#### Propiedad `<rotation>`

La propiedad `<rotación>` se utiliza para especificar un ángulo de rotación para el objeto padre. Los valores positivos rotarán en el sentido de las agujas del reloj y los negativos en sentido contrario.

```xml
<rotation>90</rotation>
```

#### Propiedad `<rotationOrigin>`

La propiedad `<rotationOrigin>` permite definir un nuevo punto de partida del origen para todos los tamaños del objeto padre sólo si se va a realizar una rotación. 

Los valores son porcentajes entre 0 (0%) y 1 (100%).

```xml
<rotationOrigin>0.5 0.5</rotationOrigin>
```

#### Propiedad `<color>`

La propiedad `<color>` se utiliza para indicar el color del texto del objeto padre. Los colores pueden estar en formato RRVVBB o RRVVBBAA (AA para transparencia, 00 = transparente y ff = opaco).

```xml
<color>666666</color>
```

#### Propiedad `<colorTop>`

La propiedad `<colorTop>` se utiliza para indicar el color superior del objeto padre. Los colores pueden estar en formato RRVVBB o RRVVBBAA (AA para transparencia, 00 = transparente y ff = opaco).

```xml
<colorTop>666666</colorTop>
```

#### Propiedad `<colorBottom>`

La propiedad `<colorBottom>`se utiliza para indicar el color del fondo del objeto padre. Los colores pueden estar en formato RRVVBB o RRVVBBAA (AA para transparencia, 00 = transparente y ff = opaco).

```xml
<colorBottom>666666</colorBottom>
```

#### Propiedad `<colorLeft>`

La propiedad `<colorLeft>`se utiliza para indicar el color izquierdo del objeto padre. Los colores pueden estar en formato RRVVBB o RRVVBBAA (AA para transparencia, 00 = transparente y ff = opaco).

```xml
<colorLeft>666666</colorLeft>
```

#### Propiedad `<colorRight>`

La propiedad `<colorRight>` se utiliza para indicar el color derecho del objeto padre. Los colores pueden estar en formato RRVVBB o RRVVBBAA (AA para transparencia, 00 = transparente y ff = opaco).

```xml
<colorRight>666666</colorRight>
```

#### Propiedad `<colorTopLeft>`

La propiedad `<colorTopLeft>` se utiliza para indicar el color en la parte superior izquierda del objeto padre. Los colores pueden estar en formato RRVVBB o RRVVBBAA (AA para transparencia, 00 = transparente y ff = opaco).

```xml
<colorTopLeft>666666</colorTopLeft>
```

#### Propiedad `<colorTopRight>`

La propiedad `<colorTopRight>` se utiliza para indicar el color de la parte superior derecha del objeto padre. Los colores pueden estar en formato RRVVBB o RRVVBBAA (AA para transparencia, 00 = transparente y ff = opaco).

```xml
<colorTopRight>666666</colorTopRight>
```

#### Propiedad `<colorBottomLeft>`

La propiedad `<colorBottomLeft>` se utiliza para indicar el color de la parte inferior izquierda del objeto padre. Los colores pueden estar en formato RRVVBB o RRVVBBAA (AA para transparencia, 00 = transparente y ff = opaco).

```xml
<colorBottomLeft>666666</colorBottomLeft>
```

#### Propiedad `<colorBottomRight>`

La propiedad `<colorBottomRight>` se utiliza para indicar el color en la parte inferior derecha del objeto padre. Los colores pueden estar en formato RRVVBB o RRVVBBAA (AA para transparencia, 00 = transparente y ff = opaco).

```xml
<colorBottomRight>666666</colorBottomRight>
```

#### Propiedad `<zIndex>`

La propiedad `<colorInferiorDerecho>` se utiliza para indicar el color en la parte inferior derecha del objeto padre. Los colores pueden estar en formato RRVVBB o RRVVBBAA (AA para transparencia, 00 = transparente y ff = opaco).

```xml
<zIndex>10</zIndex>
```

#### Propiedad `<disabled>`

La propiedad `<disabled>` desactiva el objeto padre.

```xml
<disabled>true</disabled>
```

## Objeto `<video>`

Los objetos `<video>` se utilizan para mostrar vídeo en las ubicaciones deseadas. Este objeto puede tener varios Atributos.

#### Atributo `name`

- Obligatorio: si

El Atributo `name` se utiliza para indicar sobre qué elemento de texto debe actuar el contenido. Puede tener uno o más de estos valores al mismo tiempo.

Lista de valores que se pueden utilizar en la lista de juegos (`<view name="detailed">`):

- `md_video`: el vídeo.

Lista de valores que se pueden utilizar en los clips de juego (`<view name="gameclip">`):

- `md_video`: el vídeo.

El objeto `<video>` puede contener otras propiedades.

#### Propiedad `<pos>`

La propiedad `<pos>` se utiliza para indicar la posición del objeto padre. La posición a indicar corresponde al punto más alto a la izquierda del objeto. Contiene 2 valores. El primer valor corresponde al posicionamiento desde la izquierda y el segundo valor al posicionamiento desde arriba.

Los valores son porcentajes entre 0 (0%) y 1 (100%).

```xml
<pos>0.33 0.25</pos>
```

En el ejemplo anterior, la posición indica que el objeto padre debe situarse a un 33% de la izquierda y a un 25% de la parte superior.

#### Propiedad `<size>`

La propiedad `<size>` se utiliza para definir el tamaño del elemento que contiene el objeto padre.

```xml
<size>1 1</size>
```

#### Propiedad `<maxSize>`

>Esta propiedad está obsoleta, y se mantiene por compatibilidad con los temas existentes. Será eliminada en una futura versión de Recalbox.
{.is-warning}

La propiedad `<maxSize>` previene que el elemento del objeto padre sea sobredimensionado en un eje. Por defecto, la imagen se redimensionará tanto como sea posible para que se adapte a este tamaño y conserve su relación de aspecto.

Los valores son porcentajes entre 0 (0%) y 1 (100%).

```xml
<maxSize>0.35 0.1</maxSize>
```

#### Propiedad `<keepratio>` ![](/tutorials/newicon-24.png)

La propiedad `<keepratio>` preserva la proporción del elemento padre. Se utiliza con `<size>`.

```xml
<keepratio>true</keepratio>
```

#### Propiedad `<origin>`

La propiedad `<origen>` se utiliza para definir un nuevo punto de partida de origen para todos los tamaños del objeto padre.

Los valores son porcentajes entre 0 (0%) y 1 (100%).

```xml
<origin>0.5 0.5</origin>
```

#### Propiedad `<rotation>`

La propiedad `<rotation>` se utiliza para especificar un ángulo de rotación para el objeto padre. Los valores positivos rotarán en el sentido de las agujas del reloj y los negativos en sentido contrario.

```xml
<rotation>90</rotation>
```

#### Propiedad `<rotationOrigin>`

La propiedad `<rotationOrigin>` permite definir un nuevo punto de partida del origen para todos los tamaños del objeto padre sólo si se va a realizar una rotación. 

Los valores son porcentajes entre 0 (0%) y 1 (100%).

```xml
<rotationOrigin>0.5 0.5</rotationOrigin>
```

#### Propiedad `<path>`

La propiedad `<path>` le permite cargar un archivo de imagen en lugar de establecer una imagen manualmente.

```xml
<path>./camino/hacia/fichero/imagen.png</path>
```

#### Propiedad `<zIndex>`

La propiedad `<zIndex>` se utiliza para ordenar los elementos por profundidad / capa. Cuanto mayor sea su valor, más en primer plano estará. Puede ser negativo.

```xml
<zIndex>10</zIndex>
```

#### Propiedad `<disabled>`

La propiedad `<disabled>` desactiva el objeto padre.

```xml
<disabled>true</disabled>
```

#### Propiedad `<animations>` ![](/tutorials/newicon-24.png)

La propiedad `<animations>` se utiliza para especificar la animación que se aplicará al objeto padre.

Lista de valores posibles:

- `bump`: aparición repentina procedente del fondo, el contenido salta un poco hacia delante antes de retomar su tamaño inicial.
- `breakingnews`: aparición arremolinada.
- `fade`: aparición gradual.
- `none`: ninguna animación.

Si utiliza varias animaciones diferentes, sólo se utilizará la última.

```xml
<animations>bump,breakingnews,fade</animations>
```

En el ejemplo anterior, sólo se aplicará `fade`.

#### Propiedad `<loops>` ![](/tutorials/newicon-24.png)

La propiedad `<loops>` se utiliza para indicar el número de veces que se lee el contenido del objeto padre.

```xml
<loops>true</loops>
```

#### Propiedad `<delay>` ![](/tutorials/newicon-24.png)

La propiedad `<delay>` se utiliza para indicar el retardo antes de leer el contenido del objeto padre.

```xml
<delay>2</delay>
```

#### Propiedad `<reflection>` ![](/tutorials/newicon-24.png)

La propiedad `<reflection>` se utiliza para crear efectos de reflexión debajo del objeto padre.

El primer valor corresponde a la parte superior del reflejo, el segundo valor corresponde a la parte inferior del reflejo.

Los valores pueden oscilar entre 0.0 (transparente) y 1.0 (opaco).

```xml
<reflection>0.5 0.5</reflection>
```

#### Propiedad `<link>` ![](/tutorials/newicon-24.png)

Hasta Recalbox 9.2, los objetos `<image name="md_image">` y `<video name="md_video>` estaban vinculados. Ahora no lo están y pueden tener diferentes propiedades.

La propiedad `<link>` permite enlazar estos 2 objetos como antes.

```xml
<link>md_image</link>
```

## Objeto `<text>`

Los objetos `<text>` se utilizan para mostrar texto en los lugares deseados. Este objeto puede tener varios Atributos.

#### Atributo `name`

- Obligatorio: si

El Atributo `name` se utiliza para indicar sobre qué elemento de texto debe actuar el contenido. Puede tener uno o varios de estos valores al mismo tiempo.

Lista de valores que pueden utilizarse en la lista de sistemas (`<view name="system">`) :

- `systemInfo`: el texto que indica el número de juegos presentes en el sistema, con el número de juegos favoritos y ocultos.

Lista de valores que se pueden utilizar en la lista de juegos (`<view name="detailed">`) :

- `logoText`: el nombre del sistema cuando `logo` no existe.
- `md_lbl_rating`: el texto que indica la clasificación.
- `md_lbl_releasedate`: el texto que indica la fecha de lanzamiento.
- `md_lbl_developer`: el texto que indica el desarrollador.
- `md_lbl_publisher`: el texto que indica el editor.
- `md_lbl_genre`: el texto que indica el género.
- `md_lbl_lastplayed`: el texto que indica la última vez que jugaste al juego.
- `md_lbl_players`: el texto que indica el número de jugadores.
- `md_lbl_playcount`: el texto que indica el número de veces que se ha jugado.
- `md_lbl_favorite`: el texto que indica si el juego está en la lista de favoritos.
- `md_developer`: el desarrollador.
- `md_publisher`: el editor.
- `md_genre`: el género.
- `md_players`: el número de jugadores.
- `md_playcount`: el número de veces que se ha jugado al juego.
- `md_favorite`: indica si el juego está en la lista de favoritos.
- `md_description`: el texto descriptivo de los juegos.
- `md_folder_name`: el texto que acompaña a las miniaturas de una carpeta.

Lista de valores que se pueden utilizar en los clips de juego (`<view name="gameclip">`) :

- `logoText`: el nombre del sistema cuando `logo` no existe.
- `md_lbl_gameName`: el texto que indica el nombre del juego.
- `md_lbl_systemName`: el texto que indica el nombre del sistema.
- `md_lbl_rating`: el texto que indica la clasificación.
- `md_lbl_releasedate`: el texto que indica la fecha de lanzamiento.
- `md_lbl_developer`: el texto que indica el desarrollador.
- `md_lbl_publisher`: el texto que indica el editor.
- `md_lbl_genre`: el texto que indica el género.
- `md_lbl_lastplayed`: el texto que indica la última vez que jugaste al juego.
- `md_lbl_players`: el texto que indica el número de jugadores.
- `md_lbl_playcount`: el texto que indica el número de veces que se ha jugado.
- `md_lbl_favorite`: el texto que indica si el juego está en la lista de favoritos.
- `md_gameName`: el nombre del juego.
- `md_developer`: el desarrollador.
- `md_publisher`: el editor.
- `md_genre`: el género.
- `md_players`: el número de jugadores.
- `md_playcount`: el número de veces que se ha jugado al juego.
- `md_favorite`: indica si el juego está en la lista de favoritos.
- `md_description`: el texto descriptivo de los juegos.
- `md_systemName`: el nombre del sistema.

```xml
<text name="md_developer, md_publisher, md_genre, md_playcount, md_lastplayed, md_description, md_players, md_releasedate, md_favorite">
  ...
</text>
```

#### Atributo `extra`

Podéis crear vuestro propio valor para el Atributo `name` fuera de la lista anterior, pero tendréis que añadir el atributo `extra="true"` para que se tenga en cuenta.

```xml
<text name="control" extra="true">
  ...
</text>
```

El objeto `<text>` admite varias otras propiedades.

#### Propiedad `<pos>`

La propiedad `<pos>` se utiliza para indicar la posición del objeto padre. La posición a indicar corresponde al punto más alto a la izquierda del objeto. Contiene 2 valores. El primer valor corresponde al posicionamiento desde la izquierda y el segundo valor al posicionamiento desde arriba.

Los valores son porcentajes entre 0 (0%) y 1 (100%).

```xml
<pos>0.33 0.25</pos>
```

En el ejemplo anterior, la posición indica que el objeto padre debe situarse a un 33% de la izquierda y a un 25% de la parte superior.

#### Propiedad `<size>`

La propiedad `<size>` se utiliza para definir el tamaño del elemento que contiene el objeto padre.

```xml
<size>1 1</size>
```

#### Propiedad `<origin>`

La propiedad `<origin>` se utiliza para definir un nuevo punto de partida de origen para todos los tamaños del objeto padre.

Los valores son porcentajes entre 0 (0%) y 1 (100%).

```xml
<origin>0.5 0.5</origin>
```

#### Propiedad `<rotation>`

La propiedad `<rotation>` se utiliza para especificar un ángulo de rotación para el objeto padre. Los valores positivos rotarán en el sentido de las agujas del reloj y los negativos en sentido contrario.

```xml
<rotation>90</rotation>
```

#### Propiedad `<rotationOrigin>`

La propiedad `<rotationOrigin>` permite definir un nuevo punto de partida del origen para todos los tamaños del objeto padre sólo si se va a realizar una rotación. 

Los valores son porcentajes entre 0 (0%) y 1 (100%).

```xml
<rotationOrigin>0.5 0.5</rotationOrigin>
```

#### Propiedad `<text>`

La propiedad `<text>` permite añadir texto.

```xml
<text>Text aléatoire ici</text>
```

También puede añadir texto en varios idiomas especificando el idioma en la propiedad.

```xml
<text.fr>Text aléatoire ici</text.fr>
```

Podéis utilizar el código de idioma como se muestra en el ejemplo anterior, pero también podéis especificar la región como en el ejemplo siguiente.

```xml
<text.en_us>Random text here</text.en_us>
```

Puede tener varios idiomas definidos en el tema, pero en la primera ocurrencia válida, la búsqueda de texto para mostrar se detendrá allí.

Vamos a entrar en un poco más de detalle con los siguientes 2 ejemplos:

```xml
<text text="Washington" text.fr="Paris" text.de_CH="Berne" text.de="Berlin" />
```

- Si el usuario ha configurado su Recalbox como `de_CH`, obtendrá "Berna", porque es el primero que coincide con su idioma o idioma + región.
- Si el usuario es alemán y ha configurado su Recalbox con `de_DE`, obtendrá "Berlín", porque sólo el código de idioma coincidirá con los últimos sufijos del texto.
- Si el usuario es francés con `fr_FR`. Los quebequenses con `fr_CA` y los belgas con `fr_BE` también recibirán París.
- Los que no tengan ninguna coincidencia recibirán "Washington".

```xml
<text text.de="Berlin" text.de_CH="Berne" text.fr="Paris" text="Washington" />
```

- Aquí nada cambia en comparación con el ejemplo anterior, ***EXCEPTO*** para los usuarios configurados como `de_CH` que tendrán "Berlín", porque el código de idioma de `text.de` coincidirá antes de que llegue `text.de_CH`.

Los códigos de idioma tienen siempre 2 caracteres y están disponibles [aquí](https://fr.wikipedia.org/wiki/Liste_des_codes_ISO_639-1).
Los códigos de regiones tienen siempre 2 caracteres y están disponibles [aquí](https://fr.wikipedia.org/wiki/ISO_3166-2).

#### Propiedad `<path>`

La propiedad `<path>` le permite cargar un archivo de texto en lugar de introducir el texto manualmente.

```xml
<path>./chemin/vers/fichier/texte.txt</path>
```

#### Propiedad `<backgroundColor>`

La propiedad `<backgroundColor>` se utiliza para definir un color de fondo que se aplicará al objeto padre. Los colores pueden estar en formato RRVVBB o RRVVBBAA (AA para transparencia, 00 = transparente y ff = opaco).

```xml
<backgroundColor>ffffff00</backgroundColor>
```

#### Propiedad `<fontPath>`

La propiedad `<fontPath>` se utiliza para especificar una ruta para cargar la fuente deseada desde el objeto padre.

```xml
<fontPath>./../arts/Exo2-BoldCondensed.otf</fontPath>
```

#### Propiedad `<fontSize>`

La propiedad `<fontSize>` se utiliza para indicar el tamaño de fuente que se aplicará al objeto padre. El valor se expresa como un porcentaje de la altura de la pantalla (un valor de `0.1` indica un tamaño de fuente del 10% de la altura de la pantalla).

```xml
<fontSize>0.025</fontSize>
```

#### Propiedad `<color>`

La propiedad `<color>` se utiliza para indicar el color del texto del objeto padre. Los colores pueden estar en formato RRVVBB o RRVVBBAA (AA para transparencia, 00 = transparente y ff = opaco).

```xml
<color>666666</color>
```

#### Propiedad `<alignment>`

La propiedad `<alignment>` permite elegir la alineación del objeto padre: `left` = alineación izquierda, `center` = centrada y `right` = alineación derecha. La alineación centrada también centra el objeto padre verticalmente.

```xml
<alignment>left</alignment>
```

#### Propiedad `<forceUppercase>`

La propiedad `<forceUppercase>` se utiliza para forzar que el texto del objeto padre esté todo en mayúsculas. 0 = ningún cambio, 1 = forzar a mayúsculas.

```xml
<forceUppercase>1</forceUppercase>
```

#### Propiedad `<lineSpacing>`

La propiedad `<lineSpacing>` controla el espacio entre las líneas del objeto padre en un múltiplo de la altura de la fuente.

```xml
<lineSpacing>1.5</lineSpacing>
```

#### Propiedad `<zIndex>`

La propiedad `<zIndex>` se utiliza para ordenar los elementos por profundidad / capa. Cuanto mayor sea su valor, más en primer plano estará. Puede ser negativo.

```xml
<zIndex>10</zIndex>
```

#### Propiedad `<disabled>`

La propiedad `<disabled>` desactiva el objeto padre.

```xml
<disabled>true</disabled>
```

## Objeto `<scrolltext>` ![](/tutorials/newicon.png)

Los objetos `<scrolltext>` permiten desplazar horizontalmente un texto demasiado largo sobre un área más pequeña. Este objeto puede tener varios atributos:

#### Atributo `name`

- Obligatorio: si

El atributo `name` se utiliza para indicar sobre qué elemento de texto debe actuar el contenido. Puede tener uno o varios de estos valores al mismo tiempo.

Lista de valores que pueden utilizarse en la lista de sistemas (`<view name="system">`) :

- `systemInfo`: el texto que indica el número de juegos presentes en el sistema, con el número de juegos favoritos y ocultos.

Lista de valores que se pueden utilizar en la lista de juegos (`<view name="detailed">`) :

- `md_lbl_rating`: el texto que indica la clasificación.
- `md_lbl_releasedate`: el texto que indica la fecha de lanzamiento.
- `md_lbl_developer`: el texto que indica el desarrollador.
- `md_lbl_publisher`: el texto que indica el editor.
- `md_lbl_genre`: el texto que indica el género.
- `md_lbl_lastplayed`: el texto que indica la última vez que jugaste al juego.
- `md_lbl_players`: el texto que indica el número de jugadores.
- `md_lbl_playcount`: el texto que indica el número de veces que se ha jugado.
- `md_lbl_favorite`: el texto que indica si el juego está en la lista de favoritos.
- `md_developer`: el desarrollador.
- `md_publisher`: el editor.
- `md_genre`: el género.
- `md_players`: el número de jugadores.
- `md_playcount`: el número de veces que se ha jugado al juego.
- `md_favorite`: indica si el juego está en la lista de favoritos.
- `md_description`: el texto descriptivo de los juegos.

Lista de valores que se pueden utilizar en los clips de juego (`<view name="gameclip">`):

- `md_lbl_gameName`: el texto que indica el nombre del juego.
- `md_lbl_systemName`: el texto que indica el nombre del sistema.
- `md_lbl_rating`: el texto que indica la clasificación.
- `md_lbl_releasedate`: el texto que indica la fecha de lanzamiento.
- `md_lbl_developer`: el texto que indica el desarrollador.
- `md_lbl_publisher`: el texto que indica el editor.
- `md_lbl_genre`: el texto que indica el género.
- `md_lbl_lastplayed`: el texto que indica la última vez que jugaste al juego.
- `md_lbl_players`: el texto que indica el número de jugadores.
- `md_lbl_playcount`: el texto que indica el número de veces que se ha jugado.
- `md_lbl_favorite`: el texto que indica si el juego está en la lista de favoritos.
- `md_gameName`: el nombre del juego.
- `md_developer`: el desarrollador.
- `md_publisher`: el editor.
- `md_genre`: el género.
- `md_players`: el número de jugadores.
- `md_playcount`: el número de veces que se ha jugado al juego.
- `md_favorite`: indica si el juego está en la lista de favoritos.
- `md_description`: el texto descriptivo de los juegos.
- `md_systemName`: el nombre del sistema.

```xml
<scrolltext name="md_developer, md_publisher, md_genre, md_playcount, md_lastplayed, md_description, md_players, md_releasedate, md_favorite">
  ...
</scrolltext>
```

El objeto `<scrolltext>` contiene varias otras propiedades:

#### Propiedad `<pos>`

La propiedad `<pos>` se utiliza para indicar la posición del objeto padre. La posición a indicar corresponde al punto más alto a la izquierda del objeto. Contiene 2 valores. El primer valor corresponde al posicionamiento desde la izquierda y el segundo valor al posicionamiento desde arriba.

Los valores son porcentajes entre 0 (0%) y 1 (100%).

```xml
<pos>0.33 0.25</pos>
```

En el ejemplo anterior, la posición indica que el objeto padre debe situarse a un 33% de la izquierda y a un 25% de la parte superior.

#### Propiedad `<size>`

La propiedad `<size>` se utiliza para definir el tamaño del elemento que contiene el objeto padre.

```xml
<size>1 1</size>
```

#### Propiedad `<origin>`

La propiedad `<origen>` se utiliza para definir un nuevo punto de partida de origen para todos los tamaños del objeto padre.

Los valores son porcentajes entre 0 (0%) y 1 (100%).

```xml
<origin>0.5 0.5</origin>
```

#### Propiedad `<rotation>`

La propiedad `<rotation>` se utiliza para especificar un ángulo de rotación para el objeto padre. Los valores positivos rotarán en el sentido de las agujas del reloj y los negativos en sentido contrario.

```xml
<rotation>90</rotation>
```

#### Propiedad `<rotationOrigin>`

La propiedad `<rotationOrigin>` permite definir un nuevo punto de partida del origen para todos los tamaños del objeto padre sólo si se va a realizar una rotación. 

Los valores son porcentajes entre 0 (0%) y 1 (100%).

```xml
<rotationOrigin>0.5 0.5</rotationOrigin>
```

#### Propiedad `<text>`

La propiedad `<text>` permiter incluir texto.

```xml
<text>Texto aléatorio aqui</text>
```

También puede añadir texto en varios idiomas especificando el idioma en la propiedad.

```xml
<text.fr>Texto aléatorio aqui</text.fr>
```

Puede utilizar sólo el código de idioma como se muestra en el ejemplo anterior, pero también puede especificar la región como en el ejemplo siguiente.

```xml
<text.en_us>Random text here</text.en_us>
```

Puede tener varios idiomas definidos en el tema, pero en la primera ocurrencia válida, la búsqueda de texto para mostrar se detendrá allí.

Vamos a entrar en un poco más de detalle con los siguientes 2 ejemplos:

```xml
<text text="Washington" text.fr="Paris" text.de_CH="Berne" text.de="Berlin" />
```

- Si el usuario ha configurado su Recalbox como `de_CH`, obtendrá "Berna", porque es el primero que coincide con su idioma o idioma + región.
- Si el usuario es alemán y ha configurado su Recalbox con `de_DE`, obtendrá "Berlín", porque sólo el código de idioma coincidirá con los últimos sufijos del texto.
- Si el usuario es francés con `fr_FR`. Los quebequenses con `fr_CA` y los belgas con `fr_BE` también recibirán París.
- Los que no tengan ninguna coincidencia recibirán "Washington".

```xml
<text text.de="Berlin" text.de_CH="Berne" text.fr="Paris" text="Washington" />
```

- Aquí nada cambia en comparación con el ejemplo anterior, ***EXCEPTO*** para los usuarios configurados como `de_CH` que tendrán "Berlín", porque el código de idioma de `text.de` coincidirá antes de que llegue `text.de_CH`.

Los códigos de idioma tienen siempre 2 caracteres y están disponibles [aquí](https://fr.wikipedia.org/wiki/Liste_des_codes_ISO_639-1).
Los códigos de regiones tienen siempre 2 caracteres y están disponibles [aquí](https://fr.wikipedia.org/wiki/ISO_3166-2).

#### Propiedad `<path>`

La propiedad `<path>` le permite cargar un archivo de texto en lugar de introducir el texto manualmente.

```xml
<path>./chemin/vers/fichier/texte.txt</path>
```

#### Propiedad `<backgroundColor>`

La propiedad `<backgroundColor>` se utiliza para definir un color de fondo que se aplicará al objeto padre. Los colores pueden estar en formato RRVVBB o RRVVBBAA (AA para transparencia, 00 = transparente y ff = opaco).

```xml
<backgroundColor>ffffff00</backgroundColor>
```

#### Propiedad `<fontPath>`

La propiedad `<fontPath>` se utiliza para especificar una ruta para cargar la fuente deseada desde el objeto padre.

```xml
<fontPath>./../arts/Exo2-BoldCondensed.otf</fontPath>
```

#### Propiedad `<fontSize>`

La propiedad `<fontSize>` se utiliza para indicar el tamaño de fuente que se aplicará al objeto padre. El valor se expresa como un porcentaje de la altura de la pantalla (un valor de `0.1` indica un tamaño de fuente del 10% de la altura de la pantalla).

```xml
<fontSize>0.025</fontSize>
```

#### Propiedad `<color>`

La propiedad `<color>` se utiliza para indicar el color del texto del objeto padre. Los colores pueden estar en formato RRVVBB o RRVVBBAA (AA para transparencia, 00 = transparente y ff = opaco).

```xml
<color>666666</color>
```

#### Propiedad `<alignment>`

La propiedad `<alignment>` permite elegir la alineación del objeto padre: `left` = alineación izquierda, `center` = centrada y `right` = alineación derecha. La alineación centrada también centra el objeto padre verticalmente.

```xml
<alignment>left</alignment>
```

#### Propiedad `<forceUppercase>`

La propiedad `<forceUppercase>` se utiliza para forzar que el texto del objeto padre esté todo en mayúsculas. 0 = ningún cambio, 1 = forzar a mayúsculas.

```xml
<forceUppercase>1</forceUppercase>
```

#### Propiedad `<zIndex>`

La propiedad `<zIndex>` se utiliza para ordenar los elementos por profundidad / capa. Cuanto mayor sea su valor, más en primer plano estará. Puede ser negativo.

```xml
<zIndex>10</zIndex>
```

#### Propiedad `<disabled>`

La propiedad `<disabled>` desactiva el objeto padre.

```xml
<disabled>true</disabled>
```

## Objeto `<textlist>`

Los objetos `<textlist>` se utilizan para mostrar una lista de texto. Este objeto puede tener varios Atributos :

#### Atributo `name`

- Obligatorio: si

El atributo `name` se utiliza para indicar sobre qué elemento de texto debe actuar el contenido. Puede tener uno o más de estos valores al mismo tiempo.

Lista de valores que se pueden utilizar en la lista de conjuntos (`<view name="detailed">`) :

- `gamelist` : la lista de juegos de un sistema.

```xml
<textlist name="gamelist">
  ...
</textlist>
```

El objeto `<textlist>` puede contener otras propiedades:

#### Propiedad `<pos>`

La propiedad `<pos>` se utiliza para indicar la posición del objeto padre. La posición a indicar corresponde al punto más alto a la izquierda del objeto. Contiene 2 valores. El primer valor corresponde al posicionamiento desde la izquierda y el segundo valor al posicionamiento desde arriba.

Los valores son porcentajes entre 0 (0%) y 1 (100%).

```xml
<pos>0.33 0.25</pos>
```

En el ejemplo anterior, la posición indica que el objeto padre debe situarse a un 33% de la izquierda y a un 25% de la parte superior.

#### Propiedad `<size>`

La propiedad `<size>` se utiliza para definir el tamaño del elemento que contiene el objeto padre.

```xml
<size>1 1</size>
```

#### Propiedad `<origin>`

La propiedad `<origen>` se utiliza para definir un nuevo punto de partida de origen para todos los tamaños del objeto padre.

Los valores son porcentajes entre 0 (0%) y 1 (100%).

```xml
<origin>0.5 0.5</origin>
```

#### Propiedad `<selectorHeight>`

La propiedad `<selectorHeight>` se utiliza para aplicar una altura de resaltado a los elementos del objeto padre.

```xml
<selectorHeight>0.5</selectorHeight>
```

#### Propiedad `<selectorOffsetY>`

La propiedad `<selectorOffsetY>` permite aplicar un valor de desplazamiento vertical al resaltado.

```xml
<selectorOffsetY>0.5</selectorOffsetY>
```

#### Propiedad `<selectorColor>`

La propiedad `<selectorColor>` permite utilizar un color específico para resaltar elementos del objeto padre. Los colores pueden estar en formato RRVVBB o RRVVBBAA (AA para transparencia, 00 = transparente y ff = opaco).

```xml
<selectorColor>4368ffff</selectorColor>
```

#### Propiedad `<selectorImagePath>`

La propiedad `<selectorImagePath>` se utiliza para indicar la ruta a una imagen que se utilizará como fondo para resaltar elementos en el objeto padre.

```xml
<selectorImagePath>/camino/hacia/fichero/imagen.png</selectorImagePath>
```

#### Propiedad `<selectorImageTile>`

La propiedad `<selectorImageTile>` permite definir si la imagen se muestra como un mosaico en lugar de ser estirada para ajustarse al tamaño del objeto. Esto es útil para los fondos.

```xml
<selectorImageTile>false</selectorImageTile>
```

#### Propiedad `<selectedColor>`

La propiedad `<selectedColor>` se utiliza para definir el color del texto resaltado. Los colores pueden estar en formato RRVVBB o RRVVBBAA (AA para transparencia, 00 = transparente y ff = opaco).

```xml
<selectedColor>4368ffff</selectedColor>
```

#### Propiedad `<primaryColor>`

La propiedad `<primaryColor>` permite definir el color del texto de los nombres de los juegos. Los colores pueden estar en formato RRVVBB o RRVVBBAA (AA para transparencia, 00 = transparente y ff = opaco).

```xml
<primaryColor>4368ffff</primaryColor>
```

#### Propiedad `<secondaryColor>`

La propiedad `<secondaryColor>` permite definir el color del texto de los nombres de carpeta. Los colores pueden estar en formato RRVVBB o RRVVBBAA (AA para transparencia, 00 = transparente y ff = opaco).

```xml
<secondaryColor>4368ffff</secondaryColor>
```

#### Propiedad `<fontPath>`

La propiedad `<fontPath>` se utiliza para especificar una ruta para cargar la fuente deseada desde el objeto padre.

```xml
<fontPath>./../arts/Exo2-BoldCondensed.otf</fontPath>
```

#### Propiedad `<fontSize>`

La propiedad `<fontSize>` se utiliza para indicar el tamaño de fuente que se aplicará al objeto padre. El valor se expresa como un porcentaje de la altura de la pantalla (un valor de `0.1` indica un tamaño de fuente del 10% de la altura de la pantalla).

```xml
<fontSize>0.025</fontSize>
```

#### Propiedad `<alignment>`

La propiedad `<alignment>` permite elegir la alineación del objeto padre: `left` = alineación izquierda, `center` = centrada y `right` = alineación derecha. La alineación centrada también centra el objeto padre verticalmente.

```xml
<alignment>left</alignment>
```

#### Propiedad `<horizontalMargin>`

La propiedad `<horizontalMargin>` permite aplicar un margen a los lados del texto de la lista.
Los selectores y el contenedor de la lista siguen utilizando el 100% de la anchura especificada.

```xml
<horizontalMargin>0.1</horizontalMargin>
```

#### Propiedad `<forceUppercase>`

La propiedad `<forceUppercase>` se utiliza para forzar que el texto del objeto padre esté todo en mayúsculas. 0 = ningún cambio, 1 = forzar a mayúsculas.

```xml
<forceUppercase>1</forceUppercase>
```

#### Propiedad `<lineSpacing>`

La propiedad `<lineSpacing>` controla el espacio entre las líneas del objeto padre en un múltiplo de la altura de la fuente.

```xml
<lineSpacing>1.5</lineSpacing>
```

#### Propiedad `<zIndex>`

La propiedad `<zIndex>` se utiliza para ordenar los elementos por profundidad / capa. Cuanto mayor sea su valor, más en primer plano estará. Puede ser negativo.

```xml
<zIndex>10</zIndex>
```

## Objeto `<datetime>`

Los objetos `<datetime>` se utilizan para mostrar una fecha y una hora. Este objeto puede tener varios Atributos :

#### Atributo `name`

- Obligatorio: si

El Atributo `name` se utiliza para indicar sobre qué elemento de texto debe actuar el contenido. Puede tener uno o varios de estos valores al mismo tiempo.

Lista de valores que se pueden utilizar en la lista de juegos (`<view name="detailed">`) :

- `md_lastplayed`: la fecha en que se lanzó el juego por última vez.
- `md_releasedate`: la fecha de lanzamiento del juego.

Lista de valores que se pueden utilizar en los clips de juego (`<view name="gameclipview">`):

- `md_releasedate`: la fecha de lanzamiento del juego.

```xml
<datetime name="md_releasedate">
  ...
</datetime>
```

El objeto `<datetime>` puede contener otros Propiedads.

#### Propiedad `<pos>`

La propiedad `<pos>` se utiliza para indicar la posición del objeto padre. La posición a indicar corresponde al punto más alto a la izquierda del objeto. Contiene 2 valores. El primer valor corresponde al posicionamiento desde la izquierda y el segundo valor al posicionamiento desde arriba.

Los valores son porcentajes entre 0 (0%) y 1 (100%).

```xml
<pos>0.33 0.25</pos>
```

En el ejemplo anterior, la posición indica que el objeto padre debe situarse a un 33% de la izquierda y a un 25% de la parte superior.

#### Propiedad `<size>`

La propiedad `<size>` se utiliza para definir el tamaño del elemento que contiene el objeto padre.

```xml
<size>1 1</size>
```

#### Propiedad `<origin>`

La propiedad `<origen>` se utiliza para definir un nuevo punto de partida de origen para todos los tamaños del objeto padre.

Los valores son porcentajes entre 0 (0%) y 1 (100%).

```xml
<origin>0.5 0.5</origin>
```

#### Propiedad `<color>`

La propiedad `<color>` se utiliza para indicar el color del texto del objeto padre. Los colores pueden estar en formato RRVVBB o RRVVBBAA (AA para transparencia, 00 = transparente y ff = opaco).

```xml
<color>666666</color>
```

#### Propiedad `<fontPath>`

La propiedad `<fontPath>` se utiliza para especificar una ruta para cargar la fuente deseada desde el objeto padre.

```xml
<fontPath>./../arts/Exo2-BoldCondensed.otf</fontPath>
```

#### Propiedad `<fontSize>`

La propiedad `<fontSize>` se utiliza para indicar el tamaño de fuente que se aplicará al objeto padre. El valor se expresa como un porcentaje de la altura de la pantalla (un valor de `0.1` indica un tamaño de fuente del 10% de la altura de la pantalla).

```xml
<fontSize>0.025</fontSize>
```

#### Propiedad `<alignment>`

La propiedad `<alignment>` permite elegir la alineación del objeto padre: `left` = alineación izquierda, `center` = centrada y `right` = alineación derecha. La alineación centrada también centra el objeto padre verticalmente.

```xml
<alignment>left</alignment>
```

#### Propiedad `<forceUppercase>`

La propiedad `<forceUppercase>` se utiliza para forzar que el texto del objeto padre esté todo en mayúsculas. 0 = ningún cambio, 1 = forzar a mayúsculas.

```xml
<forceUppercase>1</forceUppercase>
```

#### Propiedad `<zIndex>`

La propiedad `<zIndex>` se utiliza para ordenar los elementos por profundidad / capa. Cuanto mayor sea su valor, más en primer plano estará. Puede ser negativo.

```xml
<zIndex>10</zIndex>
```

#### Propiedad `<disabled>`

La propiedad `<disabled>` desactiva el objeto padre.

```xml
<disabled>true</disabled>
```

#### Propiedad `<display>`

La propiedad `<display>` se utiliza para indicar el formato de la fecha a mostrar.

Valores posibles:

- `date`: muestra la fecha.
- `dateTime`: muestra la fecha y la hora.
- `year`: Muestra el año.
- `realTime`: Muestra la hora real.
- `time`: Muestra la hora.
- `RelativeToNow`: Muestra la fecha relativa al ahora (`x días atrás`, `x horas atrás`, etc.).

```xml
<display>date</display>
```

## Objet `<rating>`

Los objetos `<rating>` se utilizan para mostrar la clasificación de un juego. Este objeto puede tener varios Atributos :

#### Atributo `name`

- Obligatorio: si

El atributo `name` se utiliza para indicar sobre qué elemento de texto debe actuar el contenido. Puede tener uno o varios de estos valores al mismo tiempo.

Lista de valores que se pueden utilizar en la lista de juegos (`<view name="detailed">`) :

- `md_rating`: la clasificación del juego.

```xml
<rating name="md_rating">
  ...
</rating>
```

El objeto `<rating>` puede contener otras propiedades.

#### Propiedad `<pos>`

La propiedad `<pos>` se utiliza para indicar la posición del objeto padre. La posición a indicar corresponde al punto más alto a la izquierda del objeto. Contiene 2 valores. El primer valor corresponde al posicionamiento desde la izquierda y el segundo valor al posicionamiento desde arriba.

Los valores son porcentajes entre 0 (0%) y 1 (100%).

```xml
<pos>0.33 0.25</pos>
```

En el ejemplo anterior, la posición indica que el objeto padre debe situarse a un 33% de la izquierda y a un 25% de la parte superior.

#### Propiedad `<size>`

La propiedad `<size>` se utiliza para definir el tamaño del elemento que contiene el objeto padre.

```xml
<size>1 1</size>
```

#### Propiedad `<origin>`

La propiedad `<origen>` se utiliza para definir un nuevo punto de partida de origen para todos los tamaños del objeto padre.

Los valores son porcentajes entre 0 (0%) y 1 (100%).

```xml
<origin>0.5 0.5</origin>
```

#### Propiedad `<rotation>`

La propiedad `<rotation>` se utiliza para especificar un ángulo de rotación para el objeto padre. Los valores positivos rotarán en el sentido de las agujas del reloj y los negativos en sentido contrario.

```xml
<rotation>90</rotation>
```

#### Propiedad `<rotationOrigin>`

La propiedad `<rotationOrigin>` permite definir un nuevo punto de partida del origen para todos los tamaños del objeto padre sólo si se va a realizar una rotación. 

Los valores son porcentajes entre 0 (0%) y 1 (100%).

```xml
<rotationOrigin>0.5 0.5</rotationOrigin>
```

#### Propiedad `<filledPath>`

La propiedad `<filledPath>` permite cargar un archivo de imagen correspondiente a la tasa de relleno de la nota de juego.

```xml
<filledPath>/camino/hacia/fichero/imagen.png</filledPath>
```

#### Propiedad `<unfilledPath>`

La propiedad `<unfilledPath>` permite cargar un archivo de imagen correspondiente a la puntuación total del juego.

```xml
<unfilledPath>/camino/hacia/fichero/imagen.png</unfilledPath>
```

#### Propiedad `<zIndex>`

La propiedad `<zIndex>` se utiliza para ordenar los elementos por profundidad / capa. Cuanto mayor sea su valor, más en primer plano estará. Puede ser negativo.

```xml
<zIndex>10</zIndex>
```

#### Propiedad `<disabled>`

La propiedad `<disabled>` desactiva el objeto padre.

```xml
<disabled>true</disabled>
```

## Objeto `<sound>`

Los objetos `<sound>` se utilizan para reproducir música en la interfaz. Este objeto puede tener varios Atributos :

#### Atributo `name`

- Obligatorio: si

El atributo `name` se utiliza para indicar sobre qué elemento de texto debe actuar el contenido. Puede tener uno o varios de estos valores al mismo tiempo.

Lista de valores que se pueden utilizar en la lista del sistema (`<view name="system">`) :

- `directory`: la carpeta de música.

```xml
<sound name="directory">
  ...
</sound>
```

El objeto `<sound>` puede contener una única Propiedad.

#### Propiedad `<path>`

La propiedad `<path>` permite especificar una carpeta en la que buscar la música que se va a reproducir.

```xml
<path>./camino/hacia/carpeta</path>
```

## Objeto `<helpsystem>`

Los objetos `<helpsystem>` se utilizan para mostrar los botones en la parte inferior de la interfaz. Este objeto puede tener varios Atributos :

#### Atributo `name`

- Obligatorio: si

El atributo `name` se utiliza para indicar sobre qué elemento de texto debe actuar el contenido. Puede tener uno o más de estos valores al mismo tiempo.

Lista de valores que se pueden utilizar en la lista de sistema, lista de juego, clips de juego y menús (`<view name="system, detailed, gameclip, menu">`):

- `help`: los botones de la parte inferior de la interfaz.

```xml
<helpsystem name="help">
  ...
</helpsystem>
```

El objeto `<helpsystem>` puede contener otros Propiedads.

#### Propiedad `<pos>`

La propiedad `<pos>` se utiliza para indicar la posición del objeto padre. La posición a indicar corresponde al punto más alto a la izquierda del objeto. Contiene 2 valores. El primer valor corresponde al posicionamiento desde la izquierda y el segundo valor al posicionamiento desde arriba.

Los valores son porcentajes entre 0 (0%) y 1 (100%).

```xml
<pos>0.33 0.25</pos>
```

En el ejemplo anterior, la posición indica que el objeto padre debe situarse a un 33% de la izquierda y a un 25% de la parte superior.

#### Propiedad `<textColor>`

La propiedad `<textColor>` se utiliza para indicar el color del texto del objeto padre. Los colores pueden estar en formato RRVVBB o RRVVBBAA (AA para transparencia, 00 = transparente y ff = opaco).

```xml
<textColor>666666</textColor>
```

#### Propiedad `<iconColor>`

La propiedad `<iconColor>` se utiliza para indicar el color del icono del objeto padre. Los colores pueden estar en formato RRVVBB o RRVVBBAA (AA para transparencia, 00 = transparente y ff = opaco).

```xml
<iconColor>666666</iconColor>
```

#### Propiedad `<fontPath>`

La propiedad `<fontPath>` se utiliza para especificar una ruta para cargar la fuente deseada desde el objeto padre.

```xml
<fontPath>./../arts/Exo2-BoldCondensed.otf</fontPath>
```

#### Propiedad `<fontSize>`

La propiedad `<fontSize>` se utiliza para indicar el tamaño de fuente que se aplicará al objeto padre. El valor se expresa como un porcentaje de la altura de la pantalla (un valor de `0.1` indica un tamaño de fuente del 10% de la altura de la pantalla).

```xml
<fontSize>0.025</fontSize>
```

#### Propiedad `<iconUpDown>`

La propiedad `<iconUpDown>` permite especificar el archivo que representa los botones `Up' y `Down'.

```xml
<iconUpDown>/camino/hacia/fichero/imagen.png</iconUpDown>
```

#### Propiedad `<iconLeftRight>`

La propiedad `<iconLeftRight>` se utiliza para especificar el archivo que representa los botones `Izquierda` y `Derecha`.

```xml
<iconLeftRight>/camino/hacia/fichero/imagen.png</iconLeftRight>
```

#### Propiedad `<iconUpDownLeftRight>`

La propiedad `<iconUpDownLeftright>` permite especificar el archivo que representa los botones `Arriba', `Abajo', `Izquierda' y `Derecha'.

```xml
<iconUpDownLeftRight>/camino/hacia/fichero/imagen.png</iconUpDownLeftRight>
```

#### Propiedad `<iconA>`

La propiedad `<iconA>` se utiliza para indicar el archivo que representa el botón `A` de un mando SNES.

```xml
<iconA>./camino/hacia/fichero/imagen.png</iconA>
```

#### Propiedad `<iconB>`

La propiedad `<iconB>` se utiliza para indicar el archivo que representa el botón `B` de un mando SNES.

```xml
<iconB>./camino/hacia/fichero/imagen.png</iconB>
```

#### Propiedad `<iconX>`

La propiedad `<iconX>` se utiliza para indicar el archivo que representa el botón `X` de un mando SNES.

```xml
<iconX>./camino/hacia/fichero/imagen.png</iconX>
```

#### Propiedad `<iconY>`

La propiedad `<iconY>` se utiliza para indicar el archivo que representa el botón `Y` en un mando SNES.

```xml
<iconY>./camino/hacia/fichero/imagen.png</iconY>
```

#### Propiedad `<iconL>`

La propiedad `<iconL>` se utiliza para indicar el archivo que representa el botón `L` de un mando SNES.

```xml
<iconL>./camino/hacia/fichero/imagen.png</iconL>
```

#### Propiedad `<iconR>`

La propiedad `<iconR>` se utiliza para indicar el archivo que representa el botón `R` en un mando SNES.

```xml
<iconR>./camino/hacia/fichero/imagen.png</iconR>
```

#### Propiedad `<iconStart>`

La propiedad `<iconStart>` se utiliza para indicar el archivo que representa el botón `Start` en un mando SNES.

```xml
<iconStart>./camino/hacia/fichero/imagen.png</iconStart>
```

#### Propiedad `<iconSelect>`

La propiedad `<iconSelect>` se utiliza para indicar el archivo que representa el botón `Select` en un mando SNES.

```xml
<iconSelect>./camino/hacia/fichero/imagen.png</iconSelect>
```

## Objet `<carousel>`

Los objetos `<carousel>` se utilizan para mostrar la lista de sistemas. Este objeto puede tener varios Atributos :

#### Atributo `name`

- Obligatorio: si

El atributo `name` se utiliza para indicar sobre qué elemento de texto debe actuar el contenido. Puede tener uno o varios de estos valores al mismo tiempo.

Lista de valores que se pueden utilizar en la lista de sistemas (`<view name="system">`):

- `systemcarousel`: la lista de sistemas.

```xml
<carousel name="systemcarousel">
  ...
</carousel>
```

El objeto `<carousel>` puede contener otras propiedades:

#### Propiedad `<type>`

La propiedad `<type>` se utiliza para definir la dirección en la que se muestra la lista de sistemas. El tipo por defecto es `horizontal`.

Valores posibles:

- `horizontal`: lista de sistemas orientados horizontalmente.
- `vertical`: lista de sistemas orientados verticalmente.
- `vertical_wheel`: lista de sistemas en una rueda orientada verticalmente.

```xml
<type>vertical</type>
```

#### Propiedad `<size>`

La propiedad `<size>` se utiliza para definir el tamaño del elemento que contiene el objeto padre.

```xml
<size>1 1</size>
```

#### Propiedad `<pos>`

La propiedad `<pos>` se utiliza para indicar la posición del objeto padre. La posición a indicar corresponde al punto más alto a la izquierda del objeto. Contiene 2 valores. El primer valor corresponde al posicionamiento desde la izquierda y el segundo valor al posicionamiento desde arriba.

Los valores son porcentajes entre 0 (0%) y 1 (100%).

```xml
<pos>0.33 0.25</pos>
```

En el ejemplo anterior, la posición indica que el objeto padre debe situarse a un 33% de la izquierda y a un 25% de la parte superior.

#### Propiedad `<origin>`

La propiedad `<origen>` se utiliza para definir un nuevo punto de partida de origen para todos los tamaños del objeto padre.

Los valores son porcentajes entre 0 (0%) y 1 (100%).

```xml
<origin>0.5 0.5</origin>
```

#### Propiedad `<color>`

La propiedad `<color>` se utiliza para indicar el color del texto del objeto padre. Los colores pueden estar en formato RRVVBB o RRVVBBAA (AA para transparencia, 00 = transparente y ff = opaco).

```xml
<color>666666</color>
```

#### Propiedad `<fontPath>`

La propiedad `<fontPath>` se utiliza para especificar una ruta para cargar la fuente deseada desde el objeto padre.

```xml
<fontPath>./../arts/Exo2-BoldCondensed.otf</fontPath>
```

#### Propiedad `<logoScale>`

La propiedad `<logoScale>` se utiliza para aplicar una extensión de escala al objeto padre sobre el que se pasa el ratón.

```xml
<logoScale>3</logoScale>
```

#### Propiedad `<logoRotation>`

La propiedad `<logoRotation>` permite especificar un ángulo de rotación para el objeto padre. Los valores positivos rotarán en el sentido de las agujas del reloj y los negativos en sentido contrario.

```xml
<logoRotation>90</logoRotation>
```

#### Propiedad `<logoRotationOrigin>`

La propiedad `<logoRotationOrigin>` se utiliza para definir un nuevo punto de partida de origen para todos los tamaños del objeto padre sólo si se va a realizar una rotación. 

Los valores son porcentajes entre 0 (0%) y 1 (100%).

```xml
<logoRotationOrigin>0.5 0.5</logoRotationOrigin>
```

#### Propiedad `<logoSize>`

La propiedad `<logoSize>` se utiliza para definir el tamaño del elemento que contiene el objeto padre.

```xml
<logoSize>1</logoSize>
```

#### Propiedad `<logoAlignment>`

La propiedad `<logoAlignment>` permite elegir la alineación del objeto padre. `left` = alineación izquierda, `center` = centrada y `right` = alineación derecha. La alineación centrada también centra el objeto padre verticalmente.

```xml
<logoAlignment>left</logoAlignment>
```

#### Propiedad `<maxLogoCount>`

La propiedad `<maxLogoCount>` se utiliza para indicar el número total de logotipos visibles.

```xml
<maxLogoCount>7</maxLogoCount>
```

#### Propiedad `<defaultTransition>`

La propiedad `<defaultTransition>` se utiliza para especificar la transición por defecto.

Valores posibles:

- `fade`: mostrará un fundido a negro entre cambio de sistema.
- `instant`: movimiento instantáneo en el cambio de sistema.
- `slide`: se desliza entre el cambio de sistema como un cambio de diapositiva.

```xml
<defaultTransition>slide</defaultTransition>
```

#### Propiedad `<zIndex>`

La propiedad `<zIndex>` se utiliza para ordenar los elementos por profundidad / capa. Cuanto mayor sea su valor, más en primer plano estará. Puede ser negativo.

```xml
<zIndex>10</zIndex>
```

## Objeto `<menuBackground>`

Los objetos `<menuBackground>` se utilizan para personalizar el fondo del menú. Este objeto puede tener varios Atributos :

#### Atributo `name`

- Obligatorio: si

El atributo `name` se utiliza para indicar sobre qué elemento de texto debe actuar el contenido. Puede tener uno o varios de estos valores al mismo tiempo.

Lista de valores que se pueden utilizar en los menús (`<view name="menu">`) :

- `menubg`: fondo del menú.

```xml
<menuBackground name="menubg">
  ...
</menuBackground>
```

El objeto `<menuBackground>` puede contener otras propiedades:

#### Propiedad `<color>`

La propiedad `<color>` se utiliza para indicar el color del texto del objeto padre. Los colores pueden estar en formato RRVVBB o RRVVBBAA (AA para transparencia, 00 = transparente y ff = opaco).

```xml
<color>666666</color>
```

#### Propiedad `<path>`

La propiedad `<path>` se utiliza para cargar un archivo de imagen.

```xml
<path>./camino/hacia/fichero/imagen.png</path>
```

#### Propiedad `<fadePath>`

La propiedad `<fadePath>` permite cargar un archivo de imagen para semioscurecer el fondo del menú.

```xml
<fadePath>./camino/hacia/fichero/imagen.png</fadePath>
```

## Objeto `<menuIcons>`

Los objetos `<menuIcons>` se utilizan para personalizar el fondo del menú. Este objeto puede tener varios Atributos :

#### Atributo `name`

- Obligatorio: si

El atributo `name` se utiliza para indicar sobre qué elemento de texto debe actuar el contenido. Puede tener uno o varios de estos valores al mismo tiempo.

Lista de valores que se pueden utilizar en los menús (`<view name="menu">`) :

- iconos de menú.

```xml
<menuIcons name="menuicons">
  ...
</menuIcons>
```

El objeto `<menuIcons>` puede contener otras propiedades:

#### Propiedad `<iconKodi>`

La propiedad `<iconKodi>` permite cargar un archivo de imagen para la línea de lanzamiento de Kodi en los menús.

```xml
<iconKodi>./camino/hacia/fichero/imagen.png</iconKodi>
```

#### Propiedad `<iconSystem>`

La propiedad `<iconSistema>` permite cargar un archivo de imagen para la línea de menú de configuración del sistema.

```xml
<iconSystem>./camino/hacia/fichero/imagen.png</iconSystem>
```

#### Propiedad `<iconUpdates>`

La propiedad `<iconUpdates>` permite cargar un archivo de imagen para la línea de menú de actualización.

```xml
<iconUpdates>./camino/hacia/fichero/imagen.png</iconUpdates>
```

#### Propiedad `<iconGames>`

La propiedad `<iconGames>` permite cargar un archivo de imagen para la línea de menú de opciones del juego.

```xml
<iconGames>./camino/hacia/fichero/imagen.png</iconGames>
```

#### Propiedad `<iconControllers>`

La propiedad `<iconControllers>` permite cargar un archivo de imagen para la línea de menú de configuración del joystick.

```xml
<iconControllers>./camino/hacia/fichero/imagen.png</iconControllers>
```

#### Propiedad `<iconUI>`

La propiedad `<iconUI>` permite cargar un archivo de imagen para la línea de menú de opciones de la interfaz.

```xml
<iconUI>./camino/hacia/fichero/imagen.png</iconUI>
```

#### Propiedad `<iconSound>`

La propiedad `<iconSonido>` permite cargar un archivo de imagen para la línea de menú de opciones de sonido.

```xml
<iconSound>./camino/hacia/fichero/imagen.png</iconSound>
```

#### Propiedad `<iconNetwork>`

La propiedad `<iconRed>` permite cargar un archivo de imagen para la línea de menú de opciones de red.

```xml
<iconNetwork>./camino/hacia/fichero/imagen.png</iconNetwork>
```

#### Propiedad `<iconScraper>`

La propiedad `<iconScraper>` permite cargar un archivo de imagen para la línea de menú del scraper.

```xml
<iconScraper>./camino/hacia/fichero/imagen.png</iconScraper>
```

#### Propiedad `<iconAdvanced>`

La propiedad `<iconAdvanced>` permite cargar un archivo de imagen para la línea de menú de configuración avanzada.

```xml
<iconAdvanced>./camino/hacia/fichero/imagen.png</iconAdvanced>
```

#### Propiedad `<iconQuit>`

La propiedad `<iconQuit>` permite cargar un archivo de imagen para la línea de menú de salida de la interfaz.

```xml
<iconQuit>./camino/hacia/fichero/imagen.png</iconQuit>
```

#### Propiedad `<iconRestart>`

La propiedad `<iconRestart>` permite cargar un archivo de imagen para que la línea de menú se reinicie.

```xml
<iconRestart>./camino/hacia/fichero/imagen.png</iconRestart>
```

#### Propiedad `<iconShutdown>`

La propiedad `<iconShutdown>` permite cargar un archivo de imagen para la línea de menú de apagado.

```xml
<iconShutdown>./camino/hacia/fichero/imagen.png</iconShutdown>
```

#### Propiedad `<iconFastShutdown>`

La propiedad `<iconFastShutdown>` permite cargar un archivo de imagen para la línea de menú de apagado rápido.

```xml
<iconFastShutdown>./camino/hacia/fichero/imagen.png</iconFastShutdown>
```

#### Propiedad `<iconLicense>`

La propiedad `<iconLicense>` le permite cargar un archivo de imagen para la línea de menú bajo licencia de código abierto.

```xml
<iconLicense>./camino/hacia/fichero/imagen.png</iconLicense>
```

#### Propiedad `<iconRecalboxRGBDual>`

La propiedad `<iconRecalboxRGBDual>` permite cargar un archivo de imagen para la línea de menú para crt.

```xml
<iconRecalboxRGBDual>./camino/hacia/fichero/imagen.png</iconRecalboxRGBDual>
```

#### Propiedad `<iconTate>`

La propiedad `<iconTate>` permite cargar un archivo de imagen para la línea de menú de los parámetros tate.

```xml
<iconTate>./camino/hacia/fichero/imagen.png</iconTate>
```

#### Propiedad `<iconArcade>` ![](/tutorials/newicon-24.png)

La propiedad `<iconArcade>` permite cargar un archivo de imagen para la línea de menú de los parámetros arcade.

```xml
<iconArcade>./camino/hacia/fichero/imagen.png</iconArcade>
```

#### Propiedad `<iconDownload>`

La propiedad `<iconDownload>` permite cargar un archivo de imagen para la línea de menú de descarga de contenidos.

```xml
<iconDownload>./camino/hacia/fichero/imagen.png</iconDownload>
```

## Objet `<menuSwitch>`

Los objetos `<menuSwitch>` se utilizan para personalizar los botones `on` / `off`. Este objeto puede tener varios Atributos :

#### Atributo `name`

- Obligatorio: si

El atributo `name` se utiliza para indicar sobre qué elemento de texto debe actuar el contenido. Puede tener uno o varios de estos valores al mismo tiempo.

Lista de valores que se pueden utilizar en los menús (`<view name="menu">`) :

- `menuwitch`: botones de activación/desactivación del menú.

```xml
<menuSwitch name="menuswitch">
  ...
</menuSwitch>
```

El objeto `<menuSwitch>` puede contener otras propiedades:

#### Propiedad `<pathOn>`

La propiedad `<pathOn>` permite cargar un archivo de imagen para reemplazar el botón `ON`.

```xml
<pathOn>/camino/hacia/fichero/imagen.png</pathOn>
```

#### Propiedad `<pathOff>`

La propiedad `<pathOff>` le permite cargar un archivo de imagen para reemplazar el botón `OFF`.

```xml
<pathOff>/camino/hacia/fichero/imagen.png</pathOff>
```

## Objet `<menuSlider>`

Los objetos `<menuSlider>` se utilizan para personalizar el botón redondo de una barra horizontal, como en las opciones de sonido. Este objeto puede tener varios Atributos :

#### Atributo `name`

- Obligatorio: si

El Atributo `name` se utiliza para indicar sobre qué elemento de texto debe actuar el contenido. Puede tener uno o varios de estos valores al mismo tiempo.

Lista de valores que se pueden utilizar en los menús (`<view name="menu">`) :

- `menuslider`: botón redondo en una barra horizontal.

```xml
<menuSlider name="menuslider">
  ...
</menuSlider>
```

El objeto `<menuSlider>` puede contener otros Propiedads.

#### Propiedad `<path>`

La propiedad `<path>` le permite cargar un archivo de imagen para reemplazar el botón que desea mover.

```xml
<path>/camino/hacia/fichero/imagen.png</path>
```

## Objeto `<menuButton>`

Los objetos `<menuButton>` se utilizan para personalizar los botones utilizados para validar o cancelar confirmaciones de acciones. Este objeto puede tener varios Atributos :

#### Atributo `name`

- Obligatorio: si

El atributo `name` se utiliza para indicar sobre qué elemento de texto debe actuar el contenido. Puede tener uno o varios de estos valores al mismo tiempo.

Lista de valores que se pueden utilizar en los menús (`<view name="menu">`) :

- menubutton: botones de menú.

```xml
<menuButton name="menubutton">
  ...
</menuButton>
```

El objeto `<menuButton>` puede contener otras propiedades:

#### Propiedad `<path>`

La propiedad `<path>` permite cargar un archivo de imagen para rellenar el fondo de los botones no seleccionados.

```xml
<path>/camino/hacia/fichero/imagen.png</path>
```

#### Propiedad `<filledPath>`

La propiedad `<filledPath>` se puede utilizar para cargar un archivo de imagen para rellenar el fondo del botón seleccionado/hovered.

```xml
<filledPath>/camino/hacia/fichero/imagen.png</filledPath>
```

## Objeto `<menuText>`

Los objetos `<menuText>` se utilizan para personalizar el texto básico de los menús. Este objeto puede tener varios Atributos :

#### Atributo `name`

- Obligatorio: si

El atributo `name` se utiliza para indicar sobre qué elemento de texto debe actuar el contenido. Puede tener uno o varios de estos valores al mismo tiempo.

Lista de valores que se pueden utilizar en los menús (`<view name="menu">`):

- `menutext`: texto del menú.
- `menutitle`: título del menú.
- `menufooter`: barra en la parte inferior de los menús (como en la versión actual).

```xml
<menuText name="menutext">
  ...
</menuText>
```

El objeto `<menuText>` puede contener otros Propiedads.

#### Propiedad `<fontPath>`

La propiedad `<fontPath>` se utiliza para especificar una ruta para cargar la fuente deseada desde el objeto padre.

```xml
<fontPath>./../arts/Exo2-BoldCondensed.otf</fontPath>
```

#### Propiedad `<fontSize>`

La propiedad `<fontSize>` se utiliza para indicar el tamaño de fuente que se aplicará al objeto padre. El valor se expresa como un porcentaje de la altura de la pantalla (un valor de `0.1` indica un tamaño de fuente del 10% de la altura de la pantalla).

```xml
<fontSize>0.025</fontSize>
```

#### Propiedad `<color>`

La propiedad `<color>` se utiliza para indicar el color del texto del objeto padre. Los colores pueden estar en formato RRVVBB o RRVVBBAA (AA para transparencia, 00 = transparente y ff = opaco).

```xml
<color>666666</color>
```

#### Propiedad `<separatorColor>`

La propiedad `<separatorColor>` se utiliza para definir el color de separación del texto. Los colores pueden estar en formato RRVVBB o RRVVBBAA (AA para transparencia, 00 = transparente y ff = opaco).

```xml
<separatorColor>4368ffff</separatorColor>
```

#### Propiedad `<selectedColor>`

La propiedad `<selectedColor>` se utiliza para definir el color del texto resaltado. Los colores pueden estar en formato RRVVBB o RRVVBBAA (AA para transparencia, 00 = transparente y ff = opaco).

```xml
<selectedColor>4368ffff</selectedColor>
```

#### Propiedad `<selectorColor>`

La propiedad `<selectorColor>` permite utilizar un color específico para resaltar elementos del objeto padre. Los colores pueden estar en formato RRVVBB o RRVVBBAA (AA para transparencia, 00 = transparente y ff = opaco).

```xml
<selectorColor>4368ffff</selectorColor>
```

## Objet `<menuTextSmall>`

Los objetos `<menuTextSmall>` permiten personalizar un texto más pequeño que el texto básico de los menús. Este objeto puede tener varios atributos:

#### Atributo `name`

- Obligatorio: si

El atributo `name` se utiliza para indicar sobre qué elemento de texto debe actuar el contenido. Puede tener uno o varios de estos valores al mismo tiempo.

Lista de valores que se pueden utilizar en los menús (`<view name="menu">`):

- `menutextsmall`: texto del menú.

```xml
<menuTextSmall name="menutextsmall">
  ...
</menuTextSmall>
```

El objeto `<menuTextSmall>` puede contener otras propiedades:

#### Propiedad `<fontPath>`

La propiedad `<fontPath>` se utiliza para especificar una ruta para cargar la fuente deseada desde el objeto padre.

```xml
<fontPath>./../arts/Exo2-BoldCondensed.otf</fontPath>
```

#### Propiedad `<fontSize>`

La propiedad `<fontSize>` se utiliza para indicar el tamaño de fuente que se aplicará al objeto padre. El valor se expresa como un porcentaje de la altura de la pantalla (un valor de `0.1` indica un tamaño de fuente del 10% de la altura de la pantalla).

```xml
<fontSize>0.025</fontSize>
```

#### Propiedad `<color>`

La propiedad `<color>` se utiliza para indicar el color del texto del objeto padre. Los colores pueden estar en formato RRVVBB o RRVVBBAA (AA para transparencia, 00 = transparente y ff = opaco).

```xml
<color>666666</color>
```

#### Propiedad `<selectedColor>`

La propiedad `<selectedColor>` se utiliza para definir el color del texto resaltado. Los colores pueden estar en formato RRVVBB o RRVVBBAA (AA para transparencia, 00 = transparente y ff = opaco).

```xml
<selectedColor>4368ffff</selectedColor>
```

#### Propiedad `<selectorColor>`

La propiedad `<selectorColor>` permite utilizar un color específico para resaltar elementos del objeto padre.

```xml
<selectorColor>4368ffff</selectorColor>
```

## Objeto `<menuSize>`

Los objetos `<menuSize>` se utilizan para personalizar el tamaño de los elementos de los menús.Este objeto puede tener varios atributos:

#### Atributo `name`

- Obligatorio: si

El atributo `name` se utiliza para indicar sobre qué elemento de texto debe actuar el contenido.Puede tener uno o varios de estos valores al mismo tiempo.

Lista de valores que se pueden utilizar en los menús (`<view name="menu">`) :

- `menusize`: tamaño de los elementos del menú.

```xml
<menuSize name="menusize">
  ...
</menuSize>
```

El objeto `<menuSize>` puede contener otros Propiedads.

#### Propiedad `<height>`

La propiedad `<height>` se utiliza para aplicar una altura de resaltado a los elementos del objeto padre.

```xml
<height>0.5</height>
```