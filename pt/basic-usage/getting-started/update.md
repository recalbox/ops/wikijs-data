---
title: Atualização
description: 
published: true
date: 2021-07-06T23:27:37.131Z
tags: 
editor: markdown
dateCreated: 2021-07-06T23:27:37.131Z
---

## Processo de atualização

Aqui estão as **opções de atualização** do Recalbox.  Esperamos que este resumo ajude e lhe dê as respostas que você está procurando.

## Dependendo de sua situação...

### É possível atualizar do Recalbox x.x para o Recalbox 7?

_**Não**, não é possível atualizar para a nova versão do Recalbox 7. É necessária uma instalação limpa. Veja abaixo as instruções._

### E se eu usar um tema diferente do padrão?

* Certifique-se de estar em dia com os sistemas, arquivos de configuração para seu tema.
* Contate o autor do tema ou tópico do tema para ver se uma nova versão atualizada está disponível.
* Use um tema personalizado em 720p no Raspberry Pi. Com 1080p seu Raspberry Pi vai sofrer.
* Não fornecemos suporte para temas personalizados não-oficiais do Recalbox. Os criadores dos temas são os únicos a dar suporte para seus temas.
* Por favor, use o tema padrão do Recalbox para verificar seus bugs antes de postar seu problema no fórum.

### E se eu usar OVERLAYS?

Se você criou seus próprios *Overlays*, o RetroArch acrescentou uma nova relação de aspecto que desloca alguns *Overlays*:

* Teste 22 ou 23 nesta linha nas configurações de sobreposições:

```ini
aspect_ratio_index = 21
```

### Arcades

O Recalbox atualizou os núcleos dos emuladores Arcade : MAME & FinalBurn Neo e acrescentou outros emuladores.

Por este fato, você tem que adaptar seus romsets:

* Os arquivos dat globais estão agora nas seguintes pastas:

  * /recalbox/share/bios/fba
  * /recalbox/share/bios/fbneo
  * /recalbox/share/bios/mame
  * /recalbox/share/bios/mame2000
  * /recalbox/share/bios/mame2003
  * /recalbox/share/bios/mame2003-plus
  * /recalbox/share/bios/mame2010
  * /recalbox/share/bios/mame2015

* Para fazer dat *parent-only* (somente roms "pai"), como um dat de neogeo *parent-only*, etc... basta usar este [tutorial datutil](./../../tutorials/utilities/dat-management/datutil) para criá-los.

Aqui estão as versões utilizadas:

* advanceMame: romset mame 0.106 (somente para usuários experientes).
* FBA: romset 0.2.97.44
* FBNeo: romset 1.0.0.01
* Mame 2000: romset mame 0.37b5
* Mame 2003: romset mame 0.78
* Mame 2003-plus: romset mame 0.78-0.188.
* Mame 2010: romset mame 0.139
* Mame 2015: romset mame 0.160
* Mame : romset mame 0.230
* Veja a documentação do Arcade.

#### Seu Raspberry Pi em uma *case*

* Certifique-se de que você tenha uma boa ventilação.
* Um bom cabo de energia.
* Se você tiver algum problema, retire seu Raspberry Pi da *case* e teste o Recalbox sem a *case*.

### Nossos conselhos:

* Tenha um backup da sua pasta SHARE em seu PC ou em um disco rígido externo.
* Use uma mídia removível (*pen drive* ou disco rígido externo), você entenderá rapidamente que uma reinstalação é mais rápida.
* Use um *scraper* externo
* Tenha sempre 2 cartões micro SD, 8GB é suficiente para o sistema (um para a versão estável, outro para seus testes, ou testes beta) - mais um bom motivo para ter suas roms em uma mídia removível.
* Instale dissipadores de calor e/ou ventiladores, especialmente:
  * Se você fizer *overclock* em seu Raspberry Pi.
  * Se o seu Recalbox estiver em uma *case*.

## Começar a atualização

>A **atualização do Recalbox** pode ser acessada pressionando `START` > `ATUALIZAÇÕES`.
{.is-info}

* Configure seu wifi ou conecte um cabo de rede ao seu Recalbox.
* Selecione `ATUALIZAÇÕES`.
* Então `INICIAR ATUALIZAÇÃO`. 

O sistema será reinicializado automaticamente após a atualização ser baixada.