---
title: Fonction de recherche
description: 
published: true
date: 2023-07-15T08:23:20.809Z
tags: recherche
editor: markdown
dateCreated: 2021-05-21T07:54:02.351Z
---

## Présentation

Longtemps réclamé par nos chers utilisateurs, là voila enfin !

_**Fouillez rapidement votre collection de jeu en tapant quelques lettres...**_

C'est une recherche en temps réel, la liste se rafraîchi au fur et à mesure de la frappe. Vous pourrez chercher dans les noms, les fichiers, les descriptions, ou partout en même temps.

Bien entendu, nous ne pouvions pas proposer une telle fonctionnalité sans revoir en profondeur le clavier virtuel.  
Assis devant votre TV, la manette en main, quoi de plus pénible que de se promener sur l'ancien clavier plein écran, où entrer le moindre caractère demandait bien trop de manipulations.

Nous en avons refait un tout neuf, tout beau et bien plus ergonomique : un clavier style arcade, avec une roue de caractères où sélectionner, effacer, déplacer le curseur est un vrai plaisir.


On ne peut pas donner tous les détails mais sachez qu'à-peu-près tous les boutons/dpad/joystick d'une manette complète sont utilisés par le nouveau clavier ! Bien entendu, il est compatible nativement avec un clavier physique. Et pour parfaire le tout, il est en semi-transparence, ce qui permet de voir en temps réel ce qui se passe dessous.

Sans plus attendre, quelques images!

![](/basic-usage/features/search.png)

## Fonctionnement

* En appuyant sur `R1` depuis le menu des consoles, vous pourrez faire apparaître la fonction de recherche.
* Commencer à écrire le nom d'un jeu (Exemple ci-dessus : Super).
* Appréciez le visuel de la liste des jeux commençant par votre mot recherché.
* Il ne vous reste plus qu'à sélectionner le jeu auquel vous voulez jouer.