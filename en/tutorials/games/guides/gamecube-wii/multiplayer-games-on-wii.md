---
title: Multiplayer games on Wii
description: 
published: true
date: 2021-08-08T08:33:12.012Z
tags: games, wii, multiplayer
editor: markdown
dateCreated: 2021-08-08T08:33:12.012Z
---

>This is valid for all controllers except the Wiimotes which work directly.
{.is-info}

* Open the file [recalbox.conf](./../../../../basic-usage/getting-started/recalboxconf-file).
* Find the code below in this file:

```ini
## Wiimotes
## Real wiimotes must not be paired with recalbox system so that they can work with the wii emulator
## set emulatedwiimotes to 1 to emulate wiimotes with standard pads
wii.emulatedwiimotes=0
```

* Add the following line after these lines:

```ini
wii.configfile=dummy
```

You can now play with several people on Wii with your controllers.