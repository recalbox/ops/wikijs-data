---
title: Libretro EasyRPG
description: 
published: true
date: 2024-07-14T19:20:16.521Z
tags: libretro, easyrpg
editor: markdown
dateCreated: 2023-09-11T06:27:58.549Z
---

**Libretro EasyRPG** es una adaptación del emulador **EasyRPG**.

## ![](/emulators/license.svg) Licencia

El core posee la siguiente licencia [**GPLv3**](https://github.com/EasyRPG/Player/blob/master/COPYING).

## ![](/emulators/compatibility.png) Compatibilidad

 | RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | 
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Funcionalidades

| Funcionalidad | ¿Soportada? |
| :---: | :---: |
| Reinicio | ✔ |
| Capturas de pantalla | ✔ |
| Grabaciones | ✔ |
| Controles | ✔ |

## ![](/emulators/bios.svg) BIOS

>**No necesita bios.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Extensiones de roms soportadas

Las roms deben tener las extensiones siguientes:

* .ini

### Emplazamiento

Disponed vuestras roms como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 easyrpg
┃ ┃ ┃ ┃ ┣ 📁 jeu.rpg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **RPG_RT.ini**

Cada juego tiene su conjunto de ficheros junto con un fichero **RPG_RT.ini**. Cada juego se debe encontrar en su propia carpeta.

### Lista de juegos compatibles

Podéis encontrar la lista de juegos compatibles en [este enlace](https://community.easyrpg.org/t/compatibility-list/283).

## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

>Para conservar las configuraciones personalizadas cuando se produce una actualización del sistema Recalbox, os aconsejamos utilizar las [Sobrecargas de configuración](./../../../advanced-usage/configuration-override).
{.is-info}

### Acceder a las opciones

Podéis configurar las opciones del core de dos formas diferentes.

* Via el Menú RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via el fichero `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Opciones del core

| Opción | Valor de la opción | Variable (para la sobrecarga) | Valores posibles |
| --- | --- | --- | --- |
| Debug menu and walk through walls | `Désactivé` ✅ / `Activé` | `easyrpg_debug_mode` | `disabled` / `enabled` |

## ![](/emulators/external-links.png) Enlaces externos

* **Código fuente utilizado** : [https://github.com/EasyRPG/Player/](https://github.com/EasyRPG/Player/)
* **Documentación Libretro** : [https://docs.libretro.com/library/easyrpg/](https://docs.libretro.com/library/easyrpg/)
* **Projet RPG Maker 2000** : [https://community.easyrpg.org/t/rpg-maker-2000-preservation-project/199](https://community.easyrpg.org/t/rpg-maker-2000-preservation-project/199)
* **RPG Maker**: [https://rpgmaker.net/](https://rpgmaker.net/)