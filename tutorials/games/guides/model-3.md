---
title: Model 3
description: 
published: true
date: 2025-03-22T01:11:48.716Z
tags: sega, model3
editor: markdown
dateCreated: 2021-08-07T23:49:46.878Z
---

A few tutorials for the Model 3 will have their place here.

Here are the available tutorials:

[Change country and language in-game](change-country-and-language-ingame)
[FAQ](faq)