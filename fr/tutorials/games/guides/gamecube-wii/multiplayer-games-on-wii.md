---
title: Jouer à plusieurs sur Wii
description: 
published: true
date: 2023-09-20T09:12:19.642Z
tags: wii
editor: markdown
dateCreated: 2021-05-21T08:46:14.952Z
---

>Ceci est valable pour toutes les manettes sauf les Wiimotes qui fonctionnent directement.
{.is-info}

* Ouvrez le fichier [recalbox.conf](./../../../../basic-usage/getting-started/recalboxconf-file).
* Trouvez le code ci-dessous dans ce fichier :

```ini
## Wiimotes
## Real wiimotes must not be paired with recalbox system so that they can work with the wii emulator
## set emulatedwiimotes to 1 to emulate wiimotes with standard pads
wii.emulatedwiimotes=0
```

* Ajoutez à la suite de ces lignes la ligne suivante :

```ini
wii.configfile=dummy
```

Vous pouvez à présent jouer à plusieurs sur Wii avec vos manettes.