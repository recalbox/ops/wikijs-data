---
title: Quake 1
description: 
published: true
date: 2024-07-26T08:30:40.662Z
tags: ports, quake 1
editor: markdown
dateCreated: 2023-09-04T12:46:05.696Z
---

![](/emulators/ports/quake.gif){.align-center}

## Detalles técnicos

* **Creado por :** id Software
* **Año de lanzamiento:** 1996
* **Género:** Shooter en primera persona
* **Motor:** Quake Engine
* **Última versión:** v1.09
* **Sistema operativo:** MS-DOS 5.0 (o superior), Win95/98
* **Procesador:** Pentium mínimo
* **Almacenamiento:** 16 MB en MS-DOS, 24MB en Win95/98 + 2x unidad de CD-Rom (sin = sin pistas de sonido)
* **RAM:** 8 MB MS-DOS o 16MB Win95/98 mínimo
* **Resolución:** 320x200 píxeles mínimo
* **Sonido:** Sound Blaster y AdLib o tarjeta de sonido 100% compatible
* **Desarrollador:** id Software
* **Editor:** GT Interactive
* **Diseñadores:** John Romero, American McGee, Sandy Petersen, Tim Willits
* **Programadores:** John Carmack, Michael Abrash, John Cash
* **Artistas:** Adrian Carmack, Kevin Cloud, Paul Steed
* **Compositores:** Trent Reznor, Nine Inch Nails
* **PEGI** : 16

## Presentación

Quake se desarrolla en un mundo en el que el gobierno, tras varios años de investigación y desarrollo, desarrolla un prototipo que hace posible el teletransporte. Sin embargo, el prototipo es corrompido por un enemigo, de nombre en clave Quake, que lo utiliza para enviar a sus criaturas de pesadilla a la dimensión humana, preparando una posible invasión de la Tierra. El gobierno envía un equipo de combatientes a través del portal para eliminar esta amenaza. El jugador asume el papel de un soldado anónimo, único superviviente de la operación. Desde una base aún segura, puede acceder a cuatro dimensiones controladas por el enemigo a través de portales. Cada dimensión permite al jugador adquirir una de las cuatro runas mágicas de Shub-Niggurath, el lugarteniente de Quake encargado de invadir la Tierra. Reunir las cuatro runas permite acceder a la dimensión donde se esconde el demonio.

## Emuladores

[Libretro TyrQuake](libretro-tyrquake)