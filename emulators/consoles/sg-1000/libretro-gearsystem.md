---
title: Libretro Gearsystem
description: 
published: true
date: 2024-07-24T17:34:38.687Z
tags: libretro, gearsystem, sg-1000
editor: markdown
dateCreated: 2021-08-02T17:54:02.581Z
---

**Libretro Gearsystem** is an open source, multi-platform Sega Mark III / Master System / Game Gear / SG-1000 / Othello Multivision emulator written in C++.

* Highly accurate Z80 kernel, including undocumented opcodes and behaviours such as the R and MEMPTR registers.
* Multi-Mapper support: SEGA, Codemasters, SG-1000 and ROM cartridges only.
* Automatic region detection: NTSC-JAP, NTSC-USA, PAL-EUR. Internal database for ROM detection
* Highly accurate VDP emulation, including synchronisation and support for SMS2 mode only
* Audio emulation using SDL Audio and the Sms_Snd_Emu library
* Support for battery-powered RAM saver
* Record status * Support for VDP emulation using SDL Audio and the Sms_Snd_Emu library
* Support for VDP emulation using SDL Audio and the Sms_Snd_Emu library
* Support for battery-powered RAM saver
* Game Genie and Pro Action Replay cheat support Runs on Windows, Linux, Mac OS X, Raspberry Pi, iOS and as Libretro Core (RetroArch)

The Libretro Core Gearsystem was created by Ignacio Sanchez.

## ![](/emulators/license.svg) License

This core is under [**GPLv3**](https://github.com/drhelius/Gearsystem/blob/master/LICENSE) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Features

| Feature | Supported |
| :---: | :---: |
| Restart | ✔ |
| Screenshots | ✔ |
| Saves | ✔ |
| States | ✔ |
| Rewind | ✔ |
| Core Options | ✔ |
| RetroAchievements | ✔ |
| RetroArch Cheats - Game Genie | ✔ |
| RetroArch Cheats - Pro Acion Replay | ✔ |
| Controls | ✔ |
| Remapping | ✔ |
| Softpatching | ✔ |

## ![](/emulators/bios.svg) BIOS

>**No bios is required.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Supported extensions

Roms must have the extension:

* .bin
* .sg
* .zip
* .7z

This system supports compressed roms in .zip/.7z format. However, be careful, it is only an archive.

The files contained in the .zip/.7z must correspond to the extensions mentioned above.
Moreover, each .zip/.7z file must contain only one compressed rom.

### Location

Put the roms like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 sg1000
┃ ┃ ┃ ┃ ┣ 🗒 **game.zip**

>The roms using **No-Intro** format are highly recommended.
{.is-success}

>For more information about roms, go to [this tutorial](./../../../tutorials/games/generalities/isos-and-roms)!
{.is-info}

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Accessing to options

You can configure various options in two different ways.

* Via RetroArch menu:

┣ 📁 RetroArch Menu
┃ ┣ 📁 Core options
┃ ┃ ┣ 🧩 Name_option

* Via the file `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core options

| Options | Options values | Variables (for configuration override) | Variables values |
| --- | --- | --- | --- |
| System (restart) | `Auto` ✅ / `Master System / Mark III` / `Game Gear` / `SG-1000 / Multivision` | `gearsystem_system` | `Auto` / `Master System / Mark III` / `Game Gear` / `SG-1000 / Multivision` |
| Region (restart) | `Auto` ✅ / `Master System Japan` / `Master System Export` / `Game Gear Japan` / `Game Gear Export` / `Game Gear International` | `gearsystem_region` | `Auto` / `Master System Japan` / `Master System Export` / `Game Gear Japan` / `Game Gear Export` / `Game Gear International` |
| Mapper (restart) | `Auto` ✅ / `ROM` / `SEGA` / `Codemasters` / `Korean` / `MSX` / `Janggun` / `SG-1000` | `gearsystem_mapper` | `Auto` / `ROM` / `SEGA` / `Codemasters` / `Korean` / `MSX` / `Janggun` / `SG-1000` |
| Refresh Rate (restart) | `Auto` ✅ / `NTSC (60 Hz)` / `PAL (50 Hz)` | `gearsystem_timing` | `Auto` / `NTSC (60 Hz)` / `PAL (50 Hz)` |
| Master System BIOS (restart) | `Disabled` ✅ / `Enabled` | `gearsystem_bios_sms` | `Disabled` / `Enabled` |
| Game Gear BIOS (restart) | `Disabled` ✅ / `Enabled` | `gearsystem_bios_gg` | `Disabled` / `Enabled` |
| 3D Glasses | `Both Eyes / OFF` ✅ / `Left Eye` / `Right Eye` | `gearsystem_glasses` | `Both Eyes / OFF` / `Left Eye` / `Right Eye` |
| Allow Up+Down / Left+Right | `Disabled` ✅ / `Enabled` | `gearsystem_up_down_allowed` | `Disabled` / `Enabled` |

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/drhelius/Gearsystem/](https://github.com/drhelius/Gearsystem/)
* **Libretro documentation**: [https://docs.libretro.com/library/gearsystem/](https://docs.libretro.com/library/gearsystem/)