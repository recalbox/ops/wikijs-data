---
title: GPIO controllers
description: Manage a controller connected directly to the GPIOs of the Raspberry Pi. If you plan to build a Bartop, an arcade cabinet or a portable arcade stick, you don't need to invest in a usb controller.
published: true
date: 2024-08-19T18:10:36.674Z
tags: controllers, gpio
editor: markdown
dateCreated: 2021-08-07T14:59:39.902Z
---

Recalbox is compiled with the `mk_arcade_joystick_gpio` driver which allows you to manage a controller connected directly to the GPIOs of the Raspberry Pi. If you plan to build a Bartop, an arcade cabinet or a portable arcade stick, you don't need to invest in a usb controller.

The driver can handle up to 2 controllers each consisting of a 4-way joystick and 9 buttons.

If you are using an RPi1 revision B, refer to the `mk_arcade_joystick` pinout.

>Use of GPIO on Raspberry PI 5 is not compatible.
You must use USB encoders to connect your buttons.
{.is-warning}

## Pinout

Let's take as an example a 7 button panel with this layout:

```text
. ↑      Ⓨ Ⓧ Ⓛ  
←  →     Ⓑ Ⓐ Ⓡ Ⓗ
  ↓  
```

With

```text
Ⓡ = Right trigger = TR  |  Ⓛ = Left trigger = TL  |  Ⓗ = HK = Hotkey
```

On RPI 1B+, RPI2 and RPi3, you have to connect your buttons following this pinout :

![](/tutorials/controllers/gpio/gpio-controllers/mk_joystick_arcade_gpiosb+-hk.png)

>The bottom of the image corresponds to the side of the Raspberry Pi where the USB ports are located.
{.is-info}

You can connect your buttons directly to ground, since the driver enables _**gpio internal pullups**_.

## Configuration

* In the [recalbox.conf](./../../../basic-usage/getting-started/recalboxconf-file) file, enable the GPIO driver by setting `controllers.gpio.enabled` to 1: `controllers.gpio.enabled=1` and you are ready to play!

GPIOs are configured out the box in the interface and the different emulated systems.