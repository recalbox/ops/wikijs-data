---
title: Modificar el idioma de RetroArch
description: 
published: true
date: 2023-09-18T10:19:18.378Z
tags: retroarch, idioma, lenguaje
editor: markdown
dateCreated: 2023-09-18T10:19:18.378Z
---

## Explicaciones

¿Sabéis que es posible modificar el idioma de la interfaz de RetroArch? Tenéis disponible el francés, español, inglés y muchos otros idiomas. A continuación os vamos a explicar cómo hacerlo.

## Puesta en marcha

* Lanzad un juego a través de un core Libretro.
* Una vez el juego en ejecución, acceded a las opciones de RetroArch con los botones `HOTKEY` + `B` (consultad [esta página](./../../basic-usage/getting-started/special-commands/in-game) para saber cómo hacer con vuestro mando).

![](/tutorials/others/change-retroarch-language/ralanguage1.png){.full-width}

* Una vez dentro de la interfaz de RetroArch, haced « Back » + izquierda.

![](/tutorials/others/change-retroarch-language/ralanguage2.png){.full-width}

* Cuando el cursor se encuentra sobre « Main Menu », bajad hasta « Settings » y validad.

![](/tutorials/others/change-retroarch-language/ralanguage3.png){.full-width}

* En la parte de la derecha, abajo del todo seleccionad « User » y validad.

![](/tutorials/others/change-retroarch-language/ralanguage4.png){.full-width}

* En las opciones del usuario, seleccionad « Language » y modificarlo con el idioma que queréis.

![](/tutorials/others/change-retroarch-language/ralanguage5.png){.full-width}

* RetroArch se muestra a partir de este momento en la lengua elegida.

![](/tutorials/others/change-retroarch-language/ralanguage6.png){.full-width}

* Id hacia atrás una vez, subid por el menú y dirigíros dentro de « Configuration ».

![](/tutorials/others/change-retroarch-language/ralanguage7.png){.full-width}

* En la opción « Grabar la configuración al salir », seleccionad « Activar ».

![](/tutorials/others/change-retroarch-language/ralanguage8.png){.full-width}

* Salir completamente del juego y el idioma quedará grabado y será seleccionado en cada lanzamiento de RetroArch.