---
title: Emulator-Kompatibilität
description: 
published: true
date: 2024-06-13T15:31:49.853Z
tags: emulator, kompatibilität
editor: markdown
dateCreated: 2021-06-21T12:02:48.390Z
---

## Version 9.2

Wenn du ein System in deiner Installation nicht finden kannst, bedeutet das, dass es noch nicht integriert wurde.
Habe dann etwas Geduld. Wir arbeiten sicher schon daran... 😃

## Legende

* ✅ Verfügbar
* ❌ Nicht verfügbar
* ❄️ Nicht leistungsfähig genug, um den Core zu betreiben (keine Implementierung möglich)
* 🌗 Geringe Performance bei einigen Spielen auf diesem System
* 🐌 Geringere Performance, aber spielbar
* 🎮 Netplay kompatibel
* 🆙 Übertaktung erforderlich
* 1️⃣ mame2000 (imame4all) - romset 0.37b5
* 2️⃣ Nur an der Buchse HDMI 1
* 3️⃣ Eine GPU mit OpenGL &gt;= 3.0 ist erforderlich
* 4️⃣ Eine GPU mit OpenGL &gt;= 3.1 ist erforderlich
* 5️⃣ Eine GPU mit OpenGL &gt;= 3.3 ist erforderlich

Für 3️⃣, 4️⃣ und 5️⃣, sie können Ihre GPU-Kompatibilität mit diesem SSH-Befehl überprüfen: `DISPLAY=:0.0 glxinfo | grep "OpenGL version string:"`

## Emulatoren

### Arcade

| Emulator | RPI0/RPI1 | RPI3 | RPI4/400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| **Atomiswave** |  |  |  |  |  |  |  |  |
| Libretro Flycast | ❌ ❄️ | ❌ ❄️ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Daphne** |  |  |  |  |  |  |  |  |
| Hypseus | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro Dirksimple | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **FinalBurn Neo** 🎮 |  |  |  |  |  |  |  |  |
| Libretro FBNeo | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| PifBA | ✅ | ✅ | ❌ | ❌ | ❌ | ❌ | ❌ | ❌ |
| **MAME** |  |  |  |  |  |  |  |  |
| AdvanceMame | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ❌ | ✅ |
| Libretro MAME | ❌ ❄️ | ❌ ❄️ | ✅ | ✅ | ❌ | ❌ | ✅ | ❌ |
| Libretro MAME2000 | ✅ | ✅ | ❌ | ❌ | ❌ | ❌ | ❌ | ❌ |
| Libretro MAME2003 🎮 | ✅ 🆙 1️⃣ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro MAME2003-plus 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro MAME2010 | ❌ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro MAME2015 | ❌ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Naomi** |  |  |  |  |  |  |  |  |
| Libretro Flycast | ❌ | ✅ 🐌 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Naomi 2** |  |  |  |  |  |  |  |  |  |
| Libretro Flycast | ❌ | ❌ | ❌ | ✅ | ❌ | ❌ | ✅ | ❌ |
| **Naomi GD-Rom** |  |  |  |  |  |  |  |  |
| Libretro Flycast | ❌ | ✅ 🐌 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Sega Model 3** |  |  |  |  |  |  |  |  |
| Supermodel | ❌ | ❌ | ❌ | ❌ | ❌ | ❌ | ✅ | ❌ |

### Fantasy/Homebrew Konsolen

| Emulator | RPI0/RPI1 | RPI3 | RPI4/400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| **Arduboy** |  |  |  |  |  |  |  |  |
| Libretro Arduous | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **LowRes NX** |  |  |  |  |  |  |  |  |
| Libretro LowResNX | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Lutro** |  |  |  |  |  |  |  |  |
| Libretro Lutro | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Pico-8** |  |  |  |  |  |  |  |  |
| Libretro Retro8 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Tic-80** |  |  |  |  |  |  |  |  |
| Libretro Tic80 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **WASM-4** |  |  |  |  |  |  |  |  |
| Libretro Wasm4 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

### Heimkonsolen

| Emulator | RPI0/RPI1 | RPI3 | RPI4/400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| **3DO** |  |  |  |  |  |  |  |  |
| Libretro Opera | ❌ | ✅ 🆙 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Amiga CD32** |  |  |  |  |  |  |  |  |
| Amiberry | ❌ | ✅ | ✅ | ✅ | ✅ | ✅ | ❌ | ✅ |
| Libretro PUAE | ❌ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro UAE4ARM | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ❌ | ✅ |
| **Amiga CDTV** |  |  |  |  |  |  |  |  |
| Amiberry | ❌ | ✅ | ✅ | ✅ | ✅ | ✅ | ❌ | ✅ |
| Libretro PUAE | ❌ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro UAE4ARM | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ❌ | ✅ |
| **Amstrad GX4000** |  |  |  |  |  |  |  |  |
| Libretro Cap32 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Atari 2600** |  |  |  |  |  |  |  |  |
| Libretro Stella 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro Stella2014 🎮 | ✅ | ❌ | ❌ | ❌ | ❌ | ❌ | ❌ | ❌ |
| **Atari 5200** |  |  |  |  |  |  |  |  |
| Libretro A5200 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro Atari800 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Atari 7800** |  |  |  |  |  |  |  |  |
| Libretro Prosystem | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Atari Jaguar** |  |  |  |  |  |  |  |  |
| Libretro Virtualjaguar | ❌ | ❌ | ✅ | ✅ | ✅ | ❌ | ✅ | ✅ |
| **Colecovision** |  |  |  |  |  |  |  |  |
| Libretro blueMSX | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro FBNeo 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro GearColeco 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Dreamcast** |  |  |  |  |  |  |  |  |
| Reicast | ❌ | ✅ 🐌 🆙 | ❌ | ❌ | ❌ | ❌ | ❌ | ❌ |
| Libretro Flycast | ❌ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Fairchild Channel F** |  |  |  |  |  |  |  |  |
| Libretro FBNeo 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro FreeChaF | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Famicom Disk System** |  |  |  |  |  |  |  |  |
| Libretro FCEUmm 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro FBNeo 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro Nestopia 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro Mesen 🎮 | ❌ | ❌ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **GameCube** |  |  |  |  |  |  |  |  |
| Dolphin 🎮 | ❌ | ❌ | ❌ | ❌ | ❌ | ❌ | ✅ 4️⃣ | ❌ |
| Libretro Dolphin 🎮 | ❌ | ❌ | ❌ | ❌ | ❌ | ❌ | ✅ 4️⃣ | ❌ |
| **Intellivision** |  |  |  |  |  |  |  |  |
| Libretro FreeIntv | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Master System** |  |  |  |  |  |  |  |  |
| Libretro FBNeo 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro Gearsystem | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro GenesisPlusGX 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro PicoDrive 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Megadrive** |  |  |  |  |  |  |  |  |
| Libretro FBNeo 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro GenesisPlusGX 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro GenesisPlusGX WIDE 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro PicoDrive 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Multivision** |  |  |  |  |  |  |  |  |
| Libretro Gearsystem | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **NEC PC-FX** |  |  |  |  |  |  |  |  |
| Libretro Mednafen_PCFX | ❌ | ❌ | ✅ | ✅ | ✅ | ❌ | ✅ | ✅ |
| **Neo-Geo** |  |  |  |  |  |  |  |  |
| FBA2X | ✅ | ✅ | ❌ | ❌ | ❌ | ❌ | ❌ | ❌ |
| Libretro FBNeo 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro Geolith | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro MAME2000 | ✅ | ✅ | ❌ | ❌ | ❌ | ❌ | ❌ | ❌ |
| Libretro MAME2003 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro MAME2003-Plus 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro MAME2010 | ❌ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro MAME2015 | ❌ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Neo-Geo CD** |  |  |  |  |  |  |  |  |
| Libretro FBNeo 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro NeoCD | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **NES** |  |  |  |  |  |  |  |  |
| Libretro FBNeo 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro FCEUmm 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro FCEUNext 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro Nestopia 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro QuickNES 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro Mesen 🎮 | ❌ | ❌ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Nintendo 64** |  |  |  |  |  |  |  |  |
| Libretro Mupen64Plus_Next | ❌ | ✅ | ✅ | ✅ | ❌ | ✅ | ✅ | ✅ |
| Libretro ParaLLEl_N64 | ❌ | ✅ | ✅ | ✅ | ✅ | ❌ | ✅ | ✅ |
| Mupen64Plus GLideN64 | ❌ | ✅ | ❌ | ❌ | ❌ | ❌ | ❌ | ❌ |
| Mupen64Plus GLideN64_20 | ❌ | ❌ | ❌ | ❌ | ❌ | ❌ | ❌ | ❌ |
| Mupen64Plus GLide64MK2 | ❌ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Mupen64Plus N64_GLES2 | ❌ | ✅ | ❌ | ❌ | ❌ | ❌ | ❌ | ❌ |
| Mupen64Plus RICE | ❌ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Mupen64Plus RICE_GLES2 | ❌ | ✅ | ❌ | ❌ | ❌ | ❌ | ❌ | ❌ |
| **Nintendo 64DD** |  |  |  |  |  |  |  |  |
| Libretro ParaLLEl_N64 | ❌ | ❌ | ✅ | ✅ | ✅ | ❌ | ✅ | ✅ |
| Libretro Mupen64Plus_Next | ❌ | ✅ | ✅ | ✅ | ❌ | ✅ | ✅ | ✅ |
| **Odyssey 2 / Videopac** |  |  |  |  |  |  |  |  |
| Libretro O2EM | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **PC Engine** |  |  |  |  |  |  |  |  |
| Libretro FBNeo 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro Mednafen_SuperGrafx 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro Mednafen_PCE_FAST 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **PC Engine CD** |  |  |  |  |  |  |  |  |
| Libretro Mednafen_SuperGrafx 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro Mednafen_PCE_FAST 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Philips CD-i** |  |  |  |  |  |  |  |  |
| Libretro CDI2015 | ❌ | ❌ | ✅ | ✅ | ❌ | ❌ | ✅ | ✅ |
| Libretro same_cdi | ❌ | ❌ | ❌ | ❌ | ❌ | ❌ | ✅ | ❌ |
| **PlayStation 1** |  |  |  |  |  |  |  |  |
| PCSX_ReARMed | ✅ 🐌 | ❌ | ❌ | ❌ | ❌ | ❌ | ❌ | ❌ |
| Libretro Swanstation | ❌ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro PCSX_ReARMed | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro Mednafen_PSX | ❌ | ❌ | ✅ | ✅ | ✅ | ❌ | ✅ | ✅ |
| Libretro Mednafen_PSX_HW | ❌ | ❌ | ❌ | ❌ | ❌ | ❌ | ✅ | ❌ |
| DuckStation | ❌ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ 4️⃣ | ✅ |
| **Playstation 2** |  |  |  |  |  |  |  |  |
| Libretro PCSX2 | ❌ | ❌ | ❌ | ❌ | ❌ | ❌ | ✅ 3️⃣ | ❌ |
| PCSX2 | ❌ | ❌ | ❌ | ❌ | ❌ | ❌ | ✅ 3️⃣ | ❌ |
| **Satellaview** |  |  |  |  |  |  |  |  |
| Libretro Snes9x | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro Mesen_S | ❌ | ❌ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro bsnes | ❌ | ❌ | ✅ | ✅ | ❌ | ❌ | ✅ | ✅ |
| Libretro bsnes hd | ❌ | ❌ | ✅ | ✅ | ❌ | ❌ | ✅ | ✅ |
| **Saturn** |  |  |  |  |  |  |  |  |
| Libretro YabaSanshiro | ❌ | ❌ | ✅ | ✅ | ✅ | ✅ | ✅ 5️⃣ | ✅ |
| Libretro Kronos | ❌ | ❌ | ❌ | ❌ | ✅ | ❌ | ✅ 5️⃣ | ❌ |
| Libretro Mednafen_Saturn | ❌ | ❌ | ❌ | ✅ | ❌ | ❌ | ✅ | ❌ |
| Libretro Yabause | ❌ | ❌ | ❌ | ❌ | ❌ | ❌ | ✅ | ❌ |
| **Sega 32X** |  |  |  |  |  |  |  |  |
| Libretro PicoDrive 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Sega CD** |  |  |  |  |  |  |  |  |
| Libretro GenesisPlusGX 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro PicoDrive 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Sega Pico** |  |  |  |  |  |  |  |  |
| Libretro PicoDrive | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **SG-1000** |  |  |  |  |  |  |  |  |
| Libretro FBNeo 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro Gearsystem | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro GenesisPlusGX 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro PicoDrive 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro blueMSX | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **SuFami Turbo** |  |  |  |  |  |  |  |  |
| Libretro Snes9x | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Super Cassette Vision** |  |  |  |  |  |  |  |  |
| Libretro EmuSCV 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Super Nintendo** |  |  |  |  |  |  |  |  |
| Libretro Mesen_S | ❌ | ❌ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro Snes9x 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro Snex9x2002 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro Snes9x2005 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro Snes9x2010 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro Supafaust 🎮 | ❌ | ✅ | ✅ | ✅ | ✅ | ❌ | ✅ | ✅ |
| Libretro bsnes 🎮 | ❌ | ❌ | ✅ | ✅ | ❌ | ❌ | ✅ | ✅ |
| Libretro bsnes HD 🎮 | ❌ | ❌ | ✅ | ✅ | ❌ | ❌ | ✅ | ✅ |
| PiSNES | ✅ | ❌ | ❌ | ❌ | ❌ | ❌ | ❌ | ❌ |
| **SuperGrafx** |  |  |  |  |  |  |  |  |
| Libretro Mednafen_SuperGrafx 🎮  | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro FBNeo 🎮 | ✅ |  ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Vectrex** |  |  |  |  |  |  |  |  |
| Libretro vecx | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Virtual Boy** |  |  |  |  |  |  |  |  |
| Libretro Mednafen_VB | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Wii** |  |  |  |  |  |  |  |  |
| Dolphin | ❌ | ❌ | ❌ | ❌ | ❌ | ❌ | ✅ 4️⃣ | ❌ |
| Libretro Dolphin 🎮 | ❌ | ❌ | ❌ | ❌ | ❌ | ❌ | ✅ 4️⃣ | ❌ |

### Tragbare Konsolen (Handhelds)

| Emulator | RPI0/RPI1 | RPI3 | RPI4/400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| **Game & Watch** |  |  |  |  |  |  |  |  |
| Libretro gw | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Game Boy / Game Boy Color** |  |  |  |  |  |  |  |  |
| Libretro Gambatte | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro Mesen_S 🎮 | ❌ | ❌ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro SameBoy 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro TGBDual 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro mGBA 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro bsnes 🎮 | ❌ | ❌ | ❌ | ❌ | ❌ | ❌ | ✅ | ❌ |
| **Game Boy Advance** |  |  |  |  |  |  |  |  |
| Libretro gpSP | ✅ 🐌 🆙 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro Meteor | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro mGBA 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Game Gear** |  |  |  |  |  |  |  |  |
| Libretro FBNeo 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro Gearsystem | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro GenesisPlusGX | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro PicoDrive 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Lynx** |  |  |  |  |  |  |  |  |
| Libretro Handy 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro Mednafen_Lynx 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Mega Duck** |  |  |  |  |  |  |  |  |
| Libretro Sameduck | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Neo-Geo Pocket** |  |  |  |  |  |  |  |  |
| Libretro FBNeo 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro Mednafen_NGP 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro RACE 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Neo-Go Pocket Color** |  |  |  |  |  |  |  |  |
| Libretro Mednafen_NGP 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro RACE 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Nintendo DS** |  |  |  |  |  |  |  |  |
| Libretro DeSmuME | ❌ | ❌ | ❌ | ❌ | ❌ | ❌ | ✅ | ❌ |
| Libretro melonDS | ❌ | ❌ | ❌ | ❌ | ❌ | ❌ | ✅ | ❌ |
| **Palm OS** |  |  |  |  |  |  |  |  |
| Libretro Mu | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **PlayStation Portable** |  |  |  |  |  |  |  |  |
| Libretro PPSSPP | ❌ | ❌ | ✅ | ✅ | ❌ | ❌ | ✅ | ✅ |
| PPSSPP | ❌ | ✅ 🐌 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Pocket Challenge V2** |  |  |  |  |  |  |  |  |
| Libretro Mednafen_WSwan | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Pokémon Mini** |  |  |  |  |  |  |  |  |
| Libretro PokéMini | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Wonderswan / Wonderswan Color** |  |  |  |  |  |  |  |  |
| Libretro Mednafen_WSwan | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

### Computer

| Emulator | RPI0/RPI1 | RPI3 | RPI4/400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| **Amiga 600** |  |  |  |  |  |  |  |  |
| Amiberry | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ❌ | ✅ |
| Libretro PUAE | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro UAE4ALL | ✅ | ❌ | ❌ | ❌ | ❌ | ❌ | ❌ | ❌ |
| Libretro UAE4ARM | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ❌ | ✅ |
| **Amiga 1200** |  |  |  |  |  |  |  |  |
| Amiberry | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ❌ | ✅ |
| Libretro PUAE | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro UAE4ARM | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ❌ | ✅ |
| **Amstrad CPC** |  |  |  |  |  |  |  |  |
| Libretro CrocoDS | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro Cap32 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Apple II** |  |  |  |  |  |  |  |  |
| LinApple | ✅ | ✅ | ✅ | ✅ | ✅ | ❌ | ❌ | ✅ |
| GSPlus | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Apple IIGS** |  |  |  |  |  |  |  |  |
| GSPlus | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Atari 800** |  |  |  |  |  |  |  |  |
| Libretro Atari800 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Atari ST** |  |  |  |  |  |  |  |  |
| Hatari | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro Hatari | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **BBC Micro** |  |  |  |  |  |  |  |  |
| BeebEm | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Commodore 64** |  |  |  |  |  |  |  |  |
| Libretro vice_x64 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro vice_x64sc | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro vice_x128 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro vice_xcbm2 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro vice_xpet | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro vice_xplus4 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro vice_xscpu64 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro vice_xvic | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Vice_x128 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Vice_x64 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Vice_x64sc | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Vice_xcmb2 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Vice_xcmb5x0 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Vice_xpet | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Vice_xplus4 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Vice_xscpu64 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Vice_xvic | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **DOS** |  |  |  |  |  |  |  |  |
| DOSBox | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro DOSBox_Pure | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Dragon32/64** |  |  |  |  |  |  |  |  |
| XRoar | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Elektronika BK** |  |  |  |  |  |  |  |  |
| Libretro BK | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Macintosh** |  |  |  |  |  |  |  |  |
| Libretro MinivMac | ❌ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **MSX 1** |  |  |  |  |  |  |  |  |
| Libretro FBNeo 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro blueMSX | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro FMSX | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **MSX 2** |  |  |  |  |  |  |  |  |
| Libretro blueMSX | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro FMSX | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **MSX Turbo R** |  |  |  |  |  |  |  |  |
| Libretro blueMSX | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Oric Atmos** |  |  |  |  |  |  |  |  |
| Oricutron | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **PC-88** |  |  |  |  |  |  |  |  |
| Libretro QUASI88 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **PC-98** |  |  |  |  |  |  |  |  |
| Libretro NP2Kai | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **SAM Coupé** |  |  |  |  |  |  |  |  |
| Libretro Simcoupe | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **SHARP x1** |  |  |  |  |  |  |  |  |
| Libretro Xmil | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **SHARP x68000** |  |  |  |  |  |  |  |  |
| Libretro PX68k | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Spectravideo** |  |  |  |  |  |  |  |  |
| Libretro blueMSX | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Thomson TO + MO** |  |  |  |  |  |  |  |  |
| Libretro Theodore | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **TI-99/4A** |  |  |  |  |  |  |  |  |
| TI-99/Sim | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **TRS-80 Color Computer** |  |  |  |  |  |  |  |  |
| XRoar | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **VG5000** |  |  |  |  |  |  |  |  |
| RB5000 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **VIC-20** |  |  |  |  |  |  |  |  |
| Libretro vice_xvic | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Videopac+** |  |  |  |  |  |  |  |  |
| Libretro O2EM | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **ZX81** |  |  |  |  |  |  |  |  |
| Libretro 81 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **ZX Spectrum** |  |  |  |  |  |  |  |  |
| Libretro FBNeo 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro Fuse | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

### Spiel-Engines

| Emulator | RPI0/RPI1 | RPI3 | RPI4/400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| **EasyRPG** |  |  |  |  |  |  |  |  |
| EasyRPG | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Openbor** |  |  |  |  |  |  |  |  |
| Openbor | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **ScummVM** |  |  |  |  |  |  |  |  |
| ScummVM | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Solarus** |  |  |  |  |  |  |  |  |
| Solarus | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Uzebox** |  |  |  |  |  |  |  |  |
| Libretro Uzem | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Z-Machine** |  |  |  |  |  |  |  |  |
| Frotz | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro Mojozork | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

### Ports

| Emulator | RPI0/RPI1 | RPI3 | RPI4/400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| **2048** |  |  |  |  |  |  |  |  |
| Libretro 2048 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Cave Story** |  |  |  |  |  |  |  |  |
| Libretro NXEngine | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **DinoThawr** |  |  |  |  |  |  |  |  |
| Libretro Dinothawr | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **DOOM** |  |  |  |  |  |  |  |  |
| Libretro PrBoom | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Flashback** |  |  |  |  |  |  |  |  |
| Libretro REminiscence | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Pong** |  |  |  |  |  |  |  |  |
| Libretro Gong | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Minecraft** |  |  |  |  |  |  |  |  |
| Libretro Craft | ❌ | ❌ | ❌ | ❌ | ❌ | ❌ | ✅ | ❌ |
| **MrBoom** |  |  |  |  |  |  |  |  |
| Libretro MrBoom 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Out Run** |  |  |  |  |  |  |  |  |
| Libretro Cannonball | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Prince of Persia** |  |  |  |  |  |  |  |  |
| SDLPoP | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Quake 1** |  |  |  |  |  |  |  |  |
| Libretro TyrQuake | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Quake 2** |  |  |  |  |  |  |  |  |
| Libretro VitaQuake 2 | ❌ | ❌ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Rick Dangerous** |  |  |  |  |  |  |  |  |
| Libretro XRick | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Sigil** |  |  |  |  |  |  |  |  |
| Libretro PrBoom | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Wolfenstein 3D** |  |  |  |  |  |  |  |  |
| Libretro ECWolf | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## Geräte

| Peripheriegerät | RPI0/RPI1 | RPI3 | RPI4/400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| Gamepad : Wiimote und Wiimote Classical Controller Extension | ✅ | ✅ | ❓ | ❓ | ❓ | ✅ | ✅ | ❓ |
| Gamepad : xinmo | ✅ | ✅ | ❓ | ❓ | ✅ | ❓ | ✅ | ✅ |
| Boot animation | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Noyau Linux (kompatible Geräte) | 6.1.77 | 6.1.77 | 6.1.77 | 6.1.77 | 6.1.49 | 4.4.189 | 6.1.32 | 4.19.172 |
| Internes Bluetooth | PI0 einzigartig | ✅ | ✅ | ✅ | N/A | ✅ | ✅ | ✅ |
| Internes Infrarot | N/A | N/A | N/A | N/A | N/A | N/A | N/A | N/A | N/A | N/A |
| Probleme mit hohen Temperaturen | ✅ | ❌ | ❌ | ❌ | ✅ | ❓ | ✅ | ❓ |
| HDMI-CEC (Kodi Fernbedienung) | ✅ | ✅ | ✅ | ✅ | ✅ 2️⃣ | ❓ | N/A | ❓ |

## Andere

| Emulator | RPI0/RPI1 | RPI3 | RPI4/400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| Kodi | ✅ | ✅ | ✅ | ✅ | ✅ | ❌ | ✅ | ❌ |
| Moonlight | ❌ | ❌ | ✅ | ✅ | ❌ | ❌ | ✅ | ❌ |