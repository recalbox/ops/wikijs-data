---
title: Solarus
description: Mecanismo de jogos Action-RPG
published: true
date: 2024-06-02T17:18:33.973Z
tags: solarus, motor de juego
editor: markdown
dateCreated: 2021-10-23T14:22:48.693Z
---

![](/emulators/fantasy/solarus.svg){.align-center}

## Ficha técnica

* **Fabricante:** Solarus
* **Ano de lançamento:** 19 de setembro de 2020

## Apresentação

**Solarus** é um mecanismo de jogos de RPG de ação 2D leve, gratuito e de código aberto que foi projetado especificamente com os RPGs de ação da era 2D de culto em mente, como **The Legend of Zelda: A Link to the Past** e **Secret of Mana** do Super Nintendo ou **Soleil** do Sega Megadrive/Genesis.

## Emuladores

[Solarus](solarus)