---
title: ​AdvanceMAME
description: 
published: true
date: 2024-06-25T16:26:38.568Z
tags: mame, advancemame
editor: markdown
dateCreated: 2021-05-21T08:13:24.392Z
---

**AdvanceMAME** est un émulateur standalone, initialement ajouté pour la configuration pour écrans CRT. Il est basé sur le romset MAME 0.106.

## ![](/emulators/license.svg) Licence

Ce core est sous licence [**GPLv2**](https://github.com/amadvance/advancemame/blob/master/COPYING).

## ![](/emulators/compatibility.png) Compatibilité

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ❌ | ✅ |

## ![](/emulators/features.png) Fonctionnalités

| Fonctionnalité | Supporté |
| :---: | :---: |
| Saves |  |
| States |  |
| Rewind |  |
| Netplay |  |
| RetroAchievements |  |
| RetroArch Cheats |  |
| Native Cheats |  |
| Controllers |  |
| Multi-Mouse |  |
| Rumble |  |
| Sensors |  |
| Camera |  |
| Location |  |
| Subsystem |  |

## ![](/emulators/bios.svg) BIOS

Les Romsets BIOS ne sont pas nécessaires lorsque vous utilisez des Romsets d'arcade "Full Non-Merged". Pour les roms "Split" et "Non-Merged", placez le BIOS dans le même répertoire que la Romset du jeu.

>**Remarque** :  
>
>Placer les bios dans le répertoire : `/recalbox/share/roms/mame/`   
>ou dans son sous-répertoire si vous utiliser plusieurs cores de Mame : `/recalbox/share/roms/mame/AdvanceMame/`
Les jeux et leurs bios doivent être dans le même répertoire ou sous-répertoire.
{.is-warning}

## ![](/emulators/roms.png) Roms

* Basé sur le romset : MAME 0.106 (Mai 2006)
* Fichier dat : vous trouverez le fichier dat dans votre recalbox dans le répertoire `/recalbox/share/arcade/advancemame/advancemame.dat`.

### Emplacement

Placez les roms comme ceci : 

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 mame
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.zip**

Vous pouvez aussi opter pour le sous-répertoire déjà présent `advancemame` (utile si vous voulez avoir un set mame pour un autre core). Ceci permettra de lancer le bon émulateur automatiquement.

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 mame
┃ ┃ ┃ ┃ ┣ 📁 advancemame
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **jeu.zip**

## ![](/emulators/advanced-configuration.png) Configuration avancée de l'émulateur

>Pour plus d'information sur les roms, rendez-vous sur [ce tutoriel](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/external-links.png) Liens externes

* **Progettosnaps** : [https://www.progettosnaps.net/](https://www.progettosnaps.net/)
* **Code source utilisé** : [https://github.com/amadvance/advancemame/](https://github.com/amadvance/advancemame)
* **Site officiel** : [http://www.advancemame.it/](http://www.advancemame.it/)