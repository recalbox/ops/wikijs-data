---
title: Wifi country code
description: 
published: true
date: 2021-08-06T15:00:09.820Z
tags: wifi, country, code
editor: markdown
dateCreated: 2021-08-06T08:51:33.355Z
---

You can change the wifi country code in the `recalbox.conf` file to enable more wifi channels for your country.

## Disabled channels by country

Some channels are disabled in some countries:

| Channel | Frequency (Mhz) | North America | Japan | Majority of the world |
| :---: | :---: | :---: | :---: | :---: |
| 1 | 2412 | ✅ | ✅ | ✅ |
| 2 | 2417 | ✅ | ✅ | ✅ |
| 3 | 2422 | ✅ | ✅ | ✅ |
| 4 | 2427 | ✅ | ✅ | ✅ |
| 5 | 2432 | ✅ | ✅ | ✅ |
| 6 | 2437 | ✅ | ✅ | ✅ |
| 7 | 2442 | ✅ | ✅ | ✅ |
| 8 | 2447 | ✅ | ✅ | ✅ |
| 9 | 2452 | ✅ | ✅ | ✅ |
| 10 | 2457 | ✅ | ✅ | ✅ |
| 11 | 2462 | ✅ | ✅ | ✅ |
| 12 | 2467 | ❌ | ✅ | ✅ |
| 13 | 2472 | ❌ | ✅ | ✅ |
| 14 | 2484 | ❌ | ✅  (11b only) | ❌ |

[Source](https://en.wikipedia.org/wiki/List_of_WLAN_channels)

## Configuration

This setting can be changed in the `recalbox.conf` file:

* Open the file [recalbox.conf](./../../../basic-usage/getting-started/recalboxconf-file).
* Find the following lines:

```ini
## Set Wifi region
wifi.region=JP
```

Change the `JP` value in the example to the value of your choice.

>The default value is `JP` because this country is the most permissive for wifi.
{.is-info}

## List of wifi callsigns

Here is a list of wifi callsigns that you may find useful:

| Country | Code |
| :--- | :--- |
| Austria | AT |
| Australia | AU |
| Belgium | BE |
| Brazil | BR |
| Canada | CA |
| Switzerland and Liechtenstein | CH |
| China | CN |
| Cyprus |
| Czech Republic | CZ |
| Germany | DE |
| Denmark | DK |
| Estonia | EE |
| Spain | ES |
| Finland | FI |
| France | FR |
| United Kingdom | GB |
| Greece | GR |
| Hong Kong |
| Hungary | HU |
| Indonesia | ID |
| Ireland | IE |
| Israel | IL |
| India | IN |
| Iceland | IS |
| Italy | IT |
| Japan | JP |
| Republic of Korea | KR |
| Lithuania | LT |
| Luxembourg | LU |
| Latvia | LV |
| Malaysia | MY |
| Netherlands | NL |
| Norway | NO |
| New Zealand | NZ |
| Philippines |
| Poland | PL |
| Portugal | PT |
| Sweden | SE |
| Singapore | SG |
| Slovenia | SI |
| Slovakia | SK |
| Thailand | TH |
| Taiwan | TW |
| United States |
| South Africa | ZA |

## Other callsigns

You can find more country codes [on this page](http://www.arubanetworks.com/techdocs/InstantWenger_Mobile/Advanced/Content/Instant%20User%20Guide%20-%20volumes/Country_Codes_List.htm#regulatory_domain_3737302751_1017918).