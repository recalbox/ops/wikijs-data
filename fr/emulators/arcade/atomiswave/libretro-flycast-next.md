---
title: Libretro Flycast-Next
description: 
published: true
date: 2024-11-14T19:57:55.592Z
tags: libretro, atomiswave, 9.2+, flycast-next
editor: markdown
dateCreated: 2024-07-23T21:49:43.947Z
---

**Libretro Flycast** est un émulateur **Sega Dreamcast multi-plateformes** capable d'émuler le **Sammy Atomiswave.**

## ![](/emulators/license.svg) Licence

Ce core est sous licence [**GPLv2**](https://github.com/libretro/flycast/blob/master/LICENSE).

## ![](/emulators/compatibility.png) Compatibilité

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ❌ | ❌ | ✅ | ✅ | ✅ | ✅  🐌 |✅ | ✅ |

🐌  Basses performances mais jouable

## ![](/emulators/features.png) Fonctionnalités

| Fonctionnalité | Supporté |
| :---: | :---: |
| Redémarrage | ✔ |
| Captures d'écran | ✔ |
| Sauvegardes | ✔ |
| Options du core | ✔ |
| Cheats RetroArch | ✔ |
| Controls | ✔ |
| Remapping | ✔ |
| Vibration | ✔ |
| Disk Control | ✔ |

## ![](/emulators/bios.svg) BIOS

### Liste des bios obligatoires

| Nom de fichier | Description | MD5 | Fourni |
| :---: | :---: | :---: | :---: |
| awbios.zip | Bios « Atomiswave » de Mame | 0ec5ae5b5a5c4959fa8b43fcf8687f7c | ❌ |

### Emplacement

Placez les bios comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 dc
┃ ┃ ┃ ┃ ┣ 🗒 **awbios.zip**

## ![](/emulators/roms.png) Roms

Libretro Flycast se base sur le Romset de **MAME** mais aussi sur des formats **NullDC** pour sa partie Atomiswave.

### Extensions supportées

Les roms doivent avoir les extensions suivantes :

* .zip
* .7z
* .bin/.lst
* .dat

## Romset Mame

Atomiswave se base sur le romset de Mame indiqué [ici](../).

>Vous devez obligatoirement passer votre romset MAME dans ClrMamePro pour avoir un romset Atomiswave fonctionnel.
{.is-info}

>Afin de trier vos roms arcade, le **fichier dat** est disponible dans le dossier :`/recalbox/share/arcade/libretro/`
{.is-info}

### Emplacement

Placez les roms comme ceci : 

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 atomiswave
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.zip**

## Romset NullDC

Ces roms sont compatibles avec Flycast mais **moins fiables** que des roms **issues d'un set MAME.**

>Les Roms **NullDC** sont au format : _`.bin + .lst`_
{.is-info}

### Emplacement

Placez les roms comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 atomiswave
┃ ┃ ┃ ┃ ┣ 📁 **jeu**
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **jeu.bin**
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **jeu.lst**

## ![](/emulators/advanced-configuration.png) Configuration avancée de l'émulateur

>Pour pouvoir conserver vos configurations personnalisées lors d'une mise à jour, nous vous conseillons d'utiliser notre fonctionnalité [Surcharges de configuration](./../../../advanced-usage/configuration-override).
{.is-info}

### Accéder aux options

Vous pouvez configurer diverses options de deux façons différentes.

* Via le Menu RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via le fichier `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Options du core

| Options | Valeur des options | Variable (pour surcharge) | Valeurs des variables |
| --- | --- | --- | --- |
| Région | `Japon` / `États-Unis` ✅ / `Europe` / `Par défaut` | `reicast_region` | `Japan` / `USA` / `Europe` / `Default` |
| Activer le DSP | `Désactivé` / `Activé` ✅ | `reicast_enable_dsp` | `disabled` / `enabled` |
| Autoriser les boutons de service NAOMI | `Désactivé` ✅ / `Activé` | `reicast_allow_service_buttons` | `disabled` / `enabled` |
| Définir les jeux NAOMI en Free Play | `Désactivé` / `Activé` ✅ | `reicast_force_freeplay` | `disabled` / `enabled` |
| Émulation de l'adaptateur haut débit | `Désactivé` ✅ / `Activé` | `reicast_emulate_bba` | `disabled` / `enabled` |
| Activer l'UPnP | `Désactivé` / `Activé` ✅ | `reicast_upnp` | `disabled` / `enabled` |
| Résolution interne | `320x240` / `640x480` ✅ / `800x600` / `960x720` / `1024x768` / `1280x960` / `1440x1080` / `1600x1200` / `1920x1440` / `2560x1920` / `2880x2160` / `3200x2400` / `3840x2880` / `4480x3360` / `5120x3840` / `5760x4320` / `6400x4800` / `7040x5280` / `7680x5760` / `8320x6240` / `8960x6720` / `9600x7200` / `10240x7680` / `10880x8160` / `11520x8640` / `12160x9120` / `12800x9600` | `reicast_internal_resolution` | `320x240` / `640x480` / `800x600` / `960x720` / `1024x768` / `1280x960` / `1440x1080` / `1600x1200` / `1920x1440` / `2560x1920` / `2880x2160` / `3200x2400` / `3840x2880` / `4480x3360` / `5120x3840` / `5760x4320` / `6400x4800` / `7040x5280` / `7680x5760` / `8320x6240` / `8960x6720` / `9600x7200` / `10240x7680` / `10880x8160` / `11520x8640` / `12160x9120` / `12800x9600` |
| Orientation de l'écran | `Horizontale` ✅ / `Verticale` | `reicast_screen_rotation` | `horizontal` / `vertical` |
| Tri alpha | `Par bande (rapide, moins précis)` / `Par triangle (normal)` ✅ / `Par pixel (précis, mais le plus lent)` | `reicast_alpha_sorting` | `per-strip (fast, least accurate)` / `per-triangle (normal)` / `per-pixel (accurate)` |
| Émulation complète du framebuffer | `Désactivé` ✅ / `Activé` | `reicast_emulate_framebuffer` | `disabled` / `enabled` |
| Activer la mémoire tampon du RTT (rendu vers texture) | `Désactivé` ✅ / `Activé` | `reicast_enable_rttb` | `disabled` / `enabled` |
| Mipmapping | `Désactivé` / `Activé` ✅ | `reicast_mipmapping` | `disabled` / `enabled` |
| Effets de brouillard | `Désactivé` / `Activé` ✅ | `reicast_fog` | `disabled` / `enabled` |
| Modificateur du volume | `Désactivé` / `Activé` ✅ | `reicast_volume_modifier_enable` | `disabled` / `enabled` |
| Filtrage anisotrope | `Désactivé` / `2` / `4` ✅ / `8` / `16` | `reicast_anisotropic_filtering` | `off` / `2` / `4` / `8` / `16` |
| Filtrage des textures | `Par défaut` ✅ / `Forcer au plus proche` / `Forcer linéaire` | `reicast_texture_filtering` | `0` / `1` / `2` |
| Retarder l'échange d'images | `Désactivé` ✅ / `Activé` | `reicast_delay_frame_swapping` | `disabled` / `enabled` |
| Détecter les changements de fréquence d'images | `Désactivé` ✅ / `Activé` | `reicast_detect_vsync_swap_interval` | `disabled` / `enabled` |
| Filtre de post-traitement PowerVR2 | `Désactivé` ✅ / `Activé` | `reicast_pvr2_filtering` | `disabled` / `enabled` |
| Agrandissement des textures (xBRZ) | `Désactivé` ✅ / `x2` / `x4` / `x6` | `reicast_texupscale` | `disabled` / `2` / `4` / `6` |
| Facteur maximal du filtre d'agrandissement de textures | `256` / `512` / `1024` | `reicast_` | `256` / `512` / `1024` |
| Interpolation de profondeur native | `Désactivé` ✅ / `Activé` | `reicast_native_depth_interpolation` | `disabled` / `enabled` |
| Rendu sur plusieurs fils d'exécution | `Désactivé` / `Activé` ✅ | `reicast_thraded_rendering` | `disabled` / `enabled` |
| Saut d'images automatique | `Désactivé` ✅ / `Normal` / `Maximal` | `reicast_auto_skip_frame` | `disabled` / `some` / `more` |
| Saut d'images | `Désactivé` ✅ / `1` / `2` / `3` / `4` / `5` / `6` | `reicast_frame_skipping` | `disabled` / `1` / `2` / `3` / `4` / `5` / `6` |
| Cheats d'écran large (Redémarrage requis) | `Désactivé` ✅ / `Activé` | `reicast_widescreen_cheats` | `disabled` / `enabled` |
| Hack d'écran large | `Désactivé` ✅ / `Activé` | `reicast_widescreen_hack` | `disabled` / `enabled` |
| SH4 CPU under/overclock | `100 MHz` / `110 MHz` / `120 MHz` / `130 MHz` / `140 MHz` / `150 MHz` / `160 MHz` / `170 MHz` / `180 MHz` / `190 MHz` / `200 MHz` / `210 MHz` / `220 MHz` / `230 MHz` / `240 MHz` / `250 MHz` / `260 MHz` / `270 MHz` / `280 MHz` / `290 MHz` / `300 MHz` | `reicast_sh4clock` | `100` / `110` / `120` / `130` / `140` / `150` / `160` / `170` / `180` / `190` / `200` / `210` / `220` / `230` / `240` / `250` / `260` / `270` / `280` / `290` / `300`  |
| Charger les textures personnalisées | `Désactivé` ✅ / `Activé` | `reicast_custom_textures` | `disabled` / `enabled` |
| Importer les textures | `Désactivé` ✅ / `Activé` | `reicast_dump_textures` | `disabled` / `enabled` |
| Deadzone du stick analogique | `0%` / `5%` / `10%` / `15%` ✅ / `20%` / `25%` / `30%` | `reicast_analog_stick_deadzone` | `0%` / `5%` / `10%` / `15%` / `20%` / `25%` / `30%` |
| Deadzone des gâchettes | `0%` ✅ / `5%` / `10%` / `15%` / `20%` / `25%` / `30%` | `reicast_trigger_deadzone` | `0%` / `5%` / `10%` / `15%` / `20%` / `25%` / `30%` |
| Gâchettes numériques | `Désactivé` ✅ / `Activé` | `reicast_digital_triggers` | `disabled` / `enabled` |
| Diffuser les sorties numériques | `Désactivé` ✅ / `Activé` | `reicast_network_output` | `disabled` / `enabled` |
| Afficher le viseur du pistolet 1 | `Désactivé` ✅ / `Blanc` / `Rouge` / `Vert` / `Bleu` | `reicast_lightgun1_crosshair` | `disabled` / `White` / `Red` / `Green` / `Blue` |
| Afficher le viseur du pistolet 2 | `Désactivé` ✅ / `Blanc` / `Rouge` / `Vert` / `Bleu` | `reicast_lightgun2_crosshair` | `disabled` / `White` / `Red` / `Green` / `Blue` |
| Afficher le viseur du pistolet 3 | `Désactivé` ✅ / `Blanc` / `Rouge` / `Vert` / `Bleu` | `reicast_lightgun3_crosshair` | `disabled` / `White` / `Red` / `Green` / `Blue` |
| Afficher le viseur du pistolet 4 | `Désactivé` ✅ / `Blanc` / `Rouge` / `Vert` / `Bleu` | `reicast_lightgun4_crosshair` | `disabled` / `White` / `Red` / `Green` / `Blue` |
| VMU Sounds | `Désactivé` ✅ / `Activé` | `reicast_vmu_sound` | `disabled` / `enabled` |
| Affichage de l'écran du VMU 1 | `Désactivé` ✅ / `Activé` | `reicast_vmu1_screen_display` | `disabled` / `enabled` |
| Position de l'écran du VMU 1 | `En haut à gauche` ✅ / `En haut à droite` / `En bas à gauche` / `En bas à droite` | `reicast_vmu1_screen_position` | `Upper Left` / `Upper Right` / `Lower Left` / `Lower Right` |
| Taille de l'écran du VMU 1 | `1x` ✅ / `2x` / `3x` / `4x` / `5x` | `reicast_vmu1_screen_size_mult` | `1x` / `2x` / `3x` / `4x` / `5x` |
| Couleur des pixels allumés de l'écran du VMU 1 | `Par défaut activé` ✅ / `Par défaut désactivé` / `Noire` / `Bleu` / `Bleu clair` / `Vert` / `Cyan` / `Bleu cyan` / `Vert clair` / `Vert cyan` / `Cyan clair` / `Rouge` / `Violette` / `Violet clair` / `Jaune` / `Grise` / `Violet clair (2)` / `Vert clair (2)` / `Vert clair (3)` / `Cyan clair (2)` / `Rouge clair (2)` / `Magenta` / `Violet clair (3)` / `Orange clair` / `Orange` / `Violet clair (4)` / `Jaune clair` / `Jaune clair (2)` / `Blanc` | `reicast_vmu1_pixel_on_color` | `DEFAULT_ON 00` / `DEFAULT_OFF 01` / `BLACK 02` / `BLUE 03` / `LIGHT_BLUE 04` / `GREEN 05` / `CYAN 06` / `CYAN_BLUE 07` / `LIGHT_GREEN 08` / `CYAN_GREEN 09` / `LIGHT_CYAN 10` / `RED 11` / `PURPLE 12` / `LIGHT_PURPLE 13` / `YELLOW 14` / `GREY 15` / `LIGHT_PURPLE_2 16` / `LIGHT_GREEN_2 17` / `LIGHT_GREEN_3 18` / `LIGHT_CYAN_2 19` / `LIGHT_RED_2 20` / `MAGENTA 21` / `LIGHT_PURPLE_3 22` / `LIGHT_ORANGE 23` / `ORANGE 24` / `LIGHT_PURPLE_4 25` / `LIGHT_YELLOW 26` / `LIGHT_YELLOW_2 27` / `WHITE 28` |
| Couleur des pixels éteints de l'écran du VMU 1 | `Par défaut désactivé` ✅ / `Par défaut activé` / `Noire` / `Bleu` / `Bleu clair` / `Vert` / `Cyan` / `Bleu cyan` / `Vert clair` / `Vert cyan` / `Cyan clair` / `Rouge` / `Violette` / `Violet clair` / `Jaune` / `Grise` / `Violet clair (2)` / `Vert clair (2)` / `Vert clair (3)` / `Cyan clair (2)` / `Rouge clair (2)` / `Magenta` / `Violet clair (3)` / `Orange clair` / `Orange` / `Violet clair (4)` / `Jaune clair` / `Jaune clair (2)` / `Blanc` | `reicast_vmu1_pixel_off_color` | `DEFAULT_OFF 00` / `DEFAULT_ON 01` / `BLACK 02` / `BLUE 03` / `LIGHT_BLUE 04` / `GREEN 05` / `CYAN 06` / `CYAN_BLUE 07` / `LIGHT_GREEN 08` / `CYAN_GREEN 09` / `LIGHT_CYAN 10` / `RED 11` / `PURPLE 12` / `LIGHT_PURPLE 13` / `YELLOW 14` / `GREY 15` / `LIGHT_PURPLE_2 16` / `LIGHT_GREEN_2 17` / `LIGHT_GREEN_3 18` / `LIGHT_CYAN_2 19` / `LIGHT_RED_2 20` / `MAGENTA 21` / `LIGHT_PURPLE_3 22` / `LIGHT_ORANGE 23` / `ORANGE 24` / `LIGHT_PURPLE_4 25` / `LIGHT_YELLOW 26` / `LIGHT_YELLOW_2 27` / `WHITE 28` |
| Opacité de l'écran du VMU 1 | `10%` / `20%` / `30%` / `40%` / `50%` / `60%` / `70%` / `80%` / `90%` / `100%` | `reicast_vmu1_screen_opacity` | `10%` / `20%` / `30%` / `40%` / `50%` / `60%` / `70%` / `80%` / `90%` / `100%` |
| Affichage de l'écran du VMU 2 | `Désactivé` ✅ / `Activé` | `reicast_vmu2_screen_display` | `disabled` / `enabled` |
| Position de l'écran du VMU 2 | `En haut à gauche` ✅ / `En haut à droite` / `En bas à gauche` / `En bas à droite` | `reicast_vmu2_screen_position` | `Upper Left` / `Upper Right` / `Lower Left` / `Lower Right` |
| Taille de l'écran du VMU 2 | `1x` ✅ / `2x` / `3x` / `4x` / `5x` | `reicast_vmu2_screen_size_mult` | `1x` / `2x` / `3x` / `4x` / `5x` |
| Couleur des pixels allumés de l'écran du VMU 2 | `Par défaut activé` ✅ / `Par défaut désactivé` / `Noire` / `Bleu` / `Bleu clair` / `Vert` / `Cyan` / `Bleu cyan` / `Vert clair` / `Vert cyan` / `Cyan clair` / `Rouge` / `Violette` / `Violet clair` / `Jaune` / `Grise` / `Violet clair (2)` / `Vert clair (2)` / `Vert clair (3)` / `Cyan clair (2)` / `Rouge clair (2)` / `Magenta` / `Violet clair (3)` / `Orange clair` / `Orange` / `Violet clair (4)` / `Jaune clair` / `Jaune clair (2)` / `Blanc` | `reicast_vmu2_pixel_on_color` | `DEFAULT_ON 00` / `DEFAULT_OFF 01` / `BLACK 02` / `BLUE 03` / `LIGHT_BLUE 04` / `GREEN 05` / `CYAN 06` / `CYAN_BLUE 07` / `LIGHT_GREEN 08` / `CYAN_GREEN 09` / `LIGHT_CYAN 10` / `RED 11` / `PURPLE 12` / `LIGHT_PURPLE 13` / `YELLOW 14` / `GREY 15` / `LIGHT_PURPLE_2 16` / `LIGHT_GREEN_2 17` / `LIGHT_GREEN_3 18` / `LIGHT_CYAN_2 19` / `LIGHT_RED_2 20` / `MAGENTA 21` / `LIGHT_PURPLE_3 22` / `LIGHT_ORANGE 23` / `ORANGE 24` / `LIGHT_PURPLE_4 25` / `LIGHT_YELLOW 26` / `LIGHT_YELLOW_2 27` / `WHITE 28` |
| Couleur des pixels éteints de l'écran du VMU 2 | `Par défaut désactivé` ✅ / `Par défaut activé` / `Noire` / `Bleu` / `Bleu clair` / `Vert` / `Cyan` / `Bleu cyan` / `Vert clair` / `Vert cyan` / `Cyan clair` / `Rouge` / `Violette` / `Violet clair` / `Jaune` / `Grise` / `Violet clair (2)` / `Vert clair (2)` / `Vert clair (3)` / `Cyan clair (2)` / `Rouge clair (2)` / `Magenta` / `Violet clair (3)` / `Orange clair` / `Orange` / `Violet clair (4)` / `Jaune clair` / `Jaune clair (2)` / `Blanc` | `reicast_vmu2_pixel_off_color` | `DEFAULT_OFF 00` / `DEFAULT_ON 01` / `BLACK 02` / `BLUE 03` / `LIGHT_BLUE 04` / `GREEN 05` / `CYAN 06` / `CYAN_BLUE 07` / `LIGHT_GREEN 08` / `CYAN_GREEN 09` / `LIGHT_CYAN 10` / `RED 11` / `PURPLE 12` / `LIGHT_PURPLE 13` / `YELLOW 14` / `GREY 15` / `LIGHT_PURPLE_2 16` / `LIGHT_GREEN_2 17` / `LIGHT_GREEN_3 18` / `LIGHT_CYAN_2 19` / `LIGHT_RED_2 20` / `MAGENTA 21` / `LIGHT_PURPLE_3 22` / `LIGHT_ORANGE 23` / `ORANGE 24` / `LIGHT_PURPLE_4 25` / `LIGHT_YELLOW 26` / `LIGHT_YELLOW_2 27` / `WHITE 28` |
| Opacité de l'écran du VMU 2 | `10%` / `20%` / `30%` / `40%` / `50%` / `60%` / `70%` / `80%` / `90%` / `100%` | `reicast_vmu2_screen_opacity` | `10%` / `20%` / `30%` / `40%` / `50%` / `60%` / `70%` / `80%` / `90%` / `100%` |
| Affichage de l'écran du VMU 3 | `Désactivé` ✅ / `Activé` | `reicast_vmu3_screen_display` | `disabled` / `enabled` |
| Position de l'écran du VMU 3 | `En haut à gauche` ✅ / `En haut à droite` / `En bas à gauche` / `En bas à droite` | `reicast_vmu3_screen_position` | `Upper Left` / `Upper Right` / `Lower Left` / `Lower Right` |
| Taille de l'écran du VMU 3 | `1x` ✅ / `2x` / `3x` / `4x` / `5x` | `reicast_vmu3_screen_size_mult` | `1x` / `2x` / `3x` / `4x` / `5x` |
| Couleur des pixels allumés de l'écran du VMU 3 | `Par défaut activé` ✅ / `Par défaut désactivé` / `Noire` / `Bleu` / `Bleu clair` / `Vert` / `Cyan` / `Bleu cyan` / `Vert clair` / `Vert cyan` / `Cyan clair` / `Rouge` / `Violette` / `Violet clair` / `Jaune` / `Grise` / `Violet clair (2)` / `Vert clair (2)` / `Vert clair (3)` / `Cyan clair (2)` / `Rouge clair (2)` / `Magenta` / `Violet clair (3)` / `Orange clair` / `Orange` / `Violet clair (4)` / `Jaune clair` / `Jaune clair (2)` / `Blanc` | `reicast_vmu3_pixel_on_color` | `DEFAULT_ON 00` / `DEFAULT_OFF 01` / `BLACK 02` / `BLUE 03` / `LIGHT_BLUE 04` / `GREEN 05` / `CYAN 06` / `CYAN_BLUE 07` / `LIGHT_GREEN 08` / `CYAN_GREEN 09` / `LIGHT_CYAN 10` / `RED 11` / `PURPLE 12` / `LIGHT_PURPLE 13` / `YELLOW 14` / `GREY 15` / `LIGHT_PURPLE_2 16` / `LIGHT_GREEN_2 17` / `LIGHT_GREEN_3 18` / `LIGHT_CYAN_2 19` / `LIGHT_RED_2 20` / `MAGENTA 21` / `LIGHT_PURPLE_3 22` / `LIGHT_ORANGE 23` / `ORANGE 24` / `LIGHT_PURPLE_4 25` / `LIGHT_YELLOW 26` / `LIGHT_YELLOW_2 27` / `WHITE 28` |
| Couleur des pixels éteints de l'écran du VMU 3 | `Par défaut désactivé` ✅ / `Par défaut activé` / `Noire` / `Bleu` / `Bleu clair` / `Vert` / `Cyan` / `Bleu cyan` / `Vert clair` / `Vert cyan` / `Cyan clair` / `Rouge` / `Violette` / `Violet clair` / `Jaune` / `Grise` / `Violet clair (2)` / `Vert clair (2)` / `Vert clair (3)` / `Cyan clair (2)` / `Rouge clair (2)` / `Magenta` / `Violet clair (3)` / `Orange clair` / `Orange` / `Violet clair (4)` / `Jaune clair` / `Jaune clair (2)` / `Blanc` | `reicast_vmu3_pixel_off_color` | `DEFAULT_OFF 00` / `DEFAULT_ON 01` / `BLACK 02` / `BLUE 03` / `LIGHT_BLUE 04` / `GREEN 05` / `CYAN 06` / `CYAN_BLUE 07` / `LIGHT_GREEN 08` / `CYAN_GREEN 09` / `LIGHT_CYAN 10` / `RED 11` / `PURPLE 12` / `LIGHT_PURPLE 13` / `YELLOW 14` / `GREY 15` / `LIGHT_PURPLE_2 16` / `LIGHT_GREEN_2 17` / `LIGHT_GREEN_3 18` / `LIGHT_CYAN_2 19` / `LIGHT_RED_2 20` / `MAGENTA 21` / `LIGHT_PURPLE_3 22` / `LIGHT_ORANGE 23` / `ORANGE 24` / `LIGHT_PURPLE_4 25` / `LIGHT_YELLOW 26` / `LIGHT_YELLOW_2 27` / `WHITE 28` |
| Opacité de l'écran du VMU 3 | `10%` / `20%` / `30%` / `40%` / `50%` / `60%` / `70%` / `80%` / `90%` / `100%` | `reicast_vmu3_screen_opacity` | `10%` / `20%` / `30%` / `40%` / `50%` / `60%` / `70%` / `80%` / `90%` / `100%` |
| Affichage de l'écran du VMU 4 | `Désactivé` ✅ / `Activé` | `reicast_vmu4_screen_display` | `disabled` / `enabled` |
| Position de l'écran du VMU 4 | `En haut à gauche` ✅ / `En haut à droite` / `En bas à gauche` / `En bas à droite` | `reicast_vmu4_screen_position` | `Upper Left` / `Upper Right` / `Lower Left` / `Lower Right` |
| Taille de l'écran du VMU 4 | `1x` ✅ / `2x` / `3x` / `4x` / `5x` | `reicast_vmu4_screen_size_mult` | `1x` / `2x` / `3x` / `4x` / `5x` |
| Couleur des pixels allumés de l'écran du VMU 4 | `Par défaut activé` ✅ / `Par défaut désactivé` / `Noire` / `Bleu` / `Bleu clair` / `Vert` / `Cyan` / `Bleu cyan` / `Vert clair` / `Vert cyan` / `Cyan clair` / `Rouge` / `Violette` / `Violet clair` / `Jaune` / `Grise` / `Violet clair (2)` / `Vert clair (2)` / `Vert clair (3)` / `Cyan clair (2)` / `Rouge clair (2)` / `Magenta` / `Violet clair (3)` / `Orange clair` / `Orange` / `Violet clair (4)` / `Jaune clair` / `Jaune clair (2)` / `Blanc` | `reicast_vmu4_pixel_on_color` | `DEFAULT_ON 00` / `DEFAULT_OFF 01` / `BLACK 02` / `BLUE 03` / `LIGHT_BLUE 04` / `GREEN 05` / `CYAN 06` / `CYAN_BLUE 07` / `LIGHT_GREEN 08` / `CYAN_GREEN 09` / `LIGHT_CYAN 10` / `RED 11` / `PURPLE 12` / `LIGHT_PURPLE 13` / `YELLOW 14` / `GREY 15` / `LIGHT_PURPLE_2 16` / `LIGHT_GREEN_2 17` / `LIGHT_GREEN_3 18` / `LIGHT_CYAN_2 19` / `LIGHT_RED_2 20` / `MAGENTA 21` / `LIGHT_PURPLE_3 22` / `LIGHT_ORANGE 23` / `ORANGE 24` / `LIGHT_PURPLE_4 25` / `LIGHT_YELLOW 26` / `LIGHT_YELLOW_2 27` / `WHITE 28` |
| Couleur des pixels éteints de l'écran du VMU 4 | `Par défaut désactivé` ✅ / `Par défaut activé` / `Noire` / `Bleu` / `Bleu clair` / `Vert` / `Cyan` / `Bleu cyan` / `Vert clair` / `Vert cyan` / `Cyan clair` / `Rouge` / `Violette` / `Violet clair` / `Jaune` / `Grise` / `Violet clair (2)` / `Vert clair (2)` / `Vert clair (3)` / `Cyan clair (2)` / `Rouge clair (2)` / `Magenta` / `Violet clair (3)` / `Orange clair` / `Orange` / `Violet clair (4)` / `Jaune clair` / `Jaune clair (2)` / `Blanc` | `reicast_vmu4_pixel_off_color` | `DEFAULT_OFF 00` / `DEFAULT_ON 01` / `BLACK 02` / `BLUE 03` / `LIGHT_BLUE 04` / `GREEN 05` / `CYAN 06` / `CYAN_BLUE 07` / `LIGHT_GREEN 08` / `CYAN_GREEN 09` / `LIGHT_CYAN 10` / `RED 11` / `PURPLE 12` / `LIGHT_PURPLE 13` / `YELLOW 14` / `GREY 15` / `LIGHT_PURPLE_2 16` / `LIGHT_GREEN_2 17` / `LIGHT_GREEN_3 18` / `LIGHT_CYAN_2 19` / `LIGHT_RED_2 20` / `MAGENTA 21` / `LIGHT_PURPLE_3 22` / `LIGHT_ORANGE 23` / `ORANGE 24` / `LIGHT_PURPLE_4 25` / `LIGHT_YELLOW 26` / `LIGHT_YELLOW_2 27` / `WHITE 28` |
| Opacité de l'écran du VMU 4 | `10%` / `20%` / `30%` / `40%` / `50%` / `60%` / `70%` / `80%` / `90%` / `100%` | `reicast_vmu4_screen_opacity` | `10%` / `20%` / `30%` / `40%` / `50%` / `60%` / `70%` / `80%` / `90%` / `100%` |

## ![external-links.png](/emulators/external-links.png) Liens externes

* **Code source utilisé** : [https://github.com/flyinghead/flycast](https://github.com/flyinghead/flycast)