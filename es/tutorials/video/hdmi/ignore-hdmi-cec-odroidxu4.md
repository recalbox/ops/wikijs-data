---
title: Ignorar el HDMI CEC en la Odroid XU4
description: 
published: true
date: 2023-09-14T14:22:26.075Z
tags: hdmi, cec, odroid, xu4, ignorar
editor: markdown
dateCreated: 2023-09-14T14:22:26.075Z
---

Por defecto, el HDMI CEC está habilitado en el Odroid XU4. Esto significa que el sistema se puede apagar automáticamente al apagar el televisor / cambiar la fuente HDMI. Esto no es recomendable para Recalbox ya que puede corromper el sistema.

Existe una solución para desactivar esta función e ignorar los comandos HDMI CEC.

* Montad [la partición de arranque](./../../../tutorials/system/access/remount-partition-with-write-access) en modo lectura-escritura.
* Escribid las siguientes líneas:

```shell
cd /boot/
nano boot.ini
```

* Añadid lo siguiente justo después de `setenv fdtbin exynos5422-odroidxu3.dtb` :

```ini
# --- HDMI CEC Configuration ---
# ------------------------------
setenv cecenable "false" # false or true
# set to true to enable HDMI CEC
```

Añadid lo que sigue just depués de `fatload mmc 0:1 ${fdtbin_addr_r} ${fdtbin}` :

```ini
fdt addr 0x44000000
if test "${cecenable}" = "false"; then fdt rm /cec@101B0000; fi
```

Vuestro fichero `boot.ini` debería parecerse al siguiente:

```ini
ODROIDXU-UBOOT-CONFIG

# U-Boot Parameters (DO NOT MODIFY)
setenv version 3.10
setenv zimage zImage
setenv fdtbin exynos5422-odroidxu3.dtb

# --- HDMI CEC Configuration ---
# ------------------------------
setenv cecenable "false" # false or true
# set to true to enable HDMI CEC

setenv zimage_addr_r 0x40008000
setenv fdtbin_addr_r 0x44000000

setenv fdt_high "0xffffffff"

# Default boot argument
setenv bootrootfs "root=/dev/mmcblk0p2 rootwait ro"
setenv console "console=ttySAC2,115200n8 consoleblank=0 vt.global_cursor_default=0"

setenv bootargs "${bootrootfs} ${console}"

# boot commands
fatload mmc 0:1 ${zimage_addr_r} ${zimage}
fatload mmc 0:1 ${fdtbin_addr_r} ${fdtbin}

fdt addr 0x44000000
if test "${cecenable}" = "false"; then fdt rm /cec@101B0000; fi

bootz ${zimage_addr_r} - ${fdtbin_addr_r}"
```

* Grabad y salid y a continuación reiniciad.