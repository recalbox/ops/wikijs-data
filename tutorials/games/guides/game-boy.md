---
title: Game Boy
description: 
published: true
date: 2021-09-05T12:53:55.598Z
tags: nintendo, game, boy, color, gameboy
editor: markdown
dateCreated: 2021-05-21T08:37:14.618Z
---

Avec les tutoriels disponibles ici, vous allez découvrir comment jouer à 2 sur le même jeu ! Et même sur 2 jeux différents ainsi que 2 systèmes différents !

Voici les tutoriels disponibles :

[Jouer avec deux Game Boy](play-with-two-game-boy)
[Jouer à deux jeux différents en mode GameLink](play-two-players-games-with-gamelink)