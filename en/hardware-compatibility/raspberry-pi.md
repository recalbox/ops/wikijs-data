---
title: Raspberry Pi
description: 
published: true
date: 2021-08-08T18:33:38.107Z
tags: compatibility, pi, raspberry
editor: markdown
dateCreated: 2021-08-05T23:43:44.080Z
---

The Raspberry Pi has an ARM11 700 MHz processor. It includes 1, 2 or 4 USB ports, an RJ45 port and 256 MB of RAM for the original model up to 4 GB on the latest versions. Its BMC VideoCore 4 graphics circuitry can decode full HD Blu-Ray streams (1080p 30 frames per second), emulate old consoles and play relatively recent video games.

## Additional accessories required

* Power supply
* Case
* SD Card

## Raspberry Pi Zero v1.3

![](/compatibility/pi-zero.jpg)

The first computer at $ 5! The ultra-thin and ultra-thin Raspberry Pi Zero is the smallest Raspberry Pi form factor on the market. It is 40% faster than the original Raspberry Pi but is only 65 mm long by 30 mm wide and 5 mm deep. 
The Raspberry Pi Zero supports mini-connectors to save space and the 40-pin GPIO is unpopulated, which offers the flexibility to use only the connections necessary for your project.

### Features

* **CPU**:
  * ARM 11 Single-Core 1 GHz, Broadcom BCM2835 1GHz
* **RAM**:
  * 512MB
* **Storage**:
  * Micro-SD Slot
* **Connectors**:
  * 1 X USB for data (On-The-Go ports)
  * 1 X Mini HDMI
  * 1 X Micro-USB power
  * 1 X CSI (header camera)
  * 40 X Unpopulated pin GPIO connector
* **Dimensions**:
  * 65mm x 30mm x 5mm

## Raspberry Pi Zero W (Wireless)

![](/compatibility/pi0w.png)

The Raspberry Pi Zero W! It is the smallest of the Raspberry Pi family, it also has Wi-Fi and integrated Bluetooth connectivity!
With a processor as powerful as the A + and the B +, the Raspberry Pi Zero W will offer you many possibilities, thanks to its compact design.

>You will need a 2x20 GPIO header to connect most of  expansion cards!
{.is-warning}

### Features

* **CPU**:
  * ARM11, Broadcom BCM2835, Single-core 1GHz
* **GPU**:
  * Intégré
* **RAM**:
  * 512 Mo (DDR2)
* **Connectors**:
  * 1 X micro SD slot
  * 1 X mini HDMI
  * 1 X micro USB OTG
* **Additionals**: 
  * 1 X CSI camera connector 
  * Unpopulated 40-pin GPIO connector
* **Network**: 
  * Bluetooth 4.1, Bluetooth Low Energy (BLE)
  * WIFI 802.11b/g/n Wireless LAN (150 Mbps)
* **Alimentation**:
  * 1 X micro USB
* **Dimensions**:
  * 65mm x 31mm x 5mm

## Raspberry Pi 1 Model A+

![](/compatibility/pi1a.jpg)

The Raspberry Pi 1 Model A+ Is just 56mm long, 12mm thick and uses up to 45% less power than a Raspberry Pi 1 Model B+

### Features

* **CPU**:
  * ARM 700 MHz CPU
* **RAM**:
  * 256MB
* **Connectors**:
  * 1 X Micro SD slot
  * 1 X USB 2.0
  * 40 X GPIO
* **Network**:
  * None
* **Dimensions**:
  * 65x56x12mm

## Raspberry Pi 2 Model B

![](/compatibility/pi2b.jpg)

### Features

* **CPU**:
  * 900MHz Quad-Core ARM BCM2836 Cortex-A7 CPU
* **GPU**:
  * Dual Core VideoCore IV® Multimedia Co-Processor
* **RAM**:
  * 1 GB
* **Connectors**:
  * 1 X CSI Camera Port (Video Input)
  * 40 X GPIO Pins
  * 1 X MicroSD Storage
  * Audio/Display Output: HDMI, DSI Display Port, Component over 3.5mm
  * 4 X USB Ports (USB 2.0)
  * On-board Network: 10/100 Mbit/s Ethernet
* **Power supply**: 
  * 5V MicroUSB

## Raspberry Pi 3 Modèle B 1GB

![](/compatibility/pi3b.png)

Discover the **Raspberry Pi 3 Model B 1GB** with **integrated Wi-Fi** and **Bluetooth** connectivity! 
Based on the **Quad Core Broadcom 2837 ARMv8 64bit** processor going from 900MHz (on Pi2) to 1.2Ghz, the new generation RPi is a little larger than Pi2, faster and more powerful than its predecessors.

### Features

* **CPU**:
  * Broadcom BCM2837 (ARM Cortex-A53 Quad-Core 1.2 GHz)
* **GPU**:
  * ARM v8, Dual-Core _VideoCore IV_
* **RAM**:
  * 1GB
* **Connectors**: 
  * 1 X Jack 3,5mm Female Stéréo
  * 1 x Hdmi
  * 4 x USB 2.0
  * 1 x micro SD (SDHC, SDXC)
  * 1 X RJ45 Female
* **Additionals**: 
  * 1 x CSI (header caméra)
  * 1 x DSI (header écran)
  * 40 x GPIO
* **Network**:
  * WiFi WLAN BCM43143, Wi-Fi N 150 Mbps (IEEE 802.11n)
  * Bluetooth BLE 4.1
* **Power supply**:
  * Up to 2,5A, Micro USB

## Raspberry Pi 3 Modèle B+ 1GB

![](/compatibility/pi3bplus.jpg)

#### The latest generation of Raspberry Pi 3! 

The Raspberry Pi 3 Model B + has a **1.4-bit quad-core processor** clocked at 1.4 GHz, a **2.4 GHz and 5 GHz dual-band wireless network**, a **Bluetooth 4.2 / BLE connection**, a faster Ethernet connection and the possibility of a PoE power via PoE HAT (not included).

The latest Raspberry Pi 3 model B + has a faster 64 GHz 1.4-bit quad core processor, 1 GB of RAM, a faster 802.11 b / g / n / ac dual-band wireless LAN, Bluetooth 4.2 and an Ethernet 300 Mbit/s much faster. 

With the ARMv8 processor, it can run the full range of ARM GNU / Linux distributions, including Snappy Ubuntu Core, as well as the IoT edition of Microsoft Windows 10. 

* Raspberry Pi 3 Model B + (B Plus) with 1.4GHz 64-bit quad-core ARMv8 CPU (BCM2837B0) 

### Features

* **GPU**:
  * Broadcom BCM2837B0 (ARM Cortex-A53 Quad-Core 1.4 GHz) / Arm 11
* **GPU**:
  * Dual Core VideoCore IV® Multimedia Co-Processor
  * Provides Open GL ES 2.0, hardware-accelerated OpenVG, and 1080p30 H.264 high-profile decode
  * Memory: 512MB SDRAM
* **RAM**:
  * 1GB LPDDR2 SDRAM
* **Connectors**:
  *  1 X Jack 3,5mm Femelle Stéréo
  *  4 X USB 2.0
  *  1 X RJ45 Femelle
  * 1 x HDMI
  * 1 x GPIO (40 pin)
  * 1 x micro SD slot (SDHC, SDXC)
  * 1 x Gigabit Ethernet USB 2.0 (300Mbit/s ethernet)
* **Additionnals**:
  * 1 x MIPI CSI (Camera interface)
  * 1 x MIDI DSI (Display interface)
  *  1 X GPIO 40 pins (header)
* **Network**:
  * WIFI 2.4GHz et 5GHz IEEE 802.11.b/g/n/ac WLAN ( 10/100/1000 Mbps)
  * Bluetooth 4.2 HS low-energy (BLE) (CYW43455)
* **Power supply**:
  * Micro USB socket 5V, 2A (PoE compatible requiring PoE HAT not supplied)

## Raspberry Pi 4

![](/compatibility/pi4.png)

With up to 8GB of memory, this Raspberry PI4 is **the most powerful** Pi4 and retains exceptional capabilities over the Pi3 and Pi3 Model B+.

It offers an unprecedented increase in **processor speed, multimedia performance, memory and connectivity** over the previous generation Raspberry Pi 3 Model B+, while maintaining compatibility with previous versions and similar power consumption. In addition, thanks to a 1.3 revision, the Raspberry Pi 4 can support more memory.

For the end user, the Raspberry Pi 4 Model B offers desktop performance comparable to that of entry-level x86 PC systems.

### Features

* **CPU**
  * ARM v8, Broadcom BCM2711, Quad-core Cortex-A72 64bit SoC@ 1,5GHz
* **GPU**:
  * VideoCore VI supporting OpenGL ES 3.0, HEVC 4K decoding at 60 fps
* **RAM**:
  * 1GB to 8GB LPDDR4 RAM
* **Connectors**:
  * 1 x MIPI DSI (DSI display port to connect the Raspberry Pi touch screen)
  * 2 x micro HDMI
  * 2 x USB 2.0
  * 2 x USB 3.0
  * 1 x GPIO (40 pin)
  * 1 x MIPI CSI (CSI camera port to connect the Raspberry Pi camera)
  * 1 x micro SD
  * 1 X Jack 3,5mm Female Stereo
* **Network**:
  * Gigabit Ethernet (RJ45)
  * Bluetooth 5.0
  * Wi-Fi 802.11b/g/n/ac
* **Power supply**:
  * 5V DC via USB-C connector (minimum 3A), 5V DC via GPIO header (minimum 3A), Power over Ethernet (PoE) compatible (requires HAT for PoE)

## Raspberry Pi 400

![](/compatibility/pi400.png)

The Raspberry Pi 400 is based on a Raspberry Pi 4, with a little boost.

On the technical level the Pi 400 is built on the basis of the Raspberry 4, same ports, same graphics capabilities, same memory, same power supply, etc.
The GPIO ports are of course kept and easily accessible on the keyboard side dedicated to the connectors.
The only technical difference is the processor speed, which has been increased from 1.5 Ghz for the Raspberry 4 to 1.8 Ghz for the Raspberry 400.

The shape of the Raspberry Pi 4 has also been completely redesigned to fit this new format, as shown in this image from the foundation.
Another difference is that thanks to its less compact shape and the use of integrated heat sinks (the gray part of the image above), this Raspberry Pi 400 should heat up much less than the Raspberry Pi 4.

### Features

* **CPU**:
  * Broadcom BCM2711 quad-core Cortex-A72 (ARM v8) 64 bits SoC @ 1,8 GHz
* **GPU**:
  * VideoCore VI (OpenGL ES 3.0)
    * H.265 (4Kp60 decoding)
    * H.264 (1080p60 decoding, 1080p30 encoding)
    * OpenGL ES 3.0 graphics
* **RAM**:
  * 4GB LPDDR4 - 3200 DRAM
* **Connectors**:
  * 1 x Gigabit Ethernet
  * 2 × USB 3.0 ports
  * 1 × USB 2.0 port
  * 1 x GPIO 40-pin horizontal GPIO head
  * 2 × micro HDMI ports (supports up to 4Kp60)
  * 1 x MicroSD card slot for operating system and data storage
* **Network**:
  * Dual-band (2,4 GHz et 5,0 GHz)
  * Wireless LAN IEEE 802.11b/g/n/ac
  * Bluetooth 5.0, BLE
* **Power supply**:
  * 5 V DC via USB-C connector (5V - 3A) - Official power supply recommended
* **Température de fonctionnement :**:
  * 0°C to +50°C
* **Keywoard**:
  * Compact keyboard with 78 or 79 keys (depending on the regional variant)
* **Dimensions**:
  * 286 mm × 122 mm × 23 mm (maximum)