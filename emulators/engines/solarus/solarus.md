---
title: Solarus
description: 
published: true
date: 2024-07-24T20:59:48.237Z
tags: solarus
editor: markdown
dateCreated: 2021-07-04T18:16:30.768Z
---

![](/emulators/fantasy/solarusarpg.svg){.align-center}

**Solarus** is a standalone core that allows you to play 2D Action-RPG style homebrew games.

## ![](/emulators/license.svg) License

This core is under [**GPLv3**](https://www.solarus-games.org/fr/about/legal) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Features

| Feature | Supported |
| :---: | :---: |
| Restart | ✔︎ |
| Screenshots | ✔︎ |
| Saves | ✔︎ |
| Core Options | ✔︎ |
| Controls | ✔︎ |
| Remapping | ✔︎ |
| Vibration | ✔︎ |
| Disk Control | ✔︎ |

## ![](/emulators/bios.svg) BIOS

>**No bios is required.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Supported extensions

Roms must have the extension:

* .solarus

### Location

Put your roms like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 solarus
┃ ┃ ┃ ┃ ┣ 🗒 **game.solarus**

### Games list

There is a catalog of compatible games with this engine:
[https://www.solarus-games.org/fr/games](https://www.solarus-games.org/fr/games)

![](/emulators/fantasy/solarusgamelist.png){.align-center}

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>**This core have no option.**
{.is-success}

## ![](/emulators/external-links.png) External links

* **Source code used:** [https://gitlab.com/bkg2k/solarus](https://gitlab.com/bkg2k/solarus)
* **official website:**[https://solarus-games.org/fr/](https://solarus-games.org/fr)
* **Homebrew games:** [https://www.solarus-games.org/fr/games/](https://www.solarus-games.org/fr/games)