---
title: Les Overlays
description: Ajouter des illustrations autour de l'image du jeux.
published: true
date: 2021-11-09T20:58:21.112Z
tags: overlays, bezel, custom, 8.0+
editor: markdown
dateCreated: 2021-06-24T14:49:13.664Z
---

## Présentation

Les **overlays** (aussi appelés *bezels*) sont des illustrations qui viennent "décorer" votre écran pendant le jeu. Ces illustrations sont souvent placés sur les zones noires de votre écran 16/9, non utilisés par le jeu qui s'affiche en 4/3.

**Street of Rage 3** avec l'overlay Megadrive, sur un écran 16/9:
![overlays.png](/basic-usage/features/overlays/overlays.png)


**Street of Rage 3** sans overlay, sur un écran 16/9:
![43.png](/basic-usage/features/overlays/43.png)

**Street of Rage 3** sans overlay, sur un écran 16/9, avec l'image étirée:
![169.png](/basic-usage/features/overlays/169.png)


## Les Overlays Recalbox

Recalbox intègre les **Recalbox Overlays** qui sont affichés par défaut sur les écrans compatibles.

Vous pouvez activer ou désactiver les **overlays** dans le menu **Options des jeux** de EmulationStation:

![overlays-set-option-es.png](/basic-usage/features/overlays/overlays-set-option-es.png)

### Activation par système

Dans `recalbox.conf`, il est possible d'activer ou de désactiver les Recalbox Overlays de façon générale:
```ini
global.recalboxoverlays=1
```

Ou pour un système :
```ini
snes.recalboxoverlays=0
```

## Ajouter vos propres overlays

Il existe différents packs d'overlays qui proposent pour certains, un overlay par jeu!

Lorsque vous avez téléchargé votre pack d'overlays, copiez tous les répertoires qu'il contient dans le répertoire `overlays` de votre partition `SHARE`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 overlays
┃ ┃ ┃ ┣ 📁 megadrive
┃ ┃ ┃ ┃ ┣ 🗒 megadrive.cfg
┃ ┃ ┃ ┃ ┣ 🗒 megadrive_overlay.cfg
┃ ┃ ┃ ┃ ┣ 🗒 megadrive.png
┃ ┃ ┃ ┣ 📁 snes
┃ ┃ ┃ ┃ ┣ 🗒 .....

> Les overlays custom ont la priorité sur les overlays automatiques. 
> {.is-info}