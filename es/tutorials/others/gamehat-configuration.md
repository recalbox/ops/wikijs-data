---
title: Configuración para el GameHat
description: 
published: true
date: 2023-09-18T06:29:38.895Z
tags: gamehat, configuración
editor: markdown
dateCreated: 2023-09-18T06:29:38.895Z
---

## Configuración

Para todos aquellos que quieran usar Recalbox con el GameHat, he aquí cómo hacerlo:

* Flashead la imagen Recalbox en vuestra tarjeta SD.
* Desde un PC/Mac :
  * Conectad la tarjeta SD
  * Añadid esta configuración al fichero `recalbox-user-config.txt` en la raíz de la tarjeta:

```ini
hdmi_force_hotplug=1
avoid_warnings=1
max_usb_current=1
hdmi_group=2
hdmi_mode=1
hdmi_mode=87
hdmi_cvt 640 480 60 6 0 0 0
hdmi_drive=2
display_rotate=0
```

* Conectad un joystick con cable y rellenad los ajustes wifi.
* Cuando estéis conectado a su red, a través de la interfaz web (o SSH)
  * Modificad el fichero `recalbox.conf` como sigue:
    * Cambiad la línea `controllers.gpio.enabled=0` por `controllers.gpio.enabled=1`.
    * Cambiad la línea `controllers.gpio.args=map=1,2` por `controllers.gpio.args=map=5 gpio=5,6,13,19,21,4,26,12,23,20,16,18,-1`.
* Desenchufad el controlador y reiniciad Recalbox.
* Después de reiniciar, pulsad un botón y ¡a configurar!

## Enlaces externos

* Enlace de la wiki de GameHat: [https://www.waveshare.com/wiki/Game\_HAT](https://www.waveshare.com/wiki/Game_HAT) (En inglés)