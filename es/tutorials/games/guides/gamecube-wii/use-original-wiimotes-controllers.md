---
title: Jugar con los verdaderos mandos Wiimotes
description: 
published: true
date: 2023-09-20T09:07:55.098Z
tags: wii, dolphin, bar, wiimotes, mandos
editor: markdown
dateCreated: 2023-09-20T09:07:55.098Z
---

>Para utilizar los Wiimotes con Dolphin, se necesita la barra Dolphin Bar de Mayflash.
{.is-info}

* Enchufad la Dolphin Bar a Recalbox y configurarla en el modo 4.
* Abrid el fichero `/recalbox/share/system/recalbox.conf`
* En la sección "D3 - Dolphin Controllers" buscad la siguiente línea:

```ini
wii.realwiimotes=0
```

* y modificarla por la siguiente:

```ini
wii.realwiimotes=1
```

* Esto es todo, vuestros Wiimotes funcionarán a partir de ahora en Dolphin.