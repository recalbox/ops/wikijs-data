---
title: Recalbox Manager - Interface web
description: 
published: true
date: 2025-03-16T15:47:42.422Z
tags: web, interface
editor: markdown
dateCreated: 2021-05-21T08:40:08.498Z
---

Le Gestionnaire Web Recalbox est une interface web pour accéder à votre Recalbox via un smartphone , une tablette ou votre PC.

## Interface

Lorsque vous êtes sur la page principale, vous pouvez :

* Suivre (monitoring) votre Recalbox (température, utilisation des cores du CPU, etc.).
* Modifier les options qui composent le fichier `recalbox.conf`.
* Lancer le gamepad virtuel (pratique pour configurer en 5 minuntes sa Recalbox pour la 1ère utilisation) - via smartphone ou tablette uniquement.
* Consulter les fichiers logs (journaux) de Recalbox.
* Redémarrer EmulationStation ou le système entier en cas de plantage.
* Prendre, visualiser et enregistrer des captures d'écran.

Il permet par glisser-déposer de :

* Uploader les roms au format `.zip` ou extension pris en charge par les émulateurs (pas les images CD).

Par défaut, le gestionnaire démarre automatiquement. Si vous le souhaitez, vous pouvez le désactiver dans `recalbox.conf` en modifiant la ligne `system.manager.enabled` et de changer la valeur par 0. Mais en faisant cela, en cas de problèmes (Recalbox bloqué), vous ne pourrez absolument pas l'éteindre ou le redémarrer proprement !

Vous avez aussi la possibilité de le désactiver directement dans EmulationStation (`START` > `PARAMÈTRES AVANCÉS` > `GESTIONNAIRE WEB RECALBOX`).

## Pour vous connecter

Il suffit de saisir dans votre navigateur web simplement son nom :

## {.tabset}

### Windows

[http://recalbox/](http://recalbox/)

### macOS

[http://recalbox.local/](http://recalbox.local/)

### Linux

[http://recalbox.local/](http://recalbox.local/)

## Si cela ne fonctionne pas

Vous pouvez utiliser l'adresse IP de votre Recalbox :

* **192.168.1.x**

>La plupart des routeurs sont configurés de tel façon que vous devriez trouver votre Recalbox entre 192.168.1.2 et 192.168.1.254.
>
>Pour connaitre son adresse IP, depuis le menu EmulationStation, appuyez sur `Start` > `OPTIONS RÉSEAU` et regardez l'adresse IP en face de `ADRESSE IP`.
{.is-info}

## Manette virtuelle

Pour y accéder directement, il suffit de saisir l'adresse IP de votre Recalbox avec le port 8080. Par exemple : `http://192.168.0.X:8080` ou `http://recalbox:8080`

Consultez le [manuel d'utilisation](./../../../basic-usage/first-use-and-configuration#configuration-dune-manette) de la manette virtuel.

Pour faire remonter tout bug lié au gestionnaire web, vous pouvez le faire [ici](https://gitlab.com/recalbox/recalbox-manager).