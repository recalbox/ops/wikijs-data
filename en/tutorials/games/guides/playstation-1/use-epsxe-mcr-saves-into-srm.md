---
title: Use ePSXe (.MCR) saves into RetroArch (.SRM)
description: 
published: true
date: 2021-08-08T14:40:28.688Z
tags: ps1, srm, mcr, saves
editor: markdown
dateCreated: 2021-08-08T14:40:28.688Z
---

You can use your backups on ePsxe using the following method:

* Retrieve your backup file from ePSXe (Epsxe/memcards/Your_Backup.mcr).
* Identify the **exact name** of your rom.
* Rename the file `Your_Backup.mcr` to `YourRomName.srm`.
* Move your freshly renamed file to the `/recalbox/share/saves/psx` folder in your Recalbox.