---
title: Activación de la bios
description: 
published: true
date: 2023-09-19T12:58:12.760Z
tags: bios, atari, jaguar
editor: markdown
dateCreated: 2023-09-19T12:58:12.760Z
---

## Introducción

Algunos juegos fallarán con el emulador Jaguar. Para resolver este problema, hay que activar la bios en las opciones del core.

## Utilización

### A través del juego

* Lanzad un juego que se inicie correctamente para Atari Jaguar.
* Id al menú RetroArch pulsando `Hotkey` + `B`.
* Id a `Opciones`.

![](/tutorials/games/guides/atari-jaguar/jaguarenablebios1.png){.full-width}

* Activad la opción `Bios`.

![](/tutorials/games/guides/atari-jaguar/jaguarenablebios2.png){.full-width}

* Cerrad el menú RetroArch (haced « Atràs » 1 fois y luego `Reanudar`) y luego salid completamente del emulador.
* Lanzad el juego que no funcionaba.

### A través de una sobrecarga de configuración

* Cread un fichero llamado `.core.cfg` e insertad el siguiente contenido dentro:

```ini
virtualjaguar_bios=enabled
```

* Situad el fichero dentro de `/recalbox/share/roms/jaguar`.
* Lanzad el juego que no funcionaba.

A disfrutad !