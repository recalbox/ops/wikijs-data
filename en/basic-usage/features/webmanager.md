---
title: Web Manager
description: 
published: true
date: 2021-08-08T19:05:32.006Z
tags: web, manager
editor: markdown
dateCreated: 2021-06-29T09:09:55.117Z
---

## To connect

Simply type in your web browser its name:

## {.tabset}

### Windows

[http://recalbox/](http://recalbox/)

If that doesn't work, you can use [the IP address of your Recalbox](./../../tutorials/network/ip/discover-recalbox-ip).

### Linux and macOS

[http://recalbox.local/](http://recalbox.local/)

If this does not work, you can use [the IP address of your Recalbox](./../../tutorials/network/ip/discover-recalbox-ip).

## Interface

When you are on the main page, you can:

* Edit the `recalbox.conf` file.
* Add/Remove roms.
* View logs.
* Monitor your board.
* Launch the virtual gamepad (usefull to configure in 5min your recalbox for the first use) - via smartphone or tablet only.
* View Recalbox log files.

## Disabling the web manager

By default, the web manager starts automatically. If you want, you can disable it.

* In EmulationStation, press the `START` button.
* Go to `ADVANCED SETTINGS`.
* Set the `RECALBOX WEB MANAGER` option to OFF.

## Virtual Joystick

To access it directly, just enter the IP address of your Recalbox with port 8080.

```text
http://192.168.0.X:8080
```

```text
http://recalbox:8080
```

See the virtual controller user manual [here](./../../basic-usage/first-use-and-configuration#virtual-controllers)

If you find a potential problem or bug, please report it [here](https://github.com/recalbox/recalbox-manager).