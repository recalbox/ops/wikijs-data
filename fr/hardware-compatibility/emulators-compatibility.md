---
title: Compatibilité des émulateurs
description: Vous n'avez pas tel système sur tel board ?
published: true
date: 2024-06-13T15:23:56.533Z
tags: compatibilité, émulateurs
editor: markdown
dateCreated: 2021-05-21T12:31:19.906Z
---

## Version 9.2

Si vous ne trouvez pas le système dans votre installation, cela signifie qu'il n'a pas encore été intégré.
Soyez patient, nous travaillons dessus... 😃

## Légende

* ✅ Disponible
* ❌ Non disponible
* ❄️ Pas assez puissant pour faire tourner le core (pas d'implantation possible)
* 🌗 Basses performances pour certains jeux sur ce système
* 🐌 Basses performances mais jouable
* 🎮 Compatible Netplay
* 🆙 Un overclock est requis
* 1️⃣ mame2000 (imame4all) - romset 0.37b5
* 2️⃣ Uniquement sur la prise HDMI 1
* 3️⃣ Un GPU avec OpenGL &gt;= 3.0 est requis
* 4️⃣ Un GPU avec OpenGL &gt;= 3.1 est requis
* 5️⃣ Un GPU avec OpenGL &gt;= 3.3 est requis

Pour 3️⃣, 4️⃣ et 5️⃣, vous pouvez vérifier la compatibilité de votre GPU avec cette commande SSH : `DISPLAY=:0.0 glxinfo | grep "OpenGL version string:"`.

### Émulateurs

### Arcade

| Émulateur | RPI0/RPI1 | RPI3 | RPI4/400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| **Atomiswave** |  |  |  |  |  |  |  |  |
| Libretro Flycast | ❌ ❄️ | ❌ ❄️ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Daphne** |  |  |  |  |  |  |  |  |
| Hypseus | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro Dirksimple | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **FinalBurn Neo** 🎮 |  |  |  |  |  |  |  |  |
| Libretro FBNeo | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| PifBA | ✅ | ✅ | ❌ | ❌ | ❌ | ❌ | ❌ | ❌ |
| **MAME** |  |  |  |  |  |  |  |  |
| AdvanceMame | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ❌ | ✅ |
| Libretro MAME | ❌ ❄️ | ❌ ❄️ | ✅ | ✅ | ❌ | ❌ | ✅ | ❌ |
| Libretro MAME2000 | ✅ | ✅ | ❌ | ❌ | ❌ | ❌ | ❌ | ❌ |
| Libretro MAME2003 🎮 | ✅ 🆙 1️⃣ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro MAME2003-plus 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro MAME2010 | ❌ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro MAME2015 | ❌ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Naomi** |  |  |  |  |  |  |  |  |
| Libretro Flycast | ❌ | ✅ 🐌 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Naomi 2** |  |  |  |  |  |  |  |  |  |
| Libretro Flycast | ❌ | ❌ | ❌ | ✅ | ❌ | ❌ | ✅ | ❌ |
| **Naomi GD-Rom** |  |  |  |  |  |  |  |  |
| Libretro Flycast | ❌ | ✅ 🐌 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Sega Model 3** |  |  |  |  |  |  |  |  |
| Supermodel | ❌ | ❌ | ❌ | ❌ | ❌ | ❌ | ✅ | ❌ |

### Consoles fantasy

| Émulateur | RPI0/RPI1 | RPI3 | RPI4/400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| **Arduboy** |  |  |  |  |  |  |  |  |
| Libretro Arduous | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **LowRes NX** |  |  |  |  |  |  |  |  |
| Libretro LowResNX | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Lutro** |  |  |  |  |  |  |  |  |
| Libretro Lutro | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Pico-8** |  |  |  |  |  |  |  |  |
| Libretro Retro8 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Tic-80** |  |  |  |  |  |  |  |  |
| Libretro Tic80 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **WASM-4** |  |  |  |  |  |  |  |  |
| Libretro Wasm4 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

### Consoles de salon

| Émulateur | RPI0/RPI1 | RPI3 | RPI4/400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| **3DO** |  |  |  |  |  |  |  |  |
| Libretro Opera | ❌ | ✅ 🆙 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Amiga CD32** |  |  |  |  |  |  |  |  |
| Amiberry | ❌ | ✅ | ✅ | ✅ | ✅ | ✅ | ❌ | ✅ |
| Libretro PUAE | ❌ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro UAE4ARM | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ❌ | ✅ |
| **Amiga CDTV** |  |  |  |  |  |  |  |  |
| Amiberry | ❌ | ✅ | ✅ | ✅ | ✅ | ✅ | ❌ | ✅ |
| Libretro PUAE | ❌ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro UAE4ARM | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ❌ | ✅ |
| **Amstrad GX4000** |  |  |  |  |  |  |  |  |
| Libretro Cap32 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Atari 2600** |  |  |  |  |  |  |  |  |
| Libretro Stella 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro Stella2014 🎮 | ✅ | ❌ | ❌ | ❌ | ❌ | ❌ | ❌ | ❌ |
| **Atari 5200** |  |  |  |  |  |  |  |  |
| Libretro A5200 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro Atari800 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Atari 7800** |  |  |  |  |  |  |  |  |
| Libretro Prosystem | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Atari Jaguar** |  |  |  |  |  |  |  |  |
| Libretro Virtualjaguar | ❌ | ❌ | ✅ | ✅ | ✅ | ❌ | ✅ | ✅ |
| **Colecovision** |  |  |  |  |  |  |  |  |
| Libretro blueMSX | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro FBNeo 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro GearColeco 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Dreamcast** |  |  |  |  |  |  |  |  |
| Reicast | ❌ | ✅ 🐌 🆙 | ❌ | ❌ | ❌ | ❌ | ❌ | ❌ |
| Libretro Flycast | ❌ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Fairchild Channel F** |  |  |  |  |  |  |  |  |
| Libretro FBNeo 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro FreeChaF | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Famicom Disk System** |  |  |  |  |  |  |  |  |
| Libretro FCEUmm 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro FBNeo 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro Nestopia 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro Mesen 🎮 | ❌ | ❌ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **GameCube** |  |  |  |  |  |  |  |  |
| Dolphin 🎮 | ❌ | ❌ | ❌ | ❌ | ❌ | ❌ | ✅ 4️⃣ | ❌ |
| Libretro Dolphin 🎮 | ❌ | ❌ | ❌ | ❌ | ❌ | ❌ | ✅ 4️⃣ | ❌ |
| **Intellivision** |  |  |  |  |  |  |  |  |
| Libretro FreeIntv | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Master System** |  |  |  |  |  |  |  |  |
| Libretro FBNeo 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro Gearsystem | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro GenesisPlusGX 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro PicoDrive 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Megadrive** |  |  |  |  |  |  |  |  |
| Libretro FBNeo 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro GenesisPlusGX 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro GenesisPlusGX WIDE 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro PicoDrive 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Multivision** |  |  |  |  |  |  |  |  |
| Libretro Gearsystem | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **NEC PC-FX** |  |  |  |  |  |  |  |  |
| Libretro Mednafen_PCFX | ❌ | ❌ | ✅ | ✅ | ✅ | ❌ | ✅ | ✅ |
| **Neo-Geo** |  |  |  |  |  |  |  |  |
| FBA2X | ✅ | ✅ | ❌ | ❌ | ❌ | ❌ | ❌ | ❌ |
| Libretro FBNeo 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro Geolith | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro MAME2000 | ✅ | ✅ | ❌ | ❌ | ❌ | ❌ | ❌ | ❌ |
| Libretro MAME2003 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro MAME2003-Plus 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro MAME2010 | ❌ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro MAME2015 | ❌ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Neo-Geo CD** |  |  |  |  |  |  |  |  |
| Libretro FBNeo 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro NeoCD | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **NES** |  |  |  |  |  |  |  |  |
| Libretro FBNeo 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro FCEUmm 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro FCEUNext 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro Nestopia 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro QuickNES 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro Mesen 🎮 | ❌ | ❌ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Nintendo 64** |  |  |  |  |  |  |  |  |
| Libretro Mupen64Plus_Next | ❌ | ✅ | ✅ | ✅ | ❌ | ✅ | ✅ | ✅ |
| Libretro ParaLLEl_N64 | ❌ | ✅ | ✅ | ✅ | ✅ | ❌ | ✅ | ✅ |
| Mupen64Plus GLideN64 | ❌ | ✅ | ❌ | ❌ | ❌ | ❌ | ❌ | ❌ |
| Mupen64Plus GLideN64_20 | ❌ | ❌ | ❌ | ❌ | ❌ | ❌ | ❌ | ❌ |
| Mupen64Plus GLide64MK2 | ❌ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Mupen64Plus N64_GLES2 | ❌ | ✅ | ❌ | ❌ | ❌ | ❌ | ❌ | ❌ |
| Mupen64Plus RICE | ❌ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Mupen64Plus RICE_GLES2 | ❌ | ✅ | ❌ | ❌ | ❌ | ❌ | ❌ | ❌ |
| **Nintendo 64DD** |  |  |  |  |  |  |  |  |
| Libretro ParaLLEl_N64 | ❌ | ❌ | ✅ | ✅ | ✅ | ❌ | ✅ | ✅ |
| Libretro Mupen64Plus_Next | ❌ | ✅ | ✅ | ✅ | ❌ | ✅ | ✅ | ✅ |
| **Odyssey 2 / Videopac** |  |  |  |  |  |  |  |  |
| Libretro O2EM | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **PC Engine** |  |  |  |  |  |  |  |  |
| Libretro FBNeo 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro Mednafen_SuperGrafx 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro Mednafen_PCE_FAST 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **PC Engine CD** |  |  |  |  |  |  |  |  |
| Libretro Mednafen_SuperGrafx 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro Mednafen_PCE_FAST 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Philips CD-i** |  |  |  |  |  |  |  |  |
| Libretro CDI2015 | ❌ | ❌ | ✅ | ✅ | ❌ | ❌ | ✅ | ✅ |
| Libretro same_cdi | ❌ | ❌ | ❌ | ❌ | ❌ | ❌ | ✅ | ❌ |
| **PlayStation 1** |  |  |  |  |  |  |  |  |
| PCSX_ReARMed | ✅ 🐌 | ❌ | ❌ | ❌ | ❌ | ❌ | ❌ | ❌ |
| Libretro Swanstation | ❌ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro PCSX_ReARMed | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro Mednafen_PSX | ❌ | ❌ | ✅ | ✅ | ✅ | ❌ | ✅ | ✅ |
| Libretro Mednafen_PSX_HW | ❌ | ❌ | ❌ | ❌ | ❌ | ❌ | ✅ | ❌ |
| DuckStation | ❌ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ 4️⃣ | ✅ |
| **Playstation 2** |  |  |  |  |  |  |  |  |
| Libretro PCSX2 | ❌ | ❌ | ❌ | ❌ | ❌ | ❌ | ✅ 3️⃣ | ❌ |
| PCSX2 | ❌ | ❌ | ❌ | ❌ | ❌ | ❌ | ✅ 3️⃣ | ❌ |
| **Satellaview** |  |  |  |  |  |  |  |  |
| Libretro Snes9x | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro Mesen_S | ❌ | ❌ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro bsnes | ❌ | ❌ | ✅ | ✅ | ❌ | ❌ | ✅ | ✅ |
| Libretro bsnes hd | ❌ | ❌ | ✅ | ✅ | ❌ | ❌ | ✅ | ✅ |
| **Saturn** |  |  |  |  |  |  |  |  |
| Libretro YabaSanshiro | ❌ | ❌ | ✅ | ✅ | ✅ | ✅ | ✅ 5️⃣ | ✅ |
| Libretro Kronos | ❌ | ❌ | ❌ | ❌ | ✅ | ❌ | ✅ 5️⃣ | ❌ |
| Libretro Mednafen_Saturn | ❌ | ❌ | ❌ | ✅ | ❌ | ❌ | ✅ | ❌ |
| Libretro Yabause | ❌ | ❌ | ❌ | ❌ | ❌ | ❌ | ✅ | ❌ |
| **Sega 32X** |  |  |  |  |  |  |  |  |
| Libretro PicoDrive 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Sega CD** |  |  |  |  |  |  |  |  |
| Libretro GenesisPlusGX 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro PicoDrive 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Sega Pico** |  |  |  |  |  |  |  |  |
| Libretro PicoDrive | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **SG-1000** |  |  |  |  |  |  |  |  |
| Libretro FBNeo 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro Gearsystem | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro GenesisPlusGX 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro PicoDrive 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro blueMSX | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **SuFami Turbo** |  |  |  |  |  |  |  |  |
| Libretro Snes9x | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Super Cassette Vision** |  |  |  |  |  |  |  |  |
| Libretro EmuSCV 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Super Nintendo** |  |  |  |  |  |  |  |  |
| Libretro Mesen_S | ❌ | ❌ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro Snes9x 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro Snex9x2002 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro Snes9x2005 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro Snes9x2010 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro Supafaust 🎮 | ❌ | ✅ | ✅ | ✅ | ✅ | ❌ | ✅ | ✅ |
| Libretro bsnes 🎮 | ❌ | ❌ | ✅ | ✅ | ❌ | ❌ | ✅ | ✅ |
| Libretro bsnes HD 🎮 | ❌ | ❌ | ✅ | ✅ | ❌ | ❌ | ✅ | ✅ |
| PiSNES | ✅ | ❌ | ❌ | ❌ | ❌ | ❌ | ❌ | ❌ |
| **SuperGrafx** |  |  |  |  |  |  |  |  |
| Libretro Mednafen_SuperGrafx 🎮  | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro FBNeo 🎮 | ✅ |  ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Vectrex** |  |  |  |  |  |  |  |  |
| Libretro vecx | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Virtual Boy** |  |  |  |  |  |  |  |  |
| Libretro Mednafen_VB | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Wii** |  |  |  |  |  |  |  |  |
| Dolphin | ❌ | ❌ | ❌ | ❌ | ❌ | ❌ | ✅ 4️⃣ | ❌ |
| Libretro Dolphin 🎮 | ❌ | ❌ | ❌ | ❌ | ❌ | ❌ | ✅ 4️⃣ | ❌ |

### Consoles portables

| Émulateur | RPI0/RPI1 | RPI3 | RPI4/400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| **Game & Watch** |  |  |  |  |  |  |  |  |
| Libretro gw | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Game Boy / Game Boy Color** |  |  |  |  |  |  |  |  |
| Libretro Gambatte | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro Mesen_S 🎮 | ❌ | ❌ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro SameBoy 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro TGBDual 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro mGBA 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro bsnes 🎮 | ❌ | ❌ | ❌ | ❌ | ❌ | ❌ | ✅ | ❌ |
| **Game Boy Advance** |  |  |  |  |  |  |  |  |
| Libretro gpSP | ✅ 🐌 🆙 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro Meteor | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro mGBA 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Game Gear** |  |  |  |  |  |  |  |  |
| Libretro FBNeo 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro Gearsystem | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro GenesisPlusGX | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro PicoDrive 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Lynx** |  |  |  |  |  |  |  |  |
| Libretro Handy 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro Mednafen_Lynx 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Mega Duck** |  |  |  |  |  |  |  |  |
| Libretro Sameduck | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Neo-Geo Pocket** |  |  |  |  |  |  |  |  |
| Libretro FBNeo 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro Mednafen_NGP 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro RACE 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Neo-Go Pocket Color** |  |  |  |  |  |  |  |  |
| Libretro Mednafen_NGP 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro RACE 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Nintendo DS** |  |  |  |  |  |  |  |  |
| Libretro DeSmuME | ❌ | ❌ | ❌ | ❌ | ❌ | ❌ | ✅ | ❌ |
| Libretro melonDS | ❌ | ❌ | ❌ | ❌ | ❌ | ❌ | ✅ | ❌ |
| **Palm OS** |  |  |  |  |  |  |  |  |
| Libretro Mu | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **PlayStation Portable** |  |  |  |  |  |  |  |  |
| Libretro PPSSPP | ❌ | ❌ | ✅ | ✅ | ❌ | ❌ | ✅ | ✅ |
| PPSSPP | ❌ | ✅ 🐌 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Pocket Challenge V2** |  |  |  |  |  |  |  |  |
| Libretro Mednafen_WSwan | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Pokémon Mini** |  |  |  |  |  |  |  |  |
| Libretro PokéMini | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Wonderswan / Wonderswan Color** |  |  |  |  |  |  |  |  |
| Libretro Mednafen_WSwan | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

### Ordinateurs

| Émulateur | RPI0/RPI1 | RPI3 | RPI4/400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| **Amiga 600** |  |  |  |  |  |  |  |  |
| Amiberry | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ❌ | ✅ |
| Libretro PUAE | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro UAE4ALL | ✅ | ❌ | ❌ | ❌ | ❌ | ❌ | ❌ | ❌ |
| Libretro UAE4ARM | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ❌ | ✅ |
| **Amiga 1200** |  |  |  |  |  |  |  |  |
| Amiberry | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ❌ | ✅ |
| Libretro PUAE | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro UAE4ARM | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ❌ | ✅ |
| **Amstrad CPC** |  |  |  |  |  |  |  |  |
| Libretro CrocoDS | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro Cap32 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Apple II** |  |  |  |  |  |  |  |  |
| LinApple | ✅ | ✅ | ✅ | ✅ | ✅ | ❌ | ❌ | ✅ |
| GSPlus | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Apple IIGS** |  |  |  |  |  |  |  |  |
| GSPlus | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Atari 800** |  |  |  |  |  |  |  |  |
| Libretro Atari800 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Atari ST** |  |  |  |  |  |  |  |  |
| Hatari | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro Hatari | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **BBC Micro** |  |  |  |  |  |  |  |  |
| BeebEm | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Commodore 64** |  |  |  |  |  |  |  |  |
| Libretro vice_x64 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro vice_x64sc | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro vice_x128 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro vice_xcbm2 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro vice_xpet | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro vice_xplus4 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro vice_xscpu64 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro vice_xvic | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Vice_x128 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Vice_x64 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Vice_x64sc | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Vice_xcmb2 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Vice_xcmb5x0 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Vice_xpet | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Vice_xplus4 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Vice_xscpu64 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Vice_xvic | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **DOS** |  |  |  |  |  |  |  |  |
| DOSBox | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro DOSBox_Pure | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Dragon32/64** |  |  |  |  |  |  |  |  |
| XRoar | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Elektronika BK** |  |  |  |  |  |  |  |  |
| Libretro BK | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Macintosh** |  |  |  |  |  |  |  |  |
| Libretro MinivMac | ❌ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **MSX 1** |  |  |  |  |  |  |  |  |
| Libretro FBNeo 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro blueMSX | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro FMSX | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **MSX 2** |  |  |  |  |  |  |  |  |
| Libretro blueMSX | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro FMSX | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **MSX Turbo R** |  |  |  |  |  |  |  |  |
| Libretro blueMSX | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Oric Atmos** |  |  |  |  |  |  |  |  |
| Oricutron | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **PC-88** |  |  |  |  |  |  |  |  |
| Libretro QUASI88 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **PC-98** |  |  |  |  |  |  |  |  |
| Libretro NP2Kai | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **SAM Coupé** |  |  |  |  |  |  |  |  |
| Libretro Simcoupe | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **SHARP x1** |  |  |  |  |  |  |  |  |
| Libretro Xmil | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **SHARP x68000** |  |  |  |  |  |  |  |  |
| Libretro PX68k | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Spectravideo** |  |  |  |  |  |  |  |  |
| Libretro blueMSX | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Thomson TO + MO** |  |  |  |  |  |  |  |  |
| Libretro Theodore | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **TI-99/4A** |  |  |  |  |  |  |  |  |
| TI-99/Sim | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **TRS-80 Color Computer** |  |  |  |  |  |  |  |  |
| XRoar | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **VG5000** |  |  |  |  |  |  |  |  |
| RB5000 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **VIC-20** |  |  |  |  |  |  |  |  |
| Libretro vice_xvic | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Videopac+** |  |  |  |  |  |  |  |  |
| Libretro O2EM | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **ZX81** |  |  |  |  |  |  |  |  |
| Libretro 81 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **ZX Spectrum** |  |  |  |  |  |  |  |  |
| Libretro FBNeo 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro Fuse | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

### Moteurs de jeux

| Émulateur | RPI0/RPI1 | RPI3 | RPI4/400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| **EasyRPG** |  |  |  |  |  |  |  |  |
| EasyRPG | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Openbor** |  |  |  |  |  |  |  |  |
| Openbor | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **ScummVM** |  |  |  |  |  |  |  |  |
| ScummVM | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Solarus** |  |  |  |  |  |  |  |  |
| Solarus | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Uzebox** |  |  |  |  |  |  |  |  |
| Libretro Uzem | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Z-Machine** |  |  |  |  |  |  |  |  |
| Frotz | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro Mojozork | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

### Ports

| Émulateur | RPI0/RPI1 | RPI3 | RPI4/400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| **2048** |  |  |  |  |  |  |  |  |
| Libretro 2048 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Cave Story** |  |  |  |  |  |  |  |  |
| Libretro NXEngine | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **DinoThawr** |  |  |  |  |  |  |  |  |
| Libretro Dinothawr | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **DOOM** |  |  |  |  |  |  |  |  |
| Libretro PrBoom | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Flashback** |  |  |  |  |  |  |  |  |
| Libretro REminiscence | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Pong** |  |  |  |  |  |  |  |  |
| Libretro Gong | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Minecraft** |  |  |  |  |  |  |  |  |
| Libretro Craft | ❌ | ❌ | ❌ | ❌ | ❌ | ❌ | ✅ | ❌ |
| **MrBoom** |  |  |  |  |  |  |  |  |
| Libretro MrBoom 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Out Run** |  |  |  |  |  |  |  |  |
| Libretro Cannonball | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Prince of Persia** |  |  |  |  |  |  |  |  |
| SDLPoP | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Quake 1** |  |  |  |  |  |  |  |  |
| Libretro TyrQuake | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Quake 2** |  |  |  |  |  |  |  |  |
| Libretro VitaQuake 2 | ❌ | ❌ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Rick Dangerous** |  |  |  |  |  |  |  |  |
| Libretro XRick | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Sigil** |  |  |  |  |  |  |  |  |
| Libretro PrBoom | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Wolfenstein 3D** |  |  |  |  |  |  |  |  |
| Libretro ECWolf | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## Matériel

| Périphérique | RPI0/RPI1 | RPI3 | RPI4/400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| Gamepad : Wiimote and Wiimote Classical Controller Extension | ✅ | ✅ | ❓ | ❓ | ❓ | ✅ | ✅ | ❓ |
| Gamepad : xinmo | ✅ | ✅ | ❓ | ❓ | ✅ | ❓ | ✅ | ✅ |
| Boot animation | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Noyau Linux (support matériel) | 6.1.77 | 6.1.77 | 6.1.77 | 6.1.77 | 6.1.49 | 4.4.189 | 6.1.32 | 4.19.172 |
| Bluetooth interne | PI0 uniquement | ✅ | ✅ | ✅ | N/A | ✅ | ✅ | ✅ |
| Infrarouge interne | N/A | N/A | | N/A | N/A | N/A | N/A | N/A | N/A |
| Problèmes de températures élevées | ✅ | ❌ | ❌ | ❌ | ✅ | ❓ | ✅ | ❓ |
| HDMI-CEC (télécommande Kodi) | ✅ | ✅ | ✅ | ✅ | ✅ 2️⃣ | ❓ | N/A | ❓ |

## Autres

| Émulateur | RPI0/RPI1 | RPI3 | RPI4/400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| Kodi | ✅ | ✅ | ✅ | ✅ | ✅ | ❌ | ✅ | ❌ |
| Moonlight | ❌ | ❌ | ✅ | ✅ | ❌ | ❌ | ✅ | ❌ |