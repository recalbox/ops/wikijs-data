---
title: Libretro Sameduck
description: 
published: true
date: 2024-07-25T22:32:43.898Z
tags: 8.1+, megaduck
editor: markdown
dateCreated: 2023-09-11T13:31:57.897Z
---



## ![](/emulators/license.svg) Licencia

El core posee la siguiente licencia [**MIT**](https://github.com/LIJI32/SameBoy/blob/master/LICENSE).

## ![](/emulators/compatibility.png) Compatibilidad

 | RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | 
 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Funcionalidades

| Funcionalidad | ¿Soportada? |
| :---: | :---: |
| Reinicio | ✔ |
| Capturas de pantalla | ✔ |
| Grabaciones | ✔ |
| Grabaciones instantáneas | ✔ |
| Rebobinado | ✔ |
| Opciones del core | ✔ |
| Controles | ✔ |
| Remapping | ✔ |
| Vibración | ✔ |
| Subsistemas | ✔ |

## ![](/emulators/bios.svg) BIOS

### Lista de BIOS obligatorias

>**No necesita bios.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Extensiones de roms soportadas

Las roms deben tener las extensiones siguientes:

* .bin

### Emplazamiento

Disponed vuestras roms como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 megaduck
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.bin**

>Se aconsejan las roms del proyecto **No-Intro**.
{.is-success}

>Para más información sobre las roms, consultad [este tutorial](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

>Para conservar las configuraciones personalizadas cuando se produce una actualización del sistema Recalbox, os aconsejamos utilizar las [Sobrecargas de configuración](./../../../advanced-usage/configuration-override).
{.is-info}

### Acceder a las opciones

Podéis configurar las opciones del core de dos formas diferentes.

* Via el Menú RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via el fichero `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Opciones del core

Utilizad este cuadro para 1 sólo jugador:

| Opción | Valor de la opción | Variable (para la sobrecarga) | Valores posibles |
| --- | --- | --- | --- |
| Color correction | `Désactivé` / `correct curves` / `emulate hardware` ✅ / `preserve brightness` / `reduce contrast` | `sameduck_color_correction_mode` | `off` / `correct curves` / `emulate hardware` / `preserve brightness` / `reduce contrast` |
| High-pass filter | `Désactivé` / `accurate` ✅ / `remove dc offset` | `sameduck_high_pass_filter_mode` | `off` / `accurate` / `remove dc offset` |
| Enable rumble | `all games` ✅ / `never` | `sameduck_rumble` | `all games` / `never` |

Utilizad este cuadro para el modo 2 jugadores:

| Opción | Valor de la opción | Variable (para la sobrecarga) | Valores posibles |
| --- | --- | --- | --- |
| Link cable emulation | `Activé` ✅ / `Désactivé` | `sameduck_link` | `enabled` / `disabled` |
| Screen layout | `top-down` ✅ / `left-right` | `sameduck_screen_layout` | `top-down` / `left-right` |
| Audio output | `Mega Duck #1` ✅ / `Mega Duck #2` | `sameduck_audio_output` | `Mega Duck #1` / `Mega Duck #2` |
| High-pass filter for Mega Duck #1 | `Désactivé` / `accurate` ✅ / `more dc offset` | `sameduck_high_pass_filter_mode_1` | `off` / `accurate` / `more dc offset` |
| High-pass filter for Mega Duck #2 | `Désactivé` / `accurate` ✅ / `more dc offset` | `sameduck_high_pass_filter_mode_2` | `off` / `accurate` / `more dc offset` |
| Enable rumble for Mega Duck #1 | `all games` ✅ / `never` | `sameduck_rumble_1` | `all games` / `never` |
| Enable rumble for Mega Duck #2 | `all games` ✅ / `never` | `sameduck_rumble_2` | `all games` / `never` |

## ![](/emulators/external-links.png) Enlaces externos

* **Código fuente utilizado:** [https://github.com/LIJI32/SameBoy/commits/SameDuck](https://github.com/LIJI32/SameBoy/commits/SameDuck)
* **Documentación Libretro** : [https://docs.libretro.com/library/sameduck/](https://docs.libretro.com/library/sameduck/)