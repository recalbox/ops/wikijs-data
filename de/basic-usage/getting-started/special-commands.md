---
title: Sonderbefehle
description: 
published: true
date: 2024-06-02T20:09:34.544Z
tags: 
editor: markdown
dateCreated: 2021-07-01T11:44:08.677Z
---

Um dir die Arbeit an deiner Recalbox zu erleichtern, haben wir einige spezielle Befehle integriert.

* [Im Menü](in-menu) 
* [Im Spiel](in-game)

Du kannst die wichtigsten Sonderbefehle auch als [PDF](pdf-memo) herunterladen, um sie immer griffbereit zu haben!