---
title: Libretro Mednafen_VB
description: 
published: true
date: 2024-07-14T19:14:04.661Z
tags: libretro, mednafen, virtual boy, vb
editor: markdown
dateCreated: 2023-09-08T09:42:14.930Z
---



## ![](/emulators/license.svg) Licencia

El core posee la siguiente licencia [**GPLv2**](https://github.com/libretro/beetle-vb-libretro/blob/master/COPYING).

## ![](/emulators/compatibility.png) Compatibilidad

 | RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | 

## ![](/emulators/features.png) Funcionalidades

| Funcionalidad | ¿Soportada? |
| :---: | :---: |
| Reinicio | ✔ |
| Capturas de pantalla | ✔ |
| Grabaciones | ✔ |
| Grabaciones instantáneas | ✔ |
| Rebobinado | ✔ |
| Netplay | ✔ (sin emulación de câble link) |
| Opciones del core | ✔ |
| RetroAchievements | ✔ |
| Controles | ✔ |
| Remapping | ✔ |
| Crop Overscan | ✔ |

## ![](/emulators/bios.svg) BIOS

>**No necesita bios.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Extensiones de roms soportadas

Las roms deben tener las extensiones siguientes:

* .bin
* .vb
* .zip
* .7z

Este sistema soporta las roms comprimidas en formato .zip/.7z. No obstante, se trata sólo de un archivo.

Los ficheros contenidos en los .zip/.7z deben tener las extensiones citadas previamente.
Además, cada fichero .zip/.7z sólo debe contener un fichero rom al interior.

### Emplazamiento

Disponed vuestras roms como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 virtualboy
┃ ┃ ┃ ┃ ┣ 🗒 **juego.zip**

>Se aconsejan las roms del proyecto **No-Intro**.
{.is-success}

>Para más información sobre las roms, consultad [este tutorial](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

>Para conservar las configuraciones personalizadas cuando se produce una actualización del sistema Recalbox, os aconsejamos utilizar las [Sobrecargas de configuración](./../../../advanced-usage/configuration-override).
{.is-info}

### Acceder a las opciones

Podéis configurar las opciones del core de dos formas diferentes.

* Via el Menú RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via el fichero `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Opciones del core

| Opción | Valor de la opción | Variable (para la sobrecarga) | Valores posibles |
| --- | --- | --- | --- |
| 3D mode | `anaglyph` / `cyberscope` ✅ / `side-by-side` / `vli` / `hli` | `vb_3dmode` | `anaglyph` / `cyberscope` / `side-by-side` / `vli` / `hli` |
| Anaglyph preset | `Désactivé` ✅ / `red & blue` / `red & cyan` / `red & electric cyan` / `green & magenta` / `yellow & blue` | `vb_anaglyph_preset` | `disabled` / `red & blue` / `red & cyan` / `red & electric cyan` / `green & magenta` / `yellow & blue` |
| Palette | `black & red` ✅ / `black & white` / `black & blue` / `black & cyan` / `black & electric cyan` / `black & green` / `black & magenta` / `black & yellow` | `vb_color_mode` | `black & red` / `black & white` / `black & blue` / `black & cyan` / `black & electric cyan` / `black & green` / `black & magenta` / `black & yellow` |
| Right analog to digital | `Désactivé` ✅ / `Activé` / `invert x` / `invert y` / `invert both` | `vb_right_analog_to_digital` | `disabled` / `enabled` / `invert x` / `invert y` / `invert both` |
 | CPU emulation (Restart) | `accurate` ✅ / `fast` | `vb_cpu_emulation` | `accurate` / `fast` |

## ![](/emulators/external-links.png) Enlaces externos
* **Código fuente utilizado** : [https://github.com/libretro/beetle-vb-libretro/](https://github.com/libretro/beetle-vb-libretro/)
* **Documentación Libretro** : [https://docs.libretro.com/library/beetle_vb/](https://docs.libretro.com/library/beetle_vb/)