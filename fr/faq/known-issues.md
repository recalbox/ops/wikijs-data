---
title: Problèmes connus
description: Ici un problème a une solution.
published: true
date: 2023-09-23T14:19:43.043Z
tags: problèmes, connus
editor: markdown
dateCreated: 2023-08-11T19:54:05.255Z
---

Dans cette page nous référençons les cas d'utilisation qui peuvent poser problème, les réponses et solutions apportées par l'équipe de développement. 

## Web Manager: l'éditeur de roms ne fonctionne plus normalement
Le gestionnaire de roms du Web Manager est une ancienne fonctionnalité qui a été remplacée par l'éditeur de roms mis en place directement sur l'interface Recalbox.
Même si le gestionnaire web est toujours disponible, celui-ci n'est plus utilisable en tant que tel (images qui ne s'affichent pas, éditeur qui n'affiche pas les infos d'édition,etc.). 
Il est désormais conseillé d'utiliser l'interface Recalbox pour éditer les roms. 