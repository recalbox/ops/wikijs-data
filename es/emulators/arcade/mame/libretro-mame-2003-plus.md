---
title: Libretro MAME 2003 Plus
description: 
published: true
date: 2024-06-29T17:31:07.512Z
tags: libretro, mame, mame2003+, mame2003plus
editor: markdown
dateCreated: 2023-09-05T12:49:59.932Z
---

**Libretro MAME2003_Plus** (también conocido como MAME 2003+ y mame2003-plus) es un core emulador de sistemas arcade **Libretro** que se centra en el alto rendimiento y la amplia compatibilidad con dispositivos móviles, ordenadores monoplaca, sistemas embebidos y plataformas similares.

Con el fin de aprovechar los menores requisitos de rendimiento y hardware de una arquitectura MAME anterior, MAME 2003-Plus partió de la base de código de MAME 2003, a su vez derivada de MAME 0.78.

Sobre esta base, los contribuidores de MAME 2003-Plus ha migrado varios cientos de juegos adicionales, así como otras características no presentes en la versión inicial de MAME 0.78.

## ![](/emulators/license.svg) Licencia

El core posee la licencia [**MAME no-commercial**](https://github.com/libretro/mame2003-plus-libretro/blob/master/LICENSE.md).

## ![](/emulators/compatibility.png) Compatibilidad

 | RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
 | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Funcionalidades

| Funcionalidad | ¿Soportada? |
| :---: | :---: |
| Grabaciones | ✔ |
| Grabaciones instantáneas | Depende del juego |
| Rebobinado | ✔ |
| Netplay | ✔ |
| Cheats nativos | ✔ |
| Mandos | ✔ |
| Multi-Ratón | ✔ |

## ![](/emulators/bios.svg) BIOS

Las romsets de BIOS no son necesarias si utilizáis las romsets de arcade "Full Non-Merged". Para las romset de tipo "Split" y "Non-Merged", disponed las BIOS necesarias en la misma carpeta que la romset del juego.

>**Aviso** :
>Poned las bios/devices dentro del directorio : `/recalbox/share/roms/mame/`   
>o dentro de una subcarpeta si utilizáis un directorio diferente por core Mame, por ejemplo: `/recalbox/share/roms/mame/Mame2003-Plus/`
Los juegos junto con sus bios deben encontrarse dentro del mismo directorio o subdirectorio.
{.is-warning}

## ![](/emulators/roms.png) Roms

* Basado en el romset: 0.78-0.188 (MAME 0.78 como línea inicial + otros juegos basados en versiones de MAME posteriores)
* Fichero dat :  `/recalbox/share/arcade/libretro/mame2003-plus.dat`.

### Emplazamiento

Disponed vuestras roms como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 mame
┃ ┃ ┃ ┃ ┣ 🗒 **juego.zip**
┃ ┃ ┃ ┃ ┣ 📁 **juego**
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **juego.chd**

También podéis utilizar el subdirectorio `mame2003plus`, si queréis clasificar vuestros fullsets por el core que las hace funcionar. Si utilizáis este subdirectorio vuestros juegos se lanzarán automáticamente bajo el emulador `mame2003plus`.

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 mame
┃ ┃ ┃ ┃ ┣ 📁 mame2003plus
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **juego.zip**
┃ ┃ ┃ ┃ ┃ ┣ 📁 **juego**
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 **juego.chd**

## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

>Para conservar las configuraciones personalizadas cuando se produce una actualización del sistema Recalbox, os aconsejamos utilizar las [Sobrecargas de configuración](./../../../advanced-usage/configuration-override).
{.is-info}

### Acceder a las opciones

Podéis configurar las opciones del core de dos formas diferentes.

* Via el Menú RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via el fichero `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Options du core

| Opción | Valor de la opción | Variable (para la sobrecarga) | Valores posibles |
| --- | --- | --- | --- |
| Skip Disclaimer | `Désactivé` ✅ / `Activé` | `mame2003-plus_skip_disclaimer` | `disabled` / `enabled` |
| Skip Warnings | `Désactivé` ✅ / `Activé` | `mame2003-plus_skip_warnings` | `disabled` / `enabled` |
| Display MAME Menu | `Désactivé` ✅ / `Activé` | `mame2003-plus_display_setup` | `disabled` / `enabled` |
| Legacy Remapping | `Activé` ✅ / `Désactivé` | `mame2003-plus_mame_remapping` | `enabled` / `disabled` |
 | Autosave Hiscore | `default` ✅ / `recursively` / `Désactivé` | `mame2003-plus_autosave_hiscore` | `default` / `recursively` / `disabled` |
| Locate System Files Within a Subfolder | `Activé` ✅ / `Désactivé` | `mame2003-plus_core_sys_subfolder` | `enabled` / `disabled` |
| Locate Save Files Within a Subfolder | `Activé` ✅ / `Désactivé` | `mame2003-plus_core_save_subfolder` | `enabled` / `disabled` |
| X-Y Device | `mouse` ✅ / `pointer` / `lightgun` / `Désactivé` | `mame2003-plus_xy_device` | `mouse` / `pointer` / `lightgun` / `disabled` |
| Input Interface | `simultaneous` ✅ / `retropad` / `keyboard` | `mame2003-plus_input_interface` | `simultaneous` / `retropad` / `keyboard` |
 | Show Lightgun Crosshairs | `Activé` ✅ / `Désactivé` | `mame2003-plus_crosshair_enabled` | `enabled` / `disabled` |
 | Lightgun Crosshair Appearance | `simple` ✅ / `enhanced` | `mame2003-plus_crosshair_appearance` | `simple` / `enhanced` |
 | Allow Input Button to Act as a Toggle Switch | `Activé` / `Désactivé` | `mame2003-plus_input_toggle` | `enabled` / `disabled` |
| Center Joystick Axis for Digital Controls | `Activé` ✅ / `Désactivé` | `mame2003-plus_digital_joy_centering` | `enabled` / `disabled` |
| Use Samples | `Activé` ✅ / `Désactivé` | `mame2003-plus_use_samples` | `enabled` / `disabled` |
| Sample Rate | `8000 KHz` / `11025 KHz` / `22050 KHz` / `30000 KHz` / `44100 KHz` / `48000 KHz` ✅ | `mame2003-plus_sample_rate` | `8000` / `11025` / `22050` / `30000` / `44100` / `48000` |
| Brightness | `0.2` / `0.3` / `0.4` / `0.5` / `0.6` / `0.7` / `0.8` / `0.9` / `1.0` ✅ / `1.1` / `1.2` / `1.3` / `1.4` / `1.5` / `1.6` / `1.7` / `1.8` / `1.9` / `2.0` | `mame2003-plus_brightness` | `0.2` / `0.3` / `0.4` / `0.5` / `0.6` / `0.7` / `0.8` / `0.9` / `1.0` / `1.1` / `1.2` / `1.3` / `1.4` / `1.5` / `1.6` / `1.7` / `1.8` / `1.9` / `2.0` |
| Gamma Correction | `0.5` / `0.6` / `0.7` / `0.8` / `0.9` / `1.0` ✅ / `1.1` / `1.2` / `1.3` / `1.4` / `1.5` / `1.6` / `1.7` / `1.8` / `1.9` / `2.0` | `mame2003-plus_gamma` | `0.5` / `0.6` / `0.7` / `0.8` / `0.9` / `1.0` / `1.1` / `1.2` / `1.3` / `1.4` / `1.5` / `1.6` / `1.7` / `1.8` / `1.9` / `2.0` |
| TATE Mode | `Désactivé` ✅ / `Activé` | `mame2003-plus_tate_mode` | `disabled` / `enabled` |
| Frameskip | `Désactivé` ✅ / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `9` / `10` / `11` / `auto` / `auto aggressive` / `auto max` | `mame2003-plus_frameskip` | `disabled` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `9` / `10` / `11` / `auto` / `auto_aggressive` / `auto_max` |
| Display Artwork | `Activé` ✅ / `Désactivé` | `mame2003-plus_display_artwork` | `enabled` / `disabled` |
| Resolution Multiplier | `1` ✅ / `2` / `3` / `4` / `5` / `6` / `7` / `8` | `mame2003-plus_art_resolution` | `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` |
| Hardcoded Overlay Opacity | `default` ✅ / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `21` / `22` / `23` / `24` / `25` / `50` / `70` | `mame2003-plus_art_overlay_opacity` | `default` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `21` / `22` / `23` / `24` / `25` / `50` / `70` |
 | Specify Neo Get BIOS | `default` ✅ / `euro` / `euro-s1` / `us` / `us-e` / `asia` / `japan` / `japan-s2` / `unibios40` / `unibios33` / `unibios20` / `unibios13` / `unibios11` / `unibios10` / `debug` / `asia-aes` | `mame2003-plus_` | `default` / `euro` / `euro-s1` / `us` / `us-e` / `asia` / `japan` / `japan-s2` / `unibios40` / `unibios33` / `unibios20` / `unibios13` / `unibios11` / `unibios10` / `debug` / `asia-aes` |
 | CPU Clock Scale | `default` ✅ / `25%` / `30%` / `35%` / `40%` / `45%` / `50%` / `55%` / `60%` / `65%` / `70%` / `75%` / `80%` / `85%` / `90%` / `95%` / `100%` / `105%` / `110%` / `115%` / `120%` / `125%` / `200%` / `250` / `300` | `mame2003-plus_cpu_clock_scale` | `default` / `25` / `30` / `35` / `40` / `45` / `50` / `55` / `60` / `65` / `70` / `75` / `80` / `85` / `90` / `95` / `100` / `105` / `110` / `115` / `120` / `125` / `200` / `250` / `300` |

## ![](/emulators/external-links.png) Enlaces externos

* **Progettosnaps** : [https://www.progettosnaps.net/](https://www.progettosnaps.net/)
* **Código fuente utilizado** : [https://github.com/libretro/mame2003-plus-libretro/](https://github.com/libretro/mame2003-plus-libretro)
* **Documentación Libretro** : [https://docs.libretro.com/library/mame2003\_plus/](https://docs.libretro.com/library/mame2003_plus/)