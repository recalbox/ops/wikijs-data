---
title: Tic-80
description: Libretro-tic80
published: true
date: 2022-09-06T09:51:51.155Z
tags: tic-80, fantasy
editor: markdown
dateCreated: 2021-06-30T15:14:05.223Z
---

![](/emulators/fantasy/tic80.svg){.align-center}

## Technical data

* **Manufacturer**: Vadim Grigoruk
* **Initial release**: 2017
* **Built-in features**: Code editor, Sprite Editor, Tile Map Editor, SFX Editor
* **Display**: 240x136 pixels, 16 colors palette
* **Sprites**: 256 8x8 foreground sprites, 256 8x8 background tiles
* **Sound**: 4 channels (with modifiable waveform envelopes)

## Presentation

TIC-80 is a fantastic FREE and OPEN SOURCE computer for creating, playing and sharing small games.

With TIC-80 you have built-in development tools: code, sprites, maps, sound editors and command line, which is enough to create a retro mini-game!

The games are bundled in a cartridge file, which can be easily distributed. The TIC-80 works on all popular platforms. This means that your cartridge can be played on any device.

To create a retro style game, the whole process of creation and execution takes place under certain technical limits: 240x136 pixel display, 16 color palette, 256 8x8 color sprites, 4 channel sound, etc.

## Emulators

[Libretro Tic80](libretro-tic80)

