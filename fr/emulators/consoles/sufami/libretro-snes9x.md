---
title: Libretro Snes9X
description: 
published: true
date: 2024-07-24T18:56:04.120Z
tags: libretro, snes9x, sufami turbo
editor: markdown
dateCreated: 2021-05-21T08:26:57.964Z
---

**Libretro Snes9X** est un port de Snes9x mis à jour en amont, un émulateur portable Super Nintendo Entertainment System vers Libretro.

## ![](/emulators/license.svg) Licence

Ce core est sous licence [**non commerciale**](https://github.com/snes9xgit/snes9x/blob/master/LICENSE).

## ![](/emulators/compatibility.png) Compatibilité

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Fonctionnalités

| Fonctionnalité | Supporté |
| :---: | :---: |
| Redémarrage | ✔ |
| Sauvegardes | ✔ |
| Sauvegardes instantanées | ✔ |
| Rembobinage | ✔ |
| Netplay | ✔ |
| Options du core | ✔ |
| RetroAchievements | ✔ |
| Cheats RetroArch | ✔ |
| Controls | ✔ |
| Remapping | ✔ |
| Multi-Mouse | ✔ |
| Subsystem | ✔ |
| Softpatching | ✔ |

## ![](/emulators/bios.svg) BIOS

### Liste des bios obligatoires

| Nom de fichier | Description | MD5 | Fourni |
| :---: | :---: | :---: | :---: |
| STBIOS.bin | Sufami Turbo (Japan) | d3a44ba7d42a74d3ac58cb9c14c6a5ca | ❌  |

### Emplacement

Placez le BIOS comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 sufami
┃ ┃ ┃ ┃ ┣ 🗒 **STBIOS.bin**

## ![](/emulators/roms.png) Roms

### Extensions supportées

Les roms doivent avoir l'extension suivante :

* .fig
* .gd3
* .sfc
* .smc
* .st
* .swc
* .zip
* .7z

Ce système supporte les roms compressées au format .zip/.7z. Attention toutefois, il ne s'agit que d'une archive.

Les fichiers contenus dans les .zip/.7z doivent correspondre aux extensions citées précédemment.
De plus, chaque fichier .zip/.7z ne doit contenir qu'une seule rom compressée.

### Emplacement

Placez les roms comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 sufami
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.zip**

>Les roms au format **No-Intro** sont vivement conseillées.
{.is-success}

>Pour plus d'information sur les roms, rendez-vous sur [ce tutoriel](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuration avancée de l'émulateur

>Pour pouvoir conserver vos configurations personnalisées lors d'une mise à jour, nous vous conseillons d'utiliser notre fonctionnalité [Surcharges de configuration](./../../../advanced-usage/configuration-override).
{.is-info}

### Accéder aux options

Vous pouvez configurer diverses options de deux façons différentes.

* Via le menu RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via le fichier `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Options du core

| Options | Valeurs des options | Variable (pour surcharge) | Valeurs des variables |
| --- | --- | --- | --- |
| Console Region (Reload Core) | `Auto` ✅ / `NTSC` / `PAL` | `snes9x_region` | `auto` / `ntsc` / `pal` |
| Preferred Aspect Ratio | `4:3` ✅ / `Uncorrected` / `Auto` / `NTSC` / `PAL` | `snes9x_aspect` | `4:3` / `uncorrected` / `auto` / `ntsc` / `pal` |
| Crop Overscan | `Activé` ✅ / `Désactivé` | `snes9x_overscan` | `enabled` / `disabled` |
| Hi-Res Blending | `Désactivé` ✅ / `Merge` / `Blur` | `snes9x_hires_blend` | `disabled` / `merge` / `blur` |
| Blargg NTSC Filter | `Désactivé` ✅ / `Monochrome` / `RF` / `Composite` / `S-Video` / `RGB` | `snes9x_blargg` | `disabled` / `monochrome` / `rf` / `composite` / `s-video` / `rgb` |
| Audio Interpolation | `Gaussian` ✅ / `Cubic` / `Sinc` / `None` / `Linear` | `snes9x_audio_interpolation` | `gaussian` / `cubic` / `sinc` / `none` / `linear` |
| Allow Opposing Directions | `Désactivé` ✅ / `Activé` | `snes9x_up_down_allowed` | `disabled` / `enabled` |
| SuperFX Overclocking | `50%` / `60%` / `70%` / `80%` / `90%` / `100%` ✅ / `150%` / `200%` / `250%` / `300%` / `350%` / `400%` / `450%` / `500%` | `snes9x_overclock_superfx` | `50%` / `60%` / `70%` / `80%` / `90%` / `100%` / `150%` / `200%` / `250%` / `300%` / `350%` / `400%` / `450%` / `500%` |
| Reduce Slowdown (Unsafe) | `Désactivé` ✅ / `Light` / `Compatible` / `Max` | `snes9x_overclock_cycles` | `disabled` / `light` / `compatible` / `max` |
| Reduce Flickering (Unsafe) | `Désactivé` ✅ / `Activé` | `snes9x_reduce_sprite_flicker` | `disabled` / `enabled` |
| Randomize Memory (Unsafe) | `Désactivé` ✅ / `Activé` | `snes9x_randomize_memory` | `disabled` / `enabled` |
| Block Invalid VRAM Access | `Activé` ✅ / `Désactivé` | `snes9x_block_invalid_vram_access` | `enabled` / `disabled` |
| Echo Buffer Hack (Unsafe, only enable for old addmusic hacks) | `Désactivé` ✅ / `Activé` | `snes9x_echo_buffer_hack` | `disabled` / `enabled` |
| Light Gun Mode | `Light Gun` / `Touchscreen` | `snes9x_lightgun_mode` | `Lightgun` / `Touchscreen` |
| Super Scope Reverse Trigger Buttons | `Désactivé` ✅ / `Activé` | `snes9x_superscope_reverse_buttons` | `disabled` / `enabled` |
| Super Scope Crosshair | `0` / `1` / `2` ✅ / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` | `snes9x_superscope_crosshair` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` |
| Super Scope Color | `White` ✅ / `White (blend)` / `Red` / `Red (blend)` / `Orange` / `Orange (blend)` / `Yellow` / `Yellow (blend)` / `Green` / `Green (blend)` / `Cyan` / `Cyan (blend)` / `Sky` / `Sky (blend)` / `Blue` / `Blue (blend)` / `Violet` / `Violet (blend)` / `Pink` / `Pink (blend)` / `Purple` / `Purple (blend)` / `Black` / `Black (blend)` / `25% Grey` / `25% Grey (blend)` / `50% Grey` / `50% Grey (blend)` / `75% Grey` / `75% Grey (blend)` | `snes9x_superscope_color` | `White` / `White (blend)` / `Red` / `Red (blend)` / `Orange` / `Orange (blend)` / `Yellow` / `Yellow (blend)` / `Green` / `Green (blend)` / `Cyan` / `Cyan (blend)` / `Sky` / `Sky (blend)` / `Blue` / `Blue (blend)` / `Violet` / `Violet (blend)` / `Pink` / `Pink (blend)` / `Purple` / `Purple (blend)` / `Black` / `Black (blend)` / `25% Grey` / `25% Grey (blend)` / `50% Grey` / `50% Grey (blend)` / `75% Grey` / `75% Grey (blend)` |
| Justifier 1 Crosshair | `0` / `1` / `2` / `3` / `4` ✅ / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` | `snes9x_justifier1_crosshair` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` |
| Justifier 1 Color | `White` / `White (blend)` / `Red` / `Red (blend)` / `Orange` / `Orange (blend)` / `Yellow` / `Yellow (blend)` / `Green` / `Green (blend)` / `Cyan` / `Cyan (blend)` / `Sky` / `Sky (blend)` / `Blue` ✅ / `Blue (blend)` / `Violet` / `Violet (blend)` / `Pink` / `Pink (blend)` / `Purple` / `Purple (blend)` / `Black` / `Black (blend)` / `25% Grey` / `25% Grey (blend)` / `50% Grey` / `50% Grey (blend)` / `75% Grey` / `75% Grey (blend)` | `snes9x_justifier1_color` | `White` / `White (blend)` / `Red` / `Red (blend)` / `Orange` / `Orange (blend)` / `Yellow` / `Yellow (blend)` / `Green` / `Green (blend)` / `Cyan` / `Cyan (blend)` / `Sky` / `Sky (blend)` / `Blue` / `Blue (blend)` / `Violet` / `Violet (blend)` / `Pink` / `Pink (blend)` / `Purple` / `Purple (blend)` / `Black` / `Black (blend)` / `25% Grey` / `25% Grey (blend)` / `50% Grey` / `50% Grey (blend)` / `75% Grey` / `75% Grey (blend)` |
| Justifier 2 Crosshair | `0` / `1` / `2` / `3` / `4` ✅ / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` | `snes9x_justifier2_crosshair` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` |
| Justifier 2 Color | `White` / `White (blend)` / `Red` / `Red (blend)` / `Orange` / `Orange (blend)` / `Yellow` / `Yellow (blend)` / `Green` / `Green (blend)` / `Cyan` / `Cyan (blend)` / `Sky` / `Sky (blend)` / `Blue` / `Blue (blend)` / `Violet` / `Violet (blend)` / `Pink` ✅ / `Pink (blend)` / `Purple` / `Purple (blend)` / `Black` / `Black (blend)` / `25% Grey` / `25% Grey (blend)` / `50% Grey` / `50% Grey (blend)` / `75% Grey` / `75% Grey (blend)` | `snes9x_justifier2_color` | `White` / `White (blend)` / `Red` / `Red (blend)` / `Orange` / `Orange (blend)` / `Yellow` / `Yellow (blend)` / `Green` / `Green (blend)` / `Cyan` / `Cyan (blend)` / `Sky` / `Sky (blend)` / `Blue` / `Blue (blend)` / `Violet` / `Violet (blend)` / `Pink` / `Pink (blend)` / `Purple` / `Purple (blend)` / `Black` / `Black (blend)` / `25% Grey` / `25% Grey (blend)` / `50% Grey` / `50% Grey (blend)` / `75% Grey` / `75% Grey (blend)` |
| M.A.C.S. Rifle Crosshair | `0` / `1` / `2` ✅ / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` | `snes9x_rifle_crosshair` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` |
| M.A.C.S. Rifle Color | `White` ✅ / `White (blend)` / `Red` / `Red (blend)` / `Orange` / `Orange (blend)` / `Yellow` / `Yellow (blend)` / `Green` / `Green (blend)` / `Cyan` / `Cyan (blend)` / `Sky` / `Sky (blend)` / `Blue` / `Blue (blend)` / `Violet` / `Violet (blend)` / `Pink` / `Pink (blend)` / `Purple` / `Purple (blend)` / `Black` / `Black (blend)` / `25% Grey` / `25% Grey (blend)` / `50% Grey` / `50% Grey (blend)` / `75% Grey` / `75% Grey (blend)` | `snes9x_rifle_color` | `White` / `White (blend)` / `Red` / `Red (blend)` / `Orange` / `Orange (blend)` / `Yellow` / `Yellow (blend)` / `Green` / `Green (blend)` / `Cyan` / `Cyan (blend)` / `Sky` / `Sky (blend)` / `Blue` / `Blue (blend)` / `Violet` / `Violet (blend)` / `Pink` / `Pink (blend)` / `Purple` / `Purple (blend)` / `Black` / `Black (blend)` / `25% Grey` / `25% Grey (blend)` / `50% Grey` / `50% Grey (blend)` / `75% Grey` / `75% Grey (blend)` |
| Show Layer 1 | `Activé` ✅ / `Désactivé` | `snes9x_layer_1` | `enabled` / `disabled` |
| Show Layer 2 | `Activé` ✅ / `Désactivé` | `snes9x_layer_2` | `enabled` / `disabled` |
| Show Layer 3 | `Activé` ✅ / `Désactivé` | `snes9x_layer_3` | `enabled` / `disabled` |
| Show Layer 4 | `Activé` ✅ / `Désactivé` | `snes9x_layer_4` | `enabled` / `disabled` |
| Show Sprite Layer | `Activé` ✅ / `Désactivé` | `snes9x_layer_5` | `enabled` / `disabled` |
| Enable Graphic Clip Windows | `Activé` ✅ / `Désactivé` | `snes9x_gfx_clip` | `enabled` / `disabled` |
| Enable Transparency Effects | `Activé` ✅ / `Désactivé` | `snes9x_gfx_transp` | `enabled` / `disabled` |
| Enable Sound Channel 1 | `Activé` ✅ / `Désactivé` | `snes9x_sndchan_1` | `enabled` / `disabled` |
| Enable Sound Channel 2 | `Activé` ✅ / `Désactivé` | `snes9x_sndchan_2` | `enabled` / `disabled` |
| Enable Sound Channel 3 | `Activé` ✅ / `Désactivé` | `snes9x_sndchan_3` | `enabled` / `disabled` |
| Enable Sound Channel 4 | `Activé` ✅ / `Désactivé` | `snes9x_sndchan_4` | `enabled` / `disabled` |
| Enable Sound Channel 5 | `Activé` ✅ / `Désactivé` | `snes9x_sndchan_5` | `enabled` / `disabled` |
| Enable Sound Channel 6 | `Activé` ✅ / `Désactivé` | `snes9x_sndchan_6` | `enabled` / `disabled` |
| Enable Sound Channel 7 | `Activé` ✅ / `Désactivé` | `snes9x_sndchan_7` | `enabled` / `disabled` |
| Enable Sound Channel 8 | `Activé` ✅ / `Désactivé` | `snes9x_sndchan_8` | `enabled` / `disabled` |

## ![](/emulators/external-links.png) Liens externes

* **Code source utilisé** : [https://github.com/libretro/snes9x/](https://github.com/libretro/snes9x/)
* **Documentation Libretro** : [https://docs.libretro.com/library/snes9x/](https://docs.libretro.com/library/snes9x/)