---
title: Activer le mode debug
description: 
published: true
date: 2024-06-28T15:25:36.910Z
tags: mode, debug, activer
editor: markdown
dateCreated: 2022-04-24T21:02:07.760Z
---

Afin de pouvoir dépanner votre Recalbox, il peut vous être demandé d'activer le mode debug (appelé aussi « debug logs »).

Le mode debug permet d'enregistrer d'avantage d'informations sur l'activité du système (dans des fichiers \*.log) à des fins de dépannage.

> Attention, le mode debug n'est pas une solution en tant que telle mais on ne peut plus utile pour poser un diagnostic.
{.is-info}

## Procédure

- Ouvrez le fichier [recalbox.conf](../../basic-usage/getting-started/recalboxconf-file).
- Sur une nouvelle ligne, ajouter cette ligne :

```ini
emulationstation.debuglogs=1
```

- Une fois cette modification effectuée, sauvegardez le fichier et **redémarrez**.
- Une fois redémarré, reproduisez le problème puis rendez-vous dans le répertoire `/recalbox/share/system/logs/` pour y récupérer le fichier de debug `frontend.log`. 

> N'oubliez pas de supprimer ou de commenter cette ligne avec un `;` devant pour arrêter le mode debug une fois tout le processus effectué !
{.is-info}

> Prenez soin de désactiver le mode debug s'il n'est plus nécessaire car celui-ci peut dégrader les performances de certains émulateurs et l'expérience in-game notamment MAME !
{.is-info}