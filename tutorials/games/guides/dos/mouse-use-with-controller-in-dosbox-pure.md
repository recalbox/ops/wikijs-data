---
title: Utilisation de la souris avec la manette dans DOSBox Pure
description: 
published: true
date: 2021-09-04T23:26:45.063Z
tags: mouse, dosbox pure, 7.2+
editor: markdown
dateCreated: 2021-07-29T16:32:13.051Z
---

## Introduction

Avec le core Libretro DOSBox Pure, vous pouvez activer le contrôle de la souris pour s'en servir avec un des sticks de votre manette.

## Utilisation

Pour utiliser la souris sur un joystick de votre manette, il y a une manipulation à effectuer dans les options de RetroArch. Nous allons voir ci-dessous comment utiliser la souris sur le joystick droit.

* Lancez votre jeu
* Une fois lancé, allez dans les options de RetroArch et descendez sur **Touches**.
* Dans la configuration des touches, entrez dans **Port 1 Touches**.
* Mettez-vous sur l'option **Type de périphérique** et utilisez la flèche droite pour vous arrêter sur `Mouse with Right Analog Stick`.
* Retournez en jeu et profitez ! Vous aurez les clics de souris sur L1 et R1 (clic gauche et clic droit).