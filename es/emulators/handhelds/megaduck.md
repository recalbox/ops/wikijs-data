---
title: Mega Duck
description: 
published: true
date: 2024-07-26T08:33:22.744Z
tags: 8.1+, megaduck
editor: markdown
dateCreated: 2023-09-04T08:17:40.964Z
---

![](/emulators/handheld/megaduck.png){.align-center}

## Datos técnicos

* **Fabricante**: Welback Holdings
* **Año de lanzamiento**: 1993
* **Procesador**: versión MOS del Z80 (integrado en el VLSI principal)
* **RAM**: 16kB
* **Resolución**: 160x144 píxeles, 4 niveles de gris
* **Sonido**: altavoz integrado (8Ω 200mW)
* **Soportes**: cartuchos de 36 pines

## Presentación

La Mega Duck (también conocida como Cougar Boy) es una consola portátil creada por varias empresas (Creatonic, Videojet y Timlex). Apareció en el mercado de los videojuegos en 1993 y se vendió a un precio de unos 60 euros (principalmente en Francia, Países Bajos y Alemania). En Sudamérica (principalmente en Brasil), la versión de Creatonic desarrollada en China fue distribuida por Cougar USA con el nombre de "Cougar Boy".

Los cartuchos son bastante similares a los de la Watara Supervision, aunque son ligeramente más pequeños que esta última (36 pines para la Mega Duck frente a 40 para la Supervision). La electrónica de las dos consolas también es muy similar. La ubicación de los controles de volumen y contraste, así como los botones y conectores, es prácticamente idéntica. Sin embargo, la pantalla de la Supervision es más grande que la de la Megaduck.

El Cougar Boy se vendía con un cartucho de juego 4 en 1 y auriculares estéreo. Además, dos jugadores podían enfrentarse simultáneamente utilizando un joystick externo (que se vendía por separado de la consola).

## Emuladores

[Libretro Sameduck](libretro-sameduck)