---
title: CHDMAN
description: Convert your CD roms into a readable compressed format.
published: true
date: 2024-12-02T18:43:52.716Z
tags: chd, chdman
editor: markdown
dateCreated: 2021-08-06T23:46:52.825Z
---

## Introduction

The file formats to adopt for CDs and other optical supports.

We have talked about a format, derived from researches we had on MAME, in order to be able to represent these supports in a compressed way, without losing the integrity of the data and that these can remain scrappable.

This format is the CHD (Compressed Hunks of Data), datas that could be useful for SEGA CD, PS1, PC Engine...

Unfortunately, the classic user will have difficulties to use the converter, which is a command line executable.

So here is a Zip file, with automated scripts, to convert from BIN+CUE (Redump format) to CHD, and vice versa.

It has been added also for the GDI format, which is for the Dreamcast.

For PS1 games protected by LibCrypt, normally you have SBI (Subchannel Information) files, you keep them and put them with the CHDs, otherwise your games will not pass.

If you ever lose them or make a bad manipulation, these files are available on the files of each corresponding disk at Redump, next to the re-downloadable CUE files.

## Software

## {.tabset}
### Windows

You can download the software by clicking on **CHDMAN.zip** below.

![](/tutorials/utilities/rom-conversion/chdman/archive.svg) [CHDMAN.zip](/tutorials/utilities/rom-conversion/chdman/chdman.zip)

In this zip you will find 6 files :

| File name | Description |
| :--- | :--- |
| **chdman.exe** | CHDMAN software |
| **CUE or GDI to CHD.bat** A .bat file that allows you to convert your CUE or GDI roms to CHD.
| **Extract CHD to CUE.bat** | A .bat file that allows you to convert your CHD roms to BIN+CUE.
| **Extract CHD to GDI.bat** | A .bat file that allows you to convert your CHD rom to GDI.
| **README_EN.txt** |
| **README_EN.txt** | |

#### Readme file content

* **CUE or GDI to CHD**

  Compresses all types of BIN disk files with a CUE or GDI header to CHD (v5) format.  
  Searches all subfolders and creates CHD (v5) files in the folder where the files are placed with CHDMAN.

* **Extract CHD to CUE** Decompresses a CHD (v5) file to a BIN+CUE file. The CUE format is used by games on CD.  On the Raspberry Pi, CHD is supported by TurboGrafx-CD / PC Engine CD, Sega CD / Mega CD and Dreamcast.
* **Extract CHD to GDI**

  Unzips a CHD (V5) file to GDI. (GDI is a disk format for Dreamcast).

### macOS

CHDMAN exists for macOS via Homebrew.

* Installation of XCode tools

First, you need to use the following command in the Terminal:

`xcode-select --install`

* Installation of [**Homebrew**](https://brew.sh/index)

Next, you must use the following command in the Terminal:

`/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"`

* If you are on Mac with Apple Silicon chip, you need to use this command:

`echo 'eval $(/opt/homebrew/bin/brew shellenv)' >> ~/.zprofile`

* Installation of [**rom-tools**](https://formulae.brew.sh/formula/rom-tools)

You must use the following command in the Terminal after Homebrew is installed:

`brew install rom-tools`

macOS 10.13 High Sierra minimum is required.

Here is a link to download files to automate format conversions:

![](/tutorials/utilities/rom-conversion/chdman/archive.svg) [ChdScripts.zip](/tutorials/utilities/rom-conversion/chdman/chdscripts.zip)

In this zip you will find 6 files:

| File name | Description |
| :--- | :--- |
| **<span>ConvertFromChdToCue.sh</span>** | A .sh file that allows you to convert your `CHD` roms to `BIN`/`CUE` format. |
| **<span>convertFromChdToGdi.sh</span>** | A .sh file that allows you to convert your `CHD` roms to `GDI` format. |
| **<span>convertFromChdToIso.sh</span>** | A .sh file that allows you to convert your `CHD` roms to `ISO` format. |
| **<span>convertFromCueToChd.sh</span>** | A .sh file that allows you to convert your `BIN`/`CUE` roms to `CHD`. |
| **<span>convertFromGdiToChd.sh</span>** | A .sh file that allows you to convert your `GDI` roms to `CUE`. |
| **<span>convertFromIsoToChd.sh</span>** | A .sh file that allows you to convert your `ISO` roms to `CUE`. |
| **README_EN.txt** |  |
| **README_FR.txt** |  |

#### Contents of the readme file

* **<span>convertFromChdToCue.sh</span>**

  Decompresses a CHD (V5) file into a BIN+CUE file.
  The CUE format is used by games on CD. CHD is supported by 3DO, Amiga CD32, Amiga CDTV, Dreamcast, Mega CD, Neo-Geo CD, PC Engine CD, PlayStation and Saturn.

* **<span>convertFromChdToGdi.sh</span>**
  Decompresses a CHD (V5) file into a GDI file.
  The GDI format is used by disk-based games for Dreamcast.

* **<span>convertFromChdToIso.sh</span>**
  Decompresses a CHD (V5) file into an ISO file.
  The ISO format is used by disk-based games for PlayStation 2.

* **<span>convertFromCueToChd.sh</span>**

  Compresses any type of BIN disk files with a CUE header to the CHD (v5) format. Searches all subfolders and creates CHD (v5) files in the folder where the files are placed with CHDMAN.

* **<span>convertFromGdiToChd.sh</span>**

  Compresses any type of BIN disk files with a GDI header to the CHD (v5) format. The GDI format is used by disk-based games for Dreamcast.

* **<span>convertFromIsoToChd.sh</span>**

  Compresses any type of BIN disk files with a ISO header to the CHD (v5) format. The ISO format is used by disk-based games for PlayStation 2.

### Linux

Some Linux distributions can give you chdman via `aptitude` with the `mame-tools` package. Some others don't have any `mame-tools` and you need to build it:

* Go to https://github.com/libretro/mame
* Download the content by clicking on the `Code` green button then, in the visible menu, click on `Download ZIP`

![](/tutorials/utilities/rom-conversion/chdman/mamedownload.png){.full-width}

* Uncompress the downloaded archive and go inside the extracted directory. You should retrieve the same content than into the given link in the first step.
* From your, use this command:

```ssh
make tools
```

* At the end, you should be able to find chdman into the `build` directory.

Here is a link to download files to automate format conversions:

![](/tutorials/utilities/rom-conversion/chdman/archive.svg) [ChdScripts.zip](/tutorials/utilities/rom-conversion/chdman/chdscripts.zip)

In this zip you will find 6 files:

| File name | Description |
| :--- | :--- |
| **<span>ConvertFromChdToCue.sh</span>** | A .sh file that allows you to convert your `CHD` roms to `BIN`/`CUE` format. |
| **<span>convertFromChdToGdi.sh</span>** |  A .sh file that allows you to convert your `CHD` roms to `GDI` format. |
| **<span>convertFromChdToIso.sh</span>** | A .sh file that allows you to convert your `CHD` roms to `ISO` format. |
| **<span>convertFromCueToChd.sh</span>** | A .sh file that allows you to convert your `BIN`/`CUE` roms to `CHD`. |
| **<span>convertFromGdiToChd.sh</span>** | A .sh file that allows you to convert your `GDI` roms to `CUE`. |
| **<span>convertFromIsoToChd.sh</span>** | A .sh file that allows you to convert your `ISO` roms to `CUE`. |
| **README_EN.txt** |  |
| **README_FR.txt** |  |

#### Contents of the readme file

* **<span>convertFromChdToCue.sh</span>**

  Decompresses a CHD (V5) file into a BIN+CUE file.
  The CUE format is used by games on CD. CHD is supported by 3DO, Amiga CD32, Amiga CDTV, Dreamcast, Mega CD, Neo-Geo CD, PC Engine CD, PlayStation and Saturn.

* **<span>convertFromChdToGdi.sh</span>**
  Decompresses a CHD (V5) file into a GDI file.
  The GDI format is used by disk-based games for Dreamcast.

* **<span>convertFromChdToIso.sh</span>**
  Decompresses a CHD (V5) file into an ISO file.
  The ISO format is used by disk-based games for PlayStation 2.

* **<span>convertFromCueToChd.sh</span>**

  Compresses any type of BIN disk files with a CUE header to the CHD (v5) format. Searches all subfolders and creates CHD (v5) files in the folder where the files are placed with CHDMAN.

* **<span>convertFromGdiToChd.sh</span>**

  Compresses any type of BIN disk files with a GDI header to the CHD (v5) format. The GDI format is used by disk-based games for Dreamcast.

* **<span>convertFromIsoToChd.sh</span>**

  Compresses any type of BIN disk files with a ISO header to the CHD (v5) format. The ISO format is used by disk-based games for PlayStation 2.

## Automated use

## {.tabset}
### Windows
#### Convert your game from `BIN`/`CUE` or `GDI` to `CHD`

* Put "**chdman.exe**" and "**CUE or GDI to CHD.bat**" in the folder containing your game as below.
  Example for the game "Grandia (France)":

![](/tutorials/utilities/rom-conversion/chdman/chdman-win1.png)

* Click on the file "**CUE or GDI to CHD.bat**" to start the conversion.

![](/tutorials/utilities/rom-conversion/chdman/chdman-win2.png)

* Once the CMD window above has closed automatically, your conversion is complete.

![](/tutorials/utilities/rom-conversion/chdman/chdman-win3.png)

* You can delete "**chdman.exe**" and "**CUE or GDI to CHD.bat**", your rom is ready.

![](/tutorials/utilities/rom-conversion/chdman/chdman-win4.png)

>You can also run the .bat file to start the conversion of several games at once.
{.is-info}

#### Convert your game from `CHD` to `BIN`/`CUE`

* Put "**chdman.exe**" and "**Extract CHD to CUE.bat**" in the folder containing your game as below.
  Example for the game "Grandia (France)":



* Click on the file "**Extract CHD to CUE.bat**" to start the conversion.



* Once the CMD window above has closed automatically, your conversion is complete.



* You can delete "**chdman.exe**" and "**Extract CHD to CUE.bat**", your rom is ready.



>You can also run the .bat file to start the conversion of several games at once.
{.is-info}

#### Convert your game from `CHD` to `GDI`

* Put "**chdman.exe**" and "**Extract CHD to GDI.bat**" in the folder containing your game as below.
  Example for the game "Grandia (France)":



* Click on the file "**Extract CHD to GDI.bat**" to start the conversion.



* Once the CMD window above has closed automatically, your conversion is complete.



* You can delete "**chdman.exe**" and "**Extract CHD to GDI.bat**", your rom is ready.



>You can also run the .bat file to start the conversion of several games at once.
{.is-info}

### macOS

#### Convert your game from `BIN`/`CUE` to `CHD`

* Put "**<span>convertFromCueToChd.sh</span>**" in the folder containing your game as below.
  Example for the game "Grandia (France)" :

![](/tutorials/utilities/rom-conversion/chdman/chdman-unix1.png){.full-width}

* Return to the parent folder, do a right click on it and select `Services` > `Open in Terminal`. Once opened, execute the file with the command `./convertFromCueToChd.sh` to start the conversion.

![](/tutorials/utilities/rom-conversion/chdman/chdman-unix2.png){.full-width}

* Once your Terminal window has finished, your conversion is complete.

![](/tutorials/utilities/rom-conversion/chdman/chdman-unix3.png){.full-width}

* You can delete "**<span>convertFromCueToChd.sh</span>**", your rom is ready.

#### Convert your game from `GDI` to `CHD`

* Put "**<span>convertFromGdiToChd.sh</span>**" in the folder containing your game as below.
  Example for the game "Resident Evil - Code - Veronica (France)" :

![](/tutorials/utilities/rom-conversion/chdman/chdman-unix6.png){.full-width}

* Return to the parent folder, do a right click on it and select `Services` > `Open in Terminal`. Once opened, execute the file with the command `./convertFromGdiToChd.sh` to start the conversion.

![](/tutorials/utilities/rom-conversion/chdman/chdman-unix7.png){.full-width}

* Once your Terminal window has finished, your conversion is complete.

![](/tutorials/utilities/rom-conversion/chdman/chdman-unix8.png){.full-width}

* You can delete "**<span>convertFromGdiToChd.sh</span>**", your rom is ready.

#### Convert your game from `ISO` to `CHD`

* Put "**<span>convertFromIsoToChd.sh</span>**" in the folder containing your game as below.
  Example for the game "Shadow of the Colossus (Europe, Australia) (En,Fr,De,Es,It)" :



* Return to the parent folder, do a right click on it and select `Services` > `Open in Terminal`. Once opened, execute the file with the command `./convertFromIsoToChd.sh` to start the conversion.



* Once your Terminal window has finished, your conversion is complete.



* You can delete "**<span>convertFromIsoToChd.sh</span>**", your rom is ready.

#### Convert your game from `CHD` to `BIN`/`CUE`

* Put "**<span>convertFromChdToCue.sh</span>**" in the folder containing your game as below.
  Example for the game "Grandia (France)" :

![](/tutorials/utilities/rom-conversion/chdman/chdman-unix4.png){.full-width}

* Return to the parent folder, do a right click on it and select `Services` > `Open in Terminal`. Once opened, execute the file with the command `./convertFromChdToCue.sh` to start the conversion.

![](/tutorials/utilities/rom-conversion/chdman/chdman-unix5.png){.full-width}

* Once your Terminal window has finished, your conversion is complete.

![](/tutorials/utilities/rom-conversion/chdman/chdman-unix3.png){.full-width}

* You can delete "**<span>convertFromChdToCue.sh</span>**", your rom is ready.

#### Convert your game from `CHD` to `GDI`

* Put "**<span>convertFromChdToGdi.sh</span>**" in the folder containing your game as below.
  Example for the game "Resident Evil - Code - Veronica (France)" :

![](/tutorials/utilities/rom-conversion/chdman/chdman-unix9.png){.full-width}

* Return to the parent folder, do a right click on it and select `Services` > `Open in Terminal`. Once opened, execute the file with the command `./convertFromChdToGdi.sh` to start the conversion.

![](/tutorials/utilities/rom-conversion/chdman/chdman-unix10.png){.full-width}

* Once your Terminal window has finished, your conversion is complete.

![](/tutorials/utilities/rom-conversion/chdman/chdman-unix11.png){.full-width}

* You can delete "**<span>convertFromChdToGdi.sh</span>**", your rom is ready.

#### Convert your game from `CHD` to `ISO`

* Put "**<span>convertFromChdToIso.sh</span>**" in the folder containing your game as below.
  Example for the game "Shadow of the Colossus (Europe, Australia) (En,Fr,De,Es,It)" :



* Return to the parent folder, do a right click on it and select `Services` > `Open in Terminal`. Once opened, execute the file with the command `./convertFromChdToIso.sh` to start the conversion.



* Once your Terminal window has finished, your conversion is complete.



* You can delete "**<span>convertFromChdToIso.sh</span>**", your rom is ready.

### Linux

#### Convert your game from `BIN`/`CUE` to `CHD`

* Put "**<span>convertFromCueToChd.sh</span>**" in the folder containing your game as below.
  Example for the game "Grandia (France)" :



* In your Terminal, execute the file with the command `./convertCueToChd.sh` to start the conversion.



* Once your Terminal window has finished, your conversion is complete.



* You can delete "**<span>convertFromCueToChd.sh</span>**", your rom is ready.

#### Convert your game from `GDI` to `CHD`

* Put "**<span>convertFromGdiToChd.sh</span>**" in the folder containing your game as below.
  Example for the game "Resident Evil - Code - Veronica (France)" :



* In your Terminal, execute the file with the command `./convertFromGdiToChd.sh` to start the conversion.



* Once your Terminal window has finished, your conversion is complete.



* You can delete "**<span>convertFromGdiToChd.sh</span>**", your rom is ready.

#### Convert your game from `ISO` to `CHD`

* Put "**<span>convertFromIsoToChd.sh</span>**" in the folder containing your game as below.
  Example for the game "Shadow of the Colossus (Europe, Australia) (En,Fr,De,Es,It)" :



* In your Terminal, execute the file with the command `./convertFromIsoToChd.sh` to start the conversion.



* Once your Terminal window has finished, your conversion is complete.



* You can delete "**<span>convertFromIsoToChd.sh</span>**", your rom is ready.

#### Convert your game from `CHD` to `BIN`/`CUE`

* Put "**<span>convertFromChdToCue.sh</span>**" in the folder containing your game as below.
  Example for the game "Grandia (France)" :



* In your Terminal, execute the file with the command `./convertFromChdToCue.sh` to start the conversion.



* Once your Terminal window has finished, your conversion is complete.



* You can delete "**<span>convertFromChdToCue.sh</span>**", your rom is ready.

#### Convert your game from `CHD` to `GDI`

* Put "**<span>convertFromChdToGdi.sh</span>**" in the folder containing your game as below.
  Example for the game "Resident Evil - Code - Veronica (France)" :



* In your Terminal, execute the file with the command `./convertFromChdToGdi.sh` to start the conversion.



* Once your Terminal window has finished, your conversion is complete.



* You can delete "**<span>convertFromChdToGdi.sh</span>**", your rom is ready.

#### Convert your game from `CHD` to `ISO`

* Put "**<span>convertFromChdToIso.sh</span>**" in the folder containing your game as below.
  Example for the game "Shadow of the Colossus (Europe, Australia) (En,Fr,De,Es,It)" :



* In your Terminal, execute the file with the command `./convertFromChdToIso.sh` to start the conversion.



* Once your Terminal window has finished, your conversion is complete.



* You can delete "**<span>convertFromChdToIso.sh</span>**", your rom is ready.

## Create the `.M3U` file

In CHD conversion for multi-disk games, you have to make a **M3U** file to declare all the disks.

The `.M3U` file is a list of the different CDs for a game that allows you to switch from one CD to another in a simple way by using the disc change combination (`Hotkey` + `L. STICK` to the _LEFT_ or to the _RIGHT_).

Example for the game "Grandia (France)":

* Create a `M3U` file via Notepad++ named "Grandia (France).m3u".
* In the file, fill in the `CHD` files of the game:

```text
Grandia (France) (Disc 1).chd
Grandia (France) (Disc 2).chd
```

* On Windows, you must have the extension view enabled to create this file:
  * Go to `View` at the top of the Windows Explorer window.
  * Check "File name extensions" at the top right.

![](/tutorials/utilities/rom-conversion/chdman/chdman-m3u1.png)

* Confirm the extension change

![](/tutorials/utilities/rom-conversion/chdman/chdman-m3u2.png)