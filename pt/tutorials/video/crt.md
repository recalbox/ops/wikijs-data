---
title: CRT
description: 
published: true
date: 2021-12-22T15:28:56.966Z
tags: video, crt
editor: markdown
dateCreated: 2021-12-22T15:28:05.071Z
---

Aqui você encontrará tutoriais sobre como conectar seu Recalbox a uma tela CRT.

Aqui estão os tutoriais disponíveis:

[CRT Scart e VGA no Raspberry Pi 4 / 400 / 3](recalbox-on-crt-with-scart-dac)
[Configure sua tela catódica no barramento DPI (VGA666 / PiScart / RGBPi)](crt-screen-dpi-vga666-piscart-rgbpi)
[Conecte seu Recalbox a uma tela CRT usando a saída HDMI](crt-screen-with-hdmi)
[Conecte seu Recalbox a uma Tela CRT com um cabo composto](crt-screen-with-compostite)
[Conecte seu Recalbox a uma tela catódica com HDMI e SCART](crt-screen-with-hdmi-and-scart)