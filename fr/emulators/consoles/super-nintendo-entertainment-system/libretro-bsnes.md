---
title: Libretro bsnes
description: 
published: true
date: 2024-07-24T20:37:25.895Z
tags: libretro, snes, bsnes, 8.1+
editor: markdown
dateCreated: 2022-02-27T23:17:28.681Z
---



## ![](/emulators/license.svg) Licence

Ce core est sous licence [**GPLv3**](https://github.com/libretro/bsnes-libretro/blob/master/LICENSE.txt).

## ![](/emulators/compatibility.png) Compatibilité

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ❌ | ❌ | ✅ | ✅ | ❌ | ❌ | ✅ | ✅ |

## ![](/emulators/features.png) Fonctionnalités

| Fonctionnalité | Supporté |
| :---: | :---: |
| Redémarrage | ✔ |
| Captures d'écran | ✔ |
| Sauvegardes | ✔ |
| Sauvegardes instantanées | ✔ |
| Rembobinage | ✔ |
| Netplay | ✔ |
| Options du core | ✔ |
| RetroAchievements | ✔ |
| Cheats RetroArch | ✔ |
| Controls | ✔ |
| Remapping | ✔ |
| Multi-Mouse | - |
| Sous-systèmes | ✔ |
| Softpatching | ✔ |
| Crop Overscan | ✔ |

## ![](/emulators/bios.svg) BIOS

>**Aucun bios n'est requis.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Extensions supportées

Les roms doivent avoir l'extension suivante :

* .fig
* .gd3
* .sfc
* .smc
* .swc
* .zip
* .7z

Ce système supporte les roms compressées au format .zip/.7z. Attention toutefois, il ne s'agit que d'une archive.

Les fichiers contenus dans les .zip/.7z doivent correspondre aux extensions citées précédemment.
De plus, chaque fichier .zip/.7z ne doit contenir qu'une seule rom compressée.

>Les jeux avec l'extension .fig, .gd3 et .swc doivent obligatoirement être décompressés !
{.is-info}

### Emplacement

Placez les roms comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 snes
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.zip**

>Les roms au format **No-Intro** sont vivement conseillées.
{.is-success}

>Pour plus d'information sur les roms, rendez-vous sur [ce tutoriel](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuration avancée de l'émulateur

>Pour pouvoir conserver vos configurations personnalisées lors d'une mise à jour, nous vous conseillons d'utiliser notre fonctionnalité [Surcharges de configuration](./../../../advanced-usage/configuration-override).
{.is-info}

### Accéder aux options

Vous pouvez configurer diverses options de deux façons différentes.

* Via le menu RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via le fichier `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Options du core

| Options | Valeurs des options | Variable (pour surcharge) | Valeurs des variables |
| --- | --- | --- | --- |
| Rapport d'aspect préféré | `Auto` ✅ / `Pixels parfaits à l'échelle` / `4:3` / `NTSC` / `PAL` | `bsnes_aspect_ratio` | `Auto` / `8:7` / `4:3` / `NTSC` / `PAL` |
| Recadrage du surbalayage | `8 pixels`✅ / `Désactivé` | `bsnes_ppu_show_overscan` | `rrr` / `disabled` |
| Émulation du flou | `Désactivé` ✅ / `Activé` | `bsnes_blur_emulation` | `disabled` / `enabled` |
| Filtre | `Aucune` ✅ / `NTSC (RF)` / `NTSC (Composite)` / `NTSC (S-Video)` / `NTSC (RVB)` | `bsnes_video_filter` | `None` / `NTSC (RF)` / `NTSC (Composite)` / `NTSC (S-Video)` / `NTSC (RGB)` |
| PPU - mode rapide | `Activé` ✅ / `Désactivé` | `bsnes_ppu_fast` | `enabled` / `disabled` |
| PPU - désentrelacer | `Activé` ✅ / `Désactivé` | `bsnes_ppu_deinterlace` | `enabled` / `disabled` |
| PPU - aucune limite de sprites | `Désactivé` ✅ / `Activé` | `bsnes_ppu_no_sprite_limit` | `disabled` / `enabled` |
| PPU - pas de blocage VRAM | `Désactivé` ✅ / `Activé` | `bsnes_ppu_no_vram_blocking` | `disabled` / `enabled` |
| DSP - mode rapide | `Activé` ✅ / `Désactivé` | `bsnes_dsp_fast` | `enabled` / `disabled` |
| DSP - interpolation cubique | `Désactivé` ✅ / `Activé` | `bsnes_dsp_cubic` | `disabled` / `enabled` |
| DSP - RAM de l'Echo séparée | `Désactivé` ✅ / `Activé` | `bsnes_dsp_echo_shadow` | `disabled` / `enabled` |
| Échelle | `240p (x1)` / `480p (x2)` ✅ / `720p (x3)` / `960p (x4)` / `1200p (x5)` / `1440p (x6)` / `1680p (x7)` / `1920p (x8)` | `bsnes_mode7_scale` | `1x` / `2x` / `3x` / `4x` / `5x` / `6x` / `7x` / `8x` |
| Correction de la perspective | `Activé` ✅ / `Désactivé` | `bsnes_mode7_perspective` | `enabled` / `disabled` |
| Suréchantillonnage | `Désactivé` ✅ / `Activé` | `bsnes_mode7_supersample` | `disabled` / `enabled` |
| Mosaïque HD->SD | `Activé` ✅ / `Désactivé` | `bsnes_mode7_mosaic` | `enabled` / `disabled` |
| Exécution en avance interne | `Désactivé ✅ / 1 / 2 / 3 / 4` | `bsnes_run_ahead_frames` | `disabled`/ `1` / `2` / `3` / `4` |
| Coprocesseurs - mode rapide | `Activé` ✅ / `Désactivé` | `bsnes_coprocessor_delayed_sync` | `enabled` / `disabled` |
| Coprocesseurs - préférer le mode HLE | `Activé` ✅ / `Désactivé` | `bsnes_coprocessor_prefer_hle` | `enabled` / `disabled` |
| Correctifs | `Désactivé` ✅ / `Activé` | `bsnes_hotfixes` | `disabled` / `enabled` |
| Entropie (aléatoire) | `Faible` ✅ / `Élevée` / `Aucune` | `bsnes_entropy` | `Low` / `High` / `None` |
| Calculs rapides du processeur | `Désactivé` ✅ / `Activé` | `bsnes_cpu_fastmath` | `disabled` / `enabled` |
| Processeur | `10%` / `20%` / `30%` / `40%` / `50%` / `60%` / `70%` / `80%` / `90%` / `100% (par défaut)` ✅ / `110%` / `120%` / `130%` / `140%` / `150%` / `160%` / `170%` / `180%` / `190%` / `200%` / `210%` / `220%` / `230%` / `240%` / `250%` / `260%` / `270%` / `280%` / `290%` / `300%` / `310%` / `320%` / `330%` / `340%` / `350%` / `360%` / `370%` / `380%` / `390%` / `400%` | `bsnes_cpu_overclock` | `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` / `11%` / `120` / `130` / `140` / `150` / `160` / `170` / `180` / `190` / `200` / `210` / `220` / `230` / `240` / `250` / `260` / `270` / `280` / `290` / `300` / `310` / `320` / `330` / `340` / `350` / `360` / `370` / `380` / `390` / `400` |
| Coprocesseur SA-1 | `10%` / `20%` / `30%` / `40%` / `50%` / `60%` / `70%` / `80%` / `90%` / `100% (par défaut)` ✅ / `110%` / `120%` / `130%` / `140%` / `150%` / `160%` / `170%` / `180%` / `190%` / `200%` / `210%` / `220%` / `230%` / `240%` / `250%` / `260%` / `270%` / `280%` / `290%` / `300%` / `310%` / `320%` / `330%` / `340%` / `350%` / `360%` / `370%` / `380%` / `390%` / `400%` | `bsnes_cpu_sa1_overclock` | `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` / `11%` / `120` / `130` / `140` / `150` / `160` / `170` / `180` / `190` / `200` / `210` / `220` / `230` / `240` / `250` / `260` / `270` / `280` / `290` / `300` / `310` / `320` / `330` / `340` / `350` / `360` / `370` / `380` / `390` / `400` |
| Coprocesseur SuperFX | `10%` / `20%` / `30%` / `40%` / `50%` / `60%` / `70%` / `80%` / `90%` / `100% (par défaut)` ✅ / `110%` / `120%` / `130%` / `140%` / `150%` / `160%` / `170%` / `180%` / `190%` / `200%` / `210%` / `220%` / `230%` / `240%` / `250%` / `260%` / `270%` / `280%` / `290%` / `300%` / `310%` / `320%` / `330%` / `340%` / `350%` / `360%` / `370%` / `380%` / `390%` / `400%` / `410%` / `420%` / `430%` / `440%` / `450%` / `460%` / `470%` / `480%` / `490%` / `500%` / `510%` / `520%` / `530%` / `540%` / `550%` / `560%` / `570%` / `580%` / `590%` / `600%` / `610%` / `620%` / `630%` / `640%` / `650%` / `660%` / `670%` / `680%` / `690%` / `700%` / `710%` / `720%` / `730%` / `740%` / `750%` / `760%` / `770%` / `780%` / `790%` / `800%` | `bsnes_cpu_sfx_overclock` | `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` / `11%` / `120` / `130` / `140` / `150` / `160` / `170` / `180` / `190` / `200` / `210` / `220` / `230` / `240` / `250` / `260` / `270` / `280` / `290` / `300` / `310` / `320` / `330` / `340` / `350` / `360` / `370` / `380` / `390` / `400` / `410` / `420` / `430` / `440` / `450` / `460` / `470` / `480` / `490` / `500` / `510` / `520` / `530` / `540` / `550` / `560` / `570` / `580` / `590` / `600` / `610` / `620` / `630` / `640` / `650` / `660` / `670` / `680` / `690` / `700` / `710` / `720` / `730` / `740` / `750` / `760` / `770` / `780` / `790` / `800` |
| BIOS préféré du Super Game Boy (Redémarrage requis) | `SGB1.sfc` ✅ / `SGB2.sfc` | `bsnes_sgb_bios` | `Super Game Boy (SGB1.sfc)` / `Super Game Boy 2 (SGB2.sfc)` |
| Masquer la bordure du SGB | `Désactivé` ✅ / `Activé` | `bsnes_hide_sgb_border` | `disabled` / `enabled` |
| Pistolet écran tactile | `Activé` ✅ / `Désactivé` | `bsnes_touchscreen_lightgun` | `enabled` / `disabled` |
| Inverser les touches de tir du Super Scope | `Désactivé` ✅ / `Activé` | `bsnes_touchscreen_lightgun_superscope_reverse` | `disabled` / `enabled` |

## ![](/emulators/external-links.png) Liens externes

* **Code source utilisé** : [https://github.com/libretro/bsnes-libretro/](https://github.com/libretro/bsnes-libretro/)
* **Documentation Libretro** : [https://docs.libretro.com/library/bsnes_performance](https://docs.libretro.com/library/bsnes_performance)