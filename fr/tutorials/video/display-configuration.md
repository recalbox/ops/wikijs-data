---
title: Configuration d'affichage
description: 
published: true
date: 2024-07-25T19:11:42.024Z
tags: configuration, affichage
editor: markdown
dateCreated: 2021-05-21T08:11:35.419Z
---

Vous y trouverez ici des tutoriels concernant la configuration de l'affichage.

Voici les tutoriels disponibles :

[Configurer un écran externe](external-screen-configuration)
[Guide complet sur la configuration vidéo](complete-video-configuration-guide)