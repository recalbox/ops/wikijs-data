---
title: Armazenamento
description: 
published: true
date: 2022-02-27T17:06:59.902Z
tags: compatibilidade, armazenamento
editor: markdown
dateCreated: 2022-02-27T17:06:59.902Z
---

### CARTÕES SD

A maioria dos cartões SD funcionará no seu Raspberry Pi. Se o seu cartão SD for maior que 32 GB, verifique [este link](https://www.raspberrypi.org/documentation/installation/sdxc_formatting.md).

Mas cuidado com falsificações (especialmente no Aliexpress, Ebay, Wish [e às vezes também na Amazon](https://www.cpchardware.com/when-amazon-opens-opens-a-swap-conf-sandisk/))!

| Dispositivo | Status | Última versão testada | Tamanho real |
| :---: | :---: | :---: | :---: |
| **Adata** | | | |
| Cartão microSDHC Adata Premier de 32 GB (UHS-I Classe 10) | ❌ | | |
| **Energizer** | | | |
| Cartão Micro SD de 8GB Energizer Classe 10 UHS-I | ✅ | | |
| **Kingston** | | | |
| Kingston SDC10/32GBSP Micro SD SDHC/SDXC Classe 10 UHS-I 32Go | ❌ | | |
| MicroSD Kingston (Classe 4) - 8 GB | ✅ | Recalbox 4/RPi 2B | 7,21GB / 4GB livres para jogos |
| Kingston SDC10/16GB Micro SD HC | ❌ | | |
| Kingston SDC10G2/16GB | ✅ | Recalbox 4.0.2/RPi 3B | 16 GB / 11 GB livres para jogos |
| Kingston SDCS/64GBSP Micro SD SDXC C10 UHS-I 64Go | ✅ | Rasp. Pi3 B - Recalbox 18.07.13 | 58 GB livres para jogos |
| **LD** | | | |
| LD MicroSD1 XC I / 64 GB | ✅ | Recalbox 18.06.27/RPi 3B | 64 GB |
| **Lexar** | | | |
| Lexar 633x UHC-i 16 GB | ❌ | | |
| **Netac** | | | |
| Netac 32 GB | ❌ | | |
| **Philips** | | | |
| Cartão microSDHC Philips Class10 32GB | ✅ | | |
| **PNY** | | | |
| CARTÃO PNY Elite microSDXC 64 GB Classe-10 UHS-I | ✅ | v18.04.20 | 56Gb livres para jogos |
| CARTÃO PNY Elite microSDXC 128 GB | ✅ | v4.0 final | 106gb livres para jogos |
| **QUMOX** | | | |
| CARTÃO DE MEMÓRIA MICRO SD QUMOX 64GB CLASSE 10 UHS-I | ✅ | | |
| **Samsung** | | | |
| Samsung plus (MB-MPAGC) (UHS-I Grau 0 Classe 10) | ✅ | | |
| Samsung Evo (MB-MP16D - MB-MP128D) (UHS-I Grau 1 Classe 10) | ✅ | | |
| Samsung Evo (MB-MP16DA - MB-MP128DA) (UHS-I Grau 1 Classe 10) | ✅ | | |
| Samsung Evo (MB-MP32D - MB-MP128D) (UHS-I Grau 1 Classe 10) | ✅ | | |
| Samsung Evo (MB-MC32GA - MB-MC256GA) (UHS-I Grau 1 Classe 10) | ✅ | | |
| Samsung Evo Select (MB-ME32GA - MB-ME256GA) (UHS-I Grau 1 Classe 10) | ✅ | Recalbox 17.12.02/RPi 3B | |
| **SanDisk** | | | |
| SanDisk Extreme Plus microSDHC | ✅ | | |
| SanDisk SDSQUNC MicroSDXC 128 GB-GZFMA Classe 10 | ✅ | | |
| SanDisk Ultra MicroSDXC 200 GB (UHS-I Grau 1 Classe 10) | ✅ | Recalbox 4.0.2/RPi 3B | 178 GB livres para jogos |
| SanDisk Ultra MicroSDHC 32GB UHS-I | ✅ | Recalbox 4.1.0/RPi 3B | 27,1 GB livres para jogos |
| SanDisk Ultra Plus 64GB UHS-1 Classe 10 | ✅ | | |
| SanDisk Ultra Series classe 10 | ✅ | | |
| SanDisk Ultra (GN6MA) (UHS-I Grau 1 Classe 10) | ✅ | | |
| SanDisk Extreme Pro U3 64GB | ✅ | | |
| **Strontium** | | | |
| Strontium 16GB MicroSDHC UHS-1 NITRO 433X Classe 10 | ✅ | | |
| Strontium 32GB MicroSDHC UHS-1 NITRO 466X Classe 10 | ✅ | | |
| **Toshiba** | | | |
| Toshiba microSDHC UHS-1 32GB Classe 10 | ✅ | Recalbox 4.1.0/RPi 3B | |
| **Transcend** | | | |
| Transcend 8GB e 32GB classe 10 | ✅ | | |
| Transcend 32GB (TS32GUSDU1PE) UHS-1 Classe 10 | ✅ | | |
| Transcend Premium 300x classe 10 | ❌ | | |
| **Verbatim** | | | |
| Cartão Micro SDHC Verbatim 32GB Classe 10 | ❌ | | |