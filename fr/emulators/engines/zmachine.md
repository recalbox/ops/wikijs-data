---
title: Z-Machine
description: 
published: true
date: 2023-06-27T19:24:32.427Z
tags: 9.0+, z-machine, infocom, moteur de jeu
editor: markdown
dateCreated: 2023-02-25T14:23:30.395Z
---

![](/emulators/fantasy/zmachine.svg){.align-center}

## Fiche technique

* **Développeur :** Joel Berez and Marc Blank
* **Année de sortie :** 1979
* **Périphériques :** clavier
* **Affichage :** 640x480 pixels

## Présentation

La Z-machine est une machine virtuelle développée par Joel Berez et Marc Blank en 1979, et utilisée par Infocom pour ses jeux d'aventure textuels.

Le jeu d'aventure textuel Zork fut originellement créé pour les mini-ordinateurs PDP-10, par Dave Lebling et Marc Blank, alors étudiants au MIT. Devant le succès du jeu, qui se répandit via l'Arpanet, ils participèrent à la création d'Infocom, une société de développement de jeux d'aventure textuels. L'idée était d'adapter Zork pour les ordinateurs personnels, mais le jeu original, codé en MDL (en), pesait 1 Mo, ce qui était bien trop lourd pour les micro-ordinateurs de l'époque. Il fut donc divisé en trois parties (Zork I, II et III), et un langage de programmation, le ZIL (Zork Implementation Language) fut créé à partir de MDL.

Afin de pouvoir commercialiser le jeu sur un grand nombre de plateformes et résoudre les problèmes de portabilité, il fut décidé de créer une machine virtuelle, concept relativement nouveau à l'époque, capable de lancer Zork et tout autre jeu codé en ZIL. Le jeu, compilé pour cette machine, pouvait fonctionner sur tout type de micro-ordinateur disposant d'un programme capable d’interpréter correctement les spécifications de celle-ci. C'est ainsi que les jeux Infocom sortirent sur un grand nombre de micros nord-américains (Atari 400/800, CP/M, IBM PC, TRS-80 Model II et III, NEC APC, DEC Rainbow, Commodore 64, TI Professional, DECmate, Tandy-2000, Kaypro II, Osborne 1, MS-DOS, TI 99/4A, Apple Macintosh, Epson QX-10, Apricot, Atari ST, Amiga ; on peut noter l'absence de l'Amstrad CPC ou l'Oric, plus populaires en Europe.

Plusieurs versions de la Z-machine furent développées par Infocom pendant les années 1980. La version 3 vit le jour en 1982, et permettait de jouer à des jeux n'excédant pas 128 Ko ; les jeux développés pour cette version étaient ceux de la série « Standard ». La version 4 vit le jour en 1985, avec A Mind Forever Voyaging, Trinity, et d'autres jeux de la série « Plus ». La version 5 apparut en 1987 avec Beyond Zork, et introduisit de nouvelles fonctionnalités et opcodes qui enrichissent la Z-machine (jeux de 256 Ko ou effets temporels).

Activision ayant racheté Infocom et voyant les ventes ralentir, voulut augmenter le rythme de production et la rentabilité. Ce qui conduisit à l'implantation de nouvelles fonctionnalités, pas forcément très heureuses, dépassant le cadre des jeux purement textuels.
La version 6 de la Z-machine vit le jour en 1988, et ajouta des capacités graphiques et la gestion de la souris, ce qui l'éloignait des jeux Infocom traditionnels ; seulement quatre jeux sortirent avant la fermeture du studio.

## Émulateurs

[Frotz](frotz)
[Libretro-mojozork](libretro-mojozork)