---
title: Raspberry Pi Imager
description: 
published: true
date: 2024-02-22T14:42:11.815Z
tags: pi, rapsberry, imager
editor: markdown
dateCreated: 2021-05-21T08:42:49.343Z
---

[Raspberry Pi Imager](https://www.raspberrypi.org/software/) (très simple d'utilisation), vous permet de flasher un fichier image sur le support désiré, une carte microSD par exemple. L'opération de flash d'une image est rapide et facile.

* Cliquez sur [Raspberry Pi Imager](https://www.raspberrypi.org/software/) pour vous rendre sur le lien de téléchargement puis installez-le sur votre ordinateur :

![](/tutorials/utilities/image-write/rpi-imager/rpi-imager1.png){.full-width}

* Une fois installé, l'utilitaire se présente de cette façon :

![](/tutorials/utilities/image-write/rpi-imager/rpi-imager2.png){.full-width}

* Cliquez sur `Choisissez l'OS`, sélectionnez le fichier à flasher, puis cliquez sur `Choisissez le stockage` pour lancer la procédure.

>Il est très vivement recommandé de ne brancher _**que le média de stockage que vous souhaitez flasher**_ afin d'éviter d'effacer vos données par erreur.
{.is-warning}

![](/tutorials/utilities/image-write/rpi-imager/rpi-imager3.png){.full-width}

* Vous pourrez alors cliquer sur `Écrire`. Raspberry Pi Imager se charge de tout, formatage, préparation des partitions, copie des fichiers, etc…

>Si vous avez un message vous demandant si vous souhaitez personnaliser votre installation, cliquez sur `Non`.
{.is-info}

![](/tutorials/utilities/image-write/rpi-imager/rpi-imager4.png){.full-width}

* Le programme indique le travail en cours.

![](/tutorials/utilities/image-write/rpi-imager/rpi-imager5.png){.full-width}

* Il vérifie ensuite l’image.

![](/tutorials/utilities/image-write/rpi-imager/rpi-imager6.png){.full-width}

* Et enfin signale le déroulement de l’opération.

![](/tutorials/utilities/image-write/rpi-imager/rpi-imager7.png){.full-width}

Comme indiqué, vous pouvez éjecter le support de stockage, il ne vous reste plus qu'à insérer la microSD dans votre console et à jouer !