---
title: Hyperion en Recalbox
description: 
published: true
date: 2024-07-26T07:10:24.672Z
tags: hyperion
editor: markdown
dateCreated: 2023-09-18T07:00:00.989Z
---

>Este tutorial no funciona más desde Recalbox 8.0 !
{.is-warning}

## ¡Haced brillar vuestra Recalbox con Hyperion!

[¡Hyperion](https://github.com/hyperion-project/hyperion) es un proyecto libre y de código abierto para transformar tu Raspberry Pi en un sistema **Ambilight** !

Con Hyperion y Recalbox, ¡transformaréis vuestro sistema de retrogaming y multimedia en una magnífica experiencia inmersiva!

## Descripción

En primer lugar, no estamos hablando de usar Hyperion con una parte de tu Recalbox como Kodi.  
¡Podrás usar las características de Hyperion en CADA JUEGO RETRO que juegues en Recalbox!

![Hyperion avec Recalbox](/tutorials/others/hyperion/pukerainbow.gif)

La imagen que véis en vuestro televisor (LCD o CRT) se ampliará dinámicamente en la parte de atrás de vuestra TV mediante varios LED RGB.

¿Qué aspecto tiene? Echa un vistazo a Sonic 3 en Sega Megadrive/Genesis :

![](/tutorials/others/hyperion/sonic1.jpg)

![](/tutorials/others/hyperion/sonic2.jpg)

![](/tutorials/others/hyperion/sonic3.jpg)

Aquí tenéis un vídeo:

[https://youtu.be/z1QblkdO6bs](https://youtu.be/z1QblkdO6bs)

## Hardware

Hyperion soporta muchas tiras de LED diferentes (ver [esta página](https://docs.hyperion-project.org/en/user/leddevices/)) y varios protocolos distintos:

* **SPI** : LPD6803, LPD8806, APA102, WS2812b et WS2801
* **USB** : AdaLight et SEDU
* **PWM** : WS2812b

Las siguientes pruebas se han realizado en WS2812b con PWM y SPI. Nunca hemos sido capaces de conseguir que PWM funcione correctamente, así que sólo describiremos cómo hacer la instalación Recalbox / Hyperion con WS2812b en modo SPI.  
Esta parte no existe en la documentación de Hyperion, así que explicaremos cómo hacer aquí.

El soporte para WS2812b con un cable sobre SPI fue añadido por [penfold42](https://github.com/penfold42) con [this commit](https://github.com/hyperion-project/hyperion/commit/a960894d140405e29edb413685e700d2f1714212). Gracias.

El siguiente tutorial utiliza un soldador. Si ya tenéis uno, el resto del hardware es bastante barato. Pero si queréis hacer algo sin soldar, consultad [este tutorial](https://hyperion-project.org/forum/index.php?thread/77).

### Los elementos necesarios

Necesitaréis:

* Vuestra Recalbox.
* Una tira de LED WS2812b.
* Una resistencia de 330 Ohm para poner en el pin de datos.
* Algo para cortar la tira.
* Un soldador.
* Cables o conectores para conectar la tira de LEDs.
* Un cambiador de nivel para cambiar el voltaje de datos a 5V, o un regulador de voltaje para reducir el VCC de los LEDs.
* Puentes DuPont para conectar fácilmente los cables a la Raspberry Pi.
* Una fuente de alimentación de 4A y 5V.

No dudéis en visitar [kubii.fr](https://kubii.fr/) para encontrar todo lo que necesitas.

Por supuesto, si queréis ahorrar dinero podéis:

* Soldar directamente (sin conectores ni DuPont)
* Utilizar una fuente de alimentación de 3A.

Hemos probado nuestra tira Hyperion 220 LED con una Aukru 5V 3A y funcionaba casi bien, pero cuando todos los LED son blancos, el final de la tira es un poco amarillo...

## Cableado

### LEDs

Medid bien los bordes de vuestro televisor y cortad 1 tira por borde según esta medida.

Soldad las tiras entre sí o bien soldad los conectores a las tiras. Seguid las flechas de la tira para saber qué conector utilizar (IN o OUT).

Aquí tenéis una vista previa del resultado:

![Connecteurs](/tutorials/others/hyperion/connectors.jpg)

![Soudure](/tutorials/others/hyperion/solder.jpg)

### El sistema

Para permitir que la Raspberry Pi envíe datos a los LEDs, hay las siguientes opciones:

* Reducir el voltaje de los LEDs a 4.7V usando un conversor
* Ajustar la señal GPIO a 5V

#### Regulador de voltaje

![wiring-regulator.png](/tutorials/others/hyperion/wiring-regulator.png)

Si utilizáis una Raspberry Pi 2 o 3, tenéis más GPIOs pero la disposición no cambia. El Raspberry Pi está conectado a su propia fuente de alimentación y a la tierra de la fuente de alimentación del LED. El Raspberry Pi envía los datos a través del GPIO SPI MOSI al cable de datos del LED. El regulador de voltaje se conecta entre los LEDs y su fuente de alimentación.

#### Cambiador de nivel (no probado)

Si queréis regular el voltaje de los GPIOs, utilizad el diagrama anterior, retirad el regulador y conectad el variador de nivel entre el GPIO MOSI y el cable de datos del LED.

## Configuración

**Hypercon** es una interfaz gráfica que os permite configurar una instalación Hyperion y crear el fichero de configuración que se colocará dentro de Recalbox.

![](/tutorials/others/hyperion/hypercon.jpg)

### Descarga y arranque

Descargad e iniciad Hypercon siguiendo el [tutorial oficial](https://docs.hyperion-project.org/en/user/Installation.html).

![](https://gblobscdn.gitbook.com/assets%2F-LdKTX4ollh_G72-pO8z%2F-MJOZB8uJ0fcgdF-EvJd%2F-MJO_1HkQUXpWc1uWUwn%2Fimage.png?alt=media&token=7c07bd76-2497-45fd-b8f1-399a1fd4c403)

Existen muchísimos recursos e informaciones detalladas sobre la configuración de Hypercon en la [documentación de Hyperion](https://docs.hyperion-project.org/en/user/Configuration.html).

### Configuración para Recalbox

Aquí tenéis cómo configurar Hypercon para vuestra Recalbox y el WS2812b en modo SPI :

* **Hardware**
  * Type -> WS281X-SPI
  * RGB Byte Order -> GRB
  * Construction and Image Process -> configure depending of your installation
  * Blackborder Detection -> Enabled
    * Threshold -> 5
    * mode -> osd
* **Process**
  * _Smoothing_
    * Enabled -> True
    * Time -> 200ms
    * Update Freq -> 20
    * Update Delay -> 0
* **Grabber**
  * _Internal Frame Grabber_
    * Enabled -> True
    * Width -> 64
    * Height -> 64
    * Interval -> 200
    * Priority Channel -> 890
  * _GrabberV4L2_
    * Enabled -> False
* External
  * Booteffect / Static Color -> configure the boot effect you like here!

Pulsad el botón `Create Hyperion Configuration`. Grabad el fichero json como `hyperion.config.json`.

Copiad este fichero en vuestra Recalbox dentro de `/recalbox/share/system/config/hyperion/`

#### Activación del servicio Hyperion en el recalbox.conf

* Activad Hyperion dentro del fichero [recalbox.conf](./../../basic-usage/getting-started/recalboxconf-file) cambiando la línea `hyperion.enabled=0` a `hyperion.enabled=1`.
* Modificad el fichero `recalbox-user-config.txt` de la [partición de arranque](./../../tutorials/system/access/remount-partition-with-write-access) y añadid la línea `dtparam=spi=on` :

```shell
echo 'dtparam=spi=on' >> /boot/recalbox-user-config.txt
```

* Reiniciad Recalbox.

## Disfrutad del arco iris en vuestro televisor

¡Ahora tenéis un sistema Ambilight totalmente funcional! ¡Probad varios juegos retro, películas y anime con Kodi y dejad que la belleza de los colores reflejados en vuestra pared realce vuestra experiencia multimedia!

![](/tutorials/others/hyperion/nyan.jpg)