---
title: Connect an IPEGA controller
description: 
published: true
date: 2021-08-07T14:30:11.982Z
tags: controller, ipega
editor: markdown
dateCreated: 2021-08-07T14:30:11.982Z
---

## Tips for IPEGA controllers

* Connect via [SSH](./../../system/access/root-access-terminal-cli) to your Recalbox and mount the [system partition](./../../system/access/remount-partition-with-write-access) as read/write.
* Find your controller reference via the command :

```shell
evtest
```

Example: `PG-9055

* Adapt `ATTRS{name}=="PG-9055"` to your controller's part number and copy/paste all of these commands into SSH, then press `Enter`.

```text
echo 'SUBSYSTEM=="input", ATTRS{name}=="PG-9055", MODE="0666", ENV{ID_INPUT_JOYSTICK}="1"'>>/etc/udev/rules.d/99-joysticks-exotics.rules
reboot
```

## Bluetooth connection

* Reset your IPEGA controller, if you are using it with another device.
* Set it to `Home+X` mode
* Run the bluetooth controller search in the Emulationstation menu by pressing `START` > `HANDSET SETTINGS`.

You can check if your IPEGA is already listed in the `/etc/udev/rules.d/99-joysticks-exotics.rules` file.

If it is not, you can [open an issue](https://gitlab.com/recalbox/recalbox/-/issues) to have it added to the next Recalbox update.