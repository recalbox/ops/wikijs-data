---
title: Virtual Boy
description: 
published: true
date: 2021-08-09T22:06:05.880Z
tags: consoles, nintendo, virtual boy
editor: markdown
dateCreated: 2021-07-28T18:12:54.623Z
---

![](/emulators/consoles/virtualboy.svg){.align-center}

## Technical data

* **Manufacturer**: Nintendo
* **Year of release**: 1995
* **Units sold**: 800 000
* **Best-selling game**: VB Wario Land
* **CPU**: 32Bit NEC V810 RISC @ 20MHz
* **RAM**: 1Mbits DRAM 512kb P-SRAM
* **Colors**: 4 colors / 32 intensity levels
* **Resolution**: 384 x 224 pixels
* **Screen**: 2 RTI light-emitting LEDs
* **Sound**: 16Bits stereo

## Presentation

The Virtual Boy** is a video game console created by Nintendo and released in 1995 in Japan and the United States, in the form of a 3D video headset.

Attempting to innovate with a stereoscopic display, the console was ultimately a commercial failure. Despite several price cuts, sales did not take off and the marketing of the console in Europe was cancelled. The project was then abandoned by Nintendo in 1996.

With 800,000 units sold, the Virtual Boy is the lowest selling console in the history of Nintendo's video game consoles.

The controller was however very appreciated by users because of its ergonomics and nevertheless very pleasant to use.

## Emulators

[Libretro Mednafen_VB](libretro-mednafen_vb)