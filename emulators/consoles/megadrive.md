---
title: Genesis
description: Sega Genesis
published: true
date: 2025-01-05T23:27:27.399Z
tags: genesis, consoles, sega
editor: markdown
dateCreated: 2021-06-29T16:51:56.287Z
---

![](/emulators/consoles/genesis.svg){.align-center}

## Technical data

* **Manufacturer**: Sega
* **Year of release**: 1988
* **Units sold**: 30 million
* **Best-selling game**: Sonic the Hedgehog
* **CPU**: 16-bit Motorola 68000 @ 7.6 MHz
* **CPU2**: 8-bit Zilog Z80 @ 3.58 Mhz (sound CPU)
* **RAM**: 64 kB
* **VRAM**: 64kB
* **Sound RAM**: 8kB
* **Video**: 16-bit Sega VDP
* **Resolution**: 320×224 pixels, 64 colors (512 colors palette)
* **Sound**: Yamaha YM2612 FM and TI SN76489 PSG

## Presentation

The **Sega Genesis**, known as the **Mega Drive** outside North America, is a 16-bit fourth-generation home video game console developed and sold by Sega. The Genesis was Sega's third console and the successor to the Master System. Sega released it in 1988 in Japan as the Mega Drive, and in 1989 in North America as the Genesis. In 1990, it was distributed as the Mega Drive by Virgin Mastertronic in Europe, Ozisoft in Australasia, and Tec Toy in Brazil. In South Korea, it was distributed by Samsung as the **Super Gam\*Boy** and later the **Super Aladdin Boy**.

Designed by an R&D team supervised by Hideki Sato and Masami Ishikawa, the Genesis was adapted from Sega's System 16 arcade board, centered on a Motorola 68000 processor as the CPU, a Zilog Z80 as a sound controller, and a video system supporting hardware sprites, tiles, and scrolling. It plays a library of more than 900 games on ROM-based cartridges. Several add-ons were released, including a Power Base Converter to play Master System games. It was released in several different versions, some created by third parties. Sega created two network services to support the Genesis: Sega Meganet and Sega Channel.

In Japan, the Mega Drive fared poorly against its two main competitors, Nintendo's Super Famicom and NEC's PC Engine, but it achieved considerable success in North America, Brazil, and Europe. Contributing to its success was its library of arcade game ports, the popularity of Sega's Sonic the Hedgehog series, several popular sports franchises, and aggressive youth marketing that positioned it as the cool console for adolescents. The North American release in 1991 of the Super Famicom, rebranded as the Super Nintendo Entertainment System, triggered a fierce battle for market share in the United States and Europe known as the "console war". As this contest drew increasing attention to the video game industry among the general public, the Genesis and several of its highest-profile games attracted significant legal scrutiny on matters involving reverse engineering and video game violence. Controversy surrounding violent games such as Night Trap and Mortal Kombat led Sega to create the Videogame Rating Council, a predecessor to the Entertainment Software Rating Board.

30.75 million first-party Genesis units were sold worldwide. In addition, Tec Toy sold an estimated three million licensed variants in Brazil, Majesco projected it would sell 1.5 million licensed variants of the system in the United States, and much smaller numbers were sold by Samsung in South Korea. By the mid-2010s, licensed third-party Genesis rereleases were still being sold by AtGames in North America and Europe. Many games have been re-released in compilations or on online services such as the Nintendo Virtual Console, Xbox Live Arcade, PlayStation Network, and Steam. The Genesis was succeeded in 1994 by the Sega Saturn.

## Emulators

[Libretro Picodrive](libretro-picodrive)
[Libretro GenesisplusGX](libretro-genesisplusgx)
[Libretro GenesisplusGX Wide](libretro-genesisplusgxwide)
[Libretro FBNeo](libretro-fbneo)