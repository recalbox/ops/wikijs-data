---
title: Romulus para el Arcade
description: Utilización de Romulus Rom Manager para los sitemas de Arcade (MAME, FinalBurn Neo, ...)
published: true
date: 2023-09-16T20:19:25.716Z
tags: arcade, romulus, mame, fbneo
editor: markdown
dateCreated: 2023-09-16T20:19:25.716Z
---

>Por favor, consulta la sección [Romulus para consolas] (./romulus-for-consoles) para conocer el funcionamiento básico de Romulus (añadir y configurar un .dat, reconstruir un romset, etc.).
{.is-info}

## Modos de archivo

Los romsets de Arcade se componen de muchos juegos, algunos de los cuales tienen una base común. Se habla de juegos "Padre/Clon", donde "Padre" se refiere al juego principal (el 1er juego publicado, por ejemplo) y "Clon" se refiere a todos los derivados de este juego (otro idioma, otra región, otro nombre, etc.).

Además, a diferencia de los romsets de consola, donde la norma es "1 juego = 1 rom", los juegos arcade utilizan varias roms para constituir el juego.

En Romulus, hay 3 modos posibles para reconstituir estos juegos (en otros gestores de roms se utilizan estos mismos modos).

### Detalles de los 3 modos

![main set = parent  /  clone set = clone](/tutorials/utilities/rom-management/romulus/arcade/romulus1.png)

* **Split / Not Merged** : Todas las roms requeridas para hacer funcionar el juego se encuentran dentro del fichero (o la carpeta) del juego. Estas romset utilizan más espacio de almacenamiento porque la roms comunes a varios juegos están presentes en cada uno de ellos.
* **Split / Merged** : Tomas las roms requeridas para hacer funcionar el juego Padre se encuentran en el fichero (o la carpeta) del juego. El fichero o la carpeta del juego Clon sólo contiene las roms exclusivas de él mismo, pero para funcionar, el juego Clon necesita también las roms presentes en el juego Padre.
* **Not Split** : Todas las roms necesarias para hacer funcionar el juego (que sea Padre o Clon) se encuentran mezcladas en el mismo fichero o carpeta del juego.

>Para usar los juegos en Recalbox, recomendamos usar el modo **Split / Merged**.
{.is-info}

## Los juegos de Arcade en Romolus

![](/tutorials/utilities/rom-management/romulus/arcade/romulus2.png)

* **Línea azul resaltada** : La columna vacía "**Clon de**" indica que se trata de un juego padre. El icono verde de la izquierda indica que el juego está completo (icono amarillo = juego incompleto, icono rojo = juego ausente).
* **A**: Esta tabla contiene detalles del juego seleccionado. Se trata de una lista de las roms del juego. El punto verde indica que la rom está presente y se ajusta al fichero .dat (punto rojo = rom ausente).
* **B** : La columna "**Clon de**" no está vacía, se trata de juegos clones. El nombre del juego padre se indica en esta columna.

## Ficheros Sample y CHD

![](/tutorials/utilities/rom-management/romulus/arcade/romulus3.png)

Al configurar el fichero .dat para el arcade (ver [Configurar el archivo .dat añadido](./romulus-for-consoles#configuring-the-added-dat-file)), puede ser necesario definir otros directorios además del de las roms: un directorio para las **Muestras** (archivos de audio necesarios para ciertos juegos) y un directorio para los **CHD** (imágenes de CD complementarias a la rom necesarias para ejecutar los juegos más recientes).