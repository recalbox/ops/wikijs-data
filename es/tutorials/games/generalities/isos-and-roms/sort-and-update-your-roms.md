---
title: Ordenar y actualizar las roms
description: 
published: true
date: 2024-06-22T13:58:26.027Z
tags: roms, ordenar, actualizar
editor: markdown
dateCreated: 2023-09-09T12:21:46.484Z
---

Para ordenar y actualizar vuestros juegos, necesitaréis un programa, un fichero dat y dependiendo de las consolas un fichero header.

>Es importante actualizar las roms porque las nuevas suelen corregir bugs y están mejor adaptadas para funcionar en los emuladores.
>
>Aunque estas actualizaciones no son frecuentes, son necesarias para hacer funcionar los juegos en ciertos emuladores y son esenciales para jugar en modo Netplay.
{.is-warning}

## Los programas

Existen varios programas para ordenadar y actualizar las roms

* [Clrmamepro](https://mamedev.emulab.it/clrmamepro/)
* [Romulus](https://romulus.dats.site/)
* [Universal Rom Cleaner](https://github.com/Universal-Rom-Tools/Universal-ROM-Cleaner/releases)

>Para los principiantes, se aconseja utilizar Romulus porque presenta una interfaz más sencilla.
{.is-info}

## Los ficheros dat

Un fichero dat es un fichero de referencia para catalogar los juegos por sistema (consola). Se trata de un fichero con extensión .dat (de ahí su nombre) o también .xml. 

Usando este fichero en los anteriores programas, podéis verificar si vuestro volcado de rom o de iso es correcto.

Para las roms de arcade, los ficheros .dat se encuentran en vuestra Recalbox en los siguientes directorios:

 * **AdvanceMAME** : dentro de Recalbox, en `/recalbox/share/arcade/advancemame/advancemame.dat`
 * **Atomiswave** : dentro de Recalbox, en `/recalbox/share/arcade/libretro/atomiswave.dat`.
 * **FBNeo** : dentro de Recalbox, en `/recalbox/share/arcade/libretro/fbneo.dat`.
 * **MAME** : dentro de Recalbox, en `/recalbox/share/arcade/libretro/mame.dat`.
 * **MAME 2000** : dentro de Recalbox, en `/recalbox/share/arcade/libretro/mame2000.dat`.
 * **MAME 2003 Plus** : dentro de Recalbox, en `/recalbox/share/arcade/libretro/mame2003-plus.dat`.
 * **MAME 2003** : dentro de Recalbox, en `/recalbox/share/arcade/libretro/mame2003.dat`.
 * **MAME 2010** : dentro de Recalbox, en `/recalbox/share/arcade/libretro/mame2010.dat`.
 * **MAME 2015** : dentro de Recalbox, en `/recalbox/share/arcade/libretro/mame2015.dat`.
 * **Model3** : dentro de Recalbox, en `/recalbox/share/arcade/supermodel/supermodel.dat`.
 * **Naomi** : dentro de Recalbox, en `/recalbox/share/arcade/libretro/naomi.dat`.
 * **Naomi GD-Rom** : dentro de Recalbox, en `/recalbox/share/arcade/libretro/naomigd.dat`.
 * **Naomi 2** : dentro de Recalbox, en `/recalbox/share/arcade/libretro/naomi2.dat`.
 * **Neo Geo** : dentro de Recalbox, en `/recalbox/share/arcade/libretro/fbneo-non-arcade/FinalBurn Neo (ClrMame Pro XML, Neogeo only).dat`.
 
>El acceso a estos ficheros se realiza unicamente a través de la red. 
{.is-info}


Para las consolas de salón o portátiles y para los ordenadores, podéis descargar los ficheros de las siguientes páginas web:

* **No-Intro** (consoles de cartuchos) : [https://datomatic.no-intro.org/?page=download](https://datomatic.no-intro.org/?page=download)
* **Redump** (consoles de disco) : [https://redump.org/downloads/](https://redump.org/downloads/)
* **TOSEC** (ordenadores) : [https://www.tosecdev.org/](https://www.tosecdev.org/)

Los dos enlaces que os presentamos a continuación sirven únicamente para mejorar vuestra comprensión del sistema:

* **Trurip** : [https://database.trurip.org/](https://database.trurip.org/)
* **GoodSets/Goodtools** : [https://cowering.blogspot.com/](https://cowering.blogspot.com/)

Verificad bien que seleccionáis el fichero dat que corresponde a vuestras roms!

## Los ficheros header

En algunas consolas necestaréis además del fichero dat un fichero header (fichero de cabecera).

Aquí tenéis la lista de consolas que lo necesitan:

* Atari - 7800
* Atari - Lynx
* Nintendo - Family Computer Disk System
* Nintendo - Nintendo Entertainment System

## Las preferencias de clasificación

### National o international ?

* **National** : si sólo os interesa jugar en Netplay con gente de vuestro país, entonces podéis guardar únicamente las roms de vuestro país y seleccionar las roms USA y/o Japón para los juegos no disponibles en vuestro país.
* **International** : si queréis jugar en Netplay con personas de otros países, entonces guardad una rom de cada país.

Ejemplo internacional:

* Super Street Fighter II (Europe)
* Super Street Fighter II - The New Challengers (Japan)
* Street Fighter II Plus - Champion Edition (Japan, Korea, Asia)

### Los Retroachievements

Algunos premios y recompensas de Retroachievements necesitan una rom de USA, mientras que otros juegos sólo los encontraréis en versión japonesa.  

Encontraréis más información [aquí](./../../../../basic-usage/features/retroachievements).

### Los juegos y las regiones

Dependiendo de la región geográfica donde ha sido editado el juego, pueden existir diferencias más o menos flagrantes. Además, muchos juegos sólo existen en versión japonesa.

Aquí tenéis algunos ejemplos de estas diferencias:

* Boss diferente.
* Color de la sangre diferente (rojo en USA y verde en europa)
* Fps más o menos elevados (60 fps para los juegos NTSC y 50 fps para los PAL)
* Frécuencia (MHz) más o menos elevada.
* Numéro de jugadores distinto.

### Los códigos y etiquetas (tags) en las roms

[En este enlace](./../tags-used-in-rom-names) encontraréis más información para saber a qué corresponden los códigos y etiquetas presentes en los nombres de ficheros de las roms (únicamente para las roms de cartuchos y de discos).