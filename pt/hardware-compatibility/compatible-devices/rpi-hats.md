---
title: Raspberry PI hats
description: Lista de hats suportados
published: true
date: 2021-11-28T23:12:59.576Z
tags: rpi, hats, 8.0+, compatibilidade
editor: markdown
dateCreated: 2021-11-28T23:12:31.581Z
---

# HATs Raspberry PI compatíveis com Recalbox

## Controladores PoE

Os controladores PoE permitem que o Raspberry PI seja alimentado por meio do conector Ethernet. Não há necessidade de fonte de alimentação mini-USB ou USB-C!

Para alimentar o RPI, você deve ter um switch Ethernet compatível com IEEE 802.3af ou IEEE 802.3at.

### Waveshare PoE hat (b)

Este hat permite que o Rpi seja alimentado por ethernet e tem uma tela monocromática, bem como uma ventoinha de corrente contínua programável (liga / desliga). É possível forçar a ativação da ventoinha por meio de um interruptor na placa.

Este hat é compatível com RPI 3b+ e RPI 4.

O utilitário [recalbox-wpaf](/fr/advanced-usage/fan-controller) suporta este hat e permite controlar a ventoinha (ativação / desativação de acordo com a temperatura da CPU).

### Raspberry PI PoE+

Este hat permite alimentar o RPI pela Ethernet e possui ventoinha de velocidade variável (PWM).

Este hat é compatível com RPI 3b+ e RPI4.

O utilitário [recalbox-wpaf](/fr/advanced-usage/fan-controller) suporta este hat e permite controlar a ventoinha (ativação / desativação de acordo com a temperatura da CPU).

## Placas com ventoinha

Este tipo de placa possui uma ventoinha que resfria a CPU.

### Pimoroni fan SHIM

Esta placa é anexada a uma parte do conector GPIO do Raspberry PI. Possui ventoinha silenciosa, LED RGB e botão. A função da ventoinha é suportada pelo utilitário [recalbox-wpaf](/fr/advanced-usage/fan-controller).