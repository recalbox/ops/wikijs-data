---
title: Videopac
description: Philips Videopac
published: true
date: 2024-07-26T08:07:04.580Z
tags: philips, videopac, consolas
editor: markdown
dateCreated: 2023-09-03T20:30:35.429Z
---

![](/emulators/consoles/videopac.svg){.align-center}

## Datos técnicos

* **Fabricante :** Magnavox/Philips
* **Año de lanzamiento :** 1978
* **Cantidad vendida :** 2 millones
* **Procesador :** Intel 8048 a 1,79 MHz
* **RAM:** 64 bytes
* **Audio / Video RAM:** 128 bytes
* **ROM :** 1024 bytes
* **Resolución:** 160×200 píxeles con 12 colores
* **Pantalla:** paleta fija de 16 colores; los sprites sólo pueden usar 8 de estos colores

## Presentación

La **Videopac** es una consola de videojuegos diseñada por Magnavox, fabricada por Philips y comercializada a finales de los años 70 y principios de los 80 bajo las marcas Philips (Videopac/Odyssey), Radiola-Radiotechnique (Videopac), Schneider (Videopac), Siera (Videopac), Magnavox (Odyssey²), Brandt (Jopac), Continental Edison (Jopac) y Saba (Jopac).

## Emuladores

[Libretro O2EM](libretro-o2em)