---
title: Convert a .GME save to .SRM
description: 
published: true
date: 2021-08-08T14:21:28.930Z
tags: ps1, convert, save, gme, srm
editor: markdown
dateCreated: 2021-08-08T14:21:28.930Z
---

It is possible to use Dexdrive (`.GME`) format backups and convert them to `.SRM` format for RetroArch.

* Download the software [MemcardRex v1.8](http://www.mediafire.com/download/7ytiync7oxuhw4m/MemcardRex%201.8.zip) and unzip it.
* Click on the executable MemcardRex.exe.
* In the menus, click on `File` and then click on `Open`. Select your `.gme' backup file to convert.
* Choose `File` then click `Save as...`. Name your file after the name of the game. For example **gamename.mcr**.
* Simply rename the file **gamename.mcr** to **gamename.srm**.
* Finally, place your newly created backup in the `/recalbox/share/saves/psx/` directory.