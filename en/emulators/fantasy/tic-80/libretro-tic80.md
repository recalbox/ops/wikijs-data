---
title: Libretro Tic80
description: 
published: true
date: 2021-10-04T22:39:14.561Z
tags: libretro, tic-80, tic80
editor: markdown
dateCreated: 2021-07-04T18:21:52.322Z
---

Libretro TIC-80 is an emulator that allows you to emulate a fantastic computer to create, play and share small games.

## ![](/emulators/license.svg) License

This core is under [**MIT**](https://github.com/libretro/TIC-80/blob/master/LICENSE) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Features

| Feature | Supported |
| :---: | :---: |
| Restart | ✔ |
| Saves | ✔ |
| Core Options | ✔ |
| Netplay | ✔ |
| Native Cheats | ✔ |
| Controls | ✔ |

## ![](/emulators/bios.svg) BIOS

>**No bios is required.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Supported extensions

Roms must have the extension:

* .fd
* .sap
* .k7
* .m7
* .rom
* .zip
* .7z

This system supports compressed roms in .zip/.7z format. However, be careful, it is only an archive.

The files contained in the .zip/.7z must correspond to the extensions mentioned above.
Moreover, each .zip/.7z file must contain only one compressed rom.

### Location

Put your roms like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 tic80
┃ ┃ ┃ ┃ ┣ 🗒 **game.zip**

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Accessing to options

You can configure various options in two different ways.

* Via RetroArch menu:

┣ 📁 RetroArch Menu
┃ ┣ 📁 Core options
┃ ┃ ┣ 🧩 Name_option

* Via the file `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core options

| Options | Options values | Variables (for configuration override) | Variables values |
| --- | --- | --- | --- |
| Touche Interface for Mouse | `Disabled` ✅ / `Enabled` | `tic80_mouse_pointer` | `disabled` / `enabled` |
| Mouse Cursor | `Disabled` ✅ / `Enabled` | `tic80_mouse_cursor` | `disabled` / `enabled` |
| Mouse Cursor Hide Delay | `Disabled` / `1` / `2` / `3` / `4` / `5` ✅ / `6` / `7` / `8` / `9` / `10` | `tic80_mouse_hide_delay` | `disabled` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` |

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/libretro/TIC-80/](https://github.com/libretro/TIC-80)
* **Official source code**: [https://github.com/nesbox/TIC-80/](https://github.com/nesbox/TIC-80/)
* **Official website**: [https://tic.computer/](https://tic.computer/)
* **Official wiki**: [https://github.com/nesbox/TIC-80/wiki](https://github.com/nesbox/TIC-80/wiki)
* **Official Libretro database**: [Libretro TIC-80 Database](https://github.com/libretro/libretro-database/blob/master/rdb/TIC-80.rdb)
* **Libretro Tic-80 cheats**: [https://github.com/libretro/libretro-database/tree/master/cht/TIC-80/](https://github.com/libretro/libretro-database/tree/master/cht/TIC-80)