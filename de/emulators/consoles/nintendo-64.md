---
title: Nintendo 64
description: 
published: true
date: 2024-06-02T20:58:13.454Z
tags: 
editor: markdown
dateCreated: 2021-06-21T14:16:02.287Z
---

![](/emulators/consoles/nintendo64.svg){.align-center}

## Technische Angaben

* **Hersteller** : Nintendo
* **Jahr der Veröffentlichung** : 1996
* **Verkaufte Einheiten** : 32.93 millions
* **Meistverkauftes Spiel** : Super Mario 64
* **Prozessor** : 64-bit NEC VR4300 @ 93.75 MHz
* **Co-Prozessor** : 64-bit MIPS RISC "Reality Immersion" RCP @ 62.5 MHz
* **RAM** : 4 MB RAMBUS RDRAM (modifiable pour 8 MB)
* **Sound-Chip** : Stereo 16-Bit et 48 kHz

## Beschreibung

In den frühen 1990er Jahren dominierte Nintendo mit dem Erfolg der **NES**- und **Super Nintendo**-Konsolen den weltweiten Videospielmarkt, trotz der starken Konkurrenz von Sega.

Das Aufkommen der CD-ROM veranlasste Nintendo, sich mit Sony zusammenzutun, um einen CD-Player für das Super Nintendo zu entwickeln, die **SNES-CD**, besser bekannt als die **Nintendo PlayStation**, um mit NECs **PC-Engine** und Segas **Mega-CD** zu konkurrieren.

Doch ein Streit führte dazu, dass Nintendo und Sony ihr gemeinsames Projekt aufgaben, was Sony dann zur Entwicklung einer eigenen Konsole, der **PlayStation**, nutzte. Nintendo wandte sich dann an die niederländische Firma Philips, um das Projekt fortzusetzen.

Die Ankündigung der PlayStation im Jahr 1991, zusammen mit den Misserfolgen von Segas **Mega-CD** und Philips' **CD-i**, veranlasste Nintendo, die CD-ROM-Erweiterung des **Super Nintendo** endgültig zu begraben und sich der Entwicklung einer völlig neuen Konsole zuzuwenden.

## Emulatoren

[Libretro mupen64plus_Next](libretro-mupen64plus_next)
[Libretro ParaLLEl_n64](libretro-parallel_n64)
[Mupen64plus GLide64MK2](mupen64plus-glide64mk2)
[Mupen64plus GLideN64](mupen64plus-gliden64)
[Mupen64plus GLideN64_20](mupen64plus-gliden64_20)
[Mupen64plus N64_GLES2](mupen64plus-n64_gles2)
[Mupen64plus RICE](mupen64plus-rice)
[Mupen64plus RICE_GLES2](mupen64plus-rice_gles2)