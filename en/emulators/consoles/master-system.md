---
title: Master System
description: Sega Master System
published: true
date: 2021-10-03T18:45:31.850Z
tags: system, consoles, sega, master
editor: markdown
dateCreated: 2021-07-27T20:08:39.825Z
---

![](/emulators/consoles/mastersystem.svg){.align-center}

## Technical data

* **Manufacturer**: Sega
* **Year of release**: 1985
* **Units sold**: 13 millions
* **Best-selling game**: Alex Kidd in Miracle World
* **CPU**: 8-Bit Zilog Z80 @ 3.58 MHz
* **RAM**: 8kB
* **VRAM**: 16kB
* **Video**: Sega VDP (custom Texas Instruments TMS9918)
* **Resolution**: 256 × 192 pixels, 32 colors (64 colors palette)
* **Sound**: TI SN76489 PSG / add-on module YM2413 FM
* **Cart size**: 32kB - 1MB

## Presentation

The **Master System** or **Sega Master System** (also abbreviated **SMS**) is a third-generation video game console designed and marketed by the Japanese manufacturer Sega Enterprises, Ltd. It was released in 1985, under the name **Sega Mark III** in Japan. The console is redesigned and renamed Master System before its launch in 1986 in North America. It was released under this name in most other territories, including Europe in 1986 and Brazil in 1987. The redesigned Master System was also released in 1987 in Japan.

Both initial models of the console can read game cartridges and Sega Cards, memory cards that can store games, sold at lower prices than cartridges but with less storage capacity; the Master System II and later models do not have a slot for these cards. The Master System is also compatible with accessories such as an optical gun and stereoscopic glasses designed to work with a range of specially coded games.

The successor to the SG-1000, the Master System is billed as the direct competitor to the Nintendo Entertainment System (NES) during the third generation video game console era. The Master System was built with technologically superior hardware to that used by the NES, but it failed to reverse the significant market share advantage held by Nintendo in Japan and North America. However, the console was more successful in Europe and Brazil.

The Master System's hardware shares many similarities with Sega's handheld game console, the Game Gear. Compared to its Nintendo competitor, the Master System's game library lacks several critically acclaimed titles due to Nintendo's licensing practices that restrict third-party developers to create their games solely for the NES. It is estimated that between 18 and 21 million units of the console are sold during its life cycle. In comparison, the NES had 62 million units sold during that period. In retrospect, observers believe that the Master System allowed Sega to establish itself in new markets, in preparation for the marketing of the Mega Drive.

Thanks to its exploitation by Tec Toy in Brazil, the Master System is considered to be the video game console with the longest life.

## Emulators

[Libretro PicoDrive](libretro-picodrive)
[Libretro GenesisPlusGX](libretro-genesisplusgx)
[Libretro Gearsystem](libretro-gearsystem)
[Libretro FBNeo](libretro-fbneo)