---
title: Configuración de shaders
description: 
published: true
date: 2023-09-07T17:08:10.492Z
tags: shaders, configuración
editor: markdown
dateCreated: 2023-09-07T17:04:18.155Z
---

## Introducción

Los shaders son filtros que modifican la imagen generada por los emuladores. Se utilizan para aplicar efectos "rétro" para simular la imagen generada por las pantallas CRT de la época. Pueden invertir la pantalla horizontal y/o verticalmente, y modificar los colores.

En Recalbox, existen dos tipos de ficheros diferentes:

*  **GLSL** : el shader en sí mismo.
*  **GLSLP** : Una configuración para un shader o un conjunto de shaders (GLSL Preset). Es un fichero de texto que incluye metadata para configurar ciertos shaders y combilarlos entre ellos..
* /recalbox/share/system/configs/shaders/**xxx.cfg** : El fichero de configuración Recalbox para los shaders.

El fichero de configuración de shaders (`.CFG`) hace referencia a un fichero `GLSLP` para cada sistema de Recalbox. Cada fichero `GLSLP` hace a su vez referencia a uno o varios ficheros shaders (`.GLSL`).

## Sahders predefinidos

Con los shaders predefinidos, podéis configurar el conjunto de shaders que se aplicarán a todos vuestros sistemas desde una única opción.

Los shaders predefinidos están desarrollados por la comunidad Recalbox, y se optimizan en cada nueva versión de Recalbox.

Aquí tenéis la lista de shaders predefinidos:

* `NINGUNO` : ningún shader está activo.
* `SCANLINES` : activa el shader de líneas de barrido horizontales típicas de las pantallas CRT.
* `RETRO` : sélecciona el "mejor" shader para cada sistema, shader elegido por la comunidad Recalbox, que se supone os dará la experiencia de juego más cercana a la experiencia original.

Podéis definir el paquete de shaders que queréis utilizar desde la opción `OPCIONES DE JUEGOS` dentro de EmulationStation.

## Shaders personalizados

Existen 2 formas de definir los shaders en Recalbox.

### Shader global

Tenéis la posibilidad de elegir un determinado shader para todos los sistemas de vuestra Recalbox. Para ello, seguid las siguientes etapas:

* Dentro de EmulationStation, abrid el menú con `START`.
* Dirigíros dentro de `OPTIONS DES JEUX` > `SHADERS`.
* Seleccionad el shader específico que deseáis aplicar al lanzamiento de cada juego.

### Shader por sistema

>Este apartado se aplica a los usuarios avanzados.
{.is-info}

Podéis combiar los packs de shaders predefinidos con la personalización por sistema utilizando el fichero recalbox.conf. La clave `global.shaderset` define un grupo de shaders de base para cada emulador. A continuación, podéis modificar esta configuración para un determinado sistema con la clave `systemname.shader` (`systemname` debe reemplazarse por el sistema en cuestión). Los shaders proporcionados por Recalbox se encuentran disponibles en el directorio `/recalbox/share/shaders/`

Por ejemplo, si yo quiero:

* Los shaders `RETRO` por defecto en todos los emuladores,
* no quiero shaders en la Gameboy,
* pero quiero un shader existente en Recalbox preconfigurado para la Megadrive,
* y un shader personal/custom preconfigurado para la SNES

```ini
# Set the retro shader set for all emulators, now i know that i have the best selection
global.shaderset=retro
# But my gameboy seem better for me with no shaders
gb.shaders=
# We use the already existing 4xbr_retro.glslp shader for megadrive
megadrive.shaders=/recalbox/share_init/shaders/4xbr_retro.glslp
# And i want to apply my own shader preset on snes
snes.shaders=/recalbox/share/shaders/custom/snes.glslp
```

Durante un juego, podéis cambiar de shaders utilizando vuestro mando.
Utilizad los comandos especiales `Hotkey + R2` o `Hotkey + L2` para aplicar los shaders existentes hacia delante o hacia atrás.

>Tened en cuenta que los shaders aplican cálculos y filtros en todas las imágenes generadas por el emulador, lo que puede impactar fuertemente el rendimiento del emulador.
{.is-warning}