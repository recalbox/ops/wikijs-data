---
title: Mise en marche/arrêt du Switch Mausberry
description: 
published: true
date: 2021-09-04T17:57:19.572Z
tags: mausberry, switch, marche, arrêt, bouton
editor: markdown
dateCreated: 2021-05-21T08:09:05.712Z
---

## Introduction

Les circuits Mausberry gèrent l'activation et la désactivation du Raspberry Pi à l'aide d'un interrupteur ON/OFF.  
Ce type de circuit peut être utilisé pour utiliser l'interrupteur marche/arrêt d'une NES et son bouton de RESET par exemple.

>Ce tutoriel est dédié aux Raspberry Pi 2/3 de Recalbox.
{.is-info}

## Procédure

### Connexions et câblage

Pour les connexions sur le Raspberry Pi, utilisez les ports GPIO 23 et 24 (broche 16 et 18).
Le Mausberry est opérationnel avec le GPIO23 = OUT / IN = GPIO24.

### Procédure

#### Prérequis

* Votre Raspberry Pi doit être connecté au réseau
* Vous devez connaître l'[adresse IP](./../../tutorials/network/ip/discover-recalbox-ip) de votre Raspberry Pi.

#### Étapes

* Ouvrez votre fichier [recalbox.conf](./../../basic-usage/getting-started/recalboxconf-file)
* Dans la rubrique `A - System Options`, dé-commentez le paramètre pour obtenir ceci :

```ini
# ------------ A - System Options ----------- #
# Uncomment the system.power.switch you use
system.power.switch=MAUSBERRY # http://mausberry-circuits.myshopify.com/pages/setup
```

Sauvegardez !

## Annexes

### Branchements et câblage

![](/tutorials/others/gpio-mausberry.png)

### Script : setup.sh

```shell
echo '#!/bin/bash

# C'est la broche GPIO connectée au fil de l'interrupteur marqué OUT
GPIOpin1=20

# C'est la broche GPIO connectée au fil de l'interrupteur marqué IN
GPIOpin2=21

echo "$GPIOpin1" > /sys/class/gpio/export
echo "in" > /sys/class/gpio/gpio$GPIOpin1/direction
echo "$GPIOpin2" > /sys/class/gpio/export
echo "out" > /sys/class/gpio/gpio$GPIOpin2/direction
echo "1" > /sys/class/gpio/gpio$GPIOpin2/value
while [ 1 = 1 ]; do
power=$(cat /sys/class/gpio/gpio$GPIOpin1/value)
if [ $power = 0 ]; then
sleep 1
else
poweroff
fi
done' > /recalbox/scripts/mausberry.sh
chmod 777 /recalbox/scripts/mausberry.sh
```

### Script : S99maus

```shell
#!/bin/bash
### BEGIN INIT INFO
# Provides: mausberry.sh
# Required-Start: $network $local_fs $remote_fs
# Required-Stop: $network $local_fs $remote_fs
# Default-Start: 2 3 4 5
# Default-Stop: 0 1 6
# Short-Description: switch mausberry init script.
# Description: Starts and stops SwitchDaemon service.
### END INIT INFO

#VAR
RUN="/recalbox/scripts/mausberry.sh"
BTD_PID=$(ps -eo pid,command | grep "/bin/bash $RUN" | grep -v grep | awk '{print $1}')

serviceStatus() {
   if [ ! -z "$BTD_PID" ]; then
      echo -e '33[0mservice mausberry.sh ['$BTD_PID'] [33[33;32m OK 33[0m]'
   else
      echo -e '33[0mservice mausberry.sh [33[33;31m KO 33[0m]'
   fi
}

# Carry out specific functions when asked to by the system
case "$1" in
   start)
      echo "Starting script $RUN ..."
      if [ -z "$BTD_PID" ]; then
         nice -n 19 $RUN&

         if [ $? -eq 0 ]; then
            echo -e "33[0mscript $RUN [33[33;32m STARTED 33[0m]"
         fi
      else
         echo "script $RUN already started ['$BTD_PID']!"
      fi
      #serviceStatus
   ;;
   stop)
      echo "Stopping script $RUN ..."
      if [ ! -z "$BTD_PID" ]; then
         kill $BTD_PID

         if [ $? -eq 0 ]; then
            echo -e "33[0mscript $RUN [33[33;31m STOPPED 33[0m]"
         fi
      fi
      #serviceStatus
   ;;
   status)
      serviceStatus
   ;;
   *)
      echo "Usage: /etc/init.d/S99maus {start | stop | status}"
      exit 1
   ;;
esac

exit 0
```