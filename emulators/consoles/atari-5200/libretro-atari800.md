---
title: Libretro Atari800
description: 
published: true
date: 2024-07-24T09:42:25.099Z
tags: libretro, atari, atari800, 5200, atari-5200
editor: markdown
dateCreated: 2021-07-30T16:32:06.727Z
---



## ![](/emulators/license.svg) License

This core is under [**GPLv2**](https://github.com/atari800/atari800/blob/master/COPYING) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Features

| Feature | Supported |
| :---: | :---: |
| Screenshots | ✔ |
| Saves | - |
| States | - |
| Core Options | ✔ |
| Native Cheats | - |
| Controls | ✔ |
| Remapping | ✔ |

## ![](/emulators/bios.svg) BIOS

### List of mandatory bios

| Filename | Description | MD5 | Provided |
| :---: | :---: | :---: | :---: |
| 5200.rom | 5200 BIOS | 281f20ea4320404ec820fb7ec0693b38 | ❌ |

### Location

Put the bios like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 atari5200
┃ ┃ ┃ ┃ ┣ 🗒 **5200.rom**

## ![](/emulators/roms.png) Roms

### Supported extensions

Roms must have the extension:

* .a52
* .zip

This system supports compressed roms in .zip/.7z format. However, be careful, it is only an archive.

The files contained in the .zip/.7z must correspond to the extensions mentioned above.
Moreover, each .zip/.7z file must contain only one compressed rom.

### Location

Put the roms like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 atari5200
┃ ┃ ┃ ┃ ┣ 🗒 **game.zip**

>The roms using **No-Intro** format are highly recommended.
{.is-success}

>For more information about roms, go to [this tutorial](./../../../tutorials/games/generalities/isos-and-roms)!
{.is-info}

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Accessing to options

You can configure various options in two different ways.

* Via RetroArch menu:

┣ 📁 RetroArch Menu
┃ ┣ 📁 Core options
┃ ┃ ┣ 🧩 Name_option

* Via the file `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core options

| Options | Options values | Variables (for configuration override) | Variables values |
| --- | --- | --- | --- |
| Video Standard | `NTSC` ✅ / `PAL` | `atari800_ntscpal` | `NTSC` / `PAL` |
| Hi-Res Artifacting | `Disabled` ✅ / `Enabled` | `atari800_artifacting` | `disabled` / `enabled` |
| Internal resolution | `336x240` ✅ / `320x240` / `384x240` / `384x272` / `384x288` / `400x300` | `atari800_resolution` | `336x240` / `320x240` / `384x240` / `384x272` / `384x288` / `400x300` |
| Controller Hacks | `none` ✅ / `Dual Stick` / `Swap Ports` | `atari800_opt2` | `none` / `enabled` / `Swap Ports` |
| Activate Paddle Mode | `Disabled` ✅ / `Enabled` | `paddle_active` | `disabled` / `enabled` |
| Paddle Movement Speed | `1` / `2` / `3` ✅ / `4` / `5` / `6` / `7` / `8` / `9` | `paddle_movement_speed` | `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` |
| Digital Joystick Sensitivity | `5%` / `10%` / `15%` / `20%` / `25%` / `30%` / `35%` / `40%` / `45%` / `50%` / `55%` / `60%` / `65%` / `70%` / `75%` / `80%` / `85%` / `90%` / `95%` / `100%` ✅ | `pot_digital_sensitivity` | `5` / `10` / `15` / `20` / `25` / `30` / `35` / `40` / `45` / `50` / `55` / `60` / `65` / `70` / `75` / `80` / `85` / `90` / `95` / `100` |
| Analog Joystick Sensitivity | `5%` / `10%` / `15%` / `20%` / `25%` / `30%` / `35%` / `40%` / `45%` / `50%` / `55%` / `60%` / `65%` / `70%` / `75%` / `80%` / `85%` / `90%` / `95%` / `100%` ✅ | `pot_analog_sensitivity` | `5` / `10` / `15` / `20` / `25` / `30` / `35` / `40` / `45` / `50` / `55` / `60` / `65` / `70` / `75` / `80` / `85` / `90` / `95` / `100` |
| Analog Joystick Deadzone | `0%` / `3%` / `5%` / `7%` / `10%` / `13%` / `15%` ✅ / `17%` / `20%` / `23%` / `25%` / `27%` / `30%` | `pot_analog_deadzone` | `0` / `3` / `5` / `7` / `10` / `13` / `15` / `17` / `20` / `23` / `25` / `27` / `30` |
| Retroarch Keyboard type | `poll` ✅ / `callback` | `atari800_keyboard` | `poll` / `callback` |
| Atari Keyboard Defines | `informational` ✅ | `keyboard_defines` | `informational` |
| Atari System | `Atari 400/800 (OS B)` / `Atari 800XL (64K)` / `Atari 130XE (128K)` / `Modern XL/XE(320K Comby Shop)` / `Modern XL/XE(576K)` / `Modern XL/XE(1088K)` / `Atari 5200 Super System` ✅ | `atari800_system` | `400/800 (OS B)` / `800XL (64K)` / `130XE (128K)` / `Modern XL/XE(320K CS)` / `Modern XL/XE(576K)` / `Modern XL/XE(1088K)` / `5200` |
| Internal BASIC (hold OPTION on boot) | `Disabled` ✅ / `Enabled` | `atari800_internalbasic` | `disabled` / `enabled` |
| SIO Acceleration | `Disabled` / `Enabled` ✅ | `atari800_sioaccel` | `disabled` / `enabled` |
| Boot from Cassette (Reboot) | `Disabled` ✅ / `Enabled` | `atari800_cassboot` | `disabled` / `enabled` |
| Autodetect Atari Cartridge Type (Restart) | `Disabled` ✅ / `Enabled` | `atari800_opt1` | `disabled` / `enabled` |

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/atari800/atari800/](https://github.com/atari800/atari800/)
* **Libretro documentation**: [https://docs.libretro.com/library/atari800/](https://docs.libretro.com/library/atari800/)