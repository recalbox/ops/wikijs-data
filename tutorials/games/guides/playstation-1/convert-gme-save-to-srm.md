---
title: Convertir une sauvegarde .GME en .SRM
description: 
published: true
date: 2021-09-08T12:59:02.891Z
tags: ps1, gme, srm, sauvegarde, convertir
editor: markdown
dateCreated: 2021-05-21T08:47:36.186Z
---

Il est possible d'utiliser les sauvegardes au format Dexdrive (`.GME`) et de les convertir au format `.SRM` pour RetroArch.

* Télécharger le logiciel [MemcardRex v1.8](http://www.mediafire.com/download/7ytiync7oxuhw4m/MemcardRex%201.8.zip) et décompressez-le.
* Cliquez sur l'exécutable MemcardRex.exe.
* Dans les menus, cliquez sur `File` puis cliquez sur `Open`. Sélectionnez votre fichier de sauvegarde `.gme` à convertir.
* Choisissez `File` puis cliquez sur `Save as...`. Nommez votre fichier comme le nom du jeu. Par exemple **nomjeu.mcr**.
* Renommez tous simplement le fichier **nomdujeu.mcr** en **nomdujeu.srm**.
* Enfin, placez votre sauvegarde nouvellement créée dans le répertoire `/recalbox/share/saves/psx/`.