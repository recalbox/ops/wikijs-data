---
title: Connectez un afficheur LCD avec I2C à votre Recalbox
description: 
published: true
date: 2023-11-17T18:31:49.191Z
tags: lcd, i2c, écran, afficheur
editor: markdown
dateCreated: 2021-05-21T08:44:10.615Z
---

## Pré-requis

Un écran LCD I2c comme le modèle Hd44780 en version A00 (support ascii + caractères japonais) ou A02 (support ascii + caractères européen).

![](/tutorials/video/tft/hd44780.jpg)

## Installation

### Branchement I2C sur GPIO Raspberry Pi

Connexion de l'I2c sur un Raspberry Pi

![](/tutorials/video/tft/i2c-gpio.png)

### Activation de l'I2C dans Recalbox

* Modifiez le fichier [recalbox-user-config.txt](./../../../tutorials/system/modification/configtxt-configuration) avec nano ou vim et ajoutez les lignes suivantes :

```ini
#Activate I2C
dtparam=i2c1=on
dtparam=i2c_arm=on
```

* Modifiez le fichier `/boot/cmdline.txt` avec nano ou vim et ajoutez les lignes suivantes :

```ini
bcm2708.vc_i2c_override=1
```

* Redémarrez votre Recalbox

### Vérifier l'adresse de l'I2C

Vous devrez connaître l'adresse de votre I2C** pour faire fonctionner certains scripts d'affichage. En règle générale, l'adresse est  `0x27` ou `0x3f`.

Pour le vérifier :

* Lancez la commande suivante ; celle-ci prend un certain temps !
  * sur les très vieux modèles de Raspberry : `i2cdetect -y 0` 
  * sur les Raspberry plus récents (pi4, pi3, pi2) : `i2cdetect -y 1`

```shell

     0  1  2  3  4  5  6  7  8  9  a  b  c  d  e  f
00:          -- -- -- -- -- -- -- -- -- -- -- -- --
10: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
20: -- -- -- -- -- -- -- 27 -- -- -- -- -- -- -- --
30: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
40: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
50: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
60: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
```

L'adresse retournée par la commande dans le tableau nous indique : `0x27`

## Script d'affichage disponible pour Recalbox.

Ce script est disponible dans cette [discussion](https://forum.recalbox.com/topic/8689/script-clcd-display-on-recalbox).

![](/tutorials/video/tft/hd4470-result.jpg)