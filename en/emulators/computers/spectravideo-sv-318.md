---
title: Spectravideo SV-318
description: 
published: true
date: 2021-08-09T15:03:52.767Z
tags: computers, spectravideo, sv-318
editor: markdown
dateCreated: 2021-07-28T21:32:25.179Z
---

![](/emulators/computers/spectravideo.svg){.align-center}

## Technical data

* **Manufacturer** : Spectravideo International
* **Year of release**: 1983
* **OS**: Microsoft Extended BASIC, CP/M
* **CPU**: Zilog Z80A @ 3.58 MHz
* **ROM**: 32KB (16 KB BIOS / 16 KB BASIC)
* **RAM**: 16KB (SV-318) et 64Kb (SV-328)
* **VRAM**: 16KB
* **Video**: Texas Instruments TMS9918
* **Resolution**: 256×192, 16 colors
* **Sound**: General Instrument AY-3-8910 (PSG)
* **Media**: ROM Cartridge, Cassette tape

## Presentation

The SV-318 is Spectravideo's basic model. It was equipped with a hard-to-use chiclet-style keyboard, next to which was a cursor/joystick combination. This one is disc-shaped with a hole in the center; place a red plastic "stick" in the hole and it becomes a built-in joystick, remove the stick and it is a directional pad for word processing, etc. This machine also contains 16 kB of user RAM (plus 16 kB of video RAM) which limits its usefulness, although this can be expanded via external devices.

This machine is basically identical to its big brother the SV-328, the only differences being the keyboard and the amount of memory. The ROM, which can be expanded, the motherboard and the case between the 2 machines are the same.

The operating system used, Microsoft Extended BASIC, is not to be confused with MSX BASIC, although, as a marketing matter, the term **Microsoft Extended** was considered to be the meaning of **MSX**. The SV-318 is not fully compatible with the MSX standard.

In 1983, Spectravideo announced the ColecoVision SV-603 video game adapter for the SV-318. The company stated that this $70 product would allow users to "play all ColecoVision games".

## Emulators

[Libretro blueMSX](libretro-bluemsx)