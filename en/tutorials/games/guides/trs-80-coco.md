---
title: TRS-80 Color Computer
description: 
published: true
date: 2021-11-09T20:50:53.895Z
tags: color, trs-80, computer, coco, 8.0+
editor: markdown
dateCreated: 2021-09-09T17:30:47.125Z
---

Here you will find some information to make the most of the TRS-80 Color Computer.

Here are the available tutorials:

[FAQ](faq)