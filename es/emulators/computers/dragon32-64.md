---
title: Dragon32/64
description: 
published: true
date: 2024-07-26T12:47:41.013Z
tags: dragon32, dragon64, dragon data, tano corp, 8.0+, ordenadores
editor: markdown
dateCreated: 2023-09-02T18:46:25.091Z
---

![](/emulators/computers/dragon.svg){.align-center}

## Datos técnicos

* **Fabricante**: Dragon Data
* **Año de lanzamiento**: 1982
* **Sistema operativo**: Microsoft Extended BASIC
* **Procesador**: Motorola 6809E a 0,89 MHz
* **RAM**: 32/64 KB
* **Resolución**: 256 x 192 máximo, 8 colores
* **Sonido**: 1 voz, 5 octavas con Basic / 4 voces, 7 octavas con código máquina

## Presentación

Los Dragon 32 y Dragon 64 son ordenadores personales fabricados en los años ochenta. Estos ordenadores eran muy similares al TRS-80 Color Computer (CoCo), producido para el mercado europeo por Dragon Data, Ltd, en Port Talbot, Gales, y para el mercado americano por Tano de Nueva Orleans, Luisiana. La principal diferencia entre las dos máquinas eran sus números de modelo, 32 KB de RAM para la Dragon 32 y 64 KB de RAM para la Dragon 64.

A principios de la década de 1980, el mercado de los ordenadores personales estaba en pleno auge. Casi todos los meses se lanzaban nuevos aparatos. En agosto de 1982, Dragon se unió a la lucha con el Dragon 32; el Dragon 64 le siguió un año después. Los ordenadores se vendieron muy bien al principio y atrajeron el interés de varios desarrolladores de software independientes, en particular Microdeal. Poco después del lanzamiento de la máquina comenzó a publicarse una revista (dragon user).

En el saturado mercado de los ordenadores personales, en el que la calidad de los juegos era un factor importante, el Dragon sufrió debido a sus capacidades gráficas inferiores a las de máquinas contemporáneas como el Sinclair ZX Spectrum y el BBC Micro.

El Dragon tampoco era capaz de mostrar letras minúsculas con facilidad, ni de mostrar caracteres acentuados. La máquina apenas podía ir más allá de la fase de consola de juegos, excluyendo los mercados educativo y "semiprofesional".

Debido a estas limitaciones, el Dragon no fue un gran éxito comercial, y su producción finalizó en junio de 1984.

## Emuladores

[XRoar](xroar)