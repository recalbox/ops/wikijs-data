---
title: Libretro LowResNX
description: 
published: true
date: 2021-11-16T08:05:30.481Z
tags: libretro, lowresnx, 8.0+
editor: markdown
dateCreated: 2021-10-19T22:19:51.974Z
---

**Libretro LowResNX** é um port do emulador **LowRes NX**.

## ![](/emulators/license.svg) Licença

Este núcleo está sob licença [**Zlib**](https://github.com/timoinutilis/lowres-nx/blob/master/LICENSE).

## ![](/emulators/compatibility.png) Compatibilidade

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Recursos

| Funcionalidade | Suportado |
| :---: | :---: |

## ![](/emulators/bios.svg) BIOS

> **Não é necessária nenhuma BIOS.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Extensões suportadas

As ROMs devem ter a extensão:

* .nx
* .zip
* .7z

Os arquivos dentro do .zip / .7z devem corresponder às extensões mencionadas acima.

### Local

Coloque as roms assim:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 lowresnx
┃ ┃ ┃ ┃ ┣ 🗒  **jogo.nx**

## ![](/emulators/advanced-configuration.png) Configuração avançada do emulador

> **Este núcleo não tem opção.**
{.is-success}

## ![external-links.png](/emulators/external-links.png) Links externos

* **Código-fonte utilizado**: [https://github.com/timoinutilis/lowres-nx/](https://github.com/timoinutilis/lowres-nx/)
* **Site oficial**: [https://lowresnx.inutilis.com/](https://lowresnx.inutilis.com/)
* **Documentação oficial**: [https://lowresnx.inutilis.com/help.php](https://lowresnx.inutilis.com/help.php)