---
title: Libretro TGBDual
description: 
published: true
date: 2024-07-24T23:48:25.760Z
tags: libretro, color, gameboy, tgbdual, gbc
editor: markdown
dateCreated: 2021-07-14T06:42:01.900Z
---

**Libretro Tgbdual** is an emulator that allows you to run Game Boy games with support for the game link cable.

## ![](/emulators/license.svg) License

This core is under [**GPLv2**](https://github.com/libretro/tgbdual-libretro/blob/master/docs/COPYING-2.0.txt) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Features

| Feature | Supported |
| :---: | :---: |
| Restart | ✔ |
| Screenshots | ✔ |
| Saves | ✔ |
| States | ✔ |
| Rewind | ✔ |
| Netplay | ✔ |
| Core Options | ✔ |
| Controls | ✔ |
| Remapping | ✔ |
| Softpatching | ✔ |

## ![](/emulators/bios.svg) BIOS

>**No bios is required.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Supported extensions

Roms must have the extension:

* .bin
* .gbc
* .sgb
* .zip
* .7z

This system supports compressed roms in .zip/.7z format. However, be careful, it is only an archive.

The files contained in the .zip/.7z must correspond to the extensions mentioned above.
Moreover, each .zip/.7z file must contain only one compressed rom.

### Location

Put your roms like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 gbc
┃ ┃ ┃ ┃ ┣ 🗒 **game.zip**

>The roms using **No-Intro** format are highly recommended.
{.is-success}

>For more information about roms, go to [this tutorial](./../../../tutorials/games/generalities/isos-and-roms)!
{.is-info}

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Accessing to options

You can configure various options in two different ways.

* Via RetroArch menu:

┣ 📁 RetroArch Menu
┃ ┣ 📁 Core options
┃ ┃ ┣ 🧩 Name_option

* Via the file `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core options

| Options | Options values | Variables (for configuration override) | Variables values |
| --- | --- | --- | --- |
| Link cable emulation (reload) | `Disabled` ✅ / `Enabled` | `tgbdual_gblink_enable` | `disabled` / `enabled` |
| Screen layout | `left-right` ✅ / `top-down` | `tgbdual_screen_placement` | `left-right` / `top-down` |
| Switch player screens | `normal` ✅ / `switched` | `tgbdual_switch_screens` | `normal` / `switched` |
| Show player screens | `both players` ✅ / `player 1 only` / `player 2 only` | `tgbdual_single_screen_mp` | `both players` / `player 1 only` / `player 2 only` |
| Audio output | `Game Boy #1` ✅ / `Game Boy #2` | `tgbdual_audoi_output` | `Game Boy #1` / `Game Boy #2` |

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/libretro/tgbdual-libretro/](https://github.com/libretro/tgbdual-libretro/)
* **Libretro documentation**: [https://docs.libretro.com/library/tgb\_dual/](https://docs.libretro.com/library/tgb_dual/)
* **Official source code**: [http://shinh.skr.jp/tgbdualsdl/](http://shinh.skr.jp/tgbdualsdl/)
* **Official website**: [http://gigo.retrogames.com/download.html\#tgb-dual](http://shinh.skr.jp/tgbdualsdl/)