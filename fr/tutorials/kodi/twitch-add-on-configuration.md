---
title: Configuration de l'extension Twitch
description: Ou comment pouvoir voir les vidéos de Twitch sans problème !
published: true
date: 2025-02-22T23:56:27.285Z
tags: configuration, kodi, extensions, twitch
editor: markdown
dateCreated: 2022-12-27T23:50:02.076Z
---

## Introduction

Dans Kodi, l'extension Twitch est déjà pré-installée. Cependant, vous devez le configurer pour pouvoir vous identifier et retrouver tout votre compte (comptes suivies, rediffusions, etc.).

## Procédure

### Pré-requis

- Avoir un compte Twitch.

### Configuration

>Les étapes suivantes peuvent différer si vous démarrez Kodi ou Twitch pour la première fois.
>{.is-info}

1. Lancez Kodi.

2. Allez dans `Extensions` > `Twitch`.



3. Une fenêtre vous indique d'aller générer un jeton OAuth.



4. Validez le message et allez dans `Paramètres` tout en bas.



5. Dans les paramètres, choisissez à gauche `Connexion` et au milieu, choisissez l'option `Obtenir un jeton OAuth`.



6. Au bout de quelques secondes, une fenêtre vous indiquera de vous rendre sur une adresse URL. Celle-ci n'étant peut-être plus fonctionnelle, il vous faudra aller sur https://twitchaddon.page.link/1Sk5.



7. Sur cette adresse, vous obtiendrez un jeton OAuth automatiquement si vous êtes identifié sur Twitch, auquel cas vous devrez vous identifier sur Twitch si ce n'es pas encore fait.



8. De retour dans Kodi, sélectionnez `Jeton Oauth`.



9. Recopiez le jeton OAuth et validez.



10. Validez la fenêtre des paramètres.



11. Sortez de l'extension et retournez dedans. Si vous avez bien recopié le jeton OAuth, vous serez identifié et pouvez profiter de Twitch sur Kodi sans publicité !

