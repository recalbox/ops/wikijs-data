---
title: Play Singe games
description: 
published: true
date: 2021-11-09T20:41:25.121Z
tags: daphne, singe, 8.0+
editor: markdown
dateCreated: 2021-08-08T07:01:34.697Z
---

## What are Singe Games?

The term `SINGE` (from the name of the dragon in Dragon's Lair) is the name of an extension for the Daphne emulator that allows anyone to create their own FMV: (full motion video).

It allows you to run the famous American Laser Games (ALD) that were all the rage in the arcades in their day. It allows you to play WoW Action Max and some other arcade games.

## List of Singe games

Here is the list of ALD Singe games:

* Crime Patrol
* Drug Wars
* Mad Dog McCree
* Mad Dog McCree v2.0
* The Last Bounty Hunter
* Time Traveler v2.0
* Who Shot Johnny Rock

Here is the list of WoW Action Max Singe games:

* 38 Ambush Alley
* Blue Thunder
* HydroSub 2021
* Pops Ghostly
* Sonic Fury

Here is the list of Arcade Singe games:

* Ninja Hayate
* TimeGal
* Time Traveler

>WoW ActionMax Singe games can run on PC only for now.
{.is-info}

## Games installation

Each game is divided into 2 parts:

* The game data (videos, music, etc.)
* Data to run the game (cdrom)

### For the game data

The following content is required:

* Any `.WAV` file
* Any file in `.PNG` format
* Any `.TTF` file
* Any `.SINGE` file other than the one named `gamename.monkey`
* `gamename.cfg`

You must place them in the following directory :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 singe
┃ ┃ ┃ ┃ ┃ ┣ 📁 ALG
┃ ┃ ┃ ┃ ┃ ┃ ┣ 📁 gamename
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 file1.wav
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 file2.wav
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 file3.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 file4.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 ...

### For the data to run the game

The following content is required:

* File `gamename.singe`
* File `gameName.txt`.
* Directory `cdrom` with the content intact.

You must place them in the following directory :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 gamename.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 gamename.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 gamename.txt
┃ ┃ ┃ ┃ ┃ ┣ 📁 cdrom
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 file1.dat
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 file2.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 file3.ogg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 ...

## Location of files per game

Since it is particularly difficult to place the right files in the right places, you will see below how to put the necessary files for each game in the Recalbox tree.

## {.tabset}

### American Laser Games

#### Crime Patrol

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 crimepatrol.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 crimepatrol.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 crimepatrol.txt
┃ ┃ ┃ ┃ ┃ ┣ 📁 cdrom
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 a_cpintro.dat
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 a_cpintro.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 a_cpintro.ogg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 b_airterrorist.dat
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 ...
┃ ┃ ┃ ┃ ┣ 📁 singe
┃ ┃ ┃ ┃ ┃ ┣ 📁 ALG
┃ ┃ ┃ ┃ ┃ ┃ ┣ 📁 crimepatrol
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 addlg___.ttf
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 badge3.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 badge4.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 break.wav
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 ...

#### Drug Wars

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 drugwars.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 drugwars.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 drugwars.txt
┃ ┃ ┃ ┃ ┃ ┣ 📁 cdrom
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 a_dwintro.dat
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 a_dwintro.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 a_dwintro.ogg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 a_partner01.dat
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 etc.
┃ ┃ ┃ ┃ ┣ 📁 singe
┃ ┃ ┃ ┃ ┃ ┣ 📁 ALG
┃ ┃ ┃ ┃ ┃ ┃ ┣ 📁 drugwars
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 addlg___.ttf
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 badge3.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 badge4.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 break.wav
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 ...

#### Mad Dog McCree

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 maddog.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 maddog.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 maddog.txt
┃ ┃ ┃ ┃ ┃ ┣ 📁 cdrom
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 a_bank.dat
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 a_bank.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 a_bank.ogg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 a_barrel-front.dat
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 ...
┃ ┃ ┃ ┃ ┣ 📁 singe
┃ ┃ ┃ ┃ ┃ ┣ 📁 ALG
┃ ┃ ┃ ┃ ┃ ┃ ┣ 📁 maddog
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arcadecoin.wav
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 bottlebreak.wav
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 bullet.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 choice.easy.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 ...

#### Mad Dog McCree 2

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 maddog2.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 maddog2.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 maddog2.txt
┃ ┃ ┃ ┃ ┃ ┣ 📁 cdrom
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 a_trailer.dat
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 a_trailer.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 a_trailer.ogg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 b_aintro.dat
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 ...
┃ ┃ ┃ ┃ ┣ 📁 singe
┃ ┃ ┃ ┃ ┃ ┣ 📁 ALG
┃ ┃ ┃ ┃ ┃ ┃ ┣ 📁 maddog2
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arcadecoin.wav
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 bottlebreak.wav
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 bullet.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 cdrom-globals.singe
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 ...

#### Space Pirates

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 spacepirates.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 spacepirates.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 spacepirates.txt
┃ ┃ ┃ ┃ ┃ ┣ 📁 cdrom
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 a_trailer.dat
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 a_trailer.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 a_trailer.ogg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 b_dc01.dat
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 ...
┃ ┃ ┃ ┃ ┣ 📁 singe
┃ ┃ ┃ ┃ ┃ ┣ 📁 ALG
┃ ┃ ┃ ┃ ┃ ┃ ┣ 📁 spacepirates
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 ca.ttf
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 cdrom-globals.singe
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 cdrom-setuplevel.singe
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 cdrom-shipbattle.singe
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 ...

#### The Last Bounty Hunter

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 lastbountyhunter.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 lastbountyhunter.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 lastbountyhunter.txt
┃ ┃ ┃ ┃ ┃ ┣ 📁 cdrom
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 a_trailer.dat
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 a_trailer.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 a_trailer.ogg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 b_armybase.dat
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 ...
┃ ┃ ┃ ┃ ┣ 📁 singe
┃ ┃ ┃ ┃ ┃ ┣ 📁 ALG
┃ ┃ ┃ ┃ ┃ ┃ ┣ 📁 lastbountyhunter
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 bottlebreak.wav
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 bullet2.wav
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 captured.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 cdrom-globals.singe
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 ...

#### Who Shot Johnny Hunter

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 johnnyrock.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 johnnyrock.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 johnnyrock.txt
┃ ┃ ┃ ┃ ┃ ┣ 📁 cdrom
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 a_intro.dat
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 a_intro.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 a_intro.ogg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 a_start.dat
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 ...
┃ ┃ ┃ ┃ ┣ 📁 singe
┃ ┃ ┃ ┃ ┃ ┣ 📁 ALG
┃ ┃ ┃ ┃ ┃ ┃ ┣ 📁 johnnyrock
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 anchorsteamnf.ttf
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 cashregister.wav
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 cdrom-globals.singe
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 cdrom-hitbox-cans.singe
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 ...

### WoW Action Max

#### 38 Ambush Alley

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 singe
┃ ┃ ┃ ┃ ┃ ┣ 📁 ActionMax
┃ ┃ ┃ ┃ ┃ ┃ ┣ 📁 38AmbushAlley.daphne
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 38AmbushAlley.singe
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 38AmbushAlley.txt
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 video_38AmbushAlley.dat
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 video_38AmbushAlley.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 video_38AmbushAlley.ogg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 Emulator.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 font_BlueStone.ttf
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 font_chemrea.ttf
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 font_LED_Real.ttf
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sound_ActionMax.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sound_ASteadyAimIsCritical.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sound_BadHit.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sound_GameOver.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sound_GetReadyForAction.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sound_GoodHit.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sound_Gunshot.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sprite_38AmbushAlley.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sprite_ActionMax.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sprite_Bullet.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sprite_Crosshair.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sprite_LightOff.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sprite_LightOn.png

>The file `38AmbushAlley.txt` can be named `frame_38AmbushAlley.txt`, do not hesitate to rename it.
{.is-info}

#### Blue Thunder

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 singe
┃ ┃ ┃ ┃ ┃ ┣ 📁 ActionMax
┃ ┃ ┃ ┃ ┃ ┃ ┣ 📁 BlueThunder.daphne
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 BlueThunder.singe
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 BlueThunder.txt
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 video_BlueThunder.dat
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 video_BlueThunder.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 video_BlueThunder.ogg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 Emulator.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 font_BlueStone.ttf
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 font_chemrea.ttf
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 font_LED_Real.ttf
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sound_ActionMax.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sound_ASteadyAimIsCritical.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sound_BadHit.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sound_GameOver.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sound_GetReadyForAction.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sound_GoodHit.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sound_Gunshot.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sprite_ActionMax.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sprite_BlueThunder.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sprite_Bullet.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sprite_Crosshair.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sprite_LightOff.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sprite_LightOn.png

>The file `BlueThunder.txt` can be named `frame_BlueThunder.txt`, feel free to rename it.
{.is-info}

#### HydroSub 2021

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 singe
┃ ┃ ┃ ┃ ┃ ┣ 📁 ActionMax
┃ ┃ ┃ ┃ ┃ ┃ ┣ 📁 HydroSub2021.daphne
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 HydroSub2021.singe
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 HydroSub2021.txt
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 video_HydroSub2021.dat
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 video_HydroSub2021.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 video_HydroSub2021.ogg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 Emulator.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 font_BlueStone.ttf
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 font_chemrea.ttf
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 font_LED_Real.ttf
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sound_ActionMax.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sound_ASteadyAimIsCritical.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sound_BadHit.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sound_GameOver.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sound_GetReadyForAction.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sound_GoodHit.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sound_Gunshot.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sprite_ActionMax.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sprite_Bullet.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sprite_Crosshair.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sprite_HydroSub2021.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sprite_LightOff.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sprite_LightOn.png

>The file `HydroSub2021.txt` can be named `frame_HydroSub2021.txt`, do not hesitate to rename it.
{.is-info}

#### Pops Ghostly

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 singe
┃ ┃ ┃ ┃ ┃ ┣ 📁 ActionMax
┃ ┃ ┃ ┃ ┃ ┃ ┣ 📁 PopsGhostly.daphne
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 PopsGhostly.singe
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 PopsGhostly.txt
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 video_PopsGhostly.dat
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 video_PopsGhostly.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 video_PopsGhostly.ogg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 Emulator.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 font_BlueStone.ttf
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 font_chemrea.ttf
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 font_LED_Real.ttf
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sound_ActionMax.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sound_ASteadyAimIsCritical.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sound_BadHit.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sound_GameOver.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sound_GetReadyForAction.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sound_GoodHit.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sound_Gunshot.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sprite_ActionMax.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sprite_Bullet.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sprite_Crosshair.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sprite_LightOff.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sprite_LightOn.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sprite_PopsGhostly.png

>The file `PopsGhostly.txt` can be named `frame_PopsGhostly.txt`, do not hesitate to rename it.
{.is-info}

#### Sonic Fury

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 singe
┃ ┃ ┃ ┃ ┃ ┣ 📁 ActionMax
┃ ┃ ┃ ┃ ┃ ┃ ┣ 📁 SonicFury.daphne
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 SonicFury.singe
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 SonicFury.txt
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 video_SonicFury.dat
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 video_SonicFury.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 video_SonicFury.ogg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 Emulator.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 font_BlueStone.ttf
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 font_chemrea.ttf
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 font_LED_Real.ttf
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sound_ActionMax.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sound_ASteadyAimIsCritical.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sound_BadHit.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sound_GameOver.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sound_GetReadyForAction.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sound_GoodHit.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sound_Gunshot.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sprite_ActionMax.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sprite_Bullet.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sprite_Crosshair.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sprite_LightOff.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sprite_LightOn.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sprite_SonicFury.png

>The file `SonicFury.txt` can be named `frame_SonicFury.txt`, don't hesitate to rename it.
{.is-info}

### Arcade

#### Ninja Hayate

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 hayate.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 hayate.dat
┃ ┃ ┃ ┃ ┃ ┣ 🗒 hayate.m2v
┃ ┃ ┃ ┃ ┃ ┣ 🗒 hayate.ogg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 hayate.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 hayate.txt
┃ ┃ ┃ ┃ ┣ 📁 singe
┃ ┃ ┃ ┃ ┃ ┣ 📁 hayate
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 1up.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 ...

#### TimeGal

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 timegal.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 timegal.dat
┃ ┃ ┃ ┃ ┃ ┣ 🗒 timegal.m2v
┃ ┃ ┃ ┃ ┃ ┣ 🗒 timegal.ogg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 timegal.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 timegal.txt
┃ ┃ ┃ ┃ ┣ 📁 singe
┃ ┃ ┃ ┃ ┃ ┣ 📁 timegal
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowright.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 ...

#### Time Traveler

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 timetraveler.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 timetraveler.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 timetraveler.txt
┃ ┃ ┃ ┃ ┃ ┣ 📁 dvd
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 vts_01_1.dat
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 vts_01_1.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 vts_01_1.ogg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 vts_02_1.dat
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 etc.
┃ ┃ ┃ ┃ ┣ 📁 singe
┃ ┃ ┃ ┃ ┃ ┣ 📁 timetraveler
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowright.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 ...

>The file `timetraveler.monkey` can be named `dvd-timetraveler.monkey`, do not hesitate to rename it.
{.is-info}
