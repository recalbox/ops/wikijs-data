---
title: Hypseus
description: 
published: true
date: 2021-09-23T23:11:26.043Z
tags: daphne, singe, hypseus
editor: markdown
dateCreated: 2021-09-23T23:11:26.043Z
---

**Hypseus** para a emulação **de jogos arcade em laserdisc**, é um *fork* (uma "bifurcação") do Daphne escrito por Jeffrey Clark para trazer algumas melhorias:

* Decodificador MPEG2 atualizado
* Suporte SDL2
* Etc...

## ![](/emulators/license.svg) Licença

Este núcleo é licenciado sob :

**Hypseus***, Emulador Múltiplo de Arcade Laserdisc
 Copyright (C) 2016 [Jeffrey Clark](https://github.com/h0tw1r3)

**Daphne**, o primeiro Emulador Múltiplo de Arcade Laserdisc
 Copyright (C) 1999-2013 [Matt Ownby](http://www.daphne-emu.com/site3/statement.php)**GPLv2**

## ![](/emulators/compatibility.png) Compatibilidade

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Recursos

| Recurso | Suportado |
| :---: | :---: |

## ![](/emulators/bios.svg) BIOS

>**Nenhuma BIOS é necessária**.
{.is-success}

## ![](/emulators/roms.png) Roms

### Como jogar jogos de laserdisc no Recalbox ?

* Para adicionar um jogo em laserdisc, você precisa dos dois componentes de um jogo desse tipo:

  * A imagem original do laserdisc, que contém o conteúdo de vídeo e áudio.
  * O arquivo ROM, que é o programa do jogo.

**A ROM** é um simples arquivo ZIP que deve ser copiado para a subpasta `roms/daphne/`. Ela deve conter os arquivos em `.bin'.

* A imagem do disco laser consiste em vários arquivos:

  * **Um ou mais arquivos de vídeo** (`*.m2v`)
  * **Um ou mais arquivos de áudio** (`*.ogg`)
  * **Um ou mais arquivos de índice** (`*.dat`): podem ser criados pelo emulador no primeiro início (operação lenta)
  * **Um arquivo dos quadros** (`.txt`): para associar números de quadros com arquivos de vídeo.

* Todos estes arquivos devem ser copiados para uma pasta com uma extensão `.daphne`, sob a pasta `.daphne`.
* A primeira linha do seu arquivo `.txt` é um caminho e deve ser . (apenas o caractere de ponto), altere-a se não o for.
* Você também pode criar um arquivo com a extensão `.commands` para passar as configurações específicas do jogo para o emulador. Veja abaixo para mais detalhes. 
* A pasta, o arquivo de quadros e o arquivo de comandos devem ter o mesmo nome que o arquivo ROM! (apenas as extensões são diferentes). 

#### Local

Coloque as roms assim:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 **jogo.daphne**
┃ ┃ ┃ ┃ ┃ ┣ 🗒  **jogo.command**
┃ ┃ ┃ ┃ ┃ ┣ 🗒  **jogo.txt**
┃ ┃ ┃ ┃ ┃ ┣ 🗒  **jogo.dat**
┃ ┃ ┃ ┃ ┃ ┣ 🗒  **jogo.m2v**
┃ ┃ ┃ ┃ ┃ ┣ 🗒  **jogo.ogg**
┃ ┃ ┃ ┃ ┃ ┣ 🗒  **...**
┃ ┃ ┃ ┃ ┣ 📁 **roms**
┃ ┃ ┃ ┃ ┃ ┣ 🗒  **jogo.zip**

## ![](/emulators/advanced-configuration.png) Configuração avançada do emulador

>**Este núcleo não tem opções.**
{.is-success}

## ![external-links.png](/emulators/external-links.png) Links externos

* **Código fonte utilizado** : [https://github.com/DirtBagXon/hypseus-singe](https://github.com/DirtBagXon/hypseus-singe)
* **Site oficial** : [http://www.daphne-emu.com/site3/index_hi.php](http://www.daphne-emu.com/site3/index_hi.php)
* **FAQ** : [http://www.daphne-emu.com/site3/FAQ_USA.php](http://www.daphne-emu.com/site3/FAQ_USA.php)
* **Wiki do núcleo** : [https://www.daphne-emu.com:9443/mediawiki/index.php/Main_Page](https://www.daphne-emu.com:9443/mediawiki/index.php/Main_Page)