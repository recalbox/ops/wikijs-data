---
title: Check MD5 signature of rom or bios
description: 
published: true
date: 2021-08-07T00:23:26.110Z
tags: rom, md5, signature, bios
editor: markdown
dateCreated: 2021-08-07T00:23:26.110Z
---

To check the hash of one or more roms manually from your computer:

## {.tabset}
### Windows
Download this software [md5sum](http://esrg.sourceforge.net/utils_win_up/md5sum/).

### Linux
* From a `terminal` `md5sum /path/to/file` .
* Replace `/path/to/file` by the path of your file.

### macOS
* Open `Terminal` .
* Enter `md5sum /path/to/file` (replace `/path/to/file` with the path to your file).
* Right click on your file, choose `Copy`.
* Then `Paste` in the `Terminal` window.