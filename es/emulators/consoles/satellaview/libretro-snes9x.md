---
title: Libretro Snes9X
description: 
published: true
date: 2024-11-01T12:01:51.352Z
tags: libretro, satellaview, bs-x, snes9x
editor: markdown
dateCreated: 2023-09-07T20:24:28.815Z
---

**Libretro Snes9X** es un puerto actualizado de Snes9x, un emulador portátil de Super Nintendo Entertainment System para Libretro.

## ![](/emulators/license.svg) Licencia

Este núcleo está bajo licencia [**no comercial**](https://github.com/snes9xgit/snes9x/blob/master/LICENSE).

## ![](/emulators/compatibility.png) Compatibilidad

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Características

| Característica | Soportado |
| :---: | :---: |
| Reinicio | ✔ |
| Guardados | ✔ |
| Guardados instantáneos | ✔ |
| Rebobinado | ✔ |
| Netplay | ✔ |
| Opciones del núcleo | ✔ |
| RetroAchievements | ✔ |
| Trucos RetroArch | ✔ |
| Controles | ✔ |
| Remapeo | ✔ |
| Multi-Mouse | ✔ |
| Subsistema | ✔ |
| Softpatching | ✔ |

## ![](/emulators/bios.svg) BIOS

### Lista de BIOS obligatorios

| Nombre de archivo | Descripción | MD5 | Proporcionado |
| :---: | :---: | :---: | :---: |
| BS-X.bin | BS-X BIOS | 96cf17bf589fcbfa6f8de2dc84f19fa2 (Inglés sin DRM), d5af0ab2545c053b90ffa0866074ecf1 (Japonés sin DRM), 9c72de0cf4c9c88003292a79ddd0144d (Inglés), fed4d8242cfbed61343d53d48432aced (Japonés) | ❌ |

### Ubicación

Coloca el BIOS de la siguiente manera:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 satellaview
┃ ┃ ┃ ┃ ┣ 🗒 **BS-X.bin**

## ![](/emulators/roms.png) Roms

### Extensiones soportadas

Las roms deben tener la siguiente extensión:

* .bs
* .fig
* .gd3
* .sfc
* .smc
* .swc
* .zip
* .7z

Este sistema soporta roms comprimidas en formato .zip/.7z. Sin embargo, solo se trata de un archivo.

Los archivos contenidos en los .zip/.7z deben corresponder a las extensiones mencionadas anteriormente.
Además, cada archivo .zip/.7z solo debe contener una rom comprimida.

### Ubicación

Coloca las roms de la siguiente manera:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 satellaview
┃ ┃ ┃ ┃ ┣ 🗒 **juego.zip**

>Se recomienda encarecidamente el uso de roms en formato **No-Intro**.
{.is-success}

>Para más información sobre las roms, visita [este tutorial](./../../../tutorials/games/generalities/isos-and-roms)!
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

>Para poder conservar tus configuraciones personalizadas durante una actualización, te recomendamos utilizar nuestra funcionalidad [Sobrecargas de configuración](./../../../advanced-usage/configuration-override).
{.is-info}

### Acceder a las opciones

Puedes configurar diversas opciones de dos maneras diferentes.

* A través del menú RetroArch:

┣ 📁 Menú RetroArch
┃ ┣ 📁 Opciones del núcleo
┃ ┃ ┣ 🧩 Name_option

* A través del archivo `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Opciones del núcleo

| Opciones | Valores de las opciones | Variable (para sobrecarga) | Valores de las variables |
| --- | --- | --- | --- |
| Console Region (Reload Core) | `Auto` ✅ / `NTSC` / `PAL` | `snes9x_region` | `auto` / `ntsc` / `pal` |
| Preferred Aspect Ratio | `4:3` ✅ / `Uncorrected` / `Auto` / `NTSC` / `PAL` | `snes9x_aspect` | `4:3` / `uncorrected` / `auto` / `ntsc` / `pal` |
| Crop Overscan | `Activado` ✅ / `Desactivado` | `snes9x_overscan` | `enabled` / `disabled` |
| Hi-Res Blending | `Desactivado` ✅ / `Merge` / `Blur` | `snes9x_hires_blend` | `disabled` / `merge` / `blur` |
| Blargg NTSC Filter | `Desactivado` ✅ / `Monochrome` / `RF` / `Composite` / `S-Video` / `RGB` | `snes9x_blargg` | `disabled` / `monochrome` / `rf` / `composite` / `s-video` / `rgb` |
| Audio Interpolation | `Gaussian` ✅ / `Cubic` / `Sinc` / `None` / `Linear` | `snes9x_audio_interpolation` | `gaussian` / `cubic` / `sinc` / `none` / `linear` |
| Allow Opposing Directions | `Desactivado` ✅ / `Activado` | `snes9x_up_down_allowed` | `disabled` / `enabled` |
| SuperFX Overclocking | `50%` / `60%` / `70%` / `80%` / `90%` / `100%` ✅ / `150%` / `200%` / `250%` / `300%` / `350%` / `400%` / `450%` / `500%` | `snes9x_overclock_superfx` | `50%` / `60%` / `70%` / `80%` / `90%` / `100%` / `150%` / `200%` / `250%` / `300%` / `350%` / `400%` / `450%` / `500%` |
| Reduce Slowdown (Unsafe) | `Desactivado` ✅ / `Light` / `Compatible` / `Max` | `snes9x_overclock_cycles` | `disabled` / `light` / `compatible` / `max` |
| Reduce Flickering (Unsafe) | `Desactivado` ✅ / `Activado` | `snes9x_reduce_sprite_flicker` | `disabled` / `enabled` |
| Randomize Memory (Unsafe) | `Desactivado` ✅ / `Activado` | `snes9x_randomize_memory` | `disabled` / `enabled` |
| Block Invalid VRAM Access | `Activado` ✅ / `Desactivado` | `snes9x_block_invalid_vram_access` | `enabled` / `disabled` |
| Echo Buffer Hack (Unsafe, only enable for old addmusic hacks) | `Desactivado` ✅ / `Activado` | `snes9x_echo_buffer_hack` | `disabled` / `enabled` |
| Light Gun Mode | `Light Gun` / `Touchscreen` | `snes9x_lightgun_mode` | `Lightgun` / `Touchscreen` |
| Super Scope Reverse Trigger Buttons | `Desactivado` ✅ / `Activado` | `snes9x_superscope_reverse_buttons` | `disabled` / `enabled` |
| Super Scope Crosshair | `0` / `1` / `2` ✅ / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` | `snes9x_superscope_crosshair` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` |
| Super Scope Color | `White` ✅ / `White (blend)` / `Red` / `Red (blend)` / `Orange` / `Orange (blend)` / `Yellow` / `Yellow (blend)` / `Green` / `Green (blend)` / `Cyan` / `Cyan (blend)` / `Sky` / `Sky (blend)` / `Blue` / `Blue (blend)` / `Violet` / `Violet (blend)` / `Pink` / `Pink (blend)` / `Purple` / `Purple (blend)` / `Black` / `Black (blend)` / `25% Grey` / `25% Grey (blend)` / `50% Grey` / `50% Grey (blend)` / `75% Grey` / `75% Grey (blend)` | `snes9x_superscope_color` | `White` / `White (blend)` / `Red` / `Red (blend)` / `Orange` / `Orange (blend)` / `Yellow` / `Yellow (blend)` / `Green` / `Green (blend)` / `Cyan` / `Cyan (blend)` / `Sky` / `Sky (blend)` / `Blue` / `Blue (blend)` / `Violet` / `Violet (blend)` / `Pink` / `Pink (blend)` / `Purple` / `Purple (blend)` / `Black` / `Black (blend)` / `25% Grey` / `25% Grey (blend)` / `50% Grey` / `50% Grey (blend)` / `75% Grey` / `75% Grey (blend)` |
| Justifier 1 Crosshair | `0` / `1` / `2` / `3` / `4` ✅ / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` | `snes9x_justifier1_crosshair` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` |
| Justifier 1 Color | `White` / `White (blend)` / `Red` / `Red (blend)` / `Orange` / `Orange (blend)` / `Yellow` / `Yellow (blend)` / `Green` / `Green (blend)` / `Cyan` / `Cyan (blend)` / `Sky` / `Sky (blend)` / `Blue` ✅ / `Blue (blend)` / `Violet` / `Violet (blend)` / `Pink` / `Pink (blend)` / `Purple` / `Purple (blend)` / `Black` / `Black (blend)` / `25% Grey` / `25% Grey (blend)` / `50% Grey` / `50% Grey (blend)` / `75% Grey` / `75% Grey (blend)` | `snes9x_justifier1_color` | `White` / `White (blend)` / `Red` / `Red (blend)` / `Orange` / `Orange (blend)` / `Yellow` / `Yellow (blend)` / `Green` / `Green (blend)` / `Cyan` / `Cyan (blend)` / `Sky` / `Sky (blend)` / `Blue` / `Blue (blend)` / `Violet` / `Violet (blend)` / `Pink` / `Pink (blend)` / `Purple` / `Purple (blend)` / `Black` / `Black (blend)` / `25% Grey` / `25% Grey (blend)` / `50% Grey` / `50% Grey (blend)` / `75% Grey` / `75% Grey (blend)` |
| Justifier 2 Crosshair | `0` / `1` / `2` / `3` / `4` ✅ / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` | `snes9x_justifier2_crosshair` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` |
| Justifier 2 Color | `White` / `White (blend)` / `Red` / `Red (blend)` / `Orange` / `Orange (blend)` / `Yellow` / `Yellow (blend)` / `Green` / `Green (blend)` / `Cyan` / `Cyan (blend)` / `Sky` / `Sky (blend)` / `Blue` / `Blue (blend)` / `Violet` / `Violet (blend)` / `Pink` ✅ / `Pink (blend)` / `Purple` / `Purple (blend)` / `Black` / `Black (blend)` / `25% Grey` / `25% Grey (blend)` / `50% Grey` / `50% Grey (blend)` / `75% Grey` / `75% Grey (blend)` | `snes9x_justifier2_color` | `White` / `White (blend)` / `Red` / `Red (blend)` / `Orange` / `Orange (blend)` / `Yellow` / `Yellow (blend)` / `Green` / `Green (blend)` / `Cyan` / `Cyan (blend)` / `Sky` / `Sky (blend)` / `Blue` / `Blue (blend)` / `Violet` / `Violet (blend)` / `Pink` / `Pink (blend)` / `Purple` / `Purple (blend)` / `Black` / `Black (blend)` / `25% Grey` / `25% Grey (blend)` / `50% Grey` / `50% Grey (blend)` / `75% Grey` / `75% Grey (blend)` |
| M.A.C.S. Rifle Crosshair | `0` / `1` / `2` ✅ / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` | `snes9x_rifle_crosshair` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` |
| M.A.C.S. Rifle Color | `White` ✅ / `White (blend)` / `Red` / `Red (blend)` / `Orange` / `Orange (blend)` / `Yellow` / `Yellow (blend)` / `Green` / `Green (blend)` / `Cyan` / `Cyan (blend)` / `Sky` / `Sky (blend)` / `Blue` / `Blue (blend)` / `Violet` / `Violet (blend)` / `Pink` / `Pink (blend)` / `Purple` / `Purple (blend)` / `Black` / `Black (blend)` / `25% Grey` / `25% Grey (blend)` / `50% Grey` / `50% Grey (blend)` / `75% Grey` / `75% Grey (blend)` | `snes9x_rifle_color` | `White` / `White (blend)` / `Red` / `Red (blend)` / `Orange` / `Orange (blend)` / `Yellow` / `Yellow (blend)` / `Green` / `Green (blend)` / `Cyan` / `Cyan (blend)` / `Sky` / `Sky (blend)` / `Blue` / `Blue (blend)` / `Violet` / `Violet (blend)` / `Pink` / `Pink (blend)` / `Purple` / `Purple (blend)` / `Black` / `Black (blend)` / `25% Grey` / `25% Grey (blend)` / `50% Grey` / `50% Grey (blend)` / `75% Grey` / `75% Grey (blend)` |
| Show Layer 1 | `Activado` ✅ / `Desactivado` | `snes9x_layer_1` | `enabled` / `disabled` |
| Show Layer 2 | `Activado` ✅ / `Desactivado` | `snes9x_layer_2` | `enabled` / `disabled` |
| Show Layer 3 | `Activado` ✅ / `Desactivado` | `snes9x_layer_3` | `enabled` / `disabled` |
| Show Layer 4 | `Activado` ✅ / `Desactivado` | `snes9x_layer_4` | `enabled` / `disabled` |
| Show Sprite Layer | `Activado` ✅ / `Desactivado` | `snes9x_layer_5` | `enabled` / `disabled` |
| Enable Graphic Clip Windows | `Activado` ✅ / `Desactivado` | `snes9x_gfx_clip` | `enabled` / `disabled` |
| Enable Transparency Effects | `Activado` ✅ / `Desactivado` | `snes9x_gfx_transp` | `enabled` / `disabled` |
| Enable Sound Channel 1 | `Activado` ✅ / `Desactivado` | `snes9x_sndchan_1` | `enabled` / `disabled` |
| Enable Sound Channel 2 | `Activado` ✅ / `Desactivado` | `snes9x_sndchan_2` | `enabled` / `disabled` |
| Enable Sound Channel 3 | `Activado` ✅ / `Desactivado` | `snes9x_sndchan_3` | `enabled` / `disabled` |
| Enable Sound Channel 4 | `Activado` ✅ / `Desactivado` | `snes9x_sndchan_4` | `enabled` / `disabled` |
| Enable Sound Channel 5 | `Activado` ✅ / `Desactivado` | `snes9x_sndchan_5` | `enabled` / `disabled` |
| Enable Sound Channel 6 | `Activado` ✅ / `Desactivado` | `snes9x_sndchan_6` | `enabled` / `disabled` |
| Enable Sound Channel 7 | `Activado` ✅ / `Desactivado` | `snes9x_sndchan_7` | `enabled` / `disabled` |
| Enable Sound Channel 8 | `Activado` ✅ / `Desactivado` | `snes9x_sndchan_8` | `enabled` / `disabled` |

## ![](/emulators/external-links.png) Enlaces externos

* **Código fuente utilizado** : [https://github.com/libretro/snes9x/](https://github.com/libretro/snes9x/)
* **Documentación Libretro** : [https://docs.libretro.com/library/snes9x/](https://docs.libretro.com/library/snes9x/)
