---
title: Spectravideo SV-318
description: 
published: true
date: 2024-07-26T12:51:55.195Z
tags: spectravideo, sv-318, ordenadores
editor: markdown
dateCreated: 2023-09-02T19:03:39.489Z
---

![](/emulators/computers/spectravideo.svg){.align-center}

## Datos técnicos

* **Fabricante**: Spectravideo International
* **Año de lanzamiento**: 1983
* **Sistema operativo**: Microsoft Extended BASIC, CP/M
* **Procesador**: Zilog Z80A a 3,58 MHz
* **ROM**: 32KB (16 KB BIOS / 16 KB BASIC)
* **RAM**: 16KB (SV-318) y 64Kb (SV-328)
* **VRAM**: 16KB
* **Vídeo**: Texas Instruments TMS9918
* **Resolución**: 256×192, 16 colores
* **Sonido**: General Instrument AY-3-8910 (PSG)
* **Soporte**: Cartucho ROM, casete de cinta

## Presentación

El SV-318 es el modelo básico de Spectravideo. Estaba equipado con un teclado tipo chiclet difícil de usar, junto al que había una combinación de cursor y joystick. Éste tiene forma de disco con un orificio en el centro; si se coloca un "palo" de plástico rojo en el orificio, se convierte en un joystick integrado; si se quita el palo, se convierte en un pad direccional para el tratamiento de textos, etcétera. Esta máquina también contiene 16 kB de RAM de usuario (más 16 kB de RAM de vídeo), lo que limita su utilidad, aunque puede ampliarse mediante periféricos externos.

Esta máquina es básicamente idéntica a su hermano mayor el SV-328, las únicas diferencias son el teclado y la cantidad de memoria. La ROM, que se puede ampliar, la placa base y la carcasa entre las 2 máquinas son iguales.

El sistema operativo utilizado, Microsoft Extended BASIC, no debe confundirse con MSX BASIC, aunque a efectos de marketing el término **Microsoft Extended** se utilizó para referirse a **MSX**. El SV-318 no es totalmente compatible con el estándar MSX.

En 1983, Spectravideo anunció el adaptador de videojuegos ColecoVision SV-603 para el SV-318. La compañía dijo que el producto de 70 dólares permitiría a los usuarios "jugar a todos los juegos de ColecoVision".

## Emuladores

[Libretro blueMSX](libretro-bluemsx)