---
title: Libretro MrBoom
description: 
published: true
date: 2024-06-22T11:40:34.838Z
tags: libretro, mrboom
editor: markdown
dateCreated: 2021-05-21T08:33:17.479Z
---

Libretro MrBoom est un core pour faire fonctionner un clone de Bomberman jusqu'à 8 joueurs pour LibRetro / RetroArch.

## ![](/emulators/license.svg) Licence

Ce core est sous licence [**MIT**](https://github.com/libretro/mrboom-libretro/blob/master/LICENSE).

## ![](/emulators/compatibility.png) Compatibilité

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Fonctionnalités

| Fonctionnalité | Supporté |
| :---: | :---: |
| Sauvegardes instantanées | ✔ |
| Rembobinage | ✔ |
| Netplay | ✔ |
| Options du core | ✔ |
| Controllers | ✔ |

## ![](/emulators/bios.svg) BIOS

>**Aucun bios n'est requis.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Extensions supportées

Le noyau Mr.Boom ne comporte pas d'utilisation d'extension. Il suffit de charger et de démarrer le core.

### Emplacement

Placez les roms comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 ports
┃ ┃ ┃ ┃ ┣ 📁 mrboom
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **mrboom.game**

## ![](/emulators/advanced-configuration.png) Configuration avancée de l'émulateur

>Pour pouvoir conserver vos configurations personnalisées lors d'une mise à jour, nous vous conseillons d'utiliser notre fonctionnalité [Surcharges de configuration](./../../../advanced-usage/configuration-override).
{.is-info}

### Accéder aux options

Vous pouvez configurer diverses options de deux façons différentes.

* Via le menu RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via le fichier `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Options du core

| Options | Valeur des options | Variable (pour surcharge) | Valeurs des variables |
| --- | --- | --- | --- |
| Team mode | `Selfie` ✅ / `Color` / `Sex` / `Skynet` | `mrboom-teammode` | `Selfie` / `Color` / `Sex` / `Skynet` |
| Monsters | `Activé` ✅ / `Désactivé` | `mrboom-nomonster` | `ON` / `OFF` |
| Level select | `Normal` ✅ / `Candy` / `Penguins` / `Pink` / `Jungle` / `Board` / `Soccer` / `Sky` / `Aliens` / `Random` | `mrboom-levelselect` | `Normal` / `Candy` / `Penguins` / `Pink` / `Jungle` / `Board` / `Soccer` / `Sky` / `Aliens` / `Random` |
| Aspect ratio | `Native` ✅ / `4:3` / `16:9` | `mrboom-aspect` | `Native` / `4:3` / `16:9` |
| Music volume | `0` / `5` / `10` / `15` / `20` / `25` / `30` / `35` / `40` / `45` / `50` / `55` / `60` / `65` / `70` / `75` / `80` / `85` / `90` / `95` / `100` ✅ | `mrboom-musicvolume` | `0` / `5` / `10` / `15` / `20` / `25` / `30` / `35` / `40` / `45` / `50` / `55` / `60` / `65` / `70` / `75` / `80` / `85` / `90` / `95` / `100` |
| Sfx volume | `0` / `5` / `10` / `15` / `20` / `25` / `30` / `35` / `40` / `45` / `50` ✅ / `55` / `60` / `65` / `70` / `75` / `80` / `85` / `90` / `95` / `100` | `mrboom-sfxvolume` | `0` / `5` / `10` / `15` / `20` / `25` / `30` / `35` / `40` / `45` / `50` / `55` / `60` / `65` / `70` / `75` / `80` / `85` / `90` / `95` / `100` |

## ![](/emulators/external-links.png) Liens externes

* **Code utilisé** : [https://github.com/libretro/mrboom-libretro/](https://github.com/libretro/mrboom-libretro)
* **Site officiel** : [http://mrboom.mumblecore.org/](http://mrboom.mumblecore.org/)