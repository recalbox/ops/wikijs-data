---
title: Packs de texturas personnalizadas
description: 
published: true
date: 2023-09-20T09:31:58.219Z
tags: dolphin, texturas, packs, personalizadas
editor: markdown
dateCreated: 2023-09-20T09:31:58.219Z
---

Además de ejecutar juegos de la Gamecube, Dolphin también puede utilizarse para mejorar texturas o incluso crear vídeos en alta definición. Estas texturas y vídeos han sido creados por entusiastas (gracias a ellos) para que vuestros juegos favoritos sean más bonitos todavía.

Actualmente hay disponibles paquetes de texturas para los siguientes juegos:

* Mario Kart Double Dash
* Luigi's Mansion
* The Legend of Zelda: Ocarina of Time
* Resident Evil 2
* Resident Evil 3 Nemesis

Esta lista no es exhaustiva, puede haber otros juegos en preparación o que ya han salido a la venta en el momento de leer este tutorial.

Las texturas se encuentran disponibles en el [Dolphin forum](https://forums.dolphin-emu.org/Forum-custom-texture-projects). Los formatos soportados son :

* BC7
* DDS

Colocad vuestros packs de texturas en la siguiente carpeta: `/recalbox/share/saves/dolphin-emu/Load/Textures/`.

Todas las texturas deben estar en una carpeta con el ID del juego (o el ID + el equivalente de 3 letras sin región).

* GM4: ID sin región
* GM4P01: ID completo 

![Exemple pour Mario Kart Double Dash (Europe)](/emulators/consoles/gc-textures-packs.png)

El ID se puede encontrar dentro del emulador Dolphin:

* Lanzad cualquier juego de GameCube.
* Una vez iniciado el juego, pulsad simultáneamente `Hotkey` + `B`. 
* Aparecerá el menú de Dolphin con una lista de vuestros juegos.
* Buscad el juego cuyo ID necesitas averigüar. El ID se muestra en la cuarta columna y es una combinación de 6 letras y números.

>¡Si las texturas no están en una carpeta con el nombre correcto, el pack no funcionará!
{.is-warning}