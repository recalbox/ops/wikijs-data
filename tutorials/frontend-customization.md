---
title: 🛠️ Personnalisation du Frontend
description: (EmulationStation / RetroArch)
published: true
date: 2021-09-04T18:04:54.287Z
tags: frontend, tutoriel, personnalisation
editor: markdown
dateCreated: 2021-05-21T07:52:01.078Z
---

Ici, vous pourrez personnaliser EmulationStation comme avec les thèmes et autres musiques, par exemple.

Liste des tutoriels disponibles :

[Ajouter des thèmes à EmulationStation](add-themes-into-emulationstation)
[Ajouter une musique personnalisée à EmulationStation](add-custom-music-into-emulationstation)
[Ajuster l'image (Pixel Perfect)](pixel-perfect)