---
title: 📌 Otros
description: 
published: true
date: 2024-07-26T07:12:51.362Z
tags: tutorial, otros
editor: markdown
dateCreated: 2023-09-01T09:27:16.197Z
---

Aquí se encuentran los tutoriales que no se pueden clasificar en ninguna de las categorías existentes, pero que pueden resultar muy útiles.

Aquí tenéis la lista de categorías disponibles:

[Arranque/Parada del Switch Mausberry](add-mausberry-switch-on-off-button)
[Añadir un botón on/off a vuestra Recalbox](add-on-off-button-to-your-recalbox)
[Comandos de Linux útiles](useful-linux-commands)
[Configuración para el GameHat](gamehat-configuration)
[Configurar los sistemas GPD](configure-gpd-systems)
[Desactivar del chip T2 en el Mac](disable-mac-t2-chip)
[Hyperion en Recalbox](hyperion-on-recalbox)
[Instalación del NesPi4Case](nespi4case-installation)
[Modificar el idioma de RetroArch](change-retroarch-language)
[Utilización de interfaz con el teclado / ratón](emulationstation-use-with-keyboard-and-mouse)