---
title: Configuración de botones personalizados
description: 
published: true
date: 2024-07-26T06:48:33.949Z
tags: configuración, botones, personalizados
editor: markdown
dateCreated: 2023-09-18T11:01:54.614Z
---

En esta sección encontraréis todo aquello que tiene que ver con las configuraciones específicas de los mandos/controladores.

Aquí tenéis la lista de tutoriales disponibles:

[Configuración de mandos N64](n64-controller-configuration)
[Crear una configuración personalizada para cada emulador](create-custom-configuration-per-emulator)
[Cómo personalizar el mapping de un mando](how-to-customize-controller-mapping)