---
title: 🎮 Contrôleurs
description: 
published: true
date: 2021-09-04T22:28:14.512Z
tags: tutoriel, contrôleurs
editor: markdown
dateCreated: 2021-05-21T07:51:55.015Z
---

Tout cette partie se consacre à vos manettes et autres sticks.

Voici les catégories disponibles :

[Configuration des boutons personnalisés](configuration-of-customized-buttons)
[Configuration/Test](test-configuration)
[Encodeurs USB](usb-encoders)
[GPIO](gpio)
[Manettes](controllers)