---
title: Hypseus
description: 
published: true
date: 2021-09-15T07:39:32.565Z
tags: daphne, singe, hypseus
editor: markdown
dateCreated: 2021-07-05T21:45:02.984Z
---

**Hypseus** for the emulation of **arcade games on laserdisc**, is a fork of Daphne written by Jeffrey Clark to bring him some improvements:

* Update of the MPEG2 decoder
* SDL2 support
* Etc...

## ![](/emulators/license.svg) License

This core is under [**GPLv3**](https://github.com/btolab/hypseus/blob/master/LICENSE) license.

**Hypseus**, Multiple Arcade Laserdisc Emulator  
 Copyright (C) 2016 [Jeffrey Clark](https://github.com/btolab)

**Daphne**, the First Ever Multiple Arcade Laserdisc Emulator  
 Copyright (C) 1999-2013 [Matt Ownby](http://www.daphne-emu.com/site3/statement.php)

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Features

| Feature | Supported |
| :---: | :---: |

## ![](/emulators/bios.svg) BIOS

>**No bios is required.**
{.is-success}

## ![](/emulators/roms.png) Roms

### How to play laserdisc games on Recalbox ?

* To add a game to laserdisc, you need the two components of such a game:

  * The original laserdisc image, which contains the video and audio content.
  * The ROM file, which is the game program.

**The ROM** is a simple ZIP file that must be copied to the `roms/daphne/` subdirectory. This contains `.bin' files.

* The laser disc image consists of several files:

  * **One or more video files** (`*.m2v`)
  * **One or more audio files** (`*.ogg`)
  * **One or more index files** (`*.dat`): can be created by the emulator at the first launch (slow operation)
  * **A frame file** (`.txt`) : to associate frame numbers to video files.

* All these files must be copied in a directory with a `.daphne` extension, under the `.daphne` directory.
* The first line of your `.txt` file is a path and must be . (just the dot character), change it if it is not.
* You can also create a file with the extension `.commands` to pass game specific parameters to the emulator. See below for more details. 
* The directory, frame file and commands file must have the same name as the ROM file! (only the extensions are different). 

### Location

Put your roms like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 **game.daphne**
┃ ┃ ┃ ┃ ┃ ┣ 🗒  **game.command**
┃ ┃ ┃ ┃ ┃ ┣ 🗒  **game.txt**
┃ ┃ ┃ ┃ ┃ ┣ 🗒  **game.dat**
┃ ┃ ┃ ┃ ┃ ┣ 🗒  **game.m2v**
┃ ┃ ┃ ┃ ┃ ┣ 🗒  **game.ogg**
┃ ┃ ┃ ┃ ┃ ┣ 🗒  **...**
┃ ┃ ┃ ┃ ┣ 📁 **roms**
┃ ┃ ┃ ┃ ┃ ┣ 🗒  **game.zip**

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>**This core have no option.**
{.is-success}

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/DirtBagXon/hypseus-singe](https://github.com/DirtBagXon/hypseus-singe)
* **Official website**: [http://www.daphne-emu.com/site3/index_hi.php](http://www.daphne-emu.com/site3/index_hi.php)
* **FAQ**: [http://www.daphne-emu.com/site3/FAQ_USA.php](http://www.daphne-emu.com/site3/FAQ_USA.php)
* **Core Wiki**: [https://www.daphne-emu.com:9443/mediawiki/index.php/Main_Page](https://www.daphne-emu.com:9443/mediawiki/index.php/Main_Page)