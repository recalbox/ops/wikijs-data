---
title: FAQ
description: 
published: true
date: 2024-07-16T19:50:05.370Z
tags: scummvm, faq
editor: markdown
dateCreated: 2021-08-08T09:23:25.108Z
---

## What is SCUMM?

**Script Creation Utility for Maniac Mansion** (SCUMM) is a video game engine developed by Lucasfilm Games, later renamed LucasArts, to facilitate the development of its first graphic adventure game Maniac Mansion (1987).  
It was then reused as an engine for the following adventure games of **Lucasarts.**

It sits between the game engine and the programming language, allowing designers to create locations, objects and dialogue sequences without writing code.

It also allows the script and data files to be platform independent. SCUMM is also home to embedded game engines, such as Interactive **MUsic Streaming Engine** (iMUSE), **INteractive Streaming ANimation Engine** (INSANE), **CYST** (the in-game animation engine), **FLEM** (locations and names of objects in a room), and **MMUCUS**.

**SCUMM** has been ported to the following platforms: 3DO, Amiga, Apple II, Atari ST, CDTV, Commodore 64, Fujitsu FM Towns & Marty, Apple Macintosh, Nintendo Entertainment System, DOS, Microsoft Windows, Sega CD (Mega-CD), and TurboGrafx-16/PC Engine.

## What is ScummVM?

**ScummVM** is a program that allows you to play certain graphic adventure games point-and-click, as long as you provide the game files.

The idea behind it is that ScummVM just replaces the game executables, allowing you to play it on systems for which it was not originally intended!

## How to play ScummVM games on Recalbox?

### Add a ScummVM game:

* Go to [https://www.scummvm.org/compatibility/2.7.0/](https://www.scummvm.org/compatibility/2.7.0/) to find your game et take its `Game Long Name` and its `Game Short Name`.
* Create a folder with the long name of the game, followed by the extension `.scummvm` and copy the game files into it. 
* In this same folder, you must add a new file, named `[short name].scummvm`. The file's content must be a single line with the short name of the game.

>Add the long name of the previous site as the content of the `[short name].scummvm` file. This will allow the scraper to detect it more easily.
{.is-success}

**For example**, you can copy the game « Broken Sword » into the directory « Broken Sword 1.scummvm » in the directory `scummvm`.
In this directory, create a file named `sword1.scummvm`

```text
scummvm
|- Broken Sword 1.scummvm
|  |- sword1.scummvm
|  |- ... other game files
```

The name of this directory will be displayed as name of the game list.

## Are all Scumm games compatible?

Please read the [compatibility list](http://scummvm.org/compatibility/) on the ScummVM website.

>It contains an up-to-date list of supported games and their level of compatibility.
{.is-info}

## How do I set up the Roland MT-32 emulation for ScummVM games?

* First, you need to copy the MT32 ROMs (`MT32_CONTROL.ROM` and `MT32_PCM.ROM`) into a directory, for example `/recalbox/share/bios`.
* Then modify your ScummVM configuration file (`scummvm.ini` or `.scummvmrc`) and add the following lines to the `[scummvm]` section:

```text
extrapath=/recalbox/share/bios
mt32_device=mt32
music_driver=mt32
```

**Other solution:**
You can also simply copy the two ROMs into the game directory.

This way, no need to modify the configuration file, but these files will then be duplicated for each game requiring the Roland music!

You must use the ROM files supported** by ScummVM.

For example, with the following MD5 signatures:

```text
5626206284b22c2734f3e9efefcd2675 MT32_CONTROL.ROM
89e42e386e82e0cacb4a2704a03706ca MT32_PCM.ROM
```