---
title: Vice_x64sc
description: 
published: true
date: 2024-07-25T16:32:33.324Z
tags: commodore-64, c64, vice, x64sc, 9.1+
editor: markdown
dateCreated: 2024-07-25T16:32:33.324Z
---



## ![](/emulators/license.svg) License

This core is under [**GPLv2**](https://sourceforge.net/p/vice-emu/code/HEAD/tree/trunk/vice/COPYING) license.

## ![](/emulators/compatibility.png) Compatibilité

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Fonctionnalités



## ![](/emulators/bios.svg) BIOS

### List of optional bios

| Filename | Description | MD5 | Provided |
| :---: | :---: | :---: | :---: |
| JiffyDOS_C64.bin | JiffyDOS C64 Kernal | be09394f0576cf81fa8bacf634daf9a2 | ❌ |
| JiffyDOS_C128.bin | JiffyDOS C128 Kernal | cbbd1bbcb5e4fd8046b6030ab71fc021 | ❌ |
| JiffyDOS_C1541-II.bin | JiffyDOS 1541 drive BIOS | 1b1e985ea5325a1f46eb7fd9681707bf | ❌ |
| JiffyDOS_1571_repl310654.bin | JiffyDOS 1571 drive BIOS | 41c6cc528e9515ffd0ed9b180f8467c0 | ❌ |
| JiffyDOS_1581.bin | JiffyDOS 1581 drive BIOS | 20b6885c6dc2d42c38754a365b043d71 | ❌ |

### Location

Put the bios like this:

┣ 📁 recalbox  
┃ ┣ 📁 share  
┃ ┃ ┣ 📁 bios  
┃ ┃ ┃ ┣ 📁 vice  
┃ ┃ ┃ ┃ ┣ 🗒 **JiffyDOS_C64.bin**  
┃ ┃ ┃ ┃ ┣ 🗒 **JiffyDOS_C128.bin**  
┃ ┃ ┃ ┃ ┣ 🗒 **JiffyDOS_C1541-II.bin**  
┃ ┃ ┃ ┃ ┣ 🗒 **JiffyDOS_1571_repl310654.bin**  
┃ ┃ ┃ ┃ ┣ 🗒 **JiffyDOS_1581.bin**  

## ![](/emulators/roms.png) Roms

### Supported extensions

Roms must have the extension:

* .bin
* .cmd
* .crt
* .d1m
* .d2m
* .d4m
* .d64
* .d6z
* .d67
* .d71
* .d7z
* .d80
* .d81
* .d82
* .d8z
* .d90
* .dhd
* .g41
* .g4z
* .g64
* .g6z
* .m3u
* .nbz
* .nib
* .p00
* .p64
* .prg
* .t64
* .tap
* .tcrt
* .vfl
* .vsf
* .x64
* .x6z
* .zip
* .7z
* .gz

This system supports compressed roms in .zip/.7z format. However, be careful, it is only an archive.

The files contained in the .zip/.7z must correspond to the extensions mentioned above.
Moreover, each .zip/.7z file must contain only one compressed rom.

### Location

Put the roms like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 c64
┃ ┃ ┃ ┃ ┣ 🗒 **game.zip**

>The roms using **No-Intro** format are highly recommended.
{.is-success}

>For more information about roms, go to [this tutorial](./../../../tutorials/games/generalities/isos-and-roms)!
{.is-info}

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://sourceforge.net/projects/vice-emu/](https://sourceforge.net/projects/vice-emu/)