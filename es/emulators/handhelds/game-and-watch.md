---
title: Game & Watch
description: Nintendo Game & Watch
published: true
date: 2024-07-26T08:32:02.727Z
tags: nintendo, game, game&watch, watch, portátiles
editor: markdown
dateCreated: 2023-09-04T08:01:44.708Z
---

![](/emulators/handheld/gameandwatch.svg){.align-center}

## Detalles técnicos

* **Fabricante**: Nintendo
* **Año de lanzamiento**: 1980
* **Cantidad vendida**: 43,4 millones
* **Juego más vendido**: Donkey Kong

## Presentación

Game and Watch** son juegos electrónicos portátiles, precursores de las consolas portátiles, producidos por Nintendo de 1980 a 1991. También se utilizan como relojes y despertadores. Con 43,4 millones de copias vendidas en todo el mundo, estos 59 juegos representan uno de los mayores éxitos de la compañía de Kyōto.

Game and Watch fue idea de Gunpei Yokoi, el creador de Game Boy. Tuvo la idea de Game and Watch durante un viaje en shinkansen. En el asiento contiguo, un hombre de negocios jugaba con su calculadora para pasar el rato. Quería desarrollar un juguete que los adultos japoneses pudieran utilizar discretamente en el transporte público o en viajes de negocios, por ejemplo...

Así fue como el primer Game and Watch vio la luz el 28 de abril de 1980, bajo el nombre de Ball. Fue un éxito inmediato, pero Nintendo pronto se dio cuenta de que la mayoría de los compradores eran niños, lo que animó a la empresa a sacar nuevos juegos más coloridos para los más pequeños. Así surgieron los personajes más famosos de la marca: Mario, Donkey Kong y Link.

El Game and Watch sólo permitía jugar a un número limitado de juegos por modelo (esto se debía a la pantalla de cristal líquido de la máquina, en la que las imágenes de los juegos estaban preimpresas y se iluminaban sucesivamente durante el juego).

Hay un total de 60 modelos de Game and Watch en diez series.

## Emuladores

[Libretro GW](libretro-gw)