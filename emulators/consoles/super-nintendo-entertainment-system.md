---
title: Super Nintendo Entertainment System
description: Super Nintendo Entertainment System
published: true
date: 2025-01-05T23:01:10.872Z
tags: consoles, nintendo, snes, super nintendo
editor: markdown
dateCreated: 2021-07-28T17:48:28.980Z
---

![](/emulators/consoles/snes-us.svg){.align-center}

## Technical data

* **Manufacturer**: Nintendo
* **Year of release**: 1990
* **Units sold**: 49.10 millions
* **Best-selling game**: Super Mario World
* **CPU**: 16-bit Custom 65C816 @ 3.58 MHz
* **RAM**: 128kB / VRAM : 64kB
* **Video**: 16-bit PPU (Picture Processor Unit)
* **Resolution**: 256x224, 256 colors (32768 colors palette)
* **Sound chip**: 8-bit Sony SPC700, 8 channels
* **Cart size**: 256kB - 6MB

## Presentation

The **Super Nintendo Entertainment System** (commonly abbreviated **Super NES** or **SNES** or **Super Nintendo**), or **Super Famicom** in Japan, is a video game system from the Japanese manufacturer Nintendo released in November 1990. In North America, the console was released with a very different look. In South Korea, the Super Nintendo is distributed by Hyundai Electronics under the name **Super Comboy**.

In 1997 and 1998, Nintendo decided to refresh the design by releasing the SNS-101 model, sometimes called **Super NES 2** in the United States, and the **Super Famicom Jr.** in Japan.

## Emulators

[Libretro Mesen_s](libretro-mesen_s)
[Libretro Snes9x](libretro-snes9x)
[Libretro Snes9x_2002](libretro-snes9x-2002)
[Libretro Snes9x_2005](libretro-snes9x-2005)
[Libretro Snes9x_2010](libretro-snes9x-2010)
[Libretro Supafaust](libretro-supafaust)
[Libretro bsnes](libretro-bsnes)
[Libretro bsnes HD](libretro-bsnes-hd)
[piSNES](pisnes)