---
title: Libretro Yabause
description: 
published: true
date: 2024-06-23T11:31:47.674Z
tags: libretro, saturn, yabause
editor: markdown
dateCreated: 2021-05-21T08:26:33.081Z
---

**Libretro Yabause** est un port de l'émulateur **Yabause** pour Libretro prenant en charge la Sega Saturn.

## ![](/emulators/license.svg) Licence

Ce core est sous licence [**GPLv2**](https://github.com/libretro/yabause/blob/master/yabause/COPYING).

## ![](/emulators/compatibility.png) Compatibilité

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ❌ | ❌ | ❌ | ❌ | ❌ | ❌ | ✅ | ❌ |

## ![](/emulators/features.png) Fonctionnalités

| Fonctionnalité | Supporté |
| :---: | :---: |
| Redémarrage  | ✔ |
| Captures d'écran | ✔ |
| Sauvegardes | ✔ |
| Sauvegardes instantanées | ✔ |
| Options du core | ✔ |
| RetroAchievements | ✔ |
| Cheats RetroArch | ✔ |
| Contrôles | ✔ |
| Remapping | ✔ |
| Contrôle du disque | ✔ |

## ![](/emulators/bios.svg) BIOS

### Liste des bios obligatoires

| Nom de fichier | Description | MD5 | Fourni |
| :---: | :---: | :---: | :---: |
| saturn_bios.bin | BIOS Saturn | af5828fdff51384f99b3c4926be27762 | ❌ |

### Emplacement

Placez les BIOS comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 saturn
┃ ┃ ┃ ┃ ┣ 🗒 **saturn_bios.bin**

## ![](/emulators/isos.svg) Isos

### Extensions supportées

Les isos doivent avoir les extensions suivantes :

* .cue/.bin
* .m3u
* .img/.ccd
* .iso
* .mds/.mdf
* .chd

### Emplacement

Placez les isos comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 saturn
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.bin**
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.cue**

>Les isos au format **Redump** sont vivement conseillés.
{.is-success}

>Pour plus d'information sur les isos, rendez-vous sur [ce tutoriel](/./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuration avancée de l'émulateur

>Pour pouvoir conserver vos configurations personnalisées lors d'une mise à jour, nous vous conseillons d'utiliser notre fonctionnalité [Surcharges de configuration](./../../../advanced-usage/configuration-override).
{.is-info}

### Accéder aux options

Vous pouvez configurer diverses options de deux façons différentes.

* Via le menu RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via le fichier `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Options du core

| Options | Valeurs des options | Variable (pour surcharge) | Valeurs des variables |
| --- | --- | --- | --- |
| Saut d'images | `Désactivé` ✅ / `Activé` | `yabause_frameskip` | `disabled` / `enabled` |
| Forcer le BIOS HLE (Redémarrage requis) | `Désactivé` ✅ / `Activé` | `yabause_force_hle_bios` | `disabled` / `enabled` |
| Cartouche de mémoire supplémentaire (Redémarrage requis) | `None` ✅ / `1 Mo de RAM` / `4 Mo de RAM` | `yabause_addon_cartridge` | `None` / `1M_ram` / `4M_ram` |
| Adaptateur 6 joueurs sur le port 1 | `Désactivé` ✅ / `Activé` | `yabause_multitap_port1` | `disabled` / `enabled` |
| Adaptateur 6 joueurs sur le port 2 | `Désactivé` ✅ / `Activé` | `yabause_multitap_port2` | `disabled` / `enabled` |
| Nombre de fils d'exécution (Redémarrage requis) | `1` / `2` / `4` ✅ / `8` / `16` / `32` | `yabause_numthreads` | `1` / `2` / `4` / `8` / `16` / `32` |

## ![](/emulators/external-links.png) Liens externes

* **Code source utilisé** : [https://github.com/libretro/yabause/](https://github.com/libretro/yabause/)
* **Documentation Libretro** : [https://docs.libretro.com/library/yabause/](https://docs.libretro.com/library/yabause/)