---
title: 4. 🔧 COMPATIBILIDADE DE HARDWARE
description: 
published: true
date: 2023-07-18T12:05:53.957Z
tags: hardware, compatibilidade
editor: markdown
dateCreated: 2021-11-28T14:07:57.890Z
---

Nas páginas a seguir, você poderá verificar se o seu hardware foi testado e é compatível, bem como se um sistema ou emulador específico é compatível com o seu hardware.