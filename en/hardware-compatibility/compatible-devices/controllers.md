---
title: Controllers
description: 
published: true
date: 2021-09-29T15:07:30.017Z
tags: compatibility, controllers
editor: markdown
dateCreated: 2021-08-05T21:42:23.699Z
---

## Official Console Controllers

| Device | Wired | Wireless | Latest version tested | Comments |
| :---: | :---: | :---: | :---: | :---: |
| **MICROSOFT** |  |  |  |  |
| Xbox Controller from original Xbox | ✅ |  |  | ✅ With USB modification |
| Xbox 360 Controller | ✅ | ☑️ | 7.1.1 | ☑️ With Xbox 360 dongle |
| Xbox One Controller | ✅ | ☑️ | 7.3.0 | ☑️ With dongle like: [Xbox Wireless Adapter](https://www.xbox.com/en-US/accessories/adapters/wireless-adapter-windows), 8BitDo (wait 5s), Janswall,... |
| Xbox One Elite Controller | ✅ |  | 7.1.1 |  |
| Xbox One S Controller | ✅ |  | 7.0.1 |  |
| Xbox One Series X\|S Controller | ✅ |  | 7.0.1 |  |
| **NINTENDO** |  |  |  |  |
| GameCube Controller | ✅ | ☑️ |  | ✅ Mayflash GameCube Controller Adapter for PC USB ☑️ With dongle: 8BitDo GBros Adaptator |
| GameCube WaveBird Controller | ❌ | ☑️ |  | ☑️ With dongle: 8BitDo GBros Adaptator (with a GameCube extension cable) |
| NES Mini Controller | ✅ | ☑️ |  | ✅ With Mayflash USB Converter ☑️ With dongle: 8BitDo GBros Adaptator |
| SNES Mini Controller | ✅ | ☑️ |  | ✅ With Mayflash USB Converter ☑️ With dongle: 8BitDo GBros Adaptator |
| JoyCon |  | ☑️ | 7.2.2 |  |
| Switch Pro Controller |  | ☑️ | 7.2.2 |  |
| Wii Classic Controller | ✅ | ☑️ | v4.1 | ✅ With Mayflash USB Converter ☑️ Natively & With dongle 8BitDo GBros Adaptator |
| Wii Classic Pro Controller | ✅ | ☑️ |  | ✅ With Mayflash USB Converter ☑️ With dongle: 8BitDo GBros Adaptator |
| Wiimote | ✅ | ☑️ | 7.2.2 | ✅ Avec la barre Mayflash Dolphin Bar |
| Wii U Pro Controller | ❌ | ☑️ | 4.1 | Nativement & avec un dongle : 8BitDo, Janswall,...|
| Super Nintendo Entertainment System Controller for Nintendo Switch |  | ☑️ | 7.2.2 |  |
| **SNK** |  |  |  |  |
| NeoGeo Mini Controller | ❌ |  |  | Some issues while using an USB-C adapter |
| **SONY** |  |  |  |  |
| PlayStation 1 Controller | ✅ |  | v 3.3beta 17 & v4.1.0-dev | With Generic USB adapter for 2 PS1/PS2 PAD controllers |
| PlayStation 1 Dualshock Controller | ✅ |  | v 3.3beta 17 & v4.1.0-dev | With Generic USB adapter for 2 PS1/PS2 PAD controllers |
| PlayStation 2 Dualshock 2 Controller | ✅ |  | v 3.3beta 17 & v4.1.0-dev | With Generic USB adapter for 2 PS1/PS2 PAD controllers |
| PlayStation 3 DualShock 3 | ✅ | ☑️ | 7.0 | ☑️ With dongle too, like: 8BitDo, Janswall,... or any bluetooth dongle |
| PlayStation 4 DualShock 4 | ✅ | ☑️ | 7.0.1 | ☑️ With Official Sony PS4 dongle & With dongle like: 8BitDo, Janswall,... |
| PlayStation 5 DualSense | ✅ |  | 7.0.1 | ✅ With [this](https://forum.recalbox.com/topic/8920/a-lire-manettes-ps4). |
| Playstation Classic Mini Controller | ✅ |  |  |  |

## Others controllers

| Device | Wired | Wireless | Latest version tested | Comments |
| :---: | :---: | :---: | :---: | :---: |
| 8bitdo controllers | ✅ | 7.0.1 | See the page [8bitdo on Recalbox](./../../hardware-compatibility/compatible-devices/8bitdo-on-recalbox) |
|  |  |  |  |
| **Bigben Interactive** |  |  |  |
| Pad Luxe PS3 | ✅ |  |  |
| PlayStation 3 BB4259 | ✅ |  |  |
| PlayStation 3 BB4401 | ✅ |  |  |
| Big Ben BB5033 (with 2.4GHz RF USB dongle) | ✅ | v17.11.10.2 |  |
| **Bowick** |  |  |  |
| Wireless Bluetooth Controllers for PS3 | ✅ | v4.0.2 | Select `shawan` in the configuration |
| **BUFFALO** |  |  |  |
| iBuffalo SNES Classic USB Gamepad | ✅ |  | The buttons `Turbo` and `Clear` work |
| **CSL-Computer** |  |  |  |
| Gamepad USB SNESw | ✅ |  |  |
| USB Wireless Gamepad (Model 2016) | ✅ |  |  |
| **EasySMX** |  |  |  |
| EMS-9100 Gamepad USB Wired | ✅ |  | Has a home button for configuration (see attached manual) Requires Android 4.0 or higher |
| **Free** |  |  |  |
| Freebox Controller | ✅ |  | Analog mode |
| **Gamesir** |  |  |  |
| Gamesir G3s | ✅ | v4.0.2 | Pad can be configured in menu but behaves awkwardly in game |
| **HORI** |  |  |  |
| Fighting Commander | ✅ | v4.0.2 |  |
| Pokken Tournament for Wii U | ✅ | v4.0.2 |  |
| **Kreema** |  |  |  |
| Gen Game S3 |  | v18.04.20 | Does not work properly with bluetooth connection (Wireless Bluetooth 3.0) |
| Gen Game S5 | ❌ | 17.13.02 | Can be paired, but is acting weird when configuring. Recognized as a keyboard, if both analog stick are pressed. In configuration, it doubles the pressed button for every entry. Does not work in a game, just in the main menu |
| **iNNEXT** |  |  |  |
| IN-Y-D002-024\*2 | ✅ | v4.0.2 |  |
| **Konix** |  |  |  |
| Konix PS3/PC | ✅ |  |  |
| KONIX KX-CT-PC | ✅ | v4.0.2 |  |
| **KROM** |  |  |  |
| Kay Pro Gaming Wired Gamepad (NXROMKEY) | ✅ |  | Detected as Xbox 360 Joypad, but impossible to map keys. |
| **Logitech** |  |  |  |
| Cordless Rumbleepad 2 (with 2.4GHz RF USB dongle) | ✅ |  |  |
| Dual Action Gamepad (USB) | ✅ | v4.0.0-beta4 |  |
| Precision Gamepad (USB) | ✅ | v4.0.0-beta4 |  |
| Rumblepad 2 (USB) | ✅ |  |  |
| **Logitech G** |  |  |  |
| F310 Gamepad (USB) | ✅ |  |  |
| F710 Wirelesss Gamepad (with 2.4GHz RF USB dongle) | ✅ | v4.0.0-beta4 | On top, turn the switch to D, not X. |
| **MadCatz** |  |  |  |
| MadCatz Wireless FightPad for PlayStation 3 (with 2.4GHz RF USB dongle) | ✅ |  |  |
| **Nacon** |  |  |  |
| GC-400ES Wired Controller | ✅ | 6.1.1 | [Vendeur](https://www.nacongaming.com/controllers/) (tested on v1, v2 and v3 ; recognized as a generic Xbox controller) |
| Revolution Wired | ✅ | v6.1.1 | [Vendeur](https://nacongaming.com/controllers/) (tested on v1, v2 and v3 ; recognized as a generic Xbox controller) |
| Nacon esport controllers/revolution wired et GC-400ES | ✅ |  | [Vendeur](https://www.nacongaming.com/controllers/) (tested on v1, v2 and v3 ; recognized as a generic Xbox controller) |
| **NGS** |  |  |  |
| Maverick (PS3/PC) | ✅ |  |  |
| Nvidia |  |  |  |
| Shield 2017 (USB) | ❌ |  | Works fine in ES but take some times after launching a game. |
| **PDP** |  |  |  |
| PlayStation 3 Rock Candy (With bluetooth dongle) | ✅ |  |  |
| Xbox One & Windows 10 | ❌ | v18.04.20 | Detected but cannot be configured |
| **PowerA** |  |  |  |
| MOGA PRO Power | ✅ | v17.11.02 |  |
| **Razer** |  |  |  |
| Razer Sabertooth | ✅ | v4.0.0-beta2 |  |
| **RetroUSB** |  |  |  |
| USB Super RetroPad (SNES) | ✅ |  |  |
| **Saitek** |  |  |  |
| P380 | ✅ |  |  |
| P480 | ✅ |  |  |
| P880 | ✅ |  |  |
| **SlickBlue** |  |  |  |
| SlickBlue Wireless Bluetooth Game Pad Controller for Sony PlayStation 3 | ❌ |  |  |
| **Snakebyte** |  |  |  |
| idroid:con (With bluetooth dongle) | ✅ | v3?3beta17 |  |
| blu:con (With bluetooth dongle) | ❌ | v4.0-rpi2-build15 |  |
| **Speedlink** |  |  |  |
| Speedlink Strike FX - PS3 (câble USB) | ❌ | v4.0.0-rpi2B |  |
| Speedlink Torid Wireless Gamepad | ✅ | v4.0.0-rpi3 |  |
| **SteelSeries** |  |  |  |
| SleetSeries Free Mobile Wireless | ✅ | v4.0.0-rpi3 | Require a bluetooth dongle |
| SteelSeries Stratus Wireless Gaming Controller | ❌ | v4.0.0-rpi3B | Bluetooth can be "paired" but is not detected afterwards |
| **Subsonic** |  |  |  |
| Subsonic Bluetooth Controller PS3 | ✅ |  | (gasia driver) |
| **ThrustMaster** |  |  |  |
| T-Wireless | ✅ | v17.11.10.2 |  |
| **VZTEC** |  |  |  |
| VZTEC USB Double Shock Controller Game Pad Joystick (VZ-GA6002) | ✅ | v4.0.0-beta3 |  | **Xiaomi** |  |  |  |
| Xiaomi Controller | ✅ |  | Need to pair after every disconnect |
| **Marque non trouvé** |  |  |  |
| PlayStation 3 Afterglow Wireless Controller (with 2.4GHz RF USB dongle) | ✅ |  |  |
| PlayStation 3 GASIA Clone | ✅ |  |  |
| PlayStation 3 ShawnWan Clone | ✅ |  |  |
| Sony PlayStation Brandery compatible DualShowk Pad Controller | ✅ | v3.3beta 17 & v4.1.0-dev | With Generic USB adapter for 2 PS1/PS2 PAD controllers |

## Controller adapters

| Device | Wired | Wireless | Latest version tested | Comments |
| :---: | :---: | :---: | :---: | :---: |
| 4nes4snes | ✅ | 3.3.0 |  |
| **Cronusmax** |  |  |  |
| CronusMAX Adapter (Xbox 360 Out) | ✅ | v3.3beta17 |  |
| **Mayflash** |  |  |  |
| Mayflash Dolphin Bar | ✅ | 7.2.1 |  |
| Mayflash GameCube Controller Adapter |  | 6.1 | Only work on dolphin emulator for GameCube |
| Mayflash N64 Controller Adapter | ✅ | 4.0.0-dev |  |
| Mayflash NES/SNES Controller Adapter | ✅ | 4.0.0 | Need Usbhid |
| Mayflash PC052 Wii Classic Controller | ✅ | 4.1.0 | For wii classic, nes mini & snes mini controller |
| Mayflash Sega Saturn Controller Adapter | ✅ | 4.0.0 | Need Usbhid |
| **OriGlam** |  |  |  |
| Wii Sensor Bar USB | ❌ | 2018.x.x |  |
| **Autres** |  |  |  |
| PSP 1000 - 3000 with FuSa Gamepad and MiniUSB cable | ✅ | 4.0.1 | The Pi may not charge your PSP |
| Retrode |  | 3.2.11 | Need Usbhid |
| SFC/USB | ✅ | 4.0.0 |  |
| Manette Sony PlayStation 3 | ✅ | 4.0.0 |  |
| Trenro Dual PlayStation / PlayStation 2 to PC USB Controller Adapter | ✅ | v3.3beta17 & v4.1.0-dev | With Generic USB adapter for 2 PS1/PS2 PAD controllers |

## Arcade Stick

| Device | Wired | Wireless | Latest version tested | Comments |
| :---: | :---: | :---: | :---: | :---: |
| **8bitdo** |  |  |  |
| FC30 Arcade Joystick (USB only) | ✅ |  |  |
| NES30 Arcade Joystick (USB only) | ✅ |  |  |
| **Datel** |  |  |  |
| Arcade Pro Joystick inc Rapid Fire for PS3 / Xbox 360 / PC | ✅ |  |  |
| **HORI** |  |  |  |
| Fighting Stick EX2 USB (Xbox 360 / PC) | ✅ |  |  |
| Fighting Stick MINI 3 pour PS3 | ✅ |  |  |
| Fighting Stick Mini 4 pour PS3 / PS4 | ✅ |  |  |
| Real Arcade Pro. 3 Fighting Stick | ✅ |  |  |
| Real Arcade Pro. 4 Kai (PS3 / PS4 / PC) | ✅ |  |  |
| HORI Real Arcade Pro. V (PS4) |  |  |  |
| Tekken 6 Wireless Arcade Stick PS3 | ✅ |  |  |
| **LIONCAST** |  |  |  |
| LIONCAST Arcade Fighting Stick (PS2 / PS3 / PC) | ✅ |  |  |
| **MadCatz** |  |  |  |
| Arcade Fightstick Street Fighter V Tes+ (PS3 / PS4) | ✅ | 4.0.0-beta1 |  |
| Arcade Stick Fightstick Alpha (PS3 / PS4) | ✅ | 4.0.0-beta3 |  |
| Arcade Fightsitck Street Fighter IV Standard Edition for Xbox 360 | ✅ |  |  |
| Street Fighter IV Fightstick Round 1 Tournament Edition | ✅ |  |  |
| Street Fighter IV Round 2 Arcade Fightstick Tournament Editin | ✅ |  |  |
| Arcade Fightstick Tournament Edition 2 (For PS3 / PS4) | ✅ | 4.0.0-beta6 |  |
| **Mayflash** |  |  |  |
| Mayflash XBOX 360 / PS3 / PC Arcade Fighting Stick V2 | ✅ |  |  |
| Mayflash PS2 PS3 PC USB Universal Arcade Fighting Stick | ✅ |  |  |
| **PXN** |  |  |  |
| PXN - 0082 Arcade Joystick | ✅ |  |  |
| **QanBa** |  |  |  |
| QamBa Q4 RAF Joystick Pro Fightstick Arcade 3in1 (Xbox 360 / PS3 / PC) | ✅ |  |  |
| **Sanwa** |  |  |  |
| JLF-TP-8YT (Sanwa) Arcade Joystick | ✅ |  |  |
| **Time Warner** |  |  |  |
| Mortal Kombat - Edition Ultimate (PS3) | ❌ |  |  |
| **Venom** |  |  |  |
| Venom - Arcade Stick (PS3 / PS4) | ✅ |  |  |

## Joystick

| Device | Wired | Wireless | Latest version tested | Comments |
| :---: | :---: | :---: | :---: |
| **Madcatz :** |  |  |  |
| Cyborg F.L.Y 5 | ✅ | v18.04.20 |  |

## LightGun

| Device | Wired | Wireless | Latest version tested | Comments |
| :---: | :---: | :---: | :---: | :---: |
| **Nintendo** |  |  |  |
| Wiimote | ✅ | 7.2.1 | With Mayflash USB Converter (Can be used as a Lightgun) |
| **Ultimarc** |  |  |  |
| AimTrack Lightgun | ✅ | 2018.x.x |  |

## Interface USB

| Device | Wired | Wireless | Latest version tested | Comments |
| :---: | :---: | :---: | :---: |
| **Akishop :** |  |  |  |
| PS360+ | ✅ |  |  |
|  |  |  |  |
| Zero Delay USB Encoder PC/PS3 controller |  | v 3.3beta 17 & v4.0.0-dev |  |
| Zero Delay USB Enconder Remix PC/PS3 controller | ✅ | v 3.3beta 17 & v4.0.0-dev |  |

## Arcade Push Buttons

| Device | Wired | Wireless | Latest version tested | Comments |
| :---: | :---: | :---: | :---: |
| **Halo :** |  |  |  |
| Halo Button | ✅ |  |  |
|  |  |  |  |
| **Pushbutton :** |  |  |  |
| Pushbutton SL-CV | ✅ |  |  |
