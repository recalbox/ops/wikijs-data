---
title: Utiliser vos sauvegardes ePSXe (.MCR) dans RetroArch (.SRM)
description: 
published: true
date: 2021-09-08T13:02:43.530Z
tags: ps1, srm, mcr, sauvegardes
editor: markdown
dateCreated: 2021-05-21T08:48:13.984Z
---

Vous pouvez utiliser vos sauvegardes sur ePsxe en utilisant la méthode suivante :

* Récupérez votre fichier de sauvegarde sur ePSXe (Epsxe/memcards/Votre_Sauvegarde.mcr).
* Identifiez le **nom exact** de votre rom.
* Renommez le fichier `Votre_Sauvegarde.mcr` en `NomDeVotreRom.srm`.
* Déplacez votre fichier fraîchement renommé dans le dossier `/recalbox/share/saves/psx` de votre Recalbox.