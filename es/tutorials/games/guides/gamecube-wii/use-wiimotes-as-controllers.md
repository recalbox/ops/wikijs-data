---
title: Utilizar los Wiimotes como mandos
description: 
published: true
date: 2023-09-20T09:36:59.948Z
tags: wiimotes, mandos
editor: markdown
dateCreated: 2023-09-20T09:36:59.948Z
---

>Dolphin puede gestionar simultáneamente hasta 4 Wiimotes.
>
>Para user los Wiimotes en Dolphin se necesita tener la barra Dolphin Bar de Mayflash. Hay que ponerla en modo 4.
{.is-info}

## Configuración

* Abrid el fichero [recalbox.conf](./../../../../basic-usage/getting-started/recalboxconf-file).
* Buscad el siguiente texto:

```ini
## Wiimotes
## Real wiimotes must not be paired with recalbox system so that they can work with the wii emulator
## set emulatedwiimotes to 1 to emulate wiimotes with standard pads
wii.emulatedwiimotes=0
```

* Modificar esta línea:

```ini
wii.emulatedwiimotes=0
```

* Como aquí:

```ini
wii.emulatedwiimotes=1
```

A partir de ahora, podréis jugar con vuestros Wiimotes.