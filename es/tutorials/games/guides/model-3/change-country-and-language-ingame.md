---
title: Cambio de país (idioma) dentro del juego
description: 
published: true
date: 2023-09-20T10:15:50.423Z
tags: model3, idioma, país
editor: markdown
dateCreated: 2023-09-20T10:15:50.423Z
---

>Para los siguientes juegos, el país está vinculado a la rom (no es posible cambiarlo en el "TEST MENU").
>
>* Dirt Devils
>* Llamada de Emergencia Ambulancia
>* Scud Race: 
> * Para la versión australiana (Australia), no hay menú para cambiar la región.
> Para otras versiones, el cambio se realiza directamente en el "MENÚ DE PRUEBA".
{.is-info}

![](/emulators/arcade/supermodel/changelanguage1.png){.full-width}

Para cada juego, cuando se lanza, entrad en el menú "TEST MENU" con la tecla `L3`.

A continuación, la manipulación a realizar depende de cada juego.

![](/emulators/arcade/supermodel/changelanguage2.png){.full-width}

![](/emulators/arcade/supermodel/changelanguage3.png){.full-width}

## Daytona USA 2

>Funciona por « Battle on the edge » y por « Power Edition ».
{.is-info}

Funciones de las diferentes teclas dentro del "TEST MENU".

| Funcionalidades | Atajo |
| :---: | :---: |
| Mover la flecha hacia arriba en el menú | **Y** |
| Mover la flecha hacia abajo en el menú | **X** |
| Aumentar la selección | **A** |
| Selección descendente | **B** |
| Validar | **L3** |

* Mainteniendo la tecla `START` apretada, pulsad la siguiente secuencia de teclas: `B`, `B`, `X`, `A`, `Y`, `A`, `X`
* Os encontraréis dentro de "COUNTRY ASSIGNEMENTS".
  * Desplazad la flecha sobre "COUNTRY".
  * Cambiad, con la ayuda de una de las teclas de selección, hasta que encontréis el país que os interesa (Japan, Korea, Australia, Export, USA).
* Validad.

## Fighting Vipers 2

Funciones de las diferentes teclas dentro del "TEST MENU".

| Funcionalidades | Atajo |
| :---: | :---: |
| Mover la flecha hacia arriba en el menú | **X** |
| Mover la flecha hacia abajo en el menú | **B** o **R3** |
| Validar / Cambiar el valor | **A** o **L3** |

* Entrad en el submenú de "GAME ASSIGNMENTS".
  * Desplazad la flecha sobre "Country".
  * Pulsad la siguiente secuencia de teclas con el D-pad (cruz direccional) : 3X **Gauche**, 2X **Droite**, 1X **Gauche**.
  * Cambiad el país con ayuda de **L3** (Japan, USA, Export, Asia).
* Salid del menú (Bajad la flecha hasta "EXIT").

## Harley Davidson & L.A. Riders

Funciones de las diferentes teclas dentro del "TEST MENU".

| Funcionalidades | Atajo |
| :---: | :---: |
| Mover la flecha hacia arriba en el menú | **X** ou **R1** |
| Mover la flecha hacia abajo en el menú | **B** ou **L1** ou **R3** |
| Validar / Cambiar el valor | **A** ou **L3** |

* Entrad en el submenú de "GAME ASSIGNMENTS".
  * Pulsad la siguiente secuencia de teclas: 2x `R1`, 2x `L1`, `X`, `B`, `X`, `B`.
  * La línea "Country" aparece en la parte baja de la pantalla.
  * Bajad la flecha de un nivel. 
  * Cambiad el país con la ayuda de `L3` (Japan, USA, Export, Korea, Australia).
* Salid del menú (Subid la flecha hasta "EXIT").

## L.A. Machine Guns

Funciones de las diferentes teclas dentro del "TEST MENU".

| Funcionalidades | Atajo |
| :---: | :---: |
| Mover la flecha hacia abajo en el menú | **R3** |
| Validar / Cambiar el valor | **L3** |

* Pulsad la siguiente secuencia de teclas: 2x `START`, 1x `R3`, 3x `START`,1x `R3`, 1x `L3`.

>Las funciones de las teclas dentro de "TEST MENU" no son las mismas que dentro de "COUNTRY SELECT".
{.is-warning}

Funciones de las diferentes teclas dentro del "COUNTRY SELECT MODE".

| Funcionalidades | Atajo |
| :---: | :---: |
| Cambiar el valor | **R3** |
| Validar | **L3** |

* Os encontráis dentro de "COUNTRY SELECT MODE".
  * Cambiar el valor hasta encontrar el país que queréis (Japan, USA, Export, Australia).
* Validar.

## Le Mans 24H

Funciones de las diferentes teclas dentro del "TEST MENU".

| Funcionalidades | Atajo |
| :---: | :---: |
| Mover la flecha hacia abajo en el menú | **R3** |
| Validar / Cambiar el valor | **L3** |

* Pulsad la siguiente secuencia de teclas: 2x `START`, 2x `R3`, 1x `START`, 1x `L3` .

>Las funciones de las teclas dentro de "TEST MENU" no son las mismas que dentro de "COUNTRY SELECT".
{.is-warning}

Funciones de las diferentes teclas dentro del "COUNTRY SELECT MODE".

| Funcionalidades | Atajo |
| :---: | :---: |
| Changer la valeur | **R3** |
| Valider | **L3** |

* Os encontráis dentro de "COUNTRY SELECT MODE".
  * Cambiar el valor hasta encontrar el país que queréis (Japan, USA, Export, Australia).
* Validad.

## Magical Truck Adventure

>Este juego sólo soporta como región Japón.
{.is-info}

Funciones de las diferentes teclas dentro del "TEST MENU".

| Funcionalidades | Atajo |
| :---: | :---: |
| Mover la flecha hacia abajo en el menú | **R3** |
| Validar / Cambiar el valor | **L3** |

* Pulsad la siguiente secuencia de teclas: `START`, `START`, `R3`, `START`, `R3`, `L3`.
  * Cambiar el valor hasta encontrar el país que queréis (sólo Japan).
  * Validad.

## Scud Race

Funciones de las diferentes teclas dentro del "TEST MENU".

| Funcionalidades | Atajo |
| :---: | :---: |
| Mover la flecha hacia abajo en el menú | **R3** |
| Validar / Cambiar el valor | **L3** |

Si os encontráis con un error « network board not present », realizad lo siguiente:

* Entrad en el menú test.
* Bajad hasta « Game assignements »
* Id sobre la línea « Link id » y cambiad su valor de « master » a « single ».

## Sega Bass Fishing

Funciones de las diferentes teclas dentro del "TEST MENU".

| Funcionalidades | Atajo |
| :---: | :---: |
| Mover la flecha hacia abajo en el menú | **R3** |
| Validar / Cambiar el valor | **L3** |

* Entrad en el submenú "C.R.T. TEST".
  * Cambiar a la página 2/2 (la page se muestra en lo alto de la pantalla) con ayuda de la tecla `L3`.
  * Pulsad `R3` 4x.
  * Salid con la tecla `L3`.
* Os encontráis de nuevo dentro del "TEST MENU". Seleccionad el submenú "GAME ASSIGNEMENTS".
  * Pulsad `R3` 3x , y a continuación `L3` + `R3`.

>Las funciones de las teclas dentro de "TEST MENU" no son las mismas que dentro de "COUNTRY SELECT".
{.is-warning}

Funciones de las diferentes teclas dentro del "COUNTRY SELECT".

| Funcionalidades | Atajo |
| :---: | :---: |
| Cambiar el valor | **R3** |
| Validar | **L3** |

* Os encontráis dentro de "COUNTRY SELECT MODE".
  * Cambiar el valor hasta encontrar el país que queréis (Japan, USA, Export, Australia).
* Validad.

## Sega Rally 2

Funciones de las diferentes teclas dentro del "TEST MENU".

| Funcionalidades | Atajo |
| :---: | :---: |
| Mover la flecha hacia abajo en el menú | **R3** |
| Validar / Cambiar el valor | **L3** |

* Ejecutad la siguiente sucesión de acciones con la tecla `R3` (pulsaciones largas o cortas).
  * pulsación corta 4x, larga 2x, corta 2x, larga 1x.

>Las funciones de las teclas dentro de "TEST MENU" no son las mismas que dentro de "COUNTRY SELECT".
{.is-warning}

Funciones de las diferentes teclas dentro del "COUNTRY SELECT".

| Funcionalidades | Atajo |
| :---: | :---: |
| Cambiar el valor | **R3** |
| Validar | **L3** |

* Os encontraréis dentro del menú "COUNTRY SELECT MODE".
  * Cambiar el valor hasta encontrar el país que queréis (Japan, USA, Australia, Export).
* Validad.

## Ski Champ

Funciones de las diferentes teclas dentro del "TEST MENU".

| Funcionalidades | Atajo |
| :---: | :---: |
| Mover la flecha hacia abajo en el menú | **R3** |
| Validar / Cambiar el valor | **L3** |

* Pulsad la siguiente secuencia de teclas: `Y`, `A`, `Y`, `A`, `R3`, `R3`.

>Las funciones de las teclas dentro de "TEST MENU" no son las mismas que dentro de "COUNTRY SELECT".
{.is-warning}

Funciones de las diferentes teclas dentro del "COUNTRY SELECT".

| Funcionalidades | Atajo |
| :---: | :---: |
| Cambiar el valor | **R3** |
| Validar | **L3** |

* Os encontraréis dentro del menú "COUNTRY SELECT MODE".
  * Cambiar el valor hasta encontrar el país que queréis (Japan, USA, Export, Korea, Australia).
* Validad.