---
title: Libretro MAME 2015
description: 
published: true
date: 2024-06-25T16:24:59.290Z
tags: libretro, mame, mame2015
editor: markdown
dateCreated: 2021-05-21T08:13:52.727Z
---



## ![](/emulators/license.svg) Licence

Ce core est sous licence **MAME non-commerciale**.

## ![](/emulators/compatibility.png) Compatibilité

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ❌ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Fonctionnalités

| Fonctionnalité | Supporté |
| :---: | :---: |
| Saves |  |
| States |  |
| Rewind |  |
| Netplay |  |
| RetroAchievements |  |
| RetroArch Cheats |  |
| Native Cheats |  |
| Controllers |  |
| Multi-Mouse |  |
| Rumble |  |
| Sensors |  |
| Camera |  |
| Location |  |
| Subsystem |  |

## ![](/emulators/bios.svg) BIOS

Les romsets BIOS ne sont pas nécessaires lorsque vous utilisez des romsets d'arcade "Full Non-Merged". Pour les roms "Split" et "Non-Merged", placez le BIOS dans le même répertoire que le romset du jeu.

>**Remarque** :  
>Placez les bios dans le répertoire : `/recalbox/share/roms/mame/`   
>ou dans son sous-répertoire si vous utilisez plusieurs cores de Mame : `/recalbox/share/roms/mame/Mame2015/`
Les jeux et leurs bios doivent être dans le même répertoire ou sous-répertoire.
{.is-warning}

## ![](/emulators/roms.png) Roms

* Basé sur le romset : MAME 0.160 (Mars 2015)
* Fichier dat : vous trouverez le fichier dat dans votre recalbox dans le répertoire `/recalbox/share/arcade/libretro/mame2015.dat`.

### Emplacement

Placez les roms comme ceci : 

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 mame
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.zip**
┃ ┃ ┃ ┃ ┣ 📁 **jeu**
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **jeu.chd**

Vous pouvez aussi opter pour le sous-répertoire déjà présent `mame2015` (utile si vous voulez avoir un set mame pour un autre core). Ceci permettra de lancer le bon émulateur automatiquement.

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 mame
┃ ┃ ┃ ┃ ┣ 📁 mame2015
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **jeu.zip**
┃ ┃ ┃ ┃ ┃ ┣ 📁 **jeu**
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 **jeu.chd**

## ![](/emulators/advanced-configuration.png) Configuration avancée de l'émulateur

>Pour pouvoir conserver vos configurations personnalisées lors d'une mise à jour, nous vous conseillons d'utiliser notre fonctionnalité [Surcharges de configuration](./../../../advanced-usage/configuration-override).
{.is-info}

### Accéder aux options

Vous pouvez configurer diverses options de deux façons différentes.

* Via le menu RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via le fichier `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Options du core

| Options | Valeur des options | Variable (pour surcharge) | Valeurs des variables |
| --- | --- | --- | --- |
| Read configuration | `Désactivé` ✅ / `Activé` | `mame2015_read_config` | `disabled` / `enabled` |
| Auto save/load states | `Désactivé` ✅ / `Activé` | `mame2015_auto_save` | `disabled` / `enabled` |
| XY device (Restart) | `none` ✅ / `lightgun` / `mouse` | `mame2015_mouse_mode` | `none` / `lightgun` / `mouse` |
| Enable throttle | `Désactivé` ✅ / `Activé` | `mame2015_throttle` | `disabled` / `enabled` |
| Enable cheats | `Désactivé` ✅ / `Activé` | `mame2015_cheats_enable` | `disabled` / `enabled` |
| Hide nag screen | `Désactivé` ✅ / `Activé` | `mame2015_hide_nagscreen` | `disabled` / `enabled` |
| Hide gameinfo screen | `Désactivé` ✅ / `Activé` | `mame2015_hide_infoscreen` | `disabled` / `enabled` |
| Hide warnings screen | `Désactivé` ✅ / `Activé` | `mame2015_hide_warnings` | `disabled` / `enabled` |
| Alternate render method | `Désactivé` ✅ / `Activé` | `mame2015_alternate_renderer` | `disabled` / `enabled` |
| Boot to OSD | `Désactivé` ✅ / `Activé` | `mame2015_boot_to_osd` | `disabled` / `enabled` |
| Boot from CLI | `Désactivé` ✅ / `Activé` | `mame2015_boot_from_cli` | `disabled` / `enabled` |

## ![](/emulators/external-links.png) Liens externes

* **Progettosnaps** : [https://www.progettosnaps.net/](https://www.progettosnaps.net/)
* **Code source utilisé** : [https://github.com/libretro/mame2015-libretro/](https://github.com/libretro/mame2015-libretro)