---
title: Libretro NP2Kai
description: 
published: true
date: 2024-07-24T22:13:45.434Z
tags: libretro, pc-98, np2kai
editor: markdown
dateCreated: 2021-07-29T17:13:51.576Z
---



## ![](/emulators/license.svg) License

This core is under [**MIT**](https://github.com/AZO234/NP2kai/blob/master/LICENSE) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Features

| Feature | Supported |
| :---: | :---: |
| Restart  | ✔ |
| Screenshots | ✔ |
| States | ✔ |
| Rewind | ✔ |
| Netplay | ✔ |
| Core Options | ✔ |
| RetroAchievements | ✔ |
| Controls | ✔ |
| Disk Control | ✔ |

## ![](/emulators/bios.svg) BIOS

### List of mandatory bios

| Filename | Description | MD5 | Provided |
| :---: | :---: | :---: | :---: |
| bios.rom | - | 052f5f6010877522f78803498a625727 0c052b1f1bfd0ece11286d2ff45d296f 36293e23fe0c86cb115d59b653c13876 3af0ae018c5710eec6e2891064814138 50274bb5dcb707e4450011b09accffcb c70ee9df11794bd5cc8aadb3721b4a03 cd237e16e7e77c06bb58540e9e9fca68 e246140dec5124c5e404869a84caefce | ❌ |
| font.bmp | Text display | 0c8624e9ced7cca8769e5c7b0dd4279b 7da1e5b7c482d4108d22a5b09631d967 | ❌ |
| font.rom | Alternative font | 38d32748ae49d1815b0614970849fd40 4133b0be0d470920da60b9ed28d2614f 693fd1da3239d4bbeafc77d211718fc5 829b963e21334e0fc2092ebd58f2ab4a add4a225048c85ca2bc588696c6ecdc5 ca87908a99ea423093f6d497fc367f7d fd0b856899aec843cbe69d2940df547f | ❌ |
| itf.rom | - | 1d295699ffeab0f0e24e09381299259d 72ea51443070f0e9212bfc9b793ee28e a13d96da03a28af8418d7f86ab951f1a b49ea39a1f730f1c966babc11961dc9a e9fc3890963b12cf15d0a2eea5815b72 | ❌ |
| sound.rom | - | 42c271f8b720e796a484cc1165ff4914 524473c1a5a03b17e21d86a0408ff827 a77fc2bc7c696dd68dba18e02f89d386 caf90f22197aed6f14c471c21e64658d | ❌ |

### Location

Put the bios like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 np2kai
┃ ┃ ┃ ┃ ┣ 🗒 **bios.rom**
┃ ┃ ┃ ┃ ┣ 🗒 **font.bmp**
┃ ┃ ┃ ┃ ┣ 🗒 **font.rom**
┃ ┃ ┃ ┃ ┣ 🗒 **itf.rom**
┃ ┃ ┃ ┃ ┣ 🗒 **sound.rom**

### List of optional bios

| Filename | Description | MD5 | Provided |
| :---: | :---: | :---: | :---: |
| 2608_bd.wav | YM2608 RYTHM sample | 9c6637930b1779abe00b8b63e4e41f50 d94546e70f17fd899be8df3544ab6cbb | ❌ |
| 2608_hh.wav | YM2608 RYTHM sample | 73548a1391631ff54a1f7c838d67917e 08c54a0c1f774a5538a848a6665a34b4 | ❌ |
| 2608_rim.wav | YM2608 RYTHM sample | 43d54b3e05c081fa280c9bace3af1043 465ea0768b27da404aec45dfc501404b | ❌ |
| 2608_sd.wav | YM2608 RYTHM sample | 08124ccb84a9f65e2affc29581e690c9 d71004351c8bbfdad53b18222c061d49 | ❌ |
| 2608_tom.wav | YM2608 RYTHM sample | faed5664a2dd8b1b2308e8a50ac25ea 96a4ead13f364734f79b0c58af2f0e1f | ❌ |
| 2608_top.wav | YM2608 RYTHM sample | 3721ace646ffd56439aebbb2154e9263 593cff6597ab9380d822b8f824fd2c28 | ❌ |

### Location

Put the bios like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 np2kai
┃ ┃ ┃ ┃ ┣ 🗒 **2608_hd.wav**
┃ ┃ ┃ ┃ ┣ 🗒 **2608_hh.wav**
┃ ┃ ┃ ┃ ┣ 🗒 **2608_rim.wav**
┃ ┃ ┃ ┃ ┣ 🗒 **2608_sd.wav**
┃ ┃ ┃ ┃ ┣ 🗒 **2608_tom.wav**
┃ ┃ ┃ ┃ ┣ 🗒 **2608_top.wav**

## ![](/emulators/roms.png) Roms

### Supported extensions

Roms must have the extension:

* .d98
* .98d
* .dcp
* .fdi
* .fdd
* .nfd
* .2hd
* .tfd
* .d88
* .88d
* .hdm
* .xdf
* .dup
* .cmd
* .hdi
* .thd
* .nhd
* .hdd
* .hdn
* .m3u
* .zip
* .7z

This system supports compressed roms in .zip/.7z format. However, be careful, it is only an archive.

The files contained in the .zip/.7z must correspond to the extensions mentioned above.
Moreover, each .zip/.7z file must contain only one compressed rom.

### Location

Put the roms like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 pc98
┃ ┃ ┃ ┃ ┣ 🗒 **game.zip**

>The roms using **TOSEC** format are highly recommended.
{.is-success}

>For more information about roms, go to [this tutorial](./../../../tutorials/games/generalities/isos-and-roms)!
{.is-info}

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Accessing to options

You can configure various options in two different ways.

* Via RetroArch menu:

┣ 📁 RetroArch Menu
┃ ┣ 📁 Core options
┃ ┃ ┣ 🧩 Name_option

* Via the file `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core options

| Options | Options values | Variables (for configuration override) | Variables values |
| --- | --- | --- | --- |
| Swap Disks on Drive | `FDD1` / `FDD2` ✅ | `np2kai_drive` | `FDD1` / `FDD2` |
| Keyboard (Restart) | `Ja` ✅ / `Us` | `np2kai_keyboard` | `Ja` / `Us` |
| Key-repeat | `Disabled` ✅ / `Enabled` | `np2kai_keyrepeat` | `OFF` / `ON` |
| Key-repeat delay | `250ms` / `500ms` ✅ / `1000ms` | `np2kai_keyrepeat_delay` | `250ms` / `500ms` / `1000ms` |
| Key-repeat interval | `25ms` / `50ms` ✅ / `100ms` | `np2kai_keyrepeat-interval` | `25ms` / `50ms` / `100ms` |
| PC Model (Restart) | `PC-286` / `PC-9801VM` / `PC-9801VX` ✅ | `np2kai_model` | `PC-286` / `PC-9801VM` / `PC-9801VX` |
| CPU Base Clock (Restart) | `1.9968 MHz` / `2.4576 MHz` ✅ | `np2kai_clk_base` | `1.9968 MHz` / `2.4576 MHz` |
| CPU Feature (Restart) | `Intel 80386` ✅ / `Intel i486SX` / `Intel i486DX` / `Intel Pentium` / `Intel MMX Pentium` / `Intel Pentium Pro` / `Intel Pentium II` / `Intel Pentium III` / `Intel Pentium M` / `Intel Pentium 4` / `AMD K6-2` / `AMD K6-III` / `AMD K7 Athlon` / `AMD K7 Athlon XP` / `Neko Processor II` | `np2kai_cpu_feature` | `Intel 80386` / `Intel i486SX` / `Intel i486DX` / `Intel Pentium` / `Intel MMX Pentium` / `Intel Pentium Pro` / `Intel Pentium II` / `Intel Pentium III` / `Intel Pentium M` / `Intel Pentium 4` / `AMD K6-2` / `AMD K6-III` / `AMD K7 Athlon` / `AMD K7 Athlon XP` / `Neko Processor II` |
| CPU Clock Multiplier (Restart) | `2` / `4` ✅ / `5` / `6` / `8` / `10` / `12` / `16` / `20` / `24` / `30` / `36` / `40` / `42` / `52` / `64` / `76` / `88` / `100` | `np2kai_clk_mult` | `2` / `4` / `5` / `6` / `8` / `10` / `12` / `16` / `20` / `24` / `30` / `36` / `40` / `42` / `52` / `64` / `76` / `88` / `100` |
| Async CPU(experimental) (Restart) | `Disabled` ✅ / `Enabled` | `np2kai_async_cpu` | `OFF` / `ON` |
| RAM Size (Restart) | `1` / `3` ✅ / `7` / `11` / `13` / `16` / `32` / `64` / `120` / `230` / `512` / `1024` | `np2kai_ExMemory` | `1` / `3` / `7` / `11` / `13` / `16` / `32` / `64` / `120` / `230` / `512` / `1024` |
| Fast memcheck | `Disabled` ✅ / `Enabled` | `np2kai_FastMC` | `OFF` / `ON` |
| Use last HDD mount | `Disabled` ✅ / `Enabled` | `np2kai_uselasthddmount` | `OFF` / `ON` |
| Video filter | `Disabled` ✅ / `Profile 0` / `Profile 1` / `Profile 2` | `np2kai_vf1` | `OFF` / `Profile 0` / `Profile 1` / `Profile 2` |
| GDC | `uPD7220` ✅ / `uPD72020` | `np2kai_gdc` | `uPD7220` / `uPD72020` |
| Skipline Revisions | `Full 255 lines` ✅ / `Disabled` / `Enabled` | `np2kai_skipline` | `Full 255 lines` / `OFF` / `ON` |
| Real Palettes | `Disabled` ✅ / `Enabled` | `np2kai_realpal` | `OFF` / `ON` |
| LCD (old monochrome) | `Disabled` ✅ / `Enabled` | `np2kai_lcd` | `OFF` / `ON` |
| Sound Board (Restart) | `PC9801-14` ✅ / `PC9801-86` / `PC9801-86 + 118(B460)` / `PC9801-86 + Mate-X PCM(B460)` / `PC9801-86 + Chibi-oto` / `PC9801-86 + Speak Board` / `PC9801-26K` / `PC9801-26K + 86` / `PC9801-118` / `Mate-X PCM` / `Chibi-oto` / `Speak Board` / `Spark Board` / `Sound Orchestra` / `Sound Orchestra-V` / `Little Orchestra L` / `Multimedia Orchestra` / `Sound Blaster 16` / `PC9801-86 + Sound Blaster 16` / `Mate-X PCM + Sound Blaster 16` / `PC9801-118 + Sound Blaster 16` / `PC9801-86 + Mate-X PCM(B460) + Sound Blaster 16` / `PC9801-86 + 118(B460) + Sound Blaster 16` / `AMD-98` / `WaveStar` / `Otomi-chanx2` / `Otomi-chanx2 + 86` / `None` | `np2kai_SNDboard` | `PC9801-14` / `PC9801-86` / `PC9801-86 + 118(B460)` / `PC9801-86 + Mate-X PCM(B460)` / `PC9801-86 + Chibi-oto` / `PC9801-86 + Speak Board` / `PC9801-26K` / `PC9801-26K + 86` / `PC9801-118` / `Mate-X PCM` / `Chibi-oto` / `Speak Board` / `Spark Board` / `Sound Orchestra` / `Sound Orchestra-V` / `Little Orchestra L` / `Multimedia Orchestra` / `Sound Blaster 16` / `PC9801-86 + Sound Blaster 16` / `Mate-X PCM + Sound Blaster 16` / `PC9801-118 + Sound Blaster 16` / `PC9801-86 + Mate-X PCM(B460) + Sound Blaster 16` / `PC9801-86 + 118(B460) + Sound Blaster 16` / `AMD-98` / `WaveStar` / `Otomi-chanx2` / `Otomi-chanx2 + 86` / `None` |
| enable 118 ROM | `Disabled` / `Enabled` ✅ | `np2kai_118ROM` | `OFF` / `ON` |
| JastSound | `Disabled` ✅ / `Enabled` | `np2kai_jast_snd` | `OFF` / `ON` |
| Swap PageUp/PageDown | `Disabled` / `Enabled` ✅ | `np2kai_xroll` | `OFF` / `ON` |
| Sound Generator | `Default` / `fmgen` ✅ | `np2kai_usefmgen` | `Default` / `fmgen` |
| Volume Master | `0` / `5` / `10` / `15` / `20` / `25` / `30` / `35` / `40` / `45` / `50` / `55` / `60` / `65` / `70` / `75` / `80` / `85` / `90` / `95` / `100` ✅ | `np2kai_volume_M` | `0` / `5` / `10` / `15` / `20` / `25` / `30` / `35` / `40` / `45` / `50` / `55` / `60` / `65` / `70` / `75` / `80` / `85` / `90` / `95` / `100` |
| Volume FM | `0` / `4` / `8` / `12` / `16` / `20` / `24` / `28` / `32` / `36` / `40` / `44` / `48` / `52` / `56` / `60` / `64` ✅ / `68` / `72` / `76` / `80` / `84` / `88` / `92` / `96` / `100` / `104` / `108` / `112` / `116` / `120` / `124` / `128` | `np2kai_volume_F` | `0` / `4` / `8` / `12` / `16` / `20` / `24` / `28` / `32` / `36` / `40` / `44` / `48` / `52` / `56` / `60` / `64` / `68` / `72` / `76` / `80` / `84` / `88` / `92` / `96` / `100` / `104` / `108` / `112` / `116` / `120` / `124` / `128` |
| Volume SSG | `0` / `4` / `8` / `12` / `16` / `20` / `24` / `28` ✅ / `32` / `36` / `40` / `44` / `48` / `52` / `56` / `60` / `64` / `68` / `72` / `76` / `80` / `84` / `88` / `92` / `96` / `100` / `104` / `108` / `112` / `116` / `120` / `124` / `128` | `np2kai_volume_S` | `0` / `4` / `8` / `12` / `16` / `20` / `24` / `28` / `32` / `36` / `40` / `44` / `48` / `52` / `56` / `60` / `64` / `68` / `72` / `76` / `80` / `84` / `88` / `92` / `96` / `100` / `104` / `108` / `112` / `116` / `120` / `124` / `128` |
| Volume ADPCM | `0` / `4` / `8` / `12` / `16` / `20` / `24` / `28` / `32` / `36` / `40` / `44` / `48` / `52` / `56` / `60` / `64` ✅ / `68` / `72` / `76` / `80` / `84` / `88` / `92` / `96` / `100` / `104` / `108` / `112` / `116` / `120` / `124` / `128` | `np2kai_volume A` | `0` / `4` / `8` / `12` / `16` / `20` / `24` / `28` / `32` / `36` / `40` / `44` / `48` / `52` / `56` / `60` / `64` / `68` / `72` / `76` / `80` / `84` / `88` / `92` / `96` / `100` / `104` / `108` / `112` / `116` / `120` / `124` / `128` |
| Volume PCM | `0` / `4` / `8` / `12` / `16` / `20` / `24` / `28` / `32` / `36` / `40` / `44` / `48` / `52` / `56` / `60` / `64` / `68` / `72` / `76` / `80` / `84` / `88` / `92` ✅ / `96` / `100` / `104` / `108` / `112` / `116` / `120` / `124` / `128` | `np2kai_volume_P` | `0` / `4` / `8` / `12` / `16` / `20` / `24` / `28` / `32` / `36` / `40` / `44` / `48` / `52` / `56` / `60` / `64` / `68` / `72` / `76` / `80` / `84` / `88` / `92` / `96` / `100` / `104` / `108` / `112` / `116` / `120` / `124` / `128` |
| Volume RHYTHM | `0` / `4` / `8` / `12` / `16` / `20` / `24` / `28` / `32` / `36` / `40` / `44` / `48` / `52` / `56` / `60` / `64` ✅ / `68` / `72` / `76` / `80` / `84` / `88` / `92` / `96` / `100` / `104` / `108` / `112` / `116` / `120` / `124` / `128` | `np2kai_volume_R` | `0` / `4` / `8` / `12` / `16` / `20` / `24` / `28` / `32` / `36` / `40` / `44` / `48` / `52` / `56` / `60` / `64` / `68` / `72` / `76` / `80` / `84` / `88` / `92` / `96` / `100` / `104` / `108` / `112` / `116` / `120` / `124` / `128` |
| Volume CD-DA | `0` / `8` / `16` / `24` / `32` / `40` / `48` / `56` / `64` / `72` / `80` / `88` / `96` / `104` / `112` / `120` / `128` ✅ / `136` / `144` / `154` / `160` / `168` / `196` / `184` / `192` / `200` / `208` / `216` / `224` / `232` / `240` / `248` / `255` | `np2kai_volume_C` | `0` / `8` / `16` / `24` / `32` / `40` / `48` / `56` / `64` / `72` / `80` / `88` / `96` / `104` / `112` / `120` / `128` / `136` / `144` / `154` / `160` / `168` / `196` / `184` / `192` / `200` / `208` / `216` / `224` / `232` / `240` / `248` / `255` |
| Floppy Seek Sound | `Disabled` ✅ / `Enabled` | `np2kai_Seek_Snd` | `OFF` / `ON` |
| Volume Floppy Seek | `0` ✅ / `4` / `8` / `12` / `16` / `20` / `24` / `28` / `32` / `36` / `40` / `44` / `48` / `52` / `56` / `60` / `64` / `68` / `72` / `76` / `80` / `84` / `88` / `92` / `96` / `100` / `104` / `108` / `112` / `116` / `120` / `124` / `128` | `np2kai_Seek_Vol` | `0` / `4` / `8` / `12` / `16` / `20` / `24` / `28` / `32` / `36` / `40` / `44` / `48` / `52` / `56` / `60` / `64` / `68` / `72` / `76` / `80` / `84` / `88` / `92` / `96` / `100` / `104` / `108` / `112` / `116` / `120` / `124` / `128` |
| Volume Beep | `0` / `1` / `2` / `3` ✅ | `np2kai_BEEP_vol` | `0` / `1` / `2` / `3` |
| Enable WAB (Restart App) | `Disabled` ✅ / `Enabled` | `np2kai_CLGD_en` | `OFF` / `ON` |
| WAB Type | `PC-9821Xe10,Xa7e,Xb10 built-in` ✅ / `PC-9821Bp,Bs,Be,Bf built-in` / `PC-9821Xe built-in` / `PC-9821Cb built-in` / `PC-9821Cf built-in` / `PC-9821Cb2 built-in` / `PC-9821Cx2 built-in` / `PC-9821 PCI CL-GD5446 built-in` / `MELCO WAB-S` / `MELCO WSN-A2F` / `MELCO WSN-A4F` / `I-O DATA GA-98NBI/C` / `I-O DATA GA-98NBII` / `I-O DATA GA-98NBIV` / `PC-9801-96(PC-9801B3-E02)` / `Auto Select(Xe10, GA-98NBI/C), PCI` / `Auto Select(Xe10, GA-98NBII), PCI` / `Auto Select(Xe10, GA-98NBIV), PCI` / `Auto Select(Xe10, WAB-S), PCI` / `Auto Select(Xe10, WSN-A2F), PCI` / `Auto Select(Xe10, WSN-A4F), PCI` / `Auto Select(Xe10, WAB-S)` / `Auto Select(Xe10, WSN-A2F)` / `Auto Select(Xe10, WSN-A4F)` | `np2kai_CLGD_type` | `PC-9821Xe10,Xa7e,Xb10 built-in` / `PC-9821Bp,Bs,Be,Bf built-in` / `PC-9821Xe built-in` / `PC-9821Cb built-in` / `PC-9821Cf built-in` / `PC-9821Cb2 built-in` / `PC-9821Cx2 built-in` / `PC-9821 PCI CL-GD5446 built-in` / `MELCO WAB-S` / `MELCO WSN-A2F` / `MELCO WSN-A4F` / `I-O DATA GA-98NBI/C` / `I-O DATA GA-98NBII` / `I-O DATA GA-98NBIV` / `PC-9801-96(PC-9801B3-E02)` / `Auto Select(Xe10, GA-98NBI/C), PCI` / `Auto Select(Xe10, GA-98NBII), PCI` / `Auto Select(Xe10, GA-98NBIV), PCI` / `Auto Select(Xe10, WAB-S), PCI` / `Auto Select(Xe10, WSN-A2F), PCI` / `Auto Select(Xe10, WSN-A4F), PCI` / `Auto Select(Xe10, WAB-S)` / `Auto Select(Xe10, WSN-A2F)` / `Auto Select(Xe10, WSN-A4F)` |
| Use Fake Hardware Cursor | `Disabled` ✅ / `Enabled` | `np2kai_CLGD_fc` | `OFF` / `ON` |
| Enable PEGC plane mode | `Disabled` / `Enabled` ✅ | `np2kai_PEGC` | `OFF` / `ON` |
| Enable PCI (Restart App) | `Disabled` ✅ / `Enabled` | `np2kai_PCI_en` | `OFF` / `ON` |
| PCMC Type | `Intel 82434LX` ✅ / `Intel 82441FX` / `VLSI Wildcat` | `np2kai_PCI_type` | `Intel 82434LX` / `Intel 82441FX` / `VLSI Wildcat` |
| Use BIOS32 (not recommended) | `Disabled` ✅ / `Enabled` | `np2kai_PCI_bios32` | `OFF` / `ON` |
| Use CD-ROM EDC/ECC Emulation | `Disabled` / `Enabled` ✅ | `np2kai_usecdecc` | `OFF` / `ON` |
| Use mouse or touchpanel to input mouse | `Disabled` / `Enabled` ✅ | `np2kai_inputmouse` | `OFF` / `ON` |
| S2M(Joypad Analog Stick to Mouse) Mapping | `Disabled` / `R-stick` ✅ / `L-stick` | `np2kai_stick2mouse` | `OFF` / `R-stick` / `L-stick` |
| S2M Click Shift Button Mapping | `Disabled` / `L1` / `R1` ✅ / `L2` / `R2` | `np2kai_stick2mouse_shift` | `OFF` / `L1` / `R1` / `L2` / `R2` |
| Joypad D-pad to Mouse/Keyboard/Joypad Mapping | `Disabled` ✅ / `Mouse` / `Arrows` / `Arrows 3button` / `Keypad` / `Keypad 3button` / `Manual Keyboard` / `Atari Joypad` | `np2kai_joymode` | `OFF` / `Mouse` / `Arrows` / `Arrows 3button` / `Keypad` / `Keypad 3button` / `Manual Keyboard` / `Atari Joypad` |
| Joypad to NP2 menu Mapping | `Disabled` ✅ / `L1` / `L2` / `L3` / `R1` / `R2` / `R3` / `A` / `B` / `X` / `Y` / `Start` / `Select` | `np2kai_joynp2menu` | `OFF` / `L1` / `L2` / `L3` / `R1` / `R2` / `R3` / `A` / `B` / `X` / `Y` / `Start` / `Select` |

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/AZO234/NP2kai/](https://github.com/AZO234/NP2kai/)
* **Libretro documentation**: [https://docs.libretro.com/library/neko_project_ii_kai/](https://docs.libretro.com/library/neko_project_ii_kai/)