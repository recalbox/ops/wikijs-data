---
title: 7. 📋 ABOUT RECALBOX
description: How to contribute, licenses, useful links...
published: true
date: 2023-07-18T11:50:24.049Z
tags: more informations
editor: markdown
dateCreated: 2021-05-28T22:02:46.144Z
---

You will find here the general presentation of Recalbox, who we are, the licenses and the possibility to contribute to the project in different ways 