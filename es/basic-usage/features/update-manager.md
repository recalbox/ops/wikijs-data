---
title: Gestor de actualizaciones
description: 
published: true
date: 2024-06-02T20:50:03.370Z
tags: 7.0+, actualizaciones, gestor
editor: markdown
dateCreated: 2023-08-21T22:39:48.012Z
---

![](/basic-usage/features/update.png)

## Una nueva forma de actualizar Recalbox!

El sistema de actualizaciones de Recalbox ha sido reescrito completamente y no funciona en versiones anteriores a la 7.

* El nuevo sistema de actualizaciones es mucho más rápido:	
  * _**De 5 à 10 minutos máximo para realizar una actualización_ (sin contar el tiempo de descarga de la imagen claro).
* El nuevo sistema es mucho más fiable:
  * Se acabaron las actualizaciones que fallan y que os obligan a recomenzar (puede pasar, pero ocurre mucho menos gracias à Dios)
* El nuevo sistema se gestiona desde EmulationStation: se muestra una barra de progreso, existe una gestión de errores, etc.
* Se pueden realizar actualizaciones sin conexión de una forma muy sencilla: basta con copiar la image de la nueva versión de Recalbox dentro de una carpeta de la tarjeta SD a la que se accede desde cualquier sitema operativo: Windows, Linux y macOS ! Un pequeño reinicio, el sistema detecta la existencia de la imagen en su carpeta, se lanza la actualización y eso es todo!
* Podéis incluir vuestros propios vídeos de arranque, dentro de una carpeta accesible en red (`/share/bootvideos/`) y también existe una opción para decidir si el reproductor debe lanzar unicamente los vídeos de recalbox, únicamente los vuestros, o ambos.	
  * ¿Cuál es la relación de esto con el nuevo sistema de actualizaciones, os estaréis preguntado? Nada, sólo que esta nueva posibilidad de gestion de vídeos de arranque está relacionada con la nueva estructura de carpetas que ha sido creada para facilitar las actualiaciones!
  
* Las _**notificaiones de actualizaciones no pueden desactivarse más**_ desde EmulationStation, cosa que va a molestar a todos aquellos que venden sin permiso Recalbox. Se puede desactivar la popup modal, pero siempre aparecerá la popup no intrusiva arriba a la izquierda para advertiros de que une nueva versión de Recalbox está disponible.

## Funcionamiento

Las actualizaciones se puede realizar de dos formas diferentes: en línea (online) o sin internet (offline).

## {.tabset}

### Método en línea (online)

El método online es el más sencillo:

* Dentro de EmulationStation, pulsad el botón `START`de vuestro mando.
* Id dentro de `ACTUALIZACIONES`.
* Si existe una nueva actualización para vuestro sistema, la línea `ACTUALIZACIÖN DISPONIBLE` indicarà `SI` en la parte de la derecha. Si es así, situaros en la línea `LANZAR LA ACTUALIZACIÓN' y validad.
* Comenzará la descarga de la actualización.
* Un avez descargada la imagen, vuestra Recalbox se reiniciará automáticamente y comenzará la ejecución de la actualización.

### Método sin internet (offline)

Esta forma de aplicar una actualización es un poquito más complicada a efectuar.

* Buscad en el sitio web oficial de Recalbox la imagen que corresponde a vuestro dispositivo.
* Descargad la imagen junto con el fichero `.SHA1` correspondiente. 
* Una vez descargada en vuestro ordenador, tenéis dos posibilidades:
	
  * Podéis montar la [partition de arranque](./../../tutorials/system/access/remount-partition-with-write-access) de vuestra Recalbox en modo lectura/escritura, y utilizando un programa como [WinSCP](./../../tutorials/system/access/network-access-winscp), [Cyberduck](./../../tutorials/system/access/network-access-cyberduck) o [MobaXTerm](./../../tutorials/system/access/network-access-mobaxterm), podéis depositar los dos ficheros dentro de la carpeta `/boot/update/`.
  
  * Podéis coger la tarjeta SD de vuestra Recalbox (apagándola antes de sacarla por supuesto), meterla en vuestro PC, abrir la unidad de disco llamada `RECALBOX` e introducir los dos ficheros anteriores dentro de la carpeta `/update`.

* Una vez hecho esto, reiniciad vuestra Recalbox y la actualización comenzará a ejecutarse.

## 

Durante la aplicación de la actualización, si las 7 barras de progresión pasan rápidamente al color rojo en lugar de cambiar al blanco poco a poco, fijaros en cuantas barras blancas teníais y contactad con el soporte ([forum](https://forum.recalbox.com) o nuestro [Discord](https://discord.gg/NbQFbGM)).