---
title: Libretro PokeMini
description: 
published: true
date: 2024-06-22T23:10:20.217Z
tags: libretro, pokémon, mini, pokémini
editor: markdown
dateCreated: 2021-05-21T08:32:04.220Z
---

**Libretro PokeMini** est un émulateur pour la console portable Nintendo Pokémon Mini développé en C et C++.

Il est écrit par **JustBurn**.

## ![](/emulators/license.svg) Licence

Ce core est sous licence [**GPLv3**](https://github.com/libretro/PokeMini/blob/master/LICENSE).

## ![](/emulators/compatibility.png) Compatibilité

| RPI0/RPI1 | RPI3 | RPI4/400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Fonctionnalités

| Fonctionnalité | Supporté |
| :---: | :---: |
| Redémarrage | ✔ |
| Sauvegardes | ✔ |
| Sauvegardes instantanées | ✔ |
| Rembobinage | ✔ |
| Options du core | ✔ |
| Retroachievements | ✔ |
| Controls | ✔ |
| Remapping | ✔ |
| Vibration | ✔ |

## ![](/emulators/bios.svg) BIOS

### Liste des bios optionnels

| Nom de fichier | Description | MD5 | Fourni |
| :---: | :---: | :---: | :---: |
| bios.min | BIOS Pokémon Mini | 1e4fb124a3a886865acb574f388c803d | ❌ |

### Emplacement

Placez les bios comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 pokemini
┃ ┃ ┃ ┃ ┣ 🗒 **bios.min**

## ![](/emulators/roms.png) Roms

### Extensions supportées

La rom doit avoir l'extension :

* .min
* .zip

Ce système supporte les roms compressées au format .zip. Attention toutefois, il ne s'agit que d'une archive.

Les fichiers contenus dans les .zip doivent correspondre aux extensions citées précédemment.
De plus, chaque fichier .zip ne doit contenir qu'une seule rom compressée.

### Emplacement

Placez les roms comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 pokemini
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.zip**

>Les roms au format **No-Intro** sont vivement conseillées.
{.is-success}

>Pour plus d'information sur les roms, rendez-vous sur [ce tutoriel](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuration avancée de l'émulateur

>Pour pouvoir conserver vos configurations personnalisées lors d'une mise à jour, nous vous conseillons d'utiliser notre fonctionnalité [Surcharges de configuration](./../../../advanced-usage/configuration-override).
{.is-info}

### Accéder aux options

Vous pouvez configurer diverses options de deux façons différentes.

* Via le menu RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via le fichier `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Options du core

| Options | Valeur des options | Variable (pour surcharge) | Valeurs des variables |
| --- | --- | --- | --- |
| Échelle vidéo (redémarrer) | `1x` / `2x` / `3x` / `4x` ✅ / `5x` / `6x` | `pokemini_video_scale` | `1x` / `2x` / `3x` / `4x` / `5x` / `6x` |
| 60Hz Mode | `Désactivé` / `Activé` ✅ | `pokemini_60hz_mode` | `disabled` / `enabled` |
| Filtre LCD | `Matrice de Points` ✅ / `Lignes de Balayage` / `Aucun` | `pokemini_lcdfilter` | `dotmatrix` / `scanline` / `none` |
| Mode LCD | `Analogique` ✅ / `3 Nuances` / `2 Nuances` | `pokemini_lcdmode` | `analog` / `3shades` / `2shades` |
| Contraste LCD | `0` / `16` / `32` / `48` / `64` ✅ / `80` / `96` | `pokemini_lcdcontrast` | `0` / `16` / `32` / `48` / `64` / `80` / `96` |
| Luminosité de l'écran LCD | `-80` / `-60` / `-40` / `-20` / `0` ✅ / `20` / `40` / `60` / `80` | `pokemini_lcdbright` | `-80` / `-60` / `-40` / `-20` / `0` / `20` / `40` / `60` / `80` |
| Palette | `Défaut` ✅ / `Vieux` / `Noir et Blanc` / `Vert` / `Vert Inversé` / `Rouge` / `Rouge Inversé` / `LCD Bleu` / `Rétro-éclairage LED` / `Pouvoir des Filles` / `Bleu` / `Bleu Inversé` / `Sépia` / `Noir et Blanc Inversé` | `pokemini_palette` | `Default` / `Old` / `Monochrome` / `Green` / `Green Vector` / `Red` / `Red Vector` / `Blue LCD` / `LEDBacklight` / `Girl Power` / `Blue` / `Blue Vector` / `Sepia` / `Monochrome Vector` |
| Filtre Piézo | `Activé` ✅ / `Désactivé` | `pokemini_piezofilter` | `enabled` / `disabled` |
| Low Pass Filter | `Désactivé` ✅ / `Activé` | `pokemini_lowpass_filter` | `disabled` / `enabled` |
| Low Pass Filter Level (%) | `5` / `10` / `15` / `20` / `25` / `30` / `35` / `40` / `45` / `50` / `55` / `60` ✅ / `65` / `70` / `75` / `80` / `85` / `90` / `95` | `pokemini_lowpass_range` | `5` / `10` / `15` / `20` / `25` / `30` / `35` / `40` / `45` / `50` / `55` / `60` / `65` / `70` / `75` / `80` / `85` / `90` / `95` |
| Niveau de tremblement de l'écran | `0` / `1` / `2` / `3` ✅ | `pokemini_screen_shake_lv` | `0` / `1` / `2` / `3` |
| Niveau du rumble du contrôleur | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` ✅ | `pokemini_rumble_lv` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` |
| Période du bouton Turbo | `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` ✅ / `19` / `20` / `21` / `22` / `23` / `24` / `25` / `26` / `27` / `28` / `29` / `30` / `31` / `32` / `33` / `34` / `35` / `36` / `37` / `38` / `39` / `40` / `41` / `42` / `43` / `44` / `45` / `46` / `47` / `48` / `49` / `50` / `51` / `52` / `53` / `54` / `55` / `56` / `57` / `58` / `59` / `60` / `61` / `62` / `63` / `64` / `65` / `66` / `67` / `68` / `69` / `70` / `71` / `72` / `73` / `74` / `75` / `76` / `77` / `78` / `79` / `80` / `81` / `82` / `83` / `84` / `85` / `86` / `87` / `88` / `89` / `90` / `91` / `92` / `93` / `94` / `95` / `96` / `97` / `98` / `99` / `100` / `101` / `102` / `103` / `104` / `105` / `106` / `107` / `108` / `109` / `110` / `111` / `112` / `113` / `114` / `115` / `116` / `117` / `118` / `119` / `120` | `pokemini_turbo_period` | `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `21` / `22` / `23` / `24` / `25` / `26` / `27` / `28` / `29` / `30` / `31` / `32` / `33` / `34` / `35` / `36` / `37` / `38` / `39` / `40` / `41` / `42` / `43` / `44` / `45` / `46` / `47` / `48` / `49` / `50` / `51` / `52` / `53` / `54` / `55` / `56` / `57` / `58` / `59` / `60` / `61` / `62` / `63` / `64` / `65` / `66` / `67` / `68` / `69` / `70` / `71` / `72` / `73` / `74` / `75` / `76` / `77` / `78` / `79` / `80` / `81` / `82` / `83` / `84` / `85` / `86` / `87` / `88` / `89` / `90` / `91` / `92` / `93` / `94` / `95` / `96` / `97` / `98` / `99` / `100` / `101` / `102` / `103` / `104` / `105` / `106` / `107` / `108` / `109` / `110` / `111` / `112` / `113` / `114` / `115` / `116` / `117` / `118` / `119` / `120` |

## ![](/emulators/external-links.png) Liens externes

* **Code source utilisé** : [https://github.com/libretro/PokeMini/](https://github.com/libretro/PokeMini/)
* **Documentation Libretro** : [https://docs.libretro.com/library/pokemini/](https://docs.libretro.com/library/pokemini/)
* **Code source officiel** : [https://sourceforge.net/projects/pokemini/](https://sourceforge.net/projects/pokemini/)
* **Documentation officielle** : [https://sourceforge.net/p/pokemini/wiki/Home/](https://sourceforge.net/p/pokemini/wiki/Home/)