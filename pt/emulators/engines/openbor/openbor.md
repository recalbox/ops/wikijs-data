---
title: OpenBOR
description: 
published: true
date: 2024-06-02T17:16:14.251Z
tags: openbor
editor: markdown
dateCreated: 2021-10-19T23:45:40.514Z
---

## ![](/emulators/license.svg) Licença

Este núcleo está licenciado sob [**BSD-3**](https://github.com/DCurrent/openbor/blob/master/LICENSE).

## ![](/emulators/compatibility.png) Compatibilidade

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Recursos



## ![](/emulators/bios.svg) BIOS

> **Não são necessárias BIOS.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Extensões suportadas

As Roms devem ter a extensão:

* .pak

### Local

Coloque as roms assim:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 openbor
┃ ┃ ┃ ┃ ┣ 🗒 **jogo.pak**

## ![](/emulators/advanced-configuration.png) Configuração avançada do emulador

> **Este núcleo não tem opção.**
{.is-success}

## ![external-links.png](/emulators/external-links.png) Links externos

* **Código fonte utilizado** : [https://github.com/DCurrent/openbor/](https://github.com/DCurrent/openbor/)
* **Site oficial do Senile Team** : [https://www.senileteam.com/](https://www.senileteam.com/)
* **Comunidade oficial** : [http://www.chronocrash.com/forum/](http://www.chronocrash.com/forum/)