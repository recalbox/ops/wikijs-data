---
title: Libretro Mednafen_PCE_FAST
description: 
published: true
date: 2024-07-24T15:22:32.998Z
tags: libretro, mednafen, turbografx 16, pce fast
editor: markdown
dateCreated: 2021-08-01T22:45:04.857Z
---

**Libretro Mednafen_PCE_FAST** is a libretro port of Mednafen PCE Fast with the PC Engine SuperGrafx module removed.

## ![](/emulators/license.svg) License

This core is under [**GPLv2**](https://github.com/libretro/beetle-pce-fast-libretro/blob/master/COPYING) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Features

| Feature | Supported |
| :---: | :---: |
| Restart | ✔ |
| Saves | ✔ |
| States | ✔ |
| Rewind | ✔ |
| Netplay | ✔ |
| Core Options | ✔ |
| RetroAchievements | ✔ |
| Controls | ✔ |
| Remapping | ✔ |

## ![](/emulators/bios.svg) BIOS

### List of optional bios

| Filename | Description | MD5 | Provided |
| :---: | :---: | :---: | :---: |
| gexpress.pce | Game Express CD Card | 6d2cb14fc3e1f65ceb135633d1694122 | ❌ |

### Location

Put the bios like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 pcengine
┃ ┃ ┃ ┃ ┣ 🗒 **gexpress.pce**

## ![](/emulators/roms.png) Roms

### Supported extensions

Roms must have the extension:

* .m3u
* .pce
* .zip
* .7z

This system supports compressed roms in .zip/.7z format. However, be careful, it is only an archive.

The files contained in the .zip/.7z must correspond to the extensions mentioned above.
Moreover, each .zip/.7z file must contain only one compressed rom.

### Location

Put the roms like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 pcengine
┃ ┃ ┃ ┃ ┣ 🗒 **game.zip**

>The roms using **No-Intro** format are highly recommended.
{.is-success}

>For more information about roms, go to [this tutorial](./../../../tutorials/games/generalities/isos-and-roms)!
{.is-info}

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Accessing to options

You can configure various options in two different ways.

* Via RetroArch menu:

┣ 📁 RetroArch Menu
┃ ┣ 📁 Core options
┃ ┃ ┣ 🧩 Name_option

* Via the file `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core options

| Options | Options values | Variables (for configuration override) | Variables values |
| --- | --- | --- | --- |
| Color Palette | `RVB` ✅ / `Composite` | `pce_color_palette` | `RGB` / `Composite` |
| Frameskip | `Disabled` ✅ / `Auto` / `Manual` | `pce_fast_frameskip` | `disabled` / `auto` / `manual` |
| Frameskip Threshold (%) | `15` / `18` / `21` / `24` / `27` / `30` / `33 (par défaut)` ✅ / `36` / `39` / `42` / `45` / `48` / `51` / `54` / `57` / `60` | `pce_fast_frameskip_threshold` | `15` / `18` / `21` / `24` / `27` / `30` / `33` / `36` / `39` / `42` / `45` / `48` / `51` / `54` / `57` / `60` |
| Horizontal Overscan (352 Width Mode Only) | `300` / `302` / `304` / `306` / `308` / `310` / `312` / `314` / `316` / `318` / `320` / `322` / `324` / `326` / `328` / `330` / `332` / `334` / `336` / `338` / `340` / `342` / `344` / `346` / `348` / `350` / `352` ✅ | `pce_hoverscan` | `300` / `302` / `304` / `306` / `308` / `310` / `312` / `314` / `316` / `318` / `320` / `322` / `324` / `326` / `328` / `330` / `332` / `334` / `336` / `338` / `340` / `342` / `344` / `346` / `348` / `350` / `352` |
| Initial scanline | `0` / `1` / `2` / `3` ✅ / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `21` / `22` / `23` / `24` / `25` / `26` / `27` / `28` / `29` / `30` / `31` / `32` / `33` / `34` / `35` / `36` / `37` / `38` / `39` / `40` | `pce_initial_scanline` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `21` / `22` / `23` / `24` / `25` / `26` / `27` / `28` / `29` / `30` / `31` / `32` / `33` / `34` / `35` / `36` / `37` / `38` / `39` / `40` |
| Last scanline | `208` / `209` / `210` / `211` / `212` / `213` / `214` / `215` / `216` / `217` / `218` / `219` / `220` / `221` / `222` / `223` / `224` / `225` / `226` / `227` / `228` / `229` / `230` / `231` / `232` / `233` / `234` / `235` / `236` / `237` / `238` / `239` / `240` / `241` / `242` ✅ | `pce_last_scanline` | `208` / `209` / `210` / `211` / `212` / `213` / `214` / `215` / `216` / `217` / `218` / `219` / `220` / `221` / `222` / `223` / `224` / `225` / `226` / `227` / `228` / `229` / `230` / `231` / `232` / `233` / `234` / `235` / `236` / `237` / `238` / `239` / `240` / `241` / `242` |
| PSG Sound Channel 0 Volume (%) | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `pce_fast_sound_channel_0_volume` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
| PSG Sound Channel 1 Volume (%) | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `pce_fast_sound_channel_1_volume` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
| PSG Sound Channel 2 Volume (%) | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `pce_fast_sound_channel_2_volume` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
| PSG Sound Channel 3 Volume (%) | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `pce_fast_sound_channel_3_volume` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
| PSG Sound Channel 4 Volume (%) | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `pce_fast_sound_channel_4_volume` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
| PSG Sound Channel 5 Volume (%) | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `pce_fast_sound_channel_5_volume` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
| Mouse Sensitivity | `0.25` / `0.50` / `0.75` / `1.00` / `1.25` ✅ / `1.50` / `1.75` / `2.00` / `2.25` / `2.50` / `2.75` / `3.00` / `3.25` / `3.50` / `3.75` / `4.00` / `4.25` / `4.50` / `4.75` / `5.00` | `pce_mouse_sensitivity` | `0.25` / `0.50` / `0.75` / `1.00` / `1.25` / `1.50` / `1.75` / `2.00` / `2.25` / `2.50` / `2.75` / `3.00` / `3.25` / `3.50` / `3.75` / `4.00` / `4.25` / `4.50` / `4.75` / `5.00` |
| Disable Soft Reset (RUN+SELECT) | `Désactivé` ✅ / `Activé` | `pce_disable_softreset` | `disabled` / `enabled` |
| P1 Default Joypad Type | `2 Buttons` ✅ / `6 Buttons` | `pce_fast_default_joypad_type_p1` | `2 Buttons` / `6 Buttons` |
| P2 Default Joypad Type | `2 Buttons` ✅ / `6 Buttons` | `pce_fast_default_joypad_type_p2` | `2 Buttons` / `6 Buttons` |
| P3 Default Joypad Type | `2 Buttons` ✅ / `6 Buttons` | `pce_fast_default_joypad_type_p3` | `2 Buttons` / `6 Buttons` |
| P4 Default Joypad Type | `2 Buttons` ✅ / `6 Buttons` | `pce_fast_default_joypad_type_p4` | `2 Buttons` / `6 Buttons` |
| P5 Default Joypad Type | `2 Buttons` ✅ / `6 Buttons` | `pce_fast_default_joypad_type_p5` | `2 Buttons` / `6 Buttons` |
| Turbo ON/OFF Toggle | `Disabled` ✅ / `Enabled` | `pce_turbo_toggling` | `disabled` / `enabled` |
| Alternate Turbo Hotkey | `Disabled` ✅ / `Enabled` | `pce_turbo_toggle_hotkey` | `disabled` / `enabled` |
| Turbo Delay | `1` / `2` / `3` ✅ / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` | `pce_turbo_delay` | `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` |
| CD Image Cache (Restart) | `Disabled` ✅ / `Enabled` | `pce_fast_cdimagecache` | `disabled` / `enabled` |
| CD BIOS (Restart) | `System Card 3` ✅ / `Games Express` / `System Card 1` / `System Card 2` / `System Card 2 US` / `System Card 3 US` | `pce_fast_cdbios` | `System Card 3` / `Games Express` / `System Card 1` / `System Card 2` / `System Card 2 US` / `System Card 3 US` |
| (CD) CD Speed | `1` ✅ / `2` / `4` / `8` | `pce_cdspeed` | `1` / `2` / `4` / `8` |
| (CD) ADPCM Volume % | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ / `110` / `120` / `130` / `140` / `150` / `160` / `170` / `180` / `190` / `200` | `pce_adpcmvolume` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` / `110` / `120` / `130` / `140` / `150` / `160` / `170` / `180` / `190` / `200` |
| (CD) CDDA Volume % | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ / `110` / `120` / `130` / `140` / `150` / `160` / `170` / `180` / `190` / `200` | `pce_cddavolume` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` / `110` / `120` / `130` / `140` / `150` / `160` / `170` / `180` / `190` / `200` |
| (CD) PSG Volume % | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ / `110` / `120` / `130` / `140` / `150` / `160` / `170` / `180` / `190` / `200` | `pce_cdpsgvolume` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` / `110` / `120` / `130` / `140` / `150` / `160` / `170` / `180` / `190` / `200` |
| No Sprite Limit | `Disabled` ✅ / `Enabled` | `pce_nospritelimit` | `disabled` / `enabled` |
| CPU Overclock Multiplier (Restart) | `1` ✅ / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `20` / `30` / `40` / `50` | `pce_ocmultiplier` | `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `20` / `30` / `40` / `50` |

## ![](/emulators/external-links.png) External links

* **Used source code**: [https://github.com/libretro/beetle-pce-fast-libretro/](https://github.com/libretro/beetle-pce-fast-libretro/)
* **Librtro documentation**: [https://docs.libretro.com/library/beetle_pce_fast/](https://docs.libretro.com/library/beetle_pce_fast/)