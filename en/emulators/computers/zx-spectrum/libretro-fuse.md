---
title: Libretro Fuse
description: 
published: true
date: 2021-11-07T00:31:02.784Z
tags: libretro, zx, spectrum, fuse
editor: markdown
dateCreated: 2021-07-29T18:04:50.385Z
---



## ![](/emulators/license.svg) License

This core is under [**GPLv3**](https://github.com/libretro/fuse-libretro/blob/master/LICENSE) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Features



## ![](/emulators/bios.svg) BIOS

### List of optional bios

| Filename | Description | MD5 | Provided |
| :---: | :---: | :---: | :---: |
| 128p-0.rom | Pentagon 1024K ROM | a249565f03b98d004ee7f019570069cd | ❌ |
| 128p-1.rom | Pentagon 1024K ROM | 6e09e5d3c4aef166601669feaaadc01c | ❌ |
| gluck.rom | Pentagon 1024K ROM | d5869034604dbfd2c1d54170e874fd0a | ❌ |
| trdos.rom | Pentagon 1024K ROM | 0c42e3b9ab8dd91ea096f1d0c07c55e5 0da70a5d2a0e733398e005b96b7e4ba6 14399030d8228ca1b16872ed426a5835 3f8a2a239a10b6694ec79148a292cfb4 4123fd0b5c218ef704770596dc6533e1 48b5da4079ff8394852429e53cfc607f 4a3e2b04982ac6c594adb6793be4d6e7 4da473775c4badcc83ab5d86dc8231de 53e2f417c6996df9af170e147df8e369 5c097b0fb75bb7147104d6e77db0300a 62cbbdca554c8c23676618d4c83ef40e 6459c606ff23a610e504d0387383148a 7031f2610845d39c54c97097afa8fa03 a3242d31a0624a64a56f3a6cb5484e7c a701f54355b53fa93fb8599933d518b7 a92db09c9aa3cfda478a9bf1eec7ff90 aa9f9acf05382aff569dfdadde4ef8f2 b08fa49b5de8448e94936a9d061dc0f5 b4c9634312b796063015450daef13dfa b6d8db853c24621cbd5fef3c892c976c c511eaa8fcc968cc13baf7ad80f3aea3 cc46c7bacbbaf528a864113c76d9b9c9 d6f43c70c003f07d0a694f81ff55db95 dd70541ed6a6e8176e8dace64f9344ad | ❌ |
| 256s-0.rom | Scorpion 256K ROM | b9fda5b6a747ff037365b0e2d8c4379a | ❌ |
| 256s-1.rom | Scorpion 256K ROM | 643861ad34831b255bf2eb64e8b6ecb8 | ❌ |
| 256s-2.rom | Scorpion 256K ROM | d8ad507b1c915a9acfe0d73957082926 | ❌ |
| 256s-3.rom | Scorpion 256K ROM | ce0723f9bc02f4948c15d3b3230ae831 | ❌ |

### Location

Put the bios like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 fuse
┃ ┃ ┃ ┃ ┣ 🗒 **128p-0.rom**
┃ ┃ ┃ ┃ ┣ 🗒 **128p-1.rom**
┃ ┃ ┃ ┃ ┣ 🗒 **gluck.rom**
┃ ┃ ┃ ┃ ┣ 🗒 **trdos.rom**
┃ ┃ ┃ ┃ ┣ 🗒 **256s-0.rom**
┃ ┃ ┃ ┃ ┣ 🗒 **256s-1.rom**
┃ ┃ ┃ ┃ ┣ 🗒 **256s-2.rom**
┃ ┃ ┃ ┃ ┣ 🗒 **256s-3.rom**

## ![](/emulators/roms.png) Roms

### Supported extensions

Roms must have the extension:

* .tzx
* .tap
* .z80
* .rzx
* .scl
* .trd
* .zip
* .7z

This system supports compressed roms in .zip/.7z format. However, be careful, it is only an archive.

The files contained in the .zip/.7z must correspond to the extensions mentioned above.
Moreover, each .zip/.7z file must contain only one compressed rom.

### Location

Put the roms like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 zxspectrum
┃ ┃ ┃ ┃ ┣ 🗒 **game.zip**

>The roms using **TOSEC** format are highly recommended.
{.is-success}

>For more information about roms, go to [this tutorial](./../../../tutorials/games/generalities/isos-and-roms)!
{.is-info}

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Accessing to options

You can configure various options in two different ways.

* Via RetroArch menu:

┣ 📁 RetroArch Menu
┃ ┣ 📁 Core options
┃ ┃ ┣ 🧩 Name_option

* Via the file `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core options

| Options | Options values | Variables (for configuration override) | Variables values |
| --- | --- | --- | --- |
| Model (needs content load) | `Spectrum 48K` ✅ / `Spectrum 48K (NTSC)` / `Spectrum 128K` / `Spectrum +2` / `Spectrum +2A` / `Spectrum +3` / `Spectrum +3e` / `Spectrum SE` / `Timex TC2048` / `Timex TC2068` / `Timex TS2068` / `Spectrum 16K` / `Pentagon 128K` / `Pentagon 512K` / `Pentagon 1024` / `Scorpion 256K` | `fuse_machine` | `Spectrum 48K` / `Spectrum 48K (NTSC)` / `Spectrum 128K` / `Spectrum +2` / `Spectrum +2A` / `Spectrum +3` / `Spectrum +3e` / `Spectrum SE` / `Timex TC2048` / `Timex TC2068` / `Timex TS2068` / `Spectrum 16K` / `Pentagon 128K` / `Pentagon 512K` / `Pentagon 1024` / `Scorpion 256K` |
| Hide Video Border | `Disabled` ✅ / `Enabled` | `fuse_hide_border` | `disabled` / `enabled` |
| Tape Fast Load | `Enabled` ✅ / `Disabled` | `fuse_fast_load` | `enabled` / `disabled` |
| Tape Load Sound | `Enabled` ✅ / `Disabled` | `fuse_load_sound` | `enabled` / `disabled` |
| Speaker Type | `tv speaker` ✅ / `beeper` / `unfiltered` | `fuse_speaker_type` | `tv speaker` / `beeper` / `unfiltered` |
| AY Stereo Separation | `none` ✅ / `acb` / `abc` | `fuse_ay_stereo_separation` | `none` / `acb` / `abc` |
| Transparent Keyboard Overlay | `Enabled` ✅ / `Disabled` | `fuse_key_ovrlay_transp` | `enabled` / `disabled` |
| Time to Release Key in ms | `500` ✅ / `1000` / `100` / `300` | `fuse_key_hold_time` | `500` / `1000` / `100` / `300` |
| Joypad Left mapping | `<none>` ✅ / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` / `Enter` / `Caps` / `Symbol` / `Space` | `fuse_joypad_left` | `<none>` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` / `Enter` / `Caps` / `Symbol` / `Space` |
| Joypad Right mapping | `<none>` ✅ / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` / `Enter` / `Caps` / `Symbol` / `Space` | `fuse_joypad_right` | `<none>` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` / `Enter` / `Caps` / `Symbol` / `Space` |
| Joypad Up mapping | `<none>` ✅ / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` / `Enter` / `Caps` / `Symbol` / `Space` | `fuse_joypad_up` | `<none>` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` / `Enter` / `Caps` / `Symbol` / `Space` |
| Joypad Down mapping | `<none>` ✅ / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` / `Enter` / `Caps` / `Symbol` / `Space` | `fuse_joypad_down` | `<none>` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` / `Enter` / `Caps` / `Symbol` / `Space` |
| Joypad Start mapping | `<none>` ✅ / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` / `Enter` / `Caps` / `Symbol` / `Space` | `fuse_joypad_start` | `<none>` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` / `Enter` / `Caps` / `Symbol` / `Space` |
| Joypad A button mapping | `<none>` ✅ / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` / `Enter` / `Caps` / `Symbol` / `Space` | `fuse_joypad_a` | `<none>` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` / `Enter` / `Caps` / `Symbol` / `Space` |
| Joypad B button mapping | `<none>` ✅ / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` / `Enter` / `Caps` / `Symbol` / `Space` | `fuse_joypad_b` | `<none>` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` / `Enter` / `Caps` / `Symbol` / `Space` |
| Joypad X button mapping | `<none>` ✅ / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` / `Enter` / `Caps` / `Symbol` / `Space` | `fuse_joypad_x` | `<none>` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` / `Enter` / `Caps` / `Symbol` / `Space` |
| Joypad Y button mapping | `<none>` ✅ / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` / `Enter` / `Caps` / `Symbol` / `Space` | `fuse_joypad_y` | `<none>` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` / `Enter` / `Caps` / `Symbol` / `Space` |
| Joypad L button mapping | `<none>` ✅ / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` / `Enter` / `Caps` / `Symbol` / `Space` | `fuse_joypad_l` | `<none>` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` / `Enter` / `Caps` / `Symbol` / `Space` |
| Joypad R button mapping | `<none>` ✅ / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` / `Enter` / `Caps` / `Symbol` / `Space` | `fuse_joypad_r` | `<none>` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` / `Enter` / `Caps` / `Symbol` / `Space` |
| Joypad L2 button mapping | `<none>` ✅ / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` / `Enter` / `Caps` / `Symbol` / `Space` | `fuse_joypad_l2` | `<none>` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` / `Enter` / `Caps` / `Symbol` / `Space` |
| Joypad R2 button mapping | `<none>` ✅ / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` / `Enter` / `Caps` / `Symbol` / `Space` | `fuse_joypad_r2` | `<none>` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` / `Enter` / `Caps` / `Symbol` / `Space` |
| Joypad L3 button mapping | `<none>` ✅ / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` / `Enter` / `Caps` / `Symbol` / `Space` | `fuse_joypad_l3` | `<none>` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` / `Enter` / `Caps` / `Symbol` / `Space` |
| Joypad R3 button mapping | `<none>` ✅ / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` / `Enter` / `Caps` / `Symbol` / `Space` | `fuse_joypad_r3` | `<none>` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` / `Enter` / `Caps` / `Symbol` / `Space` |

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/libretro/fuse-libretro/](https://github.com/libretro/fuse-libretro/)
* **Libretro documentation**: [https://docs.libretro.com/library/fuse/](https://docs.libretro.com/library/fuse/)