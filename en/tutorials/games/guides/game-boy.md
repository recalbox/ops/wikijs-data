---
title: Game Boy
description: 
published: true
date: 2021-09-05T12:54:09.513Z
tags: nintendo, game, boy, color, gameboy
editor: markdown
dateCreated: 2021-08-07T23:47:53.391Z
---

With the tutorials available here, you will discover how to play 2 on the same game! And even on 2 different games and 2 different systems!

Here are the available tutorials:

[Play two players games with GameLink](play-two-players-games-with-gamelink)
[Play with two Game Boy](play-with-two-game-boy)