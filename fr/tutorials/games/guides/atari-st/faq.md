---
title: FAQ
description: 
published: true
date: 2024-12-12T12:20:31.289Z
tags: atari, st, faq, 8.0+
editor: markdown
dateCreated: 2021-08-29T17:21:16.124Z
---

## Quelles machines puis-je utiliser avec l'Atari ST ?

Suivant les sous-répertoires, la machine à utiliser sera automatiquement sélectionnée en fonction de ces critères :

```text
"/atarist/": ST,
"/atari-st/": ST,
"/st/": ST,
"/atariste/": STE,
"/atari-ste/": STE,
"/ste/": STE,
"/ataritt/": TT,
"/atari-tt/": TT,
"/tt/": TT,
"/atarimegaste/": MEGA_STE,
"/atari-megaste/": MEGA_STE,
"/atari-mega-ste/": MEGA_STE,
"/megaste/": MEGA_STE,
"/mega-ste/": MEGA_STE,
"/falcon/": FALCON,
"/atari-falcon/": FALCON,
```

## Il existe plusieurs bios pour cette machine, comment cela est-il géré ?

En fonction de la machine, le système cherchera le bios correspondant automatiquement :

- En premier lieu, le bios de la machine à utiliser dans un sous-répertoire correspondant à la machine :

  - `falcon.img`
  - `megaste.img`
  - `st.img`
  - `ste.img`
  - `tt.img`

- Si le bios correspondant dans la liste ci-dessus n'est pas trouvé pour la machine souhaitée, le bios `tos.img` sera recherché dans le même sous-répertoire

- Si aucun bios `tos.img` n'est trouvé, le bios sera recherché avec le chemin `share/bios/tos.img`.

- Enfin, si aucun des bios ci-dessus n'est trouvé, le bios par défaut EmuTOS sera utilisé.

## Est-ce que l'Atari ST bénéficie des surcharges de configuration ?

Oui et pour tous les émulateurs fournis !

Pour les cores Libretro, les fichiers `.retroarch.cfg` sont à utiliser. Pour le core Hatari standalone, il faut utiliser les fichiers `.hatari.cfg`.

## Est-ce que les 2 joysticks sont pris en compte ?

Non, seul le premier joystick est utilisable pour le moment.

## Comment sont configurés les boutons ?

Voici la correspondance des boutons :

| Bouton de manette | Bouton du système |
| :--- | :--- |
| `A` | clic droit |
| `B` | clic gauche |
| `X` | ouvre l'interface (pour changer de disquette, etc...) |
| `Y` | Shift |
| `Select` | bascule du mode souris/joystick |
| `R1` | change la sensibilité de la souris |
| `L1` | ouvre le clavier virtuel |
| `L2` | ouvre un menu permettant de voir le mode de contrôle dans lequel on est (souris/joystick) ; ainsi que le niveau de réglage de la sensibilité de la souris (un chiffre qui varie à coté de MS : - valeur qui est modifiée avec R1). |

## Existe-t-il des outils concernant les roms ?

Oui, et on vous les fournis ! Ces outils au nombre de 3 se trouvent dans `/usr/share/hatari` :

* **atari-hd-<span>image.</span>sh** : ce script vous aidera à créer une disque dur avec partitionnement, nommage des volume, etc. Pour s'en servir : `bash atari-hd-image.sh`.
* **hmsa** : application qui permet de convertir entre les formats de disquette MSA <=> ST. Pour s'en servir : `./hmsa`.
* **zip2<span>st.</span>sh** : c'est un convertisseur de contenu zip => image disquette .st (utile pour les jeux falcon qu'on ne trouve que sous forme de zip...). Pour s'en servir : `bash zip2st.sh`.

## Est-ce que l'émulateur Hatari supporte les surcharges ?

Oui ! Tout comme l'émulateur Libretro Hatari, le standalone Hatari supporte ses propres surcharges avec comme suffixe `.hatari.cfg`. Celles-ci sont utilisable via `<game>.<ext>.hatari.cfg` pour un jeu spécifique ou `.hatari.cfg` pour un répertoire complet, de la même manière que les surcharges `.retroarch.cfg`.

## Puis-je utiliser des roms au format `.ZIP` ?

Oui, même pour le standalone Hatari !