---
title: Atari ST
description: 
published: true
date: 2021-08-09T14:19:27.077Z
tags: atari, st, atari-st, computers
editor: markdown
dateCreated: 2021-07-28T20:09:29.696Z
---

![](/emulators/computers/atarist.svg){.align-center}

## Technical data

* **Manufacturer**: Atari Corporation
* **Year of release**: 1985
* **OS**: Atari TOS
* **CPU**: Motorola 68000 16-/32-bit CPU @ 8 MHz
* **RAM**: 512KB or 1MB
* **Drive**: single-sided 3½" floppy disk drive
* **Sound chip**: Yamaha YM2149 3-voice squarewave
* **Display**:
  * **Low resolution** : 320 × 200 (16 colors), palette of 512 colors
  * **Medium resolution** : 640 × 200 (4 colors), palette of 512 colors

## Presentation

The **Atari ST** is a family of personal computers designed by the American firm Atari, whose commercial success marked the second half of the 1980s and the beginning of the 1990s. The success was as much for the general public (video games) as for professionals (word processing, DTP and especially MAO).

The Atari ST microcomputer is particularly noteworthy in the history of computing as the machine that enabled the development of computer music and the democratization of the MIDI standard. In 2010, the specialized magazine Sound on Sound ranked it among the 25 most important products responsible for the changes in music recording. This machine is still considered today as a reference in the field because of its robustness and its extreme precision for MIDI sequencing.

## Emulators

[Libretro Hatari](libretro-hatari)