---
title: Configurar uma tela externa
description: 
published: true
date: 2024-07-23T22:59:18.802Z
tags: configuração, tela, externa, monitor, tv
editor: markdown
dateCreated: 2021-12-23T20:46:23.640Z
---

> Este tutorial é apenas para PC!
{.is-warning}

Com um notebook/laptop (por exemplo), você pode decidir ter o sinal de vídeo em um monitor externo. Isso pode ser útil se a placa de vídeo não for compatível com o Recalbox ou se o monitor interno não estiver mais funcionando.

## Conheça as saídas de vídeo disponíveis

* Conecte via [SSH](./../../system/access/root-access-terminal-cli)
* Digite o seguinte comando:

```shell
xrandr
```

Isso fornecerá as saídas de vídeo disponíveis, suas resoluções e taxas de atualização. Por exemplo:

```shell
Screen 0: minimum 8 x 8, current 1920 x 1080, maximum 32767 x 32767
DVI-D-0 disconnected primary (normal left inverted right x axis y axis)
HDMI-0 connected 1920x1080+0+0 (normal left inverted right x axis y axis) 598mm x 336mm
   2560x1440     59.95 +  69.93
   1920x1080     60.00*   59.94    50.00    23.98    60.05    60.00    50.04
   1680x1050     59.95
   1440x900      59.89
   1280x1024     75.02    60.02
   1280x960      60.00
   1280x800      59.81
   1280x720      60.00    59.94    50.00
   1152x864      75.00
   1024x768      75.03    70.07    60.00
   800x600       75.00    72.19    60.32    56.25
   720x576       50.00
   720x480       59.94
   640x480       75.00    72.81    59.94    59.93
DP-0 disconnected (normal left inverted right x axis y axis)
DP-1 disconnected (normal left inverted right x axis y axis)
DP-2 disconnected (normal left inverted right x axis y axis)
DP-3 disconnected (normal left inverted right x axis y axis)
DP-4 disconnected (normal left inverted right x axis y axis)
DP-5 connected (normal left inverted right x axis y axis)
   2560x1440     59.95 +
   1920x1080     59.94    50.00
   1280x720      59.94    50.00
   1024x768      60.00
   800x600       60.32
   720x576       50.00
   720x480       59.94
   640x480       59.94    59.93

```

* Abra o arquivo [recalbox.conf](./../../../basic-usage/getting-started/recalboxconf-file)
* Depois de selecionar a saída, insira-a nesta parte do arquivo `recalbox.conf`:

```ini
## Prefered external screen retrieved from ssh : xrandr
system.externalscreen.prefered=DP-5
## Force selected external screen to resolution ex : 1920x1080
system.externalscreen.forceresolution=1920x1080
## Force selected external screen to frequency ex: 60.00
system.externalscreen.forcefrequency=59.94
```

> O sinal **;** estará na frente das 2 linhas que você está modificando por padrão. Você terá que remover este sinal da frente de cada uma das 2 linhas para ativar suas modificações **(senão não irá funcionar)**.
{.is-info}