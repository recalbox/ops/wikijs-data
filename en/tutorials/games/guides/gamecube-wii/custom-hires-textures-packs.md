---
title: Custom high-resolution texture packs
description: 
published: true
date: 2021-08-08T08:47:10.267Z
tags: custom, dolphin, texture, pack
editor: markdown
dateCreated: 2021-08-08T08:47:10.267Z
---

Dolphin allows, in addition to running Gamecube games, to improve textures or even for some videos in HD, created by enthusiasts (thank you to them) to further beautify your favorite games.

For the moment there are texture packs for the following games:

* Mario Kart Double Dash
* Luigi's Mansion
* The Legend of Zelda: Ocarina of Time
* Resident Evil 2
* Resident Evil 3 Nemesis

This list is not exhaustive, others are surely in preparation or already released when you read these lines.

They are available on the [Dolphin forum](https://forums.dolphin-emu.org/Forum-custom-texture-projects). Supported formats are :

* BC7
* DDS

Put your texture packs in the following folder: `/recalbox/share/saves/dolphin-emu/Load/Textures/`.

All textures must be in a folder with the ID of the game you are installing the pack for or the equivalent of 3 letters without a region.

* `GM4`: id without region
* `GM4P01`: full id 

![Example for Mario Kart Double Dash (Europe)](/emulators/consoles/gc-textures-packs.png)

You can find this information in Dolphin:

* Start any GameCube game.
* Once the game has started, press `Hotkey` + `B` simultaneously. 
* The Dolphin menu appears with a list of your games.
* Search for the game whose ID you need to know. The ID is shown in the fourth column and is a combination of 6 letters and numbers.

>If the textures are not in a correctly named folder, then the pack will not work!
{.is-warning}