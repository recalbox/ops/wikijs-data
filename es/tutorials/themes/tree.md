---
title: 1. Arborescencia
description: 
published: true
date: 2024-04-07T13:22:47.553Z
tags: 9.2+, temas
editor: markdown
dateCreated: 2024-04-07T12:48:55.158Z
---

## Introducción

Los themes presentan una estructura específica de directorios y de ficheros, como se describe a continuación.

Todos los ficheros de un theme utilizan el formato `XML`. Es un lenguaje similar al HTML muy fácil de usar. Se construye a base de tags del estilo de `<balise></balise>` o mediante tags que se cierran ellos sólos como `<balise />`.

## Un monton de carpetas...

Cada theme está compuesto de un monton de directorios.

### Sistema

Para cada sistema disponible en Recalbox, existe una carpeta que contiene todas las imágenes correspondientes a dicho sistema.

### Sistemas virtuales de base

Cuando hablamos de los sistemas virtuales de base nos referimos alos sistemas como los favoritos, los últimos juegos jugados, los juegos multijugadores, etc.

Aquí se presenta la lista de todos los sistemas virtuales de base junto con el nombre de su directorio.

| Sistema virtual | Nombre de la carpeta |
| :--- | :--- |
| Juegos arcade | arcade |
| Todos los juegos | auto-allgames |
| Ultimos juegos jugados | auto-lastplayed |
| Lightgun | auto-lightgun |
| Multijugadores | auto-multiplayer |
| Juegos TATE | auto-tate |
| Favoritos | favorites |

### Sistemas virtuales por género

Los sistemas virtuales por género corresponden a los géneros de los juegos que se recuperan al scrapear. Estos géneros tambien los podéis modificar vosotros mismos desde la interfaz de Recalbox o editando manualmente el fichero gamelist.xml. 

A continuación se presenta la lista de sistemas virtuales por género junto con el nombre de sus carpetas.

| Sistema virtual | Nombre de la carpeta |
| :--- | :--- |
| Action (todos) | auto-action |
| Action - Battle Royale | auto-actionbattleroyale |
| Action - Beat'em up | auto-actionbeatemup |
| Action - Combate | auto-actionfighting |
| Action - Disparo en primera persona | auto-actionfirstpersonshooter |
| Action - Juego de plataformas | auto-actionplatformer |
| Action - Juegos de tiros/plataformas | auto-actionplatformshooter |
| Action - Ritmo & musica | auto-actionrythm |
| Action - Shoot-em up | auto-actionshootemup |
| Action - Disparos con accesorios | auto-actionshootwithgun |
| Action - Infiltración | auto-actionstealth |
| Aventure | auto-aventura |
| Aventure - Aventura gráfica | auto-adventuregraphics |
| Aventure - Film interactivo | auto-adventureinteractivemovie |
| Aventure - Aventura 3D tiempo real | auto-adventurerealtime3d |
| Aventure - Juegos de supervivencia | auto-adventuresurvivalhorror |
| Aventure - Aventura textual | auto-adventuretext |
| Aventure - Nouvelle visuelle | auto-adventurevisualnovels |
| Juego de tablero | auto-board |
| Casino | auto-casino |
| Juego grand public | auto-casual |
| Compilación multi-juegos | auto-compilation |
| Démo de la escena démo | auto-demoscene |
| Cartas digitales | auto-digitalcard |
| Juegos educativos | auto-educative |
| Juego de sociedad multijugador | auto-party |
| Flipper | auto-pinball |
| Puzzle & lógica | auto-puzzleandlogic |
| Juego de rôles (todos) | auto-rpg |
| Juego de rôles - action | auto-rpgaction |
| Juego de rôles - exploración de mazmorras | auto-rpgdungeoncrawler |
| Juego de rôles - sociedades | auto-rpgfirstpersonpartybased |
| Jeu de rôles - japoneses | auto-rpgjapanese |
| Jeu de rôles - multijugadores masivos | auto-rpgmmo |
| Jeu de rôles - tácticos | auto-rpgtactical |
| Simulación | auto-simulation |
| Simulación -  | auto-simulationbuildandmanagement |
| Simulación -  | auto-simulationfishandhunt |
| Simulación -  | auto-simulationlife |
| Simulación -  | auto-simulationscifi |
| Simulación -  | auto-simulationvehicle |
| Deportes (todos) | auto-sports |
| Deportes - Competición deportiva | auto-sportcompetitive |
| Deportes - Combate/Deporte violento | auto-sportfight |
| Deportes - Carreras | auto-sportracing |
| Deportes - simulación deportiva | auto-sportsimulation |
| Estratégia (todos) | auto-strategy |
| Estratégia - Exploración, expansión, explotación & exterminación | auto-strategy4x |
| Estratégia - Artillería | auto-strategyartillery |
| Estratégia - Batallas automáticas | auto-strategyautobattler |
| Estratégia - Arena de combate multijugadores | auto-strategymoba |
| Estratégia - Juegos de estratégia en tiempo real | auto-strategyrts |
| Estratégia - Estrategia por turnos | auto-strategytbs |
| Estratégia - Defensa de torres | auto-strategytowerdefense |
| Estratégia - Juego de guerra | auto-strategywargame |
| Trivial | auto-trivia |

### Sistemas virtuales arcade por construtor

Estos sistemas se refieren a los sistemas arcade clasificados por constructor.

Aquí tenéis la liste de sistemas virtuales arcade por constructor junto con el nombre de la carpeta correspondiente.

| Sistema virtual | Nombre de la carpeta |
| :--- | :--- |
| Acclaim | auto-arcade-acclaim |
| Atari | auto-arcade-atari |
| Atlus | auto-arcade-atlus |
| BanPresto | auto-arcade-banpresto |
| Capcom | auto-arcade-capcom |
| Capcom CPS1 | auto-arcade-capcom-cps1 |
| Capcom CPS2 | auto-arcade-capcom-cps2 |
| Capcom CPS3 | auto-arcade-capcom-cps3 |
| CAVE | auto-arcade-cave |
| Data East | auto-arcade-dataeast |
| Exidy | auto-arcade-exidy |
| International Games System (I.G.S) | auto-arcade-igs |
| IREM Software Engineering | auto-arcade-irem |
| Incredible Technologies | auto-arcade-itech |
| Jaleco | auto-arcade-jaleco |
| Kaneko | auto-arcade-kaneko |
| Konami | auto-arcade-konami |
| Midway | auto-arcade-midway |
| Mitchell | auto-arcade-mitchell |
| NAMCO | auto-arcade-namco |
| Neo-Geo | auto-arcade-neogeo |
| Nichibutsu | auto-arcade-nichibutsu |
| Nintendo | auto-arcade-nintendo |
| NMK | auto-arcade-nmk |
| Psikyo | auto-arcade-psikyo |
| Sammy | auto-arcade-sammy |
| SEGA | auto-arcade-sega |
| SEGA System 16 | auto-arcade-sega-system16 |
| SEGA System 32 | auto-arcade-sega-system32 |
| Seibu | auto-arcade-seibu |
| SETA | auto-arcade-seta |
| SNK | auto-arcade-snk |
| TAITO | auto-arcade-taito |
| TAITO F3 | auto-arcade-taito-f3 |
| Technōs | auto-arcade-technos |
| TECMO | auto-arcade-temco |
| TOAPLAN | auto-arcade-toaplan |
| VISCO | auto-arcade-visco |

## ... Y ahora los ficheros

A la raíz del theme, existe un fichero llamado `theme.xml`. Este fichero contiene las informaciones relacionadas con los objetos generales tales como la configuración del menu, de la barra de ayuda, etc.

Dentro de cada una de las carpetas descritas aquí arriba, encontraréis un fichero xml (el nombre del fichero es configurable) que contiene las informaciones necesarias para dicho sistema, como los textos de información y los colores de las 4 barras verticales que aparecen a la derecha de vuestra pantalla en el theme por defecto de Recalbox.

## Estructura de los ficheros

La estructura de ficheros es como sigue:

```xml
<theme>
  <include>path/a/incluir1</include>
  <include>path/a/incluir2</include>
  ...
  <include>path/a/incluirX</include>
  <view name="...">
    <object>
      <property1>valor</property1> 
      <property2>valor</property2> 
      ...
      <propertyX>valor</propertyX> 
    </object>
    ...
  </view>
  ...
  <view name="...">
    <object>
      ...
    </object>
    ...
  </view>
</theme>
```

El contenido exacto se describe en los otros tutoriales dentro de la sección themes.

## Funcionamiento de las etiquetas `<theme>` y `<include>`

### Etiquetas `<theme>`

Es la etiqueta raiz que contiene todos los otros ficheros y etiquetas del theme. Todo lo demás se enctra dentro de `<theme></theme>`. En Recalbox 9.2, esta etiqueta acepta 5 atributos diferentes:

#### Atributo `name`

- Obligatorio : no

El atributo `name` indica el nombre del theme. Puede ser diferente del nombre de la carpeta del theme. Este nombre será visible en la lista de themes disponibles.

Ejemplo :

```xml
<theme name="Mi theme guay">
  ...
</theme>
```

Si no existe este atributo, se utilizará por defecto el nombre de la carpeta.

#### Atributo `recalbox`

- Obligatorio : no

El atributo `recalbox` indica la versión mínima de Recalbox con la que se puede utilizar el theme. Permite indicar si el theme es compatible con la versión de Recalbox instalada.

Ejemplo :

```xml
<theme recalbox="9.2">
  ...
</theme>
```

Si no existe este atributo, el theme se podrá instalar en cualquier version de Recalbox.

#### Atributo `version`

- Obligatorio : no

El atributo `version` indica la versión del theme. Si esta información existe, será visible en la lista de themes disponibles.

Ejemplo :

```xml
<theme version="3.1">
  ...
</theme>
```

Si no existe este atributo, no se mostrará ninguna información sobre la version del theme en la lista de themes de Recalbox.

#### Atributo `compatibility`

- Obligatorio : no

El atributo `compatibilidad` indica el tipo de pantalla con el que es compatible el theme. El theme puede ser compatible con un único tipo de pantalla o con varios.

Lista de posibles valores:

- `hdmi` : pantallas HDMI
- `crt` : pantallas CRT
- `jamma` : pantallas JAMMA
- `tate` : pantallas verticales TATE

Ejemplo :

```xml
<theme compatibility="hdmi,jamma">
  ...
</theme>
```

Si no existe este atributo, se considerará que el theme es compatible sólo en `hdmi`.

#### Atributo `resolutions`

- Obligatorio : no

El atributo `resolutions` se utiliza para indicar los rangos de resolución con los que el theme es compatible. Puede contener varios valores.

Lista de posibles valores :

- `qvga` : resoluciones QVGA (<= 288p)
- `vga` : resoluciones VGA (<= 576p)
- `hd` : resoluciones HD (<= 920p)
- `fhd` : resoluciones Full HD (> 920p)

Ejemplo :

```xml
<theme resolutions="hd,fhd">
  ...
</theme>
```

Sin este atributo, el theme se considerará compatible sólo en `hd, fhd`.

### Etiqueta `<formatVersion>`

La etiqueta `formatVersion` se utiliza para indicar la versión del formato de contenido que rige el tema.

```xml
<formatVersion>5</formatVersion>
```

### Etiqueta `<include>` 

La etiqueta `<include></include>` sirve para incluir otro fichero que contiene otras informaciones relacionadas con el theme.

Ejeumplo :

```xml
<include>
  /path/hacia/fichero
</include>
```

```xml
<include path="/path/hacia/fichero" />
```

#### Atributo `subset`

- Obligatorio : no

El atributo `subset` se utiliza para definir el grupo de opciones al que pertenece el contenido de la etiqueta `<include>`. Existe una lista definida de posibles grupos de opciones:

- `colorset` : colores
- `iconset` : iconos
- `menuset` : menus
- `systemview` : lista de sistemas
- `gamelistview` : lista de juegos
- `gameclipview` : clips de juegos

```xml
<include subset="iconset">
  /path/hacia/fichero/de/iconos.xml
</include>
```

```xml
<include subset="iconset" path="/path/hacia/fichero/de/iconos.xml" />
```

#### Atributo `name`

- Obligatorio : no

El atributo `name` permite indicar un nombre para el fichero que se incluye.

>Este nombre se utiliza junto con el atributo `subset`.
{.is-info}

```xml
<include subset="menuset" name="Azul claro">
  /path/hacia/fichero/de/menus.xml
</include>
```

```xml
<include subset="menuset" name="Azul claro" path="/path/hacia/fichero/de/menus.xml" />
```

### Etiqueta `<view>` 

La etiqueta `<view></view>` junto con los tags descendientes se describen en la [página de objetos](./objects).