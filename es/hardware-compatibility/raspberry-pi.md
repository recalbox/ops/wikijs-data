---
title: Raspberry Pi
description: 
published: true
date: 2023-09-19T09:06:33.422Z
tags: pi, compatibilidad, rasbperry
editor: markdown
dateCreated: 2023-09-19T09:06:33.422Z
---

La Raspberry Pi tiene un procesador ARM. Incluye 1, 2 o 4 puertos USB y un puerto RJ45.  
Su circuito gráfico BMC VideoCore puede descodificar flujos Blu-Ray Full HD (1080p 30 fotogramas por segundo), emular consolas antiguas y ejecutar videojuegos relativamente recientes.

## Accesorios adicionales necesarios

* Fuente de alimentación
* Estuche

## Tarjeta SD

## Raspberry Pi Zero v1.3

![](/compatibilidad/pi-zero.jpg)

¡El primer ordenador de 5 dólares! El ultra-pequeño y ultra-delgado Raspberry Pi Zero es el más pequeño Raspberry Pi factor de forma en el mercado. Es un 40% más rápido que el Raspberry Pi original, pero mide sólo 65 mm de largo por 30 mm de ancho y 5 mm de profundidad.
La Raspberry Pi Zero soporta mini-conectores para ahorrar espacio y el GPIO de 40 pines no está poblado, ofreciendo la flexibilidad de utilizar sólo las conexiones necesarias para su proyecto.

### Características :

* **Procesador:**
  * ARM 11 Single-Core 1 GHz, Broadcom BCM2835 1GHz
* **RAM:**
  * 512 MB
* **Almacenamiento:**
  * Ranura Micro-SD
* **Conectividad**
  * 1 USB para datos (puertos On-The-Go)
  * 1 mini HDMI
  * 1 X Micro-USB de alimentación
  * 1 CSI (cabezal de cámara)
  * Conector GPIO de 40 pines sin poblar
* **Dimensiones**
  * 65 mm x 30 mm x 5 mm

## Raspberry Pi Zero W (Wireless)

![](/compatibility/pi0w.png)

¡La Raspberry Pi Zero W! es el miembro más pequeño de la familia Raspberry Pi, ¡y viene con conectividad **Wi-Fi** y **Bluetooth** integrada!  
Con un procesador tan potente como el A + y B +, la Raspberry Pi Zero W le dará un montón de posibilidades, gracias a su diseño compacto.

>¡Necesitarás un cabezal 2x20 GPIO para conectar la mayoría de las placas de extensión!
{.is-warning}

### Características :

* **CPU :**
  * ARM11, Broadcom BCM2835, Single-core 1GHz
* **GPU:**
  * Integrada
* **RAM:**
  * 512 MB (DDR2)
* **Conectividad**
  * 1 lector micro SD
  * 1 X mini HDMI
  * 1 X micro USB OTG
* **Extras adicionales:** 
  * 1 conector de cámara CSI 
  * Conector GPIO de 40 pines no poblado
* **Red:** 
  * Bluetooth 4.1, Bluetooth Low Energy (BLE)
  * LAN inalámbrica WIFI 802.11b/g/n (150 Mbps)
* **Fuente de alimentación**
  * 1 X micro USB
* **Tamaño:**
  * 65 mm x 31 mm x 5 mm

## Raspberry Pi Zero 2 W

![](/compatibility/pi02w.jpg)

Con una forma idéntica y la misma conectividad, la Raspberry Pi Zero 2 W es una digna sucesora de la Raspberry Pi Zero W de primera generación. La Raspberry Pi Zero 2 W encaja en la mayoría de las carcasas Raspberry Pi Zero existentes.

Basándose en el éxito de la familia Zero, la Raspberry Pi Zero 2 W está construida alrededor de un Sistema-en-Paquete diseñado por Raspberry Pi. Este nuevo microordenador incorpora el chip BCM2710A1 ya utilizado en la Raspberry Pi 3, apoyado por 512 MB de RAM.

### Características

* **CPU:**
  * Broadcom BCM2710A1, SoC de cuatro núcleos de 64 bits (Arm Cortex-A53 @ 1GHz)
* **GPU:**
  * Integrada
* **RAM:**
  * 512 MB LPDDR2
* **Conectividad**
  * Wi-fi 2,4 GHz IEEE 802.11b/g/n
  * Bluetooth 4.2, Bluetooth Low Energy (BLE)
  * 1x Micro-USB 2.0 On-The-Go (OTG)
  * 1x Mini-HDMI
  * 1x Micro-USB para alimentación
  * Toma de E/S GPIO de 40 patillas - compatible con HAT
  * Ranura para tarjeta microSD
  * Conector para cámara CSI-2
* **Multimedia**
  * Descodificación H.264, MPEG-4 (1080p30)
  * Codificación H.264 (1080p30)
  * Gráficos OpenGL ES 1.1, 2.0
* **Tensión de entrada**
  * 5 V CC - 2,5 A
* **Dimensiones**
  * 65 mm × 30 mm

## Raspberry Pi 1 Model A+

![](/compatibility/pi1a.jpg)

La Raspberry Pi 1 Modelo A+ mide 56 mm de largo y 12 mm de ancho y consume hasta un 45% menos de energía que una Raspberry Pi 1 Modelo B+.

### Características :

* **Procesador:**
  * CPU ARM 700 MHz
* **RAM**
  * 256 MB
* **Conectividad**
  * 1 ranura Micro SD
  * 1 USB 2.0
  * 40 GPIO
* **Red:**
  * Ninguna
* **Dimensiones**
  * 65x56x12mm

## Raspberry Pi 2 Model B

![](/compatibility/pi2b.jpg)

### Características :

* **CPU:** 
  * 900MHz Quad-Core ARM BCM2836 Cortex-A7 CPU
* **GPU:** 
  * Coprocesador multimedia VideoCore IV® de doble núcleo
* **RAM **
  * 1 GB RAM
* **Conectividad**
  * 1 puerto de cámara CSI (entrada de vídeo)
  * 40 pines GPIO
  * 1 memoria MicroSD
  * Salida de audio/pantalla: HDMI, DSI Display Port, componentes sobre 3,5 mm
  * 4 puertos USB (USB 2.0)
  * Red integrada: Ethernet 10/100 Mbit/s
* **Fuente de alimentación **
  * MicroUSB de 5 V

## Raspberry Pi 3 Modèle B 1GB

![](/compatibility/pi3b.png)

Descubra la **Raspberry Pi 3 Modelo B 1GB** con **Wi-Fi** y **Bluetooth** integrados.  
Basada en el procesador **Quad Core Broadcom 2837 ARMv8 64bit** actualizado de 900MHz (en la Pi2) a 1,2Ghz, la RPi de nueva generación es ligeramente más grande que la Pi2, más rápida y más potente que sus predecesoras.

### Características :

* **CPU:** 
  * Broadcom BCM2837 (ARM Cortex-A53 Quad-Core 1.2 GHz)
* **GPU:** 
  * ARM v8 Dual-Core _VideoCore IV
* **RAM :** 
  * 1GB
* **Conectividad **
  * 1 x 3.5mm Hembra Stereo Jack
  * 1 x HDMI
  * 4 x USB 2.0
  * 1 x micro SD (SDHC, SDXC)
  * 1 x RJ45 Hembra
* **Extras adicionales:** 
  * 1 x CSI (cabezal de cámara)
  * 1 x DSI (encabezado de pantalla)
  * 40 x GPIO
* **Red:** 
  * WiFi WLAN BCM43143, Wi-Fi N 150 Mbps (IEEE 802.11n)
  * Bluetooth BLE 4.1
* **Fuente de alimentación **
  * Hasta 2,5 A, Micro USB

## Raspberry Pi 3 Modèle B+ 1GB

![](/compatibility/pi3bplus.jpg)

#### ¡La última generación de Raspberry Pi 3! 

La Raspberry Pi 3 modelo B + tiene un ** procesador quad-core de 64 bits ** que funciona a 1,4 GHz, un ** LAN inalámbrica de doble banda de 2,4 GHz y 5 GHz **, una conexión ** Bluetooth 4.2 ** / BLE, una conexión Ethernet más rápida y la opción de alimentación PoE a través del PoE HAT (no incluido).

La última Raspberry Pi 3 modelo B + tiene un procesador de 1,4 GHz quad-core de 64 bits más rápido, 1 GB de RAM, más rápido de doble banda 802.11 b / g / n / ac LAN inalámbrica, Bluetooth 4.2 y mucho más rápido de 300 Mbit / s Ethernet.

Con el procesador ARMv8, puede ejecutar toda la gama de distribuciones ARM GNU/Linux, incluido Snappy Ubuntu Core, así como la edición IoT de Microsoft Windows 10.

* Raspberry Pi 3 Modelo B+ (B Plus) con CPU ARMv8 de cuatro núcleos y 64 bits a 1,4 GHz (BCM2837B0)

### Características :

* **Procesador:** 
  * Broadcom BCM2837B0 (ARM Cortex-A53 Quad-Core 1.4 GHz) / Arm 11
* **GPU:**
  * Coprocesador multimedia VideoCore IV® de doble núcleo
  * Proporciona Open GL ES 2.0, OpenVG acelerado por hardware y descodificación de alto perfil H.264 1080p30
  * Memoria: 512 MB SDRAM
* **RAM :** 
  * 1GB LPDDR2 SDRAM
* **Conectividad **
  * 1 conector estéreo hembra de 3,5 mm
  * 4 PUERTOS USB 2.0
  * 1 RJ45 hembra
  * 1 x HDMI
  * 1 x GPIO (40 pines)
  * 1 ranura micro SD (SDHC, SDXC)
  * 1 x Gigabit Ethernet USB 2.0 (300Mbit/s ethernet)
* **Extras opcionales:**
  * 1 MIPI CSI (interfaz de cámara)
  * 1 MIDI DSI (interfaz de pantalla)
  * 1 x GPIO 40 pines (cabecera)
* **Red:**
  * WIFI 2,4 GHz y 5 GHz IEEE 802.11.b/g/n/ac WLAN ( 10/100/1000 Mbps)
  * Bluetooth 4.2 HS de baja energía (BLE) (CYW43455)
* **Fuente de alimentación:** 
  * Toma micro USB 5V, 2A (compatible con PoE que requiere PoE HAT no suministrado) 

## Raspberry Pi 4

![](/compatibility/pi4.png)

Con hasta 8 GB de memoria, esta Raspberry PI4 es **la Pi4 más potente** y conserva unas capacidades excepcionales en comparación con la Pi3 y la Pi3 modelo B+.

Ofrece un aumento sin precedentes en **velocidad de procesador, rendimiento multimedia, memoria y conectividad** en comparación con la generación anterior de Raspberry Pi 3 modelo B+, al tiempo que mantiene la compatibilidad con versiones anteriores y un consumo de energía similar. Además, gracias a la revisión 1.3, la Raspberry Pi 4 puede soportar más memoria.

Para el usuario final, la Raspberry Pi 4 Modelo B ofrece un rendimiento de escritorio comparable a los sistemas de PC x86 de nivel básico.

### Características :

* **Procesador:** 
  * ARM v8, Broadcom BCM2711, Quad-core Cortex-A72 64bit SoC@ 1.5GHz
* **GPU:**
  * VideoCore VI compatible con OpenGL ES 3.0, decodificación HEVC 4K a 60 fps
* **RAM:** 
  * 1 GB a 8 GB de RAM LPDDR4
* **Conexiones **
  * 1 x MIPI DSI (puerto de pantalla DSI para conectar la pantalla táctil Raspberry Pi)
  * 2 micro HDMI
  * 2 x USB 2.0
  * 2 x USB 3.0
  * 1 GPIO (40 pines)
  * 1 x MIPI CSI (puerto de cámara CSI para conectar la cámara de la Raspberry Pi)
  * 1 micro SD
  * 1 x 3.5mm Jack estéreo hembra
* **Red:**
  * Gigabit Ethernet (RJ45)
  * Bluetooth 5.0
  * Wi-Fi 802.11b/g/n/ac
* **Fuente de alimentación **
  * 5 V CC a través del conector USB-C (mínimo 3 A), 5 V CC a través del encabezado GPIO (mínimo 3 A), compatible con alimentación a través de Ethernet (PoE) (mínimo 3 A).

## Raspberry Pi 400

![](/compatibility/pi400.png)

La Raspberry Pi 400 se basa en la Raspberry Pi 4, pero con una pequeña mejora.

Técnicamente, la Pi 400 está construida sobre la misma base que la Raspberry 4, con los mismos puertos, capacidades gráficas, memoria, fuente de alimentación, etc.
Los puertos GPIO se mantienen, por supuesto, y son fácilmente accesibles en el lado del teclado dedicado a los conectores.
La única diferencia técnica es la velocidad del procesador, que ha pasado de 1,5 GHz para la Raspberry 4 a 1,8 GHz para la Raspberry 400.

La forma de la Raspberry Pi 4 también ha sido completamente rediseñada para adaptarse a este nuevo formato, como muestra esta imagen de la Fundación.
Otra diferencia es que, gracias a su forma menos compacta y al uso de disipadores de calor integrados (la parte gris de la imagen superior), esta Raspberry Pi 400 debería calentarse mucho menos que la Raspberry Pi 4.

### Características :

* **Procesador:**
  * Broadcom BCM2711 quad-core Cortex-A72 (ARM v8) 64-bit SoC @ 1.8 GHz
* **GPU:**
  * VideoCore VI (OpenGL ES 3.0)
    * H.265 (decodificación 4Kp60)
    * H.264 (descodificación 1080p60, codificación 1080p30)
    * Gráficos OpenGL ES 3.0
* **RAM:**
  * 4GB LPDDR4 - 3200 DRAM
* **Conectividad **
  * 1 Gigabit Ethernet
  * 2 puertos USB 3.0
  * 1 puerto USB 2.0
  * 1 GPIO horizontal de 40 patillas
  * 2 puertos micro HDMI (admiten hasta 4Kp60)
  * 1 × ranura para tarjetas MicroSD para sistema operativo y almacenamiento de datos
* **Red:**
  * Doble banda (2,4 GHz y 5,0 GHz)
  * LAN inalámbrica IEEE 802.11b/g/n/ac
  * Bluetooth 5.0, BLE
* **Fuente de alimentación**
  * DC 5 V a través del conector USB-C (5V - 3A) - Fuente de alimentación oficial recomendada.
* **Temperatura de funcionamiento**
  * De 0 °C a +50 °C
* **Teclado:**
  * Teclado compacto con 78 o 79 teclas (dependiendo de la variante regional).
* **Dimensiones:**
  * 286 mm × 122 mm × 23 mm (máximo)