---
title: OpenBOR
description: 
published: true
date: 2024-07-14T19:20:51.607Z
tags: openbor
editor: markdown
dateCreated: 2023-09-11T06:30:05.772Z
---



## ![](/emulators/license.svg) Licencia

El core posee la siguiente licencia [**BSD-3**](https://github.com/DCurrent/openbor/blob/master/LICENSE).

## ![](/emulators/compatibility.png) Compatibilidad

 | RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | 
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Funcionalidades



## ![](/emulators/bios.svg) BIOS

>**No necesita bios.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Extensiones de roms soportadas

Las roms deben tener las extensiones siguientes:

* .pak

### Emplazamiento

Disponed vuestras roms como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 openbor
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.pak**

## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

>**Este core no tiene opciones.**
{.is-success}

## ![](/emulators/external-links.png) Enlaces externos

* **Código fuente utilizado** : [https://gitlab.com/Bkg2k/openbor/](https://gitlab.com/Bkg2k/openbor/)
* **Página focial de la Senile Team** : [https://www.senileteam.com/](https://www.senileteam.com/)
* **Comunidad oficial** : [http://www.chronocrash.com/forum/](http://www.chronocrash.com/forum/)