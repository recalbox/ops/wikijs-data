---
title: Libretro RFake08
description: 
published: true
date: 2024-07-24T20:53:39.232Z
tags: libretro, 9.2+, fake08
editor: markdown
dateCreated: 2024-07-24T20:53:39.232Z
---



## ![](/emulators/license.svg) License

This core is under [**MIT**](https://github.com/jtothebell/fake-08/blob/master/LICENSE.MD) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Features

| Feature | Supported |
| :---: | :---: |

## ![](/emulators/bios.svg) BIOS

>**No bios is required.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Supported extensions

Roms must have the extension:

* .p8
* .png
* .zip
* .7z

### Location

Put your roms like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 pico8
┃ ┃ ┃ ┃ ┣ 🗒 **game.png**

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator



## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/jtothebell/fake-08/](https://github.com/jtothebell/fake-08/)
* **Pico-8 forum**: [https://www.lexaloffle.com/bbs/?cat=7](https://www.lexaloffle.com/bbs/?cat=7)