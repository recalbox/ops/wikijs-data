---
title: Libretro FMSX
description: 
published: true
date: 2024-07-17T21:10:09.752Z
tags: libretro, msx1, fmsx
editor: markdown
dateCreated: 2023-09-11T09:28:54.448Z
---



## ![](/emulators/license.svg) Licencia

El core posee la siguiente licencia [**non commerciale**](https://github.com/libretro/fmsx-libretro/blob/master/LICENSE).

## ![](/emulators/compatibility.png) Compatibilidad

 | RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | 
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | 

## ![](/emulators/features.png) Funcionalidades

| Funcionalidad | ¿Soportada? |
| :---: | :---: |
| Reinicio | ✔ |
| Capturas de pantalla | ✔ |
| Grabaciones | ✔ |
| Grabaciones instantáneas | ✔ |
| Rebobinado | ✔ |
| Netplay | ✔ |
| Opciones del core | ✔ |
| RetroAchievements | ✔ |
| Controles | ✔ |
| Remapping | ✔ |
| Control de disco | ✔ |

## ![](/emulators/bios.svg) BIOS

### Lista de BIOS obligatorias

| Nombre del fichero | Descripción | MD5 | ¿Preinstalada? |
| :---: | :---: | :---: | :---: |
| CARTS.SHA | - | d6dedca1112ddfda94cc9b2e426b818b | ✅ |
| CYRILLIC.FNT | - | 85b38e4128bbc300e675f55b278683a8 | ✅ |
| DISK.ROM | DiskROM/BDOS | 80dcd1ad1a4cf65d64b7ba10504e8190 | ✅ |
| FMPAC16.ROM | - | af8537262df8df267072f359399a7635 | ✅ |
| FMPAC.ROM | FMPAC BIOS | 6f69cc8b5ed761b03afd78000dfb0e19 | ✅ |
| ITALIC.FNT | - | c83e50e9f33b8dd893c414691822740d | ✅ |
| KANJI.ROM | Kanji Font | febe8782b466d7c3b16de6d104826b34 | ✅ |
| MSX2EXT.ROM | MSX2 ExtROM | 2183c2aff17cf4297bdb496de78c2e8a | ✅ |
| MSX2PEXT.ROM | MSX2+ ExtROM | 7c8243c71d8f143b2531f01afa6a05dc | ✅ |
| MSX2P.ROM | MSX2+ BIOS | 6d8c0ca64e726c82a4b726e9b01cdf1e | ✅ |
| MSX2.ROM | BIOS MSX2 | ec3a01c91f24fbddcbcab0ad301bc9ef | ✅ |
| MSXDOS2.ROM | MSX-DOS 2 | 6418d091cd6907bbcf940324339e43bb | ✅ |
| MSX.ROM | BIOS MSX | aa95aea2563cd5ec0a0919b44cc17d47 | ✅ |
| PAINTER.ROM | Yamaha Painter | 403cdea1cbd2bb24fae506941f8f655e | ✅ |
| RS232.ROM | - | 279efd1eae0d358eecd4edc7d9adedf3 | ✅ |

### Emplazamiento

Disponed vuestras bios como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 msx
┃ ┃ ┃ ┃ ┣ 🗒 **CARTS.SHA**
┃ ┃ ┃ ┃ ┣ 🗒 **CYRILLIC.FNT**
┃ ┃ ┃ ┃ ┣ 🗒 **DISK.ROM**
┃ ┃ ┃ ┃ ┣ 🗒 **FMPAC16.ROM**
┃ ┃ ┃ ┃ ┣ 🗒 **FMPAC.ROM**
┃ ┃ ┃ ┃ ┣ 🗒 **ITALIC.FNT**
┃ ┃ ┃ ┃ ┣ 🗒 **KANJI.ROM**
┃ ┃ ┃ ┃ ┣ 🗒 **MSX2EXT.ROM**
┃ ┃ ┃ ┃ ┣ 🗒 **MSX2PEXT.ROM**
┃ ┃ ┃ ┃ ┣ 🗒 **MSX2P.ROM**
┃ ┃ ┃ ┃ ┣ 🗒 **MSX2.ROM**
┃ ┃ ┃ ┃ ┣ 🗒 **MSXDOS2.ROM**
┃ ┃ ┃ ┃ ┣ 🗒 **MSX.ROM**
┃ ┃ ┃ ┃ ┣ 🗒 **PAINTER.ROM**
┃ ┃ ┃ ┃ ┣ 🗒 **RS232.ROM**

## ![](/emulators/roms.png) Roms

### Extensiones de roms soportadas

Las roms deben tener las extensiones siguientes:

* .cas
* .dsk
* .mx1
* .rom
* .zip
* .7z

Este sistema soporta las roms comprimidas en formato .zip/.7z. No obstante, se trata sólo de un archivo.

Los ficheros contenidos en los .zip/.7z deben tener las extensiones citadas previamente.
Además, cada fichero .zip/.7z sólo debe contener un fichero rom al interior.

### Emplazamiento

Disponed vuestras roms como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 msx1
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.zip**

>Se aconsejan las roms del proyecto **No-Intro**.
{.is-success}

>Para más información sobre las roms, consultad [este tutorial](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

>Para conservar las configuraciones personalizadas cuando se produce una actualización del sistema Recalbox, os aconsejamos utilizar las [Sobrecargas de configuración](./../../../advanced-usage/configuration-override).
{.is-info}

### Acceder a las opciones

Podéis configurar las opciones del core de dos formas diferentes.

* Via el Menú RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via el fichero `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Opciones del core

| Opción | Valor de la opción | Variable (para la sobrecarga) | Valores posibles |
| --- | --- | --- | --- |
| MSX Mode | `MSX2+` ✅ / `MSX1` / `MSX2` | `fmsx_mode` | `MSX2+` / `MSX1` / `MSX2` |
| MSX Video Mode | `NTSC` ✅ / `PAL` | `fmsx_video_mode` | `NTSC` / `PAL` |
 | Support high resolution | `Off` ✅ / `Interlaced` / `Progressive` | `fmsx_hires` | `Off` / `Interlaced` / `Progressive` |
 | Support overscan | `No` ✅ / `Yes` | `fmsx_overscan` | `No` / `Yes` |
| MSX Mapper Type Mode | `Guess Mapper Type A` ✅ / `Guess Mapper Type B` | `fmsx_mapper_type_mode` | `Guess Mapper Type A` / `Guess Mapper Type B` |
| MSX Main Memory | `Auto` ✅ / `64KB` / `128KB` / `256KB` / `512KB` | `fmsx_ram_pages` | `Auto` / `64KB` / `128KB` / `256KB` / `512KB` |
| MSX Video Memory | `Auto` ✅ / `32KB` / `64KB` / `128KB` / `192KB` | `fmsx_vram_pages` | `Auto` / `32KB` / `64KB` / `128KB` / `192KB` |
 | Enable SCC-I 128kB MegaRAM | `Yes` ✅ / `No` | `fmsx_scci_megaram` | `Yes` / `No` |
 | fMSX logging | `Off` ✅ / `Info` / `Debug` / `Spam` | `fmsx_log_level` | `Off` / `Info` / `Debug` / `Spam` |
 | Support Game Master | `No` ✅ / `Yes` | `fmsx_game_master` | `No` / `Yes` |
 | Simulate DiskROM disk access calls | `No` ✅ / `Yes` | `fmsx_simbdos` | `No` / `Yes` |
 | Use autofire on SPACE | `No` ✅ / `Yes` | `fmsx_autospace` | `No` / `Yes` |
 | Show all sprites | `No` ✅ / `Yes` | `fmsx_allsprites` | `No` / `Yes` |
 | Text font | `standard` ✅ / `DEFAULT.FNT` / `ITALIC.FNT` / `INTERNAT.FNT` / `CYRILLIC.FNT` / `KOREAN.FNT` / `JAPANESE.FNT` | `fmsx_font` | `standard` / `DEFAULT.FNT` / `ITALIC.FNT` / `INTERNAT.FNT` / `CYRILLIC.FNT` / `KOREAN.FNT` / `JAPANESE.FNT` |
 | Save disk changes | `Never` ✅ / `Immediate` / `On close` / `To/From SRAM` | `fmsx_flush_disk` | `Never` / `Immediate` / `On close` / `To/From SRAM` |
 | Create empty disk when none loaded | `No` ✅ / `Yes` | `fmsx_phantom_disk` | `No` / `Yes` |
 | Load MSXDOS2.ROM when found | `No` ✅ / `Yes` | `fmsx_dos2` | `No` / `Yes` |
 | Custom keyboard RetroPad up | `escape` / `left` / `up` ✅ / `right` / `down` / `shift` / `ctrl` / `graph` / `backspace` / `tab` / `space` / `capslock` / `select` / `home` / `enter` / `del` / `insert` / `country` / `dead` / `stop` / `f1` / `f2` / `f3` / `f4` / `f5` / `keypad0` / `keypad1` / `keypad2` / `keypad3` / `keypad4` / `keypad5` / `keypad6` / `keypad7` / `keypad8` / `keypad9` / `kp_multiply` / `kp_plus` / `kp_divide` / `kp_minus` / `kp_comma` / `kp_period` / `backquote` / `minus` / `equals` / `leftbracket` / `rightbracket` / `backslash` / `semicolon` / `quote` / `comma` / `period` / `slash` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` | `fmsx_custom_keyboard_up` | `escape` / `left` / `up` / `right` / `down` / `shift` / `ctrl` / `graph` / `backspace` / `tab` / `space` / `capslock` / `select` / `home` / `enter` / `del` / `insert` / `country` / `dead` / `stop` / `f1` / `f2` / `f3` / `f4` / `f5` / `keypad0` / `keypad1` / `keypad2` / `keypad3` / `keypad4` / `keypad5` / `keypad6` / `keypad7` / `keypad8` / `keypad9` / `kp_multiply` / `kp_plus` / `kp_divide` / `kp_minus` / `kp_comma` / `kp_period` / `backquote` / `minus` / `equals` / `leftbracket` / `rightbracket` / `backslash` / `semicolon` / `quote` / `comma` / `period` / `slash` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` |
 | Custom keyboard RetroPad down | `escape` / `left` / `up` / `right` / `down` ✅ / `shift` / `ctrl` / `graph` / `backspace` / `tab` / `space` / `capslock` / `select` / `home` / `enter` / `del` / `insert` / `country` / `dead` / `stop` / `f1` / `f2` / `f3` / `f4` / `f5` / `keypad0` / `keypad1` / `keypad2` / `keypad3` / `keypad4` / `keypad5` / `keypad6` / `keypad7` / `keypad8` / `keypad9` / `kp_multiply` / `kp_plus` / `kp_divide` / `kp_minus` / `kp_comma` / `kp_period` / `backquote` / `minus` / `equals` / `leftbracket` / `rightbracket` / `backslash` / `semicolon` / `quote` / `comma` / `period` / `slash` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` | `fmsx_custom_keyboard_down` | `escape` / `left` / `up` / `right` / `down` / `shift` / `ctrl` / `graph` / `backspace` / `tab` / `space` / `capslock` / `select` / `home` / `enter` / `del` / `insert` / `country` / `dead` / `stop` / `f1` / `f2` / `f3` / `f4` / `f5` / `keypad0` / `keypad1` / `keypad2` / `keypad3` / `keypad4` / `keypad5` / `keypad6` / `keypad7` / `keypad8` / `keypad9` / `kp_multiply` / `kp_plus` / `kp_divide` / `kp_minus` / `kp_comma` / `kp_period` / `backquote` / `minus` / `equals` / `leftbracket` / `rightbracket` / `backslash` / `semicolon` / `quote` / `comma` / `period` / `slash` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` |
 | Custom keyboard RetroPad left | `escape` / `left` ✅ / `up` / `right` / `down` / `shift` / `ctrl` / `graph` / `backspace` / `tab` / `space` / `capslock` / `select` / `home` / `enter` / `del` / `insert` / `country` / `dead` / `stop` / `f1` / `f2` / `f3` / `f4` / `f5` / `keypad0` / `keypad1` / `keypad2` / `keypad3` / `keypad4` / `keypad5` / `keypad6` / `keypad7` / `keypad8` / `keypad9` / `kp_multiply` / `kp_plus` / `kp_divide` / `kp_minus` / `kp_comma` / `kp_period` / `backquote` / `minus` / `equals` / `leftbracket` / `rightbracket` / `backslash` / `semicolon` / `quote` / `comma` / `period` / `slash` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` | `fmsx_custom_keyboard_left` | `escape` / `left` / `up` / `right` / `down` / `shift` / `ctrl` / `graph` / `backspace` / `tab` / `space` / `capslock` / `select` / `home` / `enter` / `del` / `insert` / `country` / `dead` / `stop` / `f1` / `f2` / `f3` / `f4` / `f5` / `keypad0` / `keypad1` / `keypad2` / `keypad3` / `keypad4` / `keypad5` / `keypad6` / `keypad7` / `keypad8` / `keypad9` / `kp_multiply` / `kp_plus` / `kp_divide` / `kp_minus` / `kp_comma` / `kp_period` / `backquote` / `minus` / `equals` / `leftbracket` / `rightbracket` / `backslash` / `semicolon` / `quote` / `comma` / `period` / `slash` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` |
 | Custom keyboard RetroPad right | `escape` / `left` / `up` / `right` ✅ / `down` / `shift` / `ctrl` / `graph` / `backspace` / `tab` / `space` / `capslock` / `select` / `home` / `enter` / `del` / `insert` / `country` / `dead` / `stop` / `f1` / `f2` / `f3` / `f4` / `f5` / `keypad0` / `keypad1` / `keypad2` / `keypad3` / `keypad4` / `keypad5` / `keypad6` / `keypad7` / `keypad8` / `keypad9` / `kp_multiply` / `kp_plus` / `kp_divide` / `kp_minus` / `kp_comma` / `kp_period` / `backquote` / `minus` / `equals` / `leftbracket` / `rightbracket` / `backslash` / `semicolon` / `quote` / `comma` / `period` / `slash` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` | `fmsx_custom_keyboard_right` | `escape` / `left` / `up` / `right` / `down` / `shift` / `ctrl` / `graph` / `backspace` / `tab` / `space` / `capslock` / `select` / `home` / `enter` / `del` / `insert` / `country` / `dead` / `stop` / `f1` / `f2` / `f3` / `f4` / `f5` / `keypad0` / `keypad1` / `keypad2` / `keypad3` / `keypad4` / `keypad5` / `keypad6` / `keypad7` / `keypad8` / `keypad9` / `kp_multiply` / `kp_plus` / `kp_divide` / `kp_minus` / `kp_comma` / `kp_period` / `backquote` / `minus` / `equals` / `leftbracket` / `rightbracket` / `backslash` / `semicolon` / `quote` / `comma` / `period` / `slash` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` |
 | Custom keyboard RetroPad a | `escape` / `left` / `up` / `right` / `down` / `shift` / `ctrl` / `graph` / `backspace` / `tab` / `space` ✅ / `capslock` / `select` / `home` / `enter` / `del` / `insert` / `country` / `dead` / `stop` / `f1` / `f2` / `f3` / `f4` / `f5` / `keypad0` / `keypad1` / `keypad2` / `keypad3` / `keypad4` / `keypad5` / `keypad6` / `keypad7` / `keypad8` / `keypad9` / `kp_multiply` / `kp_plus` / `kp_divide` / `kp_minus` / `kp_comma` / `kp_period` / `backquote` / `minus` / `equals` / `leftbracket` / `rightbracket` / `backslash` / `semicolon` / `quote` / `comma` / `period` / `slash` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` | `fmsx_custom_keyboard_a` | `escape` / `left` / `up` / `right` / `down` / `shift` / `ctrl` / `graph` / `backspace` / `tab` / `space` / `capslock` / `select` / `home` / `enter` / `del` / `insert` / `country` / `dead` / `stop` / `f1` / `f2` / `f3` / `f4` / `f5` / `keypad0` / `keypad1` / `keypad2` / `keypad3` / `keypad4` / `keypad5` / `keypad6` / `keypad7` / `keypad8` / `keypad9` / `kp_multiply` / `kp_plus` / `kp_divide` / `kp_minus` / `kp_comma` / `kp_period` / `backquote` / `minus` / `equals` / `leftbracket` / `rightbracket` / `backslash` / `semicolon` / `quote` / `comma` / `period` / `slash` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` |
 | Custom keyboard RetroPad b | `escape` / `left` / `up` / `right` / `down` / `shift` / `ctrl` / `graph` / `backspace` / `tab` / `space` / `capslock` / `select` / `home` / `enter` ✅ / `del` / `insert` / `country` / `dead` / `stop` / `f1` / `f2` / `f3` / `f4` / `f5` / `keypad0` / `keypad1` / `keypad2` / `keypad3` / `keypad4` / `keypad5` / `keypad6` / `keypad7` / `keypad8` / `keypad9` / `kp_multiply` / `kp_plus` / `kp_divide` / `kp_minus` / `kp_comma` / `kp_period` / `backquote` / `minus` / `equals` / `leftbracket` / `rightbracket` / `backslash` / `semicolon` / `quote` / `comma` / `period` / `slash` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` | `fmsx_custom_keyboard_b` | `escape` / `left` / `up` / `right` / `down` / `shift` / `ctrl` / `graph` / `backspace` / `tab` / `space` / `capslock` / `select` / `home` / `enter` / `del` / `insert` / `country` / `dead` / `stop` / `f1` / `f2` / `f3` / `f4` / `f5` / `keypad0` / `keypad1` / `keypad2` / `keypad3` / `keypad4` / `keypad5` / `keypad6` / `keypad7` / `keypad8` / `keypad9` / `kp_multiply` / `kp_plus` / `kp_divide` / `kp_minus` / `kp_comma` / `kp_period` / `backquote` / `minus` / `equals` / `leftbracket` / `rightbracket` / `backslash` / `semicolon` / `quote` / `comma` / `period` / `slash` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` |
 | Custom keyboard RetroPad y | `escape` / `left` / `up` / `right` / `down` / `shift` / `ctrl` / `graph` / `backspace` / `tab` / `space` / `capslock` / `select` / `home` / `enter` / `del` / `insert` / `country` / `dead` / `stop` / `f1` / `f2` / `f3` / `f4` / `f5` / `keypad0` / `keypad1` / `keypad2` / `keypad3` / `keypad4` / `keypad5` / `keypad6` / `keypad7` / `keypad8` / `keypad9` / `kp_multiply` / `kp_plus` / `kp_divide` / `kp_minus` / `kp_comma` / `kp_period` / `backquote` / `minus` / `equals` / `leftbracket` / `rightbracket` / `backslash` / `semicolon` / `quote` / `comma` / `period` / `slash` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` ✅ / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` | `fmsx_custom_keyboard_y` | `escape` / `left` / `up` / `right` / `down` / `shift` / `ctrl` / `graph` / `backspace` / `tab` / `space` / `capslock` / `select` / `home` / `enter` / `del` / `insert` / `country` / `dead` / `stop` / `f1` / `f2` / `f3` / `f4` / `f5` / `keypad0` / `keypad1` / `keypad2` / `keypad3` / `keypad4` / `keypad5` / `keypad6` / `keypad7` / `keypad8` / `keypad9` / `kp_multiply` / `kp_plus` / `kp_divide` / `kp_minus` / `kp_comma` / `kp_period` / `backquote` / `minus` / `equals` / `leftbracket` / `rightbracket` / `backslash` / `semicolon` / `quote` / `comma` / `period` / `slash` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` |
 | Custom keyboard RetroPad x | `escape` / `left` / `up` / `right` / `down` / `shift` / `ctrl` / `graph` / `backspace` / `tab` / `space` / `capslock` / `select` / `home` / `enter` / `del` / `insert` / `country` / `dead` / `stop` / `f1` / `f2` / `f3` / `f4` / `f5` / `keypad0` / `keypad1` / `keypad2` / `keypad3` / `keypad4` / `keypad5` / `keypad6` / `keypad7` / `keypad8` / `keypad9` / `kp_multiply` / `kp_plus` / `kp_divide` / `kp_minus` / `kp_comma` / `kp_period` / `backquote` / `minus` / `equals` / `leftbracket` / `rightbracket` / `backslash` / `semicolon` / `quote` / `comma` / `period` / `slash` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` ✅ / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` | `fmsx_custom_keyboard_x` | `escape` / `left` / `up` / `right` / `down` / `shift` / `ctrl` / `graph` / `backspace` / `tab` / `space` / `capslock` / `select` / `home` / `enter` / `del` / `insert` / `country` / `dead` / `stop` / `f1` / `f2` / `f3` / `f4` / `f5` / `keypad0` / `keypad1` / `keypad2` / `keypad3` / `keypad4` / `keypad5` / `keypad6` / `keypad7` / `keypad8` / `keypad9` / `kp_multiply` / `kp_plus` / `kp_divide` / `kp_minus` / `kp_comma` / `kp_period` / `backquote` / `minus` / `equals` / `leftbracket` / `rightbracket` / `backslash` / `semicolon` / `quote` / `comma` / `period` / `slash` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` |
 | Custom keyboard RetroPad start | `escape` / `left` / `up` / `right` / `down` / `shift` / `ctrl` / `graph` / `backspace` / `tab` / `space` / `capslock` / `select` / `home` / `enter` / `del` / `insert` / `country` / `dead` / `stop` / `f1` ✅ / `f2` / `f3` / `f4` / `f5` / `keypad0` / `keypad1` / `keypad2` / `keypad3` / `keypad4` / `keypad5` / `keypad6` / `keypad7` / `keypad8` / `keypad9` / `kp_multiply` / `kp_plus` / `kp_divide` / `kp_minus` / `kp_comma` / `kp_period` / `backquote` / `minus` / `equals` / `leftbracket` / `rightbracket` / `backslash` / `semicolon` / `quote` / `comma` / `period` / `slash` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` | `fmsx_custom_keyboard_start` | `escape` / `left` / `up` / `right` / `down` / `shift` / `ctrl` / `graph` / `backspace` / `tab` / `space` / `capslock` / `select` / `home` / `enter` / `del` / `insert` / `country` / `dead` / `stop` / `f1` / `f2` / `f3` / `f4` / `f5` / `keypad0` / `keypad1` / `keypad2` / `keypad3` / `keypad4` / `keypad5` / `keypad6` / `keypad7` / `keypad8` / `keypad9` / `kp_multiply` / `kp_plus` / `kp_divide` / `kp_minus` / `kp_comma` / `kp_period` / `backquote` / `minus` / `equals` / `leftbracket` / `rightbracket` / `backslash` / `semicolon` / `quote` / `comma` / `period` / `slash` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` |
 | Custom keyboard RetroPad select | `escape` / `left` / `up` / `right` / `down` / `shift` / `ctrl` / `graph` / `backspace` / `tab` / `space` / `capslock` / `select` / `home` / `enter` / `del` / `insert` / `country` / `dead` / `stop` / `f1` / `f2` / `f3` / `f4` ✅ / `f5` / `keypad0` / `keypad1` / `keypad2` / `keypad3` / `keypad4` / `keypad5` / `keypad6` / `keypad7` / `keypad8` / `keypad9` / `kp_multiply` / `kp_plus` / `kp_divide` / `kp_minus` / `kp_comma` / `kp_period` / `backquote` / `minus` / `equals` / `leftbracket` / `rightbracket` / `backslash` / `semicolon` / `quote` / `comma` / `period` / `slash` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` | `fmsx_custom_keyboard_select` | `escape` / `left` / `up` / `right` / `down` / `shift` / `ctrl` / `graph` / `backspace` / `tab` / `space` / `capslock` / `select` / `home` / `enter` / `del` / `insert` / `country` / `dead` / `stop` / `f1` / `f2` / `f3` / `f4` / `f5` / `keypad0` / `keypad1` / `keypad2` / `keypad3` / `keypad4` / `keypad5` / `keypad6` / `keypad7` / `keypad8` / `keypad9` / `kp_multiply` / `kp_plus` / `kp_divide` / `kp_minus` / `kp_comma` / `kp_period` / `backquote` / `minus` / `equals` / `leftbracket` / `rightbracket` / `backslash` / `semicolon` / `quote` / `comma` / `period` / `slash` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` |
 | Custom keyboard RetroPad l | `escape` / `left` / `up` / `right` / `down` / `shift` / `ctrl` / `graph` / `backspace` / `tab` / `space` / `capslock` / `select` / `home` / `enter` / `del` / `insert` / `country` / `dead` / `stop` / `f1` / `f2` ✅ / `f3` / `f4` / `f5` / `keypad0` / `keypad1` / `keypad2` / `keypad3` / `keypad4` / `keypad5` / `keypad6` / `keypad7` / `keypad8` / `keypad9` / `kp_multiply` / `kp_plus` / `kp_divide` / `kp_minus` / `kp_comma` / `kp_period` / `backquote` / `minus` / `equals` / `leftbracket` / `rightbracket` / `backslash` / `semicolon` / `quote` / `comma` / `period` / `slash` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` | `fmsx_custom_keyboard_l` | `escape` / `left` / `up` / `right` / `down` / `shift` / `ctrl` / `graph` / `backspace` / `tab` / `space` / `capslock` / `select` / `home` / `enter` / `del` / `insert` / `country` / `dead` / `stop` / `f1` / `f2` / `f3` / `f4` / `f5` / `keypad0` / `keypad1` / `keypad2` / `keypad3` / `keypad4` / `keypad5` / `keypad6` / `keypad7` / `keypad8` / `keypad9` / `kp_multiply` / `kp_plus` / `kp_divide` / `kp_minus` / `kp_comma` / `kp_period` / `backquote` / `minus` / `equals` / `leftbracket` / `rightbracket` / `backslash` / `semicolon` / `quote` / `comma` / `period` / `slash` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` |
 | Custom keyboard RetroPad r | `escape` / `left` / `up` / `right` / `down` / `shift` / `ctrl` / `graph` / `backspace` / `tab` / `space` / `capslock` / `select` / `home` / `enter` / `del` / `insert` / `country` / `dead` / `stop` / `f1` / `f2` / `f3` ✅ / `f4` / `f5` / `keypad0` / `keypad1` / `keypad2` / `keypad3` / `keypad4` / `keypad5` / `keypad6` / `keypad7` / `keypad8` / `keypad9` / `kp_multiply` / `kp_plus` / `kp_divide` / `kp_minus` / `kp_comma` / `kp_period` / `backquote` / `minus` / `equals` / `leftbracket` / `rightbracket` / `backslash` / `semicolon` / `quote` / `comma` / `period` / `slash` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` | `fmsx_custom_keyboard_r` | `escape` / `left` / `up` / `right` / `down` / `shift` / `ctrl` / `graph` / `backspace` / `tab` / `space` / `capslock` / `select` / `home` / `enter` / `del` / `insert` / `country` / `dead` / `stop` / `f1` / `f2` / `f3` / `f4` / `f5` / `keypad0` / `keypad1` / `keypad2` / `keypad3` / `keypad4` / `keypad5` / `keypad6` / `keypad7` / `keypad8` / `keypad9` / `kp_multiply` / `kp_plus` / `kp_divide` / `kp_minus` / `kp_comma` / `kp_period` / `backquote` / `minus` / `equals` / `leftbracket` / `rightbracket` / `backslash` / `semicolon` / `quote` / `comma` / `period` / `slash` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` |
 | Custom keyboard RetroPad l2 | `escape` / `left` / `up` / `right` / `down` / `shift` / `ctrl` / `graph` ✅ / `backspace` / `tab` / `space` / `capslock` / `select` / `home` / `enter` / `del` / `insert` / `country` / `dead` / `stop` / `f1` / `f2` / `f3` / `f4` / `f5` / `keypad0` / `keypad1` / `keypad2` / `keypad3` / `keypad4` / `keypad5` / `keypad6` / `keypad7` / `keypad8` / `keypad9` / `kp_multiply` / `kp_plus` / `kp_divide` / `kp_minus` / `kp_comma` / `kp_period` / `backquote` / `minus` / `equals` / `leftbracket` / `rightbracket` / `backslash` / `semicolon` / `quote` / `comma` / `period` / `slash` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` | `fmsx_custom_keyboard_l2` | `escape` / `left` / `up` / `right` / `down` / `shift` / `ctrl` / `graph` / `backspace` / `tab` / `space` / `capslock` / `select` / `home` / `enter` / `del` / `insert` / `country` / `dead` / `stop` / `f1` / `f2` / `f3` / `f4` / `f5` / `keypad0` / `keypad1` / `keypad2` / `keypad3` / `keypad4` / `keypad5` / `keypad6` / `keypad7` / `keypad8` / `keypad9` / `kp_multiply` / `kp_plus` / `kp_divide` / `kp_minus` / `kp_comma` / `kp_period` / `backquote` / `minus` / `equals` / `leftbracket` / `rightbracket` / `backslash` / `semicolon` / `quote` / `comma` / `period` / `slash` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` |
 | Custom keyboard RetroPad r2 | `escape` / `left` / `up` / `right` / `down` / `shift` / `ctrl` ✅ / `graph` / `backspace` / `tab` / `space` / `capslock` / `select` / `home` / `enter` / `del` / `insert` / `country` / `dead` / `stop` / `f1` / `f2` / `f3` / `f4` / `f5` / `keypad0` / `keypad1` / `keypad2` / `keypad3` / `keypad4` / `keypad5` / `keypad6` / `keypad7` / `keypad8` / `keypad9` / `kp_multiply` / `kp_plus` / `kp_divide` / `kp_minus` / `kp_comma` / `kp_period` / `backquote` / `minus` / `equals` / `leftbracket` / `rightbracket` / `backslash` / `semicolon` / `quote` / `comma` / `period` / `slash` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` | `fmsx_custom_keyboard_r2` | `escape` / `left` / `up` / `right` / `down` / `shift` / `ctrl` / `graph` / `backspace` / `tab` / `space` / `capslock` / `select` / `home` / `enter` / `del` / `insert` / `country` / `dead` / `stop` / `f1` / `f2` / `f3` / `f4` / `f5` / `keypad0` / `keypad1` / `keypad2` / `keypad3` / `keypad4` / `keypad5` / `keypad6` / `keypad7` / `keypad8` / `keypad9` / `kp_multiply` / `kp_plus` / `kp_divide` / `kp_minus` / `kp_comma` / `kp_period` / `backquote` / `minus` / `equals` / `leftbracket` / `rightbracket` / `backslash` / `semicolon` / `quote` / `comma` / `period` / `slash` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` |
 | Custom keyboard RetroPad l3 | `escape` / `left` / `up` / `right` / `down` / `shift` / `ctrl` / `graph` / `backspace` / `tab` / `space` / `capslock` / `select` / `home` / `enter` / `del` / `insert` / `country` / `dead` / `stop` / `f1` / `f2` / `f3` / `f4` / `f5` ✅ / `keypad0` / `keypad1` / `keypad2` / `keypad3` / `keypad4` / `keypad5` / `keypad6` / `keypad7` / `keypad8` / `keypad9` / `kp_multiply` / `kp_plus` / `kp_divide` / `kp_minus` / `kp_comma` / `kp_period` / `backquote` / `minus` / `equals` / `leftbracket` / `rightbracket` / `backslash` / `semicolon` / `quote` / `comma` / `period` / `slash` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` | `fmsx_custom_keyboard_l3` | `escape` / `left` / `up` / `right` / `down` / `shift` / `ctrl` / `graph` / `backspace` / `tab` / `space` / `capslock` / `select` / `home` / `enter` / `del` / `insert` / `country` / `dead` / `stop` / `f1` / `f2` / `f3` / `f4` / `f5` / `keypad0` / `keypad1` / `keypad2` / `keypad3` / `keypad4` / `keypad5` / `keypad6` / `keypad7` / `keypad8` / `keypad9` / `kp_multiply` / `kp_plus` / `kp_divide` / `kp_minus` / `kp_comma` / `kp_period` / `backquote` / `minus` / `equals` / `leftbracket` / `rightbracket` / `backslash` / `semicolon` / `quote` / `comma` / `period` / `slash` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` |
 | Custom keyboard RetroPad r3 | `escape` ✅ / `left` / `up` / `right` / `down` / `shift` / `ctrl` / `graph` / `backspace` / `tab` / `space` / `capslock` / `select` / `home` / `enter` / `del` / `insert` / `country` / `dead` / `stop` / `f1` / `f2` / `f3` / `f4` / `f5` / `keypad0` / `keypad1` / `keypad2` / `keypad3` / `keypad4` / `keypad5` / `keypad6` / `keypad7` / `keypad8` / `keypad9` / `kp_multiply` / `kp_plus` / `kp_divide` / `kp_minus` / `kp_comma` / `kp_period` / `backquote` / `minus` / `equals` / `leftbracket` / `rightbracket` / `backslash` / `semicolon` / `quote` / `comma` / `period` / `slash` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` | `fmsx_custom_keyboard_r3` | `escape` / `left` / `up` / `right` / `down` / `shift` / `ctrl` / `graph` / `backspace` / `tab` / `space` / `capslock` / `select` / `home` / `enter` / `del` / `insert` / `country` / `dead` / `stop` / `f1` / `f2` / `f3` / `f4` / `f5` / `keypad0` / `keypad1` / `keypad2` / `keypad3` / `keypad4` / `keypad5` / `keypad6` / `keypad7` / `keypad8` / `keypad9` / `kp_multiply` / `kp_plus` / `kp_divide` / `kp_minus` / `kp_comma` / `kp_period` / `backquote` / `minus` / `equals` / `leftbracket` / `rightbracket` / `backslash` / `semicolon` / `quote` / `comma` / `period` / `slash` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` |


## ![](/emulators/external-links.png) Enlaces externos

* **Código fuente utilizado** : [https://github.com/libretro/fmsx-libretro/](https://github.com/libretro/fmsx-libretro/)
* **Documentación Libretro** : [https://docs.libretro.com/library/fmsx/](https://docs.libretro.com/library/fmsx/)