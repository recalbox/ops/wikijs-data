---
title: Utilización de un mando SNES
description: 
published: true
date: 2023-09-23T19:31:08.475Z
tags: switch, nintendo, snes, 7.2+, mando
editor: markdown
dateCreated: 2023-09-23T19:31:08.475Z
---

## Sincronización

En cada mando hay un pequeño botón para encenderlo y apagarlo, pero este botón también sirve para sincronizarlo con vuestra Recalbox.

![Bouton de synchronisation](/tutorials/controllers/controllers/switch-joycons/snesconfig1.jpg)

En Recalbox, en la lista de sistemas disponibles, pulsad el botón **Start** de vuestro mando e id a **CONFIGURACIÓN DE MANDOS**.

![](/tutorials/controllers/controllers/switch-joycons/snesconfig2.png){.full-width}

En este menú, pulsad **ASOCIAR UN MANDO BLUETOOTH**.

![](/tutorials/controllers/controllers/switch-joycons/snesconfig3.png){.full-width}

Una vez iniciada la búsqueda de dispositivos Bluetooth, pulsad el botón de sincronización del mando SNES. El mando debería tener las 4 luces a su lado moviéndose de izquierda a derecha y viceversa.

![](/tutorials/controllers/controllers/switch-joycons/snesconfig4.png){.full-width}

Una vez finalizada la búsqueda de dispositivos Bluetooth, deberías ver vuestro mando SNES en la lista.

![](/tutorials/controllers/controllers/switch-joycons/snesconfig5.png){.full-width}

Seleccionad el mando y confirmad. El mando SNES debería ser reconocido y las 4 luces deberían seguir parpadeando.

Una vez completada la asociación, el mando SNES aparecerá en la lista de mandos.

![](/tutorials/controllers/controllers/switch-joycons/snesconfig6.png){.full-width}

A vosotros os queda disfrutad jugando de vuestro mando SNES !

## Configuración del mando SNES

Una vez asociado, el mando tendrá una configuración automática predefinida con botón **HOTKEY** en el botón **SELECT**.