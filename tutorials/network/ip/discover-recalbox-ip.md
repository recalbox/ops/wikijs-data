---
title: Finding the Recalbox IP address
description: All tips and tricks to find the IP address of your Recalbox
published: true
date: 2023-07-19T20:43:54.748Z
tags: network, address, ip, admin
editor: markdown
dateCreated: 2021-08-06T08:34:57.598Z
---

## Checking the network connection status

First of all make sure your Recalbox is connected to the network with an ethernet cable or USB wifi adaptor. 

You think Recalbox is connected on the network but you are not sure, let's see how checking this easily! 


Press `Start`, choose `UPDATES` and `LAUNCH UPDATES`:
- If you see a message like `An update is available`: Recalbox is well connected to the network. 
- If you see `Please connect a network cable`: Recalbox is not connected to your network and there is an issue (network misconfiguration, defective cable, etc.).

>Usually IP address are not fixed and change at regular time or after every reboot. 
>You can define a static IP address for easy access to your Recalbox. 
>You should do this configuration on your router but you can [set network parameters of your Recalbox](./../../../tutorials/network/ip/static-manual-ip) also.
{.is-warning}

## From your Recalbox 

Press `START` and go to `NETWORK SETTINGS`. The current IP address is displayed here like `192.168.0.XX`. 

## From your computer

## {.tabset}

## Using Windows

* In the taskbar, go to the search box, put `cmd` and press `Enter`.
* A command prompt is displayed, put `ping recalbox` and press `Enter` again. 
* You get the following result with a mention of your Recalbox IP address like `192.168.0.XX`:

```shell
> Sending a ping request to RECALBOX with 32 bytes of data:  
> Response from 192.168.0.XX : bytes=32 time=1 ms TTL=128  
> Answer from 192.168.0.XX : bytes=32 time=1 ms TTL=128  
> Response from 192.168.0.XX : bytes=32 time=1 ms TTL=128  
> Ping statistics for [...]
```



### Using macOS

* Go to `Applications/Utilities/Terminal`.
* terminal is displayed, put `arp -a` and press `Enter`.
* The Recalbox IP address (something like 192.168.0.XX) is here in the list of connected devices. 


## Other ways to get the IP address 

* There are many mobile applications designed to scan your network and discover connect devices. Go to our app store and try one of them.
* Go to the admin application of your router device to see the list of connected devices to your network. 
 