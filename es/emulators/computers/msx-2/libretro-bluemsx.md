---
title: Libretro blueMSX
description: 
published: true
date: 2024-07-17T21:15:26.579Z
tags: libretro, bluemsx, msx2, ordenadores
editor: markdown
dateCreated: 2023-09-11T10:00:51.028Z
---

## ![](/emulators/license.svg) Licencia

El core posee la siguiente licencia [**GPLv2**](https://github.com/libretro/blueMSX-libretro/blob/master/license.txt).

## ![](/emulators/compatibility.png) Compatibilidad

 | RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | 
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | 

## ![](/emulators/features.png) Funcionalidades

| Funcionalidad | ¿Soportada? |
| :---: | :---: |
| Reinicio | ✔ |
| Capturas de pantalla | ✔ |
| Opciones del core | ✔ |
| RetroAchievements | ✔ |
| Controles | ✔ |
| Remapping | ✔ |
| Control de disco | ✔ |

## ![](/emulators/bios.svg) BIOS

### Lista de BIOS obligatorias

| Nombre del fichero | Descripción | MD5 | ¿Preinstalada? |
| :---: | :---: | :---: | :---: |
| pesadelo.rom | Forte II Games - Pesadelo | ba53d700ec25bcb08020ce693319cd28 | ✅ |
| cbios_logo_msx2.rom | MSX2 - C-BIOS | 659db9142e62c51f2f75fd52fac78e44 | ✅ |
| cbios_main_msx2.rom | MSX2 - C-BIOS | 98b94c2d99066f272bb5de07778f7fd1 | ✅ |
| cbios_sub.rom | MSX2 - C-BIOS | 9dd9a69432cc116d0f1d4ec7a51053e0 | ✅ |
| cbios_logo_msx2+.rom | MSX2+ - C-BIOS | 462d6ecb52a8c35dd7d48c2d46687f14 | ✅ |
| cbios_main_msx2+.rom | MSX2+ - C-BIOS | 0a34c660614ead31dab78638ce7745e9 | ✅ |
| cbios_music.rom | MSX2+ - C-BIOS | e09783c4ec6d4770c5395c42bb0e1d91 | ✅ |
| cbios_sub.rom | MSX2+ - C-BIOS | 9dd9a69432cc116d0f1d4ec7a51053e0 | ✅ |
| KANJI.rom | MSX2+ - Européen | acf53887c2d2783dc059a9b442c86b90 | ✅ |
| MSX2PMUS.rom | MSX2+ - Européen | f877f32e6d8687474ac5ee83e40de23b | ✅ |
| MSXKANJI.rom | MSX2+ - Européen | 9dfdebfaa6b547222a40aab8bb2e29f8 | ✅ |
| Msx2pe.rom | MSX2+ - Européen | 277fd57c13f301fb4b366845a32d673a | ✅ |
| Msx2pexte.rom | MSX2+ - Européen | f78ab735aaad7045badbda8f4c8ac751 | ✅ |
| PANASONICDISK.ROM | MSX2+ - Européen | 00aa02b6077de40a0b51d71a3c3e1d5f | ✅ |
| XBASIC2.rom | MSX2+ - Européen | 0b6120f289336538bc564548109f97c6 | ✅ |
| sf7000.rom | SEGA - SF7000 | 561b362f9aa4cbcb240461abf83c2e0d | ✅ |
| ARAB1.ROM | - | 7fa0558bcec9dba310579521623d9f6a | ✅ |
| ARABIC.rom | - | 20c4b5d1d9469c201b145f082ec32658 | ✅ |
| BEERIDE.ROM | - | b32279f2700652a5ce0a809318626763 | ✅ |
| FMPAC.rom | - | 6f69cc8b5ed761b03afd78000dfb0e19 | ✅ |
| GCVMX80.ROM | - | fd0b8f1766dc6dcc03a43d9c79dc4e37 | ✅ |
| HANGUL.rom | - | 9baf17b190f631405b6b0eeeeb162b87 | ✅ |
| KANJI.rom | - | acf53887c2d2783dc059a9b442c86b90 | ✅ |
| MICROSOLDISK.ROM | - | 02e8b1f96049f09d6d3a735647cbbb2a | ✅ |
| MOONSOUND.rom | - | 42af93619160ef2116416f74a6cb12f2 | ✅ |
| MSX2.rom | - | ec3a01c91f24fbddcbcab0ad301bc9ef | ✅ |
| MSX2AR.ROM | - | 91764e84f977671ac7caa50b36273fd2 | ✅ |
| MSX2AREXT.ROM | - | be5d1ec8001d945ca1432285722e9d16 | ✅ |
| MSX2BR.rom | - | d3df424728a225b301510f5384cae583 | ✅ |
| MSX2BREXT.rom | - | 355c6d5ee31da441d485cd89ca906413 | ✅ |
| MSX2EXT.rom | - | 2183c2aff17cf4297bdb496de78c2e8a | ✅ |
| MSX2FR.rom | - | 7af9e84da1520a8a7fb82e73703e5075 | ✅ |
| MSX2FREXT.rom | - | 6ffe789cca2eca9d2e71ca7b69b97e6b | ✅ |
| MSX2G.rom | - | b6c33b7b3508d691d7872589c778f808 | ✅ |
| MSX2GEXT.rom | - | 4047ce143a8bf391e21991835aa65b59 | ✅ |
| MSX2HAN.rom | - | 7b9466546009d419ebd0dc27db90c30e | ✅ |
| MSX2J.rom | - | 53bec1c22b30c0a15263e04b91a7814f | ✅ |
| MSX2JEXT.rom | - | 8aabde714a42256bef36ea9b04f6ef59 | ✅ |
| MSX2KR.rom | - | 6819a1533502261bfdd52436b8346073 | ✅ |
| MSX2KREXT.rom | - | 30a0b3402a0a13b516af76d39f45a365 | ✅ |
| MSX2P.rom | - | 847cc025ffae665487940ff2639540e5 | ✅ |
| MSX2PEXTR.rom | - | 7c8243c71d8f143b2531f01afa6a05dc | ✅ |
| MSX2PMUS.rom | - | f877f32e6d8687474ac5ee83e40de23b | ✅ |
| MSX2R.rom | - | db56360c85ef8410dc4bcf8e8602f61e | ✅ |
| MSX2R2.ROM | - | 96ac231b718e88ce64d5a9b4a5e9ae12 | ✅ |
| MSX2REXT.rom | - | 8e4af1124e2b82fbdc74a7bc5f20dc6e | ✅ |
| MSX2SE.rom | - | fb8f7ab15e32a91147204bd7f7a1b451 | ✅ |
| MSX2SP.rom | - | 79caefa20a0d056fb42eaeca856c6f82 | ✅ |
| MSX2SPEXT.rom | - | d71842ce8f559a8aed2721766bc15f2b | ✅ |
| NATIONALDISK.rom | - | 86269da485e852d9f581ac27f4ba32ff | ✅ |
| NOVAXIS.rom | - | 20989124671593ab04eeb01d52a1e25c | ✅ |
| PAINT.rom | - | 41db0d7b37be479296ffd59fcd6775f0 | ✅ |
| PANASONICDISK.rom | - | 00aa02b6077de40a0b51d71a3c3e1d5f | ✅ |
| PHILIPSDISK.rom | - | 01dd1d72ed9bb6afe8a9b441c198a1cd | ✅ |
| RS232.ROM | - | 55cca200bbbdd0a1ec5c9d70966c05c0 | ✅ |
| SUNRISEIDE.rom | - | 7b564497beb2c0de0847f107dec00ddf | ✅ |
| SWP.rom | - | 490db04e2a3c1c993a2a9d3611949c76 | ✅ |
| XBASIC2.rom | - | 0b6120f289336538bc564548109f97c6 | ✅ |
| nowindDos1.rom | - | 8c294a75f14fc30fce0c8642d28ccec1 | ✅ |
| nowindDos2.rom | - | 1847398c014384b833868c64f149f338 | ✅ |

### Emplazamiento

Disponed vuestras bios como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 Machines
┃ ┃ ┃ ┃ ┣ 📁 Forte II Games - Pesadelo
┃ ┃ ┃ ┃ ┃ ┣ 🗒 pesadelo.rom
┃ ┃ ┃ ┃ ┣ 📁 MSX2 - C-BIOS
┃ ┃ ┃ ┃ ┃ ┣ 🗒 cbios_logo_msx2.rom
┃ ┃ ┃ ┃ ┃ ┣ 🗒 cbios_main_msx2.rom
┃ ┃ ┃ ┃ ┃ ┣ 🗒 cbios_sub.rom
┃ ┃ ┃ ┃ ┃ ┣ 🗒 cbios_logo_msx2+.rom
┃ ┃ ┃ ┃ ┃ ┣ 🗒 cbios_main_msx2+.rom
┃ ┃ ┃ ┃ ┃ ┣ 🗒 cbios_music.rom
┃ ┃ ┃ ┃ ┃ ┣ 🗒 cbios_sub.rom
┃ ┃ ┃ ┃ ┣ 📁 MSX2+ - European
┃ ┃ ┃ ┃ ┃ ┣ 🗒 KANJI.rom
┃ ┃ ┃ ┃ ┃ ┣ 🗒 MSX2PMUS.rom
┃ ┃ ┃ ┃ ┃ ┣ 🗒 MSXKANJI.rom
┃ ┃ ┃ ┃ ┃ ┣ 🗒 Msx2pe.rom
┃ ┃ ┃ ┃ ┃ ┣ 🗒 Msx2pexte.rom
┃ ┃ ┃ ┃ ┃ ┣ 🗒 PANASONICDISK.rom
┃ ┃ ┃ ┃ ┃ ┣ 🗒 XBASIC2.rom
┃ ┃ ┃ ┃ ┣ 📁 SEGA - SF7000
┃ ┃ ┃ ┃ ┃ ┣ 🗒 sf7000.rom
┃ ┃ ┃ ┃ ┣ 📁 Shared Roms
┃ ┃ ┃ ┃ ┃ ┣ 🗒 ARAB1.ROM
┃ ┃ ┃ ┃ ┃ ┣ 🗒 ARABIC.rom
┃ ┃ ┃ ┃ ┃ ┣ 🗒 BEERIDE.ROM
┃ ┃ ┃ ┃ ┃ ┣ 🗒 FMPAC.rom
┃ ┃ ┃ ┃ ┃ ┣ 🗒 GCVMX80.ROM
┃ ┃ ┃ ┃ ┃ ┣ 🗒 HANGUL.rom
┃ ┃ ┃ ┃ ┃ ┣ 🗒 KANJI.rom
┃ ┃ ┃ ┃ ┃ ┣ 🗒 MICROSOLDISK.ROM
┃ ┃ ┃ ┃ ┃ ┣ 🗒 MOONSOUND.rom
┃ ┃ ┃ ┃ ┃ ┣ 🗒 MSX2.rom
┃ ┃ ┃ ┃ ┃ ┣ 🗒 MSX2AR.ROM
┃ ┃ ┃ ┃ ┃ ┣ 🗒 MSX2AREXT.ROM
┃ ┃ ┃ ┃ ┃ ┣ 🗒 MSX2BR.rom
┃ ┃ ┃ ┃ ┃ ┣ 🗒 MSX2BREXT.rom
┃ ┃ ┃ ┃ ┃ ┣ 🗒 MSX2EXT.rom
┃ ┃ ┃ ┃ ┃ ┣ 🗒 MSX2FR.rom
┃ ┃ ┃ ┃ ┃ ┣ 🗒 MSX2FREXT.rom
┃ ┃ ┃ ┃ ┃ ┣ 🗒 MSX2G.rom
┃ ┃ ┃ ┃ ┃ ┣ 🗒 MSX2GEXT.rom
┃ ┃ ┃ ┃ ┃ ┣ 🗒 MSX2J.rom
┃ ┃ ┃ ┃ ┃ ┣ 🗒 MSX2JEXT.rom
┃ ┃ ┃ ┃ ┃ ┣ 🗒 MSX2KR.rom
┃ ┃ ┃ ┃ ┃ ┣ 🗒 MSX2KREXT.rom
┃ ┃ ┃ ┃ ┃ ┣ 🗒 MSX2P.rom
┃ ┃ ┃ ┃ ┃ ┣ 🗒 MSX2PEXT.rom
┃ ┃ ┃ ┃ ┃ ┣ 🗒 MSX2PMUS.rom
┃ ┃ ┃ ┃ ┃ ┣ 🗒 MSX2R.rom
┃ ┃ ┃ ┃ ┃ ┣ 🗒 MSX2R2.ROM
┃ ┃ ┃ ┃ ┃ ┣ 🗒 MSX2REXT.rom
┃ ┃ ┃ ┃ ┃ ┣ 🗒 MSX2SE.rom
┃ ┃ ┃ ┃ ┃ ┣ 🗒 MSX2SP.rom
┃ ┃ ┃ ┃ ┃ ┣ 🗒 MSX2SPEXT.rom
┃ ┃ ┃ ┃ ┃ ┣ 🗒 NATIONALDISK.rom
┃ ┃ ┃ ┃ ┃ ┣ 🗒 NOVAXIS.rom
┃ ┃ ┃ ┃ ┃ ┣ 🗒 PAINT.rom
┃ ┃ ┃ ┃ ┃ ┣ 🗒 PANASONICDISK.rom
┃ ┃ ┃ ┃ ┃ ┣ 🗒 PHILIPSDISK.rom
┃ ┃ ┃ ┃ ┃ ┣ 🗒 RS232.ROM
┃ ┃ ┃ ┃ ┃ ┣ 🗒 SUNRISEIDE.rom
┃ ┃ ┃ ┃ ┃ ┣ 🗒 SWP.rom
┃ ┃ ┃ ┃ ┃ ┣ 🗒 XBASIC2.rom
┃ ┃ ┃ ┃ ┃ ┣ 🗒 nowindDos1.rom
┃ ┃ ┃ ┃ ┃ ┣ 🗒 nowindDos2.rom

## ![](/emulators/roms.png) Roms

### Extensiones de roms soportadas

Las roms deben tener las extensiones siguientes:

* .cas
* .dsk
* .m3u
* .mx1
* .mx2
* .rom
* .zip
* .7z

Este sistema soporta las roms comprimidas en formato .zip/.7z. No obstante, se trata sólo de un archivo.

Los ficheros contenidos en los .zip/.7z deben tener las extensiones citadas previamente.
Además, cada fichero .zip/.7z sólo debe contener un fichero rom al interior.

>Los juegos con extensión .m3u deben contener juegos descomprimidos en su interior!
{.is-info}

### Emplazamiento

Disponed vuestras roms como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 msx2
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.zip**

>Se aconsejan las roms del proyecto **No-Intro**.
{.is-success}

>Para más información sobre las roms, consultad [este tutorial](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}


## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

>Para conservar las configuraciones personalizadas cuando se produce una actualización del sistema Recalbox, os aconsejamos utilizar las [Sobrecargas de configuración](./../../../advanced-usage/configuration-override).
{.is-info}

### Acceder a las opciones

Podéis configurar las opciones del core de dos formas diferentes.

* Via el Menú RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via el fichero `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Opciones del core

| Opción | Valor de la opción | Variable (para la sobrecarga) | Valores posibles |
| --- | --- | --- | --- |
| Machine Type (Restart) | `Auto` ✅ / `MSX` / `MSXturboR` / `MSX2` / `MSX2+` / `SEGA - SG-1000` / `SEGA - SC-3000` / `SEGA - SF-7000` / `SVI - Spectravideo SVI-318` / `SVI - Spectravideo SVI-328` / `SVI - Spectravideo SVI-328 MK2` / `ColecoVision` / `Coleco (Spectravideo SVI-603)` | `bluemsx_msxtype` | `Auto` / `MSX` / `MSXturboR` / `MSX2` / `MSX2+` / `SEGA - SG-1000` / `SEGA - SC-3000` / `SEGA - SF-7000` / `SVI - Spectravideo SVI-318` / `SVI - Spectravideo SVI-328` / `SVI - Spectravideo SVI-328 MK2` / `ColecoVision` / `Coleco (Spectravideo SVI-603)` |
| Crop Overscan | `Désactivé` ✅ / `Activé` / `MSX2` | `bluemsx_overscan` | `disabled` / `enabled` / `MSX2` |
| VDP Sync Type (Restart) | `Auto` / `50Hz` / `60Hz` ✅ | `bluemsx_vdp_synctype` | `auto` / `50 Hz` / `60 Hz` |
| No Sprite Limit | `Désactivé` ✅ / `Activé` | `bluemsx_nospritelimits` | `disabled` / `enabled` |
| Sound YM2413 Enable (Restart) | `Activé` ✅ / `Désactivé` | `bluemsx_ym2413_enable` | `enabled` / `disabled` |
| Cart Mapper Type (Restart) | `Auto` ✅ / `Normal` / `mirrored` / `basic` / `0x4000` / `0xC000` / `ascii8` / `ascii8sram` / `ascii16` / `ascii16sram` / `ascii16nf` / `konami4` / `konami4nf` / `konami5` / `konamisynth` / `korean80` / `korean90` / `korean126` / `MegaFlashRomScc` / `MegaFlashRomSccPlus` / `msxdos2` / `scc` / `sccexpanded` / `sccmirrored` / `sccplus` / `snatcher` / `sdsnatcher` / `SegaBasic` / `SG1000` / `SG1000Castle` / `SG1000RamA` / `SG1000RamB` / `SC3000` | `bluemsx_cartmapper` | `Auto` / `Normal` / `mirrored` / `basic` / `0x4000` / `0xC000` / `ascii8` / `ascii8sram` / `ascii16` / `ascii16sram` / `ascii16nf` / `konami4` / `konami4nf` / `konami5` / `konamisynth` / `korean80` / `korean90` / `korean126` / `MegaFlashRomScc` / `MegaFlashRomSccPlus` / `msxdos2` / `scc` / `sccexpanded` / `sccmirrored` / `sccplus` / `snatcher` / `sdsnatcher` / `SegaBasic` / `SG1000` / `SG1000Castle` / `SG1000RamA` / `SG1000RamB` / `SC3000` |
| Auto Rewind Cassette | `Activé` ✅ / `Désactivé` | `bluemsx_auto_rewind_cas` | `enabled` / `disabled` |

## ![](/emulators/external-links.png) Enlaces externos

* **Código fuente utilizado** : [https://github.com/libretro/blueMSX-libretro/](https://github.com/libretro/blueMSX-libretro/)
* **Documentación Libretro** : [https://docs.libretro.com/library/bluemsx/](https://docs.libretro.com/library/bluemsx/)