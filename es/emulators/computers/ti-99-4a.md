---
title: TI-99/4A
description: 
published: true
date: 2024-07-26T12:52:06.032Z
tags: ti-99/4a, texas instruments, 8.0+, ordenadores
editor: markdown
dateCreated: 2023-09-02T19:06:03.445Z
---

![](/emulators/computers/ti994a.svg){.align-center}

## Datos técnicos

* **Fabricante**: Texas Instruments
* **Año de lanzamiento**: 1979
* **Sistema operativo**: Basic owner
* **Procesador**: Texas Instruments TMS9900 @ 3.3MHz
* **RAM**: 16/24 KB
* **Resolución**: 256 x 192 máximo, 16 colores
* **Sonido**: 3 voces sobre 5 octavas y canal de ruido

## Presentación

Fabricado por Texas Instruments, el **TI-99/4** fue uno de los primeros ordenadores familiares (principios de los años 80). La versión comercializada en Francia pronto se convirtió en el **TI-99/4A**.

Este ordenador tiene algunas similitudes con el concepto de consola de videojuegos. Puede conectarse a un televisor a través de una toma SCART, puede leer cartuchos y tiene un conector para dos joysticks. También puede utilizar casetes de audio para cargar o guardar programas mediante un magnetófono estándar.

En junio de 1979, la empresa estadounidense Texas Instruments presentó la TI-99/4, basada en el prototipo TI-99/3. Las primeras unidades salieron a la venta en noviembre de 1979. Las primeras unidades salieron a la venta en noviembre por 1.150 dólares. Los primeros juegos fueron Conecta Cuatro, El Ahorcado, Yahtzee y Zero Zap.

Se planeó una sucesora, la TI-99/7, para el mundo profesional, pero nunca vio la luz.

La TI-99/4A se presentó al gran público en junio de 1981. La "A" hace referencia al nuevo procesador gráfico: el chip TMS9918A. A diferencia del TMS9918, el TMS9918A tenía un modo de mapa de bits.

En diciembre de 1983, Texas Instruments anunció oficialmente el cese de la producción de la TI-99/4A.

## Emuladores

[TI-99/Sim](ti-99-sim)