---
title: Instalación
description: 
published: true
date: 2024-09-29T14:07:20.075Z
tags: instalación
editor: markdown
dateCreated: 2023-09-14T06:14:21.449Z
---

Esta sección es para vosotros si queréis instalar Recalbox en un bartop o si queréis utilizar un dispositivo USB como dispositivo de almacenamiento, por ejemplo.

Aquí tenéis la lista de tutoriales disponibles:

[Configuración de Recalbox para Bartops / Máquinas recreativas](bartop-arcade-configuration)
[Utilización de un periférico de almacenamiento NMVe con Recalbox](nvme-device-as-storage)
[Usar un periférico USB de almacenamiento en Recalbox](usb-device-as-storage)