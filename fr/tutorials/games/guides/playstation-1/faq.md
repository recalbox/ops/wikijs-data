---
title: FAQ
description: 
published: true
date: 2023-12-19T01:00:41.129Z
tags: ps1, faq
editor: markdown
dateCreated: 2021-05-21T08:47:54.624Z
---

## Multi-disques

Tout d'abord, nous allons voir les différentes possibilités offertes par Recalbox pour changer de disque.

### Mode PSP

Regroupez tous les disques dans un seul fichier au format `.PBP`. Vous pouvez changer de disque dans le menu Retroarch.

#### Avantages :

* Les raccourcis des contrôleurs peuvent être utilisés pour changer de disque.
* Il existe un fichier de sauvegarde pour tous les disques.

#### Inconvénients :

* Les fichiers `.SBI` ne peuvent pas être utilisés dans les jeux multi-disques.
* Ne permet pas de basculer vers un disque patché (avec la correction de protection LibCrypt).
* Cela prend un certain temps de changer tous les jeux multi-disques au format `.PBP`.

### Mode PCSX-reARMed

Le disque suivant est recherché dans le menu Retroarch par le système de fichiers.

#### Avantages :

* Vos jeux sont au format `.BIN` / `.CUE`.

#### Inconvénients :

* Cela ne fonctionne pas actuellement.
* C'est moche et lent.
* Les fichiers sauvegardés ne sont pas partagés entre les disques.
* Les raccourcis des contrôleurs ne peuvent pas être utilisés pour changer de disque.

Par conséquent, les jeux avec protection LibCrypt et multi-disque sont une vraie plaie dans ces deux modes.

### Mode définitif

Il suffit de créer un fichier `.M3U` pour chaque jeu multi-disque. Ce fichier contiendra les noms de certains fichiers. Par exemple :

![M3U](/tutorials/games/guides/playstation-1/m3u1.png)

Le dernier aspect devrait être comme suit :

![Dossier PSX](/tutorials/games/guides/playstation-1/m3u2.png)

Comme vous pouvez le voir dans cet exemple, il y a des `.CUE` / `.BIN` / `.SBI` par disque et un `.M3U` par jeu multi-disque.

#### Avantages :

* Vos jeux restent au format d'origine.
* Les raccourcis des manettes peuvent être utilisés pour changer de disque.
* Les fichiers `.SBI` peuvent être utilisés.
* Il existe un fichier de sauvegarde pour tous les disques.

Il n'y a que des avantages.

## Jeux multi-pistes

Certains jeux ont plusieurs pistes par disque. Cela signifie que le fichier `.CUE` nécessite plusieurs entrées (une par piste).

Exemple de `.CUE` multi-pistes :

![Cue à pistes multiples](/tutorials/games/guides/playstation-1/cue1.png)

Le dernier aspect devrait être le suivant :

![Jeu multi-pistes](/tutorials/games/guides/playstation-1/cue2.png)

## Extensions

### Extension `.BIN` : cas particulier

Les fichiers roms avec l'extension `.BIN` sont également supportés mais nécessitent un fichier `.CUE` pour être démarrés.

### Extension `.CUE` : comment ça marche ?

Un fichier `.CUE` est, tout simplement, un fichier texte dont l'extension a été transformée de `.TXT` à `.CUE`. Il doit porter le même nom que le fichier `.BIN` qu'il accompagne et il contient les informations nécessaires au lancement du fichier `.BIN`. Ceci fonctionne aussi si vous avez plusieurs fichiers `.BIN` avec un seul `.CUE` (ça dépendra des jeux).

### Extension `.M3U` : gérer les jeux multi-CD

Plusieurs CD d'un même jeu peuvent être chargés simultanément dans RetroArch en créant un fichier « liste de lecture » `.M3U` (texte brut `.TXT` avec une extension `.M3U`).

>Une liste de lecture `.M3U` peut être utilisée avec toutes les extensions existantes qui ont un jeu divisé en plusieurs supports physiques, que ce soit cartouche ou CD.
{.is-warning}

## Les étapes pour gérer un jeu multi-CD

Vous devez créer un nouveau fichier texte que l'on nomme `<nom_du_jeu>.m3u`.

>Vous avez 2 exemples ci-dessous avec les extensions `.BIN` / `.CUE` et `.CHD`.
{.is-info}

### Exemple avec Final Fantasy VII en `.CUE`

On crée le fichier `Final Fantasy (France).m3u` avec le contenu suivant :

```
Final Fantasy VII (France) (Disc 1).cue
Final Fantasy VII (France) (Disc 2).cue
Final Fantasy VII (France) (Disc 3).cue
```

### Exemple avec Final Fantasy VII en `.CHD`

On crée le fichier `Final Fantasy (France).m3u` avec le contenu suivant :

```
Final Fantasy VII (France) (Disc 1).chd
Final Fantasy VII (France) (Disc 2).chd
Final Fantasy VII (France) (Disc 3).chd
```

## Changer de disque en cours de partie

Si l'on a besoin de changer de disque lorsque le jeu est lancé, il faut réaliser les actions suivantes :

1. Ouvrir le tiroir CD "virtuel" : `Touche HK + Joystick Gauche (Direction Haut)`
2. Changer de disque : `Touche HK + Joystick Gauche (Direction Droite ou Gauche)`
3. Fermer le tiroir CD "virtuel" : `Touche HK + Joystick Gauche (Direction Haut)`

Ceci fonctionne si vous avez votre jeu milti-disques dans un fichier `.M3U`.

## Divers

### Que dois-je faire avec les fichiers `.ECM` et `.APE` ?

Vous devez les décompresser. Voir [ce guide](https://www.epforums.org/showthread.php?57757-ECM-And-APE-Guide) pour le faire.

### Comment puis-je activer le Dualshock (analogues) ?

* Dans le jeu, allez dans le menu RetroArch en faisant `Hotkey` + `B`.
* Sélectionnez `Touches` > `Port 1 Touches`.

![](/tutorials/games/guides/playstation-1/enable-analog/psxanalog1.png){.full-width}

* Sélectionnez `analog` dans l'option `Type de périphérique` (identique pour tous les pavés que vous voulez).

![](/tutorials/games/guides/playstation-1/enable-analog/psxanalog2.png){.full-width}

### Je n'ai pas le fichier `.CUE`, comment je fais ?

Si vous n'avez qu'un fichier `.BIN`et aucun fichier `.CUE`, vous pouvez le générer de plusieurs manière :

* En ligne : [http://nielsbuus.dk/pg/psx_cue_maker/](http://nielsbuus.dk/pg/psx_cue_maker/)
* Avec ce logiciel freeware : [CueMaker](https://github.com/thorst/CueMaker/releases/)

Vous pouvez aussi l'obtenir [auprès de Redump](http://redump.org/cues/psx/) en allant sur la page du jeu en question.

##  Glossaire

* .ape : fichier compressé pour le fichier .wav.
* .bin : données de jeu et pistes de musique.
* .ccd/.img/.sub : clonage de fichiers CD.
* .cue : fichier dans lequel les pistes du disque sont définies. Un par données .bin.
* .ecm : fichier compressé pour le fichier .bin.
* .pbp : Fichier PSP pour les jeux PSX.
* .ppf : fichier correctif pour les jeux avec protection LibCrypt.
* .sbi : fichier qui contient les informations de protection et qui sont nécessaires pour exécuter des jeux protégés dans des émulateurs. Ils doivent porter le même nom que le fichier .cue.
* .wav : fichier de piste musicale. Vous devez le renommer en .bin.

## Liens utiles

* [Guide ECM-APE](https://www.epforums.org/showthread.php?57757-ECM-And-APE-Guide)
* [Liste des jeux PSX](https://psxdatacenter.com/pal_list.html)
* [Fichiers .sbi](http://psxdatacenter.com/sbifiles.html)
* [Générateur de fichier .cue](http://nielsbuus.dk/pg/psx_cue_maker/)