---
title: 🎞️ Vidéo
description: 
published: true
date: 2023-09-26T07:00:20.683Z
tags: tutorial, video
editor: markdown
dateCreated: 2023-09-01T09:18:07.012Z
---

Si estáis en esta página, es porque buscáis un tutorial relacionado con todo lo que tiene que ver con el vídeo!

Aquí tenéis la lista de categorías disponibles:

[CRT](crt)
[Configuration de la pantalla](display-configuration)
[HDMI](hdmi)
[LCD](lcd)
[TFT](tft)