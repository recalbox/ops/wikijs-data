---
title: Amiberry
description: 
published: true
date: 2024-06-23T00:18:23.773Z
tags: amiga-1200, amiga, 1200, amiberry
editor: markdown
dateCreated: 2021-05-21T08:14:32.754Z
---

**Amiberry** est un core pour ARM optimisé pour Amiga.

## ![](/emulators/license.svg) Licence

Ce core est sous licence [**GPLv3**](https://github.com/midwan/amiberry/blob/master/COPYING).

## ![](/emulators/compatibility.png) Compatibilité

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ❌ | ✅ |

## ![](/emulators/features.png) Fonctionnalités



## ![](/emulators/bios.svg) BIOS

### Liste des bios obligatoires

Vous trouverez les bios avec le nom indiqué dans la colonne **Description** qu'il vous faudra renommer avec le nom indiqué dans la colonne **Nom de fichier**.

| Nom de fichier | Description | MD5 | Fourni |
| :---: | :---: | :---: | :---: |
| kick39106.A1200 | Kickstart v3.0 rev 39.106 (1992)(Commodore)(A1200)\[!\].rom | b7cc148386aa631136f510cd29e42fc3 | ❌ |
| kick40068.A1200 | Kickstart v3.1 r40.068 (1993-12)(Commodore)(A1200)\[!\].rom | 646773759326fbac3b2311fd8c8793ee | ❌ |
| kick40068.A4000 | Kickstart v3.1 r40.068 (1993-12)(Commodore)(A4000)\[!\].rom | 9bdedde6a4f33555b4a270c8ca53297d | ❌ |

### Emplacement

Placez les BIOS comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 🗒 **kick39106.A1200**
┃ ┃ ┃ ┣ 🗒 **kick40068.A1200**
┃ ┃ ┃ ┣ 🗒 **kick40068.A4000**

## ![](/emulators/roms.png) Roms

>L'émulation Amiga est disponible en deux parfums : Amiga 600 et Amiga 1200.  
>Certains jeux peuvent être très capricieux donc essayez les deux systèmes si un jeu ne fonctionne pas.
>Les jeux peuvent être utilisés soit dans un format disque ADF soit dans un format WHDLoad, utilisant dans ce cas un fichier UAE complémentaire.
{.is-info}

### Extensions supportées

Les roms doivent avoir l'extension :

* .adf
* .ipf
* .rp9
* .whd
* .zip

>Les autres formats d'images disque _pourraient_ être supportés mais n'ont pas été testés par l'équipe de Recalbox.
>
>Vous pouvez retrouver plus d'informations sur ces formats sur [cette page](./../../../tutorials/games/guides/amiga/faq).
{.is-info}

### Emplacement

Placez les roms comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 amiga600
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.bin**
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.lst**

>Les roms au format **TOSEC** sont vivement conseillées.
{.is-success}

>Pour plus d'information sur les roms, rendez-vous sur [ce tutoriel](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuration avancée de l'émulateur

>Pour pouvoir conserver vos configurations personnalisées lors d'une mise à jour, nous vous conseillons d'utiliser notre fonctionnalité [Surcharges de configuration](./../../../advanced-usage/configuration-override).
{.is-info}

## ![](/emulators/external-links.png) Liens externes

* **Code source utilisé** : [https://gitlab.com/recalbox/packages/standalone/amiberry/](https://gitlab.com/recalbox/packages/standalone/amiberry)
* **Site officiel** : [https://blitterstudio.com/amiberry/](https://blitterstudio.com/amiberry/)