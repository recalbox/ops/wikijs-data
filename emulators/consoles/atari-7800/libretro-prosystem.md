---
title: Libretro Prosystem
description: 
published: true
date: 2024-07-24T09:50:33.391Z
tags: libretro, atari, 7800, atari-7800, prosystem
editor: markdown
dateCreated: 2021-07-30T16:36:50.533Z
---



## ![](/emulators/license.svg) License

This core is under [**GPLv2**](https://github.com/libretro/prosystem-libretro/blob/master/License.txt) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Features

| Feature | Supported |
| :---: | :---: |
| Restart | ✔ |
| Screenshots | ✔ |
| Sttes | ✔ |
| Rewind | ✔ |
| RetroAchievements | ✔ |
| Controls | ✔ |
| Remapping | ✔ |

## ![](/emulators/bios.svg) Bios

### List of optional bios

| Filename | Description | MD5 | Provided |
| :---: | :---: | :---: | :---: |
| 7800 BIOS (E).rom | BIOS 7800 | 397bb566584be7b9764e7a68974c4263 | ❌ |
| 7800 BIOS (U).rom | BIOS 7800 | 0763f1ffb006ddbe32e52d497ee848ae | ❌ |

### Location

Put the bios like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 atari7800
┃ ┃ ┃ ┃ ┣ 🗒 **7800 BIOS (E).rom**
┃ ┃ ┃ ┃ ┣ 🗒 **7800 BIOS (U).rom**

## ![](/emulators/roms.png) Roms

### Supported extensions

Roms must have the extension:

* .a78
* .bin
* .cdf
* .zip
* .7z

This system supports compressed roms in .zip/.7z format. However, be careful, it is only an archive.

The files contained in the .zip/.7z must correspond to the extensions mentioned above.
Moreover, each .zip/.7z file must contain only one compressed rom.

### Location

Put the roms like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 atari7800
┃ ┃ ┃ ┃ ┣ 🗒 **game.zip**

>The roms using **No-Intro** format are highly recommended.
{.is-success}

>For more information about roms, go to [this tutorial](./../../../tutorials/games/generalities/isos-and-roms)!
{.is-info}

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Accessing to options

You can configure various options in two different ways.

* Via RetroArch menu:

┣ 📁 RetroArch Menu
┃ ┣ 📁 Core options
┃ ┃ ┣ 🧩 Name_option

* Via the file `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core options

| Options | Options values | Variable (for configuration override) | Variables values |
| --- | --- | --- | --- |
| Color Depth (Restart) | `Thousands (16-bit)` ✅ / `Millions (24-bit)` | `prosystem_color_depth` | `16bit` / `24bit` |
| Audio Filter | `Disabled` ✅ / `Enabled` | `prosystem_low_pass_filter` | `disabled` / `enabled` |
| Audio Filter Level | `5%` / `10%` / `15%` / `20%` / `25%` / `30%` / `35%` / `40%` / `45%` / `50%` / `55%` / `60%` ✅ / `65%` / `70%` / `75%` / `80%` / `85%` / `90%` / `95%` | `prosystem_low_pass_range` | `5` / `10` / `15` / `20` / `25` / `30` / `35` / `40` / `45` / `50` / `55` / `60` / `65` / `70` / `75` / `80` / `85` / `90` / `95` |
| Dual Stick Controller | `Disabled` ✅ / `Enabled` | `prosystem_gamepad_dual_stick_hack` | `disabled` / `enabled` |

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/libretro/prosystem-libretro/](https://github.com/libretro/prosystem-libretro/)
* **Libretro documentation**: [https://docs.libretro.com/library/prosystem/](https://docs.libretro.com/library/prosystem/)