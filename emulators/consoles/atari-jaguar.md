---
title: Atari Jaguar
description: 
published: true
date: 2021-10-07T14:36:24.272Z
tags: consoles, atari, jaguar
editor: markdown
dateCreated: 2021-05-21T07:58:59.684Z
---

![](/emulators/consoles/atarijaguar.svg){.align-center}

## Fiche technique

* **Fabricant :** Atari Corporation
* **Année de sortie** : 1993
* **Quantités vendues** : moins de 250000
* **Processeurs :** 5 processeurs principaux sur 3 puces :
  * « Tom » : puce @ 26.59 MHz, processeur graphique (32-bit RISC), Object (64-bit RISC), Blitter (64-bit RISC)
  * « Jerry » : puce @ 26.59 MHz, Digital Signal Processor (32-bit)
  * Motorola 68000 (16-bit @ 13.295 MHz)
* **RAM :** 2 MB
* **Résolution :** Max resolution 800×576 at 24 bit "true" color

## Présentation

La **Jaguar** est une console de jeux vidéo de salon à cartouches datant de 1993, disposant d'une ludothèque limitée, et qui n'a pas connu un grand succès malgré sa technologie innovante. C’est l'avant-dernière console produite par Atari Corporation.

## Émulateurs

[Libretro Virtualjaguar](libretro-virtualjaguar)