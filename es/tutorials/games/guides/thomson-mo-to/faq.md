---
title: FAQ
description: 
published: true
date: 2023-09-21T13:34:43.968Z
tags: faq
editor: markdown
dateCreated: 2023-09-21T13:34:43.968Z
---

## Utilización

Por defecto, el tipo de ordenador emulado depende del nombre del fichero utilizado.

_**Ejemplo:**_  
**super-tennis-fil_mo5.k7** provocará que el emulador utilizado sea el **MO5**.

>Si el fichero no contiene el nombre de una de las máquinas emuladas, el emulador pasará por defecto al modo TO8.  
>Es posible forzar el tipo de máquina emulada mediante una opción en RetroArch.
{.is-info}

Los juegos no se inician automáticamente cuando se carga la máquina.  
En los ordenadores Thomson, es necesario pulsar una o varias teclas del teclado para iniciar un programa. 
  
Para facilitar el uso del emulador con los mandos, el botón `START` del mando intenta iniciar el juego utilizando un método que depende del ordenador que se esté emulando y del tipo de soporte insertado (véase el [fichero README de Theodore](https://github.com/Zlika/theodore/blob/master/README-FR.md#video_game-correspondance-des-boutons-de-la-manette)).  
  
Si esto no funciona, probad otro método, usando un teclado físico o el teclado virtual (ver más abajo).

## Mandos

Aparte del botón `B` del mando, que emula el botón único de los joysticks Thomson, los demás botones del mando se utilizan para la función del "teclado virtual".

Esta función facilita la ejecución de la mayoría de los juegos sin necesidad de un teclado real.

<table>
  <thead>
    <tr>
      <th style="text-align:center">Botón</th>
      <th style="text-align:left">Desscripción</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td style="text-align:center"><b>B</b>
      </td>
      <td style="text-align:left">
        <p>Botón "Acción" cuando no aparece el teclado virtual.</p>
        <p>Cuando se muestra el teclado virtual:</p>
        <ul>
          <li>Pulsación corta: Aprieta la tecla del teclado con el focus.</li>
          <li>Pulsación larga: Mantenimiento continuo de la tecla (o relajación de la misma si estaba ya apretada). Se pueden mantener apretadas hasta 3 teclas simultáneamente. Cuando se cierra el teclado virtual se sueltan todas las teclas apretadas.</li>
        </ul>
      </td>
    </tr>
    <tr>
      <td style="text-align:center"><b>Y</b>
      </td>
      <td style="text-align:left">Desplaza el teclado virtual en la parte de arriba o de abajo de la pantalla.</td>
    </tr>
    <tr>
      <td style="text-align:center"><b>Start</b>
      </td>
      <td style="text-align:left">
        <p>Arranca el juego si no nos encontramos dentro del teclado virtual.</p>
        <p>Atajo para apretar la tecla "Enter" cuando se muestra el teclado virtual.</p>
      </td>
    </tr>
    <tr>
      <td style="text-align:center"><b>Select</b>
      </td>
      <td style="text-align:left">Muestra/Oculta el teclado virtual.</td>
    </tr>
  </tbody>
</table>