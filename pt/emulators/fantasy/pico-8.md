---
title: Pico-8
description: 
published: true
date: 2021-10-19T23:56:54.188Z
tags: fantasy, pico-8, 7.2+
editor: markdown
dateCreated: 2021-10-19T23:56:54.188Z
---

![](/emulators/fantasy/pico8.svg){.align-center}

## Ficha técnica

* **Exibição:** 128x128 16 cores
* **Tamanho do cartucho:** 32k
* **Som:** blerps de chip de 4 canais
* **Código:** Lua
* **Sprites:** 256 sprites 8x8
* **Mapa:** 128x32 cels

## Apresentação

**Pico-8** é uma máquina virtual desenvolvida pela **Lexaloffle Games**. Seu objetivo é oferecer um console de fantasia, no qual seja possível criar, compartilhar e jogar pequenos videogames e outros programas de computador.

Parece um console normal, mas funciona em Windows / Mac / Linux.

O desenvolvimento é realizado por um ambiente Lua, no qual o usuário pode criar os diversos elementos do jogo (mapas, sons, sprites, ...).

O Pico-8 tem sido usado por criadores veteranos, mas também por não-profissionais dos vídeo games.

O display gráfico é limitado a 16 cores, 128x128 pixels, o que confere facilmente aos jogos assim criados um visual minimalista e retro.

Quando você o liga, a máquina te recepciona com uma linha de comando, um conjunto de ferramentas de criação de cartucho e um navegador de cartucho online chamado SPLORE.

## Emuladores

[Libretro Retro8](libretro-retro8)