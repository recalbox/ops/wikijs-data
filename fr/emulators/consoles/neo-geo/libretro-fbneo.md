---
title: Libretro FBNeo
description: 
published: true
date: 2024-06-25T16:29:15.483Z
tags: libretro, fbneo, neo-geo
editor: markdown
dateCreated: 2021-05-21T08:22:21.672Z
---

**FinalBurn Neo**, un émulateur pour les jeux d'arcade et certaines consoles. Il est basé sur les émulateurs FinalBurn et les anciennes versions de [MAME](https://www.mamedev.org/) (émulant moins de bornes d'arcade) mais il fonctionne suivant les mêmes principes.

FB Neo (a.k.a. FinalBurn Neo) est un émulateur d'arcade qui supporte les plateformes suivantes :

* Capcom CPS-1
* Capcom CPS-2
* Capcom CPS-3
* Cave
* Jeux basé sur Data East DEC-0, DEC-8, Cassette System, DECO IC16 et DECO-32
* Matériel bsé sur Galaxian
* Matériel Irem M62, M63, M72, M90, M92 et M107
* Kaneko 16
* Konami
* Namco Mappy, System 86, System 1 & 2 et autres
* Neo-Geo
* NMK16
* Matériel basé sur Pacman
* PGM
* Matériel basé sur Psikyo 68EC020 et SH-2
* Sega System 1, System 16 (et similaire), System 18, X-Board et Y-Board
* Matériel basé sur Seta-Sammy-Visco (SSV)
* Super Kaneko Nova System
* Toaplan 1
* Toaplan 2
* Taito F2, F3, X, Z et autres
* Divers pilotes pour d'autres matériels
* FB Neo contient aussi des pilotes en cours de travail pour les système suivants :
  * Sega Megadrive/Genesis
  * ColecoVision
  * Sega SG-1000
  * Sega Master System
  * Game Gear
  * MSX-1
  * ZX Spectrum
  * PC-Engine
  * TurboGrafx 16
  * SuperGrafx
  * Fairchild Channel F.

## ![](/emulators/license.svg) Licence

Ce core est sous licence [**non commerciale**](https://github.com/finalburnneo/FBNeo/blob/master/src/license.txt).

## ![](/emulators/compatibility.png) Compatibilité

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Fonctionnalités

| Fonctionnalité | Supporté |
| :---: | :---: |
| Sauvegardes | ✔ |
| Sauvegardes instantanées | ✔ |
| Rembobinage | ✔ |
| Netplay | ✔ |
| RetroAchievements | ✔ |
| Cheats RetroArch | ✔ |
| Cheats natifs | ✔ |
| Manettes | ✔ |
| Multi-Mouse | ✔ |
| Sous-systèmes | ✔ |

## ![](/emulators/bios.svg) Bios

### Liste des bios

En fonction des jeux, certains bios seront nécessaires et seront à placer dans le même répertoire que les jeux.

### Emplacement

Placez les bios comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 neogeo
┃ ┃ ┃ ┃ ┣ 🗒 **bios.zip**

## ![](/emulators/roms.png) Roms

### Extensions supportées

Tous les jeux FinalBurn Neo utilisent les jeux au formats suivants :

* .zip
* .7z

### Emplacement

Placez les roms comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 neogeo
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.zip**

>Les jeux peuvent aussi être lancés depuis `/recalbox/share/roms/fbneo` mais les lancer depuis le dossier `neogeo` peut optimiser son usage du fait d'être dans le bon système.
{.is-info}

>Pour plus d'information sur les roms, rendez-vous sur [ce tutoriel](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuration avancée des émulateurs

>Pour pouvoir conserver vos configurations personnalisées lors d'une mise à jour, nous vous conseillons d'utiliser notre fonctionnalité [Surcharges de configuration](./../../../advanced-usage/configuration-override).
{.is-info}

### Accéder aux options

Vous pouvez configurer diverses options de deux façons différentes.

* Via le Menu RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via le fichier `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Options du core

| Options | Valeur des options | Variable (pour surcharge) | Valeurs des variables |
| --- | --- | --- | --- |
| Use 32-bits color depth when available | `Désactivé` / `Activé` ✅ / | `fbneo-allow-depth-32` | `disabled` / `enabled` |
| Vertical mode | `Désactivé` ✅ / `Activé` / `alternate` | `fbneo-vertical-mode` | `disabled` / `enabled` / `alternate` |
| Force 60Hz | `Désactivé` ✅ / `Activé` | `fbneo-force-60hz` | `disabled` / `enabled` |
| Allow patched romsets | `Désactivé` / `Activé` ✅ | `fbneo-allow-patched-romsets` | `disabled` / `enabled` |
| Analog Speed | `25%` / `30%` / `35%` / `40%` / `45%` / `50%` / `55%` / `60%` / `65%` / `70%` / `75%` / `80%` / `85%` / `90%` / `95%` / `100%` ✅ / `105%` / `110%` / `115%` / `120%` / `125%` / `130%` / `135%` / `140%` / `145%` / `150%` / `155%` / `160%` / `165%` / `170%` / `175%` / `180%` / `185%` / `190%` / `195%` / `200%` / `205%` / `210%` / `215%` / `220%` / `225%` / `230%` / `235%` / `240%` / `245%` / `250%` / `255%` / `260%` / `265%` / `270%` / `275%` / `280%` / `285%` / `290%` / `295%` / `300%` / `305%` / `310%` / `315%` / `320%` / `325%` / `330%` / `335%` / `340%` / `345%` / `350%` / `355%` / `360%` / `365%` / `370%` / `375%` / `380%` / `385%` / `390%` / `395%` / `400%` | `fbneo-analog-speed` | `25%` / `30%` / `35%` / `40%` / `45%` / `50%` / `55%` / `60%` / `65%` / `70%` / `75%` / `80%` / `85%` / `90%` / `95%` / `100%` / `105%` / `110%` / `115%` / `120%` / `125%` / `130%` / `135%` / `140%` / `145%` / `150%` / `155%` / `160%` / `165%` / `170%` / `175%` / `180%` / `185%` / `190%` / `195%` / `200%` / `205%` / `210%` / `215%` / `220%` / `225%` / `230%` / `235%` / `240%` / `245%` / `250%` / `255%` / `260%` / `265%` / `270%` / `275%` / `280%` / `285%` / `290%` / `295%` / `300%` / `305%` / `310%` / `315%` / `320%` / `325%` / `330%` / `335%` / `340%` / `345%` / `350%` / `355%` / `360%` / `365%` / `370%` / `375%` / `380%` / `385%` / `390%` / `395%` / `400%` |
| Crosshair emulation | `hide with lightgun device` ✅ / `always hide` / `always show` | `fbneo-lightgun-crosshair-emulation` | `hide with lightgun device` / `always hide` / `always show` |
| CPU clock | `25%` / `30%` / `35%` / `40%` / `45%` / `50%` / `55%` / `60%` / `65%` / `70%` / `75%` / `80%` / `85%` / `90%` / `95%` / `100%` ✅ / `105%` / `110%` / `115%` / `120%` / `125%` / `130%` / `135%` / `140%` / `145%` / `150%` / `155%` / `160%` / `165%` / `170%` / `175%` / `180%` / `185%` / `190%` / `195%` / `200%` / `205%` / `210%` / `215%` / `220%` / `225%` / `230%` / `235%` / `240%` / `245%` / `250%` / `255%` / `260%` / `265%` / `270%` / `275%` / `280%` / `285%` / `290%` / `295%` / `300%` / `305%` / `310%` / `315%` / `320%` / `325%` / `330%` / `335%` / `340%` / `345%` / `350%` / `355%` / `360%` / `365%` / `370%` / `375%` / `380%` / `385%` / `390%` / `395%` / `400%` | `fbneo-cpu-speed-adjust` | `25%` / `30%` / `35%` / `40%` / `45%` / `50%` / `55%` / `60%` / `65%` / `70%` / `75%` / `80%` / `85%` / `90%` / `95%` / `100%` / `105%` / `110%` / `115%` / `120%` / `125%` / `130%` / `135%` / `140%` / `145%` / `150%` / `155%` / `160%` / `165%` / `170%` / `175%` / `180%` / `185%` / `190%` / `195%` / `200%` / `205%` / `210%` / `215%` / `220%` / `225%` / `230%` / `235%` / `240%` / `245%` / `250%` / `255%` / `260%` / `265%` / `270%` / `275%` / `280%` / `285%` / `290%` / `295%` / `300%` / `305%` / `310%` / `315%` / `320%` / `325%` / `330%` / `335%` / `340%` / `345%` / `350%` / `355%` / `360%` / `365%` / `370%` / `375%` / `380%` / `385%` / `390%` / `395%` / `400%` |
| Hiscores | `Désactivé` / `Activé` ✅ | `fbneo-hiscores` | `disabled` / `enabled` |
| Diagnostic Input | `Hold Start` ✅ / `Start + A + B` / `Hold Start + A + B` / `Start + L + R` / `Hold Start + L + R` / `Hold Select` / `Select + A + B` / `Hold Select + A + B` / `Select + L + R` / `Hold Select + L + R` | `fbneo-diagnostic-input` | `Hold Start` / `Start + A + B` / `Hold Start + A + B` / `Start + L + R` / `Hold Start + L + R` / `Hold Select` / `Select + A + B` / `Hold Select + A + B` / `Select + L + R` / `Hold Select + L + R` |
| Neo-Geo mode | `Use bios set in BIOS dipswitch` / `MVS Europe/Asia (English)` / `MVS USA (English - Censored)` / `MVS Japan (Japanese)` / `AES Europe/Asia (English)` / `AES Japan (Japanese)` / `UNIBIOS` | `fbneo-neogeo-mode` | `DIPSWITCH` / `MVS_EUR` / `MVS_USA` / `MVS_JAP` / `AES_EUR` / `AES_JAP` / `UNIBIOS` |
| Memory card mode | `Disabled` / `shared` / `per-game` | `fbneo-memcard-mode` | `disabled` / `shared` / `per-game` |
| Debug Dip 1_1 | `Disabled` ✅ / `Enabled` | `fbneo-debug-dip-1-1` | `disabled` / `enabled` |
| Debug Dip 1_2 | `Disabled` ✅ / `Enabled` | `fbneo-debug-dip-1-2` | `disabled` / `enabled` |
| Debug Dip 1_3 | `Disabled` ✅ / `Enabled` | `fbneo-debug-dip-1-3` | `disabled` / `enabled` |
| Debug Dip 1_4 | `Disabled` ✅ / `Enabled` | `fbneo-debug-dip-1-4` | `disabled` / `enabled` |
| Debug Dip 1_5 | `Disabled` ✅ / `Enabled` | `fbneo-debug-dip-1-5` | `disabled` / `enabled` |
| Debug Dip 1_6 | `Disabled` ✅ / `Enabled` | `fbneo-debug-dip-1-6` | `disabled` / `enabled` |
| Debug Dip 1_7 | `Disabled` ✅ / `Enabled` | `fbneo-debug-dip-1-7` | `disabled` / `enabled` |
| Debug Dip 1_8 | `Disabled` ✅ / `Enabled` | `fbneo-debug-dip-1-8` | `disabled` / `enabled` |
| Debug Dip 2_1 | `Disabled` ✅ / `Enabled` | `fbneo-debug-dip-2-1` | `disabled` / `enabled` |
| Debug Dip 2_2 | `Disabled` ✅ / `Enabled` | `fbneo-debug-dip-2-2` | `disabled` / `enabled` |
| Debug Dip 2_3 | `Disabled` ✅ / `Enabled` | `fbneo-debug-dip-2-3` | `disabled` / `enabled` |
| Debug Dip 2_4 | `Disabled` ✅ / `Enabled` | `fbneo-debug-dip-2-4` | `disabled` / `enabled` |
| Debug Dip 2_5 | `Disabled` ✅ / `Enabled` | `fbneo-debug-dip-2-5` | `disabled` / `enabled` |
| Debug Dip 2_6 | `Disabled` ✅ / `Enabled` | `fbneo-debug-dip-2-6` | `disabled` / `enabled` |
| Debug Dip 2_7 | `Disabled` ✅ / `Enabled` | `fbneo-debug-dip-2-7` | `disabled` / `enabled` |
| Debug Dip 2_8 | `Disabled` ✅ / `Enabled` | `fbneo-debug-dip-2-8` | `disabled` / `enabled` |
| Frameskip | `Désactivé` ✅ / `Fixed` / `Auto` / `Manual` | `fbneo-frameskip-type` | `disabled` / `Fixed` / `Auto` / `Manual` |
| Frameskip Threshold (%) | `15` / `18` / `21` / `24` / `27` / `30` / `33` ✅ / `36` / `39` / `42` / `45` / `48` / `51` / `54` / `57` / `60` | `fbneo-frameskip-manual-threshold` | `15` / `18` / `21` / `24` / `27` / `30` / `33` / `36` / `39` / `42` / `45` / `48` / `51` / `54` / `57` / `60` |
| Fixed Frameskip | `No skipping` ✅ / `Skip rendering of 1 frames out of 2` / `Skip rendering of 2 frames out of 3` / `Skip rendering of 3 frames out of 4` / `Skip rendering of 4 frames out of 5` / `Skip rendering of 5 frames out of 6` | `fbneo-fixed-frameskip` | `0` / `1` / `2` / `3` / `4` / `5` |
| Samplerate | `44100` / `48000` ✅ | `fbneo-samplerate` | `44100` / `48000` |
| Sample Interpolation | `Désactivé` / `2-point 1st order` / `4-point 3rd order` ✅ | `fbneo-sample-interpolation` | `disabled` / `2-point 1st order` / `4-point 3rd order` |
| FM Interpolation | `Désactivé` / `4-point 3rd order` ✅ | `fbneo-fm-interpolation` | `disabled` / `4-point 3rd order` |
| LowPass Filter | `Désactivé` ✅ / `Activé` | `fbneo-lowpass-filter` | `disabled` / `enabled` |

## ![](/emulators/external-links.png) Liens externes

* **Code source utilisé** : [https://github.com/libretro/FBNeo/](https://github.com/libretro/FBNeo/)
* **Documentation Libretro** : [https://docs.libretro.com/library/fbneo/](https://docs.libretro.com/library/fbneo/)
* **Site officiel** : [https://neo-source.com/](https://neo-source.com/)
* **Wiki du core** : [http://emulation.gametechwiki.com/index.php/FinalBurn_Neo](http://emulation.gametechwiki.com/index.php/FinalBurn_Neo)
* **Wiki officiel** : [https://github.com/finalburnneo/FBNeo/wiki](https://github.com/finalburnneo/FBNeo/wiki)