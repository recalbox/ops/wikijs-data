---
title: PC-FX
description: NEC PC-FX
published: true
date: 2024-07-26T07:58:36.067Z
tags: nec, pcfx, consolas
editor: markdown
dateCreated: 2023-09-03T20:08:08.038Z
---

![](/emulators/consoles/pcfx.svg){.align-center}

## Datos técnicos

* **Fabricante**: NEC Home Electronics
* **Año de lanzamiento**: 1994
* **Cantidades vendidas**: 400000
* **Procesador**: NEC V810 RISC de 32 bits a 21,5 MHz
* **RAM**: 2 MB (32 KB de reserva)
* **VRAM**: 1,25 MB
* **Chip de sonido**: 16 bits estéreo con ADPCM
* **Resolución**: 640x480 píxeles
* **Soporte**: CD-ROM

## Presentación

La **PC-FX** es una videoconsola de 32 bits desarrollada por NEC. Salió a la venta el 23 de diciembre de 1994, unas semanas después de la PlayStation de Sony y un mes después de la Sega Saturn. Diseñada como sucesora de la PC-Engine y la SuperGrafX, sus juegos están en formato CD-ROM.

A diferencia de sus predecesoras, la PC-FX sólo salió a la venta en Japón. La consola tiene forma de torre vertical, como las cajas de formato torre de algunos PC, y se pretendía que fuera tan actualizable como estos últimos. Sin embargo, la PC-FX no tenía un chip gráfico capaz de manejar 3D, sino que NEC se concentró en el vídeo en movimiento y los juegos 2D. Al final, esta elección hizo que el FX fuera menos potente que sus competidores. Además, era caro y no contaba con el apoyo de los desarrolladores y editores de juegos. Todo ello hizo que no pudiera competir con las demás consolas de quinta generación. La PC-FX fue la última videoconsola de NEC y su producción se interrumpió en febrero de 1998, cuando sólo se habían vendido 400.000 unidades en 4 años.

## Emuladores

[Libretro Mednafen_PCFX](libretro-mednafen_pcfx)