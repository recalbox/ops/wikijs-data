---
title: Amiga CDTV
description: 
published: true
date: 2021-11-16T08:51:07.161Z
tags: amiga-cdtv, consoles, amiga, commodore, cdtv
editor: markdown
dateCreated: 2021-05-21T07:58:35.321Z
---

![](/emulators/consoles/amigacdtv.svg){.align-center}

## Fiche technique

* **Fabricant** : Commodore International
* **Année de sortie** : 1991
* **Système d'exploitation** : CDTV / AmigaOS 1.3
* **Processeur** : Motorola 68000 @ 7.16 MHz (NTSC) ou 7.09 (PAL)
* **RAM** : 1 MB (modifiable)
* **ROM** : 256 KB Kickstart 2.5 et firmware 256 KB CDTV
* **Processeur graphique** : Commodore's Enhanced Chip Set
* **Puce sonore** : Quatre canaux 8-bit PCM (deux canaux stéréo)
* **Résolution** : 320 x 200 (32 couleurs) / 640 x 240 (16 couleurs)

## Présentation

Le CDTV, pour Commodore Dynamic Total Vision, communément appelé Amiga CDTV, est un ordinateur personnel et une console de jeu de Troisième génération développé par Commodore et commercialisée en 1991. Prenant la forme d'une platine de salon de couleur noire, le système est basé sur un ordinateur Amiga 500 avec un processeur Motorola 68000 à 7,16 MHz et un chipset graphique ECS fat Agnus exploitant 1 Mo de RAM et une ROM de 512 kio intégrant Kickstart 1.3. Elle intègre d'origine un lecteur cédérom 1x à caddie, support utilisé pour les logiciels. Son prix de lancement était de 1 000 $ US avec une manette de jeux et deux logiciels. Elle a été pour la première fois présentée au Consumer Electronics Show de Las Vegas en 1991.

En option sont disponibles un clavier, une souris, une télécommande, un lecteur de disquettes, une carte module Commodore CDTV SCSI avec un disque dur de 60 Mio.

Dans l'esprit de Commodore cette console de jeux visait un public familial néophyte en informatique, intéressé par les jeux mais aussi par les capacités multimédia et l'interactivité. L'encyclopédie Grolier a été par exemple l'un de ses produits phares. Le fait de pouvoir transformer la console en ordinateur Amiga 500 était aussi une des fonctionnalités intéressantes du produit. Cette console n'a pas eu un énorme succès et était essentiellement destinée aux marchés américain et allemand. Un prototype de CDTV-II, nommé CDTV CR (Cost Reduced, à coût réduit), a été construit mais abandonné. Il aurait dû intégrer plus de mémoire et un lecteur de disquettes. Commodore par la suite lança l'Amiga CD32 basée sur la même technologie que l'Amiga 1200, que l'on pourrait qualifier de successeur de la CDTV.

## Émulateurs

[Amiberry](amiberry)
[Libretro PUAE](libretro-puae)
[Libretro UAE4ARM](libretro-uae4arm)