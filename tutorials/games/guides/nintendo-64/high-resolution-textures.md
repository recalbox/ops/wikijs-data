---
title: High-resolution textures
description: 
published: true
date: 2025-01-31T17:35:49.544Z
tags: n64, texture, pack
editor: markdown
dateCreated: 2021-08-08T13:46:28.305Z
---

## Introducing high-resolution textures?

High-resolution textures (Hi-res) are fan-made packages designed to enhance the default textures of a game.

**Rice**, developer of the **Rice Video** plugin, was the first to make it possible to extract textures from a game, modify them and re-import them in a higher resolution. Now it is possible to replace blurred textures in games with high definition versions or a completely different version. Excited by this new possibility, many graphic designers have started to create substitute textures for their favorite games.

## Requirements

Before following installation steps you should check :

1. compatibility between the ROM file with your package because many of these packages only work with specific ROM. For example, ROM (U) works but ROM (E) does not work. You should check this looking the readme file, looking on the forum, Google, etc. 

2. get high resolution textures source files to do this installation (directory with *.png files). Sometimes high-texture packs are released as *.hts files but this page is not relevant for this kind of file. 

## Configuration

### Copy the high resolution textures to your Recalbox

Put the high resolution textures folder of each of your games in the following directory:

```text
/recalbox/share/bios/Mupen64plus/hires_texture/game_name/
```

### Activating the Libretro Mupen64Plus_Next emulator:

You need to tell the game in question to start it with the correct emulator.

* After rebooting, go to EmulationStation on your game and press `START`.
* Now go to `MODIFY GAME` > `START WITH`.
* Change the emulator to `LIBRETRO MUPEN64PLUS_NEXT`.
* Close the menus.

### Enabling high resolution textures

Now you have to activate the high resolution textures in the emulator.

* Launch a game.
* When the game starts, go to the RetroArch menu by pressing `Hotkey + B.`
* Press `Back` twice, then go to `Settings` > `Configuration`.

![](/tutorials/games/guides/nintendo-64/hires-textures/hirestextures1en.png){.full-width}

* Enable the `Save configuration on exit` setting.

![](/tutorials/games/guides/nintendo-64/hires-textures/hirestextures2en.png){.full-width}

* Press "Back" twice and go to `Main Menu` > `Quick Menu` > `Options`.

![](/tutorials/games/guides/nintendo-64/hires-textures/hirestextures3en.png){.full-width}

* Then go to `GLideN64`.

![](/tutorials/games/guides/nintendo-64/hires-textures/hirestextures4en.png){.full-width}

* Enable the `Use High-Res textures` parameter.

![](/tutorials/games/guides/nintendo-64/hires-textures/hirestextures5en.png){.full-width}

* Do "Back" twice and go to "Resume".
* Quit the game and start the game that needs to load the high resolution textures.

When the high resolution texture pack and the rom are right, the game will start with the high resolution texture pack.

## Troubleshooting

### You have followed the above instructions and it still doesn't work ?

Maybe it's a name problem...  
Many of these texture packs are provided with the right folder name but not all of them! The folder name must be exactly the internal name of your rom.

>The file name is `Mario Kart 64 (USA).v64` but the internal name of the rom is `MARIOKART64`.
{.is-warning}

### How do I know the internal name of my rom?

On Windows, you can use **Tool64** (Google is also your friend here).

* Start **Tool64**;
* `File` -> `Open`
* Select your rom folder.

![](/tutorials/games/guides/nintendo-64/hires-textures/hirestextures6.png){.full-width}

* Right click on a rom and then click `Rom Properties...`.

![](/tutorials/games/guides/nintendo-64/hires-textures/hirestextures7.png){.full-width}

Look at the `Rom Name` line to get the internal name of the rom.