---
title: Bartop / Arcade configuration
description: Arcade to the house!
published: true
date: 2023-09-19T22:10:36.083Z
tags: arcade, bartop
editor: markdown
dateCreated: 2021-08-06T09:18:39.403Z
---

If you want to use a Raspberry Pi with Recalbox to create a **Bartop** or an **arcade gate**, you can easily configure Recalbox to meet your needs.

## GPIO

You can configure the Recalbox GPIO driver in the file [recalbox.conf.](./../../../basic-usage/getting-started/recalboxconf-file)  
This driver creates two joysticks on the system and directly controlled by the GPIO PINS of the Raspberry Pi.

For this, you don't need a USB hub because you will connect your buttons directly to the [GPIO](./../../../tutorials/controllers/gpio/gpio-controllers)

## Video configuration

Many bartops use old LCD or CRT TV monitors using one of the following connections:

* **VGA monitor**: you will need an HDMI / VGA converter (active converter) costing around 10€. If you are on a 4/3 screen you will need to change the video mode to `global.videomode=default` for all your emulators in the [recalbox.conf](./../../../basic-usage/getting-started/recalboxconf-file) file.
* **DVI monitor**: you will need an HDMI / DVI adapter (passive converter). You will probably need to modify your [recalbox.conf](./../../../basic-usage/getting-started/recalboxconf-file) file based on [this tutorial](./../../video/lcd/dvi-screen).
* **CRT Screen**: You will need a jack to RCA plug with a video cable. You need to change the video mode to `global.videomode=default` for all your games in the [recalbox.conf] file(./../../basic-usage/getting-started/recalboxconf-file) and modify the settings described in [this tutorial](./../../video/crt/crt-screen-with-composite).

## Audio configuration

You can select the audio output device in the [recalbox.conf](./../../../basic-usage/getting-started/recalboxconf-file) file:

* Use `audio.device=alsa_card.0:analog-output` if you want to force the **audio output to the jack.** 
* Use `audio.device=alsa_card.1:hdmi-output-0` if you want to force the **audio output to the HDMI.**

You can also choose the sound output in EmulationStation. Press `START` > `SOUND SETTINGS` and change the value of the `OUTPUT DEVICE` option.

## Menu Setup

You can disable Kodi in the [recalbox.conf](./../../../basic-usage/getting-started/recalboxconf-file) file by setting `kodi.enable=0` in order to remove the shortcut assigned to the `X` button.

You may also want to change the menu settings:

* **For EmulationStation**:
  * By specifying `system.es.menu=bartop`, you will have access to a minimum number of options when you press the `START` button under EmulationStation.
  * If you specify `system.es.menu=none`, no menu will be available. 
* **For emulators** :
  * Setting `system.emulators.specialkey` to `nomenu` will disable menus in emulators.
  * Setting `system.emulators.specialkey` to `none` will disable menus and all special functions in emulators (except the combination to exit a game).