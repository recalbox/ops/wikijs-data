---
title: Libretro A5200
description: 
published: true
date: 2024-07-24T09:25:07.346Z
tags: libretro, atari, 5200, atari-5200, 8.1+, a5200
editor: markdown
dateCreated: 2022-07-22T10:08:34.986Z
---



## ![](/emulators/license.svg) License

This core is under [**GPLv2**](https://github.com/libretro/a5200/blob/master/License.txt) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Features

| Fonctionnalité | Supporté |
| :---: | :---: |

## ![](/emulators/bios.svg) BIOS

### List of mandatory bios

| Filename | Description | MD5 | Provided |
| :---: | :---: | :---: | :---: |
| 5200.rom | 5200 - Required | 281f20ea4320404ec820fb7ec0693b38 | ❌ |

### Location

Put the bios like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 atari5200
┃ ┃ ┃ ┃ ┣ 🗒 **5200.rom**

## ![](/emulators/roms.png) Roms

### Supported extensions

Roms must have the extension:

* .a52
* .atr
* .atr.gz
* .bas
* .bin
* .car
* .cdm
* .xex
* .xfd
* .xfd.gz
* .zip

This system supports compressed roms in .zip/.7z format. However, be careful, it is only an archive.

The files contained in the .zip/.7z must correspond to the extensions mentioned above.
Moreover, each .zip/.7z file must contain only one compressed rom.

### Location

Put the roms like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 atari5200
┃ ┃ ┃ ┃ ┣ 🗒  **game.zip**

>The roms using **No-Intro** format are highly recommended.
{.is-success}

>For more information about roms, go to [this tutorial](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Accessing to options

You can configure various options in two different ways.

* Via RetroArch menu:

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via thie file `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core options

| Options | Options values | Variable (for configuration override) | Variables values |
| --- | --- | --- | --- |
| BIOS (Restart) | `Official` ✅ / `Internal (Altirra OS)` | `a5200_bios` | `official` / `internal` |
| Interframe Blending | `Désactivé` ✅ / `Simple` / `Ghosting (65%)` / `Ghosting (75%)` / `Ghosting (85%)` / `Ghosting (95%)` | `a5200_mix_frames` | `disabled` / `mix` / `ghost_65` / `ghost_75` / `ghost_85` / `ghost_95` |
| Hi-Res Artifacting Mode | `None` ✅ / `blue/brown 1` / `blue/brown 2` / `GTIA` / `CTIA` | `a5200_artifacting_mode` | `none` / `blue/brown 1` / `blue/brown 2` / `GTIA` / `CTIA` |
| High Fidelity POKEY (Restart) | `Désactivé` / `Activé` ✅ | `a5200_enable_new_pokey` | `disabled` / `enabled` |
| Audio Filter | `Désactivé` ✅ / `Activé` | `a5200_low_pass_filter` | `disabled` / `enabled` |
| Audio Filter Level | `5%` / `10%` / `15%` / `20%` / `25%` / `30%` / `35%` / `40%` / `45%` / `50%` / `55%` / `60%` ✅ / `65%` / `70%` / `75%` / `80%` / `85%` / `90%` / `95%` | `a5200_low_pass_range` | `5` / `10` / `15` / `20` / `25` / `30` / `35` / `40` / `45` / `50` / `55` / `60` / `65` / `70` / `75` / `80` / `85` / `90` / `95` |
| Controller Hacks | `Désactivé` ✅ / `Dual Stick` / `Swap Ports` | `a5200_input_hack` | `disabled` / `dual_sticks` / `swap_ports` |
| Pause acts as Reset | `Désactivé` ✅ / `Activé` | `a5200_pause_is_reset` | `disabled` / `enabled` |
| Digital Joystick Sensitivity | `Auto` ✅ / `5%` / `7%` / `10%` / `12%` / `15%` / `17%` / `20%` / `22%` / `25%` / `27%` / `30%` / `32%` / `35%` / `37%` / `40%` / `42%` / `45%` / `47%` / `50%` / `52%` / `55%` / `57%` / `60%` / `62%` / `65%` / `67%` / `70%` / `72%` / `75%` / `77%` / `80%` / `82%` / `85%` / `87%` / `90%` / `92%` / `95%` / `97%` / `100%` | `a5200_digital_sensitivity` | `auto` / `5` / `7` / `10` / `12` / `15` / `17` / `20` / `22` / `25` / `27` / `30` / `32` / `35` / `37` / `40` / `42` / `45` / `47` / `50` / `52` / `55` / `57` / `60` / `62` / `65` / `67` / `70` / `72` / `75` / `77` / `80` / `82` / `85` / `87` / `90` / `92` / `95` / `97` / `100` |
| Analog Joystick Sensitivity | `Auto` ✅ / `5%` / `7%` / `10%` / `12%` / `15%` / `17%` / `20%` / `22%` / `25%` / `27%` / `30%` / `32%` / `35%` / `37%` / `40%` / `42%` / `45%` / `47%` / `50%` / `52%` / `55%` / `57%` / `60%` / `62%` / `65%` / `67%` / `70%` / `72%` / `75%` / `77%` / `80%` / `82%` / `85%` / `87%` / `90%` / `92%` / `95%` / `97%` / `100%` | `a5200_analog_sensitivity` | `auto` / `5` / `7` / `10` / `12` / `15` / `17` / `20` / `22` / `25` / `27` / `30` / `32` / `35` / `37` / `40` / `42` / `45` / `47` / `50` / `52` / `55` / `57` / `60` / `62` / `65` / `67` / `70` / `72` / `75` / `77` / `80` / `82` / `85` / `87` / `90` / `92` / `95` / `97` / `100` |
| Analog Joystick Response | `Linear` ✅ / `Quadratic` | `a5200_analog_response` | `linear` / `quadratic` |
| Analog Joystick Deadzone | `0` / `3` / `5` / `7` / `10` / `13` / `15` ✅ / `17` / `20` / `23` / `25` / `27` / `30` | `a5200_analog_deadzone` | `0` / `3` / `5` / `7` / `10` / `13` / `15` / `17` / `20` / `23` / `25` / `27` / `30` |
| Analog Device | `Analog Stick` ✅ / `Mouse` | `a5200_analog_device` | `analog_stick` / `mouse` |

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/libretro/a5200/](https://github.com/libretro/a5200/)