---
title: Libretro Mednafen_Saturn
description: 
published: true
date: 2024-07-24T18:43:45.889Z
tags: libretro, mednafen, saturn
editor: markdown
dateCreated: 2021-08-03T10:27:00.046Z
---

**Libretro Mednafen_Saturn** is a port of the **Mednafen Saturn** emulator (standalone) to Libretro supporting the Sega Saturn.

## ![](/emulators/license.svg) License

This core is under [**GPLv2**](https://github.com/libretro/beetle-saturn-libretro/blob/master/COPYING) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ❌ | ❌ | ❌ | ✅ | ❌ | ❌ | ✅ | ❌ |

## ![](/emulators/features.png) Features

| Feature | Supported |
| :---: | :---: |
| Restart | ✔ |
| Screenshots | ✔ |
| Saves | ✔ |
| States | ✔ |
| Rewind | ✔ |
| Netplay | ✔ |
| Core Options | ✔ |
| RetroAchievements | ✔ |
| Controls | ✔ |
| Remapping | ✔ |
| Multi-Mouse | ✔ |

## ![](/emulators/bios.svg) BIOS

### List of mandatory bios

| Filename | Description | MD5 | Provided |
| :---: | :---: | :---: | :---: |
| sega_101.bin | Saturn JP BIOS - Required for Japanese games | 85ec9ca47d8f6807718151cbcca8b964 | ❌ |
| mpr-17933.bin | Saturn US/EU BIOS - Required for Europena/America games | 3240872c70984b6cbfda1586cab68dbe | ❌ |
| mpr-18811-mx.ic1 | The King of Fighters '95 - Required for this game | 255113ba943c92a54facd25a10fd780c | ❌ |
| mpr-19367-mx.ic1 | Ultraman: Hikari no Kyojin Densetsu - Required for this game | 1cd19988d1d72a3e7caa0b73234c96b4 | ❌ |

### Location

Put the bios like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 saturn
┃ ┃ ┃ ┃ ┣ 🗒 **sega_101.bin**
┃ ┃ ┃ ┃ ┣ 🗒 **mpr-17933.bin**
┃ ┃ ┃ ┃ ┣ 🗒 **mpr-18811-mx.ic1**
┃ ┃ ┃ ┃ ┣ 🗒 **mpr-19367-mx.ic1**

## ![](/emulators/isos.svg) Isos

### Supported extensions

Isos must have the extension:

* .cue/.bin
* .m3u
* .img/.ccd
* .iso
* .chd

### Location

Put the isos like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 saturn
┃ ┃ ┃ ┃ ┣ 🗒 **game.bin**
┃ ┃ ┃ ┃ ┣ 🗒 **game.cue**

>The isos using **Redump** format are highly recommended.
{.is-success}

>For more information about isos, go to [this tutorial](./../../../tutorials/games/generalities/isos-and-roms)!
{.is-info}

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Accessing to options

You can configure various options in two different ways.

* Via RetroArch menu:

┣ 📁 RetroArch Menu
┃ ┣ 📁 Core options
┃ ┃ ┣ 🧩 Name_option

* Via the file `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core options

| Options | Options values | Variables (for configuration override) | Variables values |
| --- | --- | --- | --- |
| Horizontal Overscan Mask | `0` ✅ / `2` / `4` / `6` / `8` / `10` / `12` / `14` / `16` / `18` / `20` / `22` / `24` / `26` / `28` / `30` / `32` / `34` / `36` / `38` / `40` / `42` / `44` / `46` / `48` / `50` / `52` / `54` / `56` / `58` / `60` | `beetle_saturn_horizontal_overscan` | `0` / `2` / `4` / `6` / `8` / `10` / `12` / `14` / `16` / `18` / `20` / `22` / `24` / `26` / `28` / `30` / `32` / `34` / `36` / `38` / `40` / `42` / `44` / `46` / `48` / `50` / `52` / `54` / `56` / `58` / `60` |
| Initial scanline | `0` ✅ / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `21` / `22` / `23` / `24` / `25` / `26` / `27` / `28` / `29` / `30` / `31` / `32` / `33` / `34` / `35` / `36` / `37` / `38` / `39` / `40` | `beetle_saturn_initial_scanline` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `21` / `22` / `23` / `24` / `25` / `26` / `27` / `28` / `29` / `30` / `31` / `32` / `33` / `34` / `35` / `36` / `37` / `38` / `39` / `40` |
| Last scanline | `210` / `211` / `212` / `213` / `214` / `215` / `216` / `217` / `218` / `219` / `220` / `221` / `222` / `223` / `224` / `225` / `226` / `227` / `228` / `229` / `230` / `231` / `232` / `233` / `234` / `235` / `236` / `237` / `238` / `239` ✅ | `beetle_saturn_last_scanline` | `210` / `211` / `212` / `213` / `214` / `215` / `216` / `217` / `218` / `219` / `220` / `221` / `222` / `223` / `224` / `225` / `226` / `227` / `228` / `229` / `230` / `231` / `232` / `233` / `234` / `235` / `236` / `237` / `238` / `239` |
| Initial scanline (PAL) | `0` ✅ / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `21` / `22` / `23` / `24` / `25` / `26` / `27` / `28` / `29` / `30` / `31` / `32` / `33` / `34` / `35` / `36` / `37` / `38` / `39` / `40` / `41` / `42` / `43` / `44` / `45` / `46` / `47` / `48` / `49` / `50` / `51` / `52` / `53` / `54` / `55` / `56` / `57` / `58` / `59` / `60` | `beetle_saturn_initial_scanline_pal` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `21` / `22` / `23` / `24` / `25` / `26` / `27` / `28` / `29` / `30` / `31` / `32` / `33` / `34` / `35` / `36` / `37` / `38` / `39` / `40` / `41` / `42` / `43` / `44` / `45` / `46` / `47` / `48` / `49` / `50` / `51` / `52` / `53` / `54` / `55` / `56` / `57` / `58` / `59` / `60` |
| Last scanline (PAL) | `230` / `231` / `232` / `233` / `234` / `235` / `236` / `237` / `238` / `239` / `240` / `241` / `242` / `243` / `244` / `245` / `246` / `247` / `248` / `249` / `250` / `251` / `252` / `253` / `254` / `255` / `256` / `257` / `258` / `259` / `260` / `261` / `262` / `263` / `264` / `265` / `266` / `267` / `268` / `269` / `270` / `271` ✅ / `272` / `273` / `274` / `275` / `276` / `277` / `278` / `279` / `280` / `281` / `282` / `283` / `284` / `285` / `286` / `287` | `beetle_saturn_last_scanline_pal` | `230` / `231` / `232` / `233` / `234` / `235` / `236` / `237` / `238` / `239` / `240` / `241` / `242` / `243` / `244` / `245` / `246` / `247` / `248` / `249` / `250` / `251` / `252` / `253` / `254` / `255` / `256` / `257` / `258` / `259` / `260` / `261` / `262` / `263` / `264` / `265` / `266` / `267` / `268` / `269` / `270` / `271` / `272` / `273` / `274` / `275` / `276` / `277` / `278` / `279` / `280` / `281` / `282` / `283` / `284` / `285` / `286` / `287` |
| Enable Horizontal Blend(blur) | `Disabled` ✅ / `Enabled` | `beetle_saturn_horizontal_blend` | `disabled` / `enabled` |
| 6Player Adaptor on Port 1 | `Disabled` ✅ / `Enabled` | `beetle_saturn_multitap_port1` | `disabled` / `enabled` |
| 6Player Adaptor on Port 2 | `Disabled` ✅ / `Enabled` | `beetle_saturn_multitap_port2` | `disabled` / `enabled` |
| Allow Up+Down and Left+Right | `Disabled` ✅ / `Enabled` | `beetle_saturn_opposite_directions` | `disabled` / `enabled` |
| Analog Stick Deadzone | `0` / `5%` / `10%` / `15%` ✅ / `20%` / `25%` / `30%` | `beetle_saturn_analog_stick_deadzone` | `0` / `5%` / `10%` / `15%` / `20%` / `25%` / `30%` |
| Trigger Deadzone | `0` / `5%` / `10%` / `15%` ✅ / `20%` / `25%` / `30%` | `beetle_saturn_trigger_deadzone` | `0` / `5%` / `10%` / `15%` / `20%` / `25%` / `30%` |
| Mouse Sensitivity | `5%` / `10%` / `15%` / `20%` / `25%` / `30%` / `35%` / `40%` / `45%` / `50%` / `55%` / `60%` / `65%` / `70%` / `75%` / `80%` / `85%` / `90%` / `95%` / `100%` ✅ / `105%` / `110%` / `115%` / `120%` / `125%` / `130%` / `135%` / `140%` / `145%` / `150%` / `155%` / `160%` / `165%` / `170%` / `175%` / `180%` / `185%` / `190%` / `195%` / `200%` | `beetle_saturn_mouse_sensitivity` | `5%` / `10%` / `15%` / `20%` / `25%` / `30%` / `35%` / `40%` / `45%` / `50%` / `55%` / `60%` / `65%` / `70%` / `75%` / `80%` / `85%` / `90%` / `95%` / `100%` / `105%` / `110%` / `115%` / `120%` / `125%` / `130%` / `135%` / `140%` / `145%` / `150%` / `155%` / `160%` / `165%` / `170%` / `175%` / `180%` / `185%` / `190%` / `195%` / `200%` |
| Gun Crosshair | `Cross` ✅ / `Dot` / `Off` | `beetle_saturn_virtuagun_crosshair` | `Cross` / `Dot` / `Off` |
| Gun Input Mode | `Lightgun` ✅ / `Touchscreen` | `beetle_saturn_virtuagun_input` | `Lightgun` / `Touchscreen` |
| Cartridge | `Auto Detect` ✅ / `None` / `Backup Memory` / `Extended RAM (1MB)` / `Extended RAM (4MB)` / `The King Of Fighters '95` / `Ultraman: Hikari no Kyojin Densetsu` | `beetle_saturn_cart` | `Auto Detect` / `None` / `Backup Memory` / `Extended RAM (1MB)` / `Extended RAM (4MB)` / `The King Of Fighters '95` / `Ultraman: Hikari no Kyojin Densetsu` |
| Shared Internal Memory (Restart) | `Enabled` / `Disabled` ✅ | `beetle_saturn_shared_int` | `enabled` / `disabled` |
| Shared Backup Memory (Restart) | `Enabled` / `Disabled` ✅ | `beetle_saturn_shared_ext` | `enabled` / `disabled` |
| System Region | `Auto Detect` ✅ / `Japan` / `North America` / `Europe` / `South Korea` / `Asia (NTSC)` / `Asia (PAL)` / `Brazil` / `Latin America` | `beetle_saturn_region` | `Auto Detect` / `Japan` / `North America` / `Europe` / `South Korea` / `Asia (NTSC)` / `Asia (PAL)` / `Brazil` / `Latin America` |
| BIOS Language | `English` ✅ / `German` / `French` / `Spanish` / `Italian` / `Japanese` | `beetle_saturn_autortc_lang` | `english` / `german` / `french` / `spanish` / `italian` / `japanese` |
| CD Image Cache (Restart) | `Disabled` ✅ / `Enabled` | `beetle_saturn_cdimagecache` | `disabled` / `enabled` |
| Mid-frame Input Synchronization | `Disabled` ✅ / `Enabled` | `beetle_saturn_midsync` | `disabled` / `enabled` |
| Automatically set RTC on game load | `Enabled` ✅ / `Disabled` | `beetle_saturn_autortc` | `enabled` / `disabled` |

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/libretro/beetle-saturn-libretro/](https://github.com/libretro/beetle-saturn-libretro/)
* **Libretro documentation**: [https://docs.libretro.com/library/beetle_saturn/](https://docs.libretro.com/library/beetle_saturn/)