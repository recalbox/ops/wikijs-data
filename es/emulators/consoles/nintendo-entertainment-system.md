---
title: Nintendo Entertainment System
description: Nintendo Entertainment System
published: true
date: 2024-07-26T07:56:51.888Z
tags: system, nintendo, nes, entertainment, consolas
editor: markdown
dateCreated: 2023-09-03T20:03:19.470Z
---

![](/emulators/consoles/nes.svg){.align-center}

## Detalles técnicos

* **Fabricante**: Nintendo
* **Año de lanzamiento**: 1983
* **Cantidad vendida**: 61,91 millones
* **Juego más vendido**: Super Mario Bros.
* **Procesador**: 8-bit MOS 6502 @ 1.79MHz
* **RAM**: 2kB / VRAM: 2kB
* **Vídeo**: PPU (unidad de procesamiento de imágenes) de 8 bits.
* **Resolución**: 256 x 240 píxeles, 25 colores (en una paleta de 64 colores)
* **Chip de sonido**: sonido PSG (2 ondas cuadradas, 1 onda triangular, 1 ruido blanco)
* **Tamaño del cartucho** : 8kB - 1MB

## Presentación

La **Nintendo Entertainment System**, abreviada **NES**, también conocida comúnmente como **Nintendo** en Francia, es una videoconsola de la generación de 8 bits fabricada por la empresa japonesa Nintendo y distribuida a partir de 1985 (1987 en Europa). Su equivalente japonesa es la **Family Computer** o **Famicom**, lanzada unos años antes, en 1983. En Corea del Sur, la NES se conocía como **Hyundai Comboy** y en la India como **Tata Famicom**.

La consola fue un éxito mundial, contribuyó a revitalizar la industria del videojuego tras el crack de 1983 y sentó las bases de las consolas posteriores, desde el diseño de los juegos hasta los procedimientos de gestión.

Super Mario Bros. fue el juego más vendido de la consola. Tuvo tanto éxito que a menudo justificaba la compra de la consola por sí solo, convirtiéndose en un **killer game** por derecho propio.

La Nintendo Entertainment System es la 13ª videoconsola más vendida de todos los tiempos, con 61,91 millones de unidades vendidas.

## Emuladores

[Libretro FBNeo](libretro-fbneo)
[Libretro FCEUmm](libretro-fceumm)
[Libretro FCEUnext](libretro-fceunext)
[Libretro Mesen](libretro-mesen)
[Libretro Nestopia](libretro-nestopia)
[Libretro Quicknes](libretro-quicknes)