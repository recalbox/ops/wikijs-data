---
title: Primer arranque y configuración
description: Para configurar vuestro sistema Recalbox por la primera vez
published: true
date: 2024-04-20T09:38:30.263Z
tags: configuración, primera utilización
editor: markdown
dateCreated: 2023-04-19T13:47:16.531Z
---

## Primeros pasos

### Primer arranque

Una vez finalizada la instalación de Recalbox, la primera etapa consiste en enchufar vuestro ordenador al televisor via un cable HDMI. 

>La mayoria de los mandos funcionan directamente sin hacer nada en cuanto se enchufan. Sin embargo, si queréis configurar un mando USB, o enchufar un teclado USB, consultad esta sección [Configuración de los mandos](#configuration-dune-manette).
{.is-info}

### Cambiar el idioma de Recalbox

En la pantalla de selección de sistemas, apretad el botón `START`. Seleccionad `SYSTEM SETTINGS` > `LANGUAGE`, seleccionad `ESPAÑOL` et volved hacia atrás. Recalbox indicará que necesita reiniciar. Una vez reiniciado, todos los menus de navigación estarán en español.

###  Apagar Recalbox

En la pantalla de selección de sistemas, apretad el botón `SELECT` y seleccionad `APAGAR`. Esperad un poco para que el sistema se apage totalmente antes de desenchufarlo o antes de retirar la tarjeta SD.

### Juegos préinstalados

Recalbox viene preinstalado con varios juegos libres de derechos de autor (homebrew) y/o varias demos distribuidas en varios sistemas diferentes.

## Configuración de un mando

### Añadir y configurar un mando USB

Recalbox permite añadir varios mandos. La mayoría de los mandos son compatibles (ver la [lista de compatibilidades](./../hardware-compatibility/compatible-devices/controllers)).

Una vez enchufado el mando (o enparejado a través de Bluetooth), apretad el botón `START`, seleccionad `CONFIGURACIÓN DE MANDOS` y seguid las instrucciones.
![](/basic-usage/first-use-and-configuration/padconfig1.png)

Seleccionad `CONFIGURACIÓN DE MANDOS` y seguid las instrucciones.

![](/basic-usage/first-use-and-configuration/padconfig2.png)

Apretad una tecla para comenzarla configuración.

![](/basic-usage/first-use-and-configuration/padconfig3.png)

Mantened pulsado cualquier botón para configurar el mando.

![](/basic-usage/first-use-and-configuration/padconfig4.png)

A partir de aquí se pueden configurar cada uno de los botones del mando. Los nombres de los botones son los nombres utilizados en los mandos Super Nintendo :

![Manette Super Nintendo (SNES)](/basic-usage/first-use-and-configuration/snespad.jpg)

>Los botones L1, R1, L2, R2, L3 y R3 se basan en los mandos Playstation.
{.is-info}

>**HOTKEY**
>El último botón que se configura, llamado **HOTKEY**, es el que servira para activar los [comandos especiales](./getting-started/special-commands) dentro de los emuladores :
>
>En los mandos Xbox 360 y Xbox One, el botón **Hotkey** se encuentra mapeado como `HOME`.
>En los mandos PS3 y PS4, el botón **Hotkey** se encuentra en `PS`.
>
>Si el mando no tiene un botón dedicado, se aconseja asignar la **Hotkey** al botón `Select`.
{.is-warning}

>Para saltar una configuración de botón (excepto **Hotkey**), apretad el botón `Abajo` y se pasa a la siguiente opción.
{.is-info}

>Los mandos con 6 botones (SNES, arcade, ...) utilizan por defecto una correspondencia con el mando de la SNES (ver aquí arriba).
>Los mandos con 2 botones (NES, PC Engine, Gameboy, ...) sólo utilizan los botones `A` y `B`.
{.is-warning}

![](/basic-usage/first-use-and-configuration/padconfig5.png)

![](/basic-usage/first-use-and-configuration/padconfig6.png)

### Asignación del mando

* Al terminar la configuración del mando, nos encontramos con el menu que permite de asignar un mando a un jugador. 
* Seleccionad `JUGADOR 1` para asignar un mando al primer jugador.

![](/basic-usage/first-use-and-configuration/padassign1.png)

* Seleccionad un mando.

![](/basic-usage/first-use-and-configuration/padassign2.png)

* Vuestro mando ha sido configurado.

![](/basic-usage/first-use-and-configuration/padassign3.png)

### Utilización de un teclado

Si no podéis configurar vuestro mando, podéis intentar conectar un teclado USB. [En este enlace](./../tutorials/others/emulationstation-use-with-keyboard-and-mouse) encontraréis las teclas utilizadas por Recalbox.

### Mando PS3

Para **asociar** un mando PS3 :

* Conectad primero el mando a un puerto USB y esperad 10 segundos.
* A continuación, podéis desconectar el mando y apretad el botón **PS** durante 4 segundos para lanzar la conexión bluetooth.

>**Nota :**
>Ciertas copias asiáticas de mandos PS3 Dualshock 3 (como GASIA o SHANWAN) pueden necesitar ajustes de [ciertos parámetros](./../tutorials/controllers/controllers/ps3-controllers-drivers).
{.is-warning}

>**Atención :**
>En caso de dudas sobre el consumo de energía del mando, se recomienda evitar recargar el mando directamente a travès del Raspberry Pi porque puede provocar problemas de estabilidad.
>
>Si quereis jugar en bluetooth, enchufad el mando al Raspberry Pi unicamente para lanzar el proceso de aparejamiento descrito más arriba.
{.is-danger}

Si queréis utilizar el mando en modo USB con un cable, se aconseja desactivar el driver de bluetooth PS3 dentro del fichero recalbox.conf mediante la línea siguiente : `controllers.ps3.enabled=0`.

Recuerda que la configuración de los mandos en Recalbox se base en la configuración de los botones de un mando SNES :

| Mando PS3 | Mando SNES |
| :---: | :---: |
| X | B |
| ◯ | A |
| ⬜ | Y |
| △ | X |

>**Información :**
>Por defecto, **HOTKEY** se asocia al botón **PS** (el botón que se encuentra justo en el medio del mando)
>Para más información sobre HOTKEY, ver la página de los [comandos especiales](./getting-started/special-commands).
{.is-info}

### Mando Xbox 360 

>**Nota :**
>Los mandos bluetooth Xbox 360 necesitan un USB específico para la recepción de la conexión bluetooth.
{.is-warning}


Recuerda que la configuración de los mandos en Recalbox se base en la configuración de los botones de un mando SNES :

| Mando Xbox 360 | Mando SNES |
| :---: | :---: |
| A | B |
| B | A |
| X | Y |
| Y | X |

>**Información :**
>Por defecto, **HOTKEY** se asocia al botón **PS** (el botón que se encuentra justo en el medio del mando)
>Para más información sobre HOTKEY, ver la página de los [comandos especiales](./getting-started/special-commands).
{.is-info}

### Añadir un mando Bluetooth

Para añadir un mando Bluetooth :

* Ponez el mando en modo _apareamiento_
* A continuación, apretad `START` y elegid `REGLAGE MANETTES`
* A continuación, `ASOCIAR UN MANDO BLUETOOTH`.

Una **lista de mandos** detectados aparece !

* Basta con seleccionar vuestro mando y el mando se emparejará.
* A continuación, podéis pasar a la configuración de los botones del mando si necesitáis personalizarlos.

>**Information :**  
>Para los mandos **8bitdo** consultad la página [**8bitdo en Recalbox**](./../hardware-compatibility/compatible-devices/8bitdo-on-recalbox).
{.is-info}


### Controlador GPIO

Podéis conectar botones y joysticks arcade directamente utilizando los pines GPIO del Rasbperry Pi (ver la página [**los controladores GPIO**](./../tutorials/controllers/gpio/gpio-controllers)).
Podéis igualmente conectar los mandos originales de PSOne, Nes, Snes, Megadrive, ... (ver **controladores DB9** y **controladores Gamecon**).

### Mandos virtuales

>Los mandos virtuales no son accesibles en los Raspberry Pi0 y Pi1 por defecto.
{.is-info}

Con el projecto "Miroof Virtual Gamepads", se pueden añadir 4 mandos utilizando teléfonos moviles /tabletas !

Para ello, arrancad el navigador web de vuestro smartphone y escribid la dirección IP de vuestra Recalbox junto con el puerto de comunicación (el puerto `:8080`).

>**Información :** 
>La dirección IP de vuestra Recalbox se encuentra dentro del menu `OPCIONES DE RED`
{.is-info}

![Zonas activas en los mandos virtuales](/basic-usage/first-use-and-configuration/virutalgamepad_touch_zones.png)

## Utilización de un almacenamiento externo.

>**Información :**
>Podéis de una forma muy sencilla **Utilizar un periférico USB de almacenamiento** (llave USB, disco duro externo auto alimentado, etc.) para almacenar las roms, los ficheros personales, etc...
>
>**Con este método**, el **sistema** (de la tarjeta SD) y la **partición share** (en el périphérique externo) **se encuentran separados.**
>  
>De esta forma, **si alguna vez necesitáis reinstalar el sistema**, con este método podéis **conservar vuestras roms y datos personales.**
>Depues de una reinstalación del sistema, basta con enchufar de nuevo el periférico externo, **selecionarlo en las opciones del sistema, y jugar.**
{.is-info}

### El formateo de vuestro soporte

En primer lugar, sólo se aceptan los siguientes sistemas de ficheros :
**FAT32, EXFAT, EXT4 ou NTFS.**

>**Nota :**
>
>Se aconseja vivamente utilizar el sistema de ficheros **EXFAT**.
{.is-info}

>**Atención :**
>
>La tasa de transferencia de ficheros puede **ser lenta** en el caso de **NTFS.**
>Tened en cuenta de utilizar un **sistema de ficheros** que **sea compatible con el sistema operativo de vuestro PC.**
{.is-danger}

| Formatos | Talla máxima | Talla máxima de un fichero | Numéro máximo de ficheros | Modo lectura/escritura |
| :---: | :---: | :---: | :---: |  :---:|
| Fat | 2Tib | 4Gib (4,2949Gb) | Supérieur à **250 millions** | Windows : &#x2705; Linux : &#x2705; |
| NTFS | 256Tib (2 815,8492Tb) | 16 Tib (17,5921Tb) | 4 294 967 295 | Windows : &#x2705; Linux : &#x2705; |
| exFAT | 128Pib (144 115,1880Tb) | 128Pib (144 115,1880Tb) | 2 796 202 por directorio | Windows : &#x2705; Linux : &#x2705; |
| ext4 | 1 Eio (1 152 921,5046To - limitado a 16 Tib por e2fsprogs) | 16 Tib (17,5921Tb) | 4 mil millones | Windows : &#x274C; Lectura/Escritura via ext2fsd Linux : &#x2705; |

>**Atención :**
>
>Recalbox **no formatea** vuestro periférico.
{.is-danger}

### Configuración

Para configurar Recalbox para **utilizar un periférico de almacenamiento USB.**

* Enchufad vuestro periférico a vuestra Recalbox, y encendedla.
* Una vez en la interfaz de Recalbox, apretad el botón `INICIO` de vuestro mando, id a las `OPCIONES DEL SISTEMA` y dentro de `MEDIA DE ALMACENAMIENTO`.
* A continuación, seleccionad vuestro periférico de la lista, validez y esperad que el sistema reinicie.

>**Información :**
>
>**Durante esta fase de reinicio** Recalbox va a **créer en la raíz** de vuestro periférico **una nueva carpeta** llamada **"recalbox"** que contiene todo el arbol de vuestra **partición "share".**
{.is-info}

### Resolución de problemas

#### Disco duro invisible

Que hacer si despuès de reiniciar, Recalbox no encuentra el disco duro ?

>**Informaciones :** 
>
>De vez en cuando, cuando se selecciona el periférico en EmulationStation y se reinicia, Recalbox no consigue crear el sistema de ficheros. Cuando esto ocurre, Recalbox sigue utilizando los ficheros de la tarjeta SD. Puede ocurrir cuando el disco duro tarda en arrancar.
{.is-info}

Lo que podéis probar:

* Conectaros via [SSH](./../tutorials/system/access/root-access-terminal-cli)
* Montad la [partición de arranque](./../tutorials/system/access/remount-partition-with-write-access) en lecture/escritura.
* Escribid las comandas siguientes : `cd /boot` et `nano recalbox-boot.conf`
* Añadir la línea `sharewait=30`. El valor indica segundos.
* Enregistrad con `Ctrl+X`,`Y`,`Enter`.
* Escribid `reboot`y validez para que Recalbox se reinicie.

## Red

### Configurar via un cable RJ45

* Enchufaz el cable RJ45 y ya está, no hay nada màs a configurar.

### Configurar la Wifi

* En la interfaz general, apretad `Inicio` > `OPCIONES DE RED`. Para activar la wifi, ponez la opción `ACTIVAR LA WIFI` en **On**.
* A continuación debéis configurar la wifi vía WPS. En vuestra box Internet, existe un botón exterior o en la interfaz de gestión que permite activar la función WPS. Cuando se aprieta este botón, la funcionalidad de WPS se activa durante 2 minutos.
* En la interfaz de Recalbox, seleccionad `CONEXION WPS` en la parte de abajo del menu. Esta opción escanea el entorno buscando conexiones WPS activas.
* Una vez el proceso de comunicación WPS establecido y finalizado, la configuración de red sera enregistrada en vuestro sistema.
