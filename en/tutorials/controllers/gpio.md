---
title: GPIO
description: 
published: true
date: 2021-08-07T15:14:09.929Z
tags: gpio
editor: markdown
dateCreated: 2021-08-07T13:24:11.070Z
---

You will find here tutorials to use the GPIO port of your Raspberry Pi in many different ways.

Here are the available tutorials:

[Connect a comparative coin mechanism to a Raspberry Pi](connect-a-comparative-coin-mechanism-to-a-raspberry-pi)
[Enable GPIO button to close the emulator by pressing one button](enable-gpio-button-close-emulator)
[GPIO controllers](gpio-controllers)
[Keyboard touches via GPIO](keyboard-touches-via-gpio)
[Play with your original controller with Gamecon](use-your-original-controller-with-gamecon)
[Set until 4 features on each GPIO port](set-until-4-features-on-each-gpio-port)