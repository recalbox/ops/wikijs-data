---
title: Uzebox
description: 
published: true
date: 2024-07-26T08:24:54.510Z
tags: uzebox, motor de juego
editor: markdown
dateCreated: 2023-09-04T07:19:10.536Z
---

![](/emulators/fantasy/uzebox.svg){.align-center}

## Datos técnicos

* **Fabricante**: Belogic Software
* **Año de lanzamiento**: 2008
* **CPU**: microcontrolador Amtel ATmega644 a 28,6 Mhz
* **RAM**: 4 K
* **Memoria de programa**: 64 K
* **Sonido**: síntesis de tabla de ondas de 5 canales, mono de 8 bits, mezclado a ~15Khz y salida vía PWM
* **Resolución**: 9 modos de vídeo hasta 360x224 píxeles sólo en modo mosaico
* **Color**: 256 colores simultáneos dispuestos en espacio de color 3:3:2
* **Soporte**: tarjeta SD

## Presentación 

La Uzebox es una videoconsola retro-minimalista de código abierto. Se basa en un microcontrolador AVR de 8 bits de propósito general fabricado por Atmel. La particularidad del sistema es que está basado en un motor alimentado por interrupciones y no tiene frame buffer. Funciones como la generación de sincronización de vídeo, el renderizado de mosaicos y la mezcla de música se realizan en tiempo real en el software mediante una tarea en segundo plano, de modo que los juegos pueden desarrollarse fácilmente en C.  
El objetivo del diseño era ser lo más sencillo posible y, al mismo tiempo, tener un sonido y unos gráficos suficientemente buenos y dejar suficientes recursos para implementar juegos interesantes. Se hizo hincapié en que fuera fácil y divertido de montar y programar para todos los aficionados. El diseño final contiene sólo dos chips: un ATmega644 y un conversor AD725 RVB a NTSC.

Existen numerosas versiones comerciales, o cuando estén disponibles: Uzebox AVCore de Embedded Engineering llc y Fuzebox de Adafruit Industries. También existe el kit de lujo Uzebox y el EUzebox, una versión con interfaz SCART. ¡Hazte con uno si no sabes nada de electrónica!

Características :

* Interrupción: no es necesario contar ciclos, la mezcla de sonido y la generación de vídeo se realizan en segundo plano.
* Motor de sonido de 5 canales: el subsistema de sonido consta de 3 canales de tabla de ondas, 1 canal de ruido y 1 canal PCM. El sonido es mono de 8 bits, mezclado a ~ 15Khz
* 256 colores simultáneos dispuestos en un espacio de color 3:3:2 (rojo: 3 bits, verde: 3 bits, azul: 2 bits).
* Resolución: 9 modos de vídeo que ofrecen hasta 360 x 224 píxeles (sólo mosaicos, mosaicos y sprites y modos de vídeo bitmap).
* Desplazamiento a pantalla completa en algunos modos de vídeo.
* Sprites: hasta 32 sprites simultáneos
* Entradas compatibles: dos entradas de joypad compatibles con SNES (incluido el ratón SNES)
* Entrada MIDI: con un secuenciador musical, se puede crear música directamente en la consola.
* API SD / MicroSD y FAT16
* Extensión 'Uzenet' para ESP8266 wifi y 1 MB SPI RAM
* Interfaz de teclado PS/2
* GameLoader: gestor de arranque 4K para flashear juegos desde una tarjeta SD estándar con formato FAT16.
* Emulador multiplataforma compatible con GDB para facilitar el desarrollo.
* Varias herramientas para convertir MIDI, archivos de audio y gráficos a archivos incluidos

Las fuentes incluyen juegos totalmente funcionales, demos, herramientas de generación de contenidos e incluso un emulador multiplataforma.

## Emuladores

[Libretro uzem](libretro-uzem)