---
title: Setting up image size by using overscan on TFT
description: 
published: false
date: 2024-07-25T19:12:01.136Z
tags: tft, image, size, overscan
editor: markdown
dateCreated: 2021-08-06T17:29:54.731Z
---

The overscan option can be enabled if you have black borders on the image or if your image is cropped.

## Activation

* EmulationStation menu (`START` button on your controller)
* `ADVANCED SETTINGS`
* Activate the `OVERSCAN` option

>You must reboot your Raspberry Pi after changing this option.
{.is-warning}

## Configuration

* If it doesn't match your screen, you can change the settings for each edge in your [recalbox-user-config.txt](./../../system/modification/configtxt-configuration) file:

```ini
disable_overscan=0
overscan_left=24
overscan_right=24
overscan_top=24
overscan_bottom=24
```

The value specifies the number of pixels to be ignored at the edge of the screen. Increase this value if the image extends beyond the edge of the screen; decrease it if there is a black border between the screen edge and the image.

* If these settings are not applied correctly in the emulators or EmulationStation, try adding this:

```ini
overscan_scale=1
```