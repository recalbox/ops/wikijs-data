---
title: 6. 📋 MAIS INFORMAÇÕES
description: Como contribuir, licenças, links úteis...
published: true
date: 2023-07-18T12:07:28.953Z
tags: 
editor: markdown
dateCreated: 2021-06-24T23:31:31.937Z
---

Você encontrará aqui a apresentação geral do Recalbox, quem somos, as licenças de uso, bem como a possibilidade de contribuir para o projeto de várias maneiras!