---
title: Pico
description: 
published: true
date: 2024-07-24T16:01:01.446Z
tags: consoles, sega, pico
editor: markdown
dateCreated: 2024-06-22T23:05:55.294Z
---

![](/emulators/consoles/pico.svg){.align-center}

## Fiche technique

* **Fabricant :** Sega
* **Année de sortie :** 1993
* **Processeur :** 16-Bit Motorola 68000 @ 7.6 MHz
* **Résolution :** 320 x 224 pixels NTSC, 320 x 240 PAL

## Présentation

Le Sega Pico, également connu sous le nom de Kids Computer Pico, est une console de jeux vidéo éducatifs de Sega Toys. La Pico est sortie en juin 1993 au Japon et en novembre 1994 en Amérique du Nord et en Europe, puis en Chine en 2002.

Commercialisée sous le nom de "edutainment", la Pico était principalement destinée aux jeux vidéo éducatifs pour les enfants de 3 à 7 ans. Les versions du Pico étaient axées sur l'éducation des enfants et comprenaient des titres soutenus par des personnages animés sous licence, y compris la série Sonic the Hedgehog de Sega.

Bien que la Pico ait été vendue sans interruption au Japon jusqu'à la sortie de la Beena, en Amérique du Nord et en Europe, la Pico a connu moins de succès et a été abandonnée au début de 1998, avant d'être rééditée par Majesco Entertainment. Au total, Sega revendique la vente de 3,4 millions de consoles Pico et de 11,2 millions de cartouches de jeu, et de plus de 350 000 consoles Beena et de 800 000 cartouches. L'Advanced Pico Beena, sorti au Japon en 2005, lui a succédé.

## Émulateurs

[Libretro PicoDrive](libretro-picodrive)