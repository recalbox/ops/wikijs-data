---
title: PC-Engine
description: NEC PC-Engine
published: true
date: 2024-07-26T07:58:10.950Z
tags: nec, pc-engine, consolas
editor: markdown
dateCreated: 2023-09-03T20:04:50.299Z
---

![](/emulators/consoles/pcengine.svg){.align-center}

## Datos técnicos

* **Fabricante**: NEC
* **Año de lanzamiento**: 1987
* **Cantidad vendida**: 10 millones
* **Procesador**: HuC6280 8 bits 7,16 Mhz
* **RAM**: 8 Kbytes
* **Unidad de procesamiento de imagen**: 8 bits
* **RAM de vídeo**: 64 Kbytes
* **Soporte**: HU-Card, SHU-Card, CD-ROM, Super-CD-ROM

## Presentación

El **PC-Engine**, conocido como **TurboGrafx-16** en Norteamérica, es una videoconsola de 8 bits fabricada por NEC y comercializada a partir de 1987 en Japón. Fuera de su país natal y de Norteamérica, la gama de consolas tuvo un perfil relativamente bajo, con escasa o nula distribución.

Adelantada a su tiempo, la PC-Engine tiene un procesador central de 8 bits y un procesador gráfico de 16 bits, por lo que se considera una consola de cuarta generación. Diseñada para ser modular y actualizable, fue también la primera consola de juegos de la historia en admitir CD-ROM, con una unidad opcional, a partir de diciembre de 1988. Tras el primer modelo original, pasó por una serie de versiones, aceptando finalmente cuatro formatos de juego diferentes: HuCard, CD-ROM², Super CD-ROM² y Arcade CD-ROM².

## Emuladores

[Libretro FBNeo](libretro-fbneo)
[Libretro Mednafen_PCE_FAST](libretro-mednafen_pce_fast)
[Libretro Mednafen_SuperGrafx](libretro-mednafen_supergrafx)