---
title: Arcade
description: Sistemas arcade o de emulación de máquinas recreativas
published: true
date: 2024-06-21T10:34:50.458Z
tags: arcade, sistemas, recreativas, maquinas
editor: markdown
dateCreated: 2023-09-01T20:51:17.956Z
---

![](/emulators/arcade/arcade-logo.png)

![](/emulators/arcade/arcade-bartop.svg)

## ATENCIÓN.

Entramos en una parte bastante técnica de Recalbox
**VAMOS A HACERLO SENCILLO.**

Como para las consolas, el arcade de Recalbox se separa en dos elementos:
los emuladores *(incluídos y actualizados en vuestra Recalbox)* y los "juegos", que debéis buscar y añadir.

...EXCEPTO QUE...
A diferencia de las consolas, el arcade no ejecuta "juegos" sino "sistemas completos". 
*(= se simula/reproduce/ejecuta el conjunto de la máquina recreativa y no únicamente los datos del juego)*

![](/advanced-usage/arcade/board-to-zip.png)

La emulación de las máquinas recreativas (arcade) está en contínua evolución, incluso hoy en día.
Los emuladores no cesan de mejorarse, y las copias o "dumps" de las máquinas también.

En cada nueva versión de Recalbox incluimos las últimas versiones de los emuladores. Prestad atención a añadir LAS BUENAS VERSIONES DE vuestros juegos de arcade. *(llamados "ROMSETS")*, si las versiones de los emuladores de arcade cambian, de otra forma podéis encontraros con juegos que no funcionan más.

[**Plus d'infos sur l'émulation Arcade dans Recalbox ICI**](./../advanced-usage/arcade-in-recalbox)

## ARCADE "POR DEFECTO"

| Emulador | Romset |
| :---: | :---: |
| PiFBA | ROMSET FBA 0.2.96.71 |
| MAME | ROMSET MAME 0.78-0.188 |
| FBNEO | ROMSET FBNeo 1.0.0.03 |
| NEO-GEO | ROMSET FBNeo 1.0.0.03 |
| NAOMI | ROMSET MAME 0.258 |
| NAOMI 2 | ROMSET MAME 0.258 |
| NAOMI GD | ROMSET MAME 0.258 |
| ATOMISWAVE | ROMSET MAME 0.258 |
| SEGA MODEL 3 | ROMSET MAME 0.258 |
 
✅ *Atención : dependiendo de vuestro sistema, algunos de los sistemas de Arcade pueden no estar disponibles.*
*[**Verificad la compatibilidad de vuestro material aquí.**](./../hardware-compatibility)*
 
✅ *¿Qué romset elegir en función de vuestro hardware?*
*[**Verificad la compatibilidad de los diferentes Romsets en Recalbox**](./../tutorials/games/generalities/isos-and-roms/romsets-for-recalbox)*
 
✅ *Para los usuarios avanzados, es posible cambiar el emulador MAME por defecto, si queréis lanzar juegos recientes. Deberéis obtener un ROMSET MAS RECIENTE*
*[**TUTO para cambiar el emulador por defecto aquí.**](./../tutorials/games/guides/nintendo-64/game-core-compatibility)*
 
 ## ARCADE "USUARIO AVANZADO"
 
*Emuladores MAME alternativos :*
 
 | Emulador | Romset |
 | :---: | :---: |
 | LIBRETRO-MAME 2000 | ROMSET MAME 0.35B7 |
 | LIBRETRO-MAME 2003 | ROMSET MAME 0.78 |
 | LIBRETRO-MAME 2003-Plus | ROMSET MAME 0.78-0.188 |
 | LIBRETRO-MAME 2010 | ROMSET MAME 0.139 |
 | LIBRETRO-MAME 2015 | ROMSET MAME 0.160 |
 | LIBRETRO-MAME | ROMSET MAME 0.258 |
 
[**Más información sobre la emulación de Arcade avanzada AQUÍ**](./../advanced-usage/arcade-in-recalbox)


### LOS PROBLEMAS MÁS FRECUENTES

| Problema | ROM INCORRECTA | EMULADOR INCORRECTO | FALTA EL CHD  | DUMP ERRONEO | MAPPING ERRONEO |
| :--- | :---: | :---: | :---: | :---: | :---: |
| **Mi juego no se lanza**  | ✅ | ✅ | ✅ | ✅ |  |
| **Problema de sonido en el juego** |  | ✅ |  | ✅ |  |
| **El juego presenta bugs**  |  | ✅ |  | ✅ |  |
| **El juego se lanza pero las teclas no son buenas** |  |  |  |  | ✅ |


[**Más información sobre la emulación de Arcade avanzada AQUÍ**](./../advanced-usage/arcade-in-recalbox)
