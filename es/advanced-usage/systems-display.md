---
title: Visualización de sistemas
description: Funcionamiento y modificaciones de la visualización de sistemas
published: true
date: 2023-08-29T13:51:13.896Z
tags: emulationstation, sistemas, visualización, pantalla
editor: markdown
dateCreated: 2023-08-29T13:51:13.896Z
---

## Funcionamiento de la pantalla de sistemas

La pantalla de sistemas del menú de Recalbox se controla a partir del fichero `systemlist.xml`. Este fichero se encuentra en la siguiente ruta `/recalbox/share_init/system/.emulationstation/systemlist.xml`.

Contiene los nombres de los diferentes sistemas soportados en Recalbox. Se construye de la siguiente forma:

```xml
<?xml version="1.0"?>
<systemList>
  <defaults command="python /usr/bin/emulatorlauncher.pyc %CONTROLLERSCONFIG% -system %SYSTEM% -rom %ROM% -emulator %EMULATOR% -core %CORE% -ratio %RATIO% %NETPLAY%"/>
  <system uuid="b58b9072-0e44-4330-869d-96b72f53de1f" name="3do" fullname="Panasonic 3DO" platforms="3do">
    <descriptor path="%ROOT%/3do" theme="3do" extensions=".iso .chd .cue"/>
    <scraper screenscraper="29"/>
    <properties type="console" pad="mandatory" keyboard="no" mouse="no" lightgun="optional" releasedate="1993-10" retroachievements="1"/>
    <emulatorList>
      <emulator name="libretro">
        <core name="opera" priority="1" extensions=".chd .cue .iso" netplay="0" compatibility="high" speed="high"/>
      </emulator>
    </emulatorList>
  </system>
  <system>
      [. . .]
  </system>
  [. . .]
</systemList>
```

La visualización de los sistemas respeta el orden del fichero. El fichero contiene la configuración de cada sistema.

## Modificación del orden de presentación

>**NO HAY QUE MODIFICAR** el fichero `systemlist.xml` original (que se encuentra dentro del directorio `share_init`). En caso de tener problemas con las modificaciones que vamos a describir más adelande, este fichero será el único recurso disponible para volver a hacer funcionar Recalbox correctamente.
{.is-warning}

La modificación del orden de presentación de los sistemas se debe realizar únicamente sobre el fichero `systemlist.xml` de la siguiente ruta: `/recalbox/share/system/.emulationstation/systemlist.xml`

De fábrica este fichero no existe. Hay que copiar el fichero original, o crear un fichero desde cero. Una vez que habéis creado el fichero, se puede modificar para colocar los sistemas en el orden apropiado. La configuración de los sistemas se leerá a partir del fichero `systemlist.xml` original, pero el orden de presentación se tomará a partir del nuevo fichero que acabamos de crear.

Si en este nuevo fichero uno de los sistemas no existe o está mal configurado, se tomará como referencia el fichero original. En el nuevo fichero, sólo una clave es obligatoria: **UUID**, todas las demás son opcionales. Es decir, como mínimo el fichero debe ser construido de la siguiente forma:

```xml
<?xml version="1.0"?>
<systemList>
  <system uuid="62f2cbed-5bcb-46d8-bca9-daaa36613c7a"/> <!-- nes -->
  <system uuid="0a7fd1d3-9673-44ab-bfef-5bb3c8a0d79a"/> <!-- fds -->
  <system uuid="3df92492-d69a-48f8-8e14-9a62bd9805a6"/> <!-- snes -->
  <system uuid="99e312bc-7602-4038-a871-e53c17fd1d76"/> <!-- satellaview -->
  [. . .]
</systemList>
```

## Añadir un sistema « Custom »


>**NO HAY QUE MODIFICAR** el fichero `systemlist.xml` original (que se encuentra dentro del directorio `share_init`). En caso de tener problemas con las modificaciones que vamos a describir más adelande, este fichero será el único recurso disponible para volver a hacer funcionar Recalbox correctamente.
{.is-warning}

La manipulación que sigue no sirve para añadir un nuevo emulador a Recalbox sino para añadir una nueva entrada en la pantalla de todos los sistemas de EmulationStation.

Es posible combinar la modificación anterior sobre el orden de presentación de los sistemas junto con la adición de uno o varios sistemas custom.

Como para la modificación del orden de presentación, la adición de nuevos sistemas "custom" se realiza únicamente a partir del fichero `systemlist.xml` presente en la siguiente ruta: `/recalbox/share/system/.emulationstation/systemlist.xml`.


De fábrica este fichero no existe. Hay que copiar el fichero original, o crear un fichero desde cero.

Si existe un problema con un sistema, el fichero original es prioritario. Para los nuevos sistemas, todas las claves son obligatorias. Para crear un nuevo sistema lo más sencillo es copiar un sistema existente y modificar sus claves.

- _**« fullname »**_ : Sirve para dar un nombre al nuevo sistema.
- _**« path »**_ : Permite indicar la carpeta que contiene las roms del nuevo sistema.
- _**« theme »**_ : Indica el thème que se aplicará. Antes de esto, hay que crear el corresopndiente thème (logo, fondo, ...)

**Cualquier otra clave no debe ser modificada.**

Aquí se os muestra un ejemplo para añadir un sistema basado en la SNES y que contiene únicamente roms traducidas:

```xml
<?xml version="1.0"?>
<systemList>
  <system uuid="21b8873a-a93e-409c-ad0c-8bb6d682bef8" name="snes" fullname="Super Nintendo Fan Trad" platform="snes">
    <descriptor path="%ROOT%/snestrad" theme="snestrad" extensions=".smc .sfc .mgd .zip .7z"/>
    <scraper screenscraper=""/>
    <properties type="console" pad="mandatory" keyboard="no" mouse="optional" lightgun="optional" releasedate="1990-11" retroachievements="1"/>
    <emulatorList>
      <emulator name="libretro">
        <core>snes9x2005</core>
        <core>snes9x2010</core>
        <core>snes9x2002</core>
      </emulator>
    </emulatorList>
  </system>
</systemList>
```