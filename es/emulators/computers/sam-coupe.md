---
title: SAM Coupé
description: 
published: true
date: 2024-07-26T12:49:29.651Z
tags: sam, ordenadores, coupe
editor: markdown
dateCreated: 2023-09-02T18:59:36.363Z
---

![](/emulators/computers/samcoupe.svg){.align-center}

## Detalles técnicos

* **Desarrollador**: Miles Gordon Technology
* **Fabricante**: Miles Gordon Technology
* **Sistema operativo**: SAM BASIC
* **Año de lanzamiento**: 1989
* **Cantidades vendidas**: 12000
* **Procesador**: Zilog Z80B a 6 MHz
* **Memoria**: 256 KB/512 KB (4,5 MB máx.)
* **Almacenamiento**: Grabadora de casete, disquetera de 3,5
* **Gráficos**: 4 modos (256×192 / 512×192) y paleta de 128 colores

## Presentación

**El SAM Coupé** es un ordenador personal británico de 8 bits lanzado en 1989. Fue fabricado originalmente por Miles Gordon Technology (MGT), con sede en Swansea (Reino Unido). El microprocesador es un Zilog Z80B a 6 MHz. La producción de este ordenador finalizó alrededor de 1995 y se vendieron 12.000 unidades.

La máquina está construida en torno al microprocesador Z80B a 6 MHz y contiene un circuito integrado de aplicación específica (ASIC) comparable a la matriz lógica no comprometida del Spectrum. El modelo básico tiene 256 KB de RAM, ampliables internamente a 512 KB y externamente a 4 MB. El medio de almacenamiento básico es una unidad de casete. Se pueden instalar internamente una o dos unidades de disquete de 3 1/2". El circuito integrado Philips SAA1099 proporciona 6 canales estéreo en 8 octavas.

La máquina tiene una ROM de 32 KB que contiene código de arranque y un intérprete SAM BASIC escrito por Andrew Wright. No hay DOS incluido en las ROMs.

## Emuladores

[SimCoupe](simcoupe)