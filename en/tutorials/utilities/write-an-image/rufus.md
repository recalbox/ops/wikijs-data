---
title: Rufus
description: 
published: true
date: 2021-08-07T00:00:48.248Z
tags: rufus
editor: markdown
dateCreated: 2021-08-07T00:00:48.248Z
---

Rufus is a utility for formatting and creating bootable USB media, such as USB sticks, flash memory, etc.

It is especially useful for cases where:

* You want to create a USB installation media from a bootable ISO image (Windows, Linux, UEFI, etc.)
* You want to work on a machine that does not have an operating system installed
* You want to program a BIOS or other type of firmware from DOS
* You want to run a low-level utility

Despite its small size, Rufus provides everything you need!

Oh, and Rufus is fast. For example, it is about twice as fast as [UNetbootin](http://unetbootin.sourceforge.net/), [Universal USB Installer](http://www.pendrivelinux.com/universal-usb-installer-easy-as-1-2-3) or [Windows 7 USB Utility](https://www.microsoft.com/en-us/download/windows-usb-dvd-download-tool), for creating USB installation media from a Windows 7 ISO image.  
It is also marginally faster for creating Linux bootable USB media.

* [Download the executable file](https://rufus.akeo.ie/) and run it (no installation is required).

The executable file is digitally signed, and the signature should read:

* "Akeo Consulting" (v1.3.0 or higher)
* Pete Batard - Open Source Developer" (v1.2.0 or earlier)

## Notes on DOS support

If you create a USB bootable disk with a non-US keyboard, Rufus tries to select a keyboard layout that matches the language of your system.  
In this case, [FreeDOS](http://www.freedos.org), which is the default selection, is recommended over MS-DOS, as it supports more international keyboard layouts.

## Notes on ISO image support

All versions of Rufus, since version 1.1.0, support creating bootable USB media from an [ISO image](http://en.wikipedia.org/wiki/ISO_image) (.iso).

If you don't have an ISO file, creating such an image, from an optical disk or from a file set, is easy to do. You can just use one of the many free CD or DVD burning applications, such as [CDBurnerXP](http://cdburnerxp.se/) or [ImgBurn](http://www.imgburn.com/).

## License
[GNU General Public License (GPL) version 3](http://www.gnu.org/licenses/gpl.html) or later.
You are free to distribute, modify or sell this software, as long as you comply with the GPLv3 license.

Rufus is produced 100% transparently, from its [public source code](https://github.com/pbatard/rufus), using a [MinGW32](http://www.mingw.org) environment.