---
title: Dreamcast
description: 
published: true
date: 2024-07-26T06:59:56.404Z
tags: sega, dreamcast, juegos
editor: markdown
dateCreated: 2023-09-14T07:42:27.948Z
---

Con los siguientes tutoriales descubriréis cómo gestionar el contenido de las tarjetas de memoria asi como el intercambio de discos (para los juegos multi-discos).

Aqui tenéis la lista de tutoriales disponibles:

[FAQ](faq) 