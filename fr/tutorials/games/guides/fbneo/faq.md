---
title: FAQ
description: 
published: true
date: 2024-09-02T21:01:43.584Z
tags: fbneo, finalburn, neo, faq
editor: markdown
dateCreated: 2021-08-08T14:16:20.212Z
---



### Le jeu XXX ne se lance pas, pourquoi ?

Ce n'est pas supporté ou vous avez une mauvaise rom, vos logs vous donneront plus de détails. Créez un romset valide avec clrmamepro comme indiqué ci-dessus. Il y a aussi quelques jeux marqués comme ne fonctionnant pas, essayez l'un de leurs clones.

### J'ai corrigé le jeu XXX et je ne peux pas l'exécuter, pourquoi ?

Parce que c'est considéré comme une mauvaise rom car les crcs ne correspondront pas, cependant il existe une méthode pour utiliser un romset patché, si vous mettez la version patché du romset dans `/recalbox/share/bios/fbneo/patched` cela fonctionnera (Note : vous pouvez le retirer de tout fichier qui ne pas différent des romset non patché si vous voulez).

### Le jeu XXX a des problèmes graphiques, pourquoi ?

Rédigez un rapport avec des détails sur le problème et votre plateforme.

### Le jeu XXX s'exécute lentement, pourquoi ?

Votre matériel est probablement trop lent pour exécuter le jeu avec des paramètres normaux. Essayez ce qui suit :

* Vérifiez s'il y a un commutateur DIP Speedhack dans les options principales, réglez-le sur "Oui".
* Essayez de définir une valeur pour frameskip dans les options principales.
* Essayez de réduire l'horloge du processeur dans les options de base
* Essayez de désactiver le rembobinage, le runahead ou tout autre paramètre retroarch connu pour augmenter la surcharge.
* Essayez de réduire les paramètres audio dans les options principales.
* Si cela ne suffit pas, mettez à niveau / overclockez votre matériel ou utilisez un autre noyau.

Nous n'accepterons pas les demandes de "rendre le noyau plus rapide", en ce qui nous concerne, ce noyau a un bon équilibre entre précision et vitesse, et pour la plupart, il fonctionne déjà très bien sur des bras bon marché (rpi3, ...).

### Le jeu XXX a un son saccadé, pourquoi ?

Très probablement pour la même raison que ci-dessus.

### Le jeu XXX s'exécute plus rapidement dans MAME2003 / MAME2010, pourquoi ?

Ce n'est pas MAME, nous utilisons généralement un code plus "à jour". Dans l'ensemble, FB Alpha est plus lent que l'ancienne version MAME mais plus précis et moins buggé. Ce port libretro prend également en charge diverses fonctionnalités qui sont généralement boguées ou absentes dans les cœurs MAME (netplay, rembobinage, rétro-réalisations, ...). Il peut utiliser des ressources supplémentaires.

### Le code de triche ne fonctionne pas, pourquoi ?

Il devrait y avoir un support partiel à travers la nouvelle API reposant sur l'exposition principale du ram.

### Le CD Neogeo ne fonctionne pas, pourquoi ?

Il y a plusieurs choses à savoir :

* Vous avez besoin d'une copie de neocdz.zip et neogeo.zip dans votre répertoire système libretro
* Vous devez ajouter `--subsystem neocd`à la ligne de commande ou placer vos jeux dans un `neocd`dossier
* Les formats pris en charge sont ccd / sub / img (trurip) et le fichier unique MODE1 / 2352 cue / bin (utilisez des utilitaires comme "CDmage" pour convertir votre iso si nécessaire), **ils ne doivent pas être compressés**.

Vous pouvez convertir vos isos non pris en charge en suivant ce didacticiel :

* Obtenez [CDMage 1.02.1](https://www.videohelp.com/software/CDMage) (gratuit et sans publicité)
* Fichier &gt; Ouvrir &gt; sélectionnez votre iso (NB: pour le multi-piste, sélectionnez le fichier .cue, pas le fichier .iso)
* Fichier &gt; Enregistrer sous &gt; écrivez le nom de votre nouveau fichier
* Assurez-vous de sélectionner MODE1 / 2352 dans le deuxième menu déroulant
* Appuyez sur OK, attendez la fin du processus (quelques secondes sur mon ordinateur), et c'est fait !

### Killer Instinct ne fonctionnera pas, pourquoi ?

Ce pilote a été désactivé pour l'instant, il ne répond pas aux critères de qualité.

~~Il y a plusieurs choses à savoir:~~

* ~~Il ne fonctionne qu'à une vitesse jouable sur x86\_64 \(les autres arches auront essentiellement besoin d'un processeur à 4Ghz car elles manquent d'un dynarec mips3\), et le noyau doit être construit comme ceci pour activer ce dynarec: `make -j5 -C src/burner/libretro USE_X64_DRC=1`~~
* ~~Si votre rom est à `ROM_DIRECTORY/kinst.zip`, vous aurez besoin de l'image disque non compressée à`ROM_DIRECTORY/kinst/kinst.img`~~
* ~~Pour obtenir l'image du disque non compressé, vous devrez utiliser l'outil chdman de MAME sur le chd de mame, la commande ressemble à ceci: `chdman extracthd -i kinst.chd -o kinst.img`~~

### Hiscore ne fonctionnera pas, pourquoi?

Avoir hiscore.dat et l'option de base activée ne garantit pas que ses scores fonctionneront pour un jeu spécifique, parfois un pilote manquera le code nécessaire. Vous pouvez demander une assistance dans le suivi des problèmes tant que la demande est raisonnable (c'est-à-dire éviter de faire une liste de plusieurs dizaines / centaines de jeux).

---

## Samples

Les samples doivent être placés sous `/recalbox/share/bios/fbneo/samples`.

## Hiscore.dat

Copiez [hiscore.dat](https://github.com/libretro/FBNeo/blob/master/metadata/hiscore.dat) dans `/recalbox/share/bios/fbneo/`.

## MAPPING

Nous n'avons pas d'outil pratique comme l'OSD MAME, mais nous utilisons l'API RetroArch pour personnaliser les mappages, vous pouvez le faire en allant dans `Quick menu` > `Controls`.
Pour ceux qui ne souhaitent pas personnaliser entièrement leur mappage, il existe 2 préréglages pratiques que vous pouvez appliquer en changeant le "type d'appareil" pour un lecteur dans ce menu :

* **Classic/Classique** : il appliquera le mappage "carré" du cd neogeo original dans les jeux neogeo, et utilisera L / R comme 5ème et 6ème bouton pour les jeux à 6 boutons comme Street Fighter II.
* **Modern/Moderne** : il appliquera la cartographie King of Fighters de Playstation 1 et supérieure dans les jeux de combat neogeo, et il utilisera R1 / R2 comme 5ème et 6ème bouton pour les jeux à 6 boutons comme Street Fighter II (pour la même raison que les jeux neogeo) , c'est vraiment pratique pour la plupart des sticks d'arcade.

Les "types d'appareils" suivants existent également, mais ils ne seront pas compatibles avec tous les jeux:

* **Mouse (ball only) / Souris (balle uniquement)** : elle utilisera la souris / trackball pour les mouvements analogiques, les boutons resteront sur retropad
* **Mouse (full) / Souris (pleine)** : comme ci-dessus, mais les boutons seront sur la souris
* **Pointer / Pointeur** : il utilisera un dispositif "pointeur" (peut être une souris / trackball) pour déterminer les coordonnées à l'écran, les boutons resteront sur retropad
* **Lightgun** : il utilisera le lightgun pour déterminer les coordonnées à l'écran, les boutons seront également sur le lightgun.

## Dat

Utilisez [clrmamepro](./../../../utilities/rom-management/clrmamepro) (qui fonctionne bien sur Linux avec Wine et sur macOS avec Crossover) pour créer des romsets valides avec des données à partir du répertoire [dats](https://github.com/libretro/FBNeo/tree/master/dats).
Ne signalez pas les problèmes si vous n'avez pas créé de romset valide.  
De plus, seul les dat "parent seulement" sont fournis, car il n'est pas recommander d'utiliser uniquement des roms parent (certains ne fonctionnent pas et certains clones sont vraiment différents de leur parent).

## Émuler des consoles

Vous pouvez émuler des consoles (avec des romsets spécifiques, les dats sont également dans le répertoire [dats](https://github.com/libretro/FBNeo/blob/master/dats)) en préfixant le nom des roms avec `XXX_` et en supprimant l'extension `zip|7z`, ou en utilisant l'argument `--subsystem XXX` de la ligne de commande, voici la liste des préfixes disponibles :

* CBS ColecoVision : `cv`
* MSX 1 : `msx`
* Nec PC-Engine : `pce`
* Nec SuperGrafX : `sgx`
* Nec TurboGrafx-16 : `tg`
* Nintendo Entertainment System : `nes`
* Nintendo Family Disk System : `fds`
* Sega GameGear : `gg`
* Sega Master System : `sms`
* Sega Megadrive : `md`
* Sega SG-1000 : `sg1k`
* SNK Neo-Geo Pocket : `ngp`
* ZX Spectrum : `spec`

Il est également possible d'utiliser le nom du dossier pour la détection (cette deuxième méthode a été ajoutée car certains appareils ne sont pas compatibles avec les sous-systèmes):

* CBS ColecoVision : `coleco`
* MSX 1 : `msx`
* Nec PC-Engine : `pce`
* Nec SuperGrafX : `sgx`
* Nec TurboGrafx-16 : `tg16`
* Nintendo Entertainment System : `nes`
* Nintendo Family Disk System : `fds`
* Sega GameGear : `gamegear`
* Sega Master System : `sms`
* Sega Megadrive : `megadriv`
* Sega SG-1000 : `sg1000`
* SNK Neo-Geo Pocket : `ngp`
* ZX Spectrum : `spectrum`