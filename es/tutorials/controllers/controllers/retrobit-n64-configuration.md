---
title: Configuración del mando N64 Retrobit
description: 
published: true
date: 2023-09-19T09:18:57.088Z
tags: n64, mando, retrobit
editor: markdown
dateCreated: 2023-09-19T09:18:57.088Z
---

## Introducción

Hay varios mandos de N64 en el mercado, y RetroArch no sabe cómo utilizar los botones C de la Nintendo64 correctamente. Este tutorial os mostrará cómo utilizar el mando Tribute64 de retrobit.

![](/tutorials/controllers/controllers/retrobit-tribute64.png)

## Utilización

Desgraciadamente, RetroArch no reconoce los 4 botones C del mando original y se limita a A / B / X / Y. Vamos a explicar cómo podemos poner remedio a esto.

### Encendido / configuración

El controlador tiene su propio dongle USB. Hay que conectarlo a Recalbox y usar la combinación de teclas `START` + `HIGH` para encenderlo.

La configuración básica ya viene con recalbox, así que no hay necesidad de remapear. Sin embargo, si queréis mapear el mando, seguid estos puntos:

- El botón A en EmulationStation debe corresponder al botón C-Down en el controlador.
- El botón B en EmulationStation debe coincidir con el botón A del mando.
- El botón X en EmulationStation debe coincidir con el botón C-Izquierda del controlador
- El botón Y de EmulationStation debe coincidir con el botón B del controlador
- El botón Select de EmulationStation debe coincidir con el botón gris izquierdo situado sobre Start en el controlador
- El botón Hotkey de EmulationStation debe coincidir con el botón gris de la derecha, encima de Start en el mando.

> Utilizad el mando sólo con juegos de la N64 y la 64DD. Este mando no está diseñado para ningún otro sistema.
> {.is-info}

Una vez hecho esto, hay que crear 1 fichero de sobrecarga de configuración. Esto permitirá que todos los botones funcionen correctamente.

### Sobrecargas de configuración

>Lo que sigue sólo funcionará en los cores Libretro
{.is-info}

Hay que crear un fichero de texto vacío `.retroarch.cfg` dentro de `/recalbox/share/roms/n64` e introducir el siguiente contenido:

```ini
input_player1_b_btn = "2" # A
input_player1_a_btn = "15" # No affectation
input_player1_y_btn = "15" # No affectation
input_player1_y_btn = "1" # B
input_player1_start_btn = "9" # Start
input_player1_select_btn = "8" # Select
input_player1_up_btn = "h0up" # D-pad up
input_player1_down_btn = "h0down" # D-pad down
input_player1_left_btn = "h0left" # D-pad left
input_player1_right_btn = "h0right" # D-pad right
input_player1_l_btn = "4" # L
input_player1_r_btn = "5" # R
input_player1_l2_btn = "6" # Z
input_player1_r_x_plus_btn = "10" # C-right
input_player1_r_x_minus_btn = "0" # C-left
input_player1_r_y_plus_btn = "3" # C-down
input_player1_r_y_minus_btn = "11" # C-up
input_player1_l_x_plus_axis = "+0" # Stick right
input_player1_l_x_minus_axis = "-0" # Stick left
input_player1_l_y_plus_axis = "+1" # Stick down
input_player1_l_y_minus_axis = "-1" # Stick up
input_player1_menu_toggle_btn = "12" # Unknown
```

Buen juego !