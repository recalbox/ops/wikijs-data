---
title: Libretro Theodore
description: 
published: true
date: 2021-11-06T23:58:53.739Z
tags: libretro, mo5, to8, theodore
editor: markdown
dateCreated: 2021-07-29T17:50:13.255Z
---

**This computer** is emulated by the **core Libretro** [theodore](https://github.com/Zlika/theodore).  
**This core** is able to emulate **all models of the MOTO** range (MO5, MO6, TO7, TO7/70, TO8, TO8D, TO9, TO9+) as well as the Olivetti PC128, a MO6 clone for the Italian market.

## ![](/emulators/license.svg) License

This core is under [**GPLv3**](https://github.com/Zlika/theodore/blob/master/LICENSE) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Features



## ![](/emulators/bios.svg) BIOS

>**No bios is required.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Supported extensions

Roms must have the extension:

* .fd (floppy disk)
* .sap (floppy disk)
* .k7 (tape)
* .rom (cartridge)
* .m7 (cartridge)
* .m5 (cartridge)
* .zip
* .7z

This system supports compressed roms in .zip/.7z format. However, be careful, it is only an archive.

The files contained in the .zip/.7z must correspond to the extensions mentioned above.
Moreover, each .zip/.7z file must contain only one compressed rom.

### Location

Put the roms like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 thomson
┃ ┃ ┃ ┃ ┣ 🗒 **game.zip**

>The roms using **TOSEC** format are highly recommended.
{.is-success}

>For more information about roms, go to [this tutorial](./../../../tutorials/games/generalities/isos-and-roms)!
{.is-info}

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Accessing to options

You can configure various options in two different ways.

* Via RetroArch menu:

┣ 📁 RetroArch Menu
┃ ┣ 📁 Core options
┃ ┃ ┣ 🧩 Name_option

* Via the file `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core options

| Options | Options values | Variables (for configuration override) | Variables values |
| --- | --- | --- | --- |
| Thomson model | `Auto` ✅ / `TO8` / `TO8D` / `TO9` / `TO9+` / `MO5` / `MO6` / `PC128` / `TO7` / `TO7/70` | `theodore_rom` | `Auto` / `TO8` / `TO8D` / `TO9` / `TO9+` / `MO5` / `MO6` / `PC128` / `TO7` / `TO7/70` |
| Auto run game | `Disabled` ✅ / `Enabled` | `theodore_autorun` | `disabled` / `enabled` |
| Virtual keyboard transparency | `0%` ✅ / `10%` / `20%` / `30%` / `40%` / `50%` / `60%` / `70%` / `80%` / `90%` | `theodore_vkb_transparency` | `0%` / `10%` / `20%` / `30%` / `40%` / `50%` / `60%` / `70%` / `80%` / `90%` |
| Floppy write protection | `Enabled` ✅ / `Disabled` | `theodore_floppy_write_protect` | `enabled` / `disabled` |
| Tape write protection | `Enabled` ✅ / `Disabled` | `theodore_tape_write_protect` | `enabled` / `disabled` |
| Dump printer data to file | `Disabled` ✅ / `Enabled` | `theodore_printer_emulation` | `disabled` / `enabled` |
| Interactive disassembler | `Disabled` ✅ / `Enabled` | `theodore_disassembler` | `disabled` / `enabled` |
| Break on illegal opcode | `Disabled` ✅ / `Enabled` | `theodore_break_illegal_opcode` | `disabled` / `enabled` |

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/Zlika/theodore/](https://github.com/Zlika/theodore/)
* **Libretro documentation**: [https://docs.libretro.com/library/theodore/](https://docs.libretro.com/library/theodore/)