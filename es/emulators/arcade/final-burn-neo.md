---
title: FBNeo
description: FinalBurn Neo
published: true
date: 2024-07-26T07:39:05.374Z
tags: arcade, fbneo, finalburn
editor: markdown
dateCreated: 2023-09-01T21:08:29.369Z
---

![](/emulators/arcade/fbneo.svg){.align-center}

## Historia

Final Burn Alpha (FB Alpha o FBA) es un emulador de juegos arcade. Toma su nombre del juego "After Burner", que era el único juego emulado cuando se creó por primera vez; originalmente estaba basado en el emulador FinalBurn, iniciado en agosto de 2000 por Dave3 (también conocido como FinalDave).

El proyecto se fue agotando sobre la marcha, y de sus cenizas surgió FinalBurn Alpha (ahora enterrado desde el verano de 2019). Renació una última vez bajo las siglas FBNeo o FinalBurn Neo, tras la repentina y reciente rescisión de los derechos de FBA por parte de uno de sus principales miembros: Barry Harris.
Actualmente incluye más de treinta sistemas/consolas emulados y nada menos que 10.000 juegos.

## Emuladores

[Libretro FBNeo](libretro-fbneo)
[PiFBA](pifba)