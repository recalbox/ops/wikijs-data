---
title: Acesso root via Terminal CLI
description: 
published: true
date: 2023-09-17T13:17:25.979Z
tags: root, terminal, cli, acesso
editor: markdown
dateCreated: 2021-12-23T21:03:10.437Z
---

Para abrir um terminal com acesso root no Recalbox, você tem duas opções:

>Nunca exponha seu Recalbox à internet. Em particular a porta 22. Toda a gente sabe a palavra-passe predefinida do root e pode ser pirateado.
{.is-danger}

## SSH

Conecte-se via ssh ao Recalbox, com as seguintes credenciais:

* Endereço IP do seu Recalbox no formato: `192.168.x.x`
* ID do usuário: `root`
* Senha: `recalboxroot`

## {.tabset}
### Windows

Você pode usar estes programas:

* [Putty](https://www.chiark.greenend.org.uk/~sgtatham/putty/latest.html)
* [MobaXterm](https://mobaxterm.mobatek.net/)
* PowerShell

### macOS

O Terminal está incluso no sistema. Abra a pasta `Applications` e vá para o diretório` Utilities` para encontrá-lo.

Linha de comando para executar: `ssh root@ip-address-of-your-Recalbox`

### Linux

O Terminal é fornecido como padrão no sistema. Sua localização depende da distribuição Linux que você está usando.

Linha de comando para executar: `ssh root@ip-address-of-your-Recalbox`

## Acesso direto

Na interface do EmulationStation com um teclado:

* Pressione `F4` para sair do EmulationStation.
* Pressione `Alt + F2` para abrir um terminal.
* Use as mesmas credenciais acima.

## Alterar a palavra-passe predefinida

Pode alterar a palavra-passe predefinida com:

```sh
mount -o remount,rw /
passwd
```

>A alteração da palavra-passe não sobreviverá às actualizações.
{.is-warning}
