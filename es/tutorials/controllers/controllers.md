---
title: Mandos
description: Controladores
published: true
date: 2024-07-26T06:52:39.636Z
tags: controladores, mandos
editor: markdown
dateCreated: 2023-09-01T20:22:36.289Z
---

¿Un problema con un mando? Quizás encontraréis vuestra respuesta en estas páginas.

Lista de categorías disponibles:

[Mandos Nintendo Switch inhalambricos](nintendo-online-wireless-controllers)

Lista de tutoriales de esta sección:

[Asociación de un mandos PS4](associate-ps4-controller)
[Configuración del mando N64 Retrobit](retrobit-n64-configuration)
[Connectar un mando IPEGA](connect-ipega-controller)
[Jugar desde vuestro móvil utilizando el mando virtual](play-with-smartphone-using-virtual-gamepad)
[Jugar mediante una PSP con el mando de juegos FuSa](play-using-psp-with-fusa-controller)
[Los drivers de los mandos PS3](ps3-controllers-drivers)
[Utilización de vuestro periférico XArcade](xarcade-usage)