---
title: Nintendo 64
description: Nintendo 64
published: true
date: 2025-01-05T23:23:29.577Z
tags: n64, consoles, nintendo, nintendo 64
editor: markdown
dateCreated: 2021-07-27T16:30:45.188Z
---

![](/emulators/consoles/nintendo64.svg){.align-center}

## Technical data

* **Manufacturer**: Nintendo
* **Year of release**: 1996
* **Units sold**: 32.93 millions
* **Best-selling game**: Super Mario 64
* **CPU**: 64-bit NEC VR4300 @ 93.75 MHz
* **Co-CPU**: 64-bit MIPS RISC "Reality Immersion" RCP @ 62.5 MHz
* **RAM**: 4 MB RAMBUS RDRAM (expandable up to 8 MB)
* **Sound**: Stereo 16-Bit and 48 kHz

## Presentation

In the early 1990s, with the success of the **NES** and **Super Nintendo** consoles, Nintendo dominated the world video game market, despite the fierce competition from Sega.
The emergence of the CD-ROM led Nintendo to partner with Sony to develop a CD player for the Super Nintendo, the **SNES-CD**, better known as the **Nintendo PlayStation**, to compete with NEC's **PC-Engine** and Sega's **Mega-CD**.
But a dispute led Nintendo and Sony to abandon their joint project, which Sony then recycled to develop its own console, the **PlayStation**. Nintendo then turned to the Dutch firm Philips to pursue its project.

The announcement of the PlayStation in 1991, added to the failures of Sega's **Mega-CD** and Philips' **CD-i**, persuaded Nintendo to bury the CD-ROM extension of the **Super Nintendo** for good and to turn to the development of an entirely new console.

## Emulators

[Libretro mupen64plus_Next](libretro-mupen64plus_next)
[Libretro ParaLLEl_n64](libretro-parallel_n64)
[Mupen64plus GLide64MK2](mupen64plus-glide64mk2)
[Mupen64plus GLideN64](mupen64plus-gliden64)
[Mupen64plus N64_GLES2](mupen64plus-n64_gles2)
[Mupen64plus RICE](mupen64plus-rice)
[Mupen64plus RICE_GLES2](mupen64plus-rice_gles2)