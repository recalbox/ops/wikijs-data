---
title: Dolphin
description: 
published: true
date: 2021-09-06T13:20:46.700Z
tags: wii, dolphin
editor: markdown
dateCreated: 2021-08-03T11:02:49.827Z
---

Dolphin is an emulator for two recent Nintendo game consoles: the GameCube and the Wii. It allows PC gamers to enjoy games made for these two consoles in **full HD** (1080p) with various enhancements: compatibility with all PC controllers, turbo speed, online network play, and much more!

More than 200 people have been working hard for years to create Dolphin. The list of contributors can be found [on GitHub](https://github.com/dolphin-emu/dolphin/graphs/contributors).

## ![](/emulators/license.svg) License

This core is under [**GPLv2**](https://github.com/dolphin-emu/dolphin/blob/master/license.txt) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ❌ | ❌ | ❌ | ❌ | ❌ | ❌ | ❌ | ✅ |

## ![](/emulators/features.png) Features

| Feature | Supported |
| :---: | :---: |
| Netplay | ✔ |

## ![](/emulators/bios.svg) BIOS

>**No bios is required.**
{.is-success}

## ![](/emulators/isos.svg) Isos

### Supported extensions

Isos must have the extension:

* .ciso
* .iso
* .rvz
* .wbfs

### Location

Put the isos like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 wii
┃ ┃ ┃ ┃ ┣ 🗒 **game.iso**

Dolphin works with roms in the following formats:

* **GCM**/**ISO** (uncompressed dumps ~4.7 GB for a Wii game)
* **WBFS** (not supported by Dolphin 3.0 or older)
* **WAD** (mini wiiware games)

>Note that WBFS and CISO are lossy compression formats: they remove unused information from the disks to make them smaller. So we recommend you to try to dumper your games in GCM/ISO format if you have problems with a WBFS dump.
{.is-warning}

>The isos using **Redump** format are highly recommended.
{.is-success}

>For more information about isos, go to [this tutorial](./../../../tutorials/games/generalities/isos-and-roms)!
{.is-info}

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/dolphin-emu/dolphin/](https://github.com/dolphin-emu/dolphin)
* **Official website**: [https://www.dolphin-emu.org/](https://www.dolphin-emu.org/)