---
title: Libretro Stella2014
description: 
published: true
date: 2024-06-30T15:35:50.484Z
tags: libretro, atari-2600, atari, 2600, stella2014, 8.0+
editor: markdown
dateCreated: 2023-09-06T08:32:48.225Z
---

**Libretro Stella2014** es un emulador Atari multiplateforma.

## ![](/emulators/license.svg) Licencia

El core posee la siguiente licencia [**GPLv2**](https://github.com/stella-emu/stella/blob/master/License.txt).

## ![](/emulators/compatibility.png) Compatibilidad

 | RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
 | ✅ | ❌ | ❌ | ❌ | ❌ | ❌ | ❌ | ❌ |

## ![](/emulators/features.png) Funcionalidades

| Funcionalidad | ¿Soportada? |
| :---: | :---: |
| Reinicio | ✔ |
| Capturas de pantalla | ✔ |
| Grabaciones instantáneas | ✔ |
| Rebobinado | ✔ |
| Netplay | ✔ |
| RetroAchievements | ✔ |
| Controles | ✔ |
| Remapping | ✔ |

## ![](/emulators/bios.svg) BIOS

>**No se necesitan bios.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Extensiones de roms soportadas

Las roms deben tener las extensiones siguientes:

* .a26
* .bin
* .zip
* .7z

Este sistema soporta las roms comprimidas en formato .zip/.7z. No obstante, se trata sólo de un archivo.

Los ficheros contenidos en los .zip/.7z deben tener las extensiones citadas previamente.
Además, cada fichero .zip/.7z sólo debe contener un fichero rom al interior.

### Emplazamiento

Disponed vuestros juegos como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 atari2600
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.zip**

>Se aconsejan las roms del projecto **No-Intro**.
{.is-success}

>Para más información sobre las roms, consultad [este tutorial](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

>Para conservar las configuraciones personalizadas cuando se produce una actualización del sistema Recalbox, os aconsejamos utilizar las [Sobrecargas de configuración](./../../../advanced-usage/configuration-override).
{.is-info}

### Acceder a las opciones

Podéis configurar las opciones del core de dos formas diferentes.

* Via el Menú RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via el fichero `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Opciones del core

| Opción | Valor de la opción | Variable (para la sobrecarga) | Valores posibles |
| --- | --- | --- | --- |
| Color Depth (Restart) | `Thousands (16-bit)` ✅ / `Millions (24-bit)` | `stella2014_color_depth` | `16bit` / `24bit` |
| Interframe Blending | `Désactivé` ✅ / `Simple` / `Ghosting (65%)` / `Ghosting (75%)` / `Ghosting (85%)` / `Ghosting (95%)` | `stella2014_mix_frames` | `disabled` / `mix` / `ghost_65` / `ghost_75` / `ghost_85` / `ghost_95` |
| Audio Filter | `Désactivé` ✅ / `Activé` | `stella2014_low_pass_filter` | `disabled` / `enabled` |
| Audio Filter Level | `5%` / `10%` / `15%` / `20%` / `25%` / `30%` / `35%` / `40%` / `45%` / `50%` / `55%` / `60%` ✅ / `65%` / `70%` / `75%` / `80%` / `85%` / `90%` / `95%` | `stella2014_low_pass_range` | `5` / `10` / `15` / `20` / `25` / `30` / `35` / `40` / `45` / `50` / `55` / `60` / `65` / `70` / `75` / `80` / `85` / `90` / `95` |
| Gamepad: Paddle Sensitivity (Digital) | `10%` / `15%` / `20%` / `25%` / `30%` / `35%` / `40%` / `45%` / `50%` ✅ / `55%` / `60%` / `65%` / `70%` / `75%` / `80%` / `85%` / `90%` / `95%` / `100%` | `stella2014_paddle_digital_sensitivity` | `10` / `15` / `20` / `25` / `30` / `35` / `40` / `45` / `50` / `55` / `60` / `65` / `70` / `75` / `80` / `85` / `90` / `95` / `100` |
| Gamepad: Paddle Sensitivity (Analog) | `10%` / `15%` / `20%` / `25%` / `30%` / `35%` / `40%` / `45%` / `50%` ✅ / `55%` / `60%` / `65%` / `70%` / `75%` / `80%` / `85%` / `90%` / `95%` / `100%` / `105%` / `110%` / `115%` / `120%` / `125%` / `130%` / `135%` / `140%` / `145%` / `150%` | `stella2014_paddle_analog_sensitivity` | `10` / `15` / `20` / `25` / `30` / `35` / `40` / `45` / `50` / `55` / `60` / `65` / `70` / `75` / `80` / `85` / `90` / `95` / `100` / `105` / `110` / `115` / `120` / `125` / `130` / `135` / `140` / `145` / `150` |
| Gamepad: Paddle Response (Analog) | `Linear` ✅ / `Quadratic` | `stella2014_paddle_analog_response` | `linear` / `quadratic` |
| Gamepad: Paddle Deadzone (Analog) | `0%` / `3%` / `6%` / `9%` / `12%` / `15%` ✅ / `18%` / `21%` / `24%` / `37%` / `30%` | `stella2014_paddle_analog_deadzone` | `0` / `3` / `6` / `9` / `12` / `15` / `18` / `21` / `24` / `37` / `30` |
| Stelladaptor: Paddle Sensitivity | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` ✅ / `21` / `22` / `23` / `24` / `25` / `26` / `27` / `28` / `29` / `30` | `stella2014_stelladaptor_analog_sensitivity` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `21` / `22` / `23` / `24` / `25` / `26` / `27` / `28` / `29` / `30` |
| Stelladaptor: Paddle Centre Offset | `-10` / `-9` / `-8` / `-7` / `-6` / `-5` / `-4` / `-3` / `-2` / `-1` / `0` ✅ / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `21` / `22` / `23` / `24` / `25` / `26` / `27` / `28` / `29` / `30` | `stella2014_stelladaptor_analog_center` | `-10` / `-9` / `-8` / `-7` / `-6` / `-5` / `-4` / `-3` / `-2` / `-1` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `21` / `22` / `23` / `24` / `25` / `26` / `27` / `28` / `29` / `30` |

## ![](/emulators/external-links.png) Enlances externos

* **Código fuente utilizado:** [https://github.com/libretro/stella2014-libretro/](https://github.com/libretro/stella2014-libretro/)
* **Documentación Libretro :** [https://docs.libretro.com/library/stella/](https://docs.libretro.com/library/stella/)