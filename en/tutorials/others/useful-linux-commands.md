---
title: Useful Linux commands
description: 
published: true
date: 2021-08-06T15:28:45.863Z
tags: linux, commands, useful
editor: markdown
dateCreated: 2021-08-06T15:28:45.863Z
---

## Common commands

Here is a list of commands for Linux that will be useful to navigate Recalbox via a terminal.

* **ls**
Its Windows equivalent is `dir`. It lists the contents of a folder. You can see the options available on [this page](https://man7.org/linux/man-pages/man1/ls.1.html).
* **cd**
Its Windows equivalent is `cd`. It allows you to change the directory (cd = Change Directory). You can consult the available options on [this page](https://man7.org/linux/man-pages/man1/cd.1p.html).
* **cp**
Its Windows equivalent is `copy`. It allows you to copy files or directories. You can consult the available options on [this page](https://man7.org/linux/man-pages/man1/cp.1.html).
* **mv**
Its Windows equivalent is `move` or `ren`. It allows you to move or rename a file or directory. You can see the available options on [this page](https://man7.org/linux/man-pages/man1/mv.1.html).
* **mkdir**
Its Windows equivalent is `mkdir` or `md`. It is used to create an empty directory. You can see the available options on [this page](https://man7.org/linux/man-pages/man1/mkdir.1.html).
* **rmdir**
Its Windows equivalent is `rmdir` or `rd`. It allows you to delete a directory. You can see the available options on [this page](https://man7.org/linux/man-pages/man1/rmdir.1.html).
* **nano**
Allows you to open a text editor. You can consult the available options on [this page](https://linuxize.com/post/how-to-use-nano-text-editor/).
* **chmod**
Allows you to change the access permissions to a file. You can consult the options available on [this page](https://man7.org/linux/man-pages/man1/chmod.1.html).
* **wget**
Allows you to download a source available on the Internet. You can consult the available options on [this page](https://man7.org/linux/man-pages/man1/wget.1.html).

## Specific commands

### Check the temperature of the Raspberry Pi processor

To check the processor (CPU) temperature, run the following command: 

`cat /sys/class/thermal/thermal_zone0/temp`

Then divide the result by 1000 to get the temperature in Celsius.

```shell
# cat /sys/class/thermal/thermal_zone0/temp
54530
```

### Checking the applied overclocking settings

To check the applied overclocking values, run the following command:

`cat /boot/recalbox-oc-config.txt`

You will get a result similar to this example:

```text
# Plateform: RPI4
# Description: Extrem
# Warning
# Frequency: 2000

over_voltage=6
arm_freq=2000
gpu_freq=750
```

### Avoid rebooting the system if the Mupen64plus standalone N64 emulator crashes

If you experience crashes / lockups with the Mupen64 standalone N64 emulator, due to compatibility issues with the roms, you can easily "kill" the emulator instead of rebooting the Raspberry. To do this, you need to identify the Mupen64 process ID with the following command:

`ps aux | grep mupen64`

You should get a list similar to :

```shell
26193 root mupen64plus --corelib /usr/lib/libmupen64plus.so.2 .0.0 --gfx /usr/lib/mupen64plus/mupen64plus-video-gliden64.so --configdir /recalbox/share/system/configs/mupen64/ --datadir /recalbox/share/system/configs/mupen64/ /recalbox/share/roms/n64/007 - GoldenEye (Europe).n64
26196 root grep mupen64
```

In this case, the process ID of the Mupen64 process (PID) is **26193**. You can now kill the Mupen64 process with the following command:

`kill -1 <your_PID>`

So for our example: `kill -1 26193`

Once the process is killed, you will automatically return to EmulationStation.

### Stop / start EmulationStation

To update the roms list for example or to update the images of newly scraped games from your PC, you will need to stop and restart EmulationStation:

* Stop: `es stop` 
* Restart / boot: `es start`