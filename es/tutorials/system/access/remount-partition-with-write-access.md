---
title: Acceder a una partición en escritura
description: 
published: true
date: 2023-09-13T20:17:03.720Z
tags: acceso, partición, escritura
editor: markdown
dateCreated: 2023-09-13T20:17:03.720Z
---

En algún momento podéis necesitar modificar ciertos ficheros que se encuentran protegidos en sólo-lectura.

En el sistma, existen 2 puntos protegidos contra la escritura:

* La partición de arranque : `/boot`
* La partición sistema : `/`

## Commando a utilizar

Via [SSH](./../../../tutorials/system/access/root-access-terminal-cli), hay que utilizar el comando `mount`.

## {.tabset}
### Partición de arranque

```shell
mount -o remount,rw /boot
```

### Partición sistema :

```shell
mount -o remount,rw /
```