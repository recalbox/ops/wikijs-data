---
title: EmulationStation use with keyboard and mouse
description: If your current controller is failing...
published: true
date: 2021-08-06T16:57:19.529Z
tags: emulationstation, keyboard, mouse
editor: markdown
dateCreated: 2021-08-06T16:23:41.497Z
---

In case you have your only joystick that fails while using EmulationStation, you can navigate with a keyboard and mouse.

## Mouse commands

Here is what you can do with the mouse:

* A: right click
* B: left click

## Keyboard commands

Here is what you can do with the keyboard:

* A = S
* B = A
* X = P
* Y = L
* Start = Enter
* Select = Space
* L1 = Page Up (⇞)
* R1 = Page Down (⇟)
* Hotkey = Escape

