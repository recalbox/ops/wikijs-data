---
title: Salida audio analógica en el Pi Zero
description: 
published: true
date: 2023-09-17T20:50:41.234Z
tags: pi, cero, salida, analógica
editor: markdown
dateCreated: 2023-09-17T20:50:06.681Z
---

## ¿No hay salida de audio en el Pi cero? ¿verdad?

Sí que hay! Para mantener el Pi Zero pequeño y barato, el filtro de audio para auriculares no está incluido. No hay jack para obtener música de vuestra Recalbox. Puedes obtener el audio digitalmente a través de HDMI, así que si conectáis el Pi a un monitor con altavoces, funcionará correctamente.

Esto está bien si habéis enchufado la Recalbox a la TV del salón, pero en un mueble de arcade con un monitor de PC estándar sin HDMI... no puedéis obtener el sonido.

Bueno, esto no es realmente cierto. Si estáis utilizando un conversor HDMI a VGA, podéis tener la suerte de conseguir uno que tenga una salida de audio jack como [aquí](https://www.amazon.fr/dp/B075L812M3/).

Pero si estáis usando un Pi cero, probablemente el objetivo es integrarlo en un dispositivo pequeño, con una pequeña pantalla TFT en el bus SPI/I2C/DPI, y seguramente no tendréis espacio suficiente para utilizar este conversor.

## Cómo se emite el audio en el Raspberry Pi

Como se describe en [esta página](https://learn.adafruit.com/introducing-the-raspberry-pi-zero/audio-outputs), otros Raspberry Pi utilizan 2 salidas PWM con un filtro para crear la salida de sonido. En esta página, encontraréis la electrónica real integrada en los Raspberry Pi para crear este sonido.

Pero si leéis atentamente, se utilizan los GPIOs 40 y 45 para el PWM, pines que no están resaltados en el Pi Zero.

La solución consiste en poner el PWM0 en el GPIO 18 (ALT5) y el PWM1 en el GPIO 13 (ALT0) o el GPIO 19 (ALT5), y crear vuestro propio circuito de filtrado.

El procedimiento se describe en detalle en [esta página](https://learn.adafruit.com/adding-basic-audio-ouput-to-raspberry-pi-zero/pi-zero-pwm-audio).

Con Recalbox, la forma más sencilla consiste en añadir la siguiente línea al fichero `recalbox-user-config.txt`. Esta línea reconfigurará los pines al arranque sin ningún software o servicio externo. El PWMO estará en el GPIO 18 (pin 12 del conector), y el PWM1 en el GPIO 13 (pin 33 del conector).

```text
dtoverlay=pwm-2chan,pin=18,func=2,pin2=13,func2=4
```

Como se describe en [esta página](https://hackaday.io/project/9467-piboy-zero/log/35090-pi-zero-pwm-audio-device-tree-overlay), podéis realizar vuestra propia superposición con el siguiente código fuente:

```text
/dts-v1/;
/plugin/;

/ {
  compatible = "brcm,bcm2708";

  fragment@0 {
    target = <&gpio>;
    __overlay__ {
      pinctrl-names = "default";
      pinctrl-0 = <&pwm_audio_pins>;

    pwm_audio_pins: pwm_audio_pins {
	brcm,pins = <13 18>;   /* gpio no ('BCM' number) */
	brcm,function = <4 2>; /* 0:in, 1:out, 2: alt5, 3: alt4, 4: alt0, 5: alt1, 6: alt2, 7: alt3 */
	brcm,pull = <0 0>;     /* 2:up 1:down 0:none */
      };
    };
  };
 };
```

Si habéis configurado buildroot y ya habéis montado Recalbox, podéis compilar los dts con :

```shell
output/build/linux-FIRMWARE/scripts/dtc/dtc -@ -I dts -O dtb -o pwm-audio-pi-zero-overlay.dtbo pwm-audio-pi-zero-overlay.dts
```

A continuación copiáis el fichero `pwm-audio-pi-zero-overlay.dtbo` dentro del directorio `/boot/overlays` de Recalbox. La configuración ahora resulta más sencilla:

```ini
dtoverlay=pwm-audio-pi-zero
```

### Prueba rápida de la salida de sonido

Podéis probar si el sonido funciona correctamente sin crear el circuito de filtrado. Podéis conectar un altavoz de auriculares directamente entre el pin PWM y una masa como se muestra en la siguiente figura.

El sonido debería funcionar, aunque puede ser algo defectuoso.

### Mejorar la calidad del sonido

Para crear vuestro filtro, necesitaréis algunos componentes electrónicos:

* 2 resistencias de 270 ohmios
* 2 resistencias de 150 ohmios
* 2 condensadores de 10µF
* 2 condensadores de 33nF (pueden sustituirse por condensadores de 22nF o 10nF)

Aquí tenéis los esquemas:

![](/tutorials/audio/filter-schema.png)

y el verdadero filtro:

![](/tutorials/audio/filter-made.jpg)

Necesitaréis amplificar la salida porque las señales están filtradas y tienen niveles muy bajos.

## Amplificación y control de volumen

Podéis utilizar un pequeño amplificador de 5V 2x3W.  
También podéis añadir un potenciómetro de audio estéreo para ajustar el volumen.

![](/tutorials/audio/ampli.png)