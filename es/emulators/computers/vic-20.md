---
title: VIC-20
description: 
published: true
date: 2024-07-26T12:52:51.308Z
tags: commodore, vic-20, ordenadores
editor: markdown
dateCreated: 2023-09-03T13:00:28.340Z
---

![](/emulators/computers/vic20.svg){.align-center}

## Datos técnicos

* **Fabricante**: Commodore
* **Año de lanzamiento**: 1980
* **Sistema operativo**: Commodore KERNAL / Commodore BASIC 2.0
* **Procesador**: MOS Technology 6502 a 1,02 MHz (NTSC) / 1,10 MHz (PAL)
* **RAM**: 5 KB (hasta 32 KB)
* **RROM**: 20 KB
* **Procesador gráfico**: MOS Technology VIC-I 6561
* **Chip de sonido**: 3 x cuadrado, 1 x nariz, mono
* **Resolución**: 128x128 (16 colores)

## Presentación

El **VIC-20** era un ordenador personal de 8 bits construido por Commodore International con 5 kB de RAM (incluidos 3,5 kB para aplicaciones BASIC) y una CPU basada en un procesador MOS 6502. Se parecía a sus sucesores, el C64 y el C16. El VIC-20 apareció en junio de 1980, 2 años y 9 meses después del lanzamiento del primer ordenador personal de Commodore, el PET.

A diferencia del PET, el VIC-20 iba a posicionarse como un ordenador para el mercado de masas. Se dice que el chip de vídeo del VIC-20, llamado VIC y creado por MOS Technology, había sido diseñado originalmente para terminales gráficos y consolas de juegos de gama básica, pero Commodore no consiguió venderlo adecuadamente. Al mismo tiempo, Commodore tenía un excedente de memoria SRAM de 4x1kB. En abril de 1980, Jack Tramiel, presidente de Commodore, pidió un ordenador que se vendiera por menos de 300 dólares. El resultado fue el VIC-20. Mientras que el PET sólo se vendía a través de distribuidores autorizados, el VIC-20 se vendió al por menor desde el principio, sobre todo en tiendas de descuento y jugueterías, donde competía directamente con las videoconsolas. Commodore publicó anuncios en los que el actor de Star Trek William Shatner preguntaba: "¿Por qué conformarse sólo con un videojuego?

Aunque las revistas criticaron el VIC-20 por ser demasiado pequeño, la estrategia funcionó: se convirtió en el primer ordenador en superar el millón de unidades vendidas y en el ordenador más vendido del mundo en 1982. En su punto álgido, se produjeron 9.000 unidades al día, y se vendieron 2.500.000 unidades hasta el cese de la producción en enero de 1985, cuando Commodore reposicionó el C64 como su ordenador de gama básica en preparación para el próximo lanzamiento del Commodore 128 y el Amiga (este último introdujo a Commodore en el mundo de los 16/32 bits).

Debido a su escasa memoria y a su pantalla de baja resolución, en comparación con otros ordenadores de la época, el VIC-20 se utilizaba principalmente con fines educativos o de entretenimiento. Sin embargo, también se desarrollaron para esta máquina programas de utilidad como la gestión del presupuesto familiar, hojas de cálculo y terminales de comunicación. Su facilidad de uso por parte del público en general hizo que muchos futuros desarrolladores pudieran iniciarse en el VIC-20 aprendiendo el lenguaje BASIC, o incluso ensamblador (o lenguaje máquina). Varias revistas, como Compute !4, publicaron códigos fuente de programas para el VIC-20, uno de ellos propuesto por la propia Commodore. Como resultado, muchos usuarios aprendieron a programar tecleando, estudiando, ejecutando y modificando estos programas. Es el caso de Linus Torvalds, por ejemplo.

La facilidad de programación y la disponibilidad de un módem de bajo coste hicieron que el VIC-20 pudiera ofrecer una biblioteca bien surtida de software de dominio público y freeware, aunque mucho más pequeña que la biblioteca del C64. El software se distribuía a través de servicios en línea como CompuServe, BBS (Bulletin Board System) y entre grupos de usuarios.

En cuanto al catálogo comercial, se calcula que había 300 títulos disponibles en cartuchos electrónicos y más de 500 en casete. En comparación, la Atari 2600, la videoconsola más popular de la época, ofrecía unos 900 títulos.

## Emuladores

[Libretro vice_xvic](libretro-vice_xvic)