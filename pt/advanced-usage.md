---
title: 2. 🔨 USO AVANÇADO
description: 
published: true
date: 2023-07-18T12:05:14.006Z
tags: 
editor: markdown
dateCreated: 2021-08-17T00:24:20.289Z
---

Esta parte da documentação mostrará com mais detalhes algumas das características que são úteis para um pequeno número de pessoas.

Páginas nesta categoria :

[Sobrecarga de Configuração](./advanced-usage/configuration-override)
[Visualização dos Sistemas](./advanced-usage/systems-display)
[O Arcade no Recalbox](./advanced-usage/arcade-in-recalbox)
[Atualização manual e/ou offline](./advanced-usage/manual-update)
[Pad To Keyboard](./advanced-usage/pad-to-keyboard)
[RetroArch](./advanced-usage/retroarch)
[Scripts em eventos do EmulationStation](./advanced-usage/scripts-on-emulationstation-events)
