---
title: Skraper - Scrapear vuestras roms
description: 
published: true
date: 2023-09-23T14:40:59.268Z
tags: roms, scraper, skraper, scrapear
editor: markdown
dateCreated: 2023-09-23T14:40:59.268Z
---

## Guía

## Tutorial vídeo

El tutorial vídeo se puede encontrar en [YouTube](https://www.youtube.com/watch?v=G388Gc6kkRs).

## Posibles problemas

He aquí una lista de posibles problemas:

* Mal nombrado
* Falta en la base de datos
* Opción de búsqueda incorrecta en **Skraper.**.
* Servidor de **Screenscaper** en mantenimiento.

## Financiar el hosting y el desarrollo de ScreenScraper

| 1 euro : gracias ! | 2 euros : muchas gracias ! | 5 euros : 1 thread adicional ! | 10 euros : 5 threads adicionales ! |
| :---: | :---: | :---: | :---: |
| Colaborador financiero de nivel BRONCE en la página de ScreenScraper, en la sección de miembros. | Colaborador financiero de nivel PLATA en la página de ScreenScraper, en la sección de miembros. | 1 thread extra para toda la vida para scrapear más rápido además de apoyar la vida del proyecto. Contribuyente financiero de nivel ORO en la página de ScreenScraper en la sección de miembros. | 5 threads extra para toda la vida para scrapear más rápido además de apoyar la vida del proyecto. Contribuyente financiero de nivel ORO en la página de ScreenScraper en la sección de miembros. |

## Página oficial de Screenscraper

[https://www.screenscraper.fr/](https://www.screenscraper.fr/)