---
title: Skraper - Scrap your roms
description: 
published: true
date: 2021-09-04T21:08:43.166Z
tags: scrap, roms, skraper
editor: markdown
dateCreated: 2021-08-07T00:07:27.464Z
---

## Guide



## Video Tutorial

The video tutorial can be found on [YouTube](https://www.youtube.com/watch?v=G388Gc6kkRs).

## Possible problem

Here is a list:

* Badly named
* Missing from the database.
* Wrong search option in **Skraper**.
* Server of **Screenscaper** under maintenance.

## Help finance the hosting and development of ScreenScraper

| 1 euro: thank you! | 2 euros: thank you very much! | 5 euros: 1 additional thread! | 10 euros: 5 additional threads!
| :---: | :---: | :---: | :---: |
| Financial contributor level BRONZE on the site ScreenScraper Member section! | Financial contributor level SILVER on the site ScreenScraper Member section! | 1 additional thread for life to scrape faster in addition to bringing your support to life. Gold level financial contributor on the ScreenScraper website in the Member section! | 5 extra threads for life to scrape 5 times faster and support life. Gold level financial contributor on the ScreenScraper website in the Member section! |

## Screenscraper website

[https://www.screenscraper.fr/](https://www.screenscraper.fr/)