---
title: Amiga 1200
description: 
published: true
date: 2024-07-26T12:44:25.544Z
tags: amiga-1200, amiga, 1200, commodore, ordenadores
editor: markdown
dateCreated: 2023-09-02T18:28:34.080Z
---

![](/emulators/computers/amiga1200.svg){.align-center}

## Especificaciones técnicas

* **Fabricante**: Commodore
* **Año de lanzamiento**: 1992
* **Sistema operativo**: AmigaOS 3.0/3.1
* **Procesador**: Motorola 68EC020 a 14,32 MHz
* **RAM**: 2 MB
* **ROM**: 512 KB de ROM Kickstart 3.0-3.1 
* **Procesador gráfico**: Arquitectura gráfica avanzada
* **Chip de sonido**: PCM de 4 canales y 8 bits
* **Resolución**: 320x200 a 1504x484 (16,8 millones de colores)

## Presentación

El **Amiga 1200**, o **A1200**, fue el microordenador Amiga de tercera generación de Commodore International, destinado al mercado familiar. Salió a la venta en octubre de 1992, con un precio de 399 £ en el Reino Unido y 599 $ en EE.UU..

Al igual que sus predecesores, el Amiga 500 y el Amiga 600 (de los que tomó su diseño, con la adición de un teclado numérico), fue diseñado como un sistema "todo en uno", incorporando procesadores, teclado y disquetera en una sola unidad. Se vendió con 2 MiB de memoria, el chipset gráfico Amiga de tercera generación (AGA) y AmigaOS 3.0. Utiliza un procesador Motorola MC68EC020 a 14 MHz. Como complemento, el Amiga 1200 dispone de una ranura para memoria/CPU y un puerto PCMCIA Tipo II1.

Como la memoria se comparte entre el procesador principal y los coprocesadores de vídeo y sonido, ampliar la memoria más allá de 2 Mio aumenta enormemente el rendimiento de la máquina. Varios fabricantes de terceros ofrecieron aceleradores que contenían procesadores 68020, 68030, 68040, 68060 y, más tarde, PowerPC, que aumentaban considerablemente la potencia de la máquina y a veces obligaban a volver a montar el Amiga en una torre para evitar el sobrecalentamiento de la placa base original.

## Emuladores

[Amiberry](amiberry)
[Libretro PUAE](libretro-puae)
[Libretro UAE4ARM](libretro-uae4arm)