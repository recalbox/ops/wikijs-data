---
title: Definir la posición del Dolphin Bar
description: 
published: true
date: 2024-07-26T07:01:55.984Z
tags: wii, dolphin, bar
editor: markdown
dateCreated: 2023-09-20T09:04:01.629Z
---

* Abrid el fichier `/recalbox/share/system/recalbox.conf`
* En la sección "D3 - Dolphin Controllers" buscad esta línea:

```ini
wii.sensorbar.position
```
* Si vuestra Dolphin Bar se encuentra sobre vuestra pantalla, reemplazadla por:

```ini
wii.sensorbar.position=1
```

* Si vuestra Dolphin Bar se encuentra bajo la pantalla, reemplzadla por:

```ini
wii.sensorbar.position=0
```