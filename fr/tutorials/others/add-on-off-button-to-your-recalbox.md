---
title: Ajouter un bouton on/off à votre Recalbox
description: 
published: true
date: 2024-03-26T19:26:14.504Z
tags: marche, arrêt, bouton
editor: markdown
dateCreated: 2021-05-21T08:09:11.782Z
---

>Vous pouvez maintenant ajouter un bouton on/off à votre Recalbox. Le bouton peut être un bouton poussoir (aussi appelé "momentary switch") ou un bouton ON/OFF. 
{.is-info}

## Pour câbler le bouton au GPIO du Raspberry Pi

* Connectez une broche au **GPIO3** (le **cinquième** gpio en partant d'en haut à gauche), et l'autre à la masse sur sa droite (le **sixième** gpio).

* Enfin, vous devez activer le support du bouton dans le fichier [recalbox.conf](./../../basic-usage/getting-started/recalboxconf-file) en ajoutant une de ces lignes :

  * `system.power.switch=PIN56ONOFF` pour un bouton ON/OFF 
  * `system.power.switch=PIN56PUSH` pour un bouton poussoir

Et vous avez une Recalbox qui peut être éteinte/allumée par un simple bouton !

## Rajouter un bouton reset et une LED.

>Il existe une autre option où vous pouvez rajouter un bouton reset et une LED.
{.is-info}

* Dans recalbox.conf, ajoutez/décommentez (en enlevant le ; au début de la ligne) la ligne suivante :

  * `system.power.switch=PIN356ONOFFRESET` pour un bouton power ON/OFF 
  * `system.power.switch=PIN356PUSHRESET` pour un bouton power poussoir

*  Pour câbler le bouton reset au GPIO du Raspberry Pi :
   * Connectez une broche au GPIO2 (le troisième gpio en partant d'en haut à gauche)
   * Connectez l'autre broche à la même masse que le bouton on/off (le sixième gpio).
* La LED est connectée au GPIO14 (le huitième gpio) ainsi qu'à la masse.

#### Pour résumer

* _Power+_ sur **GPIO 3** (**PIN 5**) 
* _Reset+_ sur **GPIO 2** (**PIN 3**) 
* _LED+_ sur **GPIO 14** (**PIN 8**) 
* _Power-_, _Reset-_ et _Led-_ sur l'un des **masses** (**PIN 6, 9, 14, 20, 25, 30, 34 ou 39**)

>Notez que cela ne fonctionne qu'avec un bouton poussoir pour le reset et un bouton ON/OFF pour l'alimentation
{.is-info}

## Utilisation

### Avec le Bouton POWER en mode PUSH (PIN 5 + GROUND)

* Pression courte _(éteint)_ : Allumer votre Recalbox 
* Pression courte _(allumé)_ : Quitter l’émulateur en cours et revenir au menu principal 
* Pression longue _(allumé)_ : Éteindre proprement votre Recalbox _(équivaut à faire `SELECT` > `Éteindre` normalement)_

### Avec le Bouton POWER en mode ON/OFF (PIN 5 + GROUND)

* Activation du bouton _(éteint)_ : Allumer votre Recalbox 
* Désactivation du bouton _(allumé)_ : Éteinds proprement votre Recalbox _(équivaut à faire `SELECT` > `Éteindre` normalement)_

### Avec le Bouton RESET (PIN 3 + GROUND)

* Pression courte _(éteint)_ : - 
* Pression courte _(allumé)_ : Reset le jeu, comme à l'époque sur la console 
* Pression longue _(allumé)_ : Redémarrer votre Recalbox _(équivaut à faire `SELECT` > `Redémarrer`)_