---
title: Guide complet sur la configuration vidéo
description: 
published: true
date: 2024-03-14T17:27:19.752Z
tags: configuration, guide, vidéo
editor: markdown
dateCreated: 2021-05-21T08:43:30.809Z
---

## Introduction

Dans Recalbox, vous pouvez régler plusieurs résolutions d'écran différemment de ce que vous souhaitez :

- Résolution globale (tout le système)
- Résolution d'écran de l'interface
- Résolution d'écran par système (incluant Kodi)

Jusqu'à maintenant, pour définir une résolution d'écran à utiliser, vous deviez utiliser des modes vidéos, etn'était pas disponible en dehors des Raspberry Pis. Ceci est fini, il y a bien plus simple, et accessible pour tout le monde.

Pour définir une résolution d'écran à utiliser, vous devez tout simplement indiquer la résolution d'écran à utiliser. Par exemple :

```ini
global.videomode=1280x1080
```

## Configuration manuelle

### Configuration de la résolution globale

Pour définir la résolution globale à utiliser, vous devez modifier la valeur de `global.videomode` dans le fichier `recalbox.conf`.

```ini
global.videomode=1280x1080
```

Vous pouvez aussi utiliser la valeur `default` pour utiliser la résolution native de votre écran.

>Si la ligne `global.videomode` n'existe pas, vous devez l'ajouter sur une ligne vide dans le même fichier.
{.is-info}

### Configuration de la résolution de l'interface

Pour définir la résolution de l'interface à utiliser, vous devez modifier la valeur de `es.videomode` dans le fichier `recalbox.conf`.

```ini
es.videomode=1280x1080
```

Vous pouvez aussi utiliser la valeur `default` pour utiliser la résolution native de votre écran.

>Si la ligne `es.videomode` n'existe pas, vous devez l'ajouter sur une ligne vide dans le même fichier.
{.is-info}

### Configuration de la résolution par système

Pour définir la résolution d'un système en particulier, vous devez modifier la valeur qui correspond au système. Exemples :

```ini
nes.videomode=640x480
snes.videomode=1280x1024
```

Vous pouvez aussi utiliser la valeur `default` pour utiliser la résolution native de votre écran.

>Si la ligne du système n'existe pas, vous devez l'ajouter sur une ligne vide dans le même fichier.
{.is-info}

## Configuration pratique

En dehors des PC, vous pouvez définir les mêmes résolutions depuis l'interface en appuyant sur `START` et en allant dans `Paramètres avancées` > `Résolutions`.