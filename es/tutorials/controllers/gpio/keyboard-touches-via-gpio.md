---
title: Teclas del teclado a travès del GPIO
description: 
published: true
date: 2023-09-18T20:21:51.574Z
tags: gpio, teclado, teclas
editor: markdown
dateCreated: 2023-09-18T20:21:51.574Z
---

Recalbox incluye el módulo `retrogame` bajo el nombre `recalbox-retrogame`. Este módulo os permite usar los pines GPIOs para reproducir el comportamiento de un teclado USB real en Recalbox.

>Sólo funciona si la línea `controllers.gpio.enabled=0` está presente en recalbox.conf.
{.is-warning}

* Si queréis integrar un Raspberry Pi en una consola antigua y reutilizar los botones del panel frontal (por ejemplo, los `PAUSE/SELECT` de una Atari 7800):
   * Si queréis cablear (dentro del mueble arcade) un botón para que se comporte como la tecla Escape de un teclado.
   * O simplemente para prescindir totalmente del teclado.

## Instalación

### Cableado

Necesitáis usar pulsadores (que suben al pulsarlos) conectando un terminal a una masa (GROUND) y el otro terminal a un GPIO.

### Script

Para lanzar el módulo automáticamente cuando se inicia Recalbox, es importante añadir un script.

* Cread un fichero de texto `S99retrogame` sin extensión.
* Añadid las siguientes dos líneas de código: 

```ini
/usr/bin/recalbox-retrogame
exit 0
```

* Asignad los permisos de ejecución adecuados:

```shell
chmod 775 /etc/init.d/S99retrogame
```

* Meted este fichero dentro de `/etc/init.d/`.
* Reiniciad y verificad por [SSH](./../../../tutorials/system/access/root-access-terminal-cli) si se ha lanzado correctamente el módulo utilizando el siguiente comando:

```shell
ps aux | grep recalbox-retrogame
```

Si el comando devuelve algo, quiere decir que está funcionando correctamente.