---
title: Libretro MAME 2003
description: 
published: true
date: 2021-10-11T23:23:30.280Z
tags: libretro, neo-geo, mame2003
editor: markdown
dateCreated: 2021-08-01T10:34:17.585Z
---



## ![](/emulators/license.svg) License

This core is under **MAME non-commercial** license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Features



## ![](/emulators/bios.svg) BIOS

### List of mandatory bios

| Filename | Description | MD5 | Provided |
| :---: | :---: | :---: | :---: |
| neogeo.zip | Neo-Geo BIOS | e023d0a4b5249fdff2a4620c28963944 8cbdff53661f41ddbe486a98915e1ec9 42e359e394f0be1d61b94ddca0efbe6c c1e0a738d266926f604186aa9d63e4db d12bba59121229ff13702878b415cb7c 2b4ffdf24d5d4f24c4b116c50c4bafc3 005b843b6e70b939c2cca41887cbc371 42e359e394f0be1d61b94ddca0efbe6c 4b9432a4571d03cc8bae9d24d8d6eb40 4cd64313e26537c04cdb561b8f84b0e4 50bbfd6bf297038b085e29265c65723a 76ee7390ccd537cddea1018025ca29a8 84d6c35aad790a77ba3cfc1b319e1061 912c0a56bafe0fba39d0c668b139b250 94748719c0ef31e7375b0c357d82fc89 ab25b9e25052a6064901a7a221037eb6 ae9e5825cde82c58e39dfd48534f7060 b18f83feef474b8a1306ef199cd810a2 b4519df27a352c2a6e42e06d31330d91 bf00272e7150b31156a8995d60bf185d ca66fd463ef7dcab492a6de8ce5f45eb f4a125538dfd7a8044e025fc5188fb88 a3b2c9a69c28ad0bd4ea07877b744bbe | ❌  |

### Location

Put the bios like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 neogeo
┃ ┃ ┃ ┃ ┣ 🗒 **neogeo.zip**

## ![](/emulators/roms.png) Roms

### Supported extensions

Roms must have the extension:

* .zip
* .7z

### Location

Put the roms like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 neogeo
┃ ┃ ┃ ┃ ┣ 🗒 **game.zip**

>The roms using **No-Intro** format are highly recommended.
{.is-success}

>For more information about roms, go to [this tutorial](./../../../tutorials/games/generalities/isos-and-roms)!
{.is-info}

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Accessing to options

You can configure various options in two different ways.

* Via RetroArch menu:

┣ 📁 RetroArch Menu
┃ ┣ 📁 Core options
┃ ┃ ┣ 🧩 Name_option

* Via the file `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core options

| Options | Options values | Variables (for configuration override) | Variables values |
| --- | --- | --- | --- |
| Skip Disclaimer | `Disabled` ✅ / `Enabled` | `mame2003_skip_disclaimer` | `disabled` / `enabled` |
| Skip Warnings | `Disabled` ✅ / `Enabled` | `mame2003_skip_warnings` | `disabled` / `enabled` |
| Display MAME Menu | `Disabled` ✅ / `Enabled` | `mame2003_display_setup` | `disabled` / `enabled` |
| Legacy Remapping | `Enabled` ✅ / `Disabled` | `mame2003_mame_remapping` | `enabled` / `disabled` |
| Locate System Files Within a Subfolder | `Enabled` ✅ / `Disabled` | `mame2003_core_sys_subfolder` | `enabled` / `disabled` |
| Locate Save Files Within a Subfolder | `Enabled` ✅ / `Disabled` | `mame2003_core_save_subfolder` | `enabled` / `disabled` |
| Mouse Device | `mouse` ✅ / `pointer` / `disabled` | `mame2003_mouse_device` | `mouse` / `pointer` / `disabled` |
| Input Interface | `simultaneous` ✅ / `retropad` / `keyboard` | `mame2003_input_interface` | `simultaneous` / `retropad` / `keyboard` |
| Map Right Analog Stick as Buttons | `Enabled` ✅ / `Disabled` | `mame2003_rstick_to_btns` | `enabled` / `disabled` |
| Dip Switch/Cheat Input Ports | `Disabled` ✅ / `Enabled` | `mame2003_cheat_input_ports` | `disabled` / `enabled` |
| DCS Speedhack | `Enabled` ✅ / `Disabled` | `mame2003_dcs_speedhack` | `enabled` / `disabled` |
| Sample Rate | `8000 KHz` / `11025 KHz` / `22050 KHz` / `30000 KHz` / `44100 KHz` ✅ / `48000 KHz` | `mame2003_sample_rate` | `8000` / `11025` / `22050` / `30000` / `44100` / `48000` |
| Bypass Timing Skew | `Disabled` ✅ / `Enabled` | `mame2003_machine_timing` | `disabled` / `enabled` |
| Brightness | `0.2` / `0.3` / `0.4` / `0.5` / `0.6` / `0.7` / `0.8` / `0.9` / `1.0` ✅ / `1.1` / `1.2` / `1.3` / `1.4` / `1.5` / `1.6` / `1.7` / `1.8` / `1.9` / `2.0` | `mame2003_brightness` | `0.2` / `0.3` / `0.4` / `0.5` / `0.6` / `0.7` / `0.8` / `0.9` / `1.0` / `1.1` / `1.2` / `1.3` / `1.4` / `1.5` / `1.6` / `1.7` / `1.8` / `1.9` / `2.0` |
| Gamma Correction | `0.5` / `0.6` / `0.7` / `0.8` / `0.9` / `1.0` ✅ / `1.1` / `1.2` / `1.3` / `1.4` / `1.5` / `1.6` / `1.7` / `1.8` / `1.9` / `2.0` | `mame2003_gamma` | `0.5` / `0.6` / `0.7` / `0.8` / `0.9` / `1.0` / `1.1` / `1.2` / `1.3` / `1.4` / `1.5` / `1.6` / `1.7` / `1.8` / `1.9` / `2.0` |
| TATE Mode | `Disabled` ✅ / `Enabled` | `mame2003_tate_mode` | `disabled` / `enabled` |
| Frameskip | `Disabled` ✅ / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `9` / `10` / `11` / `auto` / `auto aggressive` / `auto max` | `mame2003_frameskip` | `disabled` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `9` / `10` / `11` / `auto` / `auto_aggressive` / `auto_max` |
| Display Artwork | `Enabled` ✅ / `Disabled` | `mame2003_display_artwork` | `enabled` / `disabled` |
| Resolution Multiplier | `1` ✅ / `2` | `mame2003_art_resolution` | `1` / `2` |
| Specify Neo Geo BIOS | `default` ✅ / `euro` / `euro-s1` / `us` / `us-e` / `asia` / `japan` / `japan-s2` / `unibios33` / `unibios20` / `unibios13` / `unibios11` / `unibios10` / `debug` / `asia-aes` | `mame2003_neogeo_bios` | `default` / `euro` / `euro-s1` / `us` / `us-e` / `asia` / `japan` / `japan-s2` / `unibios33` / `unibios20` / `unibios13` / `unibios11` / `unibios10` / `debug` / `asia-aes` |
| CPU Clock Scale | `default` ✅ / `25%` / `30%` / `35%` / `40%` / `45%` / `50%` / `55%` / `60%` / `65%` / `70%` / `75%` / `80%` / `85%` / `90%` / `95%` / `105%` / `110%` / `115%` / `120%` / `125%` | `mame2003_cpu_clock_scale` | `default` / `25` / `30` / `35` / `40` / `45` / `50` / `55` / `60` / `65` / `70` / `75` / `80` / `85` / `90` / `95` / `105` / `110` / `115` / `120` / `125` |

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/libretro/mame2003-libretro/](https://github.com/libretro/mame2003-libretro/)