---
title: 8Bitdo en Recalbox
description: Lista completa de mandos 8bitdo que han sido probados por la comunidad Recalbox.
published: true
date: 2023-11-03T14:19:59.850Z
tags: 8bitdo, mandos
editor: markdown
dateCreated: 2023-09-19T07:40:15.473Z
---

![8bitdo](http://support.8bitdo.com/images/Logo-black.svg)

![Liste des manettes 8Bitdo au 13 novembre 2019](/compatibility/8bitdocollection.png){.full-width}

## Mandos soportados 8Bitdo

* FW = Firmware
* min = Mínimo
* rec = Recomendado
* BT = Bluetooth

### Mando estilo SNES

| Imagen | Mando | Firmware | USB | BT | Modo de arranque | Emparejamiento *BT* | Reset BT | Reset de fábrica |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ![](/compatibility/8bitdo/bluetooth/pro2.gif) | Pro 2 | 3.02 |  ✅ | ? | ? | ? | ? | ? |
| ![](/compatibility/8bitdo/wired/pro2_xbox.png) | Pro2 Xbox | ? | ❌ | ❌ | n/a | n/a | n/a | n/a |
| ![](/compatibility/8bitdo/bluetooth/sn30pro+.gif) | SN30 Pro+ | 5.04 | ✅ | ☑️ | START + B | Mantened pulsado el botón PAIR durante 2 segundos | Mantened pulsado el botón PAIR durante 2 segundos | [Ver el manual en la página oficial de 8Bitdo](https://download.8bitdo.com/Manual/Controller/SN30-Pro+/SN30_Pro+_Manual.pdf) |
| ![](/compatibility/8bitdo/bluetooth/sn30prov2.gif) | SN30 Pro 2 | 1.32 | ✅ | ☑️ | START + B | Mantened pulsado el botón PAIR durante 3 segundos | Mantened pulsado el botón PAIR durante 3 segundos | [Ver el manual en la página oficial de 8Bitdo](https://download.8bitdo.com/Manual/Controller/SN30pro+SF30pro/SN30pro_Manual.pdf) |
| ![](/compatibility/8bitdo/bluetooth/sf30prov1.gif) | SN30 Pro & SF30 Pro | 2.02 | ✅ | ☑️ | START + B | Mantened pulsado el botón PAIR durante 3 segundos | Mantened pulsado el botón Select durante 3 segundos | [Mantened apretado el botón Select durante 8 segundos](https://download.8bitdo.com/Manual/Controller/SN30pro+SF30pro/SN30pro+SF30pro_Manual.pdf) |
| ![](/compatibility/8bitdo/wired/sn30pro-usb.gif) | SN30 Pro USB | 1.03 | ? | n/a | n/a | n/a| n/a | [Ver el manual en la página oficial de 8Bitdo](https://download.8bitdo.com/Manual/Controller/SN30pro-USB/SN30-Pro-USB.pdf) |
| ![](/compatibility/8bitdo/bluetooth/sn30v3color.gif) | SN30 GP 2 | 6.16 | ? | ? | B + START | Mantened pulsado el botón Select durante 3 segundos | Mantened pulsado el botón Select durante 3 segundos | [Ver el manual en la página oficial de 8Bitdo](https://download.8bitdo.com/Manual/Controller/SN30+SF30/SN30_Manual.pdf) |
| ![](/compatibility/8bitdo/bluetooth/sn30v2.gif) | SN30 Retro Set | 4.10 | ? | ? | START | Mantened pulsado el botón Select durante 3 segundos | Mantened pulsado el botón Select durante 3 segundos | [Ver el manual en la página oficial de 8Bitdo](https://download.8bitdo.com/Manual/Controller/SN30+SF30/SN30_Analogue.pdf) |
| ![](/compatibility/8bitdo/bluetooth/sn30v3.gif) | SN30 | 4.20 | ✅ | ☑️ | START + R | Mantened pulsado el botón Select durante 3 segundos | Mantened pulsado el botón Select durante 3 segundos | [Mantened apretado el botón Select durante 8 segundos](https://download.8bitdo.com/Manual/Controller/SN30+SF30/SN30_Analogue.pdf) |
| ![](/compatibility/8bitdo/bluetooth/sf30.gif) | SF30 &amp; SFC30 | 4.20 | ✅ | ☑️ | START + R | Mantened pulsado el botón Select durante 3 segundos | Mantened pulsado el botón Select durante 3 segundos | [Mantened apretado el botón Select durante 8 segundos](https://download.8bitdo.com/Manual/Controller/SN30+SF30/SN30+SF30_Manual_V4.pdf) |
| ![](/compatibility/8bitdo/bluetooth/sn30pro_xcloud.png) | SN30 Xbox | ? | ❌ | ❌ | n/a | n/a | n/a | n/a |

### Mandos estilo SNESxNES

| Imagen | Mando | Firmware | USB | BT | Modo de arranque | Emparejamiento *BT* | Reset BT | Reset de fábrica |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ![](/compatibility/8bitdo/bluetooth/n30pro2.gif) | N30 Pro 2 | 6.10 | ? | ? | START | Maintenir Mantened pulsado el botón Pair durante 1 segundos | Mantened pulsado el botón Pair durante 3 segundos | [Ver el manual en la página oficial de 8Bitdo](https://download.8bitdo.com/Manual/Controller/N30pro2/N30pro2-Manual.pdf) |
| ![](/compatibility/8bitdo/bluetooth/nes30pro.jpg) | N30 &amp; NES30 Pro | 4.10 | ✅ | ☑️ | POWER | Mantened pulsado el botón Pair durante 1 segundos | Mantened pulsado el botón Pair durante 3 segundos | [Mantened apretado el botón Start durante 8 segundos](https://download.8bitdo.com/Manual/Controller/N30pro+F30pro/N30pro+F30pro_Manual_V4.pdf) |
| ![](/compatibility/8bitdo/bluetooth/fc30pro.jpg) | F30 &amp; FC30 Pro | 4.10 | ✅ | ☑️ | POWER + X | Mantened pulsado el botón Pair durante 1 segundos | Mantened pulsado el botón Pair durante 3 segundos | [Mantened apretado el botón Start durante 8 segundos](https://download.8bitdo.com/Manual/Controller/N30pro+F30pro/N30pro+F30pro_Manual_V4.pdf) |

### Mandos estilo NES

| Imagen | Mando | Firmware | USB | BT | Modo de arranque | Emparejamiento *BT* | Reset BT | Reset de fábrica |
| :---: |  :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ![](/compatibility/8bitdo/bluetooth/n30.png) | N30 NS | 6.10 | ? | ? | START | Maintenir appuyé le bouton Select pendant 3 secondes | Maintenir appuyé le bouton Select pendant 3 secondes | [Ver el manual en la página oficial de 8Bitdo](https://download.8bitdo.com/Manual/Controller/N30-NS/N30-NS.pdf) |
| ![](/compatibility/8bitdo/bluetooth/nes30.jpg) | N30 &amp; NES30 | 4.20 | ✅ | ☑️ | START + R | Maintenir Mantened pulsado el botón Select durante 3 segundos | Mantened pulsado el botón Select durante 3 segundos | [Mantened apretado el botón Select durante 8 segundos](https://download.8bitdo.com/Manual/Controller/N30+F30/N30+F30_Manual_V4.pdf) |
| ![](/compatibility/8bitdo/bluetooth/fc30.jpg) | F30 &amp; FC30 | 4.20 | ✅ | ☑️ | START + R | Maintenir Mantened pulsado el Select Pair durante 3 segundos | Mantened pulsado el botón Select durante 3 segundos | [Mantened apretado el botón Select durante 8 segundos](https://download.8bitdo.com/Manual/Controller/N30+F30/N30+F30_Manual_V4.pdf) |

### Mandos estilo MEGADRIVE

| Imagen | Mando | Firmware | USB | BT | Modo de arranque | Emparejamiento *BT* | Reset BT | Reset de fábrica |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ![](/compatibility/8bitdo/24gcontrollers/m30.png) | M30 | 1.15 | ✅ | ☑️ | START + B | Maintenir Mantened pulsado el botón Select durante 1 segundos | Mantened pulsado el botón Select durante 3 segundos | [Ver el manual en la página oficial de 8Bitdo](https://download.8bitdo.com/Manual/Controller/M30/M30_Manual.pdf) |
| ![](/compatibility/8bitdo/24gcontrollers/m30-2.4g.gif) | M30 2.4G | - | ❌ | ❌ | - | START | START | [Voir le manuel sur le site de 8Bitdo](https://download.8bitdo.com/Manual/Controller/M30-2.4g/M30-2.4G-for-genesis-mini-mega-drive-mini.pdf)

### Mandos estilo ARCADE

| Imagen | Mando | Firmware | USB | BT | Modo de arranque | Emparejamiento *BT* | Reset BT | Reset de fábrica |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ![](/compatibility/8bitdo/arcade/n30-arcade-stick.png) | N30 &amp; NES30 Arcade Stick | 5.01 | ✅ | ☑️ | HOME | Mantened pulsado el botón Select durante 3 segundos | Mantened pulsado el botón Select durante 3 segundos | [Ver el manual en la página oficial de 8Bitdo](https://download.8bitdo.com/Manual/Controller/Joystick/N30-Arcade-Manual.pdf) |
| ![](/compatibility/8bitdo/arcade/fc30-arcade-stick.png) | F30 & FC30 Arcade Stick | 1.42 | ✅ | ☑️ | HOME | Mantened pulsado el botón Select durante 3 segundos | Mantened pulsado el botón Select durante 3 segundos | [Ver el manual en la página oficial de 8Bitdo](https://download.8bitdo.com/Manual/Controller/Joystick/F30-ArcadeJoystick-eng.pdf) |
| ![](/compatibility/8bitdo/arcade/arcade-stick.png) | Arcade Stick | 1.03 | ? | ? | ? | ? | ? | [Ver el manual en la página oficial de 8Bitdo](https://download.8bitdo.com/Manual/Controller/Joystick/Arcade-Stick-V3-Manual.pdf) |

### Otros estilos de mandos

| Imagen | Mando | Firmware | USB | BT | Modo de arranque | Emparejamiento *BT* | Reset BT | Reset de fábrica |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ![](/compatibility/8bitdo/24gcontrollers/neogeo.png) | NEOGEO Wireless Controller | ? | ? | ? | START | Pulsad el botón Pair | Mantened pulsado el botón Pair durante 1 segundos | [Ver el manual en la página oficial de 8Bitdo](https://download.8bitdo.com/Manual/Controller/SNK/neogeo-wireless-controller_Manual.pdf) |
| ![](/compatibility/8bitdo/bluetooth/lite.gif) | Lite | 1.03 | ✅ | ❌ | START + B | Mantened pulsado el botón Select durante 2 segundos | Mantened pulsado el botón Pair durante 1 segundos | [Ver el manual en la página oficial de 8Bitdo](https://download.8bitdo.com/Manual/Controller/Lite/Lite_Manual.pdf) |
| ![](/compatibility/8bitdo/bluetooth/lite-se.png) | Lite SE | ? | ? | ? | ? | ? | ? | ? |
| ![](/compatibility/8bitdo/bluetooth/lite-2.gif) | Lite 2 | ? | ? | ? | ? | ? | ? | ? |
| ![](/compatibility/8bitdo/bluetooth/zero.gif) | Zero | n/a | ❌ | ☑️ | START + R1 | Mantened pulsado el botón Select durante 3 segundos | Mantened pulsado el botón Select durante 3 segundos | [Mantened apretado el botón Select durante 8 segundos](https://download.8bitdo.com/Manual/Controller/ZERO/ZERO_Manual_V4.pdf) |
| ![](/compatibility/8bitdo/bluetooth/zero2.gif) | Zero 2 | 1.06 | ✅ | ☑️ | B + START | Mantened pulsado el botón Select durante 3 segundos | Mantened pulsado el botón Select durante 3 segundos | [Mantened apretado el botón Select durante 8 segundos](https://download.8bitdo.com/Manual/Controller/Zero2/Zero2_Manual.pdf) |
| ![](/compatibility/8bitdo/24gcontrollers/pce-2_4g.gif) | PCE / TG16 / PCE Core | ? | ? | ? | Home | Appuyer le bouton Home | ? | ? |
| ![](/compatibility/8bitdo/24gcontrollers/ultimate-c-2.4g.gif) | Ultimate C Wireless Controller | ? | ? | ? | HOME | HOME + B | HOME + B | [Ver el manual en la página oficial de 8Bitdo](https://download.8bitdo.com/Manual/Controller/Ultimate/Ultimate-C-2.4G-Wireless-Controller.pdf) |
| ![](/compatibility/8bitdo/24gcontrollers/ultimate-2.4g.gif) | Ultimate Wireless Controller (en mode D) | ? | ? | ? | Home | Mantened pulsado el botón estrella durante 3 segundos | ? | ? |
| ![](/compatibility/8bitdo/bluetooth/ultimate-bluetooth.gif) | Ultimate Bluetooth Controller | ? | ? | ? | ? | Mantened pulsado el botón Pair durante 3 segundos | ? | ? |

## Dongles 8Bitdo soportados

| Imagen | Dongle | Firmware | Compatible con Recalbox | Mandos compatibles |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ![](/compatibility/8bitdo/dongles/wireless-usb-adapter-1.png) | USB Wireless Adapter (en modo X-Input) | 2.00 | ✅ | PS3, PS4 (pro), PS5, Xbox One S, Xbox One X, Wiimote, Wii U Pro, Switch Joy-Cons, Switch Pro, mandos y sticks 8bitdo. |
| ![](/compatibility/8bitdo/dongles/usb-rr-for-ps-classic.png) | USB Adapter for PS Classic (en modo X-Input) | 1.33 | ✅ | PS3, PS4, Wiimote, Wii U Pro, Switch Joy-Cons, Switch Pro, mandos 8bitdo (sólo Bluetooth). |
| ![](/compatibility/8bitdo/dongles/wireless-usb-adapter-2.gif) | USB Wireless Adaptater 2 | 1.06 | ✅ | PS3, PS4 (pro), PS5, Xbox Series X \| S, Xbox One, Wiimote, Wii U Pro, Switch Joy-Cons, Switch Pro, manettes et sticks 8bitdo. |
| ![](/compatibility/8bitdo/dongles/gbros.png) | GBros Adapter Emeter (With 8Bitdo Receiver) | 2.24 | ✅ | GameCube, GameCube Wavebird (avec le câble d'extension), Wii Classic, Wii Classic Pro, NES Mini, SNES Mini. |
| ![](/compatibility/8bitdo/dongles/nesmini-rr.png) | NES Receiver | 1.14 | ? | ? |
| ![](/compatibility/8bitdo/dongles/snes-rr.png) | SNES Receiver | 1.34 | ? | ? |

## Ayuda y soporte de 8Bitdo

* [Soporte oficial de 8Bitdo](https://support.8bitdo.com/)
* [Programa para aplicar actualizaciones](https://support.8bitdo.com/firmware-updater.html) 

Los mandos 8Bitdo deben desvincularse antes de poder asociarse de nuevo. Si tenéis problemas para emparejarlos, podéis probar el método de "Reinicialización del Bluetooth", y si esto no funciona, probad el método de "Reset de fábrica".

Encontraréis el manual de cada uno de los mandos en la página web [8Bitdo support] (https://support.8bitdo.com/).

## Faq

### ¿Cómo se recarga el mando?

Preferiblemente, utilizad el puerto USB de vuestro ordenador o consola para recargar el mando.
Si utilizáis un cargador convencional o un cargador de teléfono móvil, verificad que nunca supere los 5V 1A.
Una corriente demasiado alta puede dañar la batería e inutilizar el mando en modo Bluetooth.

### ¿Cómo actualizo el firmware?

Simplemente id a la página web [8Bitdo support] (https://support.8bitdo.com/) y descargad el software `Upgrade tool` en la parte superior de la página.

1. Instalad la aplicación descargada `8BitDo Firmware Upgrader`.
2. Abrid la aplicación
3. Una vez abierta la aplicación, se os pedirá conectar vuestro controlador al PC.
4. El mando será reconocido automáticamente. Pulsad sobre el botón de Actualización de Firmware para acceder a las actualizaciones disponibles para el controlador.
5. Seleccionad el firmware que queréis installar y haced clic en el botón azul `Actualizar`.
6. Seguid el procedimiento indicado para realizar la actualización. La actualización mostrará una barra roja de progreso.
7. Una vez que la actualización se haya completado con éxito, pulsad en el botón verde `Success` y desconectad el controlador.

### ¿Cómo enciendo/conecto correctamente mi mando?

Dependiendo del dispositivo (Windows, Mac OS, Android, Switch, etc.), existen diferentes combinaciones de encendido:

* `START` + `Y` = Modo Switch
* `START` + `A` = modo MacOS
* `START` + `X` = modo PC
* `START` + `B` = modo Bluetooth (a menudo el mejor para Android)

Hay que mantener pulsadas las teclas simultáneamente hasta que se encienda el mando y luego realizar la asociación Bluetooth.

Si el mando se conecta por USB, mantened pulsadas las teclas simultáneamente hasta que se encienda el mando y, a continuación, conectad el cable USB.

### ¿Cómo conecto mi controlador a otro dispositivo?

El mando solo conserva una dirección de dispositivo externo. Si queréis cambiar de Switch a Android, por ejemplo, tendrás que restablecer los datos de asociación del mando. El procedimiento difiere ligeramente en función del modelo. Consultad la tabla anterior para ver el procedimiento.

### ¿Cómo emparejo mi adaptador USB con mi mando de PS3?

Hay una herramienta que podéis utilizar para gestionar el mando de PS3 y los adaptadores USB.

### {.tabset}
#### Windows

Descargad este [programa](https://download.8bitdo.com/Tools/Receiver/8Bitdo_PS3_Tool_Win_V1.1.zip)

En caso de problemas con su instalación bajo Windows, existen dos soluciones:

**Solución 1** :
Instalad SixaxisPairToolSetup-0.3.1.exe. Lo podéis descargar de [esta página](https://sixaxispairtool.en.lo4d.com/download).

**Solución 2** :
Desactivar temporalmente la aplicación de firmas de drivers de Windows. Las imágenes de ayuda se pueden encontrar en la carpeta `How To Temporarily Disable Driver Signature Enforcement On Win8 And Win10` contenida dentro del fichero `8Bitdo_PS3_Tool_Win_V1.1.zip`.

En caso de tener otros problemas, podéis instalar el driver 'libusb-win32'.

>La función de refuerzo de la firma del controlador de Windows puede reactivarse automáticamente durante una actualización del sistema operativo.
{.is-info}

#### macOS
Descargad este [programa](https://download.8bitdo.com/Tools/Receiver/8BitDo_PS3_Tool_MacOS_V1.1.zip)

### ¿Qué pasa con los Arcade sticks?

Dependiendo del dispositivo (Windows, Mac OS, Android, Switch, etc.), existen diferentes combinaciones de encendido:

* Windows: posición XINPUT.  
* Android: posición DINPUT.  
* MacOS y Switch no son necesarios.  
  
Procedimiento de encendido :

1. Seleccionad el modo XINPUT o DINPUT si es necesario.
2. Pulsad el botón PAIR para inicializar el dispositivo (el LED azul parpadea).
3. Configurad el emparejamiento Bluetooth en vuestro dispositivo (Windows, macOS, Android o Switch).
4. El LED debería volverse azul y el joystick queradá listo para usarse.

Mantened pulsados los botones simultáneamente hasta que se encienda el joystick y, a continuación, efectúa la asociación Bluetooh. Si el mando se conecta por USB, mantened pulsados los botones simultáneamente hasta que el mando se encienda y, a continuación, conectad el cable USB.

### ¿Qué firmware se recomienda para Recalbox?

Consultad la tabla de la parte superior de la página para ver el firmware recomendado para vuestro mando.

### ¿Qué pasa con la compatibilidad con el pad zero V1?

El pad zero v1 sólo funciona mediante Bluetooth. Encended el mando manteniendo pulsados los botones `START` + `R1`.

Si utilizáis dos pads zero V1:

- Primer mando, mantened pulsados los botones `START` + `R1`.
- Segundo mando, mantened pulsados los botones `START` + `R1` + `B`.

### ¿No se reconoce mi pad con el cable USB?

Comprobad que estáis utilizando el cable suministrado con el mando. Si no es así, aseguráos que vuestro cable USB es un cable de transferencia de datos y no sólo un cable USB de carga. Algunos cables USB sólo dejan pasar la corriente de carga y no los datos.