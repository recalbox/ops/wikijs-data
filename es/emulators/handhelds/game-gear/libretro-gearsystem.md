---
title: Libretro Gearsystem
description: 
published: true
date: 2024-07-25T22:30:09.907Z
tags: libretro, gamegear, gg, gearsystem
editor: markdown
dateCreated: 2023-09-11T13:25:56.491Z
---

**Libretro Gearsystem** es un emulador multiplataforma y de código abierto de Sega Mark III / Sega Master System / Game Gear / SG-1000 / Othello Multivision escrito en C++.

* Kernel Z80 de alta precisión, incluyendo opcodes no documentados y comportamientos como los registros R y MEMPTR.
* Soporte Multi-Mapper: SEGA, Codemasters, SG-1000 y sólo cartuchos ROM.
* Detección automática de región: NTSC-JAP, NTSC-USA, PAL-EUR.
* Base de datos interna para la detección de gitanos.
* Emulación VDP de alta precisión, incluyendo sincronización y soporte sólo para modo SMS2.
* Emulación de audio mediante SDL Audio y la biblioteca Sms_Snd_Emu.
* Soporte para ahorrador de RAM alimentado por batería.
* Guardar estados.
* Soporte para trucos Game Genie y Pro Action Replay.
* Funciona en Windows, Linux, Mac OS X, Raspberry Pi, iOS y como núcleo Libretro (RetroArch).

El núcleo Libretro Gearsystem fue creado por Ignacio Sánchez

## ![](/emulators/license.svg) Licencia

El core posee la siguiente licencia [**GPLv3**](https://github.com/drhelius/Gearsystem/blob/master/LICENSE).

## ![](/emulators/compatibility.png) Compatibilidad

 | RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | 
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | 

## ![](/emulators/features.png) Funcionalidades

| Funcionalidad | ¿Soportada? |
| :---: | :---: |
| Reinicio | ✔ |
| Capturas de pantalla | ✔ |
| Grabaciones | ✔ |
| Grabaciones instantáneas | ✔ |
| Rebobinado | ✔ |
| Opciones del core | ✔ |
| RetroAchievements | ✔ |
| Cheats RetroArch - Game Genie | ✔ |
| Cheats RetroArch - Pro Acion Replay | ✔ |
| Controles | ✔ |
| Remapping | ✔ |
| Softpatching | ✔ |

## ![](/emulators/bios.svg) BIOS

### Lista des bios opcionales

| Nombre del fichero | Descripción | MD5 | ¿Proporcionado? |
| :---: | :---: | :---: | :---: |
| <span>bios</span>.gg | BIOS GameGear (bootrom) | 672e104c3be3a238301aceffc3b23fd6 | ❌ |
 
 ### Localización
 
 Colocad las bios como en la siguiente figura:
 
 ┣ 📁 recalbox
 ┃ ┣ 📁 share
 ┃ ┃ ┣ 📁 bios
 ┃ ┃ ┃ ┣ 📁 gamegear
 ┃ ┃ ┃ ┃ ┣ 🗒 **<span>bios</span>.gg**
 

## ![](/emulators/roms.png) Roms

### Extensiones de roms soportadas

Las roms deben tener las extensiones siguientes:

* .gg
* .zip
* .7z

Este sistema soporta las roms comprimidas en formato .zip/.7z. No obstante, se trata sólo de un archivo.

Los ficheros contenidos en los .zip/.7z deben tener las extensiones citadas previamente.
Además, cada fichero .zip/.7z sólo debe contener un fichero rom al interior.

### Emplazamiento

Disponed vuestras roms como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 gamegear
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.zip**

>Se aconsejan las roms del proyecto **No-Intro**.
{.is-success}

>Para más información sobre las roms, consultad [este tutorial](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

>Para conservar las configuraciones personalizadas cuando se produce una actualización del sistema Recalbox, os aconsejamos utilizar las [Sobrecargas de configuración](./../../../advanced-usage/configuration-override).
{.is-info}

### Acceder a las opciones

Podéis configurar las opciones del core de dos formas diferentes.

* Via el Menú RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via el fichero `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Opciones del core

| Opción | Valor de la opción | Variable (para la sobrecarga) | Valores posibles |
| --- | --- | --- | --- |
 | System (restart) | `Auto` ✅ / `Master System / Mark III` / `Game Gear` / `SG-1000 / Multivision` | `gearsystem_system` | `Auto` / `Master System / Mark III` / `Game Gear` / `SG-1000 / Multivision` |
 | Region (restart) | `Auto` ✅ / `Master System Japan` / `Master System Export` / `Game Gear Japan` / `Game Gear Export` / `Game Gear International` | `gearsystem_region` | `Auto` / `Master System Japan` / `Master System Export` / `Game Gear Japan` / `Game Gear Export` / `Game Gear International` |
 | Mapper (restart) | `Auto` ✅ / `ROM` / `SEGA` / `Codemasters` / `Korean` / `MSX` / `Janggun` / `SG-1000` | `gearsystem_mapper` | `Auto` / `ROM` / `SEGA` / `Codemasters` / `Korean` / `MSX` / `Janggun` / `SG-1000` |
 | Refresh Rate (restart) | `Auto` ✅ / `NTSC (60 Hz)` / `PAL (50 Hz)` | `gearsystem_timing` | `Auto` / `NTSC (60 Hz)` / `PAL (50 Hz)` |
 | Master System BIOS (restart) | `Désactivé` ✅ / `Activé` | `gearsystem_bios_sms` | `Disabled` / `Enabled` |
 | Game Gear BIOS (restart) | `Désactivé` ✅ / `Activé` | `gearsystem_bios_gg` | `Disabled` / `Enabled` |
 | 3D Glasses | `Both Eyes / OFF` ✅ / `Left Eye` / `Right Eye` | `gearsystem_glasses` | `Both Eyes / OFF` / `Left Eye` / `Right Eye` |
 | Allow Up+Down / Left+Right | `Désactivé` ✅ / `Activé` | `gearsystem_up_down_allowed` | `Disabled` / `Enabled` |


## ![](/emulators/external-links.png) Enlaces externos

* **Código fuente utilizado** : [https://github.com/drhelius/Gearsystem/](https://github.com/drhelius/Gearsystem/)
* **Documentación Libretro** : [https://docs.libretro.com/library/gearsystem/](https://docs.libretro.com/library/gearsystem/)