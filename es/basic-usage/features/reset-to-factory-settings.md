---
title: Restauración de los parámetros de fábrica
description: 
published: true
date: 2024-11-11T05:29:53.941Z
tags: 7.2+, configuración, restaurar, fábrica, parámetros
editor: markdown
dateCreated: 2023-08-23T14:08:00.937Z
---

## Presentación

Una opción llamada `Retorno a los parámetros de fábrica` que permite reiniciar toda la configuración para dejarla en el estado inicial de cuando se instala Recalbox por primera vez.

Esta funcionalidad permite reconfigurar Recalbox como si fuera una nueva instalación pero sin tener que grabar una image del sistema.

>Esta opción **no modifica** las bios, juegos y/o grabaciones de vuestra Recalbox, sólo la configuración!
{.is-success}

## Puesta en marcha

Se accede a la opción desde EmulationStation. Para ello, pulsad `START`, id dentro de `PARAMETROS AVANZADOS` y seleccionad `RESTAURAR LOS PARÁMETROS DE FÁBRICA`.

![](/basic-usage/features/resetfactory1.png){.full-width}

Una vez hecho esto, se muestran dos ventanas de aviso.

![](/basic-usage/features/resetfactory2.png){.full-width}

![](/basic-usage/features/resetfactory3.png){.full-width}

>Tened cuidado porque una vez validadas las dos ventanas de aviso, **no podréis echaros atrás**.
{.is-danger}

Cuando la restauración de la configuración de fábrica (o configuración inicial del sistema, o configuración predefinida) termine, tendréis vuestro sistema de vuelta al estado inicial igual que cuando instaláis Recalbox por la primera vez! 

Se recomienda utilizar esta opción depués de una actualización mayor del sistema o cuando os encontráis con cosas raras que no funcionan. En la mayoría de los casos una restauración de la configuración de base puede corrigir estos problemas.
{.is-info}



