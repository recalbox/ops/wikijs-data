---
title: Libretro BK
description: 
published: true
date: 2021-11-05T23:32:17.244Z
tags: libretro, elektronika, bk, 7.2+
editor: markdown
dateCreated: 2021-07-29T16:36:55.626Z
---



## ![](/emulators/license.svg) License

This core is under [**custom**](https://github.com/libretro/bk-emulator/blob/master/COPYING) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅  | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Features



## ![](/emulators/bios.svg) BIOS

### List of mandatory bios

| Filename | Description | MD5 | Provided |
| :---: | :---: | :---: | :---: |
| B11M_BOS.ROM | - | fe4627d1e3a1535874085050733263e7 | ❌ |
| B11M_EXT.ROM | - | dc52f365d56fa1951f5d35b1101b9e3f | ❌ |
| BAS11M_0.ROM | - | 946f6f23ded03c0e26187f0b3ca75993 | ❌ |
| BAS11M_1.ROM | - | 1e6637f32aa7d1de03510030cac40bcf | ❌ |
| DISK_327.ROM | - | 5015228eeeb238e65da8edcd1b6dfac7 | ❌ |
| FOCAL10.ROM | - | 5737f972e8638831ab71e9139abae052 | ❌ |
| MONIT10.ROM | - | 95f8c41c6abf7640e35a6a03cecebd01 | ❌ |

### Location

Put the bios like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 bk
┃ ┃ ┃ ┃ ┣ 🗒 **B11M_BOS.ROM**
┃ ┃ ┃ ┃ ┣ 🗒 **B11M_EXT.ROM**
┃ ┃ ┃ ┃ ┣ 🗒 **BAS11M_0.ROM**
┃ ┃ ┃ ┃ ┣ 🗒 **BAS11M_1.ROM**
┃ ┃ ┃ ┃ ┣ 🗒 **DISK_327.ROM**
┃ ┃ ┃ ┃ ┣ 🗒 **FOCAL10.ROM**
┃ ┃ ┃ ┃ ┣ 🗒 **MONIT10.ROM**

## ![](/emulators/roms.png) Roms

### Supported extensions

Roms must have the extension:

* .bin
* .zip

This system supports compressed roms in .zip/.7z format. However, be careful, it is only an archive.

The files contained in the .zip/.7z must correspond to the extensions mentioned above.
Moreover, each .zip/.7z file must contain only one compressed rom.

### Location

Put the roms like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 bk
┃ ┃ ┃ ┃ ┣ 🗒 **game.zip**

>The roms using **TOSEC** format are highly recommended.
{.is-success}

>For more information about roms, go to [this tutorial](./../../../tutorials/games/generalities/isos-and-roms)!
{.is-info}

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Accessing to options

You can configure various options in two different ways.

* Via RetroArch menu:

┣ 📁 RetroArch Menu
┃ ┣ 📁 Core options
┃ ┃ ┣ 🧩 Name_option

* Via the file `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core options

| Options | Options values | Variables (for configuration override) | Variables values |
| --- | --- | --- | --- |
| Model (restart) | `BK-0010` ✅ / `BK-0010.01` / `BK-0010.01 + FDD` / `BK-0011M + FDD` / `Terak 8510/a` / `Slow BK-0011M` | `bk_model` | `BK-0010` / `BK-0010.01` / `BK-0010.01 + FDD` / `BK-0011M + FDD` / `Terak 8510/a` / `Slow BK-0011M` |
| Peripheral (UP port, restart) | `none` ✅ / `covox` / `ay_3_8910` / `mouse_high` / `mouse_low` / `joystick` | `bk_peripheral` | `none` / `covox` / `ay_3_8910` / `mouse_high` / `mouse_low` / `joystick` |
| Keyboard layout | `qwerty` ✅ / `jcuken` | `bk_layout` | `qwerty` / `jcuken` |
| Double CPU speed | `disabled` ✅ / `enabled` | `bk_doublespeed` | `disabled` / `enabled` |
| Use color display | `enabled` ✅ / `disabled` | `bk_color` | `enabled` / `disabled` |

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/libretro/bk-emulator/](https://github.com/libretro/bk-emulator/)