---
title: Atari 7800
description: 
published: true
date: 2024-07-26T07:44:30.324Z
tags: atari, 7800, atari-7800, consolas
editor: markdown
dateCreated: 2023-09-03T15:44:00.406Z
---

![](/emulators/consoles/atari7800.svg){.align-center}

## Datos técnicos

* **Fabricante:** Atari Corporation
* **Año de lanzamiento :** 1986
* **Cantidades vendidas :** 3,77 millones
* **Procesador:** Atari SALLY 6502 ("6502C") @ 1.19-1.79MHz
* **RAM:** 4KB
* **Tamaño del cartucho:** 48KB
* **Procesador de audio/vídeo:** Atari SALLY 6502 ("6502C") a 1,19-1,79 MHz
* **Puertos:** 2 puertos de joystick, 1 puerto de cartucho, 1 puerto de expansión, fuente de alimentación, salida RF

## Presentación

La **Atari 7800** es una videoconsola de 8 bits de tercera generación diseñada y comercializada por el fabricante estadounidense Atari. Salió a la venta a principios de 1986 en Estados Unidos, más tarde ese mismo año en Japón y sólo en 1991 en Francia.

Sucesora de la Atari 5200, la Atari 7800 compitió con la Nintendo Entertainment System (NES) y la Sega Master System durante la era de las videoconsolas de tercera generación. Sin embargo, la consola nunca llegó a rivalizar con sus competidoras.

## Emuladores

[Libretro Prosystem](libretro-prosystem)