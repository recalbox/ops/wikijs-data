---
title: CRT Scart and VGA on Raspberry Pi 4
description: How to play Recalbox on your CRT screens on Raspberry Pi 4.
published: false
date: 2021-09-18T18:46:34.524Z
tags: crt, screen, cathodic, pixel perfect, vga, 240p, 224p
editor: markdown
dateCreated: 2021-09-10T23:13:51.200Z
---

## Presentation

As Recalbox users, you know how good it is to feel the nostalgia when playing again the games that rocked our childhood.

Playing with a PS4 controller on the living room OLED TV already allows us to find and share this pleasure of retro gaming.

However, some of us have an insatiable thirst that forces us to go further and further in the search for the closest possible experience to the original.

In this endless search, the display, or the image of the game is probably the most important point.

And what could be closer to the original than playing on the CRT screens of the time?

## Recalbox on your CRT screen

Recalbox is able to reproduce the exact resolution and frequencies of most consoles and arcade games of the time, on your CRT TV.

Get a VGA666 LINK and a SCART cable, or a RGBPi LINK.

Once the VGA-666 or the RGBPi is connected to your Rasbperry Pi 4 and to the TV, a single line of configuration is enough to activate the **Recalbox CRT** mode:

```
system.crt=vga666
```

**That's all**, after restarting the Recalbox, the system configures itself to send the image directly to your TV.

------------------

## Hardware Compatibility

* **VGA666**: CRT TV with modified cable, CRT VGA monitor, PVM
The VGA666 allows you to connect a VGA cable (old CRT monitors). With a modified VGA->PERITEL cable, it is fully compatible with CRT TVs
* **RGBPi**: CRT TV, PVM
The RGBPi is the turnkey solution to connect your Recalbox to your CRT.


## Intermediate configuration
```
system.crt=vga666
# Image shift to the left (negative) or right (positive)
system.crt.horizontal_offset=-50
# Offset the image up (negative) or down (positive)
system.crt.horizontal_offset=-2

system.crt=vga666
```

## Advanced configuration

The configuration of the display modes by system and by arcade game can be found in the folder `/recalbox/system/configs/crt/`

* `modes.txt` lists the display modes that can be used
* `system.txt` defines for each system the mode to use
* `arcade_games.txt` defines for each arcade game the mode to use

To override or replace these configurations, you just have to create the corresponding files in the `/recalbox/share/system/configs/crt/` folder.

These overload files should only contain the modes, systems or games you want to overload or add.


File structure:
┣ 📁 recalbox
┃ ┣ 📁 system
┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┣ 📁 crt
┃ ┃ ┃ ┃ ┣ 🗒 arcade_games.txt (readonly)
┃ ┃ ┃ ┃ ┣ 🗒 modes.txt (readonly)
┃ ┃ ┃ ┃ ┣ 🗒 systems.txt (readonly)
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 crt
┃ ┃ ┃ ┃ ┃ ┣ 🗒 arcade_games.txt
┃ ┃ ┃ ┃ ┃ ┣ 🗒 modes.txt
┃ ┃ ┃ ┃ ┃ ┣ 🗒 systems.txt

### File structure
#### `modes.txt`
The `modes.txt` file contains one mode per line, along with its identifier, in the following format:
```
MODE_ID,MODE
```

Example:
```
snes:nes:ntsc:224@60.0988,1920 1 80 184 312 224 1 9 3 24 0 0 0 60 0 39001717 1
```
Here the MODE_ID is `snes:nes:ntsc:224@60.0988` and the MODE is `1920 1 80 184 312 224 1 9 3 24 0 0 0 60 0 39001717 1`


#### `systems.txt`
The `systems.txt` file allows for each system, to configure a display mode per region, and to select the size of the display area:
```
SYSTEM_ID,REGION,MODE_ID,VIEWPORT_WIDTH,VIEWPORT_HEIGHT
```

Example:
```
snes,ntsc,snes:nes:ntsc:224@60.0988,0,0
```

This line lets Recalbox know that the Super Nintendo (`snes`), when loading an NTSC (non-European) game, will use the display mode identified by `snes:nes:ntsc:224@60.0988` (in `modes.txt`).

The next two zeros let Recalbox set the best value for the height and width of the image.

#### `arcade_games.txt`
The `arcade_games.txt` file allows to select the display mode for each arcade game, depending on the core (or emulator) on which it will be launched. It also allows to change the size of the display area:
```
GAME_ID,CORE_ID,MODE_ID,VIEWPORT_WIDTH,VIEWPORT_HEIGHT
```

Example:
```
dino,fbneo,arcade:224@59.637405,0,0
```

The GAME_ID is the name of the game files, without the `.zip`.
The CORE_ID is the name of the core used by Recalbox.

This line lets Recalbox know that the game `dino.zip`, launched on retroarch with the core `fbneo`, will use the `arcade:224@59.637405` mode.


### Example of overloading

Overloading allows you to add or replace arcade modes, systems or games.

Each of the overload modes, systems or games is added to the base configuration, or replaces it if the id already exists.

Let's say I want to change the display mode of `Cadillac and Dinosaurs` on arcade, and change it to 224 lines at 60HZ.

In the file modes.txt, there is a mode corresponding to 224 lines at 60HZ. It has MODE_ID `standard:all:240@60`.