---
title: 🎬 Kodi
description: How to have the best Kodi!
published: true
date: 2023-02-24T15:20:28.835Z
tags: kodi, tutorials
editor: markdown
dateCreated: 2022-12-20T22:09:33.570Z
---

In this section, you can find some tips and tricks for everything related to Kodi.

Here are the available categories:

[Add Amazon Prime add-on](add-amazon-prime-add-on)
[Add Disney+ add-on](add-disney-plus-add-on)
[Play 3D videos](play-3d-videos)
[Twitch add-on configuration](twitch-add-on-configuration)
[YouTube add-on configuration](youtube-add-on-configuration)