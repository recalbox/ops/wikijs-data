---
title: FAQ
description: 
published: true
date: 2021-11-09T20:40:46.754Z
tags: faq, dragon32, dragon64, 8.0+
editor: markdown
dateCreated: 2021-09-04T23:33:47.059Z
---

## Some useful information...

- XRoar core supports:
  - Smooth (in EmulationStation, `START` > `GAME SETTIGNS` > `SMOOTH`).
  - Configuration overrides but they do not override each other. The first one found in order is the one that is taken into account: `rom.ext.config` > `<folder>.config` > `<parent>.config` > ... The syntax of the configuration file does not allow to manage overrides without making a gas factory. To generate a config file, you can use this SSH command: `xroar --config-all > .config`
- By default, the Dragon machine is a Dragon64
- The machine to use is automatically selected based on the subdirectories:

```
"/dragon/": "dragon64",
"/dragon32/": "dragon32",
"/dragon64/": "dragon64",
"/tano/": "tano",
"/dragon200/": "dragon200e",
"/dragon200e/": "dragon200e",
```

- Joystick 0 and 1 are automatically taken into account regardless of the order chosen in EmulationStation.
- If the machine "prints", the printed text will go into the file `/recalbox/share/saves/dragon/printer/rom-extension.txt`.