---
title: Activar la Wifi
description: 
published: true
date: 2023-09-13T14:34:52.227Z
tags: wifi, activación
editor: markdown
dateCreated: 2023-09-13T14:34:52.227Z
---

## Requisitos previos

* Comprobad que vuestro dongle wifi está en la [lista de compatibilidades] (./../../../hardware-compatibility/compatible-devices/wifi-dongles) o que tienes una placa con wifi.
* Comprobad que vuestra clave wifi es WPA2 y que no contiene caracteres especiales.
* Comprobad que vuestra wifi está en modo descubrimiento.
* Si vuestro punto de acceso utiliza el canal 12 o 13, prueba a cambiar el [wifi-codigo-pais](./wifi-codigo-pais).

## Configuración

La configuración es muy sencilla a través de EmulationStation.

* Pulsad `Inicio` > `Opciones de Red` y configurad la opción `WIFI` a **On**. 
* Tenéis 2 formas de configurar la wifi:
  * Podéis introducir directamente el SSID y la contraseña de la conexión Wi-Fi.
  * También podéis utilizar la conexión WPS. Para ello:
    * En el módem/router, activad la función WPS. Ésta permanecerá activa durante 2 minutos.
    * Seleccionad el botón `WPS CONNECTION` en EmulationStation.
