---
title: Vidéo Snaps
description: 
published: true
date: 2023-08-25T14:16:07.999Z
tags: snap, vidéos
editor: markdown
dateCreated: 2021-05-21T07:54:14.055Z
---

Les « Vidéos Snap », son ni plus ni moins l’affichage d’une petite vidéo de gameplay en lieu et place de votre « scrap » habituel dans votre liste de jeux.

Vous pouvez en voir un extrait vidéo [ici](https://www.youtube.com/watch?v=r5oabVrfQpk).

>Pour ceux qui n’ont jamais utilisé cette fonction, il faut savoir que les vidéos prennent une place non négligeable malgré leur excellente compression MP4 _(entre 1Mo à 6Mo)_.
>Pour profiter pleinement de cette fonction, il est vivement recommandé de prévoir un stockage étendu à 1 ou 2To!
{.is-warning}

## Configuration

Afin de profiter de ces vidéos, il vous faudra au préalable (re)scraper tous les jeux à l’aide de Skraper ou du scrapeur interne de Recalbox.

### Avec le scrapeur interne

Reportez-vous à [cette page](./../../basic-usage/features/internal-scraper) de cette documentation.

### Via le logiciel Skraper

Vous pouvez consulter [cette vidéo](https://www.youtube.com/watch?v=G388Gc6kkRs) pour apprendre à vous servir de Skraper.