---
title: Mupen64Plus GLideN64
description: 
published: true
date: 2024-07-24T14:31:00.253Z
tags: n64, mupen64plus, nintendo 64, gliden64
editor: markdown
dateCreated: 2021-09-30T10:42:44.912Z
---



## ![](/emulators/license.svg) License

This core is under [**GPLv2**](https://github.com/gonetz/GLideN64/blob/master/LICENSE) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ❌ | ✅ | ❌ | ❌ | ❌ | ❌ | ❌ | ❌ |

## ![](/emulators/features.png) Features



## ![](/emulators/bios.svg) BIOS

>**No bios is required.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Supported extensions

Roms must have the extension:

* .n64
* .v64
* .z64

### Location

Put the roms like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 n64
┃ ┃ ┃ ┃ ┣ 🗒 **game.n64**

>The roms using **No-Intro** format are highly recommended.
{.is-success}

>For more information about roms, go to [this tutorial](./../../../tutorials/games/generalities/isos-and-roms)!
{.is-info}

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/gonetz/GLideN64/](https://github.com/gonetz/GLideN64/)
* **Documentation**: [https://gliden64.blogspot.com/](https://gliden64.blogspot.com/)