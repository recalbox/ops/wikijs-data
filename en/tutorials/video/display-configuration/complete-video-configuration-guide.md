---
title: Complete video configuration guide
description: 
published: true
date: 2021-08-06T17:26:55.849Z
tags: configuration, video, guide
editor: markdown
dateCreated: 2021-08-06T17:26:55.849Z
---

In this tutorial you will find the best recommendations for configuring the video to fit your screen.

There are three levels of configuration: system, emulator default video mode, emulator specific video mode.

Keep in mind that the screen resolution can have a significant impact on the frame rate, especially with CPU-intensive emulators (N64, PSX and others).

Most of this guide will focus on the HDMI output and will expose as often as possible the 3 cases: Pure HDMI, HDMI to DVI, HDMI to VGA.

## What are the possible video modes?

First of all, you have to understand the basics of screen resolution: a number of pixels in width by a number of pixels in height at (@) a given refresh rate in Hertz.

For example: 1280x1024@60 which is a common screen resolution of 4x3 for 19" monitors.

Nowadays, TVs all display Full HD (FHD) at 1080p for example, like 1920x1080@60. We won't go through interlaced or progressive modes, that's beside the point. Please refer to your display manufacturer's manual for its native resolution.

There are two groups of video modes: CEA and DMT. All these modes are listed [here](http://elinux.org/RPiconfig) or [here](https://www.raspberrypi.org/documentation/configuration/config-txt/README.md).  
Make sure the mode is compatible with your native screen resolution.  
A simple rule of thumb: CEA is for TV mode, DMT is for all other modes.

### The most common modes

* DMT 4 : 640x480@60
* DMT 9 : 800x600@60
* DMT 16 : 1024x768@60
* DMT 35 : 1280x1024@60
* DMT 57 : 1680x1050@60
* 720p : CEA 4
* 1080p : CEA 16

In this tutorial, we will cover 3 cases:

* A normal 1080p television. Let's call it "1080pTV".
* A 720p VGA monitor connected with HDMI to VGA. Let's call it "720pVGA".
* A good old 17" DVI capable of displaying 1280x1024@60. Let's name it ... "DVIboy".

Don't forget to check the tutorial for DVI screens [here](./../lcd/dvi-screen).

With what you already know about screen modes, these are the most common settings:

* "1080pTV" corresponds to CEA 16
* "720pVGA" corresponds to CEA 4
* "DVIboy" corresponds to DMT 35

## Your system boots up

At startup, the Pi will ask your monitor for its preferred screen resolution.  
Your monitor sends what is called its _EDID (Extended Display Identification Data)_ and the Raspberry Pi selects the screen resolution noted as "preferred".

This is where the very first problem occurs:

* Most - if not all - native HDMI displays (TVs, monitors) should send the correct EDID.
* HDMI to DVI should work almost as well.
* HDMI to VGA adapters can be a bit tricky and provide an inaccurate EDID ...

Now that you know a little more about what can happen, here are some examples:

* The "1080pTV" is a good boy and will get its native 1080p right away.
* The "720pVGA" has a crappy HDMI to VGA that matches the "DMT 16" as its preferred mode.
* The "DVIboy" sets its DMT 35 flawlessly.

Since the screen detected at startup is the one that displays EmulationStation with its preferred resolution, the "720pVGA" will have an ugly display.

Fortunately, this can be fixed by editing the `recalbox-user-config.txt` file.

Here are the lines you need to change:

```ini
#hdmi_group=1
#hdmi_mode=1
```

* **hdmi_group** : 1 for CEA, 2 for DMT.
* **hdmi_mode** : refer to the lines seen above.

So for our "720pVGA", here is how we modify the `recalbox-user-config.txt` file:

```ini
hdmi_group=1
hdmi_mode=4
```

>The "#" sign is removed, the technical term for this is "uncommented". A line starting with a # is a comment, not a command:
>`hdmi_group=1` means CEA
>`hdmi_mode=4` means 720p
{.is-info}

Now that we are done, our 3 screens are set, let's keep going!

## Global video mode for emulators

Now that EmulationStation is running in full screen, your hand is eager to test a game. The main thing you need to know here is that the screen resolution is changed before launching an emulator.

But to what resolution?

* Open your [recalbox.conf](./../../../basic-usage/getting-started/recalboxconf-file) file
* Read the `global.videomode` parameter. Its default value is `CEA 4 HDMI`. This means that the screen resolution will be changed to 720p just before starting the emulator.

It's time to check some examples:

* "1080pTV" can display 720p without problems.
* "720pVGA" can display 720p without a scratch.
* "DVIboy" is ... mmh ... well, it can't display 720p.
  * So just modify the [recalbox.conf](./../../../basic-usage/getting-started/recalboxconf-file) file
  * Set `global.videomode` to its native video mode:

```ini
global.videomode=DMT 35 HDMI
```

>Feeling lucky, want to save yourself a change of screen resolution? By specifying `global.videomode=default`, Recalbox will not change the screen resolution before launching an emulator. But beware: this can have a considerable impact on performance.
>Scaling an image from the emulator's native resolution to 1080p can be very CPU intensive and slow down the emulation. It is not recommended to use the default setting if your monitor can display a resolution higher than CEA 4.
{.is-info}

## Video mode by emulator

Finally, the video mode can be set specifically for the emulator of your choice.

For example, if you look in the [recalbox.conf](./../../../basic-usage/getting-started/recalboxconf-file) file, you will notice that the `n64.videomode` parameter is set to `DMT 4 HDMI`, which means we are overriding the `global.videomode` parameter for the N64 emulation.

I hope this guide has helped you understand how screen resolution is handled on Raspberry Pi and Recalbox. Feel free to ask questions on the forum or on Discord.

## Useful tips

### tvservice

>This tool is available on all Raspberry Pi except the Raspberry Pi4.
{.is-info}

The **tvservice** is an excellent tool to diagnose your output:

```shell
# tvservice --help
Usage: tvservice [OPTION]...
  -p, --preferred                   Power on HDMI with preferred settings
  -e, --explicit="GROUP MODE DRIVE" Power on HDMI with explicit GROUP (CEA, DMT, CEA_3D_SBS, CEA_3D_TB, CEA_3D_FP, CEA_3D_FS)
                                      MODE (see --modes) and DRIVE (HDMI, DVI)
  -t, --ntsc                        Use NTSC frequency for HDMI mode (e.g. 59.94Hz rather than 60Hz)
  -c, --sdtvon="MODE ASPECT [P]"    Power on SDTV with MODE (PAL or NTSC) and ASPECT (4:3 14:9 or 16:9) Add P for progressive
  -o, --off                         Power off the display
  -m, --modes=GROUP                 Get supported modes for GROUP (CEA, DMT)
  -M, --monitor                     Monitor HDMI events
  -s, --status                      Get HDMI status
  -a, --audio                       Get supported audio information
  -d, --dumpedid <filename>         Dump EDID information to file
  -j, --json                        Use JSON format for --modes output
  -n, --name                        Print the device ID from EDID
  -l, --list                        List all attached devices
  -v, --device                      Specify the device to use (see --list)
  -h, --help                        Print this information
```

The most commonly used switches:

* `tvservice -s` gets the current display information.
* `tvservice -m CEA` or `tvservice -m DMT`: list of supported CEA or DMT modes.