---
title: Set until 4 features on each GPIO port
description: 
published: true
date: 2021-08-07T14:48:56.697Z
tags: gpio, feature
editor: markdown
dateCreated: 2021-08-07T14:48:30.645Z
---

If you want to build your arcade with a Raspberry Pi, you can add extra buttons and assign them to special functions. Here's how it works.

## About the Raspberry Pi GPIO ports

On a Raspberry Pi, you have several GPIO ports, from GPIO2 to GPIO26:

![GPIO](/tutorials/controllers/gpio/gpio-functions/pi-gpio.png)

When you want to connect a button, you must connect it between a GPIOxx port and a GND port. Please note that GPIO numbers are used, not PIN numbers (1 to 40)!

## How to install it on Recalbox

1. Add this to the file `/recalbox/share/system/recalbox.conf`:

```ini
# Uncomment to enable custom GPIO script feature
system.rpi.gpio=enable
```

2. Edit the `/recalbox/share/system/configs/retroarch/retroarchcustom.cfg` file and add this line:

```ini
network_cmd_enable = true
```
3. Configure the file `/recalbox/share/system/configs/rpi-gpio.ini` (see the available commands below).
4. Restart Recalbox and enjoy!

## Syntax of the rpi-gpio.ini file

You have to start with [GPIOxx] and replace `xx` by the GPIO number in question.  
Then add these 4 lines :

```ini
quick=
standard=
hold=
release=
```

`quick` is an action that will be performed if you press and release the button quickly.
`standard` is an action that will be performed if you hold and release the button for about 1 second.
`hold` is an action that will be performed if you hold the button for a long time (more than 2 seconds).
`release` is an action that will be executed if you release the button after a `hold` event.

All actions are optional. If you don't want to assign a function, just leave a blank after `=`.

Example:

```ini
[GPIO26]
quick=VOLUP
standard=VOLDOWN
hold=VOLMUTE
release=
[GPIO16]
quick=RESET
standard=QUIT
hold=
release=
```

## Actions available

### Global actions

* VOLUP : Increase the volume
* VOLDOWN: Decrease volume
* VOLMUTE: Mute / unmute volume
* QUIT: Quit the current emulator and return to EmulationStation

### Actions specific to RetroArch emulators

* CHEAT_INDEX_MINUS
* CHEAT_INDEX_PLUS
* CHEAT_TOGGLE
* DISK_EJECT_TOGGLE
* DISK_NEXT
* DISK_PREV
* FAST_FORWARD
* FAST_FORWARD_HOLD
* FRAMEADVANCE
* FULLSCREEN_TOGGLE
* GRAB_MOUSE_TOGGLE
* LOAD_STATE
* MENU_TOGGLE
* MOVIE_RECORD_TOGGLE
* NETPLAY_FLIP
* OVERLAY_NEXT
* PAUSE_TOGGLE
* RESET
* REWIND
* SAVE_STATE
* SCREENSHOT
* SHADER_NEXT
* SHADER_PREV
* SLOWMOTION
* STATE_SLOT_MINUS
* STATE_SLOT_PLUS