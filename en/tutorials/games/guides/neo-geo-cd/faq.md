---
title: FAQ
description: 
published: true
date: 2021-09-08T12:47:14.690Z
tags: faq, neo-geo cd
editor: markdown
dateCreated: 2021-08-08T09:10:16.926Z
---

## Notes on the format of the games

### "Normal" and "cdz pached" versions

FBNeo takes both formats, but other Neogeo cd emulators only take the cdz patched versions. It may be better to keep only those.

### Redump format

Redump files are in `.CUE` format with several `.BIN` files called **multibin**. This core does not support multibin and you must convert your games to have a single bin file.

## Conversion

You can do this with the **CDmage** 1.02.1 software:

**File** &gt; **Open** &gt; Select your game in .CUE format
* **File** &gt; **Save As** &gt; Enter a new name for your game so as not to overwrite the existing one and click `Save
* Make sure you have chosen "MODE1/2352" in the drop-down list on the right of the new window and click `OK`.

Sources:

* [https://neo-source.com/index.php?topic=2487.msg26465#msg26465](https://neo-source.com/index.php?topic=2487.msg26465#msg26465)
* [https://github.com/libretro/FBNeo/blob/master/src/burner/libretro/README.md#neogeo-cd-doesnt-work-why-](https://github.com/libretro/FBNeo/blob/master/src/burner/libretro/README.md#neogeo-cd-doesnt-work-why-)