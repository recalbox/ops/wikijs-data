---
title: Libretro Mu
description: 
published: true
date: 2024-06-23T12:01:16.864Z
tags: libretro, palm, palm-os, mu
editor: markdown
dateCreated: 2021-05-21T08:31:51.482Z
---

**Libretro Mu** est un émulateur de Palm Os développé en C et C++.

La continuation du Core est dédiée à Emily (1998-2020).

## ![](/emulators/license.svg) Licence

Ce core est sous licence [**Creative Commons Attribution-NonCommercial 3.0 United States**](https://github.com/libretro/Mu#license).

## ![](/emulators/compatibility.png) Compatibilité

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Fonctionnalités



## ![](/emulators/bios.svg) BIOS

### Liste des bios obligatoires

| Nom de fichier | Description | MD5 | Fourni |
| :---: | :---: | :---: | :---: |
| palmos41-en-m515.rom | BIOS Palm OS 4.1 | 83cb1d1c76e568b916dc2e7c0bf669f6 | ❌ |

### Liste des bios optionnels

| Nom de fichier | Description | MD5 | Fourni |
| :---: | :---: | :---: | :---: |
| bootloader-dbvz.rom | Palm Bootloader | 9da101cd2317830649a31f8fa46debec | ❌ |

### Emplacement

Placez les bios comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 🗒 **palmos41-en-m515.rom**
┃ ┃ ┃ ┣ 🗒 **bootloader-dbvz.rom**

## ![](/emulators/roms.png) Roms

### Extensions supportées

La rom doit avoir l'extension :

* .prc
* .pdb
* .pqa
* .img

### Emplacement

Placez les roms comme ceci : 

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 palm
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.prc**

>Les roms au format **No-Intro** sont vivement conseillées.
{.is-success}

>Pour plus d'information sur les roms, rendez-vous sur [ce tutoriel](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuration avancée de l'émulateur

>Pour pouvoir conserver vos configurations personnalisées lors d'une mise à jour, nous vous conseillons d'utiliser notre fonctionnalité [Surcharges de configuration](./../../../advanced-usage/configuration-override).
{.is-info}

### Accéder aux options

Vous pouvez configurer diverses options de deux façons différentes.

* Via le menu RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via le fichier `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Options du core

| Options | Valeur des options | Variable (pour surcharge) | Valeurs des variables |
| --- | --- | --- | --- |
| CPU Speed | `1.0` ✅ / `1.5` / `2.0` / `2.5` / `3.0` / `0.5` | `palm_emu_cpu_speed` | `1.0` / `1.5` / `2.0` / `2.5` / `3.0` / `0.5` |
| Force Match System Clock | `Désactivé` ✅ / `Activé` | `palm_emu_feature_synced_rtc` | `disabled` / `enabled` |
| Ignore Invalid Behavior | `Désactivé` ✅ / `Activé` | `palm_emu_feature_durable` | `disabled` / `enabled` |
| Use Left Joystick As Mouse | `Désactivé` ✅ / `Activé` | `palm_emu_use_joystick_as_mouse` | `disabled` / `enabled` |
| Disable Graffiti Area | `Désactivé` ✅ / `Activé` | `palm_emu_disable_graffiti` | `disabled` / `enabled` |
| OS Version | `Palm m515/Palm OS 4.1` ✅ / `Tungsten T3/Palm OS 5.2.1` / `Tungsten T3/Palm OS 6.0` / `Palm m500/Palm OS 4.0` | `palm_emu_os_version` | `Palm m515/Palm OS 4.1` / `Tungsten T3/Palm OS 5.2.1` / `Tungsten T3/Palm OS 6.0` / `Palm m500/Palm OS 4.0` |

## ![](/emulators/external-links.png) Liens externes

* **Code source utilisé** : [https://github.com/libretro/Mu/](https://github.com/libretro/Mu/)
* **Code source officiel** : [https://github.com/meepingsnesroms/Mu/](https://github.com/meepingsnesroms/Mu/)
* **Site officiel** : [https://meepingsnesroms.github.io/](https://meepingsnesroms.github.io/)