---
title: Atari 2600
description: 
published: true
date: 2024-07-26T07:44:11.378Z
tags: atari-2600, atari, 2600, consolas
editor: markdown
dateCreated: 2023-09-03T15:41:13.623Z
---

![](/emulators/consoles/atari2600.svg){.align-center}

## Datos técnicos

* **Fabricante:** Atari Corporation
* **Año de lanzamiento :** 1977
* **Cantidad vendida :** 30 millones
* **Juego más vendido :** Pac-Man
* **Procesador:** MOS 6507 de 8 bits a 1,19 MHz
* **RAM**:128 bytes
* **Procesador de audio/vídeo:** TIA
* **Resolución:** 160 x 192 píxeles, 4 colores por línea (paleta de 128 colores)
* **Chip de sonido:** 2 canales de 1 bit mono con control de volumen de 4 bits
* **Tamaño del cartucho:** 2kB - 32kB

## Presentación

En 1976, Fairchild Camera and Instrument lanzó el sistema [**Channel F**](./channelf), la primera consola doméstica compatible con cartuchos. Un año más tarde llegó la Studio II (creada por RCA), la segunda consola doméstica de la historia... Y finalmente, en octubre de 1977, Atari lanzó el **Atari VCS** (Video Computer System) con 9 juegos disponibles.

En 1982, Atari aceptó que una docena de desarrolladores lanzaran juegos para la consola. Desde el punto de vista financiero, Atari obtuvo beneficios, pero desde el punto de vista lúdico fue un desastre... Aunque todavía había algunos títulos de calidad, se lanzó mucha basura para la **Atari VCS**...

En 1983, cada vez más empresas producían juegos y accesorios; y pasó lo que tenía que pasar: había demasiados productos para la demanda y muchos desarrolladores quebraron. Fue un **crash para el mundo de los videojuegos**.

En 1984, el precio de la **Atari 2600** se desplomó a menos de un tercio de su precio original. Los años 1984 y 1985 fueron realmente pobres en cuanto a videojuegos y sólo se lanzaron unos pocos juegos, entre ellos Cosmic Commuter y Ghostbusters. A pesar de ello, la consola siguió vendiéndose bien porque se había vuelto muy barata y contaba con una enorme biblioteca de juegos.

En 1986, Nintendo lanzó su [**NES**](./nintendo-entertainement-system), que fue un gran éxito. Atari decidió entonces lanzar el **Atari 2600** **Jr**, un Atari 2600 rediseñado.

En 1987 y 1988, se lanzaron algunos juegos excelentes, como Pac-Man Jr. La **Atari VCS** pasó a llamarse **Atari 2600** algún tiempo después y dominó el mercado de las consolas durante varios años, hasta principios de los 80.

## Emuladores

[Libretro Stella](libretro-stella)
[Libretro Stella2014](libretro-stella2014)

