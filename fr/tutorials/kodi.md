---
title: 🎬 Kodi
description: Comment avoir le meilleur Kodi possible !
published: true
date: 2023-02-24T15:20:09.252Z
tags: kodi, tutoriels
editor: markdown
dateCreated: 2022-12-20T21:04:41.006Z
---

Dans cette rubrique, vous pouvez trouver quelques astuces et autres pour tout ce qui touche à Kodi.

Voici les catégories disponibles :

[Ajouter l'extension Amazon Prime](add-amazon-prime-add-on)
[Ajouter l'extension Disney+](add-disney-plus-add-on)
[Configuration de l'extension Twitch](twitch-add-on-configuration)
[Configuration de l'extension YouTube](youtube-add-on-configuration)
[Lire les vidéos 3D](play-3d-videos)