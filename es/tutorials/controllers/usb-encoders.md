---
title: Codificadores USB
description: 
published: true
date: 2023-09-26T07:02:58.373Z
tags: usb, codificador, codificadores
editor: markdown
dateCreated: 2023-09-18T11:45:26.582Z
---

En esta sección encontráis ayuda para configurar vuestros codificadores USB.

Aquí tenéis la lista de tutoriales disponibles:

[Configuración de controladores Xin-mo](xin-mo-controllers-configuration)
[Recalbox para vuestro codificador de teclado USB](recalbox-for-your-usb-keyboard-encoder)