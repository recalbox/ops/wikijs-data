---
title: Amiberry
description: 
published: true
date: 2024-06-30T15:31:46.781Z
tags: amiga-cdtv, amiga, amiberry, cdtv, 8.0+
editor: markdown
dateCreated: 2023-09-06T08:09:19.709Z
---

**Amiberry** es un core ARM optimizado para Amiga.

## ![](/emulators/license.svg) Licencia

El core posee la siguiente licencia [**GPLv3**](https://github.com/midwan/amiberry/blob/master/COPYING).

## ![](/emulators/compatibility.png) Compatibilidad

 | RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
 | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
 | ❌ | ✅ | ✅ | ✅ | ✅ | ✅ &nbsp;🐌 | ❌ | ✅ |

🐌  Bajo rendimiento pero jugable

## ![](/emulators/features.png) Funcionalidades



## ![](/emulators/bios.svg) BIOS

### Lista de bios obligatorias

Encontraréis las bios con el nombre indicado en la columna **Descripción**. Tendréis que renomarlas con el nombre indicado en la columna **Nombre del fichero**.

| Nombre del fichero | Descripción | MD5 | ¿Preinstalada? |
| :---: | :---: | :---: | :---: |
| kick34005.CDTV | CDTV Extended-ROM v1.0 (1991)(Commodore)(CDTV)\[!\] | 89da1838a24460e4b93f4f0c5d92d48d d98112f18792ee3714df16a6eb421b89 d1145ab3a0f89340f94c9e734762c198 | ❌ |

### Emplazamiento

Disponed vuestras bios como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 🗒 **kick34005.CDTV**

## ![](/emulators/isos.svg) Isos

### Extensiones soportadas

Las isos deben tener las siguientes extensiones:

* .bin/.cue
* .iso
* .img/.ccd

### Emplazamiento

Disponed vuestras isos como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 amigacdtv
┃ ┃ ┃ ┃ ┣ 🗒  **jeu.cue**
┃ ┃ ┃ ┃ ┣ 🗒  **jeu.bin**

>Se aconseja las isos del projecto **Redump** 
{.is-success}

>Para más información sobre las isos, consultad [este tutorial](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

>Para conservar las configuraciones personalizadas cuando se produce una actualización del sistema Recalbox, os aconsejamos utilizar las [Sobrecargas de configuración](./../../../advanced-usage/configuration-override).
{.is-info}

## ![](/emulators/external-links.png) Enlaces externos

* **Código fuente utilizado** : [https://github.com/midwan/amiberry/](https://github.com/midwan/amiberry/)
* **Documentación** : [https://blitterstudio.com/amiberry/](https://blitterstudio.com/amiberry/)