---
title: Libretro YabaSanshiro
description: 
published: true
date: 2024-07-24T18:47:02.426Z
tags: libretro, saturn, yabasanshiro
editor: markdown
dateCreated: 2021-08-03T10:30:30.400Z
---

**Libretro YabaSanshiro** is a port of the **Yabause** emulator to Libretro supporting the Sega Saturn.

## ![](/emulators/license.svg) License

This core is under [**GPLv2**](https://github.com/libretro/yabause/blob/master/yabause/COPYING) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ❌ | ❌ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Features



## ![](/emulators/bios.svg) BIOS

### List of mandatory bios

| Filename | Description | MD5 | Provided |
| :---: | :---: | :---: | :---: |
| saturn_bios.bin | Saturn BIOS | af5828fdff51384f99b3c4926be27762 | ❌ |

### Location

Put the bios like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 saturn
┃ ┃ ┃ ┃ ┣ 🗒 **saturn_bios.bin**

## ![](/emulators/isos.svg) Isos

### Supported extensions

Isos must have the extension:

* .bin/.cue
* .m3u
* .img/.ccd
* .iso
* .mds/.mdf
* .m3u
* .chd

### Location

Put the isos like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 saturn
┃ ┃ ┃ ┃ ┣ 🗒 **game.bin**
┃ ┃ ┃ ┃ ┣ 🗒 **game.cue**

>The isos using **Redump** format are highly recommended.
{.is-success}

>For more information about isos, go to [this tutorial](./../../../tutorials/games/generalities/isos-and-roms)!
{.is-info}

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Accessing to options

You can configure various options in two different ways.

* Via RetroArch menu:

┣ 📁 RetroArch Menu
┃ ┣ 📁 Core options
┃ ┃ ┣ 🧩 Name_option

* Via the file `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core options

| Options | Options values | Variables (for configuration override) | Variables values |
| --- | --- | --- | --- |
| Force HLE BIOS (restart) | `Disabled` ✅ / `Enabled` | `yabasanshiro_force_hle_bios` | `disabled` / `enabled` |
| Auto-frameskip | `Enabled` ✅ / `Disabled` | `yabasanshiro_frameskip` | `enabled` / `disabled` |
| Addon Cartridge (restart) | `1M_extended_ram` / `4M_extended_ram` ✅ | `yabasanshiro_addon_cart` | `1M_extended_ram` / `4M_extended_ram` |
| System Language (restart) | `english` ✅ / `deutsch` / `french` / `spanish` / `italian` / `japanese` | `yabasanshiro_system_language` | `english` / `deutsch` / `french` / `spanish` / `italian` / `japanese` |
| 6Player Adaptor on Port 1 | `Disabled` ✅ / `Enabled` | `yabasanshiro_multitap_port1` | `disabled` / `enabled` |
| 6Player Adaptor on Port 2 | `Disabled` ✅ / `Enabled` | `yabasanshiro_multitap_port2` | `disabled` / `enabled` |
| SH2 Core (restart) | `dynarec` ✅ / `interpreter` | `yabasanshiro_sh2coretype` | `dynarec` / `interpreter` |
| Polygon Mode (restart) | `perspective_correction` ✅ / `gpu_tesselation` / `cpu_tesselation` | `yabasanshiro_polygon_mode` | `perspective_correction` / `gpu_tesselation` / `cpu_tesselation` |
| Resolution Mode (restart) | `original` ✅ / `2x` / `4x` / `720p` / `1080p` / `4k` | `yabasanshiro_resolution_mode` | `original` / `2x` / `4x` / `720p` / `1080p` / `4k` |
| RGB resolution mode | `original` ✅ / `2x` / `720p` / `1080p` / `Fit_to_emulation` | `yabasanshiro_rbg_resolution_mode` | `original` / `2x` / `720p` / `1080p` / `Fit_to_emulation` |
| RGB use compute shader for RGB | `Disabled` ✅ / `Enabled` | `yabasanshiro_rbg_use_compute_shader` | `disabled` / `enabled` |

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/libretro/yabause/](https://github.com/libretro/yabause/)
* **Libretro documentation**: [https://docs.libretro.com/library/yabasanshiro/](https://docs.libretro.com/library/yabasanshiro/)