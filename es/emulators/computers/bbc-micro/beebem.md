---
title: BeebEm
description: 
published: true
date: 2024-07-29T18:47:13.684Z
tags: bbc micro, beebem, 8.0+
editor: markdown
dateCreated: 2023-09-11T08:35:04.751Z
---

## ![](/emulators/license.svg) Licencia

Este núcleo está bajo la licencia **GPL**.

## ![](/emulators/compatibility.png) Compatibilidad

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Funcionalidades

| Funcionalidad | Soportado |
| :---: | :---: |
| Reinicio | ✔ |
| Capturas de pantalla | ✔ |

## ![](/emulators/bios.svg) BIOS

>**No se requiere ningún BIOS.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Extensiones soportadas

La rom debe tener la extensión:

* .ssd
* .dsd
* .adl
* .adf
* .uef
* .csw

### Ubicación

Coloca las roms de la siguiente manera:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 bbcmicro
┃ ┃ ┃ ┃ ┣ 🗒 **juego.ssd**

>Para más información sobre las roms, visita [este tutorial](./../../../tutorials/games/generalities/isos-and-roms)!
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

>Para poder conservar tus configuraciones personalizadas durante una actualización, te recomendamos utilizar nuestra funcionalidad [Sobrecargas de configuración](./../../../advanced-usage/configuration-override).
{.is-info}

## ![](/emulators/external-links.png) Enlaces externos

* **Código fuente utilizado** : [https://gitlab.com/bkg2k/beebem-linux](https://gitlab.com/bkg2k/beebem-linux)
* **Sitio web oficial** : [http://www.mkw.me.uk/beebem/](http://www.mkw.me.uk/beebem/)
