---
title: Libretro MAME 2003 Plus
description: 
published: true
date: 2024-07-05T10:49:54.251Z
tags: libretro, neo-geo, mame2003+, mame2003plus
editor: markdown
dateCreated: 2023-09-06T14:05:03.389Z
---




**Libretro MAME2003_Plus** (también conocido como MAME 2003+ y mame2003-plus) es un core emulador de sistemas arcade **Libretro** que se centra en el alto rendimiento y la amplia compatibilidad con dispositivos móviles, ordenadores monoplaca, sistemas embebidos y plataformas similares.

MAME 2003-Plus ha sido creado a partir del código de MAME 2003, derivado a su vez de MAME 0.78. Estas versiones iniciales de MAME necesitan pocos recursos hardware, lo que permite que MAME 2003-Plus se ejecute correctamente en sistemas de bajo rendimiento.

Sobre esta base, los contribuidores de MAME 2003-Plus han implementado el soporte de varios cientos de juegos adicionales, así como otras características que no están presentes en MAME 0.78.


## ![](/emulators/license.svg) Licencia

El core posee la siguiente licencia **MAME no-commercial**.

## ![](/emulators/compatibility.png) Compatibilidad

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | 
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Funcionalidades


| Funcionalidad | ¿Soportada? |
| :---: | :---: |
| Grabaciones | ✔ |
| Grabaciones instantáneas | Depende del juego |
| Rebobinado | ✔ |
| Netplay | ✔ |
| Cheats nativos | ✔ |
| Controladores | ✔ |
| Multi-Ratón | ✔ |


## ![](/emulators/bios.svg) BIOS

### Lista de BIOS obligatorias

| Nombre del fichero | Descripción | MD5 | ¿Preinstalada? |
| :---: | :---: | :---: | :---: |
| neogeo.zip | BIOS Neo-Geo | e023d0a4b5249fdff2a4620c28963944 8cbdff53661f41ddbe486a98915e1ec9 42e359e394f0be1d61b94ddca0efbe6c c1e0a738d266926f604186aa9d63e4db d12bba59121229ff13702878b415cb7c 2b4ffdf24d5d4f24c4b116c50c4bafc3 005b843b6e70b939c2cca41887cbc371 42e359e394f0be1d61b94ddca0efbe6c 4b9432a4571d03cc8bae9d24d8d6eb40 4cd64313e26537c04cdb561b8f84b0e4 50bbfd6bf297038b085e29265c65723a 76ee7390ccd537cddea1018025ca29a8 84d6c35aad790a77ba3cfc1b319e1061 912c0a56bafe0fba39d0c668b139b250 94748719c0ef31e7375b0c357d82fc89 ab25b9e25052a6064901a7a221037eb6 ae9e5825cde82c58e39dfd48534f7060 b18f83feef474b8a1306ef199cd810a2 b4519df27a352c2a6e42e06d31330d91 bf00272e7150b31156a8995d60bf185d ca66fd463ef7dcab492a6de8ce5f45eb f4a125538dfd7a8044e025fc5188fb88 a3b2c9a69c28ad0bd4ea07877b744bbe | ❌  |

### Emplazamiento

Disponed vuestras bios como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 neogeo
┃ ┃ ┃ ┃ ┣ 🗒 **neogeo.zip**

## ![](/emulators/roms.png) Roms

### Extensiones de roms soportadas

Las roms deben tener las extensiones siguientes:

* .zip
* .7z

### Emplazamiento

Disponed vuestras roms como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 neogeo
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.zip**

>Para más información sobre las roms, consultad [este tutorial](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

>Para conservar las configuraciones personalizadas cuando se produce una actualización del sistema Recalbox, os aconsejamos utilizar las [Sobrecargas de configuración](./../../../advanced-usage/configuration-override).
{.is-info}


### Acceder a las opciones

Podéis configurar las opciones del core de dos formas diferentes.

* Via el Menú RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via el fichero `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Opciones del core

| Opción | Valor de la opción | Variable (para la sobrecarga) | Valores posibles |
| --- | --- | --- | --- |
| Skip Disclaimer | `Désactivé` ✅ / `Activé` | `mame2003-plus_skip_disclaimer` | `disabled` / `enabled` |
| Skip Warnings | `Désactivé` ✅ / `Activé` | `mame2003-plus_skip_warnings` | `disabled` / `enabled` |
| Display MAME Menu | `Désactivé` ✅ / `Activé` | `mame2003-plus_display_setup` | `disabled` / `enabled` |
| Legacy Remapping | `Activé` ✅ / `Désactivé` | `mame2003-plus_mame_remapping` | `enabled` / `disabled` |
| Autosave Hiscore | `default` ✅ / `recursively` / `Désactivé` | `mame2003-plus_autosave_hiscore` | `default` / `recursively` / `disabled` |
| Locate System Files Within a Subfolder | `Activé` ✅ / `Désactivé` | `mame2003-plus_core_sys_subfolder` | `enabled` / `disabled` |
| Locate Save Files Within a Subfolder | `Activé` ✅ / `Désactivé` | `mame2003-plus_core_save_subfolder` | `enabled` / `disabled` |
| X-Y Device | `mouse` ✅ / `pointer` / `lightgun` / `Désactivé` | `mame2003-plus_xy_device` | `mouse` / `pointer` / `lightgun` / `disabled` |
| Input Interface | `simultaneous` ✅ / `retropad` / `keyboard` | `mame2003-plus_input_interface` | `simultaneous` / `retropad` / `keyboard` |
| Show Lightgun Crosshairs | `Activé` ✅ / `Désactivé` | `mame2003-plus_crosshair_enabled` | `enabled` / `disabled` |
| Lightgun Crosshair Appearance | `simple` ✅ / `enhanced` | `mame2003-plus_crosshair_appearance` | `simple` / `enhanced` |
| Allow Input Button to Act as a Toggle Switch | `Activé` / `Désactivé` | `mame2003-plus_input_toggle` | `enabled` / 
`disabled` |
| Center Joystick Axis for Digital Controls | `Activé` ✅ / `Désactivé` | `mame2003-plus_digital_joy_centering` | `enabled` / `disabled` |
| Use Samples | `Activé` ✅ / `Désactivé` | `mame2003-plus_use_samples` | `enabled` / `disabled` |
| Sample Rate | `8000 KHz` / `11025 KHz` / `22050 KHz` / `30000 KHz` / `44100 KHz` / `48000 KHz` ✅ | `mame2003-plus_sample_rate` | `8000` / `11025` / `22050` / `30000` / `44100` / `48000` |
| Brightness | `0.2` / `0.3` / `0.4` / `0.5` / `0.6` / `0.7` / `0.8` / `0.9` / `1.0` ✅ / `1.1` / `1.2` / `1.3` / `1.4` / `1.5` / `1.6` / `1.7` / `1.8` / `1.9` / `2.0` | `mame2003-plus_brightness` | `0.2` / `0.3` / `0.4` / `0.5` / `0.6` / `0.7` / `0.8` / `0.9` / `1.0` / `1.1` / `1.2` / `1.3` / `1.4` / `1.5` / `1.6` / `1.7` / `1.8` / `1.9` / `2.0` |
| Gamma Correction | `0.5` / `0.6` / `0.7` / `0.8` / `0.9` / `1.0` ✅ / `1.1` / `1.2` / `1.3` / `1.4` / `1.5` / `1.6` / `1.7` / `1.8` / `1.9` / `2.0` | `mame2003-plus_gamma` | `0.5` / `0.6` / `0.7` / `0.8` / `0.9` / `1.0` / `1.1` / `1.2` / `1.3` / `1.4` / `1.5` / `1.6` / `1.7` / `1.8` / `1.9` / `2.0` |
| TATE Mode | `Désactivé` ✅ / `Activé` | `mame2003-plus_tate_mode` | `disabled` / `enabled` |
| Frameskip | `Désactivé` ✅ / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `9` / `10` / `11` / `auto` / `auto aggressive` / `auto max` | `mame2003-plus_frameskip` | `disabled` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `9` / `10` / `11` / `auto` / `auto_aggressive` / `auto_max` |
| Display Artwork | `Activé` ✅ / `Désactivé` | `mame2003-plus_display_artwork` | `enabled` / `disabled` |
| Resolution Multiplier | `1` ✅ / `2` / `3` / `4` / `5` / `6` / `7` / `8` | `mame2003-plus_art_resolution` | `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` |
| Hardcoded Overlay Opacity | `default` ✅ / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `21` / `22` / `23` / `24` / `25` / `50` / `70` | `mame2003-plus_art_overlay_opacity` | `default` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `21` / `22` / `23` / `24` / `25` / `50` / `70` |
| Specify Neo Get BIOS | `default` ✅ / `euro` / `euro-s1` / `us` / `us-e` / `asia` / `japan` / `japan-s2` / `unibios40` / `unibios33` / `unibios20` / `unibios13` / `unibios11` / `unibios10` / `debug` / `asia-aes` | `mame2003-plus_` | `default` / `euro` / `euro-s1` / `us` / `us-e` / `asia` / `japan` / `japan-s2` / `unibios40` / `unibios33` / `unibios20` / `unibios13` / `unibios11` / `unibios10` / `debug` / `asia-aes` |
| CPU Clock Scale | `default` ✅ / `25%` / `30%` / `35%` / `40%` / `45%` / `50%` / `55%` / `60%` / `65%` / `70%` / `75%` / `80%` / `85%` / `90%` / `95%` / `100%` / `105%` / `110%` / `115%` / `120%` / `125%` / `200%` / `250` / `300` | `mame2003-plus_cpu_clock_scale` | `default` / `25` / `30` / `35` / `40` / `45` / `50` / `55` / `60` / `65` / `70` / `75` / `80` / `85` / `90` / `95` / `100` / `105` / `110` / `115` / `120` / `125` / `200` / `250` / `300` |

## ![](/emulators/external-links.png) Enlaces externos

* **Código fuente utilizado** : [https://github.com/libretro/mame2003-plus-libretro/](https://github.com/libretro/mame2003-plus-libretro/)