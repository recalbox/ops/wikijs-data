---
title: Jouer aux jeux Windows 95 & 98 avec Dosbox Pure
description: Jouez à vos anciens jeux Windows !
published: true
date: 2025-03-14T15:39:08.248Z
tags: dosbox pure, 9.0+, windows95, windows 98, win95, win98
editor: markdown
dateCreated: 2023-02-26T14:21:28.955Z
---

## Introduction

Avec Recalbox 9.0, vous pouvez désormais jouer aux jeux Windows 95 et 98 ! Ce tutoriel va décrire comment procéder.

Vous devez avoir les pré-requis suivants :

- Windows 95/98 au format image CD
- 1 clavier
- 1 souris
- Le jeu que vous souhaitez utiliser dans un fichier compressé en .zip ou .7z

## Installation

Pour la démonstration suivante, nous allons l'effectuer avec Windows 95 OSR 2.5 (support natif USB à l'époque) et un CD de Wolfenstein compressé.

Cette installation se passe en 2 temps. La première est l'installation de Windows lui-même, et le second temps est le démarrage de votre jeu avec votre installation de Windows.

### Installation de Windows

- Compressez l'image CD (.iso) de votre Windows dans un fichier .zip ou .7z pour que le core puisse le lancer.
- Mettez l'image CD de Windows que vous venez de compresser dans le dossier `roms/dos` de votre Recalbox et mettez à jour la liste des jeux.
- Lancez Windows depuis l'interface.
- Une fois lancé, choisissez l'option `[ Boot and Install New Operating System ]`

![](/tutorials/games/guides/dos/w95-1.png){.full-width}

- Il vous proposera de créer une installation dans une image qui sera dans vos bios. Vous n'avez qu'à choisir la taille que prendra l'installation de Windows.

![](/tutorials/games/guides/dos/w95-2.png){.full-width}

>Faites bien attention à bien choisir la bonne taille, celle-ci doit pouvoir contenir vos jeux installés si ils doivent être installées !
{.is-warning}

>Gardez un œil sur votre écran car une fois que la création de l'espace disque définie pour cette installation est terminée, c'est l'exécutable d'installation de Windows qui prend sans attendre le relais, et ce dernier attend un choix de votre part (démarrer depuis le CD-ROM, choix / option 2) sous 5 à 10 secondes maximum !
{.is-warning}

- Une fois votre choix effectué, quelques secondes vont se passer sans rien afficher et votre Windows démarrera.

![](/tutorials/games/guides/dos/w95-3-fr.png){.full-width}

- Poursuivez l'installation de Windows avec ses redémarrages jusqu'à être sur le bureau une fois toutes les installations effectuées.

> N'oubliez pas qu'il faut un clé de license pour pouvoir l'installer !
{.is-info}

![](/tutorials/games/guides/dos/w95-4-fr.png){.full-width}

- Une fois toutes les installations de Windows effectuées, vous pouvez éteindre en faisant `Démarrer` > `Arrêter`. Une fois qu'il vous est indiqué que vous pouvez éteindre, faites `HOTKEY` + `START` sur votre manette pour retourner à l'interface.

> Si vous faites `HOTKEY` + `START` pendant que Windows est affiché, vous devrez le réparer au prochain démarrage !
{.is-info}

### Installation de votre jeu
- Mettez le .zip de votre jeu dans le dossier `roms/dos` de votre Recalbox et mettez à jour la liste des jeux.
- Dans la liste de vos jeu DOS, lancez le jeu à installer sur votre image virtuelle Windows crée précédemment.
- Une fois lancé, sélectionnez `[ Run Installed Operating System ]`.

![](/tutorials/games/guides/dos/w95-5.png){.full-width}

- La liste des Windows disponibles vous sera listé. Choisissez celui que vous désirez et validez.

![](/tutorials/games/guides/dos/w95-6.png){.full-width}

- Windows se lancera en montant votre jeu en tant que CD dans l'explorateur Windows.

![](/tutorials/games/guides/dos/w95-7-fr.png){.full-width}

- Il vous suffit de lancer le programme d'installation de votre jeu pour pouvoir y jouer.

![](/tutorials/games/guides/dos/w95-8-fr.png){.full-width}

## FAQ
> Je n'ai pas l'option `[ Boot and Install New Operating System ]` pour installer Windows.

Deux raisons possibles :
- Vous n'avez pas le fichier .iso de l'image Windows dans votre fichier zip
- L'image .iso de votre Windows n'est pas une version correcte. Vous devez en trouvez une autre 

---

> J'ai un écran avec un `_` clignotant et il ne se passe rien.

Dans ce cas, vous n'avez certainement pas prêté attention au message du tutoriel vous demandant de rester vigilant après le choix de l'espace disque. 
Un écran MS-DOS vous a laissé quelques secondes pour choisir entre **"1 - Lancer à partir du disque dur"** ou **"2 - Lancer à partir du CD-ROM"**. 
Il est important de choisir l'option CD-ROM avant l'expiration du délai.

---
