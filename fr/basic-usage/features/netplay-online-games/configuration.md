---
title: Préparation
description: 
published: true
date: 2025-02-06T23:52:42.336Z
tags: netplay, préparation
editor: markdown
dateCreated: 2021-05-21T08:12:24.749Z
---

## I - Préparation des roms

Vous devez respecter ces étapes pour que vos roms soient préparées pour jouer en Netplay.

### A - Core/Romsets Compatibles Netplay Libretro

#### 1. Arcades

| Systèmes | Core | Romset |
| :---: | :---: | :---: |
| **FinalBurn Neo** | Libretro FbNeo | 1.0.0.03 |
| **Mame** |  |  |
|  | Libretro Mame 2003 | Mame 0.78 |
|  | Libretro Mame 2003Plus | Mélange Mame de 0.78 à 0.188 |
|  | Libretro Mame 2010 | Mame 0.139 |
| **Neo-geo** | Libretro FbNeo | 1.0.0.03 |

#### 2. Console Fantasy

| Systèmes | Core | Romset |
| :---: | :---: | :---: |
| **TIC80** | Libretro Tic80 |  |

#### 3. Consoles de Salon

| Systèmes | Core | Romset |
| :---: | :---: | :---: |
| **Atari 2600** | Libetro Stella | No Intro |
| **Famicom Disk System** |  | No-Intro |
|  | Libretro FCEUmm |  |
|  | Libretro Nestopia |  |
|  | Libretro Mesen |  |
| **Master System** |  | No-Intro |
|  | Libretro PicoDrive |  |
|  | Libretro Genesisplusgx |  |
| **Megadrive** |  | No-Intro |
|  | Libretro PicoDrive |  |
|  | Libretro Genesisplusgx |  |
| **Nintendo Entertainment System** |  | No-Intro |
|  | Libretro FCEUmm |  |
|  | Libretro FCEUNext |  |
|  | Libretro Nestopia |  |
|  | Libretro Quicknes |  |
|  | Libretro Mesen |  |
| **Super Nintendo** |  | No-Intro |
|  | Libretro Snes9x (activer l'option « multitap » |  |
|  | Libretro Snes9x_2002 |  |
|  | Libretro Snes9x_2005 |  |
|  | Libretro Snes9x_2010 |  |
| **Pc-Engine** |  | No-Intro |
|  | Libretro Mednafen_supergrafx |  |
|  | Libretro Mednafen_PCE_FAST |  |
| **Pc-Engine CD** |  | Redump |
|  | Libretro Mednafen_PCE_FAST |  |
|  | Libretro Mednafen_supergrafx |  |
| **PC-FX** | Libretro Mednafen_PCFX | Redump |
| **Sega 32x** | Libretro PicoDrive | No-Intro |
| **Sega CD** |  | Redump |
|  | Libretro PicoDrive |  |
|  | Libretro Genesisplusgx |  |
| **SG-1000** | Libretro blueMSX | No-Intro |
| **Supergrafx** | Libretro Mednafen_supergrafx | No-Intro |

#### 4. Port

| Systèmes | Core | Romset |
| :---: | :---: | :---: |
| **MR BOOM** | Libretro MrBoom | ? |

### B - Core/Romsets Compatibles Netplay Standalone

#### Consoles de Salon

| Systèmes | Core | Romset |
| :---: | :---: | :---: |
| **GameCube** | Dolphin-emu | Redump |
| **Wii** | Dolphin-emu | Redump |

### C - Trier et vérifier vos romsets

Les différentes préférences de tris. Que choisir ?

#### 1 - National ou international ?

* **National :** Si vous voulez jouer uniquement avec les personnes du même pays que vous : gardez uniquement les roms de votre pays **puis privilégiez les roms USA et Japon pour les manquants.**
* **International :** Si vous voulez jouer avec des personnes des autres pays : gardez une rom de chaque pays.

#### 2 - Obtention d'un fichier dat pour les logiciels de tri

Téléchargez les fichiers dat de vos consoles à trier (Lien à venir prochainement... ?).

#### 3 - Le logiciel de tri

* Clrmamepro (**Meilleur logiciel** mais demande de la pratique avant maîtrise, **indispensable pour l'arcade**)

[https://mamedev.emulab.it/clrmamepro/](https://mamedev.emulab.it/clrmamepro/)

* Romulus (Logiciel **beaucoup plus simple d'utilisation** mais ne gèrent pas assez bien l'arcade)

[http://romulus.net63.net/#downloads](https://romulus.cc/#downloads)

Pour savoir déterminer à quoi correspondent [les codes et tags dans les roms](./../../../tutorials/games/generalities/tags-used-in-rom-names).

### D - Scraper vos romsets

Plusieurs façons existent pour scraper vos roms :

#### 1. Scraper avec l'outil intégré de Recalbox

* MENU EMULATIONSTATION (`START`)
* Scrapeur

#### 2. Scraper vos roms avec un logiciel dédiés pour une meilleur personnalisation (Metadata exemple : Hack Region traduction)

* **Skraper**

[https://www.skraper.net/](https://www.skraper.net/) (compatible Windows/Linux et bientôt macOS).

**Tuto video de Skraper :**

[https://www.youtube.com/watch?v=vV8DxtdOCaU&index=15&list=PL2oNQ0AT7fx2ExiSNrfHUzga5GnogI4sh](https://www.youtube.com/watch?v=vV8DxtdOCaU&index=15&list=PL2oNQ0AT7fx2ExiSNrfHUzga5GnogI4sh)

### E - Hasher vos roms

Le **HASH** est une empreinte numérique unique pour une **meilleure reconnaissance** lors du choix d’une partie (il s'agit d'un checksum CRC32 de la rom).

**Le Hash est donc obligatoire pour le bon fonctionnement du Netplay.**

* Menu EmulationStation
* Options des jeux
* Options Netplay
* Hash
* Lancer

Cependant des roms ne posséderont pas de Hash, c’est le cas des systèmes d’Arcade:

* Fba Libretro
* Mame
* Neo-Geo

>Certains programmes peuvent hasher et scrapper en même temps par exemple Skraper !
{.is-info}

## II- Paramétrage du système Netplay

### A - Activation et configuration de Netplay

* Depuis votre Recalbox :
  * Menu EmulationStation
  * Options des jeux
  * Options Netplay

![](/basic-usage/features/netplay/netplaypreparation.png)

* Netplay (Réseau)
  * **ON/OFF**
* Surnom
  * Sélectionner pour configurer votre Surnom

>Veillez à ne pas mettre d'accent dans le Surnom, cela empeche le fonctionnement du Netplay.
{.is-warning}

* Port
  * 55435 (Correspond au serveur de RetroArch)
* Netplay MITM Server relais (voir en bas pour plus d'explications)
  * Aucun
  * New-York
  * Madrid
* Mot de passe prédéfinis
  * Sélectionner pour modifier les mots de passe prédéfinis.
* Hasher les roms Vous avez deux choix.
  * Filtre
    * Seulement hashs manquants
    * Tous les jeux
  * Systèmes Liste des systèmes à effectuer le hash

### B - Configurer votre port (UPNP/Ouvrir son port/Netplay MITM)

>Certains modems possèdent l'UPNP qui permet l'ouverture/fermeture automatiques des ports.
{.is-info}

#### 1. Vérifiez que votre port gère l’UPnP

Nous vous recommandons fortement d'activer l'option UPNP.

* Commencez à héberger un jeu depuis votre recalbox.
* Sur votre pc, allez sur : [https://www.yougetsignal.com/tools/open-ports/](https://www.yougetsignal.com/tools/open-ports/)
* Renseignez le port 55435 puis appuyez sur Check.
  * **a. La réponse est “positif” :** Vous n'avez pas besoin d'ouvrir le port, passer à l'étape B.
  * **b. La réponse est “négatif” :** Soit votre box ne gère pas l'UPNP soit cette option est à activer. Ouvrez **un port manuellement** (recommandé) ou utilisez **Netplay MITM**.

#### 2. Ouvrir son port (Connexion directe)

Il est nécessaire d’ouvrir le port 55435 pour le lobby Libretro et le 2626 pour Dolphin de votre modem internet.

* **Ouvrir votre port manuellement :** Selon votre FAI l’opération à faire est différente. Renseignez-vous sur le site de votre opérateur ou sur des forums.
  * Le nom peut être ce que vous voulez, mais cela aide si vous le nommez exactement comme le logiciel dont les ports sont votre transfert. N'oubliez pas que vous ne pouvez pas utiliser un nom déjà utilisé par une autre règle d'exception.
  * Ensuite, vous choisirez le Protocol, TCP ou UDP. Certains routeurs vous permettent de choisir l'un ou l'autre, tandis que d'autres vous offrent une troisième option pour choisir les deux simultanément.
  * Vous aurez besoin de l'un ou de l'autre, ou des deux. Si ce n'est pas précisez, il est préférable d'ouvrir les ports en utilisant les deux protocoles.
  * Ensuite, vous avez une plage de ports. Dans cette section, nous noterons le port ou la plage de ports que nous allons ouvrir.
  * Si vous n'ouvrez qu'un seul port, notez-le uniquement dans la première case et laissez l'autre vide.
  * Si vous ouvrez, par exemple, 100 ports entre les ports 1500 et 1600, vous notez 1500 dans la première case et 1600 dans la seconde.
  * Tout le reste peut être ignoré en toute sécurité. Vous pouvez ajouter une définition ou un type d'application si vous le souhaitez, mais ce n'est pas nécessaire.
  * Si le routeur ne vous permet pas de choisir à la fois UDP et TCP en même temps, vous devrez faire 2 entrées distinctes pour le même port.
  * Une fois que vous avez terminé d'ajouter votre port, cliquez sur Ajouter une définition ou Accepter ou sur tout ce qui vous permet d'enregistrer vos modifications.
  * Certains routeurs vous permettent de créer des règles d'exception, mais ne les attribuent pas automatiquement à votre ordinateur / périphérique. Il devrait y avoir une option pour le faire.

#### 3. L' option Netplay MITM

L'option Netplay MITM, Relay Server (Server relais) permet d’outrepasser l’ouverture des ports de votre modem.  
Cela permet d'acheminer les deux côtés de la connexion via un serveur d'intermédiaire.

Cependant cela aura un impact sur la vitesse à laquelle les données se synchronisent et donc sur la réactivité du jeu

* **Dans quelles cas l'utiliser ?**

  * Votre routeur ne prend pas en charge le protocole UPnP (ouverture et fermeture automatique des ports).
  * Vous ne voulez pas ouvrir vos ports.
  * Vous avez un doute :

  Activez l'option Utiliser l’option Netplay MITM.

>Il est recommandé d'ouvrir son port manuellement ou d'avoir une box qui gère l'UPnP, car le serveur relais va ajouter une latence supplémentaire.
>
>RetroArch ne vérifie pas si vos ports sont ouverts, pas plus que le serveur de lobby Libretro.  
>Assurez-vous donc d'ouvrir votre port correctement ou activez le serveur relais, sinon les utilisateurs ne pourront pas se connecter à votre session.
{.is-warning}