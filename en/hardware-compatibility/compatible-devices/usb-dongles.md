---
title: USB Dongles
description: 
published: true
date: 2021-08-08T18:29:56.115Z
tags: compatibility, usb, dongles
editor: markdown
dateCreated: 2021-08-05T20:35:41.111Z
---

| Device | Status | Kernel Linux | Latest version tested | Comments |
| :---: | :---: | :---: | :---: | :---: |
| Asus USB-BT400 Bluetooth 4.0 | ✅ |  |  | Works with customizations to the Broadcom firmware - doesn't work from stock. |
| AZiO BTD-V401 Bluetooth 4.0 | ✅ |  |  |  |
| AZiO BTD-V201 USB 2.0 Micro Bluetooth Adapter | ✅ |  |  |  |
| BCM2046B1 | ✅ |  |  |  |
| Belkin Mini Bluetooth v4.0 Adapter | ✅ | 4.0+ |  | Doesn't work on Recalbox on XU4 because of the kernel version (3.10). |
| BigTec BIG121 | ❌ |  |  |  |
| Billionton USBBT02-B-DX | ✅ |  |  |  |
| Cambridge Silicon Radio ‪Bluetooth | ✅ | 3.0+ | v4.0.0 - rpi2B |  |
| CLiPtec USB Bluetooth Dongle Ver. 4.0 + EDR RZB939 | ✅ |  |  |  |
| CSL - V4.0 USB nano Adaptateur Bluetooth with DEL Class 4.0 | ✅ |  |  |  |
| CSR Ltd Bluetooth Dongle (HCI mode) | ❌ |  |  |  |
| D-Link DBT-122 Dongle Bluetooth 1.2 | ✅ |  |  |  |
| Digitus Adapter tiny USB Bluetooth V 4.0 | ✅ |  |  |  |
| Dongle Bluegiga Technologies BLED112-V1 4.0 0dBm | ❌ |  |  |  |
| EMTEC USB Bluetooth dongle 100m | ✅ |  |  |  |
| Energy Sistem 1550T | ✅ |  |  |  |
| Essentiel B Micro dongle bluetooth 2.0 | ❌ |  |  |  |
| ESSENTIEL B Bluetooth USB Bluetooth 4.0 | ✅ |  |  |  |
| Generic Bluetooth 4.0 dongle / adapter. USB 2.0 | ✅ |  |  | Doesn't work on Recalbox on XU4 because of the kernel version (3.10). |
| ieGeek® Mini USB Bluetooth CSR V4.0 Dongle Dual Mode | ✅ |  |  |  |
| IOGEAR Bluetooth 4.0 USB Micro Adapter (GBU521) | ✅ |  |  |  |
| Inateck (BTA-BC4B6) Bluetooth 4.0+EDR | ✅ |  |  |  |
| Inateck USB Bluetooth 4.0 (B00N2E3ZHG) | ✅ |  |  |  |
| Insignia USB Bluetooth Adapter NS-PCY5BMA2 | ✅ |  |  | Works with Recalbox 4.1 with a PS3 controller. |
| IT Works (DONGLE BT-150) USB Bluetooth 4.0 | ✅ |  |  |  |
| Lindy 52213 Dongle USB 3.0 WiFi Bluetooth | ✅ |  |  |  |
| LogiLink BT0006A | ❌ |  |  |  |
| Logilink BT0007A | ✅ |  |  | With an official PS3 DualShock controller, Bluetooth signal seems too low. |
| Maxxtro Bluetooth 4.0 USB adapter micro | ✅ |  |  |  |
| Nano USB to Bluetooth Dongle V2.0 (dynamode) | ❌ |  |  |  |
| ORICO BTA-402 USB Bluetooth 4.0 Micro Adapter (Model X) | ✅ |  |  |  |
| ORICO BTA-403 USB Bluetooth 4.0 | ✅ |  | Raspberry pi 3 | Tested with PS3 cechzc2ua1 controller. |
| PANRICH PBT06H | ✅ |  |  |  |
| Plugable Technologies USB-BT4LE | ✅ |  |  |  |
| Sitecom CN-512 v1 001 | ✅ |  |  |  |
| Sitecom CN-516 | ✅ |  | 4.0.2 | Tested with 8bitdo zero controller. |
| SoundBot SB340 | ✅ |  |  |  |
| Trendnet TBW-102UB | ✅ |  |  |  |
| Trendnet TBW-107UB | ✅ |  | 4.0.0-beta4 | Latency issue |
| Trust Ultra Small Bluetooth 2 USB Adapter 10m BT-2400p (15542) | ✅ |  | 4.0.0-beta |  |
| Trust nano Bluetooth universal 4.0 Adapter | ✅ |  |  |  |
| VIVANCO USB Bluetooth Dongle v4.0 (30447) | ✅ |  |  |  |