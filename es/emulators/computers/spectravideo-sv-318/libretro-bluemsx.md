---
title: Libretro blueMSX
description: 
published: true
date: 2024-07-18T18:47:43.648Z
tags: libretro, bluemsx, sv-318
editor: markdown
dateCreated: 2023-09-11T11:47:36.206Z
---



## ![](/emulators/license.svg) Licencia

El core posee la siguiente licencia [**GPLv2**](https://github.com/libretro/blueMSX-libretro/blob/master/license.txt).

## ![](/emulators/compatibility.png) Compatibilidad

 | RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | 
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | 

## ![](/emulators/features.png) Funcionalidades

| Funcionalidad | ¿Soportada? |
| :---: | :---: |
| Reinicio | ✔ |
| Capturas de pantalla | ✔ |
| Opciones del core | ✔ |
| RetroAchievements | ✔ |
| Controles | ✔ |
| Remapping | ✔ |
| Control de disco | ✔ |

## ![](/emulators/bios.svg) BIOS

### Lista de BIOS obligatorias

| Nombre del fichero | Descripción | MD5 | ¿Preinstalada? |
| :---: | :---: | :---: | :---: |
| svi318.rom | - | ee6ad7ea29e791b03a28a9443e622648 | ✅ |
| svi328a.rom | - | 352f054ab09605070bdff49d73f335cc | ✅ |
| svi806.rom | - | 82f7bc9c08f43db1d79bcb565a0de12b | ✅ |
| svi328a.rom | - | 352f054ab09605070bdff49d73f335cc | ✅ |
| svi806se.rom | - | 6a5536c1eb4f0477c4f76488cd8ca3ad | ✅ |
| svi328a.rom | - | 352f054ab09605070bdff49d73f335cc | ✅ |
| svi806.rom | - | 82f7bc9c08f43db1d79bcb565a0de12b | ✅ |
| svi328.rom | - | 2cebd1db9dda475a011b7bce65c984a2 | ✅ |

### Emplazamiento

Disponed vuestras bios como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 Machines
┃ ┃ ┃ ┃ ┣ 📁 SVI - Spectravideo SVI-318
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **svi318.rom**
┃ ┃ ┃ ┃ ┣ 📁 SVI - Spectravideo SVI-328 80 Column
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **svi328a.rom**
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **svi806.rom**
┃ ┃ ┃ ┃ ┣ 📁 SVI - Spectravideo SVI-328 80 Swedish
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **svi328a.rom**
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **svi806se.rom**
┃ ┃ ┃ ┃ ┣ 📁 SVI - Spectravideo SVI-328 MK2
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **svi328a.rom**
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **svi806.rom**
┃ ┃ ┃ ┃ ┣ 📁 SVI - Spectravideo SVI-328
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **svi328.rom**

## ![](/emulators/roms.png) Roms

### Extensiones de roms soportadas

Las roms deben tener las extensiones siguientes:

* .cas
* .bin
* .zip
* .7z

Este sistema soporta las roms comprimidas en formato .zip/.7z. No obstante, se trata sólo de un archivo.

Los ficheros contenidos en los .zip/.7z deben tener las extensiones citadas previamente.
Además, cada fichero .zip/.7z sólo debe contener un fichero rom al interior.

>Los juegos con la extensión .bin no admiten la compresión en ficheros zip!
{.is-info}
 
### Emplazamiento

Disponed vuestras roms como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 spectravideo
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.zip**

>Se aconsejan las roms del proyecto **TOSEC**.
{.is-success}

>Para más información sobre las roms, consultad [este tutorial](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

>Para conservar las configuraciones personalizadas cuando se produce una actualización del sistema Recalbox, os aconsejamos utilizar las [Sobrecargas de configuración](./../../../advanced-usage/configuration-override).
{.is-info}

### Acceder a las opciones

Podéis configurar las opciones del core de dos formas diferentes.

* Via el Menú RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via el fichero `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Opciones del core

| Opción | Valor de la opción | Variable (para la sobrecarga) | Valores posibles |
| --- | --- | --- | --- |
| Machine Type (Restart) | `Auto` ✅ / `MSX` / `MSXturboR` / `MSX2` / `MSX2+` / `SEGA - SG-1000` / `SEGA - SC-3000` / `SEGA - SF-7000` / `SVI - Spectravideo SVI-318` / `SVI - Spectravideo SVI-328` / `SVI - Spectravideo SVI-328 MK2` / `ColecoVision` / `Coleco (Spectravideo SVI-603)` | `bluemsx_msxtype` | `Auto` / `MSX` / `MSXturboR` / `MSX2` / `MSX2+` / `SEGA - SG-1000` / `SEGA - SC-3000` / `SEGA - SF-7000` / `SVI - Spectravideo SVI-318` / `SVI - Spectravideo SVI-328` / `SVI - Spectravideo SVI-328 MK2` / `ColecoVision` / `Coleco (Spectravideo SVI-603)` |
| Crop Overscan | `Désactivé` ✅ / `Activé` / `MSX2` | `bluemsx_overscan` | `disabled` / `enabled` / `MSX2` |
| VDP Sync Type (Restart) | `Auto` / `50Hz` / `60Hz` ✅ | `bluemsx_vdp_synctype` | `auto` / `50 Hz` / `60 Hz` |
| No Sprite Limit | `Désactivé` ✅ / `Activé` | `bluemsx_nospritelimits` | `disabled` / `enabled` |
| Sound YM2413 Enable (Restart) | `Activé` ✅ / `Désactivé` | `bluemsx_ym2413_enable` | `enabled` / `disabled` |
| Cart Mapper Type (Restart) | `Auto` ✅ / `Normal` / `mirrored` / `basic` / `0x4000` / `0xC000` / `ascii8` / `ascii8sram` / `ascii16` / `ascii16sram` / `ascii16nf` / `konami4` / `konami4nf` / `konami5` / `konamisynth` / `korean80` / `korean90` / `korean126` / `MegaFlashRomScc` / `MegaFlashRomSccPlus` / `msxdos2` / `scc` / `sccexpanded` / `sccmirrored` / `sccplus` / `snatcher` / `sdsnatcher` / `SegaBasic` / `SG1000` / `SG1000Castle` / `SG1000RamA` / `SG1000RamB` / `SC3000` | `bluemsx_cartmapper` | `Auto` / `Normal` / `mirrored` / `basic` / `0x4000` / `0xC000` / `ascii8` / `ascii8sram` / `ascii16` / `ascii16sram` / `ascii16nf` / `konami4` / `konami4nf` / `konami5` / `konamisynth` / `korean80` / `korean90` / `korean126` / `MegaFlashRomScc` / `MegaFlashRomSccPlus` / `msxdos2` / `scc` / `sccexpanded` / `sccmirrored` / `sccplus` / `snatcher` / `sdsnatcher` / `SegaBasic` / `SG1000` / `SG1000Castle` / `SG1000RamA` / `SG1000RamB` / `SC3000` |
| Auto Rewind Cassette | `Activé` ✅ / `Désactivé` | `bluemsx_auto_rewind_cas` | `enabled` / `disabled` |

## ![](/emulators/external-links.png) Enlaces externos

* **Código fuente utilizado** : [https://github.com/libretro/blueMSX-libretro/](https://github.com/libretro/blueMSX-libretro/)
* **Documentación Libretro** : [https://docs.libretro.com/library/bluemsx/](https://docs.libretro.com/library/bluemsx/)