---
title: Softpatching
description: Parchear vuestros juegos sin modificar las roms originales !
published: true
date: 2024-11-17T15:19:04.929Z
tags: 8.1+, softpatching, parchear
editor: markdown
dateCreated: 2023-08-24T22:42:59.220Z
---

## ¿Qué es el softpatching?

El "softpatching" consiste en aplicar a un juego un parche sin modificar el fichero rom original. Podéis elegir si queréis aplicar el parche o no al lanzamiento del juego.

Los parches proporcionan diferentes funcionalidades que van desde la aplicación de códigos para hacer trampas hasta la traducción complete de juegos japoneses a otros idiomas.

> No se pueden aplicar varios parches simultáneamente en un mismo juego.
{.is-info}

## Funcionamiento

La utilización de estos parches es muy sencilla y se realiza en dos etapas:

### Disposición del fichero de patch

En primer lugar, vuestro fichero de patch debe encontrarse en la misma carpeta que vuestro juego. Además, el fichero de patch debe tener el mismo nombre que el fichero rom sin tener en cuenta la extensión.

> Un fichero de patch puede tener las extensiones `.UPS`, `.IPS` ou `.BPS`.
{.is-info}

### Activación del parche

¿El parche se encuentra en el lugar apropiado y está bien renombrado? Entonces ahora debéis elegir qué queréis hacer con el parche. ¿Queréis que se aplique por defecto al lanzar el juego o bien preferís que se os pregunte en cada lanzamiento? ¿O bien queréis que por el momento no se aplique?

Para todo esto, dentro de EmulationStation, en la lista de todos los sitemas, pulsad `Start` y dirigiros dentro de `OPCIONES DE JUEGOS`. Veréis una opción llamada `SOFTPATCHING` con tres posible valores:

- `AUTO`: el parche se aplicará automáticamente à cada lanzamiento.
- `SELECCIONAR`: se os preguntará si queréis aplicar el parche.
- `DESACTIVAR`: se ignorarán todos los parches existentes.

Una vez que habéis elegido una opción, basta con lanzar el juego para probar.

### Activación de parches

A partir de la version 9 de Recalbox, podéis tener varios parches para un mismo juego!

Para ello, cread una carpeta que contendra todos los parches con el mismo nombre que la rom y con el sufijo « -patches ». Por ejemplo :

┣ 📁 nes
┃ ┣ 📁 Legend of Zelda, The (USA) (Rev 1)-patches
┃ ┃ ┣ 🗒 ZeldaC.ips
┃ ┃ ┣ 🗒 Zelda - The Legend of Link (v3-12-20).ips
┃ ┃ ┣ 🗒 Un autre patch.ups
┃ ┣ 🗒 Legend of Zelda, The (USA) (Rev 1).zip

Una vez hecho esto y si habéis elegido la opción `SELECCIONAR`, al lanzamiento del juego, se os presentará una ventana para seleccionar el/los patch/es que queréis aplicar a vuestro juego!

 ## Utilización de múltiples patches al mismo tiempo
 
 Se pueden utilizar varios patches al mismo tiempo. Para ello, hay que asignar un nombre específico como se explica a continuación:
 
 - Los patches que se vayan a aplicar al mismo tiempo deben tener el mismo nombre.
 - Las extensiones de cada patch deben tener un número al final, para indicar el orden de aplicación.
 
 Por ejemplo, supongamos que tenemos el juego `rom.bin` al que queremos aplicar varios patches. Podéis nombrarlos de la siguiente forma:
 
 - rom.ips
 - rom.ips1
 - rom.ips2
 
 Aquí, `rom.ips` será el primer parche a aplicar, `rom.ips1` será el segundo parche a aplicar, y así sucesivamente.
 
 Además, para aplicar los patches de forma automática, desde el menú de la interfaz, podéis hacer `START`, id a `GAME OPTIONS` y poned la opción `SOFTPATCHING` en `AUTO`.
 
 ## Aplicar un único patch automáticamente en cada inicio
 
 También podéis iniciar un juego siempre con el mismo parche. Para hacer esto, hay que aplicar dos etapas:
 
 - En la interfaz, haced `START`, id a `GAME OPTIONS` y seleccionad `LAUNCH LAST` para la opción `SOFTPATCHING`.
 - Todavía dentro de la interfaz, situados sobre el juego que queréis parchear, haced `START` y `MODIFY GAME`, y en la opción `SOFTPATCHING`, eligid el patch que queréis aplicar.
 
De esta forma, no aparecerá ninguna ventana preguntado qué patch queréis aplicar, y el juego siempre se lanzará parcheado.

## Listado de emuladores compatibles

Aquí tenéis un listado de emuladores que soportan los softpatches:

- ColecoVision
  - Libretro GearColeco
- Famicom Disk System
  - Libretro FCEUmm
- Game Boy / Game Boy Color
  - Libretro bsnes
  - Libretro Gambatte
  - Libretro Mesen_S
  - Libretro mGBA
  - Libretro TGB Dual
- Game Boy Advance
  - Libretro mGBA
  - Libretro Meteor
- Master System
  - Libretro Gearsystem
- Megadrive
  - Libretro GenesisPlus GX
  - Libretro GenesisPlus GX Wide
  - Libretro Picodrive
- Neo Geo Pocket / Neo Geo Pocket Color
  - Libretro Mednafen_NGP
- NES
  - Libretro FCEUmm
  - Libretro FCEUNext
  - Libretro Mesen
  - Libretro Nestopia
  - Libretro QuickNES
- Nintendo 64
  - Libretro Mupen64Plus NX
- Satellaview
  - Libretro bsnes
  - Libretro Snes9x
- SNES
  - Libretro bsnes
  - Libretro bsnes HD
  - Libretro Mesen_S
  - Libretro Snes9x
  - Libretro Snes9x 2002
  - Libretro Snes9x 2005
  - Libretro Snes9x 2010
- WonderSwan/Color
  - Libretro Mednafen_WSWAN

## Direcciones web de parches

GohanSenior ha reunido una selección de casi mil patches de traducción al francés para juegos de 8 y 16 bits.

 Para beneficiaros de estos patches, tendréis que: 
 - utilizar los romsets no-intro.
 - descargar el archivo del parche de https://github.com/GohanSenior/Pack-TradFr-Sets-No-Intro-Softpatching-Recalbox/archive/refs/heads/main.zip (enlace a github: https://github.com/GohanSenior/Pack-TradFr-Sets-No-Intro-Softpatching-Recalbox/).
 - descomprimirlo todo en vuestra carpeta "roms".
 - activad el softpatching en las opciones de Recalbox.
 - seleccionad el patch cuando Recalbox te pregunte cómo lanzar el juego.
 
 ¡Y ya lo tenéis todo!, ¡mil juegos traducidos al francés!
 
 ¡No dudéis en poneros en contacto con él en nuestro discord para darle las gracias o informarnos de cualquier problema que podáis encontrar!
 
 Encontraréis otros patches utilizables en los siguientes sitios:
 
- https://traf.romhack.org/
- https://terminus.romhack.net/
