---
title: Libretro Gearsystem
description: libretro-super
published: true
date: 2024-07-24T23:56:40.132Z
tags: libretro, gamegear, gg, gearsystem
editor: markdown
dateCreated: 2021-07-18T17:00:38.089Z
---

**Libretro Gearsystem** is an open source, multi-platform, Sega Master System / Game Gear / SG-1000 emulator written in C++.

* Very accurate Z80 kernel, including undocumented opcodes and behaviors such as R and MEMPTR registers.
* Multi-Mapper support: SEGA, Codemasters, SG-1000 and ROM cartridges only.
* Automatic region detection: NTSC-JAP, NTSC-USA, PAL-EUR.
* Internal database for Roma detection
* Highly accurate VDP emulation, including synchronization and SMS2 mode support only.
* Audio emulation using SDL Audio and Sms_Snd_Emu library.
* Support for battery powered RAM saver.
* Save states.
* Support for Game Genie and Pro Action Replay cheats.
* Runs on Windows, Linux, Mac OS X, Raspberry Pi, iOS and as Libretro core (RetroArch).

The core Libretro Gearsystem was created by Ignacio Sanchez

## ![](/emulators/license.svg) License

This core is under [**GPLv3**](https://github.com/drhelius/Gearsystem/blob/master/LICENSE) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Features

| Feature | Supported |
| :---: | :---: |
| Restart | ✔ |
| Screenshots | ✔ |
| Saves | ✔ |
| States | ✔ |
| Rewind | ✔ |
| Core Options | ✔ |
| RetroAchievements | ✔ |
| RetroArch Cheats - Game Genie | ✔ |
| RetroArch Cheats - Pro Acion Replay | ✔ |
| Controls | ✔ |
| Remapping | ✔ |
| Softpatching | ✔ |

## ![](/emulators/bios.svg) BIOS

### Liste des bios optionnels

| Filename | Description | MD5 | Provided |
| :---: | :---: | :---: | :---: |
| <span>bios</span>.gg | GameGear BIOS (bootrom) | 672e104c3be3a238301aceffc3b23fd6 | ❌ |

### Location

Put the bios like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 gamegear
┃ ┃ ┃ ┃ ┣ 🗒 **<span>bios</span>.gg**

## ![](/emulators/roms.png) Roms

### Supported extensions

Roms must have this extension:

* .gg
* .zip
* .7z

This system supports compressed roms in .zip/.7z format. However, be careful, it is only an archive.

The files contained in the .zip/.7z must correspond to the extensions mentioned above.
Moreover, each .zip/.7z file must contain only one compressed rom.

### Location

Put your roms like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 gamegear
┃ ┃ ┃ ┃ ┣ 🗒 **game.zip**

>The roms using **No-Intro** format are highly recommended.
{.is-success}

>For more information about roms, go to [this tutorial](./../../../tutorials/games/generalities/isos-and-roms)!
{.is-info}

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Accessing to options

You can configure various options in two different ways.

* Via RetroArch menu:

┣ 📁 RetroArch Menu
┃ ┣ 📁 Core options
┃ ┃ ┣ 🧩 Name_option

* Via the file `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core options

| Options | Options values | Variables (for configuration override) | Variables values |
| --- | --- | --- | --- |
| System (restart) | `Auto` ✅ / `Master System / Mark III` / `Game Gear` / `SG-1000 / Multivision` | `gearsystem_system` | `Auto` / `Master System / Mark III` / `Game Gear` / `SG-1000 / Multivision` |
 | Region (restart) | `Auto` ✅ / `Master System Japan` / `Master System Export` / `Game Gear Japan` / `Game Gear Export` / `Game Gear International` | `gearsystem_region` | `Auto` / `Master System Japan` / `Master System Export` / `Game Gear Japan` / `Game Gear Export` / `Game Gear International` |
 | Mapper (restart) | `Auto` ✅ / `ROM` / `SEGA` / `Codemasters` / `Korean` / `MSX` / `Janggun` / `SG-1000` | `gearsystem_mapper` | `Auto` / `ROM` / `SEGA` / `Codemasters` / `Korean` / `MSX` / `Janggun` / `SG-1000` |
 | Refresh Rate (restart) | `Auto` ✅ / `NTSC (60 Hz)` / `PAL (50 Hz)` | `gearsystem_timing` | `Auto` / `NTSC (60 Hz)` / `PAL (50 Hz)` |
 | Master System BIOS (restart) | `Disabled` ✅ / `Enabled` | `gearsystem_bios_sms` | `Disabled` / `Enabled` |
 | Game Gear BIOS (restart) | `Disabled` ✅ / `Enabled` | `gearsystem_bios_gg` | `Disabled` / `Enabled` |
 | 3D Glasses | `Both Eyes / OFF` ✅ / `Left Eye` / `Right Eye` | `gearsystem_glasses` | `Both Eyes / OFF` / `Left Eye` / `Right Eye` |
 | Allow Up+Down / Left+Right | `Disabled` ✅ / `Enabled` | `gearsystem_up_down_allowed` | `Disabled` / `Enabled` |

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/drhelius/Gearsystem/](https://github.com/drhelius/Gearsystem/)
* **Libretro Documentation**: [https://docs.libretro.com/library/gearsystem/](https://docs.libretro.com/library/gearsystem/)