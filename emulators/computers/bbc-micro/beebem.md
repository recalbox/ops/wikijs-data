---
title: BeebEm
description: 
published: true
date: 2024-07-24T21:55:22.354Z
tags: bbc micro, beebem, 8.0+
editor: markdown
dateCreated: 2021-09-23T22:51:27.877Z
---



## ![](/emulators/license.svg) License

This core is under **GPL** license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Features

| Feature | Supported |
| :---: | :---: |
| Restart | ✔ |
| Screensots | ✔ |

## ![](/emulators/bios.svg) BIOS

>**No bios is required.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Supported extensions

Roms must have the extension:

* .ssd
* .dsd
* .adl
* .adf
* .uef
* .csw

### Location

Put the roms like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 bbcmicro
┃ ┃ ┃ ┃ ┣ 🗒 **game.ssd**

>For more information about roms, go to [this tutorial](./../../../tutorials/games/generalities/isos-and-roms)!
{.is-info}

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://gitlab.com/bkg2k/beebem-linux](https://gitlab.com/bkg2k/beebem-linux)
* **Official website**: [http://www.mkw.me.uk/beebem/](http://www.mkw.me.uk/beebem/)