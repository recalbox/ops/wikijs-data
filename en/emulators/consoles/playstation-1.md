---
title: Playstation 1
description: 
published: true
date: 2021-10-17T12:27:44.667Z
tags: consoles, sony, ps1, playstation 1
editor: markdown
dateCreated: 2021-07-28T06:02:03.732Z
---

![](/emulators/consoles/playstation1.svg){.align-center}

## Technical data

* **Manufacturer**: Sony Computer Entertainment
* **Year of release**: 1994
* **Units sold**: 102.48 millions
* **Best-selling game**: Gran Turismo
* **CPU**: 32-bit R3000A RISC cadencé à 33.8688 Mhz
* **RAM**: 16 Mbits
* **Picture Processor Unit**: 32-bit
* **Video RAM**: 8 Mbits
* **Audio RAM**: 4 Mbits
* **GPU**: GFX processor unit

## Presentation

The PlayStation is a fifth-generation console produced by Sony Computer Entertainment starting in 1994. The original PlayStation was the first machine in the PlayStation line, later declined in PSone (a smaller and lighter version than the original).

On May 18, 2004, almost ten years after its launch, Sony announced that it had distributed 100 million consoles worldwide and over 962 million PlayStation games.

## Emulators

[PCSX-ReARMed](pcsx-rearmed)
[Libretro PCSX-ReARMed](libretro-pcsx-rearmed)
[Libretro Swanstation](libretro-swanstation)
[Libretro Mednafen_PSX](libretro-mednafen_psx)
[Libretro Mednafen_PSX_HW](libretro-mednafen_psx_hw)

