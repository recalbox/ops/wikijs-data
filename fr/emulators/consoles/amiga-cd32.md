---
title: Amiga CD32
description: 
published: true
date: 2021-11-16T08:50:37.383Z
tags: amiga-cd32, consoles, amiga, commodore, cd32
editor: markdown
dateCreated: 2021-05-21T07:58:29.270Z
---

![](/emulators/consoles/amigacd32.svg){.align-center}

## Fiche technique

* **Fabricant :** Commodore International
* **Année de sortie :** 1993
* **Système d'exploitation :** AmigaOS 3.1
* **Processeur :** Motorola 68EC020 @ 14.32 MHz (NTSC), 14.18 MHz (PAL)
* **RAM :** 2 MB
* **ROM :** 1 MB Kickstart avec le firmware CD32, 1 KB EEPROM pour les sauvegardes des jeux
* **Processeur graphique :** Advanced Graphics Architecture
* **Puce sonore :** 4 canaux 8-bit PCM
* **Résolution :** 320x200 à 1280x400i (NTSC), 320x256 à 1280x512i (PAL)

## Présentation

L’**Amiga CD32** est une console de jeux vidéo de cinquième génération développée par Commodore et commercialisée en septembre 1993. La CD32 est une console 32-bit dont l'architecture est basée sur l'ordinateur **Amiga 1200**.

L'**Amiga CD32** est la première 32-bit à voir le jour en Occident (la FM Towns Marty l'a précédée au Japon). Comme la **3DO** ou l'**Atari Jaguar**, commercialisées dans la même période, la CD32 n'est pas parvenue à percer sur le marché malgré le soutien des fans de la gamme **Amiga**. Ses limitations techniques, son mode d'affichage en bitplan la limitant grandement dans l'affichage des jeux en 3D texturée comme **Doom** en vogue à l'époque, le manque d'implication des développeurs et les difficultés financières de Commodore sont autant d'explications avancées de son échec.

## Émulateurs

[Amiberry](amiberry)
[Libretro PUAE](libretro-puae)
[Libretro UAE4ARM](libretro-uae4arm)