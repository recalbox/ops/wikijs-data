---
title: Flashear una imagen
description: 
published: true
date: 2023-09-26T07:06:55.051Z
tags: imagen, flashear
editor: markdown
dateCreated: 2023-09-16T08:21:20.677Z
---

En esta sección aprenderéis a flashear Recalbox usando diferentes programas.

Aquí tenéis la lista de tutoriales disponibles:

[Balena Etcher](balena-etcher)
[Raspberry Pi Imager](raspberry-pi-imager)
[Rufus](rufus)
[Unetbootin](unetbootin)