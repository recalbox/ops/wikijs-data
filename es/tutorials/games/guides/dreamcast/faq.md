---
title: FAQ
description: 
published: true
date: 2023-09-19T19:15:55.349Z
tags: faq
editor: markdown
dateCreated: 2023-09-19T19:15:55.349Z
---

## Conversión de juegos

### ¿Qué extensiones de juego se pueden utilizar con la Dreamcast?

Existen varias extensiones, pero nos centraremos en las siguientes:

* `.BIN`/`.CUE`
* `.CHD
* GDI

### Quiero convertir mis isos del formato `.BIN`/`.CUE` a `.CHD` ¿Es posible?

Sí, pero es posible que más adelante tengáis problemas con los emuladores. Esto se debe a que el formato Redump (`.BIN`/`.CUE`) almacena más información que los simples datos del juego, como los datos añadidos cuando se creó el Master Glass en la fábrica y después en el momento del prensado. Esta información no es leída por los emuladores y no les sería de utilidad.

El formato del proyecto Redump está diseñado para la conservación y el archivado puros, no para la reproducción.

Para la reproducción, se recomienda encarecidamente utilizar el formato TOSEC (`.GDI`). Este formato incluye pistas y datos legibles por los reproductores de Dreamcast (Yamaha GD-ROM) y es totalmente comprensible con todos los emuladores.

El formato `.CHD` (v4 y v5) se diseñó para convertir sólamente los `.GDI` al formato de TOSEC.

Convertirlo al formato Redump os dará problemas, porque no es posible "recalibrar" las pistas de forma sencilla (sin utilizar software de terceros, transfiriendo datos y desplazando las posiciones grabadas en el fichero `.CUE` a un fichero `.GDI` + reformateando los archivos RAW) a diferencia de TOSEC.

Cuando el emulador intentará leer una imagen `.BIN`/`.CUE` convertida en `.CHD`, el emulador no podrá entender el origen del conjunto, y esperará encontrar pistas temporizadas como en TOSEC.

Entre `BIN/CUE` y `GDI` no podemos decir que un formato sea mejor que el otro.

### ¿Es mejor convertir mis isos del formato `.GDI` al formato `.CHD`?

Sí, eso es lo mejor que puedes hacer, convertir tus isos al formato `.CHD`, ganaréis bastan espacio de almacenamiento y la operación es reversible.

## Juegos multidisco

En la **Dreamcast**, hay **algunos juegos multi-disco**.

**Ejemplo**:

* _Resident Evil Code Veronica_
* _Skies Of Arcadia_
* _Grandia 2_
* _Etc..._

**Así se cambian los discos** :

Al final del disco 1 de tu juego, el juego realiza una _grabación_. Volved al menú con **Hotkey** (en Reicast esto es suficiente), luego lanzad el segundo disco **recuperando la grabación anterior**.

¡No puede ser más sencillo!

## Acceder al menú de la tarjeta de memoria

### Objetivo

El objetivo es poder modificar y manipular el contenido de las tarjetas de memoria. Hay 2 formas de hacerlo.

### {.tabset}
#### Expulsión del disco

* Iniciad un juego de Dreamcast.
* Cuando el juego se esté reproduciendo, expulsad el disco con `Hotkey` + `Left stick up`.
* La consola parará automáticamente el juego y entrará en el sistema. Desde aquí podéis acceder a las tarjetas de memoria.
* Para reanudar el juego, pulsad de nuevo `Hotkey` + `Stick izquierdo arriba`.

#### Disco vacío

* Cread un fichero de texto vacío en la carpeta `/share/roms/dreamcast`, que podéis renombrar a vuestro gusto, con la extensión `.cdi` (Dreamcast BIOS.cdi por ejemplo).
* Actualizad la lista de juegos en Recalbox.
* Lanzad el juego y ¡voilà!