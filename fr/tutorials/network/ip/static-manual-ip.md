---
title: Définir l'adresse IP de la connexion Wi-Fi
description: L'adresse de votre Recalbox change régulièrement ? Fixez-là ! 
published: true
date: 2023-07-20T10:28:53.679Z
tags: wifi, ip, réseau, adresse, administration
editor: markdown
dateCreated: 2021-05-21T08:38:57.717Z
---

Recalbox permet de définir une adresse IP à utiliser pour la connexion Wi-Fi, cette configuration s'effectue dans [le fichier de configuration recalbox.conf](./../../../basic-usage/getting-started/recalboxconf-file).

>Recalbox permet de définir les paramètres réseau afin de répondre aux besoins spécifiques, une erreur de configuracion peut conduire à un conflit ou d'autres problèmes. L'idéale est d'effectuer cette configuration au niveau du routeur. Utilisez ce tutoriel uniquement si vous savez ce que vous faites. 
{.is-warning} 



Ouvrez le fichier `recalbox.conf` et rendez-vous au niveau bloc suivant:

```ini
## Wifi - static IP
## if you want a static IP address, you must set all 4 values (ip, netmask, gateway and nameservers)
## if any value is missing or all lines are commented out, it will fall back to the
## default of DHCP. For nameservers, you must set at least 1 nameserver.
;wifi.ip=manual ip address (ex: 192.168.1.2)
;wifi.netmask=network mask (ex: 255.255.255.0)
;wifi.gateway=ip address of gateway (ex: 192.168.1.1)
;wifi.nameservers=ip address of domain name servers, space separated (ex: 192.168.1.1 8.8.8.8)
```

Supprimez le caractère `;` présent au début des trois dernières lignes et adaptez les valeurs comme bon vous semble. 

```ini
wifi.ip=192.168.1.10
wifi.netmask=255.255.255.0
wifi.gateway=192.168.1.254
wifi.nameservers=8.8.8.8 8.8.4.4
```

Redémarrez votre Recalbox pour prendre en compte la nouvelle configuration à l'aide de la commande `reboot`. 