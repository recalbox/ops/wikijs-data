---
title: GameCube
description: Nintendo GameCube
published: true
date: 2021-08-09T19:58:00.084Z
tags: consoles, nintendo, gamecube, gc
editor: markdown
dateCreated: 2021-07-27T18:45:12.095Z
---

![](/emulators/consoles/gamecube.svg){.align-center}

## Technical data

* **Manufacturer**: Nintendo
* **Year of release**: 2001
* **Units sold**: 21.74 million
* **Best-selling game**: Super Smash Bros. Melee
* **CPU**: 128-bit IBM Gekko PowerPC @ 485 MHz
* **RAM**: 43 MB total non-unified RAM
* **Video**: 162 MHz "Flipper" LSI
* **Resolution**: 640×480 480i or 480p - 60 Hz
* **Sound**: 16-bit Dolby Pro Logic IIMedia : 1.5 GB miniDVD
* **Media**: 1.5 GB miniDVDî×

## Presentation

The **Nintendo GameCube** is a home video game console from the Japanese manufacturer Nintendo, released in 2001 (2002 in Europe), developed in association with IBM, NEC and ATI. It was the second to last of the sixth generation video game consoles.

## Emulators

[Dolphin](dolphin)