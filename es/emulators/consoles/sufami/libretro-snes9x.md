---
title: Libretro Snes9X
description: 
published: true
date: 2024-07-13T08:13:06.724Z
tags: libretro, snes9x, sufami turbo
editor: markdown
dateCreated: 2023-09-07T20:52:14.707Z
---

**Libretro Snes9X** es una adaptación ascendente de Snes9x, un emulador poratble Super Nintendo Entertainment System adaptado para el sistema Libretro.

## ![](/emulators/license.svg) Licencia

El core posee la siguiente licencia [**non commerciale**](https://github.com/snes9xgit/snes9x/blob/master/LICENSE).

## ![](/emulators/compatibility.png) Compatibilidad

 | RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | 
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Funcionalidades

| Funcionalidad | ¿Soportada? |
| :---: | :---: |
| Reinicio | ✔ |
| Capturas de pantalla | ✔ |
| Grabaciones | ✔ |
| Grabaciones instantáneas | ✔ |
| Rebobinado | ✔ |
| Netplay | ✔ |
| Opciones del core | ✔ |
| RetroAchievements | ✔ |
| Cheats RetroArch | ✔ |
| Controles | ✔ |
| Remapping | ✔ |
| Multi-Ratón | ✔ |
| Subsystem | ✔ |
| Softpatching | ✔ |

## ![](/emulators/bios.svg) BIOS

### Lista de BIOS obligatorias

| Nombre del fichero | Descripción | MD5 | ¿Preinstalada? |
| :---: | :---: | :---: | :---: |
| STBIOS.bin | Sufami Turbo (Japan) | d3a44ba7d42a74d3ac58cb9c14c6a5ca | ❌  |

### Emplazamiento

Disponed vuestras bios como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 sufami
┃ ┃ ┃ ┃ ┣ 🗒 **STBIOS.bin**

## ![](/emulators/roms.png) Roms

### Extensiones de roms soportadas

Las roms deben tener las extensiones siguientes:

* .fig
* .gd3
* .sfc
* .smc
* .st
* .swc
* .zip
* .7z

Este sistema soporta las roms comprimidas en formato .zip/.7z. No obstante, se trata sólo de un archivo.

Los ficheros contenidos en los .zip/.7z deben tener las extensiones citadas previamente.
Además, cada fichero .zip/.7z sólo debe contener un fichero rom al interior.

### Emplazamiento

Disponed vuestras roms como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 sufami
┃ ┃ ┃ ┃ ┣ 🗒 **juego.zip**

>Se aconsejan las roms del proyecto **No-Intro**.
{.is-success}

>Para más información sobre las roms, consultad [este tutorial](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

>Para conservar las configuraciones personalizadas cuando se produce una actualización del sistema Recalbox, os aconsejamos utilizar las [Sobrecargas de configuración](./../../../advanced-usage/configuration-override).
{.is-info}

### Acceder a las opciones

Podéis configurar las opciones del core de dos formas diferentes.

* Via el Menú RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via el fichero `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Opciones del core

| Opción | Valor de la opción | Variable (para la sobrecarga) | Valores posibles |
| --- | --- | --- | --- |
| Console Region (Reload Core) | `Auto` ✅ / `NTSC` / `PAL` | `snes9x_region` | `auto` / `ntsc` / `pal` |
| Preferred Aspect Ratio | `4:3` ✅ / `Uncorrected` / `Auto` / `NTSC` / `PAL` | `snes9x_aspect` | `4:3` / `uncorrected` / `auto` / `ntsc` / `pal` |
| Crop Overscan | `Activé` ✅ / `Désactivé` | `snes9x_overscan` | `enabled` / `disabled` |
| Hi-Res Blending | `Désactivé` ✅ / `Merge` / `Blur` | `snes9x_hires_blend` | `disabled` / `merge` / `blur` |
| Blargg NTSC Filter | `Désactivé` ✅ / `Monochrome` / `RF` / `Composite` / `S-Video` / `RGB` | `snes9x_blargg` | `disabled` / `monochrome` / `rf` / `composite` / `s-video` / `rgb` |
| Audio Interpolation | `Gaussian` ✅ / `Cubic` / `Sinc` / `None` / `Linear` | `snes9x_audio_interpolation` | `gaussian` / `cubic` / `sinc` / `none` / `linear` |
| Allow Opposing Directions | `Désactivé` ✅ / `Activé` | `snes9x_up_down_allowed` | `disabled` / `enabled` |
| SuperFX Overclocking | `50%` / `60%` / `70%` / `80%` / `90%` / `100%` ✅ / `150%` / `200%` / `250%` / `300%` / `350%` / `400%` / `450%` / `500%` | `snes9x_overclock_superfx` | `50%` / `60%` / `70%` / `80%` / `90%` / `100%` / `150%` / `200%` / `250%` / `300%` / `350%` / `400%` / `450%` / `500%` |
 | Reduce Slowdown (Unsafe) | `Désactivé` ✅ / `Light` / `Compatible` / `Max` | `snes9x_overclock_cycles` | `disabled` / `light` / `compatible` / `max` |
 | Reduce Flickering (Unsafe) | `Désactivé` ✅ / `Activé` | `snes9x_reduce_sprite_flicker` | `disabled` / `enabled` |
| Randomize Memory (Unsafe) | `Désactivé` ✅ / `Activé` | `snes9x_randomize_memory` | `disabled` / `enabled` |
 | Block Invalid VRAM Access | `Activé` ✅ / `Désactivé` | `snes9x_block_invalid_vram_access` | `enabled` / `disabled` |
| Echo Buffer Hack (Unsafe, only enable for old addmusic hacks) | `Désactivé` ✅ / `Activé` | `snes9x_echo_buffer_hack` | `disabled` / `enabled` |
 | Mode | `Light Gun` / `Touchscreen` | `snes9x_lightgun_mode` | `Lightgun` / `Touchscreen` |
| Super Scope Reverse Trigger Buttons | `Désactivé` ✅ / `Activé` | `snes9x_superscope_reverse_buttons` | `disabled` / `enabled` |
| Super Scope Crosshair | `0` / `1` / `2` ✅ / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` | `snes9x_superscope_crosshair` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` |
| Super Scope Color | `White` ✅ / `White (blend)` / `Red` / `Red (blend)` / `Orange` / `Orange (blend)` / `Yellow` / `Yellow (blend)` / `Green` / `Green (blend)` / `Cyan` / `Cyan (blend)` / `Sky` / `Sky (blend)` / `Blue` / `Blue (blend)` / `Violet` / `Violet (blend)` / `Pink` / `Pink (blend)` / `Purple` / `Purple (blend)` / `Black` / `Black (blend)` / `25% Grey` / `25% Grey (blend)` / `50% Grey` / `50% Grey (blend)` / `75% Grey` / `75% Grey (blend)` | `snes9x_superscope_color` | `White` / `White (blend)` / `Red` / `Red (blend)` / `Orange` / `Orange (blend)` / `Yellow` / `Yellow (blend)` / `Green` / `Green (blend)` / `Cyan` / `Cyan (blend)` / `Sky` / `Sky (blend)` / `Blue` / `Blue (blend)` / `Violet` / `Violet (blend)` / `Pink` / `Pink (blend)` / `Purple` / `Purple (blend)` / `Black` / `Black (blend)` / `25% Grey` / `25% Grey (blend)` / `50% Grey` / `50% Grey (blend)` / `75% Grey` / `75% Grey (blend)` |
| Justifier 1 Crosshair | `0` / `1` / `2` / `3` / `4` ✅ / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` | `snes9x_justifier1_crosshair` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` |
| Justifier 1 Color | `White` / `White (blend)` / `Red` / `Red (blend)` / `Orange` / `Orange (blend)` / `Yellow` / `Yellow (blend)` / `Green` / `Green (blend)` / `Cyan` / `Cyan (blend)` / `Sky` / `Sky (blend)` / `Blue` ✅ / `Blue (blend)` / `Violet` / `Violet (blend)` / `Pink` / `Pink (blend)` / `Purple` / `Purple (blend)` / `Black` / `Black (blend)` / `25% Grey` / `25% Grey (blend)` / `50% Grey` / `50% Grey (blend)` / `75% Grey` / `75% Grey (blend)` | `snes9x_justifier1_color` | `White` / `White (blend)` / `Red` / `Red (blend)` / `Orange` / `Orange (blend)` / `Yellow` / `Yellow (blend)` / `Green` / `Green (blend)` / `Cyan` / `Cyan (blend)` / `Sky` / `Sky (blend)` / `Blue` / `Blue (blend)` / `Violet` / `Violet (blend)` / `Pink` / `Pink (blend)` / `Purple` / `Purple (blend)` / `Black` / `Black (blend)` / `25% Grey` / `25% Grey (blend)` / `50% Grey` / `50% Grey (blend)` / `75% Grey` / `75% Grey (blend)` |
| Justifier 2 Crosshair | `0` / `1` / `2` / `3` / `4` ✅ / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` | `snes9x_justifier2_crosshair` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` |
| Justifier 2 Color | `White` / `White (blend)` / `Red` / `Red (blend)` / `Orange` / `Orange (blend)` / `Yellow` / `Yellow (blend)` / `Green` / `Green (blend)` / `Cyan` / `Cyan (blend)` / `Sky` / `Sky (blend)` / `Blue` / `Blue (blend)` / `Violet` / `Violet (blend)` / `Pink` ✅ / `Pink (blend)` / `Purple` / `Purple (blend)` / `Black` / `Black (blend)` / `25% Grey` / `25% Grey (blend)` / `50% Grey` / `50% Grey (blend)` / `75% Grey` / `75% Grey (blend)` | `snes9x_justifier2_color` | `White` / `White (blend)` / `Red` / `Red (blend)` / `Orange` / `Orange (blend)` / `Yellow` / `Yellow (blend)` / `Green` / `Green (blend)` / `Cyan` / `Cyan (blend)` / `Sky` / `Sky (blend)` / `Blue` / `Blue (blend)` / `Violet` / `Violet (blend)` / `Pink` / `Pink (blend)` / `Purple` / `Purple (blend)` / `Black` / `Black (blend)` / `25% Grey` / `25% Grey (blend)` / `50% Grey` / `50% Grey (blend)` / `75% Grey` / `75% Grey (blend)` |
| M.A.C.S. Rifle Crosshair | `0` / `1` / `2` ✅ / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` | `snes9x_rifle_crosshair` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` |
| M.A.C.S. Rifle Color | `White` ✅ / `White (blend)` / `Red` / `Red (blend)` / `Orange` / `Orange (blend)` / `Yellow` / `Yellow (blend)` / `Green` / `Green (blend)` / `Cyan` / `Cyan (blend)` / `Sky` / `Sky (blend)` / `Blue` / `Blue (blend)` / `Violet` / `Violet (blend)` / `Pink` / `Pink (blend)` / `Purple` / `Purple (blend)` / `Black` / `Black (blend)` / `25% Grey` / `25% Grey (blend)` / `50% Grey` / `50% Grey (blend)` / `75% Grey` / `75% Grey (blend)` | `snes9x_rifle_color` | `White` / `White (blend)` / `Red` / `Red (blend)` / `Orange` / `Orange (blend)` / `Yellow` / `Yellow (blend)` / `Green` / `Green (blend)` / `Cyan` / `Cyan (blend)` / `Sky` / `Sky (blend)` / `Blue` / `Blue (blend)` / `Violet` / `Violet (blend)` / `Pink` / `Pink (blend)` / `Purple` / `Purple (blend)` / `Black` / `Black (blend)` / `25% Grey` / `25% Grey (blend)` / `50% Grey` / `50% Grey (blend)` / `75% Grey` / `75% Grey (blend)` |
| Show Layer 1 | `Activé` ✅ / `Désactivé` | `snes9x_layer_1` | `enabled` / `disabled` |
| Show Layer 2 | `Activé` ✅ / `Désactivé` | `snes9x_layer_2` | `enabled` / `disabled` |
| Show Layer 3 | `Activé` ✅ / `Désactivé` | `snes9x_layer_3` | `enabled` / `disabled` |
| Show Layer 4 | `Activé` ✅ / `Désactivé` | `snes9x_layer_4` | `enabled` / `disabled` |
| Show Sprite Layer | `Activé` ✅ / `Désactivé` | `snes9x_layer_5` | `enabled` / `disabled` |
| Enable Graphic Clip Windows | `Activé` ✅ / `Désactivé` | `snes9x_gfx_clip` | `enabled` / `disabled` |
| Enable Transparency Effects | `Activé` ✅ / `Désactivé` | `snes9x_gfx_transp` | `enabled` / `disabled` |
| Enable Sound Channel 1 | `Activé` ✅ / `Désactivé` | `snes9x_sndchan_1` | `enabled` / `disabled` |
| Enable Sound Channel 2 | `Activé` ✅ / `Désactivé` | `snes9x_sndchan_2` | `enabled` / `disabled` |
| Enable Sound Channel 3 | `Activé` ✅ / `Désactivé` | `snes9x_sndchan_3` | `enabled` / `disabled` |
| Enable Sound Channel 4 | `Activé` ✅ / `Désactivé` | `snes9x_sndchan_4` | `enabled` / `disabled` |
| Enable Sound Channel 5 | `Activé` ✅ / `Désactivé` | `snes9x_sndchan_5` | `enabled` / `disabled` |
| Enable Sound Channel 6 | `Activé` ✅ / `Désactivé` | `snes9x_sndchan_6` | `enabled` / `disabled` |
| Enable Sound Channel 7 | `Activé` ✅ / `Désactivé` | `snes9x_sndchan_7` | `enabled` / `disabled` |
| Enable Sound Channel 8 | `Activé` ✅ / `Désactivé` | `snes9x_sndchan_8` | `enabled` / `disabled` |

## ![](/emulators/external-links.png) Enlaces externos

* **Código fuente utilizado** : [https://github.com/libretro/snes9x/](https://github.com/libretro/snes9x/)
* **Documentación Libretro** : [https://docs.libretro.com/library/snes9x/](https://docs.libretro.com/library/snes9x/)