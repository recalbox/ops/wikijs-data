---
title: UNetbootin
description: 
published: true
date: 2021-12-01T05:56:14.181Z
tags: unetbootin
editor: markdown
dateCreated: 2021-08-07T00:01:59.895Z
---

## Features

[UNetbootin](https://unetbootin.github.io/) can create a bootable Live USB stick.

It loads distributions by downloading ISO files (CD image) for you, or by [using an ISO file already downloaded by you](https://unetbootin.github.io/#other).

![](/tutorials/utilities/image-write/unetbootin/unetbootin1.jpg){.full-width}

## Using Unetbootin

Select an ISO file or a distribution to download, choose a target disk (USB key or hard disk), then reboot when finished. If your USB key is not recognized, reformat it to FAT32 format.

![](/tutorials/utilities/image-write/unetbootin/unetbootin2.jpg){.full-width}

![](/tutorials/utilities/image-write/unetbootin/unetbootin3.jpg){.full-width}

![](/tutorials/utilities/image-write/unetbootin/unetbootin4.jpg){.full-width}

If you use the "USB stick" installation mode: after reboot, [boot from USB stick](http://pcsupport.about.com/od/tipstricks/ht/bootusbflash.htm).

On PCs, it is usually enough to press the Escape or F12 key immediately after restarting the computer, while on Macs, you need to hold down the Option key (⌥) before starting macOS.

If you are using the "Hard disk" installation mode: after reboot, select the UNetbootin entry from the boot menu.

## Removal instructions (only applicable for hard disk installations)

If you are using Windows, UNetbootin should offer to remove it the next time Windows starts. Alternatively, you can remove it via `Add/Remove Programs` in the Control Panel.

If you are using Linux, restart the UNetbootin executable (with administrator privileges) and press OK on the uninstall prompt.

Removal is only required if you used the "Hard disk" installation mode; to remove the boot loader from a USB stick, save its contents and then reformat it.

Uninstalling UNetbootin simply removes the UNetbootin entry from your boot menu; if you have installed an operating system on a partition using UNetbootin, removing UNetbootin will not remove the OS.

To manually remove a Linux installation, you need to restore the Windows boot loader using "fixmbr" from a recovery CD and use Parted Magic to erase the Linux partition and resize the Windows partition.

## {.tabset}
### License
UNetbootin was created and written by [Geza Kovacs](http://www.gkovacs.com/) (Github: [gkovacs](http://github.com/gkovacs), Launchpad: [gezakovacs](https://launchpad.net/~gezakovacs), [contact info](http://wiki.ubuntu.com/GezaKovacs)).

The translators are listed on the [translations page](https://github.com/unetbootin/unetbootin/wiki/translations).

UNetbootin is distributed under the [GNU General Public License version 2](http://www.gnu.org/licenses/old-licenses/gpl-2.0.html) or [higher](http://www.gnu.org/copyleft/gpl.html). The content of the site (documentation, screenshots and logos) is distributed under [Creative Commons - Share Alike 3.0 license](http://creativecommons.org/licenses/by-sa/3.0/).