---
title: 2. Las variables
description: 
published: true
date: 2024-04-20T10:08:22.978Z
tags: 9.2+, temas
editor: markdown
dateCreated: 2024-04-07T13:05:25.491Z
---

## Introducción

Para que los themes sean más dinámicos, se pueden utilizar varias variables. Dichas variables se pueden utilizar en cualquiera de las etiquetas `XML`.

A la excepción de las variables `$system`, `$language` et `$country`, todas las demás se encuentran disponibles à partir de Recalbox 9.2.

## Lista de variables

### Variable `$system`

La variable `$system` permite que el nombre del sistema sea dinámico.

Ejemplo :

```xml
<include>
  ./$system/custom.xml
</include>
```

### Variable `$language`
 
La variable `$language` permite recuperar el código del idioma elegido por el usuario.
 
 Ejemplo:
 
 ```xml
 <include>
   ./data/$language/texts.xml
 </include>
 ```
 
 ### Variable `$country`
 
 La variable `$country` permite recuperar el país del usuario.
 
 Ejemplo:
 
 ```xml
 <include>
   ./data/$country/logo.svg
 </include>
 ```
 

### Variable `$fullname`

La variable `$fullname` contiene el nombre completo del sistema en curso.

Ejemplo :

```xml
<text name="systemCurrent" text="$fullname"></text>
```

### Variable `$type`

La variable `$type` contiene el tipo de sistema. Se trata de un valor entre los posibles valores de la siguiente lista:

- arcade : sistema arcade
- computer : micro-ordenador
- console : consola de salon
- engine : motor de juego
- fantasy : consola fantasia
- handheld : consola portable
- port : los ports
- virtual : sistema virtual excepto el arcade
- virtual-arcade : sistema virtual arcade por constructor

Ejemplo :

```xml
<text name="systemGameName" text="$type"></text>
```

### Variable `$pad`

La variable `$pad` indica el nivel de necesidad de un pad o de un mando para poder utilizar dicho sistema. Se trata de un valor entre los siguientes:

- mandatory : obligatorio
- recommended : fuertemente recomendado
- optionnal : opcional, se puede no utilizar
- no : no requerido

Ejemplo:
 
 ```xml
 <image>
   <path path="./data/assets/pad-$pad.svg"></path>
 </image>
 ```

### Variable `$keyboard`

La variable `$keyboard` indica el nivel de necesidad de un teclado para poder utilizar dicho sistema. Se trata de un valor entre los siguientes:

- mandatory : obligatorio
- recommended : fuertemente recomendado
- optionnal : opcional, se puede no utilizar
- no : no requerido

```xml
<image>
  <path path="./data/assets/keyboard-$keyboard.svg"></path>
</image>
```


### Variable `$mouse`

La variable `$mouse` indica el nivel de necesidad de un ratón para poder utilizar dicho sistema. Se trata de un valor entre los siguientes:

- mandatory : obligatorio
- recommended : fuertemente recomendado
- optionnal : opcional, se puede no utilizar
- no :no requerido

Ejemplo: 

```xml
<image>
  <path path="./data/assets/mouse-$mouse.svg"></path>
</image>
```


### Variable `$releaseyear`

La variable `$releaseyear` indica el año de lanzamiento del sistema.

Ejemplo:
 
```xml
 <text name="date" extra="true">
   <text>Date de sortie du système : $releaseyear</text>
 </text>
```


### Variable `$netplay`

La variable `$netplay` indica mediante `yes` o `no` si el sistema está compatible con el Netplay.

Ejemplo:

```xml
 <text name="netplay" extra="true">
   <text>Netplay disponible pour le système : $netplay</text>
 </text>
```


### Variable `$lightgun`

La variable `$lightgun` indica mediante `yes` o `no` si el sistema tiene juegos jugables mediante el lightgun.

 ```xml
 <text name="lightgun" extra="true">
   <text>Jeu lightgun : $lightgun</text>
 </text>
 ```


### Variable `$crt`

La variable `$crt` indica mediante `yes` o `no` si un adaptador CRT está activo.

 ```xml
 <text name="crt" extra="true">
   <text>CRT actif : $crt</text>
 </text>
 ```


### Variable `$jamma`

La variable `$jamma` indica mediante `yes` o `no` si un adaptador Jamma está activo.

 ```xml
 <text name="jamma" extra="true">
   <text>Jamma actif : $jamma</text>
 </text>
 ```


### Variable `$tate`

La variable `$tate` indica mediante `yes` o `no` si la pantalla está girada a 90° o -90°.

 ```xml
 <text name="tate" extra="true">
   <text>Tate actif : $tate</text>
 </text>
 ```


### Variable `$overscan`

La variable `$overscan` indique par `yes` ou `no` si un adaptateur CRT sans adaptateur Jamma est actif ou pas.

 ```xml
 <text name="overscan" extra="true">
   <text>Overscan actif : $overscan</text>
 </text>
 ```

### Variable `$resolution`

La variable `$resolution` almacena determinadas informaciones relacionadas con la resolución de la pantalla.

Lista de posibles valores:

- `qvga` : inferior o igual a 288 pixels de altura.
- `vga` : inferior o igual a 576 pixels de altura.
- `hd` : inferior o igual a 920 pixels de altura.
- `fhd` : superior a 920 pixels de altura.

 
 ```xml
 <text name="resolution" extra="true">
   <text>Résolution : $resolution</text>
 </text>
 ```
