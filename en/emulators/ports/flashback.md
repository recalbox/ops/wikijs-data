---
title: Flashback
description: Flashback - The Quest for Identity
published: true
date: 2021-08-08T23:51:02.296Z
tags: ports, flashback
editor: markdown
dateCreated: 2021-06-29T18:43:15.272Z
---

![](/emulators/ports/flashback.jpg){.align-center}

## Technical data

* **Created by**: Delphine Software International
* **Year of release**: 1992
* **Genre**: Platformer
* **Plateform(s)**: Amiga, Acorn Archimedes, Mega Drive/Genesis, MS-DOS, NEC PC-9801, Super NES, Sega CD, FM Towns, 3DO, CD-i, Atari Jaguar, Mac OS, iPhone OS, Symbian, Maemo, Dreamcast, Nintendo Switch, PlayStation 4.
* **Designer**: Paul Cuisset
* **Compositors**: Jean Baudlot, Fabrice Visserot, Raphaël Gesqua (version Amiga)
* **PEGI**: 12

## Synopsis

![](/emulators/ports/flashback_01.jpg)

Year 2142. Eminent scientist, Conrad B. Hart, struck by amnesia, is in the heart of the jungle when he regains consciousness. Not far from him is a curious box generating a holographic image representing him. The latter asks him to go to New Washington, where a certain Ian is waiting for him. On his way to the city, Conrad is unaware that aliens have infiltrated the population and that he will have to thwart an incredible interplanetary plot while trying to find the holocubes containing fragments of his memory...

This scenario is reminiscent of Philip K. Dick's short story We Can Remember it for You Wholesale, published in 1966 and adapted for the cinema in 1990 by Paul Verhoeven under the title Total Recall, just two years before the release of Flashback.

## Emulators

[Libretro REminiscence](libretro-reminiscence)