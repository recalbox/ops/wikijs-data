---
title: Añadir músicas personalizadas a interfaz
description: 
published: true
date: 2024-07-26T07:22:03.729Z
tags: emulationstation, musica, personalizada
editor: markdown
dateCreated: 2023-09-15T09:09:55.703Z
---

Podéis añadir vuestras propias músicas a interfaz.

## Procedimiento

* Debéis poder acceder a vuestra Recalbox:
  * Vía [WinSCP](./../system/access/network-access-winscp)
  * Vía [Cyberduck](./../system/access/network-access-cyberduck)
  * Vía [MobaXTerm](./../system/access/network-access-mobaxterm)
* Una vez conectado, depositad vuestra música en el directorio `/recalbox/share/music`.
* Una vez hecho esto, vuestra música se reproducirá aleatoriamente en interfaz sin necesidad de reiniciar.

## Ver también

Poner vuestras músicas favoritas en la interfaz de interfaz [siguiendo este tutorial paso a paso de Fabrice](https://www.youtube.com/watch?v=uu40MDzBdJM).

![https://www.youtube.com/watch?v=uu40MDzBdJM](/tutorials/frontend-customization/add-music-into-emulationstation/changer-la-musique-sur-es.png)

![Rendez-vous 2:42 pour les explications.](/tutorials/frontend-customization/add-music-into-emulationstation/ajouter-des-musiques-sur-emulationstation.png)