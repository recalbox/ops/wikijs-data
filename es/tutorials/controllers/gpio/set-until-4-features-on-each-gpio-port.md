---
title: Atribuir hasta 4 funciones en cada puerto GPIO
description: 
published: true
date: 2023-09-18T19:32:52.541Z
tags: gpio, funciones
editor: markdown
dateCreated: 2023-09-18T19:32:52.541Z
---

Si queréis construir vuestra propia máquina recreativa con un Raspberry Pi, podéis añadir botones adicionales y asignarlos a funciones especiales. Así es como funciona.

## Acerca de los puertos GPIO del Raspberry Pi

En un Raspberry Pi, existen varios puertos GPIO disponibles, desde el GPIO2 hasta el GPIO26 :

![GPIO](/tutorials/controllers/gpio/gpio-functions/pi-gpio.png)

Cuando queréis conectar un botón, hay que conectarlo entre un puerto GPIOxx y un puerto GND. ¡Tened en cuenta que se usan números GPIO, no números PIN (1 a 40)!

## Cómo instalarlo en Recalbox

1. Añade esto al fichero `/recalbox/share/system/recalbox.conf`:

```ini
# Uncomment to enable custom GPIO script feature
system.rpi.gpio=enable
```

2. Modificad el fichero `/recalbox/share/system/configs/retroarch/retroarchcustom.cfg` y añadid esta línea:

```ini
network_cmd_enable = true
```
3. Configurad el fichero `/recalbox/share/system/configs/rpi-gpio.ini` (consultad los comandos disponibles aquí abajo).

4. Reiniciad Recalbox y disfrutad !

## Sintaxis del fichero rpi-gpio.ini

Debéis comenzar por el [GPIOxx] reemplazando `xx` por el número de GPIO en cuestión.
Añadid a continuación estas cuatro líneas:

```ini
quick=
standard=
hold=
release=
```

`quick` es una acción que se ejecutará si pulsáis y soltáis el botón rápidamente.
`standard` es una acción que se ejecutará si mantenéis pulsado y soltáis el botón durante aproximadamente 1 segundo. 
`hold` es una acción que se ejecutará si mantenéis pulsado el botón durante mucho tiempo (más de 2 segundos).
`release` es una acción que se ejecutará si soltáis el botón después de un evento `hold`.

Todas las acciones son opcionales. Si no queréis atribuir ninguna función a alguna de ellas, dejad simplemente un espacio en blanco depués de `=`

Ejemplo:

```ini
[GPIO26]
quick=VOLUP
standard=VOLDOWN
hold=VOLMUTE
release=
[GPIO16]
quick=RESET
standard=QUIT
hold=
release=
```

## Acciones disponibles

### Acciones globales

* VOLUP: Subir el volumen
* VOLDOWN: Bajar el volumen
* VOLMUTE: Silenciar / Desilenciar el volumen
* QUIT: Salir del emulador actual y volver a EmulationStation.

### Acciones específicas para los emuladores RetroArch

* CHEAT_INDEX_MINUS
* CHEAT_INDEX_PLUS
* CHEAT_TOGGLE
* DISK_EJECT_TOGGLE
* DISK_NEXT
* DISK_PREV
* FAST_FORWARD
* FAST_FORWARD_HOLD
* FRAMEADVANCE
* FULLSCREEN_TOGGLE
* GRAB_MOUSE_TOGGLE
* LOAD_STATE
* MENU_TOGGLE
* MOVIE_RECORD_TOGGLE
* NETPLAY_FLIP
* OVERLAY_NEXT
* PAUSE_TOGGLE
* RESET
* REWIND
* SAVE_STATE
* SCREENSHOT
* SHADER_NEXT
* SHADER_PREV
* SLOWMOTION
* STATE_SLOT_MINUS
* STATE_SLOT_PLUS