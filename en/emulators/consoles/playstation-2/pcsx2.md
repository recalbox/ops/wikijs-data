---
title: PCSX2
description: 
published: false
date: 2021-11-09T20:23:56.081Z
tags: playstation 2, ps2, pcsx2, 8.0+
editor: markdown
dateCreated: 2021-08-02T18:15:32.332Z
---



## ![](/emulators/license.svg) License

This core is under [**GPLv2**](https://github.com/PCSX2/pcsx2/blob/master/COPYING.GPLv2) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ❌ | ❌ | ❌ | ❌ | ❌ | ❌ | ❌ | ✅ |

## ![](/emulators/features.png) Features



## ![](/emulators/bios.svg) BIOS

### List of mandatory bios

| Filename | Description | MD5 | Provided |
| :---: | :---: | :---: | :---: |
| ps2-0230a-20080220.bin | SCPH-90001/SCPH-90010 (Version 5.0 02/20/08 A) | 21038400dc633070a78ad53090c53017 | ❌ |
| ps2-0230e-20080220.bin | SCPH-90002/SCPH-90003/SCPH-90004/SCPH-90008 (Version 5.0 02/20/08 E) | dc69f0643a3030aaa4797501b483d6c4 | ❌ |
| ps2-0230h-20080220.bin | SCPH-90005/SCPH-90006/SCPH-90007 (Version 5.0 02/20/08 J) | 30d56e79d89fbddf10938fa67fe3f34e | ❌ |
| ps2-0230j-20080220.bin | SCPH-90000 (Version 5.0 02/20/08 J) | 80ac46fa7e77b8ab4366e86948e54f83 | ❌ |

### Location

Put the bios like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 ps2
┃ ┃ ┃ ┃ ┣ 🗒 **ps2-0230a-20080220.bin**
┃ ┃ ┃ ┃ ┣ 🗒 **ps2-0230e-20080220.bin**
┃ ┃ ┃ ┃ ┣ 🗒 **ps2-0230h-20080220.bin**
┃ ┃ ┃ ┃ ┣ 🗒 **ps2-0230j-20080220.bin**
┃ ┃ ┃ ┃ ┣ 🗒 **ps2-0250e-20100415.bin**

## ![](/emulators/isos.svg) Isos

### Supported extensions

Isos must have the extension:

* .bin
* .chd
* .cso
* .gz
* .iso
* .img
* .mdf
* .nrg

### Location

Put the isos like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 ps2
┃ ┃ ┃ ┃ ┣ 🗒 **game.iso**

>The isos using **Redump** format are highly recommended.
{.is-success}

>For more information about isos, go to [this tutorial](./../../../tutorials/games/generalities/isos-and-roms)!
{.is-info}

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/pcsx2/pcsx2/](https://github.com/pcsx2/pcsx2/)