---
title: Add Amazon Prime add-on
description: 
published: true
date: 2024-09-19T09:03:23.035Z
tags: kodi, amazon, add-ons
editor: markdown
dateCreated: 2022-12-27T21:48:15.854Z
---

## Introduction

There is an add-on for Kodi to play Amazon Prime videos. This tutorial describes how to install it.

>The add-on is not currently compatible with the Raspberry Pi 4 and 5 because the WideVine video module for 64-bit ARM systems does not yet exist.
>{.is-info}

## Prerequisites

You must check several points:

1. Do not have a double authentication enabled on Amazon. Most of the time, login via Kodi does not work because of this extra request.

2. Enable installation from a zip file in Kodi:
    * Go to `Settings` > `System` > `Add-ons`.
    * Enable the `Unknown sources` option. Confirm when prompted.

## Installation

1. Start Kodi.

2. You should first install a zip file containing the repository information where the Amazon Prime add-on is located. Go to the following address and download the latest `Plugin Repository`:

https://github.com/Sandmann79/xbmc/releases

3. Transfer the downloaded zip file to your Recalbox in the desired location in the share.

4. In Kodi, go to `Add-ons` > `Install from Zip file` and select the zip file you downloaded. Confirm the confirmation messages if you have any.



5. Once installed, still in `Add-ons`, go to `Install from a repository`.



6. Find the add-on in `Sandmann79s Repository` > `Video add-ons` > `Amazon VOD`. Open the add-on to install it. Confirm all necessary dependencies to install it.



7. Once installed, the add-on should be visible in the list of installed add-ons.



## Usage

You will need to log in before you can use the add-on.

1. Open the add-on.

2. You will automatically have the options window open. On the left, move to `Connection` on the left and select `Sign In...`.



3. You will be asked to fill in your email address and password in two windows in a row.

>You will probably get windows where you have to retype your password and a captcha. This is quite normal, Amazon does its checks, even if they can be too repetitive.
>{.is-info}

4. Once you are logged-in, you will be presented with a window indicating the login process is done.



5. Now you can enjoy Amazon Prime.