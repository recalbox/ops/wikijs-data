---
title: Où trouver des roms 100% légales
description: 
published: true
date: 2022-05-19T12:25:55.625Z
tags: roms, trouver, légales
editor: markdown
dateCreated: 2021-05-21T08:38:03.148Z
---

## Introduction

Cette page vise à recenser les différents moyens "100% légaux", pour se procurer des fichiers roms pour les différentes plateformes supportées par Recalbox.

## Atari2600

* Sur Steam, les roms Atari 2600 peuvent être extraites du jeu **Atari Vault**. Il y a 100 jeux Atari 2600 dans ce pack qui coûte **9,99€**. Voici le [lien vers la page Steam du jeu](https://store.steampowered.com/app/400020/Atari_Vault/).

## Daphne

* Certains jeux peuvent être directement et légalement téléchargés par l'outil **DaphneLoader** de la distribution Windows de Daphne.
* Pour d'autres (Dragon's Lair, Dragon's Lair II, Space Ace, ...), vous devez prouver que vous possédez une licence valide du jeu (version DVD par exemple).

## DOS

* Sur GOG.com (Good Old Games, une plateforme de téléchargement de jeux PC sans DRM), il est possible d'acheter des jeux MS-DOS comme Disney's Aladdin, Disney's Lion King, Disney's Jungle Book, ...  Ces jeux tournent sous dosbox et sont donc compatibles avec Recalbox.
* Sur le site « Legacy » de 3D Realms (anciennement Apogee), il est possible de télécharger certains de leurs anciens titres. La plupart sont proposés en version freeware et certains ne le sont qu’en version shareware. Il y a des titres assez intéressants comme **Alien Carnage (Halloween Harry)**. La liste des jeux téléchargeables se trouve [ici](http://legacy.3drealms.com/downloads.html). Ces jeux tournent sous DOSbox et sont donc compatibles avec Recalbox. 
* Blizzard offre en libre téléchargement le jeu **The Lost Vikings (USA)** sur [son site](https://us.battle.net/account/download/?show=classic).

## Master System

* Sur Steam, il est possible d'acheter le remake **"Wonder Boy: The Dragon's trap".** Une fois installé, le repertoire du jeu contient un sous dossier `bin_pc`, puis un autre sous-dossier `rom`, ce dernier contient un fichier `wb3.sms` qui est une rom correspondant au jeu Master System **Wonder boy III: The Dragon's Trap** (il ne correspond cependant pas à un jeu référencé par No-intro).

## Megadrive

* Sur Steam, il est possible d'acheter le jeu **SEGA Mega Drive & Genesis Classics**. Lors de l'installation, un répertoire `uncompressed roms` est crée. Il suffit alors de modifier l'extension des roms en .md ou .bin afin qu'elles soient reconnues par Recalbox. Le jeu coûte **29,99€** et contient 58 titres Megadrive + Sonic CD. La liste des jeux inclus est disponible sur la [page Steam du jeu](https://store.steampowered.com/sub/102625/?l=french), sur les 67 Roms présentes, 61 correspondent à des roms référencées par No-intro.
* Il existe des jeux "homebrews" créés par le Shiru et disponibles en téléchargement libre sur [son site](https://shiru.untergrund.net/software.shtml#genesis).

## NeoGeo

* Sur GOG.com (Good Old Games, une plateforme de téléchargement de jeux PC sans DRM), il est possible d'acheter **24 jeux Neo Geo 5,19€ par titre**. Voici le [lien vers tous les titres SNK disponibles sur GOG.com](https://www.gog.com/games?sort=popularity&search=snk&page=1). 
* Sur Humble Bundle, il est possible d’obtenir une compilation de 23 jeux pour **34,99€**. Les jeux sont également sans DRM. La liste des jeux disponibles est sur la [page du pack](https://www.humblebundle.com/store/neogeo-classic-complete-collection). Il est aussi possible d’acheter les titres **individuellement pour 5,99€**. Le pack contient les mêmes jeux que sur GOG.com à l'exception de The King of Fighters '98 qui doit être acheté séparé avec le pack "Unlimited Match".

## NES

* Il existe des jeux "homebrews" créés par le Shiru et disponibles en téléchargement libre sur [son site](https://shiru.untergrund.net/software.shtml#snes).
* Il y a également la possibilité d’acheter des bundle Capcom sur Steam (directement ou par Humble Bundle).

Depuis ces bundles, il est possible d’en extraire les roms .nes grâce à l’utilitaire [romextract](https://gitlab.com/vaiski/romextract/tree/master). Parmi les bundles proposés par Capcom depuis lesquels il est possible d’extraire les roms NES, il y a [Disney Afternoon Collection](https://www.humblebundle.com/store/the-disney-afternoon-collection) et [Megaman Legacy Collection (le premier pack uniquement)](https://www.humblebundle.com/store/mega-man-legacy-collection).

## ScummVM

* Il existe des jeux "freewares" disponibles en téléchargement libre sur le [site de ScummVM](https://www.scummvm.org/games/).

## SNES

* Il existe des jeux "homebrews" créés par le Shiru et disponibles en téléchargement libre sur [son site](https://shiru.untergrund.net/software.shtml#nes).
* Blizzard offre en libre téléchargement la rom du jeu Rock 'N Roll Racing (USA) sur [son site](https://us.battle.net/account/download/?show=classic).
* Au niveau de l’offre commerciale, sur Steam, le publieur [Piko Interactive](https://store.steampowered.com/publisher/Piko) propose certains titres du catalogue SNES. Ces jeux se présentent sous la forme d’un émulateur avec la rom du jeu. Ces roms sont aussi disponibles en [bundle](https://store.steampowered.com/bundle/5256/RETRO_Action_SNES_Volume_1/).
* Tout comme pour la NES, Capcom a publié le bundle [Megaman X Legacy Collection](https://www.humblebundle.com/store/mega-man-x-legacy-collection) achetable sur Steam (directement ou par Humble Bundle) qui contiennent les roms SNES. Ces roms peuvent être extraites avec l’utilitaire l’utilitaire [romextract](https://gitlab.com/vaiski/romextract/tree/master). Seules les roms du premier pack peuvent être extraites.