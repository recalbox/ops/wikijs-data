---
title: Script de ayuda para el soporte técnico
description: 
published: true
date: 2024-07-29T09:50:38.276Z
tags: script, logs, debug, soporte
editor: markdown
dateCreated: 2023-09-17T20:04:59.118Z
---

Un script llamado `recalbox-support.sh` ha sido añadido para ayudar a los desarrolladores con los problemas que puedas encontrar con tus periféricos.

Este archivo proporciona la siguiente información sobre:

* joystick
* kodi
* lirc
* system (lsmod, lsusb, tv service, es_settings, recalbox.conf, recalbox.log, config, df, etc...)

## Requisitos

Para que el archivo de soporte sea útil, debes reproducir tu problema con el [modo debug activado](../troubleshooting-information/enable-debug-mode). No es necesario realizar el paso después del reinicio. Después de haber activado el modo debug y haber reiniciado, intenta reproducir tu error.

## Uso

Su uso se realiza a través del Web manager.

* Dirígete al [Web manager](./../../basic-usage/features/webmanager).
* Haz clic en el engranaje en la parte inferior derecha (en la parte superior derecha en móvil).
* Haz clic en `Archivo de soporte`.

Después de un momento, se te proporcionará un enlace de descarga. Descarga el archivo y proporciona el archivo descargado en un mensaje dirigido a la persona que te lo solicitó.

>No olvides [desactivar el modo debug](../troubleshooting-information/enable-debug-mode) después de enviar el archivo de soporte.
{.is-info}
