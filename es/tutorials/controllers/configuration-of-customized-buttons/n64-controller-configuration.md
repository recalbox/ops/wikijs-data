---
title: Configuración de mandos N64
description: 
published: true
date: 2023-09-21T14:55:19.516Z
tags: n64, configuración, mando, mandos, controlador
editor: markdown
dateCreated: 2023-09-18T11:24:35.847Z
---

Para poder utilizar un mando N64 con Recalbox, hay que utilizar el comando `sdl2-jstest`.

### Añadir la configuración de vuestro controlador a Recalbox:

Para configurar el mando, necesitaremos varias informaciones que se extraen con el siguiente comando:

```shell
sdl2-jstest -l
```

>Podéis obtener más detalles sobre el comando `sdl2-jstest` en [esta página](./../../../tutorials/controllers/test-configuration/test-your-joystick-with-sdl2-jstest).
{.is-info}

En nuestro ejemplo, obtenemos lo siguiente:

```shell
Joystick (Broadcom Bluetooth Wireless  Joystick                        ) has 6 axes (X, Y, Z, Rz, Hat0X, Hat0Y)
and 12 buttons (Trigger, ThumbBtn, ThumbBtn2, TopBtn, TopBtn2, PinkieBtn, BaseBtn, BaseBtn2, BaseBtn3, BaseBtn4, BaseBtn5, BaseBtn6).
```

Una vez que hemos identificado el nombre del mando y cada uno de sus botones, hay que modificar el fichero `/usr/share/mupen64plus/InputAutoCfg.ini`. Para ello, utilizad este comando:

```shell
nano /usr/share/mupen64plus/InputAutoCfg.ini
```

Si el fichero no existía antes, se creará.

Al final de este fichero, añadid las informaciones de configuración como aquí abajo:

>El modelo que se presenta aquí es un ejemplo, tomado a partir de un mando concreto (nombre del mando, número del botón etc...). No lo podéis copiar tal cual, debéis adaptarlo con las informaciones detectadas de vuestro propio controlador.
{.is-info}

```ini
[Broadcom Bluetooth Wireless  Joystick                        ]
plugged = True
plugin = 2
mouse = False
AnalogDeadzone = 4096,4096
AnalogPeak = 32768,32768
DPad R = hat(0 Right)
DPad L = hat(0 Left)
DPad D = hat(0 Down)
DPad U = hat(0 Up)
Start = button(9)
Z Trig = button(7)
B Button = button(2)
A Button = button(1)
C Button R = axis(3+)
C Button L = axis(3-)
C Button D = axis(2+)
C Button U = axis(2-)
R Trig = button(5)
L Trig = button(4)
Mempak switch = button(6)
Rumblepak switch = 
X Axis = axis(0-,0+)
Y Axis = axis(1-,1+)
```

Cuando la configuración sea correcta, pulsad `Ctrl + X` para salir de nano, aceptad sobrescribir el archivo con `Y` y pulsad `Enter` para salir. Ahora podéis iniciar un juego de N64 y probar su configuración.

La configuración es correcta, ¿os gusta? Genial ^^ Pero hay un "problema". Cuando actulizaréis Recalbox, todos estos archivos de configuración también serán actualizados. Así que se perderéis las modificaciones. Si no queréis repetir todo esto después de cada actualización, haced una copia de seguridad del fichero `InputAutoCfg.ini`.

También es posible añadir vuestra configuración al código fuente de Recalbox. Publicad vuestra propia configuración como un comentario en nuestro GitHub. Nosotros la añadiremos al sistema en una futura actualización de Recalbox.

### Añadir órdenes especiales

Mupen64plus no admite combinaciones de botones, como los emuladores RetroArch, para lanzar un comando especial. Pero podéis asignar botones no utilizados a un comando específico, como guardar/cargar una grabación instantánea, cambiar las ubicaciones de guardado, etc...

Para ello, primero hay que identificar todos los botones no utilizados de vuestra configuración. Como se mencionó anteriormente, conectaros a Recalbox vía [SSH](./../../../tutorials/system/access/root-access-terminal-cli) y utilizad el comando `sdl2-jstest`, anotad el número de código de todos los botones que no se utilizan.

>Encontraréis más detalles sobre el funcionamiento del comando `sdl2-jstest` en [esta página](./../../../tutorials/controllers/test-configuration/test-your-joystick-with-sdl2-jstest).
{.is-info}

Una vez hecho esto, todavía dentro de la sesión de SSH, modificad el fichero `mupen64plus.cfg` con el siguiente comando :

```shell
nano /recalbox/configs/mupen64/mupen64plus.cfg
```

Dirigiros a la sección titulada `[CoreEvents]`. El buen punto es aquí:

```ini
# Joystick event string for stopping the emulator
Joy Mapping Stop = ""
# Joystick event string for switching between fullscreen/windowed modes
Joy Mapping Fullscreen = ""
# Joystick event string for saving the emulator state
Joy Mapping Save State = ""
# Joystick event string for loading the emulator state
Joy Mapping Load State = ""
# Joystick event string for advancing the save state slot
Joy Mapping Increment Slot = ""
# Joystick event string for taking a screenshot
Joy Mapping Screenshot = ""
# Joystick event string for pausing the emulator
Joy Mapping Pause = ""
# Joystick event string for muting/unmuting the sound
Joy Mapping Mute = ""
# Joystick event string for increasing the volume
Joy Mapping Increase Volume = ""
# Joystick event string for decreasing the volume
Joy Mapping Decrease Volume = ""
# Joystick event string for fast-forward
Joy Mapping Fast Forward = ""
# Joystick event string for pressing the game shark button
Joy Mapping Gameshark = ""
```

Hay que tener en cuenta que Mupen64plus identifica el `player 1` como `J0`, `player 2` como `J1` etc. De esta forma, si por ejemplo, queréis afectar el botón número 10 del jugador 1 al comando "_save savestates_", tendréis que editar el fichero anterior como sigue:

```ini
Joy Mapping Save State = "J0B10"
```

Y si queréis mapear el botón 5 del jugador 2 al comando "_load savestates_", deberéis editar el fichero anterior con la siguiente línea:

```ini
Joy Mapping Load State = "J1B5"
```

Si vuestra configuración es correcta, pulsad `Ctrl + X` para salir de nano, aceptad la sobreescritura del fichero con `Y` y pulsad `Enter` para salir.
