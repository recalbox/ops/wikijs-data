---
title: Guides
description: 
published: true
date: 2024-09-18T12:45:49.518Z
tags: guides
editor: markdown
dateCreated: 2021-08-07T15:15:51.508Z
---

Here you will find several guides to some of the systems to help you use them better.

Here are the systems available:

[32X](32x)
[Amiga](amiga)
[Atari Jaguar](atari-jaguar)
[Atari ST](atari-st)
[BBC Micro](bbc-micro)
[DOS](dos)
[Daphne](daphne)
[Dragon32/64](dragon32-64)
[Dreamcast](dreamcast)
[Family Computer Disk System](family-computer-disk-system)
[FinalBurn Neo](fbneo)
[Game Boy](game-boy)
[GameCube / Wii](gamecube-wii)
[MAME](mame)
[Macintosh](macintosh)
[Model 3](model-3)
[Neo-Geo](neo-geo)
[Neo-Geo CD](neo-geo-cd)
[Nintendo 64](nintendo-64)
[Nintendo 64DD](64-dd)
[Playstation 1](playstation-1)
[Playstation 2](playstation-2)
[ScummVM](scummvm)
[Super Nintendo](super-nintendo)
[TI-99/4A](ti-99-4a)
[TRS-80 Color Computer](trs-80-coco)
[Thomson MO/TO](thomson-mo-to)
[Tic-80](tic-80)
[Virtual Boy](virtualboy)
[ZX Spectrum](zx-spectrum)