---
title: LowRes NX
description: 
published: false
date: 2021-11-09T20:04:20.433Z
tags: fantasy, lowres-nx, 8.0+
editor: markdown
dateCreated: 2021-09-16T04:24:39.640Z
---

![](/emulators/fantasy/lowresnx.svg){.align-center}

## Technical data

* **Developer**: Timo Kloss
* **Year of release**: 2017
* **Display**: 160x128 pixels, 60 Hz, 8 dynamic 6-bit palettes with 4 colours each
* **Sprites**: 64, max 32x32 pixels
* **Sound**: 4 voices, saw/tri/pulse/noise, pulse width, volume, ADSR, LFO

## Presentation

LowRes NX was inspired by real 8- and 16-bit systems and simulates chips for graphics, sound, and I/O that actually work like real hardware. It supports hardware sprites as well as hardware parallax scrolling, and even features vertical white and frame breaks to create authentic retro effects.

## Emulators

[Libretro LowResNX](libretro-lowresnx)