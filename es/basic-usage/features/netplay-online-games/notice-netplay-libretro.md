---
title: Aviso Netplay Libretro
description: 
published: true
date: 2023-08-17T15:06:46.096Z
tags: libretro, netplay
editor: markdown
dateCreated: 2023-08-17T15:06:46.096Z
---

## La sala de espera Netplay

El "lobby" (o vestíbulo o sala de espera en español), es un lugar que réune todos aquellos que utilizan la funcionalidad de netplay. Para acceder existen dos posibilidades.

* Como anfitrión, podéis crear vuestra propia partida online para que las otras personas se puedan conectar.
* Como cliente, podéis uniros a un juego online ya lanzado.

Cuando os unís a una partida online, podéis elegir entre acceder como jugador o simplemente como espectador (streaming).

### Configuración

Por defecto el netplay no está activado. Para activarlo y para personalizarlo, desde el menu principal de la lista de sistemas, seleccionad `Start` > `Opciones de juegos` > `Opciones de netplay`.

Las opciones son las siguientes:

* `NETPLAY (JUEGO EN RED)` : para activar el netplay
* `NICKNAME` : vuestro pseudo dentro del lobby de netplay

>No utilicéis caracteres acentuados, podría impedir el buen funcionamiento de netplay.
>{.is-info}

* `PUERTO`: el puerto TCP necesario para la conexión. Por defecto es el 55435.

>Si el puerto por defecto no os conviene o no está disponible en vuestro router, debéis modificarlo aquí y crear una redirección de puertos en vuestro router de internet.
>{.is-info}

Una vez rellenadas estas opciones, estáis listos para probar una partida online de netplay.

### Lanzar un juego en modo netplay (anfitrión)

* Situaros en la lista de juegos y apretad el botón `X` sobre el juego que queréis lanzar en modo netplay.
* Aparecerá una ventana con un menú de contraseñas.

![](/basic-usage/features/netplay/libretro-netplay1.png)

Aquí están las opciones que se proponen:

* `CONTRASEÑA OBLIGATORIA PARA JUGADORES`: seleccionad ON si queréis configurar una contraseña para acceder a vuestra partida.

	* Las personas que intenten conectarse como jugador necesitarán conocer esta contraseña.
  
* `CONTRASEÑA OBLIGATORIA PARA ESPECTADORES`: seleccionad ON si queréis configurar una contraseña para acceder al juego como espectador.

  * Las personas que intenten conectarse como espectadores deberán introducir esta contraseña.
  
* `CONTRASEÑA JUGADORES`: una lista de contraseñas predefinidas.

	* Evidentemente, también podéis personalizar las contraseñas predefinidas en esta lista.

* `CONTRASEÑA ESPECTADORES`: una lista de contraseñas predefinidas.

	* Por supuesto, también podéis personalizar las contraseñas de esta lista.

Depués de haber elegido una option:

  * Seleccionad `ARRANCAR`: esto lanzará el juego.
	* Esperad que alguien se conecte a vuestro juego: un pop-up aparecerá en la pantalla de todos los usuarios de Recalbox conectados a internet para indicarles que una sesión de Netplay acaba de lanzarse y se encuentra disponible.

![](/basic-usage/features/netplay/libretro-netplay2.png)

### Conectarse a un juego online (cliente)

* Utilizad el botón `X` desde la lista de todos los sistemas. Si Kodi está activado, una nueva ventana aparecerá con un menú para seleccionar el lobby Netplay.

![](/basic-usage/features/netplay/libretro-netplay3.png)

* Una vez en el lobby de Netplay, seleccionad el juego al que os queréis conectar.

## Informaciones dentro del lobby

Para cada juego, el lobby de netplay presenta los siguientes datos:

### En la parte de la izquierda

* **Un signo** : ✅ (correcto) ou ❌ (incorrecto) delante del logo de Recalbox o delante del nombre de la rom.
* **El logo Recalbox** : indica si la persona utiliza Recalbox. Aparece entre el signo y el nombre de la rom.
* **El nombre de la rom**: El nombre exacto del fichero rom que contiene el juego. Aparece después del signo y el logo de Recalbox.

### En la parte de la derecha

#### Resultados básicos

* `NOMBRE`: nombre/nickname del jugador anfitrión
* `PAÍS`: codigo del país del anfitrión.
* `Hash de la rom`: firma electrónica del fichero rom
* `Fichero rom`: rom del anfitrión.
* `CORE`: emulador utilizado por el anfitrión
* `Ver. de Core` : versión del émulador
* `LATENCIA`: tiempo de respuesta.
* `Ver. de RA` : versión de RetroArch
* `Arch. anfitrión` : arquitectura del anfitrión (si el anfitrión utiliza Recalbox aparecerá el logo de Recalbox aquí)

Delante de cada elemento de la lista podéis observad el résultado de compatibilidad, ✅ (correcto) o ❌ (incorrecto).

#### Resultado global

El resultado de compatibilidad global aparece abajo a la derecha de la ventana y os indica las probabilidades de establecer una conexión correcta:

* **Verde** : disponéis de una rom equivalente con la misma firma digital que el anfitrión y utilizáis el mismo emulador. Si esto ocurre, deberíais poder jugar online sin problemas!

![](/basic-usage/features/netplay/netplay-lobby-ok.png)

* **Azul**: la firma digital no coincide (algunas rom no tienen firma, como los sistemas de arcade), pero el sistema ha encontrado una rom equivalente. Debería funcionar.

![](/basic-usage/features/netplay/netplay-ok-nok.png)

* **Rojo**: no se ha encontrado la rom en vuestro sistemas, o bien no disponéis del mismo emulador que el anfitrión. No hay ninguna probablidad de que podáis jugar online, no funcionará !

![](/basic-usage/features/netplay/netplay-nok.png)