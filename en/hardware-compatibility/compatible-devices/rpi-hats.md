---
title: Raspberry PI hats
description: List of compatible rpi hats
published: true
date: 2021-11-09T20:57:31.101Z
tags: rpi, hats, compatible, 8.0+
editor: markdown
dateCreated: 2021-08-11T19:34:21.399Z
---

# Recalbox compatible Raspberry PI HATs

## PoE controllers

PoE controllers allow the Raspberry PI to be powered through the ethernet connector. No need for a mini-USB or USB-C power supply!

To power the RPI, you need an ethernet switch that is IEEE 802.3af or IEEE 802.3at compatible.

### Waveshare PoE hat (b)

This hat provides ethernet power to the RPI and has a monochrome oled screen and a programmable DC fan (on/off). It is possible to force the activation of the fan by a switch on the card.

This hat is RPI 3b+ and RPI 4 compatible.

The [recalbox-wpaf](/en/advanced-usage/fan-controller) utility supports this hat and allows to control the fan (on/off depending on the CPU temperature).

### Raspberry PI PoE+

This hat allows the RPI to be powered by ethernet and has a variable speed fan (PWM).

This hat is compatible with RPI 3b+ and RPI4.

The [recalbox-wpaf](/en/advanced-usage/fan-controller) utility supports this hat and allows to control the fan (enable/disable depending on the CPU temperature).

## Boards with fan

This type of board has a fan to cool the CPU.

### Pimoroni fan SHIM

This board is attached to a part of the GPIO connector of the Raspberry PI. It has a silent fan, an RGB LED and a button. The fan function is supported by the [recalbox-wpaf](/en/advanced-usage/fan-controller) utility.
