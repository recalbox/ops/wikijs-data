---
title: Encodeurs USB
description: 
published: true
date: 2021-09-04T22:33:23.490Z
tags: usb, encodeurs
editor: markdown
dateCreated: 2021-05-21T08:06:58.960Z
---

Ici, vous pourrez trouver de l'aide pour configurer vos encodeurs USB.

Voici les tutoriels disponibles :

[Configuration des contrôleurs Xin-mo](xin-mo-controllers-configuration)
[Recalbox pour votre encodeur de clavier USB](recalbox-for-your-usb-keyboard-encoder)