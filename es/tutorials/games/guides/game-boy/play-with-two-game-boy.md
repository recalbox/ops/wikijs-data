---
title: Jugar a dobles al mismo juego en modo GameLink
description: 
published: true
date: 2023-09-20T07:28:43.634Z
tags: dos, gameboy, gamelink, 8.0+, jugadores, dobles
editor: markdown
dateCreated: 2023-09-20T07:09:19.346Z
---

## Introducción

Es posible jugar a dobles con dos GameBoys al mismo tiempo en Recalbox. 

>Aunque estaría muy bien, Recalbox no soporta el Netplay si se utiliza este modo
>
>Además, bajo el modo un-juego-dos-gameboys, las grabaciones no funcionan en la segunda GameBoy, a diferencia del modo [dos-juegos-dos-gameboys](./play-two-players-games-with-gamelink) (el cual tampoco sorporta el Netplay).
{.is-warning}

## Utilización

Desde Recalbox 8, las carpetas de roms `gb` y `gbc` poseen una subcarpeta llamada `gamelink`. Este directorio permite jugar a dobles con el mismo juego sin realizar ninguna configuración manual.

1. Colocad el juego que queréis dentro del directorio `gamelink`.
2. Actualizad la lista de juegos, y entrad dentro del sistema en cuestión.

![](/tutorials/games/guides/game-boy/play-two-players/samegametwoplayers1.png){.full-width}

3. En la lista de juegos del sistema, dirigíros dentro de `gamelink`, colocaros sobre el juego que queréis lanzar a dobles y pulsad `START`.

![](/tutorials/games/guides/game-boy/play-two-players/samegametwoplayers2.png){.full-width}

El juego se lanza y aparecerá en doble en la pantalla.

![](/tutorials/games/guides/game-boy/play-two-players/samegametwoplayers3.png){.full-width}

>Para jugar a dobles a dos juegos diferentes al mismo tiempo, hay que utilizar [la siguiente manipulation](./play-two-players-games-with-gamelink).
{.is-info}