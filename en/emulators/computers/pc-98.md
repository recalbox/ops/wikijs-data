---
title: PC-98
description: 
published: true
date: 2021-08-09T14:57:51.886Z
tags: computers, nec, pc-98
editor: markdown
dateCreated: 2021-07-28T21:18:59.222Z
---

![](/emulators/computers/pc98.svg){.align-center}

## Technical data

* **Manufacturer**: NEC Corporation
* **Year of release**: 1982
* **OS**: CP/M-86, MS-DOS, OS/2, Windows
* **CPU**: 8086 @ 5 MHz and higher
* **RAM**: 128 KB and higher
* **Sound chip**: Internal beeper, FM sound, PCM

## Presentation

The **PC-98** (from its full name **NEC PC-9801**) is a Japanese microcomputer created by NEC in 1982

The first version of the PC-98 used an Intel 8086 processor running at a clock rate of 5 Mhz and was equipped with 128 KB of RAM, upgradeable to 640 KB. The graphics resolution was up to 640 x 400 pixels with an 8-color display. Its successor, the PC-9801E, released in 1983, used an Intel 8086-2 processor, which could be run at either 5 Mhz or 8 Mhz.

During the 1980s and early 1990s, NEC dominated the Japanese personal computer market with over 60% of PC-98s and PC-88s sold. In 1990, IBM Japan launched its new operating system, DOS/V, which was able to display Japanese characters on ordinary IBM PCs. After that, PC-98 sales started to decline. The last successor of the line was the PC-9821Ra43 released in 2000 and based on a Celeron processor, running at a frequency of 433 Mhz.

Seiko Epson developed PC-98 clones with compatible peripherals.

The PC-98 had thousands of video games designed for it. Despite the fact that it was far inferior in hardware to Fujitsu's FM Towns or Sharp Corporation's Sharp X68000, the large installed base as well as the large number of video game titles (especially "Dōjin"-style dating simulations or role-playing games) brought it into favor with many Japanese developers, before the advent of clones supporting DOS/V. The first five games of the Touhou series, known to have been developed solely by ZUN, were released on PC-98.

As a result, the PC-9801 can be seen as the Japanese equivalent of the IBM PC, except that NEC kept the hardware and the platform under a proprietary license until the end.

## Emulators

[Libretro NP2Kai](libretro-np2kai)