---
title: Amiberry
description: 
published: true
date: 2024-07-14T19:24:10.273Z
tags: amiga-1200, amiga, 1200, amiberry
editor: markdown
dateCreated: 2023-09-11T07:17:21.409Z
---

**Amiberry** es un core ARM optimizado para emular el Amiga.

## ![](/emulators/license.svg) Licencia

El core posee la siguiente licencia [**GPLv3**](https://github.com/midwan/amiberry/blob/master/COPYING).

## ![](/emulators/compatibility.png) Compatibilidad

 | RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | 
 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ❌ | ✅ |

## ![](/emulators/features.png) Funcionalidades



## ![](/emulators/bios.svg) BIOS

### Lista de BIOS obligatorias

Encontraréis los ficheros de bios con el nombre indicado en la columna **Descripción**. Tendréis que renombrarlos para que tengan el nombre indicado en la columna **Nombre del fichero**.

| Nombre del fichero | Descripción | MD5 | ¿Preinstalada? |
| :---: | :---: | :---: | :---: |
| kick39106.A1200 | Kickstart v3.0 rev 39.106 (1992)(Commodore)(A1200)\[!\].rom | b7cc148386aa631136f510cd29e42fc3 | ❌ |
| kick40068.A1200 | Kickstart v3.1 r40.068 (1993-12)(Commodore)(A1200)\[!\].rom | 646773759326fbac3b2311fd8c8793ee | ❌ |
| kick40068.A4000 | Kickstart v3.1 r40.068 (1993-12)(Commodore)(A4000)\[!\].rom | 9bdedde6a4f33555b4a270c8ca53297d | ❌ |

### Emplazamiento

Disponed vuestras bios como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 🗒 **kick39106.A1200**
┃ ┃ ┃ ┣ 🗒 **kick40068.A1200**
┃ ┃ ┃ ┣ 🗒 **kick40068.A4000**

## ![](/emulators/roms.png) Roms

>La emulación Amiga se presenta en dos formas diferentes: Amiga 600 et Amiga 1200.
>Algunos juegos pueden ser caprichosos no dudéis a probar los dos sistemas si un juego no funciona.
>Los juegos se pueden utilizar con un formato de disco ADF o en formato WHDLoad, utilizando en este último caso un fichero UAE adicional.
{.is-info}

### Extensiones de roms soportadas

Las roms deben tener las extensiones siguientes:

* .adf
* .ipf
* .rp9
* .whd
* .zip

>Los otros formatos de imágenes de disco __pueden__ estar soportados pero no han sido probados por el equipo de Recalbox.
>
>Encontraréis más información sobre estos formatos en [esta página](./../../../tutorials/games/guides/amiga/faq).
{.is-info}

### Emplazamiento

Disponed vuestras roms como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 amiga600
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.bin**
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.lst**

>Se aconsejan las roms del proyecto **TOSEC**.
{.is-success}

>Para más información sobre las roms, consultad [este tutorial](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

>Para conservar las configuraciones personalizadas cuando se produce una actualización del sistema Recalbox, os aconsejamos utilizar las [Sobrecargas de configuración](./../../../advanced-usage/configuration-override).
{.is-info}

## ![](/emulators/external-links.png) Enlaces externos

* **Código fuente utilizado** : [https://gitlab.com/recalbox/packages/standalone/amiberry/](https://gitlab.com/recalbox/packages/standalone/amiberry)
* **Página oficial** : [https://blitterstudio.com/amiberry/](https://blitterstudio.com/amiberry/)