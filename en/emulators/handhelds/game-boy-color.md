---
title: Game Boy Color
description: 
published: true
date: 2021-08-09T12:46:42.383Z
tags: color, gameboy, handhelds, gbc
editor: markdown
dateCreated: 2021-07-06T00:16:59.151Z
---

![](/emulators/handheld/gameboycolor-us.svg){.align-center}

## Technical data

* **Manufacturer**: Nintendo
* **Year of release**: 1998
* **Units sold**: 118.69 millions
* **Best-selling game**: Pokemon Gold and Silver
* **CPU**: 8-bit Zilog Z80 @ 8.33 MHz
* **RAM**: 32kB
* **VRAM**: 26kB
* **Video**: PPU (embedded in CPU)
* **Resolution**: 160 x 144 pixels, 56 colors (32768 colors palette)
* **Sound**: 4 channels stereo sound
* **Cart size**: 256kB - 8MB

## Presentation

The **Game Boy Color**, is the portable video game console that succeeds the Game Boy. Created by Nintendo, it incorporates a color screen only slightly larger than the Game Boy's. However, its processor is twice as fast and its memory twice as large. It is backwards compatible with all first generation Game Boy games.

The Game Boy Color is available in different shells: translucent, purple, blue, green, fuchsia, yellow, translucent purple. Limited editions were also published: translucent black, translucent green, midnight blue, translucent blue, light orange, green and gold and a dozen other specific variants, as well as 7 "Pokémon" versions.

It was released in late 1998 in Japan, the United States, Europe and Australia.

Since the release of the Game Boy, Nintendo had concentrated on games and had not released a real new generation (the Pocket and Light models were only evolutions of the Game Boy). It was in 1998 that a second generation was considered, which would coincide with the tenth anniversary of the first model. But in reality, this console was mainly created to prevent **Bandai** from taking over the monopoly of portable gaming with its WonderSwan, because in the meantime, Nintendo Research & Development 1 (R&D1) was already working on the Game Boy Advance. The main expectation of the players was to have a color screen, as it was done on the competing machines for a long time (Game Gear, Atari Lynx...).

## Emulators

[Libretro Gambatte](libretro-gambatte)
[Libretro Mesen_S](libretro-mesen_s)
[Libretro mGBA](libretro-mgba)
[Libretro SameBoy](libretro-sameboy)
[Libretro TGBDual](libretro-tgbdual)