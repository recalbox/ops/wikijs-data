---
title: Atari 5200
description: 
published: true
date: 2022-07-22T10:09:08.429Z
tags: consoles, atari, 5200, atari-5200
editor: markdown
dateCreated: 2021-07-27T16:53:53.215Z
---

![](/emulators/consoles/atari5200.svg){.align-center}

## Technical data

* **Manufacturer**: Atari Corporation
* **Year of release**: 1982
* **Units sold**: 1 million
* **CPU**: MOS Technology 6502C @ 1.79MHz
* **RAM**: 16 kB
* **ROM**: 2KB on-board BIOS; 32 KB window for standard game cartridges
* **Sound**: 4-channel POKEY chip
* **Display**: 320x192 pixels with 256 colors

## Presentation

The Atari 5200 is essentially an Atari 400 without a keyboard. This proves that Atari's designs can quickly adapt to the market.

The console brings a revolutionary new joystick with an analog stick and function keys (start, pause, reset). Other innovations include automatic firing and four joystick ports.

The non-centering design of the joystick proved to be without advantage and unreliable, annoying many buyers. While the 5200 gained a large following thanks to a high-quality game library, it faced new competition from the [**Colecovision**](./colecovision) and an economy that was becoming increasingly shaky.

The question of which system would be superior to the other became irrelevant when the game market collapsed in 1983-1984, killing both consoles in their perfection.

## Emulators

[Libretro A5200](libretro-a5200)
[Libretro Atari800](libretro-atari800)