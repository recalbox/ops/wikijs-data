---
title: Jugar a dobles a dos juegos diferentes en modo GameLink
description: 
published: true
date: 2023-09-20T07:41:46.972Z
tags: dos, gameboy, gamelink, jugadores, dobles
editor: markdown
dateCreated: 2023-09-20T07:13:30.935Z
---

TGBDual permite jugar con la Game Boy y la Game Boy Color simulando un cable GameLink para jugar a dobles. Sin embargo, si queréis utilizar dos juegos diferentes, hay que aplicar unas pequeñas configuraciones extras.

Este tutorial asume que ya sabéis cómo lanzar [un mismo juego a dobles avec TGBDual](./play-with-two-game-boy).

>El hecho de utilizar dos juegos diferentes permite utilizar las grabaaciones de los juegos en cada GameBoy.
{.is-success}

## Dos juegos diferentes en el mismo sistema

La configuración más sencilla consiste en jugar a dos juegos diferentes bajo el mismo sistema (es decir gb+gb o bien gbc+gbc), ya que este modo de funcionamiento no necesita crear grabacaciones previas.

Para ello

* Colocad los dos juegos descomprimidos en el directorio `gamelink` (gb o gbc).
* Lanzad un juego desde el sistema deseado (gb o gbc) en el directorio `gamelink`. Los juegos de esta carpeta están preconfigurados para ser lanzados con TGBDual.
* Una vez iniciado el juego, pulsad `Hotkey + B` para acceder al menú RetroArch.
* Pulsad "Atrás" una vez e id a "Subsistemas".
* En el menú veréis la opción `Load 2 Player Game Boy Link ★` junto con la línea `Current Content: GameBoy #1`.

![](/tutorials/games/guides/game-boy/play-two-different-games-gamelink/2diffgames1core-1.png){.full-width}

* Seleccionad la opción disponible y os encontraréis con el directorio desde del cual se ha lanzado el juego. Seleccionad el juego que habéis lanzado y aceptad.

![](/tutorials/games/guides/game-boy/play-two-different-games-gamelink/2diffgames1core-2.png){.full-width}

>Si el juego está comprimido, os hara falta recorrer el contenido del archivo zip, y abrir la rom que se encuentra en su interior.
{.is-warning}

* Después de validar, volved a `Sous-systèmes`. Aparecerá la opción `Load 2 Player Game Boy Link ★` junto con la línea `Current Content: GameBoy #2`.

![](/tutorials/games/guides/game-boy/play-two-different-games-gamelink/2diffgames1core-3.png){.full-width}

* Seleccionad la opción disponible y os encontraréis con el directorio desde del cual se ha lanzado el juego. Seleccionad el segundo juego y aceptad.

![](/tutorials/games/guides/game-boy/play-two-different-games-gamelink/2diffgames1core-4.png){.full-width}

>Igual que antes, si el juego está comprimido, os hara falta recorrer el contenido del archivo zip, y abrir la rom que se encuentra en su interior.
{.is-warning}

* Después de validar, volved a `Sous-systèmes`. Aparecerá la opción `Start 2 Player Game Boy Link ★` con los juegos que se van a lanzar en la parte de abajo.

![](/tutorials/games/guides/game-boy/play-two-different-games-gamelink/2diffgames1core-5.png){.full-width}

* Seleccionad esta opción y el emulador se reiniciará completamente y lanzará los dos juegos uno al lado del otro.

![](/tutorials/games/guides/game-boy/play-two-different-games-gamelink/2diffgames1core-6.png){.full-width}

## Dos juegos diferentes, dos sistemas diferentes.

Si queréis ejecutar dos juegos, uno para la Game Boy Color y otro para la Game Boy, por ejemplo, hay que aplicar un paso extra. Tenéis que guardar ambos juegos en la misma carpeta antes de empezar.

Así, si queréis ejecutar el sistema Game Boy Color desde EmulationStation, tendréis que desplazar la grabación del juego de la Game Boy (en `share\saves\gb`) a la carpeta `share\saves\gbc` (y viceversa).

Por ejemplo, para el juego `Pokemon - Version Bleue (France) (SGB Enhanced)` de la carpeta `gb`, tenéis que desplazar los ficheros `Pokemon - Version Bleue (France) (SGB Enhanced).rtc` y `Pokemon - Version Bleue (France) (SGB Enhanced).srm` a la carpeta `share\saves\gbc`. Recuperad estos ficheros una vez que el juego haya terminado y se haya cerrado el emulador para ponerlos de nuevo en su ubicación original.