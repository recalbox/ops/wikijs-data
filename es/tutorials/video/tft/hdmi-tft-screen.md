---
title: Configurar vuestra pantalla TFT HDMI
description: 
published: true
date: 2023-09-14T14:34:53.755Z
tags: hdmi, tft, pantalla
editor: markdown
dateCreated: 2023-09-14T14:34:53.755Z
---

## Configurad vuestra pantalla TFT HDMI 5"

Podéis comprar esta pantalla [aquí](https://www.banggood.com/5-Inch-800-x-480-HD-TFT-LCD-Touch-Screen-For-Raspberry-PI-2-Model-B-or-B+-or-A+-or-B-p-1023438.html). Es equivalente al [dispositivo Adafruit](https://learn.adafruit.com/adafruit-5-800x480-tft-hdmi-monitor-touchscreen-backpack/overview).

Podéis hacerla funcionar en Recalbox configurando el fichero [recalbox-user-config.txt](./../../../tutorials/system/modification/configtxt-configuration)

¡No olvidéis que el driver TFP401 no tiene escalador de video! Si no le das exactamente 800×480 píxeles, ¡la imagen no se estirará y se encogerá para encajar! Por lo tanto, hay que configurar la resolución exacta en el fichero `recalbox-user-config.txt`.

* Conectaros por [SSH](./../../../tutorials/system/access/root-access-terminal-cli).
* Montad [la partición de arranque](./../../../tutorials/system/modification/configtxt-configuration) en modo lectura-escritura.
* Modificad el fichero `recalbox-user-config.txt` con nano o vim y añadid lo siguiente:

```ini
# uncomment to force a specific HDMI mode (here we are forcing 800x480!)
hdmi_group=2
hdmi_mode=1
hdmi_mode=87
hdmi_cvt=800 480 60 6 0 0 0
 
max_usb_current=1
```

La línea `max_usb_current=1` aumenta la corriente máxima de salida USB del Pi. De esta forma, vuestro Pi suministrará suficiente energía a la pantalla. Comprobad que la fuente de alimentación principal es lo suficientemente potente (~ 2000mA).

Si la imagen no tiene la resolución correcta cuando se emula, tendréis que modificar el fichero `recalbox.conf` y cambiar la variable `global.videomode`:

```ini
global.videomode=default
```

Consultad [este mensaje](https://forum.recalbox.com/topic/4539/how-to-config-portable-5-inch-screen-pics-inside) para ver todo ello en imágenes.


## Configurad vuestra pantalla TFT HDMI 7"

Podéis encontrar esta pantalla a un precio correcto [aquí](https://www.waveshare.com/7inch-HDMI-LCD-C.htm) o [aquí](https://www.waveshare.com/wiki/7inch_HDMI_LCD_%28C%29).

Podéis hacerla funcionar en Recalbox configurando el fichero [recalbox-user-config.txt](./../../../tutorials/system/modification/configtxt-configuration)

¡No olvidéis que el driver TFP401 no tiene escalador de video! Si no le das exactamente 800×480 píxeles, ¡la imagen no se estirará y se encogerá para encajar! Por lo tanto, hay que configurar la resolución exacta en el fichero `recalbox-user-config.txt`.

* Conectaros por [SSH](./../../../tutorials/system/access/root-access-terminal-cli).
* Montad [la partición de arranque](./../../../tutorials/system/modification/configtxt-configuration) en modo lectura-escritura.
* Modificad el fichero `recalbox-user-config.txt` con nano o vim y añadid lo siguiente:

```ini
config_hdmi_boost=7
hdmi_max_current=1
# uncomment to force a specific HDMI mode (here we are forcing 2014x600!)
hdmi_group=2
hdmi_mode=1
hdmi_mode=87
hdmi_cvt=1024 600 60 6 0 0 0
display_rotate=0
max_usb_current=1
hdmi_drive=1
hdmi_ignore_edid=0xa5000080
```

La línea `max_usb_current=1` aumenta la corriente máxima de salida USB del Pi. De esta forma, vuestro Pi suministrará suficiente energía a la pantalla. Comprobad que la fuente de alimentación principal es lo suficientemente potente (~ 2000mA).

Si la imagen no tiene la resolución correcta cuando se emula, tendréis que modificar el fichero `recalbox.conf` y cambiar la variable `global.videomode`:

```ini
global.videomode=default
```

Podéis ajustar el modo personalizado siguiendo [esta página](https://www.raspberrypi.org/documentation/configuration/config-txt/video.md).