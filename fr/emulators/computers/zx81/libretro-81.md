---
title: Libretro 81
description: 
published: true
date: 2024-06-14T14:51:56.072Z
tags: libretro, zx81, 81
editor: markdown
dateCreated: 2021-05-21T08:18:50.358Z
---



## ![](/emulators/license.svg) Licence

Ce core est sous licence [**GPLv3**](https://github.com/libretro/81-libretro/blob/master/LICENSE).

## ![](/emulators/compatibility.png) Compatibilité

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Fonctionnalités

| Fonctionnalité | Supporté |
| :---: | :---: |
| Redémarrage  | ✔ |
| Captures d'écran | ✔ |
| Sauvegardes instantanées | ✔ |
| Rembobinage | ✔ |
| Netplay | ✔ |
| Options du core | ✔ |
| Contrôles | ✔ |

## ![](/emulators/bios.svg) BIOS

>**Aucun bios n'est requis.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Extensions supportées

Les roms doivent avoir les extensions suivantes :

* .tzx
* .t81
* .p
* .zip
* .7z

Ce système supporte les roms compressées au format .zip/.7z. Attention toutefois, il ne s'agit que d'une archive.

Les fichiers contenus dans les .zip/.7z doivent correspondre aux extensions citées précédemment.
De plus, chaque fichier .zip/.7z ne doit contenir qu'une seule rom compressée.

### Emplacement

Placez les roms comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 zx81
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.zip**

>Les roms au format **TOSEC** sont vivement conseillées.
{.is-success}

>Pour plus d'information sur les roms, rendez-vous sur [ce tutoriel](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuration avancée de l'émulateur

>Pour pouvoir conserver vos configurations personnalisées lors d'une mise à jour, nous vous conseillons d'utiliser notre fonctionnalité [Surcharges de configuration](./../../../advanced-usage/configuration-override).
{.is-info}

### Accéder aux options

Vous pouvez configurer diverses options de deux façons différentes.

* Via le menu RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via le fichier `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Options du core

| Options | Valeur des options | Variable (pour surcharge) | Valeurs des variables |
| --- | --- | --- | --- |
| Tape Fast Load | `Activé` ✅ / `Désactivé` | `81_fast_load` | `enabled` / `disabled` |
| 8K-16K Contents | `auto` ✅ / `ROM shadow` / `RAM` / `dK'tronics 4K Graphics ROM + 4K RAM` | `81_8_16_contents` | `auto` / `ROM shadow` / `RAM` / `dK'tronics 4K Graphics ROM + 4K RAM` |
| Size Video Border | `normal` ✅ / `small` / `none` | `81_border_size` | `normal` / `small` / `none` |
| High Resolution | `auto` ✅ / `none` / `WRX` | `81_highres` | `auto` / `none` / `WRX` |
| Emulate Chroma 81 | `auto` ✅ / `Désactivé` / `Activé` | `81_chroma_81` | `auto` / `disabled` / `enabled` |
| Video Presets | `clean` ✅ / `tv` / `noisy` | `81_video_presets` | `clean` / `tv` / `noisy` |
| Sound emulation | `auto` ✅ / `none` / `Zon X-81` | `81_sound` | `auto` / `none` / `Zon X-81` |
| Joypad Left mapping | `auto` ✅ / `default` / `new line` / `shift` / `space` / `.` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` | `81_joypad_left` | `auto` / `default` / `new line` / `shift` / `space` / `.` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` |
| Joypad Right mapping | `auto` ✅ / `default` / `new line` / `shift` / `space` / `.` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` | `81_joypad_right` | `auto` / `default` / `new line` / `shift` / `space` / `.` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` |
| Joypad Up mapping | `auto` ✅ / `default` / `new line` / `shift` / `space` / `.` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` | `81_joypad_up` | `auto` / `default` / `new line` / `shift` / `space` / `.` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` |
| Joypad Down mapping | `auto` ✅ / `default` / `new line` / `shift` / `space` / `.` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` | `81_joypad_down` | `auto` / `default` / `new line` / `shift` / `space` / `.` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` |
| Joypad A button mapping | `auto` ✅ / `default` / `new line` / `shift` / `space` / `.` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` | `81_joypad_a` | `auto` / `default` / `new line` / `shift` / `space` / `.` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` |
| Joypad B button mapping | `auto` ✅ / `default` / `new line` / `shift` / `space` / `.` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` | `81_joypad_b` | `auto` / `default` / `new line` / `shift` / `space` / `.` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` |
| Joypad X button mapping | `auto` ✅ / `default` / `new line` / `shift` / `space` / `.` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` | `81_joypad_x` | `auto` / `default` / `new line` / `shift` / `space` / `.` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` |
| Joypad Y button mapping | `auto` ✅ / `default` / `new line` / `shift` / `space` / `.` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` | `81_joypad_y` | `auto` / `default` / `new line` / `shift` / `space` / `.` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` |
| Joypad L button mapping | `auto` ✅ / `default` / `new line` / `shift` / `space` / `.` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` | `81_joypad_l` | `auto` / `default` / `new line` / `shift` / `space` / `.` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` |
| Joypad R button mapping | `auto` ✅ / `default` / `new line` / `shift` / `space` / `.` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` | `81_joypad_r` | `auto` / `default` / `new line` / `shift` / `space` / `.` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` |
| Joypad L2 button mapping | `auto` ✅ / `default` / `new line` / `shift` / `space` / `.` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` | `81_joypad_l2` | `auto` / `default` / `new line` / `shift` / `space` / `.` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` |
| Joypad R2 button mapping | `auto` ✅ / `default` / `new line` / `shift` / `space` / `.` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` | `81_joypad_r2` | `auto` / `default` / `new line` / `shift` / `space` / `.` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` |
| Transparent Keyboard Overlay | `Activé` ✅ / `Désactivé` | `81_keybovl_transp` | `enabled` / `disabled` |
| Time to Release Key in ms | `100` ✅ / `300` / `500` / `1000` | `81_key_hold_time` | `100` / `300` / `500` / `1000` |

## ![](/emulators/external-links.png) Liens externes

* **Code source utilisé** : [https://github.com/libretro/81-libretro/](https://github.com/libretro/81-libretro/)
* **Documentation Libretro** : [https://docs.libretro.com/library/eightyone/](https://docs.libretro.com/library/eightyone/)