---
title: Neo-Geo Pocket Color
description: 
published: true
date: 2024-07-26T08:33:44.755Z
tags: color, snk, neo-geo, pocket, ngpc, portátiles
editor: markdown
dateCreated: 2023-09-04T08:22:50.534Z
---

![](/emulators/handheld/ngpc.svg){.align-center}

## Detalles técnicos

* **Fabricante**: SNK
* **Año de lanzamiento**: 1999
* **Cantidades vendidas: 2 millones, incluyendo la versión en color.
* **Procesadores**: Toshiba TLCS900H @ 6.144 MHz, Z80 @ 3.072 MHz
* **RAM**: 12K para el 900H, 4K para el Z80
* **ROM**: 64 Kbytes
* **Unidad de procesamiento de imágenes**: 16 bits
* **Resolución**: 160x152 (256×256 pantalla virtual)
* **Soporte**: cartucho de 4 MB como máximo

## Presentación

La **Neo-Geo Pocket Color** (o **NGPC**) es una consola portátil de 16 bits diseñada por SNK. La consola salió a la venta el 16 de marzo de 1999 en Japón, el 6 de agosto de 1999 en Norteamérica y el 1 de octubre de 1999 en algunos países europeos. Fue la sucesora de la consola monocroma Neo-Geo Pocket, lanzada un año antes.

El sistema operativo tiene una función original: el idioma se configura en la consola. Como resultado, los juegos muestran sus textos en el idioma seleccionado (suponiendo que el cartucho proporcione el idioma solicitado). En la consola se pueden configurar otros parámetros, como la hora y la fecha, y el sistema operativo puede proporcionar horóscopos personalizados cuando se introduce una fecha de nacimiento.

Existen cables para conectar varias consolas, así como un cable para conectar la consola con una Sega Dreamcast para determinados juegos. También existe un conector inalámbrico lanzado en Japón que permite jugar a varios jugadores a la vez. Se desarrolló una extensión para el reproductor de MP3, pero no salió a la venta debido al cierre de SNK.

## Emuladores

[Libretro Mednafen_NGP](libretro-mednafen_ngp)
[Libretro RACE](libretro-race)
