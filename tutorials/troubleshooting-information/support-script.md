---
title: Support script
description: 
published: true
date: 2024-07-26T07:20:37.545Z
tags: script, support
editor: markdown
dateCreated: 2021-08-06T14:37:01.057Z
---

A script named recalbox-support&#46;sh has been added to help developers with problems you may encounter with your devices.

This file gives the following information about :

* joystick
* kodi
* lirc
* system (lsmod, lsusb, tv service, es_settings, recalbox.conf, recalbox.log,config, df, etc...)

## Prerequisites

For the support archive to be useful, you need to reproduce your problem with the [debug mode enabled](../troubleshooting-information/enable-debug-mode). There is no need to do the step described after rebooting. After enabling debug mode and rebooting, try again to reproduce your error.

## Use

Its use is done via the Web manager.

* Go to the [Webmanager](./../../basic-usage/features/webmanager).
* Click on the cogwheel at bottom right (top right on mobile)
* Click on `Support archive`.

After a while, you will be given a download link. Download the archive and provide the downloaded file in a message to the person who requested it.

>Don't forget to deactivate debug mode after sending the support file.
{.is-info}