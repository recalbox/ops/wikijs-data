---
title: Atomiswave
description: 
published: true
date: 2024-07-24T07:48:00.406Z
tags: arcade, atomiswave
editor: markdown
dateCreated: 2021-06-29T17:11:51.314Z
---

![](/emulators/arcade/atomiswave.svg){.align-center}

## Technical data

* **Manufacturer**: Sammy Corporation
* **Year of release**: 2003
* **CPU**: Hitachi SH-4 32-bit RISC CPU (@ 200 MHz 360 MIPS / 1.4 GFLOPS)
* **GPU**: NEC-Videologic PowerVR (PVR2DC/CLX2) @ 100 MHz
* **Sound**: ARM7 Yamaha AICA 45 MHZ (with internal 32-bit RISC, 64 channels ADPCM)
* **RAM**: 40 MB
* **Media**: ROM Board (max. size of 168 MB)

## Presentation

The Atomiswave is a custom arcade system board and cabinet from Sammy Corporation. It is based on Sega's Dreamcast console (it shares similarities with the NAOMI, as far as it uses removable game cartridges, as well as a removable module for changing the control scheme and it is common to see the "Sega" logo on its boot up screen, it is commonly believed that the Atomiswave has more VRAM and audio RAM than a Dreamcast, but this is not true). The Atomiswave uses interchangeable game cartridges and the cabinet's control panel can be easily switched out with different control sets, including dual joysticks, dual light guns and a steering wheel.

With the retirement of the aging Neo Geo MVS system, SNK Playmore chose the Atomiswave as its next system to develop games for. In a contract with Sammy, SNK Playmore agreed to develop five games for the Atomiswave system. Metal Slug 6 was SNK Playmore's fifth game for the Atomiswave, after which SNK moved on to a Taito Type X2 arcade board.

## Emulators

[Libretro Flycast](libretro-flycast)
[Libretro Flycast-Next](libretro-flycast-next)