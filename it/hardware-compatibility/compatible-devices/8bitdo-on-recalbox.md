---
title: 8bitdo su Recalbox
description: Elenco completo dei controller 8bitdo già testati
published: true
date: 2022-08-16T22:46:30.201Z
tags: joystick, 8bitdo
editor: markdown
dateCreated: 2021-06-19T15:31:44.208Z
---

![8bitdo](http://support.8bitdo.com/images/Logo-black.svg)

![Controller in produzione 8Bitdo al 13 novembre 2019](/compatibility/8bitdocollection.png){.full-width}

## Controller Supportati 8Bitdo

### Controller stile SNES :

| Controller | FW (min / rec*) | USB | BT | Accensione | Associazione BT* | Reinizializzazione BT | Ripristino impostazioni di fabbrica |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| SN30 Pro+ | 4.01 / 5.04 | ✅ | ☑️ | START + B | Premere il tasto PAIR per 2 secondi | Premere il tasto PAIR per 2 secondi | [Controlla il manuale sul sito 8Bitdo](https://download.8bitdo.com/Manual/Controller/SN30-Pro+/SN30_Pro+_Manual.pdf) |
| SN30 Pro v2.0 | ? / 1.32 | ✅ | ☑️ | START + B | Premere il tasto PAIR per 3 secondi | Premere il tasto Select per 3 secondi | [Controlla il manuale sul sito 8Bitdo](https://download.8bitdo.com/Manual/Controller/SN30pro+SF30pro/SN30pro_Manual.pdf) |
| SN30 & SF30 Pro | 1.23 / 1.38 | ✅ | ☑️ | START + B | Premere il tasto PAIR per 3 secondi | Premere il tasto Select per 3 secondi | [Premere il tasto Select per 8 secondi](https://download.8bitdo.com/Manual/Controller/SN30pro+SF30pro/SN30pro+SF30pro_Manual.pdf) |
| SN30 Pro USB | ? / 1.03 | ? | n/a | n/a | n/a| n/a | [Controlla il manuale sul sito 8Bitdo](https://download.8bitdo.com/Manual/Controller/SN30pro-USB/SN30-Pro-USB.pdf) |
| SN30 GP v2.0 | 6.11 / 6.16 | ? | ? | START + B | Premere il tasto Select per 3 secondi | Premere il tasto Select per 3 secondi | [Controlla il manuale sul sito 8Bitdo](https://download.8bitdo.com/Manual/Controller/SN30+SF30/SN30_Manual.pdf) |
| SN30 Retro Set | ? / 4.10 | ? | ? | START | Premere il tasto Select per 3 secondi | Premere il tasto Select per 3 secondi | [Controlla il manuale sul sito 8Bitdo](https://download.8bitdo.com/Manual/Controller/SN30+SF30/SN30_Analogue.pdf) |
| SN30 | 2.62 / 4.20 | ✅ | ☑️ | START + R | Premere il tasto Select per 3 secondi | Premere il tasto Select per 3 secondi | [Premere il tasto Select per 8 secondi](https://download.8bitdo.com/Manual/Controller/SN30+SF30/SN30_Analogue.pdf) |
| SF30 &amp; SFC30 | 2.70 / 4.20 | ✅ | ☑️ | START + R | Premere il tasto Select per 3 secondi | Premere il tasto Select per 3 secondi | [Premere il tasto Select per 8 secondi](https://download.8bitdo.com/Manual/Controller/SN30+SF30/SN30+SF30_Manual_V4.pdf) |

### Controller stile SNESxNES :

| Controller | FW (min / rec*) | USB | BT | Accensione | Associazione BT* | Reinizializzazione BT | Ripristino impostazioni di fabbrica |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| N30 Pro 2 | ? / 6.10 | ? | ? | START | Premere il tasto PAIR per 1 secondo | Premere il tasto PAIR per 3 secondi | [Controlla il manuale sul sito 8Bitdo](https://download.8bitdo.com/Manual/Controller/N30pro2/N30pro2-Manual.pdf) |
| N30 &amp; NES30 Pro | 1.62 / 4.10 | ✅ | ☑️ | POWER | Premere il tasto PAIR per 1 secondo | Premere il tasto PAIR per 3 secondi | [Premere il tasto START per 8 secondi](https://download.8bitdo.com/Manual/Controller/N30pro+F30pro/N30pro+F30pro_Manual_V4.pdf) |
| F30 &amp; FC30 Pro | 1.62 / 4.10 | ✅ | ☑️ | POWER + X | Premere il tasto PAIR per 1 secondo | Premere il tasto PAIR per 3 secondi | [Premere il tasto START per 8 secondi](https://download.8bitdo.com/Manual/Controller/N30pro+F30pro/N30pro+F30pro_Manual_V4.pdf) |

### Controller stile NES :

| Controller | FW (min / rec*) | USB | BT | Accensione | Associazione BT* | Reinizializzazione BT | Ripristino impostazioni di fabbrica |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| N30 NS | ? / 6.10 | ? | ? | START | Premere il tasto Select per 3 secondi | Premere il tasto Select per 3 secondi | [Controlla il manuale sul sito 8Bitdo](https://download.8bitdo.com/Manual/Controller/N30-NS/N30-NS.pdf) |
| N30 &amp; NES30 | 2.62 / 4.20 | ✅ | ☑️ | START + R | Premere il tasto Select per 3 secondi | Premere il tasto Select per 3 secondi | [Premere il tasto Select per 8 secondi](https://download.8bitdo.com/Manual/Controller/N30+F30/N30+F30_Manual_V4.pdf) |
| F30 &amp; FC30 | 2.62 / 4.20 | ✅ | ☑️ | START + R | Premere il tasto Select per 3 secondi | Premere il tasto Select per 3 secondi | [Premere il tasto Select per 8 secondi](https://download.8bitdo.com/Manual/Controller/N30+F30/N30+F30_Manual_V4.pdf) |

### Controller stile MEGADRIVE :

| Manette | FW (min / rec*) | USB | BT | Accensione | Associazione BT* | Reinizializzazione BT | Ripristino Impostazioni di fabbrica |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| M30 | 1.13 / 1.15 | ✅ | ☑️ | START + B | Premere il tasto Select 1 secondo | Premere il tasto Select 3 secondi | [Controlla il manuale sul sito 8Bitdo](https://download.8bitdo.com/Manual/Controller/M30/M30_Manual.pdf) |

### Controller stile ARCADE/CABINATO :

| Controller | FW (min / rec*) | USB | BT | Accensione | Associazione BT* | Reinizializzazione BT | Ripristino impostazioni di fabbrica |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| N30 &amp; NES30 Arcade Stick | 1.41 / 5.01 | ✅ | ☑️ | HOME | Premere il tasto Select per 3 secondi | Premere il tasto Select per 3 secondi | [Controlla il manuale sul sito 8Bitdo](https://download.8bitdo.com/Manual/Controller/Joystick/N30-Arcade-Manual.pdf) |
| F30 & FC30 Arcade Stick | 1.42 | ✅ | ☑️ | HOME | Premere il tasto Select per 3 secondi | Premere il tasto Select per 3 secondi | [Controlla il manuale sul sito 8Bitdo](https://download.8bitdo.com/Manual/Controller/Joystick/F30-ArcadeJoystick-eng.pdf) |

### Altri stili di controller :

| Controller | FW (min / rec*) | USB | BT | Accensione | Associazione BT* | Reinizializzazione BT | Ripristino impostazioni di fabbrica |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| Lite | ?? / 1.20 | ? | ? | HOME | Premere il tasto PAIR per 2 secondi | Premere il tasto PAIR per 1 secondo | [Controlla il manuale sul sito 8Bitdo](https://download.8bitdo.com/Manual/Controller/Lite/Lite_Manual.pdf) |
| Zero | n/a | ❌ | ☑️ | START + R1 | Premere il tasto Select per 3 secondi | Premere il tasto Select per 3 secondi | [Premere il tasto Select per 8 secondi](https://download.8bitdo.com/Manual/Controller/ZERO/ZERO_Manual_V4.pdf) |
| Zero 2 | 1.05 / 1.06 | ✅ | ☑️ | START + B | Premere il tasto Select per 3 secondi | Premere il tasto Select per 3 secondi | [Premere il tasto Select per 8 secondi](https://download.8bitdo.com/Manual/Controller/Zero2/Zero2_Manual.pdf) |

* FW = Firmware
* min = Minimo
* rec = Raccomandato
* BT = Bluetooth

## Dongles/Chiavette 8Bitdo Supportati

| Dongle | FW (min / rec*) | Compatibilita con Recalbox | Controller Compatibili |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| USB Adapter for PS Classic (in X-Input Mode) | 1.28 / 1.33 | ✅ | PS3, PS4, Xbox One S, Wii Mote, Wii U Pro, Switch Joycons, Switch Pro, M30, SFC30, SF30 Pro. |
| USB Adapter Receiver (in X-Input Mode) | 1.28 / 2.00 | ✅ | PS3, PS4, PS5, Xbox One S, Xbox One X, Wii Mote, Wii U Pro, Switch Joycons, Switch Pro, M30, SFC30, SF30 Pro. |
| GBros Adapter Emeter (With 8Bitdo Receiver) | 2.23 / 2.24 | ✅ | GameCube, GameCube Wavebrid (avec le câble d'extension), Wii Classic, Wii Classic Pro, NES Mini, SNES Mini. |
| NES & SNES Mini Receiver | ? / 1.14 | ? | ? |
| SNES Receiver | ? / 1.34 | ? | ? |
| USB Wireless Adapter 2 | 1.00 / 1.01 | ✅ | Xbox One X |

## Support 8Bitdo

* [Supporto per joystick 8Bitdo](https://support.8bitdo.com/).
* [Strumento di aggiornamento](https://support.8bitdo.com/firmware-updater.html) 

I controller 8Bitdo vanno disassociati al Recalbox prima di associarli nuovamente. Se hai un problema di associazione, puoi provare il metodo "Ripristino Bluetooth" e, se non funziona, puoi provare il metodo "Ripristino impostazioni di fabbrica".

Puoi trovare il manuale per ciascuno dei tuoi controller sul sito di [supporto 8Bitdo](https://support.8bitdo.com/).

## FAQ / Domande frequenti

### Come ricaricare il mio controller ?

Utilizzare preferibilmente la porta USB del computer o della console per caricare il controller.
Se utilizzi un caricabatterie convenzionale o un caricabatterie per telefono, assicurati che non superi mai i 5V 1A.
Un'intensità troppo elevata può danneggiare la batteria e rendere il controller inutilizzabile in Bluetooth.

### Come aggiornare il Firmware?

È sufficiente andare sul sito di [supporto di 8Bitdo](https://support.8bitdo.com/) e scaricare lo `Upgrade tool` in cima alla pagina.

1. Installare l'applicazione `8BitDo Firmware Upgrader` scaricata.
2. Aprire l'applicazione
3. Una volta aperta l'applicazione, vi chiederà di collegare il controller al PC.
4. Il controller verrà riconosciuto automaticamente. Fare clic sul pulsante `Firmware Update` per accedere agli aggiornamenti disponibili per il controllore.
5. Selezionare il firmware desiderato e fare clic sul pulsante blu `Update`.
6. Seguire la procedura indicata per eseguire l'aggiornamento. L'aggiornamento sarà caratterizzato da una barra di avanzamento rossa.
7. Una volta completato l'aggiornamento, fare clic sul pulsante verde `Success` (successo) e scollegare il controller.

### Come accendere/collegare correttamente il mio controller?

A seconda del dispositivo (Windows, Mac OS, Android, Switch, ...), ci sono vari modi per accendere il controller 8bitdo:

* `Start` + `Y` = Modalità Switch
* `Start` + `A` = Modalità macOS
* `Start` + `X` = Modalità PC
* `Start` + `B` = Modalità Bluetooth (comunemente consigliata per Android)

È necessario tenere premuti i pulsanti contemporaneamente finché il controller non si accende e quindi collegarsi.

In caso di connessione USB, tenere premuti contemporaneamente i pulsanti fino all'accensione del controller, quindi collegare il cavo USB.

### Come collego il mio controller a un altro dispositivo?

Il controllore ricorda un solo indirizzo di dispositivo esterno. Se si desidera passare da Switch ad Android, ad esempio, è necessario reimpostare i dati di associazione del controller. A seconda del modello, la procedura varia leggermente. Per la procedura, consultare la tabella precedente.

### Adattatore USB + controller PS3

Esiste uno strumento per gestire il controller PS3 e gli adattatori USB.

### {.tabset}
#### Windows
È possibile scaricare questa [utility](http://download.8bitdo.com/Tools/Receiver/8Bitdo_PS3_Tool_Win_V1.1.zip)

Se avete un problema con Windows, ci sono due soluzioni:

**Soluzione 1 :**
Installa SixaxisPairToolSetup-0.3.0.exe  
È possibile scaricarlo su [questa pagina](http://sixaxispairtool.en.lo4d.com/download).
  
**Soluzione 2 :**
Disabilita temporaneamente l'applicazione della firma del driver di Windows
Troverai immagini di aiuto nella cartella: `Come disabilitare temporaneamente l'applicazione della firma del driver su Win8 e Win10` contenuto nel `8Bitdo_PS3_Tool_Win_V1.1.zip`.

Se altri problemi persistono: installa il driver 'libusb-win32'.
  
>L'applicazione della firma del driver di Windows può essere riattivata durante un aggiornamento o altro.
{.is-info}

#### macOS
È possibile scaricare questa [utility](http://download.8bitdo.com/Tools/Receiver/8Bitdo_PS3_Tool_MacOS_V1.1.zip)

### Arcade Stick

A seconda del dispositivo (Windows, Mac OS, Android, Switch, ecc.), esistono diverse combinazioni di accensione:

* Windows : modalità XINPUT.  
* Android : modalità DINPUT.  
* MacOS e Switch non necessarie.  
  
Procedura di accensione:

1. Se necessario, selezionare la modalità XINPUT o DINPUT.
2. Premere il pulsante PAIR per inizializzare il dispositivo (il led blu lampeggia).
3. Configura il controller sul tuo dispositivo (Windows, macOS, Android o Switch).
4. Il LED dovrebbe diventare blu fisso e il controller è pronto per l'uso.

È necessario tenere premuti i pulsanti contemporaneamente finché il controller non si accende, quindi effettuare il collegamento. In caso di connessione USB, tenere premuti i tasti contemporaneamente finché il controller non si accende, quindi collegare il cavo USB.

### Firmware consigliati su Recalbox?

Per il firmware consigliato per il controllore, consultare la tabella in cima alla pagina.

### Pad zero V1 e Recalbox ?

Funziona solamente con il Bluetooth. Accendi il controller tenendo premuti i pulsanti `START` + `R1`.

Quando si utilizzano due pad zero V1:

- Primo controller, tieni premuti i pulsanti `START` + `R1`.
- Secondo controller, tieni premuti i pulsanti `START` + `R1` + `B`.

### Il tuo pad non viene riconosciuto con il cavo USB?

Verifica di utilizzare il cavo fornito con il controller, altrimenti assicurati di disporre di un cavo dati USB e non solo di ricarica USB. Alcuni cavi USB lasciano passare solo la corrente di carica e non il flusso di dati.