---
title: Interactuar con el emulador a través de un mapping GPIO
description: 
published: true
date: 2023-09-13T15:16:03.114Z
tags: gpio, emuladores, interacciones
editor: markdown
dateCreated: 2023-09-13T15:16:03.114Z
---

Los comandos de red se utilizan para controlar ciertas partes de RetroArch. Para ello, los comandos se envían a RetroArch utilizando el Protocolo de Datagramas de Usuario (UDP).

Normalmente, en Recalbox, estos comandos se envían a través de la función **Hotkey** del mando. Por ejemplo, para reiniciar el juego, se pulsa `Hotkey + B` o para guardar la partida, se pulsa `Hotkey + Y`.

>Esto sólo funciona con RetroArch / emuladores que forman parte de Libretro.
{.is-info}

## Activación

Estos comandos también pueden ser enviados a RetroArch a través de una línea de comandos o a travès de los pines GPIO. Para ello, es necesario activar la opción **comandos de red** en el menú de RetroArch o modificar la siguiente línea en `retroarch.cfg` :

```text
network_cmd_enable = true
```

## Enviar un comando a través de los GPIO

#### Ejemplos de aplicación :

Estáis utilizando la carcasa de una consola retro (NES, N64, etc) para vuestra Recalbox y queréis utilizar el botón de reinicio de la carcasa original para reiniciar el juego al que estáis jugando actualmente sin reiniciar completamente vuestro hardware (Raspberr Pi, etc). El script que se muestra a continuación os explica cómo enviar el comando de red **RESET** a RetroArch a travès de los pines GPIO 5 y 6 para reiniciar el juego en curso.

```python
import RPi.GPIO as GPIO
import time
import socket

# addressing information of target
IPADDR = "127.0.0.1"
PORTNUM = 55355

# enter the data content of the UDP packet
COMMAND = "RESET"

# initialize a socket, think of it as a cable
# SOCK_DGRAM specifies that this is UDP
try:
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
except socket.error:
    print 'Failed to create socket'
    sys.exit()

GPIO.setmode(GPIO.BCM)
GPIO.setup(3, GPIO.IN, pull_up_down=GPIO.PUD_UP)

def exitEmulator(channel):
    s.sendto(COMMAND, (IPADDR, PORTNUM))

GPIO.add_event_detect(3, GPIO.FALLING, callback=exitEmulator, bouncetime=500)
while True:
    time.sleep(10)
```

Con este script, no sólo es posible hacer **RESET** en los emuladores, sino que también podéis utilizar otros comandos. Podéis hacer **QUIT** para volver a EmulationStation, **CHANGE** o **SAVE** para grabar el estado instantáneo de juego, poner la partida en **PAUSE**, etc. Para ello, basta con reemplazar el comando **RESET** de la **COMMAND = RESET** por el comando de vuestra elección.

>Este script puede no funcionar con todos los comandos de RetroArch. Para alguno de ellos, funciona, pero para otros, necesitará ser modificado para hacerlos funcionar correctamente (por ejemplo, el FASTFORWARD_HOLD).
{.is-warning}

También podéis re-mapear los pines de vuestra Raspberry Pi.

Y no lo olvidéis: Recalbox ya dispone de varias opciones integradas para faciliar el reinicio, el arranque y el apagado de vuestro material/hardware usando [botones y GPIO](./../../tutorials/others/add-on-off-button-to-your-recalbox).

## Commandos

RetroArch soporta los siguientes comandos:

* CHEAT_INDEX_MINUS
* CHEAT_INDEX_PLUS
* CHEAT_TOGGLE
* DISK_EJECT_TOGGLE
* DISK_NEXT
* DISK_PREV
* FAST_FORWARD
* FAST_FORWARD_HOLD
* FRAMEADVANCE
* FULLSCREEN_TOGGLE
* GRAB_MOUSE_TOGGLE
* LOAD_STATE
* MENU_TOGGLE
* MOVIE_RECORD_TOGGLE
* MUTE
* NETPLAY_FLIP
* OVERLAY_NEXT
* PAUSE_TOGGLE
* QUIT
* RESET
* REWIND
* SAVE_STATE
* SCREENSHOT
* SHADER_NEXT
* SHADER_PREV
* SLOWMOTION
* STATE_SLOT_MINUS
* STATE_SLOT_PLUS
* VOLUME_DOWN
* VOLUME_UP

## Enviar un comando a través de la línea de comandos

También podéis enviar comandos de red a través del terminal desde Linux. Aquí tenéis cómo hacer:

```shell
echo -n "QUIT" | nc -u -w1 127.0.0.1 55435
```

RetroArch escucha el puerto 55435 por defecto (el mismo puerto que para el Netplay).