---
title: Libretro MAME 2000
description: 
published: true
date: 2024-07-03T15:56:09.130Z
tags: libretro, neo-geo, mame2000
editor: markdown
dateCreated: 2023-09-06T13:59:50.447Z
---



## ![](/emulators/license.svg) Licencia

El core posee la siguiente licencia **MAME non-commerciale**.

## ![](/emulators/compatibility.png) Compatibilidad

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ❌ | ❌ | ❌ | ❌ | ❌ | ❌ |

## ![](/emulators/features.png) Funcionalidades



## ![](/emulators/bios.svg) BIOS

### Lista de BIOS obligatorias

| Nombre del fichero | Descripción | MD5 | ¿Preinstalada? |
| :---: | :---: | :---: | :---: |
| neogeo.zip | BIOS Neo-Geo | e023d0a4b5249fdff2a4620c28963944 8cbdff53661f41ddbe486a98915e1ec9 42e359e394f0be1d61b94ddca0efbe6c c1e0a738d266926f604186aa9d63e4db d12bba59121229ff13702878b415cb7c 2b4ffdf24d5d4f24c4b116c50c4bafc3 005b843b6e70b939c2cca41887cbc371 42e359e394f0be1d61b94ddca0efbe6c 4b9432a4571d03cc8bae9d24d8d6eb40 4cd64313e26537c04cdb561b8f84b0e4 50bbfd6bf297038b085e29265c65723a 76ee7390ccd537cddea1018025ca29a8 84d6c35aad790a77ba3cfc1b319e1061 912c0a56bafe0fba39d0c668b139b250 94748719c0ef31e7375b0c357d82fc89 ab25b9e25052a6064901a7a221037eb6 ae9e5825cde82c58e39dfd48534f7060 b18f83feef474b8a1306ef199cd810a2 b4519df27a352c2a6e42e06d31330d91 bf00272e7150b31156a8995d60bf185d ca66fd463ef7dcab492a6de8ce5f45eb f4a125538dfd7a8044e025fc5188fb88 a3b2c9a69c28ad0bd4ea07877b744bbe | ❌  |

### Emplazamiento

Disponed vuestras bios como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 neogeo
┃ ┃ ┃ ┃ ┣ 🗒 **neogeo.zip**

## ![](/emulators/roms.png) Roms

### Extensions supportées

La rom doit avoir l'extension :

* .zip
* .7z

### Extensiones de roms soportadas

Las roms deben tener las extensiones siguientes:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 neogeo
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.zip**

>Para más información sobre las roms, consultad [este tutorial](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

>Para conservar las configuraciones personalizadas cuando se produce una actualización del sistema Recalbox, os aconsejamos utilizar las [Sobrecargas de configuración](./../../../advanced-usage/configuration-override).
{.is-info}

### Acceder a las opciones

Podéis configurar las opciones del core de dos formas diferentes.

* Via el Menú RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via el fichero `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Opciones del core

| Opción | Valor de la opción | Variable (para la sobrecarga) | Valores posibles |
| --- | --- | --- | --- |
| Frameskip | `Désactivé` ✅ / `auto` / `threshold` | `mame2000-frameskip` | `disabled` / `auto` / `threshold` |
| Frameskip Threshold (%) | `30` ✅ / `40` / `50` / `60` | `mame2000-frameskip_threshold` | `30` / `40` / `50` / `60` |
| Frameskip Interval | `1` ✅ / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` | `mame2000-frameskip_interval` | 
| Skip Disclaimer | `Activé` ✅ / `Désactivé` | `mame2000-skip_disclaimer` | `enabled` / `disabled` |
| Show Game Information | `Désactivé` ✅ / `Activé` | `mame2000-show_gameinfo` | `disabled` / `enabled` |
| Audio Rate (Restart) | `11025` / `22050` ✅ / `32000` / `44100` | `mame2000-sample_rate` | `11025` / `22050` / `32000` / `44100` |
| Stereo (Restart) | `Activé` ✅ / `Désactivé` | `mame2000-stereo` | `enabled` / `disabled` |

## ![](/emulators/external-links.png) Enlaces externos

* **Código fuente utilizado** : [https://github.com/libretro/mame2000-libretro/](https://github.com/libretro/mame2000-libretro/)