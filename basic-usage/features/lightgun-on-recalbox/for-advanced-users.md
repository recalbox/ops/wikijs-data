---
title: And for advanced users
description: How to add lightgun games yourself
published: true
date: 2024-07-25T13:03:30.601Z
tags: lightgun, advanced users, 7.2+
editor: markdown
dateCreated: 2021-08-05T18:28:20.990Z
---

## How to go further?

### The basic idea...

The principle is to have a unique configuration file gathering all the necessary information for the "Lightgun" games of all systems & emulators. 

Thus, we can make inheritance, grouping of games but also manage particularities without having a file by game or by system as we can do it already but without being able to group systems for example. This is possible with this unique xml. 

Moreover, in the next versions, we will be able to add online help, messages when loading games, etc...

Another advantage compared to the classic overloads : we can add functions in the Configgen python to fetch/identify if a hardware is present like the Mayflash dolphin bar or to know on which board/pc we are. You can decide to configure between 1 and 3 Lightgun players in this way.

>As a reminder, this is only possible for Libretro for the moment. Nothing would prevent us to do it for other emulators with the xml file we already have.
{.is-info}

### The configuration XML file

In fact, the configuration of the games is in an xml file (for info, it's a ".cfg" in fact).

The file is organized like this in terms of structure (here it is empty without configurations to show the complete xml structure):

```xml
<?xml version="1.0"?>
<root>
	<emulatorOptions>
 	</emulatorOptions>
  <coreOptions>
  </coreOptions>
  <system>
		<emulatorList>
			<emulator></emulator>
		</emulatorList>
    <emulatorOptions>
    </emulatorOptions>
    <coreOptions>
    </coreOptions>
    <gameList>
      <emulatorOptions>
      </emulatorOptions>
      <coreOptions>
      </coreOptions>
      <game>
        <option /> 
        <emulatorOptions>
        </emulatorOptions>
        <coreOptions>
        </coreOptions>
      </game>
    </gameList>
  </system>
</root>
```

### XML Tags

**&lt;root>**: this is the base of the XML file where everything will be found as well as a version to follow the evolution of this file

```xml
<?xml version="1.0"?>
<root>
	<!-- internal version of the file to help follow-up of update, same version until release -->
	<version>1.0.6 - 18-03-2024</version>
</root>
```

**&lt;emulatorOptions>**: this is the part in &lt;root> and common override of `retroarchcustom.cfg` that we'll have for all systems, in the case of the lightgun, we'll put the basic keys like select/exit/start/trigger there.

```xml
	<!-- MANDATORY inputs (for retroarchcustom.cfg): comon part to use for all systems (using dolphinbar and until 2 wiimotes) -->
	<emulatorOptions>
        <option name="input_player1_gun_start" value="enter" />
        <option name="input_player2_gun_start" value="enter" />
        <option name="input_player1_gun_select" value="escape" />
        <option name="input_player2_gun_select" value="escape" />
        <!-- force selecte and start for consoles games -->
        <option name="input_player1_select" value="escape" />
        <option name="input_player2_select" value="escape" />
        <option name="input_player1_start" value="enter" />
        <option name="input_player2_start" value="enter" />
        <!-- for many it's necessary to move in menu or else -->
        <option name="input_player1_gun_dpad_up" value="up" />
        <option name="input_player1_gun_dpad_down" value="down" />
        <option name="input_player1_gun_dpad_right" value="right" />
        <option name="input_player1_gun_dpad_left" value="left" />
        <option name="input_player2_gun_dpad_up" value="up" />
        <option name="input_player2_gun_dpad_down" value="down" />
        <option name="input_player2_gun_dpad_right" value="right" />
        <option name="input_player2_gun_dpad_left" value="left" />
        <!-- index to manage distinction between guns -->
        <option name="input_player1_mouse_index" value="GUNP1" />
        <option name="input_player2_mouse_index" value="GUNP2" />
        <!-- wiimotes home button for exit game -->
		    <option name="input_exit_emulator" value="lsuper" />
	  	  <option name="input_enable_hotkey" value="lsuper" />
        <!-- set on fullscreen for don't see cursor mouse on x86(-64) -->
	  	  <option name="video_fullscreen" value="true" />
        <!-- to have help using overlay switch and finally ask to press 2 times home to exit -->
        <option name="input_overlay_next" value="lsuper" />
        <option name="quit_press_twice" value="true" />
 	</emulatorOptions>
```

**&lt;coreOptions>**: this is the part in &lt;root> and common override of `retroarch-core-options.cfg` that we will have. In our case, it is not used in a common way but it would be possible.

```xml
    <!-- OPTIONAL options (for retroarch-core-options.cfg): comon part to use for all systems (using dolphinbar and until 2 wiimotes) -->
  	<coreOptions>
        <!-- only for more log on test -->
        <!-- <string name="libretro_log_level" value="2" />
        <option name="log_verbosity" value="true" /> -->
  	</coreOptions>
```

**&lt;option>:** these are the basic configuration values always composed of 2 attributes `name` and `value`:

```xml
<option name="input_player1_gun_start" value="enter" />
```

**&lt;system>:** this is the part that groups all the configuration of a system. You can find the tags `&lt;emulatorList>`, `&lt;emulator>`, `&lt;emulatorOptions>`, `&lt;coreOptions>`, `&lt;gameList>` and `&lt;game>`.

```xml
  <!-- ********************************************************* Nintendo Entertainment System ************************************************************** -->
	<system name="nes">
		<!-- MANDATORY: emulator and core selected for lightgun -->
		<emulatorList name="libretro">
			<emulator priority="1" name="libretro" core="fceumm" />
		</emulatorList>
    <!-- MANDATORY: inputs common to this system -->
		<emulatorOptions>
      	<!-- gamepad -->
      	<option name="input_libretro_device_p1" value="1" />
      	<!-- lightgun -->
      	<option name="input_libretro_device_p2" value="258" />
      	<!-- Wiimote button B -->
		  	<option name="input_player2_gun_trigger_mbtn" value="1" />
        <!-- Set index to GUNP1 on player 2 to have first wiimote as player 1 -->
        <option name="input_player2_mouse_index" value="GUNP1" />
		</emulatorOptions>
		<!-- MANDATORY: options common to this system -->
		<coreOptions>
		  	<option name="fceumm_zapper_mode" value="lightgun" />
		  	<option name="fceumm_zapper_tolerance" value="6" />
      	<option name="fceumm_show_crosshair" value="enabled" />
		</coreOptions>
        <gameList>
            <!-- list of games using lightgun -->
            <!-- see 'duckhunt' game to see more explainations to know how to setup game part -->
            <game name="3in1supergun" tested="true" />
```

**&lt;emulator>**: this tag lists the list of compatible emulators, generally limited to one only:

```xml
    	<!-- MANDATORY: emulator and core selected for lightgun-->
    	<emulatorList name="libretro">
      		<emulator priority="1" name="libretro" core="flycast" />
    	</emulatorList>
```

**&lt;emulator>**: this tag has for value the name of the emulator to use. it is always in `&lt;emulatorList>` as above.

```xml
<emulator priority="1" name="libretro" core="flycast" />
```

**&lt;emulatorOptions>**: this tag allows to override `retroarchcustom.cfg` in addition to the `&lt;emulatorOptions>` part at the level of `&lt;system>`, `&lt;gameList>` and/or `&lt;game>`.

```xml
<system>
.....
		<emulatorOptions>
      	<!-- gamepad -->
      	<option name="input_libretro_device_p1" value="1" />
      	<!-- lightgun -->
      	<option name="input_libretro_device_p2" value="258" />
      	<!-- Wiimote button B -->
		  	<option name="input_player2_gun_trigger_mbtn" value="1" />
        <!-- Set index to GUNP1 on player 2 to have first wiimote as player 1 -->
        <option name="input_player2_mouse_index" value="GUNP1" />
.....
```

```xml
.....
        <gameList>
            <emulatorOptions>
                <!-- no reload, this game is a free fire but need secondary input for grenade and more -->
                <option name="input_player1_a_mbtn" value="2" />
                <option name="input_player2_a_mbtn" value="2" />     
            </emulatorOptions>
            <game name="alien3thegun" tested="true" />
.....
```

```xml
.....            
            <game name="gunbuster" tested="true">
                <emulatorOptions>
                    <!-- no reload free fire game but ned secondary input for grenade and more -->
                    <!-- move players with dpad on wiimote  -->
                    <string name="input_player1_a" value="2" />
                    <string name="input_player1_gun_offscreen_shot_mbtn" value="nul" />
                    <string name="input_player2_a" value="2" />
                    <string name="input_player2_gun_offscreen_shot_mbtn" value="nul" />
                </emulatorOptions>
            </game>
.....
```

**&lt;coreOptions>**: this tag allows to override `retroarch-core-options.cfg` in addition to the `&lt;coreOptions>` part at the level of `&lt;system>`, `&lt;gameList>` and/or `&lt;game>`. 

```xml
        <!-- options common to this system-->
        <coreOptions>
            <option name="genesis_plus_gx_gun_input" value="lightgun" />
            <option name="genesis_plus_gx_gun_cursor" value="enabled" />
        </coreOptions>
```

**&lt;gameList>**: this tag is the root of the games groups. It allows to have a distinct configuration for a part of the games in the same system, so you can have several `&lt;gameList>` sections in the same system.

```xml
        <gameList>
            <emulatorOptions>
                <!-- no reload, these games have a free fire but need secondary input for grenade and more -->
                <option name="input_player1_a_mbtn" value="2" />
                <option name="input_player2_a_mbtn" value="2" />     
            </emulatorOptions>
            <game name="alien3thegun" tested="true" />
            <game name="beastbusters" tested="true" />
            <game name="dragongun" tested="true" />
        </gameList>

```

**&lt;game>**: this tag which is in `&lt;gameList>` is to define the configuration of the particular game.

```xml
            <game name="gunbuster" tested="true">
                <emulatorOptions>
                    <!-- no reload free fire game but ned secondary input fo grenade and more -->
                    <!-- move players with dpad on wiimote  -->
                    <option name="input_player1_a" value="2" />
                    <option name="input_player1_gun_offscreen_shot_mbtn" value="nul" />
                    <option name="input_player2_a" value="2" />
                    <option name="input_player2_gun_offscreen_shot_mbtn" value="nul" />
                </emulatorOptions>
            </game>
```

>**&lt;game>** has the attributes `name` (to recognize the launched game) and `tested` to know if this particular rom has already been tested and its status (2 possible values):
>
>* **true** -> tested with the defined configuration.
>* **false** -> tested but not functional, so will not be configured to work in lightgun mode for the moment (we have hope).

>You must always put the name **without spaces/special characters and in lower case** for the parsing of the "gamelist" of a system by Recalbox. Be careful, it is the name of the game in its simplest form, it is neither the name of the file of the rom nor the name with the geographical zone, etc... it is necessary to remain the simplest possible so that the correspondence is done for sure. **Scraping will make sure you have a game with an identifiable name.
{.is-warning}

### Inheritance

In fact in this paragraph we want to explain that in order the configuration will be done like this: 

`common` -> `system` -> `gameList` -> `game`

.... We let you tell us what `input_player2_gun_trigger_mbtn` will be in the following example

```xml
<?xml version="1.0"?>
<root>
	<emulatorOptions>
	  <option name="input_player2_gun_trigger_mbtn" value="1" />
 	</emulatorOptions>
  <coreOptions>
  </coreOptions>
  <system>
		<emulatorList>
			<emulator></emulator>
		</emulatorList>
    <emulatorOptions>
      <string name="input_player2_gun_trigger_mbtn" value="2" />
    </emulatorOptions>
    <coreOptions>
    </coreOptions>
    <gameList>
      <emulatorOptions>
        <string name="input_player2_gun_trigger_mbtn" value="1" />
      </emulatorOptions>
      <coreOptions>
      </coreOptions>
      <game>
        <emulatorOptions>
          <option name="input_player2_gun_trigger_mbtn" value="2" />
        </emulatorOptions>
        <coreOptions>
        </coreOptions>
      </game>
    </gameList>
  </system>
</root>
```

### Index management for multiplayer

We had to manage the mouse indexes and specify it in a common way or more precisely per game. For example when player 1 is finally on port 2, and player 2 on port 3. So we have keywords like `GUNP1`, `GUNP2` or `GUNP3`.

```xml
.....
     <emulatorOptions>
        <!-- index to manage distinction between guns -->
        <option name="input_player1_mouse_index" value="GUNP1" />
        <option name="input_player2_mouse_index" value="GUNP2" />
        .....
```

```xml
        <gameList> <!-- games using justifier -->
            <emulatorOptions>
                <!-- lightguns always on port 2 -->
                <!-- For Justifier device (blue cross) -->
                <string name="input_libretro_device_p2" value="516" />
                <string name="input_player2_mouse_index" value="GUNP1" />
                <!-- Wiimote button B or Z -->
                <string name="input_player2_gun_trigger_mbtn" value="1" />
                <!-- For Justifier(P2) device (pink cross) -->
                <string name="input_libretro_device_p3" value="772" />
                <string name="input_player3_mouse_index" value="GUNP2" />
                <!-- Wiimote button B or Z -->
                <string name="input_player3_gun_trigger_mbtn" value="1" />
                <!-- Common inputs part replicated for this game using player 3 -->
                <string name="input_player3_gun_start" value="enter" />
                <string name="input_player3_gun_select" value="escape" />
                <!-- Common inputs part replicated to force select and start for consoles games -->
                <string name="input_player3_select" value="escape" />
                <string name="input_player3_start" value="enter" />
            </emulatorOptions>
            <game name="lethalenforcers" tested="true" />
        </gameList>
```

## But more simply...

In fact, the configuration xml file is not in your "share" but it is in the system and accessible only by SSH: `/recalbox/share_init/system/.emulationstation/lightgun.cfg`

>To be able to modify this file, it will be necessary to mount the [system partition](./../../../tutorials/system/access/remount-partition-with-write-access) as read/write.
{.is-warning}

### How to add a game/rom ?

You just have to find the system, example for the NES:

![](/basic-usage/features/lightgun/advanced-users/advancedusers2.png)

And to add in the section &lt;gameList> the tag &lt;game> like this:

```xml
           <game name="mygame2" tested="ok" />
```

>REMINDER: You must always put the name **without spaces/special characters and in lower case** for the parsing of the "gamelist" of a system by Recalbox. Be careful, it is the name of the game in its simplest form, it is not the name of the file of the rom or the name with the geographical zone, etc... it is necessary to remain the simplest possible so that the correspondence is done for sure. **Scraping will make sure you have a game with an identifiable name.
{.is-warning}

### To check that it is taken into account

* If the file is badly formatted, you go back to the list of games.
* If the name is not found/recognized, you will not be able to activate the Lightgun mode.
* If you are really in Lightgun mode, you should be able to exit the game with the HOME button of the Wiimote.

### To add configurations to a game

The best way is to look at what has been done for other games... In general, you will need to modify the settings corresponding to the primary and secondary fire buttons, or even disable some keys (see below) but it can be even more complicated... If this is the case, you will have to look at the manuals of the games of the time and spend a lot of time in the RetroArch menus... so master the art of overloading!

```xml
        <game name="ninjaassault" tested="true">
            <emulatorOptions>
                <!-- for this game start p1 for P2 aux_c -->
                <string name="input_player1_gun_start" value="nul" />
                <string name="input_player1_gun_aux_b" value="enter" />
                <!-- for this game trigger -->
                <string name="input_player1_gun_trigger_mbtn" value="nul" />
                <string name="input_player1_gun_aux_a_mbtn" value="1" />
                <!-- for this game real offscreen reload -->
                <string name="input_player1_gun_offscreen_shot_mbtn" value="nul" />
            </emulatorOptions>
        </game>
```

### To add a new system

In this case, you really need to know the core to use, the RetroArch parameters and in general also configure the "device" in `&lt;emulatorOptions>` as well as the "lightgun" parameters of the core in `&lt;coreOptions>`.

```xml
......
<system name="megadrive">
    <!-- MANDATORY: emulator and core selected for lightgun -->
    <emulatorList name="libretro">
        <emulator priority="1" name="libretro" core="genesisplusgx" />
    </emulatorList>
    <!-- MANDATORY: inputs common to this system -->
    <emulatorOptions>
        <!-- Wiimote button B or Z -->
        <option name="input_player2_gun_trigger_mbtn" value="1" />
        <!-- Wiimote button A or c - aux when it's necessary as jump, rocket, etc... -->
        <option name="input_player2_gun_aux_a_mbtn" value="2" />
        <!-- <option name="input_player2_gun_aux_a_mbtn" value="2" /> -->
        <!-- gamepad always on port 1-->
        <option name="input_libretro_device_p1" value="1" />
    </emulatorOptions>
    <!-- MANDATORY: options common to this system -->
    <coreOptions>
        <option name="genesis_plus_gx_gun_input" value="lightgun" />
        <option name="genesis_plus_gx_gun_cursor" value="enabled" />
    </coreOptions>
    .....
```

**Good luck!!!**