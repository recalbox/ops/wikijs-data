---
title: Libretro NeoCD
description: 
published: true
date: 2024-07-05T10:56:27.490Z
tags: libretro, neo-geo cd, neocd
editor: markdown
dateCreated: 2023-09-06T14:19:59.682Z
---

## ![](/emulators/license.svg) Licencia

El core posee la siguiente licencia [**LGPLv3**](https://github.com/libretro/neocd_libretro/blob/master/LICENSE.md).

## ![](/emulators/compatibility.png) Compatibilidad

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | 
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Funcionalidades

| Funcionalidad | ¿Soportada? |
| :---: | :---: |
|  | ✔ |

## ![](/emulators/bios.svg) BIOS

### Lista de BIOS obligatorias

| Nombre del fichero | Descripción | MD5 | ¿Preinstalada? |
| :---: | :---: | :---: | :---: |
| neocd.bin | CDZ BIOS (MAME) | f39572af7584cb5b3f70ae8cc848aba2 | ❌ |

### Emplazamiento

Disponed vuestras bios como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 neocd
┃ ┃ ┃ ┃ ┣ 🗒 **neocd.bin**

### Lista de bios opcionales

| Nombre del fichero | Descripción | MD5 | ¿Preinstalada? |
| :---: | :---: | :---: | :---: |
| uni-bioscd.rom | Universe BIOS CD 3.3 | 08ca8b2dba6662e8024f9e789711c6fc | ❌ |
| top-sp1.bin | Top Loader BIOS (MAME) | 122aee210324c72e8a11116e6ef9c0d0 | ❌ |
| front-sp1.bin | Front Loader BIOS (MAME) | 5c2366f25ff92d71788468ca492ebeca | ❌ |
| neocd_sz.rom | CDZ BIOS (SMKDAN 0.7b DEC 2010) | 971ee8a36fb72da57aed01758f0a37f5 | ❌ |
| neocd_z.rom | CDZ BIOS | 11526d58d4c524daef7d5d677dc6b004 | ❌ |
| neocd_st.rom | Top Loader BIOS (SMKDAN 0.7b DEC 2010) | f6325a33c6d63ea4b9162a3fa8c32727 | ❌ |
| neocd_t.rom | Top Loader BIOS | de3cf45d227ad44645b22aa83b49f450 | ❌ |
| neocd_sf.rom | Front Loader BIOS (SMKDAN 0.7b DEC 2010) | 043d76d5f0ef836500700c34faef774d | ❌ |
| neocd_f.rom | Front Loader BIOS | 8834880c33164ccbe6476b559f3e37de | ❌ |
| 000-lo.lo | ZOOM Rom | fc7599f3f871578fe9a0453662d1c966 | ❌ |
| ng-lo.rom | ZOOM Rom | e255264d85d5765013b1b2fa8109dd53 | ❌ |

### Emplazamiento

Disponed vuestras bios como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 neocd
┃ ┃ ┃ ┃ ┣ 🗒 **uni-bioscd.rom**
┃ ┃ ┃ ┃ ┣ 🗒 **top-sp1.bin**
┃ ┃ ┃ ┃ ┣ 🗒 **front-sp1.bin**
┃ ┃ ┃ ┃ ┣ 🗒 **neocd_sz.rom**
┃ ┃ ┃ ┃ ┣ 🗒 **neocd_z.rom**
┃ ┃ ┃ ┃ ┣ 🗒 **neocd_st.rom**
┃ ┃ ┃ ┃ ┣ 🗒 **neocd_t.rom**
┃ ┃ ┃ ┃ ┣ 🗒 **neocd_sf.rom**
┃ ┃ ┃ ┃ ┣ 🗒 **neocd_f.rom**
┃ ┃ ┃ ┃ ┣ 🗒 **000-lo.lo**
┃ ┃ ┃ ┃ ┣ 🗒 **ng-lo.rom**

## ![](/emulators/isos.svg) Isos

### Extensiones de roms soportadas

Las isos deben tener las extensiones siguientes:

* .bin/.cue
* .iso/.cue
* .img/.cue
* .chd

### Emplazamiento

Disponed vuestras isos como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 neogeocd
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.cue**
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.bin**

>Para más información sobre las roms, consultad [este tutorial](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

>Para conservar las configuraciones personalizadas cuando se produce una actualización del sistema Recalbox, os aconsejamos utilizar las [Sobrecargas de configuración](./../../../advanced-usage/configuration-override).
{.is-info}


### Acceder a las opciones

Podéis configurar las opciones del core de dos formas diferentes.

* Via el Menú RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via el fichero `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Opciones del core

| Opción | Valor de la opción | Variable (para la sobrecarga) | Valores posibles |
| --- | --- | --- | --- |
| Region | `Japan` ✅ / `USA` / `Europe` | `neocd_region` | `Japan` / `USA` / `Europe` |
| BIOS Select | `front-sp1.bin (Front Loader)` ✅ / `neocd.bin (CDZ)` / `neocd_f.rom (Front Loader)` / `neocd_sf.rom (Front Loader, SMKDan 0.07b)`, `neocd_st.rom (Top Loader, SMKDan 0.07b)` / `neocd_sz.rom (CDZ, SMKDan 0.07b)` / `neocd_t.rom (Top Loader)` / `neocd_z.rom (CDZ)` / `top-sp1.bin (Top Loader)` / `uni-bioscd.rom (CDZ, Universe 3.3)` | `neocd_bios` | `front-sp1.bin (Front Loader)` / `neocd.bin (CDZ)` / `neocd_f.rom (Front Loader)` / `neocd_sf.rom (Front Loader, SMKDan 0.07b)`, `neocd_st.rom (Top Loader, SMKDan 0.07b)` / `neocd_sz.rom (CDZ, SMKDan 0.07b)` / `neocd_t.rom (Top Loader)` / `neocd_z.rom (CDZ)` / `top-sp1.bin (Top Loader)` / `uni-bioscd.rom (CDZ, Universe 3.3)` |
| CD Speed Hack | `Activé` ✅ / `Désactivé` | `neocd_cdspeedhack` | `enabled` / `disabled` |
| Skip CD Loading | `Activé` ✅ / `Désactivé` | `neocd_loadskip` | `enabled` / `disabled` |
| Per-Game Saves (Restart) | `Désactivé` ✅ / `Activé` | `neocd_per_content_saves` | `disabled` / `enabled` |

## ![](/emulators/external-links.png) Enlaces externos

* **Código fuente utilizado** : [https://github.com/libretro/neocd_libretro/](https://github.com/libretro/neocd_libretro/)
* **Documentación Libretro** : -