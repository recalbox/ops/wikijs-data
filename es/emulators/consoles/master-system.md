---
title: Master System
description: Sega Master System
published: true
date: 2024-07-26T07:50:19.267Z
tags: sega, master, consolas
editor: markdown
dateCreated: 2023-09-03T19:25:23.345Z
---

![](/emulators/consoles/mastersystem.svg){.align-center}

## Detalles técnicos

* **Fabricante :** Sega
* **Año de lanzamiento :** 1985
* **Cantidad vendida :** 13 millones
* **Juego más vendido :** Alex Kidd in Miracle World
* **Procesador :** Zilog Z80 de 8 bits a 3,58 MHz
* **RAM :** 8kB / VRAM : 16kB
* **Vídeo:** Sega VDP (Texas Instruments TMS9918 modificado)
* **Resolución:** 256 × 192 píxeles, 32 colores (en una paleta de 64 colores)
* **Chip de sonido:** TI SN76489 PSG / módulo adicional YM2413 FM
* **Tamaño del cartucho:** 32kB - 1MB

## Presentación

La **Master System** o **Sega Master System** (también abreviada **SMS**) es una videoconsola de tercera generación diseñada y comercializada por el fabricante japonés Sega Enterprises, Ltd. (SSE). Salió a la venta en 1985, con el nombre de **Sega Mark III** en Japón. La consola fue rediseñada y rebautizada como Master System antes de su lanzamiento en 1986 en Norteamérica. Salió a la venta con este nombre en la mayoría de los demás territorios, incluidos Europa en 1986 y Brasil en 1987. La Master System rediseñada también salió a la venta en Japón en 1987.

Los dos modelos iniciales de la consola pueden leer cartuchos de juegos y Sega Cards, tarjetas de memoria que pueden almacenar juegos, vendidas a precios más bajos que los cartuchos pero con menos capacidad de almacenamiento; la Master System II y los modelos posteriores no tienen ranura para estas tarjetas. La Master System también es compatible con accesorios como una pistola óptica y gafas estereoscópicas diseñadas para funcionar con una serie de juegos especialmente codificados.

La Master System, sucesora de la SG-1000, se presentó como la competidora directa de la Nintendo Entertainment System (NES) en la era de las videoconsolas de tercera generación. La Master System se construyó con un hardware tecnológicamente superior al utilizado por la NES, pero no consiguió revertir la significativa ventaja de Nintendo en cuota de mercado en Japón y Norteamérica. Sin embargo, la consola tuvo más éxito en Europa y Brasil.

El hardware de la Master System comparte muchas similitudes con la consola portátil de Sega, la Game Gear. En comparación con su competidora de Nintendo, la biblioteca de juegos de la Master System carecía de varios títulos aclamados por la crítica debido a las prácticas de concesión de licencias de Nintendo, que restringían a los desarrolladores de terceros la creación de sus juegos exclusivamente para la NES. Según las estimaciones, se vendieron entre 18 y 21 millones de unidades de la consola durante su ciclo de vida. En comparación, la NES vendió 62 millones de unidades durante ese periodo. En retrospectiva, los observadores creen que la Master System permitió a Sega afianzarse en nuevos mercados, con vistas a comercializar la Mega Drive, pero se critica a la consola por no tener una biblioteca de juegos muy significativa.

Gracias a su uso por Tec Toy en Brasil, la Master System está considerada como la videoconsola más longeva.

## Emuladores

[Libretro FBNeo](libretro-fbneo)
[Libretro Gearsystem](libretro-gearsystem)
[Libretro GenesisPlusGX](libretro-genesisplusgx)
[Libretro PicoDrive](libretro-picodrive)