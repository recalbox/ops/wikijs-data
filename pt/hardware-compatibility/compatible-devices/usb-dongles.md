---
title: Dongles USB
description: Adaptadores Bluetooth
published: true
date: 2021-11-28T22:56:23.496Z
tags: usb, dongles, compatibilidade
editor: markdown
dateCreated: 2021-11-28T22:56:23.496Z
---

| Periférico | Status | Kernel Linux | Última versão testada | Comentários |
| :---: | :---: | :---: | :---: | :---: |
| Asus USB-BT400 Bluetooth 4.0 | ✅ |  |  | Funciona depois de fazer muitas personalizações no firmware Broadcom - não funciona por padrão. |
| AZiO BTD-V401 Bluetooth 4.0 | ✅ |  |  |  |
| AZiO BTD-V201 USB 2.0 Micro Bluetooth Adapter | ✅ |  |  |  |
| BCM2046B1 | ✅ |  |  |  |
| Belkin Mini Bluetooth v4.0 Adapter | ✅ | 4.0+ |  | Não funciona no Recalbox no XU4 devido ao kernel versão 3.10. |
| BigTec BIG121 | ❌ |  |  |  |
| Billionton USBBT02-B-DX | ✅ |  |  |  |
| Cambridge Silicon Radio ‪Bluetooth | ✅ | 3.0+ | v4.0.0 - rpi2B |  |
| CLiPtec USB Bluetooth Dongle Ver. 4.0 + EDR RZB939 | ✅ |  |  |  |
| CSL - V4.0 USB nano Adaptateur Bluetooth avec DEL Class 4.0 | ✅ |  |  |  |
| CSR Ltd Bluetooth Dongle (HCI mode) | ❌ |  |  |  |
| D-Link DBT-122 Dongle Bluetooth 1.2 | ✅ |  |  |  |
| Digitus Adapter tiny USB Bluetooth V 4.0 | ✅ |  |  |  |
| Dongle Bluegiga Technologies BLED112-V1 4.0 0dBm | ❌ |  |  |  |
| EMTEC USB Bluetooth dongle 100m | ✅ |  |  |  |
| Energy Sistem 1550T | ✅ |  |  |  |
| Essentiel B Micro dongle bluetooth 2.0 | ❌ |  |  |  |
| ESSENTIEL B Bluetooth  USB Bluetooth 4.0 | ✅ |  |  |  |
| Generic Bluetooth 4.0 dongle / adapter. USB 2.0 | ✅ |  |  | Não funciona no Recalbox no XU4 devido ao kernel versão 3.10. |
| ieGeek® Mini USB Bluetooth CSR V4.0 Dongle Dual Mode | ✅ |  |  |  |
| IOGEAR Bluetooth 4.0 USB Micro Adapter (GBU521) | ✅ |  |  |  |
| Inateck (BTA-BC4B6) Bluetooth 4.0+EDR | ✅ |  |  |  |
| Inateck USB Bluetooth 4.0 (B00N2E3ZHG) | ✅ |  |  |  |
| Insignia USB Bluetooth Adapter NS-PCY5BMA2 | ✅ |  |  | Funciona na versão 4.1 com um controle PS3.  |
| IT Works (DONGLE BT-150) USB Bluetooth 4.0 | ✅ |  |  |  |
| Lindy 52213 Dongle USB 3.0 WiFi Bluetooth | ✅ |  |  |  |
| LogiLink BT0006A | ❌ |  |  |  |
| Logilink BT0007A | ✅ |  |  | Com o controle PS3 DualShock 3 oficial, a captura de sinal Bluetooth parece ser muito curta |
| Maxxtro Bluetooth 4.0 USB adapter micro | ✅ |  |  |  |
| Nano USB to Bluetooth Dongle V2.0 (dynamode) | ❌ |  |  |  |
| ORICO BTA-402 USB Bluetooth 4.0 Micro Adapter (Model X) | ✅ |  |  |  |
| ORICO BTA-403 USB Bluetooth 4.0 | ✅ |  | Raspberry pi 3 | Testado com o controle PS3 cechzc2ua1 |
| PANRICH PBT06H | ✅ |  |  |  |
| Plugable Technologies USB-BT4LE | ✅ |  |  |  |
| Sitecom CN-512 v1 001 | ✅ |  |  |  |
| Sitecom CN-516 | ✅ |  | raspberry pi 3. V 4.0.2 | Testado com o controle 8bitdo zero |
| SoundBot SB340 | ✅ |  |  |  |
| Trendnet TBW-102UB | ✅ |  |  |  |
| Trendnet TBW-107UB | ✅ |  | 4.0.0-beta4 | Problemas de latência |
| Trust Ultra Small Bluetooth 2 USB Adapter 10m BT-2400p (15542) | ✅ |  | 4.0.0-beta |  |
| Trust nano Bluetooth universal 4.0 Adapter | ✅ |  |  |  |
| VIVANCO USB Bluetooth Dongle v4.0 (30447) | ✅ |  |  |  |