---
title: Dolphin
description: 
published: true
date: 2024-07-14T19:15:00.482Z
tags: wii, dolphin
editor: markdown
dateCreated: 2023-09-08T09:46:12.925Z
---

Dolphin es un emulador para las dos consolas de Nintendo más recientes: GameCube y Wii. Permite a los jugadores de PC disfrutar de juegos hechos para estas dos consolas en **full HD** (1080p) con varias mejoras: compatibilidad con todos los mandos de PC, velocidad turbo, juego en red online, ¡y mucho más!

Más de 200 personas han trabajado duro durante años para crear Dolphin. La lista de colaboradores puede consultarse [en GitHub](https://github.com/dolphin-emu/dolphin/graphs/contributors).

## ![](/emulators/license.svg) Licencia

El core posee la siguiente licencia [**GPLv2**](https://github.com/dolphin-emu/dolphin/blob/master/license.txt).

## ![](/emulators/compatibility.png) Compatibilidad

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ❌ | ❌ | ❌ | ❌ | ❌ | ❌ | ✅ | ❌ |

## ![](/emulators/features.png) Funcionalidades

| Funcionalidad | ¿Soportada? |
| :---: | :---: |
| Netplay | ✔ |

## ![](/emulators/bios.svg) BIOS

>**No necesita bios.**
{.is-success}

## ![](/emulators/isos.svg) Isos

### Extensiones de roms soportadas

Las roms deben tener las extensiones siguientes:

* .ciso
* .iso
* .rvz
* .wbfs

### Emplazamiento

Disponed vuestras isos como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 wii
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.iso**

>Se aconsejan las isos del proyecto **Redump**.
{.is-success}

>Para más información sobre las isos, consultad [este tutorial](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

Dolphin funciona con roms en los siguientes formatos:

* **GCM**/**ISO** (volcados sin comprimir ~4.7 GB para un juego de Wii)
* **WBFS** (no compatible con Dolphin 3.0 o anterior)
* **WAD** (minijuegos Wiiware)

>Tened en cuenta que WBFS y CISO son formatos de compresión con pérdida: eliminan información no utilizada de los discos para hacerlos más pequeños. Por lo tanto, os recomendamos que probéis a volcar vuestros juegos en formato GCM/ISO, si tienes problemas con un volcado WBFS.
{.is-warning}

## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

>Para conservar las configuraciones personalizadas cuando se produce una actualización del sistema Recalbox, os aconsejamos utilizar las [Sobrecargas de configuración](./../../../advanced-usage/configuration-override).
{.is-info}

### Opciones del core

## ![](/emulators/external-links.png) Enlaces externos

* **Código fuente utilizado** : [https://github.com/dolphin-emu/dolphin/](https://github.com/dolphin-emu/dolphin)
* **Página oficial** : [https://www.dolphin-emu.org/](https://www.dolphin-emu.org/)