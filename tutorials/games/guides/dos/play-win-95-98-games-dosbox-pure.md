---
title: Play to Windows 95 & 98 games with Dosbox Pure
description: Play to your old Windows games!
published: true
date: 2025-03-13T19:29:58.998Z
tags: dosbox pure, 9.0+, windows95, win95, win98, windows98
editor: markdown
dateCreated: 2023-02-26T18:45:17.085Z
---

## Introduction

With Recalbox 9.0, you can now play Windows 95 and 98 games! This tutorial will describe how to do it.

You must have the following prerequisites:

- Windows 95/98 in CD image format
- 1 keyboard
- 1 mouse
- The game you want to use in a .zip or .7z compressed file

## Installation

For the following demonstration, we are going to do it with Windows 95 OSR 2.5 (native USB support at that time) image compressed in a zip file and Wolfenstein CD iso compressed in a zip file.

This installation is done in 2 steps. The first one is the installation of Windows itself, and the second one is the starting of your game with your Windows installation.

### Windows installation

- Put the zipped Windows CD image and your zipped game in the `dos` folder of your Recalbox and update the game list.
- Launch your Windows with the `Libretro-DOSbox_Pure` core
- Once launched, choose the option `[ Boot and Install New Operating System ]`.

![](/tutorials/games/guides/dos/w95-1-fr.png)

- It will propose you to create an installation in an image located in your bios. You just have to choose the size that the Windows installation will take.

![](/tutorials/games/guides/dos/w95-2-fr.png)

>Be careful to choose the right size, it must be able to hold your installed games if they are to be installed!
{.is-warning}

>Keep an eye on your screen, because once the creation of the disk space defined for this installation has been completed, the Windows installation executable takes over without delay, and waits for a choice on your part (boot from CD-ROM, choice / option 2) within 5 to 10 seconds maximum!
{.is-warning}

- Once you have made your choice, a few seconds will pass without displaying anything and your Windows will start.

![](/tutorials/games/guides/dos/w95-3-fr.png)

- Continue the installation of Windows with its restarts until you are on the desktop once all the installations are done.

> Don't forget that you need a license key to install it !
{.is-info}

![](/tutorials/games/guides/dos/w95-4-fr.png)

- Once all the Windows installation is done, you can shut down by doing `Start` > `Shut Down`. Once you are told that you can shut down, press `HOTKEY` + `START` on your controller to return to the frontend.

> If you do `HOTKEY` + `START` while Windows is displayed, you will have to fix it the next time you start up!
{.is-info}

### Installing your game

- In the list of your DOS games, launch the game to be installed on your previously created Windows virtual image.
- Once launched, select `[ Run Installed Operating System ]`.

![](/tutorials/games/guides/dos/w95-5-fr.png)

- The list of available Windows will be listed. Choose the one you want and validate.

![](/tutorials/games/guides/dos/w95-6-fr.png)

- Windows will launch by mounting your game as a CD in Windows Explorer.

![](/tutorials/games/guides/dos/w95-7-fr.png)

- All you have to do is launch the installation program of your game to be able to play it.

![](/tutorials/games/guides/dos/w95-8-fr.png)