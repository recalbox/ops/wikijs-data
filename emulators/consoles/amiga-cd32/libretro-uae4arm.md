---
title: Libretro UAE4ARM
description: 
published: true
date: 2024-07-24T09:16:24.470Z
tags: libretro, amiga-cd32, amiga, uae4arm, cd32, 8.0+
editor: markdown
dateCreated: 2021-08-02T18:47:45.744Z
---



## ![](/emulators/license.svg) License

This core is under **GPLv2** license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ❌ | ✅ |

## ![](/emulators/features.png) Features

| Feature | Supported |
| :---: | :---: |
| Restart | ✔ |
| Screenshots | ✔ |
| Saves | ✔ |
| States | ✔ |
| Rewind | ✔ |
| Core Options | ✔ |
| Controls | ✔ |
| Remapping | ✔ |
| Multi-Mouse | ✔ |
| Disk Control | ✔ |
| LEDs | ✔ |

## ![](/emulators/bios.svg) BIOS

### List of mandatory bios

You will find the bios with the name indicated in the column Description that you will have to rename with the name indicated in the column Filename.

| Filename | Description | MD5 | Provided |
| :---: | :---: | :---: | :---: |
| kick40060.CD32 | Kickstart v3.1 r40.060 (1993-05)(Commodore)(CD32)\[!\].rom | f2f241bf094168cfb9e7805dc2856433 5f8924d013dd57a89cf349f4cdedc6b1 | ❌ |
| kick40060.CD32.ext | CD32 Extended-ROM r40.60 (1993)(Commodore)(CD32).rom | bb72565701b1b6faece07d68ea5da639 | ❌ |

### Location

Put the bios like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 🗒 **kick40060.CD32**
┃ ┃ ┃ ┣ 🗒 **kick40060.CD32.ext**

## ![](/emulators/isos.svg) Isos

### Supported extensions

Isos must have the extension:

* .bin/.cue
* .iso
* .mds/.mdf
* .img/.ccd
* .nrg
* .chd
* .m3u
* .uae

### Lcoation

Put the isos like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 cd32
┃ ┃ ┃ ┃ ┣ 🗒 **game.cue**
┃ ┃ ┃ ┃ ┣ 🗒 **game.bin**

>The isos using **Redump** format are highly recommended.
{.is-success}

>For more information about isos, go to [this tutorial](./../../../tutorials/games/generalities/isos-and-roms)!
{.is-info}

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Accessing to options

You can configure various options in two different ways.

* Via RetroArch menu:

┣ 📁 RetroArch Menu
┃ ┣ 📁 Core options
┃ ┃ ┣ 🧩 Name_option

* Via the file `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core options

| Options | Options values | Variables (for configuration override) | Variables values |
| --- | --- | --- | --- |
| Model | `Auto` ✅ / `A500` / `A600` / `A1200` / `CDTV` / `CD32` | `uae4arm_model` | `Auto` / `A500` / `A600` / `A1200` / `CDTV` / `CD32` |
| Fast Mem | `None` ✅ / `1 MB` / `2 MB` / `4 MB` / `8 MB` | `uae4arm_fastmem` | `None` / `1 MB` / `2 MB` / `4 MB` / `8 MB` |
| Internal resolution | `640x270` ✅ / `320x240` / `320x256` / `320x262` / `640x240` / `640x256` / `640x262` / `640x270` / `768x270` | `uae4arm_resolution` | `640x270` / `320x240` / `320x256` / `320x262` / `640x240` / `640x256` / `640x262` / `640x270` / `768x270` |
| Leds on screen | `Enabled` ✅ / `Disabled` | `uae4arm_leds_on_screen` | `enabled` / `disabled` |
| Floppy speed | `100` ✅ / `200` / `400` / `800` | `uae4arm_floppy_speed` | `100` / `200` / `400` / `800` |
| Line doubling (de-interlace) | `Disabled` ✅ / `Enabled` | `uae4arm_linedoubling` | `disabled` / `enabled` |
| whdload mode | `files` ✅ / `hdfs` | `uae4arm_whdloadmode` | `files` / `hdfs` |
| fast copper | `Disabled` ✅ / `Enabled` | `uae4arm_fastcopper` | `disabled` / `enabled` |

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/Chips-fr/uae4arm-rpi/](https://github.com/Chips-fr/uae4arm-rpi/)