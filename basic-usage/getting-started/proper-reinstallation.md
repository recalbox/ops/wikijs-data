---
title: Proper reinstallation
description: 
published: true
date: 2024-11-11T07:50:07.372Z
tags: proper, reinstallation
editor: markdown
dateCreated: 2021-06-28T23:00:23.117Z
---

## What should I do before a clean reinstallation of the system?

### 1. I do not have removable media, only the microsd card

* Always have a copy of your roms, saves, bios on your PC or on a backup disk.
* Make a backup of the Recalbox folder containing the roms, bios, saves on your PC.
* Download and reinstall the [latest version of Recalbox](https://download.recalbox.com/) on your microsd card.

#### The bios

* Check the signature of your bios in the frontend.
* Recalbox adds new emulators that sometimes need bios.

### 2. I have a removable media (usb key/external usb hard drive)

* The exFAT format is recommended for large capacity hard drives.
* Plug the external hard drive or USB key into your PC, then rename the `recalbox` folder to `recalbox.old`.
* Download and install [the latest version of Recalbox](https://www.recalbox.com/download/stable/) on your microSD card.
* Connect your external hard drive or USB key to your board.
* Choose your removable media for your media.
* The Recalbox folder tree will be created again on your external device.
* Turn off your Recalbox.
* Connect your external hard drive or USB key to your PC.
* Move ONLY ROMS, BIOS AND SAVE.
* **DO NOT move gamelist/scrap, custom theme and OVERLAYS** (responsible for many bugs).
* Test your Recalbox with the default settings.
* If all is well, you can start customizing your settings, gradually.

## Clean reinstallation of the system

Follow the guide [Preparation and Installation](./../preparation-and-installation)