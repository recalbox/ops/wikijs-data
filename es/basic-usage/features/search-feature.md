---
title: Función de búsqueda
description: 
published: true
date: 2023-08-20T16:26:23.489Z
tags: busqueda
editor: markdown
dateCreated: 2023-08-20T16:25:17.231Z
---

## Presentación

La interfaz de búsquedas de Recalbox ha sido muy reclamada por los usuarios desde el principio, y finalmente ha sido implementada!

_**Filtrad rápidamente todos vuestros juegos mediante la función de búsqueda escribiendo letra a letra...**_

Se trata de una búsqueda en tiempo real. La lista de resultados se refresca a medida que se escriben letras. La búsqueda se realiza en los nombres de los juegos, en los nombres de los ficheros, en las descripciones, o en todas partes al mismo tiempo.

Por supuesto, no podíamos proponer esta funcionalidad utilizando el teclado virtual existente.

Cuando estáis frente a vuestra Recalbox, con el mando de juegos en las manos, no hay nada más penible que tener que enchufar un teclado para realizar una búsqueda.

Para facilitar las cosas al máximo, hemos creado un nuevo teclado virtual mucho más ergonómico que el anterior: un teclado como en los juegos de arcade, con una rueda de caracteres donde se pueden seleccionar las letras, borrar y desplazarse de una forma extremadamente sencilla, un verdadero placer!

No tiene sentido explicar aqui todos los detaller de la nueva intefaz, pero que sepáis que casi todos los botones/dpad/joystick del mando pueden utilizarse con la rueda. Por supuesto, el sistema también es compatible con un teclado físico. Para completar el círculo, la rueda es semi-transparente, lo que permite observar los resultados que se muestran en segundo plano sin tener que cerrarla.

Sin más espera, aquí os dejamos algunas imágenes!

![](/basic-usage/features/search.png)

## Funcionamiento

* La funcionalidad de búsqueda se activa utilizando `R1` desde la lista de todas las consolas.
* Comenzad por escribir el nombre de un juego (por ejemplo : "Super")
* Observad cómo en la lista de resultados en segundo plano aparecen todos los juegos que contienen vuestra búsqueda.
* Sólo tenéis que seleccionar el juego que buscábais en la lista para lanzarlo o añadirlo a vuestros favoritos.
