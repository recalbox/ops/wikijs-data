---
title: YouTube add-on configuration
description: Or how to watch YouTube videos without problems!
published: true
date: 2023-02-21T19:57:36.340Z
tags: configuration, kodi, youtube, add-ons
editor: markdown
dateCreated: 2022-12-20T22:14:40.618Z
---

## Introduction

In Kodi, the YouTube extension is already pre-installed. However, you need to configure it to be able to identify yourself and find all your account (favorite videos, subscriptions, etc.).

## Procedure

### Pre-requisites

- Have a Google / YouTube account.
- Have Recalbox 9.0. Doing it on a previous version **may** work but is not 100% guaranteed.

### Configuration

#### Creation of API / OAuth keys

##### Creation of the project

1. Go to https://console.cloud.google.com/ and click on 'New Project'. This button is at the top left of the window.

>If you already have projects, click on the list of projects.
>{.is-info}

2. Click on `New Project`.

3. Enter the name of the project and click `Create`. You can give it any name you like.

![](/tutorials/kodi/youtube/ytb1en.png){.full-width}

##### Add the YouTube library

4. Once the project is created, click on the icon with the 3 horizontal lines to open the menu, then go to `API and Services` > `Library`.

![](/tutorials/kodi/youtube/ytb2en.png){.full-width}

5. On this page, select the project you created earlier and search for `YouTube Data API v3` and click on it.

![](/tutorials/kodi/youtube/ytb3en.png){.full-width}

6. On the library page, click `Enable`.

![](/tutorials/kodi/youtube/ytb4en.png){.full-width}

##### Generate the API key

7. Open the menu in Step 4 and go to `API and Services` > `Identifiers`.

![](/tutorials/kodi/youtube/ytb5en.png){.full-width}

8. In the list of credentials at the top of the page, click `Create Credentials` > `API Key`.

![](/tutorials/kodi/youtube/ytb6en.png){.full-width}

9. A popup will appear after a few seconds giving you a generated API key. Copy this API key and keep it aside.

![](/tutorials/kodi/youtube/ytb7en.png){.full-width}

##### OAuth consent screen

10. On the left hand side, click `OAuth Consent Screen`.

![](/tutorials/kodi/youtube/ytb8en.png){.full-width}

11. Specify the `External` user type and click `Create`.

![](/tutorials/kodi/youtube/ytb9en.png){.full-width}

12. Here, fill in the `application name`, `user support email address` and an `email address` at the bottom of the page and click `Save and Continue`.

![](/tutorials/kodi/youtube/ytb10en.png){.full-width}

>The 2 email addresses can be different.
>{.is-info}

13. Return to the `OAuth consent screen` and create a test user by clicking `Add users`, then entering an email address.

![](/tutorials/kodi/youtube/ytb11en.png){.full-width}

14. On the same page, don't forget to click `PUBLISH APP`.

![](/tutorials/kodi/youtube/ytb12en.png){.full-width}

##### Generate the OAuth key

15. On the left hand side, click `Credentials` then, on top of the page, click `Create Credentials` then select `OAuth client ID` this time.

![](/tutorials/kodi/youtube/ytb13en.png){.full-width}

16. You need to configure the application type to obtain the OAuth client ID. Select `TVs and Limited Input devices` and click `Create`.

![](/tutorials/kodi/youtube/ytb14en.png){.full-width}

>The given name of the app on the same page can be edited or left as is.
>{.is-info}

![](/tutorials/kodi/youtube/ytb15en.png){.full-width}

17. You will get the `Client ID` and the `Client Secret` code. Keep these aside too.

![](/tutorials/kodi/youtube/ytb16en.png){.full-width}

#### Setting up YouTube in Kodi

>The following steps can differ if you start Kodi or YouTube for the first time.
>{.is-info}

1. Start Kodi.

2. Go to `Extensions` > `YouTube`.

![](/tutorials/kodi/youtube/ytb17en.png){.full-width}

3. In the list on the screen, choose `Settings` at the bottom.

![](/tutorials/kodi/youtube/ytb18en.png){.full-width}

4. In the settings, choose `API` on the left.

![](/tutorials/kodi/youtube/ytb19en.png){.full-width}

5. In the middle, edit each line as follows:

    1. API key: the API key you generated first.

    2. API ID: the client ID you got second. Do not enter the `.apps.googleusercontent.com` part, it is not used.

    3. API secret: the client secret code you got last.

6. Once you have entered all 3 pieces of information, click `OK` in the settings window.

7. A popup will ask if you want to run the configuration wizard. Select `Yes`.

![](/tutorials/kodi/youtube/ytb20en.png){.full-width}

8. Select your language and region.

![](/tutorials/kodi/youtube/ytb21en.png){.full-width}

9. Select if you would like to use geolocation.

![](/tutorials/kodi/youtube/ytb22en.png){.full-width}

10. Choose `Video suggestions`. If a list of videos is visible, everything should work after your identification is done.

![](/tutorials/kodi/youtube/ytb23en.png){.full-width}

#### Identification

1. Choose `Log in`. A popup will tell you to log in twice.

![](/tutorials/kodi/youtube/ytb24en.png){.full-width}

2. Go to https://google.com/device and give the first code that Kodi will give you.

![](/tutorials/kodi/youtube/ytb25en.png){.full-width}

3. On your browser, choose your YouTube account and confirm the permissions.

![](/tutorials/kodi/youtube/ytb26en.png){.full-width}

4. Once the first validation is done, you have to go back to the same address with a second code that Kodi will give you and follow the same procedure.

![](/tutorials/kodi/youtube/ytb27en.png){.full-width}

>You will get a window telling you that the page is dangerous. Continue anyway to finish identifying yourself.
>{.is-info}

5. Once these 2 validations are done, you are connected and can enjoy YouTube on Kodi without any ads!

![](/tutorials/kodi/youtube/ytb28en.png){.full-width}