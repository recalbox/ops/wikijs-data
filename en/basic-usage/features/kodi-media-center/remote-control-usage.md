---
title: Remote control usage
description: 
published: true
date: 2021-08-08T19:07:55.302Z
tags: kodi, control, remote
editor: markdown
dateCreated: 2021-08-03T23:01:25.123Z
---

The Recalbox can be controlled via any Infrared remote control. This feature is for Kodi.

It will cost you about 2€ and is very simple to set up.

## I - Requirements

### A - Infrared receiver

To work, you will need an IR receiver like a 38KHz TSOP4838 module; note that it can also work with some other components. You can buy one on the internet for about 1€ at any electronics store or online.

![Récepteur IR](/basic-usage/features/kodi/ir-control/ir1.jpg)

### B - Dupont female/female cables

In order to easily connect your receiver without soldering, you will need 3 dupont F/F cables. You can get them from the same retailers as your receiver for a few cents.

![Câbles dupont F/F](/basic-usage/features/kodi/ir-control/ir2.jpg)

### C - Diagram

To connect the receiver and the cables, follow the diagram below:

![](/basic-usage/features/kodi/ir-control/ir3.png)

Example of assembly :

![](/basic-usage/features/kodi/ir-control/ir4.jpg)

### D - Remote control

Almost all remote controls are supported, provided they use standards.

All these remotes have been successfully tested:

* A remote Philips hifi.
* A Samsung remote control of a video recorder.
* A Universal remote control.
* A remote control of a MAC computer.

![](/basic-usage/features/kodi/ir-control/ir5.jpg)

## II - Configuration

### A - config.txt

* Open the file [recalbox-user-config.txt](./../../../tutorials/system/modification/configtxt-configuration) via [SSH](./../../../tutorials/system/access/root-access-terminal-cli).
* Remember to enable read-write mode on the [boot partition](./../../../tutorials/system/access/remount-partition-with-write-access)
* Uncomment the following line by removing the `#`:

```ini
#dtoverlay=lirc-rpi
```

Becomes:

```ini
dtoverlay=lirc-rpi
```

* Restart your Recalbox.

### B - Remote control configuration

#### 1. Checking the IR events

Let's check that the hardware is working:

* Connect to your Recalbox via [SSH](./../../../tutorials/system/access/root-access-terminal-cli)
* Run the following command:

```shell
lsmod
```

You should see a line starting with `lirc_rpi`.

![](/basic-usage/features/kodi/ir-control/ir6.png)

* Then run the following command:

```shell
mode2 -d /dev/lirc0
```

* Each time you press a button on your remote control in front of your receiver, you should see numbers appear on the terminal. If this is the case, both the remote and the receiver are working properly.
* Press `Ctrl`+`C` to exit.

![](/basic-usage/features/kodi/ir-control/ir7.png)

#### 2. Register your remote control

* Launch the command :

```shell
irrecord -H /recalbox/share/custom.conf
```

* Press `Enter` to continue.
* Enter `customremote` as the remote name and press `Enter` to continue.

![](/basic-usage/features/kodi/ir-control/ir8.png)

* Now start pressing the keys on your remote for about a second.

![](/basic-usage/features/kodi/ir-control/ir9.png)

* Enter the name of your key from the names below and press the key on the remote control:

  * KEY_DOWN (Down)
  * KEY_EXIT (Back/Quit)
  * KEY_INFO (Informations about current playing media)
  * KEY_LEFT (Left)
  * KEY_MENU (Menu)
  * KEY_MUTE (Mute)
  * KEY_OK (Ok)
  * KEY_PLAY (Play and Pause)
  * KEY_POWER (Quit)
  * KEY_RIGHT (Right)
  * KEY_STOP (Stop)
  * KEY_UP (Up)
  * KEY_VOLUMEDOWN (Volume -)
  * KEY_VOLUMEUP (Volume +)

* And eventually:

  * KEY_AUDIO
  * KEY_BLUE
  * KEY_CHANNELDOWN
  * KEY_CHANNELUP
  * KEY_DELETE
  * KEY_ENTER
  * KEY_EPG
  * KEY_FASTFORWARD
  * KEY_GREEN
  * KEY_LANGUAGE
  * KEY_MEDIA
  * KEY_NEXT
  * KEY_NUMERIC_1
  * KEY_NUMERIC_2
  * KEY_NUMERIC_3
  * KEY_NUMERIC_4
  * KEY_NUMERIC_5
  * KEY_NUMERIC_6
  * KEY_NUMERIC_7
  * KEY_NUMERIC_8
  * KEY_NUMERIC_9
  * KEY_NUMERIC_0
  * KEY_PAUSE
  * KEY_PREVIOUS
  * KEY_PVR
  * KEY_RADIO
  * KEY_RECORD
  * KEY_RED
  * KEY_REWIND
  * KEY_SUBTITLE
  * KEY_VIDEO
  * KEY_YELLOW
  * KEY_ZOOM

![](/basic-usage/features/kodi/ir-control/ir10.png)

* Repeat for all or most of the buttons on your remote.

To set up a button again, retype its name and start again.

![](/basic-usage/features/kodi/ir-control/ir11.png)

* When you've saved all the keys you want, press `Enter` to continue.
* Then press one of the keys very quickly without holding it down and without changing keys.

![](/basic-usage/features/kodi/ir-control/ir12.png)

* At the end, the program is automatically closed.
* If you want to start again, delete the configuration file with the command :

```shell
rm /tmp/custom.conf
```

* And restart `irrecord`.

#### 3. Configuration file

* Then move the file with the following command to replace the `lircd.conf` file with your :

```shell
mv /recalbox/share/custom.conf /recalbox/share/system/.config/lirc/lircd.conf
```

* Restart the `lircd` service with the command:

```shell
/etc/init.d/S25lircd restart
```

![](/basic-usage/features/kodi/ir-control/ir13.png)

#### 4. Verify that the setup works

* Type the command `irw`.
* Each time you press a key, you should see a line appear with the name of the key.
* Press `Ctrl` + `C` to exit.

![](/basic-usage/features/kodi/ir-control/ir14.png)

* Start Kodi and check the operation.

## III - Advanced configuration

### A - Lircmap.xml

* You can customize the remote control key settings by editing the `/recalbox/share/system/.kodi/userdata/Lircmap.xml` file.

>If you have manually deleted the `~/.kodi` folder and restarted Kodi without first restarting your Recalbox, you will not see the customizations in Kodi or the `Lircmap.xml` file.
{.is-warning}

* Consider copying `/recalbox/share_init/system/.kodi/userdata/Lircmap.xml` to `/recalbox/share/system/.kodi/userdata/Lircmap.xml`

### B - remote.xml

You can change the definition of keys and actions in the file `/recalbox/share/system/.kodi/userdata/keymaps/remote.xml`.

### C - QUIT / STOP

This is for people who don't like the fact that the `Return` button doesn't stop a movie in Kodi or because you only have a single Stop / Return button on your remote.

You can change in the remote.xml file the section "", "Back" by "Stop".

### D - VOLUME UP/DOWN

>This item is for CEC remotes.
{.is-info}

If your CEC remote control does not transfer volume commands, you can use other keys by replacing for example in remote.xml in the global section :

```xml
  <skipplus>SkipNext</skipplus>
  <skipminus>SkipPrevious</skipminus>
```

by

```xml
  <skipplus>VolumeUp</skipplus>
  <skipminus>VolumeDown</skipminus>
```

### E - PAUSE on OK

On the Refocus skin to make pausing easier (mainly on an Apple remote), you can modify the file `~/.kodi/addons/skin.refocus/720p/VideoOSD.xml` by replacing :

```xml
  <defaultcontrol always="true">700</defaultcontrol>
```

By this:

```xml
  <defaultcontrol always="true">705</defaultcontrol>
```