---
title: Compatibilité des jeux
description: 
published: true
date: 2021-09-04T23:14:16.654Z
tags: jeux, compatibilité 32x
editor: markdown
dateCreated: 2021-07-30T11:06:31.003Z
---

## Introduction

Certains jeux du 32X peuvent avoir des soucis d'émulation qui son listés ci-dessous.

## Tableau de compatibilité

| Jeu | Problèmes connus |
| :--- | :--- |
| Brutal Unleashed – Above the Claw | Softlock après le premier combat. |
| FIFA Soccer ’96 | Texte du menu principal avec des glitches. |
| Knuckles’ Chaotix | Graphismes avec glitches sur l'écran de sélection du joueur. |
| NBA Jam Tournament Edition | Problèmes de framerate. |
| NFL Quarterback Club | Certains graphismes du menu sont manquants. |
| Virtua Racing Deluxe | Ligne clignotante pendant l'affichage du logo SEGA. |
| World Series Baseball Starring Deion Sanders | Plante quand un match commence. |
| WWF Raw | Certains graphismes sont manquants. |