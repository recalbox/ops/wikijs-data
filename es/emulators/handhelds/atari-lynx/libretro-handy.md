---
title: Libretro Handy
description: 
published: true
date: 2024-07-19T15:09:35.587Z
tags: libretro, atari, lynx, handy
editor: markdown
dateCreated: 2023-09-11T12:14:30.791Z
---

**Libretro Handy** es un emulador del sistema de videojuegos Atari Lynx que puede utilizarse como core de libretro.  
Handy era el nombre original del proyecto Lynx, que fue lanzado por Epyx y posteriormente finalizado por Atari.

## ![](/emulators/license.svg) Licencia

El core posee la siguiente licencia [**zlib**](https://sourceforge.net/projects/handy/).

## ![](/emulators/compatibility.png) Compatibilidad


| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | 

## ![](/emulators/features.png) Funcionalidades

| Funcionalidad | ¿Soportada? |
| :---: | :---: |
| Reinicio | ✔ |
| Capturas de pantalla | ✔ |
| Grabaciones instantáneas | ✔ |
| Rebobinado | ✔ |
| Netplay (State based) | ✔ (no hay emulación de cable link) |
| Opciones del core | ✔ |
| RetroAchievements | ✔ |
| Controles | ✔ |
| Remapping | ✔ |

## ![](/emulators/bios.svg) BIOS

### Lista de BIOS obligatorias

| Nombre del fichero | Descripción | MD5 | ¿Preinstalada? |
| :---: | :---: | :---: | :---: |
| lynxboot.img | Image Boot Lynx | fcd403db69f54290b51035d82f835e7b | ❌  |

### Emplazamiento

Disponed vuestras bios como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 lynx
┃ ┃ ┃ ┃ ┣ 🗒 **lynxboot.img**

## ![](/emulators/roms.png) Roms

### Extensiones de roms soportadas

Las roms deben tener las extensiones siguientes:

* .lnx
* .o
* .zip
* .7z

Este sistema soporta las roms comprimidas en formato .zip/.7z. No obstante, se trata sólo de un archivo.

Los ficheros contenidos en los .zip/.7z deben tener las extensiones citadas previamente.
Además, cada fichero .zip/.7z sólo debe contener un fichero rom al interior.

### Emplazamiento

Disponed vuestras roms como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 lynx
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.lnx**

>Se aconsejan las roms del proyecto **No-Intro**.
{.is-success}

>Para más información sobre las roms, consultad [este tutorial](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

>Para conservar las configuraciones personalizadas cuando se produce una actualización del sistema Recalbox, os aconsejamos utilizar las [Sobrecargas de configuración](./../../../advanced-usage/configuration-override).
{.is-info}

### Acceder a las opciones

Podéis configurar las opciones del core de dos formas diferentes.

* Via el Menú RetroArch :


┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via el fichero `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Opciones del core

| Opción | Valor de la opción | Variable (para la sobrecarga) | Valores posibles |
| --- | --- | --- | --- |
 | Video Refresh Rate | `50Hz` / `60Hz` ✅ / `75Hz` / `100Hz` / `125Hz` | `handy_refresh_rate` | `50` / `60` / `75` / `100` / `125` |
| Rotation d'écran | `Auto` ✅ / `Désactivé` / `Horaire` / `Bottom` / `Anti-horaire` | `handy_rot` | `Auto` / `None` / `90` / `180` / `270` |
| Profondeur des couleurs (Redémarrage requis) | `Milliers (16 bits)` ✅ / `Millions (24 bits)` | `handy_gfx_colors` | `16bit` / `24bit` |
| Filtre de rémanence LCD | `Désactivé` ✅ / `2 Frames` / `3 Frames` / `4 Frames` | `handy_lcd_ghosting` | `disabled` / `2frames` / `3frames` / `4frames` |
| CPU Overclock Multiplier | `1` ✅ / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `20` / `30` / `40` / `50` | `handy_overclock` | `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `20` / `30` / `40` / `50` |
| Saut d'images | `Désactivé` ✅ / `Auto` / `Manuel` | `handy_frameskip` | `disabled` / `auto` / `manual` |
| Seuil de saut d'images (%) | `15` / `18` / `21` / `24` / `27` / `30` / `33` ✅ / `36` / `39` / `42` / `45` / `48` / `51` / `54` / `57` / `60` | `handy_frameskip_threshold` | `15` / `18` / `21` / `24` / `27` / `30` / `33` / `36` / `39` / `42` / `45` / `48` / `51` / `54` / `57` / `60` |



## ![](/emulators/external-links.png) Enlaces externos

* **Código fuente utilizado** : [https://github.com/libretro/libretro-handy](https://github.com/libretro/libretro-handy)
* **Documentación Libretro** : [https://docs.libretro.com/library/handy/](https://docs.libretro.com/library/handy/)
* **Código fuente oficial** : [http://handy.sourceforge.net/download.htm](http://handy.sourceforge.net/download.htm)
* **Site officiel** :  [http://handy.sourceforge.net/](http://handy.sourceforge.net/)