---
title: FAQ
description: 
published: true
date: 2023-09-21T13:23:47.422Z
tags: faq
editor: markdown
dateCreated: 2023-09-21T13:23:47.422Z
---

### Configuración de botones

### Introducción

Las teclas y otros botones de la TI-99/4A pueden resultar difíciles de manejar.

### Utilización

Esta es la correspondencia de las teclas para el teclado:

| Botón Joystick | Botón Sistema |
| :--- | :--- |
| `L1` | (selección de arranque)
| `R1` | 2 (selección en el arranque) |
| `X` | 3 (selección en el arranque) |
| `Y` | 4 (selección en el arranque)
| `START` | `Enter` | |
| `B` | `Espacio` |

Para el joystick, vuestro stick izquierdo funcionará con el botón `A`.

>Un fichero `.P2K` se suministra de serie con el emulador.
{.is-success}