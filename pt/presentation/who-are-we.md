---
title: Quem somos nós?
description: 
published: true
date: 2021-06-25T00:19:14.310Z
tags: 
editor: markdown
dateCreated: 2021-06-25T00:12:03.777Z
---

## Apresentação

**O Recalbox** é um console de **código aberto** que lhe permite jogar uma variedade de consoles e plataformas em sua sala de estar, com facilidade!

**Recalbox** é o **sistema operacional** do projeto Recalbox, um **console de emulação pronto para o uso** baseado no **Raspberry Pi** que é totalmente **livre e de código aberto**, e foi projetado especificamente para que **você possa criar o seu próprio em pouco tempo**.

O Recalbox oferece **uma ampla seleção de consoles e sistemas**, desde os primeiros terminais arcade, até plataformas como o NES, o Megadrive e o Playstation.

**Recalbox** utiliza vários elementos existentes, tais como [EmulationStation](https://github.com/Aloshi/EmulationStation) como **interface**, [piFBA](https://github.com/digitalLumberjack/pifba) e [RetroArch](https://github.com/libretro/RetroArch) como **emuladores**.

**Recalbox** pode ser descrito em uma palavra: **FÁCIL**.
Queremos fornecer um sistema que seja o mais fácil de usar e intuitivo possível.  
Não há necessidade de modificar uma infinidade de arquivos de configuração ou conectar via SSH.
Basta instalar o sistema e jogar.
A configuração do sistema é feita através de um único arquivo, chamado `recalbox.conf`.   
Este arquivo permite o ajuste específico de todos os emuladores!

Mas para **usuários avançados** que desejam ter uma **configuração mais avançada**, nós lhes demos a possibilidade de fazer um sistema a seu gosto com **sobrecarga de configurações** (modificar somente se você tiver certeza do que está fazendo).

### Dezenas de software de código aberto...... combinados em um sistema.

**Recalbox** baseia-se no sistema **GNU/Linux** com *Buildroot*.  
Ele integra centenas de softwares de código aberto, desde o menor utilitário, até seus emuladores e sua interface EmulationStation.

Encontre as dezenas de emuladores do RetroArch/Libretro, o famoso media player Kodi, e a interface personalizada do EmulationStation!

![](/presentation/opensource.png)

#### Com homebrews e hacks, descubra jogos nunca lançados no console!

*Homebrews* são jogos criados a partir do zero por desenvolvedores. Muitos deles são muito boas surpresas e farão com que você se divirta muito.

Os *hacks* são modificações dos jogos existentes. Eles pegam toda ou parte dos gráficos e jogabilidade de um jogo, e modificam a história, a dificuldade, a atmosfera, etc.
Alguns são considerados como sequências completas dos jogos originais, tais como Super Mario Bros 4 (hack do Super Mario World) ou Legend Of Zelda - Parallels Worlds (hack de Legend Of Zelda - A Link to The Past).

![](/presentation/zeldaparallelsworlds.png)

### Central multimídia

Através do **Kodi**, o Recalbox também atua como **Central Multimídia**.  
Ao conectá-lo à rede doméstica, você pode reproduzir vídeos de todos os seus dispositivos compatíveis (modem, computador, etc).

### Características

* Sistema :
  * [Sistemas suportados](./../hardware-compatibility/emulators-compatibility)
  * Versão otimizada do FBA com suporte a 4 jogadores.
  * Construído com *Buildroot*: Isto significa que o sistema base de arquivos pesa apenas 150MB comprimido.
  * Sistema de segurança baseado no *NOOBS*: Capacidade de reinstalar o sistema diretamente do cartão SD, ou obtendo a última versão da Internet.
  * Idiomas suportados pelo sistema: Francês, Inglês, Espanhol, Alemão, Italiano, Português e talvez outros possam surgir, se você participar.
  * Interface: Baseada no EmulationStation desenvolvido por Aloshi.
  * Música de fundo na interface.
  * Gestão de Favoritos.
* Rede:
  * Acesso via rede: o diretório de roms, capturas de tela e os *saves* dos jogos,
  * Suporte Wifi e RJ45
  * Atualizações on-line
* Controles:
  * Configure seu controle diretamente da interface: Uma vez configurado, ele será compatível com todos os emuladores.
  * Suporte integrado para controles PS3, Xbox360, 8BitDo e Bluetooth: Ligue um controle e jogue!
  * Presença de drivers GPIO: Para usar controles de arcade ou seus controles originais NES, SNES, Megadrive, PSX.
  * Suporte para controles Xin-Mo de 2 jogadores.
  * Suporte do Controle Virtual [Miroof's Virtual Gamepad](https://github.com/miroof/node-virtual-gamepads): Use seu *smartphone* como controle.
* Característica:
  * Kodi Media Center
  * Moonlight: Transmita jogos em sua rede local ou na Internet (requer placa gráfica Nvidia).
  * Netplay: Jogos online.
  * Retroarch IA: Traduzir texto em jogos em tempo real.
  * *Scraper* interno e externo: inserir uma imagem, um vídeo e uma descrição para cada jogo.
  * *Snaps videos*: Apresentação em vídeo do jogo nos *scrapes*.

## Agradecimentos

Recalbox **não existiria** sem os **emuladores de código aberto** que utiliza.  
Gostaríamos de **agradecer a todas as pessoas** que contribuem para fornecer **emuladores de código aberto**, graças aos quais tivemos a oportunidade de jogar, mais uma vez, **todos aqueles jogos clássicos dos videogames.** 🤩

### Aqui está a lista de softwares usados no Recalbox

* Buildroot: [http://buildroot.uclibc.org/](http://buildroot.uclibc.org/)
* EmulationStation: [http://emulationstation.org/](http://emulationstation.org/
* EmulationStation UI Design & tema padrão original "simples": Nils Bonenberger - [http://blog.nilsbyte.de/](http://blog.nilsbyte.de/)

### Emuladores

* RetroArch e Núcleo Libretro: [http://www.libretro.com/](http://www.libretro.com/)
* PiFBA: [https://code.google.com/p/pifba/](https://code.google.com/p/pifba/)
* Mupen64: [https://github.com/mupen64plus/](https://github.com/mupen64plus/)
* Dolphin-emu: [https://fr.dolphin-emu.org/](https://fr.dolphin-emu.org/)
* PPSSPP: [https://www.ppsspp.org/](https://www.ppsspp.org/)

### Outros

* Kodi media player: [http://kodi.tv/](http://kodi.tv/)
* Qtsixa: [http://qtsixa.sourceforge.net/](http://qtsixa.sourceforge.net/)