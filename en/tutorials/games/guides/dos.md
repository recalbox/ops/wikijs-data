---
title: DOS
description: 
published: true
date: 2021-08-08T08:54:46.061Z
tags: dos, msdos, pc
editor: markdown
dateCreated: 2021-08-07T23:42:16.347Z
---

You will have here some tutorials about DOS.

Here are the available tutorials:

[DOSbox to play with DOS games](dosbox-to-play-with-dos-games)
[Mouse use with controller in DOSBox Pure](mouse-use-with-controller-in-dosbox-pure)