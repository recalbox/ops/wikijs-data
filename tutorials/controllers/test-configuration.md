---
title: Configuration / Test
description: 
published: true
date: 2021-09-04T22:28:42.335Z
tags: configuration, test
editor: markdown
dateCreated: 2021-05-21T08:06:53.432Z
---

Ici, vous trouverez de quoi tester votre manette en cas de prooblème avec.

Voici les tutoriels disponibles :

[Testez votre joystick avec sdl2-jstest](test-your-joystick-with-sdl2-jstest)