---
title: Désactiver la puce T2 sur Mac
description: 
published: true
date: 2021-09-04T18:01:21.852Z
tags: mac, t2, désactiver, puce
editor: markdown
dateCreated: 2021-07-12T00:42:37.029Z
---

## Introduction

Si vous utilisez un Mac Intel avec une puce de sécurité T2, vous devez modifier la sécurité au démarrage afin de pouvoir démarrer sur un support externe (carte SD ou clé USB).

## Utilisation

* Redémarrez votre Mac.
* Au début du redémarrage, restez appuyé sur la combinaison de touches `Commande (⌘) + R`.
* Une fois démarré, sélectionnez le compte administrateur à utiliser et renseignez son mot de passe.
* Sélectionnez `Utilitaires` > `Utilitaire Sécurité au démarrage`.
* Sur la nouvelle fenêtre, sélectionnez les options `Aucune sécurité` et `Autoriser le démarrage à partir de supports externes` et fermez la fenêtre.
* Redémarrez
* Au début du redémarrage, restez appuyé sur la touche `Option (⌥)` et une fois que vous avez les disques disponibles affichés sur votre écran, sélectionnez `EFI Boot`.

Bon jeu !