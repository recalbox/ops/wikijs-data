---
title: Naomi GD-Rom
description: 
published: true
date: 2021-10-14T23:06:25.872Z
tags: arcade, naomi, gd-rom, naomi-gd
editor: markdown
dateCreated: 2021-10-14T23:06:25.872Z
---

![](/emulators/arcade/naomigd.svg){.align-center}

## Ficha técnica


* **Processador** : Hitachi SH-4 RISC 360 MIPS/1.4 GFLOPS
* **Vídeo** : PowerVR 2 a 100 MHz (Nec CLX2)
* **Audio** : ARM7 Yamaha XG AICA RISC 32 bits 25 MHz
* **RAM**: 32 MB1,2
* **Vídeo RAM**: 16 MB 1,2
* **Áudio RAM**: 8 MB 1,2
* **Mídia**: Cartucho (PCB de ROMs)
* **GD-ROM** : CompactFlash8

## Apresentação

O sistema Naomi foi lançado sob o nome **Naomi Multi System** (também conhecido como **Naomi Multiboard**), um acrônimo para "_New Arcade Operation Machine Idea"_ literalmente "Nova Idéia de Máquina de Arcade"; Naomi também se traduz como "beleza" em japonês. 

O sistema tem uma arquitetura **muito semelhante à do console Dreamcast**: um microprocessador Hitachi SH-4 a 200 MHz, um processador gráfico PowerVR Series 2 GPU (PVR2DC) e um processador de som Yamaha AICA; mas com **mais memória RAM e memória de vídeo** (ou seja, duas vezes mais que o Dreamcast). Os jogos são armazenados em **um cartucho** (Naomi) **ou um GD-ROM** (NaomiGD)**.**
  
Produzido pela SEGA, a líder do arcade na época, para competir com o Sistema 23 da NAMCO, seu marketing começou em 1998 com **The House of the Dead 2** como o primeiro jogo até 2006. Mais de 160 jogos foram lançados neste hardware (o recorde mundial para o número de jogos em um hardware de arcade).

A Sega anuncia que seu **Naomi 2** permanece compatível com os **cartuchos e GD-Rom Naomi 1.**

## Emuladores

[Libretro Flycast](libretro-flycast)
