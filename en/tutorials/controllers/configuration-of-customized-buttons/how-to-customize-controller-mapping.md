---
title: How to customize controller mapping
description: 
published: true
date: 2021-08-07T13:35:07.075Z
tags: customization, controller, mapping
editor: markdown
dateCreated: 2021-08-07T13:35:07.075Z
---

## Explanations

With RetroArch, you have the possibility to define specific actions on the buttons of your controller by modifying the ones defined by default.

## Use

* Launch a game with a Libretro emulator.
* Go to the RetroArch menu by pressing `Hotkey` + `B`.
* Go to `Keys` > `Port 1 Keys`.

![](/tutorials/controllers/buttons-customization/how-to/controllerconfig1.png)

* You can reconfigure the keys here. Remember to have a keyboard handy, because at some point it might get stuck, and you'll have to finish with a keyboard (`W` to validate and `X` to go back).

![](/tutorials/controllers/buttons-customization/how-to/controllerconfig2.png)

* Press ``Back'' once and you have the choice to save:
  * "Save mapping for core": use this option if you want to save the configuration for all the core currently running the game.

![](/tutorials/controllers/buttons-customization/how-to/controllerconfig3.png)

  * "Save mapping for folder": use this option if you want to save the configuration for the whole emulator folder.

![](/tutorials/controllers/buttons-customization/how-to/controllerconfig4.png)

  * Save mapping for the game": use this option if you want to save the configuration for this game only.

![](/tutorials/controllers/buttons-customization/how-to/controllerconfig5.png)

All remap files are located in `/recalbox/share/system/.config/retroarch/config/remaps/` and will have the extension `*.rmp`.