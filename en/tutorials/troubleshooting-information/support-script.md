---
title: Support script
description: 
published: true
date: 2021-08-06T14:37:01.057Z
tags: script, support
editor: markdown
dateCreated: 2021-08-06T14:37:01.057Z
---

A script named recalbox-support&#46;sh has been added to help developers with problems you may encounter with your devices.

This file gives the following information about :

* joystick
* kodi
* lirc
* system (lsmod, lsusb, tv service, es_settings, recalbox.conf, recalbox.log,config, df, etc...)

## Use

Its use is done via the Web manager.

* Go to the [Webmanager](./../../basic-usage/features/webmanager).
* Click on `Troubleshooting` on the left.
* Click on `Run Script`.

After a while you will be given a link. Copy the link and paste it into a message to the person who asked you for it.