---
title: Libretro MAME 2010​
description: 
published: true
date: 2024-06-01T10:38:17.251Z
tags: libretro, mame, mame2010
editor: markdown
dateCreated: 2021-10-01T00:08:53.912Z
---


## ![](/emulators/license.svg) Licença

Este núcleo é licenciado sob uma licença **MAME não-comercial**.

## ![](/emulators/compatibility.png) Compatibilidade

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ❌ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Recursos

| Recurso | Suportado |
| :---: | :---: |
| Reiniciar | ✔ |
| Capturas de tela | ✔ |
| Opções do núcleo | ✔ |


## ![](/emulators/bios.svg) BIOS

Os Romsets BIOS não são necessários quando se utilizam romsets de arcade "Full Non-Merged" (Completamente Não-Mesclado). Para romsets "Split"(divididos) e "Non-Merged"(Não-mesclado), coloque a BIOS na mesma pasta que o romset do jogo.

>**Nota** :
>Coloque as BIOS na pasta: `/recalbox/share/roms/mame/`   
>ou sua subpasta se você estiver utilizando múltiplos núcleos Mame, por exemplo: :`/recalbox/share/roms/mame/Mame 2010/`
{.is-warning}

## ![](/emulators/roms.png) Roms

* Baseado no romset: MAME 0.139 (agosto de 2010)
* Tamanho: 80 gb
* Romsets emulados: 8835 (incluindo clones, etc ...)
* Conjuntos ativos: 8835
* Pais: 1849
* Clones: 4310
* Outros: 2625
* BIOS: 51
* CHDs: 459
* Versão Chds : 4
* Amostras: 2566
* Arquivo dat: [mame2010.xml] (https://github.com/libretro/mame2010-libretro/blob/master/metadata/mame2010.xml)


### Local

Coloque as roms assim:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 mame
┃ ┃ ┃ ┃ ┣ 🗒 **jogo.zip**
┃ ┃ ┃ ┃ ┣ 📁 **jogo**
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **jogo.chd**

Você também pode optar por uma subpasta (útil se você quiser ter um Set MAME para outro núcleo).

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 mame
┃ ┃ ┃ ┃ ┣ 📁 Mame 2010
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **jogo.zip**
┃ ┃ ┃ ┃ ┃ ┣ 📁 **jogo**
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 **jogo.chd**

## ![](/emulators/advanced-configuration.png) Configuração avançada do emulador

> Para poder manter suas configurações personalizadas durante uma atualização, aconselhamos que você use nosso recurso de [Sobrecarga de configuração](./../../../advanced-usage/configuration-override).
{.is-info}

### Acessar as Opções

Você pode configurar várias opções de duas maneiras diferentes.

* Por meio do Menu RetroArch:

┣ 📁 Menu RetroArch
┃ ┣ 📁 Opções do núcleo
┃ ┃ ┣ 🧩 Nome_da_opção

* Por meio do arquivo `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Opções de núcleo

## ![external-links.png](/emulators/external-links.png) Links externos

* **Progettosnaps** : [https://www.progettosnaps.net/](https://www.progettosnaps.net/)
* **Código fonte utilizado** : [https://github.com/libretro/mame2010-libretro/](https://github.com/libretro/mame2010-libretro)
* **Documentação Libretro** : [https://docs.libretro.com/library/mame\_2010/](https://docs.libretro.com/library/mame_2010/)