---
title: FAQ
description: 
published: true
date: 2024-11-04T20:22:38.343Z
tags: faq
editor: markdown
dateCreated: 2023-09-19T19:18:34.906Z
---

## Cambiar la cara del disquete

### Introducción

Los juegos se escriben en las 2 caras de un disquete, y hay que darle la vuelta para leer la otra cara.

### Utilización

La forma de utilizar esto depende del emulador.

#### Libretro FBNeo

Los botones `L3` y `R3` os permiten expulsar el disco (`L3`) y cambiar la cara y leer la nueva cara (`R3`). Por lo tanto, para cambiar de cara es necesario realizar sucesivamente las pulsaciones `L3` + `R3`.

#### Otros emuladores del sistema

Los botones `L1` y `R1` permiten expulsar el disco (`R1`) y cambiar la cara (`L1`). Así que hay que pulsar `R1` + `L1` + `R1` (expulsar + dar la vuelta + leer) sucesivamente para cambiar de cara.