---
title: Libretro NP2Kai
description: 
published: true
date: 2024-07-17T21:19:39.489Z
tags: libretro, pc-98, np2kai
editor: markdown
dateCreated: 2023-09-12T11:22:16.122Z
---



## ![](/emulators/license.svg) Licencia

El core posee la siguiente licencia [**MIT**](https://github.com/AZO234/NP2kai/blob/master/LICENSE).

## ![](/emulators/compatibility.png) Compatibilidad

 | RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | 
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | 

## ![](/emulators/features.png) Funcionalidades

| Funcionalidad | ¿Soportada? |
| :---: | :---: |
| Reinicio | ✔ |
| Capturas de pantalla | ✔ |
| Grabaciones instantáneas | ✔ |
| Rebobinado | ✔ |
| Netplay | ✔ |
| Opciones del core | ✔ |
| RetroAchievements | ✔ |
| Controles | ✔ |
| Control de disco | ✔ |

## ![](/emulators/bios.svg) BIOS

### Lista de BIOS obligatorias

| Nombre del fichero | Descripción | MD5 | ¿Preinstalada? |
| :---: | :---: | :---: | :---: |
| bios.rom | - | 052f5f6010877522f78803498a625727 0c052b1f1bfd0ece11286d2ff45d296f 36293e23fe0c86cb115d59b653c13876 3af0ae018c5710eec6e2891064814138 50274bb5dcb707e4450011b09accffcb c70ee9df11794bd5cc8aadb3721b4a03 cd237e16e7e77c06bb58540e9e9fca68 e246140dec5124c5e404869a84caefce | ❌ |
| font.bmp | Affichage du texte | 0c8624e9ced7cca8769e5c7b0dd4279b 7da1e5b7c482d4108d22a5b09631d967 | ❌ |
| font.rom | Police alternative | 38d32748ae49d1815b0614970849fd40 4133b0be0d470920da60b9ed28d2614f 693fd1da3239d4bbeafc77d211718fc5 829b963e21334e0fc2092ebd58f2ab4a add4a225048c85ca2bc588696c6ecdc5 ca87908a99ea423093f6d497fc367f7d fd0b856899aec843cbe69d2940df547f | ❌ |
| itf.rom | - | 1d295699ffeab0f0e24e09381299259d 72ea51443070f0e9212bfc9b793ee28e a13d96da03a28af8418d7f86ab951f1a b49ea39a1f730f1c966babc11961dc9a e9fc3890963b12cf15d0a2eea5815b72 | ❌ |
| sound.rom | - | 42c271f8b720e796a484cc1165ff4914 524473c1a5a03b17e21d86a0408ff827 a77fc2bc7c696dd68dba18e02f89d386 caf90f22197aed6f14c471c21e64658d | ❌ |

### Emplazamiento

Disponed vuestras bios como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 np2kai
┃ ┃ ┃ ┃ ┣ 🗒 **bios.rom**
┃ ┃ ┃ ┃ ┣ 🗒 **font.bmp**
┃ ┃ ┃ ┃ ┣ 🗒 **font.rom**
┃ ┃ ┃ ┃ ┣ 🗒 **itf.rom**
┃ ┃ ┃ ┃ ┣ 🗒 **sound.rom**

### Lista de BIOS opcionales

| Nombre del fichero | Descripción | MD5 | ¿Preinstalada? |
| :---: | :---: | :---: | :---: |
| 2608_bd.wav | YM2608 RYTHM sample | 9c6637930b1779abe00b8b63e4e41f50 d94546e70f17fd899be8df3544ab6cbb | ❌ |
| 2608_hh.wav | YM2608 RYTHM sample | 73548a1391631ff54a1f7c838d67917e 08c54a0c1f774a5538a848a6665a34b4 | ❌ |
| 2608_rim.wav | YM2608 RYTHM sample | 43d54b3e05c081fa280c9bace3af1043 465ea0768b27da404aec45dfc501404b | ❌ |
| 2608_sd.wav | YM2608 RYTHM sample | 08124ccb84a9f65e2affc29581e690c9 d71004351c8bbfdad53b18222c061d49 | ❌ |
| 2608_tom.wav | YM2608 RYTHM sample | faed5664a2dd8b1b2308e8a50ac25ea 96a4ead13f364734f79b0c58af2f0e1f | ❌ |
| 2608_top.wav | YM2608 RYTHM sample | 3721ace646ffd56439aebbb2154e9263 593cff6597ab9380d822b8f824fd2c28 | ❌ |

### Emplazamiento

Disponed vuestras bios como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 np2kai
┃ ┃ ┃ ┃ ┣ 🗒 **2608_hd.wav**
┃ ┃ ┃ ┃ ┣ 🗒 **2608_hh.wav**
┃ ┃ ┃ ┃ ┣ 🗒 **2608_rim.wav**
┃ ┃ ┃ ┃ ┣ 🗒 **2608_sd.wav**
┃ ┃ ┃ ┃ ┣ 🗒 **2608_tom.wav**
┃ ┃ ┃ ┃ ┣ 🗒 **2608_top.wav**

## ![](/emulators/roms.png) Roms

### Extensiones de roms soportadas

Las roms deben tener las extensiones siguientes:

* .d98
* .98d
* .dcp
* .fdi
* .fdd
* .nfd
* .2hd
* .tfd
* .d88
* .88d
* .hdm
* .xdf
* .dup
* .cmd
* .hdi
* .thd
* .nhd
* .hdd
* .hdn
* .m3u
* .zip
* .7z

Este sistema soporta las roms comprimidas en formato .zip/.7z. No obstante, se trata sólo de un archivo.

Los ficheros contenidos en los .zip/.7z deben tener las extensiones citadas previamente.
Además, cada fichero .zip/.7z sólo debe contener un fichero rom al interior.

### Emplazamiento

Disponed vuestras roms como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 pc98
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.zip**

>Se aconsejan las roms del proyecto **TOSEC**.
{.is-success}

>Para más información sobre las roms, consultad [este tutorial](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

>Para conservar las configuraciones personalizadas cuando se produce una actualización del sistema Recalbox, os aconsejamos utilizar las [Sobrecargas de configuración](./../../../advanced-usage/configuration-override).
{.is-info}

### Acceder a las opciones

Podéis configurar las opciones del core de dos formas diferentes.

* Via el Menú RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via el fichero `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Opciones del core

| Opción | Valor de la opción | Variable (para la sobrecarga) | Valores posibles |
| --- | --- | --- | --- |
| Swap Disks on Drive | `FDD1` / `FDD2` ✅ | `np2kai_drive` | `FDD1` / `FDD2` |
| Keyboard (Restart) | `Ja` ✅ / `Us` | `np2kai_keyboard` | `Ja` / `Us` |
| Key-repeat | `Désactivé` ✅ / `Activé` | `np2kai_keyrepeat` | `OFF` / `ON` |
| Key-repeat delay | `250ms` / `500ms` ✅ / `1000ms` | `np2kai_keyrepeat_delay` | `250ms` / `500ms` / `1000ms` |
| Key-repeat interval | `25ms` / `50ms` ✅ / `100ms` | `np2kai_keyrepeat-interval` | `25ms` / `50ms` / `100ms` |
| PC Model (Restart) | `PC-286` / `PC-9801VM` / `PC-9801VX` ✅ | `np2kai_model` | `PC-286` / `PC-9801VM` / `PC-9801VX` |
| CPU Base Clock (Restart) | `1.9968 MHz` / `2.4576 MHz` ✅ | `np2kai_clk_base` | `1.9968 MHz` / `2.4576 MHz` |
| CPU Feature (Restart) | `Intel 80386` ✅ / `Intel i486SX` / `Intel i486DX` / `Intel Pentium` / `Intel MMX Pentium` / `Intel Pentium Pro` / `Intel Pentium II` / `Intel Pentium III` / `Intel Pentium M` / `Intel Pentium 4` / `AMD K6-2` / `AMD K6-III` / `AMD K7 Athlon` / `AMD K7 Athlon XP` / `Neko Processor II` | `np2kai_cpu_feature` | `Intel 80386` / `Intel i486SX` / `Intel i486DX` / `Intel Pentium` / `Intel MMX Pentium` / `Intel Pentium Pro` / `Intel Pentium II` / `Intel Pentium III` / `Intel Pentium M` / `Intel Pentium 4` / `AMD K6-2` / `AMD K6-III` / `AMD K7 Athlon` / `AMD K7 Athlon XP` / `Neko Processor II` |
| CPU Clock Multiplier (Restart) | `2` / `4` ✅ / `5` / `6` / `8` / `10` / `12` / `16` / `20` / `24` / `30` / `36` / `40` / `42` / `52` / `64` / `76` / `88` / `100` | `np2kai_clk_mult` | `2` / `4` / `5` / `6` / `8` / `10` / `12` / `16` / `20` / `24` / `30` / `36` / `40` / `42` / `52` / `64` / `76` / `88` / `100` |
| Async CPU(experimental) (Restart) | `Désactivé` ✅ / `Activé` | `np2kai_async_cpu` | `OFF` / `ON` |
| RAM Size (Restart) | `1` / `3` ✅ / `7` / `11` / `13` / `16` / `32` / `64` / `120` / `230` / `512` / `1024` | `np2kai_ExMemory` | `1` / `3` / `7` / `11` / `13` / `16` / `32` / `64` / `120` / `230` / `512` / `1024` |
| Fast memcheck | `Désactivé` ✅ / `Activé` | `np2kai_FastMC` | `OFF` / `ON` |
| Use last HDD mount | `Désactivé` ✅ / `Activé` | `np2kai_uselasthddmount` | `OFF` / `ON` |
| Video filter | `Désactivé` ✅ / `Profile 0` / `Profile 1` / `Profile 2` | `np2kai_vf1` | `OFF` / `Profile 0` / `Profile 1` / `Profile 2` |
| GDC | `uPD7220` ✅ / `uPD72020` | `np2kai_gdc` | `uPD7220` / `uPD72020` |
| Skipline Revisions | `Full 255 lines` ✅ / `Désactivé` / `Activé` | `np2kai_skipline` | `Full 255 lines` / `OFF` / `ON` |
| Real Palettes | `Désactivé` ✅ / `Activé` | `np2kai_realpal` | `OFF` / `ON` |
| LCD (old monochrome) | `Désactivé` ✅ / `Activé` | `np2kai_lcd` | `OFF` / `ON` |
| Sound Board (Restart) | `PC9801-14` ✅ / `PC9801-86` / `PC9801-86 + 118(B460)` / `PC9801-86 + Mate-X PCM(B460)` / `PC9801-86 + Chibi-oto` / `PC9801-86 + Speak Board` / `PC9801-26K` / `PC9801-26K + 86` / `PC9801-118` / `Mate-X PCM` / `Chibi-oto` / `Speak Board` / `Spark Board` / `Sound Orchestra` / `Sound Orchestra-V` / `Little Orchestra L` / `Multimedia Orchestra` / `Sound Blaster 16` / `PC9801-86 + Sound Blaster 16` / `Mate-X PCM + Sound Blaster 16` / `PC9801-118 + Sound Blaster 16` / `PC9801-86 + Mate-X PCM(B460) + Sound Blaster 16` / `PC9801-86 + 118(B460) + Sound Blaster 16` / `AMD-98` / `WaveStar` / `Otomi-chanx2` / `Otomi-chanx2 + 86` / `None` | `np2kai_SNDboard` | `PC9801-14` / `PC9801-86` / `PC9801-86 + 118(B460)` / `PC9801-86 + Mate-X PCM(B460)` / `PC9801-86 + Chibi-oto` / `PC9801-86 + Speak Board` / `PC9801-26K` / `PC9801-26K + 86` / `PC9801-118` / `Mate-X PCM` / `Chibi-oto` / `Speak Board` / `Spark Board` / `Sound Orchestra` / `Sound Orchestra-V` / `Little Orchestra L` / `Multimedia Orchestra` / `Sound Blaster 16` / `PC9801-86 + Sound Blaster 16` / `Mate-X PCM + Sound Blaster 16` / `PC9801-118 + Sound Blaster 16` / `PC9801-86 + Mate-X PCM(B460) + Sound Blaster 16` / `PC9801-86 + 118(B460) + Sound Blaster 16` / `AMD-98` / `WaveStar` / `Otomi-chanx2` / `Otomi-chanx2 + 86` / `None` |
| enable 118 ROM | `Désactivé` / `Activé` ✅ | `np2kai_118ROM` | `OFF` / `ON` |
| JastSound | `Désactivé` ✅ / `Activé` | `np2kai_jast_snd` | `OFF` / `ON` |
| Swap PageUp/PageDown | `Désactivé` / `Activé` ✅ | `np2kai_xroll` | `OFF` / `ON` |
| Sound Generator | `Default` / `fmgen` ✅ | `np2kai_usefmgen` | `Default` / `fmgen` |
| Volume Master | `0` / `5` / `10` / `15` / `20` / `25` / `30` / `35` / `40` / `45` / `50` / `55` / `60` / `65` / `70` / `75` / `80` / `85` / `90` / `95` / `100` ✅ | `np2kai_volume_M` | `0` / `5` / `10` / `15` / `20` / `25` / `30` / `35` / `40` / `45` / `50` / `55` / `60` / `65` / `70` / `75` / `80` / `85` / `90` / `95` / `100` |
| Volume FM | `0` / `4` / `8` / `12` / `16` / `20` / `24` / `28` / `32` / `36` / `40` / `44` / `48` / `52` / `56` / `60` / `64` ✅ / `68` / `72` / `76` / `80` / `84` / `88` / `92` / `96` / `100` / `104` / `108` / `112` / `116` / `120` / `124` / `128` | `np2kai_volume_F` | `0` / `4` / `8` / `12` / `16` / `20` / `24` / `28` / `32` / `36` / `40` / `44` / `48` / `52` / `56` / `60` / `64` / `68` / `72` / `76` / `80` / `84` / `88` / `92` / `96` / `100` / `104` / `108` / `112` / `116` / `120` / `124` / `128` |
| Volume SSG | `0` / `4` / `8` / `12` / `16` / `20` / `24` / `28` ✅ / `32` / `36` / `40` / `44` / `48` / `52` / `56` / `60` / `64` / `68` / `72` / `76` / `80` / `84` / `88` / `92` / `96` / `100` / `104` / `108` / `112` / `116` / `120` / `124` / `128` | `np2kai_volume_S` | `0` / `4` / `8` / `12` / `16` / `20` / `24` / `28` / `32` / `36` / `40` / `44` / `48` / `52` / `56` / `60` / `64` / `68` / `72` / `76` / `80` / `84` / `88` / `92` / `96` / `100` / `104` / `108` / `112` / `116` / `120` / `124` / `128` |
| Volume ADPCM | `0` / `4` / `8` / `12` / `16` / `20` / `24` / `28` / `32` / `36` / `40` / `44` / `48` / `52` / `56` / `60` / `64` ✅ / `68` / `72` / `76` / `80` / `84` / `88` / `92` / `96` / `100` / `104` / `108` / `112` / `116` / `120` / `124` / `128` | `np2kai_volume A` | `0` / `4` / `8` / `12` / `16` / `20` / `24` / `28` / `32` / `36` / `40` / `44` / `48` / `52` / `56` / `60` / `64` / `68` / `72` / `76` / `80` / `84` / `88` / `92` / `96` / `100` / `104` / `108` / `112` / `116` / `120` / `124` / `128` |
| Volume PCM | `0` / `4` / `8` / `12` / `16` / `20` / `24` / `28` / `32` / `36` / `40` / `44` / `48` / `52` / `56` / `60` / `64` / `68` / `72` / `76` / `80` / `84` / `88` / `92` ✅ / `96` / `100` / `104` / `108` / `112` / `116` / `120` / `124` / `128` | `np2kai_volume_P` | `0` / `4` / `8` / `12` / `16` / `20` / `24` / `28` / `32` / `36` / `40` / `44` / `48` / `52` / `56` / `60` / `64` / `68` / `72` / `76` / `80` / `84` / `88` / `92` / `96` / `100` / `104` / `108` / `112` / `116` / `120` / `124` / `128` |
| Volume RHYTHM | `0` / `4` / `8` / `12` / `16` / `20` / `24` / `28` / `32` / `36` / `40` / `44` / `48` / `52` / `56` / `60` / `64` ✅ / `68` / `72` / `76` / `80` / `84` / `88` / `92` / `96` / `100` / `104` / `108` / `112` / `116` / `120` / `124` / `128` | `np2kai_volume_R` | `0` / `4` / `8` / `12` / `16` / `20` / `24` / `28` / `32` / `36` / `40` / `44` / `48` / `52` / `56` / `60` / `64` / `68` / `72` / `76` / `80` / `84` / `88` / `92` / `96` / `100` / `104` / `108` / `112` / `116` / `120` / `124` / `128` |
| Volume CD-DA | `0` / `8` / `16` / `24` / `32` / `40` / `48` / `56` / `64` / `72` / `80` / `88` / `96` / `104` / `112` / `120` / `128` ✅ / `136` / `144` / `154` / `160` / `168` / `196` / `184` / `192` / `200` / `208` / `216` / `224` / `232` / `240` / `248` / `255` | `np2kai_volume_C` | `0` / `8` / `16` / `24` / `32` / `40` / `48` / `56` / `64` / `72` / `80` / `88` / `96` / `104` / `112` / `120` / `128` / `136` / `144` / `154` / `160` / `168` / `196` / `184` / `192` / `200` / `208` / `216` / `224` / `232` / `240` / `248` / `255` |
| Floppy Seek Sound | `Désactivé` ✅ / `Activé` | `np2kai_Seek_Snd` | `OFF` / `ON` |
| Volume Floppy Seek | `0` ✅ / `4` / `8` / `12` / `16` / `20` / `24` / `28` / `32` / `36` / `40` / `44` / `48` / `52` / `56` / `60` / `64` / `68` / `72` / `76` / `80` / `84` / `88` / `92` / `96` / `100` / `104` / `108` / `112` / `116` / `120` / `124` / `128` | `np2kai_Seek_Vol` | `0` / `4` / `8` / `12` / `16` / `20` / `24` / `28` / `32` / `36` / `40` / `44` / `48` / `52` / `56` / `60` / `64` / `68` / `72` / `76` / `80` / `84` / `88` / `92` / `96` / `100` / `104` / `108` / `112` / `116` / `120` / `124` / `128` |
| Volume Beep | `0` / `1` / `2` / `3` ✅ | `np2kai_BEEP_vol` | `0` / `1` / `2` / `3` |
| Enable WAB (Restart App) | `Désactivé` ✅ / `Activé` | `np2kai_CLGD_en` | `OFF` / `ON` |
| WAB Type | `PC-9821Xe10,Xa7e,Xb10 built-in` ✅ / `PC-9821Bp,Bs,Be,Bf built-in` / `PC-9821Xe built-in` / `PC-9821Cb built-in` / `PC-9821Cf built-in` / `PC-9821Cb2 built-in` / `PC-9821Cx2 built-in` / `PC-9821 PCI CL-GD5446 built-in` / `MELCO WAB-S` / `MELCO WSN-A2F` / `MELCO WSN-A4F` / `I-O DATA GA-98NBI/C` / `I-O DATA GA-98NBII` / `I-O DATA GA-98NBIV` / `PC-9801-96(PC-9801B3-E02)` / `Auto Select(Xe10, GA-98NBI/C), PCI` / `Auto Select(Xe10, GA-98NBII), PCI` / `Auto Select(Xe10, GA-98NBIV), PCI` / `Auto Select(Xe10, WAB-S), PCI` / `Auto Select(Xe10, WSN-A2F), PCI` / `Auto Select(Xe10, WSN-A4F), PCI` / `Auto Select(Xe10, WAB-S)` / `Auto Select(Xe10, WSN-A2F)` / `Auto Select(Xe10, WSN-A4F)` | `np2kai_CLGD_type` | `PC-9821Xe10,Xa7e,Xb10 built-in` / `PC-9821Bp,Bs,Be,Bf built-in` / `PC-9821Xe built-in` / `PC-9821Cb built-in` / `PC-9821Cf built-in` / `PC-9821Cb2 built-in` / `PC-9821Cx2 built-in` / `PC-9821 PCI CL-GD5446 built-in` / `MELCO WAB-S` / `MELCO WSN-A2F` / `MELCO WSN-A4F` / `I-O DATA GA-98NBI/C` / `I-O DATA GA-98NBII` / `I-O DATA GA-98NBIV` / `PC-9801-96(PC-9801B3-E02)` / `Auto Select(Xe10, GA-98NBI/C), PCI` / `Auto Select(Xe10, GA-98NBII), PCI` / `Auto Select(Xe10, GA-98NBIV), PCI` / `Auto Select(Xe10, WAB-S), PCI` / `Auto Select(Xe10, WSN-A2F), PCI` / `Auto Select(Xe10, WSN-A4F), PCI` / `Auto Select(Xe10, WAB-S)` / `Auto Select(Xe10, WSN-A2F)` / `Auto Select(Xe10, WSN-A4F)` |
| Use Fake Hardware Cursor | `Désactivé` ✅ / `Activé` | `np2kai_CLGD_fc` | `OFF` / `ON` |
| Enable PEGC plane mode | `Désactivé` / `Activé` ✅ | `np2kai_PEGC` | `OFF` / `ON` |
| Enable PCI (Restart App) | `Désactivé` ✅ / `Activé` | `np2kai_PCI_en` | `OFF` / `ON` |
| PCMC Type | `Intel 82434LX` ✅ / `Intel 82441FX` / `VLSI Wildcat` | `np2kai_PCI_type` | `Intel 82434LX` / `Intel 82441FX` / `VLSI Wildcat` |
| Use BIOS32 (not recommended) | `Désactivé` ✅ / `Activé` | `np2kai_PCI_bios32` | `OFF` / `ON` |
| Use CD-ROM EDC/ECC Emulation | `Désactivé` / `Activé` ✅ | `np2kai_usecdecc` | `OFF` / `ON` |
| Use mouse or touchpanel to input mouse | `Désactivé` / `Activé` ✅ | `np2kai_inputmouse` | `OFF` / `ON` |
| S2M(Joypad Analog Stick to Mouse) Mapping | `Désactivé` / `R-stick` ✅ / `L-stick` | `np2kai_stick2mouse` | `OFF` / `R-stick` / `L-stick` |
| S2M Click Shift Button Mapping | `Désactivé` / `L1` / `R1` ✅ / `L2` / `R2` | `np2kai_stick2mouse_shift` | `OFF` / `L1` / `R1` / `L2` / `R2` |
| Joypad D-pad to Mouse/Keyboard/Joypad Mapping | `Désactivé` ✅ / `Mouse` / `Arrows` / `Arrows 3button` / `Keypad` / `Keypad 3button` / `Manual Keyboard` / `Atari Joypad` | `np2kai_joymode` | `OFF` / `Mouse` / `Arrows` / `Arrows 3button` / `Keypad` / `Keypad 3button` / `Manual Keyboard` / `Atari Joypad` |
| Joypad to NP2 menu Mapping | `Désactivé` ✅ / `L1` / `L2` / `L3` / `R1` / `R2` / `R3` / `A` / `B` / `X` / `Y` / `Start` / `Select` | `np2kai_joynp2menu` | `OFF` / `L1` / `L2` / `L3` / `R1` / `R2` / `R3` / `A` / `B` / `X` / `Y` / `Start` / `Select` |

## ![](/emulators/external-links.png) Enlaces externos

* **Código fuente utilizado** : [https://github.com/AZO234/NP2kai/](https://github.com/AZO234/NP2kai/)
* **Documentación Libretro** : [https://docs.libretro.com/library/neko_project_ii_kai/](https://docs.libretro.com/library/neko_project_ii_kai/)