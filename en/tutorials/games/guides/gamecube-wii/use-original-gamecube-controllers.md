---
title: Use original Gamecube controllers
description: 
published: true
date: 2021-10-16T11:47:06.464Z
tags: controllers, gamecube
editor: markdown
dateCreated: 2021-08-08T08:32:08.549Z
---

>An USB adapter for Gamecube controller is required for this function !
{.is-info}

* Connect your Gamecube adapter to your Recalbox 
* Open the file: `/recalbox/share/system/recalbox.conf`.
  * In the controllers section, look for the following line:
  
```ini
gamecube.realgamecubepads=0
```

  * Change it to this:

```ini
gamecube.realgamecubepads=1
```

* Your Gamecube controller is now working in Dolphin.