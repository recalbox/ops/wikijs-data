---
title: Libretro Gambatte
description: 
published: true
date: 2021-10-19T18:20:19.170Z
tags: libretro, color, gameboy, gambatte, gbc
editor: markdown
dateCreated: 2021-07-14T06:34:16.543Z
---

**Libretro Gambatte** is a cross-platform, open source, precision-oriented Game Boy Color emulator written in C++.  
It is based on hundreds of hardware test cases, as well as previous documentation and reverse engineering efforts.

## ![](/emulators/license.svg) License

This core is under [**GPLv2**](https://github.com/libretro/gambatte-libretro/blob/master/COPYING) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Features

| Feature | Supported |
| :---: | :---: |
| Restart | ✔ |
| Saves | ✔ |
| States | ✔ |
| Rewind | ✔ |
| Core Options | ✔ |
| RetroAchievements | ✔ |
| RetroArch Cheats | ✔ |
| Controls | ✔ |
| Remapping | ✔ |
| Softpatching | ✔ |

## ![](/emulators/bios.svg) BIOS

### List of optional bios

| Filename | Description | MD5 | Provided |
| :---: | :---: | :---: | :---: |
| gbc_bios.bin | Game Boy Color BIOS | dbfce9db9deaa2567f6a84fde55f9680 | ❌ |

### Location

Put your bios like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 🗒 **gbc_bios.bin**

## ![](/emulators/roms.png) Roms

### Supported extensions

Roms must have the extension:

* .bin
* .gbc
* .zip
* .7z

This system supports compressed roms in .zip/.7z format. However, be careful, it is only an archive.

The files contained in the .zip/.7z must correspond to the extensions mentioned above.
Moreover, each .zip/.7z file must contain only one compressed rom.

### Location

Put your roms like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 gbc
┃ ┃ ┃ ┃ ┣ 🗒 **game.zip**

>The roms using **No-Intro** format are highly recommended.
{.is-success}

>For more information about roms, go to [this tutorial](./../../../tutorials/games/generalities/isos-and-roms)!
{.is-info}

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Accessing to options

You can configure various options in two different ways.

* Via RetroArch menu:

┣ 📁 RetroArch Menu
┃ ┣ 📁 Core options
┃ ┃ ┣ 🧩 Name_option

* Via the file `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core options

| Options | Options values | Variables (for configuration override) | Variables values |
| --- | --- | --- | --- |
| Allow Opposing Directions | `Disabled` ✅ / `Enabled` | `gambatte_up_down_allowed` | `disabled` / `enabled` |
| GB Colorization | `Disabled` ✅ / `Auto` / `GBC` / `SGB` / `Internal` / `Custom` | `gambatte_gb_colorization` | `disabled` / `auto` / `GBC` / `SGB` / `internal` / `custom` |
| Internal Palette | `GB - DMG` ✅ / `GB - Pocket` / `GB - Light` / `GBC - Blue` / `GBC - Brown` / `GBC - Dark Blue` / `GBC - Dark Brown` / `GBC - Dark Green` / `GBC - Grayscale` / `GBC - Green` / `GBC - Inverted` / `GBC - Orange` / `GBC - Pastel Mix` / `GBC - Red` / `GBC - Yellow` / `SGB - 1A` / `SGB - 1B` / `SGB - 1C` / `SGB - 1D` / `SGB - 1E` / `SGB - 1F` / `SGB - 1G` / `SGB - 1H` / `SGB - 2A` / `SGB - 2B` / `SGB - 2C` / `SGB - 2D` / `SGB - 2E` / `SGB - 2F` / `SGB - 2G` / `SGB - 2H` / `SGB - 3A` / `SGB - 3B` / `SGB - 3C` / `SGB - 3D` / `SGB - 3E` / `SGB - 3F` / `SGB - 3G` / `SGB - 3H` / `SGB - 4A` / `SGB - 4B` / `SGB - 4C` / `SGB - 4D` / `SGB - 4E` / `SGB - 4F` / `SGB - 4G` / `SGB - 4H` / `Special 1` / `Special 2` / `Special 3` / `TWB01 - 756 Production` / `TWB02 - AKB48 Pink` / `TWB03 - Angry Volcano` / `TWB04 - Anime Expo` / `TWB05 - Aqours Blue` / `TWB06 - Aquatic Iro` / `TWB07 - Bandai Namco` / `TWB08 - Blossom Pink` / `TWB09 - Bubbles Blue` / `TWB10 - Builder Yellow` / `TWB11 - Buttercup Green` / `TWB12 - Camouflage` / `TWB13 - Cardcaptor Pink` / `TWB14 - Christmas` / `TWB15 - Crunchyroll Orange` / `TWB16 - Digivice` / `TWB17 - Do The Dew` / `TWB18 - Eevee Brown` / `TWB19 - Fruity Orange` / `TWB20 - Game.com` / `TWB21 - Game Grump Orange` / `TWB22 - GameKing` / `TWB23 - Game Master` / `TWB24 - Ghostly Aoi` / `TWB25 - Golden Wild` / `TWB26 - Green Banana` / `TWB27 - Greenscale` / `TWB28 - Halloween` / `TWB29 - Hero Yellow` / `TWB30 - Hokage Orange` / `TWB31 - Labo Fawn` / `TWB32 - Legendary Super Saiyan` / `TWB33 - Lemon Lime Green` / `TWB34 - Lime Midori` / `TWB35 - Mania Plus Green` / `TWB36 - Microvision` / `TWB37 - Million Live Gold` / `TWB38 - Miraitowa Blue` / `TWB39 - NASCAR` / `TWB40 - Neo Geo Pocket` / `TWB41 - Neon Blue` / `TWB42 - Neon Green` / `TWB43 - Neon Pink` / `TWB44 - Neon Red` / `TWB45 - Neon Yellow` / `TWB46 - Nick Orange` / `TWB47 - Nijigasaki Orange` / `TWB48 - Odyssey Gold` / `TWB49 - Patrick Star Pink` / `TWB50 - Pikachu Yellow` / `TWB51 - Pocket Tales` / `TWB52 - Pokemon mini` / `TWB53 - Pretty Guardian Gold` / `TWB54 - S.E.E.S. Blue` / `TWB55 - Saint Snow Red` / `TWB56 - Scooby-Doo Mystery` / `TWB57 - Shiny Sky Blue` / `TWB58 - Sidem Green` / `TWB59 - Slime Blue` / `TWB60 - Spongebob Yellow` / `TWB61 - Stone Orange` / `TWB62 - Straw Hat Red` / `TWB63 - Superball Ivory` / `TWB64 - Super Saiyan Blue` / `TWB65 - Super Saiyan Rose` / `TWB66 - Supervision` / `TWB67 - Survey Corps Brown` / `TWB68 - Tea Midori` / `TWB69 - TI-83` / `TWB70 - Tokyo Midtown` / `TWB71 - Travel Wood` / `TWB72 - Virtual Boy` / `TWB73 - VMU` / `TWB74 - Wisteria Murasaki` / `TWB75 - WonderSwan` / `TWB76 - Yellow Banana` | `gambatte_gb_internal_palette` | `GB - DMG` / `GB - Pocket` / `GB - Light` / `GBC - Blue` / `GBC - Brown` / `GBC - Dark Blue` / `GBC - Dark Brown` / `GBC - Dark Green` / `GBC - Grayscale` / `GBC - Green` / `GBC - Inverted` / `GBC - Orange` / `GBC - Pastel Mix` / `GBC - Red` / `GBC - Yellow` / `SGB - 1A` / `SGB - 1B` / `SGB - 1C` / `SGB - 1D` / `SGB - 1E` / `SGB - 1F` / `SGB - 1G` / `SGB - 1H` / `SGB - 2A` / `SGB - 2B` / `SGB - 2C` / `SGB - 2D` / `SGB - 2E` / `SGB - 2F` / `SGB - 2G` / `SGB - 2H` / `SGB - 3A` / `SGB - 3B` / `SGB - 3C` / `SGB - 3D` / `SGB - 3E` / `SGB - 3F` / `SGB - 3G` / `SGB - 3H` / `SGB - 4A` / `SGB - 4B` / `SGB - 4C` / `SGB - 4D` / `SGB - 4E` / `SGB - 4F` / `SGB - 4G` / `SGB - 4H` / `Special 1` / `Special 2` / `Special 3` / `TWB01 - 756 Production` / `TWB02 - AKB48 Pink` / `TWB03 - Angry Volcano` / `TWB04 - Anime Expo` / `TWB05 - Aqours Blue` / `TWB06 - Aquatic Iro` / `TWB07 - Bandai Namco` / `TWB08 - Blossom Pink` / `TWB09 - Bubbles Blue` / `TWB10 - Builder Yellow` / `TWB11 - Buttercup Green` / `TWB12 - Camouflage` / `TWB13 - Cardcaptor Pink` / `TWB14 - Christmas` / `TWB15 - Crunchyroll Orange` / `TWB16 - Digivice` / `TWB17 - Do The Dew` / `TWB18 - Eevee Brown` / `TWB19 - Fruity Orange` / `TWB20 - Game.com` / `TWB21 - Game Grump Orange` / `TWB22 - GameKing` / `TWB23 - Game Master` / `TWB24 - Ghostly Aoi` / `TWB25 - Golden Wild` / `TWB26 - Green Banana` / `TWB27 - Greenscale` / `TWB28 - Halloween` / `TWB29 - Hero Yellow` / `TWB30 - Hokage Orange` / `TWB31 - Labo Fawn` / `TWB32 - Legendary Super Saiyan` / `TWB33 - Lemon Lime Green` / `TWB34 - Lime Midori` / `TWB35 - Mania Plus Green` / `TWB36 - Microvision` / `TWB37 - Million Live Gold` / `TWB38 - Miraitowa Blue` / `TWB39 - NASCAR` / `TWB40 - Neo Geo Pocket` / `TWB41 - Neon Blue` / `TWB42 - Neon Green` / `TWB43 - Neon Pink` / `TWB44 - Neon Red` / `TWB45 - Neon Yellow` / `TWB46 - Nick Orange` / `TWB47 - Nijigasaki Orange` / `TWB48 - Odyssey Gold` / `TWB49 - Patrick Star Pink` / `TWB50 - Pikachu Yellow` / `TWB51 - Pocket Tales` / `TWB52 - Pokemon mini` / `TWB53 - Pretty Guardian Gold` / `TWB54 - S.E.E.S. Blue` / `TWB55 - Saint Snow Red` / `TWB56 - Scooby-Doo Mystery` / `TWB57 - Shiny Sky Blue` / `TWB58 - Sidem Green` / `TWB59 - Slime Blue` / `TWB60 - Spongebob Yellow` / `TWB61 - Stone Orange` / `TWB62 - Straw Hat Red` / `TWB63 - Superball Ivory` / `TWB64 - Super Saiyan Blue` / `TWB65 - Super Saiyan Rose` / `TWB66 - Supervision` / `TWB67 - Survey Corps Brown` / `TWB68 - Tea Midori` / `TWB69 - TI-83` / `TWB70 - Tokyo Midtown` / `TWB71 - Travel Wood` / `TWB72 - Virtual Boy` / `TWB73 - VMU` / `TWB74 - Wisteria Murasaki` / `TWB75 - WonderSwan` / `TWB76 - Yellow Banana` |
| Color Correction | `GBC Only` ✅ / `Always` / `Disabled` | `gambatte_gbc_color_correction` | `GBC only` / `always` / `disabled` |
| Color Correction Mode | `Accurate` ✅ / `Fast` | `gambatte_gbc_color_correction_mode` | `accurate` / `fast` |
| Color Correction - Frontlight Position | `Central` ✅ / `Above Screen` / `Below Screen` | `gambatte_gbc_frontlight_position` | `central` / `above screen` / `below screen` |
| Dark Filter Level (%) | `0` ✅ / `5` / `10` / `15` / `20` / `25` / `30` / `35` / `40` / `45` / `50` | `gambatte_dark_filter_level` | `0` / `5` / `10` / `15` / `20` / `25` / `30` / `35` / `40` / `45` / `50` |
| Emulated Hardware (restart) | `Auto` ✅ / `GB` / `GBC` / `GBA` | `gambatte_gb_hwmode` | `Auto` / `GB` / `GBC` / `GBA` |
| Use Official Bootloader (restart) | `Enabled` ✅ / `Disabled` | `gambatte_gb_bootloader` | `enabled` / `disabled` |
| Interframe Blending | `Disabled` ✅ / `Simple (Accurate)` / `Simple (Fast)` / `LCD Ghosting (Accurate)` / `LCD Ghosting (Fast)` | `gambatte_mix_frames` | `disabled` / `mix` / `mix_fast` / `lcd_ghosting` / `lcd_ghosting_fast` |
| Show Game Boy Link Settings | `Enabled` / `Disabled` ✅ | `gambatte_show_gb_link_settings` | `enabled` / `disabled` |
| Game Boy Link Mode | `Not Connected` ✅ / `Network Server` / `Network Client` | `gambatte_gb_link_mode` | `Not Connected` / `Network Server` / `Network Client` |
| Network Link Port | `56400` ✅ / `56401` / `56402` / `56403` / `56404` / `56405` / `56406` / `56407` / `56408` / `56409` / `56410` / `56411` / `56412` / `56413` / `56414` / `56415` / `56416` / `56417` / `56418` / `56419` / `56420` | `gambatte_gb_link_network_port` | `56400` / `56401` / `56402` / `56403` / `56404` / `56405` / `56406` / `56407` / `56408` / `56409` / `56410` / `56411` / `56412` / `56413` / `56414` / `56415` / `56416` / `56417` / `56418` / `56419` / `56420` |
| Network Link Server Address Pt. 01: \_\_\_.\_\_\_.\_\_\_.\_\_x | `0` ✅ / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` | `gambatte_gb_link_network_server_ip_1` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` |
| Network Link Server Address Pt. 02: \_\_\_.\_\_\_.\_\_\_.\_\_x | `0` ✅ / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` | `gambatte_gb_link_network_server_ip_2` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` |
| Network Link Server Address Pt. 03: \_\_\_.\_\_\_.\_\_\_.\_\_x | `0` ✅ / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` | `gambatte_gb_link_network_server_ip_3` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` |
| Network Link Server Address Pt. 04: \_\_\_.\_\_\_.\_\_\_.\_\_x | `0` ✅ / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` | `gambatte_gb_link_network_server_ip_4` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` |
| Network Link Server Address Pt. 05: \_\_\_.\_\_\_.\_\_\_.\_\_x | `0` ✅ / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` | `gambatte_gb_link_network_server_ip_5` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` |
| Network Link Server Address Pt. 06: \_\_\_.\_\_\_.\_\_\_.\_\_x | `0` ✅ / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` | `gambatte_gb_link_network_server_ip_6` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` |
| Network Link Server Address Pt. 07: \_\_\_.\_\_\_.\_\_\_.\_\_x | `0` ✅ / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` | `gambatte_gb_link_network_server_ip_7` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` |
| Network Link Server Address Pt. 08: \_\_\_.\_\_\_.\_\_\_.\_\_x | `0` ✅ / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` | `gambatte_gb_link_network_server_ip_8` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` |
| Network Link Server Address Pt. 09: \_\_\_.\_\_\_.\_\_\_.\_\_x | `0` ✅ / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` | `gambatte_gb_link_network_server_ip_9` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` |
| Network Link Server Address Pt. 10: \_\_\_.\_\_\_.\_\_\_.\_\_x | `0` ✅ / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` | `gambatte_gb_link_network_server_ip_10` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` |
| Network Link Server Address Pt. 11: \_\_\_.\_\_\_.\_\_\_.\_\_x | `0` ✅ / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` | `gambatte_gb_link_network_server_ip_11` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` |
| Network Link Server Address Pt. 12: \_\_\_.\_\_\_.\_\_\_.\_\_x | `0` ✅ / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` | `gambatte_gb_link_network_server_ip_12` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` |

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/libretro/gambatte-libretro/](https://github.com/libretro/gambatte-libretro)
* **Libretro documentation**: [https://docs.libretro.com/library/gambatte/](https://docs.libretro.com/library/gambatte/)
* **Official source code**: [https://github.com/sinamas/gambatte](https://github.com/sinamas/gambatte)
* **Old standalone source code**: [https://sourceforge.net/projects/gambatte/files/gambatte/](https://sourceforge.net/projects/gambatte/files/gambatte/)