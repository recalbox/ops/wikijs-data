---
title: Los drivers de los mandos PS3
description: 
published: true
date: 2023-09-21T14:52:20.907Z
tags: ps3, drivers, controladores, mandos
editor: markdown
dateCreated: 2023-09-19T09:31:16.178Z
---

Existen varias versiones de mandos PS3 _sixaxis_ y _dualshock3_ en el mercado.

Recalbox soporta los mandos oficiales por defecto. Pero si utilizáis un clon GASIA o Shanwan, necesitaréis modificar el controlador en [recalbox.conf](./../../../basic-usage/getting-started/recalboxconf-file) modificando la siguiente línea con el contenido de la tabla de abajo:

```ini
controllers.ps3.driver=oficial
```

| Modelo | Driver |
| :--- | :--- |
| Sixaxis | `official` |
| Dualshock3 | `official` |
| US Sixaxis | `shanwan` ?? |
| Shanwan | `shanwan` |
| Gasia | `gasia` |

Para determinar qué tipo de clon tenéis (GASIA o SHANWAN), intentad:

* Conectar el controlador por USB
* A continuación, utilizar el siguiente comando:

```shell
dmesg
```

Deberíais poder ver qué tipo de controlador es identificado por Recalbox.