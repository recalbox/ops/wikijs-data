---
title: Oricutron
description: 
published: true
date: 2024-06-23T11:51:34.469Z
tags: oric, oric-atmos, oricutron
editor: markdown
dateCreated: 2021-05-21T08:17:31.365Z
---



## ![](/emulators/license.svg) Licence

Ce core est sous licence [**GPLv2**](https://github.com/pete-gordon/oricutron/issues/159#issuecomment-749596344).

## ![](/emulators/compatibility.png) Compatibilité

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Fonctionnalités



## ![](/emulators/bios.svg) BIOS

### Liste des bios obligatoires

Vous trouverez les bios avec le nom indiqué dans la colonne **Description** qu'il vous faudra renommer avec le nom indiqué dans la colonne **Nom de fichier**.

| Nom de fichier | Description | MD5 | Fourni |
| :---: | :---: | :---: | :---: |
| basic11b.rom | - | a330779c42ad7d0c4ac6ef9e92788ec6 | ❌ |
| basic10.rom | - | ebe418ec8a6c85d5ac32956c9a96c179 | ❌ |
| jasmin.rom | - | 5136f764a7dbd1352519351fbb53a9f3 | ❌ |
| microdis.rom | - | df864344d2a2091c3f952bd1c5ce1707 | ❌ |
| pravetzt.rom | - | 8712a22e7e078de3343667d9fc1f2390 | ❌ |
| teleass.rom | - | 2324c9cc227c1327a72a667c97ed2990 | ❌ |
| hyperbas.rom | - | 364bf095e0dc4222d75354d50b8cddfc | ❌ |
| telmon24.rom | - | 9a432244d9ee4a49e8ddcde64af94e05 | ❌ |

### Emplacement

Placez les BIOS comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 oricutron
┃ ┃ ┃ ┃ ┣ 🗒 **basic11b.rom**
┃ ┃ ┃ ┃ ┣ 🗒 **basic10.rom**
┃ ┃ ┃ ┃ ┣ 🗒 **jasmin.rom**
┃ ┃ ┃ ┃ ┣ 🗒 **microdis.rom**
┃ ┃ ┃ ┃ ┣ 🗒 **pravetzt.rom**
┃ ┃ ┃ ┃ ┣ 🗒 **teleass.rom**
┃ ┃ ┃ ┃ ┣ 🗒 **hyperbas.rom**
┃ ┃ ┃ ┃ ┣ 🗒 **telmon24.rom**

### Liste des bios optionnels

| Nom de fichier | Description | MD5 | Fourni |
| :---: | :---: | :---: | :---: |
| bd500.rom | - | 2b6498fd29a0adbf1c529762c02c33ab | ❌ |
| 8dos2.rom | - | 5f3cd5a4307fed7a9dfe8faa4c044273 | ❌ |

### Emplacement

Placez les bios comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 oricutron
┃ ┃ ┃ ┃ ┣ 🗒 **bd500.rom**
┃ ┃ ┃ ┃ ┣ 🗒 **8dos2.rom**

## ![](/emulators/roms.png) Roms

### Extensions supportées

Les roms doivent avoir les extensions suivantes :

* .dsk
* .tap

### Emplacement

Placez les roms comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 oricatmos
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.dsk**

>Les roms au format **TOSEC** sont vivement conseillées.
{.is-success}

>Pour plus d'information sur les roms, rendez-vous sur [ce tutoriel](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuration avancée de l'émulateur

>Pour pouvoir conserver vos configurations personnalisées lors d'une mise à jour, nous vous conseillons d'utiliser notre fonctionnalité [Surcharges de configuration](./../../../advanced-usage/configuration-override).
{.is-info}

## ![](/emulators/external-links.png) Liens externes

* **Code source utilisé** : [https://github.com/pete-gordon/oricutron/](https://github.com/pete-gordon/oricutron/)
* **Documentation** : [http://www.petergordon.org.uk/oricutron/](http://www.petergordon.org.uk/oricutron/)