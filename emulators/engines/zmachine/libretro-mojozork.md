---
title: Libretro-mojozork
description: 
published: true
date: 2024-07-24T21:01:34.892Z
tags: libretro, z-machine, 9.1+, mojozork
editor: markdown
dateCreated: 2023-08-06T10:18:35.555Z
---

**Libretro-mojozork** is an interpreter for Infocom games and other developers for the **Z-machine**.

## ![](/emulators/license.svg) License

This core is under [**zlib**](https://github.com/icculus/mojozork/blob/main/LICENSE.txt) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Features

| Feature | Supported |
| :---: | :---: |

## ![](/emulators/bios.svg) BIOS

>**No bios are required.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Supported extensions

Roms must have the extension:

* .dat
* .zip
* .z1
* .z3
* .7z

Ce système supporte les roms compressées au format .zip/.7z. Attention toutefois, il ne s'agit que d'une archive.

Les fichiers contenus dans les .zip/.7z doivent correspondre aux extensions citées précédemment.
De plus, chaque fichier .zip/.7z ne doit contenir qu'une seule rom compressée.

### Location

Put the roms like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 zmachine
┃ ┃ ┃ ┃ ┣ 🗒 **game.zip**

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

> **This core have no option!**
{.is-success}

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/icculus/mojozork/](https://github.com/icculus/mojozork/)