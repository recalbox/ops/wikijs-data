---
title: Links úteis
description: 
published: true
date: 2023-09-24T20:38:45.855Z
tags: changelog
editor: markdown
dateCreated: 2021-06-24T23:50:31.523Z
---

## *Changelog*

* [Notas de lançamento](https://gitlab.com/recalbox/recalbox/raw/master/RELEASE-NOTES.md)


## Download

* [Última imagem do Recalbox](https://download.recalbox.com/)

## Nossos sites

* [www.recalbox.com](https://www.recalbox.com/fr/)
* [Gitlab](https://gitlab.com/recalbox/recalbox/)

## Nossas redes de suporte

* [Discord](https://discord.gg/NbQFbGM)
* [Fórum](https://forum.recalbox.com/)

## Nossas redes sociais

* [Facebook](https://www.facebook.com/recalbox)
* [Facebook da França](https://www.facebook.com/recalbox.fr)
* [Instagram](https://instagram.com/recalbox/)
* [Twitter](https://twitter.com/recalbox)

## Nossos *Streams*

* [Twitch](https://www.twitch.tv/recalbox)

## Nosso canal do youtube

* [Youtube](https://www.youtube.com/c/RecalboxOfficial)

