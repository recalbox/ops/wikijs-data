---
title: Wolfenstein 3D
description: 
published: true
date: 2024-07-26T08:31:31.637Z
tags: ports, wolfenstein3d
editor: markdown
dateCreated: 2023-09-04T12:53:03.645Z
---

![](/emulators/ports/wolfenstein3d.png){.align-center}

## Detalles técnicos

* **Creado por :** id Software
* **Fecha de lanzamiento :** 5 de mayo de 1992
* **Género:** Shooter en primera persona
* **Motor:** Wolfenstein Engine
* **Sistema operativo:** MS-DOS 5.0 o superior
* **Procesador:** Ordenador IBM 286
* **Almacenamiento:** 8 MB
* **RAM:** 528k de memoria convencional
* **Resolución**: 320x200
* **Sonido:** Sound Blaster y AdLib o tarjeta de sonido 100% compatible.
* **Desarrollador:** id Software
* **Editor:** Apogee Software (FormGen para Spear of Destiny)
* **Director:** Tom Hall
* **Diseñadores:** John Romero, Tom Hall
* **Programadores:** John Carmack, John Romero
* **Artista :** Adrian Carmack
* **Compositor:** Robert Prince (DOS)
* **PEGI** : 12+

## Presentación

**Wolfenstein 3D** es un videojuego de disparos en primera persona desarrollado por id Software y publicado por Apogee Software el 5 de mayo de 1992 para MS-DOS. Está basado en los juegos Castle Wolfenstein y Beyond Castle Wolfenstein desarrollados por Muse Software en la década de 1980. El jugador asume el papel del espía B.J. Blazkowicz en su intento de escapar de un castillo nazi en el que está prisionero durante la Segunda Guerra Mundial. Para ello, tiene que explorar varios niveles para encontrar el ascensor o al jefe y luchar contra los enemigos utilizando un cuchillo, una pistola y otras armas que pueda encontrar.

Wolfenstein 3D es el segundo gran juego publicado por id Software tras la serie Commander Keen. Su desarrollo comenzó en 1991, cuando John Carmack intentaba crear un motor 3D de alto rendimiento, con el que experimentó en Hovertank 3D y Catacomb 3-D. Durante una reunión de diseño, el estudio decidió dar a su próximo título una dirección más violenta y el programador John Romero sugirió hacer un remake de Castle Wolfenstein utilizando el motor 3D desarrollado por John Carmack. El juego fue diseñado por John Romero y Tom Hall, con ilustraciones de Adrian Carmack y efectos de sonido y música de Bobby Prince. El juego fue publicado como shareware por Apogee en 1992 en dos series de tres episodios. Ese mismo año, FormGen publicó un episodio adicional, Spear of Destiny.

En su lanzamiento, Wolfenstein 3D fue un éxito comercial y de crítica, ganando numerosos premios y vendiendo más de 200.000 copias a finales de 1992. También se le conoce como el precursor del famoso juego Doom, por lo que puede considerarse el primer "juego tipo Doom", ya que contribuyó a popularizar el género y a definir sus principios básicos, que más tarde se copiarían en numerosos títulos. También demostró la viabilidad de la distribución shareware.

## Historia

Wolfenstein 3D tiene lugar durante la Segunda Guerra Mundial. Los jugadores asumen el papel del espía estadounidense B.J. Blazkowicz en su lucha contra los nazis. En la versión original del juego, dividida en tres episodios, el héroe primero intenta escapar del castillo Wolfenstein tras ser capturado en una misión para recuperar los planos de una operación secreta. Tras escapar, consigue recuperar y frustrar los planes de sus enemigos, que intentan crear un ejército de mutantes. A continuación, se infiltra en el Führerbunker, bajo la Cancillería del Reich, donde finalmente se enfrenta a Adolf Hitler, en un traje robótico equipado con cuatro ametralladoras Gatling.

La segunda trilogía, titulada Las misiones nocturnas, es una precuela de la primera en la que B.J. Blazkowicz intenta frustrar los planes nazis de fabricar armas químicas. El primer episodio sigue una persecución por el complejo de investigación armamentística del científico responsable del desarrollo de estas armas químicas. A continuación, el héroe viaja al castillo de Erlangen para robar los planos de estas armas, y después al castillo de Offenbach, donde se enfrenta al general nazi responsable de la operación.

El episodio extra, publicado bajo el título Spear of Destiny, es también una precuela del juego original. En él, el jugador sigue asumiendo el papel de B.J. Blazkowicz en su intento de recuperar la Lanza Sagrada de manos de los nazis. En las dos expansiones de Spear of Destiny, Return to Danger y Ultimate Challenge, el héroe también debe recuperar la Lanza Sagrada, que los nazis han robado una vez más con el fin de utilizarla para construir un arma nuclear o invocar demonios.

## Emuladores

[Libretro ECWolf](libretro-ecwolf)


