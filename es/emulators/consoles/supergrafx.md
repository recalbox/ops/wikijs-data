---
title: SuperGrafX
description: NEC SuperGrafX
published: true
date: 2024-07-26T08:06:44.753Z
tags: nec, supergrafx, consolas
editor: markdown
dateCreated: 2023-09-03T20:27:18.830Z
---

![](/emulators/consoles/supergrafx.svg){.align-center}

## Datos técnicos

* **Fabricante**: NEC
* **Año de lanzamiento**: 1989
* **Cantidad vendida**: 7,5 millones
* **Juego más vendido**: Ghouls 'n Ghosts
* **Procesador**: HuC6280 8 bits 7,16 Mhz
* **RAM**: 8 Kbytes
* **Unidad de procesamiento de imagen**: 16 bits
* **RAM de vídeo**: 64 Kbytes
* **Soportes** : HU-Card, SHU-Card, CD-ROM, Super-CD-ROM

## Presentación

La **SuperGrafx** es una consola de videojuegos fabricada por NEC Corporation y lanzada en diciembre de 1989 en Japón.

Se trata de una versión mejorada del PC-Engine (o CoreGrafX). En comparación con esta última, tenía cuatro veces más memoria para el procesador, un segundo chip de vídeo con su propia memoria y un controlador de prioridad que permitía combinar la salida de los dos chips de vídeo de diferentes maneras.

La consola salió a la venta en Francia en mayo de 1990 a un precio de 2.490 FF sin juegos. En noviembre de 1990, se redujo a 1990 FF con un juego, y luego, en junio de 1991, a 1.490 FF con un juego.

La SuperGrafx fue en gran medida un fracaso debido a la falta de software que utilizara sus capacidades: sólo se produjeron 5 títulos. Sin embargo, era compatible con muchos juegos de PC-Engine. Ningún juego de CD, Super CD o Arcade CD sacó partido de la plataforma.

Además, su sencillo procesador de 8 bits tenía una carga de trabajo bastante pesada para controlar el hardware de vídeo adicional.

## Emuladores

[Libretro FBNeo](libretro-fbneo)
[Libretro Mednafen_SuperGrafx](libretro-mednafen_supergrafx)