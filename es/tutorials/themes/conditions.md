---
title: 3. Las condiciones
description: 
published: true
date: 2024-04-07T13:23:37.657Z
tags: 9.2+, temas
editor: markdown
dateCreated: 2024-04-07T13:22:08.356Z
---

## Introducción

Las condiciones son una nueva opción disponbile para crear vuestros themes. Si sois desarrolladores, no os debería sonar a chino.

Las condiciones se utilizan con el atributo `if="condition"` dentro de:

- las etiquetas `<include>`

```xml
<include if="crt">
  ...
</include>
```

- las etiquetas `<view>`

```xml
<view name="system" if="arcade">
  ...
</view>
```

- los objetos

```xml
<image name="deco" extra="true" if="favorite">
  ...
</image>
```

```xml
<image name="deco" extra="true" if="favorite" />
```

- las propiedades de los objetos

```xml
<image name="deco" extra="true">
  ...
  <path>default/image/path.png</path>
  <path if="crt">image/path/for/crt/only.png</path>
  ...
</image>
```

```xml
<image name="deco" extra="true" path="default/image/path.png">
  <path if="crt">image/path/for/crt/only.png</path>
</image>
```

## Funcionamiento

Las condiciones pueden ser únicas o complejas. Cada pseudo-identificador se evalúa a `true` / `false` (`verdadero` / `falso`) cuando se cambia o actualiza el tema. Pueden combinarse o invertirse mediante sencillas operaciones de lógica booleana: `and`, `or` y `not`.

### Ejemplos sencillos

Ejemplo 1 :

```xml
if="qvga and not crt"
```

```xml
if="qvga & !crt"
```

Ejemplo 2 :

```xml
if="hd or fhd"
```

```xml
if="hd | fhd"
```

### Ejemplos más complicados

```xml
if="qvga or vga and not crt"
```

```xml
if="qvga | vga & !crt"
```

La evaluación se realiza de derecha a izquierda, siempre entre el último resultado de la izquierda y el nuevo identificador de la derecha.

La siguiente línea :

```xml
if="qvga or vga and not crt"
```

que equivale a `(qvga ou vga) et pas crt` no es igual que ésta otra:

```xml
if="not crt and qvga or vga"
```

Que equivale a `(pas crt et qvga) ou vga` !

Para facilitar la escritura *humana*, `&` equivale a `and`, `|` equivale a `or` y `!` equivale a `not`. Los caracteres `&|!` pueden aparecer pegados a otros identificadores (por ejemplo `qvga&!crt`) mientras que `and`, `or` y `not` necesitan estar separados por espacios (`qvga and not crt`).

## Los identificadores disponibles

- `crt` :

Verdadero si hay un adaptador CRT conectado y activo.

- `jamma` :

Verdadero si hay un adaptador Jamma RGB conectado y activo.

- `overscan` :

Corresponde a `crt and !jamma`.

- `tate` :

Verdaderos si estamos en modo TATE.

- `qvga` :

Verdadero si la altura máxima es de 288 píxeles.

- `vga` :

Verdadero si la altura máxima es de 576 píxeles y no qvga.

- `hd` :

Verdadero si la altura máxima es de 920 píxeles y no está en qvga o vga.

- `fhd` :

Verdadero si la altura es superior a 920 píxeles.

- `virtual` :

Verdadero si estamos en un sistema virtual.

- `arcade` :

Verdadero si estamos en un sistema arcade.

- `port` :

Verdadero si estamos en el sistema de ports.

- `console` :

Verdadero si estamos en un sistema consola de salón.

- `handheld` :

Verdadero si estamos en un sistema consola portable.

- `computer` :

Verdadero si estamos en un sistema de ordenador.

- `fantasy` :

Verdadero si estamos en un sistema fantasy.

- `engine` :

Verdadero si estamos en un motor de juego.

- `favorite` :

Verdadero si estamos en el sistema de favoritos.

- `lastplayed` :

Verdadero si estamos en el sistema de últimos juegos jugados.

## Combinaciones

Podéis identificar groupos de sistemas facilmente con las siguientes condiciones:

- Todos los sitemas arcade por constructor:

```xml
if="arcade & virtual"
```

- Todos los tipos de consolas:

```xml
if="console | handheld | fantasy"
```

- Todos los sitemas extraños que no poseen una existencia física:

```xml
if="fantasy | engine"
```