---
title: Playstation 1
description: 
published: true
date: 2021-12-12T17:12:33.544Z
tags: sony, ps1, playstation 1
editor: markdown
dateCreated: 2021-08-07T23:55:06.823Z
---

Here you can discover several tutorials related to the PlayStation.

Here are the available tutorials:

[Convert .BIN image discs to .ISO](convert-psx-bin-to-iso)
[Convert a .GME save to .SRM](convert-gme-save-to-srm)
[Convert disc images to .BIN + .CUE](convert-disc-files-to-bin+cue)
[Dump your own games quickly](dump-you-own-games-quickly)
[Enable analog mode](enable-analog-mode)
[FAQ](faq)
[Grab the ps1_rom.bin bios from the PS3 firmware](grab-ps1-bios-from-ps3-firmware)
[Memory cards management](memory-cards-management)
[Multi-disc management with .M3U](multidisc-management-with-m3u)
[Multi-disc management with .PBP](multidisc-management-with-pbp)
[Use ePSXe (.MCR) saves into RetroArch (.SRM)](use-epsxe-mcr-saves-into-srm)