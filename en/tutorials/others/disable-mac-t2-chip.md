---
title: Disable Mac T2 chip
description: 
published: true
date: 2021-08-06T15:31:55.107Z
tags: mac, t2, chip, disable
editor: markdown
dateCreated: 2021-08-06T15:31:55.107Z
---

## Introduction

If you are using an Intel Mac with a T2 security chip, you need to change the boot security in order to be able to boot on an external media (SD card or USB key).

## Usage

* Restart your Mac.
* At the beginning of the reboot, press and hold the key combination `Command (⌘) + R`.
* Once started, select the administrator account to use and fill in its password.
* Select `Tools` > `Boot Security Tool`.
* On the new window, select the `No security` and `Allow booting from external media` options and close the window.
* Reboot
* At the start of the reboot, hold down the `Option (⌥)` key and once you have the available drives displayed on your screen, select `EFI Boot`.

Enjoy the game!