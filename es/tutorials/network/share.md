---
title: Compartir la red
description: Cómo acceder a Recalbox desde carpetas compartidas
published: true
date: 2024-07-26T06:26:17.929Z
tags: red, unidades, compartidas, carpetas
editor: markdown
dateCreated: 2023-09-13T06:52:39.173Z
---


En esta sección encontraréis ayuda para todo lo que es compartir información a través de la red local.

Aquí tenéis la lista de tutoriales disponibles:

[Cargar roms a través de una carpeta compartida con Samba (NAS)](roms-on-a-network-share-samba-nas)
[Imposible acceder a Recalbox desde la red de Windows](access-network-share)