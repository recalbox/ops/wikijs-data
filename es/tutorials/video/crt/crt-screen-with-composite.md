---
title: Conectad vuestra Recalbox a un CRT en composite
description: 
published: true
date: 2024-08-25T19:58:52.668Z
tags: crt, composite, pantalla
editor: markdown
dateCreated: 2023-09-12T11:43:42.692Z
---

Si quieres conectar tu Recalbox a un monitor CRT, necesitarás un cable mini-jack a RCA como este:

![RPi Audio/Video Socket](/tutorials/video/crt/rpi_av_socket.jpg)

>En los cables de las videocámaras, la salida de vídeo podría estar en la toma de audio derecha (roja).
>
>Además, como el tipo de cable necesario no está muy extendido, es preferible utilizar un multímetro para comprobar que el cable utilizado se corresponde con el esquema sugerido anteriormente (una inversión de la masa y el vídeo en la toma da lugar a una imagen irregular en blanco y negro, señal de un cable inadecuado).
{.is-warning}

## Configuración

### Fichero config.txt

>Este fichero se reemplaza cada vez que se actualiza el sistema. ¡Tendréis que rehacer los siguientes cambios en cada actualización !
{.is-warning}

* Modificad el fichero [config.txt](./../../../tutorials/system/modification/configtxt-configuration), la línea siguiente (hay 2, buscad la que corresponde a vuestro sistema) :

```ini
dtoverlay=vc4-kms-v3d,cma-512
```

Y reemplazadla por esta otra:

```ini
dtoverlay=vc4-kms-v3d,composite=1
```

* Buscad la siguiente línea y comentadla poniendo un '#' al pricipio, como aquí:

```ini
#hdmi_enable_4kp60=1
```

### Fichero recalbox-user-config.txt

* Modificad el fichero [recalbox-user-config.txt](./../../../tutorials/system/modification/configtxt-configuration) y añadid el `sdtv_mode` soportado (sólo una de las siguientes líneas):

```ini
sdtv_mode=0    # NTSC normal
sdtv_mode=1    # NTSC japonais – no pedesta
sdtv_mode=2    # PAL normal
sdtv_mode=3    # PAL brésilien – 525/60 rather than 625/50, different subcarrier
```

* Para tener un mejor sonido en la salida composite, activad el driver audio experimental para el  Raspberry Pi :

```ini
audio_pwm_mode=2
```

* Este modo puede ralentizar Recalbox en general. Si este es el caso, cambiad la línea a comentario y forzad la salida de audio por el jack:

```ini
#audio_pwm_mode=2
hdmi_drive=1
```

* Finalmente, añadid `hdmi_ignore_hotplug=1` para forzar el composite. Vuestro fichero recalbox-user-config.txt debería tener este aspecto:

```ini
sdtv_mode=2
hdmi_ignore_hotplug=1
audio_pwm_mode=2
```

## En modo composite todo es más lento

La opción `audio_pwm_mode=2` mejora el sonido pero puede hacer más lento el sistema en modo composite, en particular en los Raspberry Pi Zéros. En este caso, podéis probar a utilizar el siguiente valor:

```ini
audio_pwm_mode=0
```