---
title: CDi
description: Philips CDi
published: true
date: 2024-07-26T07:46:23.086Z
tags: philips, cdi, 8.1+, consolas
editor: markdown
dateCreated: 2023-09-03T15:47:35.800Z
---

![](/emulators/consoles/cdi.svg){.align-center}

## Datos técnicos

* **Fabricante**: Philips, Magnavox
* **Año de lanzamiento:** 1991
* **Sistema operativo:** CD-RTOS (Compact Disc Real Time Operating System)
* **Procesador:** 16-bit CISC (680070) @ 15.5MHz
* **RAM :** 1 MB
* **VRAM:** 1 MB
* **ROM :** 512 KB
* **GPU:** SCC66470, MCD 212 posterior
* **Chip de sonido:** Sonido estéreo de 16 bits con frecuencia de muestreo de 44,1KHz. 
* **Resolución:** 384x280 a 768x560 (16,7 millones de colores)

## Presentación

CD-i, acrónimo de Compact Disc Interactive, es una consola de videojuegos y reproductor multimedia diseñada por Philips, Sony y Matsushita para imponer un nuevo estándar "multimedia", concepto muy en boga desde principios de 1991. Este sistema multimedia ofrece más funciones que un reproductor de CD de audio o una consola de videojuegos, sin dejar de ser menos caro que un ordenador personal. En comparación con un ordenador personal, el coste es menor porque no hay disco duro, disquetera, teclado ni ratón, se utiliza un televisor normal en lugar de un monitor especial y el sistema operativo es más sencillo.

El nombre CD-i también hace referencia al soporte, el disco CD-i, un formato derivado del Compact Disc estándar. Sus especificaciones se describen en el Libro Verde. Se trata de un disco óptico utilizado para almacenar datos en formato digital para su lectura por un reproductor compatible con CD-i.

El desarrollo del CD-i comenzó en 1984, con el primer anuncio público del proyecto en 1986. Los primeros reproductores salieron a la venta en 1992, en Francia en septiembre de 1992. Son capaces de reproducir discos CD-i, CD de audio, CD de karaoke, CD de fotos y CD de vídeo, aunque estos últimos requieren el módulo Full Motion Video para descodificar MPEG-1. El sistema no sólo admite juegos, sino también títulos educativos como enciclopedias interactivas, visitas a museos, etc. Los discos CD-i pueden contener datos en bruto, además de vídeo o sonido, pero no pueden reproducirse en un reproductor de CD estándar y se diseñaron para su uso con televisión.


Considerado por el gran público como una consola de juegos, el formato CD-i resultó ser un fracaso comercial. Philips perdió casi mil millones de dólares en todo el proyecto. El aparato estuvo a la venta hasta 1998, pero el fracaso del formato CD-i obligó a Philips a abandonar la industria de los videojuegos. El CD-i sigue siendo una de las pocas consolas de videojuegos creadas por una empresa europea. La consola se hizo famosa a su pesar por albergar dos de los peores juegos de la franquicia Zelda.


## Emuladores


[Libretro-cdi2015](libretro-cdi2015)
[Libretro-same_cdi](libretro-same_cdi)

