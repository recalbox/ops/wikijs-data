---
title: Actualización del bootloader del Raspberry Pi 4
description: 
published: true
date: 2024-09-29T13:38:10.794Z
tags: rpi4, bootloader, rpi400, actualización
editor: markdown
dateCreated: 2023-09-16T08:09:40.112Z
---

## Introducción

El bootloader (también llamado eeprom) del Raspberry Pi es un microprograma que permite interactuar con la BIOS del sistema y que proporciona los servicios básicos de arranque a vuestro Raspberry Pi 4/400/5.

Se aconseja actualizar periódicamente este microprograma en los Rasberry Pi 4/400/5, ya que puede corregir determinados bugs del sistema operativo.

## Procedimiento

El proceso de actualización del bootload es muy sencillo. Sólamente necesitáis disponer de la última versión de Recalbox.

En la interfaz de Recalbox, pulsad `START` e id a `ADVANCED SETTINGS` > `UPDATE BOOTLOADER`.

Aparecerá una ventana mostrando la versión instalada y la versión disponible.

![](/tutorials/utilities/upgrade-rpi4-bootloader/bootloaderen1.png){.full-width}

Ejecutad la actualización seleccionando `UPDATE`. Al cabo de 10 segundos recibiréis un mensaje indicando que la actualización se ha efectuado correctamente.

![](/tutorials/utilities/upgrade-rpi4-bootloader/bootloaderen2.png){.full-width}

Una vez validado el mensaje, se os pedirá reiniciar. Confirmad el reinicio para comenzar a utilizar el nuevo bootloader.

![](/tutorials/utilities/upgrade-rpi4-bootloader/bootloaderen3.png){.full-width}

Podéis comprobar que el gestor de arranque se ha actualizado correctamente volviendo dentro del mismo menú. Aparecerá un mensaje indicando que el gestor de arranque está actualizado.

![](/tutorials/utilities/upgrade-rpi4-bootloader/bootloaderfr4.png){.full-width}