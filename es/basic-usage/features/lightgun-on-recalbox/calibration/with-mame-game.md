---
title: Con un juego MAME
description: 
published: true
date: 2023-09-23T20:12:55.125Z
tags: lightgun, mame, 7.2+, calibración
editor: markdown
dateCreated: 2023-09-23T20:12:55.125Z
---

## Ejemplo de calibración en MAME 2003 plus

1) Iniciad el juego y activad este menú pulsando L3 en vuestro mando.

![](/basic-usage/features/lightgun/calibration/mame1.png){.full-width}

2) Utilizad el dpad o el stick izquierdo para desplazaros hasta "Controles analógicos" y pulsad `X` _(mando de SNES como referencia, por supuesto_) para confirmar.

![](/basic-usage/features/lightgun/calibration/mame2.png){.full-width}

3) Seleccionad y ajustad la "Sensibilidad X del Lightgun" y la "Sensibilidad Y del Lightgun" al 50 o al 100% (puede ajustarse al 25% por defecto) usando el dpad o el stick izquierdo.

![](/basic-usage/features/lightgun/calibration/mame3.png){.full-width}

4) Salid de los menús pulsando `X` _(mando de SNES como referencia)_ para volver al juego. La configuración surtirá efecto inmediatamente ¡Disfrutad! 

5) La próxima vez que lancéis el juego, la configuración se guardará (¡¡¡mantén tus "saves" en lugar seguro!!!)

>Hay que guardar bien estos ficheros para no perder la configuración, que quedará grabada junto con vuestros "saves": 
>
>![](/basic-usage/features/lightgun/calibration/calibration1.png){.full-width}
{.is-warning}