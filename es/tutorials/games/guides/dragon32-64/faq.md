---
title: FAQ
description: 
published: true
date: 2023-09-19T18:58:38.396Z
tags: faq
editor: markdown
dateCreated: 2023-09-19T18:58:38.396Z
---

## Algunas informaciones útiles...

- El core XRoar soporta :
  - Suavizados (en EmulationStation, `START` > `GAME OPTIONS` > `SMOOTHING`).
  - La configuración se puede sobrecargar, pero no se sobrecarga recursivamente. La primera que se encuentra es la que se tiene en cuenta: `rom.ext.config` > `<carpeta>.config` > `<padre>.config` > ... La sintaxis del fichero de configuración no permite gestionar las sobrecargas de una forma óptima. Para generar un archivo de configuración, podéis utilizar este comando SSH: `xroar --config-all > .config`.
- Por defecto, la máquina Dragon es una Dragon64
- La máquina a utilizar se selecciona automáticamente en función del archivo :

```
"/dragon/": "dragon64",
"/dragon32/": "dragon32",
"/dragon64/": "dragon64",
"/tano/": "tano",
"/dragon200/": "dragon200e",
"/dragon200e/": "dragon200e",
```

- Los joystick 0 y 1 se reconocen automáticamente, independientemente del orden elegido en EmulationStation.
- Si la máquina "imprime", el texto impreso irá al fichero `/recalbox/share/saves/dragon/printer/rom-extension.txt`.