---
title: Cases (Carcaças)
description: 
published: true
date: 2021-11-28T22:51:51.579Z
tags: cases, compatibilidade
editor: markdown
dateCreated: 2021-11-28T22:38:58.086Z
---

## Pi ZERO/Pi ZERO W

### GPi CASE

![Para Rpi Zero e Rpi Zero W](/compatibility/cases/gpicase.png)


## PI2/PI3/PI3B/PI3B+

### NESPi CASE

![](/compatibility/cases/nespicase.png)


### NESPi CASE PLUS

![Para Rpi 2, 3, 3B e 3B +](/compatibility/cases/nespicaseplus.png)


### SUPERPI CASE J

![Para Rpi 2B, 3B et 3B+](/compatibility/cases/superpicase.png)


### SUPERPI CASE U

![Para Rpi 2, 3 et 3B+](/compatibility/cases/superpicaseusa.png)

### SUPERPI CASE U (New)

![Para Rpi 2, 3 et 3B+](/compatibility/cases/superpicaseusanew.png)

### MEGAPi CASE M

![Para Rpi 2B, 3B et 3B+](/compatibility/cases/megapicase.png)

## PI4

### NESPi 4 CASE

![Para Rpi 4](/compatibility/cases/nespi4case.png)

### Argon One

![Para Rpi 4](/compatibility/cases/argonone.png)

### Thingivers 

Sem pré-visualização, diversas cases diferentes para imprimir com uma impressora 3D.

[Link para download](https://www.thingiverse.com/tag:raspberry_pi_4)

## Carcaças baratas e funcionais para o RPI4

### GeeekPi

![Para Rpi 4](/compatibility/cases/geekpi.png)

### GeeekPi

![Para Rpi 4](/compatibility/cases/geekpi2.png)


### Bruphny

![Para Rpi 4](/compatibility/cases/bruphny.png)

## Odroid XU4

### Carcaça Gaming

![Para Odroid XU4](/compatibility/cases/ogst.png)
