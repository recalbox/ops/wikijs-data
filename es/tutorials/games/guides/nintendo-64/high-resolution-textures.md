---
title: Texturas de alta resolución
description: 
published: true
date: 2025-01-31T17:36:53.789Z
tags: n64, packs, textura
editor: markdown
dateCreated: 2023-09-20T21:24:33.048Z
---

## Introducción 

Las texturas de alta resolución (Hi-res) son paquetes creados por aficionados para mejorar las texturas por defecto de un juego.

**Rice**, desarrollador del plugin **Rice Video**, fue el primero en hacer posible extraer texturas de un juego, modificarlas y reimportarlas a mayor resolución. Ahora es posible sustituir las texturas borrosas de los juegos por versiones de alta definición o una versión completamente distinta. Entusiasmados por esta nueva posibilidad, muchos diseñadores gráficos han empezado a crear texturas de sustitución para sus juegos favoritos.

## Requisitos previos 

Para instalar nuevas texturas, aseguráos de que :

1. tenéis una ROM compatible con vuestro pack de texturas ya que muchos packs de texturas sólo funcionan con una ROM específica. Por ejemplo, la ROM (U) es compatible mientras que la ROM (E) no lo es. Averigüad más leyendo el fichero readme, buscando en el foro o en Google.
2. Disponed de los ficheros fuente para la instalación (directorio con archivos \*.png). A veces los packs se suministran en forma de archivos \*.hts, ¡pero este procedimiento no es válido para este formato de archivo! 

### Configuración

### Copiad las texturas de alta resolución en Recalbox

Colocad la carpeta que contiene las texturas de alta resolución de cada uno de vuestros juegos en el siguiente directorio :

```text
/recalbox/share/bios/Mupen64plus/hires_texture/nom_du_jeu/
```

### Activad el emulador Libretro Mupen64Plus_Next:

Tenéis que decirle al juego en cuestión que arranque con el emulador correcto.

* Después de reiniciar, lanzad vuestro juego con `START`.
* Id a `MODIFICAR JUEGO` > `INICIAR CON`.
* Cambiad el emulador a `LIBRETRO MUPEN64PLUS_NEXT`.
* Cerrad los menús.

### Activación de las texturas de alta resolución

Ahora tenéis que activar las texturas de alta resolución en el emulador.

* Lanzad un juego.
* Cuando el juego comience, id al menú de RetroArch pulsando `Hotkey + B.`.
* Pulsad "Atrás" dos veces e id a "Ajustes" > "Configuración".

![](/tutorials/games/guides/nintendo-64/hires-textures/hirestextures1fr.png){.full-width}

* Activad el parámetro `Guardar configuración al salir`.

![](/tutorials/games/guides/nintendo-64/hires-textures/hirestextures2fr.png){.full-width}

* Pulsad "Atrás" dos veces e id a "Menú principal" > "Menú rápido" > "Opciones".

![](/tutorials/games/guides/nintendo-64/hires-textures/hirestextures3fr.png){.full-width}

* Luego vaya a `GLideN64`.

![](/tutorials/games/guides/nintendo-64/hires-textures/hirestextures4fr.png){.full-width}

* Activa el parámetro "Use High-Res textures".

![](/tutorials/games/guides/nintendo-64/hires-textures/hirestextures5fr.png){.full-width}

* Haced « Volver » 2 veces et id a  `Reanudar`.
* Salid del juego y volved a lanzarlo. El juego debería cambiar sus texturas en alta resolución.

Cuando el pack de texturas de alta resolución y la rom sean correctos, el juego se iniciará con el pack de texturas de alta resolución.

## Solución de problemas

### ¿Habéis seguido las instrucciones anteriores y sigue sin funcionar?

Tal vez sea un problema con el nombre...  
Muchos de estos packs de texturas se suministran con el nombre de carpeta correcto, ¡pero no todos! El nombre de la carpeta debe ser exactamente el mismo que el nombre interno de vuestra rom.

>El nombre del fichero de rom es `Mario Kart 64 (USA).v64` pero el nombre interno de la rom es `MARIOKART64` !.
{.is-warning}

### ¿Cómo puedo averiguar el nombre interno de mi rom?

En Windows, podéis utilizar **Tool64** (Google también es vuestro mejor amigo).

* Lanzad **Tool64**;
* `File` -> `Open`
* Seleccionad vuestro directorio de roms.

![](/tutorials/games/guides/nintendo-64/hires-textures/hirestextures6.png){.full-width}

* Haced click derecho en una rom y pulsad en `Rom Properties…`

![](/tutorials/games/guides/nintendo-64/hires-textures/hirestextures7.png){.full-width}

Observad la línea `Rom Name` para obtener el nombre interno de vuestra rom.