---
title: Libretro TyrQuake
description: 
published: true
date: 2024-07-25T08:17:40.648Z
tags: libretro, quake 1, tyrquake
editor: markdown
dateCreated: 2021-06-29T22:23:39.049Z
---



## ![](/emulators/license.svg) License

This core is under [**GPLv2**](https://github.com/libretro/tyrquake/blob/master/gnu.txt) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Features



## ![](/emulators/bios.svg) BIOS

>**No bios is required.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Supported extensions

Roms must have the extension:

* .pak

### Location

Put your roms like this: 

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 ports
┃ ┃ ┃ ┃ ┣ 📁 Quake
┃ ┃ ┃ ┃ ┃ ┣ 📁 id1
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 **pak0.pak**

This rom is included in your Recalbox.

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Accessing to options

You can configure various options in two different ways.

* Via RetroArch menu:

┣ 📁 RetroArch Menu
┃ ┣ 📁 Core options
┃ ┃ ┣ 🧩 Name_option

* Via the file `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core options

| Options | Options values | Variables (for configuration override) | Variables values |
| --- | --- | --- | --- |
| Internal resolution(restart) | `320x200` ✅ / `320x240` / `320x480` / `360x200` / `360x240` / `360x400` / `360x480` / `400x224` / `400x240` / `480x272` / `512x224` / `512x240` / `512x384` / `512x512` / `640x224` / `640x240` / `640x448` / `640x400` / `640x480` / `720x576` / `800x480` / `800x600` / `960x720` / `960x600` / `1024x768` / `1280x720` / `1280x800` / `1600x900` / `1600x1000` / `1600x1200` / `1920x1080` / `1920x1200` | `tyrquake_resolution` | `320x240` / `320x480` / `360x200` / `360x240` / `360x400` / `360x480` / `400x224` / `400x240` / `480x272` / `512x224` / `512x240` / `512x384` / `512x512` / `640x224` / `640x240` / `640x448` / `640x400` / `640x480` / `720x576` / `800x480` / `800x600` / `960x720` / `960x600` / `1024x768` / `1280x720` / `1280x800` / `1600x900` / `1600x1000` / `1600x1200` / `1920x1080` / `1920x1200` |
| Framerate (restart) | `Auto` ✅ / `10fps` / `15fps` / `20fps` / `25fps` / `30fps` / `40fps` / `50fps` / `60fps` / `72fps` / `75fps` / `90fps` / `100fps` / `119fps` / `120fps` / `144fps` / `155fps` / `160fps` / `165fps` / `180fps` / `200fps` / `240fps` / `244fps` / `300fps` / `360fps` | `tyrquake_framerate` | `auto` / `10` / `15` / `20` / `25` / `30` / `40` / `50` / `60` / `72` / `75` / `90` / `100` / `119` / `120` / `144` / `155` / `160` / `165` / `180` / `200` / `240` / `244` / `300` / `360` |
| Colored Lighting (restart) | `Disabled` ✅ / `Enabled` | `tyrquake_colored_lighting` | `disabled` / `enabled` |
| Rumble | `Disabled` ✅ / `Enabled` | `tyrquake_rumble` | `disabled` / `enabled` |
| Invert Y Axis | `Disabled` ✅ / `Enabled` | `tyrquake_invert_y_axis` | `disabled` / `enabled` |
| Analog Deadzone (percent) | `0` / `5` / `10` / `15` ✅ / `20` / `25` / `30` | `tyrquake_analog_deadzone` | `0` / `5` / `10` / `15` / `20` / `25` / `30` |

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/libretro/tyrquake/](https://github.com/libretro/tyrquake/)
* **Libretro documentation**: [https://docs.libretro.com/library/tyrquake/](https://docs.libretro.com/library/tyrquake/)