---
title: Wer sind wir?
description: 
published: true
date: 2021-06-22T08:40:00.852Z
tags: 
editor: markdown
dateCreated: 2021-06-22T08:40:00.852Z
---

## Vorstellung

**Die Recalbox** ist eine Open-Source-Retrogaming-Konsole, die es dir ermöglicht, eine Vielzahl von Konsolen und Plattformen zu spielen - und das mit Leichtigkeit!

**Recalbox** ist das **Betriebssystem** des Recalbox-Projekts, eine **gebrauchsfertige** Emulationskonsole auf Basis des **Raspberry Pi**, die komplett **kostenlos und Open Source** ist und speziell dafür entwickelt wurde, dass **du im Handumdrehen deine eigene erstellen kannst**.

Recalbox bietet **eine Vielzahl an Konsolen und Spielsystemen**. Von den ersten Arcade Systemen über den NES, den MEGADRIVE, bis hin zu 32-bit Plattformen wie der Playstation.

**Recalbox** verwendet mehrere bestehende Elemente, wie [EmulationStation](https://github.com/Aloshi/EmulationStation) als **Oberfläche**, und [piFBA](https://github.com/digitalLumberjack/pifba) und [RetroArch](https://github.com/libretro/RetroArch) als **Emulatoren**.

**Recalbox** lässt sich mit einem Wort beschreiben: **EINFACH**.

Wir wollen ein System anbieten, das so benutzerfreundlich und intuitiv wie möglich ist. Es muss also nicht erst in vielen Konfigurationsdateien etwas geändert  oder eine Verbindung über SSH hergestellt werden. Einfach das System installieren und spielen.

Die Systemkonfiguration erfolgt über eine einzige Datei, die `recalbox.conf`.   
Diese Datei ermöglicht eine Feinkonfiguration aller Emulatoren!

Jedoch haben wir für **fortgeschrittene Benutzer**, die mehr **Kontrolle über die Konfiguration** haben wollen, die Möglichkeit eingebaut, ein System nach ihrem Geschmack mit Hilfe von **Overrides** zu erstellen (bitte ändere nur etwas, wenn du dir sicher bist, was du tust).

### Zahlreiche Open-Source-Software in einem einzigen System zusammengefasst.

**Recalbox** basiert auf dem GNU/Linux-System mit Buildroot.  
Das System integriert Hunderte von Open-Source-Software, vom kleinsten Dienstprogramm über die Emulatoren bis hin zum EmulationStation-Frontend.

Entdecke zahlreiche Emulatoren von RetroArch/Libretro, den berühmten Kodi-Medienplayer und das maßgeschneiderte EmulationStation-Frontend!

![](/presentation/opensource.png)

### Entdecke mit Homebrews und Hacks Spiele, die offiziell nie erschienen sind!

Homebrews sind Spiele, die in Gänze von unabhängigen Entwicklern erstellt worden sind. Viele dieser Spiele sind unerwartete Überraschungen und werden Dir eine tolle Zeit bescheren!

Hacks sind modifizierte Original-Spiele. Meist beinhalten sie alle, oder auch nur Teile, der vorhandenen Grafiken, sowie das Gameplay, unterscheiden sich aber in der Story, dem "Feel", oder der Atmosphäre. Einige werden sogar als komplett neue Original-Spiele bezeichnet. Z.B. 'Super Mario Bros. 4' (Hack von Super Mario World), oder 'The Legend Of Zelda - Parallels Worlds' (Hack von 'A link to the past')

![](/presentation/zeldaparallelsworlds.png)

### Media Center

Unterstützt durch **Kodi**, ist Recalbox auch ein **Media Center**.  
Wenn du sie mit dem Heimnetzwerk verbindest, kannst du Videos von all deinen kompatiblen Geräten abspielen.

### Eigenschaften

* System:
  * [Unterstützte Systeme](./Hardware-Kompatibilität/Emulatoren-Kompatibilität)
  * Optimierte FBA-Version mit 4-Spieler-Unterstützung
  * **Kompiliert mit Buildroot:** Das bedeutet, dass das Basis-Dateisystem komprimiert nur 150MB groß ist.
  * NOOBS-basiertes Rescue-System:** Es besteht die Möglichkeit, das System direkt von der SD-Karte neu zu installieren oder die neueste Version aus dem Internet zu beziehen.
  * **Unterstützte Systemsprachen:** Französisch, Englisch, Spanisch, Deutsch, Italienisch, Portugiesisch und vielleicht noch weitere, wenn du mitmachst.
  * **Benutzeroberfläche:** Basierend auf der von Aloshi entwickelten EmulationStation.
  * Hintergrundmusik auf der Benutzeroberfläche.
  * **Favoritenverwaltung**.
* Netzwerk:
  * **Zugang über Netzwerk:** zu Roms, Screenshots und Spielständen.
  * **Wifi und RJ45 Unterstützung**
  * **Online-Updates**
* Controller:
  * **Konfiguriere deinen Game-Controller direkt über die Oberfläche:** Einmal konfiguriert, ist er mit allen Emulatoren kompatibel.
  * **Eingebaute Unterstützung für PS3, Xbox360, 8BitDo und Bluetooth-Controller:** Schließe einen Controller an und spiele!
  * ** GPIO-Treiber verfügbar:** Zur Verwendung von Arcade-Controllern oder deinen originalen Nes-, Snes-, Megadrive-, oder PSX-Controllern.
  * Unterstützung für Xin-Mo 2-Spieler-Controller.
  * **Unterstützung für virtuelle Gamepads** [Miroof's Virtual Gamepad](https://github.com/miroof/node-virtual-gamepads) : Verwenden Sie Ihr Telefon als Controller.
* Features:
  * **Kodi Media Center**
  * **Moonlight:** Streame Spiele über dein lokales Netzwerk oder das Internet (erfordert eine Nvidia-Grafikkarte).
  * **Netplay:** Spiele online.
  * **Retroarch AI:** Übersetzung von Text in Spielen während des Spielens (on the fly).
  * **Interne und externe Scraper** Füge Bilder, Videos und Beschreibungen für jedes Spiel hinzu.
  * **Vorschauvideos:** Vorschauvideos für Spiele via Scrapes

## Danksagung

Recalbox **würde** ohne die **Open-Source**-Emulatoren, die es verwendet, nicht existieren.  

Wir möchten **allen Leuten** danken, die dazu beigetragen haben, **Open-Source-Emulatoren** zur Verfügung zu stellen, dank derer wir die Chance haben, **alle diese klassischen Videospiele** wieder zu spielen. 🤩

### Hier ist die Liste der in Recalbox verwendeten Software

* Buildroot: [http://buildroot.uclibc.org/](http://buildroot.uclibc.org/)
* EmulationStation: [http://emulationstation.org/](http://emulationstation.org/)
* EmulationStation UI Design & Standard Theme “simple”: Nils Bonenberger - [http://blog.nilsbyte.de/](http://blog.nilsbyte.de/)

### Emulatoren

* Noyaux RetroArch et Libretro : [http://www.libretro.com/](http://www.libretro.com/)
* PiFBA : [https://code.google.com/p/pifba/](https://code.google.com/p/pifba/)
* Mupen64 : [https://github.com/mupen64plus/](https://github.com/mupen64plus/)
* Dolphin-emu : [https://fr.dolphin-emu.org/](https://de.dolphin-emu.org/)
* PPSSPP : [https://www.ppsspp.org/](https://www.ppsspp.org/)

### Weitere

* Kodi Media Player: [http://kodi.tv/](http://kodi.tv/)
* Qtsixa: [http://qtsixa.sourceforge.net/](http://qtsixa.sourceforge.net/)