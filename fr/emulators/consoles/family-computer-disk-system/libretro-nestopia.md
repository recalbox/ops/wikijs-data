---
title: Libretro Nestopia
description: 
published: true
date: 2024-07-24T12:45:13.319Z
tags: libretro, famicom, family computer disk system, nestopia
editor: markdown
dateCreated: 2021-05-21T08:20:41.750Z
---



## ![](/emulators/license.svg) Licence

Ce core est sous licence [**GPLv2**](https://github.com/libretro/nestopia/blob/master/COPYING).

## ![](/emulators/compatibility.png) Compatibilité

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Fonctionnalités

| Fonctionnalité | Supporté |
| :---: | :---: |
| Redémarrage | ✔ |
| Captures d'écran | ✔ |
| Sauvegardes | ✔ |
| Sauvegardes instantanées | ✔ |
| Rembobinage | ✔ |
| Netplay | ✔ |
| Options du core | ✔ |
| Cheats RetroArch | ✔ |
| Controls | ✔ |
| Remapping | ✔ |
| Softpatching | ✔ |

## ![](/emulators/bios.svg) BIOS

### Liste des bios obligatoires

| Nom de fichier | Description | MD5 | Fourni |
| :---: | :---: | :---: | :---: |
| disksys.rom | BIOS Family Computer Disk System | ca30b50f880eb660a320674ed365ef7a | ❌  |

### Emplacement

Placez les bios comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 fds
┃ ┃ ┃ ┃ ┣ 🗒 **disksys.rom**

## ![](/emulators/roms.png) Roms

### Extensions supportées

Les roms doivent avoir les extensions suivantes :

* .fds
* .zip
* .7z

Ce système supporte les roms compressées au format .zip/.7z. Attention toutefois, il ne s'agit que d'une archive.

Les fichiers contenus dans les .zip/.7z doivent correspondre aux extensions citées précédemment.
De plus, chaque fichier .zip/.7z ne doit contenir qu'une seule rom compressée.

### Emplacement

Placez les roms comme ceci : 

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 fds
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.zip**

>Les roms au format **No-Intro** sont vivement conseillées.
{.is-success}

>Pour plus d'information sur les roms, rendez-vous sur [ce tutoriel](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuration avancée de l'émulateur

>Pour pouvoir conserver vos configurations personnalisées lors d'une mise à jour, nous vous conseillons d'utiliser notre fonctionnalité [Surcharges de configuration](./../../../advanced-usage/configuration-override).
{.is-info}

### Accéder aux options

Vous pouvez configurer diverses options de deux façons différentes.

* Via le menu RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via le fichier `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Options du core

| Options | Valeur des options | Variable (pour surcharge) | Valeurs des variables |
| --- | --- | --- | --- |
| Région du système | `Auto` ✅ / `NTSC` / `PAL` / `Famicom` / `Dendy` | `nestopia_favored_system` | `auto` / `ntsc` / `pal` / `famicom` / `dendy` |
| Insertion automatique du FDS | `Désactivé` / `Activé` ✅ | `nestopia_fds_auto_insert` | `disabled` / `enabled` |
| Filtre NTSC de Blargg | `Désactivé` ✅ / `Vidéo composite` / `S-Video` / `Péritel RVB (SCART)` / `Monochrome` | `nestopia_blargg_ntsc_filter` | `disabled` / `composite` / `svideo` / `rgb` / `monochrome` |
| Palette | `CXA2025AS` ✅ / `Consommateur` / `Canonique` / `Alternative` / `RGB` / `PAL` / `FBx composite direct` / `FBx style PVM D93` / `FBx matériel NTSC` / `FBx NES Classic (corrigée)` / `Données brutes` / `Personnalisée` | `nestopia_palette` | `cxa2025as` / `consumer` / `composite` / `alternate` / `rgb` / `pal` / `composite-direct-fbx` / `pvm-style-d93-fbx` / `ntsc-hardware-fbx` / `nes-classic-fbx-fs` / `raw` / `custom` |
| Mask Overscan (Top Vertical) | `0` / `4` / `8` ✅ / `12` / `16` / `20` / `24` | `nestopia_overscan_v_top` | `0` / `4` / `8` / `12` / `16` / `20` / `24` |
| Mask Overscan (Bottom Vertical) | `0` / `4` / `8` ✅ / `12` / `16` / `20` / `24` | `nestopia_overscan_v_bottom` | `0` / `4` / `8` / `12` / `16` / `20` / `24` |
| Mask Overscan (Left Horizontal) | `0` ✅ / `4` / `8` / `12` / `16` / `20` / `24` | `nestopia_overscan_h_left` | `0` / `4` / `8` / `12` / `16` / `20` / `24` |
| Mask Overscan (Right Horizontal) | `0` ✅ / `4` / `8` / `12` / `16` / `20` / `24` | `nestopia_overscan_h_right` | `0` / `4` / `8` / `12` / `16` / `20` / `24` |
| Rapport d'aspect préféré | `Auto` ✅ / `NTSC` / `PAL` / `4:3` | `nestopia_aspect` | `auto` / `ntsc` / `pal` / `4:3` |
| Distorsion sonore du Game Genie | `Désactivé` ✅ / `Activé` | `nestopia_genie_distortion` | `disabled` / `enabled` |
| Volume du canal carré 1 (%) | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `nestopia_audio_vol_sq1` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
| Volume du canal carré 2 (%) | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `nestopia_audio_vol_sq2` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
| Volume du canal triangle (%) | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `nestopia_audio_vol_tri` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
| Volume du canal de bruit (%) | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `nestopia_audio_vol_noise` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
| Volume du canal DPCM (%) | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `nestopia_audio_vol_dpcm` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
| Volume du canal FDS (%) | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `nestopia_audio_vol_fds` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
| Volume du canal MMC5 (%) | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `nestopia_audio_vol_mmc5` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
| Volume du canal VRC6 (%) | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `nestopia_audio_vol_vrc6` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
| Volume du canal VRC7 (%) | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `nestopia_audio_vol_vrc7` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
| Volume du canal N163 (%) | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `nestopia_audio_vol_n163` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
| Volume du canal S5B (%) | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `nestopia_audio_vol_s5b` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
| Audio Output | `mono` / `stereo` ✅ | `nestopia_audio_type` | `mono` / `stereo` |
| Adaptateur 4 joueurs | `Auto` ✅ / `NTSC` / `Famicom` | `nestopia_select_adapter` | `auto` / `ntsc` / `famicom` |
| Touches décalées dans le sens horaire | `Désactivé` ✅ / `Activé` | `nestopia_button_shift` | `disabled` / `enabled` |
| Appareil Arkanoid | `Souris` ✅ / `Pointeur` | `nestopia_arkanoid_device` | `mouse` / `pointer` |
| Périphérique Zapper | `Pistolet` ✅ / `Souris` / `Pointeur` | `nestopia_zapper_device` | `lightgun` / `mouse` / `pointer` |
| Afficher le viseur | `Désactivé` / `Activé` ✅ | `nestopia_show_crosshair` | `disabled` / `enabled` |
| Vitesse d'impulsion du turbo | `2` ✅ / `3` / `4` / `5` / `6` / `7` / `8` / `9` | `nestopia_turbo_pulse` | `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` |
| Supprimer la limite de sprites | `Désactivé` ✅ / `Activé` | `nestopia_nospritelimit` | `disabled` / `enabled` |
| Vitesse du processeur (overclock) | `1x` ✅ / `2x` | `nestopia_overclock` | `1x` / `2x` |
| État de la RAM à la mise sous tension | `0x00` ✅ / `0xFF` / `Aléatoire` | `nestopia_ram_power_state` | `0x00` / `0xFF` / `random` |

## ![](/emulators/external-links.png) Liens externes

* **Code source utilisé** : [https://github.com/libretro/nestopia/](https://github.com/libretro/nestopia/)
* **Documentation Libretro** : [https://docs.libretro.com/library/nestopia\_ue/](https://docs.libretro.com/library/nestopia_ue/)