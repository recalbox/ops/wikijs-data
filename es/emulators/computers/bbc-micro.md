---
title: BBC Micro
description: 
published: true
date: 2024-07-26T12:46:24.916Z
tags: bbc micro, 8.0+, ordenadores
editor: markdown
dateCreated: 2023-09-02T18:41:52.789Z
---

![](/emulators/computers/bbcmicro.svg){.align-center}

## Datos técnicos

* **Fabricante**: Acorn Computers
* **Año de lanzamiento**: 1981
* **Sistema operativo**: Acorn MOS
* **Procesador**: MOS Technology 6502/6512 @ 2MHz
* **RAM**: 64KB - 128 KB
* **Unidad de disco**: Unidad de casete, unidad de disquete de 5 1/4" o 3 1/2", disco duro
* **Chip de sonido**: Texas Instruments SN76489, 4 canales, mono
* **Resolución**: 640x256, 8 colores (varios modos de framebuffer)

## Presentación

El BBC Microcomputer System, o BBC Micro, es una serie de microordenadores construidos por Acorn Computers Ltd para el BBC Computer Literacy Project iniciado por la British Broadcasting Corporation. Diseñados para uso educativo, los ordenadores de la gama BBC Micro son famosos por su modularidad y la calidad de sus sistemas operativos.

## Emuladores

[BeebEm](beebem)