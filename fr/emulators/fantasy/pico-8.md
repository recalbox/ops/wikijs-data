---
title: Pico-8
description: 
published: true
date: 2024-07-24T20:54:11.278Z
tags: fantasy, pico-8, 7.2+
editor: markdown
dateCreated: 2021-05-21T08:02:26.197Z
---

![](/emulators/fantasy/pico8.svg){.align-center}

## Fiche technique

* **Affichage :** 128x128 16 couleurs
* **Taille de cartouche :** 32k
* **Son :** 4 canaux chip blerps
* **Code :** Lua
* **Sprites :** 256 8x8 sprites
* **Map :** 128x32 cels

## Présentation

**Pico-8**  est une machine virtuelle, développée par **Lexaloffle Games**. Son but est d'offrir une fantasy console, sur laquelle il est possible de créer, partager et jouer de petits jeux vidéo et à d'autres programmes informatiques.

Cela ressemble à une console ordinaire, mais fonctionne sous Windows / Mac / Linux.

Le développement se réalise par un environnement en Lua, dans lequel l'utilisateur peut créer les différents éléments du jeu (cartes, sons, sprites, ...).

Pico-8 a été utilisé par des créateurs vétérans, mais aussi par des non-professionnels du jeu vidéo.

L'affichage graphique est limité à 16 couleurs, 128x128 pixels, ce qui donne facilement aux jeux ainsi créés un aspect minimaliste et rétro.

Lorsque vous l'allumez, la machine vous accueille avec une ligne de commande, une suite d'outils de création de cartouches et un navigateur de cartouches en ligne appelé SPLORE.

## Émulateurs

[Libretro Fake08](libretro-fake08)
[Libretro Retro8](libretro-retro8)