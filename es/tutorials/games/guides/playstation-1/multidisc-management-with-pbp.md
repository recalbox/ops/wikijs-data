---
title: Gestión de multi-discos con el formato .PBP
description: 
published: true
date: 2024-07-26T07:06:55.381Z
tags: ps1, pbp, gestion, multidiscos
editor: markdown
dateCreated: 2023-09-21T12:14:08.359Z
---

Es posible tener vuestros juegos multi-disco en un sólo fichero en formato `PBP`. Basta con crear un fichero eboot utilizando el software gratuito **psx2psp** que contendrá las imágenes iso, bin e img.

>¡Os recordamos que para ellos debéis disponer de vuestras propias imágenes de disco PSX!
{.is-info}

## Crear el archivo eboot

* Buscad la versión **psx2psp v1.42**, descárgala y ejecútala.
* Eligid el modo clásico, luego en la parte izquierda, cargad las ISOs una a una.
* Eligid el directorio de guardado.
* Pulsad en el botón "Convertir" y esperad.

Colocad vuestra carpeta o renombrad el fichero eboot creado dentro de la carpeta `/recalbox/share/roms/psx`.

## Cambiar los CDs

Para cambiar de CD durante el juego, activad 2-3 opciones en el menú RetroArch.

* Abrid el menú RetroArch durante el juego con `Hotkey + B`.
* Id a ajustes/ajustes generales y activad la opción de "configuration save on exit". 

>A partir de ese momento, cualquier cambio en las opciones quedará guardado al salir del menú RetroArch.
{.is-warning}

* Id a "settings > input settings" y buscad opciones con el nombre "disk eject toggle", "disk next", "disk previous". Tendréis que asignar estas opciones a las teclas de vuestro mando que estén libres (que no tengan ya asignada una función especial).
* Salid de RetroArch y volved al juego.

>Cuando estéis en la pantalla del juego y se os pida cambiar el CD, simplemente expulsad el CD virtualmente (pulsando hotkey + el atajo de teclado asignado a la opción de conmutación de expulsión de disco), cambiad el cd, y volved a cerrar virtualmente la unidad de cd.  
>A continuación, el juego arrancará automáticamente en el siguiente CD.
>
>Podéis [consultar este hilo](https://forum.recalbox.com/topic/482/roms-playstation-et-nombre-de-jeux) en el foro si tenéis algún problema.
{.is-warning}

## Para cambiar el CD en PCSX-reARMed (r22)

Método para los archivos .bin

* Entrad en el menú RetroArch con `Hotkey + B`.
* Elegid menú rápido, luego la opción core disk.
* Expulsad el cd virtual eligiendo "disk cycle tray status".
* Buscad el directorio donde se encuentra el cd que queréis cargar (lógicamente en `/recalbox/share/roms/psx`) con "disk image append".

## ¿Por qué no es recomendable convertir al formato .PBP?

Al convertir un juego al formato `.PBP`, el programa de conversión cambiará la alineación de las pistas en juegos multipista y/o multidiscos  (generalmente PSX2PSP). Si alguna vez queréis volver atrás, existe el riesgo de acabar con pistas mal truncadas o solapadas al querer invertir el proceso.

Cuando el programa intentará separar las pistas en `.BIN`/`.CUE`, cortará 2048 bytes de más. El resultado final es un poco de la pista 2 en la pista 1.

El gran problema ocurre con la pista 3: tenéis un trozo de la pista 3 en la pista 2, pero desplazado de ... 2x 2048 bytes, y así sucesivamente.

La calidad del dump sigue ahí, no es un problema de calidad, pero las pistas se cortarán inadecuadamente, y las cabeceras PBP no permiten recrear exactamente el `.CUE`, ni tampoco los marcadores de pista (LEAD IN/LEAD OUT).

Al final, es más rápido volver a volcar el disco original que intentar corregirl todo esto (automáticamente o a la mano).