---
title: Unibios
description: 
published: true
date: 2025-03-22T01:10:53.762Z
tags: neo-geo, unibios
editor: markdown
dateCreated: 2021-08-08T09:08:42.400Z
---

## What does Unibios allow

Among other things, UNIBIOS allows:

* Change the region (Europe/USA/Japan) and the mode (AES/MVS).
* Access the "soft dip" to modify the difficulty/life/time/etc. of the game.
* Use the cheats code database.
* Access to the jukebox... and much more !

## FinalBurn Neo

The latest UNIBIOS is provided in the updated FinalBurn Neo romset.

### Installation

* Start a game and go to the RetroArch menu with `Hotkey + B` and go to `Options`.

![](/tutorials/games/guides/neo-geo/unibios/unibios1-en.png){.full-width}

* In the options, scroll down to the `Neo-Geo mode` setting.

![](/tutorials/games/guides/neo-geo/unibios/unibios2-en.png){.full-width}

* On this option, choose `Use UNIBIOS bios`.

![](/tutorials/games/guides/neo-geo/unibios/unibios3-en.png){.full-width}

* And close the RetroArch menu (go back to the game). The game will restart with UNIBIOS enabled automatically.

![](/tutorials/games/guides/neo-geo/unibios/unibios4.png){.full-width}

### Usage

* Start any Neo-Geo game, you will see the new UNIVERSE BIOS boot screen.
* During this boot screen, press and hold the `Y + B + A` buttons (on the original controller, the buttons correspond to `C + A + B` respectively).

![Neo-Geo Controller](/tutorials/games/guides/neo-geo/unibios/neogeo-controller.jpg){.full-width}

![](/tutorials/games/guides/neo-geo/unibios/unibios5.png){.full-width}

* To exit, use the `X` button (the `C` button on the original controller).

* To access the in-game menu, you must press and hold the `X + A + B + START` buttons.

## Dip Switch

To access the dip switch menu during the UNIBIOS boot screen, press and hold `Y + X + A` (on the original controller, the buttons correspond to `C + D + B` respectively).

### Setting the Dip Switch

To display the blood in Metal Slug, once in the `dip switch` menu:

* Go to `SETTING UP THE SOFT DIP`.

![](/tutorials/games/guides/neo-geo/unibios/unibios6.png){.full-width}

* Go to `SLOT1 METAL SLUG`.

![](/tutorials/games/guides/neo-geo/unibios/unibios7.png){.full-width}

* Switch to `BLOOD` and change from `OFF` to `ON`.

![](/tutorials/games/guides/neo-geo/unibios/unibios8.png){.full-width}

To navigate through the menus, press `A` to confirm and press `C` to go back. Change the desired values with `A` and `B`.

## BIOS content

Neo-Geo requires a BIOS file named `neogeo.zip`.

* It will be placed with your ROMs in :

```text
/recalbox/share/roms/neogeo
```
or

```text
/recalbox/share/bios
```

* Here is the content of a certified working neogeo.zip BIOS file

```text
000-lo.lo
japan-j3.bin
neo-epo.bin
neo-po.bin
neodebug.bin
neopen.sp1
sfix.sfix
sm1.sm1
sp-1v1_3db8c.bin
sp-45.sp1
sp-e.sp1
sp-j2.sp1
sp-j3.sp1
sp-s.sp1
sp-s2.sp1
sp-s3.sp1
sp-u2.sp1
sp1-j3.bin
sp1-u2
sp1-u3.bin
sp1-u4.bin
sp1.jipan.1024
uni-bios_1_0.rom
uni-bios_1_1.rom
uni-bios_1_2.rom
uni-bios_1_2o.rom
uni-bios_1_3.rom
uni-bios_2_0.rom
uni-bios_2_1.rom
uni-bios_2_2.rom
uni-bios_2_3.rom
uni-bios_2_3o.rom
uni-bios_3_0.rom
uni-bios_3_1.rom
uni-bios_3_2.rom
uni-bios_3_3.rom
uni-bios_4_0.rom
vs-bios.rom
```