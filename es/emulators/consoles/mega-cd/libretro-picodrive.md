---
title: Libretro PicoDrive
description: 
published: true
date: 2024-07-03T15:34:27.188Z
tags: libretro, picodrive, mega cd
editor: markdown
dateCreated: 2023-09-06T13:24:14.374Z
---

**Libretro PicoDrive** es un emulador de código abierto de Sega **8**/**16** bits y **32X**. Ha sido diseñado para dispositivos portátiles basados en la arquitectura **ARM**.

## ![](/emulators/license.svg) Licencia

El core posee la siguiente licencia [**non-commerciale**](https://github.com/libretro/picodrive/blob/master/COPYING).

## ![](/emulators/compatibility.png) Compatibilidad

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Funcionalidades

| Funcionalidad | ¿Soportada? |
| :---: | :---: |
| Reinicio | ✔ |
| Grabaciones | ✔ |
| Grabaciones instantáneas | ✔ |
| Rebobinado | ✔ |
| Netplay | ✔ |
| Opciones del core | ✔ |
| RetroAchievements | ✔ |
| Cheats RetroArch | ✔ |
| Controles | ✔ |
| Remapping | ✔ |
| Control de disco | ✔ |

## ![](/emulators/bios.svg) BIOS

### Lista de bios opcionales

| Nombre del fichero | Descripción | MD5 | ¿Preinstalada? |
| :---: | :---: | :---: | :---: |
| bios_CD_E.bin | BIOS EU MegaCD (bootrom) | e66fa1dc5820d254611fdcdba0662372 | ❌ |
| bios_CD_U.bin | BIOS US SegaCD (bootrom) | 2efd74e3232ff260e371b99f84024f7f | ❌ |
| bios_CD_J.bin | BIOS JP MegaCD (bootrom) | 278a9397d192149e84e820ac621a8ed bdeb4c47da613946d422d97d98b21cda | ❌ |

### Emplazamiento

Poned las bios como se muestra en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 segacd
┃ ┃ ┃ ┃ ┣ 🗒 **bios_CD_E.bin**
┃ ┃ ┃ ┃ ┣ 🗒 **bios_CD_U.bin**
┃ ┃ ┃ ┃ ┣ 🗒 **bios_CD_J.bin**

## ![](/emulators/roms.png) Isos

### Extensiones de roms soportadas

Las roms deben tener las extensiones siguientes:

* .bin/.cue
* .chd
* .iso
* .m3u

### Emplazamiento

Disponed vuestras roms como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 segacd
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.zip**

>Se aconsejan las roms del proyecto **Redump**.
{.is-success}

>Para más información sobre las roms, consultad [este tutorial](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

>Para conservar las configuraciones personalizadas cuando se produce una actualización del sistema Recalbox, os aconsejamos utilizar las [Sobrecargas de configuración](./../../../advanced-usage/configuration-override).
{.is-info}

### Acceder a las opciones

Podéis configurar las opciones del core de dos formas diferentes.

* Via el Menú RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via el fichero `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Opciones del core

| Opción | Valor de la opción | Variable (para la sobrecarga) | Valores posibles |
| --- | --- | --- | --- |
| Region | `Auto` ✅ / `Japan NTSC` / `Japan PAL` / `US` / `Europe` | `picodrive_region` | `Auto` / `Japan NTSC` / `Japan PAL` / `US` / `Europe` |
| Master System Type | `Auto` ✅ / `Game Gear` / `Master System` | `picodrive_smstype` | `Auto` / `Game Gear` / `Master System` |
| Master System ROM Mapping | `Auto` ✅ / `Sega` / `Codemasters` / `Korea` / `Korea MSX` / `Korea X-in-1` / `Korea 4-Pak` / `Korea Janggun` / `Korea Nemesis` | `picodrive_smsmapper` | `Auto` / `Sega` / `Codemasters` / `Korea` / `Korea MSX` / `Korea X-in-1` / `Korea 4-Pak` / `Korea Janggun` / `Korea Nemesis` |
| Sega CD RAM Cart | `Désactivé` ✅ / `Activé` | `picodrive_ramcart` | `disabled` / `enabled` |
| Core-provided aspect ratio | `PAR` ✅ / `4:3` / `CRT` | `picodrive_aspect` | `PAR` / `4/3` / `CRT` |
| LCD Ghosting Filter | `Désactivé` ✅ / `Weak` / `Normal` | `picodrive_ggghost` | `off` / `weak` / `normal` |
| Renderer | `Accurate` ✅ / `Good` / `Fast` | `picodrive_renderer` | `accurate` / `good` / `fast` |
| Sample Rate (Hz) | `16000` / `22050` / `32000` / `44100` ✅ | `picodrive_sound_rate` | `16000` / `22050` / `32000` / `44100` |
| FM filtering | `Désactivé` ✅ / `Activé` | `picodrive_fm_filter` | `off` / `on` |
| Master System FM Sound Unit | `Désactivé` ✅ / `Activé` | `picodrive_smsfm` | `off` / `on` |
| Mega Drive FM DAC noise | `Désactivé` ✅ / `Activé` | `picodrive_dacnoise` | `off` / `on` |
| Audio filter | `Désactivé` ✅ / `Low-pass` | `picodrive_audio_filter` | `disabled` / `low-pass` |
| Low-pass filter % | `5` / `10` / `15` / `20` / `25` / `30` / `35` / `40` / `45` / `50` / `55` / `60` ✅ / `65` / `70` / `75` / `80` / `85` / `90` / `95` | `picodrive_lowpass_range` | `5` / `10` / `15` / `20` / `25` / `30` / `35` / `40` / `45` / `50` / `55` / `60` / `65` / `70` / `75` / `80` / `85` / `90` / `95` |
| Input Device 1 | `3 Button Pad` ✅ / `6 Button Pad` / `None` | `picodrive_input1` | `3 Button Pad` / `6 Button Pad` / `None` |
| Input Device 2 | `3 Button Pad` ✅ / `6 Button Pad` / `None` | `picodrive_input2` | `3 Button Pad` / `6 Button Pad` / `None` |
| Dynamic Recompilers | `Activé` ✅ / `Désactivé` | `picodrive_drc` | `disabled` / `enabled` |
| Frameskip | `Désactivé` ✅ / `Auto` / `Manual` | `picodrive_frameskip` | `disabled` / `auto` / `manual` |
| Frameskip Threshold (%) | `15` / `18` / `21` / `24` / `27` / `30` / `33` ✅ / `36` / `39` / `42` / `45` / `48` / `51` / `54` / `57` / `60` | `picodrive_frameskip_threshold` | `15` / `18` / `21` / `24` / `27` / `30` / `33` / `36` / `39` / `42` / `45` / `48` / `51` / `54` / `57` / `60` |
| No Sprite Limit | `Désactivé` ✅ / `Activé` | `picodrive_sprlim` | `disabled` / `enabled` |
| 68K Overclock | `Désactivé` ✅ / `+25%` / `+50%` / `+75%` / `+100%` / `+200%` / `+400%` | `picodrive_overclk68k` | `disabled` / `+25%` / `+50%` / `+75%` / `+100%` / `+200%` / `+400%` |

## ![](/emulators/external-links.png) Enlaces externos

* **Código fuente utilizado** : [https://github.com/libretro/picodrive/](https://github.com/libretro/picodrive/)
* **Documentación Libretro** : [https://docs.libretro.com/library/picodrive/](https://docs.libretro.com/library/picodrive/)