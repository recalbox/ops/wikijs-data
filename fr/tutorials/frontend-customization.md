---
title: 🛠️ Personnalisation de l'interface
description: (EmulationStation / RetroArch)
published: true
date: 2024-07-26T07:28:59.430Z
tags: frontend, tutoriel, personnalisation
editor: markdown
dateCreated: 2021-05-21T07:52:01.078Z
---

Ici, vous pourrez personnaliser l'interface comme avec les thèmes et autres musiques, par exemple.

Liste des tutoriels disponibles :

[Ajouter des thèmes à l'interface](add-themes-into-emulationstation)
[Ajouter une musique personnalisée à l'interface](add-custom-music-into-emulationstation)
[Ajuster l'image (Pixel Perfect)](pixel-perfect)