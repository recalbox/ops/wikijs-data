---
title: Arcade
description: Arcade-Systeme
published: true
date: 2024-06-02T20:54:46.613Z
tags: 
editor: markdown
dateCreated: 2021-06-30T16:12:55.089Z
---

![Arcade Logo](/emulators/arcade/arcade-logo.png)

![Arcade Bartop](/emulators/arcade/arcade-bartop.svg)

Ein Arcade-Spiel oder Coin-Game ist ein münzbetriebenes Unterhaltungsgerät. Bei den meisten Arcade-Spielen handelt es sich um Videospiele, Flipperautomaten, elektromechanische Spiele, Gewinnspiele oder Merchandise.

Während die genauen Daten umstritten sind, wird das goldene Zeitalter der Arcade-Videospiele allgemein als ein Zeitraum definiert, der in den späten 1970er Jahren begann und Mitte der 1980er Jahre endete.

Abgesehen von einem kurzen Wiederaufleben in den frühen 1990er Jahren ging die Arcade-Industrie dann in der westlichen Hemisphäre zurück, als die Zahl der Videospielkonsolen, die in Bezug auf Grafik und Spielfunktionen konkurrierten, stieg und die Kosten sanken.