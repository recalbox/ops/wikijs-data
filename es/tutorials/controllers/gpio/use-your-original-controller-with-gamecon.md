---
title: Jugad con vuestro mando original utilizando Gamecon
description: 
published: true
date: 2024-11-17T16:26:01.435Z
tags: original, gamecon, mando
editor: markdown
dateCreated: 2023-09-18T20:06:58.792Z
---

## Introducción

El driver está diseñado para ser usado con controladores de juegos retro conectados a los GPIOs de Raspberry Pi.  
Se pueden utilizar hasta 4 controladores al mismo tiempo si son del siguiente tipo:

* Mando NES
* Mando SNES
* Mando PSX / PS2
* Mando N64
* Mando de Gamecube

>El driver se basa en el driver de Gamecon, pero utiliza diferentes parámetros y pinouts. ¡Por lo tanto, no hay garantía de que funcione, nosotros no os garantizamos nada!
{.is-warning}

 >Ce tutoriel ne convient pas au Raspberry Pi 5.
 {.is-warning}
 

## Conexión de los joysticks

Los PINS P1-01 y P1-06 se utilizan para la alimentación de 3,3V y masa. Son comunes a todos los mandos conectados a GPIO. Aunque los mandos SNES / NES funcionan nominalmente a 5V, deberían funcionar a 3.3V. Por lo tanto NO hay necesidad de utilizar un convertidor en los PINS de datos.  
La corriente máxima (50mA) de la Raspberry debería ser suficiente para alimentar 4 joysticks en GPIOs.
Sin embargo, debes utilizar una fuente de alimentación Raspberry de alto rendimiento (tipo 2.1A) para evitar cualquier caída de tensión no deseada.

Los PINS de datos P1-03, 05, 07, 26 (GPIO0, GPIO1, GPIO4 y GPIO7) son independientes entre sí y cada joystick utiliza un único PIN.

>* PINS P1-03 & P1-05 corresponden a GPIO2 & GPIO3 en placas rev.2. Esto debe tenerse en cuenta al cargar el módulo como se explica en la sección 3. 
>* PINS P1-19 & P1-23 (GPIO10 & 11) son los PINS de señal comunes a todos los controladores NES / SNES.
>* PINS P1-08, P1-10 & P1-12 (GPIO14, 15 & 18) son los PINS de señal comunes a todos los controladores PSX / PS2.

### Mandos NES / SNES

| PIN RASPBERRY | PIN mando SNES | PIN mando NES |
| :--- | :--- | :--- |
| P1-01 (3.3V) | 1 (power - 5V) | 1 (power - 5V) |
| GPIO10 | 2 (clock) | 5 (clock) |
| GPIO11 | 3 (latch) | 6 (latch) |
| GPIOXX | 4 (data) | 7 (data) |
| P1-06 (GND) | 7 (ground) | 4 (ground) |

 #### PINOUT NES y SNES

![](/tutorials/controllers/gpio/use-original-controller/pins-nes-snes.png)

Los GPIOXX son los PIN de datos independientes. Consultad la [sección de configuración del driver](./use-your-original-controller-with-gamecon#configurer-le-pilote) para poder seleccionar el buen GPIO.

### Mandos N64

![](/tutorials/controllers/gpio/use-original-controller/pins-n64.gif)

### Mandos GameCube

| PIN RASPBERRY | PIN mando GAMECUBE |
| :--- | :--- |
| P1-01 (3.3V) | 6 (power/3.43V) |
| GPIOXX | 3 (data) |
| P1-06 (GND) | 2&5 (gnd) |

Los GPIOXX son los PIN de datos independientes. Consultad la [sección de configuración del driver](./use-your-original-controller-with-gamecon#configurer-le-pilote) para poder seleccionar el buen GPIO.

![](/tutorials/controllers/gpio/use-original-controller/pins-ngc.png)

### Manettes PSX / PS2

| PIN RASPBERRY | PIN mando PSX / PS2 |
| :--- | :--- |
| P1-01 (3.3V) | 5 (VCC) |
| GPIO14 | 2 (Command) |
| GPIO15 | 6 (ATT) |
| GPIO18 | 7 (CLK) |
| GPIOXX | 1 (Data) |
| P1-06 (GND) | 4 (ground) |

Los GPIOXX son los PIN de datos independientes. Consultad la [sección de configuración del driver](./use-your-original-controller-with-gamecon#configurer-le-pilote) para poder seleccionar el buen GPIO.

![](/tutorials/controllers/gpio/use-original-controller/pinout.gif)

## Configurar el driver

### Configurar los mandos

* Activad el driver Gamecon en el fichero [recalbox.conf](./../../../basic-usage/getting-started/recalboxconf-file) en trouvant le paramètre `controllers.gamecon.enabled=` y ponedlo a 1 : `controllers.gamecon.enabled=1`.
* Utilizad el índice de los mandos que se muestra a continuación para configurar el parámetro `controllers.gamecon.args` : `map=<pad1/GPIO0>,<pad2/GPIO1>,<pad3/GPIO4>,<pad4/GPIO7>,<pad5/GPIO2>,<pad6/GPIO3>`

Donde <pad...> es el número que define el tipo de mando:

* 0 = no conectado
* 1 = mando SNES
* 2 = mando NES
* 3 = mando Gamecube
* 6 = mando N64
* 7 = mando PSX/PS2
* 8 = mando PSX DDR
* 9 = ratón SNES

Por ejemplo, si un mando SNES está conectado al GPIO4 y un mando N64 al GPIO7, el mapping será `map=0,0,1,6`.

>* pad1 & pad2 sólo se utilizan con una tarjeta rev.1.
>* pad5 & pad6 sólo se utilizan con una tarjeta rev.2.
>* Los controladores PSX/PS2 requieren una resistencia de pull-up. pad5 (GPIO2) y pad6 (GPIO3) tienen esta resistencia incorporada.

Si tenéis una tarjeta rev.2, pad1 & pad2 deben estar a 0.

El índice final del joystick (utilizado por los programas) se asigna secuencialmente para los joysticks conectados, empezando por 0. Así, en el ejemplo anterior, el mando de SNES tendría un índice igual a 0 y el mando de N64 un índice igual a 1.

* Usad el siguiente comando para verificar que el módulo se ha cargado correctamente:

```shell
tail /var/log/kern.log
```

### Calibrar el eje analógico de un mando N64

Para cada mando N64, lanzad el siguiente comando:

```shell
jscal -s 4,1,0,0,0,6972137,6972137,1,0,0,0,6547006,6468127,1,0,0,0,536854528,536854528,1,0,0,0,536854528,536854528 /dev/input/jsX
```

### Configuración del tiempo de respuesta para los mandos PSX

El tiempo de respuesta para los mandos PSX es de 10µs, lo que minimiza el retardo inducido por el controlador. Sin embargo, en algunos casos poco frecuentes, esto no es suficiente para garantizar una entrada estable. Si tiene problemas al utilizar controladores PSX, aumente el retardo siguiendo el parámetro "extra modbprobe": "psx_delay=" donde "delay" es el valor en µs entre 1 y 50.

## Para ir mas lejos

[Driver de gamepad multi-consola para GPIO](https://www.raspberrypi.org/forums/viewtopic.php?f=78&t=15787) (inglés).

Gracias a **marqs** por el driver y por permitirnos usar su "README" en la documentación de Recalbox.