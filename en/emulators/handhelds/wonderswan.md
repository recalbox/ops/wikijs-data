---
title: Wonderswan
description: 
published: true
date: 2021-08-09T13:13:40.488Z
tags: bandai, wonderswan, handlhelds
editor: markdown
dateCreated: 2021-07-06T09:38:59.007Z
---

![](/emulators/handheld/wonderswan.svg){.align-center}

## Technical data

* **Manufacturer**: Bandai
* **Year of release**: 1999
* **Units sold**: 3.5 millions
* **CPU**: 16Bits NEC V20
* **RAM**: 512k video
* **Resolution**: 224 x 144 pixels
* **Sound**: 4 PCM channels, 32 samples 4Bit for each channel

## Presentation

The **WonderSwan** was released in March 1999 in Japan, where it met a certain success. It is a monochrome console which sells well despite the release of the Game Boy Color one year earlier, thanks to its small size. Thus, it has a great autonomy, while being powerful.

The inventor of this console is Gunpei Yokoi, the creator of the Game Boy.

The WonderSwan had two evolutions: the WonderSwan Color which brings a color screen and the SwanCrystal which is a variation of the WonderSwan Color with a better screen.

These three models sold 2,061,193 copies in Japan, their only market.

## Emulators

[Libretro Mednafen_WSwan](libretro-mednafen-wswan)