---
title: Mupen64Plus RICE_GLES2
description: 
published: true
date: 2024-06-22T23:59:01.172Z
tags: n64, mupen64plus, nintendo 64, rice gles2
editor: markdown
dateCreated: 2021-05-21T08:23:48.956Z
---

## ![](/emulators/license.svg) Licence

Ce core est sous licence **MIT**.

## ![](/emulators/compatibility.png) Compatibilité

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ❌ | ✅ | ❌ | ❌ | ❌ | ❌ | ❌ | ❌ |

## ![](/emulators/features.png) Fonctionnalités



## ![](/emulators/bios.svg) BIOS

>**Aucun bios n'est requis.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Extensions supportées

Les roms doivent avoir les extensions suivantes :

* .n64
* .v64
* .z64

### Emplacement

Placez les roms comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 n64
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.n64**

>Les roms au format **No-Intro** sont vivement conseillées.
{.is-success}

>Pour plus d'information sur les roms, rendez-vous sur [ce tutoriel](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuration avancée de l'émulateur

>Pour pouvoir conserver vos configurations personnalisées lors d'une mise à jour, nous vous conseillons d'utiliser notre fonctionnalité [Surcharges de configuration](./../../../advanced-usage/configuration-override).
{.is-info}

## ![](/emulators/external-links.png) Liens externes

* **Code source utilisé** : [https://github.com/ricrpi/mupen64plus-video-gles2rice/](https://github.com/ricrpi/mupen64plus-video-gles2rice/)
* **Documentation** : [http://www.mupen64plus.org/](http://www.mupen64plus.org/)