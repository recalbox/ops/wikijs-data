---
title: Licenças para projetos associados
description: 
published: true
date: 2021-08-11T00:03:53.263Z
tags: 
editor: markdown
dateCreated: 2021-08-11T00:03:53.263Z
---

## Projetos de software

O Recalbox não é composto apenas por seu diretório principal. Também mantemos alguns outros projetos de código aberto incluídos no Recalbox ou como parte da infraestrutura geral do Recalbox.

Aqui está uma visão geral do licenciamento dos principais projetos:

* O [fork do EmulationStation do Recalbox](https://gitlab.com/recalbox/recalbox-emulationstation) é licenciado sob MIT
* O [gerenciador web](https://gitlab.com/recalbox/recalbox-manager) do Recalbox está sob a formalidade de "todos os direitos reservados".
* A API Recalbox é licenciada sob os termos da Licença MIT
* O [site](https://gitlab.com/recalbox/www.recalbox.com) do Recalbox está sob a formalidade de "todos os direitos reservados".

## Obras criativas

Também mantemos e distribuímos [temas](https://gitlab.com/recalbox/recalbox-themes) para Recalbox, que são lançados sob as seguintes licenças:

| Tema | Licença |
| :---: | :---: |
| recalbox-goa2 | [CreativeCommons BY-NC-ND 4.0](https://creativecommons.org/licenses/by-nc-nd/4.0/) |
| recalbox-next | [CreativeCommons BY-NC-ND 4.0](https://creativecommons.org/licenses/by-nc-nd/4.0/) |