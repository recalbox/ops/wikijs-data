---
title: Gestion des multi-disques au format .M3U
description: 
published: true
date: 2022-12-27T10:57:13.394Z
tags: ps1, m3u, gestion, multi-disques
editor: markdown
dateCreated: 2021-07-13T16:40:57.223Z
---

Le contenu a été transféré à la [page suivante](./../../generalities/multidisc-management-with-m3u).