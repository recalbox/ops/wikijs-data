---
title: Odroid
description: 
published: true
date: 2022-02-27T17:26:28.041Z
tags: odroid, compatibilidade
editor: markdown
dateCreated: 2022-02-27T17:26:28.041Z
---

## Odroid XU4

![](/compatibility/xu4-exynos-8-coeurs.jpg)

O ODROID-XU4 é uma nova geração de dispositivos de computação com hardware mais poderoso e mais eficiente em termos de energia e um formato menor.
Implementando as interfaces eMMC 5.0, USB 3.0 e Gigabit Ethernet, o ODROID-XU4 oferece velocidades de transferência de dados incríveis, uma função cada vez mais necessária para suportar o poder de processamento avançado dos dispositivos ARM.
Isso permite que os usuários experimentem verdadeiramente um computador melhorado, especialmente com inicialização mais rápida, navegação na web, rede e jogos 3D.

### Recursos:

* **CPU**:
  * Exynos 5422 (ARM Cortex-A15 Quad-Core @ 2,0 GHz / ARM Cortex-A7 Quad-Core @ 1,4 GHz)
* **GPU**:
  * Mali™ -T628 MP6 3D Accelerator (OpenGL ES 3.1 / 3.0 / 2.0 / 1.1 e OpenCL 1.2 Full profile)
* **RAM**:
  * 2Go LPDDR3 RAM PoP (750Mhz, largura de banda de memória 12GB/s, barramento 2x32 bits)
* **Conectores**:
  * 1 X RJ45 Fêmea
  * 1 X USB 2.0
  * 2 X USB 3.0
  * 1 X HDMI
  * Slot para cartão MicroSD (até 128 Gb)
* **Conectores adicionais**:
  * Armazenamento (opcional) plugue do módulo eMMC: armazenamento flash eMMC 5.0 (até 64 GB)
  * Interface HDD / SSD SATA (opcional) Adaptador USB SuperSpeed (USB 3.0) para Serial ATA3 para HDD e SSD de 2,5 "/ 3,5" para armazenamento em Rede
  * Placa de som USB (Opcional)
  * WiFi (Opcional): USB IEEE 802.11 ac / b / g / n 1T1R WLAN com antena (adaptador USB externo)
* **Rede**:
  * Ethernet LAN 10/100 / 1000Mbps Fast Ethernet com soquete RJ-45 (suporte Auto-MDIX)
* **Fonte de energia**:
  * 4,8 Volt ~ 5,2 Volt (5V / 4A recomendado)
* **Dimensão**:
  * 83 x 58 x 20 mm (peso: 38 gr) aprox., sem refrigeração

## Odroid XU4Q

![](/compatibility/xu4q.jpg)

Se você deseja construir um media center, um console de jogos, um sistema de vigilância integrado ou qualquer outra coisa, a placa-mãe ultracompacta **Odroid XU4Q** será a escolha ideal. Incorporando um processador Samsung Exynos 5422 Octo-Core 2.0 GHz e 2 GB de RAM, é uma solução muito acessível.
Quanto ao dissipador de calor já montado na placa, ele irá girar apenas quando a carga da CPU estiver alta e a temperatura atingir um limite mínimo.

### Recursos:

* **CPU**:
  * Samsung Exynos 5422 (ARM Cortex-A15 Quad-Core com clock de 2,0 GHz / ARM Cortex-A7 Quad-Core com clock de 1,4 GHz)
* **Número de núcleos**:
  * 8
* **GPU**:
  * Mali T628 MP6 OpenGL ES 3.1 / 3.0 / 2.0 / 1.1 e OpenCL 1.2
* **RAM**:
  * 2 Gb LPDDR3
* **Conectores**:
  * 1 X RJ45 Fêmea
  * 1 X USB 2.0
  * 2 X USB 3.0
  * 1 X HDMI Fêmea
  * cartão microSD, micro SDHC, micro SDXC (compatível)
* **Conectores adicionais**:
  * 1 X GPIO 12 pinos (header)
  * 1 X GPIO 30 pinos (header)
* **Rede**:
  * Ethernet LAN 10/100/1000 Mbps
* **Fonte de energia**:
  * 5V / 4A (_transformador não fornecido_)
* **Dimensões**:
  * 83 x 58 x 20 milímetros
* **Peso**:
  * 38g

## Odroid Go Advance 2

![](/compatibility/odroid_go_advance.png)

Um console de videogame portátil D.I.Y. (Faça-Você-Mesmo) dedicado à emulação e retrogaming, inspirado no visual do Gameboy Advance e disponível em 2 cores.
Concebido como um kit de montagem completo e sem emendas, oferecendo uma montagem divertida e lúdica, facilmente acessível a todos. Compatível com sistemas de jogos recentes (Playstation, PSP, ..) e outros emuladores como no Recalbox.

Alto-falantes e conector de 3,5 mm, controle de brilho, modo de suspensão do console, modo "baixo consumo" para mais de 7 horas de autonomia...

* **CPU**:
  * RockChip RK3326 (Quad-Core ARM Cortex A35 1,3 GHz)
* **GPU**:
  * Mali-G31 Dvalin
* **Memória**:
  * 1 Gb (DDR3L 786 Mhz, largura de barramento de 32 bits)
* **Tela**:
  * LCD TFT 3,5 polegadas 320 x 480 (ILI9488, interface MIPI)
* **Bateria**:
  * Li-Polymer 3.7V / 3000mAh, até 9 horas de reprodução
* **Entradas**:
  * F1, F2, F3, F4, F5, F6, A, B, X, Y, D-Pad, botão L1, botão R1, botão L2, botão R2, joystick analógico
* **Conectores**:
  * 2.0 Hôte USB 2.0 x 1
  * porta 10 pinos (I2C, GPIO, IRQ a 3,3 volts)
* **Fone de ouvido**:
  * Jack, 0,5 Watt 8&#x3A9; Mono
* **Armazenamento**:
  * Flash SPI (boot 16 Mb), slot para cartão Micro SD (compatível com UHS-1)
* **Fonte de energia**:
  * Entrada 5V, conector de alimentação USB-C
* **Wi-fi**:
  * 2.4Ghz 802.11b/g/n interno (ESP-WROOM-S2)
* **Consumo**:
  * Emulação de jogos: 300 ~ 550mA (depende do brilho da luz de fundo e do tipo de emulações de jogos e uso sem fio)
  * Modo de desligamento: < 1mA
  * Se você desabilitar o WiFi, o consumo de energia será o mesmo da revisão anterior 1.0.
* **O tempo de carga**:
  * 2,5 ~ 3 horas quando desligado.
  * Em jogo, 4 a 5 horas.
* **Dimensões**:
  * 155x72x20 mm (6,1 x 2,8 x 0,8 polegadas)
* **Peso**:
  * 349g

## Odroid Go Super

![](/compatibilidade/odroid-go-super.png)

Com um formato próximo ao do Switch Lite, o novo Odroid Go Super tenta conquistar o coração dos fãs de retrogaming! Com muitos dos mesmos elementos que seu antecessor, o Odroid Go Advance, esta versão marca uma importante evolução na linha de consoles portáteis do HardKernel.

* **CPU**:
  * RockChip RK3326 (Quad-Core ARM 64bit Cortex-A35 1,3 GHz)
* **GPU**:
  * Mali-G31 MP2 OS Ubuntu 18.04. 19.10 e 20.04 no Kernel 4.4 Aarch64
* **Memória**:
  * 1 Gb (DDR3L 786 Mhz, largura de barramento de 32 bits)
* **Armazenamento**:
  * Flash SPI (boot 16 Mb), slot para cartão Micro SD (compatível com UHS-1)
* **Tela**:
  * LCD TFT de 5 polegadas 854 × 480 (tela grande angular, interface MIPI-DSI)
* **Áudio**:
  * Fone de ouvido, alto-falantes mono 0,5 W 8Ω
* **Bateria**:
  * Li-Polymer 3,7 V / 4000 mAh (76,5 × 54,5 × 7,5 mm (L * W * T)), até 10 horas de jogo
* **Fonte de energia**:
  * Entrada DC 5 V, USB-C.
* **Corrente máxima de tração**:
  * 1,5A
* **Entradas/saídas**:
  * Host USB 2.0 x 1, porta 10 pinos (I2C, GPIO, IRQ a 3,3 volts)
* **Entradas**:
  * F1, F2, F3, F4, F5, F6, A, B, X, Y, D-Pad, botão L1, botão R1, botão L2, botão R2, joystick analógico, joystick analógico 2
* **Wireless**:
  * Adaptador Wi-Fi USB opcional
* **Consumo**:
  * Emulação de jogos: 350 ~ 600mA (depende do brilho da luz de fundo e do tipo de emulações de jogos e uso sem fio)
* **Modo desligado**:
  * <1mA
* **O tempo de carga**:
  * 3,4 ~ 4 horas quando desligado. Em jogo 4,5 a 5,5 horas.
* **Dimensões**:
  * 204x86x25mm
* **Peso**:
  * 280g