---
title: Libretro Geolith
description: 
published: true
date: 2024-07-24T13:57:56.783Z
tags: libretro, neo-geo, 9.2+, geolith
editor: markdown
dateCreated: 2024-07-24T13:57:56.783Z
---


## ![](/emulators/license.svg) License

This core is under [**BSD-3**](https://github.com/libretro/geolith-libretro/blob/master/LICENSE) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Features

| Feature | Supported |
| :---: | :---: |

## ![](/emulators/bios.svg) BIOS

### List of optional bios

| Filename | Description | MD5 | Provided |
| :---: | :---: | :---: | :---: |
| aes.zip | Neo-Geo AES BIOS | ad9585c72130c56f04ae26aae87c289d 7a4dcd56b3924215aac7f903ff907339 | ❌ |
| neogeo.zip | Neo-Geo BIOS | 00dad01abdbf8ea9e79ad2fe11bdb182 912c0a56bafe0fba39d0c668b139b250 | ❌ |

### Location

Put the bios like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 geolith
┃ ┃ ┃ ┃ ┣ 🗒 **aes.zip**
┃ ┃ ┃ ┃ ┣ 🗒 **neogeo.zip**

## ![](/emulators/roms.png) Roms

### Supported extensions

Roms must have the extension:

* .neo

### Location

Put the roms like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 neogeo
┃ ┃ ┃ ┃ ┣ 🗒 **game.neo**

>For more information about roms, go to [this tutorial](./../../../tutorials/games/generalities/isos-and-roms)!
{.is-info}

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Accessing to options

You can configure various options in two different ways.

* Via RetroArch menu:

┣ 📁 RetroArch Menu
┃ ┣ 📁 Core options
┃ ┃ ┣ 🧩 Name_option

* Via the file `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core options

| Options | Options values | Variables (for configuration override) | Variables values |
| --- | --- | --- | --- |
| System Type (Restart) | `Neo Geo AES (Home Console)` ✅ / `Neo Geo MVS (Arcade)` / `Universe BIOS (Community-enhanced BIOS)` | `geolith_system_type` | `aes` / `mvs` / `uni` |
| Region (Restart) | `USA` ✅ / `Japan` / `Asia` / `Europe` | `geolith_region` | `us` / `jp` / `as` / `eu` |
| Setting Mode (Restart, DIP Switch) | `Disabled` ✅ / `Enabled` | `geolith_settingmode` | `off` / `on` |
| Four Player Mode (Restart, Asia/Japan MVS Only) | `Disabled` ✅ / `Enabled` | `geolith_4player` | `off` / `on` |
| Freeplay (DIP Switch) | `Disabled` ✅ / `Enabled` | `geolith_freeplay` | `off` / `on` |
| Mask Overscan (Top) | `16` / `12` / `8` ✅ / `4` / `0` | `geolith_overscan_t` | `16` / `12` / `8` / `4` / `0` |
| Mask Overscan (Bottom) | `16` / `12` / `8` ✅ / `4` / `0` | `geolith_overscan_b` | `16` / `12` / `8` / `4` / `0` |
| Mask Overscan (Left) | `16` / `12` / `8` ✅ / `4` / `0` | `geolith_overscan_l` | `16` / `12` / `8` / `4` / `0` |
| Mask Overscan (Right) | `16` / `12` / `8` ✅ / `4` / `0` | `geolith_overscan_r` | `16` / `12` / `8` / `4` / `0` |
| Aspect Ratio | `Perfectly Square Pixels (1:1 PAR)` ✅ / `Ostensibly Accurate NTSC Aspect Ratio (45:44 PAR)` / `Very Traditional NTSC Aspect Ratio (4:3 PAR)` | `geolith_aspect` | `1:1` / `45:44` / `4:3` |
| Sprites-per-line limit (Hack) | `Hardware Accurate (96)` ✅ / `Double (192)` / `Triple (288)` / `MAX 381 MEGA PRO-GEAR SPEC` | `geolith_sprlimit` | `96` / `192` / `288` / `381` |
| Overclocking (Hack) | `Disabled` ✅ / `Enabled` | `geolith_oc` | `off` / `on` |

## ![](/emulators/external-links.png) External links

* **Source code used:** [https://github.com/libretro/geolith-libretro/](https://github.com/libretro/geolith-libretro)
* **Documentation Libretro** :