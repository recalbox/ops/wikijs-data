---
title: No menu
description: Quando você está no menu, alguns comandos especiais estão disponíveis.
published: true
date: 2021-07-15T23:41:01.238Z
tags: 
editor: markdown
dateCreated: 2021-07-15T23:41:01.238Z
---

## Função de pesquisa

A partir do menu de sistemas ative a **função de pesquisa**.

* **`R1`**


## Menu de desligamento

No menu de sistemas, você pode fazer aparecer o **menu de desligamento** do Recalbox.

* **`Select`**