---
title: Neo-Geo CD
description: 
published: true
date: 2024-07-26T07:55:43.381Z
tags: snk, neo-geo cd, consolas
editor: markdown
dateCreated: 2023-09-03T19:56:39.825Z
---

![](/emulators/consoles/neogeocd.svg){.align-center}

## Detalles técnicos

* **Fabricante**: SNK Corporation
* **Año de lanzamiento**: 1990
* **Cantidades vendidas**: 570000
* **Procesador**: 16-bit Motorola 68000 @ 12MHz, 8-bit Zilog Z80 @ 4MH
* **RAM**: 7 MB
* **SRAM**: 2 KB
* **RAM de vídeo**: 512 KB
* **Chip de sonido**: Yamaha YM2610 con 13 canales
* **Resolución** : 304x224, 4096 colores sobre 65536

## Presentación

La **Neo-Geo CD** es una consola de videojuegos lanzada el 9 de septiembre de 1994 en Japón por SNK. Se basa en la arquitectura de la Neo-Geo AES y la Neo-Geo MVS, pero utiliza CD-ROM en lugar de cartuchos, con el fin de reducir el precio de venta de los juegos.

Existen tres versiones del Neo-Geo CD:

* El modelo original, con cargador de CD frontal, salió a la venta a finales de 1994 y se distribuye exclusivamente en Japón,
* el modelo con cargador de CD en la parte superior, que sustituye al primero y se distribuye en Europa y Estados Unidos,
* el Neo-Geo CDZ, lanzado el 29 de diciembre de 1995, con un lector de CD de doble velocidad.

La unidad de CD-ROM de la Neo-Geo CD es a la vez su mejor característica (el precio de los juegos es muy inferior al de sus homólogos en formato cartucho) y su principal inconveniente. En efecto, su velocidad de transferencia de 150 kio/s (velocidad única), combinada con la pequeña cantidad de RAM disponible, hace que se tarde mucho en cargar los juegos. Aunque los títulos más antiguos sólo sufrían moderadamente por ello, el aumento del tamaño de las producciones de SNK obligó rápidamente a los jugadores a esperar varios segundos antes y durante cada partida.

Es más, con la D-RAM limitada a 56 MiB, algunos de los últimos juegos han sido modificados. En la mayoría de los casos, se trataba de fases de animación o efectos reducidos, como en The Last Blade y The Last Blade 2. Sólo Art of Fighting 3: The Path of the Warrior sufrió modificaciones significativas, con sprites más pequeños que en la versión MVS/AES.

## Emuladores

[Libretro FBNeo](libretro-fbneo)
[Libretro NeoCD](libretro-neocd)

