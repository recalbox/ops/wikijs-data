---
title: VIC-20
description: 
published: true
date: 2021-08-09T15:05:37.080Z
tags: computers, commodore, vic-20
editor: markdown
dateCreated: 2021-07-28T21:40:17.004Z
---

![](/emulators/computers/vic20.svg){.align-center}

## Technical data

* **Manufacturer**: Commodore
* **Year of release**: 1980
* **OS**: Commodore KERNAL / Commodore BASIC 2.0
* **CPU**: MOS Technology 6502 @ 1.02 MHz (NTSC) / 1.10 MHz (PAL)
* **RAM**: 5 KB (expandable to 32 KB)
* **ROM**: 20KB
* **GPU**: MOS Technology VIC-I 6561
* **Sound chip**: 3 x square, 1 x nose, mono
* **Resolution**: 128x128 (16 colors)

## Presentation

The **VIC-20** is an 8-bit personal computer that was built by Commodore International with 5 kB of RAM (including 3.5 kB for BASIC applications) and a CPU based on a MOS 6502 processor. It resembled its successors the C64 and the C16. The VIC-20 appeared in June 1980, 2 years and 9 months after the release of Commodore's first personal computer, the PET.

The VIC-20 was to be positioned as a consumer computer, unlike the PET. It is reported that the VIC-20's video chip, named VIC created by MOS Technology, was originally designed for entry-level graphics terminals and game consoles, but Commodore could not sell it properly. At the same time, Commodore had a surplus of 4x1K SRAM memory. In April 1980, Jack Tramiel, the president of Commodore, asked that a computer be made that would sell for less than $300. The surplus gave birth to the VIC-20. While the PET was sold only through authorized dealers, the VIC-20 was sold at retail from the beginning, especially in discount and toy stores where it competed directly with game consoles. Commodore put up ads in which Star Trek actor William Shatner asked, "Why settle for just a video game?"

Although magazines criticized the VIC-20 for being undersized, the strategy worked: it became the first computer to surpass the one million unit sales mark and became the world's best-selling computer in 1982. At its peak, 9,000 units were produced every day, and 2,500,000 units were sold until its production stopped in January 1985, when Commodore repositioned the C64 as its entry-level computer in preparation for the upcoming release of the Commodore 128 and the Amiga (the latter introducing Commodore to the 16/32-bit world).

Due to its low memory and low resolution display, compared to other computers of the time, the VIC-20 was mainly used for educational or entertainment purposes. However, utility programs such as family budgeting, spreadsheet and communication terminals were also developed for this machine. Its ease of use by the general public allowed many future developers to start working on the VIC-20 by learning BASIC or even assembly language (or machine language). Several magazines, such as Compute !4, published source codes of programs for the VIC-20, one of them having been proposed by Commodore itself. Thus, many users learned to program by typing, studying, running and modifying these programs. This is the case of Linus Torvalds, for example.

The ease of programming and the availability of a low-cost modem made it possible to offer the VIC-20 a large library of public domain and freeware software, although it was much smaller than the C64 library. The software was distributed on online services such as CompuServe, BBS (Bulletin Board System) and between user groups.

As for the commercial catalog, it is estimated that 300 titles were available on electronic cartridges, and more than 500 on cassette. In comparison, the Atari 2600, the most popular game console at the time, offered about 900 titles.

## Emulators

[Libretro vice_xvic](libretro-vice_xvic)