---
title: Commodore 64
description: 
published: true
date: 2021-08-09T14:20:32.935Z
tags: computers, commodore, commodore-64, c64
editor: markdown
dateCreated: 2021-07-28T20:11:50.951Z
---

![](/emulators/computers/commodore64.svg){.align-center}

## Technical data

* **Manufacturer**: Commodore
* **Year of release**: 1982
* **OS**: Commodore KERNAL / Commodore BASIC 2.0 GEOS
* **CPU**: MOS Technology 6510/8500 @ 1.023 MHz (NTSC version)
* **RAM**: 64KB RAM
* **ROM**: 20KB
* **GPU**: MOS Technology VIC-II
* **Sound chip**: MOS Technology 6581 SID
* **Resolution**: 320×200 (16 colors)

## Presentation

The **Commodore 64** is a personal computer designed by Commodore Business Machines Inc. in 1982, under the leadership of Jack Tramiel. It was the first machine to sell several million copies (from 17 to 25 million according to estimates), and it remains the best-selling personal computer model to this day, according to the Guinness Book of Records.

The Commodore 64 uses an 8-bit 6510 microprocessor (a close derivative of the 6502 that has the ability to manage banks of memories by bringing them into the processor's address space on demand) and has 64 kilobytes of RAM. In the UK, it rivaled the ZX Spectrum in popularity and benefited from a full-size keyboard and more advanced graphics and sound chips.

The graphics chip, VIC-II, provided 16 colors, eight sprites, scrolling capabilities, and two bitmap graphics modes. The standard text mode provides 40 columns, as do most Commodore PET models.

The sound chip, SID, has three voices, multiple waveforms, sound modulations, and filtering capabilities. It is very advanced for its time. Its designer, Bob Yannes, will be the co-founder of the Ensoniq synthesizer company.

The built-in BASIC does not provide an easy way to access the advanced graphics and sound capabilities of the machine, so users must use the PEEK and POKE commands to directly address the memory to get the desired result, or use extensions like Simon's BASIC, or program directly in assembler. The extreme limitations of this BASIC, the necessity to have to research machine information close to the system (audio chip, video chip) to display graphics, to be able to play music and create sounds, the heaviness of using the PEEK and POKE instructions in BASIC on important programs, All this probably led the programmers of the time to switch very quickly from BASIC to assembly language, which was much faster and offered more possibilities, which may explain in part the very strong craze around this machine and the superior quality of the games and demos on it, compared to other microcomputers of the same time. This being said, Commodore had a better implementation of BASIC, but finally chose to sell the C64 with the same BASIC 2.0 used in the VIC-20 for fear that the C64 would make the PET/CBM sales drop.

The C64 inherited from the CBM machines and the VIC-20 a programmable user port (6522) and a proprietary serial port operating on a principle close to IEEE-488 and allowing to connect (and address) peripherals, in particular one or more units of 5.25 inch mono-floppy disks 1540 (floppy drive of the VIC-20), 1541 and 1542.

There was a portable model with integrated floppy drive and screen, but without the cassette drive port.

With this computer (probably) appeared an underground computer culture known as the demo scene.

## Emulators

[Libretro vice_x64](libretro-vice_x64)
[Libretro vice_x64sc](libretro-vice_x64sc)
[Libretro vice_xplus4](libretro-vice_xplus4)
[Libretro vice_xpet](libretro-vice_xpet)
[Libretro vice_xvic](libretro-vice_xvic)
[Libretro vice_x128](libretro-vice_x128)
[Libretro vice_xcbm2](libretro-vice_xcbm2)
[Libretro vice_xscpu64](libretro-vice_xscpu64)