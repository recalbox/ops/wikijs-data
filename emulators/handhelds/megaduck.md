---
title: Mega Duck
description: 
published: true
date: 2022-07-22T10:23:29.553Z
tags: 8.1+, megaduck
editor: markdown
dateCreated: 2022-07-22T10:23:29.553Z
---

![](/emulators/handheld/megaduck.png){.align-center}

## Technical data

* **Manufacturer**: Welback Holdings
* **Year of release**: 1993
* **CPU**: MOS version of the Z80 (embedded in the main VLSI)
* **RAM**: 16kB
* **Resolution**: 160x144 pixels, 4 shades of grey
* **Sound**: built-in speaker (8Ω 200mW)
* **Media**: 36 pins ROM cartridge

## Presentation

The Mega Duck WG-108 (also known as Cougar Boy) is a handheld game console that was developed and manufactured by Hong Kong-based Welback Holdings through its Timlex International division, and released in 1993.

It was marketed under various different brands worldwide including Creatronic and Videojet, and the shell of the console came in white or black plastic. It was sold for about fl 129 in the Netherlands, and for a similar price in France and Germany.

In South America (mainly in Brazil), the Chinese-made Creatronic version was distributed by Cougar USA, also known as "Cougar Electronic Organization", and sold as the "Cougar Boy". Cougar USA didn't release the Cougar Boy in its origin country.

The cartridges are very similar to those of the Watara Supervision, but slightly narrower with fewer contacts (36 pins, whereas Supervision cartridges have 40). Conceptually, the electronics inside the Supervision and the Mega Duck are also very similar. The position of the volume controls, contrast controls, buttons, and connectors are virtually identical. However, the LCD of the Supervision is larger than the Mega Duck's.

The Cougar Boy came with a 4-in-one game cartridge and a stereo earphone.

With an external joystick (not included) two players could play against each other simultaneously.

## Emulators

[Libretro Sameduck](libretro-sameduck)