---
title: Libretro Mednafen_PCFX
description: 
published: true
date: 2021-10-16T21:59:39.622Z
tags: libretro, mednafen, pc-fx, pcfx
editor: markdown
dateCreated: 2021-08-01T21:14:53.208Z
---

**Libretro Mednafen_PCFX** is a port of the Mednafen PC-FX emulator for the NEC PC-FX.

## ![](/emulators/license.svg) License

This core is under [**GPLv2**](https://github.com/libretro/beetle-pcfx-libretro/blob/master/COPYING) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ❌ | ❌ | ❌ | ✅ | ✅ | ❌ | ✅ | ✅ |

## ![](/emulators/features.png) Features

| Feature | Supported |
| :---: | :---: |
| Restart | ✔ |
| Screenshots | ✔ |
| Saves | ✔ |
| States | ✔ |
| Rewind | ✔ |
| Netplay | ✔ |
| Core Options | ✔ |
| RetroAchievements | ✔ |
| Controls | ✔ |
| Remapping | ✔ |
| Crop Overscan | ✔ |

## ![](/emulators/bios.svg) BIOS

### List of mandatory bios

| Filename | Description | MD5 | Provided |
| :---: | :---: | :---: | :---: |
| pcfx.rom | BIOS PC-FX v1.00 - 2 Sep 1994 | 08e36edbea28a017f79f8d4f7ff9b6d7 | ❌  |

### Location

Put the bios like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 🗒 **pcfx.rom**

## ![](/emulators/isos.svg) Isos

### Supported extensions

Isos must have the extension:

* .cue
* .ccd
* .toc
* .chd

### Location

Put the isos like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 pcfx
┃ ┃ ┃ ┃ ┣ 🗒 **game.cue**

>The isos using **Redump** format are highly recommended.
{.is-success}

>For more information about isos, go to [this tutorial](./../../../tutorials/games/generalities/isos-and-roms)!
{.is-info}

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Accessing to options

You can configure various options in two different ways.

* Via RetroArch menu:

┣ 📁 RetroArch Menu
┃ ┣ 📁 Core options
┃ ┃ ┣ 🧩 Name_option

* Via the file `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core options

| Options | Options values | Variables (for configuration override) | Variables values |
| --- | --- | --- | --- |
| High Dotclock Width (Restart) | `1024` ✅ / `256` / `341` | `pcfx_high_dotclock_width` | `1024` / `256` / `341` |
| Suppress Channel Reset Clicks (Restart) | `Enabled` ✅ / `Disabled` | `pcfx_suppress_channel_reset_clicks` | `enabled` / `disabled` |
| Emulate Buggy Codec (Restart) | `Disabled` ✅ / `Enabled` | `pcfx_emulate_buggy_codec` | `disabled` / `enabled` |
| Sound Quality (Restart) | `0` / `1` / `2` / `3` ✅ / `4` / `5` | `pcfx_resamp_quality` | `0` / `1` / `2` / `3` / `4` / `5` |
| Chroma channel bilinear interpolation (Restart) | `Disabled` ✅ / `Enabled` | `pcfx_rainbow_chromaip` | `disabled` / `enabled` |
| No Sprite Limit (Restart) | `Disabled` ✅ / `Enabled` | `pcfx_nospritelimit` | `disabled` / `enabled` |
| Initial scanline | `0` / `1` / `2` / `3` / `4` ✅ / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `21` / `22` / `23` / `24` / `25` / `26` / `27` / `28` / `29` / `30` / `31` / `32` / `33` / `34` / `35` / `36` / `37` / `38` / `39` / `40` | `pcfx_initial_scanline` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `21` / `22` / `23` / `24` / `25` / `26` / `27` / `28` / `29` / `30` / `31` / `32` / `33` / `34` / `35` / `36` / `37` / `38` / `39` / `40` |
| Last scanline | `208` / `209` / `210` / `211` / `212` / `213` / `214` / `215` / `216` / `217` / `218` / `219` / `220` / `221` / `222` / `223` / `224` / `225` / `226` / `227` / `228` / `229` / `230` / `231` / `232` / `233` / `234` / `235` ✅ / `236` / `237` / `238` / `239` | `pcfx_last_scanline` | `208` / `209` / `210` / `211` / `212` / `213` / `214` / `215` / `216` / `217` / `218` / `219` / `220` / `221` / `222` / `223` / `224` / `225` / `226` / `227` / `228` / `229` / `230` / `231` / `232` / `233` / `234` / `235` / `236` / `237` / `238` / `239` |
| Mouse Sensitivity | `0.25` / `0.50` / `0.75` / `1.00` / `1.25` ✅ / `1.50` / `1.75` / `2.00` / `2.25` / `2.50` / `2.75` / `3.00` / `3.25` / `3.50` / `3.75` / `4.00` / `4.25` / `4.50` / `4.75` / `5.00` | `pcfx_mouse_sensitivity` | `0.25` / `0.50` / `0.75` / `1.00` / `1.25` / `1.50` / `1.75` / `2.00` / `2.25` / `2.50` / `2.75` / `3.00` / `3.25` / `3.50` / `3.75` / `4.00` / `4.25` / `4.50` / `4.75` / `5.00` |

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/libretro/beetle-pcfx-libretro/](https://github.com/libretro/beetle-pcfx-libretro/)
* **Libretro documentation**: [https://docs.libretro.com/library/beetle_pc_fx/](https://docs.libretro.com/library/beetle_pc_fx/)