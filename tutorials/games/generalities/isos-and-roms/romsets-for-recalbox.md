---
title: Romsets for Recalbox
description: 
published: true
date: 2023-09-27T06:12:57.169Z
tags: romsets
editor: markdown
dateCreated: 2021-08-07T15:34:18.038Z
---

**Legend:**

  🥇    ==> Set to use
  🥈    ==> To complete (for hack roms, translation, etc...)
  🥉    ==> As a last resort
  🎮    ==> Recommended set for Netplay
  ❌    ==> No set available for this group

## Arcade

For arcade, the romset is different depending on the core you are using. You'll find all the romset versions you need on this [page](./../../../../emulators/arcade).

>For more information on Arcade roms, please visit [this page](./../../../../advanced-usage/arcade-in-recalbox).
{.is-info}

## Consoles

| Platform | No-Intro | TOSEC | Redump | Trurip |
| :---: | :---: | :---: | :---: | :---: |
| **32x** | 🥇 🎮 | 🥈 | ❌ | 🥉 |
| **Amiga CD32** | ❌ | 🥈 | 🥇 | 🥉 |
| **Amiga CDTV** | ❌ | 🥈 | 🥇 | 🥉 |
| **Amstrad GX4000** | ❌ | 🥇 | ❌ | 🥈 |
| **Atari 2600** | 🥇 🎮 | 🥈 | ❌ | ❌ |
| **Atari 5200** | 🥇 | 🥈 | ❌ | 🥉 |
| **Atari 7800** | 🥇 | 🥈 | ❌ | 🥉 |
| **Atari Jaguar** | 🥇 🎮 | 🥈 | ❌ | 🥉 |
| **ColecoVision** | 🥇 | 🥈 | ❌ | 🥉 |
| **Dreamcast** | ❌ | 🥇 | 🥈 | 🥉 |
| **Fairchild Channel F** | 🥇 | 🥈 | ❌ | 🥉 |
| **Famicom Disk System** | 🥇 🎮 | 🥈 | ❌ | 🥉 |
| **GameCube** | ❌ | 🥈 | 🥇 🎮 | 🥉 |
| **Intellivision** | 🥇 | 🥈 | ❌ | 🥉 |
| **Master System** | 🥇 🎮 | 🥈 | ❌ | 🥉 |
| **Genesis** | 🥇 🎮 | 🥈 | ❌ | 🥉 |
| **Multivision** | ❌ | 🥇 | ❌ | ❌ |
| **NeoGeo CD** | ❌ | ❌ | 🥇 | 🥇 |
| **NES** | 🥇 🎮 | 🥈 | ❌ | ❌ |
| **Nintendo 64** | 🥇 | 🥈 | ❌ | ❌ |
| **Nintendo 64DD** | 🥇 | 🥈 | ❌ | ❌ |
| **Odyssey 2** | 🥇 | 🥈 | ❌ | 🥉 |
| **Panasonic 3DO** | ❌ | 🥈 | 🥇 | 🥉 |
| **Pc-Engine** | 🥇 🎮 | 🥈 | ❌ | 🥉 |
| **Pc-Engine CD** | ❌ | 🥈 | 🥇 | 🥉 |
| **PCFX** | ❌ | 🥉 | 🥇 | 🥈 |
| **PlayStation 1** | ❌ | 🥉 | 🥇 | 🥈 |
| **PlayStation 2** | ❌ | 🥉 | 🥇 | 🥈 |
| **Satellaview** | 🥇 | 🥉 | ❌ | ❌ |
| **Saturn** | ❌ | 🥉 | 🥇 | 🥈 |
| **Sega CD** | ❌ | 🥉 | 🥇 | 🥈 |
| **Sega SG1000** | 🥇 🎮 | 🥉 | ❌ | ❌ |
| **Super Famicom** | 🥇 | 🥈 | ❌ | 🥉 |
| **SuFami Turbo** | 🥇 | 🥈 | ❌ | ❌ |
| **Super Nintendo** | 🥇 🎮 | 🥈 | ❌ | ❌ |
| **SuperGrafx** | 🥇 🎮 | 🥈 | ❌ | 🥉 |
| **Vectrex** | 🥇 | 🥈 | ❌ | 🥉 |
| **Virtual Boy** | 🥇 | 🥈 | ❌ | 🥉 |
| **Wii** | ❌ | 🥈 | 🥇 🎮 | ❌ |

## Handhelds

| Platform | No-Intro | Tosec | Redump | Trurip |
| :---: | :---: | :---: | :---: | :---: |
| **Game and Watch** | ❌ | ❌ | ❌ | ❌ |
| **Game Boy** | 🥇 | 🥈 | ❌ | 🥉 |
| **Game Boy Color** | 🥇 | 🥈 | ❌ | 🥉 |
| **Game Boy Advance** | 🥇 | 🥈 | ❌ | 🥉 |
| **Game Gear** | 🥇 | 🥈 | ❌ | 🥉 |
| **Lynx** | 🥇 | 🥈 | ❌ | 🥉 |
| **Nintendo DS** | 🥇 | ❌ | ❌ | ❌ |
| **NeoGeo Pocket** | 🥇 | 🥈 | ❌ | 🥉 |
| **NeoGeo Pocket Color** | 🥇 | 🥈 | ❌ | 🥉 |
| **Palm OS** | 🥇 | ❌ | 🥈 | 🥉 |
| **Pokémini** | 🥇 | 🥈 | ❌ | 🥉 |
| **PSP** | 🥇 | ❌ | 🥈 | ❌ |
| **Wonderswan** | 🥇 | 🥈 | ❌ | 🥉 |
| **Wonderswan Color** | 🥇 | 🥈 | ❌ | 🥉 |

## Computers

| Platform | No-Intro | Tosec | Redump | Trurip |
| :---: | :---: | :---: | :---: | :---: |
| **Amiga 600** | ❌ | 🥇 | ❌ | ❌ |
| **Amiga 1200** | ❌ | 🥇 | ❌ | ❌ |
| **Amstrad CPC** | 🥈 | 🥇 | ❌ | 🥉 |
| **Apple II** | ❌ | 🥇 | ❌ | 🥈 |
| **Apple II Gs** | ❌ | 🥇 | ❌ | 🥈 |
| **Atari 800** | ❌ | 🥇 | ❌ | 🥈 |
| **Atari St** | 🥈 | 🥇 | ❌ | 🥉 |
| **Commodore 64** | 🥈 | 🥇 | ❌ | 🥉 |
| **Dragon 32/64** | ❌ | 🥇 | ❌ | ❌ |
| **Elektronika BK** | ❌ | 🥇 | ❌ | ❌ |
| **MSX 1** | 🥈 | 🥇 | ❌ | 🥉 |
| **MSX 2** | 🥈 | 🥇 | ❌ | 🥉 |
| **MSX Turbo R** | 🥈 | 🥇 | ❌ | 🥉 |
| **NEC Pc8801** | ❌ | 🥈 | 🥇 | 🥉 |
| **NEC Pc9801** | ❌ | 🥈 | 🥇 | 🥉 |
| **Oric Atmos** | ❌ | 🥇 | ❌ | ❌ |
| **Samcoupé** | ❌ | 🥇 | ❌ | 🥈 |
| **Sharp X1** | ❌ | 🥇 | ❌ | 🥈 |
| **Sharp X68000** | ❌ | 🥈 | 🥇 | 🥉 |
| **Sinclair ZXspectrum** | 🥇 | 🥈 | ❌ | 🥉 |
| **Sinclair ZX81** | ❌ | 🥇 | ❌ | 🥈 |
| **Spectravideo SV-318** | ❌ | 🥇 | ❌ | 🥈 |
| **Tandy TRS-80 Color Computer** | ❌ | 🥇 | ❌ | ❌ |
| **Texas Instruments TI-99/4A** | ❌ | 🥇 | ❌ | ❌ |
| **Thomson MO/TO** | ❌ | 🥇 | ❌ | 🥈 |