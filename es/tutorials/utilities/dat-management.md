---
title: Gestión de los ficheros dat
description: 
published: true
date: 2023-09-26T07:07:05.373Z
tags: dat, gestión
editor: markdown
dateCreated: 2023-09-16T08:23:50.089Z
---

En esta sección podéis aprender a crear ficheros dats a partir de ficheros dat existentes con vuestras propias opciones dentro, como por ejemplo para eliminar los clones.

Aquí están los tutoriales disponibles:

[DatUtil](datutil)