---
title: Commandes linux utiles
---



## Commandes courantes <a id="1-commandes-courantes-a-connaitre"></a>

Voici une **liste de commande linux** qui vous seront utiles **pour naviguer** dans **Recalbox** via un **terminal.**

* **ls** Son équivalent sous Windows est `dir`. Elle permet de lister le contenu d'un dossier. Vous pouvez consulter les options disponible sur [cette page](http://pwet.fr/man/linux/commandes/ls/).
* **cd** Son équivalent sous Windows est `cd`. Elle permet de changer de répertoire \(cd = Change Directory\). Vous pouvez consulter les options disponibles sur [cette page](http://pwet.fr/man/linux/commandes/posix/cd/).
* **cp​** Son équivalent sous Windows est `copy`. Elle permet de copier des fichiers ou des répertoire. Vous pouvez consulter les options disponibles sur [cette page](http://pwet.fr/man/linux/commandes/cp/).
* **mv** Son équivalent sous Windows est `move` ou `ren`. Elle permet de déplacer ou de renommer un fichier ou un répertoire. Vous pouvez consulter les options disponibles sur [cette page](http://pwet.fr/man/linux/commandes/mv/).
* **mkdir** Son équivalent sous Windows est `mkdir` ou `md`. Elle permet de créer un répertoire vide. Vous pouvez consulter les options disponibles sur [cette page](http://pwet.fr/man/linux/commandes/mkdir/).
* **rmdir** Son équivalent sous Windows est `rmdir` ou `rd`. Elle permet de supprimer un répertoire. Vous pouvez consulter les options disponibles sur [cette page](http://pwet.fr/man/linux/commandes/rmdir/).
* **nano** Elle permet d'ouvrir un éditeur de texte. Vous pouvez consulter les options disponibles sur [cette page](http://pwet.fr/man/linux/commandes/nano/).​
* **chmod** Elle permet de modifier les autorisations d'accès à un fichier. Vous pouvez consulter les options disponibles sur [cette page](http://pwet.fr/man/linux/commandes/chmod/).​
* **wget** Elle permet de télécharger une source disponible sur Internet. Vous pouvez consulter les options disponibles sur [cette page](http://pwet.fr/man/linux/commandes/wget/).​

## Commandes spécifiques <a id="2-commandes-specifiques"></a>

### Vérifier la température du processeur du Raspberry <a id="verifier-la-temperature-du-processeur-du-raspberry"></a>

Pour vérifier la température du processeur \(CPU\), exécutez la commande suivante : 

`cat /sys/class/thermal/thermal_zone0/temp`

Puis divisez le résultat par 1000 pour avoir la température en Celsius.

```text
# cat /sys/class/thermal/thermal_zone0/temp
54530
```

### Vérifier les paramètres d'overclocking appliqués <a id="verifier-les-parametres-doverclocking-appliques"></a>

Pour vérifier les valeurs d'overclocking appliquées, exécutez la commande suivante :

`cat /boot/recalbox-oc-config.txt`

Vous aurez un résultat similaire à cet exemple :

```text
# Plateform: RPI4
# Description: Extrem
# Warning
# Frequency: 2000

over_voltage=6
arm_freq=2000
gpu_freq=750
```

### Éviter de redémarrer le système si l'émulateur N64 standalone Mupen64 plante

Si vous rencontrez des problèmes de plantages / bloquages avec l'émulateur N64 standalone Mupen64, due notamment à des problèmes de compatibilité avec les roms, vous pouvez facilement « tuer » l'émulateur au lieu de redémarrer le Raspberry. Pour cela, vous devez identifier l'identifiant du processus Mupen64 avec la commande suivante :

`ps aux | grep mupen64`.

Vous devriez obtenir une liste similaire à :

```text
26193 root     mupen64plus --corelib /usr/lib/libmupen64plus.so.2.0.0 --gfx /usr/lib/mupen64plus/mupen64plus-video-gliden64.so --configdir /recalbox/share/system/configs/mupen64/ --datadir /recalbox/share/system/configs/mupen64/ /recalbox/share/roms/n64/007 - GoldenEye (Europe).n64
26196 root     grep mupen64
```

Dans le cas présent, l'identifiant du processus Mupen64 \(PID\) est **26193**. Vous pouvez maintenant tuer le processus Mupen64 avec la commande suivante :

`kill -1 <votre_PID>`

Soit pour notre exemple : `kill -1 26193`

Une fois le processus tué, vous retournerez automatiquement sur EmulationStation.

### Arrêter / démarrer EmulationStation <a id="arreter-demarrer-emulationstation"></a>

Pour **mettre à jour la liste des ROMS** par exemple ou pour **mettre à jour les images des jeux** nouvellement scrapés depuis votre PC vous devrez arrêter puis redémarrer EmulationStation :

* Arrêt :  `es stop` 
* Redémarrage / démarrage : `es start` 

