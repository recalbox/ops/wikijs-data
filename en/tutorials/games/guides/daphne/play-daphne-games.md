---
title: Play Daphne games
description: 
published: true
date: 2021-08-08T16:15:05.240Z
tags: games, daphne
editor: markdown
dateCreated: 2021-08-08T16:14:47.465Z
---

## Daphne games list

Daphne games are the games that do not use extensions such as Singe.

Here is the list of Daphne games:

* Astron Belt
* Badlands
* Bega's Battle
* Cliff Hanger
* Cobra Command
* Dragon's Lair
* Dragon's Lair II: Time Warp
* Esh's Aurunmilla
* Galaxy Ranger
* GP World
* M.A.C.H. 3
* Road Blaster
* Space Ace
* Super Don Quix-Ote
* Thayer's Quest
* Us Vs Them

## Installation of the games

Each game is divided into 2 parts:

* The data to run the game (videos, music, etc.)
* The game data (zip file)

### For the game data

The game data are, for each, contained in a `.ZIP` file with specific extensions. These files are not to be opened and must have the same name as the folder `gamename.daphne`.

You must place them in the following directory:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┃ ┃ ┣ 🗒 gamename.zip

### For the data to run the game

The following contents are required:

* 1 file `gamename.commands` (optional)
* 1 file `gamename.dat` (optional)
* 1 or more `gamename.m2v` files
* 1 or more `gamename.m2v.bf` files (optional)
* 1 or more `gamename.ogg` files
* 1 or more files `gamename.ogg.bf` (optional)
* 1 file `gamename.txt` (optional)

You must place them in the following directory:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 gamename.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 gamename.commands
┃ ┃ ┃ ┃ ┃ ┣ 🗒 gamename.dat
┃ ┃ ┃ ┃ ┃ ┣ 🗒 gamename.m2v
┃ ┃ ┃ ┃ ┃ ┣ 🗒 gamename.ogg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 gamename.txt
┃ ┃ ┃ ┃ ┃ ┣ 🗒 ...

>Some file names may not have the same names as the game like `.DAT`, `.M2V` and `.OGG` files. This is not a problem in itself, as long as the `.M2V` files are correctly listed in the `.TXT` file, I should be able to run the game without any problems.
{.is-info}

## Location of files per game

Since it is particularly difficult to place the right files in the right places, you will see below how to put the necessary files for each game in the Recalbox tree.

## {.tabset}
### Astron Best

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 astron.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 astron.commands
┃ ┃ ┃ ┃ ┃ ┣ 🗒 astron.txt
┃ ┃ ┃ ┃ ┃ ┣ 🗒 astron512.m2v
┃ ┃ ┃ ┃ ┃ ┣ 🗒 astron512.ogg
┃ ┃ ┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┃ ┃ ┣ 🗒 astron.zip

### Badlands

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 badlands.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 badlands-r.dat
┃ ┃ ┃ ┃ ┃ ┣ 🗒 badlands-r.m2v
┃ ┃ ┃ ┃ ┃ ┣ 🗒 badlands-r.ogg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 badlands.commands
┃ ┃ ┃ ┃ ┃ ┣ 🗒 badlands.txt
┃ ┃ ┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┃ ┃ ┣ 🗒 badlands.zip

### Bega's Battle

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 bega.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 bega.commands
┃ ┃ ┃ ┃ ┃ ┣ 🗒 bega.dat
┃ ┃ ┃ ┃ ┃ ┣ 🗒 bega.m2v
┃ ┃ ┃ ┃ ┃ ┣ 🗒 bega.ogg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 bega.txt
┃ ┃ ┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┃ ┃ ┣ 🗒 bega.zip

### Cliff Hanger

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 .daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 .commands
┃ ┃ ┃ ┃ ┃ ┣ 🗒 CH_640x480_24p.dat
┃ ┃ ┃ ┃ ┃ ┣ 🗒 CH_640x480_24p.m2v
┃ ┃ ┃ ┃ ┃ ┣ 🗒 CH_640x480_24p.ogg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 cliff.txt
┃ ┃ ┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┃ ┃ ┣ 🗒 cliff.zip

### Cobra Command

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 cobra.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 cc.dat
┃ ┃ ┃ ┃ ┃ ┣ 🗒 cc.m2v
┃ ┃ ┃ ┃ ┃ ┣ 🗒 cc.ogg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 cobra.commands
┃ ┃ ┃ ┃ ┃ ┣ 🗒 cobra.txt
┃ ┃ ┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┃ ┃ ┣ 🗒 cobra.zip

### Dragon's Lair

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 lair.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 lair.commands
┃ ┃ ┃ ┃ ┃ ┣ 🗒 lair.dat
┃ ┃ ┃ ┃ ┃ ┣ 🗒 lair.m2v
┃ ┃ ┃ ┃ ┃ ┣ 🗒 lair.m2v.bf
┃ ┃ ┃ ┃ ┃ ┣ 🗒 lair.ogg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 lair.ogg.bf
┃ ┃ ┃ ┃ ┃ ┣ 🗒 lair.txt
┃ ┃ ┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┃ ┃ ┣ 🗒 lair.zip

### Dragon's Lair II : Time Warp

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 lair2.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 dl2-00001.dat
┃ ┃ ┃ ┃ ┃ ┣ 🗒 dl2-00001.m2v
┃ ┃ ┃ ┃ ┃ ┣ 🗒 dl2-00001.m2v.bf
┃ ┃ ┃ ┃ ┃ ┣ 🗒 dl2-00001.ogg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 dl2-00001.ogg.bf
┃ ┃ ┃ ┃ ┃ ┣ 🗒 ...
┃ ┃ ┃ ┃ ┃ ┣ 🗒 dl2-44550.dat
┃ ┃ ┃ ┃ ┃ ┣ 🗒 dl2-44550.m2v
┃ ┃ ┃ ┃ ┃ ┣ 🗒 dl2-44550.m2v.bf
┃ ┃ ┃ ┃ ┃ ┣ 🗒 dl2-44550.ogg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 dl2-44550.ogg.bf
┃ ┃ ┃ ┃ ┃ ┣ 🗒 lair2.commands
┃ ┃ ┃ ┃ ┃ ┣ 🗒 lair2.txt
┃ ┃ ┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┃ ┃ ┣ 🗒 lair2.zip

### Esh's Aurunmilla

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 esh.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 esh.dat
┃ ┃ ┃ ┃ ┃ ┣ 🗒 esh.m2v
┃ ┃ ┃ ┃ ┃ ┣ 🗒 esh.ogg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 esh.txt
┃ ┃ ┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┃ ┃ ┣ 🗒 esh.zip

### Galaxy Ranger

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 galaxy.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 galaxy.dat
┃ ┃ ┃ ┃ ┃ ┣ 🗒 galaxy.m2v
┃ ┃ ┃ ┃ ┃ ┣ 🗒 galaxy.ogg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 galaxy.txt
┃ ┃ ┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┃ ┃ ┣ 🗒 galaxy.zip

### GP World

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 gpworld.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 gpdiags.dat
┃ ┃ ┃ ┃ ┃ ┣ 🗒 gpdiags.m2v
┃ ┃ ┃ ┃ ┃ ┣ 🗒 gpdiags.ogg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 gpworld.dat
┃ ┃ ┃ ┃ ┃ ┣ 🗒 gpworld.m2v
┃ ┃ ┃ ┃ ┃ ┣ 🗒 gpworld.ogg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 gpworld.txt
┃ ┃ ┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┃ ┃ ┣ 🗒 gpworld.zip

### M.A.C.H. 3

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 mach3.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 mach3.commands
┃ ┃ ┃ ┃ ┃ ┣ 🗒 mach3.dat
┃ ┃ ┃ ┃ ┃ ┣ 🗒 mach3.m2v
┃ ┃ ┃ ┃ ┃ ┣ 🗒 mach3.ogg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 mach3.txt
┃ ┃ ┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┃ ┃ ┣ 🗒 mach3.zip

### Road Blaster

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 roadblaster.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 rb.d2v
┃ ┃ ┃ ┃ ┃ ┣ 🗒 rb.dat
┃ ┃ ┃ ┃ ┃ ┣ 🗒 rb.demuxed.m2v
┃ ┃ ┃ ┃ ┃ ┣ 🗒 rb.m2v
┃ ┃ ┃ ┃ ┃ ┣ 🗒 rb.mpeg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 rb.ogg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 roadblater.txt
┃ ┃ ┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┃ ┃ ┣ 🗒 roadblaster.zip

### Space Ace

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 ace.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 ace.commands
┃ ┃ ┃ ┃ ┃ ┣ 🗒 ace.m2v
┃ ┃ ┃ ┃ ┃ ┣ 🗒 ace.m2v.bf
┃ ┃ ┃ ┃ ┃ ┣ 🗒 ace.ogg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 ace.ogg.bf
┃ ┃ ┃ ┃ ┃ ┣ 🗒 ace.txt
┃ ┃ ┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┃ ┃ ┣ 🗒 ace.zip

### Super Don Quix-Ote

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 sdq.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 sdq-blank.dat
┃ ┃ ┃ ┃ ┃ ┣ 🗒 sdq-blank.m2v
┃ ┃ ┃ ┃ ┃ ┣ 🗒 sdq-colorbars.dat
┃ ┃ ┃ ┃ ┃ ┣ 🗒 sdq-colorbars.m2v
┃ ┃ ┃ ┃ ┃ ┣ 🗒 sdq-crosshairs.dat
┃ ┃ ┃ ┃ ┃ ┣ 🗒 sdq-crosshairs.m2v
┃ ┃ ┃ ┃ ┃ ┣ 🗒 sdq.commands
┃ ┃ ┃ ┃ ┃ ┣ 🗒 sdq.dat
┃ ┃ ┃ ┃ ┃ ┣ 🗒 sdq.m2v
┃ ┃ ┃ ┃ ┃ ┣ 🗒 sdq.ogg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 sdq.txt
┃ ┃ ┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┃ ┃ ┣ 🗒 sdq.zip

### Thayer's Quest

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 tq.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 tq.dat
┃ ┃ ┃ ┃ ┃ ┣ 🗒 tq.m2v
┃ ┃ ┃ ┃ ┃ ┣ 🗒 tq.m2v.bf
┃ ┃ ┃ ┃ ┃ ┣ 🗒 tq.ogg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 tq.ogg.bf
┃ ┃ ┃ ┃ ┃ ┣ 🗒 tq.txt
┃ ┃ ┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┃ ┃ ┣ 🗒 tq.zip

### Us Vs Them

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 uvt.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 uvt-170606.dat
┃ ┃ ┃ ┃ ┃ ┣ 🗒 uvt-170606.m2v
┃ ┃ ┃ ┃ ┃ ┣ 🗒 uvt-170606.ogg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 uvt.commands
┃ ┃ ┃ ┃ ┃ ┣ 🗒 uvt.txt
┃ ┃ ┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┃ ┃ ┣ 🗒 uvt.zip