---
title: Nintendo DS
description: 
published: true
date: 2021-08-09T12:59:37.341Z
tags: nintendo, screens, handhelds, ds, dual
editor: markdown
dateCreated: 2021-07-06T09:01:59.697Z
---

![](/emulators/handheld/nds.svg){.align-center}

## Technical data

* **Manufacturer**: Nintendo
* **Year of release**: 2004
* **Units sold**: 153.99 millions
* **Best-selling game**: New Super Mario Bros.
* **CPU**: 66 MHz ARM9 + 33 MHz ARM7
* **RAM**: 4MB
* **Video RAM**: 656KB
* **Screen**: Reflective TFT Colour LCD
* **Resolution**: 256x192 pixels
* **Display ability**: 260000 colors
* **Sound**: Stereo speakers w/virtual surround, headphones

## Presentation

The **Nintendo DS** ("DS" for Dual Screen, Double Screen in Japan), is a portable console created by Nintendo, released in late 2004 in Japan and North America and in 2005 in Europe.

It is equipped with several features that were previously rare or even unheard of in the field of portable video games, such as two simultaneously backlit screens, one of which is a touch screen, a microphone, two cartridge ports (one for Nintendo DS games, the other for Game Boy Advance game cartridges and accessories), two surround (virtual) compatible speakers, and integrated Wi-Fi, with a range of 10 to 30 meters in LAN, which makes it possible to connect sixteen consoles to each other, and to connect to the Nintendo Wi-Fi Connection to play online.

In 2006, Nintendo released the Nintendo DS Lite, a revised, smaller and lighter model of the system. In 2008, Nintendo released the Nintendo DSi, a new revision of the system with new additions, such as dual cameras.

The Nintendo DS, with its various variants, is now the best-selling handheld console of all time and the second best-selling console when home consoles are taken into account, just behind Sony's PlayStation 2. Its flagship game, New Super Mario Bros, is one of the best-selling games in the world.

## Emulators

[Libretro DeSmuME](libretro-desmume)
[Libretro melonDS](libretro-melonds)