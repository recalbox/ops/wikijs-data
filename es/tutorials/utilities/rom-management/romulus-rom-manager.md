---
title: Romulus Rom Manager
description: 
published: true
date: 2025-02-24T17:47:22.217Z
tags: manager, rom, romulus
editor: markdown
dateCreated: 2023-09-16T20:05:31.673Z
---

## Introducción

Para la mayoría de los usuarios, la gestión de Roms sigue siendo un tema que  complicado, lo que da lugar a muchas preguntas en el foro y en el Discord.

Con el fin de hacer la gestión de roms más accesible, este tutorial os enseñará a utilizar **Romulus Rom Manager** (en adelante **Romulus**), ya que es la herramienta más sencilla disponible actualmente. Por supuesto, existen otras herramientas como **clrMamePro** o **Romcenter** (que son más completas, sobre todo para el arcade) pero que son mucho más complicadas.

La gestión de roms no es sólo para los juegos de arcade, también es muy recomendable para juegos de consola.

## Descargar Romulus

Página oficial: [https://romulus.dats.site/](https://romulus.dats.site/)

## Instalación

Una vez descargado Romulus, descomprima el fichero en cualquier directorio.

Esto producirá los siguientes archivos:

![romulus1.png](/tutorials/utilities/rom-management/romulus/romulus1.png)

Al lanzar **Romulus.exe**, aparecerá la siguiente ventana:

![](/tutorials/utilities/rom-management/romulus/romulus2.png)

Pulsad **OK**. Bienvenidos a Romulus:

![](/tutorials/utilities/rom-management/romulus/romulus3.png)

## Romulus para el arcade

Visitad: [Romulus para el Arcade](romulus-for-arcade)

## Romulus para las consolas

Visitad: [Romulus para las consolas](romulus-for-consoles)

## Información sobre Romulus Rom Manager

El desarrollo de este software ha sido oficialmente abandonado por su autor y ya no será actualizado. Sin embargo, uno de nuestros usuarios pudo obtener el código fuente de Romulus de su desarrollador. Si queréis haceros cargo del proyecto y seguir desarrollándolo, ponéos en contacto con nosotros en nuestro [Discord](https://discord.gg/HKg3mZG)