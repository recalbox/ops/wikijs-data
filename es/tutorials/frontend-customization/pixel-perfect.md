---
title: Ajustar la imagen (Pixel Perfect)
description: 
published: true
date: 2023-09-15T09:14:19.677Z
tags: pixel, perfect, imagen, ajustar
editor: markdown
dateCreated: 2023-09-15T09:14:19.677Z
---

## Objetivo

Activa el modo de escalado de imagen "Pixel Perfect" para obtener el renderizado exacto de los juegos en una pantalla moderna, sin agrandir la imagen y sin deformar los pixels.

## Etapas

* Abrid el menú de EmulationStation pulsado el botón `START`.
* Id dentro de `OPCIONES DE LOS JUEGOS`
* Al nivel de la opción `SHADERS PREDEFINIDOS`, seleccionad `SCANLINES` o `RETRO`.
* Activad la opción `AJUSTAR A LA ESCALA (PIXEL PERFECT)`.

>Usando este modo, ¡puede que la imagen no cubra complétamente la pantalla!
{.is-warning}