---
title: Libretro QUASI88
description: 
published: true
date: 2021-11-06T00:09:02.963Z
tags: libretro, pc-88, quasi88
editor: markdown
dateCreated: 2021-07-29T17:08:17.011Z
---



## ![](/emulators/license.svg) License

This core is under [**BSD 3-Clause**](https://github.com/libretro/quasi88-libretro/blob/master/LICENSE) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Features



## ![](/emulators/bios.svg) BIOS

### List of mandatory bios

| Filename | Description | MD5 | Provided |
| :---: | :---: | :---: | :---: |
| n88.rom | BASIC rom | 22be239bc0c4298bc0561252eed98633 f7cba6a308c2718dbe97e60e46ddd66a 4f984e04a99d56c4cfe36115415d6eb8 | ❌ |
| n88_0.rom | BASIC rom #1 | e28fe3f520bea594350ea8fb00395370 c254685f00ca9c31b97203d6ef19f5e2 d675a2ca186c6efcd6277b835de4c7e5 | ❌ |
| n88_1.rom | BASIC rom #2 | e844534dfe5744b381444dbe61ef1b66 a8e298da7ac947669bcb1ff25cee0a83 | ❌ |
| n88_2.rom | BASIC rom #3 | 6548fa45061274dee1ea8ae1e9e93910 9d03154fd9abfc28c4e6d4dc705e6e23 | ❌ |
| n88_3.rom | BASIC rom #4 | fc4b76a402ba501e6ba6de4b3e8b4273 e1791f8154f1cdf22b576a1a365b6e1f | ❌ |
| n88n.rom | Emulation de la série PC-8000 | 5d6854624dd01cd791f58727fc43a525 93cd1d78b7b9c50b80041ed330332ece 2ff07b8769367321128e03924af668a0 | ❌ |
| disk.rom | Chargement des images disque | 793f86784e5608352a5d7f03f03e0858 01b1af474fcabe93c40d779b234a3825 | ❌ |

### Locarion

Put the bios like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 quasi88
┃ ┃ ┃ ┃ ┣ 🗒 **n88.rom**
┃ ┃ ┃ ┃ ┣ 🗒 **n88_0.rom**
┃ ┃ ┃ ┃ ┣ 🗒 **n88_1.rom**
┃ ┃ ┃ ┃ ┣ 🗒 **n88_2.rom**
┃ ┃ ┃ ┃ ┣ 🗒 **n88_3.rom**
┃ ┃ ┃ ┃ ┣ 🗒 **n88n.rom**
┃ ┃ ┃ ┃ ┣ 🗒 **disk.rom**

### List of optional bios

| Filename | Description | MD5 | Provided |
| :---: | :---: | :---: | :---: |
| n88knj1.rom | Kanjis display. | d81c6d5d7ad1a4bbbd6ae22a01257603 | ❌ |

### Location

Put the bios like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 quasi88
┃ ┃ ┃ ┃ ┣ 🗒 **n88knj1.rom**

## ![](/emulators/roms.png) Roms

### Supported extensions

Roms must have the extension:

* .d88
* .t88
* .cmt
* .m3u
* .zip
* .7z

This system supports compressed roms in .zip/.7z format. However, be careful, it is only an archive.

The files contained in the .zip/.7z must correspond to the extensions mentioned above.
Moreover, each .zip/.7z file must contain only one compressed rom.

### Location

Put the roms like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 pc88
┃ ┃ ┃ ┃ ┣ 🗒 **game.zip**

>The roms using **TOSEC** format are highly recommended.
{.is-success}

>For more information about roms, go to [this tutorial](./../../../tutorials/games/generalities/isos-and-roms)!
{.is-info}

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Accessing to options

You can configure various options in two different ways.

* Via RetroArch menu:

┣ 📁 RetroArch Menu
┃ ┣ 📁 Core options
┃ ┃ ┣ 🧩 Name_option

* Via the file `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core options

| Options | Options values | Variables (for configuration override) | Variables values |
| --- | --- | --- | --- |
| BASIC mode | `N88-BASIC V2` ✅ / `N88-BASIC V1H` / `N88-BASIC V1S` / `N-BASIC` | `q88_basic_mode` | `N88 V2` / `N88 V1H` / `N88 V1S` / `N` |
| Sub-CPU Mode | `Run SUB-CPU only during the disk access` ✅ / `Run both CPUs during the disk access` / `Always run both CPUs` | `q88_sub_cpu_mode` | `0` / `1` / `2` |
| CPU | `1 MHz (underclock)` / `2 MHz (underclock)` / `4 MHz (NEC µPD780)` ✅ / `8 MHz (NEC µPD70008)` / `16 MHz (overclock)` / `32 MHz (overclock)` / `64 MHz (overclock)` | `q88_cpu_clock` | `1` / `2` / `4` / `8` / `16` / `32` / `64` |
| Sound board | `OPN (Yamaha YM2203)` / `OPNA (Yamaha YM2608)` ✅ | `q88_sound_board` | `OPN` / `OPNA` |
| Use FDC-Wait | `Enabled` ✅ / `Disabled` | `q88_use_fdc_wait` | `enabled` / `disabled` |
| Use PCG-8100 | `Disabled` ✅ / `Enabled` | `q88_use_pcg-8100` | `disabled` / `enabled` |
| Save to disk image | `Disabled` ✅ / `Enabled` | `q88_save_to_disk_image` | `disabled` / `enabled` |
| Rumble on disk access | `Enabled` ✅ / `Disabled` | `q88_rumble` | `enabled` / `disabled` |
| Screen size | `Full (640x400)` ✅ / `Half (320x200)` | `q88_screen_size` | `full` / `half` |

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/libretro/quasi88-libretro/](https://github.com/libretro/quasi88-libretro/)
* **Libretro documentation**: [https://docs.libretro.com/library/quasi88/](https://docs.libretro.com/library/quasi88/)