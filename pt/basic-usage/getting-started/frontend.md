---
title: EmulationStation
description: A interface do Recalbox
published: true
date: 2024-11-11T07:30:41.685Z
tags: 
editor: markdown
dateCreated: 2021-06-30T00:15:32.102Z
---

## Apresentação

Ao ligar o Recalbox, você acessa a interface do software chamado de **EmulationStation** (ES).

Isso permite que você inicie seus jogos, ajuste certas opções ou atualize seu Recalbox.

### Lista de sistemas

A primeira tela é a lista de sistemas. Ela lista os consoles e sistemas disponíveis.

![](/basic-usage/getting-started/es1-pt.png){.full-width}

Quando você seleciona um sistema, a tela muda e você é levado à sua lista de jogos disponíveis.

### Lista de jogos

Uma lista de jogos está disponível para o sistema selecionado na tela anterior.

Assim que o jogo for iniciado, consulte [Durante o jogo](./../../basic-usage/getting-started/special-commands/in-game) para as opções disponíveis.

## Opções no EmulationStation

Ao pressionar o botão `START`, você poderá modificar algumas configurações do seu Recalbox.

![](/basic-usage/getting-started/es2-pt.png){.full-width}

### Kodi Media Center

* Inicia o **Kodi Media Center** (anteriormente conhecido como `XBMC`).

Você pode iniciar o Kodi na tela inicial pressionando o botão `X` em seu controle.

Para sair do Kodi, selecione `EXIT` no programa e você estará de volta ao EmulationStation.

Os controles são suportados no Kodi. Mas se preferir, você também pode usar HDMI-CEC (permite usar o controle remoto da TV para navegar no Kodi) ou um aplicativo de controle remoto para smartphone.

Para obter mais informações sobre o Kodi, você pode verificar os tutoriais:

[Kodi Media Center](./../features/kodi-media-center)
[Use um controle remoto infravermelho](./../features/kodi-media-center/remote-control-usage)

### Configurações de sistema

Você terá acesso às informações do seu Recalbox.

* **Versão do Recalbox**: a versão do Recalbox atualmente instalada.
* **Espaço usado**: espaço de armazenamento usado/disponível.
* **Armazenamento**: lista as mídias de armazenamento disponíveis:
  * Partição SHARE interna (primeiro disco rígido usado pelo sistema para a primeira inicialização).
  * Qualquer dispostivo externo (todos os discos rígidos e/ou *pen drives* detectados).
* **Idioma**: a seleção do idioma do menu.
* **Teclado**: o tipo de teclado que você deseja usar.

### Atualizações

Este menu se propõe a ativar ou não as atualizações e escolher se deseja instalar apenas uma versão estável ou também as versões beta do Recalbox.

* **Verificar atualizações**: permite verificar se existe uma atualização.
* **Atualização disponível**: indique se há uma atualização disponível.
* **Mudanças da atualização**: permite que você leia o conteúdo da atualização.
* **Iniciar atualização**: permite que você inicie o processo de atualização.

### Configurações dos Jogos

Este menu oferece as seguintes configurações:

* **Formato de tela**:
  * Automático
  * Configuração RetroArch
  * Fornecido pelo núcleo
  * Deixar o padrão
  * Píxel quadrado
  * Personalizações RetroArch
  * 1/1
  * 16/10
  * 16/15
  * 16/9
  * 19/12
  * 19/14
  * 2/1
  * 21/9
  * 3/2
  * 3/4
  * 30/17
  * 32/9
  * 4/1
  * 4/3
  * 5/4
  * 6/5
  * 7/9
  * 8/3
  * 8/7
  * 9/16 
* **Suavizar imagem**: permite que você dê um pequeno efeito de desfoque aos pixels para que fiquem mais bonitos em nossas TVs modernas.
* **Rebobinar**: permite que você volte no tempo durante o jogo.

> Isso pode tornar alguns emuladores mais lentos (PS1, Dreamcast, ...) se você ativá-lo por padrão nesses emuladores.
> Você pode ativá-lo apenas para alguns emuladores através do menu de [Configurações Avançadas](#configurações_avançadas).
{.is-warning}

* **Salvar/carregar automático**: permite que você retome um jogo de onde parou ao encerrar o jogo.
* **Pressione duas vezes para sair**: permite que você confirme que deseja encerrar o jogo em andamento.
* **Escala inteira (Pixel perfeito)**: Exibe os jogos na resolução original.
* **Shaders**: você pode aplicar um filtro na imagem do jogo para dar um efeito especial, como uma imagem mais retrô.
* **Conjunto de Shaders**: você pode configurar facilmente os _shaders_ para os diferentes sistemas:
  * Os *shaders **scanlines*** ativam as *scanlines* em todos os sistemas para obter uma renderização próxima a uma tela CRT.
  * Os *shaders* **retro** são um pacote de *shaders*, correspondendo ao melhor *shader* para aplicar em cada sistema. Os shaders que compõem este pacote, são escolhidos pela comunidade e trarão a você a experiência de jogo mais próxima da experiência original para cada sistema!
  Para mais informações, você pode consultar o tutorial [configuração de shaders](./../../tutorials/games/generalities/shaders-configuration).

> Você também pode alterar o _shader_ durante o jogo, usando seu controle. Use os [Comandos especiais](./special-commands/in-game) `Hotkey` + `R2` ou `Hotkey` + `L2` para ver o próximo shader ou o shader anterior.
{.is-info}


* **Configurações do Retroachievements**: permite que você configure Retroachievements (equivalente a conquistas/troféus, mas para consoles antigos).
* **Configurações de Jogo em Rede**: permite configurar o NetPlay (jogo online).
* **Ocultar jogos pré-instalados**: permite ocultar os jogos que vem pré-instalados com o sistema.

### Configurações dos controles

É aqui que você pode configurar seus controles.

* **Configurar um controle**: permite que você defina os botões de um de seus controles.
* **Parear controle Bluetooth**: permite que você associe um controle sem fio ao seu Recalbox.
* **Esquecer controles Bluetooth**: permite desemparelhar todos os controles sem fio.
* **Driver**: permite definir o driver a ser usado com seu controle. Você não precisa modificá-lo se o seu controle estiver funcionando.

### Configurações da interface

Este menu oferece acesso às configurações da interface.

* **Protetor de tela**: Altere o protetor de tela com diferentes opções.
* **Relógio no menu**: Exibe a hora atual nos menus do EmulationStation.
* **Ajuda na tela**: permite que você exiba um pop-up de ajuda no canto superior direito da tela para cada uma das opções, pressionando o botão `Y`.
* **Configurações da caixa de diálogo**: permite que você modifique as opções para os pop-ups de música e NetPlay.

> Os pop-ups de atualização não são configuráveis.
{.is-info}

* **Seleção rápida de sistema**: usado para alternar de um sistema para outro usando as teclas direita ou esquerda nas listas de jogos.
* **Tema**: escolha entre os diferentes temas criados pela comunidade (esses novos temas não estão disponíveis por padrão no Recalbox).
* **Configuração do tema**: alguns dos temas instalados possuem opções de configuração.
* **Atualizar listas de jogos**: Se você adicionou ou excluiu um jogo, precisa atualizar a lista de jogos para que as alterações tenham efeito.
* **Trocar botões de Validar/Cancelar**: inverta os botões validar e cancelar em seus controles.

### Configurações de áudio

Este menu oferece as seguintes opções:

* **Volume do sistema**: ajuste o volume de todo o sistema.
* **Modo de áudio**: permite que você escolha quais sons deseja ouvir no EmulationStation.
* **Saída de áudio**: selecione a saída de áudio
  * VC4-HDMI-0 - HDMI / DISPLAYPORT
  * Fones de ouvido - SAÍDA ANALÓGICA

Outras saídas de áudio podem ser listadas, cabe a você ver o que funciona para você.

### Configurações de rede

Este menu oferece as seguintes opções:

* **Status**: indica se você está conectado ou não.
* **Endereço IP**: permite que você obtenha o endereço IP do seu Recalbox.
* **Ativar wifi**: permite ativar ou desativar a wifi.
* **Nome da rede wifi (SSID)**: permite que você insira o nome da sua rede wi-fi.
* **Senha da wifi**: permite que você insira sua senha wifi.
* **Nome**: permite que você modifique o nome do seu Recalbox em sua rede local.

### Scraper

Para cada jogo, você pode obter informações (capa, resumo, etc.) que serão exibidas no menu de seleção de jogos do EmulationStation.

Pressione `START` e vá para `SCRAPER`. Então siga as instruções na tela. Você encontrará mais informações sobre o scraper interno [aqui](./../features/internal-scraper).

### Configurações avançadas

* **Overclock**: permite que você ajuste o overclock.
  * NENHUM
  * HIGH
  * TURBO
  * EXTREM
  * NOLIMIT

> As configurações TURBO e EXTREM podem causar danos irreversíveis ao Raspberry Pi se não forem feitas nas condições certas (dissipação de calor, ventilação, etc.). A garantia do seu equipamento pode ser anulada.
{.is-danger}

* **Configurações de inicialização**: permite definir certos parâmetros de inicialização.
* **Sistemas virtuais**: usado para exibir os sistemas virtuais na lista de sistemas.
* **Esconder jogos para adultos**: permite ocultar jogos marcados como jogos para adultos. Funciona assim que seus jogos forem classificados dessa forma pelo scraper.
* **Configuração avançada do emulador**: permite configurar as opções dos emuladores independentemente dos demais.
* **Configurações do Kodi**: Permite que você ative/desative o Kodi, inicie automaticamente e entre no Kodi ao iniciar o Recalbox ou ative o atalho para iniciar o Kodi a partir da lista de sistemas.
* **Segurança**: Usado para reforçar a segurança do Recalbox e para modificar a senha de root.
* **Overscan**: ajuda se você tiver faixas pretas ou se a imagem for maior que a tela.
* **Mostrar FPS**: exibe a frequência (quadros por segundo) no EmulationStation e nos jogos.
* **Gerenciador Recalbox**: permite ativar ou desativar o gerenciador da web do Recalbox.
* **Redefinir para as configurações de fábrica**: permite redefinir seu Recalbox para o mesmo estado como se fosse uma nova instalação. Isso não afeta seus dados pessoais de forma alguma.

### Verificação das BIOS

Este menu permite que você gerencie as BIOS exigidas por alguns dos emuladores Recalbox. Você pode obter mais informações [nesta página](./../features/bios-manager).

### Licença open-source

Este menu irá mostrar a você a licença para usar o Recalbox.

### Sair

Menu que permite **desligar o Recalbox.**

* Desligar Sistema
* Desligar Sistema Rapidamente
* Reiniciar Sistema

## Controles

#### Os comandos na interface:

* `A` → Seleciona
* `B` → Volta
* `Y` → Favorita um jogo (na lista de jogos), ou Inicia clipes de jogos (na lista do sistema)
* `X` → Inicia Kodi ou Netplay na lista de sistemas
* `START` → Abre o Menu do Emulationstation
* `SELECT` → Opções para sair da lista do sistema, mostrar os favoritos do sistema nas listas de jogos
* `R` → Próximo grupo de jogos em ordem alfabética nas listas de jogos, abre a função de pesquisa na lista do sistema
* `L` → Grupo anterior de jogos em ordem alfabética nas listas de jogos

## Favoritos

Você pode definir um jogo como favorito pressionando a tecla `Y` no jogo. O jogo será colocado no topo da lista com uma ☆ na frente do nome.

> O sistema deve ser desligado de forma segura usando o menu do EmulationStation para que a lista de jogos favoritos seja salva e você os encontre favoritados na próxima inicialização.
{.is-warning}