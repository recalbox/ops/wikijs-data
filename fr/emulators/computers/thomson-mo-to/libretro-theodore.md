---
title: Libretro Theodore
description: 
published: true
date: 2024-06-23T11:55:22.207Z
tags: libretro, mo5, to8, theodore
editor: markdown
dateCreated: 2021-05-21T08:18:19.713Z
---

**Cet ordinateur** est émulé par le **core Libretro** [theodore](https://github.com/Zlika/theodore).
**Ce core** est capable d'émuler **tous les modèles de la gamme MOTO** (MO5, MO6, TO7, TO7/70, TO8, TO8D, TO9, TO9+) ainsi que l'Olivetti PC128, un clone de MO6 pour le marché Italien.

## ![](/emulators/license.svg) Licence

Ce core est sous licence [**GPLv3**](https://github.com/Zlika/theodore/blob/master/LICENSE).

## ![](/emulators/compatibility.png) Compatibilité

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Fonctionnalités

| Fonctionnalité | Supporté |
| :---: | :---: |
| Redémarrage  | ✔ |
| Captures d'écran | ✔ |
| Sauvegardes | ✔ |
| Sauvegardes instantanées | ✔ |
| Rembobinage | ✔ |
| Netplay | ✔ |
| Options du core | ✔ |
| Contrôles | - |
| Remapping | - |
| Multi-Mouse | - |

## ![](/emulators/bios.svg) BIOS

>**Aucun bios n'est requis.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Extensions supportées

Les roms doivent avoir les extensions suivantes  :

* .fd (disquette)
* .k7 (cassette)
* .m5 (cartouche)
* .m7 (cartouche)
* .rom (cartouche)
* .sap (disquette)
* .zip
* .7z

Ce système supporte les roms compressées au format .zip/.7z. Attention toutefois, il ne s'agit que d'une archive.

Les fichiers contenus dans les .zip/.7z doivent correspondre aux extensions citées précédemment.
De plus, chaque fichier .zip/.7z ne doit contenir qu'une seule rom compressée.

### Emplacement

Placez les roms comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 thomson
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.zip**

>Les roms au format **TOSEC** sont vivement conseillées.
{.is-success}

>Pour plus d'information sur les roms, rendez-vous sur [ce tutoriel](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuration avancée de l'émulateur

>Pour pouvoir conserver vos configurations personnalisées lors d'une mise à jour, nous vous conseillons d'utiliser notre fonctionnalité [Surcharges de configuration](./../../../advanced-usage/configuration-override).
{.is-info}

### Accéder aux options

Vous pouvez configurer diverses options de deux façons différentes.

* Via le menu RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via le fichier `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Options du core

| Options | Valeur des options | Variable (pour surcharge) | Valeurs des variables |
| --- | --- | --- | --- |
| Thomson model | `Auto` ✅ / `TO8` / `TO8D` / `TO9` / `TO9+` / `MO5` / `MO6` / `PC128` / `TO7` / `TO7/70` | `theodore_rom` | `Auto` / `TO8` / `TO8D` / `TO9` / `TO9+` / `MO5` / `MO6` / `PC128` / `TO7` / `TO7/70` |
| Auto run game | `Activé` ✅ / `Désactivé` | `theodore_autorun` | `enabled` / `disabled` |
| Use game hash for autostart | `Activé` ✅ / `Désactivé` | `theodore_autostart_use_game_hash` | `enabled` / `disabled` |
| Display hint to start a game | `Activé` ✅ / `Désactivé` | `theodore_autostart_message_hint` | `enabled` / `disabled` |
| Virtual keyboard transparency | `0%` ✅ / `10%` / `20%` / `30%` / `40%` / `50%` / `60%` / `70%` / `80%` / `90%` | `theodore_vkb_transparency` | `0%` / `10%` / `20%` / `30%` / `40%` / `50%` / `60%` / `70%` / `80%` / `90%` |
| Floppy write protection | `Activé` ✅ / `Désactivé` | `theodore_floppy_write_protect` | `enabled` / `disabled` |
| Tape write protection | `Activé` ✅ / `Désactivé` | `theodore_tape_write_protect` | `enabled` / `disabled` |
| Dump printer data to file | `Désactivé` ✅ / `Activé` | `theodore_printer_emulation` | `disabled` / `enabled` |
| Interactive disassembler | `Désactivé` ✅ / `Activé` | `theodore_disassembler` | `disabled` / `enabled` |
| Break on illegal opcode | `Désactivé` ✅ / `Activé` | `theodore_break_illegal_opcode` | `disabled` / `enabled` |

## ![](/emulators/external-links.png) Liens externes

* **Code source utilisé** : [https://github.com/Zlika/theodore/](https://github.com/Zlika/theodore/)
* **Documentation Libretro** : [https://docs.libretro.com/library/theodore/](https://docs.libretro.com/library/theodore/)