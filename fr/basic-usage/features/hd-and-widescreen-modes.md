---
title: Modes HD et écran large
description: 
published: true
date: 2024-11-11T06:10:52.301Z
tags: écran, hd, large
editor: markdown
dateCreated: 2024-11-11T06:10:52.301Z
---

## Présentation

Les modes HD et écran large (widescreen) sont disponibles sur certains systèmes et vous permettent d'avoir une image d'une meilleure qualité ainsi que de l'agrandir sur toute la surface de votre écran.

>Le mode écran large est disponible sur les Raspberry Pi 4 / 400 / 5 et PC. Le mode HD est disponible sur les Raspberry Pi 5 et PC.
{.is-info}

## Configuration

Dans l'interface, faites `START` puis allez dans `OPTIONS DES JEUX`. En fonction de votre board, vous aurez l'une ou les 2 options.

## Notes concernant ces options

Ces options **modifient l'émulateur/core** utilisé ! Voici les cores utilisés en fonction de ces options :

| Système | Core utilisé | Ecran large | HD |
| :---: | :---: | :---: | :---: |
| Atomiswave | Libretro-Flycast | ✅ | ✅ |
| Dreamcast | Libretro-Flycast | ✅ | ✅ |
| Megadrive | Libretro-GenesisPlusGX-Wide | ✅ | ❌ |
| Naomi | Libretro-Flycast | ✅ | ✅ |
| Nintendo 64 | Libretro-Parallel_n64 | ❌ | ✅ |
| Playstation | Libretro-PCSX_reARMed | ❌ | ✅ |
| Saturn | Libretro-Yabasanshiro | ❌ | ✅ |
| Super Nintendo | Libretro-bsneshd | ✅ | ❌ |