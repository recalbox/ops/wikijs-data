---
title: License
description: This space contains explanations on legal aspects and other boring but important things
published: true
date: 2023-04-17T11:53:48.431Z
tags: license, recalbox
editor: markdown
dateCreated: 2021-08-06T07:45:31.554Z
---

Recalbox uses several packages under certain license types (GPL, BSD, MIT, etc.) and Recalbox itself uses a free license,

Here is the description of the license used for Recalbox:

>Redistribution and use of the RECALBOX code or any derivative works are permitted provided that the following conditions are met:
>
>Redistributions may not be sold without authorization, nor may they be used in a commercial product or activity.
>Redistributions that are modified from the original source must include the complete source code, including the source code for all components used by a binary built from the modified sources.
>Redistributions must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
>THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

>Although it is not a legal requirement under the license, we ask that anyone who wishes to create derivative works, modified versions, or forks of Recalbox not use code that has not yet reached the `master` branch.
{.is-info}