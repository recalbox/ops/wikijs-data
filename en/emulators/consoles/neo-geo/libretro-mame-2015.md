---
title: Libretro MAME 2015
description: 
published: true
date: 2021-10-12T00:34:56.053Z
tags: libretro, neo-geo, mame2015
editor: markdown
dateCreated: 2021-08-01T10:43:43.936Z
---



## ![](/emulators/license.svg) License

This core is under **MAME non-commercial** license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ❌ | ❌ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Features



## ![](/emulators/bios.svg) BIOS

### List of mandatory bios

| Filename | Description | MD5 | Provided |
| :---: | :---: | :---: | :---: |
| neogeo.zip | Neo-Geo BIOS | e023d0a4b5249fdff2a4620c28963944 8cbdff53661f41ddbe486a98915e1ec9 42e359e394f0be1d61b94ddca0efbe6c c1e0a738d266926f604186aa9d63e4db d12bba59121229ff13702878b415cb7c 2b4ffdf24d5d4f24c4b116c50c4bafc3 005b843b6e70b939c2cca41887cbc371 42e359e394f0be1d61b94ddca0efbe6c 4b9432a4571d03cc8bae9d24d8d6eb40 4cd64313e26537c04cdb561b8f84b0e4 50bbfd6bf297038b085e29265c65723a 76ee7390ccd537cddea1018025ca29a8 84d6c35aad790a77ba3cfc1b319e1061 912c0a56bafe0fba39d0c668b139b250 94748719c0ef31e7375b0c357d82fc89 ab25b9e25052a6064901a7a221037eb6 ae9e5825cde82c58e39dfd48534f7060 b18f83feef474b8a1306ef199cd810a2 b4519df27a352c2a6e42e06d31330d91 bf00272e7150b31156a8995d60bf185d ca66fd463ef7dcab492a6de8ce5f45eb f4a125538dfd7a8044e025fc5188fb88 a3b2c9a69c28ad0bd4ea07877b744bbe | ❌  |

### Location

Put the bios like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 neogeo
┃ ┃ ┃ ┃ ┣ 🗒 **neogeo.zip**

## ![](/emulators/roms.png) Roms

### Supported extensions

Roms must have the extension:

* .zip
* .7z

### Location

Put the roms like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 neogeo
┃ ┃ ┃ ┃ ┣ 🗒 **game.zip**

>For more information about roms, go to [this tutorial](./../../../tutorials/games/generalities/isos-and-roms)!
{.is-info}

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Accessing to options

You can configure various options in two different ways.

* Via RetroArch menu:

┣ 📁 RetroArch Menu
┃ ┣ 📁 Core options
┃ ┃ ┣ 🧩 Name_option

* Via the file `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core options

| Options | Options values | Variables (for configuration override) | Variables values |
| --- | --- | --- | --- |
| Read configuration | `Disabled` ✅ / `Enabled` | `mame2015_read_config` | `disabled` / `enabled` |
| Auto save/load states | `Disabled` ✅ / `Enabled` | `mame2015_auto_save` | `disabled` / `enabled` |
| Enable in-game mouse | `Disabled` ✅ / `Enabled` | `mame2015_mouse_enable` | `disabled` / `enabled` |
| Enable throttle | `Disabled` ✅ / `Enabled` | `mame2015_throttle` | `disabled` / `enabled` |
| Enable cheats | `Disabled` ✅ / `Enabled` | `mame2015_cheats_enable` | `disabled` / `enabled` |
| Hide nag screen | `Disabled` ✅ / `Enabled` | `mame2015_hide_nagscreen` | `disabled` / `enabled` |
| Hide gameinfo screen | `Disabled` ✅ / `Enabled` | `mame2015_hide_infoscreen` | `disabled` / `enabled` |
| Hide warnings screen | `Disabled` ✅ / `Enabled` | `mame2015_hide_warnings` | `disabled` / `enabled` |
| Alternate render method | `Disabled` ✅ / `Enabled` | `mame2015_alternate_renderer` | `disabled` / `enabled` |
| Boot to OSD | `Disabled` ✅ / `Enabled` | `mame2015_boot_to_osd` | `disabled` / `enabled` |
| Boot from CLI | `Disabled` ✅ / `Enabled` | `mame2015_boot_from_cli` | `disabled` / `enabled` |

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/libretro/mame2015-libretro/](https://github.com/libretro/mame2015-libretro/)