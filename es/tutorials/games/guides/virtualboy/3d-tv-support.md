---
title: Visualizar los juegos en 3D
description: 
published: true
date: 2024-02-04T16:58:53.209Z
tags: nintendo, virtual boy, 3d
editor: markdown
dateCreated: 2024-02-04T16:58:53.209Z
---

## Visualizar los juegos en 3D

### Introducción

La consola virtual boy permitía disfrutar de un efecto de profundidad mediante una visualización 3D à través de un sistema de lentes como las que se utilizan actualmente en los cascos de realidad virtual. Se puede obtener el mismo efecto que la consola original mediante el uso de dos técnicas que dependen del tipo de televisor disponible.

### Tele no compatible 3D

Si vuestra tele no soporta nativamente la 3D se pueden utilizar gafas anaglifo (las famosas gafas rojas y azules).
* Para ello, lanzad un juego virtual boy y entrad dentro del menu retroarch mediante la combinación Hotkey + B.

* Dirigíos a continuación dentro de las opciones del core

* Entrad dentro del menu "3D mode" y seleccionad "anaglyph"


Calzaros vuestras magníficas gafas y disfrutad de los juegos 3D.

### Tele compatible con la 3D

Si vuestro televisor es compatible con la tecnología 3D (con gafas activas o pasivas), es posible disfrutar de estas capacidades para visualizar la Virtual Boy en tres dimensiones.

* Para ello, lanzad un juego virtual boy y entrad dentro del menu retroarch mediante la combinación Hotkey + B.

* Dirigíos a continuación dentro de las opciones del core

* Entrad dentro del menu "3D mode" y seleccionad "side by side"

* A continuación volved hacia atrás hasta ver el menu "configuración" en la columna de la izquierda

* Dirigíos hacia el menu "vídeo", "escala"

* Entrad dentro de "aspect ratio" y seleccionad "personalizado"

Ponéos vuestras magnificas gafas, poned vuestro televisor en modo 3D y disfrutad de la belleza de los juegos Virtual Boy en 3D.
