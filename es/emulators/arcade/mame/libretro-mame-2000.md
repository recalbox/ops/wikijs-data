---
title: Libretro MAME 2000
description: 
published: true
date: 2024-06-29T17:28:54.282Z
tags: libretro, mame, mame2000
editor: markdown
dateCreated: 2023-09-05T12:03:37.990Z
---



## ![](/emulators/license.svg) Licencia

El core posee la licencia **MAME no-commercial**.

## ![](/emulators/compatibility.png) Compatibilidad

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ❌ | ❌ | ❌ | ❌ | ❌ | ❌ |


## ![](/emulators/features.png) Funcionalidades

| Funcionalidad | ¿Soportada? |
| :---: | :---: |
| Grabaciones | ✔ |
| Grabaciones instantáneas | Depende del juego |
| Rebobinado | ✔ |
| Netplay | ✔ |
| Cheats nativos | ✔ |
| Mandos | ✔ |
| Multi-Ratón | ✔ |

## ![](/emulators/bios.svg) BIOS

Las romsets de BIOS no son necesarias si utilizáis las romsets de arcade "Full Non-Merged". Para las romset de tipo "Split" y "Non-Merged", disponed las BIOS necesarias en la misma carpeta que la romset del juego.

>**Aviso** :
>Poned las bios dentro del directorio : `/recalbox/share/roms/mame/`   
>o dentro de una subcarpeta si utilizáis un directorio diferente por core Mame, por ejemplo: `/recalbox/share/roms/mame/Mame2000/`
Los juegos y sus bios se deben poner en la misma carpeta o subdirectorio.
{.is-warning}

## ![](/emulators/roms.png) Roms

* Basado en el romset: MAME 0.37b5 (Juillet 2000)
* Fichero dat : `/recalbox/share/arcade/libretro/mame2000.dat`.

### Emplazamiento

Disponed vuestras roms como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 mame
┃ ┃ ┃ ┃ ┣ 🗒 **juego.zip**
┃ ┃ ┃ ┃ ┣ 📁 **juego**
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **juego.chd**

También podéis utilizar el subdirectorio `mame2000` presente dentro de `mame`, para clasificar vuestros fullsets por el core que los hace funcionar. Si utilizáis este subdirectorio el emulador `mame2000` se lanzará automáticamente.

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 mame
┃ ┃ ┃ ┃ ┣ 📁 mame2000
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **juego.zip**
┃ ┃ ┃ ┃ ┃ ┣ 📁 **juego**
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 **juego.chd**

## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

>Para conservar las configuraciones personalizadas cuando se produce una actualización del sistema Recalbox, os aconsejamos utilizar las [Sobrecargas de configuración](./../../../advanced-usage/configuration-override).
{.is-info}

### Accéder a las opciones

Podéis configurar las opciones del core de dos formas diferentes.

* Via el Menú RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via el fichero `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Opciones del core

| Opción | Valor de la opción | Variable (para la sobrecarga) | Valores posibles |
| --- | --- | --- | --- |
| Frameskip | `Désactivé` ✅ / `auto` / `threshold` | `mame2000-frameskip` | `disabled` / `auto` / `threshold` |
| Frameskip Threshold (%) | `30` ✅ / `40` / `50` / `60` | `mame2000-frameskip_threshold` | `30` / `40` / `50` / `60` |
| Frameskip Interval | `1` ✅ / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` | `mame2000-frameskip_interval` | `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` |
| Skip Disclaimer | `Activé` ✅ / `Désactivé` | `mame2000-skip_disclaimer` | `enabled` / `disabled` |
| Show Game Information | `Désactivé` ✅ / `Activé` | `mame2000-show_gameinfo` | `disabled` / `enabled` |
| Audio Rate (Restart) | `11025` / `22050` ✅ / `32000` / `44100` | `mame2000-sample_rate` | `11025` / `22050` / `32000` / `44100` |
| Stereo (Restart) | `Activé` ✅ / `Désactivé` | `mame2000-stereo` | `enabled` / `disabled` |

## ![](/emulators/external-links.png) Enlaces externos

* **Progettosnaps** : [https://www.progettosnaps.net/](https://www.progettosnaps.net/)
* **Código fuente utilizado** : [https://github.com/libretro/mame2000-libretro/](https://github.com/libretro/mame2000-libretro/)