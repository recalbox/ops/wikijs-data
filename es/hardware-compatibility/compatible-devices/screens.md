---
title: Pantallas
description: 
published: true
date: 2023-09-19T08:31:52.450Z
tags: compatibilidad, pantallas
editor: markdown
dateCreated: 2023-09-19T08:31:52.450Z
---

### Pantallas TFT HDMI

| Periférico | Tamaño | Resolución | Estado | Comentarios |
| :---: | :---: | :---: | :---: | :---: |
|  | 5" | 800x480 | ✅ | [banggood](https://www.banggood.com/5-Inch-800-x-480-HDMI-TFT-LCD-Touch-Screen-For-Raspberry-PI-2-Model-B-B-A-B-p-1023438.html) |
| PI-TOP CEED | 14" | 1366×768 | ✅ | Funciona pero no hay sonido. Necesita "pt-speaker" [Fuente](https://www.neozone.org/tests/test-pi-top-ceed-lordi-ideal-pour-les-makers-en-herbe/) |

### Pantallas TFT SPI Bus

| Periférico | Tamaño | Resolución | Estado | Comentarios |
| :---: | :---: | :---: | :---: | :---: |
|  |  |  |  |  |

### Pantallas TFT DPI Bus

| Periférico | Tamaño | Resolución | Estado | Comentarios |
| :---: | :---: | :---: | :---: | :---: |
|  |  |  |  |  |

### Pantallas TFT con panel de control

| Periférico | Tamaño | Resolución | Estado | Comentarios |
| :---: | :---: | :---: | :---: | :---: |
| Assus N71Jq Laptop panel | 17.3" | 1600x900 | ✅ | Pantalla LCD reciclada Asus N71JQ conectada a un controlador de pantalla LCD (*HDMI VGA DVI Audio Panel de control de pantalla LCD para pantalla LCD LP173WD1 N173FGE B173RW01 15,6" 17,3" 1600x900 LED*) mediante un conector LVDS de 30 patillas. |