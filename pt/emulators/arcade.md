---
title: Arcade
description: 
published: true
date: 2021-11-28T14:03:25.782Z
tags: arcade
editor: markdown
dateCreated: 2021-09-21T23:59:31.944Z
---

![](/emulators/arcade/arcade-logo.png)

![](/emulators/arcade/arcade-bartop.svg)

## ATENÇÃO.

Aqui estamos entrando em uma parte um tanto técnica do Recalbox. **VAMOS SIMPLIFICAR.**

Tal como acontece com os consoles, o arcade no Recalbox é dividido em 2 elementos:
emuladores *(incluídos e atualizados em seu Recalbox)* e "jogos", que você deve adicionar.

...EXCETO QUE...
Ao contrário dos consoles, o arcade não inicia os "jogos", mas sim "sistemas completos"
*(= nós emulamos toda a máquina de arcade, não apenas os dados do jogo)*.

![](/advanced-usage/arcade/board-to-zip.png)

A emulação de arcade está em constante evolução, ainda hoje.
Os emuladores estão melhorando, assim como os "dumps" dos arcades.

Com cada nova versão do Recalbox, oferecemos a você as versões mais recentes dos emuladores. Tenha o cuidado de adicionar AS VERSÕES CERTAS dos sistemas de arcade *(denominado "ROMSET")*.

[**Mais informações sobre emulação de Arcade no Recalbox AQUI**](./../advanced-usage/arcade-in-recalbox)

##  ARCADE "PADRÃO" (Recalbox 8.0.0)

| Emulador | Romset |
| :---: | :---: |
| MAME | ROMSET MAME 0.78 |
| FBNEO | ROMSET FBNeo 1.0.0.03 |
| NEO-GEO  | ROMSET FBNeo 1.0.0.03 |
| NAOMI  | ROMSET MAME 0.235 |
| NAOMI GD  | ROMSET MAME 0.235 |
| ATOMISWAVE  | ROMSET MAME 0.235 |

✅ *Observação: nem todos os sistemas Arcade estão necessariamente disponíveis em seu hardware.*
*[**Verifique a compatibilidade do seu hardware AQUI.**](./../hardware-compatibility)*

✅ *Qual romset escolher em relação ao seu hardware?*
*[**Verifique a compatibilidade dos Romsets no Recalbox**](./../tutorials/games/generalities/isos-and-roms/romsets-for-recalbox)*

✅ *Para usuários avançados, é possível alterar o emulador MAME padrão, para tentar executar jogos mais novos. Você precisará então de um ROMSET  mais recente*
*[**TUTO para alterar o emulador padrão AQUI.**](./../tutorials/games/guides/nintendo-64/game-core-compatibility)*


## ARCADE "USUÁRIO AVANÇADO" (Recalbox 8.0.0)

*Emuladores MAME alternativos:*

| Emulador | Romset |
| :---: | :---: |
| LIBRETRO-MAME 2003-Plus | ROMSET MAME 0.78-0.188 |
| LIBRETRO-MAME 2010 | ROMSET MAME 0.139 |
| LIBRETRO-MAME 2015  | ROMSET MAME 0.160 |
| LIBRETRO-MAME  | ROMSET MAME 0.235 |

[**Mais informações sobre emulação avançada de Arcade AQUI**](./../advanced-usage/arcade-in-recalbox)

### PROBLEMAS FREQUENTES

| Problema | ROM RUIM | EMULADOR RUIM | CHD FALTANDO | BAD DUMP | MAPEAMENTO RUIM |
| :--- | :---: | :---: | :---: | :---: | :---: |
| **Meu jogo não inicia**  | ✅ | ✅ | ✅ | ✅ |  |
| **Problema de som no jogo** |  | ✅ |  | ✅ |  |
| **O jogo apresenta bugs, e glitchs**  |  | ✅ |  | ✅ |  |
| **O jogo inicia, mas os botões estão errados** |  |  |  |  | ✅ |

[**Mais informações sobre emulação avançada de Arcade AQUI**](./../advanced-usage/arcade-in-recalbox)