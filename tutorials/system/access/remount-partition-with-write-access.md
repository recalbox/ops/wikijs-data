---
title: Accéder à une partition en écriture
description: 
published: true
date: 2021-09-04T21:52:11.279Z
tags: partition, accès, écriture
editor: markdown
dateCreated: 2021-05-21T08:40:15.520Z
---

À certains moments, vous aurez besoin de modifier des fichiers qui peuvent être protégés en écriture.

Dans le système, il y a 2 endroits protégés en écriture :

* Partition de démarrage : `/boot`
* Partition du système : `/`

## Commande

Via [SSH](./../../../tutorials/system/access/root-access-terminal-cli), vous devrez utiliser la commande `mount`.

## {.tabset}
### Partition de démarrage

```shell
mount -o remount,rw /boot
```

### Partition du système :

```shell
mount -o remount,rw /
```