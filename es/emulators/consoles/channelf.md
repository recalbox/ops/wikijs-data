---
title: Channel F
description: 
published: true
date: 2024-07-26T07:46:33.183Z
tags: channelf, fairchild, consolas
editor: markdown
dateCreated: 2023-09-03T15:49:11.802Z
---

![](/emulators/consoles/channelf.svg){.align-center}

## Datos técnicos

* **Fabricante**: Fairchild Semiconductor
* **Año de lanzamiento :** 1976
* **Cantidades vendidas :** 250000
* **Procesador :** Fairchild F8 a 1,79 MHz (PAL 2,22 MHz)
* **RAM:** 64 bytes
* **VRAM:** 2 kB
* **Resolución**:128x64, 8 colores
* **Audio:** 500 Hz, 1 kHz y 1,5 kHz tonos (ajustable)
* **Soporte:** Cartucho ROM

## Presentación

La **Fairchild Channel F** es una consola de videojuegos producida por Fairchild Semiconductor. Salió a la venta en noviembre de 1976 en Norteamérica, a un precio de 169,95 dólares. La consola destaca por ser la primera del mundo basada en un sistema de cartuchos, así como la primera en incorporar un microprocesador.

En el momento de su lanzamiento, la consola se conocía como Video Entertainment System**, o **VES**, pero cuando Atari lanzó la VCS al año siguiente, Fairchild cambió el nombre de su consola. En 1977, la Fairchild Channel había vendido 250.000 unidades y seguía la estela de la VCS, que pasó a llamarse Atari 2600.

## Emuladores

[Libretro FBNeo](libretro-fbneo)
[Libretro FreeChaF](libretro-freechaf)