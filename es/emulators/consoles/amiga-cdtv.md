---
title: Amiga CDTV
description: 
published: true
date: 2024-07-26T07:44:01.175Z
tags: amiga-cdtv, amiga, commodore, cdtv, consolas
editor: markdown
dateCreated: 2023-09-03T15:38:49.655Z
---

![](/emulators/consoles/amigacdtv.svg){.align-center}

## Datos técnicos

* **Fabricante**: Commodore International
* **Año de lanzamiento**: 1991
* **Sistema operativo**: CDTV / AmigaOS 1.3
* **Procesador**: Motorola 68000 a 7,16 MHz (NTSC) o 7,09 (PAL)
* **RAM**: 1 MB (modificable)
* **ROM**: 256 KB de Kickstart 2.5 y 256 KB de firmware CDTV
* **Procesador gráfico**: Conjunto de chips mejorado de Commodore
* **Chip de sonido**: Cuatro canales PCM de 8 bits (dos canales estéreo)
* **Resolución** : 320 x 200 (32 colores) / 640 x 240 (16 colores)

## Presentación

El CDTV, por Commodore Dynamic Total Vision, comúnmente conocido como Amiga CDTV, es un ordenador personal y consola de juegos de tercera generación desarrollado por Commodore y comercializado en 1991. Con la forma de un tocadiscos negro, el sistema se basa en un ordenador Amiga 500 con un procesador Motorola 68000 a 7,16 MHz y un chipset gráfico ECS fat Agnus con 1 MB de RAM y una ROM de 512 kB que incorpora Kickstart 1.3. Viene con una unidad de CD-ROM 1x caddy, el soporte utilizado para el software. Su precio de lanzamiento fue de 1.000 dólares, con un mando de juegos y dos paquetes de software. Se presentó por primera vez en el Consumer Electronics Show de Las Vegas en 1991.

Entre los extras opcionales se incluían un teclado, un ratón, un mando a distancia, una disquetera y una tarjeta de módulo SCSI Commodore CDTV con un disco duro de 60 MB.

En la mente de Commodore, esta consola de juegos estaba dirigida a un público familiar que era novato en informática, interesado no sólo en los juegos, sino también en las capacidades multimedia y la interactividad. La enciclopedia Grolier, por ejemplo, era uno de sus productos estrella. La posibilidad de transformar la consola en un ordenador Amiga 500 era también una de las características interesantes del producto. Esta consola no tuvo un gran éxito y se destinó principalmente a los mercados estadounidense y alemán. Se construyó un prototipo de CDTV-II, denominado CDTV CR (Cost Reduced), pero se abandonó. Debería haber incorporado más memoria y una disquetera. Commodore lanzó más tarde el Amiga CD32 basado en la misma tecnología que el Amiga 1200, que podría describirse como el sucesor del CDTV.

## Emuladores

[Amiberry](amiberry)
[Libretro PUAE](libretro-puae)
[Libretro UAE4ARM](libretro-uae4arm)
