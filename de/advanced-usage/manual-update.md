---
title: Manuelles und/oder Offline-Update
description: 
published: true
date: 2021-06-21T14:07:09.468Z
tags: 
editor: markdown
dateCreated: 2021-06-21T14:07:09.468Z
---

## Wann sollte ein manuelles Update durchgeführt werden?

In manchen Fällen kann es notwendig sein, ein manuelles Update Ihrer Recalbox durchzuführen:
- Zum Aktualisieren einer Recalbox, die keine Internetverbindung hat.
- Wenn das Team dich bittet, ein *Entwicklungs*-Image zu testen, um einen Bugfix zu verifizieren.
- Wenn Du ein Systemproblem hast (sehr selten).

>Die folgenden Beispiele werden für das Raspberry Pi 4 Image angewandt. Du musst diese Beispiele also entsprechend an dein Board anpassen.
> {.is-info}

Um dies zu tun, benötigst du zwei Dateien:
- Die **Image-Datei** von Recalbox mit dem Namen `recalbox-rpi4.img.xz`.
- Die **Prüfsummendatei**, die die Prüfsumme des Abbilds enthält, so dass dessen Integrität automatisch überprüft werden kann. Sie hat den gleichen Namen wie das Image, aber mit der Erweiterung `.sha1`, also in unserem Beispiel `recalbox-rpi4.img.xz.sha1`.


## Lade das offizielle Image herunter
Um deine Recalbox auf die neueste Version im Offline-Modus zu aktualisieren, besuche [diese Seite](https://download.recalbox.com) und wähle "Alle Images anzeigen":

![seeallimages.png](/advanced-usage/manual-update/seeallimages.png)

Lade dann das Image, das deinem Board entspricht, und die entsprechende Prüfsumme `sha1` herunter:

![download.png](/advanced-usage/manual-update/download.png)

## Kopiere die Dateien auf die SD-Karte
Jetzt, da du die beiden Dateien `recalbox-rpi4.img.xz` und `recalbox-rpi4.img.xz.sha1` hast, musst du sie nur noch in das Verzeichnis `update` der Partition namens `RECALBOX` auf der SD-Karte, auf der Recalbox installiert ist, kopieren:

![images-in-boot-partition.png](/advanced-usage/manual-update/images-in-boot-partition.png)

## Starte die Installation
Die Installation startet automatisch, wenn du deine Recalbox das nächste Mal startest. Um zu überprüfen, ob die Installation erfolgreich war, kannst du das Menü mit dem Button `Start` anzeigen lassen und die Versionsänderung überprüfen.