---
title: FAQ
description: 
published: true
date: 2023-09-19T13:26:31.704Z
tags: faq
editor: markdown
dateCreated: 2023-09-19T13:26:31.704Z
---

## ¿Cuál es la versión del emulador BeebEm?

Hemos trabajado para transponer la última versión de BeebEm a SDL2 y la hemos mejorado para satisfacer las necesidades de Recalbox.

## ¿Qué sistemas soporta BeebEm?

Se admiten cuatro sistemas:

* BBC Micro
* B Plus
* Integra B
* Master128

Al colocar vuestros juegos en subdirectorios nombrados como en la tabla de abajo, los juegos en cada subdirectorio se ejecutarán mediante una plantilla diferente a la original (BBC Micro). Los directorios no distinguen entre mayúsculas y minúsculas:

| Modelo | Carpetas |
|--------|-------------|
|B+|`bbcbplus`, `bbc-bplus`, `modelbplus`, `model-bplus`, `bbcmodelbplus`, `bbc-model-bplus`|
|Integra B|`bbcmicrointegrab`, `bbcmicro-integra-b`, `integrab`, `integra-b`|
|Master 128|`bbcmaster`, `bbc-master`, `bbcmaster128`, `bbc-master-128`, `master128`, `master-128`|

Se aconseja, excepto en casos especiales, de limitarse a los modelos BBC Micro y Master 128 para los juegos más exigentes.

## ¿Existen atajos de teclado para BeebEm?

Sí, aquí está la lista:

| Combinación de botones | Acción |
| --- | --- |
| `Hotkey` + `Start` | Salir |
| `Hotkey` + `X` | Visualización de los LED del teclado y los periféricos (iconos en la parte inferior, casete, disquete, hdd, teclas especiales del teclado) |
| `Hotkey` + `Y` | Captura de pantalla en resolución nativa |
| `Hotkey` + `B` | Reinicio |
| `Hotkey` + `A` | Activación del suavizado |
| `Hotkey` + `L1` / `Hotkey` + `R1` | Ciclos de casete/disquete. A utilizar con los ficheros `.M3U` para cambiar casetes y disquetes. |
| `Hotkey` + `L2` | Pausa |
| `Hotkey` + `R2` | Muestra los FPS |
| `Hotkey` + `L3` | Cambio de pantalla (color, blanco y negro, verde, ámbar) |
| `Hotkey` + `R3` | Cambiar la protección del disquete (los ficheros de sólo lectura o zips siguen siendo de sólo lectura) |

## ¿Cuál es la asignación del teclado?

El teclado es QWERTY.

## ¿Funcionan las sobrecargas de configuración?

Sí, sólo en 2 niveles: `<game>.<ext>.xxx.cfg` y `.xxx.cfg`.

## ¿Funcionan los periféricos originales?

Sí, como el joystick y el ratón. El joystick está modelado a partir del stick izquierdo y las flechas de dirección (d-pad), así como los botones A y B.

Las tarjetas de expansión de tubo también son compatibles, a través de las opciones del núcleo o por anulación de la configuración.

## ¿Se pueden cargar discos duros IDE o SCSI?

No, de momento no es posible.

## ¿Hay periféricos o partes del sistema no disponibles?

Sí, como teletexto, soporte econet, depurador, desensamblador y puertos serie.

## ¿Sólo son compatibles algunas controladoras de disquete?

Si, solo estas controladoras son soportadas:

* acorn1770
* opusddos
* watford

Estas controladoras deben activarse en las opciones del core.

Para el modelo B Plus, el controlador de disquetes `acorn1770` está activado por defecto.

## ¿Son compatibles los ficheros en formato `.ZIP`?

Sí, sólo para disquetes. Los formatos de casete (.uef y .csw) ya están comprimidos, por lo que utilizar zip en ellos hace perder espacio.