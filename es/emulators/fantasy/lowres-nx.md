---
title: LowRes NX
description: 
published: true
date: 2024-07-26T08:22:00.518Z
tags: fantasy, lowres-nx, 8.0+, consolas
editor: markdown
dateCreated: 2023-09-04T06:57:37.214Z
---

![](/emulators/fantasy/lowresnx.svg){.align-center}

## Datos técnicos

* **Desarrollador**: Timo Kloss
* **Año de lanzamiento**: 2017
* **Pantalla** 160x128 píxeles, 60 Hz, 8 paletas dinámicas de 6 bits con 4 colores cada una.
* **Píxeles**: 64, máximo 32x32 píxeles
* **Sonido**: 4 voces, sierra/tri/pulso/ruido, ancho de pulso, volumen, ADSR, LFO

## Presentación

LowRes NX se inspira en los sistemas reales de 8 y 16 bits y simula chips para gráficos, sonido y E/S que funcionan realmente como el hardware real. Admite sprites por hardware, así como desplazamiento de paralaje por hardware, e incluso cuenta con blanco vertical y saltos de fotograma para crear auténticos efectos retro.

## Emuladores

[Libretro LowResNX](libretro-lowresnx)