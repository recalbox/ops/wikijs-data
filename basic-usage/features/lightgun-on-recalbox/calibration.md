---
title: La "Calibration"
description: Comment faire en sorte que les jeux fonctionnent de manière plus optimale ?!
published: true
date: 2021-09-04T17:53:41.561Z
tags: lightgun, calibration, 7.2+
editor: markdown
dateCreated: 2021-05-21T08:12:10.972Z
---

## Introduction

Souvent, dans certains jeux, surtout d'arcade, il sera nécessaire de configurer le jeu avant d'y jouer. Dans le cas de l'arcade, il faudra juste le configurer une première fois et cela restera dans vos NVRAM ou autre .CFG que vous retrouverez par système et/ou émulateur dans votre répertoire "saves" de votre share.

![](/basic-usage/features/lightgun/calibration/calibration1.png)

Mais pour configurer il faudra parfois aller dans la configuration du core ou même du jeu.