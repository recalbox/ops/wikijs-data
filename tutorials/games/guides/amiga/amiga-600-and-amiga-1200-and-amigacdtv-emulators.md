---
title: Emulateur Amiga 600 & 1200 & AmigaCD32
description: 
published: true
date: 2021-09-04T23:16:18.017Z
tags: amiga-cdtv, amiga-1200, amiga-600, amiga
editor: markdown
dateCreated: 2021-05-21T08:38:09.909Z
---

## Sous-systèmes

Dans Recalbox, les machines Amiga ont été divisées en 3 sous-systèmes virtuels :  


* Amiga 600 : Toutes les machines _OCS/ECS_ de l'A1000 jusqu'à la dernière A600
* Amiga 120à : Toutes les machines _AGA_, à partir de la 1200
* Amiga CD32 : La seule vraie console de la série Amiga     
  * Le CDTV n'est pas encore pris en charge, bien que les configurations du thème Recalbox et du CDTV soient prêtes. Il sera ajouté plus tard comme nouveau sous-système, dès qu'Amiberry le supportera complètement.

## « Roms » supportées

Voici une liste des formats de fichiers supportés, classé par sous-système :

* Amiga600 & Amiga1200 :

  * Disques **ADF** (`*.adf`), le format de disque le plus populaire (ne peut pas intégrer de protections de disque). Peut être zippé ou 7-zippé.
  * Disques **IPF** (`*.ipf`), le plus utilisé par les dumps sans intro (peut intégrer des protections de disque). Peuvent être zippés.
  * **WHD** (dossiers, `*.lha/lzh/lz`, `*.zip`), le format bien connu pour les jeux sur disque dur. Les dossiers WHD sont reconfigurés en utilisant le fichier `*.slave` interne ; cependant il est fortement recommandé de les stocker au format `lha` ou `zip`. Les WHD décompressés ne sont pas plus rapides et même pire, ils peuvent rendre EmulationStation extrêmement lent).
  * **HDD FS**, disque dur sur système de fichiers. Les dossiers doivent se terminer par des extensions de type `.hd`. Rarement utilisé. Pour les vrais amateurs d'Amiga qui ont sauvegardé leur Amiga HD. Peut être zippé, mais pas recommandé.
  * **HDF** (`*.hdf`), Images de disque dur dans un seul fichier. Peut être zippé (lecture/écriture).
  * **RP9** (`*.rp9`), Packs « tout-en-un » Amiga Forever.

* **Amiga CD32** :

  * **ISO** (`.bin`, `*.iso`, avec ou sans `*.cue`), CD dumps. Peut être zippé.

D'autres formats d'images de CD peuvent être pris en charge mais n'ont pas été testés par l'équipe Recalbox.

Lors de l'exécution de fichiers zippés/7-zippés, notre configurateur essaie d'identifier le type de rom en recherchant des extensions spécifiques à l'intérieur de l'archive. 

* `ADF/IPF/BIN/ISO` sont rapides à identifier.
* `WHD` sont rapides, mais `LHA` doit être préféré.
* `HDDFS` peut être plus long à vérifier et peut conduire à des interprétations erronées. Si vous utilisez `HDDFS`, laissez-les comme des fichiers et des dossiers normaux et créez le nom du dossier racine se terminant par `.hd` pour une identification rapide.

## Configurations prioritaires

Comme vous pouvez le voir, ce nouvel Amiberry supporte tous les formats de roms d'Amiga. Pour rendre les choses plus faciles pour tout le monde, nous avons fait un énorme travail pour configurer automatiquement l'émulateur en fonction de ce que vous voulez démarrer.

Recalbox construit une configuration de machine par défaut par sous-système. Cependant, dans certaines situations, vous pouvez vouloir outrepasser les paramètres générés automatiquement.

Recalbox génère les fichiers de configuration dans `/tmp/amiga/conf`. Le plus important est le fichier `uaeconfig.uae`, dans lequel vous trouverez toutes les clés définissant la machine, les disques durs, etc.

Je ne peux pas décrire toutes les clés ici. Si vous êtes curieux, je vous recommande de jeter un coup d'oeil aux documentations Amiberry et UAE appropriées.

Vous avez deux façons d'écraser les paramètres :

### Par système :

Créer un fichier nommé `/recalbox/share/system/configs/amiberry/<subsystem>.uae` (`/recalbox/share/system/configs/amiberry/amiga1200.uae` pour outrepasser l’Amiga1200)

* Réglez la clé que vous voulez contourner.
* Par exemple, vous pouvez créer un fichier de configuration avec le contenu suivant pour désactiver l'affichage LED dans le coin inférieur droit :

```ini
show_leds=false
```

### Par jeu

Créez un fichier nommé exactement comme votre "rom" (ou dossier) mais qui se termine par `.uae`.

Par exemple, dans votre dossier rom, si vous avez un fichier nommé `Aladdin (AGA).ipf`, vous pouvez créer un `Aladdin (AGA).uae` et remplacer les clés dans ce fichier.

Recalbox génère la configuration dans l'ordre suivant :

* Créer la configuration par défaut concernant le type de rom et le sous-système.
* Chargez `/recalbox/share/configs/<subsystem>.uae` si le fichier existe, et ajoutez/écrasez les clés de configuration.
* Chargez `<game>.uae` si le fichier existe, et ajoutez/écrasez les clés de configuration.

## Dans le jeu

Comme dans les autres émulateurs :

* `Hotkey + START` permettent de quitter l'émulateur, 
* `Hotkey + B` lance l'interface Amiberry.