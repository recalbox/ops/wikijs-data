---
title: Für NAOMI Spiele
description: 
published: true
date: 2024-06-02T20:26:35.839Z
tags: lightgun, naomi, 7.2+, kalibrierung
editor: markdown
dateCreated: 2022-05-19T19:03:43.944Z
---

## Beispiel für die Kalibrierung in einem Naomi Spiel

1) Aktiviere den Naomi-Service-Modus in RetroArch (`Hotkey` + `B`).

2) Richte einen Controller für Spieler 1 ein.

3) Kehre zum Spiel zurück (`Hotkey` + `B`) und starte das TEST MENU mit der `L3` Taste.

4) Wähle das Menü mit `R3` und bestätige es mit `L3`.

5) Sobald Du in der Kalibrierung bist oder kurz davor, gehe zurück zu RetroArch (`Hotkey` + `B`) und lege eine "Lightgun" für Spieler 1 an.

6) Wenn die Kalibrierung beendet ist, musst Du eventuell Schritt 2 erneut ausführen, um das TEST MENU zu verlassen.

7) Wenn Du zum Spiel zurückkehrst, gehst Du zurück zu RetroArch (`Hotkey` + `B`), um die Lightgun auf Spieler 1 zu legen.

8) Jetzt kann das Ergebnis der Kalibrierung getestet werden

## Beispiel für die Kalibrierung in einem Naomi-Spiel (bspw. *The House of the Dead 2*)

Für diese Art von Spiel (bspw. *The House of Dead 2* auf Naomi), musst Du das Menü "**NAOMI TEST MODE**" aufrufen, was aber leider nicht so einfach ist...

**Voraussetzung:** Du brauchst einen Controller

1) Du musst das RetroArch Menü (`Hotkey` + `B`)  aufrufen

![](/basic-usage/features/lightgun/calibration/naomi1-en.png){.full-width}

2) Gehe dann zu "Optionen".

![](/basic-usage/features/lightgun/calibration/naomi2-en.png){.full-width}

3) Du musst überprüfen, ob die Einstellung " Allow NAOMI Service Buttons" aktiviert ist

![](/basic-usage/features/lightgun/calibration/naomi3-en.png){.full-width}

4) Gehe dazu zu Hauptmenü > Einstellungen > Eingabe

![](/basic-usage/features/lightgun/calibration/naomi4-en.png){.full-width}

5) Gehe zu "Port 1 Controls".

![](/basic-usage/features/lightgun/calibration/naomi5-en.png){.full-width}

6) Um den "Controller" auszuwählen, benötigst Du einen Controller und nicht die Lightgun, um das Menü zu aktivieren

7) Um den Vorgang abzuschließen, drücke `Hotkey` + `B`, um das Menü zu verlassen und ins Spiel zurückzukehren.

**NAOMI TEST MODE**: Dieser kann jetzt mit `L3` auf deinem Controller aufgerufen werden.

![](/basic-usage/features/lightgun/calibration/naomi6.png){.full-width}

8) Wähle das Menü "**GAME TEST MODE**" mit `R3` und bestätige den Menüeintrag mit `L3`.

![Menu selection](/basic-usage/features/lightgun/calibration/naomi7.png){.full-width}

![After validation, we arrive in the "GAME TEST MODE" menu](/basic-usage/features/lightgun/calibration/naomi8.png){.full-width}

9) Nun kann mit `R3` das Menü "**GUN SETTING**" angewählt und mit `L3` bestätigt werde.

![Menu selection](/basic-usage/features/lightgun/calibration/naomi9.png){.full-width}

![After validation, we arrive in the menu "GAME TEST MODE"](/basic-usage/features/lightgun/calibration/naomi10.png){.full-width}

10) Nun können wir mit `R3` das Menü "**PLAYER1 GUN ADJUSTMENT**" auswählen und mit `L3` den Menüeintrag bestätigen.

![Menu selection](/basic-usage/features/lightgun/calibration/naomi11.png){.full-width}

![](/basic-usage/features/lightgun/calibration/naomi12.png){.full-width}

11) **Hinweis**: Du musst zurück zu RetroArch (`Hotkey` + `B`) und dann zum Hauptmenü → Einstellungen → Inputs → Port 1 Controls gehen, um die "Light Gun" (anstelle von "Controller") wieder zu aktivieren, um sie zu kalibrieren.

Jetzt kannst Du die Kalibrierung vornehmen, indem Du zuerst **auf das blaue Fadenkreuz oben links und dann unten rechts** schießt.

![](/basic-usage/features/lightgun/calibration/naomi13.png){.full-width}
![After selecting the 2 corners, the calibration calculation is done...](/basic-usage/features/lightgun/calibration/naomi14.png){.full-width}

12) Du kannst das Ergebnis sehen und den OFF-SCREEN testen (der in diesem Spiel unten links ist).

![](/basic-usage/features/lightgun/calibration/naomi15.png){.full-width}

![](/basic-usage/features/lightgun/calibration/naomi16.png){.full-width}

13) **Hinweis**: Du musst zurück zu RetroArch gehen (`Hotkey` + `B`) und dann zum Hauptmenü → Einstellungen → Inputs → Port 1 Controls, um den "Controller" (anstelle der "Light Gun") wieder zu aktivieren, um das Spiel beenden zu können.

Um die Konfiguration zu bestätigen, drücke die Taste **R3** auf deinem Controller und kehre zum vorherigen Menü zurück.

14) Du kannst wieder bei Schritt 3 beginnen, aber wähle das Menü "**PLAYER2 GUN ADJUSTMENT**", wenn Du ein Setup für zwei Spieler hast.

15) Wähle abschließend EXIT mit `L3`/`R3` in jedem Menü, um das Spiel zu verlassen und zum Spiel zurückzukehren.

![Exit the "GUN SETTING" menu](/basic-usage/features/lightgun/calibration/naomi17.png){.full-width}

![Exit "GAME TEST" menu](/basic-usage/features/lightgun/calibration/naomi18.png){.full-width}

![Exit "NAOMI TEST MODE" menu](/basic-usage/features/lightgun/calibration/naomi19.png){.full-width}

![The game system restarts](/basic-usage/features/lightgun/calibration/naomi20.png){.full-width}

16) Und schließlich empfehlen wir, vor dem Spielen das Spiel neu zu starten, damit alle Einstellungen von RetroArch auf die Standardkonfiguration zurückgesetzt werden.

![](/basic-usage/features/lightgun/calibration/naomi21.png){.full-width}

>Achte auf deine Dateien, um nicht alle Einstellungen erneut vornehmen zu müssen, sondern verwende einfach deine Backups:
>
>![](/basic-usage/features/lightgun/calibration/calibration2.png){.full-width}
{.is-warning}