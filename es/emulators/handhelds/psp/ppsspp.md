---
title: PPSSPP
description: 
published: true
date: 2024-08-08T09:43:05.085Z
tags: psp, ppsspp
editor: markdown
dateCreated: 2023-09-11T13:59:11.552Z
---

**PPSSPP** es un emulador gratuito para la consola portátil PSP desarrollado en C++.  
Está disponible en muchas plataformas, incluyendo Android, Windows, iOS y GNU/Linux.

Está escrito por **Henrik Hrydgard**.

## Requisitos

* OpenGL / Open GL ES 2.0 o superior para el renderizador OpenGL.
* Vulkan para el renderizador Vulkan.
* Direct3D 11 para el renderizador Direct3D 11.

## ![](/emulators/license.svg) Licencia

El core posee la siguiente licencia [**GPLv2**](https://github.com/hrydgard/ppsspp/blob/master/LICENSE.TXT).

## ![](/emulators/compatibility.png) Compatibilidad

 | RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | 
 | ❌ | ✅ 🐌 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Funcionalidades

| Funcionalidad | ¿Soportada? |
| :---: | :---: |
| Reinicio | ✔ |
| Grabaciones | ✔ |
| Grabaciones instantáneas | ✔ |
| Opciones del core | ✔ |
| Cheats natifs | ✔ |
| Controles | ✔ |
| Remapping | ✔ |
| Usuario | ✔ |
| Idioma | ✔ |

## ![](/emulators/bios.svg) BIOS

>**No necesita bios.**
{.is-success}

## ![](/emulators/roms.png) Isos

### Extensiones de roms soportadas

Las roms deben tener las extensiones siguientes:

* .cso
* .elf
* .iso
* .pbp
* .prx

### Emplazamiento

Disponed vuestras isos como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 psp
┃ ┃ ┃ ┃ ┣ 🗒 **juego.iso**

>Se aconsejan las isos del proyecto **Redump**.
{.is-success}

>Para más información sobre las roms, consultad [este tutorial](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuration avancée de l'émulateur

>Pour pouvoir conserver vos configurations personnalisées lors d'une mise à jour, nous vous conseillons d'utiliser notre fonctionnalité [Surcharges de configuration](./../../../advanced-usage/configuration-override).
{.is-info}

## ![](/emulators/external-links.png) Enlaces externos

* **Código fuente utilizado** : [https://github.com/hrydgard/ppsspp/](https://github.com/hrydgard/ppsspp/)
* **Documentación Libretro** : [https://docs.libretro.com/library/ppsspp/](https://docs.libretro.com/library/ppsspp/)
* **Página web oficial** : [http://www.ppsspp.org/](http://www.ppsspp.org/)
* **Foro oficial** : [https://forums.ppsspp.org/](https://forums.ppsspp.org/)
* **Documentación oficial** : [http://www.ppsspp.org/guides.html](http://www.ppsspp.org/guides.html)