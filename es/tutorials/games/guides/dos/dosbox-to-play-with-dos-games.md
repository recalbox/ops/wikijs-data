---
title: Emulador DOSbox para los juegos MS-DOS
description: 
published: true
date: 2023-08-20T00:14:35.266Z
tags: dos, msdos, dosbox
editor: markdown
dateCreated: 2023-08-20T00:14:35.266Z
---

## Funcionamiento básico

Tomaremos como ejemplo el juego Alley Cat:

* Primero hay que crear la carpeta que contendra el juego y que se debe llamar `AlleyCat.pc`. La primera parte del nombre antes del punto debe tener como máximo 8 caracteres y sin caracteres especiales
* Copiad dentro los ficheros de vuestro juego (para  los juegos gordos, instalad-los primero en un windows antes de copiarlos)
* Dentro de la carpeta `AlleyCat.pc`, cread un fichero `dosbox.bat` y modificad su contenido como sigue para ejecutar el juego.

El contenido es:

```dos
c: 
CAT.EXE
```

* DOSBox sitúa el disco virtual C: sobre la carpeta de vuestro juego (es decir que C: aquí equivale a `/recalbox/share/roms/dos/AlleyCat.pc`). Ocurre lo mismo con el directorio actual '.'
* Reiniciad o refrescad la lista de juegos para ver aparecer el juego.
* Para salir del emulador, utilizad `CTRL + F9` en vuestro teclado.

## Funcionalidades avanzadas

Para definir una configuración específica para un juego, se puede añadir un fichero `dosbox.cfg` dentro de vuestra carpeta al mismo nivel que `dosbox.bat`.

Podéis o bien copiar el fichero `dosbox.conf` desde `\recalbox\share\system\configs\dosbox\dosbox.conf` o bien utilizar [este](https://pastebin.com/13xrJdkw).

>La extensión del fichero debe ser `cfg` y no `conf`!
{.is-warning}

Dentro de este fichero, la línea `mapperfile=mapper.map` os permite utilizar otro fichero `mapper.map` para asociar las teclas del teclado e incluso el ratón a vuestro mando de juegos. Este fichero se puede crear directamente desde vuestro juego utlizando `CTRL + F1` en cualquier momento y se grabará al lado de vuestros ficheros `dosbox.cfg` y `dosbox.bat`.

Algunos de los parámetros de configuración del fichero `dosbox.cfg` se pueden añadir igualmente al comienzo de vuestro fichero `dosbox.bat` (si no queréis utilizar el `dosbox.cfg`) pero algunos de estos paramétros no funcionan dentro del `dosbox.bat`; todo lo que hace referencia a la parte gráfica parece funcionar bien, pero por ejemplo el parámetro `mapperfile` no funciona.

## Convertir un juego para utilizarlo en Recalbox

¿Qué hay que hacer par convertir un juego para que sea utilizable en Recalbox?

En primer lugar, copiad el contenido de la carpeta del susodicho juego (renombrandolo correctamente con la extensión .pc)

A continuación debemos adaptar el contenido del `dosbox.cfg` y del fichero bat que se utiliza par lanzar el juego.

Ejemplo con el juego Wacky Wheels:

No copiéis directamente el fichero `dosbox.cfg` (o .conf) de la carpeta original del juego porque éste puede producir errores difíciles de identificar. Copiad más bien el fichero standard describo más arriba. Después, podéis comparar con el fichero original para detectar configuraciones específicas y en este caso, copiarlas dentro de vuestro `dosbox.cfg`, concretamente dentro de la sección `[autoexec]`.

En concreto, desplazad el contenido de `[autoexec]` al comienzo de vuestro fichero `dosbox.bat` y ajustad las rutas. En nuestro ejemplo tenemos:

```bat
[autoexec] 
cd .. 
cd .. 
mount c .\games\WackyWhe  
imgmount d .\games\WackyWhe\cd\wackywheels.iso -t cdrom 
c: 
cd wacky
cls 
@ww
exit 
```

Una vez convertido para ser utilizado por Recalbox, obtenemos:

```dos
imgmount d .\cd\wackyw~1.iso -t cdrom
c: 
cd WACKY
pause
WW.EXE
```

###  Explicaciones:

* C: ya se encuetra montado por DOSBox (en la carpeta del juego), no necesitamos esto
* '.' está igualmente colocado por DOSBox en la misma carpeta que anteriormente y si se utilizan rutas relativas, vuestro juego podrá ser utilizado en un directorio diferente o en otra distribución
* exit ha sido suprimido porque DOSBox lo gestiona automáticamente.
* La ruta utilizada por `imgmount` se simplifica teniendo cuidado de utilizar como nombre de la imagen un nombre DOS standard (com máximo 8 caracteres donde los dos últimos se transforman en ~1, ~2, ~3 etc en caso de superar éste límite (o cuando tenéis varios fichero que se llaman igual). En estos casos, se aconseja renombrar la imagen y utilizar un nombre más sencillo.
* Los nombres largos tampoco están soportados en los ficheros de tipo `.cue`, así que es posible que tengáis que renombrar los ficheros `.vue`/`.bin` a la mano y modificar el contenido del fichero `.cue` para que haga referencia a los nuevos nombres de ficheros `.bin`. DOSBox es más bien estricto con estas cosas: nada de caracteres especiales, y a veces ni siquiera mayúsculas.
* El comando pause permite congelar la pantalla y debugar fácilmente las instrucciones DOS, lo podéis eliminar después de comprobar que el comando `imgmount` funciona bien (atención a no utilizar el comando pause despues del lanzamiento del ejecutable del juego o no veréis nada)
* `@ww` se convierte en `WW.EXE` (se trata simplemente de llamar al fichero ejecutable del juego, que en este caso se llama así).

Y ya está todo! Algunos juegos utilizan también un ejecutable con extensión .bat en vez de .exe. En este caso, copiad sus instrucciones al final de vuestro `dosbox.bat` a continuación de las instrucciones de la sección `[autoexec]` de `dosbox.cfg` y no olvidéis de adaptarlas si necesario.

## ¿Cómo emular los juegos DOS en Recalbox?


Este tutorial tiene como objetivo ayudaros a jugar a vuestros juegos DOS vía el emulador DOSBox preinstalado por Recalbox.

>No hay que olvidar que existen una gran multitud de contextos diferentes y posibles, cada juego tien su particularidad.
>Es imposible a día de hoy de crear una configuración "Plug&Play" en la emulación DOS que se pueda aplicar a todos los juegos existentes.
>No obstante, sin entrar en detalles y para la mayoría de los juegos el funcionamiento es siempre el mismo.
{.is-info}

En general podemos tener 3 casos diferentes: 1. el juego no necesita ni instalción ni soporte CD; 2. el juego necesita ser instalado pero no necesita CD; 3. El juego necesita una instalación y un soporte CD.

Antes de entrar a describir estos casos, es conveniente describir en grandes rasgos cómo funciona el emulador DOSBox disponible en Recalbox.

## Las grandes líneas

* Vuestros ficheros y carpetas de juego se deben depositar dentro de la carpeta "dos" de la carpeta "roms": DOSBox no funciona con ficheros comprimidos así que si utilizáis un juego en formato .zip debéis descomprimirlo primero.
* Las carpetas de los jeugos dentro de la carpeta "dos" deben cumplir dos reglas muy importantes:

	* La carpeta debe finalizar por ".pc", esta extensión permite que EmulationStation identifique el jeugo par poder mostrarlo en la interfaz.
  * Evitad si es posible la utilización de caracteres especiales, espacios, acentos, etc en los nombres para simplificar el acceso.

* La carpeta del juego (la que termina por ".pc") se monta como unidad "C:" cuando se lanza DOSBox. Debéis tener en la cabeza que vuestro contexto se limita automáticamente a la carpeta del juego que lanzáis, todas las otras rutas no existiran para DOSBox.
* Dentro de la carpeta del juego (aquella con el sufijo ".pc") se debe depositar un fichero "dosbox.bat"; este fichero permite ejecutar dentro del emulador los comandos e instrucciones necesarias para instalar/configurar el juego y para ejecutarlo. Generalmente hay que editar el fichero "dosbox.bat" varias veces y ejecutarlo después de cada modificación para completar las etapas necesarias a su funcionamiento, pero una vez hecho esto y que el juego funciona correctamente, no se toca más. Un último punto relacionado con los ficheros "dosbox.bat": podéis editarlos con cualquier editor de ficheros de texto (como notepad en Windows)
* Los ficheros importantes en el juego son de forma general aquellos con extensión ".bat" en primer lugar y en segundo lugar los ".exe".
* Un teclado y un ratón son indispensables (en este tutorial suponemos que estáis correctamente équipados)

**Los diferentes casos posibles**

A continuación vamos a describir los diferentes tipos de juegos que os podéis encontrar. Os recomendamos de leer todos los posibles casos porque ciertos puntos descritos en un caso pueden ser útiles en los otros.

**El juego no necesita ni instalación ni soporte CD:**

Este es el caso más sencillo: el juego no posee ningún fichero llamado "install.bat" (o ".exe") o cuando lo posee, no se trata de una verdadera instalación sino más bien una configuración como por ejemplo el sonido.
  
Idealmente, deberíais tener dos ficheros importantes dentro de la carpeta del juego (con extensión ".bat" o ".ext"): "setup" (a veces el fichero "install" sirve como "setup") y otro fichero que lleva el nombre del juego (por ejemplo "wolf3d" por "Wolfenstein 3D" o bien "s2" por "settlers2" o bien simplemente "go" que significa "lanzar" en inglés).

Si no existe ningún fichero "dosbox.bat" hay que crearlo y editarlo con las siguientes líneas:

```bat
setup
pause
```

La instrucción `pause` sirve para que DOSBox se pare antes de volver a EmulationStation. Esto permite leer los posibles errores del fichero "dosbox.bat".
  
Una vez creado el fichero "dosbox.bat", volved a EmulationStation, actualizad la lista de juegos y lanzadlo!

Entraréis en el programa de configuración del juego. Suele ser interesante para configurar el sonido. Cada "setup" es diferente pero en regla general veréis aparecer un menú en inglés con instrucciones de tipo "setup sound...".
Debéis seleccionar (para el "midi" Y para el "sound effect" en caso de que el setup haga la diferencia) la opción "Sound Blaster 100% compatible" junto con su configuración por defecto que os puedan proponer.
  
A veces el setup os propone un test para vérificar que el sonido es correcto.
Cuando hallais terminado con la parte del sonido, quitad el setup (suele existir una opción para ello. En caso de que el setup os propoga de grabar la configuración, no dudéis en aceptar).
A continuación se os monstrará un "prompt" donde se os pedirá de apretar una tecla para continuar. Se trata de la instrucción "pause". Corregid los posibles errores si existen y continuad.

Una vez de vuelta a EmulationStation, editad el fichero "dosbox.bat" como se indica aquí abajo (en este ejemplo hemos utilizado el fichero ejecutable "s2.bat" por "Settlers 2", en vuestro caso adaptad el ejemplo renombrando "s2" por el nombre de vuestro fichero ".bat" ou ".exe" de vuestro juego):

```bat
s2
pause
```

Grabad el contenido y lanzad el juego otra vez. Normamente todo funciona ;-)
Si no es así, no dudéis en contactarnos a travès del foro o de nuestro Discord, precisando los mensajes de error que habéis recibido.

## El juego necesita una instalación, pero no tiene soporte de CD

Se siguen los mismos pasos que en el apartado anterior, pero con una etapa nueva de instalación.
Si habéis intentado seguir los pasos del apartado anterior pero no ha funcionado, es posible que vuestro juego necesite ser instalado.
Para evitar mezclar los ficheros, se aconseja crear una subcarpeta "source" dentro de la carpeta del juego ".pc". Una vez hecho esto desplazad todos los ficheros del juego dentro de la carpeta "source".
Aseguráos que existe un fichero "install" (".bat" o ".exe" - a veces el fichero "setup" propone igualmente hacer una instalación).
A continuación cread/editad el fichero "dosbox.bat" como sigue:

```bat
cd source
install
pause
```

Lanzad otra vez el juego desde EmulationStation, en este caso la ejecución del fichero "dosbox.bat" os lleva directamente al programa de instalación del juego.
En ingles para la mayoria de los juegos, tendréis acceso a un menu de instalación que depende, como en el caso del "setup", de cada juego. No hay nada universal, así que hay que adaptarse. Normalmente la instalación no debería ser complicada.
Durante el proceso de instalación, se os pedirá una ruta.

Rellenad la ruta como en el siguiente ejemplo:

```bat
c:\cible
```

El nombre "cible" es sólo una palabra como ejemplo, que debe hacer referencia a la subcarpeta "source" si la habéis creado anteriormente. Lo podéis reemplazar por lo que queráis, pero en esta documentación usaremos a partir de ahora la palabra "cible"
Una vez que se ha terminado la instalación, continuad con la configuración del sonido si se os propone. A continuación, salid del programa de instalación.
Llegaréis al prompt provocado por la instrucción "pause", aunque ya lo hemos dicho esta instrucción os permite leer/capturar los posibles errores de DOSBox antes de volver a EmulationStation.
Apretad cualquier tecla para volver otra vez a EmulationStation.

A continuación el proceso continúa como en el apartado anterior con la excepción de que hace falta añadir un cambio de directorio.

Asi dicho, para configurar el sonido, si no ha sido hecho todavía y suponiendo que el programa de instalación del sonido se llama "setup", vuestro fichero "dosbox.bat" debería parecerse al siguiente:

```bat
cd cible
setup
pause
```

Lanzad otra vez el juego y configurad el sonido como en el apartado anterior.

A continuación, si la instalación y la configuración del sonido son OK, podéis editar el fichero "dosbox.bat" para proceder con el lanzamiento del juego en sí mismo.
Vuestro fichero "dosbox.bat" debería contener estas líneas:

```bat
cd cible
s2
pause
```

Y, normalmente, esto es todo ;-).

>El repertorio "source" a veces no sirve para nada una vez que el juego se ha instalado. Este tipo de instalaciones estaban hechas en su época para simplemente copiar el contenido del disquete al disco duro para no tener que utilizar la disquetera. A vosotros de probar si lo podéis borrar.
{.is-warning}

## El juego necesita una instalación y un soporte CD

Este caso posee varios puntos comunes con los otros dos junto con dos diferencias principales:

* Va a ser necesario "montar" el CD en un lector de CDs virtual (no os preocupéis, no es complicado :-) )
* Como regla general habrá que conservar el montado del CD para poder jugar o, como mínimo, poder beneficiar de contenidos extras "in game" como vídeos o músicas.

Asi que, en un primer tiempo. vamos a crar una carpeta "source_cd" en la raíz de la carpeta ".pc". Dentro de esta carpeta vamos a depositar el o los ficheros imagen CD (bin, cue, iso, img, ...).

Si comparamos con el apartado anterior, la mayor diferencia consiste en el montaje de la imagen CD en un lector CD virtual. La instrucción que vamos a utilizar para ello varía ligeramente dependiendo del formato de la imagen CD. Por ejemplo, en dos, los nombre que superan los 6 o 7 caracteres - no esto seguro del número exacto - son truncados con "~X":

* formato bin/cue : `imgmount D "C:\source~1\settle~1.cue" -t iso -fs iso`
* formato iso : `imgmount D "C:\source~1\settle~1.iso" -t iso -fs iso`
* formato img : `imgmount D "C:\source~1\settle~1.img"`
* formato fichero/carpeta directa (aquí, el montaje no es imprescindible, únicamente para disfrutar de contenidos visuales o sonoros extras): `mount D "C:\source~1"`

Algunos CDs de juegos son CS mixtos. Es decir que poseen los datos del juego asi como las pistas audio que permiten tocar la música durante el juego.
Es importante para estos CDs que la imagen se encuentre en un sólo fichero .img o .bin porque DOSBox no soporta las imágenes separadas en varios ficheros .bin.
El montaje en este caso se debe realizar imperativamente vía el fichero .cue correspondiente para que todas las pistas audio se carguen correctamente.

Si no tenemos en cuenta este paso de montaje, el resto del procedimiento es casi idéntico a los apartados anteriores.
Sólo hay que añadir la líne "mount ..." al principio del fichero "dosbox.bat" de forma sistemática y proceder a continuación a la instalación, la configuración y el lanzamiento del juego.
Tened cuidado para desplazaros correctamente dentro de la buena carpeta (comando "cd"): hay que hacer `cd source_cd` para la instalación, y `cd cible` para acceder a la carpeta del juego (como en el apartado anterior)
>Cuando el juego funciona correctamente, podéis quitar la instrucción "pause" para no tener que esperar para volver a EmulationStation al salir del juego.
{.is-info}

## ¿Cómo utilizar el teclado virtual?

Por defecto, el teclado virtual se abre con Ctrl-F2.
Existe un nuevo botón en el mapper (Ctrl-F1 &gt; _Virt Keyb_) 
si queréis asociarlo a otro evento (teclado, raton o joystick).

Una vez abierto, utilizad el ratón o el joystick para seleccionar y accionar las teclas:

* Utilizad el boton izquierdo del ratón o el primer botón del joystick para simular una acción de presión-relajación de la tecla.
* Utilizad el botón derecho del ratón o el segundo botón del joystick para simular una presión continua de la tecla (la tecla cambiará a un color más oscuro).

Haced click de nuevo con cualquier botón del ratón o del joystick par soltarla.

Esto permite realizar combinaciones con Shift, Ctrl o Alt.

Finalmente, apretad el botón V verde para cerrar el teclado vritual y enviar los eventos a DOSBox. También se puede apretar sobre la cruz roja par anular los eventos en curso y cerrar el teclado virtual.

>* Si utilizáis un fichero de mapping, la combinación por defecto Ctrl-F2 no estará disponible.
>* Si cerrais el teclado virtual mientras se realiza una pulsación de tecla sostenida, el evento se enviará constantemente a DOSBox, hasta que volváis a abrir el teclado virtual!
>* Se ignoran todos los mappings configurados para el joystick o el ratón mientras el teclado virtual está abierto.
{.is-warning}

## Solución de problemas (dentro de `dosbox.cfg`)

**La imagen está deformada**: En algunos juegos antiguos, deberéis modificar la opción `aspect=false` a `aspect=true` para obtener una imagen en un ratio de 4/3. A notar que en juegos más modernos, esta opción puede provocar problemas de rendimiento

**El joystick se mueve solo**: Intentad modificar el parámetro `timed=false` a `timed=true`. Este defecto puede venir de las _deadzone_ de los joysticks en sí mismos y desgraciadamente no existen ninguna opción en DOSBox para corregir este problema por el momento

**No puedo mapear el D-pad de un mando box360**: Si que se puede, el mapper de DOSBox permite configurar el D-PAD pero parece que no funciona una vez dentro del juego. En este caso podéis intentar cambiar el tipo de joystick:

**Cómo configurar la deadzone de un joystick**: Desgraciadamente, no se puede hacer con DOSBox por el momento.

**CD Not Found / CD Driver not present:** Habéis sin dudo renombrado mal vuestros ficheros cd (iso, cue, bin, o el contenido del cue), o también habéis utilizado nombres que no respectan el standard DOS.

**No consigo hacer funcionar `mount a` o `mount d`**: A diferecia de `imgmount`, el comando `mount` no puede utilizar rutas 'virtuales' del interior de la máquina DOSBox. Para que `mount` funcione, hay que utilizar rutas reales del sistema de ficheros de Recalbox. Cuando se lanza DOSBox, el '.' en Recalbox se encuentra posicionado en el directorio raíz '/', asi que hay que utilizar la ruta absoluta completa hasta el fichero o la carpeta

**El mapper hace tonterías, borra mi fichero o mezcla los botones**: Parece que eres víctima del parámetro `buttonwrap`. Cuando se encuentra configurado a `true`, los botones de vuestro pad se configuran con un ID más grande que el ID de los botones emulados por el joystick partiendo de cero (5 sera remapeado a 0, 6 a 1, etc...) . Cambiad este paramétro a `false` y todo debería entrar en orden de nuevo.
