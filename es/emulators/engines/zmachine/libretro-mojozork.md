---
title: Libretro-mojozork
description: 
published: true
date: 2024-07-14T19:23:29.696Z
tags: libretro, z-machine, 9.1+, mojozork
editor: markdown
dateCreated: 2023-09-11T06:43:18.143Z
---

**Frotz** es un intérprete de juegos Infocom y de otros juegos desarrolados para la **Z-máquina**.

## ![](/emulators/license.svg) Licencia

El core posee la siguiente licencia [**zlib**](https://github.com/icculus/mojozork/blob/main/LICENSE.txt).

## ![](/emulators/compatibility.png) Compatibilidad

 | RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | 
 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Funcionalidades

| Funcionalidad | ¿Soportada? |
| :---: | :---: |

## ![](/emulators/bios.svg) BIOS

>**No neceista bios.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Extensiones de roms soportadas

Las roms deben tener las extensiones siguientes:

* .dat
* .zip
* .z1
* .z3
* .7z

Este sistema soporta las roms comprimidas en formato .zip/.7z. No obstante, se trata sólo de un archivo.

Los ficheros contenidos en los .zip/.7z deben tener las extensiones citadas previamente.
Además, cada fichero .zip/.7z sólo debe contener un fichero rom al interior.

### Emplazamiento

Disponed vuestras roms como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 zmachine
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.zip**

## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

> **Este core no tiene opciones**
{.is-success}

## ![](/emulators/external-links.png) Enlaces externos

* **Código fuente utilizado** : [https://github.com/icculus/mojozork/](https://github.com/icculus/mojozork/)