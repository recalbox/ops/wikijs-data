---
title: Utilisation de la manette SNES
description: Comment synchroniser les manettes Nintendo Switch Online - SNES dans Recalbox
published: true
date: 2021-09-04T22:52:36.331Z
tags: switch, nintendo, snes, 7.2+, manette
editor: markdown
dateCreated: 2021-05-21T08:46:08.657Z
---

## Synchronisation

Sur chaque manette, il y a un petit bouton qui permet de l'allumer et de l'éteindre mais ce bouton permet aussi de faire une synchronisation avec votre boitier.

![Bouton de synchronisation](/tutorials/controllers/controllers/switch-joycons/snesconfig1.jpg)

Dans Recalbox, dans vos listes de systèmes disponibles, appuyez sur **Start** de votre manette habituelle et allez dans **RÉGLAGES MANETTES**.

![](/tutorials/controllers/controllers/switch-joycons/snesconfig2.png){.full-width}

Dans ce menu, faites **ASSOCIER UNE MANETTE BLUETOOTH**.

![](/tutorials/controllers/controllers/switch-joycons/snesconfig3.png){.full-width}

Une fois la recherche de périphériques Bluetooth lancé, appuyez sur le bouton de synchronisation de la manette à associer. Celle-ci doit commencer à avoir les 4 lumières à côté en train de faire une sorte d'aller-retours de gauche à droite et inversement.

![](/tutorials/controllers/controllers/switch-joycons/snesconfig4.png){.full-width}

Une fois que la recherche de périphériques Bluetooth est terminée, vous devriez voir votre manette dans la liste.

![](/tutorials/controllers/controllers/switch-joycons/snesconfig5.png){.full-width}

Sélectionnez la manette et validez. Votre manette doit maintenant être reconnue et toujours avoir les 4 lumières faire des allers-retours.

Une fois l'association terminée, la manette apparaîtra dans la liste des manettes.

![](/tutorials/controllers/controllers/switch-joycons/snesconfig6.png){.full-width}

À vous les joies de jouer avec votre manette SNES !

## Paramétrage de la manette SNES

Une fois associé, la manette aura une configuration automatique détectée avec la touche **HOTKEY** sur **SELECT**.