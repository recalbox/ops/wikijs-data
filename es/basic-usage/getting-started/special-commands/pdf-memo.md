---
title: Infografía en PDF
description: Descargar la infografía de comandos especiales en PDF
published: true
date: 2023-09-03T13:18:29.157Z
tags: pdf, memo, comandos especiales, infografía, descargar
editor: markdown
dateCreated: 2023-09-03T13:18:29.157Z
---

## Lista de enlaces par las descargas

[Infografía en formato PDF](https://mega.nz/file/zcpE2DIb#mb4e8arxm7QVN5qz613TUPQrGi65EEkKfzegvkqq1kk)

## Imágenes de la infografía

![](/basic-usage/getting-started/special-commands/pdf1.jpg){.full-width}

![](/basic-usage/getting-started/special-commands/pdf2.jpg){.full-width}

![](/basic-usage/getting-started/special-commands/pdf3.jpg){.full-width}

![](/basic-usage/getting-started/special-commands/pdf4.jpg){.full-width}