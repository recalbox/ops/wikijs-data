---
title: Gestion des multi-disques au format .M3U
description: 
published: true
date: 2024-07-01T17:01:33.224Z
tags: m3u, gestion, multi-disques
editor: markdown
dateCreated: 2022-12-27T10:56:20.216Z
---

## Introduction

Plusieurs CD d'un même jeu peuvent être chargés simultanément depuis EmulationStation dans RetroArch en créant un fichier "liste de lecture" `.M3U` (texte brut `.TXT` avec une extension `.M3U`).

Une liste de lecture `.M3U` peut être utilisé avec n'importe quel extension de jeu :
- `.BIN` / `.CUE`,
- `.BIN` / `.IMG` / `.CCD` / `.SUB`,
- `.BIN` / `.GDI` / `.RAW`,
- `.CHD`,
- `.CDI`,
- etc.

## Utilisation

Voici quelques exemples ci-dessous. Ceci s'applique à tous les systèmes qui ont plusieurs disques / cartouches / disquettes par jeu et supportant l'extension `.M3U`.

## {.tabset}

### Format `.BIN` / `.CUE`

* Créez un nouveau fichier texte que l'on nomme `Final Fantasy VII (France).m3u` et qui contient les informations suivantes :

```text
Final Fantasy VII (France) (Disc 1).cue
Final Fantasy VII (France) (Disc 2).cue
Final Fantasy VII (France) (Disc 3).cue
```

### Format `.CHD`

* Créez un nouveau fichier texte que l'on nomme `Final Fantasy VII (France).m3u` et qui contient les informations suivantes :

```text
Final Fantasy VII (France) (Disc 1).chd
Final Fantasy VII (France) (Disc 2).chd
Final Fantasy VII (France) (Disc 3).chd
```

### Format `.BIN` / `.IMG` / `.CCD` / `.SUB`

* Créez un nouveau fichier texte que l'on renomme `Final Fantasy VII (France).m3u` et qui contient les informations suivantes :

```text
Final Fantasy VII (France) (Disc 1).ccd
Final Fantasy VII (France) (Disc 2).ccd
Final Fantasy VII (France) (Disc 3).ccd
```

### Format `.BIN` / `.GDI` / `.RAW`

* Créez un nouveau fichier texte que l'on renomme `Final Fantasy VII (France).m3u` et qui contient les informations suivantes :

```text
Final Fantasy VII (France) (Disc 1).gdi
Final Fantasy VII (France) (Disc 2).gdi
Final Fantasy VII (France) (Disc 3).gdi
```

### Format `.CDI`

* Créez un nouveau fichier texte que l'on renomme `Grandia II (USA)[Zorlon].m3u` et qui contient les informations suivantes :

```text
Grandia II (USA)(CD 1 of 3)[Zorlon].cdi
Grandia II (USA)(CD 2 of 3)[Zorlon].cdi
Grandia II (USA)(CD 3 of 3)[Zorlon].cdi
```

## Changer de disque en cours de partie

Si l'on a besoin de changer de disque lorsque le jeu est lancé, il faut réaliser les actions suivantes :

* Ouvrir le tiroir CD "virtuel" : Touche `HK` + Joystick Gauche (Direction Haut)
* Changer de disque : Touche `HK` + Joystick Gauche (Direction Droite ou Gauche)
* Fermer le tiroir CD "virtuel" : Touche `HK` + Joystick Gauche (Direction Haut)

## Le cas Dolphin

En fonction de votre système d'opération (le plus souvent Windows), pous pouvez voir un message de rejet tel que celui-ci :

![Message d'erreur du fichier M3U avec Dolphin](/tutorials/games/generalities/m3u-dolphin-fr.png){.center}

Dans ce cas, ouvrez l'éditeur Notepad++ ([installez-le](https://notepad-plus-plus.org/downloads/) avant si vous ne l'avez pas) et ouvrez votre fichier `.M3U`. et dans les menus, allez dans `Édition` > `Convertir les sauts de ligne` et choisissez `Convertir au format UNIX (LF)`. Une fois effectué, enregistrez votre fichier et il devrait être lu correctement par Dolphin.