---
title: Configurez votre écran cathodique sur Raspberry Pi 5 / 500 / 4 / 400 / 3 / Zero2
description: Comment rejouer à Recalbox sur vos écrans cathodiques sur Raspberry Pi.
published: true
date: 2025-02-21T01:49:55.033Z
tags: video, crt, screen, cathodique, pixel perfect, trinitron, pvm, 240p, 224p, 15khz
editor: markdown
dateCreated: 2021-09-10T22:09:46.057Z
---

## Présentation

En tant qu'utilisateurs de Recalbox, vous savez à quel point il est bon de ressentir cette nostalgie en rejouant aux jeux qui ont bercés notre enfance.

Jouer avec une manette PS4 sur la TV OLED du salon nous permet déjà de retrouver et de partager ce plaisir du jeu retro.

Cependant, certains d'entre-nous ont une soif insatiable qui les oblige à aller toujours plus loin dans la recherche d'une expérience la plus proche possible de l'originale.

Dans cette recherche sans fin, l'affichage, ou l'image du jeu est sans doute le point le plus important.

Et quoi de plus proche de l'original que de jouer sur les écrans CRT de l'époque ?

## Recalbox sur votre écran CRT

Recalbox est capable de reproduire la résolution et les fréquences exactes de la plupart des consoles et des jeux d'arcade de l'époque, sur votre TV CRT (cathodique).

Équipez-vous d'un [**Recalbox RGB DUAL**](https://www.recalbox.com/recalbox-rgb-dual), d'un VGA666 et un câble Péritel, d'un Pi2Scart ou d'un RGBPi.

Recalbox RGB Dual se configure automatiquement, pour les autres, une fois le péripherique branché à votre Rasbperry Pi 4 et à la TV, éditez le fichier de configuration `/crt/recalbox-crt-options.cfg` qui se trouve dans la partition `[RECALBOX]` lorsque vous placez la carte sd dans votre ordinateur:

```
# Pour recalboxrgbdual c'est automatique !
# Pour vga666
adapter.type = vga666
# Pour pi2scart
adapter.type = pi2scart
# Pour rgbpi
adapter.type = rgbpi
```

Vous pouvez aussi passer par le menu "Configuration avancée => Recalbox CRT" dans l'interface.

**C'est tout**, après avoir redémarré votre Recalbox, le système se configure pour envoyer l'image directement sur votre TV.

------------------

## Compatibilité matérielle

* **Recalbox RGB DUAL**: TV CRT, PVM, VGA Monitor
Le Recalbox RGB DUAL est **la solution plug and play jouer à Recalbox sur CRT**. De plus, il s'agit d'une carte développée par l'équipe de Recalbox.

* **VGA666**: TV CRT avec câble modifié, Ecran CRT VGA, PVM
Le VGA666 vous permet de brancher un câble VGA (anciens écrans PC cathodiques). Avec un câble VGA->PERITEL modifié, il est tout à fait compatible avec les TV CRT.
* **Pi2Scart**: TV CRT, PVM
Un H.A.T. pour Raspberry Pi 3 et 4.
* **RGBPi**: TV CRT, PVM
Un câble péritel pour brancher Recalbox à votre CRT.

## Configuration intermédiaire
Dans le fichier de configuration `/crt/recalbox-crt-options.cfg` qui se trouve dans la partition `[RECALBOX]`:
```
# Décalage de l'image vers la gauche (négatif) ou vers la droite (positif)
mode.offset.horizontal = -2
# Décalage de l'image vers le haut (négatif) ou vers le bas (positif)
mode.offset.vertical = -2
```

## Configuration avancée

La configuration par defaut des modes d'affichage par système et par jeux arcade se trouve dans le dossier `/recalbox/system/configs/crt/` (`/system/configs/crt/` dans la partition `[SHARE]`)

* `modes.txt` liste les modes d'affiches pouvant être utilisés
* `system.txt` définit pour chaque système le mode à utiliser
* `arcade_games.txt` définit pour chaque jeu arcade le mode à utiliser

Pour surcharger ou remplacer ces configurations, il vous suffit de créer les fichiers correspondants dans le dossier `/recalbox/share/system/configs/crt/` (`/system/configs/crt/` dans la partition `[SHARE]`).

Ces fichiers de surcharge ne doivent contenir que les modes, systèmes ou jeux que vous voulez surcharger ou ajouter.


Structure des fichiers :
┣ 📁 recalbox
┃ ┣ 📁 system
┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┣ 📁 crt
┃ ┃ ┃ ┃ ┣ 🗒 arcade_games.txt (lecture seule)
┃ ┃ ┃ ┃ ┣ 🗒 modes.txt (lecture seule)
┃ ┃ ┃ ┃ ┣ 🗒 systems.txt (lecture seule)
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 crt
┃ ┃ ┃ ┃ ┃ ┣ 🗒 arcade_games.txt
┃ ┃ ┃ ┃ ┃ ┣ 🗒 modes.txt
┃ ┃ ┃ ┃ ┃ ┣ 🗒 systems.txt


Voir [l'exemple de surcharge](#exemple-de-surcharge)

### Structure des fichiers

#### `modes.txt`

Le fichier `modes.txt` contient un mode par ligne, avec son identifiant, sous le format suivant :

```ini
MODE_ID,MODE,REFRESH_RATE
```

Exemple:

```ini
snes:nes:ntsc:224@60.0988,1920 1 80 184 312 224 1 9 3 24 0 0 0 60 0 39001717 1,60.0988
```

Ici le mode MODE_ID est `snes:nes:ntsc:224@60.0988` et le MODE est `1920 1 80 184 312 224 1 9 3 24 0 0 0 60 0 39001717 1`

#### `systems.txt`

Le fichier `systems.txt` permet pour chaque système, de configurer un mode d'affichage par région, et de sélectionner la taille de la zone d'affichage :

```ini
SYSTEM_ID,REGION,SCREEN_TYPE,INTERLACED,MODE_ID,VIEWPORT_WIDTH,VIEWPORT_HEIGHT
```

Exemple :

```ini
vectrex,all,15kHz,progressive,standard:ntsc:240@60,0,0
```

Cette ligne permet à Recalbox de savoir que la Super Nintendo (`snes`), lorsqu'elle va charger un jeu NTSC (non européen) sur un TV 15kHz utilisera le mode d'affichage identifié par `snes:nes:ntsc:224@60.0988` (dans `modes.txt`).

Les deux zéros suivants laissent Recalbox définir la meilleur valeur pour la hauteur et la largeur de l'image.

#### `arcade_games.txt`

Le fichier `arcade_games.txt` permet de sélectionner le mode d'affichage pour chaque jeu arcade, suivant le core (ou l’émulateur) sur lequel il sera lancé. Il permet aussi de modifier la taille de la zone d'affichage :

```ini
GAME_ID,CORE_ID,MODE_ID,VIEWPORT_WIDTH,VIEWPORT_HEIGHT,ROTATION
```

Exemple :

```ini
dino,fbneo,snes:nes:ntsc:224@60.0988,0,0
```

Les GAME_ID sont les noms des fichiers des jeux, sans le `.zip`.
Le CORE_ID est le nom du core utilisé par Recalbox.

Cette ligne permet à Recalbox de savoir que le jeu `dino.zip`, lancé sur RetroArch avec le core `fbneo`, utilisera le mode `arcade:224@59.637405`.

### Exemple de surcharge

La surcharge permet d'ajouter ou de remplacer des modes, systèmes ou jeux d'arcade.

> Toutes les surchages se font dans de nouveaux fichiers, situés dans `/recalbox/share/system/configs/crt/` (`/system/configs/crt/` dans la partition `[SHARE]`).
> {.is-info}

Chacun des modes, systèmes ou jeux des surcharges est ajouté à la configuration de base, ou la remplace si l'id existe déjà.

Imaginons que je veuille changer le mode d'affiche de `Cadillac and Dinosaurs` sur arcade, et le passer en 224 lignes à 60Hz.

Dans le fichier modes.txt, il existe un mode correspondant à 224 lignes à 60Hz. Il a pour MODE_ID `standard:all:240@60`

Je vais donc créer le fichier `/recalbox/share/system/configs/crt/arcade_games.txt` (`/system/configs/crt/arcade_games.txt` dans la partition `[SHARE]`) et y mettre une seule ligne :

```ini
dino,fbneo,standard:all:240@60,0,0,0
```

Les VIEWPORT_WIDTH et VIEWPORT_HEIGHT permettent de redimensionner l'image affichée. Ils ne changent pas le mode d'affichage de l’écran.
Vous pouvez vouloir ajouter des configurations spécifiques pour des systèmes ou des jeux d'arcade.

Définissons une fois pour toute la largeur d’écran de `Metal Slug 3` à 1800 pixels dans `/recalbox/share/system/configs/crt/arcade_games.txt` (`/system/configs/crt/arcade_games.txt` dans la partition `[SHARE]`)

```ini
mslug3,fbneo,arcade:224@59.185606,1800,0,0
```

Relancez `Metal Slug 3` et vous pourrez observer que la largeur de l'écran a diminué.