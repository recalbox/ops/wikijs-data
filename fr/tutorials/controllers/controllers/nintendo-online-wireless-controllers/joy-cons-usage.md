---
title: Utilisation des Joycons
description: Comment synchroniser et utiliser les Joy-Cons dans Recalbox
published: true
date: 2023-01-18T20:33:26.664Z
tags: joy-con, 7.2+
editor: markdown
dateCreated: 2021-05-21T08:46:02.002Z
---

Nous allons voir comment synchroniser une paire de Joycons et comment les utiliser séparément ou ensemble (combinés).

## Mode un Joycon par joueur

Sur chaque Joycon, il y a un petit bouton pour allumer et éteindre le Joycon mais ce bouton vous permet aussi de synchroniser le Joycon avec votre Recalbox.

![Bouton de synchronisation sur chaque Joy-Con](/tutorials/controllers/controllers/switch-joycons/joyconconfig1.jpg)

Dans Recalbox, allez dans les listes systèmes, appuyez sur **Start** sur votre manette habituelle ou **Enter** sur un clavier et allez dans **RÉGLAGES MANETTES**.

![](/tutorials/controllers/controllers/switch-joycons/joyconconfig2.png){.full-width}

Dans ce menu, sélectionnez **ASSOCIER UNE MANETTE BLUETOOTH**. et validez. Le processus de recherche de Bluetooth démarre.

![](/tutorials/controllers/controllers/switch-joycons/joyconconfig3.png){.full-width}

Sur le Joycon, appuyez sur le bouton de synchronisation pendant quelques secondes (généralement trois secondes) pour le passer en mode association. Vous devriez voir les quatre voyants latéraux aller et venir.
Si les voyants s'éteignent, recommencez.

![](/tutorials/controllers/controllers/switch-joycons/joyconconfig4.png){.full-width}

Une fois que la recherche de périphériques Bluetooth est terminée, vous devriez voir votre Joycon dans la liste.

![](/tutorials/controllers/controllers/switch-joycons/joyconconfig5.png){.full-width}

Sélectionnez le Joycon et validez. Lorsque vous obtenez le message **MANETTE ASSOCIÉE**, appuyez sur les boutons **SL** et **SR**. Votre Joycon doit avoir une lumière latérale allumée. L'appairage est réussi.

Faites de même avec le second Joycon en reprenant les mêmes étapes vues ci-dessus.

![](/tutorials/controllers/controllers/switch-joycons/joyconconfig6.png){.full-width}

Validez le second Joycon pour terminer son association et appuyez sur **SL** + **SR** comme dernière étape. Une fois que vous avez associé vos 2 Joycons, ils seront visibles dans la liste des contrôleurs.

![](/tutorials/controllers/controllers/switch-joycons/joyconconfig7.png){.full-width}

## Joycons en mode combiné

Pour utiliser les deux Joycons comme une seule manette, vous devez associer les deux Joycons et, lorsque les voyants clignotent, appuyer sur **L** du Joycon gauche et **R** du Joycon droit (L+R) en même temps. Maintenant vous avez une seule manette à partir de deux Joycons.

Si les Joycons étaient déjà connectés en mode solo, éteignez-les, attendez environ 10 secondes et rallumez-les, les voyants commenceront à clignoter, puis appuyez sur **L** + **R** comme indiqué précédemment.

![](/tutorials/controllers/controllers/switch-joycons/joyconconfig8.png){.full-width}

À vous les joies de jouer avec les Joy-Cons !