---
title: SimCoupe
description: 
published: true
date: 2024-07-17T21:20:24.875Z
tags: sam, coupé, simcoupe
editor: markdown
dateCreated: 2023-09-11T11:39:41.618Z
---



## ![](/emulators/license.svg) Licencia

El core posee la siguiente licenci [**GPLv2**](https://gitlab.com/recalbox/packages/standalone/simcoupe/-/blob/dev/License.txt).

## ![](/emulators/compatibility.png) Compatibilidad

 | RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | 
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Funcionalidades



## ![](/emulators/bios.svg) BIOS

>**No necesita bios.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Extensiones de roms soportadas

Las roms deben tener las extensiones siguientes:

* .dsk
* .sad
* .mgt
* .sdf
* .td0
* .sbt
* .cpm

### Emplazamiento

Disponed vuestras roms como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 samcoupe
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.dsk**

>Para más información sobre las roms, consultad [este tutorial](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

SimCoupe puede utilizar los siguientes formatos de disco:

`.MGT` - Volcado simple de sectores de discos +D/SAM: 2 caras, 80 pistas por cara, 10 sectores por pista, 512 bytes por sector = 819200 bytes. Las imágenes antiguas en este formato pueden tener la extensión de fichero .dsk. Este es el formato preferido para los discos SAM de formato normal, y es compatible con los dispositivos /dev/fd0u800 de Linux.

`.SAD` - Formato de disco SAM, creado por Aley Keprt. También es un volcado basado en sectores, pero con un fichero de cabecera de 22 bytes que permite ajustes geométricos de pistas por disco, pistas por lado, sectores por pista y bytes por sector. Los discos SAM normales que se almacenan en formato SAD contienen 819222 bytes, pero una diferencia en el orden de las pistas evita eliminar la cabecera de 22 bytes para obtener una imagen MGT equivalente. La versión 2 de las imágenes SAD tiene el mismo formato pero comprimido con gzip.

`.DSK` - Imágenes Extended DSK (EDSK), utilizadas originalmente con los soportes Amstrad CPC y Spectum +3. Un formato flexible capaz de representar todos los discos SAM existentes, y también el formato preferido de la página web worldofsam.org. El tamaño de la imagen es proporcional a la geometría del disco, con un disco SAM normal de unos 840K.

`.SBT` - Archivo de arranque Sam, creado por Andrew Collier. Estos ficheros son ficheros autoarrancables creados para ser copiados a un disco SAM vacío cuando se arranca. Aunque técnicamente no son imágenes arrancables, SimCoupe los trata como tales (sólo lectura).

## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

>Para conservar las configuraciones personalizadas cuando se produce una actualización del sistema Recalbox, os aconsejamos utilizar las [Sobrecargas de configuración](./../../../advanced-usage/configuration-override).
{.is-info}

## ![](/emulators/external-links.png) Enlaces externos

* **Código fuente utilizado** : [https://gitlab.com/recalbox/packages/standalone/simcoupe/](https://gitlab.com/recalbox/packages/standalone/simcoupe/)