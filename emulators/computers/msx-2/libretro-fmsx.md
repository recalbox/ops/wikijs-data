---
title: Libretro FMSX
description: 
published: true
date: 2024-07-24T22:09:37.518Z
tags: libretro, fmsx, msx2
editor: markdown
dateCreated: 2021-07-29T16:50:40.317Z
---



## ![](/emulators/license.svg) License

This core is under [**non-commercial**](https://github.com/libretro/fmsx-libretro/blob/master/LICENSE) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Features

| Feature | Supported |
| :---: | :---: |
| Restart  | ✔ |
| Screenshots | ✔ |
| States | ✔ |
| Rewind | ✔ |
| Netplay | ✔ |
| Core Options | ✔ |
| RetroAchievements | ✔ |
| Controls | ✔ |
| Remapping | ✔ |
| Disk Control | ✔ |

## ![](/emulators/bios.svg) BIOS

### List of mandatory bios

| Filename | Description | MD5 | Provided |
| :---: | :---: | :---: | :---: |
| CARTS.SHA | - | d6dedca1112ddfda94cc9b2e426b818b | ✅ |
| CYRILLIC.FNT | - | 85b38e4128bbc300e675f55b278683a8 | ✅ |
| DISK.ROM | DiskROM/BDOS | 80dcd1ad1a4cf65d64b7ba10504e8190 | ✅ |
| FMPAC16.ROM | - | af8537262df8df267072f359399a7635 | ✅ |
| FMPAC.ROM | FMPAC BIOS | 6f69cc8b5ed761b03afd78000dfb0e19 | ✅ |
| ITALIC.FNT | - | c83e50e9f33b8dd893c414691822740d | ✅ |
| KANJI.ROM | Kanji Font | febe8782b466d7c3b16de6d104826b34 | ✅ |
| MSX2EXT.ROM | MSX2 ExtROM | 2183c2aff17cf4297bdb496de78c2e8a | ✅ |
| MSX2PEXT.ROM | MSX2+ ExtROM | 7c8243c71d8f143b2531f01afa6a05dc | ✅ |
| MSX2P.ROM | MSX2+ BIOS | 6d8c0ca64e726c82a4b726e9b01cdf1e | ✅ |
| MSX2.ROM | BIOS MSX2 | ec3a01c91f24fbddcbcab0ad301bc9ef | ✅ |
| MSXDOS2.ROM | MSX-DOS 2 | 6418d091cd6907bbcf940324339e43bb | ✅ |
| MSX.ROM | BIOS MSX | aa95aea2563cd5ec0a0919b44cc17d47 | ✅ |
| PAINTER.ROM | Yamaha Painter | 403cdea1cbd2bb24fae506941f8f655e | ✅ |
| RS232.ROM | - | 279efd1eae0d358eecd4edc7d9adedf3 | ✅ |

### Location

Put the bios like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 msx
┃ ┃ ┃ ┃ ┣ 🗒 **CARTS.SHA**
┃ ┃ ┃ ┃ ┣ 🗒 **CYRILLIC.FNT**
┃ ┃ ┃ ┃ ┣ 🗒 **DISK.ROM**
┃ ┃ ┃ ┃ ┣ 🗒 **FMPAC16.ROM**
┃ ┃ ┃ ┃ ┣ 🗒 **FMPAC.ROM**
┃ ┃ ┃ ┃ ┣ 🗒 **ITALIC.FNT**
┃ ┃ ┃ ┃ ┣ 🗒 **KANJI.ROM**
┃ ┃ ┃ ┃ ┣ 🗒 **MSX2EXT.ROM**
┃ ┃ ┃ ┃ ┣ 🗒 **MSX2PEXT.ROM**
┃ ┃ ┃ ┃ ┣ 🗒 **MSX2P.ROM**
┃ ┃ ┃ ┃ ┣ 🗒 **MSX2.ROM**
┃ ┃ ┃ ┃ ┣ 🗒 **MSXDOS2.ROM**
┃ ┃ ┃ ┃ ┣ 🗒 **MSX.ROM**
┃ ┃ ┃ ┃ ┣ 🗒 **PAINTER.ROM**
┃ ┃ ┃ ┃ ┣ 🗒 **RS232.ROM**

## ![](/emulators/roms.png) Roms

### Supported extensions

Roms must have the extension:

* .rom
* .mx1
* .mx2
* .dsk
* .cas
* .zip
* .7z

This system supports compressed roms in .zip/.7z format. However, be careful, it is only an archive.

The files contained in the .zip/.7z must correspond to the extensions mentioned above.
Moreover, each .zip/.7z file must contain only one compressed rom.

### Location

Put the roms like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 msx2
┃ ┃ ┃ ┃ ┣ 🗒 **game.zip**

>The roms using **No-Intro** format are highly recommended.
{.is-success}

>For more information about roms, go to [this tutorial](./../../../tutorials/games/generalities/isos-and-roms)!
{.is-info}

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Accessing to options

You can configure various options in two different ways.

* Via RetroArch menu:

┣ 📁 RetroArch Menu
┃ ┣ 📁 Core options
┃ ┃ ┣ 🧩 Name_option

* Via the file `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core options

| Options | Options values | Variables (for configuration override) | Variables values |
| --- | --- | --- | --- |
| MSX Mode | `MSX2+` ✅ / `MSX1` / `MSX2` | `fmsx_mode` | `MSX2+` / `MSX1` / `MSX2` |
| MSX Video Mode | `NTSC` ✅ / `PAL` | `fmsx_video_mode` | `NTSC` / `PAL` || Support high resolution | `Off` ✅ / `Interlaced` / `Progressive` | `fmsx_hires` | `Off` / `Interlaced` / `Progressive` |
| Support overscan | `No` ✅ / `Yes` | `fmsx_overscan` | `No` / `Yes` |
| MSX Mapper Type Mode | `Guess Mapper Type A` ✅ / `Guess Mapper Type B` | `fmsx_mapper_type_mode` | `Guess Mapper Type A` / `Guess Mapper Type B` |
| MSX Main Memory | `Auto` ✅ / `64KB` / `128KB` / `256KB` / `512KB` | `fmsx_ram_pages` | `Auto` / `64KB` / `128KB` / `256KB` / `512KB` |
| MSX Video Memory | `Auto` ✅ / `32KB` / `64KB` / `128KB` / `192KB` | `fmsx_vram_pages` | `Auto` / `32KB` / `64KB` / `128KB` / `192KB` |
| Enable SCC-I 128kB MegaRAM | `Yes` ✅ / `No` | `fmsx_scci_megaram` | `Yes` / `No` |
| fMSX logging | `Off` ✅ / `Info` / `Debug` / `Spam` | `fmsx_log_level` | `Off` / `Info` / `Debug` / `Spam` |
| Support Game Master | `No` ✅ / `Yes` | `fmsx_game_master` | `No` / `Yes` |
| Simulate DiskROM disk access calls | `No` ✅ / `Yes` | `fmsx_simbdos` | `No` / `Yes` |
| Use autofire on SPACE | `No` ✅ / `Yes` | `fmsx_autospace` | `No` / `Yes` |
| Show all sprites | `No` ✅ / `Yes` | `fmsx_allsprites` | `No` / `Yes` |
| Text font | `standard` ✅ / `DEFAULT.FNT` / `ITALIC.FNT` / `INTERNAT.FNT` / `CYRILLIC.FNT` / `KOREAN.FNT` / `JAPANESE.FNT` | `fmsx_font` | `standard` / `DEFAULT.FNT` / `ITALIC.FNT` / `INTERNAT.FNT` / `CYRILLIC.FNT` / `KOREAN.FNT` / `JAPANESE.FNT` |
| Save disk changes | `Never` ✅ / `Immediate` / `On close` / `To/From SRAM` | `fmsx_flush_disk` | `Never` / `Immediate` / `On close` / `To/From SRAM` |
| Create empty disk when none loaded | `No` ✅ / `Yes` | `fmsx_phantom_disk` | `No` / `Yes` |
| Load MSXDOS2.ROM when found | `No` ✅ / `Yes` | `fmsx_dos2` | `No` / `Yes` |
| Custom keyboard RetroPad up | `escape` / `left` / `up` ✅ / `right` / `down` / `shift` / `ctrl` / `graph` / `backspace` / `tab` / `space` / `capslock` / `select` / `home` / `enter` / `del` / `insert` / `country` / `dead` / `stop` / `f1` / `f2` / `f3` / `f4` / `f5` / `keypad0` / `keypad1` / `keypad2` / `keypad3` / `keypad4` / `keypad5` / `keypad6` / `keypad7` / `keypad8` / `keypad9` / `kp_multiply` / `kp_plus` / `kp_divide` / `kp_minus` / `kp_comma` / `kp_period` / `backquote` / `minus` / `equals` / `leftbracket` / `rightbracket` / `backslash` / `semicolon` / `quote` / `comma` / `period` / `slash` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` | `fmsx_custom_keyboard_up` | `escape` / `left` / `up` / `right` / `down` / `shift` / `ctrl` / `graph` / `backspace` / `tab` / `space` / `capslock` / `select` / `home` / `enter` / `del` / `insert` / `country` / `dead` / `stop` / `f1` / `f2` / `f3` / `f4` / `f5` / `keypad0` / `keypad1` / `keypad2` / `keypad3` / `keypad4` / `keypad5` / `keypad6` / `keypad7` / `keypad8` / `keypad9` / `kp_multiply` / `kp_plus` / `kp_divide` / `kp_minus` / `kp_comma` / `kp_period` / `backquote` / `minus` / `equals` / `leftbracket` / `rightbracket` / `backslash` / `semicolon` / `quote` / `comma` / `period` / `slash` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` |
| Custom keyboard RetroPad down | `escape` / `left` / `up` / `right` / `down` ✅ / `shift` / `ctrl` / `graph` / `backspace` / `tab` / `space` / `capslock` / `select` / `home` / `enter` / `del` / `insert` / `country` / `dead` / `stop` / `f1` / `f2` / `f3` / `f4` / `f5` / `keypad0` / `keypad1` / `keypad2` / `keypad3` / `keypad4` / `keypad5` / `keypad6` / `keypad7` / `keypad8` / `keypad9` / `kp_multiply` / `kp_plus` / `kp_divide` / `kp_minus` / `kp_comma` / `kp_period` / `backquote` / `minus` / `equals` / `leftbracket` / `rightbracket` / `backslash` / `semicolon` / `quote` / `comma` / `period` / `slash` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` | `fmsx_custom_keyboard_down` | `escape` / `left` / `up` / `right` / `down` / `shift` / `ctrl` / `graph` / `backspace` / `tab` / `space` / `capslock` / `select` / `home` / `enter` / `del` / `insert` / `country` / `dead` / `stop` / `f1` / `f2` / `f3` / `f4` / `f5` / `keypad0` / `keypad1` / `keypad2` / `keypad3` / `keypad4` / `keypad5` / `keypad6` / `keypad7` / `keypad8` / `keypad9` / `kp_multiply` / `kp_plus` / `kp_divide` / `kp_minus` / `kp_comma` / `kp_period` / `backquote` / `minus` / `equals` / `leftbracket` / `rightbracket` / `backslash` / `semicolon` / `quote` / `comma` / `period` / `slash` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` |
| Custom keyboard RetroPad left | `escape` / `left` ✅ / `up` / `right` / `down` / `shift` / `ctrl` / `graph` / `backspace` / `tab` / `space` / `capslock` / `select` / `home` / `enter` / `del` / `insert` / `country` / `dead` / `stop` / `f1` / `f2` / `f3` / `f4` / `f5` / `keypad0` / `keypad1` / `keypad2` / `keypad3` / `keypad4` / `keypad5` / `keypad6` / `keypad7` / `keypad8` / `keypad9` / `kp_multiply` / `kp_plus` / `kp_divide` / `kp_minus` / `kp_comma` / `kp_period` / `backquote` / `minus` / `equals` / `leftbracket` / `rightbracket` / `backslash` / `semicolon` / `quote` / `comma` / `period` / `slash` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` | `fmsx_custom_keyboard_left` | `escape` / `left` / `up` / `right` / `down` / `shift` / `ctrl` / `graph` / `backspace` / `tab` / `space` / `capslock` / `select` / `home` / `enter` / `del` / `insert` / `country` / `dead` / `stop` / `f1` / `f2` / `f3` / `f4` / `f5` / `keypad0` / `keypad1` / `keypad2` / `keypad3` / `keypad4` / `keypad5` / `keypad6` / `keypad7` / `keypad8` / `keypad9` / `kp_multiply` / `kp_plus` / `kp_divide` / `kp_minus` / `kp_comma` / `kp_period` / `backquote` / `minus` / `equals` / `leftbracket` / `rightbracket` / `backslash` / `semicolon` / `quote` / `comma` / `period` / `slash` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` |
| Custom keyboard RetroPad right | `escape` / `left` / `up` / `right` ✅ / `down` / `shift` / `ctrl` / `graph` / `backspace` / `tab` / `space` / `capslock` / `select` / `home` / `enter` / `del` / `insert` / `country` / `dead` / `stop` / `f1` / `f2` / `f3` / `f4` / `f5` / `keypad0` / `keypad1` / `keypad2` / `keypad3` / `keypad4` / `keypad5` / `keypad6` / `keypad7` / `keypad8` / `keypad9` / `kp_multiply` / `kp_plus` / `kp_divide` / `kp_minus` / `kp_comma` / `kp_period` / `backquote` / `minus` / `equals` / `leftbracket` / `rightbracket` / `backslash` / `semicolon` / `quote` / `comma` / `period` / `slash` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` | `fmsx_custom_keyboard_right` | `escape` / `left` / `up` / `right` / `down` / `shift` / `ctrl` / `graph` / `backspace` / `tab` / `space` / `capslock` / `select` / `home` / `enter` / `del` / `insert` / `country` / `dead` / `stop` / `f1` / `f2` / `f3` / `f4` / `f5` / `keypad0` / `keypad1` / `keypad2` / `keypad3` / `keypad4` / `keypad5` / `keypad6` / `keypad7` / `keypad8` / `keypad9` / `kp_multiply` / `kp_plus` / `kp_divide` / `kp_minus` / `kp_comma` / `kp_period` / `backquote` / `minus` / `equals` / `leftbracket` / `rightbracket` / `backslash` / `semicolon` / `quote` / `comma` / `period` / `slash` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` |
| Custom keyboard RetroPad a | `escape` / `left` / `up` / `right` / `down` / `shift` / `ctrl` / `graph` / `backspace` / `tab` / `space` ✅ / `capslock` / `select` / `home` / `enter` / `del` / `insert` / `country` / `dead` / `stop` / `f1` / `f2` / `f3` / `f4` / `f5` / `keypad0` / `keypad1` / `keypad2` / `keypad3` / `keypad4` / `keypad5` / `keypad6` / `keypad7` / `keypad8` / `keypad9` / `kp_multiply` / `kp_plus` / `kp_divide` / `kp_minus` / `kp_comma` / `kp_period` / `backquote` / `minus` / `equals` / `leftbracket` / `rightbracket` / `backslash` / `semicolon` / `quote` / `comma` / `period` / `slash` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` | `fmsx_custom_keyboard_a` | `escape` / `left` / `up` / `right` / `down` / `shift` / `ctrl` / `graph` / `backspace` / `tab` / `space` / `capslock` / `select` / `home` / `enter` / `del` / `insert` / `country` / `dead` / `stop` / `f1` / `f2` / `f3` / `f4` / `f5` / `keypad0` / `keypad1` / `keypad2` / `keypad3` / `keypad4` / `keypad5` / `keypad6` / `keypad7` / `keypad8` / `keypad9` / `kp_multiply` / `kp_plus` / `kp_divide` / `kp_minus` / `kp_comma` / `kp_period` / `backquote` / `minus` / `equals` / `leftbracket` / `rightbracket` / `backslash` / `semicolon` / `quote` / `comma` / `period` / `slash` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` |
| Custom keyboard RetroPad b | `escape` / `left` / `up` / `right` / `down` / `shift` / `ctrl` / `graph` / `backspace` / `tab` / `space` / `capslock` / `select` / `home` / `enter` ✅ / `del` / `insert` / `country` / `dead` / `stop` / `f1` / `f2` / `f3` / `f4` / `f5` / `keypad0` / `keypad1` / `keypad2` / `keypad3` / `keypad4` / `keypad5` / `keypad6` / `keypad7` / `keypad8` / `keypad9` / `kp_multiply` / `kp_plus` / `kp_divide` / `kp_minus` / `kp_comma` / `kp_period` / `backquote` / `minus` / `equals` / `leftbracket` / `rightbracket` / `backslash` / `semicolon` / `quote` / `comma` / `period` / `slash` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` | `fmsx_custom_keyboard_b` | `escape` / `left` / `up` / `right` / `down` / `shift` / `ctrl` / `graph` / `backspace` / `tab` / `space` / `capslock` / `select` / `home` / `enter` / `del` / `insert` / `country` / `dead` / `stop` / `f1` / `f2` / `f3` / `f4` / `f5` / `keypad0` / `keypad1` / `keypad2` / `keypad3` / `keypad4` / `keypad5` / `keypad6` / `keypad7` / `keypad8` / `keypad9` / `kp_multiply` / `kp_plus` / `kp_divide` / `kp_minus` / `kp_comma` / `kp_period` / `backquote` / `minus` / `equals` / `leftbracket` / `rightbracket` / `backslash` / `semicolon` / `quote` / `comma` / `period` / `slash` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` |
| Custom keyboard RetroPad y | `escape` / `left` / `up` / `right` / `down` / `shift` / `ctrl` / `graph` / `backspace` / `tab` / `space` / `capslock` / `select` / `home` / `enter` / `del` / `insert` / `country` / `dead` / `stop` / `f1` / `f2` / `f3` / `f4` / `f5` / `keypad0` / `keypad1` / `keypad2` / `keypad3` / `keypad4` / `keypad5` / `keypad6` / `keypad7` / `keypad8` / `keypad9` / `kp_multiply` / `kp_plus` / `kp_divide` / `kp_minus` / `kp_comma` / `kp_period` / `backquote` / `minus` / `equals` / `leftbracket` / `rightbracket` / `backslash` / `semicolon` / `quote` / `comma` / `period` / `slash` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` ✅ / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` | `fmsx_custom_keyboard_y` | `escape` / `left` / `up` / `right` / `down` / `shift` / `ctrl` / `graph` / `backspace` / `tab` / `space` / `capslock` / `select` / `home` / `enter` / `del` / `insert` / `country` / `dead` / `stop` / `f1` / `f2` / `f3` / `f4` / `f5` / `keypad0` / `keypad1` / `keypad2` / `keypad3` / `keypad4` / `keypad5` / `keypad6` / `keypad7` / `keypad8` / `keypad9` / `kp_multiply` / `kp_plus` / `kp_divide` / `kp_minus` / `kp_comma` / `kp_period` / `backquote` / `minus` / `equals` / `leftbracket` / `rightbracket` / `backslash` / `semicolon` / `quote` / `comma` / `period` / `slash` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` |
| Custom keyboard RetroPad x | `escape` / `left` / `up` / `right` / `down` / `shift` / `ctrl` / `graph` / `backspace` / `tab` / `space` / `capslock` / `select` / `home` / `enter` / `del` / `insert` / `country` / `dead` / `stop` / `f1` / `f2` / `f3` / `f4` / `f5` / `keypad0` / `keypad1` / `keypad2` / `keypad3` / `keypad4` / `keypad5` / `keypad6` / `keypad7` / `keypad8` / `keypad9` / `kp_multiply` / `kp_plus` / `kp_divide` / `kp_minus` / `kp_comma` / `kp_period` / `backquote` / `minus` / `equals` / `leftbracket` / `rightbracket` / `backslash` / `semicolon` / `quote` / `comma` / `period` / `slash` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` ✅ / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` | `fmsx_custom_keyboard_x` | `escape` / `left` / `up` / `right` / `down` / `shift` / `ctrl` / `graph` / `backspace` / `tab` / `space` / `capslock` / `select` / `home` / `enter` / `del` / `insert` / `country` / `dead` / `stop` / `f1` / `f2` / `f3` / `f4` / `f5` / `keypad0` / `keypad1` / `keypad2` / `keypad3` / `keypad4` / `keypad5` / `keypad6` / `keypad7` / `keypad8` / `keypad9` / `kp_multiply` / `kp_plus` / `kp_divide` / `kp_minus` / `kp_comma` / `kp_period` / `backquote` / `minus` / `equals` / `leftbracket` / `rightbracket` / `backslash` / `semicolon` / `quote` / `comma` / `period` / `slash` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` |
| Custom keyboard RetroPad start | `escape` / `left` / `up` / `right` / `down` / `shift` / `ctrl` / `graph` / `backspace` / `tab` / `space` / `capslock` / `select` / `home` / `enter` / `del` / `insert` / `country` / `dead` / `stop` / `f1` ✅ / `f2` / `f3` / `f4` / `f5` / `keypad0` / `keypad1` / `keypad2` / `keypad3` / `keypad4` / `keypad5` / `keypad6` / `keypad7` / `keypad8` / `keypad9` / `kp_multiply` / `kp_plus` / `kp_divide` / `kp_minus` / `kp_comma` / `kp_period` / `backquote` / `minus` / `equals` / `leftbracket` / `rightbracket` / `backslash` / `semicolon` / `quote` / `comma` / `period` / `slash` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` | `fmsx_custom_keyboard_start` | `escape` / `left` / `up` / `right` / `down` / `shift` / `ctrl` / `graph` / `backspace` / `tab` / `space` / `capslock` / `select` / `home` / `enter` / `del` / `insert` / `country` / `dead` / `stop` / `f1` / `f2` / `f3` / `f4` / `f5` / `keypad0` / `keypad1` / `keypad2` / `keypad3` / `keypad4` / `keypad5` / `keypad6` / `keypad7` / `keypad8` / `keypad9` / `kp_multiply` / `kp_plus` / `kp_divide` / `kp_minus` / `kp_comma` / `kp_period` / `backquote` / `minus` / `equals` / `leftbracket` / `rightbracket` / `backslash` / `semicolon` / `quote` / `comma` / `period` / `slash` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` |
| Custom keyboard RetroPad select | `escape` / `left` / `up` / `right` / `down` / `shift` / `ctrl` / `graph` / `backspace` / `tab` / `space` / `capslock` / `select` / `home` / `enter` / `del` / `insert` / `country` / `dead` / `stop` / `f1` / `f2` / `f3` / `f4` ✅ / `f5` / `keypad0` / `keypad1` / `keypad2` / `keypad3` / `keypad4` / `keypad5` / `keypad6` / `keypad7` / `keypad8` / `keypad9` / `kp_multiply` / `kp_plus` / `kp_divide` / `kp_minus` / `kp_comma` / `kp_period` / `backquote` / `minus` / `equals` / `leftbracket` / `rightbracket` / `backslash` / `semicolon` / `quote` / `comma` / `period` / `slash` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` | `fmsx_custom_keyboard_select` | `escape` / `left` / `up` / `right` / `down` / `shift` / `ctrl` / `graph` / `backspace` / `tab` / `space` / `capslock` / `select` / `home` / `enter` / `del` / `insert` / `country` / `dead` / `stop` / `f1` / `f2` / `f3` / `f4` / `f5` / `keypad0` / `keypad1` / `keypad2` / `keypad3` / `keypad4` / `keypad5` / `keypad6` / `keypad7` / `keypad8` / `keypad9` / `kp_multiply` / `kp_plus` / `kp_divide` / `kp_minus` / `kp_comma` / `kp_period` / `backquote` / `minus` / `equals` / `leftbracket` / `rightbracket` / `backslash` / `semicolon` / `quote` / `comma` / `period` / `slash` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` |
| Custom keyboard RetroPad l | `escape` / `left` / `up` / `right` / `down` / `shift` / `ctrl` / `graph` / `backspace` / `tab` / `space` / `capslock` / `select` / `home` / `enter` / `del` / `insert` / `country` / `dead` / `stop` / `f1` / `f2` ✅ / `f3` / `f4` / `f5` / `keypad0` / `keypad1` / `keypad2` / `keypad3` / `keypad4` / `keypad5` / `keypad6` / `keypad7` / `keypad8` / `keypad9` / `kp_multiply` / `kp_plus` / `kp_divide` / `kp_minus` / `kp_comma` / `kp_period` / `backquote` / `minus` / `equals` / `leftbracket` / `rightbracket` / `backslash` / `semicolon` / `quote` / `comma` / `period` / `slash` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` | `fmsx_custom_keyboard_l` | `escape` / `left` / `up` / `right` / `down` / `shift` / `ctrl` / `graph` / `backspace` / `tab` / `space` / `capslock` / `select` / `home` / `enter` / `del` / `insert` / `country` / `dead` / `stop` / `f1` / `f2` / `f3` / `f4` / `f5` / `keypad0` / `keypad1` / `keypad2` / `keypad3` / `keypad4` / `keypad5` / `keypad6` / `keypad7` / `keypad8` / `keypad9` / `kp_multiply` / `kp_plus` / `kp_divide` / `kp_minus` / `kp_comma` / `kp_period` / `backquote` / `minus` / `equals` / `leftbracket` / `rightbracket` / `backslash` / `semicolon` / `quote` / `comma` / `period` / `slash` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` |
| Custom keyboard RetroPad r | `escape` / `left` / `up` / `right` / `down` / `shift` / `ctrl` / `graph` / `backspace` / `tab` / `space` / `capslock` / `select` / `home` / `enter` / `del` / `insert` / `country` / `dead` / `stop` / `f1` / `f2` / `f3` ✅ / `f4` / `f5` / `keypad0` / `keypad1` / `keypad2` / `keypad3` / `keypad4` / `keypad5` / `keypad6` / `keypad7` / `keypad8` / `keypad9` / `kp_multiply` / `kp_plus` / `kp_divide` / `kp_minus` / `kp_comma` / `kp_period` / `backquote` / `minus` / `equals` / `leftbracket` / `rightbracket` / `backslash` / `semicolon` / `quote` / `comma` / `period` / `slash` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` | `fmsx_custom_keyboard_r` | `escape` / `left` / `up` / `right` / `down` / `shift` / `ctrl` / `graph` / `backspace` / `tab` / `space` / `capslock` / `select` / `home` / `enter` / `del` / `insert` / `country` / `dead` / `stop` / `f1` / `f2` / `f3` / `f4` / `f5` / `keypad0` / `keypad1` / `keypad2` / `keypad3` / `keypad4` / `keypad5` / `keypad6` / `keypad7` / `keypad8` / `keypad9` / `kp_multiply` / `kp_plus` / `kp_divide` / `kp_minus` / `kp_comma` / `kp_period` / `backquote` / `minus` / `equals` / `leftbracket` / `rightbracket` / `backslash` / `semicolon` / `quote` / `comma` / `period` / `slash` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` |
| Custom keyboard RetroPad l2 | `escape` / `left` / `up` / `right` / `down` / `shift` / `ctrl` / `graph` ✅ / `backspace` / `tab` / `space` / `capslock` / `select` / `home` / `enter` / `del` / `insert` / `country` / `dead` / `stop` / `f1` / `f2` / `f3` / `f4` / `f5` / `keypad0` / `keypad1` / `keypad2` / `keypad3` / `keypad4` / `keypad5` / `keypad6` / `keypad7` / `keypad8` / `keypad9` / `kp_multiply` / `kp_plus` / `kp_divide` / `kp_minus` / `kp_comma` / `kp_period` / `backquote` / `minus` / `equals` / `leftbracket` / `rightbracket` / `backslash` / `semicolon` / `quote` / `comma` / `period` / `slash` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` | `fmsx_custom_keyboard_l2` | `escape` / `left` / `up` / `right` / `down` / `shift` / `ctrl` / `graph` / `backspace` / `tab` / `space` / `capslock` / `select` / `home` / `enter` / `del` / `insert` / `country` / `dead` / `stop` / `f1` / `f2` / `f3` / `f4` / `f5` / `keypad0` / `keypad1` / `keypad2` / `keypad3` / `keypad4` / `keypad5` / `keypad6` / `keypad7` / `keypad8` / `keypad9` / `kp_multiply` / `kp_plus` / `kp_divide` / `kp_minus` / `kp_comma` / `kp_period` / `backquote` / `minus` / `equals` / `leftbracket` / `rightbracket` / `backslash` / `semicolon` / `quote` / `comma` / `period` / `slash` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` |
| Custom keyboard RetroPad r2 | `escape` / `left` / `up` / `right` / `down` / `shift` / `ctrl` ✅ / `graph` / `backspace` / `tab` / `space` / `capslock` / `select` / `home` / `enter` / `del` / `insert` / `country` / `dead` / `stop` / `f1` / `f2` / `f3` / `f4` / `f5` / `keypad0` / `keypad1` / `keypad2` / `keypad3` / `keypad4` / `keypad5` / `keypad6` / `keypad7` / `keypad8` / `keypad9` / `kp_multiply` / `kp_plus` / `kp_divide` / `kp_minus` / `kp_comma` / `kp_period` / `backquote` / `minus` / `equals` / `leftbracket` / `rightbracket` / `backslash` / `semicolon` / `quote` / `comma` / `period` / `slash` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` | `fmsx_custom_keyboard_r2` | `escape` / `left` / `up` / `right` / `down` / `shift` / `ctrl` / `graph` / `backspace` / `tab` / `space` / `capslock` / `select` / `home` / `enter` / `del` / `insert` / `country` / `dead` / `stop` / `f1` / `f2` / `f3` / `f4` / `f5` / `keypad0` / `keypad1` / `keypad2` / `keypad3` / `keypad4` / `keypad5` / `keypad6` / `keypad7` / `keypad8` / `keypad9` / `kp_multiply` / `kp_plus` / `kp_divide` / `kp_minus` / `kp_comma` / `kp_period` / `backquote` / `minus` / `equals` / `leftbracket` / `rightbracket` / `backslash` / `semicolon` / `quote` / `comma` / `period` / `slash` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` |
| Custom keyboard RetroPad l3 | `escape` / `left` / `up` / `right` / `down` / `shift` / `ctrl` / `graph` / `backspace` / `tab` / `space` / `capslock` / `select` / `home` / `enter` / `del` / `insert` / `country` / `dead` / `stop` / `f1` / `f2` / `f3` / `f4` / `f5` ✅ / `keypad0` / `keypad1` / `keypad2` / `keypad3` / `keypad4` / `keypad5` / `keypad6` / `keypad7` / `keypad8` / `keypad9` / `kp_multiply` / `kp_plus` / `kp_divide` / `kp_minus` / `kp_comma` / `kp_period` / `backquote` / `minus` / `equals` / `leftbracket` / `rightbracket` / `backslash` / `semicolon` / `quote` / `comma` / `period` / `slash` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` | `fmsx_custom_keyboard_l3` | `escape` / `left` / `up` / `right` / `down` / `shift` / `ctrl` / `graph` / `backspace` / `tab` / `space` / `capslock` / `select` / `home` / `enter` / `del` / `insert` / `country` / `dead` / `stop` / `f1` / `f2` / `f3` / `f4` / `f5` / `keypad0` / `keypad1` / `keypad2` / `keypad3` / `keypad4` / `keypad5` / `keypad6` / `keypad7` / `keypad8` / `keypad9` / `kp_multiply` / `kp_plus` / `kp_divide` / `kp_minus` / `kp_comma` / `kp_period` / `backquote` / `minus` / `equals` / `leftbracket` / `rightbracket` / `backslash` / `semicolon` / `quote` / `comma` / `period` / `slash` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` |
| Custom keyboard RetroPad r3 | `escape` ✅ / `left` / `up` / `right` / `down` / `shift` / `ctrl` / `graph` / `backspace` / `tab` / `space` / `capslock` / `select` / `home` / `enter` / `del` / `insert` / `country` / `dead` / `stop` / `f1` / `f2` / `f3` / `f4` / `f5` / `keypad0` / `keypad1` / `keypad2` / `keypad3` / `keypad4` / `keypad5` / `keypad6` / `keypad7` / `keypad8` / `keypad9` / `kp_multiply` / `kp_plus` / `kp_divide` / `kp_minus` / `kp_comma` / `kp_period` / `backquote` / `minus` / `equals` / `leftbracket` / `rightbracket` / `backslash` / `semicolon` / `quote` / `comma` / `period` / `slash` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` | `fmsx_custom_keyboard_r3` | `escape` / `left` / `up` / `right` / `down` / `shift` / `ctrl` / `graph` / `backspace` / `tab` / `space` / `capslock` / `select` / `home` / `enter` / `del` / `insert` / `country` / `dead` / `stop` / `f1` / `f2` / `f3` / `f4` / `f5` / `keypad0` / `keypad1` / `keypad2` / `keypad3` / `keypad4` / `keypad5` / `keypad6` / `keypad7` / `keypad8` / `keypad9` / `kp_multiply` / `kp_plus` / `kp_divide` / `kp_minus` / `kp_comma` / `kp_period` / `backquote` / `minus` / `equals` / `leftbracket` / `rightbracket` / `backslash` / `semicolon` / `quote` / `comma` / `period` / `slash` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` |

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/libretro/fmsx-libretro/](https://github.com/libretro/fmsx-libretro/)
* **Libretro documentation**: [https://docs.libretro.com/library/fmsx/](https://docs.libretro.com/library/fmsx/)