---
title: CRT Scart and VGA on Raspberry Pi 5 / 500 / 4 / 400 / 3 / Zero 2
description: How to play Recalbox on your CRT screens on Raspberry Pi.
published: true
date: 2025-02-21T01:48:10.430Z
tags: crt, screen, cathodic, pixel perfect, vga, 240p, 224p
editor: markdown
dateCreated: 2021-09-10T23:13:51.200Z
---

## Presentation

As Recalbox users, you know how good it is to feel the nostalgia when playing again the games that rocked our childhood.

Playing with a PS4 controller on the living room OLED TV already allows us to find and share this pleasure of retro gaming.

However, some of us have an insatiable thirst that forces us to go further and further in the search for an experience as close as possible to the original.

In this never-ending search, the display, or image of the game is probably the most important point.

And what could be closer to the original than playing on the CRT screens of the time?

## Recalbox on your CRT screen

Recalbox is able to reproduce the exact resolution and frequencies of most consoles and arcade games of the time, on your CRT TV.

Get a [**Recalbox RGB DUAL**](https://www.recalbox.com/recalbox-rgb-dual), VGA666 and a SCART cable, a Pi2Scart or a RGBPi.

For Recalbox RGB Dual, it's automatic.
For other devices : once the device is connected to your Rasbperry Pi 4 and to the TV, edit the config file `/crt/recalbox-crt-options.cfg` in the `[RECALBOX]` partition mounted when you plug the SD card in your computer:

```
# For recalboxrgbdual it's automatic
# For vga666
adapter.type = vga666
# For pi2scart
adapter.type = pi2scart
# For rgbpi
adapter.type = rgbpi
```

You can also use the "Advanced configuration => Recalbox CRT" menu in frontend.

**That's all**, after restarting your Recalbox, the system is configured to send the image directly to your TV.

------------------

## Hardware Compatibility

* **Recalbox RGB DUAL**: CRT TV, PVM, VGA monitor
The Recalbox RGB DUAL is **the plug and play solution to run Recalbox on CRT**. Moreover, it is a card developed by the Recalbox team.
* **VGA666**: CRT TV with modified cable, VGA CRT monitor, PVM
The VGA666 allows you to connect a VGA cable (old CRT PC monitors). With a modified VGA->PERITEL cable, it is fully compatible with CRT TVs.
* **Pi2Scart**: CRT TV, PVM
A H.A.T. for Raspberry Pi 3 and 4
* **RGBPi**: CRT TV, PVM
A scart cable to connect Recalbox to your CRT.

## Intermediate configuration

In the configuration file `/crt/recalbox-crt-options.cfg` in the `[RECALBOX]` partition:

```
# Image shift to the left (negative) or right (positive)
mode.offset.horizontal = -2
# Offset the image up (negative) or down (positive)
mode.offset.vertical = -2
```

## Advanced configuration

The default configuration of the display modes per system and per arcade game can be found in the folder `/recalbox/system/configs/crt/` (`system/configs/crt/` in the `[SHARE]` partition)

* `modes.txt` lists the display modes that can be used
* `system.txt` defines for each system the mode to use
* `arcade_games.txt` defines for each arcade game the mode to use

To override or replace these configurations, you just have to create the corresponding files in the `/recalbox/share/system/configs/crt/` folder (`system/configs/crt/` in the `[SHARE]` partition).

These override files should only contain the modes, systems or games you want to override or add.

File structure:
┣ 📁 recalbox
┃ ┣ 📁 system
┃ ┃ ┣ 📁 configs
┃ ┃ ┣ 📁 crt
┃ ┃ ┃ ┣ 🗒 arcade_games.txt (read only)
┃ ┃ ┃ ┣ 🗒 modes.txt (read only)
┃ ┃ ┃ ┣ 🗒 systems.txt (read only)
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 crt
┃ ┃ ┃ ┃ ┃ ┣ 🗒 arcade_games.txt
┃ ┃ ┃ ┃ ┃ ┣ 🗒 modes.txt
┃ ┃ ┃ ┃ ┃ ┣ 🗒 systems.txt

See [override example](#example-of-overriding)

### File structure

#### `modes.txt`

The `modes.txt` file contains one mode per line, with its identifier, in the following format:

```ini
MODE_ID,MODE,REFRESH_RATE
```

Example:

```ini
snes:nes:ntsc:224@60.0988,1920 1 80 184 312 224 1 9 3 24 0 0 0 60 0 39001717 1,60.0988
```

Here the MODE_ID is `snes:nes:ntsc:224@60.0988` and the MODE is `1920 1 80 184 312 224 1 9 3 24 0 0 0 60 0 39001717 1`

#### `systems.txt`

The `systems.txt` file allows for each system, to configure a display mode by region, and to select the size of the display area:

```ini
SYSTEM_ID,REGION,SCREEN_TYPE,INTERLACED,MODE_ID,VIEWPORT_WIDTH,VIEWPORT_HEIGHT
```

Example:

```ini
vectrex,all,15kHz,progressive,standard:ntsc:240@60,0,0
```

This line lets Recalbox know that the Super Nintendo (`snes`), when loading an NTSC (non-European) game, will use the display mode identified by `snes:nes:ntsc:224@60.0988` (in `modes.txt`).

The next two zeros let Recalbox set the best value for the height and width of the image.

#### `arcade_games.txt`

The `arcade_games.txt` file allows to select the display mode for each arcade game, depending on the core (or emulator) on which it will be launched. It also allows to change the size of the display area:

```ini
GAME_ID,CORE_ID,MODE_ID,VIEWPORT_WIDTH,VIEWPORT_HEIGHT,ROTATION
```

Example:

```ini
dino,fbneo,snes:nes:ntsc:224@60.0988,0,0
```

The GAME_ID is the name of the game files, without the `.zip`.
The CORE_ID is the name of the core used by Recalbox.

This line lets Recalbox know that the game `dino.zip`, launched on RetroArch with the core `fbneo`, will use the `arcade:224@59.637405` mode.

### Example of overriding

The override allows you to add or replace modes, systems or arcade games.

> All overrides are new files, located in `/recalbox/share/system/configs/crt/` directory (`system/configs/crt/` in the `[SHARE]` partition).
> {.is-info}

Each of the override modes, systems or games is added to the base configuration, or replaces it if the id already exists.

Let's say I want to change the display mode of `Cadillac and Dinosaurs` on arcade, and change it to 224 lines at 60Hz.

In the modes.txt file, there is a mode corresponding to 224 lines at 60Hz. It has MODE_ID `standard:all:240@60`.

So I will create the file `/recalbox/share/system/configs/crt/arcade_games.txt` (`system/configs/crt/arcade_games.txt` in the `[SHARE]` partition) and put a single line in it:

```ini
dino,fbneo,standard:all:240@60,0,0,0
```

The VIEWPORT_WIDTH and VIEWPORT_HEIGHT allow you to resize the displayed image. They do not change the display mode of the screen.
You may want to add specific settings for systems or arcade games.

Let's set the screen width of Metal Slug 3 to 1800 pixels in `/recalbox/share/system/configs/crt/arcade_games.txt` (`system/configs/crt/arcade_games.txt` in the `[SHARE]` partition) once and for all

```ini
mslug3,fbneo,arcade:224@59.185606,1800,0,0
```

Restart `Metal Slug 3` and you can see that the screen width has decreased.