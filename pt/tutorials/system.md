---
title: 🎛️ Sistema
description: 
published: true
date: 2021-12-23T21:06:01.908Z
tags: tutorial, sistema
editor: markdown
dateCreated: 2021-12-23T21:04:56.736Z
---

Você vai encontrar tudo sobre a base, ou seja, o sistema: como acessá-la ou como modificar certas opções, por exemplo.

Aqui estão as categorias disponíveis:

[Acesso](access)
[Backups e clonagem](backups-and-clone)
[Instalação](installation)
[Modificação](modification)