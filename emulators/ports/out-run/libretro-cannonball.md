---
title: Libretro Cannonball
description: 
published: true
date: 2024-07-25T08:16:20.063Z
tags: libretro, out run, cannonball
editor: markdown
dateCreated: 2021-06-29T22:18:05.306Z
---

Libretro Cannonball is a recreation of the OutRun game engine written by Chris White in 2014.

## ![](/emulators/license.svg) License

This core is under a [**non-commercial**](https://github.com/libretro/cannonball/blob/master/docs/license.txt) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Features

| Feature | Supporté |
| :---: | :---: |
| Saves | ✔ |
| Core Options | ✔ |
| Controls | ✔ |
| Remapping | ✔ |

## ![](/emulators/bios.svg) BIOS

>**No bios is required.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Supported extensions

La rom doit avoir l'extension :

* .88

### Location

Put your roms like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 ports
┃ ┃ ┃ ┃ ┣ 📁 Out Run
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **outrun.88**

### Files list

There is the complete files list to play Out Run:

| Filename | SHA1 signature | MAME game with the file |
| --- | --- | --- |
| epr-10187.88 | 01c8a819587a66d2ee4d255656e36fa0904377b0 | outrun.zip |
| epr-10327a.76 | f715bde96c73ed47035acf5a41630fdeb41bb2f9 | outrun.zip |
| epr-10328a.75 | a4e3cfca4d803e72bc4fcf91ab00e21bf3f8959f | outrun.zip |
| epr-10329a.58 | 57d5219bd0e2fd886217e37e8773fd76be9b40eb | outrun.zip |
| epr-10330a.57 | 2136c9572e26b7ae6de402c0cd53174407cc6018 | outrun.zip |
| epr-10380b.133 | 31e870f307f44eb4f293b607123b623beee2bc3c | outrun.zip |
| epr-10381a.132 | bf3ff05bbf81bdd44567f3b9bb4919ed4a499624 | outrunra.zip |
| epr-10381b.132 | bf3ff05bbf81bdd44567f3b9bb4919ed4a499624 | outrun.zip |
| epr-10382b.118 | 69236cf9f27691dee290c79db1fc9b5e73ea77d7 | outrun.zip |
| epr-10373b.117 | 1970895145ad8b5735f66ed8c837d9d453ce9b23 | outrun.zip |
| mpr-10371.9 | 21320f945f7c8e990c97c9b1232a0f4b6bd00f8f | outrun.zip |
| mpr-10372.13 | a3746a2da077a8df4932348f650a061f413e8430 | outrun.zip |
| mpr-10373.10 | 3a9ce8547cd43b7b04abddf9a9ab5634e0bbfaba | outrun.zip |
| mpr-10374.14 | 9c8509fbed170b4ac74c169da573393e54774f49 | outrun.zip |
| mpr-10375.11 | e5d8c67e020608edd24ba87b7687b2ac2483ee7f | outrun.zip |
| mpr-10376.15 | a5f2532613f33a64441e0f75443c10ba78dccc6e | outrun.zip |
| mpr-10377.12 | 9f7210cb4153ac9029a785dcd4b45f4513a4b008 | outrun.zip |
| mpr-10378.16 | 4399030a155caf71f2dec7f75c4b65531ab53576 | outrun.zip |
| opr-10185.11 | a554d4b68e71861a0d0da4d031b3b811b246f082 | outrun.zip |
| opr-10186.47 | a554d4b68e71861a0d0da4d031b3b811b246f082 | outrun.zip |
| opr-10188.71 | f70dd3a6362c314adef313b064102f7a250401c8 | outrun.zip |
| opr-10189.70 | f467a6b807694d5832a985f5381c170d24aaee4e | outrun.zip |
| opr-10190.69 | a3c581d2b438630d0d4c39481dcfd85681c9f889 | outrun.zip |
| opr-10191.68 | 7c9027416d4122791ba53782fe2230cf02b7d506 | outrun.zip |
| opr-10192.67 | 686bdf44d45c1d6002622f6658f037735382f3e0 | outrun.zip |
| opr-10193.66 | 417ce1d7242884640c5b14f4db8ee57cde7d085d | outrun.zip |
| opr-10230.104 | 03697b892f911177968aa40de6c5f464eb0258e7 | outrun.zip |
| opr-10231.103 | 8e1237b640a6f26bdcbfd5e201dadb2687c4febb | outrun.zip |
| opr-10232.102 | e3477961d19e694c97643066534a1f720e0c4327 | outrun.zip |
| opr-10266.101 | 09164e858ebeedcff4d389524ddf89e7c216dcae | outrun.zip |
| opr-10267.100 | a7e0143dee5a47e679fd5155e58e717813912692 | outrun.zip |
| opr-10268.99 | b3480714b11fc49b449660431f85d4ba92f799ba | outrun.zip |
| outrun.88 | 01c8a819587a66d2ee4d255656e36fa0904377b0 | empty file |

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Accessing to options

You can configure various options in two different ways.

* Via RetroArch menu:

┣ 📁 RetroArch Menu
┃ ┣ 📁 Core options
┃ ┃ ┣ 🧩 Name_option

* Via the file `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core options

| Options | Options values | Variables (for configuration override) | Variables values |
| --- | --- | --- | --- |
| Menu At Start | `Enabled` ✅ / `Disabled` | `canonball_menu_enabled` | `ON` / `OFF` |
| Menu Road Scroll Spee | `5` / `10` / `15` / `20` / `25` / `30` / `40` / `50` ✅ / `60` / `70` / `80` / `90` / `100` / `150` / `200` / `300` / `400` / `500` | `canonball_menu_road_scroll_speed` | `5` / `10` / `15` / `20` / `25` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` / `150` / `200` / `300` / `400` / `500` |
| Video Widescreen Mode | `Enabled` ✅ / `Disabled` | `canonball_video_widescreen` | `ON` / `OFF` |
| Video High-Resolution Mode | `Disabled` ✅ / `Enabled` | `canonball_video_hires` | `OFF` / `ON` |
| Video Framerate | `Smooth (60)` ✅ / `Ultra Smooth (120)` / `Original (60/30)` | `canonball_video_fps` | `Smooth (60)` / `Ultra Smooth (120)` / `Original (60/30)` |
| Advertise Sound | `Enabled` ✅ / `Disabled` | `canonball_sound_advertise` | `ON` / `OFF` |
| Preview Music | `Enabled` ✅ / `Disabled` | `canonball_sound_preview` | `ON` / `OFF` |
| Fix Samples (use opr-10188.71f) | `Enabled` ✅ / `Disabled` | `canonball_sound_fix_samples` | `ON` / `OFF` |
| Gear Mode | `Manual` ✅ / `Manual Cabinet` / `Manual 2 Buttons` / `Automatic` | `canonball_gear` | `Manual` / `Manual Cabinet` / `Manual 2 Buttons` / `Automatic` |
| Analog Controls (off to allow digital speed setup) | `Enabled` ✅ / `Disabled` | `canonball_analog` | `ON` / `OFF` |
| Digital Steer Speed | `1` / `2` / `3` ✅ / `4` / `5` / `6` / `7` / `8` / `9` | `canonball_steer_speed` | `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` |
| Digital Pedal Speed | `1` / `2` / `3` / `4` ✅ / `5` / `6` / `7` / `8` / `9` | `canonball_pedal_speed` | `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` |
| Time | `Easy (80s)` ✅ / `Normal (75s)` / `Hard (72s)` / `Very Hard (70s)` / `Infinite Time` | `canonball_dip_time` | `Easy (80s)` / `Normal (75s)` / `Hard (72s)` / `Very Hard (70s)` / `Infinite Time` |
| Traffic | `Normal` ✅ / `Hard` / `Very Hard` / `No Traffic` / `Easy` | `canonball_dip_traffic` | `Normal` / `Hard` / `Very Hard` / `No Traffic` / `Easy` |
| Freeplay Mode | `Disabled` ✅ / `Enabled` | `canonball_freeplay` | `OFF` / `ON` |
| Use Japanese Tracks Version | `Disabled` ✅ / `Enabled` | `canonball_jap` | `OFF` / `ON` |
| Use Prototype Stage 1 | `Disabled` ✅ / `Enabled` | `canonball_prototype` | `OFF` / `ON` |
| Objects Limit Enhanced | `Enabled` ✅ / `Disabled` | `canonball_level_objects` | `ON` / `OFF` |
| Original Traffic Patterns Randomization | `Enabled` ✅ / `Disabled` | `canonball_randomgen` | `ON` / `OFF` |
| Force AI To Play | `Disabled` ✅ / `Enabled` | `canonball_force_ai` | `OFF` / `ON` |
| Fix Original Game Bugs | `Enabled` ✅ / `Disabled` | `canonball_fix_bugs` | `ON` / `OFF` |
| Fix Timing Bugs | `Disabled` ✅ / `Enabled` | `canonball_fix_timer` | `OFF` / `ON` |
| Display Debug Info For LayOut | `Disabled` ✅ / `Enabled` | `canonball_layout_debug` | `OFF` / `ON` |
| New Attract | `Enabled` ✅ / `Disabled` | `canonball_new_attract` | `ON` / `OFF` |
| Time Trial Laps | `1` / `2` / `3` ✅ / `4` / `5` | `canonball_ttrial_laps` | `1` / `2` / `3` / `4` / `5` |
| Time Trial Traffic | `0` / `1` / `2` / `3` ✅ / `4` / `5` / `6` / `7` / `8` | `canonball_ttrial_traffic` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` |
| Continuous Mode Traffic Amount | `0` / `1` / `2` / `3` ✅ / `4` / `5` / `6` / `7` / `8` | `canonball_cont_traffic` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` |

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/libretro/cannonball/](https://github.com/libretro/cannonball)
* **Official source code**: [https://github.com/djyt/cannonball/](https://github.com/djyt/cannonball)
* **Official documentation**: [https://github.com/djyt/cannonball/wiki](https://github.com/djyt/cannonball/wiki)