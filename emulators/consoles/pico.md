---
title: Pico
description: 
published: true
date: 2024-07-24T16:04:27.668Z
tags: consoles, sega, pico
editor: markdown
dateCreated: 2024-07-24T16:01:04.638Z
---

![](/emulators/consoles/pico.svg){.align-center}

## Technical data

* **Manufacturer**: Sega
* **Year of release**: 1993
* **CPU**: 16-bit Motorola 68000 @ 7.6 MHz
* **Resolution**: 320x224 pixels NTSC, 320x240 PAL

## Presentation

The Sega Pico, also known as Kids Computer Pico, is an educational video game console by Sega Toys. The Pico was released in June 1993 in Japan and November 1994 in North America and Europe, later reaching China in 2002.

Marketed as "edutainment", the main focus of the Pico was educational video games for children between 3 and 7 years old. Releases for the Pico were focused on education for children and included titles supported by licensed franchised animated characters, including Sega's own Sonic the Hedgehog series.

Though the Pico was sold continuously in Japan through the release of the Beena, in North America and Europe the Pico was less successful and was discontinued in early 1998, later being re-released by Majesco Entertainment. Overall, Sega claims sales of 3.4 million Pico consoles and 11.2 million game cartridges, and over 350,000 Beena consoles and 800,000 cartridges. It was succeeded by the Advanced Pico Beena, which was released in Japan in 2005.

## Emulators

[Libretro PicoDrive](libretro-picodrive)