---
title: GameCube / Wii
description: 
published: true
date: 2021-08-08T08:50:28.007Z
tags: nintendo, gamecube, wii, gc
editor: markdown
dateCreated: 2021-08-07T23:49:00.677Z
---

You will be able to find here information to better play your GameCube and Wii games.

Here are the available tutorials:

[Custom high-resolution texture packs](custom-hires-textures-packs)
[Define Dolphin Bar position](define-dolphinbar-position)
[Multiplayer games on Wii](multiplayer-games-on-wii)
[Set multilingual Wii/Gamecube games in your language](set-multilingual-wii-gamecube-games-in-your-language)
[Use Wiimotes as controllers](use-wiimotes-as-controllers)
[Use original Gamecube controllers](use-original-gamecube-controllers)
[Use original Wiimotes controllers](use-original-wiimotes-controllers)