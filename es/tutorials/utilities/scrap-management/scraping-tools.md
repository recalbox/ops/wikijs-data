---
title: Lista de herramientas de scraping
description: 
published: true
date: 2023-09-17T07:42:05.466Z
tags: scrap, scraping, programas, lista, herramientas
editor: markdown
dateCreated: 2023-09-17T07:42:05.466Z
---

Trabajo en curso... tentativa de reunir todas las herramientas/progrmas de scraping en esta página.

| Programa | Windows | Linux | macOS | Consola | Arcade | Fuentes |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| [Scrapeur interne] |  | recalbox |  | 🎮 | 👾 | [ScreenScraper](https://screenscraper.fr/), [TheGamesDB](http://thegamesdb.net/), [MameDB-mirror](http://mamedb.blu-ferret.co.uk/) |
| [UXS] | ✔️ |  |  | 🎮 | 👾 | [ScreenScraper](https://screenscraper.fr/) |
| [sselph's scraper] | ✔️ | ✔️ | ✔️ | 🎮 | 👾 | [ScreenScraper](https://screenscraper.fr/), [TheGamesDB](http://thegamesdb.net/), [MameDB-mirror](http://mamedb.blu-ferret.co.uk/), [ArcadeItalia](http://adb.arcadeitalia.net/), [OpenVGDB](https://github.com/OpenVGDB/OpenVGDB/) |
| [Skraper] | ✔️ | ✔️ | ✔️ | 🎮 | 👾 | [ScreenScraper](https://screenscraper.fr/) |
| [GameList Editor] | ✔️ |  |  | 🎮 | 👾 | [ScreenScraper](https://screenscraper.fr/) |
| [ARRM] | ✔️ |  |  | 🎮 | 👾 | [ScreenScraper](https://screenscraper.fr/), [TheGamesDB](http://thegamesdb.net/), [MameDB-mirror](http://mamedb.blu-ferret.co.uk/), [HFSDb](https://db.hfsplay.fr/), [LaunchBox](https://gamesdb.launchbox-app.com/), [Steam](https://store.steampowered.com/), [GOG](https://www.gog.com/), [IGDB](https://www.igdb.com/), [MobyGames](https://www.mobygames.com/), [GameTDB](https://www.gametdb.com/)|
| [mamescraper] | ✔️ | ✔️ | ✔️ |  | 👾 | bigode, [ArcadeItalia](http://adb.arcadeitalia.net/) |
| [SkyScraper] |  | ✔️ | ✔️ | 🎮 |  | [ScreenScraper](https://screenscraper.fr/), [TheGamesDB](http://thegamesdb.net/), [ArcadeItalia](http://adb.arcadeitalia.net/), [OpenRetro](http://openretro.org), [WorldOfSpectrum](http://worldofspectrum.org), [IGDB](http://igdb.com), [MobyGames](http://mobygames.com) |

## Scrapeur interne

[https://gitlab.com/recalbox/recalbox-emulationstation](https://gitlab.com/recalbox/recalbox-emulationstation)

 [![Recalbox 4.1 - Scrapeur interne](https://i.ytimg.com/vi/xd1i1mJdkjU/maxresdefault.jpg)](https://www.youtube.com/watch?v=xd1i1mJdkjU)  
\* Cliquez sur l'image pour visualiser une vidéo de présentation.

## Universal XML Scraper (UXS)

[https://github.com/Universal-Rom-Tools/Universal-XML-Scraper](https://github.com/Universal-Rom-Tools/Universal-XML-Scraper)

## sselph's scraper

[https://github.com/sselph/scraper](https://github.com/sselph/scraper)  
Conseil : utilisez un [fastscraper](https://github.com/paradadf/recaltools/tree/master/fastscraper) avec !

## Skraper

[https://www.skraper.net/](https://www.skraper.net/)

## GameList Editor

[https://github.com/NeeeeB/GameList\_Editor](https://github.com/NeeeeB/GameList_Editor)

## Another Recalbox Roms Manager (ARRM)

[http://jujuvincebros.fr/telechargements2/file/10-arrm-another-recalbox-roms-manager](http://jujuvincebros.fr/telechargements2/file/10-arrm-another-recalbox-roms-manager)

## mamescraper

[https://github.com/pdrb/mamescraper](https://github.com/pdrb/mamescraper)

## SkyScraper

[https://github.com/muldjord/skyscraper](https://github.com/muldjord/skyscraper)