---
title: Arduboy
description: 
published: true
date: 2024-07-26T08:23:41.345Z
tags: fantasy, arduboy, 9.1+
editor: markdown
dateCreated: 2023-06-17T22:08:37.104Z
---

![](/emulators/fantasy/arduboy.svg){.align-center}

## Fiche technique

* **Développeur** : Kevin Bates
* **Année de sortie** : 2015
* **CPU** : ATMega32u4 @ 8 MHz
* **RAM** : 2,5KB RAM
* **Affichage** : écran OLED monochrome de 1.3 pouces et de résolution 128 x 64 pixels
* **Stockage** : 32KB Flash
* **Son** : 2 Ch. Piezo Speaker

## Présentation

L'arduboy est une console portable de jeux vidéo 8 bits basée sur la plateforme open source Arduino.

C'est initialement un projet Kickstarter de Kevin Bates qui a vu le jour début 2015 et qui a obtenu plus de 400 000$ avec plus de 7 000 contributeurs.

## Émulateurs

[Libretro Arduous](libretro-arduous)