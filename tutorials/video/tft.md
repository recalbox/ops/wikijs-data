---
title: TFT
description: 
published: true
date: 2021-09-04T22:23:30.591Z
tags: tft
editor: markdown
dateCreated: 2021-05-21T08:11:53.239Z
---

Si vous utilisez un écran TFT, vous devriez trouver votre bonheur ici.

Voici les tutoriels disponibles :

[Configurer votre écran TFT HDMI](hdmi-tft-screen)
[Configurez votre petit écran TFT sur le bus DPI](tft-lcd-on-dpi-bus)
[Configurez votre petit écran TFT sur le bus SPI](tft-lcd-on-spi-bus)
[Connectez un afficheur LCD avec I2C à votre Recalbox](lcd-display-with-i2c)
[Deuxième miniTFT pour les scrapes et les vidéos](second-minitft-or-ogst-for-scraps-and-videos)