---
title: Libretro Snes9X 2010
description: 
published: true
date: 2021-10-18T16:13:20.337Z
tags: libretro, snes, super nintendo, snes9x 2010
editor: markdown
dateCreated: 2021-08-03T19:31:18.804Z
---

**Libretro Snes9X** is an upstream port of Snes9x, a portable Super Nintendo Entertainment System emulator to Libretro.

## ![](/emulators/license.svg) License

This core is under [**non-commercial**](https://github.com/libretro/snes9x/blob/master/LICENSE) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Features



## ![](/emulators/bios.svg) BIOS

>**No bios is required.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Supported extensions

Roms must have the extension:

* .fig
* .gd3
* .sfc
* .smc
* .swc
* .zip
* .7z

This system supports compressed roms in .zip/.7z format. However, be careful, it is only an archive.

The files contained in the .zip/.7z must correspond to the extensions mentioned above.
Moreover, each .zip/.7z file must contain only one compressed rom.

### Location

Put the roms like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 snes
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.zip**

>The roms using **No-Intro** format are highly recommended.
{.is-success}

>For more information about roms, go to [this tutorial](./../../../tutorials/games/generalities/isos-and-roms)!
{.is-info}

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Accessing to options

You can configure various options in two different ways.

* Via RetroArch menu:

┣ 📁 RetroArch Menu
┃ ┣ 📁 Core options
┃ ┃ ┣ 🧩 Name_option

* Via the file `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core options

| Options | Options values | Variables (for configuration override) | Variables values |
| --- | --- | --- | --- |
| SuperFX Overclock | `5 MHz (Underclock)` / `8 MHz (Underclock)` / `9 MHz (Underclock)` / `10 MHz (Default)` ✅ / `11 MHz` / `12 MHz` / `13 MHz` / `14 MHz` / `15 MHz` / `20 MHz` / `30 MHz` / `40 MHz` | `snes9x_2010_overclock` | `5 MHz (Underclock)` / `8 MHz (Underclock)` / `9 MHz (Underclock)` / `10 MHz (Default)` / `11 MHz` / `12 MHz` / `13 MHz` / `14 MHz` / `15 MHz` / `20 MHz` / `30 MHz` / `40 MHz` |
| Reduce Slowdown (Hack, Unsafe) | `Disabled` ✅ / `Light` / `Compatible` / `Max` | `snes9x_2010_overclock_cycles` | `disabled` / `light` / `compatible` / `max` |
| Reduce Flickering (Hack, Unsafe) | `Disabled` ✅ / `Enabled` | `snes9x_2010_reduce_sprite_flicker` | `disabled` / `enabled` |

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/libretro/snes9x2010/](https://github.com/libretro/snes9x2010/)
* **Libretro documentation**: [https://docs.libretro.com/library/snes9x_2010/](https://docs.libretro.com/library/snes9x_2010/)