---
title: Cavestory
description: 
published: true
date: 2024-07-26T08:28:39.558Z
tags: ports, cavestory
editor: markdown
dateCreated: 2023-09-04T12:34:21.550Z
---

![](/emulators/ports/cavestory.svg){.align-center}

## Detalles técnicos

* **Desarrollador:** Studio Pixel 
* **Editor:** Studio Pixel 
* **Diseñador:** Daisuke Amaya 
* **Guionista:** Daisuke Amaya 
* **Compositor:** Daisuke Amaya 
* **Plataforma:** Windows 
* **Fecha de lanzamiento**:20/12/2004 
* **Género**: Plataforma - Aventura 
* **Mode:** Solo

## Presentación

Cave Story es un videojuego de aventuras y plataformas gratuito lanzado en 2004 para PC, desarrollado durante 5 años por **Daisuke "Pixel" Amaya** en su tiempo libre. La jugabilidad es como una combinación de varios juegos de NES / Famicom.

Se han añadido varios finales extra, varias batallas contra jefes, montones de objetos para coleccionar, todo tipo de bonificaciones secretas extra y desafíos especiales, formas de jugar variadas y considerablemente diferentes en función de tus acciones, funciones de clasificación de la competición y un valor de rejugabilidad extremadamente alto. Para ser un juego gratuito, también consume bastante tiempo.

La fluida jugabilidad de este juego parece una combinación de varios juegos de NES / Famicom con controles sólidos, un gran elenco de personajes y diálogos, una trama interesante, buena música y bellas ilustraciones en 2D a la antigua usanza.

Consulta [http://www.cavestory.org/](http://www.cavestory.org/) para más información.

## Emuladores

[Libretro NXEngine](libretro-nxengine)