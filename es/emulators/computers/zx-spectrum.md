---
title: ZX Spectrum
description: 
published: true
date: 2024-07-26T12:53:20.695Z
tags: sinclair, zx, spectrum, ordenadores
editor: markdown
dateCreated: 2023-09-03T13:03:56.440Z
---

![](/emulators/computers/zxspectrum.svg){.align-center}

## Datos técnicos

* **Fabricante**: Sinclair Research
* **Sistema operativo**: Sinclair BASIC
* **Año de lanzamiento**: 1982
* **Unidades vendidas**: 5 millones
* **Procesador**: Z80 a 3,5 MHz y equivalente
* **Memoria**: 16 KB / 48 KB / 128 KB
* **Soportes**: lector de casetes, disquetera de 3" en Spectrum +3

## Presentación

El **ZX Spectrum** es un pequeño ordenador personal lanzado por el fabricante británico de ordenadores Sinclair Research en 1982. Basado en el procesador Zilog Z80 a 3,5 MHz, el Spectrum estaba equipado con 16 ó 48 kB de RAM (también existía una ampliación para pasar de 16 a 48 kB).

Con un precio de 125 libras para el modelo de 16k y de 175 libras para el modelo de 48k, el Spectrum fue el primer ordenador personal de consumo en el Reino Unido, similar en tamaño al Commodore 64 en Estados Unidos (que también fue rival del Spectrum en Europa). Timex comercializó en EE.UU. una versión ligeramente modificada del Spectrum, con cuerpo plateado y teclas de plástico duro, bajo el nombre de TS2068. Disponía de una extensión ROM adicional de 8k, con la interfaz ZX 1 o ZX 2 que se vendía por separado: un puerto para cartuchos, dos puertos para joystick y un microprocesador de audio AY-3-8912, así como comandos BASIC adicionales para controlar este hardware (STICK, SOUND).

Los modelos posteriores incluyeron el ZX Spectrum +, con un teclado mejorado, y el ZX Spectrum 128, con mejor sonido y 128 kB de RAM. Tras la compra de Sinclair Research por parte de Amstrad en 1986, se lanzaron dos versiones adicionales: el ZX Spectrum +2, con una grabadora de casetes incluida en la máquina, y el ZX Spectrum +3, con una disquetera de 3" incluida.

Varios de los desarrolladores de juegos más importantes de la actualidad comenzaron su carrera en el ZX Spectrum, como Ultimate Play the Game (ahora Rareware), Peter Molyneux (ex Bullfrog Productions) y Shiny Entertainment.

Se produjeron varios clones, sobre todo en Europa del Este (Elwro, HC85) y Sudamérica. Algunos siguieron produciéndose hasta bien entrada la década de 1990, como el Didaktik y el Sprinter de Peters Plus Ltd.

## Emuladores

[Libretro FBNeo](libretro-fbneo)
[Libretro Fuse](libretro-fuse)