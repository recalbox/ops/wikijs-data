---
title: Connecter une manette IPEGA
description: 
published: true
date: 2021-09-04T22:45:56.306Z
tags: ipega, manette
editor: markdown
dateCreated: 2021-05-21T08:34:58.030Z
---

## Astuces pour les manettes IPEGA

* Connectez-vous via [SSH](./../../../tutorials/system/access/root-access-terminal-cli) à votre Recalbox et montez la [partition du système](./../../../tutorials/system/access/remount-partition-with-write-access) en lecture/écriture.
* Trouvez la référence de votre manette via la commande :

```shell
evtest
```

Exemple : `PG-9055`

* Adaptez `ATTRS{name}=="PG-9055"` en fonction de la référence de votre manette et copiez/collez l'ensemble de ces commandes dans SSH, puis valider par la touche `Entrée`.

```text
echo 'SUBSYSTEM=="input", ATTRS{name}=="PG-9055", MODE="0666", ENV{ID_INPUT_JOYSTICK}="1"'>>/etc/udev/rules.d/99-joysticks-exotics.rules
reboot
```

## Connexion en Bluetooth

* Faites un reset de votre manette IPEGA, si vous l'utilisez avec un autre périphérique.
* Mettez le mode `Home+X`
* Exécuter la recherche de manette bluetooth dans le menu Emulationstation en appuyant sur `START` > `RÉGLAGES MANETTES`.

Vous pouvez vérifier si votre IPEGA est déjà dans la liste du fichier `/etc/udev/rules.d/99-joysticks-exotics.rules`.

Si ce n'est pas le cas, vous pouvez [ouvrir une issue](https://gitlab.com/recalbox/recalbox/-/issues) pour qu'elle soit ajouté à la prochaine mise à jour de Recalbox.