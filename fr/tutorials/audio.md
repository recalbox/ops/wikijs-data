---
title: 🔊 Audio
description: 
published: true
date: 2021-11-20T19:58:02.261Z
tags: audio, tutoriel
editor: markdown
dateCreated: 2021-05-21T07:51:49.330Z
---

Vous trouverez ici des tutoriels concernant les sorties audio de vos boards.

Voici les tutoriels disponibles :

[Audio HiFiBerry](hifiberry-audio)
[Codeur rotatif via GPIO (Contrôle du volume numérique)](rotary-encoder-via-gpio-digital-volume-control)
[Corriger les problèmes de son dans EmulationStation](fix-emulationstation-sound-issues)
[Sortie audio analogique sur Pi Zero](analog-audio-output-on-pi-zero)