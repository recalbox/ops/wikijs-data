---
title: Videopac+
description: 
published: true
date: 2021-08-09T15:06:46.875Z
tags: computers, philips, videopac+
editor: markdown
dateCreated: 2021-07-28T21:43:01.033Z
---

![](/emulators/computers/videopacplus.svg){.align-center}

## Technical data

* **Manufacturer**: Philips
* **Year of release**: 1983
* **CPU**: Intel 8048 @ 5.91MHz
* **RAM**: 6 KB + 192 Bytes
* **Audio**: 1 channel, 8 sounds
* **ROM**: 1024 bytes
* **Resolution**: 320x238 pixels with 16 colors

## Presentation

The Philips Videopac+ G7400 is a third generation video game console released in limited quantities in 1983, and only in Europe; an American release of the **Odyssey³ Command Center** was planned but never arrived. The G7400 was the successor to the Philips Videopac G7000, the European counterpart to the American Magnavox Odyssey². The system had perfectly matching background and foreground graphics.

The G7400 could play 3 different types of games: all normal G7000 games, special G7000 games with enhanced background graphics that only appeared on the G7400 and G7400 compatible games with high resolution backgrounds and sprites.

## Emulators

[Libretro O2EM](libretro-o2em)