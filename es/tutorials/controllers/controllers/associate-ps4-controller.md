---
title: Asociación de un mando PS4
description: Cómo utilizar un mando de PS4 con Recalbox
published: true
date: 2024-04-20T10:24:13.125Z
tags: mando, ps4, bluetoo, asociación
editor: markdown
dateCreated: 2024-04-20T10:24:13.125Z
---

## Introducción

Los mandos de PS4 pueden ser un rompecabezas a la hora de reconocerlos. Este tutorial debería ayudaros.

## Procedimiento de asociación

>El siguiente procedimiento ha sido probado con un mando PS2 versión 2. Puede ser diferente para una versión 1.
{.is-info}

A continuación os explicamos cómo reconocer un mando de PS4:

- Encended el mando con vuestra PS4 desenchufada (sólo es necesario encender el mando).
- Mientras el mando está encendido, debajo del mando veréis un pequeño botón accesible mediante un clip. Púlsadlo para reiniciarlo.

![Localisation du bouton reset](/tutorials/controllers/controllers/ps4/resetbutton.jpeg){.center}

- Una vez que el mando ha sido reiniciado y apagado, pulsad simultáneamente los botones `PS` y `SHARE` durante 5 segundos.

![Boutons PS et SHARE pour l'appairage](/tutorials/controllers/controllers/ps4/ds4pairingguide.png){.center}

Ahora vuestro mando debería estar emparejado y funcionando sin necesidad de reconfigurarlo en Recalbox.

Fuente original del proceso: https://forum.recalbox.com/topic/8920/a-lire-manettes-ps4
Fuente de las imágenes: [aquí](https://helpdeskgeek.com/how-to/how-to-reset-a-ps4-controller/) y [aquí](https://randocity.com/2015/07/31/how-to-pair-your-ps4-controller-wirelessly/)

## Cómo apagar el mando

Por supuesto, el mando puede apagarse. Para ello, mantened pulsado el botón PS durante 10 segundos.