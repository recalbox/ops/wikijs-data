---
title: FBNeo
description: 
published: true
date: 2021-09-23T23:18:14.656Z
tags: arcade, finalburn, neo
editor: markdown
dateCreated: 2021-09-23T23:18:14.656Z
---

![](/emulators/arcade/fbneo.svg){.align-center}

## História

O Final Burn Alpha (FB Alpha ou FBA) é um emulador de jogos arcade. Ele toma seu nome do jogo "After Burner" que foi o único jogo emulado logo no início de sua criação; ele é originalmente baseado no emulador FinalBurn, iniciado em agosto de 2000 por Dave3 (também conhecido como FinalDave).

O projeto se apagou e das cinzas deste nasceu FinalBurn Alpha (agora enterrado desde o verão de 2019). Renasceu uma última vez sob a sigla FBNeo ou FinalBurn Neo, após a súbita e recente extinção dos direitos da FBA por um de seus principais membros: Barry Harris.
Atualmente, inclui mais de 30 sistemas/consoles emulados e nada menos que 10.000 jogos.

## Emuladores

[Libretro FBNeo](libretro-fbneo)
[PiFBA](pifba)