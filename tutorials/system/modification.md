---
title: Modification
description: 
published: true
date: 2021-09-04T21:54:19.485Z
tags: modification
editor: markdown
dateCreated: 2021-05-21T08:10:14.583Z
---

Cette partie vous concernera si vous avez certaines modifications spécifiques à effectuer.

Voici les tutoriels disponibles :

[Ajouter votre propre script au démarrage](add-your-own-startup-script)
[Augmenter le courant disponible sur les ports USB](increase-available-power-on-usb)
[Modifier les fichiers de configuration de démarrage](configtxt-configuration)