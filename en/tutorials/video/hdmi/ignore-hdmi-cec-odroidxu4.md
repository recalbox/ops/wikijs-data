---
title: Ignore HDMI CEC on Odroid XU4
description: 
published: true
date: 2021-08-06T17:13:29.758Z
tags: hdmi, ignore, cec, odroid, xu4
editor: markdown
dateCreated: 2021-08-06T17:13:29.758Z
---

By default, HDMI CEC is enabled on Odroid XU4. This means that the card can be turned off automatically when you turn off your TV / change HDMI source. This is not recommended for Recalbox as it can corrupt the system.

Here is a workaround to disable this feature and ignore HDMI CEC commands.

* Make the [boot partition](./../../system/access/remount-partition-with-write-access) writeable.
* Type the following command lines:

```shell
cd /boot/
nano boot.ini
```

* Add this just after `setenv fdtbin exynos5422-odroidxu3.dtb` :

```ini
# --- HDMI CEC Configuration ---
# ------------------------------
setenv cecenable "false" # false or true
# set to true to enable HDMI CEC
```

Add this right after `fatload mmc 0:1 ${fdtbin_addr_r} ${fdtbin}` :

```ini
fdt addr 0x440000
if test "${cecenable}" = "false"; then fdt rm /cec@101B0000; fi
```

Your `boot.ini` file should look like this:

```ini
ODROIDXU-UBOOT-CONFIG

# U-Boot Parameters (DO NOT MODIFY)
setenv version 3.10
setenv zimage zImage
setenv fdtbin exynos5422-odroidxu3.dtb

# --- HDMI CEC Configuration ---
# ------------------------------
setenv cecenable "false" # false or true
# set to true to enable HDMI CEC

setenv zimage_addr_r 0x40008000
setenv fdtbin_addr_r 0x44000000

setenv fdt_high "0xffffffff"

# Default boot argument
setenv bootrootfs "root=/dev/mmcblk0p2 rootwait ro"
setenv console "console=ttySAC2,115200n8 consoleblank=0 vt.global_cursor_default=0"

setenv bootargs "${bootrootfs} ${console}"

# boot commands
fatload mmc 0:1 ${zimage_addr_r} ${zimage}
fatload mmc 0:1 ${fdtbin_addr_r} ${fdtbin}

fdt addr 0x44000000
if test "${cecenable}" = "false"; then fdt rm /cec@101B0000; fi

bootz ${zimage_addr_r} - ${fdtbin_addr_r}"
```

* Save and exit, then restart.