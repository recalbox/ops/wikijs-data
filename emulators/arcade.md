---
title: Arcade
description: Arcade systems
published: true
date: 2024-06-21T10:34:31.840Z
tags: arcade
editor: markdown
dateCreated: 2021-06-29T16:55:58.827Z
---

![](/emulators/arcade/arcade-logo.png)

![](/emulators/arcade/arcade-bartop.svg)

## WARNING.

Here, we enter a bit of a technical part of Recalbox. **WE'LL KEEP IT SIMPLE.**

As for the consoles, the arcade in Recalbox is separated into 2 elements: 
- emulators *(included and updated in your Recalbox)*,
- "games", to be added.

...EXCEPT...
Unlike consoles, the arcade does not launch "games", but "complete systems" 
*(= you emulate the whole arcade, not just the game data)*

![](/advanced-usage/arcade/board-to-zip.png)

Arcade emulation is still evolving today.
Emulators are getting better and better, and so are the dumps of arcade systems.

With each new version of Recalbox, we offer you the latest versions of emulators. So be sure to add THE RIGHT VERSIONS of the arcade systems. *(called "ROMSET")*

[**More info on Arcade emulation in Recalbox HERE**](./../advanced-usage/arcade-in-recalbox)

## ARCADE "BY DEFAULT"

| Emulator | Romset |
| :---: | :---: |
| PiFBA | ROMSET FBA 0.2.96.71 |
| MAME | ROMSET MAME 0.78-0.188 |
| FBNEO | ROMSET FBNeo 1.0.0.03 |
| NEO-GEO | ROMSET FBNeo 1.0.0.03 |
| NAOMI | ROMSET MAME 0.258 |
| NAOMI 2 | ROMSET MAME 0.258 |
| NAOMI GD | ROMSET MAME 0.258 |
| ATOMISWAVE | ROMSET MAME 0.258 |
| SEGA MODEL 3 | ROMSET MAME 0.258 |

✅ *Please note: not all Arcade systems may be available on your hardware.*
*[**Please check your hardware compatibility HERE.**](./../hardware-compatibility)*

✅ *Which romset to choose for your hardware?*
*[**View the compatibility of Romsets in Recalbox**](./../tutorials/games/generalities/isos-and-roms/romsets-for-recalbox)*

✅ *For advanced users, it is possible to change the default MAME emulator, to try to run newer games. You will then need a MORE RECENT ROMSET*
*[**TUTO to change the default emulator HERE.**](./../tutorials/games/guides/nintendo-64/game-core-compatibility)*

## ARCADE "ADVANCED USER"

*Alternative MAME emulators:*

| Emulator | Romset |
| :---: | :---: |
| LIBRETRO-MAME 2000 | ROMSET 0.35B7 |
| LIBRETRO-MAME 2003 | ROMSET MAME 0.78 |
| LIBRETRO-MAME 2003-Plus | ROMSET MAME 0.78-0.188 |
| LIBRETRO-MAME 2010 | ROMSET MAME 0.139 |
| LIBRETRO-MAME 2015 | ROMSET MAME 0.160 |
| LIBRETRO-MAME | ROMSET MAME 0.258 |

[**More info on Advanced Arcade emulation HERE**](./../advanced-usage/arcade-in-recalbox)

## COMMON PROBLEMS

| Problem | BAD ROM | BAD EMULATOR | BAD CHD | BAD DUMP | BAD MAPPING |
| :--- | :---: | :---: | :---: | :---: | :---: |
| **My game won't start** | ✅ | ✅ | ✅ | ✅ |  |
| **Game sound problem** |  | ✅ |  | ✅ |  |
| **The game I'm using is buggy, glitchy** |  | ✅ |  | ✅ |  |
| **The game launches but the keys are wrong** |  |  |  |  | ✅ |

[**More info on Advanced Arcade Emulation HERE**](./../advanced-usage/arcade-in-recalbox)