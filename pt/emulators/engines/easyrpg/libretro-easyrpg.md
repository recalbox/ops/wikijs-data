---
title: Libretro EasyRPG
description: 
published: true
date: 2024-06-02T17:13:23.219Z
tags: libretro, easyrpg
editor: markdown
dateCreated: 2021-10-15T00:15:54.766Z
---

**Libretro EasyRPG** é um port do emulador **EasyRPG**.

## ![](/emulators/license.svg) Licença

Este núcleo é licenciado sob uma licença [**GPLv3**](https://github.com/EasyRPG/Player/blob/master/COPYING).

## ![](/emulators/compatibility.png) Compatibilidade

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Recursos

| Recurso | Suportado |
| :---: | :---: |
| Reiniciar | ✔ |
| Capturas de tela | ✔ |
| Salvar | ✔ |
| Controles | ✔ |

## ![](/emulators/bios.svg) BIOS

> **Não é necessária nenhuma BIOS.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Extensões suportadas

As ROMs deve ter a extensão:

* .ini

### Local

Coloque as roms assim:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 easyrpg
┃ ┃ ┃ ┃ ┣ 📁 jogo.rpg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **RPG_RT.ini**

Cada jogo possui seu próprio conjunto de arquivos e deve ter o arquivo **RPG_RT.ini** que o acompanha. Cada jogo deve estar em sua pasta específica.

### Lista de jogos compatíveis

Você pode encontrar a lista de jogos compatíveis seguindo [este link](https://community.easyrpg.org/t/compatibility-list/283).

## ![](/emulators/advanced-configuration.png) Configuração avançada do emulador

> **Este núcleo não tem essa opção.**
{.is-success}

## ![external-links.png](/emulators/external-links.png) Links externos

* **Código fonte utilizado** : [https://github.com/EasyRPG/Player/](https://github.com/EasyRPG/Player/)
* **Documentação Libretro** : [https://docs.libretro.com/library/easyrpg/](https://docs.libretro.com/library/easyrpg/)
* **Projeto RPG Maker 2000** : [https://community.easyrpg.org/t/rpg-maker-2000-preservation-project/199](https://community.easyrpg.org/t/rpg-maker-2000-preservation-project/199)
* **RPG Maker**: [https://rpgmaker.net/](https://rpgmaker.net/)