---
title: Libretro PrBoom
description: 
published: true
date: 2024-07-25T22:50:55.478Z
tags: libretro, doom, prboom
editor: markdown
dateCreated: 2023-09-11T14:42:14.329Z
---

**PrBoom** es la culminación de años de trabajo de varias personas y proyectos sobre el código fuente de Doom.  
PrBoom incluye el trabajo de los siguientes proyectos:

* La versión original del código fuente de Doom
* DosDoom
* Boom](http://www.teamtnt.com/boompubl/boom2.htm)
* MBF
* LxDoom](http://lxdoom.linuxgames.com/)
* lSDLDoom

PrBoom es un port del motor gráfico de Doom. Puede ejecutar Doom 1 y Doom 2.

## ![](/emulators/license.svg) Licencia

El core posee la siguiente licencia [**GPLv2**](https://github.com/libretro/libretro-prboom/blob/master/COPYING).

## ![](/emulators/compatibility.png) Compatibilidad

 | RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | 
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | 

## ![](/emulators/features.png) Funcionalidades

| Funcionalidad | ¿Soportada? |
| :---: | :---: |
| Reinicio | ✔ |
| Capturas de pantalla | ✔ |
| Grabaciones | ✔ |
| Opciones del core | ✔ |
| RetroArch Cheats | ✔ |
| Cheats nativos| ✔ |
| Controles | ✔ |
| Remapping | ✔ |

## ![](/emulators/bios.svg) BIOS

>La bios se encuentra ya presente en el directorio `/recalbox/share/bios/prboom/prboom.wad`
{.is-info}

## ![](/emulators/roms.png) Roms

>Los ficheros **WADs** son como las "roms".
{.is-info}

>Disponed vuestros ficheros en el directorio `/recalbox/share/roms/ports/doom/`
{.is-warning}

>Utilizad sólo minusculas en los ficheros de tipo `.wad`
{.is-warning}

### Algunos datos sobre los WADs

Aunque todos los ficheros tienen la extensión `.wad`, existen varios tipos:

* **IWAD**: se trata de un `.wad` que contiene todos los datos y se ejecuta solo.  
  Los más conocidos son

  * `doom1.wad` para las versiones shareware de **DOOM.**.
  * `doom.wad` para versiones comerciales de **DOOM.**.
  * `doom.wad o doomu.wad` para las versiones de **The Ultimate DOOM.**.
  * doom2.wad para versiones de **DOOM II - Hell on Earth.**.
  * tnt.wad` para versiones de **Final DOOM - TNT Evilution**.
  * Plutonia.wad` para las versiones de **Final DOOM - The Plutonia Experiment**.
  * `sigil.wad` para las versiones del último episodio de **DOOM - SIGIL**.

* **PWAD**: se trata de un `.wad` 'add-on' que necesitará un IWAD para funcionar (por ejemplo: casi todos los niveles amateur creados por la comunidad Doom).

Los WAD también deben ser compatibles con PrBoom. Algunos mods van muy lejos en la modificación **(como Brutal Doom, Doom Raider, Golden Eye,...)** pero **no** funcionarán con PrBoom. Tendrás que informarte sobre el WAD al que quieres jugar.  

Si un WAD sustituye a un único nivel, no será necesariamente el nivel 1 del episodio 1. Tendrás que llegar al nivel correcto o usar un cheat para acceder a él directamente.

### Lista de compatibilidad de los juegos WADs

| Fichero WAD | Nombre del fichero - MD5 | Estado |
| :--- | :--- | :--- |
| **SIGIL** |  |  |
| SIGIL (v1.21) | `SIGIL_v1_21.WAD` - 743d6323cb2b9be24c258ff0fc350883 | ✅ en Recalbox |
| **MASTER LEVEL for DOOM II (Registered)** |  |  |
| Master Levels for Doom II (Classic Complete) (PlayStation 3) | `MASTERLEVELS.WAD` - 84cb8640f599c4a17c8eb526f90d2b7a | ❌ |
| Master Levels for Doom II | `20x fichiers WADs séparés` | ❌ |
| **FINAL DOOM (Registred)** |  |  |
| Final Doom - Evilution | `TNT.WAD` - 4e158d9953c79ccf97bd0663244cc6b6 | ✅ |
| Final Doom - The Plutonia Experiment *v1.9* | `PLUTONIA.WAD` - 75c8cf89566741fa9d22447604053bd7 | ✅ |
| **DOOM II - Hell on Earth (Registered)** |  |  |
| Doom II - Hell on Earth (v1.9) | `DOOM2.WAD` - 25e1459ca71d321525f84628f45ca8cd | ✅ |
| **The Ultimate DOOM (Registered)** |  |  |
| Ultimate Doom, The *"v1,9ud"* | `DOOM.WAD, UDOOM.WAD, DOOMU.WAD` - c4fe9fd920207691a9f493668e0a2083 | ✅ |
| **DOOM (Registred)** |  |  |
| Doom (v1.9) | `DOOM.WAD` - 1cd63c5ddff1bf8ce844237f580e9cf3 | ✅ |
| **DOOM (Shareware)** |  |  |
| Doom (v1.9) (Demo) | `DOOM1.WAD` - f0cefca49926d00903cf57551d901abe | ✅ en Recalbox |

## Jugar SIGIL :

>El fichero `doom.wad` en el directorio roms es en realidad la versión Shareware 1.9 del juego **Doom** y debería renombrarse a `doom1.wad`.
{.is-info}

* Para jugar a **SIGIL** debe reemplazar esta versión Shareware `doom.wad` por la versión **Registrada** de **The Ultimate DOO v1.9.**.

>¡Ninguna otra versión de `doom.wad` podrá ejecutar **SIGIL** !
{.is-warning}

* Para hacer esto:
  * Cambie el nombre del fichero existente en la raíz de `roms\ports\doom.wad` a `doom1.wad` (o bórrelo).
  * Pega el nuevo fichero `doom.wad` de **The Ultimate DOOM v1.9** en lugar del antiguo.
  * Reinicia una actualización de tu lista de juegos y ejecuta **SIGIL**.

>Cuando lances **SIGIL**, el lanzamiento de los cuatro primeros episodios lanzará invariablemente el primer episodio. Para lanzar **SIGIL**, simplemente selecciona el episodio SIGIL.
{.is-info}

>Para obtener una versión **Registrada** de **DOOM**, debes tener los disquetes o CD-Rom originales, o conseguirlos en STEAM o GOG.
{.is-danger}

### Emplazamiento

Disponed vuestras bios como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 ports
┃ ┃ ┃ ┃ ┣ 📁 Doom
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **\*.wad**

## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

>Para conservar las configuraciones personalizadas cuando se produce una actualización del sistema Recalbox, os aconsejamos utilizar las [Sobrecargas de configuración](./../../../advanced-usage/configuration-override).
{.is-info}

### Acceder a las opciones

Podéis configurar las opciones del core de dos formas diferentes.

* Via el Menú RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via el fichero `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Opciones del core

| Opción | Valor de la opción | Variable (para la sobrecarga) | Valores posibles |
| --- | --- | --- | --- |
| Résolution interne (Redémarrage requis) | `320x200` ✅ / `640x400` / `960x600` / `1280x800` / `1600x1000` / `1920x1200` / `2240x1400` / `2560x1600` | `prboom-resolution` | `320x200` / `640x400` / `960x600` / `1280x800` / `1600x1000` / `1920x1200` / `2240x1400` / `2560x1600` |
 | Souris active lors de l'utilisation d'une manette | `Désactivé` ✅ / `Activé` | `prboom-mouse_on` | `disabled` / `enabled` |
 | Rechercher dans les dossiers parents pour les IWADs | `Activé` ✅ / `Désactivé` | `prboom-find_recursive_one` | `enabled` / `disabled` |
 | Effets de vibration | `Désactivé` ✅ / `Activé` | `prboom-rumble` | `disabled` / `enabled` |
 | Deadzone analogique (%) | `0` / `5` / `10` / `15` ✅ / `20` / `25` / `30` | `prboom-analog_deadzone` | `0` / `5` / `10` / `15` / `20` / `25` / `30` |
 | Taille du cache | `8 MB` / `12 MB` / `16 MB` / `24 MB` / `32 MB` / `48 MB` / `64 MB` / `128 MB` / `256 MB` | `prboom-purge_limit` | `8` / `12` / `16` / `24` / `32` / `48` / `64` / `128` / `256` |


## ![](/emulators/external-links.png) Enlaces externos

* **Código fuente utilizado** : [https://github.com/libretro/libretro-prboom/](https://github.com/libretro/libretro-prboom)
* **Documentación Libretro** :  [https://docs.libretro.com/library/prboom/](https://docs.libretro.com/library/prboom/)
* **Página web oficial** : [http://prboom.sourceforge.net/about.html](http://prboom.sourceforge.net/about.html)