---
title: Añadir un botón on/off a vuestra Recalbox
description: 
published: true
date: 2024-04-18T13:26:44.937Z
tags: botón, encendido, apagado
editor: markdown
dateCreated: 2023-09-18T06:17:08.336Z
---

>Se puede añadir un botón on/off a Recalbox. El botón puede ser un pulsador (también llamado "momentary switch") o un botón con dos posiciones ON/OFF. 
{.is-info}

## Para conectar el botón al GPIO de la Raspberry Pi

* Conectad un pin al **GPIO3** (el **quinto** gpio desde arriba a la izquierda), y el otro a tierra a su derecha (el **sexto** gpio).

* Finalmente, hay que habilitar el soporte del botón en el fichero  [recalbox.conf] (./../../basic-usage/getting-started/recalboxconf-file) agregando una de estas líneas:

  * `system.power.switch=PIN56ONOFF` pour un bouton ON/OFF 
  * `system.power.switch=PIN56PUSH` pour un bouton poussoir

Et vous avez une Recalbox qui peut être éteinte/allumée par un simple bouton !

## Añadir un botón de reset y un LED.

>También exite una opción para añadir un botón de reset junto con un LED.
{.is-info}

* En el fichero recalbox.conf, añadid/descomentad (borrando el ; al principio de la línea) lo siguiente:

* `system.power.switch=PIN356ONOFFRESET` para un switch ON/OFF
* `system.power.switch=PIN356PUSHRESET` para un pulsador de encendido

* Para conectar el botón de reset a los GPIO :
   * Conectad un pin al GPIO2 (el tercer gpio desde arriba a la izquierda).
   * Conectad el otro pin a la misma masa que el botón de encendido/apagado (el sexto gpio).
* Conectad el LED al GPIO14 (el octavo gpio) y a la masa.

#### En resumen

* _Power+_ en **GPIO 3** (**PIN 5**) 
* _Reset+_ en **GPIO 2** (**PIN 3**) 
* _LED+_ en **GPIO 14** (**PIN 8**) 
* _Power-_, _Reset-_ y _Led-_ en una de las masas **masses** (**PIN 6, 9, 14, 20, 25, 30, 34 o 39**)

>Tened en cuenta que esto sólo funciona con un pulsador para hacer reset y un botón ON/OFF para la alimentación (encendido/apagado).
{.is-info}

## Utilización

### Con el botón de POWER en modo PUSH (PIN 5 + GROUND)

* Presión corta _(apagado)_ : Enciende Recalbox 
* Presión corta _(encendido)_ : Sale del emulador y vuelve a EmulationStation 
* Presión larga _(encendido)_ : Apaga limpiamente Recalbox _(equivale a hacer `SELECT` > `Apagar` con el mando)_

### Con el botón POWER en modo ON/OFF (PIN 5 + GROUND)

* Activación del botón _(apagado)_ : Enciende vuestra Recalbox 
* Désactivación del botón _(encendido)_ : Apaga de forma ordenada vuestra Recalbox _(equivale a hacer `SELECT` > `Apagar` desde EmulationStation)_

### Con el botón de RESET (PIN 3 + GROUND)

* Presión corta _(apagado)_ : - 
* Presión corta _(encendido)_ : Resetea el juego, como en la época de las consolas
* Presión larga _(encendido)_ : Reinicia Recalbox _(equivale a hacer `SELECT` > `Reiniciar` con el mando)_