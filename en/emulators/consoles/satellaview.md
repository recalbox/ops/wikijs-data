---
title: Satellaview
description: Nintendo Satellaview
published: true
date: 2021-10-03T21:01:20.702Z
tags: consoles, nintendo, satellaview, bs-x
editor: markdown
dateCreated: 2021-07-28T15:50:07.688Z
---

![](/emulators/consoles/satellaview.svg){.align-center}

## Technical data

* **Manufacturer**: Nintendo
* **Year of release**: 1995
* **Units sold**: 2 millions
* **ROM**: 1024 kB
* **RAM**: 512 kB
* **Internal memory**: 256 kB memory flash
* **Media**: BS-X Application Cartridge + optional 8MB Memory Pak

## Presentation

The **Satellaview** is an extension of the Super Famicom, marketed only in Japan from 1995.

Also known under the initials **BS-X**, this game system, delivered with a "BS-X" type cartridge and an 8 Mbit memory card, brought some additional technical capabilities to the Super Famicom as well as a download service for games, codes and information. All the data was in fact transiting through a satellite TV station named St.GIGA.

The official launch of Satellaview took place on April 23, 1995. The first game to be broadcast was BS Zelda. As the name suggests, it was a remake of the first game in The Legend of Zelda series, updated with many additions and changes.

During the first year of operation of the Satellaview, it was possible to consult "online" information as well as audio and video reports. The content was updated regularly and the games in episodes had their new part added after 7 days.  
But by 1996, the 32-bit generation of consoles was already well established (more than one million PlayStation units sold). Even then, Satellaview's sales were beginning to be considered insufficient. The service provided by the St.GIGA company suffered, as many reruns replaced the original content. In 1997, the programs were cut by two hours. In 1998, a Satellaview owner found himself with another hour less.

In April 1999, following financial disagreements between Nintendo and St.GIGA, notably concerning the restructuring of the large debt accumulated by St.GIGA, Nintendo definitively stopped supplying the service with new programs and withdrew from the project. In the absence of new programs, St.GIGA nevertheless kept the service running until June 2000 but, due to the lack of new programs, could only offer reruns. The service was definitively closed on the evening of June 30, 2000.

## Emulators

[Libretro SNES9x](libretro-snes9x)
[Libretro Mesen_S](libretro-mesen_s)