---
title: Frotz
description: 
published: true
date: 2024-07-24T21:01:16.417Z
tags: 9.0+, z-machine, frotz
editor: markdown
dateCreated: 2023-02-26T18:35:43.771Z
---

**Frotz** is an interpretor of Infocom games and from others developers for  **Z-machine**.

## ![](/emulators/license.svg) License

This core is under [**GPL-2.0+**](https://gitlab.com/DavidGriffith/frotz/-/blob/master/COPYING) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Features

| Feature | Supported |
| :---: | :---: |
| Sauvegardes | ✔ |

## ![](/emulators/bios.svg) BIOS

>**No bios is required.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Supported extensions

Roms must have the extension:

* .dat
* .z1
* .z2
* .z3
* .z4
* .z5
* .z6
* .z7
* .z8

### Location

Put the roms like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 zmachine
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.z1**

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

> **This core have no option!**
{.is-success}

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://gitlab.com/DavidGriffith/frotz/](https://gitlab.com/DavidGriffith/frotz/)