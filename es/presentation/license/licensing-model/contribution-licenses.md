---
title: Licencias de contribución
description: 
published: true
date: 2023-08-31T14:49:17.243Z
tags: licencia, contribución
editor: markdown
dateCreated: 2023-08-31T14:49:17.243Z
---

Para los proyectos de desarrollo de software y las obras creativas, se aplica el régimen habitual de licencias "inbound=outbound", a menos que se indique lo contrario.

Como se explica claramente en las [guías de código abierto] de GitHub (https://opensource.guide/fr/legal/), esto significa que una licencia de código abierto sirve implícitamente como licencia de entrada (de los colaboradores) y de salida (a otros colaboradores y usuarios).

Esto significa que al contribuir a uno de nuestros proyectos, aceptas los términos de su licencia y envías tu contribución bajo los mismos términos.