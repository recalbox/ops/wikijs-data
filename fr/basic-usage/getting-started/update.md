---
title: Mise à jour
description: 
published: true
date: 2024-11-11T07:45:21.669Z
tags: mise à jour
editor: markdown
dateCreated: 2021-05-21T07:54:57.100Z
---

## Processus de mise à jour

Voici les **possibilités de mises à jour** de Recalbox.  Nous espèrons que ce résumé vous aidera et vous apportera les réponses que vous cherchez.

## En fonction de votre situation...

### Est il possible de faire la mise à jour de Recalbox x.x vers Recalbox 7 ?

_**Non**, il n'est pas possible de faire la mise à jour vers la nouvelle version de Recalbox 7. Une installation propre est requise. Voir plus bas pour obtenir les instructions._

### Que faire si j'utilise un thème autre que celui fourni par défaut ?

* Assurez-vous d'être à jour au niveau des systèmes, des fichiers de configuration de votre thème.
* Contactez l'auteur de votre thème ou le sujet de votre thème afin de vérifier si une nouvelle version à jour est disponible.
* Utilisez un thème personnalisé en 720p sur Raspberry Pi. En 1080p, votre Raspberry Pi va souffrir.
* Nous n'assurons pas le support pour les thèmes personnalisés non officiel de Recalbox. Les créateurs des thèmes sont les seuls à supporter leurs thèmes.
* Merci de mettre le thème de Recalbox par défaut pour vérifier vos bugs avant de poster votre problème sur le forum.

### Que faire si j'utilise des OVERLAYS ?

Si vous avez crée vos propres overlays, RetroArch a ajouté un nouveau aspect ratio qui décale certains overlays :

* Testez 22 ou 23 sur cette ligne dans les configurations des overlays :

```ini
aspect_ratio_index = 21
```

### Les systèmes arcades

Recalbox met régulièrement à jour les cores des émulateurs arcade MAME & FinalBurn Neo. De ce fait, il faut adapter vos romsets :

* Les fichiers dat globaux se trouvent dans `/recalbox/share/arcade`. Vous trouverez plus d'informations sur [cette page](./../../tutorials/games/generalities/isos-and-roms/sort-and-update-your-roms).

* Pour réaliser des dat parent-only, neogeo parent only etc... il vous suffira d'utiliser ce [tutoriel datutil](./../../tutorials/utilities/dat-management/datutil) pour les créer.

* Pour connaitre les versions des romsets arcade en cours, vous pouvez consulter [cette page](./../../emulators/arcade).

### Votre Raspberry Pi dans un boitier

* Assurez-vous d'avoir une bonne ventilation.
* Un bon câble d'alimentation.
* Si vous avez des soucis, sortez votre Raspberry Pi de votre boitier et tester Recalbox 7 à nu.

### Nos conseils :

* Avoir une sauvegarde de votre share sur votre PC ou disque dur externe.
* Utiliser un support amovible (clé usb ou disque dur externe), vous comprendrez rapidement qu'une réinstallation est plus rapide.
* Utiliser un scraper externe
* Toujours avoir 2 microsd, 16Go suffit pour le système (une pour la version stable, une autre pour vos tests, ou tests bêta) d'où l'intérêt d'avoir ses roms sur support amovible.
* Installer des dissipateurs thermiques ou ventilateur si nécessaire notamment :
  * Si vous overlockez votre Raspberry Pi.
  * Si votre Recalbox est dans un boîtier.

## Lancer la mise à jour

>La **mise à jour de Recalbox** est accessible en appuyant sur `START` > `MISES À JOUR`.
{.is-info}

* Configurez votre wifi ou branchez un câble réseau sur votre Recalbox.
* Sélectionnez `MISES À JOUR`.
* Puis `LANCER LA MISE À JOUR`. 

Le système redémarre automatiquement après le téléchargement de la mise à jour.