---
title: GSPlus
description: 
published: true
date: 2023-09-11T07:53:10.261Z
tags: gsplus, apple-2gs
editor: markdown
dateCreated: 2023-09-11T07:53:10.261Z
---

**GSPlus** es un emulador creado a partir de los resto de los emuladores KEGS y GSPort que sirve para emular el Apple ]\[GS (Apple 2GS).

## ![](/emulators/license.svg) Licencia

El core posee la siguiente licenci [**GPLv2**](https://github.com/digarok/gsplus/blob/master/LICENSE.txt).

## ![](/emulators/compatibility.png) Compatibilidad

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Funcionalidades



## ![](/emulators/bios.svg) BIOS

### Lista de BIOS obligatorias

| Nombre del fichero | Descripción | MD5 | ¿Preinstalada? |
| :---: | :---: | :---: | :---: |
| apple2gs1.rom | - | 20a0334c447cb069a040ae5be1d938df | ❌ |
| apple2gs3.rom | - | ba89edf2729a28a17cd9e0f7a0ac9a39 | ❌ |

### Emplazamiento

Disponed vuestras bios como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 🗒 **apple2gs.rom**

## ![](/emulators/roms.png) Roms

### Extensiones de roms soportadas

Las roms deben tener las extensiones siguientes:

* .2mg
* .po
* .hdv

### Emplazamiento

Disponed vuestras roms como en la siguiente imagen: 

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 apple2gs
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.2mg**

>Se aconsejan las roms del proyecto **TOSEC**.
{.is-success}

>Para más información sobre las roms, consultad [este tutorial](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

>Para conservar las configuraciones personalizadas cuando se produce una actualización del sistema Recalbox, os aconsejamos utilizar las [Sobrecargas de configuración](./../../../advanced-usage/configuration-override).
{.is-info}

## ![](/emulators/external-links.png) Enlaces externos

* **Código fuente utilizado** : [https://github.com/digarok/gsplus/](https://github.com/digarok/gsplus/)
* **Documentación** : [http://apple2.gs/plus/](http://apple2.gs/plus/)