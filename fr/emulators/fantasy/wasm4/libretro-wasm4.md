---
title: Libretro Wasm4
description: 
published: true
date: 2024-06-23T00:14:25.462Z
tags: libretro, 9.1+, wasm4
editor: markdown
dateCreated: 2023-06-17T22:59:59.485Z
---

**Libretro Wasm4** est un émulateur de console fantasy utilisant **WebAssembly**.

## ![](/emulators/license.svg) Licence

Ce core est sous licence [**ISC**](https://github.com/aduros/wasm4/blob/main/LICENSE.txt).

## ![](/emulators/compatibility.png) Compatibilité

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Fonctionnalités

| Fonctionnalité | Supporté |
| :---: | :---: |

## ![](/emulators/bios.svg) BIOS

>**Aucun bios n'est requis.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Extensions supportées

Les roms doivent avoir l'extension :

* .wasm

### Emplacement

Placez les roms comme ceci : 

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 wasm4
┃ ┃ ┃ ┃ ┣ 🗒  **jeu.wasm**

## ![](/emulators/advanced-configuration.png) Configuration avancée de l'émulateur

>Pour pouvoir conserver vos configurations personnalisées lors d'une mise à jour, nous vous conseillons d'utiliser notre fonctionnalité [Surcharges de configuration](./../../../advanced-usage/configuration-override).
{.is-info}

### Accéder aux options

Vous pouvez configurer diverses options de deux façons différentes.

* Via le Menu RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via le fichier `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Options du core

| Options | Valeur des options | Variable (pour surcharge) | Valeurs des variables |
| --- | --- | --- | --- |
| Pixel type | `xrgb8888` ✅ / `rgb565` | `wasm4_pixel_type` | `xrgb8888` / `rgb565` |
| Audio type | `callback` ✅ / `normal` | `wasm4_audio_type` | `callback` / `normal` |
| Frames to hold touch value | `5` / `10` ✅ / `20` / `30` | `wasm4_touchscreen_hold_frames` | `5` / `10` / `20` / `30` |

## ![](/emulators/external-links.png) Liens externes

* **Code source utilisé** : [https://github.com/aduros/wasm4/](https://github.com/aduros/wasm4/)