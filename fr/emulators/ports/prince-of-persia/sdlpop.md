---
title: SDLPoP
description: 
published: true
date: 2024-06-23T12:20:57.735Z
tags: 9.2+, prince of persia, sdlpop
editor: markdown
dateCreated: 2024-06-23T12:20:57.735Z
---



## ![](/emulators/license.svg) Licence

Ce core est sous licence [**GPL-3.0**](https://github.com/NagyD/SDLPoP/blob/master/COPYING).

## ![](/emulators/compatibility.png) Compatibilité

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Fonctionnalités

| Fonctionnalité | Supporté |
| :---: | :---: |

## ![](/emulators/bios.svg) BIOS

>**Aucun bios n'est requis.**
{.is-success}

## ![](/emulators/roms.png) Roms

Le jeu depuis la version décompilée DOS est déjà fourni.

### Emplacement

Placez les fichiers dans le dossier suivant :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 ports
┃ ┃ ┃ ┃ ┣ 📁 Prince of Persia
┃ ┃ ┃ ┃ ┃ ┣ 📁 data

## ![](/emulators/advanced-configuration.png) Configuration avancée de l'émulateur

> **Ce core n'a pas d'options !**
{.is-success}

## ![](/emulators/external-links.png) Liens externes

* **Code source utilisé** : [https://github.com/NagyD/SDLPoP/](https://github.com/NagyD/SDLPoP/)