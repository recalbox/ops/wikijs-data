---
title: Jouer avec deux Game Boy
description: 
published: true
date: 2021-11-17T19:25:50.710Z
tags: gameboy, deux
editor: markdown
dateCreated: 2021-05-21T08:46:52.599Z
---

## Introduction

Il est possible de jouer à deux joueurs avec deux GameBoy sur Recalbox. 

>Même si cela peut être tentant, Recalbox ne supporte pas le Netplay dans ce mode.
>
>De plus, en mode un jeu pour deux Game Boy, les sauvegardes ne sont pas supportées pour la deuxième Game Boy, contrairement au mode [deux jeux deux Game Boy](./play-two-players-games-with-gamelink), où là encore, le Netplay n'est pas supporté.
{.is-warning}

## Utilisation

Depuis Recalbox 8, un dossier nommé `gamelink` s'est automatiquement ajouté dans les systèmes `gb` et `gbc`. Ce dossier permet de jouer à 2 joueurs sur le même jeu sans effectuer la moindre configuration manuelle !

1. Placez le jeu souhaité dans le répertoire `gamelink` correspondant au système souhaité.
2. Une fois la mise à jour des jeux effectuée, sélectionnez la Game Boy ou Game Boy Color.

![](/tutorials/games/guides/game-boy/play-two-players/samegametwoplayers1.png){.full-width}

3. Dans la liste de jeux, allez dans `gamelink`, positionnez-vous sur le jeu voulu et appuyez sur `START`.

![](/tutorials/games/guides/game-boy/play-two-players/samegametwoplayers2.png){.full-width}

Le jeu lancé se dédoublera automatiquement sur votre écran.

![](/tutorials/games/guides/game-boy/play-two-players/samegametwoplayers3.png){.full-width}

>Pour jouer à 2 jeux différents en même temps, vous devez utiliser la [manipulation suivante](./play-two-players-games-with-gamelink).
{.is-info}