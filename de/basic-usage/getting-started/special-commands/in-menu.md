---
title: Im Menü
description: Wenn man sich im Hauptmenü befindet, sind spezielle Befehle verfügbar.
published: true
date: 2021-07-01T11:48:20.942Z
tags: 
editor: markdown
dateCreated: 2021-07-01T11:48:20.942Z
---

## Suchfunktion

Die **Suchfunktion** erscheint im Systemmenü durch drücken von

* **`R1`**

## Abschaltmenü

Im Systemmenü erscheint das **Abschaltmenü** der Recalbox durch drücken von

* **`Select`**

