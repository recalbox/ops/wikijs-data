---
title: Libretro vecx
description: 
published: true
date: 2021-10-19T05:48:15.394Z
tags: libretro, vectrex, vecx
editor: markdown
dateCreated: 2021-08-03T19:45:01.411Z
---



## ![](/emulators/license.svg) License

This core is under [**GPLv3**](https://github.com/libretro/libretro-vecx/blob/master/LICENSE.md) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Features



## ![](/emulators/bios.svg) BIOS

>**No bios is required.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Supported extensions

Roms must have the extension:

* .bin
* .vec
* .zip
* .7z

This system supports compressed roms in .zip/.7z format. However, be careful, it is only an archive.

The files contained in the .zip/.7z must correspond to the extensions mentioned above.
Moreover, each .zip/.7z file must contain only one compressed rom.

### Location

Put the roms like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 vectrex
┃ ┃ ┃ ┃ ┣ 🗒 **game.zip**

>The roms using **No-Intro** format are highly recommended.
{.is-success}

>For more information about roms, go to [this tutorial](./../../../tutorials/games/generalities/isos-and-roms)!
{.is-info}

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Accessing to options

You can configure various options in two different ways.

* Via RetroArch menu:

┣ 📁 RetroArch Menu
┃ ┣ 📁 Core options
┃ ┃ ┣ 🧩 Name_option

* Via the file `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core options

| Options | Options values | Variables (for configuration override) | Variables values |
| --- | --- | --- | --- |
| Use Hardware Rendering | `Software` / `Hardware` ✅ | `vecx_use_hw` | `Software` / `Hardware` |
| Internal Resolution Multiplier | `1` ✅ / `2` / `3` / `4` | `vecx_res_multiplier` | `1` / `2` / `3` / `4` |
| Hardware Rendering Resolution | `434x540` / `515x640` / `580x720` / `618x768` / `824x1024` ✅ / `845x1050` / `869x1080` / `966x1200` / `1159x1440` / `1648x2048` | `vecx_res_hw` | `434x540` / `515x640` / `580x720` / `618x768` / `824x1024` / `845x1050` / `869x1080` / `966x1200` / `1159x1440` / `1648x2048` |
| Line brightness | `1` / `2` / `3` / `4` ✅ / `5` / `6` / `7` / `8` / `9` | `vecx_line_brightness` | `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` |
| Line width | `1` / `2` / `3` / `4` ✅ / `5` / `6` / `7` / `8` / `9` | `vecx_line_width` | `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` |
| Bloom brightness | `0` / `1` / `2` / `3` / `4` ✅ / `5` / `6` / `7` / `8` / `9` | `vecx_bloom_brightness` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` |
| Bloom width | `2px` / `3px` / `4px` / `6px` / `8px` ✅ / `10px` / `12px` / `14px` / `16px` | `vecx_bloom_width` | `2px` / `3px` / `4px` / `6px` / `8px` / `10px` / `12px` / `14px` / `16px` |
| Scale vector display horizontally | `0.845` / `0.85` / `0.855` / `0.86` / `0.865` / `0.87` / `0.875` / `0.88` / `0.885` / `0.89` / `0.895` / `0.90` / `0.905` / `0.91` / `0.915` / `0.92` / `0.925` / `0.93` / `0.935` / `0.94` / `0.945` / `0.95` / `0.955` / `0.96` / `0.965` / `0.97` / `0.975` / `0.98` / `0.985` / `0.99` / `0.995` / `1` ✅ / `1.005` / `1.01` | `vecx_scale_x` | `0.845` / `0.85` / `0.855` / `0.86` / `0.865` / `0.87` / `0.875` / `0.88` / `0.885` / `0.89` / `0.895` / `0.90` / `0.905` / `0.91` / `0.915` / `0.92` / `0.925` / `0.93` / `0.935` / `0.94` / `0.945` / `0.95` / `0.955` / `0.96` / `0.965` / `0.97` / `0.975` / `0.98` / `0.985` / `0.99` / `0.995` / `1` / `1.005` / `1.01` |
| Scale vector display vertically | `0.845` / `0.85` / `0.855` / `0.86` / `0.865` / `0.87` / `0.875` / `0.88` / `0.885` / `0.89` / `0.895` / `0.90` / `0.905` / `0.91` / `0.915` / `0.92` / `0.925` / `0.93` / `0.935` / `0.94` / `0.945` / `0.95` / `0.955` / `0.96` / `0.965` / `0.97` / `0.975` / `0.98` / `0.985` / `0.99` / `0.995` / `1` ✅ / `1.005` / `1.01` | `vecx_scale_y` | `0.845` / `0.85` / `0.855` / `0.86` / `0.865` / `0.87` / `0.875` / `0.88` / `0.885` / `0.89` / `0.895` / `0.90` / `0.905` / `0.91` / `0.915` / `0.92` / `0.925` / `0.93` / `0.935` / `0.94` / `0.945` / `0.95` / `0.955` / `0.96` / `0.965` / `0.97` / `0.975` / `0.98` / `0.985` / `0.99` / `0.995` / `1` / `1.005` / `1.01` |
| Horizontal shift | `-0.03` / `-0.025` / `-0.02` / `-0.015` / `-0.01` / `-0.005` / `0` ✅ / `0.005` / `0.01` / `0.015` / `0.02` / `0.025` / `0.03` | `vecx_shift_x` | `-0.03` / `-0.025` / `-0.02` / `-0.015` / `-0.01` / `-0.005` / `0` / `0.005` / `0.01` / `0.015` / `0.02` / `0.025` / `0.03` |
| Vertical shift | `-0.035` / `-0.03` / `-0.025` / `-0.02` / `-0.015` / `-0.01` / `-0.005` / `0` ✅ / `0.005` / `0.01` / `0.015` / `0.02` / `0.025` / `0.03` / `0.035` / `0.04` / `0.045` / `0.05` / `0.055` / `0.06` / `0.065` / `0.07` / `0.075` / `0.08` / `0.085` / `0.09` / `0.095` / `0.1` | `vecx_shift_y` | `-0.035` / `-0.03` / `-0.025` / `-0.02` / `-0.015` / `-0.01` / `-0.005` / `0` / `0.005` / `0.01` / `0.015` / `0.02` / `0.025` / `0.03` / `0.035` / `0.04` / `0.045` / `0.05` / `0.055` / `0.06` / `0.065` / `0.07` / `0.075` / `0.08` / `0.085` / `0.09` / `0.095` / `0.1` |

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/libretro/libretro-vecx/](https://github.com/libretro/libretro-vecx/)
* **Libretro documentation**: [https://docs.libretro.com/library/vecx/](https://docs.libretro.com/library/vecx/)