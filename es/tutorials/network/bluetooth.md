---
title: Bluetooth
description: 
published: true
date: 2024-07-26T06:25:15.977Z
tags: bluetooth
editor: markdown
dateCreated: 2023-09-01T14:32:13.975Z
---

Podéis encontraros con pequeños problemas con el Bluetooth, en estas páginas os explicamos cómo solucionarlos.

Aquí tenéis los tutoriales disponibles:

[Cambiar la dirección MAC del Bluetooth de vuestro Raspberry Pi](change-bluetooth-mac-address-on-rpi)