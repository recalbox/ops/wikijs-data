---
title: 8Bitdo on Recalbox
description: 
published: true
date: 2021-09-23T23:26:58.280Z
tags: compatibility, controller, 8bitdo
editor: markdown
dateCreated: 2021-08-08T18:23:55.276Z
---

![8bitdo](http://support.8bitdo.com/images/Logo-black.svg)

![Liste des manettes 8Bitdo au 13 novembre 2019](/compatibility/8bitdocollection.png){.full-width}

## Supported controllers 8Bitdo

### SNES style controllers

| Controller | FW (min / rec*) | USB | BT | Startup Mode | BT Association* | BT Reset | Factory Reset |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| SN30 Pro+ | ?? / 4.01 | ✅ | ☑️ | START | Press and hold the PAIR button for 2 seconds | Press and hold the PAIR button for 2 seconds | [See the manual on the 8Bitdo website](https://download.8bitdo.com/Manual/Controller/SN30-Pro+/SN30_Pro+_Manual.pdf) |
| SN30 Pro v2.0 | ? / 1.32 | ✅ | ☑️ | START + B | Press and hold the PAIR button for 3 seconds | Hold down the Select button for 3 seconds | [See the manual on the 8Bitdo website](https://download.8bitdo.com/Manual/Controller/SN30pro+SF30pro/SN30pro_Manual.pdf) |
| SN30 & SF30 Pro | 1.23 / 1.33 | ✅ | ☑️ | START + B | Press and hold the PAIR button for 3 seconds | Hold down the Select button for 3 seconds | [Press and hold the Select button for 8 seconds](https://download.8bitdo.com/Manual/Controller/SN30pro+SF30pro/SN30pro+SF30pro_Manual.pdf) |
| SN30 Pro USB | ? / 1.03 | ? | n/a | n/a | n/a| n/a | [See the manual on the 8Bitdo website](https://download.8bitdo.com/Manual/Controller/SN30pro-USB/SN30-Pro-USB.pdf) |
| SN30 GP | ? / 6.11 | ? | ? | START | Hold down the Select button for 3 seconds | Hold down the Select button for 3 seconds | [See the manual on the 8Bitdo website](https://download.8bitdo.com/Manual/Controller/SN30+SF30/SN30_Manual.pdf) |
| SN30 Retro Set | ? / 4.10 | ? | ? | START | Hold down the Select button for 3 seconds | Hold down the Select button for 3 seconds | [See the manual on the 8Bitdo website](https://download.8bitdo.com/Manual/Controller/SN30+SF30/SN30_Analogue.pdf) |
| SN30 | 2.62 / 4.20 | ✅ | ☑️ | START + R | Hold down the Select button for 3 seconds | Hold down the Select button for 3 seconds | [Press and hold the Select button for 8 seconds](https://download.8bitdo.com/Manual/Controller/SN30+SF30/SN30_Analogue.pdf) |
| SF30 &amp; SFC30 | 2.70 / 4.20 | ✅ | ☑️ | START + R | Hold down the Select button for 3 seconds | Hold down the Select button for 3 seconds | [Press and hold the Select button for 8 seconds](https://download.8bitdo.com/Manual/Controller/SN30+SF30/SN30+SF30_Manual_V4.pdf) |

### SNESxNES style controllers

| Controller | FW (min / rec*) | USB | BT | Startup Mode | BT Association* | BT Reset | Factory Reset |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| N30 Pro 2 | ? / 6.10 | ? | ? | START | Press and hold the PAIR button for 1 second | Press and hold the PAIR button for 3 seconds | [See the manual on the 8Bitdo website](https://download.8bitdo.com/Manual/Controller/N30pro2/N30pro2-Manual.pdf) |
| N30 &amp; NES30 Pro | 1.62 / 4.10 | ✅ | ☑️ | POWER | Press and hold the PAIR button for 1 second | Press and hold the PAIR button for 3 seconds | [Press and hold the Start button for 8 seconds](https://download.8bitdo.com/Manual/Controller/N30pro+F30pro/N30pro+F30pro_Manual_V4.pdf) |
| F30 &amp; FC30 Pro | 1.62 / 4.10 | ✅ | ☑️ | POWER + X | Press and hold the PAIR button for 1 second | Press and hold the PAIR button for 3 seconds | [Press and hold the Start button for 8 seconds](https://download.8bitdo.com/Manual/Controller/N30pro+F30pro/N30pro+F30pro_Manual_V4.pdf) |

### NES style controllers

| Controller | FW (min / rec*) | USB | BT | Startup Mode | BT Association* | BT Reset | Factory Reset |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| N30 NS | ? / 6.10 | ? | ? | START | Hold down the Select button for 3 seconds | Hold down the Select button for 3 seconds | [See the manual on the 8Bitdo website](https://download.8bitdo.com/Manual/Controller/N30-NS/N30-NS.pdf) |
| N30 &amp; NES30 | 2.62 / 4.20 | ✅ | ☑️ | START + R | Hold down the Select button for 3 seconds | Hold down the Select button for 3 seconds | [Press and hold the Select button for 8 seconds](https://download.8bitdo.com/Manual/Controller/N30+F30/N30+F30_Manual_V4.pdf) |
| F30 &amp; FC30 | 2.62 / 4.20 | ✅ | ☑️ | START + R | Hold down the Select button for 3 seconds | Hold down the Select button for 3 seconds | [Press and hold the Select button for 8 seconds](https://download.8bitdo.com/Manual/Controller/N30+F30/N30+F30_Manual_V4.pdf) |

### GENESIS style controllers

| Controller | FW (min / rec*) | USB | BT | Startup Mode | BT Association* | BT Reset | Factory Reset |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| M30 | 1.13 / 1.14 | ❌ | ☑️ | START | Hold down the Select button for 1 second | Hold down the Select button for 3 seconds | [See the manual on the 8Bitdo website](https://download.8bitdo.com/Manual/Controller/M30/M30_Manual.pdf) |

### ARCADE style controllers

| Controller | FW (min / rec*) | USB | BT | Startup Mode | BT Association* | BT Reset | Factory Reset |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| N30 &amp; NES30 Arcade Stick | 1.41 / 5.01 | ✅ | ☑️ | HOME | Hold down the Select button for 3 seconds | Hold down the Select button for 3 seconds | [See the manual on the 8Bitdo website](https://download.8bitdo.com/Manual/Controller/Joystick/N30-Arcade-Manual.pdf) |
| F30 & FC30 Arcade Stick | 1.42 | ✅ | ☑️ | HOME | Hold down the Select button for 3 seconds | Hold down the Select button for 3 seconds | [See the manual on the 8Bitdo website](https://download.8bitdo.com/Manual/Controller/Joystick/F30-ArcadeJoystick-eng.pdf) |

### Others style controllers

| Controller | FW (min / rec*) | USB | BT | Startup Mode | BT Association* | BT Reset | Factory Reset |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| Lite | ?? / 1.20 | ❌ | ❌ | HOME | Press and hold the PAIR button for 2 seconds | Press and hold the PAIR button for 1 second | [See the manual on the 8Bitdo website](https://download.8bitdo.com/Manual/Controller/Lite/Lite_Manual.pdf) |
| Zero | n/a | ❌ | ☑️ | START + R1 | Hold down the Select button for 3 seconds | Hold down the Select button for 3 seconds | [Press and hold the Select button for 8 seconds](https://download.8bitdo.com/Manual/Controller/ZERO/ZERO_Manual_V4.pdf) |
| Zero 2 | ?? / 1.05 | ✅ | ☑️ | START + R1 | Hold down the Select button for 3 seconds | Hold down the Select button for 3 seconds | [Press and hold the Select button for 8 seconds](https://download.8bitdo.com/Manual/Controller/Zero2/Zero2_Manual.pdf) |
* FW = Firmware
* min = Minimum
* rec = Recommended
* BT = Bluetooth

## Dongles 8Bitdo Supportés

| Dongle | FW (min / rec*) | Compatibility with Recalbox | Compatible Controllers |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| USB Adapter for PS Classic (in X-Input Mode) | 1.28 / 1.33 | ✅ | PS3, PS4, Xbox One S, Wii Mote, Wii U Pro, Switch Joycons, Switch Pro, M30, SFC30, SF30 Pro. |
| USB Adapter Receiver (in X-Input Mode) | 1.28 / 2.00 | ✅ | PS3, PS4, PS5, Xbox One S, Xbox One X, Wii Mote, Wii U Pro, Switch Joycons, Switch Pro, M30, SFC30, SF30 Pro. |
| GBros Adapter Emeter (With 8Bitdo Receiver) | 2.23 / 2.24 | ✅ | GameCube, GameCube Wavebird (with extension cable), Wii Classic, Wii Classic Pro, NES Mini, SNES Mini. |
| NES & SNES Mini Receiver | ? / 1.14 | ? | ? |
| SNES Receiver | ? / 1.34 | ? | ? |

## 8Bitdo support

* [8Bitdo Controller Support](https://support.8bitdo.com/)
* [Update Tool](https://support.8bitdo.com/firmware-updater.html) 

The 8Bitdo controllers must be unlinked before they can be linked again. If you have a problem with the association, you can try the "Bluetooth Reset" method, and if that doesn't work, you can try the "Factory Reset" method.

You can find the manual for each of your controllers on the [8Bitdo support] website (https://support.8bitdo.com/).

## Faq

### How to charge your controller?

Preferably, use the USB port of your computer or console to charge your controller.
If you use a standard charger or a phone charger, make sure that it never exceeds 5V 1A.
Too high a current can damage the battery and make the controller unusable in Bluetooth.

### How to update the firmware ?

Just go to the [8Bitdo support] website (https://support.8bitdo.com/) and download the `Upgrade tool` software at the top of the page.

1. Install the downloaded `8BitDo Firmware Upgrader` application.
2. Open the application
3. Once the application is open, it will ask you to connect your controller to your PC.
4. Your controller will be automatically recognized. Click on the `Firmware Update` button to access the available updates for your controller.
5. Select the desired firmware and click the blue `Update' button.
6. Follow the procedure shown to perform the update. The update will have a red progress bar.
7. Once the update is successful, click the green `Success` button and you can disconnect your controller.

### How do I turn on/connect my controller correctly?

Depending on the device (Windows, Mac OS, Android, Switch, etc.), there are different power-up combinations:

* `START` + `Y` = Switch mode
* `START` + `A` = MacOS mode
* `START` + `X` = PC mode
* `START` + `B` = Bluetooth mode (often best for Android)

You must hold the keys simultaneously until the controller is turned on and the association is made.

In case of USB connection, hold the keys simultaneously until the controller is turned on, then connect the USB cable.

### How do I connect my controller to another device?

The controller only retains one external device address. If you want to switch from your Switch to Android for example, you will have to reset the controller's association data. Depending on the model, the procedure differs slightly. Please refer to the table above for the procedure.

### How do I pair my USB adapter with my PS3 controller?

There is a tool to manage your PS3 controller and USB adapters.

### {.tabset}
#### Windows

You can download this [utility](https://download.8bitdo.com/Tools/Receiver/8Bitdo_PS3_Tool_Win_V1.1.zip)

In case of problem with Windows, there are 2 solutions:

**Solution 1**:
Install SixaxisPairToolSetup-0.3.1.exe. You can download it on [this page](https://sixaxispairtool.en.lo4d.com/download).

**Solution 2**:
Temporarily disable Windows driver signature enforcement. You can find help images in the `How To Temporarily Disable Driver Signature Enforcement On Win8 And Win10` folder contained in the `8Bitdo_PS3_Tool_Win_V1.1.zip` file.

In case of other problems, you can install the 'libusb-win32' driver.

>The signature enforcement function of Windows drivers can be reactivated during an update or other.
{.is-info}

#### macOS
You can download this [utility](https://download.8bitdo.com/Tools/Receiver/8BitDo_PS3_Tool_MacOS_V1.1.zip)

### What about Arcade sticks ?

Depending on the device (Windows, Mac OS, Android, Switch, etc.), there are different ignition combinations:

* Windows: XINPUT position.  
* Android: DINPUT position.  
* MacOS and Switch are not necessary.  
  
Powering up procedure:

1. Select XINPUT or DINPUT mode if necessary.
2. Press the PAIR button to initialize the device (the blue LED blinks).
3. Configure the device on your device (Windows, macOS, Android or Switch).
4. The led should freeze in blue and the controller is ready to be used.

You have to hold the keys simultaneously until the controller lights up, then make the association. In case of USB connection, hold the keys simultaneously until the controller is turned on, then connect the USB cable.

### Which firmware is recommended for Recalbox?

Please refer to the table at the top of the page for the recommended firmware for your controller.

### What about compatibility with the pad zero V1?

The pad zero v1 works with Bluetooth only. Turn on the controller by holding the `START` + `R1` buttons.

When using two zero V1 pads:

- First controller, hold the `START` + `R1` buttons.
- Second controller, hold down the `START` + `R1` + `B` buttons.

### My pad is not recognized with the USB cable?

Make sure you are using the cable that came with the controller. If not, make sure you have a USB data cable and not the USB charging cable only. Some USB cables only let through the charging current and not the data flow. Translated with www.DeepL.com/Translator (free version)