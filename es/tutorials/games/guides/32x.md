---
title: 32X
description: 
published: true
date: 2024-07-26T06:56:51.742Z
tags: 32x, sega, juegos
editor: markdown
dateCreated: 2023-09-14T07:33:51.880Z
---

Tutoriales relacionados con el periférico 32X para la Megadrive.

Aqui tenéis la lista de tutoriales disponibles:

[Compatibilidad de juegos](games-compatibility)