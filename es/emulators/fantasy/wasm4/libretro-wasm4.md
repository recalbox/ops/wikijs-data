---
title: Libretro Wasm4
description: 
published: true
date: 2024-07-14T19:19:46.132Z
tags: libretro, 9.1+, wasm4
editor: markdown
dateCreated: 2023-09-11T06:24:10.708Z
---

**Libretro Wasm4** es une emulador de consolas virtuales que utiliza **WebAssembly**.

## ![](/emulators/license.svg) Licencia

El core posee la siguiente licencia [**ISC**](https://github.com/aduros/wasm4/blob/main/LICENSE.txt).

## ![](/emulators/compatibility.png) Compatibilidad

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Funcionalidades

| Funcionalidad | ¿Soportada? |
| :---: | :---: |

## ![](/emulators/bios.svg) BIOS

>**No se necesitan bios.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Extensiones de roms soportadas

Las roms deben tener las extensiones siguientes:

* .wasm

### Emplazamiento

Disponed vuestras roms como en la siguiente imagen: 

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 wasm4
┃ ┃ ┃ ┃ ┣ 🗒  **jeu.wasm**

## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

>Para conservar las configuraciones personalizadas cuando se produce una actualización del sistema Recalbox, os aconsejamos utilizar las [Sobrecargas de configuración](./../../../advanced-usage/configuration-override).
{.is-info}

### Acceder a las opciones

Podéis configurar las opciones del core de dos formas diferentes.

* Via el Menú RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via el fichero `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Opciones del core

| Opción | Valor de la opción | Variable (para la sobrecarga) | Valores posibles |
| --- | --- | --- | --- |
| Pixel type | `xrgb8888` ✅ / `rgb565` | `wasm4_pixel_type` | `xrgb8888` / `rgb565` |
| Audio type | `callback` ✅ / `normal` | `wasm4_audio_type` | `callback` / `normal` |
| Frames to hold touch value | `5` / `10` ✅ / `20` / `30` | `wasm4_touchscreen_hold_frames` | `5` / `10` / `20` / `30` |

## ![](/emulators/external-links.png) Enlaces externos

* **Código fuente utilizado** : [https://github.com/aduros/wasm4/](https://github.com/aduros/wasm4/)