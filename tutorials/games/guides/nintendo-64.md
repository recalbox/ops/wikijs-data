---
title: Nintendo 64
description: 
published: true
date: 2021-09-08T12:48:30.841Z
tags: n64, nintendo, 64
editor: markdown
dateCreated: 2021-05-21T08:37:27.579Z
---

Avec les tutoriels disponibles ici, vous allez découvrir comment mettre le son sur la sortie casque en cas de refus du système ou que faire si le jeu ne se lance pas.

Voici les tutoriels disponibles :

[Compatibilité des roms avec les cores](game-core-compatibility)
[Pas de son sur la sortie casque avec mupen64plus](no-sound-on-jack-with-mupen64plus)
[Plusieurs packs de textures haute résolution différents sur le même jeu](load-differents-high-resolution-texture-packs-on-same-game)
[Retour dans EmulationStation](return-to-emulationstation-with-mupen64plus)
[Textures haute résolution](high-resolution-textures)