---
title: Wi-Fi
description: 
published: true
date: 2023-07-20T10:31:25.159Z
tags: wifi, réseau, administration
editor: markdown
dateCreated: 2021-05-21T08:08:47.880Z
---

Si vous souhaitez activer le Wi-Fi sur votre Recalbox ou que vous ayez un soucis de connexion, vous devriez trouver votre bonheur ici.

Voici les tutoriels disponibles :

- [Activer le Wi-Fi](enable-wifi)
- [Définir l'adresse IP de la connexion Wi-Fi](/fr/tutorials/network/ip/static-manual-ip)
- [Définir l'indicatif pays de la connexion Wi-Fi](wifi-country-code)