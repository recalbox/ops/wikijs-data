---
title: Comment personnaliser le mapping d'une manette
description: 
published: true
date: 2021-09-04T22:31:08.068Z
tags: mapping, manette, personnaliser
editor: markdown
dateCreated: 2021-05-21T08:34:33.332Z
---

## Explications

Avec RetroArch, vous avez la possibilité de définir des actions spécifiques sur les boutons de votre manette en modifiant ceux définis par défaut.

## Utilisation

* Lancez un jeu avec un émulateur Libretro.
* Allez dans le menu de RetroArch en faisant `Hotkey` + `B`.
* Allez dans `Touches` > `Port 1 Touches`.

![](/tutorials/controllers/buttons-customization/how-to/controllerconfig1.png)

* Vous pouvez reconfigurer les touches ici. Pensez à avoir un clavier sous la main, car à un moment donné, ça pourrait coincer, et il vous faudra terminer avec un clavier (`W` pour valider et `X` pour revenir en arrière).

![](/tutorials/controllers/buttons-customization/how-to/controllerconfig2.png)

* Faites « Retour » 1 fois et vous avez le choix pour la sauvegarde :
  * « Sauvegarder le mappage pour le cœur » : utilisez cette option si vous souhaitez enregistrer la configuration pour tout le cœeur faisant tourner le jeu actuellement.

![](/tutorials/controllers/buttons-customization/how-to/controllerconfig3.png)

  * « Sauvegarder le mappage pour le dossier » : utilisez cette option si vous souhaitez enregistrer la configuration pour tout le dossier de l'émulateur.

![](/tutorials/controllers/buttons-customization/how-to/controllerconfig4.png)

  * « Sauvegarder le mappage pour le jeu » : utilisez sette option si vous souhaitez enregistrer la configuration pour ce jeu uniquement.

![](/tutorials/controllers/buttons-customization/how-to/controllerconfig5.png)

Tous les fichiers de remappage se trouvent dans `/recalbox/share/system/.config/retroarch/config/remaps/` et auront l'extension `*.rmp`.