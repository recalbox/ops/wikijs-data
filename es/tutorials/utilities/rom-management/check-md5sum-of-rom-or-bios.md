---
title: Verificar la firma MD5 de una rom o de un bios
description: 
published: true
date: 2023-09-16T20:48:13.263Z
tags: rom, md5, bios, firma
editor: markdown
dateCreated: 2023-09-16T20:48:13.263Z
---

Para verificar el hash MD5 de una o varias roms manualmente desde vuestro ordenador:

## {.tabset}
### Windows
Descargar este programa: [md5sum](http://esrg.sourceforge.net/utils_win_up/md5sum/).​

### Linux

* Desde el `terminal` `md5sum /path/to/file` .
* Reemplazar `/path/to/file` por la ruta a vuestro fichero.

### macOS

* Abrid `Terminal` .
* Teclear `md5sum /path/to/file` (reemplazad `/path/to/file` por la ruta a vuestro fichero).
* Click derecho en vuestro fichero, seleccionad `Copiar`.
* A continuación `Pegar` dentro de la ventana del `Terminal`.