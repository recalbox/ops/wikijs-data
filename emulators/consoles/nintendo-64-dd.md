---
title: Nintendo 64 DD
description: Nintendo 64 Disk Drive
published: true
date: 2021-10-03T20:33:46.915Z
tags: consoles, nintendo, 64dd, n64dd
editor: markdown
dateCreated: 2021-05-21T08:00:24.802Z
---

![](/emulators/consoles/nintendo64dd.svg){.align-center}

## Fiche technique

* **Fabricant** : Nintendo
* **Année de sortie** : 1999
* **Quantités vendues** : 15000
* **Meilleur jeu vendu** : Super Mario 64
* **Processeur** : 64-bit NEC VR4300 @ 93.75 MHz
* **Co-processeur** : 64-bit MIPS RISC "Reality Immersion" RCP @ 62.5 MHz
* **Processeur 64DD** : Co-processeur 32-bit pour la lecture et le transfert de données des disques
* **RAM** : 4 MB RAMBUS RDRAM (modifiable pour 8 MB)
* **Puce sonore** : Stereo 16-Bit et 48 kHz

## Présentation

Le **Nintendo 64DD** (Disk Drive) est un périphérique de la console Nintendo 64 sorti au Japon le 1er décembre 1999.

Le 64DD se connecte à la Nintendo 64 via le port extension situé sous la console. Il permet à la Nintendo 64 de lire des disques magnétiques.   
Un modem permettait également de se connecter au réseau Randnet qui offrait différentes options comme le jeu en ligne, un service de messagerie ou la navigation sur Internet.

Le 64DD offre la possibilité de faire communiquer deux jeux, l'un sur la Nintendo 64 au format cartouche et l'autre sur le 64DD au format disque, pouvant ainsi accueillir des projets plus ambitieux ou des extensions.

## Émulateur

[Libretro ParaLLEl_n64](libretro-parallel_n64)
[Libretro Mupen64plus_Next](libretro-mupen64plus_next)