---
title: Dongles Wifi
description: Adaptadores Wifi
published: true
date: 2023-12-07T07:36:03.907Z
tags: wifi, dongles, compatibilidade
editor: markdown
dateCreated: 2021-11-28T22:58:52.891Z
---


| Periférico | Status | Chipset | Última versão testada | Comentários | Loja |
| :---: | :---: | :---: | :---: | :---: | :---: |
| Belkin N300 F7D2102 | ✅ |  |  |  |  |
| CSL 300 Mbit/s WLAN Stick avec antenne | ❌ | RT 8191SU |  |  |  |
| Canakit Wifi Adapter | ✅ | RT5370 |  |  |  |
| Cudy WU650S AC | ✅  | RTL8821CU | 7.2.1 |  |  |
| D-link dwl 123 | ❌ | RT5370 |  |  |  |
| D-link DWA 131 | ❌ |  |  |  |  |
| Edimax EW-7811UN | ✅ | RTL8188CUS | v3.3beta17 & v4.0.0-dev | Funciona, mas cria instabilidades com outros dongles ou dispositivos USB. |  |
| Edimax EW-7811UTC - AC600 | ❌ | RT8812au |  |  |  |
| EDUP EP-N8508 USB Nano WiFi 150Mbps | ✅ |  |  |  |  |
| Hercules HWNUm-300 | ❌ |  |  |  |  |
| Inter-Tech DMG-04 | ✅ | RTL8811CU | 9.1 |  | [https://www.amazon.fr/gp/product/B097CKTBLJ](https://www.amazon.fr/gp/product/B097CKTBLJ) |
| Lindy 52213 Dongle USB 3.0 WiFi Bluetooth | ✅ |  |  |  |  |
| Netgear WIFI USB N300 Nano WNA3100M | ✅ |  |  |  |  |
| Netis WF2122 USB WiFi 11n 300M 2 antenas 5dB | ✅ |  | v4.1 |  |  |
| Ourlink | ✅ | RTl8192cu |  |  |  |
| Ralink Wi-fi 802.11n with Antenna | ✅ | RT5370 |  |  |  |
| Tenda W311Ma Wireless N150 Mini High Gain USB Adapter | ✅ |  |  |  |  |
| TP-LINK TL-WN822N 300Mbps High Gain Wireless USB Adapter | ✅ |  | v4.0.0-dev |  |  |
| TP-LINK TL-WN823N 300Mbps Wireless Mini USB Adapter | ❌ |  | v4.0.2 |  |  |
| TP-Link TL-WN722N High Gain Wireless USB Adapter HW v1.0 | ✅ |  | v4.0.0 - rpi2B |  |  |
| TP-Link TL-WN722N High Gain Wireless USB Adapter HW v2.0 | ❌ |  | v4.1 - rpi3 |  |  |
| TP-Link TL-WN725N Nano WiFi N 150 Mbps | ❌ |  | v4.1 - rpi2B | FFunciona, mas cria instabilidades com outros dongles ou dispositivos USB. |  |
| Wi-key 150 | ✅ |  |  |  |  |
| Wi-Pi Wifi Dongle | ✅ |  |  |  |  |
| Generic RTL8188EUS | ❌ | RTL8188EUS | 4.1 |  |  |
| Netis WF2116 Wireless N300 Long-Range USB Adapter, Supports Windows, Mac OS, Linux, 5dBi High Gain Antennas, Free USB Cradle | ✅ | rtl8192cu | 4.1 |  | [https://www.amazon.com/gp/product/B006THNH7Q](https://www.amazon.com/gp/product/B006THNH7Q) |
| Ralink RT5370 Raspberry PI wifi adapter/ wifi dongle with soft AP function | ✅ | RT5370 | 4.1 |  | [https://www.amazon.com/gp/product/B019XUDHFC](https://www.amazon.com/gp/product/B019XUDHFC) |
| OURLiNK 600Mbps AC600 Dual Band USB WiFi Dongle | ❌ | ????? | 4.1 |  | [https://www.amazon.com/gp/product/B011T5IF06](https://www.amazon.com/gp/product/B011T5IF06) |