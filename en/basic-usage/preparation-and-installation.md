---
title: Preparation and Installation
description: How to install Recalbox
published: true
date: 2021-08-08T18:47:26.961Z
tags: installation, preparation
editor: markdown
dateCreated: 2021-06-28T21:15:29.536Z
---

## I - Compatible boards

It is possible to install Recalbox on different types of devices:

- **Raspberry Pi 4**
- Raspberry Pi 3
- Raspberry Pi 2
- Raspberry Pi 1/Zero
- Odroid Go Advance (handheld)
- Odroid Go Super (handheld)
- 32 or 64 bits computer
- Odroid XU4

To discover Recalbox, the **Raspberry Pi 4** is definitely the team's choice !

## II - Install Recalbox

### 1 - Download / Flash the image

Go to [download.recalbox.com](https://download.recalbox.com/) and follow the instructions to download and flash the Recalbox image on your storage media (SD card, eMMC card or Hard Drive).

>Only the **latest version** of Recalbox is available.
>The **old versions** are no longer **downloadable** or **supported by the development team.**
{.is-warning}

>You must use 8GB media for the system (we recommend the **Sandisk Ultra series** for SD cards).
>
>* For the **installation on Raspberry Pi** :
>  * As storage media: an **SD card**.
>* For the **installation on Odroid** :
>  * As storage media: an SD or eMMC card**. 
>* For installation on x86 and x86_64**:
>  * As storage media: a **hard disk**.

### 2 - Installation

#### Raspberry Pi / Odroid Go Advance / Odroid Go Super

* Insert the microSD card in the device you want to use Recalbox with.
* Turn on/plug in your board and the installation of Recalbox will start automatically.
* Installation takes a few minutes and varies depending on your hardware.

#### GPI Retroflag Case

Installing on a Gpi Case is just as easy: insert the micro SD card, plug your GPI Case into the power supply, and Recalbox will automatically detect and configure the case. For more information, go to the [RetroFlag GPi Case](./preparation-and-installation/retroflag-gpi-case) page.

#### Odroid XU4 / Odroid XU4Q

* Insert your microSD or eMMC card into the device you want to use Recalbox with.
* Start it up and you should see Recalbox come to life.

#### x86 / x86_64

Installing on a USB stick (all hardware) is only recommended for test purposes. But as it is not permanent, it may cause bugs to appear that are not present on the hard disk installation.

We therefore advise you **to install on a hard disk:**

* One for the **System**.
* One for the **Storage** of roms, bios, saves, etc...

* Start it up and you should see Recalbox come to life.

## III - The necessary hardware and accessories

Check if you have the required storage device and power supply for the chosen device.

| Category |Raspberry Pi 0/0w/1/2/3 | Raspberry Pi 4 | Odroid XU4 | Odroid Go | PC |
| :---: | :---: | :---: | :---: | :---: | :---: |
| Power supply | µUSB 2.5A (good quality) | USB-C 3A 5V | Use official Odroid power supply | Internal battery | Standard PC power supply |
| Video | HDMIc, HDMI2VGA or HDMI2DVI cable | µHDMI-HDMI cable | HDMI cable | Internal | HDMI/VGA (DVI and DP probably)
| USB or Bluetooth | USB or Bluetooth | USB or Bluetooth | USB or Bluetooth | USB or Bluetooth | USB or Bluetooth |
| System storage | 8GB+ class 10 µSD card except Pi1: SD | 8GB+ µSD card | 8GB+ µSD card | 8GB+ µSD card | 8GB+ hard disk |
| Storage configuration, bios and roms | External hard disk with its own power supply | External hard disk with its own power supply | External hard disk with its own power supply | µSD card | Internal hard disk or external USB hard disk (Plugged on the motherboard) |

>You need the following items to create your Recalbox:
>Feel free to visit [our store](https://www.kubii.fr/221-recalbox) at Kubii !
{.is-info}

## Troubleshooting x86 / x86_64:

* **Your computer only boots on Windows:** Check that the **Secure Boot** is disabled.

  * Check on the Internet where you can find the Secure boot option (different according to the manufacturer and the model of the computer).
  * Then assign Off or Disable for Secure Boot.
  * Turn off your computer.

* **Recalbox does not boot:** Set your bios to Legacy.
  * To set your bios to Legacy, at boot time, press the bios access key `F*` (`F1` to `F12`, different from manufacturer) or `Del`.
  * Find out on the Internet where the Legacy option is located (different depending on the computer manufacturer and model).
  * Then assign Legacy by activating it with "**enabled**".
* **In case of a dedicated Multiboot PC**, if you want your **Recalbox to boot** before any other system:
  * Go to the **Boot Menu** of the hard drives.
  * Press `F*` to access the hard disk **Boot Menu** (`F1` to `F12`, different depending on the manufacturer).
  * Select your **system hard drive** Recalbox.