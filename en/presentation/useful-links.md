---
title: Useful links
description: 
published: true
date: 2021-08-08T18:36:09.893Z
tags: useful, links, gitlab, forum, discord, twitch, youtube
editor: markdown
dateCreated: 2021-06-28T17:45:48.815Z
---

## Changelog

* [Release notes](https://gitlab.com/recalbox/recalbox/raw/master/RELEASE-NOTES.md)
* [Changelog (technical information)](https://gitlab.com/recalbox/recalbox/raw/master/CHANGELOG.md)

## Download

* [Latest image of Recalbox](https://download.recalbox.com/)

## Our sites

* [www.recalbox.com](https://www.recalbox.com/fr/)
* [Gitlab](https://gitlab.com/recalbox/recalbox/)

## Our support networks

* [Discord](https://discord.gg/NbQFbGM)
* [Forum](https://forum.recalbox.com/)

## Our social networks

* [Facebook](https://www.facebook.com/recalbox)
* [Facebook France](https://www.facebook.com/recalbox.fr)
* [Instagram](https://instagram.com/recalbox/)
* [Twitter](https://twitter.com/recalbox)

## Our Streams

* [Twitch](https://www.twitch.tv/recalbox)

## Our youtube channel

* [Youtube](https://www.youtube.com/c/RecalboxOfficial)