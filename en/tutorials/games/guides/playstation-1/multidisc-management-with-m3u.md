---
title: Multi-disc management with .M3U
description: 
published: true
date: 2021-08-08T14:36:16.911Z
tags: management, ps1, multidisc, m3u
editor: markdown
dateCreated: 2021-08-08T14:36:16.911Z
---

## Introduction

Multiple CDs of the same game can be loaded simultaneously from EmulationStation into RetroArch by creating a `.M3U` playlist file (plain text `.TXT` with a `.M3U` extension).

>An `.M3U` playlist can only be used with games that have one `.CUE` file and only one `.BIN` per disk.
>Any multi-disc game that has multiple `.BIN` files for a single disc will not work. It is advisable to convert these disks to a single `.BIN` file.
{.is-info}

## Usage

We will use the game `Final Fantasy VII (France)` for the examples below.

## {.tabset}

## Disk format `.BIN` / `.CUE`

* Create a new text file called `Final Fantasy VII (France).m3u` which contains the following information:

```text
Final Fantasy VII (France) (Disc 1).cue
Final Fantasy VII (France) (Disc 2).cue
Final Fantasy VII (France) (Disc 3).cue
```

### Disc format `.CHD`

* Create a new text file named `Final Fantasy VII (France).m3u` which contains the following information:

```text
Final Fantasy VII (France) (Disc 1).chd
Final Fantasy VII (France) (Disc 2).chd
Final Fantasy VII (France) (Disc 3).chd
```

## Changing discs during the game

If you need to change the disc while the game is running, you have to do the following:

* Open the "virtual" CD tray: Key `HK` + Left Joystick (Up direction)
* Change the disc: `HK` key + Left Joystick (Right or Left direction)
* Close the "virtual" CD tray: `HK` key + Left Joystick (Up Direction)