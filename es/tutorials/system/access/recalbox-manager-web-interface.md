---
title: Recalbox Manager - Interfaz web
description: 
published: true
date: 2023-09-13T20:32:33.165Z
tags: web, interfaz, administrador
editor: markdown
dateCreated: 2023-09-13T20:32:33.165Z
---

El Administrador Web Recalbox es una interfaz web para acceder a vuestra Recalbox desde un téléfono móvil, una tableta o vuestro PC.

## Interfaz

Desde la página principal, podéis:

* Monitorizar vuestra Recalbox (temperatura, uso de los núcleos de la CPU, etc.).
* Modificar el fichero `recalbox.conf`.
* Iniciar el gamepad virtual (útil para configurar tu Recalbox en 5 minutos por primera vez) - sólo a través de smartphone o tablet.
* Comprobar los ficheros de log de Recalbox.
* Reiniciar EmulationStation o todo el sistema en caso de fallo.
* Tomar, ver y guardar capturas de pantalla.

Mediante la funcionalidad de arrastrar & soltar del ratón, permite:

* Cargar roms en formato `.zip` o otras extensiones soportadas por los emuladores (no imágenes de CD).

Por defecto, el Administrador Web arranca automáticamente. Si preferís, podéis desactivarlo en el fichero `recalbox.conf` editando la línea `system.manager.enabled` y cambiando el valor a 0. Sin embargo, si hacéis esto, en caso de problemas (Recalbox bloqueada), ¡no podréis en absoluto apagarla ni reiniciarla correctamente!

También tenéis la posibilidad de desactivar el WebManager directamente desde EmulationStation (`START` > `PARAMATROS AVANZADOS` > `ADMINISTRADOR WEB RECALBOX`).

## Para conectarse

Basta con escribir en vuestro navegador web el siguiente nombre:

## {.tabset}

### Windows

[http://recalbox/](http://recalbox/)

### macOS

[http://recalbox.local/](http://recalbox.local/)

### Linux

[http://recalbox.local/](http://recalbox.local/)

## Si esto no funciona

También podéis escribir directamente la dirección IP de vuestra Recalbox:

* **192.168.1.x**

>La mayoría de los routers están configurados por defecto para proporcionar un rango de direcciones IP entre la 192.168.1.2 et 192.168.1.254.
>
>Para conocer la direccioón IP de vuestra Recalbox, desde el menú de EmulationStation, pulsad `START` > `OPCIONES DE RED` y apuntad la dirección que aparece al lado de `DIRECCION IP`.
{.is-info}

## Mando virtual

Para acceder al mando virtual, basta con escribir la dirección IP de vuestra Recalbox junto con el puerto 8080. Por ejemplo: `http://192.168.0.X:8080` o `http://recalbox:8080`

Consultad el [manual de utilización](./../../../basic-usage/first-use-and-configuration#configuration-dune-manette) del mando virtual.

Para comunicarnos cualquier bug liado al Administrador Web, podéis utilizar [esto](https://gitlab.com/recalbox/recalbox-manager).