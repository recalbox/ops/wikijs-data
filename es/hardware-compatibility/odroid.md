---
title: Odroid
description: 
published: true
date: 2023-09-19T08:49:28.884Z
tags: odroid, compatibilidad
editor: markdown
dateCreated: 2023-09-19T08:49:28.884Z
---

## Odroid XU4

![](/compatibility/xu4-exynos-8-coeurs.jpg)

El ODROID-XU4 es una nueva generación de dispositivos informáticos con un hardware más potente y eficiente energéticamente y un factor de forma más pequeño.
Con interfaces eMMC 5.0, USB 3.0 y Gigabit Ethernet, el ODROID-XU4 ofrece increíbles velocidades de transferencia de datos, una característica cada vez más necesaria para soportar la avanzada potencia de procesamiento de los dispositivos ARM.
Esto permite a los usuarios experimentar realmente una mejora en la informática, especialmente con un arranque más rápido, navegación web, redes y juegos 3D.

### Características :

* **Procesador :** 
  * Exynos 5422 (ARM Cortex-A15 Quad-Core cadencé à 2.0 GHz / ARM Cortex-A7 Quad-Core cadencé à 1.4 GHz)
* **GPU :**
  * Acelerator 3D Mali™ -T628 MP6 (OpenGL ES 3.1 / 3.0 / 2.0 / 1.1 et OpenCL 1.2 Full profile)
* **RAM :**
  * 2Go LPDDR3 RAM PoP (750Mhz, ancho de banda de memoria 12GB / s, bus 2x32 bits)
* **Connectique disponible:** 
  * 1 X RJ45 Hembra
  * 1 X USB 2.0
  * 2 X USB 3.0
  * 1 X HDMI
  * Ranura para las tarjetas MicroSD (hasta 128 Go)
* **Conector(es) adicional(es):** 
  * Almacenamiento (opcional) Zócalo para módulo eMMC: almacenamiento flash eMMC 5.0 (hasta 64 GB)
  * Interfaz HDD / SSD SATA (opcional) Adaptador de SuperSpeed USB (USB 3.0) a Serial ATA3 para HDD y SSD de 2,5"/ 3,5 "Almacenamiento en red
  * Tarjeta de sonido USB opcional
  * WiFi (opcional) : USB IEEE 802.11 ac / b / g / n 1T1R WLAN con antena (adaptador USB externo)
* **Red :**
  * Ethernet LAN 10/100 / 1000Mbps Ethernet rápido con conector RJ-45 (suporta el Auto-MDIX)
* **Alimentación :**
  * 4 ,8Volt ~ 5,2Volt (alimentacíon de 5V / 4A recomendado)
* **Dimensiones :**
  * 83 x 58 x 20 mm (poids: 38 gr) sin refrigerador.

## Odroid XU4Q

![](/compatibility/xu4q.jpg)

Tanto para servir como centro multimedia, como consola de juegos, como sistema de video-vigilancia en el coche o para cualquier otra cosa, la carta base ultracompacta **Odroid XU4Q** es una solución ideal. Con un procesador Samsung Exynos 5422 Octo-Core a 2,0 GHz y 2 GB de RAM, es muy asequible.
La tarjeta ya viene equipada con un radiador. Permite refrigerar la cpu sin ventilador.

### Características :

* **Procesador :**
  * Samsung Exynos 5422 (ARM Cortex-A15 Quad-Core cadencé à 2.0 GHz / ARM Cortex-A7 Quad-Core cadencé à 1.4 GHz)
* **Número de cores del procesador :**
  * 8
* **GPU :**
  * Mali T628 MP6 OpenGL ES 3.1 / 3.0 / 2.0 / 1.1 et OpenCL 1.2
* **RAM :**
  * 2 Go LPDDR3
* **Conectores disponibles :** 
  * 1 X RJ45 Femelle
  * 1 X USB 2.0
  * 2 X USB 3.0
  * 1 X HDMI Hembra
  * tarjetas microSD, micro SDHC, micro SDXC (compatibles)
* **Connectores adicionales:** 
  * 1 X GPIO 12 pins (header)
  * 1 X GPIO 30 pins (header)
* **Red :**
  * Ethernet LAN 10/100/1000 Mbps
* **Alimentación :**
  * 5V / 4A (_el transformador no viene por defecto_)
* **Dimensiones :**
  * 83 x 58 x 20 mm
* **Peso :**
  * 38 g

## Odroid Go Advance 2

![](/compatibility/odroid_go_advance.png)

Una videoconsola portátil D.I.Y. dedicada a la emulación y al retrogaming, inspirada en el aspecto de la Gameboy Advance y disponible en 2 colores.
Diseñada en forma de kit de montaje completo sin soldaduras, ofrece un proceso de montaje lúdico y divertido, fácilmente accesible para todosCompatible con los sistemas de juego más recientes (Playstation, PSP, etc.) y emuladores de tipo Recalbox

Altavoces y conector de 3,5 mm, control de brillo, modo de suspensión de la consola, modo "bajo consumo" para más de 7 horas de autonomía, etc.

* **Procesador :**
  * RockChip RK3326 (Quad-Core ARM Cortex A35 1,3 GHz)
* **GPU :**
  * Mali-G31 Dvalin
* **Memoria :**
  * 1 Go (DDR3L 786 Mhz, ancho de bus 32 bits)
* **Pantalla :**
  * LCD TFT 3,5 pouces 320 x 480 (ILI9488, interfaz MIPI)
* **Batería :**
  * Li-Polymer 3.7V / 3000mAh, hasta 9 horas de tiempo de juego continuado
* **Boutons d'entrée :**
  * F1, F2, F3, F4, F5, F6, A, B, X, Y, pad direccional, hombro izquierdo, hombro derecho, hombro izquierdo 2, hombro derecho 2, joystick analógico
* **Conectores :**
  * 2.0 Hôte USB 2.0 x 1
  * port 10 broches (I2C, GPIO, IRQ à 3,3 volts)
* **Toma de cascos :**
  * Jack, 0.5Watt 8&#x3A9; Mono
* **Almacenamiento :**
  * Flash SPI (démarrage 16 Mo), emplazamiento para las Micro SD (interfaz compatible UHS-1)
* **Alimentación :**
  * Entrada 5V, conector USB-C (En el embalaje de Kubii se incluye un cable de carga USB-C)
* **Wi-Fi :**
  * 2.4Ghz 802.11b / g / n integrado (ESP-WROOM-S2)
* **Consumo :**
  * Durante un juego : 300 ~ 550mA (depende del brillo de la retroiluminación, el tipo de emulación de los juegos y el uso de la conexión inalámbrica)
  * Modo apagado: < 1mA
  * Si desactivas el WiFi, el consumo de energía será el mismo que el de la revisión 1.0 anterior.
* **Tiempo de carga :**
  * 2,5 ~ 3 horas con el aparato apagado.
  * El juego dura entre 4 y 5 horas.
* **Dimensiones :**
  * 155x72x20 mm (6,1 x 2,8 x 0,8 pulgadas), peso: 180 g (6 onzas)
* **Peso :**
  * 349 grammes

## Odroid Go Super

![](/compatibility/odroid-go-super.png)

Con un formato similar al de la Switch Lite, la nueva Odroid Go Super pretende conquistar los corazones de los aficionados a los juegos retro. Con muchas de las mismas características que su predecesor, el Odroid Go Advance , esta versión marca una importante evolución en la gama de consolas portátiles de HardKernel.

* **Procesador** :
  * RockChip RK3326 (Quad-Core ARM 64bit Cortex-A35 1,3 GHz)
* **GPU** :
  * Mali-G31 MP2 OS Ubuntu 18.04. 19.10 et 20.04 sur Kernel 4.4 Aarch64
* **Memoria** :
  * 1 Go (DDR3L 786 Mhz, largeur de bus 32 bits)
* **Espacio para el almacenamiento** :
  * Flash SPI (démarrage 16 Mo), emplazamiento para tarjetas Micro SD (interfaz compatible UHS-1)
* **Pantalla** :
  * 5 pulgadas 854 × 480 TFT LCD (pantalla gran angular, interfaz MIPI-DSI)
* **Audio** :
  * Toma de auriculares estéreo, altavoz mono de 0,5 W y 8Ω
* **Batería** :
  * Li-Polymer 3,7 V / 4000 mAh (76,5 × 54,5 × 7,5 mm (L * W * T)), hasta 10 horas de tiempo de juego sin parar
* **Alimentación** :
  * Toma DC Entrada 5 V, conector USB-C.
* **Corriente máxima** :
  * 1.5A
* **Entradas / Salidas externas** :
  * USB 2.0 x 1 host, puerto de 10 patillas (I2C, GPIO, IRQ a 3,3 voltios)
* **Botones de entrada** :
  * F1, F2, F3, F4, F5, F6, A, B, X, Y, Joystick, Hombro izquierdo, Hombro derecho, Hombro izquierdo 2, Hombro derecho 2, Joystick analógico, Joystick analógico 2
* **Wireless** :
  * Adaptador WiFi USB opcional
* **Consumo de energía** :
  * Emulación de juegos: 350 ~ 600mA (depende del brillo de la retroiluminación, el tipo de emulación de juegos y el uso inalámbrico),
* **Modo apagado** :
  * <1mA
* **Tiempo de carga* :
  * 3,4 ~ 4 horas cuando el dispositivo está apagado. Al jugar, tarda entre 4,5 y 5,5 horas.
* **Dimensiones** :
  * 204x86x25 mm
* **Peso** :
  * 280 g