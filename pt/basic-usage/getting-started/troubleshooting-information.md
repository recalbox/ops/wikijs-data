---
title: Solução de problemas
description: 
published: true
date: 2024-06-02T19:48:22.866Z
tags: 
editor: markdown
dateCreated: 2021-06-29T23:44:01.968Z
---

## Controles

### Meu controle funciona no Emulationstation, mas não funciona nos emuladores

Estamos adicionando o máximo possível de configurações de controles no EmulationStation para permitir que você navegue, mas muitos deles exigem que você reconfigure para funcionarem nos emuladores.

Para isso, você deve abrir o menu do EmulationStation com o botão `START` e ir para `CONFIGURAÇÕES DOS CONTROLES `> `CONFIGURAR UM CONTROLE`.

### Controle PS3 piscando, mas não emparelhando

* Conecte um controle ao Recalbox e aguarde 10 segundos.
* Agora você pode desconectar o controle e pressionar o botão `PS`.

### O controle PS3 parece morto

* Você precisa reiniciar o controle através do pequeno botão em um pequeno orifício atrás do controle, com um clipe de papel.

## Exibição

### Bordas pretas ou imagem muito grande

* Na sua TV, encontre o menu de imagem e defina o tamanho da imagem para 1:1 pixel ou completo.

Se isso não funcionar, tente ativar o `OVERSCAN` no menu de `CONFIGURAÇÕES AVANÇADAS`. Você encontrará mais informações [neste tutorial](./../../tutorials/video/display-configuration/image-size-settings-overscan-tft).

### Tela preta no monitor do PC

Se você tiver uma tela preta no monitor do PC (HDMI ou DVI), edite o arquivo `recalbox-user-config.txt` (atualiza após reiniciar) e coloque um `#` na frente da linha `hdmi_drive=2` (se ainda não existir). Você encontrará mais informações [neste tutorial](./../../tutorials/video/lcd/dvi-screen).


## Sistema

### Acesso *root*

* Você pode se conectar via [SSH](./../../tutorials/system/access/root-access-terminal-cli) ao seu Recalbox. Você pode acessar um terminal saindo dos emuladores com `F4` e, em seguida, pressione `ALT` + `F2`. Você terá muito mais informações consultando [este tutorial](./../../tutorials/system/access/root-access-terminal-cli).