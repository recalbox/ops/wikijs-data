---
title: La "Calibración"
description: ¿Cómo podemos hacer que los juegos funcionen de la forma más óptima posible?
published: true
date: 2023-09-23T20:08:48.179Z
tags: lightgun, calibration, 7.2+
editor: markdown
dateCreated: 2023-09-23T20:08:48.179Z
---

## Introducción

En algunos juegos, especialmente los de arcade, suele ser necesario configurar el juego antes de poder jugar. Para el arcade, solo necesitáis configurarlo una vez y se quedara grabado en vuestra NVRAM o en los ficheros .CFG que encontraréis dentro del directorio "saves" de vuestro SHARE, clasificados por sistema y/o emulador.

![](/basic-usage/features/lightgun/calibration/calibration1.png)

Para realizar la configuración, a veces tendréis que ir a la configuración del core o incluso a la configuración del juego.