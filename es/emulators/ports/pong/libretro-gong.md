---
title: Libretro Gong
description: 
published: true
date: 2024-07-25T22:55:30.791Z
tags: libretro, gong, 8.0+
editor: markdown
dateCreated: 2023-09-11T14:48:51.739Z
---

Libretro Gong est une reconstitution ouverte de Pong écrite par Dan Zeitan en 2018.

## ![](/emulators/license.svg) Licencia

El core posee la siguiente licencia [**GPLv3**](https://github.com/libretro/gong/blob/master/COPYING).

## ![](/emulators/compatibility.png) Compatibilidad

 | RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | 
 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Funcionalidades

| Funcionalidad | ¿Soportada? |
| :---: | :---: |
| Opciones del core | ✔ |

## ![](/emulators/bios.svg) BIOS

>**No necesita bios.**
{.is-success}

## ![](/emulators/roms.png) Roms

El núcleo de Gong no especifica ninguna extensión en concreto. Basta con cargar y arrancar el core.

### Emplazamiento

Disponed vuestras bios como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 ports
┃ ┃ ┃ ┃ ┣ 📁 Gong
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **gong.c**

## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

>Para conservar las configuraciones personalizadas cuando se produce una actualización del sistema Recalbox, os aconsejamos utilizar las [Sobrecargas de configuración](./../../../advanced-usage/configuration-override).
{.is-info}

### Acceder a las opciones

Podéis configurar las opciones del core de dos formas diferentes.

* Via el Menú RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via el fichero `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Opciones del core

| Opción | Valor de la opción | Variable (para la sobrecarga) | Valores posibles |
| --- | --- | --- | --- |
| Player 2 | `CPU` ✅ / `Human` | `gong_player2` | `CPU` / `Human` |
| Video Refresh Rate (Restart) | `60` ✅ / `70` / `72` / `75` / `100` / `119` / `120` / `140` / `144` | `gong_refresh` | `60` / `70` / `72` / `75` / `100` / `119` / `120` / `140` / `144` |

## ![](/emulators/external-links.png) Enlaces externos

* **Código fuente utilizado** : [https://github.com/libretro/gong/](https://github.com/libretro/gong/)
* **Código fuente oficial** : [https://danzaidan.itch.io/pong-in-c](https://danzaidan.itch.io/pong-in-c)