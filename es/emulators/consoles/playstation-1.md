---
title: Playstation 1
description: Sony Playstation 1
published: true
date: 2024-07-26T07:59:36.170Z
tags: sony, ps1, playstation 1, consolas
editor: markdown
dateCreated: 2023-09-03T20:10:04.943Z
---

![](/emulators/consoles/playstation1.svg){.align-center}

## Especificaciones técnicas

* **Fabricante**: Sony Computer Entertainment
* **Año de lanzamiento**: 1994
* **Cantidad vendida**: 102,48 millones
* **Juego más vendido**: Gran Turismo
* **Procesador**: R3000A RISC de 32 bits funcionando a 33,8688 Mhz
* **RAM**: 16 Mbits
* **Unidad de procesamiento de imágenes**: 32 bits
* **RAM de vídeo**: 8 Mbits
* **RAM de audio**: 4 Mbits
* **Procesador gráfico**: Unidad de procesador GFX

## Presentación

PlayStation es una consola de quinta generación producida por Sony Computer Entertainment a partir de 1994. La PlayStation original fue la primera consola de la gama PlayStation, y más tarde se actualizó a la PSone (una versión más pequeña y ligera de la original).

El 18 de mayo de 2004, casi diez años después de su lanzamiento, Sony anunció que había distribuido 100 millones de consolas en todo el mundo y más de 962 millones de juegos de PlayStation.

## Emuladores

[DuckStation](duckstation)
[Libretro Mednafen_PSX](libretro-mednafen_psx)
[Libretro Mednafen_PSX_HW](libretro-mednafen_psx_hw)
[Libretro PCSX-ReARMed](libretro-pcsx-rearmed)
[Libretro Swanstation](libretro-swanstation)
[PCSX-ReARMed](pcsx-rearmed)
