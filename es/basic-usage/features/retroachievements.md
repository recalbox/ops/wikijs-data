---
title: Retroachievements
description: Retro-recompensas, Retro-retos
published: true
date: 2023-08-23T14:27:57.580Z
tags: retroachievements, retrorecompensas
editor: markdown
dateCreated: 2023-08-23T14:27:14.842Z
---

## Introducción

La comunidad retrogaming ha desarrollado una plataforma
[retroachievements.org](http://www.retroachievements.org/)
que propone a los jugadores desafíos y recompensas al jugar a determinados juegos.

Esta plataforma soporta la mayoría de las consolas (NES, SNES, GB, GBC, GBA, Megadrive, PC engine, etc.) y propone desafíos para la mayoría de los juegos.

Recalbox permite conectar una cuenta de [retroachievements.org](http://www.retroachievements.org/) para participar en los retos y para compararse a otros jugadores.

## Creación de una cuenta

* Visitad [retroachievements.org](http://www.retroachievements.org/) y [abrid una cuenta](http://retroachievements.org/createaccount.php) 
* Apuntad el pseudónimo y la contraseña para introducirlas en EmulationStation. 

## Activación de la cuenta en Recalbox

Abrid los parámetros correspondientes de EmulationStation.

* Abrid el menú con el botón `START`.
* Id dentro de `OPCIONES DE JUEGOS` > `OPCIONES DE RETROACHIEVEMENTS`.
* Rellenad los campos como sigue:

  * `RETROACHIEVEMENTS` : ON
  * `NOMBRE DE USUARIO` : vuestro pseudo.
  * `CONTRASEÑA` : vuestra contraseña.

Además de esto, tenéis la posiblidad de activar el modo **hardcore** (duro). Esta opción desactiva las funcionalidades que proponen la mayoría de los emuladores (grabaciones en cualquier punto, rebobinar, pokes y códigos para hacer trampas, etc) para jugar a los juegos exactamente como en los juegos originales, lo que implica una mayor dificultad.

## Compatibilidad de los emuladores

Aunque el proyecto de retroachievements es compatible con la mayoría de los emuladores y cores de Recalbox, no todos lo son. Por ejemplo la Game Cube et la Wii no están soportadas, mientras que en otras consolas sólamente algunos de los cores lo son.

Par verifcar la compatibilidad de une emulador/core, consultad [este cuadro de compatibilidades](https://docs.retroachievements.org/Emulator-Support-and-Issues/).

## Compatibilidades de juegos y de roms

Los títulos más famosos están soportados, en [esta lista](http://retroachievements.org/gameList.php) podéis vérificar si vuestro juego es compatible.

Tened en cuenta que por cada juego, sólo determinadas roms están soportadas. Como retroachievements.org es un proyecto de origen americano, las roms US tienen más compatibilidad que las rom europeas. Para maximizar las probabilidades de compatibilidad, se recomienda utilizar roms de la version "No-Intro".

Para verificar la compatibilidae una rom, basta con comparar la firma digital de vuestro juego con la lista de juegos proporciada por el sitio de retroachievements.org. 

## Consultar los retos en curso de un juego

Para consultar los retos y recompensas disponibles en un juego, acceded al menú de RetroArch con el combo `Hotkey` + `B` y consultad el menú `Achievements`.

Para volver a la partida en curso, pulsad `A` para volver atrás y seleccionad `Resume` con el botón `B`.
