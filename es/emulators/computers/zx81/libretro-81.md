---
title: Libretro 81
description: 
published: true
date: 2024-07-19T15:08:36.183Z
tags: libretro, zx81, 81
editor: markdown
dateCreated: 2023-09-11T12:10:47.278Z
---



## ![](/emulators/license.svg) Licencia

El core posee la siguiente licencia [**GPLv3**](https://github.com/libretro/81-libretro/blob/master/LICENSE).

## ![](/emulators/compatibility.png) Compatibilidad


| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | 
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | 

## ![](/emulators/features.png) Funcionalidades

| Funcionalidad | ¿Soportada? |
| :---: | :---: |
| Reinicio | ✔ |
| Capturas de pantalla | ✔ |
| Grabaciones instantáneas | ✔ |
| Rebobinado | ✔ |
| Netplay | ✔ |
| Opciones del core | ✔ |
| Controles | ✔ |

## ![](/emulators/bios.svg) BIOS

>**No necesita bios.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Extensiones de roms soportadas

Las roms deben tener las extensiones siguientes:

* .tzx
* .t81
* .p
* .zip
* .7z

Este sistema soporta las roms comprimidas en formato .zip/.7z. No obstante, se trata sólo de un archivo.

Los ficheros contenidos en los .zip/.7z deben tener las extensiones citadas previamente.
Además, cada fichero .zip/.7z sólo debe contener un fichero rom al interior.

### Emplazamiento

Disponed vuestras roms como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 zx81
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.zip**

>Se aconsejan las roms del proyecto **TOSEC**.
{.is-success}

>Para más información sobre las roms, consultad [este tutorial](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

>Para conservar las configuraciones personalizadas cuando se produce una actualización del sistema Recalbox, os aconsejamos utilizar las [Sobrecargas de configuración](./../../../advanced-usage/configuration-override).
{.is-info}

### Acceder a las opciones

Podéis configurar las opciones del core de dos formas diferentes.

* Via el Menú RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via el fichero `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Opciones del core

| Opción | Valor de la opción | Variable (para la sobrecarga) | Valores posibles |
| --- | --- | --- | --- |
| Tape Fast Load | `Activé` ✅ / `Désactivé` | `81_fast_load` | `enabled` / `disabled` |
| 8K-16K Contents | `auto` ✅ / `ROM shadow` / `RAM` / `dK'tronics 4K Graphics ROM + 4K RAM` | `81_8_16_contents` | `auto` / `ROM shadow` / `RAM` / `dK'tronics 4K Graphics ROM + 4K RAM` |
| Size Video Border | `normal` ✅ / `small` / `none` | `81_border_size` | `normal` / `small` / `none` |
| High Resolution | `auto` ✅ / `none` / `WRX` | `81_highres` | `auto` / `none` / `WRX` |
| Emulate Chroma 81 | `auto` ✅ / `Désactivé` / `Activé` | `81_chroma_81` | `auto` / `disabled` / `enabled` |
| Video Presets | `clean` ✅ / `tv` / `noisy` | `81_video_presets` | `clean` / `tv` / `noisy` |
| Sound emulation | `auto` ✅ / `none` / `Zon X-81` | `81_sound` | `auto` / `none` / `Zon X-81` |
| Joypad Left mapping | `auto` ✅ / `default` / `new line` / `shift` / `space` / `.` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` | `81_joypad_left` | `auto` / `default` / `new line` / `shift` / `space` / `.` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` |
| Joypad Right mapping | `auto` ✅ / `default` / `new line` / `shift` / `space` / `.` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` | `81_joypad_right` | `auto` / `default` / `new line` / `shift` / `space` / `.` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` |
| Joypad Up mapping | `auto` ✅ / `default` / `new line` / `shift` / `space` / `.` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` | `81_joypad_up` | `auto` / `default` / `new line` / `shift` / `space` / `.` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` |
| Joypad Down mapping | `auto` ✅ / `default` / `new line` / `shift` / `space` / `.` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` | `81_joypad_down` | `auto` / `default` / `new line` / `shift` / `space` / `.` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` |
| Joypad A button mapping | `auto` ✅ / `default` / `new line` / `shift` / `space` / `.` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` | `81_joypad_a` | `auto` / `default` / `new line` / `shift` / `space` / `.` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` |
| Joypad B button mapping | `auto` ✅ / `default` / `new line` / `shift` / `space` / `.` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` | `81_joypad_b` | `auto` / `default` / `new line` / `shift` / `space` / `.` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` |
| Joypad X button mapping | `auto` ✅ / `default` / `new line` / `shift` / `space` / `.` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` | `81_joypad_x` | `auto` / `default` / `new line` / `shift` / `space` / `.` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` |
| Joypad Y button mapping | `auto` ✅ / `default` / `new line` / `shift` / `space` / `.` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` | `81_joypad_y` | `auto` / `default` / `new line` / `shift` / `space` / `.` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` |
| Joypad L button mapping | `auto` ✅ / `default` / `new line` / `shift` / `space` / `.` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` | `81_joypad_l` | `auto` / `default` / `new line` / `shift` / `space` / `.` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` |
| Joypad R button mapping | `auto` ✅ / `default` / `new line` / `shift` / `space` / `.` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` | `81_joypad_r` | `auto` / `default` / `new line` / `shift` / `space` / `.` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` |
| Joypad L2 button mapping | `auto` ✅ / `default` / `new line` / `shift` / `space` / `.` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` | `81_joypad_l2` | `auto` / `default` / `new line` / `shift` / `space` / `.` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` |
| Joypad R2 button mapping | `auto` ✅ / `default` / `new line` / `shift` / `space` / `.` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` | `81_joypad_r2` | `auto` / `default` / `new line` / `shift` / `space` / `.` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` |
| Transparent Keyboard Overlay | `Activé` ✅ / `Désactivé` | `81_keybovl_transp` | `enabled` / `disabled` |
| Time to Release Key in ms | `100` ✅ / `300` / `500` / `1000` | `81_key_hold_time` | `100` / `300` / `500` / `1000` |

## ![](/emulators/external-links.png) Enlaces externos

* **Código fuente utilizado** : [https://github.com/libretro/81-libretro/](https://github.com/libretro/81-libretro/)
* **Documentación Libretro** : [https://docs.libretro.com/library/eightyone/](https://docs.libretro.com/library/eightyone/)