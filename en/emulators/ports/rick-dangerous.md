---
title: Rick Dangerous
description: 
published: true
date: 2021-08-08T23:52:07.474Z
tags: ports, rick dangerous
editor: markdown
dateCreated: 2021-06-29T18:52:54.835Z
---

![](/emulators/ports/rick-dangerous.png){.align-center}

## Technical data

* **Created by**: Core Design
* **Year of release**: 1989
* **Genre**: Platesforms, Die and retry
* **Operating system**: Amiga 500, Amstrad CPC, Atari ST, Commodore 64, DOS and ZX Spectrum.
* **Programmation, conception**: Simon Phipps
* **Graphisms**: Simon Phipps, Terry Lloyd
* **Musiqc**: Dave Pridmore
* **Level design**: Rob Toone, Bob Churchill
* **Amstrad and Spectrum programmation**: Dave Pridmore
* **C64 programmation**: Stu Gregg
* **Editor**: Rainbird Software
* **PEGI**: 3

## Presentation

**Rick Dangerous** is a video game developed by Core Design and published by Rainbird Software in 1989. The first original game and the first success of Core Design, Rick Dangerous was released on Amiga 500, Amstrad CPC, Atari ST, Commodore 64, DOS and ZX Spectrum. The game gained a cult status and many amateur versions - ports, clones and remakes - were released afterwards.

Long before Lara Croft, in the 1980s and early 1990s, Rick Dangerous was the Indiana Jones of video games, fleeing rolling rocks, avoiding traps, from South America to a futuristic missile base to Egypt to Schwarzendumpf Castle.
It's a platformer with cartoonish graphics in which the player is Rick Dangerous, a sort of Indiana Jones in search of the lost Goolu tribe in the Amazon. But his plane crashes, and that's when the game begins. The action takes place in 1945.

## Emulators

[Libretro XRick](libretro-xrick)