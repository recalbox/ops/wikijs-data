---
title: Motores de juegos
description: Engines
published: true
date: 2023-09-04T07:10:40.801Z
tags: motores
editor: markdown
dateCreated: 2023-09-04T07:10:40.801Z
---

Los motores de juegos (engines) permiten crear juegos a partir de diferentes recursos. En vez de programar el juego desde cero, el motor proporciona un entorno bien definido y una interfaz para el diseño del juego y la entrada de datos.