---
title: Libretro EasyRPG
description: 
published: true
date: 2024-06-14T17:08:47.160Z
tags: libretro, easyrpg
editor: markdown
dateCreated: 2021-05-21T08:28:19.862Z
---

**Libretro EasyRPG** est un port de de l'émulateur **EasyRPG**.

## ![](/emulators/license.svg) Licence

Ce core est sous licence [**GPLv3**](https://github.com/EasyRPG/Player/blob/master/COPYING).

## ![](/emulators/compatibility.png) Compatibilité

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Fonctionnalités

| Fonctionnalité | Supporté |
| :---: | :---: |
| Redémarrage | ✔ |
| Captures d'écran | ✔ |
| Sauvegardes | ✔ |
| Contrôles | ✔ |

## ![](/emulators/bios.svg) BIOS

>**Aucun bios n'est requis.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Extensions supportées

Les roms doivent avoir l'extension :

* .ini

### Emplacement

Placez les roms comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 easyrpg
┃ ┃ ┃ ┃ ┣ 📁 jeu.rpg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **RPG_RT.ini**

Chaque jeu a son lot de fichiers et doit avoir le fichier **RPG_RT.ini** fourni avec. Chaque jeu doit se trouver dans son répertoire.

### Liste de jeux compatibles

Vous pouvez trouver la liste des jeux compatibles en suivant [ce lien](https://community.easyrpg.org/t/compatibility-list/283).

## ![](/emulators/advanced-configuration.png) Configuration avancée de l'émulateur

>Pour pouvoir conserver vos configurations personnalisées lors d'une mise à jour, nous vous conseillons d'utiliser notre fonctionnalité [Surcharges de configuration](./../../../advanced-usage/configuration-override).
{.is-info}

### Accéder aux options

Vous pouvez configurer diverses options de deux façons différentes.

* Via le menu RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via le fichier `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Options du core

| Options | Valeurs des options | Variable (pour surcharge) | Valeurs des variables |
| --- | --- | --- | --- |
| Debug menu and walk through walls | `Désactivé` ✅ / `Activé` | `easyrpg_debug_mode` | `disabled` / `enabled` |

## ![](/emulators/external-links.png) Liens externes

* **Code source utilisé** : [https://github.com/EasyRPG/Player/](https://github.com/EasyRPG/Player/)
* **Documentation Libretro** : [https://docs.libretro.com/library/easyrpg/](https://docs.libretro.com/library/easyrpg/)
* **Projet RPG Maker 2000** : [https://community.easyrpg.org/t/rpg-maker-2000-preservation-project/199](https://community.easyrpg.org/t/rpg-maker-2000-preservation-project/199)
* **RPG Maker**: [https://rpgmaker.net/](https://rpgmaker.net/)