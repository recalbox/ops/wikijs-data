---
title: Con un juego Naomi
description: 
published: true
date: 2023-09-23T20:32:40.721Z
tags: lightgun, naomi, 7.2+, calibración
editor: markdown
dateCreated: 2023-09-23T20:32:40.721Z
---

## Ejemplo de calibración de un juego de Naomi

1) Activad el modo servicio de Naomi en RetroArch (`Hotkey` + `B`).

2) Configurad el mando del jugador 1.

3) Volved al juego (`Hotkey` + `B`) e lanzad el MENÚ DE PRUEBA con el botón `L3`.

4) Seleccionad el menú con `R3` y confirmad con `L3`.

5) Una vez en el modo de calibración o justo antes, volved a RetroArch (`Hotkey` + `B`) y poned un "lightgun" en el jugador 1.

6) Una vez finalizada la calibración, es posible que tengáis que repetir el paso 2 para salir del MENÚ DE PRUEBA.

7) Cuando volváis al juego, volved a entrar en RetroArch (`Hotkey` + `B`) para volver a poner el "lightgun" en el jugador 1.

8) Ahora podéis probar el resultado de la calibración.

## Ejemplo de calibración de un juego Naomi (_el caso de The House of The Dead 2_)

Por ejemplo, para este tipo de juegos (The house of dead 2 en Naomi), entramos en el menú "**NAOMI TEST MODE**", pero hacerlo no es facil...

**Requisitos:** un mando

1) Entrad en RetroArch (`Hotkey` + `B`) - Menú rápido

![](/basic-usage/features/lightgun/calibration/naomi1.png){.full-width}

2) "Opciones"

![](/basic-usage/features/lightgun/calibration/naomi2.png){.full-width}

3) Vérificad que la opción "Allow NAOMI Service Buttons" está activada.

![](/basic-usage/features/lightgun/calibration/naomi3.png){.full-width}

4) Id al menú principal / Configuración / Entradas

![](/basic-usage/features/lightgun/calibration/naomi4.png){.full-width}

5) Dentro de "Teclas del port 1"

![](/basic-usage/features/lightgun/calibration/naomi5.png){.full-width}

6) Seleccionamos cómo "controlador" nuestro mando (necesitamos un mando y no un lightgun para activar el NAOMI TEST MODE)

7) Para terminar, (`Hotkey` + `B`) para salir del menú rápido y volver al juego

**NAOMI TEST MODE :** accedemos pulsado `L3` en el mando

![](/basic-usage/features/lightgun/calibration/naomi6.png){.full-width}

8) Seleccionamos "**GAME TEST MODE**" gracias a `R3`, y validamos la entrada del menú con `L3`.

![Sélection du menu](/basic-usage/features/lightgun/calibration/naomi7.png){.full-width}

![Après validation, on arrive dans le menu "GAME TEST MODE"](/basic-usage/features/lightgun/calibration/naomi8.png){.full-width}

9) Ahora podemos seleccionar el menú "**GUN SETTING**" con `R3` y validar la entrada del menú con `L3`.

![Sélection du menu](/basic-usage/features/lightgun/calibration/naomi9.png){.full-width}

![Après validation, on arrive dans le menu "GAME TEST MODE"](/basic-usage/features/lightgun/calibration/naomi10.png){.full-width}

10) Podemos ahora seleccionar el menú "**PLAYER1 GUN ADJUSTEMENT**" con `R3` y validar la entrada del menú con `L3`.

![Sélection du menu](/basic-usage/features/lightgun/calibration/naomi11.png){.full-width}

![](/basic-usage/features/lightgun/calibration/naomi12.png){.full-width}

11) **Aviso**, tendréis que volver a RetroArch (`Hotkey` + `B`) y luego al menú principal / Configuración / Entradas / Teclas de puerto 1 para reactivar el "lightgun" (en vez del "controlador") para calibrarlo.

Ahora podemos pasar a calibrar la pistola **tirando en el visor azul arriba a la izquierda y abajo a la derecha**.

![](/basic-usage/features/lightgun/calibration/naomi13.png){.full-width}

![Après selection des 2 coins, le calcul de la calibration opère...](/basic-usage/features/lightgun/calibration/naomi14.png){.full-width}

12) Podemos ver el resultado y testear el OFF SCREEN (que se encuentra abajo a la izquierda dentro de este juego en concreto).

![](/basic-usage/features/lightgun/calibration/naomi15.png){.full-width}

![](/basic-usage/features/lightgun/calibration/naomi16.png){.full-width}

13) **Aviso**, tendréis que volver a RetroArch (`Hotkey` + `B`) y luego al menú principal / Configuración / Entradas / Teclas de puerto 1 para reactivar el "controlador" (en vez del "lightgun") para poder salir.

Podemos validar la configuración de calibración con **R3** del mando y volver al menú precedente.

14) Podemos recomenzar a partir de la etapa 3 pero seleccionando el menú "**PLAYER2 GUN ADJUSTEMENT**" si necesitamos realizar esta configuración para jugar a dobles.

15) Para terminar, seleccionamos EXIT con `L3`/`R3` en cada uno de los menús hasta salir completamente y volver al juego.

![On sort du menu "GUN SETTING"](/basic-usage/features/lightgun/calibration/naomi17.png){.full-width}

![On sort du menu "GAME TEST"](/basic-usage/features/lightgun/calibration/naomi18.png){.full-width}

![On sort du menu "NAOMI TEST MODE"](/basic-usage/features/lightgun/calibration/naomi19.png){.full-width}

![Le système de jeu se relance](/basic-usage/features/lightgun/calibration/naomi20.png){.full-width}

16) Por último, antes de jugar, os aconsejamos reiniciar el juego para refrescar la configuración de RetroArch, para que se aplique la configuración predeterminada.

![](/basic-usage/features/lightgun/calibration/naomi21.png){.full-width}

>Guardad bien los ficheros generados, para no tener que volver a realizar estas etapas. La configuración quedará grabada dentro de vuestra carpeta "saves":
>
>![](/basic-usage/features/lightgun/calibration/calibration2.png){.full-width}
{.is-warning}