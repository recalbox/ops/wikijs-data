---
title: Licencia de paquetes
description: 
published: true
date: 2023-08-31T14:47:57.591Z
tags: licencia, paquetes
editor: markdown
dateCreated: 2023-08-31T14:47:57.591Z
---

Recalbox es un sistema que se compila gracias al sistema de paquetes de [Buildroot](https://buildroot.org/). Buildroot regrupa varios paquetes con diferentes licencias y sirve para generar un sistema operativo completo y funcional.

Cada paquete de Buildroot está protegido y cubierto por sus propias licencias y pertenece a sus autores respectivos.

[Hemos trabajado](https://gitlab.com/recalbox/recalbox/issues/1501) para que la [información legal](https://buildroot.org/downloads/manual/manual.html#_complying_with_open_source_licenses) del código fuente de Recalbox se encuentre disponible en cada paquete, para permitir una visión global y exhaustiva.

Mientras tanto, os invitamos a verificar manualmente el o las licencias de cada paquete.

>Aunque ciertas licencias pueden parecer incompatibles entre ellas, Recalbox no se considera una "obra derivada" o una "versión modificada" de ellas, sino como un simple agregador.
>
>Como tal, la viralidad de ciertas licencias no tiene ningún impacto en la concesión de licencias de otros paquetes o en la concesión de licencias del propio Recalbox.
>
>Consultad la [FAQ de la licencia pública general (GPL)](https://www.gnu.org/licenses/gpl-faq.fr.html) para más detalles.
{.is-info}