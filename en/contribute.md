---
title: Contributing to the Recalbox Wiki
description: Please read this guide before contributing to the wiki.
published: true
date: 2021-08-08T18:34:33.239Z
tags: wiki, contribution
editor: markdown
dateCreated: 2021-05-27T15:42:54.905Z
---

## Documentation Naming System

The documentation follows a certain nomenclature that allows the Recalbox wiki to have identical URLs for each translation of every page.

Take this page for example. The French translation has this URL:
`/fr/contribute/`
And the English page has this:
`/en/contribute`

This lets people quickly find the translation of a section, and allows contributors to make translations more easily.
This is why the URLs of all documents appear in English.

## Formatting Rules

The documents are written in [markdown](https://docs.requarks.io/editors/markdown). Markdown is a very simple and accessible document formatting tool. Feel free to use this document as an example to start writing your own.

### Titles

The title of the page is to be defined in the editor's page options. 
![title-metadata.png](/contribute/title-metadata.png)

It should be the only level 1 heading in the document.

Within the page's contents, the titles should start with a level 2 heading (double hash: `##`):
![double-hash.png](/contribute/double-hash.png)

### Citation blocks

To let readers know about important information or details, you can use quote blocks:
```markdown
> This is information!
> {.is-info}
```

> This is information!
> {.is-info}

## Supported Markdown version

For more information on supported markdown capabilities, visit https://docs.requarks.io/editors/markdown.

## Images

When adding images to your documents, make sure you replicate the document hierarchy in the image folders, omitting the language.

For example, on this page, images should be created in the `/contribute` directory.


## Localization

### How to translate a page

1. Login to the wiki with your Discord account
2. Go to the page to translate, in the default language of the wiki (FR for now)
3. Click on the language icon at the top of the page and select the language you want to translate the page into
![select-lang.png](/contribute/select-lang.png)
4. A screen offers you to create a new page, click on the create page button
5. Click on the Markdown editor
![markdown.png](/contribute/markdown.png)
6. Open a new tab on the original page and click on edit in the page actions menu
![edit.png](/contribute/edit.png)
7. Access the title, tags and description of the page by clicking on the PAGE button:
![editpage.png](/contribute/editpage.png)
8. You can translate these elements and add them in the form of your newly created page
9. After closing the title popup on the original page, copy the markdown content of the page, and go to https://www.deepl.com/translator to paste your text and get a pre-translated version of the page.
![deepl.png](/contribute/deepl.png)
10. Now you just have to check the translation, the images and the links of the newly created page!
11. Don't forget to remove the last line of the text (the translated with deepl line)
12. Thank you very much for your contribution 🙏

### Relative links

Except in certain cases, page links must be relative to the current language. For example, on this page, a link to the `/en/home/` page should point to `./home`:

```markdown
Visit the [home page](./home)
```
This will allow you to create new pages for your language more easily, so you do not have to specify the language yourself.

### Adding new languages

If your language does not appear in the Wiki menu, and you want to start a translation, please contact the team on our [social networks, Discord, or the forum](./presentation/useful-links).

## Tutorial difficulty level

In order to make the Recalbox experience fun and accessible, when you create a tutorial, mention its difficulty level, calibrated as follows:

1.  Any action that does not require entering the advanced settings in the general menu or the Recalbox .conf files will be considered as : _level:star:_

1. Any action that will require configuring the advanced settings to make changes that may cause instabilities: _level:star::star:_

1.  Any action that will require changing entries in the recalbox.conf file: _level :star::star::star:_ (caution recommended).

1.  Any action that will require SSH manipulations and/or boot file modifications (MobaXTerm or Powershell the Staff has not yet agreed on the software to use): _level_ :star::star::star::star:

1.  Any action that will require to create a merge request : _level_ :star::star::star::star::star:

1.  Finally any tutorial that would try to explain the use of clrmamepro will be automatically classified in the category: :scream: _**Nightmare**_

Example:

- Put a rom, a bios, change the language: :star:
- Change the theme, restore factory settings, change the default core: :star::star: 
- Activate AI retroarch translation: :star::star::star:

*To make the star emoji appear on your tutorials, put ":" in front and behind "star" = : star: all attached.*
