---
title: Los romsets para Recalbox
description: 
published: true
date: 2023-09-09T12:37:41.864Z
tags: romsets
editor: markdown
dateCreated: 2023-09-09T12:37:41.864Z
---

**Leyenda:**

  🥇    ==> Mejor set disponible
  🥈    ==> Vale para completar (para las roms hack, traducciones, etc...)
  🥉    ==> A utilizar como último recurso
  🎮    ==> Set recomendado para el Netplay
  ❌    ==> No hay set disponible para este grupo
  ❎    ==> Set existente pero **"No aconsejado"**
  ❓    ==> Desconocido

## Arcade

>En el arcade, el romset es distinto dependiendo del core que utiliza.
{.is-info}

### Atomiswave

| Core | Romset | Arquitectura |
| :---: | :---: | :---: |
| **Flycast** | MAME 0.135 o superior, 0.239 aconsejado | Pi4 / x86_64 |

### FinalBurn

| Core | Romset | Arquitectura |
| :---: | :---: | :---: |
| **FBNeo** | FinalBurn Neo v1.0.0.03 | Pi2 / Pi3 / Pi4 / Odroid / x86_84 |
| **PiFBA** | fba 0.2.96.71 | Pi0 / Pi0w / Pi1 |

### Laserdisc (Daphne)

* Algunos juegos pueden descargarse directa y legalmente utilizando la herramienta DaphneLoader de la [distribución Windows de Daphne](http://www.daphne-emu.com/site3/index_hi.php). 
* Para otros (Dragon's Lair, Dragon's Lair II, Space Ace, etc.), es necesario demostrar que se dispone de una licencia válida para el juego (versión DVD, por ejemplo).

Una vez descargados, copiad los ficheros necesarios (ROM y ficheros de imagen del disco-laser) dentro de vuestra Recalbox, como se explica en la wiki.

### Mame

| Core | Romset | Arquitectura |
| :---: | :---: | :---: |
| AdvanceMame | MAME 0.106 | Todos los Pi |
| Mame2000 | MAME 0.37b5 | Pi1 |
| Mame2003 | MAME 0.78 | Todos |
| Mame2003Plus  | MAME 0.78-0.188 | Todos |
| Mame2010 | MAME 0.139 | Pi2 / Pi3 / Pi4 / x86 / x86_64 |
| Mame2015 | MAME 0.160 | Pi3 / Pi4 / x86 / x86_64 |
| Mame | MAME 0.239 | x86_64 |

### Neo-Geo

| Core | Romset | Arquitectura |
| :---: | :---: | :---: |
| **FBNeo** | FinalBurn Neo v1.0.0.03 | Todas las plataformas |
| **Mame** | Consultad los romsets [aquí abajo](./romsets-for-recalbox#mame). | Todas las plateformas |

### Naomi

| Core | Romset | Arquitectura |
| :---: | :---: | :---: |
| **Flycast** | MAME 0.135 o superior, 0.239 aconsejado | Pi4 / x86_64 |

### Naomi GD-ROM

| Core | Romset | Arquitectura |
| :---: | :---: | :---: |
| **Flycast** | MAME 0.135 o superior, 0.239 aconsejado | Pi4 / x86_64 |

>Para más información sobre las roms de arcade, consultad [esta página](./../../../../advanced-usage/arcade-in-recalbox).
{.is-info}

### Sega Model 3

| Core | Romset | Arquitectura |
| :---: | :---: | :---: |
| **Supermodel** | MAME 0.135 o superior, 0.235 aconsejado | x86_64 |

## Consolas de salón

| Plataforma | No-Intro | TOSEC | Redump | Trurip |
| :---: | :---: | :---: | :---: | :---: |
| **32x** | 🥇 🎮 | 🥈 | ❌ | 🥉 |
| **Amiga CD32** | ❌ | 🥈 | 🥇 | 🥉 |
| **Amiga CDTV** | ❌ | 🥈 | 🥇 | 🥉 |
| **Amstrad GX4000** | ❌ | 🥇 | ❌ | 🥈 |
| **Atari 2600** | 🥇 🎮 | 🥈 | ❌ | ❌ |
| **Atari 5200** | 🥇 | 🥈 | ❌ | 🥉 |
| **Atari 7800** | 🥇 | 🥈 | ❌ | 🥉 |
| **Atari Jaguar** | 🥇 🎮 | 🥈 | ❌ | 🥉 |
| **ColecoVision** | 🥇 | 🥈 | ❌ | 🥉 |
| **Dreamcast** | ❌ | 🥇 | 🥈 | 🥉 |
| **Fairchild Channel F** | 🥇 | 🥈 | ❌ | 🥉 |
| **Famicom Disk System** | 🥇 🎮 | 🥈 | ❌ | 🥉 |
| **GameCube** | ❌ | 🥈 | 🥇 🎮 | 🥉 |
| **Intellivision** | 🥇 | 🥈 | ❌ | 🥉 |
| **Master System** | 🥇 🎮 | 🥈 | ❌ | 🥉 |
| **Megadrive** | 🥇 🎮 | 🥈 | ❌ | 🥉 |
| **Multivision** | ❌ | 🥇 | ❌ | ❌ |
| **NeoGeo CD** | ❌ | ❌ | 🥇 | 🥇 |
| **NES** | 🥇 🎮 | 🥈 | ❌ | ❌ |
| **Nintendo 64** | 🥇 | 🥈 | ❌ | ❌ |
| **Nintendo 64DD** | 🥇 | 🥈 | ❌ | ❌ |
| **Panasonic 3DO** | ❌ | 🥈 | 🥇 | 🥉 |
| **Pc-Engine** | 🥇 🎮 | 🥈 | ❌ | 🥉 |
| **Pc-Engine CD** | ❌ | 🥈 | 🥇 | 🥉 |
| **PCFX** | ❌ | 🥉 | 🥇 | 🥈 |
| **PlayStation 1** | ❌ | 🥉 | 🥇 | 🥈 |
| **PlayStation 2** | ❌ | 🥉 | 🥇 | 🥈 |
| **Satellaview** | 🥇 | 🥉 | ❌ | ❌ |
| **Saturn** | ❌ | 🥉 | 🥇 | 🥈 |
| **Sega Mega CD** | ❌ | 🥉 | 🥇 | 🥈 |
| **Sega SG1000** | 🥇 🎮 | 🥉 | ❌ | ❌ |
| **Super Famicom** | 🥇 | 🥈 | ❌ | 🥉 |
| **SuFami Turbo** | 🥇 | 🥈 | ❌ | ❌ |
| **Super Nintendo** | 🥇 🎮 | 🥈 | ❌ | ❌ |
| **SuperGrafx** | 🥇 🎮 | 🥈 | ❌ | 🥉 |
| **Vectrex** | 🥇 | 🥈 | ❌ | 🥉 |
| **VideoPac** | 🥇 | 🥈 | ❌ | 🥉 |
| **Virtual Boy** | 🥇 | 🥈 | ❌ | 🥉 |
| **Wii** | ❌ | 🥈 | 🥇 🎮 | ❌ |

## Consolas portátiles

| Plataforma | No-Intro | Tosec | Redump | Trurip |
| :---: | :---: | :---: | :---: | :---: |
| **Game and Watch** | ❌ | ❌ | ❌ | ❌ |
| **Game Boy** | 🥇 | 🥈 | ❌ | 🥉 |
| **Game Boy Color** | 🥇 | 🥈 | ❌ | 🥉 |
| **Game Boy Advance** | 🥇 | 🥈 | ❌ | 🥉 |
| **Game Gear** | 🥇 | 🥈 | ❌ | 🥉 |
| **Lynx** | 🥇 | 🥈 | ❌ | 🥉 |
| **Nintendo DS** | 🥇 | ❌ | ❌ | ❌ |
| **NeoGeo Pocket** | 🥇 | 🥈 | ❌ | 🥉 |
| **NeoGeo Pocket Color** | 🥇 | 🥈 | ❌ | 🥉 |
| **Palm OS** | 🥇 | ❌ | 🥈 | 🥉 |
| **Pokémini** | 🥇 | 🥈 | ❌ | 🥉 |
| **PSP** | 🥇 | ❌ | 🥈 | ❌ |
| **Wonderswan** | 🥇 | 🥈 | ❌ | 🥉 |
| **Wonderswan Color** | 🥇 | 🥈 | ❌ | 🥉 |

## Ordenadores

| Plataforma | No-Intro | Tosec | Redump | Trurip |
| :---: | :---: | :---: | :---: | :---: |
| **Amiga 600** | ❌ | 🥇 | ❌ | ❌ |
| **Amiga 1200** | ❌ | 🥇 | ❌ | ❌ |
| **Amstrad CPC** | 🥈 | 🥇 | ❌ | 🥉 |
| **Apple II** | ❌ | 🥇 | ❌ | 🥈 |
| **Apple II Gs** | ❌ | 🥇 | ❌ | 🥈 |
| **Atari 800** | ❌ | 🥇 | ❌ | 🥈 |
| **Atari St** | 🥈 | 🥇 | ❌ | 🥉 |
| **Commodore 64** | 🥈 | 🥇 | ❌ | 🥉 |
| **Dragon 32/64** | ❌ | 🥇 | ❌ | ❌ |
| **Elektronika BK** | ❌ | 🥇 | ❌ | ❌ |
| **MSX 1** | 🥈 | 🥇 | ❌ | 🥉 |
| **MSX 2** | 🥈 | 🥇 | ❌ | 🥉 |
| **MSX Turbo R** | 🥈 | 🥇 | ❌ | 🥉 |
| **NEC Pc8801** | ❌ | 🥈 | 🥇 | 🥉 |
| **NEC Pc9801** | ❌ | 🥈 | 🥇 | 🥉 |
| **Oric Atmos** | ❌ | 🥇 | ❌ | ❌ |
| **Samcoupé** | ❌ | 🥇 | ❌ | 🥈 |
| **Sharp X1** | ❌ | 🥇 | ❌ | 🥈 |
| **Sharp X68000** | ❌ | 🥈 | 🥇 | 🥉 |
| **Sinclair ZXspectrum** | 🥇 | 🥈 | ❌ | 🥉 |
| **Sinclair ZX81** | ❌ | 🥇 | ❌ | 🥈 |
| **Spectravideo SV-318** | ❌ | 🥇 | ❌ | 🥈 |
| **Tandy TRS-80 Color Computer** | ❌ | 🥇 | ❌ | ❌ |
| **Texas Instruments TI-99/4A** | ❌ | 🥇 | ❌ | ❌ |
| **Thomson MO/TO** | ❌ | 🥇 | ❌ | 🥈 |