---
title: 4. Les objets
description: 
published: true
date: 2024-04-03T22:42:33.044Z
tags: themes, 9.2+
editor: markdown
dateCreated: 2024-01-23T22:27:46.589Z
---

## Introduction

Le contenu des fichiers de votre thème doit suivre une certaine écriture. Chaque objet doit se placer dans les balises `<view></view>`.

## Balise `<view>`

Les écrans de l'interface sont gérés via ce qu'on appelle des vues. Ces vues peuvent avoir plusieurs attributs.

#### Attribut `name`

- Obligatoire : oui

L'attribut `name` permet d'indiquer sur quel écran son contenu doit agir. Il peut avoir une de ces valeurs en même temps :

- `system` : correspond à la liste des systèmes.
- `detailed` : correspond à la liste des jeux.
- `menu` : correspond aux menus des options.
- `gameclip` : correspond à l'écran de veille « clips de jeux ».

Cet attribut peut avoir une ou plusieurs valeurs en même temps :

```xml
<view name="system">
  ...
</view>
```

```xml
<view name="system, detailed">
  ...
</view>
```

L'utilisation de plusieurs valeurs implique que vous utilisez les mêmes données dans la balise `<view>` dans les endroits différents (dans le second exemple ci-dessus, dans la vue système et la vue de la liste des jeux).

## Objet `<image>`

Les objets `<image>` permettent d'afficher une image aux endroits souhaités. Cet objet peut avoir plusieurs attributs :

#### Attribut `name`

- Obligatoire : oui

L'attribut `name` permet d'indiquer sur quel élément visuel le contenu doit agir. Il peut avoir une ou plusieurs de ces valeurs en même temps.

Liste des valeurs utilisables dans la liste des systèmes (`<view name="system">`) :

- `logo` : l'image du logo du système.

Liste des valeurs utilisables dans la liste des jeux (`<view name="detailed">`) :

- `console` : l'image affichant la console du système.
- `logo` : l'image du logo du système.
- `md_folder_image_0` : sur un dossier, première miniature.
- `md_folder_image_1` : sur un dossier, seconde miniature.
- `md_folder_image_2` : sur un dossier, troisième miniature.
- `md_folder_image_3` : sur un dossier, quatrième miniature.
- `md_folder_image_4` : sur un dossier, cinquième miniature.
- `md_folder_image_5` : sur un dossier, sixième miniature.
- `md_folder_image_6` : sur un dossier, septième miniature.
- `md_folder_image_7` : sur un dossier, huitième miniature.
- `md_folder_image_8` : sur un dossier, neuvième miniature.
- `md_image` : emplacement de l'image de la miniature du scrap.
- `md_region1` : première région du jeu sélectionné.
- `md_region2` : seconde région du jeu sélectionné.
- `md_region3` : troisième région du jeu sélectionné.
- `md_region4` : quatrième région du jeu sélectionné.

Liste des valeurs utilisables dans les clips de jeux (`<view name="gameclip">`) :

- `favoriteIcon` : icône de favori.
- `logo` : l'image du logo du système.
- `md_image` : emplacement de l'image de la miniature du scrap.
- `md_thumbnail` : miniature du scrap.
- `recalboxLogo` : logo de Recalbox.

```xml
<image name="logo">
  ...
</image>
```

#### Attribut `extra`

Vous pouvez créer votre propre valeur pour l'attribut `name` en dehors de la liste ci-dessus, mais vous devrez ajouter l'attribut `extra="true"` pour pouvoir être pris en compte.

```xml
<image name="background" extra="true">
  ...
</image>
```

#### Attribut `region`

Si vous spécifiez une image à utiliser pour une région spécifique, vous pouvez utiliser l'attribut `region`.

```xml
<image name="logo" region="us">
  ...
</image>
```

L'objet `<image>` peut contenir plusieurs autres propriétés.

#### Propriété `<pos>`

La propriété `<pos>` permet d'indiquer la position de l'objet parent. La position à indiquer correspond au point le plus en haut à gauche de l'objet. Celui-ci contient 2 valeurs. La première valeur correspond au positionnement depuis la gauche et la seconde valeur au positionnement depuis le haut.

Les valeurs sont des pourcentages compris en 0 (0%) et 1 (100%).

```xml
<pos>0.33 0.25</pos>
```

Dans l'exemple ci-dessus, on peut voir que la position indique de positionner l'objet parent à 33% de la gauche et à 25% du haut.

#### Propriété `<size>`

La propriété `<size>` permet de définir la taille de l'élément qui contient l'objet parent.

```xml
<size>1 1</size>
```

#### Propriété `<maxSize>`

>Cette propriété est déprécié, et est conservée pour la rétro-compatibilité avec les thèmes existants. Elle sera supprimée dans une future version de Recalbox.
{.is-warning}

La propriété `<maxSize>` permet d'éviter d'avoir l'élément de l'objet parent surdimensionné sur un axe. Par défaut, l'image sera redimensionnée aussi grande que possible afin qu'elle s'adapte à cette taille et conserve son rapport hauteur / largeur.

Les valeurs sont des pourcentages compris en 0 (0%) et 1 (100%).

```xml
<maxSize>0.35 0.1</maxSize>
```

#### Propriété `<keepratio>` ![](/tutorials/newicon-24.png)

La propriété `<keepratio>` permet de conserver le ratio de l'élément parent. À utiliser avec `<size>`.

```xml
<keepratio>true</keepratio>
```

#### Propriété `<origin>`

La propriété `<origin>` permet de définir un nouveau point de départ d'origine pour toutes les tailles de l'objet parent.

Les valeurs sont des pourcentages compris en 0 (0%) et 1 (100%).

```xml
<origin>0.5 0.5</origin>
```

#### Propriété `<rotation>`

La propriété `<rotation>` permet de spécifier un angle de rotation de l'objet parent. Les valeurs positives feront une rotation horaire et les valeurs négatives feront une rotation antihoraire.

```xml
<rotation>90</rotation>
```

#### Propriété `<rotationOrigin>`

La propriété `<rotationOrigin>` permet de définir un nouveau point de départ d'origine pour toutes les tailles de l'objet parent uniquement si une rotation doit être effectuée. 

Les valeurs sont des pourcentages compris en 0 (0%) et 1 (100%).

```xml
<rotationOrigin>0.5 0.5</rotationOrigin>
```

#### Propriété `<path>`

La propriété `<path>` permet de charger un fichier texte au lieu de mettre du texte manuellement.

```xml
<path>./chemin/vers/fichier/texte.txt</path>
```

#### Propriété `<tile>`

La propriété `<tile>` permet de définir si l'image est affichée tel une mosaïque au lieu d'être étirée pour s'adapter à la taille de l'objet. C'est utile pour les arrière-plans.

```xml
<tile>false</tile>
```

#### Propriété `<color>`

La propriété `<color>` permet d'indiquer la couleur du texte de l'objet parent. Les couleurs peuvent être au format RRVVBB ou RRVVBBAA (AA pour la transparence, 00 = transparent et ff = opaque).

```xml
<color>666666</color>
```

#### Propriété `<colorTop>` ![](/tutorials/newicon-24.png)

La propriété `<colorTop>` permet d'indiquer la couleur du haut de l'objet parent. Les couleurs peuvent être au format RRVVBB ou RRVVBBAA (AA pour la transparence, 00 = transparent et ff = opaque).

```xml
<colorTop>666666</colorTop>
```

#### Propriété `<colorBottom>` ![](/tutorials/newicon-24.png)

La propriété `<colorBottom>` permet d'indiquer la couleur du bas de l'objet parent. Les couleurs peuvent être au format RRVVBB ou RRVVBBAA (AA pour la transparence, 00 = transparent et ff = opaque).

```xml
<colorBottom>666666</colorBottom>
```

#### Propriété `<colorLeft>` ![](/tutorials/newicon-24.png)

La propriété `<colorLeft>` permet d'indiquer la couleur de gauche de l'objet parent. Les couleurs peuvent être au format RRVVBB ou RRVVBBAA (AA pour la transparence, 00 = transparent et ff = opaque).

```xml
<colorLeft>666666</colorLeft>
```

#### Propriété `<colorRight>` ![](/tutorials/newicon-24.png)

La propriété `<colorRight>` permet d'indiquer la couleur de droite de l'objet parent. Les couleurs peuvent être au format RRVVBB ou RRVVBBAA (AA pour la transparence, 00 = transparent et ff = opaque).

```xml
<colorRight>666666</colorRight>
```

#### Propriété `<colorTopLeft>` ![](/tutorials/newicon-24.png)

La propriété `<colorTopLeft>` permet d'indiquer la couleur en haut à gauche de l'objet parent. Les couleurs peuvent être au format RRVVBB ou RRVVBBAA (AA pour la transparence, 00 = transparent et ff = opaque).

```xml
<colorTopLeft>666666</colorTopLeft>
```

#### Propriété `<colorTopRight>` ![](/tutorials/newicon-24.png)

La propriété `<colorTopRight>` permet d'indiquer la couleur en haut à droite de l'objet parent. Les couleurs peuvent être au format RRVVBB ou RRVVBBAA (AA pour la transparence, 00 = transparent et ff = opaque).

```xml
<colorTopRight>666666</colorTopRight>
```

#### Propriété `<colorBottomLeft>` ![](/tutorials/newicon-24.png)

La propriété `<colorBottomLeft>` permet d'indiquer la couleur en bas à gauche de l'objet parent. Les couleurs peuvent être au format RRVVBB ou RRVVBBAA (AA pour la transparence, 00 = transparent et ff = opaque).

```xml
<colorBottomLeft>666666</colorBottomLeft>
```

#### Propriété `<colorBottomRight>` ![](/tutorials/newicon-24.png)

La propriété `<colorBottomRight>` permet d'indiquer la couleur en bas à droite de l'objet parent. Les couleurs peuvent être au format RRVVBB ou RRVVBBAA (AA pour la transparence, 00 = transparent et ff = opaque).

```xml
<colorBottomRight>666666</colorBottomRight>
```

#### Propriété `<zIndex>`

La propriété `<zIndex>` permet d'ordonner les éléments par profondeur / couche. Plus sa valeur est grande, plus il sera sur le premier plan. Elle peut être négative.

```xml
<zIndex>10</zIndex>
```

#### Propriété `<disabled>`

La propriété `<disabled>` permet de désactiver l'objet parent.

```xml
<disabled>true</disabled>
```

#### Propriété `<reflection>` ![](/tutorials/newicon-24.png)

La propriété `<reflection>` permet de créer des effets de reflets sous l'objet parent.

La première valeur correspond à la partie supérieure de la réflexion, la seconde valeur correspond à la partie inférieure de la réflexion.

Les valeurs peuvent aller de 0.0 (transparent) à 1.0 (opaque).

```xml
<reflection>0.5 0.5</reflection>
```

## Objet `<box>` ![](/tutorials/newicon.png)

Les objets `<box>` permettent de créer des zones de couleurs décoratives aux endroits souhaités. Cet objet peut avoir plusieurs attributs.

#### Attribut `name`

- Obligatoire : oui

L'attribut `name` permet d'indiquer sur quel élément textuel le contenu doit agir. Il peut avoir une ou plusieurs de ces valeurs en même temps.

Il n'y a pas de liste à fournir, ce contenu vous est propre.

L'objet `<box>` peut contenir plusieurs autres propriétés.

#### Propriété `<pos>`

La propriété `<pos>` permet d'indiquer la position de l'objet parent. La position à indiquer correspond au point le plus en haut à gauche de l'objet. Celui-ci contient 2 valeurs. La première valeur correspond au positionnement depuis la gauche et la seconde valeur au positionnement depuis le haut.

Les valeurs sont des pourcentages compris en 0 (0%) et 1 (100%).

```xml
<pos>0.33 0.25</pos>
```

Dans l'exemple ci-dessus, on peut voir que la position indique de positionner l'objet parent à 33% de la gauche et à 25% du haut.

#### Propriété `<size>`

La propriété `<size>` permet de définir la taille de l'élément qui contient l'objet parent.

```xml
<size>1 1</size>
```

#### Propriété `<origin>`

La propriété `<origin>` permet de définir un nouveau point de départ d'origine pour toutes les tailles de l'objet parent.

Les valeurs sont des pourcentages compris en 0 (0%) et 1 (100%).

```xml
<origin>0.5 0.5</origin>
```

#### Propriété `<rotation>`

La propriété `<rotation>` permet de spécifier un angle de rotation de l'objet parent. Les valeurs positives feront une rotation horaire et les valeurs négatives feront une rotation antihoraire.

```xml
<rotation>90</rotation>
```

#### Propriété `<rotationOrigin>`

La propriété `<rotationOrigin>` permet de définir un nouveau point de départ d'origine pour toutes les tailles de l'objet parent uniquement si une rotation doit être effectuée. 

Les valeurs sont des pourcentages compris en 0 (0%) et 1 (100%).

```xml
<rotationOrigin>0.5 0.5</rotationOrigin>
```

#### Propriété `<color>`

La propriété `<color>` permet d'indiquer la couleur du texte de l'objet parent. Les couleurs peuvent être au format RRVVBB ou RRVVBBAA (AA pour la transparence, 00 = transparent et ff = opaque).

```xml
<color>666666</color>
```

#### Propriété `<colorTop>`

La propriété `<colorTop>` permet d'indiquer la couleur du haut de l'objet parent. Les couleurs peuvent être au format RRVVBB ou RRVVBBAA (AA pour la transparence, 00 = transparent et ff = opaque).

```xml
<colorTop>666666</colorTop>
```

#### Propriété `<colorBottom>`

La propriété `<colorBottom>` permet d'indiquer la couleur du bas de l'objet parent. Les couleurs peuvent être au format RRVVBB ou RRVVBBAA (AA pour la transparence, 00 = transparent et ff = opaque).

```xml
<colorBottom>666666</colorBottom>
```

#### Propriété `<colorLeft>`

La propriété `<colorLeft>` permet d'indiquer la couleur de gauche de l'objet parent. Les couleurs peuvent être au format RRVVBB ou RRVVBBAA (AA pour la transparence, 00 = transparent et ff = opaque).

```xml
<colorLeft>666666</colorLeft>
```

#### Propriété `<colorRight>`

La propriété `<colorRight>` permet d'indiquer la couleur de droite de l'objet parent. Les couleurs peuvent être au format RRVVBB ou RRVVBBAA (AA pour la transparence, 00 = transparent et ff = opaque).

```xml
<colorRight>666666</colorRight>
```

#### Propriété `<colorTopLeft>`

La propriété `<colorTopLeft>` permet d'indiquer la couleur en haut à gauche de l'objet parent. Les couleurs peuvent être au format RRVVBB ou RRVVBBAA (AA pour la transparence, 00 = transparent et ff = opaque).

```xml
<colorTopLeft>666666</colorTopLeft>
```

#### Propriété `<colorTopRight>`

La propriété `<colorTopRight>` permet d'indiquer la couleur en haut à droite de l'objet parent. Les couleurs peuvent être au format RRVVBB ou RRVVBBAA (AA pour la transparence, 00 = transparent et ff = opaque).

```xml
<colorTopRight>666666</colorTopRight>
```

#### Propriété `<colorBottomLeft>`

La propriété `<colorBottomLeft>` permet d'indiquer la couleur en bas à gauche de l'objet parent. Les couleurs peuvent être au format RRVVBB ou RRVVBBAA (AA pour la transparence, 00 = transparent et ff = opaque).

```xml
<colorBottomLeft>666666</colorBottomLeft>
```

#### Propriété `<colorBottomRight>`

La propriété `<colorBottomRight>` permet d'indiquer la couleur en bas à droite de l'objet parent. Les couleurs peuvent être au format RRVVBB ou RRVVBBAA (AA pour la transparence, 00 = transparent et ff = opaque).

```xml
<colorBottomRight>666666</colorBottomRight>
```

#### Propriété `<zIndex>`

La propriété `<zIndex>` permet d'ordonner les éléments par profondeur / couche. Plus sa valeur est grande, plus il sera sur le premier plan. Elle peut être négative.

```xml
<zIndex>10</zIndex>
```

#### Propriété `<disabled>`

La propriété `<disabled>` permet de désactiver l'objet parent.

```xml
<disabled>true</disabled>
```

## Objet `<video>`

Les objets `<video>` permettent d'afficher de la vidéo aux endroits souhaités. Cet objet peut avoir plusieurs attributs.

#### Attribut `name`

- Obligatoire : oui

L'attribut `name` permet d'indiquer sur quel élément textuel le contenu doit agir. Il peut avoir une ou plusieurs de ces valeurs en même temps.

Liste des valeurs utilisables dans la liste des jeux (`<view name="detailed">`) :

- `md_video` : la vidéo.

Liste des valeurs utilisables dans les clips de jeux (`<view name="gameclip">`) :

- `md_video` : la vidéo.

L'objet `<video>` peut contenir plusieurs autres propriétés.

#### Propriété `<pos>`

La propriété `<pos>` permet d'indiquer la position de l'objet parent. La position à indiquer correspond au point le plus en haut à gauche de l'objet. Celui-ci contient 2 valeurs. La première valeur correspond au positionnement depuis la gauche et la seconde valeur au positionnement depuis le haut.

Les valeurs sont des pourcentages compris en 0 (0%) et 1 (100%).

```xml
<pos>0.33 0.25</pos>
```

Dans l'exemple ci-dessus, on peut voir que la position indique de positionner l'objet parent à 33% de la gauche et à 25% du haut.

#### Propriété `<size>`

La propriété `<size>` permet de définir la taille de l'élément qui contient l'objet parent.

```xml
<size>1 1</size>
```

#### Propriété `<maxSize>`

>Cette propriété est déprécié, et est conservée pour la rétro-compatibilité avec les thèmes existants. Elle sera supprimée dans une future version de Recalbox.
{.is-warning}

La propriété `<maxSize>` permet d'éviter d'avoir l'élément de l'objet parent surdimensionné sur un axe. Par défaut, l'image sera redimensionnée aussi grande que possible afin qu'elle s'adapte à cette taille et conserve son rapport hauteur / largeur.

Les valeurs sont des pourcentages compris en 0 (0%) et 1 (100%).

```xml
<maxSize>0.35 0.1</maxSize>
```

#### Propriété `<keepratio>` ![](/tutorials/newicon-24.png)

La propriété `<keepratio>` permet de conserver le ratio de l'élément parent. À utiliser avec `<size>`.

```xml
<keepratio>true</keepratio>
```

#### Propriété `<origin>`

La propriété `<origin>` permet de définir un nouveau point de départ d'origine pour toutes les tailles de l'objet parent.

Les valeurs sont des pourcentages compris en 0 (0%) et 1 (100%).

```xml
<origin>0.5 0.5</origin>
```

#### Propriété `<rotation>`

La propriété `<rotation>` permet de spécifier un angle de rotation de l'objet parent. Les valeurs positives feront une rotation horaire et les valeurs négatives feront une rotation antihoraire.

```xml
<rotation>90</rotation>
```

#### Propriété `<rotationOrigin>`

La propriété `<rotationOrigin>` permet de définir un nouveau point de départ d'origine pour toutes les tailles de l'objet parent uniquement si une rotation doit être effectuée. 

Les valeurs sont des pourcentages compris en 0 (0%) et 1 (100%).

```xml
<rotationOrigin>0.5 0.5</rotationOrigin>
```

#### Propriété `<path>`

La propriété `<path>` permet de charger un fichier image au lieu de mettre une image manuellement.

```xml
<path>./chemin/vers/fichier/image.png</path>
```

#### Propriété `<zIndex>`

La propriété `<zIndex>` permet d'ordonner les éléments par profondeur / couche. Plus sa valeur est grande, plus il sera sur le premier plan. Elle peut être négative.

```xml
<zIndex>10</zIndex>
```

#### Propriété `<disabled>`

La propriété `<disabled>` permet de désactiver l'objet parent.

```xml
<disabled>true</disabled>
```

#### Propriété `<animations>` ![](/tutorials/newicon-24.png)

La propriété `<animations>` permet d'indiquer l'animation à appliquer sur l'objet parent.

Liste des valeurs possibles :

- `bump` : apparition soudaine venant du fond, le contenu bump un peu devant avant de reprendre sa taille initiale.
- `breakingnews` : apparition en tourbillon.
- `fade` : apparition progressive.
- `none` : aucune animation.

Si vous utilisez plusieurs animations différentes, seul la dernière sera retenue.

```xml
<animations>bump,breakingnews,fade</animations>
```

Dans l'exemple ci-dessus, seul `fade` sera appliqué.

#### Propriété `<loops>` ![](/tutorials/newicon-24.png)

La propriété `<loops>` permet d'indiquer le nombre de fois que le contenu de l'objet parent est lue.

```xml
<loops>true</loops>
```

#### Propriété `<delay>` ![](/tutorials/newicon-24.png)

La propriété `<delay>` permet d'indiquer le délai de début de lecture du contenu de l'objet parent.

```xml
<delay>2</delay>
```

#### Propriété `<reflection>` ![](/tutorials/newicon-24.png)

La propriété `<reflection>` permet de créer des effets de reflets sous l'objet parent.

La première valeur correspond à la partie supérieure de la réflexion, la seconde valeur correspond à la partie inférieure de la réflexion.

Les valeurs peuvent aller de 0.0 (transparent) à 1.0 (opaque).

```xml
<reflection>0.5 0.5</reflection>
```

#### Propriété `<link>` ![](/tutorials/newicon-24.png)

Jusqu'à Recalbox 9.2, les objets `<image name="md_image">` et `<video name="md_video>` étaient liés. Maintenant, elles ne le sont plus et peuvent avoir des propriétés différentes.

La propriété `<link>` permet de relier ces 2 objets ensemble comme avant.

```xml
<link>md_image</link>
```

## Objet `<text>`

Les objets `<text>` permettent d'afficher du texte aux endroits souhaités. Cet objet peut avoir plusieurs attributs.

#### Attribut `name`

- Obligatoire : oui

L'attribut `name` permet d'indiquer sur quel élément textuel le contenu doit agir. Il peut avoir une ou plusieurs de ces valeurs en même temps.

Liste des valeurs utilisables dans la liste des systèmes (`<view name="system">`) :

- `systemInfo` : le texte indiquant le nombre de jeux présent dans le système, avec le nombre de jeux favoris et cachés.

Liste des valeurs utilisables dans la liste des jeux (`<view name="detailed">`) :

- `logoText` : le nom du système quand `logo` n'existe pas.
- `md_lbl_rating` : le texte indiquant la note.
- `md_lbl_releasedate` : le texte indiquant la date de sortie.
- `md_lbl_developer` : le texte indiquant le développeur.
- `md_lbl_publisher` : le texte indiquant l'éditeur.
- `md_lbl_genre` : le texte indiquant le genre.
- `md_lbl_lastplayed` : le texte indiquant la dernière fois que vous avez joué au jeu.
- `md_lbl_players` : le texte indiquant le nombre de joueurs.
- `md_lbl_playcount` : le texte indiquant le nombre de fois que le jeu a été joué.
- `md_lbl_favorite` : le texte indiquant si le jeu est dans la liste des favoris.
- `md_developer` : le développeur.
- `md_publisher` : l'éditeur.
- `md_genre` : le genre.
- `md_players` : le nombre de joueurs.
- `md_playcount` : le nombre de fois que le jeu a été joué.
- `md_favorite` : indique si le jeu est dans la liste des favoris.
- `md_description` : le texte descriptif des jeux.
- `md_folder_name` : le texte accompagnant les miniatures sur un dossier.

Liste des valeurs utilisables dans les clips des jeux (`<view name="gameclip">`) :

- `logoText` : le nom du système quand `logo` n'existe pas.
- `md_lbl_gameName` : le texte indiquant le nom du jeu.
- `md_lbl_systemName` : le texte indiquant le nom du système.
- `md_lbl_rating` : le texte indiquant la note.
- `md_lbl_releasedate` : le texte indiquant la date de sortie.
- `md_lbl_developer` : le texte indiquant le développeur.
- `md_lbl_publisher` : le texte indiquant l'éditeur.
- `md_lbl_genre` : le texte indiquant le genre.
- `md_lbl_lastplayed` : le texte indiquant la dernière fois que vous avez joué au jeu.
- `md_lbl_players` : le texte indiquant le nombre de joueurs.
- `md_lbl_playcount` : le texte indiquant le nombre de fois que le jeu a été joué.
- `md_lbl_favorite` : le texte indiquant si le jeu est dans la liste des favoris.
- `md_gameName` : le nom du jeu.
- `md_developer` : le développeur.
- `md_publisher` : l'éditeur.
- `md_genre` : le genre.
- `md_players` : le nombre de joueurs.
- `md_playcount` : le nombre de fois que le jeu a été joué.
- `md_favorite` : indique si le jeu est dans la liste des favoris.
- `md_description` : le texte descriptif des jeux.
- `md_systemName` : le nom du système.

```xml
<text name="md_developer, md_publisher, md_genre, md_playcount, md_lastplayed, md_description, md_players, md_releasedate, md_favorite">
  ...
</text>
```

#### Attribut `extra`

Vous pouvez créer votre propre valeur pour l'attribut `name` en dehors de la liste ci-dessus, mais vous devrez ajouter l'attribut `extra="true"` pour pouvoir être pris en compte.

```xml
<text name="control" extra="true">
  ...
</text>
```

L'objet `<text>` peut contenir plusieurs autres propriétés.

#### Propriété `<pos>`

La propriété `<pos>` permet d'indiquer la position de l'objet parent. La position à indiquer correspond au point le plus en haut à gauche de l'objet. Celui-ci contient 2 valeurs. La première valeur correspond au positionnement depuis la gauche et la seconde valeur au positionnement depuis le haut.

Les valeurs sont des pourcentages compris en 0 (0%) et 1 (100%).

```xml
<pos>0.33 0.25</pos>
```

Dans l'exemple ci-dessus, on peut voir que la position indique de positionner l'objet parent à 33% de la gauche et à 25% du haut.

#### Propriété `<size>`

La propriété `<size>` permet de définir la taille de l'élément qui contient l'objet parent.

```xml
<size>1 1</size>
```

#### Propriété `<origin>`

La propriété `<origin>` permet de définir un nouveau point de départ d'origine pour toutes les tailles de l'objet parent.

Les valeurs sont des pourcentages compris en 0 (0%) et 1 (100%).

```xml
<origin>0.5 0.5</origin>
```

#### Propriété `<rotation>`

La propriété `<rotation>` permet de spécifier un angle de rotation de l'objet parent. Les valeurs positives feront une rotation horaire et les valeurs négatives feront une rotation antihoraire.

```xml
<rotation>90</rotation>
```

#### Propriété `<rotationOrigin>`

La propriété `<rotationOrigin>` permet de définir un nouveau point de départ d'origine pour toutes les tailles de l'objet parent uniquement si une rotation doit être effectuée. 

Les valeurs sont des pourcentages compris en 0 (0%) et 1 (100%).

```xml
<rotationOrigin>0.5 0.5</rotationOrigin>
```

#### Propriété `<text>`

La propriété `<text>` permet de mettre du texte.

```xml
<text>Text aléatoire ici</text>
```

Vous pouvez aussi ajouter du texte dans plusieurs langues en précisant la langue dans la propriété.

```xml
<text.fr>Text aléatoire ici</text.fr>
```

Vous pouvez utiliser uniquement le code de la langue comme visible dans l'exemple ci-dessus, mais vous pouvez aussi préciser la région comme dans l'exemple ci-dessous.

```xml
<text.en_us>Random text here</text.en_us>
```

Vous pouvez avoir plusieurs langues définies dans le thème mais à la première occurence valide, la recherche de texte à afficher s'y arrêtera.

Allons un peu plus dans les détails avec les 2 exemples suivants :

```xml
<text text="Washington" text.fr="Paris" text.de_CH="Berne" text.de="Berlin" />
```

- Si l'utilisateur a configuré sa Recalbox en `de_CH`, alors il obtiendra bien "Berne", car c'est la première qui correspond avec sa langue ou sa langue + région
- Si l'utilisateur est allemand et qu'il a configuré sa Recalbox en `de_DE`, il obtiendra bien "Berlin", car seul le code langue correspondra sur les derniers text.suffix
- Si l'utilisateur est français avec `fr_FR`. Les Quebecois `fr_CA` et les Belges `fr_BE` auront également Paris !
- Tous ceux pour qui rien ne matche auront "Washington"

```xml
<text text.de="Berlin" text.de_CH="Berne" text.fr="Paris" text="Washington" />
```

- Ici rien ne change par rapport à l'exemple précédent, ***SAUF*** pour les utilisateurs configurés en `de_CH` qui auront "Berlin", parce que le code langue de `text.de` aura une correspondance avant que `text.de_CH` arrive.

Les codes langues sont toujours sur 2 caractères et sont disponibles [ici](https://fr.wikipedia.org/wiki/Liste_des_codes_ISO_639-1).
Les codes régions sont aussi sur 2 caractères et sont disponibles [ici](https://fr.wikipedia.org/wiki/ISO_3166-2).

#### Propriété `<path>`

La propriété `<path>` permet de charger un fichier texte au lieu de mettre du texte manuellement.

```xml
<path>./chemin/vers/fichier/texte.txt</path>
```

#### Propriété `<backgroundColor>`

La propriété `<backgroundColor>` permet de définir une couleur d'arrière-plan à appliquer à l'objet parent. Les couleurs peuvent être au format RRVVBB ou RRVVBBAA (AA pour la transparence, 00 = transparent et ff = opaque).

```xml
<backgroundColor>ffffff00</backgroundColor>
```

#### Propriété `<fontPath>`

La propriété `<fontPath>` permet d'indiquer un chemin pour charger la police souhaitée de l'objet parent.

```xml
<fontPath>./../arts/Exo2-BoldCondensed.otf</fontPath>
```

#### Propriété `<fontSize>`

La propriété `<fontSize>` permet d'indiquer la taille de police à appliquer à l'objet parent. La valeur est exprimée en pourcentage de la hauteur de l'écran (une valeur de `0.1` indique la taille de 10% de la hauteur de l'écran).

```xml
<fontSize>0.025</fontSize>
```

#### Propriété `<color>`

La propriété `<color>` permet d'indiquer la couleur du texte de l'objet parent. Les couleurs peuvent être au format RRVVBB ou RRVVBBAA (AA pour la transparence, 00 = transparent et ff = opaque).

```xml
<color>666666</color>
```

#### Propriété `<alignment>`

La propriété `<alignment>` permet de choisir l'alignement de l'objet parent. `left` = alignement à gauche, `center` = centré et `right` = alignement à droite. L'alignement centré centre aussi l'objet parent verticalement.

```xml
<alignment>left</alignment>
```

#### Propriété `<forceUppercase>`

La propriété `<forceUppercase>` permet de forcer le texte de l'objet parent à être tout en majuscule. 0 = aucun changement, 1 = forçage en majuscule.

```xml
<forceUppercase>1</forceUppercase>
```

#### Propriété `<lineSpacing>`

La propriété `<lineSpacing>` permet de contrôler l'espace entre les lignes de l'objet parent suivant le multiple de la hauteur de la police.

```xml
<lineSpacing>1.5</lineSpacing>
```

#### Propriété `<zIndex>`

La propriété `<zIndex>` permet d'ordonner les éléments par profondeur / couche. Plus sa valeur est grande, plus il sera sur le premier plan. Elle peut être négative.

```xml
<zIndex>10</zIndex>
```

#### Propriété `<disabled>`

La propriété `<disabled>` permet de désactiver l'objet parent.

```xml
<disabled>true</disabled>
```

## Objet `<scrolltext>` ![](/tutorials/newicon.png)

Les objets `<scrolltext>` permettent de faire défiler horizontalement le texte trop long sur une zone plus réduite. Cet objet peut avoir plusieurs attributs :

#### Attribut `name`

- Obligatoire : oui

L'attribut `name` permet d'indiquer sur quel élément textuel le contenu doit agir. Il peut avoir une ou plusieurs de ces valeurs en même temps.

Liste des valeurs utilisables dans la liste des systèmes (`<view name="system">`) :

- `systemInfo` : le texte indiquant le nombre de jeux présent dans le système, avec le nombre de jeux favoris et cachés.

Liste des valeurs utilisables dans la liste des jeux (`<view name="detailed">`) :

- `md_lbl_rating` : le texte indiquant la note.
- `md_lbl_releasedate` : le texte indiquant la date de sortie.
- `md_lbl_developer` : le texte indiquant le développeur.
- `md_lbl_publisher` : le texte indiquant l'éditeur.
- `md_lbl_genre` : le texte indiquant le genre.
- `md_lbl_lastplayed` : le texte indiquant la dernière fois que vous avez joué au jeu.
- `md_lbl_players` : le texte indiquant le nombre de joueurs.
- `md_lbl_playcount` : le texte indiquant le nombre de fois que le jeu a été joué.
- `md_lbl_favorite` : le texte indiquant si le jeu est dans la liste des favoris.
- `md_developer` : le développeur.
- `md_publisher` : l'éditeur.
- `md_genre` : le genre.
- `md_players` : le nombre de joueurs.
- `md_playcount` : le nombre de fois que le jeu a été joué.
- `md_favorite` : indique si le jeu est dans la liste des favoris.
- `md_description` : le texte descriptif des jeux.

Liste des valeurs utilisables dans les clips des jeux (`<view name="gameclip">`) :

- `md_lbl_gameName` : le texte indiquant le nom du jeu.
- `md_lbl_systemName` : le texte indiquant le nom du système.
- `md_lbl_rating` : le texte indiquant la note.
- `md_lbl_releasedate` : le texte indiquant la date de sortie.
- `md_lbl_developer` : le texte indiquant le développeur.
- `md_lbl_publisher` : le texte indiquant l'éditeur.
- `md_lbl_genre` : le texte indiquant le genre.
- `md_lbl_lastplayed` : le texte indiquant la dernière fois que vous avez joué au jeu.
- `md_lbl_players` : le texte indiquant le nombre de joueurs.
- `md_lbl_playcount` : le texte indiquant le nombre de fois que le jeu a été joué.
- `md_lbl_favorite` : le texte indiquant si le jeu est dans la liste des favoris.
- `md_gameName` : le nom du jeu.
- `md_developer` : le développeur.
- `md_publisher` : l'éditeur.
- `md_genre` : le genre.
- `md_players` : le nombre de joueurs.
- `md_playcount` : le nombre de fois que le jeu a été joué.
- `md_favorite` : indique si le jeu est dans la liste des favoris.
- `md_description` : le texte descriptif des jeux.
- `md_systemName` : le nom du système.

```xml
<scrolltext name="md_developer, md_publisher, md_genre, md_playcount, md_lastplayed, md_description, md_players, md_releasedate, md_favorite">
  ...
</scrolltext>
```

L'objet `<scrolltext>` peut contenir plusieurs autres propriétés.

#### Propriété `<pos>`

La propriété `<pos>` permet d'indiquer la position de l'objet parent. La position à indiquer correspond au point le plus en haut à gauche de l'objet. Celui-ci contient 2 valeurs. La première valeur correspond au positionnement depuis la gauche et la seconde valeur au positionnement depuis le haut.

Les valeurs sont des pourcentages compris en 0 (0%) et 1 (100%).

```xml
<pos>0.33 0.25</pos>
```

Dans l'exemple ci-dessus, on peut voir que la position indique de positionner l'objet parent à 33% de la gauche et à 25% du haut.

#### Propriété `<size>`

La propriété `<size>` permet de définir la taille de l'élément qui contient l'objet parent.

```xml
<size>1 1</size>
```

#### Propriété `<origin>`

La propriété `<origin>` permet de définir un nouveau point de départ d'origine pour toutes les tailles de l'objet parent.

Les valeurs sont des pourcentages compris en 0 (0%) et 1 (100%).

```xml
<origin>0.5 0.5</origin>
```

#### Propriété `<rotation>`

La propriété `<rotation>` permet de spécifier un angle de rotation de l'objet parent. Les valeurs positives feront une rotation horaire et les valeurs négatives feront une rotation antihoraire.

```xml
<rotation>90</rotation>
```

#### Propriété `<rotationOrigin>`

La propriété `<rotationOrigin>` permet de définir un nouveau point de départ d'origine pour toutes les tailles de l'objet parent uniquement si une rotation doit être effectuée. 

Les valeurs sont des pourcentages compris en 0 (0%) et 1 (100%).

```xml
<rotationOrigin>0.5 0.5</rotationOrigin>
```

#### Propriété `<text>`

La propriété `<text>` permet de mettre du texte.

```xml
<text>Text aléatoire ici</text>
```

Vous pouvez aussi ajouter du texte dans plusieurs langues en précisant la langue dans la propriété.

```xml
<text.fr>Text aléatoire ici</text.fr>
```

Vous pouvez utiliser uniquement le code de la langue comme visible dans l'exemple ci-dessus, mais vous pouvez aussi préciser la région comme dans l'exemple ci-dessous.

```xml
<text.en_us>Random text here</text.en_us>
```

Vous pouvez avoir plusieurs langues définies dans le thème mais à la première occurence valide, la recherche de texte à afficher s'y arrêtera.

Allons un peu plus dans les détails avec les 2 exemples suivants :

```xml
<text text="Washington" text.fr="Paris" text.de_CH="Berne" text.de="Berlin" />
```

- Si l'utilisateur a configuré sa Recalbox en `de_CH`, alors il obtiendra bien "Berne", car c'est la première qui correspond avec sa langue ou sa langue + région
- Si l'utilisateur est allemand et qu'il a configuré sa Recalbox en `de_DE`, il obtiendra bien "Berlin", car seul le code langue correspondra sur les derniers text.suffix
- Si l'utilisateur est français avec `fr_FR`. Les Quebecois `fr_CA` et les Belges `fr_BE` auront également Paris !
- Tous ceux pour qui rien ne matche auront "Washington"

```xml
<text text.de="Berlin" text.de_CH="Berne" text.fr="Paris" text="Washington" />
```

- Ici rien ne change par rapport à l'exemple précédent, ***SAUF*** pour les utilisateurs configurés en `de_CH` qui auront "Berlin", parce que le code langue de `text.de` aura une correspondance avant que `text.de_CH` arrive.

Les codes langues sont toujours sur 2 caractères et sont disponibles [ici](https://fr.wikipedia.org/wiki/Liste_des_codes_ISO_639-1).
Les codes régions sont aussi sur 2 caractères et sont disponibles [ici](https://fr.wikipedia.org/wiki/ISO_3166-2).

#### Propriété `<path>`

La propriété `<path>` permet de charger un fichier texte au lieu de mettre du texte manuellement.

```xml
<path>./chemin/vers/fichier/texte.txt</path>
```

#### Propriété `<backgroundColor>`

La propriété `<backgroundColor>` permet de définir une couleur d'arrière-plan à appliquer à l'objet parent. Les couleurs peuvent être au format RRVVBB ou RRVVBBAA (AA pour la transparence, 00 = transparent et ff = opaque).

```xml
<backgroundColor>ffffff00</backgroundColor>
```

#### Propriété `<fontPath>`

La propriété `<fontPath>` permet d'indiquer un chemin pour charger la police souhaitée de l'objet parent.

```xml
<fontPath>./../arts/Exo2-BoldCondensed.otf</fontPath>
```

#### Propriété `<fontSize>`

La propriété `<fontSize>` permet d'indiquer la taille de police à appliquer à l'objet parent. La valeur est exprimée en pourcentage de la hauteur de l'écran (une valeur de `0.1` indique la taille de 10% de la hauteur de l'écran).

```xml
<fontSize>0.025</fontSize>
```

#### Propriété `<color>`

La propriété `<color>` permet d'indiquer la couleur du texte de l'objet parent. Les couleurs peuvent être au format RRVVBB ou RRVVBBAA (AA pour la transparence, 00 = transparent et ff = opaque).

```xml
<color>666666</color>
```

#### Propriété `<alignment>`

La propriété `<alignment>` permet de choisir l'alignement de l'objet parent. `left` = alignement à gauche, `center` = centré et `right` = alignement à droite. L'alignement centré centre aussi l'objet parent verticalement.

```xml
<alignment>left</alignment>
```

#### Propriété `<forceUppercase>`

La propriété `<forceUppercase>` permet de forcer le texte de l'objet parent à être tout en majuscule. 0 = aucun changement, 1 = forçage en majuscule.

```xml
<forceUppercase>1</forceUppercase>
```

#### Propriété `<zIndex>`

La propriété `<zIndex>` permet d'ordonner les éléments par profondeur / couche. Plus sa valeur est grande, plus il sera sur le premier plan. Elle peut être négative.

```xml
<zIndex>10</zIndex>
```

#### Propriété `<disabled>`

La propriété `<disabled>` permet de désactiver l'objet parent.

```xml
<disabled>true</disabled>
```

## Objet `<textlist>`

Les objets `<textlist>` permettent d'afficher une liste de texte. Cet objet peut avoir plusieurs attributs :

#### Attribut `name`

- Obligatoire : oui

L'attribut `name` permet d'indiquer sur quel élément textuel le contenu doit agir. Il peut avoir une ou plusieurs de ces valeurs en même temps.

Liste des valeurs utilisables dans la liste des jeux (`<view name="detailed">`) :

- `gamelist` : la liste des jeux dans un système.

```xml
<textlist name="gamelist">
  ...
</textlist>
```

L'objet `<textlist>` peut contenir plusieurs autres propriétés.

#### Propriété `<pos>`

La propriété `<pos>` permet d'indiquer la position de l'objet parent. La position à indiquer correspond au point le plus en haut à gauche de l'objet. Celui-ci contient 2 valeurs. La première valeur correspond au positionnement depuis la gauche et la seconde valeur au positionnement depuis le haut.

Les valeurs sont des pourcentages compris en 0 (0%) et 1 (100%).

```xml
<pos>0.33 0.25</pos>
```

Dans l'exemple ci-dessus, on peut voir que la position indique de positionner l'objet parent à 33% de la gauche et à 25% du haut.

#### Propriété `<size>`

La propriété `<size>` permet de définir la taille de l'élément qui contient l'objet parent.

```xml
<size>1 1</size>
```

#### Propriété `<origin>`

La propriété `<origin>` permet de définir un nouveau point de départ d'origine pour toutes les tailles de l'objet parent.

Les valeurs sont des pourcentages compris en 0 (0%) et 1 (100%).

```xml
<origin>0.5 0.5</origin>
```

#### Propriété `<selectorHeight>`

La propriété `<selectorHeight>` permet d'appliquer une hauteur de surlignage des éléments de l'objet parent.

```xml
<selectorHeight>0.5</selectorHeight>
```

#### Propriété `<selectorOffsetY>`

La propriété `<selectorOffsetY>` permet d'appliquer une valeur de décalage verticale du surlignage.

```xml
<selectorOffsetY>0.5</selectorOffsetY>
```

#### Propriété `<selectorColor>`

La propriété `<selectorColor>` permet d'utiliser une couleur spécifique pour le surlignage des éléments de l'objet parent. Les couleurs peuvent être au format RRVVBB ou RRVVBBAA (AA pour la transparence, 00 = transparent et ff = opaque).

```xml
<selectorColor>4368ffff</selectorColor>
```

#### Propriété `<selectorImagePath>`

La propriété `<selectorImagePath>` permet d'indiquer le chemin vers une image à utiliser en arrière-plan du surlignage des éléments de l'objet parent.

```xml
<selectorImagePath>./chemin/vers/fichier/image.png</selectorImagePath>
```

#### Propriété `<selectorImageTile>`

La propriété `<selectorImageTile>` permet de définir si l'image est affichée tel une mosaïque au lieu d'être étirée pour s'adapter à la taille de l'objet. C'est utile pour les arrière-plans.

```xml
<selectorImageTile>false</selectorImageTile>
```

#### Propriété `<selectedColor>`

La propriété `<selectedColor>` permet de définir la couleur du texte surligné. Les couleurs peuvent être au format RRVVBB ou RRVVBBAA (AA pour la transparence, 00 = transparent et ff = opaque).

```xml
<selectedColor>4368ffff</selectedColor>
```

#### Propriété `<primaryColor>`

La propriété `<primaryColor>` permet de définir la couleur du texte des noms des jeux. Les couleurs peuvent être au format RRVVBB ou RRVVBBAA (AA pour la transparence, 00 = transparent et ff = opaque).

```xml
<primaryColor>4368ffff</primaryColor>
```

#### Propriété `<secondaryColor>`

La propriété `<secondaryColor>` permet de définir la couleur du texte des noms des dossiers. Les couleurs peuvent être au format RRVVBB ou RRVVBBAA (AA pour la transparence, 00 = transparent et ff = opaque).

```xml
<secondaryColor>4368ffff</secondaryColor>
```

#### Propriété `<fontPath>`

La propriété `<fontPath>` permet d'indiquer un chemin pour charger la police souhaitée de l'objet parent.

```xml
<fontPath>./../arts/Exo2-BoldCondensed.otf</fontPath>
```

#### Propriété `<fontSize>`

La propriété `<fontSize>` permet d'indiquer la taille de police à appliquer à l'objet parent. La valeur est exprimée en pourcentage de la hauteur de l'écran (une valeur de `0.1` indique la taille de 10% de la hauteur de l'écran).

```xml
<fontSize>0.025</fontSize>
```

#### Propriété `<alignment>`

La propriété `<alignment>` permet de choisir l'alignement de l'objet parent. `left` = alignement à gauche, `center` = centré et `right` = alignement à droite. L'alignement centré centre aussi l'objet parent verticalement.

```xml
<alignment>left</alignment>
```

#### Propriété `<horizontalMargin>`

La propriété `<horizontalMargin>` permet d'appliquer sur les côtés une marge des textes de la liste. Les sélecteurs et le conteneur de la liste continuent à utiliser 100% de la largeur spécifiée.

```xml
<horizontalMargin>0.1</horizontalMargin>
```

#### Propriété `<forceUppercase>`

La propriété `<forceUppercase>` permet de forcer le texte de l'objet parent à être tout en majuscule. 0 = aucun changement, 1 = forçage en majuscule.

```xml
<forceUppercase>1</forceUppercase>
```

#### Propriété `<lineSpacing>`

La propriété `<lineSpacing>` permet de contrôler l'espace entre les lignes de l'objet parent suivant le multiple de la hauteur de la police.

```xml
<lineSpacing>1.5</lineSpacing>
```

#### Propriété `<zIndex>`

La propriété `<zIndex>` permet d'ordonner les éléments par profondeur / couche. Plus sa valeur est grande, plus il sera sur le premier plan. Elle peut être négative.

```xml
<zIndex>10</zIndex>
```

## Objet `<datetime>`

Les objets `<datetime>` permettent d'afficher une date et heure. Cet objet peut avoir plusieurs attributs :

#### Attribut `name`

- Obligatoire : oui

L'attribut `name` permet d'indiquer sur quel élément textuel le contenu doit agir. Il peut avoir une ou plusieurs de ces valeurs en même temps.

Liste des valeurs utilisables dans la liste des jeux (`<view name="detailed">`) :

- `md_lastplayed` : la date de dernier lancement du jeu.
- `md_releasedate` : la date de sortie du jeu.

Liste des valeurs utilisables dans les clips de jeux (`<view name="gameclipview">`) :

- `md_releasedate` : la date de sortie du jeu.

```xml
<datetime name="md_releasedate">
  ...
</datetime>
```

L'objet `<datetime>` peut contenir plusieurs autres propriétés.

#### Propriété `<pos>`

La propriété `<pos>` permet d'indiquer la position de l'objet parent. La position à indiquer correspond au point le plus en haut à gauche de l'objet. Celui-ci contient 2 valeurs. La première valeur correspond au positionnement depuis la gauche et la seconde valeur au positionnement depuis le haut.

Les valeurs sont des pourcentages compris en 0 (0%) et 1 (100%).

```xml
<pos>0.33 0.25</pos>
```

Dans l'exemple ci-dessus, on peut voir que la position indique de positionner l'objet parent à 33% de la gauche et à 25% du haut.

#### Propriété `<size>`

La propriété `<size>` permet de définir la taille de l'élément qui contient l'objet parent.

```xml
<size>1 1</size>
```

#### Propriété `<origin>`

La propriété `<origin>` permet de définir un nouveau point de départ d'origine pour toutes les tailles de l'objet parent.

Les valeurs sont des pourcentages compris en 0 (0%) et 1 (100%).

```xml
<origin>0.5 0.5</origin>
```

#### Propriété `<color>`

La propriété `<color>` permet d'indiquer la couleur du texte de l'objet parent. Les couleurs peuvent être au format RRVVBB ou RRVVBBAA (AA pour la transparence, 00 = transparent et ff = opaque).

```xml
<color>666666</color>
```

#### Propriété `<fontPath>`

La propriété `<fontPath>` permet d'indiquer un chemin pour charger la police souhaitée de l'objet parent.

```xml
<fontPath>./../arts/Exo2-BoldCondensed.otf</fontPath>
```

#### Propriété `<fontSize>`

La propriété `<fontSize>` permet d'indiquer la taille de police à appliquer à l'objet parent. La valeur est exprimée en pourcentage de la hauteur de l'écran (une valeur de `0.1` indique la taille de 10% de la hauteur de l'écran).

```xml
<fontSize>0.025</fontSize>
```

#### Propriété `<alignment>`

La propriété `<alignment>` permet de choisir l'alignement de l'objet parent. `left` = alignement à gauche, `center` = centré et `right` = alignement à droite. L'alignement centré centre aussi l'objet parent verticalement.

```xml
<alignment>left</alignment>
```

#### Propriété `<forceUppercase>`

La propriété `<forceUppercase>` permet de forcer le texte de l'objet parent à être tout en majuscule. 0 = aucun changement, 1 = forçage en majuscule.

```xml
<forceUppercase>1</forceUppercase>
```

#### Propriété `<zIndex>`

La propriété `<zIndex>` permet d'ordonner les éléments par profondeur / couche. Plus sa valeur est grande, plus il sera sur le premier plan. Elle peut être négative.

```xml
<zIndex>10</zIndex>
```

#### Propriété `<disabled>`

La propriété `<disabled>` permet de désactiver l'objet parent.

```xml
<disabled>true</disabled>
```

#### Propriété `<display>`

La propriété `<display>` permet d'indiquer le format de la date à afficher.

Valeurs possibles :

- `date` : affiche la date.
- `dateTime` : affiche la date et l'heure.
- `year` : affiche l'année.
- `realTime` : affiche l'heure réelle.
- `time` : affiche l'heure.
- `RelativeToNow` : affiche la date relative à maintenant (`il y a x jours`, `il y a x heures`, etc.).

```xml
<display>date</display>
```

## Objet `<rating>`

Les objets `<rating>` permettent d'afficher la note d'un jeu. Cet objet peut avoir plusieurs attributs :

#### Attribut `name`

- Obligatoire : oui

L'attribut `name` permet d'indiquer sur quel élément textuel le contenu doit agir. Il peut avoir une ou plusieurs de ces valeurs en même temps.

Liste des valeurs utilisables dans la liste des jeux (`<view name="detailed">`) :

- `md_rating` : la note du jeu.

```xml
<rating name="md_rating">
  ...
</rating>
```

L'objet `<rating>` peut contenir plusieurs autres propriétés.

#### Propriété `<pos>`

La propriété `<pos>` permet d'indiquer la position de l'objet parent. La position à indiquer correspond au point le plus en haut à gauche de l'objet. Celui-ci contient 2 valeurs. La première valeur correspond au positionnement depuis la gauche et la seconde valeur au positionnement depuis le haut.

Les valeurs sont des pourcentages compris en 0 (0%) et 1 (100%).

```xml
<pos>0.33 0.25</pos>
```

Dans l'exemple ci-dessus, on peut voir que la position indique de positionner l'objet parent à 33% de la gauche et à 25% du haut.

#### Propriété `<size>`

La propriété `<size>` permet de définir la taille de l'élément qui contient l'objet parent.

```xml
<size>1 1</size>
```

#### Propriété `<origin>`

La propriété `<origin>` permet de définir un nouveau point de départ d'origine pour toutes les tailles de l'objet parent.

Les valeurs sont des pourcentages compris en 0 (0%) et 1 (100%).

```xml
<origin>0.5 0.5</origin>
```

#### Propriété `<rotation>`

La propriété `<rotation>` permet de spécifier un angle de rotation de l'objet parent. Les valeurs positives feront une rotation horaire et les valeurs négatives feront une rotation antihoraire.

```xml
<rotation>90</rotation>
```

#### Propriété `<rotationOrigin>`

La propriété `<rotationOrigin>` permet de définir un nouveau point de départ d'origine pour toutes les tailles de l'objet parent uniquement si une rotation doit être effectuée. 

Les valeurs sont des pourcentages compris en 0 (0%) et 1 (100%).

```xml
<rotationOrigin>0.5 0.5</rotationOrigin>
```

#### Propriété `<filledPath>`

La propriété `<filledPath>` permet de charger un fichier image correspondant au taux de remplissage de la note du jeu.

```xml
<filledPath>./chemin/vers/fichier/image.png</filledPath>
```

#### Propriété `<unfilledPath>`

La propriété `<unfilledPath>` permet de charger un fichier image correspondant au total de la note du jeu.

```xml
<unfilledPath>./chemin/vers/fichier/image.png</unfilledPath>
```

#### Propriété `<zIndex>`

La propriété `<zIndex>` permet d'ordonner les éléments par profondeur / couche. Plus sa valeur est grande, plus il sera sur le premier plan. Elle peut être négative.

```xml
<zIndex>10</zIndex>
```

#### Propriété `<disabled>`

La propriété `<disabled>` permet de désactiver l'objet parent.

```xml
<disabled>true</disabled>
```

## Objet `<sound>`

Les objets `<sound>` permettent de lire des musiques dans l'interface. Cet objet peut avoir plusieurs attributs :

#### Attribut `name`

- Obligatoire : oui

L'attribut `name` permet d'indiquer sur quel élément textuel le contenu doit agir. Il peut avoir une ou plusieurs de ces valeurs en même temps.

Liste des valeurs utilisables dans la liste des systèmes (`<view name="system">`) :

- `directory` : le dossier de musique.

```xml
<sound name="directory">
  ...
</sound>
```

L'objet `<sound>` peut contenir une seule propriété.

#### Propriété `<path>`

La propriété `<path>` permet d'indiquer un dossier où chercher les musiques à jouer.

```xml
<path>./chemin/vers/dossier</path>
```

## Objet `<helpsystem>`

Les objets `<helpsystem>` permettent d'afficher les boutons en bas de l'interface. Cet objet peut avoir plusieurs attributs :

#### Attribut `name`

- Obligatoire : oui

L'attribut `name` permet d'indiquer sur quel élément textuel le contenu doit agir. Il peut avoir une ou plusieurs de ces valeurs en même temps.

Liste des valeurs utilisables dans la liste des systèmes, la liste des jeux, les clips de jeux et les menus (`<view name="system, detailed, gameclip, menu">`) :

- `help` : les boutons en bas de l'interface.

```xml
<helpsystem name="help">
  ...
</helpsystem>
```

L'objet `<helpsystem>` peut contenir plusieurs autres propriétés.

#### Propriété `<pos>`

La propriété `<pos>` permet d'indiquer la position de l'objet parent. La position à indiquer correspond au point le plus en haut à gauche de l'objet. Celui-ci contient 2 valeurs. La première valeur correspond au positionnement depuis la gauche et la seconde valeur au positionnement depuis le haut.

Les valeurs sont des pourcentages compris en 0 (0%) et 1 (100%).

```xml
<pos>0.33 0.25</pos>
```

Dans l'exemple ci-dessus, on peut voir que la position indique de positionner l'objet parent à 33% de la gauche et à 25% du haut.

#### Propriété `<textColor>`

La propriété `<textColor>` permet d'indiquer la couleur du texte de l'objet parent. Les couleurs peuvent être au format RRVVBB ou RRVVBBAA (AA pour la transparence, 00 = transparent et ff = opaque).

```xml
<textColor>666666</textColor>
```

#### Propriété `<iconColor>`

La propriété `<iconColor>` permet d'indiquer la couleur de l'icône de l'objet parent. Les couleurs peuvent être au format RRVVBB ou RRVVBBAA (AA pour la transparence, 00 = transparent et ff = opaque).

```xml
<iconColor>666666</iconColor>
```

#### Propriété `<fontPath>`

La propriété `<fontPath>` permet d'indiquer un chemin pour charger la police souhaitée de l'objet parent.

```xml
<fontPath>./../arts/Exo2-BoldCondensed.otf</fontPath>
```

#### Propriété `<fontSize>`

La propriété `<fontSize>` permet d'indiquer la taille de police à appliquer à l'objet parent. La valeur est exprimée en pourcentage de la hauteur de l'écran (une valeur de `0.1` indique la taille de 10% de la hauteur de l'écran).

```xml
<fontSize>0.025</fontSize>
```

#### Propriété `<iconUpDown>`

La propriété `<iconUpDown>` permet d'indiquer le fichier représentant les boutons `Haut` et `Bas`.

```xml
<iconUpDown>./chemin/vers/fichier/image.png</iconUpDown>
```

#### Propriété `<iconLeftRight>`

La propriété `<iconLeftRight>` permet d'indiquer le fichier représentant les boutons `Gauche` et `Droite`.

```xml
<iconLeftRight>./chemin/vers/fichier/image.png</iconLeftRight>
```

#### Propriété `<iconUpDownLeftRight>`

La propriété `<iconUpDownLeftright>` permet d'indiquer le fichier représentant les boutons `Haut`, `Bas`, `Gauche` et `Droite`.

```xml
<iconUpDownLeftRight>./chemin/vers/fichier/image.png</iconUpDownLeftRight>
```

#### Propriété `<iconA>`

La propriété `<iconA>` permet d'indiquer le fichier représentant le bouton `A` d'une manette de SNES.

```xml
<iconA>./chemin/vers/fichier/image.png</iconA>
```

#### Propriété `<iconB>`

La propriété `<iconB>` permet d'indiquer le fichier représentant le bouton `B` d'une manette de SNES.

```xml
<iconB>./chemin/vers/fichier/image.png</iconB>
```

#### Propriété `<iconX>`

La propriété `<iconX>` permet d'indiquer le fichier représentant le bouton `X` d'une manette de SNES.

```xml
<iconX>./chemin/vers/fichier/image.png</iconX>
```

#### Propriété `<iconY>`

La propriété `<iconY>` permet d'indiquer le fichier représentant le bouton `Y` d'une manette de SNES.

```xml
<iconY>./chemin/vers/fichier/image.png</iconY>
```

#### Propriété `<iconL>`

La propriété `<iconL>` permet d'indiquer le fichier représentant le bouton `L` d'une manette de SNES.

```xml
<iconL>./chemin/vers/fichier/image.png</iconL>
```

#### Propriété `<iconR>`

La propriété `<iconR>` permet d'indiquer le fichier représentant le bouton `R` d'une manette de SNES.

```xml
<iconR>./chemin/vers/fichier/image.png</iconR>
```

#### Propriété `<iconStart>`

La propriété `<iconStart>` permet d'indiquer le fichier représentant le bouton `Start` d'une manette de SNES.

```xml
<iconStart>./chemin/vers/fichier/image.png</iconStart>
```

#### Propriété `<iconSelect>`

La propriété `<iconSelect>` permet d'indiquer le fichier représentant le bouton `Select` d'une manette de SNES.

```xml
<iconSelect>./chemin/vers/fichier/image.png</iconSelect>
```

## Objet `<carousel>`

Les objets `<carousel>` permettent d'afficher la liste des systèmes. Cet objet peut avoir plusieurs attributs :

#### Attribut `name`

- Obligatoire : oui

L'attribut `name` permet d'indiquer sur quel élément textuel le contenu doit agir. Il peut avoir une ou plusieurs de ces valeurs en même temps.

Liste des valeurs utilisables dans la liste des systèmes (`<view name="system">`) :

- `systemcarousel` : la liste des systèmes.

```xml
<carousel name="systemcarousel">
  ...
</carousel>
```

L'objet `<carousel>` peut contenir plusieurs autres propriétés.

#### Propriété `<type>`

La propriété `<type>` permet de définir le sens d'affichage de la liste des systèmes. Par défaut, le type est `horizontal`.

Valeurs possibles :

- `horizontal` : liste des systèmes orienté horizontalement.
- `vertical` : liste des systèmes orienté verticalement.
- `vertical_wheel` : liste des systèmes dans une roue orienté verticalement.

```xml
<type>vertical</type>
```

#### Propriété `<size>`

La propriété `<size>` permet de définir la taille de l'élément qui contient l'objet parent.

```xml
<size>1 1</size>
```

#### Propriété `<pos>`

La propriété `<pos>` permet d'indiquer la position de l'objet parent. La position à indiquer correspond au point le plus en haut à gauche de l'objet. Celui-ci contient 2 valeurs. La première valeur correspond au positionnement depuis la gauche et la seconde valeur au positionnement depuis le haut.

Les valeurs sont des pourcentages compris en 0 (0%) et 1 (100%).

```xml
<pos>0.33 0.25</pos>
```

Dans l'exemple ci-dessus, on peut voir que la position indique de positionner l'objet parent à 33% de la gauche et à 25% du haut.

#### Propriété `<origin>`

La propriété `<origin>` permet de définir un nouveau point de départ d'origine pour toutes les tailles de l'objet parent.

Les valeurs sont des pourcentages compris en 0 (0%) et 1 (100%).

```xml
<origin>0.5 0.5</origin>
```

#### Propriété `<color>`

La propriété `<color>` permet d'indiquer la couleur du texte de l'objet parent. Les couleurs peuvent être au format RRVVBB ou RRVVBBAA (AA pour la transparence, 00 = transparent et ff = opaque).

```xml
<color>666666</color>
```

#### Propriété `<fontPath>`

La propriété `<fontPath>` permet d'indiquer un chemin pour charger la police souhaitée de l'objet parent.

```xml
<fontPath>./../arts/Exo2-BoldCondensed.otf</fontPath>
```

#### Propriété `<logoScale>`

La propriété `<logoScale>` permet d'appliquer un agrandissement de l'échelle de l'objet parent survolé.

```xml
<logoScale>3</logoScale>
```

#### Propriété `<logoRotation>`

La propriété `<logoRotation>` permet de spécifier un angle de rotation de l'objet parent. Les valeurs positives feront une rotation horaire et les valeurs négatives feront une rotation antihoraire.

```xml
<logoRotation>90</logoRotation>
```

#### Propriété `<logoRotationOrigin>`

La propriété `<logoRotationOrigin>` permet de définir un nouveau point de départ d'origine pour toutes les tailles de l'objet parent uniquement si une rotation doit être effectuée. 

Les valeurs sont des pourcentages compris en 0 (0%) et 1 (100%).

```xml
<logoRotationOrigin>0.5 0.5</logoRotationOrigin>
```

#### Propriété `<logoSize>`

La propriété `<logoSize>` permet de définir la taille de l'élément qui contient l'objet parent.

```xml
<logoSize>1</logoSize>
```

#### Propriété `<logoAlignment>`

La propriété `<logoAlignment>` permet de choisir l'alignement de l'objet parent. `left` = alignement à gauche, `center` = centré et `right` = alignement à droite. L'alignement centré centre aussi l'objet parent verticalement.

```xml
<logoAlignment>left</logoAlignment>
```

#### Propriété `<maxLogoCount>`

La propriété `<maxLogoCount>` permet d'indiquer le nombre de logos visible au total.

```xml
<maxLogoCount>7</maxLogoCount>
```

#### Propriété `<defaultTransition>`

La propriété `<defaultTransition>` permet d'indiquer la transition par défaut.

Valeurs possibles :

- `fade` : affichera un fondu noir entre le changement de système.
- `instant` : déplacement instantanée au changement de système.
- `slide` : glisse entre le changement de système comme un changement de diapositive.

```xml
<defaultTransition>slide</defaultTransition>
```

#### Propriété `<zIndex>`

La propriété `<zIndex>` permet d'ordonner les éléments par profondeur / couche. Plus sa valeur est grande, plus il sera sur le premier plan. Elle peut être négative.

```xml
<zIndex>10</zIndex>
```

## Objet `<menuBackground>`

Les objets `<menuBackground>` permettent de personnaliser l'arrière-plan des menus. Cet objet peut avoir plusieurs attributs :

#### Attribut `name`

- Obligatoire : oui

L'attribut `name` permet d'indiquer sur quel élément textuel le contenu doit agir. Il peut avoir une ou plusieurs de ces valeurs en même temps.

Liste des valeurs utilisables dans les menus (`<view name="menu">`) :

- `menubg` : arrière-plan des menus.

```xml
<menuBackground name="menubg">
  ...
</menuBackground>
```

L'objet `<menuBackground>` peut contenir plusieurs autres propriétés.

#### Propriété `<color>`

La propriété `<color>` permet d'indiquer la couleur du texte de l'objet parent. Les couleurs peuvent être au format RRVVBB ou RRVVBBAA (AA pour la transparence, 00 = transparent et ff = opaque).

```xml
<color>666666</color>
```

#### Propriété `<path>`

La propriété `<path>` permet de charger un fichier image.

```xml
<path>./chemin/vers/fichier/image.png</path>
```

#### Propriété `<fadePath>`

La propriété `<fadePath>` permet de charger un fichier image pour semi-assombrir le fond des menus.

```xml
<fadePath>./chemin/vers/fichier/image.png</fadePath>
```

## Objet `<menuIcons>`

Les objets `<menuIcons>` permettent de personnaliser l'arrière-plan des menus. Cet objet peut avoir plusieurs attributs :

#### Attribut `name`

- Obligatoire : oui

L'attribut `name` permet d'indiquer sur quel élément textuel le contenu doit agir. Il peut avoir une ou plusieurs de ces valeurs en même temps.

Liste des valeurs utilisables dans les menus (`<view name="menu">`) :

- `menuicons` : icônes des menus.

```xml
<menuIcons name="menuicons">
  ...
</menuIcons>
```

L'objet `<menuIcons>` peut contenir plusieurs autres propriétés.

#### Propriété `<iconKodi>`

La propriété `<iconKodi>` permet de charger un fichier image pour la ligne pour lancer Kodi dans les menus.

```xml
<iconKodi>./chemin/vers/fichier/image.png</iconKodi>
```

#### Propriété `<iconSystem>`

La propriété `<iconSystem>` permet de charger un fichier image pour la ligne des menus des réglages systèmes.

```xml
<iconSystem>./chemin/vers/fichier/image.png</iconSystem>
```

#### Propriété `<iconUpdates>`

La propriété `<iconUpdates>` permet de charger un fichier image pour la ligne des menus des mises à jour.

```xml
<iconUpdates>./chemin/vers/fichier/image.png</iconUpdates>
```

#### Propriété `<iconGames>`

La propriété `<iconGames>` permet de charger un fichier image pour la ligne des menus des options des jeux.

```xml
<iconGames>./chemin/vers/fichier/image.png</iconGames>
```

#### Propriété `<iconControllers>`

La propriété `<iconControllers>` permet de charger un fichier image pour la ligne des menus des réglages manettes.

```xml
<iconControllers>./chemin/vers/fichier/image.png</iconControllers>
```

#### Propriété `<iconUI>`

La propriété `<iconUI>` permet de charger un fichier image pour la ligne des menus des options de l'interface.

```xml
<iconUI>./chemin/vers/fichier/image.png</iconUI>
```

#### Propriété `<iconSound>`

La propriété `<iconSound>` permet de charger un fichier image pour la ligne des menus des options du son.

```xml
<iconSound>./chemin/vers/fichier/image.png</iconSound>
```

#### Propriété `<iconNetwork>`

La propriété `<iconNetwork>` permet de charger un fichier image pour la ligne des menus des options réseau.

```xml
<iconNetwork>./chemin/vers/fichier/image.png</iconNetwork>
```

#### Propriété `<iconScraper>`

La propriété `<iconScraper>` permet de charger un fichier image pour la ligne des menus du scrapeur.

```xml
<iconScraper>./chemin/vers/fichier/image.png</iconScraper>
```

#### Propriété `<iconAdvanced>`

La propriété `<iconAdvanced>` permet de charger un fichier image pour la ligne des menus des paramètres avancés.

```xml
<iconAdvanced>./chemin/vers/fichier/image.png</iconAdvanced>
```

#### Propriété `<iconQuit>`

La propriété `<iconQuit>` permet de charger un fichier image pour la ligne des menus pour quitter l'interface.

```xml
<iconQuit>./chemin/vers/fichier/image.png</iconQuit>
```

#### Propriété `<iconRestart>`

La propriété `<iconRestart>` permet de charger un fichier image pour la ligne des menus pour redémarrer.

```xml
<iconRestart>./chemin/vers/fichier/image.png</iconRestart>
```

#### Propriété `<iconShutdown>`

La propriété `<iconShutdown>` permet de charger un fichier image pour la ligne des menus pour éteindre.

```xml
<iconShutdown>./chemin/vers/fichier/image.png</iconShutdown>
```

#### Propriété `<iconFastShutdown>`

La propriété `<iconFastShutdown>` permet de charger un fichier image pour la ligne des menus pour l'extinction rapide.

```xml
<iconFastShutdown>./chemin/vers/fichier/image.png</iconFastShutdown>
```

#### Propriété `<iconLicense>`

La propriété `<iconLicense>` permet de charger un fichier image pour la ligne des menus pour la licence open-source.

```xml
<iconLicense>./chemin/vers/fichier/image.png</iconLicense>
```

#### Propriété `<iconRecalboxRGBDual>`

La propriété `<iconRecalboxRGBDual>` permet de charger un fichier image pour la ligne des menus pour crt.

```xml
<iconRecalboxRGBDual>./chemin/vers/fichier/image.png</iconRecalboxRGBDual>
```

#### Propriété `<iconTate>`

La propriété `<iconTate>` permet de charger un fichier image pour la ligne des menus pour les paramètres tate.

```xml
<iconTate>./chemin/vers/fichier/image.png</iconTate>
```

#### Propriété `<iconArcade>` ![](/tutorials/newicon-24.png)

La propriété `<iconArcade>` permet de charger un fichier image pour la ligne des menus pour les paramètres arcade.

```xml
<iconArcade>./chemin/vers/fichier/image.png</iconArcade>
```

#### Propriété `<iconDownload>`

La propriété `<iconDownload>` permet de charger un fichier image pour la ligne des menus pour le téléchargement de contenus.

```xml
<iconDownload>./chemin/vers/fichier/image.png</iconDownload>
```

## Objet `<menuSwitch>`

Les objets `<menuSwitch>` permettent de personnaliser les boutons `on` / `off`. Cet objet peut avoir plusieurs attributs :

#### Attribut `name`

- Obligatoire : oui

L'attribut `name` permet d'indiquer sur quel élément textuel le contenu doit agir. Il peut avoir une ou plusieurs de ces valeurs en même temps.

Liste des valeurs utilisables dans les menus (`<view name="menu">`) :

- `menuswitch` : boutons on / off des menus.

```xml
<menuSwitch name="menuswitch">
  ...
</menuSwitch>
```

L'objet `<menuSwitch>` peut contenir plusieurs autres propriétés.

#### Propriété `<pathOn>`

La propriété `<pathOn>` permet de charger un fichier image pour remplacer le bouton `ON`.

```xml
<pathOn>./chemin/vers/fichier/image.png</pathOn>
```

#### Propriété `<pathOff>`

La propriété `<pathOff>` permet de charger un fichier image pour remplacer le bouton `OFF`.

```xml
<pathOff>./chemin/vers/fichier/image.png</pathOff>
```

## Objet `<menuSlider>`

Les objets `<menuSlider>` permettent de personnaliser le bouton rond sur une barre horizontale comme dans les options du son. Cet objet peut avoir plusieurs attributs :

#### Attribut `name`

- Obligatoire : oui

L'attribut `name` permet d'indiquer sur quel élément textuel le contenu doit agir. Il peut avoir une ou plusieurs de ces valeurs en même temps.

Liste des valeurs utilisables dans les menus (`<view name="menu">`) :

- `menuslider` : bouton rond sur barre horizontale.

```xml
<menuSlider name="menuslider">
  ...
</menuSlider>
```

L'objet `<menuSlider>` peut contenir plusieurs autres propriétés.

#### Propriété `<path>`

La propriété `<path>` permet de charger un fichier image pour remplacer le bouton à déplacer.

```xml
<path>./chemin/vers/fichier/image.png</path>
```

## Objet `<menuButton>`

Les objets `<menuButton>` permettent de personnaliser les boutons servant à valider ou annuler des confirmations d'action. Cet objet peut avoir plusieurs attributs :

#### Attribut `name`

- Obligatoire : oui

L'attribut `name` permet d'indiquer sur quel élément textuel le contenu doit agir. Il peut avoir une ou plusieurs de ces valeurs en même temps.

Liste des valeurs utilisables dans les menus (`<view name="menu">`) :

- `menubutton` : boutons des menus.

```xml
<menuButton name="menubutton">
  ...
</menuButton>
```

L'objet `<menuButton>` peut contenir plusieurs autres propriétés.

#### Propriété `<path>`

La propriété `<path>` permet de charger un fichier image pour remplir l'arrière-plan des boutons non-sélectionné.

```xml
<path>./chemin/vers/fichier/image.png</path>
```

#### Propriété `<filledPath>`

La propriété `<filledPath>` permet de charger un fichier image pour remplir l'arrière-plan du bouton sélectionné / survolé.

```xml
<filledPath>./chemin/vers/fichier/image.png</filledPath>
```

## Objet `<menuText>`

Les objets `<menuText>` permettent de personnaliser les textes de base dans les menus. Cet objet peut avoir plusieurs attributs :

#### Attribut `name`

- Obligatoire : oui

L'attribut `name` permet d'indiquer sur quel élément textuel le contenu doit agir. Il peut avoir une ou plusieurs de ces valeurs en même temps.

Liste des valeurs utilisables dans les menus (`<view name="menu">`) :

- `menutext` : texte des menus.
- `menutitle` : titre des menus.
- `menufooter` : barre en bas des menus (comme la version en cours).

```xml
<menuText name="menutext">
  ...
</menuText>
```

L'objet `<menuText>` peut contenir plusieurs autres propriétés.

#### Propriété `<fontPath>`

La propriété `<fontPath>` permet d'indiquer un chemin pour charger la police souhaitée de l'objet parent.

```xml
<fontPath>./../arts/Exo2-BoldCondensed.otf</fontPath>
```

#### Propriété `<fontSize>`

La propriété `<fontSize>` permet d'indiquer la taille de police à appliquer à l'objet parent. La valeur est exprimée en pourcentage de la hauteur de l'écran (une valeur de `0.1` indique la taille de 10% de la hauteur de l'écran).

```xml
<fontSize>0.025</fontSize>
```

#### Propriété `<color>`

La propriété `<color>` permet d'indiquer la couleur du texte de l'objet parent. Les couleurs peuvent être au format RRVVBB ou RRVVBBAA (AA pour la transparence, 00 = transparent et ff = opaque).

```xml
<color>666666</color>
```

#### Propriété `<separatorColor>`

La propriété `<separatorColor>` permet de définir la couleur de séparation des textes. Les couleurs peuvent être au format RRVVBB ou RRVVBBAA (AA pour la transparence, 00 = transparent et ff = opaque).

```xml
<separatorColor>4368ffff</separatorColor>
```

#### Propriété `<selectedColor>`

La propriété `<selectedColor>` permet de définir la couleur du texte surligné. Les couleurs peuvent être au format RRVVBB ou RRVVBBAA (AA pour la transparence, 00 = transparent et ff = opaque).

```xml
<selectedColor>4368ffff</selectedColor>
```

#### Propriété `<selectorColor>`

La propriété `<selectorColor>` permet d'utiliser une couleur spécifique pour le surlignage des éléments de l'objet parent. Les couleurs peuvent être au format RRVVBB ou RRVVBBAA (AA pour la transparence, 00 = transparent et ff = opaque).

```xml
<selectorColor>4368ffff</selectorColor>
```

## Objet `<menuTextSmall>`

Les objets `<menuTextSmall>` permettent de personnaliser les textes de taille plus petits que les textes de base dans les menus. Cet objet peut avoir plusieurs attributs :

#### Attribut `name`

- Obligatoire : oui

L'attribut `name` permet d'indiquer sur quel élément textuel le contenu doit agir. Il peut avoir une ou plusieurs de ces valeurs en même temps.

Liste des valeurs utilisables dans les menus (`<view name="menu">`) :

- `menutextsmall` : texte des menus.

```xml
<menuTextSmall name="menutextsmall">
  ...
</menuTextSmall>
```

L'objet `<menuTextSmall>` peut contenir plusieurs autres propriétés.

#### Propriété `<fontPath>`

La propriété `<fontPath>` permet d'indiquer un chemin pour charger la police souhaitée de l'objet parent.

```xml
<fontPath>./../arts/Exo2-BoldCondensed.otf</fontPath>
```

#### Propriété `<fontSize>`

La propriété `<fontSize>` permet d'indiquer la taille de police à appliquer à l'objet parent. La valeur est exprimée en pourcentage de la hauteur de l'écran (une valeur de `0.1` indique la taille de 10% de la hauteur de l'écran).

```xml
<fontSize>0.025</fontSize>
```

#### Propriété `<color>`

La propriété `<color>` permet d'indiquer la couleur du texte de l'objet parent. Les couleurs peuvent être au format RRVVBB ou RRVVBBAA (AA pour la transparence, 00 = transparent et ff = opaque).

```xml
<color>666666</color>
```

#### Propriété `<selectedColor>`

La propriété `<selectedColor>` permet de définir la couleur du texte surligné. Les couleurs peuvent être au format RRVVBB ou RRVVBBAA (AA pour la transparence, 00 = transparent et ff = opaque).

```xml
<selectedColor>4368ffff</selectedColor>
```

#### Propriété `<selectorColor>`

La propriété `<selectorColor>` permet d'utiliser une couleur spécifique pour le surlignage des éléments de l'objet parent. Les couleurs peuvent être au format RRVVBB ou RRVVBBAA (AA pour la transparence, 00 = transparent et ff = opaque).

```xml
<selectorColor>4368ffff</selectorColor>
```

## Objet `<menuSize>`

Les objets `<menuSize>` permettent de personnaliser les tailles des éléments dans les menus. Cet objet peut avoir plusieurs attributs :

#### Attribut `name`

- Obligatoire : oui

L'attribut `name` permet d'indiquer sur quel élément textuel le contenu doit agir. Il peut avoir une ou plusieurs de ces valeurs en même temps.

Liste des valeurs utilisables dans les menus (`<view name="menu">`) :

- `menusize` : taille des éléments des menus.

```xml
<menuSize name="menusize">
  ...
</menuSize>
```

L'objet `<menuSize>` peut contenir plusieurs autres propriétés.

#### Propriété `<height>`

La propriété `<height>` permet d'appliquer une hauteur de surlignage des éléments de l'objet parent.

```xml
<height>0.5</height>
```