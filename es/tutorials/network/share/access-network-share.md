---
title: Imposible acceder a Recalbox desde la red de Windows
description: 
published: true
date: 2024-11-28T16:05:13.301Z
tags: red, acceso, compartidas, carpetas
editor: markdown
dateCreated: 2023-09-13T14:26:57.965Z
---

A veces ocurre que Recalbox no aparece en el explorador de red de Windows.

## Windows 10

### Ningún ordenador es visible

Si no puedes ver ningún ordenador que incluya Recalbox desde la red de Windows 10, hay varias cosas que podrían estar causando esto:

* La detección de red está desactivada por defecto y la configuración de red está establecida en "pública". 
* La función "Make this PC discoverable" está en "Disabled".

Tenéis que ir a la configuración de red y establecer vuestra red en modo "privado", o establecer "Hacer este PC detectable" en "habilitado".

### Recalbox no es más accesible desde la actualización 1709 de Windows 10

Desde Windows 10 Update 1709, podéis ver Recalbox en la red de Windows, pero no podéis más acceder.

Windows impide acceder a los recursos compartidos de red con el acceso de invitados activado. Y no se puede utilizar el protocolo SMBv1, que ha sido desactivado.

Para corregir esto, podéis descargar y aplicar [este parche](/tutorials/network/win10_allow_recalbox.reg). Este parche desactivará la seguridad en el acceso anónimo a la red en Windows, para restaurar el acceso a su Recalbox.

* [Fuente 1](https://tech.nicolonsky.ch/windows-10-1709-cannot-access-smb2-share-guest-access/)
* [Fuente 2](https://support.microsoft.com/fr-fr/help/4046019/guest-access-in-smb2-disabled-by-default-in-windows-10-and-windows-server-2016)

### Otras posibilidades

* Abrid el menú `Inicio` y luego `Parámetros` > `Aplicaciones` > `Aplicaciones y funcionalidades` > `Activar o desactivar funcionalidades de Windows`
* Activad la opción `sorpote de ficheros compartidos SMB 1.0/CIFS / client SMB1.0/CIFS...`
* Reiniciad Windows.

## Windows 11

### Recalbox no es más accesible desde la actualización 24H2 de Windows 11

Desde la actualización Windows 11 Update 24H2, se puede ver Recalbox en la red, pero no se puede acceder porque Windows pide un usuario y una contraseña.

Para remediarlo, podéis descargar y aplicar [este patch](/tutorials/network/win11_allow_recalbox.reg). El patch desactivará la seguridad de Windows de acceso anónimo a la red, y podréis volver a acceder a vuestra Recalbox como antes.

