---
title: Emulador Amiga 600 & 1200 & AmigaCD32
description: 
published: true
date: 2023-09-19T12:39:54.776Z
tags: amiga-cdtv, amiga-1200, amiga-600, amiga
editor: markdown
dateCreated: 2023-09-19T12:39:54.776Z
---

## Sub-sistemas

En Recalbox, las máquinas Amiga se han dividido en 3 subsistemas virtuales:  

* Amiga 600: Todas las máquinas _OCS/ECS_ desde la A1000 hasta la última A600.
* Amiga 120 a : Todas las máquinas _AGA_, desde la 1200 en adelante.
* Amiga CD32: La única consola verdadera de la serie Amiga.     
  * CDTV aún no está soportado, aunque el tema Recalbox y las configuraciones CDTV están listas. Será añadido más tarde como un nuevo subsistema, tan pronto como Amiberry lo soporte completamente.

## « Roms » soportadas

A continuación figura una lista de los formatos de archivo compatibles, ordenados por subsistema:

* **Amiga600 & Amiga1200** :

  * Discos **ADF** (`*.adf`), el formato de disco más popular (no puede incorporar protectores de disco). Pueden comprimirse en zip o 7-zip.
  **Discos IPF** (`*.ipf`), el formato más popular para volcados no introductorios (pueden incluir protectores de disco). Se pueden comprimir.
  **WHD** (carpetas, `*.lha/lzh/lz`, `*.zip`), el formato más conocido para juegos en disco duro. Las carpetas WHD se reconfiguran utilizando el archivo interno `*.slave`; sin embargo, se recomienda encarecidamente almacenarlas en formato `lha` o `zip`. Los WHDs descomprimidos no son más rápidos y lo que es peor, pueden hacer que EmulationStation sea extremadamente lento).
  **HDD FS**, disco duro en sistema de archivos. Las carpetas deben terminar con la extensión `.hd`. Raramente utilizado. Para verdaderos fans de Amiga que han hecho copias de seguridad de su Amiga HD. Se puede comprimir, pero no se recomienda.
  **HDF** (`*.hdf`), Imágenes de disco duro en un único archivo. Se puede comprimir (lectura/escritura).
  * **RP9** (`*.rp9`), packs Amiga Forever todo en uno.

* **Amiga CD32**:

  * **ISO** (`.bin`, `*.iso`, con o sin `*.cue`), volcados de CD. Puede comprimirse.

Otros formatos de imagen de CD pueden ser soportados pero no han sido probados por el equipo de Recalbox.


Al ejecutar los ficheros comprimidos/7-zipped, nuestro configurador intenta identificar el tipo de rom buscando extensiones específicas dentro del mismo. 

* ADF/IPF/BIN/ISO se identifican rápidamente.
* WHD` son rápidos, pero `LHA` es preferible.
* `HDDFS` puede tardar más en comprobar y puede dar lugar a interpretaciones erróneas. Si utilizáis `HDDFS`, dejadlos como ficheros y carpetas normales sin comprimir y cread un nombre de carpeta raíz que termine en `.hd` para que la identificación sea más rápida.

## Configuraciones prioritarias

Como podéis observar, este nuevo Amiberry soporta todos los formatos de roms de Amiga. Para hacer las cosas más fáciles para todos, hemos la gran parte del trabajo necesario para configurar automáticamente el emulador basado en lo que se quiere arrancar.

Recalbox construye una configuración de máquina por defecto por subsistema. Sin embargo, en algunas situaciones necesitaréis saltar la configuración generada automáticamente.

Recalbox genera los ficheros de configuración dentro de `/tmp/amiga/conf`. El más importante de ellos es el fichero `uaeconfig.uae`, en el que se encuentran todas las claves que definen la máquina, los discos duros, etc. 

No podemos describir aquí todas las claves de este fichero. Si tenéis curiosidad, os recomiendo echad un vistazo a las documentaciones relevantes de Amiberry y EAU.

Existen dos formas para modificar/saltar estos parámetros:

### Por sistema :

Cread un fichero llamado `/recalbox/share/system/configs/amiberry/<subsystem>.uae` (`/recalbox/share/system/configs/amiberry/amiga1200.uae` para el Amiga1200)

* Meted la clave que queréis saltar.
* Por ejemplo, podéis crear un fichero de configuración con el siguiente contenido para desactivar la pantalla LED en la esquina inferior derecha:

```ini
show_leds=false
```

### Por juego

Cread un fichero con el mismo nombre que la "rom" (o carpeta) pero que termine en `.uae`.

Por ejemplo, en vuestra carpeta de roms, si tenéis un fichero llamado `Aladdin (AGA).ipf`, podéis crear un `Aladdin (AGA).uae` y reemplazar las claves del mismo.

Recalbox genera la configuración en el siguiente orden:

* Crear la configuración por defecto para el tipo de rom y subsistema.
* Cargar `/recalbox/share/configs/<subsystem>.uae` si el fichero existe, y añadir/sobrescribir las claves de configuración.
* Cargar `<game>.uae` si el archivo existe, y añadir/sobrescribir las claves de configuración.

## Dentro del juego

Como en otros emuladores:

* `Hotkey + START` sale del emulador, 
* `Hotkey + B` lanza la interfaz de Amiberry.