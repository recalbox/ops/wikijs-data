---
title: Gestión de roms
description: 
published: true
date: 2024-07-26T07:32:20.326Z
tags: roms, gestión
editor: markdown
dateCreated: 2023-09-16T08:25:05.986Z
---

Aquí puedes encontrar cómo gestionar tus juegos con un software diseñado para gestionarlos.

Lista de categorías disponibles:

[Romulus Rom Manager](romulus-rom-manager)
[Verificar la firma MD5 de una rom o de un bios](check-md5sum-of-rom-or-bios)

Lista de tutoriales disponibles:

[Clrmamepro](clrmamepro)
[Lista de utilidades de gestión de roms](roms-management-tools)