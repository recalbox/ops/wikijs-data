---
title: Neo-Geo Pocket
description: 
published: true
date: 2024-07-26T08:33:33.820Z
tags: snk, neo-geo, pocket, ngp, portátiles
editor: markdown
dateCreated: 2023-09-04T08:20:22.540Z
---

![](/emulators/handheld/ngp.svg){.align-center}

## Detalles técnicos

* **Fabricante**: SNK
* **Año de lanzamiento**: 1999
* **Cantidades vendidas**: 2 millones, incluyendo la versión en color
* **Procesadores**: Toshiba TLCS900H @ 6.144 MHz, Z80 @ 3.072 MHz
* **RAM**: 12K para el 900H, 4K para el Z80
* **ROM**: 64 Kbytes
* **Unidad de procesamiento de imágenes**: 16 bits
* **Resolución**: 160x152 (256×256 pantalla virtual)
* **Soporte**: cartucho de 4 MB como máximo

## Presentación

La **Neo-Geo Pocket** es la primera consola portátil de SNK. Salió a la venta en Japón a finales de 1998, y se dejó de fabricar en 1999, cuando llegó la Neo-Geo Pocket Color, debido a que las ventas de la Neo-Geo Pocket monocroma fueron menores de lo esperado.

Este sistema sólo se comercializó en Japón y Hong Kong. Aunque tuvo una vida corta, se lanzaron en él algunos juegos importantes, como Samurai Shodown y King of Fighters R-1.

La Neo-Geo Pocket puede reproducir la mayoría de los nuevos juegos en color. Sin embargo, hay algunas excepciones notables, como Sonic the Hedgehog Pocket Adventure o SNK vs. Capcom: Match of the Millennium y The Last Blade. La Neo-Geo Pocket Color es totalmente retrocompatible.

## Emuladores

[Libretro FBNeo](libretro-fbneo)
[Libretro Mednafen_NGP](libretro-mednafen_ngp)
[Libretro-RACE](libretro-race)