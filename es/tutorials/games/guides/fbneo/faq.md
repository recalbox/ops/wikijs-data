---
title: FAQ
description: 
published: true
date: 2024-09-02T21:15:01.896Z
tags: faq
editor: markdown
dateCreated: 2023-09-20T07:00:43.920Z
---



### ¿Por qué no se inicia el juego XXX?

No estará soportado o quizás tengas una rom mala, los logs os darán más detalles. Cread un romset válido con clrmamepro. También hay algunos juegos marcados como que no funcionan, probad con uno de sus clones.

### He arreglado el juego XXX y ahora no puedo ejecutarlo, ¿por qué?

Porque se considera una rom mala ya que los crcs no coinciden, sin embargo hay un método para usar una romset parcheada, si ponéis la versión parcheada de la romset en `/recalbox/share/bios/fbneo/patched`, el juego funcionará.

### El juego XXX tiene problemas gráficos, ¿por qué?

Contactadnos con un informe detallado del problema y de vuestra plataforma hardware.

### ¿Por qué el juego XXX se ejecuta lentamente?

Probablemente vuestro hardware es demasiado lento para ejecutar el juego con los ajustes normales. Probad lo siguiente:


* Verificad si hay un interruptor DIP Speedhack en las opciones principales, y activadlo.
* Intentad establecer un valor para frameskip en las opciones principales.
* Intentad reducir el reloj del procesador en las opciones básicas.
* Intentad desactivar el rebobinado, runahead o cualquier otro parámetro retroarch conocido por aumentar la sobrecarga del sistema.
* Intentad reducir los parámetros de audio en las opciones principales.
* Si esto no ayuda, actualizad / overclockear vuestro hardware.

No aceptaremos peticiones para "hacer el kernel más rápido", por lo que a nosotros respecta, nuestro kernel posee un buen equilibrio entre precisión y velocidad, y en su mayor parte funciona muy bien en las cartas que tienen poco rendimiento (rpi3, ...).

### El juego XXX tiene un sonido entrecortado, ¿por qué?

Probablemente por la misma razón que arriba.

### El juego XXX funciona más rápido en MAME2003 / MAME2010, ¿por qué?

El emulador no es exactamente MAME, en realidad usamos un código más "actualizado". En general, FB Alpha es más lento que la versión antigua de MAME, pero más preciso y con menos errores. El port libretro también soporta varias características que son generalmente buggy o ausentes en los cores de MAME (netplay, rebobinado, retroiluminación, etc.). Todo esto hace que el emulador necesite utilizar recursos adicionales.

### El código de trucos no funciona, ¿por qué?

Debe existir un soporte parcial mediante la nueva API basada en la exposición de la ram principal.

### ¿Por qué no funciona más el CD de Neogeo?

Hay varias cosas que tenéis que saber:

* Necesitáis una copia de neocdz.zip y neogeo.zip dentro de vuestro directorio de roms.
* Necesitáis añadir `--subsystem neocd` a la linea de comandos o bien poner vuestros juegos en una carpeta `neocd`.
* Los formatos soportados son ccd / sub / img (trurip) y el fichero único MODE1 / 2352 cue / bin (podéis utilizar "CDmage" para convertir vuestra iso en caso de necesidad), **las isos no deben estar comprimidas**.

Utilizad los siguientes pasos para convertir vuestras isos no compatibles:

* Descargad [CDMage 1.02.1](https://www.videohelp.com/software/CDMage) (gratuito y sin publicidad)
* Fichero `Abrir`; seleccionad vuestra iso (NB: para las isos multi-pistas, seleccionad el fichero .cue, no el archivo .iso)
* Fichero `Guardar como`; escribid el nombre del nuevo fichero que será generado.
* Comprobad que habéis seleccionado MODO1 / 2352 en el segundo menú desplegable.
* Pulsad OK, esperad a que termine el proceso (unos segundos), ¡y ya está!

### Killer Instinct no funciona, ¿por qué?

Este driver ha sido desactivado por el momento ya que no responde a nuestro criterios de calidad.

~~Debéis saber varias cosas:~~

* ~~Sólo funciona correctamente bajo x86\_64 \(se necesita un procesador de al menos 4Ghz porque las cartas de bajo coste no poseen un dynarec mips3\), y el core tiene que ser compilado con el siguiente comando para activar este dynarec: `make -j5 -C src/burner/libretro USE_X64_DRC=1`~~
* ~~Si vuestra rom se encuentra como aquí `ROM_DIRECTORY/kinst.zip`, necesitaréis que vuestra imagen de disco no comprimida se encuentre aquí: `ROM_DIRECTORY/kinst/kinst.img`~~
* ~~Para obtener la image de disco no comprimida, necesitaréis utilizar la herramienta chdman de MAME a partir del chd proporcionado por mame, el comando se parecerá al siguiente: `chdman extracthd -i kinst.chd -o kinst.img`~~

### ¿Por qué no funciona Hiscore?

Tener hiscore.dat y la opción básica activada no garantiza que las puntuaciones funcionen para un juego en concreto, a veces el driver no dispone del código necesario. Podéis pedir ayuda para intentar resolver estos problemas siempre y cuando la petición sea razonable (es decir, evitad hacer una lista de varias docenas/cientos de juegos).

---

## Samples

Los samples deben colocarse dentro de `/recalbox/share/bios/fbneo/samples`.

## Hiscore.dat

Copiad [hiscore.dat](https://github.com/libretro/FBNeo/blob/master/metadata/hiscore.dat) dentro de `/recalbox/share/bios/fbneo/`.

## MAPPING

No disponemos de una herramienta tan práctica como el OSD MAME, pero podemos utilizar el API de RetroArch para personalizar los mappings, desde `Quick menu` > `Controls`.
Para aquellos que no quieran personalizar completamente los mappings, podéis aplicar dos ajustes predefinidos muy útiles simplemente cambiando el "tipo de dispositivo" dentro de este menú:

* **Classic/Clásico**: esto aplicará el mapping 'cuadrado' del neogeo cd original en los juegos neogeo, y usará L / R como 5º y 6º botón para los juegos de 6 botones como Street Fighter II.
* **Modern/Moderno**: aplicará el mapeado de King of Fighters de Playstation 1 y superiores en los juegos de lucha neogeo, y usará R1 / R2 como 5º y 6º botón para los juegos de 6 botones como Street Fighter II (por la misma razón que los juegos neogeo), lo cual es realmente práctico para la mayoría de los arcade sticks.

También existen los siguientes "tipos de dispositivos", pero no serán compatibles con todos los juegos:


* **Mouse (ball only) / Ratón (bola únicamente)** : utilizará el ratón/trackball para los movimientos analógicos, los botones permanecerán en el retropad
* **Mouse (full) / Ratón (completo)** : como el anterior, pero los bontones serán los botones del ratón
* **Pointer / Pointeur** : utilizará un dispositivo "puntero" (quizás un ratón/trackball) para determinar las coordenadas de la pantalla, y los botones permanecerán en el retropad
* **Lightgun** : utilizará el lightgun para determinar las coordenadas en la pantalla, los botones también estarán en el lightgun.

## Dat

Utilizad [clrmamepro](./../../../utilities/rom-management/clrmamepro) (que funciona muy bien bajo Linux mediante Wine y en macOS avec Crossover) para crear romsets válidos basados en los datos proporcionados por los ficheros [dats](https://github.com/libretro/FBNeo/tree/master/dats).
No vale la pena comunicarnos problemas relacionados con los juegos si no habéis validado vuestro romset.
Tened en cuenta que los ficheros dat proporcionados por recalbox son únicamente de roms padres, aunque no se aconseja utilizar sólo las roms padres (algunas no funcionan y los clones pueden ser muy diferentes de sus padres).

## Emulación de consolas

Podéis emular consolas (con romsets específicos, los dats también están presentes en el directorio [dats] (https://github.com/libretro/FBNeo/blob/master/dats)) anteponiendo al nombre de las roms el prefijo `XXX_` y eliminando la extensión `zip|7z`, o usando el argumento `--subsystem XXX` por línea de comandos. La lista de prefijos disponibles es la siguiente:

* CBS ColecoVision : `cv`
* MSX 1 : `msx`
* Nec PC-Engine : `pce`
* Nec SuperGrafX : `sgx`
* Nec TurboGrafx-16 : `tg`
* Nintendo Entertainment System : `nes`
* Nintendo Family Disk System : `fds`
* Sega GameGear : `gg`
* Sega Master System : `sms`
* Sega Megadrive : `md`
* Sega SG-1000 : `sg1k`
* SNK Neo-Geo Pocket : `ngp`
* ZX Spectrum : `spec`

También es posible utilizar el nombre de la carpeta para la detección (este segundo método se ha añadido porque algunos dispositivos no son compatibles con los subsistemas):

* CBS ColecoVision : `coleco`
* MSX 1 : `msx`
* Nec PC-Engine : `pce`
* Nec SuperGrafX : `sgx`
* Nec TurboGrafx-16 : `tg16`
* Nintendo Entertainment System : `nes`
* Nintendo Family Disk System : `fds`
* Sega GameGear : `gamegear`
* Sega Master System : `sms`
* Sega Megadrive : `megadriv`
* Sega SG-1000 : `sg1000`
* SNK Neo-Geo Pocket : `ngp`
* ZX Spectrum : `spectrum`