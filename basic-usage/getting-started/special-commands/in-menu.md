---
title: Dans le Menu
description: Lorsque vous êtes dans le menu, des commandes spéciales sont disponibles.
published: true
date: 2021-09-04T17:07:33.344Z
tags: menu, commandes spéciales
editor: markdown
dateCreated: 2021-05-21T08:12:47.872Z
---

## Fonction de recherche

Depuis le menu des systèmes active la **fonction de recherche**.

* `R1`

## Menu d'extinction

Depuis le menu des systèmes fait apparaitre le **menu d'extinction** de Recalbox.

* `Select`

## Lancer les clips de jeux

Si vous avez choisi d'utiliser comme écran de veille les clips de jeux, vous pouvez les lancer sans attendre un certain temps pour les démarrer.

* `Y`