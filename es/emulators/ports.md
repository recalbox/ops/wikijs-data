---
title: Ports
description: 
published: true
date: 2024-07-26T08:27:56.758Z
tags: ports
editor: markdown
dateCreated: 2023-09-04T07:25:41.662Z
---

## Emuladores de motores de juego

[Doom](doom)
[Minecraft](minecraft)
[Out Run](out-run)
[Quake 1](quake-1)
[Sigil](sigil)
[Wolfenstein 3D](wolfenstein-3d)

## Emuladores de juegos vídeo amateur

[CaveStory](cavestory)

## Emuladores de juegos de puzles

[2048](2048)
[DinoThawr](dinothawr)

## Emuladores de juegos de plataformas

[Flashback](flashback)
[Prince of Persia](prince-of-persia)
[Rick Dangerous](rick-dangerous)

## Emuladores de juegos de acción

[MrBoom](mrboom)

## Emuladores de juegos rompe-ladrillos

[Pong](pong)