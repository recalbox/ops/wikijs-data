---
title: Conectar un mecanismo de monedas comparativo en un Raspberry Pi
description: 
published: true
date: 2023-09-18T19:54:54.367Z
tags: raspberry, mecanismo, monedas, comparativo, conectar, tragamonedas
editor: markdown
dateCreated: 2023-09-18T19:54:54.367Z
---

Este tutorial utiliza elementos de las instrucciones suministradas con [este tragamonedas exclusivo](http://www.smallcab.net/monnayeur-comparateur-facade-silver-p-902.html).

## Principio de funcionamiento del tragamonedas

El tragamonedas funciona comparando la moneda insertada con la moneda "testigo" instalada permanentemente en el tragamonedas. El mecanismo de monedas analiza la firma electrónica de la moneda insertada y la compara con la firma electrónica de la moneda testigo.

Si la moneda introducida tiene una firma cercana a la de la moneda de referencia, la moneda es aceptada, de lo contrario es rechazada. Cuando se acepta una moneda, el monedero genera un impulso eléctrico en el Raspberry Pi.

Este impulso es reconocido por Recalbox (botón `SELECT`) y se concede entonces un crédito al jugador.

>Es posible utilizar fichas metálicas ("_token_") en lugar de monedas.
{.is-info}

El tragamonedas funciona de forma muy sencilla por medio de cuatro alambres:

* Rojo: +12V para conectar a [una fuente de alimentación de 12V/1A](https://www.amazon.fr/dp/B00B0T5D5W/).
* Blanco: "Insert coin" para conectar al GPIO (o controlador USB), correspondiente al botón `SELECT` en Recalbox.

>¡Tiene que ser a través de un "relé"! (interruptor electro-mecánico)
{.is-danger}

* Negro: masa a conectar al 0V de la alimentación 12V/1A.
* Gris: contador (no se utiliza aquí).

## Instalación en un Raspberry Pi

Como os habréis dado cuenta, utilizar este monedero con un Raspberry Pi requiere necesariamente el uso de un componente electrónico intermedio llamado "relé" o "keyboard hack" para los puristas.

Aquí tenéis una solución de bajo coste para fabricaros vuestro propio "hack de teclado".

## Equipamiento a preveer

* 1 buen soldador, alambre para hacer pistas, estaño y grasa para los codos
* 1 [relé 12V](https://www.amazon.fr/dp/B013SPRXQC) (5 cables)
* 1 [placa prototipo](https://www.amazon.fr/dp/B01GUZ5L6G/)
* 2 [bloques de terminales](https://www.amazon.fr/dp/B00I00OHHY)

## Cableado

>¡No dudéis en probar vuestra tarjeta ANTES de conectarla al Raspberry y a vuestro monedero!
{.is-warning}

El cableado del monedero en el Raspberry Pi requiere la instalación de este "hack de teclado" ENTRE el monedero Y el GPIO del Raspberry Pi.

Los 2 pines en el "Bloque de terminales 1" deben ser conectados:

* Uno al cable "0V" de la fuente de alimentación de 12V/1A.
* El otro al cable "blanco" del monedero.

Los 2 pines del "bloque de terminales 2" deben conectarse:

* Uno al pin "GND" del conector GPIO del Raspberry Pi.
* El otro a un pin "GPIOxx" del conector GPIO del Raspberry Pi (botón `SELECT`).

Consultad la documentación específica de vuestro modelo para identificar el conector GPIO del Raspberry Pi y la disposición de los pines "GND" y "GPIOxx" dentro de vuestro conector.