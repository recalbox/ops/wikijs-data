---
title: Nintendo DS
description: 
published: true
date: 2024-07-26T08:33:57.357Z
tags: nintendo, dos, ds, portátiles, pantallas
editor: markdown
dateCreated: 2023-09-04T08:25:17.165Z
---

![](/emulators/handheld/nds.svg){.align-center}

## Detalles técnicos

* **Fabricante**: Nintendo
* **Año de lanzamiento**: 2004
* **Cantidad vendida**: 153,99 millones
* **Juego más vendido**: New Super Mario Bros.
* **Procesador**: 66 MHz ARM9 + 33 MHz ARM7
* **RAM**: 4 MB
* **RAM de vídeo**: 656 KB
* **Pantalla**: TFT LCD táctil
* **Resolución**: 256x192 píxeles
* **Capacidad de visualización**: 260000 colores
* **Sonido**: Estéreo con sonido virtual, auriculares

## Presentación

La **Nintendo DS** ("DS" por Dual Screen, Doble Pantalla en Japón), es una consola portátil creada por Nintendo, lanzada a finales de 2004 en Japón y Norteamérica y en 2005 en Europa.

Está equipada con una serie de características que antes eran raras, si no inéditas, en los videojuegos portátiles, como dos pantallas retroiluminadas simultáneamente, una de las cuales es táctil, un micrófono, dos puertos para cartuchos (uno para juegos de Nintendo DS, el otro para cartuchos y accesorios de Game Boy Advance), dos altavoces compatibles con sonido envolvente (virtual) y Wi-Fi integrado, con un alcance de 10 a 30 metros en modo LAN, que permite conectar dieciséis consolas entre sí y conectarse a la Conexión Wi-Fi de Nintendo para jugar en línea.

En 2006, Nintendo lanzó la Nintendo DS Lite, un modelo revisado de la consola, más pequeño y ligero. En 2008, Nintendo lanzó la Nintendo DSi, una nueva revisión de la consola con nuevos añadidos como la doble cámara.

La Nintendo DS, con sus diversas variantes, es actualmente la consola portátil más vendida de todos los tiempos y la segunda consola más vendida si se tienen en cuenta las consolas domésticas, sólo por detrás de la PlayStation 2 de Sony. Su juego estrella, New Super Mario Bros, es uno de los más vendidos del mundo.

## Emulador

[Libretro DeSmuME](libretro-desmume)
[Libretro melonDS](libretro-melones)
