---
title: Libretro FCEUmm
description: 
published: true
date: 2024-07-24T14:57:34.928Z
tags: libretro, fceumm, nes
editor: markdown
dateCreated: 2021-08-01T13:36:40.920Z
---



## ![](/emulators/license.svg) License

This core is under [**GPLv2**](https://github.com/libretro/libretro-fceumm/blob/master/Copying) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Features

| Feature | Supported |
| :---: | :---: |
| Restart | ✔ |
| Screenshots | ✔ |
| Saves | ✔ |
| States | ✔ |
| Rewind | ✔ |
| Netplay | ✔ |
| Core Options | ✔ |
| RetroAchievements | ✔ |
| RetroArch Cheats | ✔ |
| Native Cheats | ✔ |
| Controls | ✔ |
| Remapping | ✔ |
| Softpatching | ✔ |

## ![](/emulators/bios.svg) BIOS

>**No bios is required.**
{.is-success}

## ![](/migration-images/emulateurs/consoles-de-salon/nintendo-entertainement-system/rom-30098_640.png) Roms

### Supported extensions

Roms must have the extensions:

* .nes
* .unf
* .unif
* .zip
* .7z

This system supports compressed roms in .zip/.7z format. However, be careful, it is only an archive.

The files contained in the .zip/.7z must correspond to the extensions mentioned above.
Moreover, each .zip/.7z file must contain only one compressed rom.

### Location

Put the roms like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 nes
┃ ┃ ┃ ┃ ┣ 🗒 **game.zip**

>The roms using **No-Intro** format are highly recommended.
{.is-success}

>For more information about roms, go to [this tutorial](./../../../tutorials/games/generalities/isos-and-roms)!
{.is-info}

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Accessing to options

You can configure various options in two different ways.

* Via RetroArch menu:

┣ 📁 RetroArch Menu
┃ ┣ 📁 Core options
┃ ┃ ┣ 🧩 Name_option

* Via the file `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core options

| Options | Options values | Variables (for configuration override) | Variables values |
| --- | --- | --- | --- |
| Region | `Auto` ✅ / `NTSC` / `PAL` / `Dendy` | `fceumm_region` | `Auto` / `NTSC` / `PAL` / `Dendy` |
| Game Genie Add-On (Restart Required) | `Disabled` ✅ / `Enabled` | `fceumm_game_genie` | `disabled` / `enabled` |
| Aspect Ratio | `8:7 PAR` ✅ / `4:3` / `Pixel Perfect` | `fceumm_aspect` | `8:7 PAR` / `4:3` / `PP` |
| Crop Horizontal Left Overscan | `Disabled` ✅ / `Enabled` | `fceumm_overscan_h_left` | `disabled` / `enabled` |
| Crop Horizontal Right Overscan | `Disabled` ✅ / `Enabled` | `fceumm_overscan_h_right` | `disabled` / `enabled` |
| Crop Vertical Top Overscan | `Disabled` ✅ / `Enabled` | `fceumm_overscan_v_top` | `disabled` / `enabled` |
| Crop Vertical Bottom Overscan | `Disabled` ✅ / `Enabled` | `fceumm_overscan_v_bottom` | `disabled` / `enabled` |
| Color Palette | `Default` ✅ / `AspiringSquire's Real` / `Nintendo Virtual Console` / `Nintendo RGB PPU` / `FBX's YUV-V3` / `FBX's Unsaturated-Final` / `Sony CXA2025AS US` / `PAL` / `BMF's Final 2` / `BMF's Final 3` / `FBX's Smooth` / `FBX's Composite Direct` / `FBX's PVM Style D93` / `FBX's NTSC Hardware` / `FBX's NES Classic (fixed)` / `RGBSource's NESCAP` / `nakedarthur's Wavebeam` / `FBX's Digital Prime` / `FBX's Magnum` / `FBX's Smooth V2` / `FBX's NES Classic` / `Raw` / `Custom` | `fceumm_palette` | `default` / `asqrealc` / `nintendo-vc` / `rgb` / `yuv-v3` / `unsaturated-final` / `sony-cxa2025as-us` / `pal` / `bmf-final2` / `bmf-final3` / `smooth-fbx` / `composite-direct-fbx` / `pvm-style-d93-fbx` / `ntsc-hardware-fbx` / `nes-classic-fbx-fs` / `nescap` / `wavebeam` / `dogotal-prime-fbx` / `magnum-fbx` / `smooth-v2-fbx` / `nes-classic-fbx` / `raw` / `custom` |
| NTSC Filter | `Disabled` ✅ / `Composite` / `S-Video` / `RGB` / `Monochrome` | `fceumm_ntsc_filter` | `disabled` / `composite` / `svideo` / `rgb` / `monochrome` |
| Sound Quality | `Low` ✅ / `High` / `Very High` | `fceumm_sndquality` | `Low` / `High` / `Very High` |
| Audio RF Filter | `Disabled` ✅ / `Enabled` | `fceumm_sndlowpass` | `disabled` / `enabled` |
| Stereo Sound Effect | `Disabled` ✅ / `1ms Delay` / `2ms Delay` / `3ms Delay` / `4ms Delay` / `5ms Delay` / `6ms Delay` / `7ms Delay` / `8ms Delay` / `9ms Delay` / `10ms Delay` / `11ms Delay` / `12ms Delay` / `13ms Delay` / `14ms Delay` / `15ms Delay` / `16ms Delay` / `17ms Delay` / `18ms Delay` / `19ms Delay` / `20ms Delay` / `21ms Delay` / `22ms Delay` / `23ms Delay` / `24ms Delay` / `25ms Delay` / `26ms Delay` / `27ms Delay` / `28ms Delay` / `29ms Delay` / `30ms Delay` / `31ms Delay` / `32ms Delay` | `fceumm_sndstereodelay` | `disabled` / `01_ms_delay` / `02_ms_delay` / `03_ms_delay` / `04_ms_delay` / `05_ms_delay` / `06_ms_delay` / `07_ms_delay` / `08_ms_delay` / `09_ms_delay` / `10_ms_delay` / `11_ms_delay` / `12_ms_delay` / `13_ms_delay` / `14_ms_delay` / `15_ms_delay` / `16_ms_delay` / `17_ms_delay` / `18_ms_delay` / `19_ms_delay` / `20_ms_delay` / `21_ms_delay` / `22_ms_delay` / `23_ms_delay` / `24_ms_delay` / `25_ms_delay` / `26_ms_delay` / `27_ms_delay` / `28_ms_delay` / `29_ms_delay` / `30_ms_delay` / `31_ms_delay` / `32_ms_delay` |
| Swap Audio Duty Cycles | `Disabled` ✅ / `Enabled` | `fceumm_swapduty` | `disabled` / `enabled` |
| Master Volume | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` ✅ / `8` / `9` / `10` | `fceumm_sndvolume` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` |
| Audio Channel 1 (Square 1) | `Enabled` ✅ / `Disabled` | `fceumm_apu_1` | `enabled` / `disabled` |
| Audio Channel 2 (Square 2) | `Enabled` ✅ / `Disabled` | `fceumm_apu_2` | `enabled` / `disabled` |
| Audio Channel 3 (Triangle) | `Enabled` ✅ / `Disabled` | `fceumm_apu_3` | `enabled` / `disabled` |
| Audio Channel 4 (Noise) | `Enabled` ✅ / `Disabled` | `fceumm_apu_4` | `enabled` / `disabled` |
| Audio Channel 5 (PCM) | `Enabled` ✅ / `Disabled` | `fceumm_apu_5` | `enabled` / `disabled` |
| Turbo Enable | `None` ✅ / `Player 1` / `Player 2` / `Both` | `fceumm_turbo_enable` | `None` / `Player 1` / `Player 2` / `Both` |
| Turbo Delay (in frames) | `1` / `2` / `3` ✅ / `5` / `10` / `15` / `30` / `60` | `fceumm_turbo_delay` | `1` / `2` / `3` / `5` / `10` / `15` / `30` / `60` |
| Zapper Mode | `Crosshair light gun` ✅ / `Sequential Targets light gun` / `Touchscreen` / `Mouse` | `fceumm_zapper_mode` | `clightgun` / `stlightgun` / `touchscreen` / `mouse` |
| Show Zapper Crosshair | `Enabled` ✅ / `Disabled` | `fceumm_show_crosshair` | `enabled` / `disabled` |
| Zapper Tolerance | `0` / `1` / `2` / `3` / `4` / `5` / `6` ✅ / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` | `fceumm_zapper_tolerance` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` |
| Invert Zapper Trigger Signal | `Enabled` ✅ / `Disabled` | `fceumm_zapper_trigger` | `enabled` / `disabled` |
| Invert Zapper Sensor Signal | `Enabled` ✅ / `Disabled` | `fceumm_zapper_sensor` | `enabled` / `disabled` |
| Arkanoid Mode | `Absolute mouse` / `Mouse` ✅ / `Stelladaptor` / `Touchscreen` | `fceumm_arkanoid_mode` | `abs_mouse` / `mouse` / `stelladaptor` / `touchscreen` |
| Mouse Sensitivity | `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ / `110` / `120` / `130` / `140` / `150` / `160` / `170` / `180` / `190` / `200` | `fceumm_mouse_sensitivity` | `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` / `110` / `120` / `130` / `140` / `150` / `160` / `170` / `180` / `190` / `200` |
| Allow Opposing directions | `Disabled` ✅ / `Enabled` | `fceumm_up_down_allowed` | `disabled` / `enabled` |
| No Sprit Limit | `Disabled` ✅ / `Enabled` | `fceumm_nospritelimit` | `disabled` / `enabled` |
| Overclock | `Disabled` ✅ / `Post-rendu x2` / `Rafraîchissement vertical x2` | `fceumm_overclocking` | `disabled` / `2x-Postrender` / `2x-VBlank` |
| RAM Power-On Fill (Restart Required) | `$FF` ✅ / `$00` / `Aléatoire` | `fceumm_ramstate` | `fill $ff` / `fill $00` / `random` |

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/libretro/libretro-fceumm/](https://github.com/libretro/libretro-fceumm/)
* **Libretro documentation**: [https://docs.libretro.com/library/fceumm/](https://docs.libretro.com/library/fceumm/)