---
title: Vice_xscpu64
description: 
published: true
date: 2024-07-25T16:32:59.625Z
tags: commodore-64, c64, vice, xscpu64, 9.1+
editor: markdown
dateCreated: 2024-07-25T16:32:59.625Z
---



## ![](/emulators/license.svg) License

This core is under [**GPLv2**](https://sourceforge.net/p/vice-emu/code/HEAD/tree/trunk/vice/COPYING) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Features



## ![](/emulators/bios.svg) BIOS

### List of optional bios

| Filename | Description | MD5 | Provided |
| :---: | :---: | :---: | :---: |
| scpu-dos-1.4.bin | CMD SuperCPU Kernal 1.4 | cda2fcd2e1f0412029383e51dd472095 | ❌ |
| scpu-dos-2.04.bin | CMD SuperCPU Kernal 2.04 | b2869f8678b8b274227f35aad26ba509 | ❌ |

### Location

Put the bios like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 vice
┃ ┃ ┃ ┃ ┣ 📁 SCPU64
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **scpu-dos-1.4.bin**
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **scpu-dos-2.04.bin**

## ![](/emulators/roms.png) Roms

### ESupported extensions

Roms must have the extension:

* .bin
* .cmd
* .crt
* .d1m
* .d2m
* .d4m
* .d64
* .d6z
* .d67
* .d71
* .d7z
* .d80
* .d81
* .d82
* .d8z
* .d90
* .dhd
* .g41
* .g4z
* .g64
* .g6z
* .m3u
* .nbz
* .nib
* .p00
* .p64
* .prg
* .t64
* .tap
* .tcrt
* .vfl
* .vsf
* .x64
* .x6z
* .zip
* .7z
* .gz

This system supports compressed roms in .zip/.7z format. However, be careful, it is only an archive.

The files contained in the .zip/.7z must correspond to the extensions mentioned above.
Moreover, each .zip/.7z file must contain only one compressed rom.

### Location

Put the roms like this: 

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 c64
┃ ┃ ┃ ┃ ┣ 🗒 **game.zip**

>The roms using **No-Intro** format are highly recommended.
{.is-success}

>For more information about roms, go to [this tutorial](./../../../tutorials/games/generalities/isos-and-roms)!
{.is-info}

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://sourceforge.net/projects/vice-emu/](https://sourceforge.net/projects/vice-emu/)