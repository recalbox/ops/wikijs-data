---
title: Associer une manette PS4
description: Comment associer sa manette PS4 à Recalbox
published: true
date: 2024-02-27T21:34:41.446Z
tags: bluetooth, manette, ps4, association
editor: markdown
dateCreated: 2024-02-27T14:45:49.879Z
---

## Introduction

Les manettes PS4 peuvent être un casse-tête à être reconnues. Ce tutoriel devrait pouvoir vous aider.

## Procédure d'association

>La procédure suivante a été testée avec une manette PS2 version 2. Il peut être différent pour une version 1.
{.is-info}

Voici comment faire reconnaitre votre manette PS4 :

- Allumez la manette pendant que votre PS4 est débranchée (seule la manette doit être allumée).
- Pendant que la manette est allumée, sous la manette, vous aurez un petit bouton accessible via un trombone. Appuyez dessus pour la réinitialiser.

![Localisation du bouton reset](/tutorials/controllers/controllers/ps4/resetbutton.jpeg){.center}

- Une fois la manette réinitialisée et éteinte, appuyez pendant 5 secondes sur les boutons `PS` et `SHARE` en même temps.

![Boutons PS et SHARE pour l'appairage](/tutorials/controllers/controllers/ps4/ds4pairingguide.png){.center}

Voilà, votre manette devrait être appairée / jumelée et fonctionner sans même devoir la reconfigurer.

Source de la procédure : https://forum.recalbox.com/topic/8920/a-lire-manettes-ps4
Sources des images : [ici](https://helpdeskgeek.com/how-to/how-to-reset-a-ps4-controller/) et [ici](https://randocity.com/2015/07/31/how-to-pair-your-ps4-controller-wirelessly/)

## Éteindre la manette

La manette peut bien évidemment être éteinte. Pour cela, vous devez rester appuyé sur le bouton PS pendant 10 secondes.