---
title: Preparación e instalación
description: Cómo instalar Recalbox
published: true
date: 2024-11-28T15:21:02.543Z
tags: instalación, preparación
editor: markdown
dateCreated: 2023-04-19T09:59:31.109Z
---

## I - Los sistemas compatibles

Recalbox se puede instalar en diferentes dispositivos:

- **Raspberry Pi 5 / CM5**
- Raspberry Pi 4 / 400 / CM4
- Raspberry Pi 3
- Raspberry Pi 2
- Raspberry Pi 1 / Zero / Zero 2
- Anbernic RG353M (consola portátil) 
- Odroid Go Advance (consola portatil
- Odroid Go Super (consola portátil) 
- Ordinateur 32 ou 64 bits
- Odroid XU4

Para descubrir Recalbox, el equipo os aconseja vivamente de elegir el **Raspberry Pi 5** !

## II - Instalación de Recalbox

### 1 - Descarga / flash de la imagen

Situaros en [https://www.recalbox.com/es/download/stable/](https://www.recalbox.com/es/download/stable/) y seguid las instrucciones de pantalla para descargar y grabar la imagen de Recalbox en vuestro media de almacenamiento (carta SD, carta eMMC o disco duro)

>**NO DESCOMPRIMÁIS NUNCA LA IMAGEN DESGARGADA**. El programa se ocupará de grabarla tal cual.
{.is-danger}

>Sólo se puede descargar la **última version (la más reciente)** de Recalbox.
>Las **versiones antiguas** no se pueden **descargar** y el equipo de desarrollo **no proporcionará ninguna ayuda** sobre estas versiones.
{.is-warning}

>Se debe utilizar un media de minimo 8 Gb para instalar el sistema (se recomiendan las tarjetas SD de tipo **Sandisk Ultra series**).
>
>* Para la **instalación en una Raspberry Pi** :
>   * Como media de almacenamiento : una **tarjeta microSD**.
>* Para la **instalación en una Odroid** :
>    * Como media de almacenamiento : una **tarjeta microSD o eMMC**. 
>* Para la **instalación en un x86 y x86_64** :
>    * Como media de almacenamiento : una **llave USB 3.0** o un **disco duro**.

### 2 - Instalación

#### Raspberry Pi / Odroid Go Advance / Odroid Go Super

* Introduce la tarjeta microSD en el aparato sobre el que queréis utilizar Recalbox.
* Encended/enchufad vuestro sistema. La instalación de Recalbox se lanza automaticamente.
* La instalación dura varios minutos y depende de la velocidad del material utilizado.

#### Retroflag GPI Case

La instalación sobre una Gpi Case se hace de la misma forma : insertad la tarjeta micro SD, enchufad el sistema a la corriente y Recalbox detecta y configura automaticamente todo. Para más información, consultad la página [RetroFlag GPi Case](./preparation-and-installation/retroflag-gpi-case)

#### Odroid XU4 / Odroid XU4Q

* Insertad la tarjeta microSD ou eMMC en el dispositivo sobre el que se quiere instalar Recalbox.
* Reinicad y la instalación se lanza.

#### x86 / x86_64

La instalación se puede efectuar en una llave USB o directamente en un disco duro externo. Una vez que la imagen ha sido grabada:

* Enchufad la llave USB o disco duro externo y reiniciad.
* Seleccionad el arranque mediante la tecla de BIOS necesaria (F12 o F9 u otra tecla de función en la mayoría de los PCs) y seleccionad Recalbox.
* Vuestro sistema Recalbox se lanza por la primera vez par finalizar la instalación y ya está listo. 

## III - El material y los accesorios requeridos.

Comprobad que utilizáis un disposivo de almacenamiento apropiado al igual que una buena fuente de alimentación.

| Categoría | Raspberry Pi 0/0w/1/2/3 | Raspberry Pi 4 | Odroid XU4 | Odroid Go | PC |
| :---: | :---: | :---: | :---: | :---: | :---: |
| Alimentación | µUSB 2.5A (de buena calidad) | USB-C 3A 5V | Utilizar la alimentación oficial Odroid | Batería interna | Alimentación PC standard |
| Vídeo | Cable HDMIc, HDMI2VGA ou HDMI2DVI | Cable µHDMI-HDMI | Cable HDMI | Interne | HDMI/VGA (DVI y DP seguramente) |
| Mandos | USB o Bluetooth | USB o Bluetooth | USB o Bluetooth | USB o Bluetooth | USB o Bluetooth |
| Almacenamiento | Tarjeta µSD 16GB+ clase 10 excepto Pi1 : SD | Tarjeta µSD 16GB+ | Tarjeta µSD 16GB+ | Tarjeta µSD 16GB+ | Disco duro 16GB+ |
| Configuración de almacenamiento, bios y roms | Disco duro externo con alimentación propia | Disco duro externo con alimentación propia | Disco duro externo con alimentación propia | Tarjeta µSD | Disco duro interno o disco duro externo USB (enchufado en la placa base) |

>Si necesitáis algunos de estos elementos para crear vuestra Recalbox, 
> no dudéis en visitar [nuestra tienda oline](https://www.kubii.fr/221-recalbox) gracias a Kubii !
{.is-info}

## Resolución de problemas x86 / x86_64 :

* **Vuestro ordenador sólo arranca con Windows :** Verificad que el **Secure Boot** está desactivado en las opciones de la BIOS.
	* Buscad en internet donde se encuentra la opción de Secure boot (depende del modelo y de la marca del ordenador).
  * Desactivad la opción poniéndola a Off o Disable.
  * Reiniciad el ordenador.

* **Recalbox no arranca:** Configurad vuestra **bios** en modo **Legacy.**
  * Para configurar vuestra bios en modo Legacy, al arrancar el ordenador, apretad la tecla que permite acceder a la bios `F*` (de `F1` a `F12`, depende de la marca y modelo del ordenador) o `Suppr`.
  * Buscad en internet dónde se encuentra la opción Legay de vuestro ordenador.
  * Configurad esta opción activandola con "**enabled**"
  
* **En caso de tener una configuración Multiboot**, si queréis que **Recalbox arranque** antes que los otros sistemas ya instalados :
  * Accedez al **Boot Menu** de discos duros.
  * Apretad en las teclas `F*` para accéder al **Boot Menu** de discos duros (`F1` à `F12`, dependiendo del modelo del ordenador).
  * Seleccionad como **disco duro sistema** Recalbox.