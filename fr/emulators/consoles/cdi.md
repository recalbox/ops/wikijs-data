---
title: CDi
description: Philips CDi
published: true
date: 2023-06-27T18:33:56.954Z
tags: consoles, philips, cdi, 8.1+
editor: markdown
dateCreated: 2022-02-03T16:54:52.979Z
---

![](/emulators/consoles/cdi.svg){.align-center}

## Fiche technique

* **Fabricant** : Philips, Magnavox
* **Année de sortie :** 1991
* **Système d'exploitation :** CD-RTOS (Compact Disc Real Time Operating System)
* **Processeur :** 16-bit CISC (680070) @ 15.5MHz
* **RAM :** 1 MB
* **VRAM :** 1 MB
* **ROM :** 512 KB
* **GPU :** SCC66470, MCD 212 plus tard
* **Puce sonore :** son stéréo 16 bits avec taux d'échantillonnage 44.1KHz 
* **Résolution :** 384x280 à 768x560 (16.7 millions de couleurs)

## Présentation

Le CD-i, sigle de Compact Disc Interactif, est une console de jeux vidéo et lecteur multimédia conçu par Philips, Sony et Matsushita pour imposer un nouveau standard « multimédia », concept très en vogue depuis le début de l'année 1991. Ce système multimédia permet de fournir plus de fonctionnalités qu'un lecteur de CD audio ou qu'une console de jeux vidéo, tout en restant inférieur au prix d'un ordinateur personnel. Comparé à l'ordinateur individuel, le coût est moindre par l'absence de disque dur, de lecteur de disquette, de clavier et souris, par l'usage d'un téléviseur ordinaire plutôt que d'un moniteur spécifique et par un système d'exploitation plus simple.

Le nom CD-i se réfère aussi au support, le disque CD-i, un format dérivé du Disque Compact standard. Ses spécifications sont décrites dans le Green Book. C'est un disque optique utilisé pour stocker des données sous forme numérique destinées à être lues par un lecteur compatible CD-i.

Le développement du CD-i commence en 1984, avec une première annonce publique du projet en 1986. En 1992 les premiers lecteurs sont disponibles à la vente, en septembre 1992 pour la France. Ils sont capables de jouer disques CD-i, des CD audio, des CD Karaoké, des Photo CD et des Vidéo CD, bien que ces derniers nécessitent le module Full Motion Video pour décoder le MPEG-1. Le système permet d'utiliser des jeux, mais aussi des titres éducatifs, tels que des encyclopédies interactives, des visites de musées, etc. Les disques CD-i peuvent contenir aussi bien des données brutes que de la vidéo ou du son ; mais ils ne peuvent pas être lus dans un lecteur de CD standard et ont été conçus pour être utilisés avec la télévision.

Vu par le grand public comme une console de jeu, le format CD-i s’avère être un échec commercial. La société Philips a perdu près d'un milliard de dollars américains sur l'ensemble du projet. Le dispositif est disponible à la vente jusqu'en 1998, mais l'échec du format CD-i oblige Philips à quitter l'industrie du jeu vidéo. Le CD-i reste l'une des rares consoles de jeux vidéo créées par une société européenne. Cette console est rendue célèbre malgré elle pour avoir hébergé deux des plus mauvais jeux de la franchise Zelda.

## Émulateurs

[Libretro-cdi2015](libretro-cdi2015)
[Libretro-same_cdi](libretro-same_cdi)