---
title: Nintendo Entertainment System
description: 
published: true
date: 2021-08-09T20:29:13.772Z
tags: system, consoles, nintendo, nes, entertainment
editor: markdown
dateCreated: 2021-07-28T05:29:51.593Z
---

![](/emulators/consoles/nes.svg){.align-center}

## Technical data

* **Manufacturer**: Nintendo
* **Year of release**: 1983
* **Units sold**: 61.91 millions
* **Best-selling game**: Super Mario Bros.
* **CPU**:8-bit MOS 6502 @ 1.79MHz
* **RAM**: 2kB / VRAM : 2kB
* **Video**: 8-Bit PPU (Picture Processing Unit)
* **Resolution**: 256 x 240 pixels, 25 colors (64 colors palette)
* **Sound chip**: PSG sound (2 Square Waves, 1 Triangle Wave, 1 White Noise)
* **Cart size**: 8kB - 1MB

## Presentation

The **Nintendo Entertainment System**, abbreviated **NES**, also commonly called **Nintendo** in France, is an 8-bit generation video game console manufactured by the Japanese company Nintendo and distributed from 1985 (1987 in Europe). Its Japanese equivalent is the **Family Computer** or **Famicom** released a few years earlier, in 1983. In South Korea, the NES was called **Hyundai Comboy** and in India, **Tata Famicom**.

The console was a worldwide success, which helped revitalize the video game industry after the 1983 video game crash, and set the standards for subsequent consoles, from game design to management procedures.

Super Mario Bros. was the best-selling game on the console. It was so successful that it often justified the purchase of the console on its own, becoming a killer game.

The Nintendo Entertainment System is the 13th best-selling video game system of all time with 61.91 million units sold.

## Emulators

[Libretro Nestopia](libretro-nestopia)
[Libretro FCEUmm](libretro-fceumm)
[Libretro FCEUnext](libretro-fceunext)
[Libretro Quicknes](libretro-quicknes)
[Libretro Mesen](libretro-mesen)
[Libretro FBNeo](libretro-fbneo)