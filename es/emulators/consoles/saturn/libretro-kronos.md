---
title: Libretro Kronos
description: 
published: true
date: 2024-07-12T13:05:17.947Z
tags: libretro, saturn, kronos
editor: markdown
dateCreated: 2023-09-07T20:35:26.465Z
---

**Libretro Kronos** es una adaptación del emulador **Yabause** para el sistema Libretro que emula la Sega Saturn.

## ![](/emulators/license.svg) Licencia

El core posee la siguiente licencia [**GPLv2**](https://github.com/libretro/yabause/blob/master/yabause/COPYING).

## ![](/emulators/compatibility.png) Compatibilidad

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | 
| ❌ | ❌ | ❌ | ❌ | ✅ | ❌ | ✅ | ❌ |

## ![](/emulators/features.png) Funcionalidades

| Funcionalidad | ¿Soportada? |
| :---: | :---: |
| Reinicio | ✔ |
| Capturas de pantalla | ✔ |
| Grabaciones | ✔ |
| Grabaciones instantáneas | ✔ |
| Rebobinado | ✔ |
| Opciones del core | ✔ |
| RetroAchievements | ✔ |
| Cheats RetroArch | ✔ |
| Controles | ✔ |
| Remapping | ✔ |
| Control de disco | ✔ |

## ![](/emulators/bios.svg) BIOS

### Lista de BIOS obligatorias

| Nombre del fichero | Descripción | MD5 | ¿Preinstalada? |
| :---: | :---: | :---: | :---: |
| mpr-18811-mx.ic1 | The King of Fighters '95 - Requis pour ce jeu | 255113ba943c92a54facd25a10fd780c | ❌ |
| mpr-19367-mx.ic1 | Ultraman: Hikari no Kyojin Densetsu - Requis pour ce jeu | 1cd19988d1d72a3e7caa0b73234c96b4 | ❌ |
| saturn_bios.bin | BIOS Saturn | af5828fdff51384f99b3c4926be27762 | ❌ |
| stvbios.zip | BIOS ST-V | 2b99d9dcecde22db4371580b6bb33f32 53a094ad3a188f86de4e64624fe9b3ca 17116d2aea72454096b3f80ab714bfba 2b99d9dcecde22db4371580b6bb33f32 4cb34733b741ada074f11e3abcdc56d8 6b6d422a6c392eaec178b64b92987e99 88b9da6cda268ba1d9e37dd82d038801 8c34608cc140e1bfbf9c945a12fed0e8 b876d86e244d00b336980b4e26e94132 | ❌ |

### Emplazamiento

Disponed vuestras bios como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 saturn
┃ ┃ ┃ ┃ ┣ 🗒 **mpr-18811-mx.ic1**
┃ ┃ ┃ ┃ ┣ 🗒 **mpr-19367-mx.ic1**
┃ ┃ ┃ ┃ ┣ 🗒 **saturn_bios.bin**
┃ ┃ ┃ ┃ ┣ 🗒 **stvbios.zip**

## ![](/emulators/isos.svg) Isos

### Extensiones de roms soportadas

Las isos deben tener las extensiones siguientes:

* .bin/.cue
* .m3u
* .img/.ccd
* .chd

### Emplazamiento

Disponed vuestras isos como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 saturn
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.bin**
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.cue**

>Se aconsejan las isos del proyecto **Redump**.
{.is-success}

>Para más información sobre las isos, consultad [este tutorial](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

>Para conservar las configuraciones personalizadas cuando se produce una actualización del sistema Recalbox, os aconsejamos utilizar las [Sobrecargas de configuración](./../../../advanced-usage/configuration-override).
{.is-info}

### Acceder a las opciones

Podéis configurar las opciones del core de dos formas diferentes.

* Via el Menú RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via el fichero `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Opciones del core

| Opción | Valor de la opción | Variable (para la sobrecarga) | Valores posibles |
| --- | --- | --- | --- |
| Force HLE BIOS (restart) | `Désactivé` ✅ / `Activé` | `kronos_force_hle_bios` | `disabled` / `enabled` |
| Video format | `auto` ✅ / `NTSC` / `PAL` | `kronos_videoformattype` | `auto` / `NTSC` / `PAL` |
| Frameskip | `No skipping` ✅ / `Skip rendering of 1 frames out of 2` / `Skip rendering of 2 frames out of 3` / `Skip rendering of 3 frames out of 4` / `Skip rendering of 4 frames out of 5` / `Skip rendering of 5 frames out of 6` | `kronos_skipframe` | `0` / `1` / `2` / `3` / `4` / `5` |
| SH-2 cache support (experimental) | `Désactivé` ✅ / `Activé` | `kronos_usercache` | `disabled` / `enabled` |
| Share saves with beetle | `Désactivé` ✅ / `Activé` | `kronos_use_beetle_saves` | `disabled` / `enabled` |
| Addon Cartridge (restart) | `None` / `1M_extended_ram` / `4M_extended_ram` / `16M_extended_ram` / `512K_backup_ram` ✅ / `1M_backup_ram` / `2M_backup_ram` / `4M_backup_ram` | `kronos_addon_cartridge` | `None` / `1M_extended_ram` / `4M_extended_ram` / `16M_extended_ram` / `512K_backup_ram` / `1M_backup_ram` / `2M_backup_ram` / `4M_backup_ram` |
| 6Player Adaptor on Port 1 | `Désactivé` ✅ / `Activé` | `kronos_multitap_port1` | `disabled` / `enabled` |
| 6Player Adaptor on Port 2 | `Désactivé` ✅ / `Activé` | `kronos_multitap_port2` | `disabled` / `enabled` |
| Resolution | `original` ✅ / `480p` / `720p` / `1080p` / `4k` / `8k` | `kronos_resolution_mode` | `original` / `480p` / `720p` / `1080p` / `4k` / `8k` |
| Output to original resolution | `Désactivé` ✅ / `Activé` | `kronos_force_downsampling` | `disabled` / `enabled` |
| Improved mesh | `Désactivé` ✅ / `Activé` | `kronos_meshmode` | `disabled` / `enabled` |
| Improved banding | `Désactivé` ✅ / `Activé` | `kronos_bandingmode` | `disabled` / `enabled` |
| Wireframe mode | `Désactivé` ✅ / `Activé` | `kronos_wireframe_mode` | `disabled` / `enabled` |
| ST-V Service/Test Buttons | `Désactivé` ✅ / `Activé` | `kronos_service_enabled` | `disabled` / `enabled` |
| ST-V Favorite Region | `EU` ✅ / `US` / `JP` / `TW` | `kronos_stv_favorite_region` | `EU` / `US` / `JP` / `TW` |
| Bios Language | `English` ✅ / `German` / `French` / `Spanish` / `Italian` / `Japanese` | `kronos_language_id` | `English` / `German` / `French` / `Spanish` / `Italian` / `Japanese` |

## ![](/emulators/external-links.png) Enlaces externos

* **Código fuente utilizado** : [https://github.com/FCare/Kronos/](https://github.com/FCare/Kronos/)
* **Documentación Libretro** : [https://docs.libretro.com/library/kronos/](https://docs.libretro.com/library/kronos/)