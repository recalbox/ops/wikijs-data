---
title: DOSbox to play with DOS games
description: 
published: true
date: 2021-08-08T13:53:36.698Z
tags: dos, msdos, dosbox
editor: markdown
dateCreated: 2021-08-08T06:50:15.923Z
---

## Basic operation

Let's take a simple example with the Alley Cat game:

* Start by creating the directory for your game by naming it `AlleyCat.pc`. The first part of the directory name before the '.' must be at most 8 characters long and must not contain any special characters;
* Copy your game files into it (for large games, install them on your computer before copying them)
* Inside `AlleyCat.pc`, create a `dosbox.bat` file and modify it to call the game executable.

This gives us:

```dos
c: 
CAT.EXE
```

* The C: disk is positioned by DOSBox on the directory of your game (here C: is equivalent to `/recalbox/share/roms/dos/AlleyCat.pc`). The same goes for '.
* Reboot or refresh your game list to see the game.
* To exit the emulator, press `CTRL + F9` on your keyboard.

## Advanced operation

Adding a `dosbox.cfg' file to your game directory at the same level as `dosbox.bat' allows you to define a specific DOS configuration for that game.

You can either copy the `dosbox.conf` file from `recalbox\share\system\configs\dosbox\dosbox.conf` or use [this](https://pastebin.com/13xrJdkw).

>The extension in the game directory must be `cfg` and not `conf`!
{.is-warning}

In this file, the line `mapperfile=mapper.map` allows you to use a `mapper.map` file to map any keyboard key or even the mouse to your controller. This file can be created from within your game by pressing `CTRL + F1` at any time and will then be saved alongside your `dosbox.cfg` and `dosbox.bat` files.

Some of the parameters in the `dosbox.cfg` file can also be put at the beginning of your `dosbox.bat` file if you don't want to use a specific `dosbox.cfg` file, but some of these parameters won't work in the `dosbox.bat`; everything graphical seems to work, but the `mapperfile` parameter doesn't work for example.

## Convert a game to use it on Recalbox

How to convert a game to use it on Recalbox ?

First of all, copy the content of the game directory (rename it correctly with .pc).

Then we have to adapt the content of the `dosbox.cfg` and the bat file used to launch the game.

Example with the game Wacky Wheels:

Do not copy the `dosbox.cfg` (or .conf) directly from the original directory as this can lead to bugs that are difficult to find. Instead, copy the standard file linked above, then check if the `dosbox.cfg` had a special configuration and if so, use it in your `dosbox.cfg`. This is in the `[autoexec]` part of the file.

First move the contents of this part to the beginning of your `dosbox.bat` file and adapt the paths. Here we have :

```bat
[autoexec] 
cd .. 
cd .. 
mount c .\games\WackyWhe  
imgmount d .\games\WackyWhe\cd\wackywheels.iso -t cdrom 
c: 
cd wacky
cls 
@ww
exit 
```

and this will give us once converted to Recalbox:

```dos
imgmount d .\cd\wackyw~1.iso -t cdrom
c: 
cd WACKY
pause
WW.EXE
```

### Explanations :

* C: is already mounted by DOSBox (in the game directory), we don't need it ;
* '.' is also positioned by DOSBox on the same directory and by using relative paths, your game will not be hard configured on a directory (you will be able to use it in a different subdirectory or with another distribution) ;
* exit is removed because this is handled by DOSBox too;
* The path used by `imgmount` is simplified and we have to convert the iso name to a standard DOS name (8 characters maximum with the last two changed to ~1, ~2, ~3 etc. if the path is longer and if you have several files with the same name). In this case it is better to change the file name to something simple anyway;
* Long names are not supported in `.cue` files either, so you may have to rename the `.vue`/`.bin` files manually and edit the `.cue` file to reference the new `.bin` file name. And DOSBox can be rather strict: no special characters, and sometimes no capital letters.
* The pause command allows you to freeze the screen and easily debug DOS instructions, you can remove it once you have verified that the `imgmount` command works (don't use pause after calling the game executable or you won't see anything)
* `@ww` becomes `WW.EXE` (this is just the call to the game executable).

And that's it! Some games may also use a `.bat` launcher, copy its instructions to the end of your `dosbox.bat` after the `[autoexec]` part of `dosbox.cfg` and adapt them.

## How to emulate DOS games in Recalbox?

This wiki aims to allow you to play DOS games via the DOSBox emulator made available by RecalBox.

>You have to know that there is a multitude of possible contexts, each game has its specificity.  
>This is why it is impossible, at the moment, to have something "Plug&Play" on the DOS emulation.
>However, in general terms and for the majority of games, it's more or less the same thing.
{.is-info}

We will see 3 cases here: 1. the game requiring neither installation nor CD support; 2. the game requiring an installation, but no CD support; 3. the game requiring an installation and a CD support.

But before that, some generalities about the functioning of the DOSBox emulator made available by Recalbox.

## Generalities

* Your game files/folders will be placed in the "dos" directory of the "roms" directory; you must not place a zip/rar there but the game files/folders after extraction of a possible archive; 
* to name your game directories in the "dos" directory, two important rules :
  * suffix the directory name with ".pc", this allows EmulationStation to locate the directory and list it in the interface;
  * to keep it simple, avoid spaces (" ") or special/emphasized characters in the names as much as possible, it will simplify access; 
* the game directory (the one suffixed with ".pc") is automatically "mounted" as "C:" upon launching from EmulationStation; thus, you should keep in mind that your context is limited to the running game directory; the rest does not exist for DOSBox; 
* inside the game directory (the one suffixed with ".pc") we must place a "dosbox.bat" file; this file will allow us to execute the commands necessary to install/configure the game and to launch it; when setting up a game, we will generally have to edit the "dosbox. bat" several times successively (launching the game at each edition) in order to complete the different steps necessary for its operation; but this has to be done only once, once the game is installed/configured and running, you don't need to touch it anymore; last point about the "dosbox.bat" files: you can edit them with basic text editors (like notepad under Windows);
* the files that will be important to us in a general way are those whose extension is ".bat" in priority and ".exe" otherwise; 
* a keyboard/mouse is, if not essential, strongly recommended (in this documentation we assume that you have one).

**The different cases of figure **

We will explain below the different scenarios. Do not read only the case that interests you, but the whole: some points acquired in the first cases will not be repeated in the following ones.

**The game does not require installation or CD support:**

The simplest of all cases: you have a disk version of the game, which does not have an "install.bat" (or ".exe") file or which, failing that, does not require installation (the "install.bat" file is sometimes also used to configure the game, especially the sound).  
  
Ideally, you have two key files in the game directory (either ".bat" or ".exe"): "setup" (sometimes the "install" file is used as "setup") and another one with either the name of the game (for example "wolf3d" for "Wolfenstein 3D" or "s2" for "settlers2" or simply "go" meaning "launch")

So create the file "dosbox.bat" if you haven't already done so, then edit it so that it contains the following lines:

```bat
setup
pause
```

The `pause` instruction will allow us to ask DOSBox to wait before "closing its session" and thus to return to EmulationStation.
This will allow us to read the possible errors returned by the execution of such or such command.
  
Once the "dosbox.bat" file is edited and saved, return to EmulationStation, update the game list (if it hasn't already been done once since the dosbox.bat file was created), and start the game!
  
You arrive in the game configuration program. This one is especially interesting to configure the sound.
Each "setup" is different but in general you are offered a menu in English with "setup sound...".
You have to select (for the "midi" AND for the "sound effect" if the difference is made) the "Sound Blaster 100% compatible" input and the automatic setting (or failing that, all the default choices that you will be offered).
  
Sometimes, a test is proposed to you which will allow you to see that everything is OK.  
Once this is done, you can quit the setup (there is an entry in the setup for this purpose, remember to save the changes if you are asked).
You will then get a "prompt" asking you to press a key to skip the pause instruction: execute it after having noticed a possible error message.

After returning to the EmulationStation interface, edit the "dosbox.bat" file as follows (here we take the example of the executable file mentioned above "s2.bat" for "Settlers 2", replace "s2" by the name of the ".bat" or ".exe" file corresponding to the game being processed):

```bat
s2
pause
```

Saved, then launched the game again. And normally everything works ;-)  
If it's not the case, expose your problem on the forum or Discord, by specifying the error messages eventually caught.

## The game requiring an installation, but no CD support

The principle is the same as in the previous case, but with the installation step in addition.
If you were not satisfied with the previous case, it is probably because the game needs to be installed.
In order to avoid any file confusion, I advise you to create a "source" subdirectory in the ".pc" type directory and to place the game files/folders in it.
Make sure you have an "install" file (either ".bat" or ".exe" - sometimes the "setup" file also offers the installation option).
Then create/edit the "dosbox.bat" file as follows:

```bat
cd source
install
pause
```

Launch the game again from EmulationStation, this execution leads you to the installation program.
Sometimes similar to the presentation of the classic "setup", it has an additional option offering installation.
Just like the "setup", there is nothing universal or standard, so you have to adapt.
But the titles, although often in English, are generally clear.  
During the installation process, you will be asked for a path.

Make sure it is filled in as follows:

```bat
c:\target
```

The name "target" is an example (it must echo the name "source" previously used), you can replace it by what you wish, but in this documentation we will start on the principle that it is called "target".
Once the installation is finished, proceed to the sound configuration if it is proposed to you. Then, quit the installation program.
You arrive on the prompt caused by "pause", again this allows you to note any errors returned.
Press any key to return to EmulationStation.

Then, the process is similar to the previous one, except that you need to add access to the "target" directory as well.

So to configure the sound, if it hasn't already been done and with the example of a file named "setup", your "dosbox.bat" file should look like this:

```bat
cd target
setup
pause
```

Launch the input again, and proceed as described in the previous case.

Then, once all installation/configuration is OK, you can proceed to launch the game itself.
Your "dosbox.bat" file should then contain the following lines:

```bat
cd target
s2
pause
```

And, normally, the trick is done ;-).

>The "source" directory is sometimes not useful anymore after installation, this kind of installation being made at the time to simply copy the content of the floppy disk to the hard disk to do without the removable media. To be tested.
{.is-warning}

## The game requiring an installation and a CD support

This last case has many points in common with the previous one, with two major differences:

* it will be necessary to "mount" the CD on a virtual CD drive (don't panic, in one command line it will be done ;-) );
* as a general rule it will be necessary to keep the CD mount in order to run the game or, at least, to benefit from additional "in game" content such as cinematics and/or music.

So, first of all, we will create a "source_cd" directory at the root of our ".pc" directory. This directory will receive the CD image files (bin, cue, iso, img, ...).

Compared to the previous case (a game on CD media still requires an installation), the major difference is the mounting of the CD image on a virtual CD drive. The command line used for this will vary slightly depending on the format of the CD image. Some examples (in the back, names longer than 6 or 7 characters - I have a doubt - are truncated with "~x"):

* bin/cue format: `imgmount D "C:\source~1\settle~1.cue" -t iso -fs iso`
* iso format: `imgmount D "C:\source~1\settle~1.iso" -t iso -fs iso`
* img format: `imgmount D "C:\source~1\settle~1.img"`
* direct file/folder format (here, mounting is not essential, except to take advantage of possible additional contents): `mount D "C:\source~1"`

Some game CDs are "mixed" CDs. This means that they contain the game data as well as the audio tracks that will be used to play the music in the game.  
It is important for these CDs that the image is contained in a single .img or .bin file because DOSBOX does not support CD images split into several .bin files.  
In this case, the editing must be done via the corresponding .cue file so that all the audio tracks are correctly loaded.

With the exception of this mount line, the rest is almost identical to the previous case.
You just have to add the line "mount ..." at the beginning of the file "dosbox.bat" in a systematic way then proceed to the installation, the configuration, then the launching of the game.
Be careful also to move to the right directory (command "cd") : thus you have to do `cd source_cd` for the installation, then `cd target` to access the game directory (as in the previous case).

>Once a game is running, you can remove the "pause" instruction to avoid the timeout prompt each time the game is exited.
{.is-info}

## How to use the virtual keyboard?

By default, the virtual keyboard is opened with Ctrl-F2.  
But a new button is available in the mapper (Ctrl-F1 &gt; _Virt Keyb_) to associate it with another event (keyboard, mouse or joystick).

Once opened, use the mouse or joystick to select and operate the keys:

* Using the left mouse button or the first joystick button simulates a simple press-release of the key.
* Using the right mouse button or the second joystick button simulates a sustained key press (the key becomes darker).

Click again with any mouse or joystick button to release it.

This allows combinations with Shift, Ctrl or Alt.

Finally, press the green V button to close the virtual keyboard and send the events to DOSBox. It is also possible to press the red cross to cancel the events and close the virtual keyboard.

>* If you are using an existing mapping file, the default Ctrl-F2 combination will not be configured.
>* If you quit the virtual keyboard with a keystroke, this event will be constantly sent to DOSBox, until you open the virtual keyboard again! 
>* All the configured mappings of the joystick or the mouse will be ignored as long as the virtual keyboard is open
{.is-warning}

## Troubleshooting (in `dosbox.cfg`)

**The image is distorted**: For some older games, you may have to change the `aspect=false` parameter to `aspect=true` to get a 4/3 ratio image. Be careful on newer games, this could lead to a performance problem;

**The joystick moves by itself**: Try changing `timed=false` to `timed=true`. This may be due to the _deadzone_ of the joysticks as well and unfortunately there is no way to configure this in DOSBox at the moment;

**You can't map the D-pad of an xbox360 controller** : Yes, the DOSBox mapper allows you to configure the D-PAD but it doesn't work in game, maybe you can try to change the joystick type;

**How to configure the deadzone of a joystick** : Unfortunately, this is not possible in DOSBox for the moment;

**CD Not Found / CD Driver not present :** You have problems renaming your cd files (io, cue, bin, cue edition) or using names that do not respect the DOS standard (see adv. rundown) ;

**I can't get `mount to` or `mount from` to work: Unlike `imgmount`, the `mount` command cannot use 'virtual' paths from inside the DOSBox machine. For `mount` to work, you can only use real paths local to the Recalbox file system. As '.' is set to the root / directory when DOSBox is launched, it is mandatory to use the full absolute path to the file or directory;

**The mapper is buggy, it deletes my file or mixes my buttons** : You are probably victim of the `buttonwrap` parameter. When this is set to `true`, it sets the buttons on your pad to an ID greater than the number of buttons on the emulated joystick, starting from scratch (5 will be remapped to 0, 6 to 1, etc.). Set this parameter to `false` and everything should be fine.