---
title: Out Run
description: 
published: true
date: 2021-09-16T04:50:13.375Z
tags: ports, out run
editor: markdown
dateCreated: 2021-06-29T18:47:03.393Z
---

![](/emulators/ports/outrun.gif){.align-center}

## Technical data

* **Developer**: Chris White
* **Year of release**: 2014

## Presentation

**Out Run**, also spelled **OutRun** or **Outrun**, is a car racing arcade video game designed by Yū Suzuki and Sega-AM2 and released by Sega in 1986 on arcade cabinets. It was ported to many other media.

The game was a big hit among arcade fans, so much so that it is often considered one of the best arcade racing games. It is notable for its innovative hardware (including a mobile gaming booth on a hydraulic system), innovative graphics and music, the ability to choose the soundtrack and course, and its strong theme of luxury and relaxation.

In retrospective interviews at the game, Yū Suzuki classified Out Run not as a racing game, but as a "driving" game, although he admitted to being partly inspired by the 1981 film The Cannonball Crew.

## Emulators

[Libretro Cannonball](libretro-cannonball)