---
title: Odroid
description: 
published: true
date: 2022-04-04T12:20:21.836Z
tags: odroid, compatibilité
editor: markdown
dateCreated: 2021-05-21T07:51:12.996Z
---

## Odroid XU4

![](/compatibility/xu4-exynos-8-coeurs.jpg)

ODROID-XU4 est une nouvelle génération d'appareil informatique avec un matériel plus puissant et plus économe en énergie et un facteur de forme plus petit.
N'implémentant les interfaces eMMC 5.0, USB 3.0 et Gigabit Ethernet, l'ODROID-XU4 offre des vitesses de transfert de données incroyables, une fonction de plus en plus requise pour prendre en charge la puissance de traitement avancée des périphériques ARM.
Cela permet aux utilisateurs de vraiment faire l'expérience d'une mise à niveau en informatique, en particulier avec un démarrage plus rapide, la navigation Web, la mise en réseau et les jeux 3D.

### Caractéristiques :

* **Processeur :** 
  * Exynos 5422 (ARM Cortex-A15 Quad-Core cadencé à 2.0 GHz / ARM Cortex-A7 Quad-Core cadencé à 1.4 GHz)
* **GPU :**
  * Accélérateur 3D Mali™ -T628 MP6 (OpenGL ES 3.1 / 3.0 / 2.0 / 1.1 et OpenCL 1.2 Full profile)
* **RAM :**
  * 2Go LPDDR3 RAM PoP (750Mhz, bande passante mémoire 12GB / s, bus 2x32 bits)
* **Connectique disponible:** 
  * 1 X RJ45 Femelle
  * 1 X USB 2.0
  * 2 X USB 3.0
  * 1 X HDMI
  * Fente pour carte MicroSD (jusqu'à 128 Go)
* **Connecteur(s) additionnels :** 
  * Stockage (Option) Prise du module eMMC: eMMC 5.0 Stockage Flash (jusqu'à 64 Go)
  * Interface HDD / SSD SATA (en option) Adaptateur SuperSpeed USB (USB 3.0) à Serial ATA3 pour stockage HDD et SSD 2.5 "/ 3.5"Réseau
  * Carte son USB en option
  * WiFi (Option) : USB IEEE 802.11 ac / b / g / n 1T1R WLAN avec antenne (adaptateur USB externe)
* **Réseau :**
  * Ethernet LAN 10/100 / 1000Mbps Ethernet rapide avec prise RJ-45 (prise en charge Auto-MDIX)
* **Alimentation :**
  * 4 ,8Volt ~ 5,2Volt (alimentation 5V / 4A recommandé)
* **Dimension :**
  * 83 x 58 x 20 mm (poids: 38 gr) sans refroidisseur env.

## Odroid XU4Q

![](/compatibility/xu4q.jpg)

Que vous souhaitiez réaliser un média center, une console de jeux, un système de surveillance embarquée ou tout autre chose, la carte mère ultra-compacte **Odroid XU4Q** sera un choix idéal. Embarquant un processeur Samsung Exynos 5422 Octo-Core cadencé 2.0 GHz et 2 Go de RAM, elle est une solution très abordable.
 Un radiateur est déja monté sur la carte. Il permet de refroidir le cpu sans ventilateur.

### Caractéristiques :

* **Processeur :**
  * Samsung Exynos 5422 (ARM Cortex-A15 Quad-Core cadencé à 2.0 GHz / ARM Cortex-A7 Quad-Core cadencé à 1.4 GHz)
* **Nombre de core du processeur :**
  * 8
* **GPU :**
  * Mali T628 MP6 OpenGL ES 3.1 / 3.0 / 2.0 / 1.1 et OpenCL 1.2
* **RAM :**
  * 2 Go LPDDR3
* **Connectique disponibles :** 
  * 1 X RJ45 Femelle
  * 1 X USB 2.0
  * 2 X USB 3.0
  * 1 X HDMI Femelle
  * Carte microSD, micro SDHC, micro SDXC (compatible)
* **Connecteur(s) additionnels :** 
  * 1 X GPIO 12 pins (header)
  * 1 X GPIO 30 pins (header)
* **Réseau :**
  * Ethernet LAN 10/100/1000 Mbps
* **Alimentation :**
  * 5V / 4A (_transformateur non fourni_)
* **Dimensions :**
  * 83 x 58 x 20 mm
* **Poids :**
  * 38 g

## Odroid Go Advance 2

![](/compatibility/odroid_go_advance.png)

Une console de jeux vidéo portable D.I.Y dédiée à l'émulation et au rétrogaming, inspirée du look de la Gameboy Advance et disponible en 2 coloris  
Conçue sous la forme d'un kit d'assemblage complet et sans-soudures, proposant un montage ludique et amusant, facilement accessible à tousCompatible avec les systèmes de jeux récents (Playstation, PSP, ..) et les émulateurs type Recalbox

Haut-parleurs et Jack 3,5mm, Réglage de la luminosité, Mode de mise en veille de la console, Mode "faible consommation" pour plus de 7 heures d'autonomie, ...

* **Processeur :**
  * RockChip RK3326 (Quad-Core ARM Cortex A35 1,3 GHz)
* **GPU :**
  * Mali-G31 Dvalin
* **Mémoire :**
  * 1 Go (DDR3L 786 Mhz, largeur de bus 32 bits)
* **Écran :**
  * LCD TFT 3,5 pouces 320 x 480 (ILI9488, interface MIPI)
* **Batterie :**
  * Li-Polymer 3.7V / 3000mAh, jusqu'à 9 heures de temps de jeu continu
* **Boutons d'entrée :**
  * F1, F2, F3, F4, F5, F6, A, B, X, Y, pavé directionnel, épaule gauche, épaule droite, épaule gauche 2, épaule droite 2, joystick analogique
* **Connectique :**
  * 2.0 Hôte USB 2.0 x 1
  * port 10 broches (I2C, GPIO, IRQ à 3,3 volts)
* **Prise casque :**
  * Jack, 0.5Watt 8&#x3A9; Mono
* **Stockage :**
  * Flash SPI (démarrage 16 Mo), emplacement pour carte Micro SD (interface compatible UHS-1)
* **Alimentation :**
  * Entrée 5V, connecteur d'alimentation USB-C (Un câble de chargement USB-C est inclus dans l'emballage chez Kubii)
* **Wi-Fi :**
  * 2.4Ghz 802.11b / g / n intégré (ESP-WROOM-S2)
* **Consommation :**
  * Émulation de jeu : 300 ~ 550mA (dépend de la luminosité rétro-éclairée et du type d'émulations de jeu et de l'utilisation sans fil)
  * Mode de mise hors tension: < 1mA
  * Si vous désactivez le WiFi, la consommation d'énergie sera la même que la précédente révision 1.0.
* **Temps de charge :**
  * 2,5 ~ 3 heures lorsque l'appareil s'éteint.
  * Lorsque vous jouez au jeu, cela prend 4 à 5 heures.
* **Dimensions :**
  * 155x72x20 mm (6,1 x 2,8 x 0,8 pouces), poids: 180 g (6 oz)
* **Poids :**
  * 349 grammes

## Odroid Go Super

![](/compatibility/odroid-go-super.png)

Avec un format proche de celui de la Switch Lite, la nouvelle Odroid Go Super tente de venir conquérir le cœur des amateurs de rétrogaming ! Reprenant beaucoup d'éléments de sa prédécesseur, l’Odroid Go Advance , cette mouture marque toutefois une évolution importante au sein de la gamme de console portable de HardKernel.

* **Processeur** :
  * RockChip RK3326 (Quad-Core ARM 64bit Cortex-A35 1,3 GHz)
* **GPU** :
  * Mali-G31 MP2 OS Ubuntu 18.04. 19.10 et 20.04 sur Kernel 4.4 Aarch64
* **Mémoire** :
  * 1 Go (DDR3L 786 Mhz, largeur de bus 32 bits)
* **Espace de rangement** :
  * Flash SPI (démarrage 16 Mo), emplacement pour carte Micro SD (interface compatible UHS-1)
* **Affichage** :
  * 5 pouces 854 × 480 TFT LCD (affichage grand angle de vision, interface MIPI-DSI)
* **Audio** :
  * Prise stéréo pour écouteurs, haut-parleur mono 0,5 W 8Ω
* **Batterie** :
  * Li-Polymer 3,7 V / 4000 mAh (76,5 × 54,5 × 7,5 mm (L * W * T)), jusqu'à 10 heures de temps de jeu continu
* **Alimentation** :
  * Prise DC Entrée 5 V, connecteur d'alimentation USB-C.
* **Courant de dessin maximum** :
  * 1.5A
* **Entrées / Sorties externes** :
  * Hôte USB 2.0 x 1, port 10 broches (I2C, GPIO, IRQ à 3,3 volts)
* **Boutons d'entrée** :
  * F1, F2, F3, F4, F5, F6, A, B, X, Y, Manette de direction, Épaule gauche, Épaule droite, Épaule gauche 2, Épaule droite 2, Joystick analogique, Joystick analogique 2
* **Sans fil** :
  * Adaptateur WiFi USB en option
* **Consommation d'énergie** :
  * Émulation de jeu: 350 ~ 600mA (dépend de la luminosité rétroéclairée et du type d'émulations de jeu et de l'utilisation sans fil),
* **Mode de mise hors tension** :
  * <1mA
* **Temps de charge** :
  * 3,4 ~ 4 heures lorsque l'appareil s'éteint. Lorsque vous jouez au jeu, cela prend 4,5 à 5,5 heures.
* **Dimensions** :
  * 204x86x25 mm
* **Poids** :
  * 280 g