---
title: 🛠️ Personalización del Frontend
description: 
published: true
date: 2024-07-26T07:23:21.964Z
tags: tutorial, frontend, personalización, adaptación
editor: markdown
dateCreated: 2023-09-01T09:37:41.721Z
---

Aquí, podéis personalizar EmulationStation utilizando nuevos themes, otras músicas, etc.

Lista de tutoriales disponibles:

[Ajustar la imagen (Pixel Perfect)](pixel-perfect)
[Añadir músicas personalizadas a interfaz](add-custom-music-into-emulationstation)
[Añadir nuevos themes a interfaz](add-themes-into-emulationstation)