---
title: L'interface
description: 
published: true
date: 2024-11-17T15:58:16.385Z
tags: frontend
editor: markdown
dateCreated: 2021-05-21T07:54:26.787Z
---

## Présentation

Lorsque vous allumez votre Recalbox, vous accédez à l'interface du logiciel. Celle-ci vous permet de lancer vos jeux, de régler certaines options ou de mettre à jour votre Recalbox.

### Liste des systèmes

Le premier écran est celui de la liste des systèmes. Il liste les consoles et systèmes disponibles.

![](/basic-usage/getting-started/es1.png){.full-width}

Lorsque vous sélectionnez un système, l'écran change et vous accédez à votre liste de jeux disponibles.

### Liste des jeux

Une liste des jeux est disponible pour le système sélectionné à l'écran précédent.

Une fois le jeu lancé, reportez-vous à la section [Pendant le jeu](./../../basic-usage/getting-started/special-commands/in-game) pour connaître les options disponibles.

## Options dans l'interface

En appuyant sur le bouton `START`, vous serez en mesure de modifier certains réglages de votre Recalbox.

![](/basic-usage/getting-started/es2.png){.full-width}

### Kodi Media Center

* Permet de démarrer **Kodi Media Center** (anciennement connu sous le nom de `XBMC`).

Vous pouvez démarrer Kodi depuis l'écran d'accueil en pressant le bouton `X` de votre manette.

Pour quitter Kodi, sélectionnez `QUIT` dans le programme, et vous serez de retour dans l'interface.

Les manettes sont supportées dans Kodi. Mais si vous préférez, vous pouvez également utiliser le HDMI-CEC (permet d'utiliser la télécommande de votre TV pour naviguer dans Kodi) ou une application smartphone de contrôle à distance.

Pour plus d'informations sur Kodi, vous pouvez consulter ses tutoriels :

[Kodi Media Center](./../features/kodi-media-center)
[Utiliser une télécommande infrarouge](./../features/kodi-media-center/remote-control-usage)

### Réglages système

Vous accéderez aux informations de votre Recalbox.

* **Version de Recalbox** : la version de Recalbox actuellement installée. 
* **Utilisation du disque** : espace de stockage utilisé/disponible.  
* **Média de stockage** : liste les médias de stockage disponible :
  * Partition share interne (1er disque dur utilisé par le système pour le premier boot).
  * N'importe quel stockage externe (tous les disques durs et autre clés USB détectés).
* **Langue** : la sélection de la langue des menus. 
* **Clavier** : le type de clavier que vous voulez utiliser.

### Mise à jour

Ce menu propose d'activer ou non les mises à jour et de choisir si on veut installer uniquement une version stable ou aussi les versions bêta de Recalbox.

* **Vérifier les mises à jour** : permet de de,ander à vérifier si une mise à jor existe.
* **Mise à jour disponible** : permet de vous indiquer si une mise à jour est disponible.
* **Nouveautés de la mise à jour** : permet de prendre connaissance du contenu de la mise à jour.
* **Lancer la mise à jour** : permet de lancer le processus de mise à jour.

### Options des jeux

Ce menu propose les réglages suivants :

* **Format jeux** :
  * Auto
  * Configuration RetroArch
  * Fourni par le core
  * Ne pas régler
  * Pixel carré
  * Spécifique RetroArch
  * 1/1
  * 16/10
  * 16/15
  * 16/9
  * 19/12
  * 19/14
  * 2/1
  * 21/9
  * 3/2
  * 3/4
  * 30/17
  * 32/9
  * 4/1
  * 4/3
  * 5/4
  * 6/5
  * 7/9
  * 8/3
  * 8/7
  * 9/16 
* **Lisser les jeux** : permet de donner un petit effet de flou aux pixels pour que se soit plus joli sur nos TV modernes. 
* **Rembobinage** : vous autorise à effectuer des retours dans le temps lors de votre partie.

>Cela peut ralentir certains émulateurs (PS1, Dreamcast, ...) si vous l'activez par défaut sur ces émulateurs.  
>Vous pouvez l'activer seulement pour certains émulateurs via le menu [Paramètres avancés](#parametres-avances). 
{.is-warning}

* **Softpatching** : permet d'appliquer un à plusieurs patchs sur le jeu souhaité. Pour plus d'informations, vous pouvez consulter sa [page dédiée](../../features/softpatching).
* **Afficher les saves states au lancement du jeu** : permet de reprendre la partie là où vous avez crée une save state.
* **Sauvegarde/Chargement auto** : permet de reprendre une partie là où on l'avait laissé en quittant le jeu. 
* **Appuyer deux fois pour quitter le jeu** : permet de confirmer que l'on veux bien quitter le jeu en cours.
* **Ajuster l'échelle (pixel parfait)** : affiche les jeux dans leur résolution d'origine.
* **Shaders prédéfinis** : vous pouvez configurer facilement les _shaders_ pour les différents systèmes :
  * Les shaders **scanlines** activent les scanlines sur tous les systèmes afin d'obtenir un rendu proche d'un écran CRT. 
  * Les shaders **retro** sont un pack de shaders, correspondant au meilleur shader à appliquer à chaque système. Les shaders composant ce pack, sont choisis par la communauté et vous apporteront l'expérience de jeux la plus proche de l'expérience originale pour chaque système !
  Pour plus d'informations, vous pouvez consulter le tutoriel [Configuration des shaders](./../../tutorials/games/generalities/shaders-configuration).

>Vous pouvez aussi changer de _shader_ pendant le jeu, en utilisant votre manette. Utilisez les [Commandes spéciales](./special-commands/in-game) `Hotkey` + `R2` ou `Hotkey` + ` L2` pour voir s'afficher le shader suivant ou précédent.
{.is-info}

* **Shaders avancés** : vous pouvez appliquer un filtre sur l'image de votre jeu afin de lui donner un effet particulier comme une image plus rétro.
* **Mode HD** : 
* **Mode écran large (16/9)** : 
* **Activer le pilote Vulkan** : permet d'activer le pilote graphique Vulkan. Ceci est compatible uniquement pour l'Atomiswave, Dreamcast, Naomi, Naomi 2 (PC uniquement), Naomi GD et PSP (libretro-ppsspp).
* **Mode Game Boy** : permet de lancer les jeux du système Game Boy avec le mode Super Game Boy.
* **Options de Retroachievements** : permet de paramétrer les Retroachievements (équivalent des succès/trophées mais pour les anciennes consoles). 
* **Options NetPlay** : permet de paramétrer le NetPlay (jeu en ligne).
* **Cacher les jeux pré-installés** : permet de cacher les jeux pré-installés de votre système.

### Téléchargement de contenus

Ceci vous permet de télécharger des jeux 100% légaux pour certains systèmes :

* TIC-80
* Uzebox
* Vectrex
* WASM-4

### Réglages manettes

C'est ici que vous pouvez configurer vos manettes.

* **Configurer une manette** : permet de définir les boutons d'une de vos manettes.
* **Associer des manette Bluetooth** : permet d'associer une manette sans fil à votre Recalbox.
* **Oublier les manettes Bluetooth** : permet de désassocier toutes les manettes sans fil.
* **Activer le jumelage au démarrage** : permet de pouvoir associer une manette au démarrage si vous en avez aucune configurée.
* **Pilote** : permet de définir le pilote à utiliser avec votre manette. Vous n'avez pas à y toucher tant que votre manette fonctionne.
* **Toujours associer l'OSD des manettes** : permet d'afficher une petite icône de manette en haut à gauche de votre écran, ceci indique quelle manette est en quelle position quand un appui est effectué sur la manette. Sur certaines manettes, le niveau de batterie peut aussi être visible.
* **Type de l'OSD des manettes** : permet de choisir le visual des manettes.

### Options de l'interface

Ce menu propose l'accès aux réglages de l'interface.

* **Économiseur d'écran** : permet de modifier l'économiseur d'écran avec différentes options.
* **Thème** : choix des différents thèmes créés par la communauté (ces nouveaux thèmes ne sont pas disponibles par défaut avec Recalbox).
* **Configuration du thème** : certains des thèmes installés ont des options de configuration.
* **Tri des systèmes** : permet de changer le tri des systèmes par défaut.
* **Sélection rapide du système** : permet de basculer d'un système à l'autre par les touches droite ou gauche dans les listes de jeux.
* **Affichage de l'aide** : permet d'afficher une popup d'aide en haut à droite de votre écran pour chacune des options en appuyant sur le bouton `Y`.
* **Inverser les boutons Valider/Annuler** : inverse les boutons valider et annuler de vos manettes.
* **Horloge dans le menu** : permet d'afficher l'heure actuelle dans les menus de l'interface.
* **Réglage des popups** : permet de modifier les options des popups d'aide de musique et du NetPlay.

>Les popups de mise à jour ne sont pas configurables.
{.is-info}

* **Filtres des jeux** : permet d'appliquer certains filtres pour certaines actions :
  * **Afficher uniquement la dernière version (beta)** : permet d'afficher le jeu le plus récent. Par exemple, si vous avez un jeu marqué `Rev1` et un autre `Rev3`, seul le `Rev3` sera affiché dans la liste des jeux.
  * **N'afficher que les favoris** : permet d'afficher que les jeux favoris dans les systèmes.
  * **Afficher les jeux cachés** : permet d'afficher les jeux qui ont été cachés.
  * **Cacher les jeux de Mahjong et casino** : permet de cacher les jeux d'arcade de mahjong, casino, etc.
  * **Cacher les jeux pour adultes** : permet de cacher les jeux pour adultes. Fonctionne après avoir scrappé ses jeux.
  * **Cacher les jeux pré-installés** : permet de cacher les jeux fournis avec Recalbox.
  * **Afficher uniquement les jeux à 3+ joueurs** : permet d'afficher uniquement les jeux qui se jouent à 3 joueurs ou plus.
  * **Afficher uniquement les jeux Yoko (horizontal)** : permet d'afficher que les jeux qui se joue sur un écran horizontal.
  * **Afficher uniquement les jeux Tate (vertical)** : permet d'afficher que les jeux qui se joue sur un écran vertical.
  * **Cacher les non jeux** : permet de cacher les jeux qui ne sont pas des jeux.
* **Afficher par nom de fichier** : permet d'afficher les jeux par leur nom de fichier.
* **Mise à jour de la liste des jeux** : si vous avez ajouté ou supprimé un jeu, vous devez mettre à jour la liste des jeux pour que vos modifications soient prises en compte.

### Paramètres arcade

Ce menu vous permet de modifier les options concernant les jeux d'arcade.

* **Activer la vue améliorée** : permet d'activer une vue améliorée pour les systèmes arcade.
* **Replier les clones par défaut** : permet de cacher les jeux arcade reconnus comme clone.
* **Cacher les bios** : permet de cacher les bios des jeux d'arcade.
* **Cacher les jeux non fonctionnels** : permet de cacher les jeux reconnus comme ne fonctionnant pas.
* **Toujours utiliser les noms officiels** : permet d'afficher les noms officiels au lieu des noms des fichiers ou du scrap.
* **Systèmes virtuels des constructeurs** : permet de choisir les fabricants de jeux d'acade que vous souhaitez ajouter dans la liste des systèmes.
* **Système arcade tout-en-un** : permet de modifier quelques options arcade supplémentaires.

### Paramètres tate

Ce menu vous permet de modifier les options des jeux jouables sur un écran vertical.

* **Activer le système virtuel tate** : permet d'afficher le système virtuel tate dans la liste des systèmes.
* **Afficher uniquement les jeux tate dans les listes de jeux** : permet d'afficher uniquement les jeux se jouant sur un écran vertical dans les systèmes.
* **Rotation des jeux** : permet de choisir la rotation à applique sur les jeux tate.
* **Rotation complète du système** : permet d'appliquer une rotation sur toute l'interface.

### Options du son

Ce menu vous propose les options suivantes :

* **Volume du système** : permet de régler le volume de tout le système.
* **Volume de la musique** : permet de régler le volume du son dans l'interface.
* **Mode audio** : permet de choisir quels sons vous souhaitez entendre dans l'interface.
* **Sortie audio** : permet de sélectionner la sortie audio
* **Associer des appareils audio Bluetooth** : permet d'associer une enceinte ou un caque Bluetooth.

### Options réseau

Ce menu propose les options suivantes :

* **Statut** : permet d'afficher si vous êtes connecté ou pas.
* **Adresse IP** : permet d'obtenir l'adresse IP de votre Recalbox.
* **Activer le wifi** : permet d'activer ou de désactiver le wifi.
* **SSID wifi** : permet d'indiquer le nom de votre réseau wifi.
* **Clef wifi** : permet d'indiquer votre mot de passe wifi.
* **Nom du réseau** : permet de modifier le nom de votre Recalbox sur votre réseau local.
* **Connexion WPS automatique** : permet d'activer la connexion WPS.

### Scrapeur

Pour chaque jeu, vous pouvez obtenir des informations (pochette, résumé, etc...) qui seront affichées dans le menu de sélection des jeux de l'interface.

Pressez `START` et aller dans `SCRAPEUR`. Suivez alors les instructions à l'écran.  Vous trouverez plus d'infos sur le scrapeur interne [ici](./../features/internal-scraper).

### Paramètres avancés

* **Overclock** : permet de régler l'overclocking. Les options sont disponibles en fonction de votre installation.
  * AUCUN
  * HIGH
  * TURBO
  * EXTREM
  * NOLIMIT

>Les réglages TURBO et EXTREM peuvent causer des dommages irréversibles au Raspberry Pi s'ils ne sont pas fait dans de bonnes conditions (dissipation de chaleur, ventilation, etc.). La garantie de votre matériel peut être annulée.
{.is-danger}

* **Paramètres de démarrage** : permet de définir certains paramètres de démarrage.
* **Systèmes virtuels** : permet de faire afficher des systèmes virtuels dans la liste des systèmes.
* **Recalbox CRT** : permet de modifier certaines options liés à l'utilisation d'un écran CRT, comme le RGB Dual.
* **Résolutions** : permet de choisir plusieurs résolutions à appliquer. Cette option n'est pas disponible que PC.
* **Configuration avancée de l'émulateur** : permet de configurer les options des émulateurs indépendamment des autres. 
* **Options Kodi** : permet d'activer/désactiver Kodi, de démarrer automatique,ent Kodi au démarrage de Recalbox ou d'activer le raccourci pour démarrer Kodi depuis la liste des systèmes.
* **Gestion des boitiers** : permet de choisir le boitier utilisé si celui-ci bénéficie d'un script de safe shutdown.
* **Afficher la fréquence** : permet d'afficher la fréquence (images par secondes) dans l'interface et dans les jeux.
* **Gestionnaire web Recalbox** : permet d'activer ou de désactiver le gestionnaire web Recalbox.
* **Mise à jour du bootloader** : permet de mettre à jour le bootloader de votre Raspberry Pi 4 / 400 / 5.
* **Restaurer les paramètres d'usine** : permet de réinitialiser votre Recalbox au même état comme si c'était une nouvelle installation. Ceci ne touche absolument pas vos données personnelles.

### Vérification des bios

Ce menu vous permet de gérer les bios requis par certains des émulateurs de Recalbox. Vous obtiendrez plus d'informations sur [cette page](./../features/bios-manager).

### Licence open-source

Ce menu vous affichera la licence d'utilisation de Recalbox.

### Quitter

Menu qui permet **d'éteindre Recalbox.**

* Éteindre
* Extinction rapide
* Redémarrage

## Contrôles

#### Les commandes dans l'interface :

* `A` → Sélectionner
* `B` → Retour
* `Y` → Favoris dans la liste des jeux, démarre les clips de jeux dans la liste des systèmes
* `X` → Démarrer Kodi ou le lobby Netplay dans la liste des systèmes
* `Start` → Menu
* `Select` → Options pour quitter dans la liste des systèmes
* `R` → Groupe de jeux alphabétique suivant dans les listes de jeux, ouvre la fonction recherche dans la liste des systèmes
* `L` → Groupe de jeux alphabétique précédent dans les listes de jeux

## Favoris

Vous pouvez définir un jeu comme favoris en pressant la touche `Y` sur le jeu. Le jeu sera alors placé en début de liste avec une ☆ devant son nom.

>Le système doit être éteint proprement en utilisant le menu de l'interface pour que que la liste de jeux en favoris soit sauvegardée et que vous la retrouviez au prochain démarrage.
{.is-warning}