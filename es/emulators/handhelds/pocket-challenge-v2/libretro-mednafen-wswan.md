---
title: Libretro Mednafen_WSwan
description: 
published: true
date: 2024-07-25T22:44:29.077Z
tags: libretro, mednafen, pocket, challenge, v2, pcv2, wswan, 7.2+
editor: markdown
dateCreated: 2023-09-11T14:08:07.991Z
---

**Libretro Mednafen_WSwan** es un fork para librero del emulador standalone Mednafen WonderSwan, que a su vez es un fork de Cygne.

## ![](/emulators/license.svg) Licencia

El core posee la siguiente licencia [**GPLv2**](https://github.com/libretro/beetle-wswan-libretro/blob/master/COPYING).

## ![](/emulators/compatibility.png) Compatibilidad

 | RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | 
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | 

## ![](/emulators/features.png) Funcionalidades

| Funcionalidad | ¿Soportada? |
| :---: | :---: |
| Reinicio | ✔ |
| Capturas de pantalla | ✔ |
| Grabaciones | ✔ |
| Grabaciones instantáneas | ✔ |
| Rebobinado | ✔ |
| Netplay | ✔ (no hay emulación de câble link) |
| Opciones del core | ✔ |
| Controles | ✔ |
| Remapping | ✔ |
| Softpatching | ✔ |

## ![](/emulators/bios.svg) BIOS

>**No necesita bios.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Extensiones de roms soportadas

Las roms deben tener las extensiones siguientes:

* .pcv2
* .zip

Este sistema soporta las roms comprimidas en formato .zip/.7z. No obstante, se trata sólo de un archivo.

Los ficheros contenidos en los .zip/.7z deben tener las extensiones citadas previamente.
Además, cada fichero .zip/.7z sólo debe contener un fichero rom al interior.

### Emplazamiento

Disponed vuestras roms como en la siguiente imagen: 

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 pcv2
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.zip**

>Se aconsejan las roms del proyecto **No-Intro**.
{.is-success}

>Para más información sobre las roms, consultad [este tutorial](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

>Para conservar las configuraciones personalizadas cuando se produce una actualización del sistema Recalbox, os aconsejamos utilizar las [Sobrecargas de configuración](./../../../advanced-usage/configuration-override).
{.is-info}

### Acceder a las opciones

Podéis configurar las opciones del core de dos formas diferentes.

* Via el Menú RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via el fichero `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Opciones del core

| Opción | Valor de la opción | Variable (para la sobrecarga) | Valores posibles |
| --- | --- | --- | --- |
| Rotation d'écran | `Manuelle` ✅ / `Paysage` / `Portrait` | `wswan_rotate_display` | `manual` / `landscape` / `portrait` |
| Faire pivoter le mappage des touches | `Automatiquement` ✅ / `Paysage` / `Portrait` | `wswan_rotate_keymap` | `auto` / `disabled` / `enabled` |
| Palette de couleurs | `Niveaux de gris` ✅ / `WS - WonderSwan` / `WS - WonderSwan Color` / `WS - SwanCrystal` / `DMG Game Boy` / `Game Boy Pocket` / `Game Boy Light` / `Rose Belle` / `Bleu Bulle` / `Vert Rebelle` / `Digivice` / `Game.com` / `GameKing` / `Game Master` / `Doré Wild` / `Niveaux de vert` / `Orange Hokage` / `Faon Labo` / `Super Saiyan Légendaire` / `Microvision` / `Or Million Live` / `Or Odyssey` / `Bleu Shiny Sky` / `Bleu Slime` / `TI-83` / `Bois des voyages` / `Virtual Boy` | `wswan_mono_palette` | `default` / `wonderswan` / `wondeswan_color` / `swancrystal` / `gb_dmg` / `gb_pocket` / `gb_light` / `blossom_pink` / `bubbles_blue` / `buttercup_green` / `digivice` / `game_com` / `gameking` / `game_master` / `golden_wild` / `greenscale` / `hokage_orange` / `labo_fawn` / `legendary_super_saiyan` / `microvision` / `million_live_gold` / `odyssey_gold` / `shiny_sky_blue` / `slime_blue` / `ti_83` / `travel_wood` / `virtual_boy` |
| Profondeur des couleurs (Redémarrage requis) | `Milliers (16 bits)` ✅ / `Millions (24 bits)` | `wswan_gfx_colors` | `16bit` / `24bit` |
| Saut d'images | `Désactivé` ✅ / `Automatiquement` / `Manuelle` | `wswan_frameskip` | `disabled` / `auto` / `manual` |
| Seuil de saut d'images (%) | `15` / `18` / `21` / `24` / `27` / `30` / `33` ✅ / `36` / `39` / `42` / `45` / `48` / `51` / `54` / `57` / `60` | `wswan_frameskip_threshold` | `15` / `18` / `21` / `24` / `27` / `30` / `33` / `36` / `39` / `42` / `45` / `48` / `51` / `54` / `57` / `60` |
| Mode 60 Hz | `Désactivé` / `Activé` ✅ | `wswan_60hz_mode` | `disabled` / `enabled` |
| Sound Output Sample Rate | `11025` / `22050` / `44100` ✅ / `48000` | `wswan_sound_sample_rate` | `11025` / `22050` / `44100` / `48000` |
| Filtre audio | `Désactivé` ✅ / `Activé` | `wswan_sound_low_pass` | `disabled` / `enabled` |

## ![](/emulators/external-links.png) Enlaces externos

* **Código fuente utilizado:** [https://github.com/libretro/beetle-wswan-libretro/](https://github.com/libretro/beetle-wswan-libretro/)
* **Página web oficial:** [https://mednafen.github.io/](https://mednafen.github.io/)