---
title: Acceso
description: 
published: true
date: 2024-07-26T06:31:14.842Z
tags: acceso
editor: markdown
dateCreated: 2023-09-01T14:40:25.211Z
---

Aprenderéis como acceder al contenido de vuestra Recalbox en las siguientes páginas.

Aquí tenéis los tutoriales disponibles:

[Acceder a una partición en escritura](remount-partition-with-write-access)
[Acceso en red via Cyberduck](network-access-cyberduck)
[Acceso en red via MobaXTerm](network-access-mobaxterm)
[Acceso en red via WinSCP](network-access-winscp)
[Acceso root a través del terminal](root-access-terminal-cli)
[Directorios y comandos SSH](folders-and-ssh-commands)
[No se pueder acceder a la partición « share » desde Windows](share-partition-not-visible-on-windows)
[Recalbox Manager - Interfaz web](recalbox-manager-web-interface)