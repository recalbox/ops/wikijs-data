---
title: Fan controller
description: Fan controller
published: true
date: 2021-11-09T20:37:16.160Z
tags: controller, rpi, fan, 8.0+
editor: markdown
dateCreated: 2021-08-11T19:31:52.642Z
---

# Description

The **recalbox-wpaf** utility allows you to control certain cards (*hat*) that have a fan.  
The list of cards supported by recalbox-wpaf is available [here](./../hardware-compatibility/compatible-devices/rpi-hats).

NB: this utility is still in beta and will be available with Recalbox 7.3.

## Configuration

Configuration is done in the `recalbox.conf` file under the `hat.wpaf.enabled` and `hat.wpaf.board` keys.

### `hat.wpaf.enabled`

Set this key to `1` to enable the recalbox-wpaf utility.

### `hat.wpaf.board`

This key allows you to select the board that powers your Raspberry PI. Possible values are:

* `wspoehatb` for the Waveshare PoE hat board (b),
* `argonforty` for the card that powers the Argon One box,
* `piboy` for the Experimental PI box,
* `rpipoeplus` for the Raspberry PI PoE+ board,
* `fanshim` for the Pimoroni fan SHIM board.

Example:

```ini
hat.wpaf.enabled=1
hat.wpaf.board=rpipoeplus
```

Reboot to take the settings into account.
