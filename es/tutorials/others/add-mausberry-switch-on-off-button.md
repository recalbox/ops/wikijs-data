---
title: Arranque/Parada del Switch Mausberry
description: 
published: true
date: 2023-09-18T10:23:04.354Z
tags: mausberry, switch, arranque, botón, parada
editor: markdown
dateCreated: 2023-09-18T10:23:04.354Z
---

## Introducción

Los circuitos Mausberry gestionan la activación y desactivación del Raspberry Pi mediante un interruptor ON/OFF.  
Este tipo de circuitos se puede utilizar para conectar el interruptor de encendido / apagado de una NES y su botón de RESET, por ejemplo.

>Este tutorial está dedicado a la Raspberry Pi 2/3 de Recalbox.
{.is-info}

## Procedimiento

### Conexiones y cableado

Para las conexiones a la Raspberry Pi, utilizad los puertos GPIO 23 y 24 (pin 16 y 18).
El Mausberry es operativo con GPIO23 = OUT / IN = GPIO24.

### Procedimiento

#### Prerequisitos

* Vuestro Raspberry Pi debe estar conectado a la red
* Debéis conocer la [dirección IP](./../../tutorials/network/ip/discover-recalbox-ip) de vuestro Raspberry Pi.

#### Etapas

* Abrid el fichero [recalbox.conf](./../../basic-usage/getting-started/recalboxconf-file)
* En la sección `A - System Options`, descomentar esta línea:

```ini
# ------------ A - System Options ----------- #
# Uncomment the system.power.switch you use
system.power.switch=MAUSBERRY # http://mausberry-circuits.myshopify.com/pages/setup
```

Grabad !

## Anexos

### Conexiones y cableado

![](/tutorials/others/gpio-mausberry.png)

### Script : setup.sh

```shell
echo '#!/bin/bash

# C'est la broche GPIO connectée au fil de l'interrupteur marqué OUT
GPIOpin1=20

# C'est la broche GPIO connectée au fil de l'interrupteur marqué IN
GPIOpin2=21

echo "$GPIOpin1" > /sys/class/gpio/export
echo "in" > /sys/class/gpio/gpio$GPIOpin1/direction
echo "$GPIOpin2" > /sys/class/gpio/export
echo "out" > /sys/class/gpio/gpio$GPIOpin2/direction
echo "1" > /sys/class/gpio/gpio$GPIOpin2/value
while [ 1 = 1 ]; do
power=$(cat /sys/class/gpio/gpio$GPIOpin1/value)
if [ $power = 0 ]; then
sleep 1
else
poweroff
fi
done' > /recalbox/scripts/mausberry.sh
chmod 777 /recalbox/scripts/mausberry.sh
```

### Script : S99maus

```shell
#!/bin/bash
### BEGIN INIT INFO
# Provides: mausberry.sh
# Required-Start: $network $local_fs $remote_fs
# Required-Stop: $network $local_fs $remote_fs
# Default-Start: 2 3 4 5
# Default-Stop: 0 1 6
# Short-Description: switch mausberry init script.
# Description: Starts and stops SwitchDaemon service.
### END INIT INFO

#VAR
RUN="/recalbox/scripts/mausberry.sh"
BTD_PID=$(ps -eo pid,command | grep "/bin/bash $RUN" | grep -v grep | awk '{print $1}')

serviceStatus() {
   if [ ! -z "$BTD_PID" ]; then
      echo -e '33[0mservice mausberry.sh ['$BTD_PID'] [33[33;32m OK 33[0m]'
   else
      echo -e '33[0mservice mausberry.sh [33[33;31m KO 33[0m]'
   fi
}

# Carry out specific functions when asked to by the system
case "$1" in
   start)
      echo "Starting script $RUN ..."
      if [ -z "$BTD_PID" ]; then
         nice -n 19 $RUN&

         if [ $? -eq 0 ]; then
            echo -e "33[0mscript $RUN [33[33;32m STARTED 33[0m]"
         fi
      else
         echo "script $RUN already started ['$BTD_PID']!"
      fi
      #serviceStatus
   ;;
   stop)
      echo "Stopping script $RUN ..."
      if [ ! -z "$BTD_PID" ]; then
         kill $BTD_PID

         if [ $? -eq 0 ]; then
            echo -e "33[0mscript $RUN [33[33;31m STOPPED 33[0m]"
         fi
      fi
      #serviceStatus
   ;;
   status)
      serviceStatus
   ;;
   *)
      echo "Usage: /etc/init.d/S99maus {start | stop | status}"
      exit 1
   ;;
esac

exit 0
```