---
title: Controllers
description: 
published: true
date: 2024-07-26T06:50:38.934Z
tags: controllers
editor: markdown
dateCreated: 2021-08-07T13:25:34.800Z
---

A problem with the controller? Maybe you will find your happiness here.

Here are the available categories:
[Nintendo Online wireless controllers](nintendo-online-wireless-controllers)

Here are the available tutorials:

[Connect an IPEGA controller](connect-ipega-controller)
[PS3 controllers drivers](ps3-controllers-drivers)
[Play using a PSP with FuSa controller](play-using-psp-with-fusa-controller)
[Play with your smartphone using the virtual gamepad](play-with-smartphone-using-virtual-gamepad)
[XArcade usage](xarcade-usage)