---
title: GPIO
description: 
published: true
date: 2023-09-26T07:03:42.723Z
tags: gpio
editor: markdown
dateCreated: 2023-09-18T19:15:35.226Z
---

En esta sección encontraréis tutoriales para utilizar el puerto GPIO de vuestros Raspberry Pi de varias formas diferentes.

Aquí tenéis la lista de tutoriales disponibles:

[Activar el botón GPIO para cerrar el emulador con un solo botón](enable-gpio-button-close-emulator)
[Atribuir hasta 4 funciones en cada puerto GPIO](set-until-4-features-on-each-gpio-port)
[Conectar un mecanismo de monedas comparativo en un Raspberry Pi](connect-a-comparative-coin-mechanism-to-a-raspberry-pi)
[Jugad con vuestro mando original utilizando Gamecon](use-your-original-controller-with-gamecon)
[Los controladores GPIO](gpio-controllers)
[Teclas del teclado a travès del GPIO](keyboard-touches-via-gpio)