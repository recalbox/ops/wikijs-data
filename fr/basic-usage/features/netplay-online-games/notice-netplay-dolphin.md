---
title: Notice Netplay Dolphin
description: Jouer en ligne sur Gamecube et Wii
published: true
date: 2023-01-02T08:02:40.755Z
tags: dolphin, netplay
editor: markdown
dateCreated: 2021-05-21T08:12:31.143Z
---

>Afin de jouer en ligne à la Gamecube, quelques configurations sont requises ainsi qu'un clavier et une souris.
{.is-info}

## Accéder au menu Netplay 

* Lancez n'importe quel jeux de Gamecube ou de Wii.
* En jeu, avec un clavier, appuyez sur `ALT` + `F4` pour accéder au menu Dolphin.
* Dans le menu, à l'aide de votre souris, ouvrez le menu `Outils` .

![](/basic-usage/features/netplay/dolphin-netplay-outil-menu.png){.full-width}

### Créer un salon de jeux (hôte)

* Cliquez sur `Démarrer Netplay...`

![](/basic-usage/features/netplay/dolphin-hote-netplay.png){.full-width}

* Cliquez sur l'onglet `Hôte`, sélectionnez la région, sélectionnez votre jeu dans la liste et cliquez sur `Hôte` pour ouvrir un salon Netplay.

>Vous avez la possibilité sur cette fenêtre de définir un mot de passe pour le salon Netplay ainsi que le port à utiliser si vous devez utiliser un port spécifique.
{.is-info}

![](/basic-usage/features/netplay/dolphin-netplay-hote-setting.png){.full-width}

* Une fenêtre de chat textuel apparaît et votre salon Netplay est désormais joignable !

![](/basic-usage/features/netplay/dolphin-netplay-hote-chat-room.png){.full-width}

>Dans la plupart des jeux, ajoutez environ 1 Buffer par 15 ms de latence par client.
{.is-warning}

>Deux joueurs avec une latence de 50 ms représentent environ 3 à 4 Buffer, tandis que trois joueurs avec une latence de 50 et 65 ms représentent environ 7 Buffer.
{.is-success}

* Pour démarrer le jeu, cliquez sur `START` !

### Rejoindre un salon de jeux (client)

* Ouvrez le menu `Outils` et cliquez sur `Parcourir les sessions NetPlay`.

![](/basic-usage/features/netplay/dolphin-netplay-client.png){.full-width}

* Dans le lobby Netplay de Dolphin, vous pouvez apercevoir toutes les parties en ligne.

![](/basic-usage/features/netplay/dolphin-netplay-client-lobby.png){.full-width}

* Double-cliquez sur le salon pour accéder au chat textuel.

>Si un mot de passe a été défini, vous serez amené à le saisir à ce moment-là.
{.is-info}

* Patientez pendant le lancement du jeu par l'hôte dans le salon de jeu !

![](/basic-usage/features/netplay/dolphin-netplay-client-chat-room.png){.full-width}