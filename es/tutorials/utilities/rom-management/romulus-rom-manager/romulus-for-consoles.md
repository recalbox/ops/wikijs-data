---
title: Romulus para las consolas
description: Utilización de Romulus Rom Manager para las consolas (Romsets No-Intro en particular)
published: true
date: 2023-09-16T20:44:15.878Z
tags: romulus, consolas
editor: markdown
dateCreated: 2023-09-16T20:44:15.878Z
---

## Antes de empezar

Para clasificar las roms, Romulus necesita un fichero .dat. Este archivo actúa como un catálogo que lista los ficheros válidos para cada rom junto con su descripción, lo que permite determinar si vuestro fichero corresponde o no al fichero requerido por romset.

Los archivos Dat más adecuados para consolas son los del proyecto **No-Intro**. Existen otros proyectos que proporcionan roms, pero no se tratarán aquí.

Visitad la página [DAT-o-MATIC] (https://datomatic.no-intro.org/?page=download) para descargar los fichero .dat de **No-Intro**.

### DAT-o-MATIC

He aquí un ejemplo de cómo descargar un fichero .dat No-Intro. Dado que el sitio Dat-O-Matic evoluciona regularmente, es posible que el método presentado aquí no esté al día.

#### 1. Página de inicio

![](/tutorials/utilities/rom-management/romulus/consoles/romulus1.png)

* A: Lista de todos los sistemas con un fichero .dat disponible

#### 2. Descargar un fichero .dat

![](/tutorials/utilities/rom-management/romulus/consoles/romulus2.png)

* **A** : Selección del sistema
* **B** : Pulsad **Preparar** para mostrar la siguiente pantalla

![](/tutorials/utilities/rom-management/romulus/consoles/romulus3.png)

* **A** : Lanzar la descarga

### Fichero Header

Para funcionar con Romulus, algunas consolas necesitarán un archivo adicional llamado **Header** (por ejemplo, es el caso de la Nintendo NES). Recordad descargarlo al mismo tiempo que el archivo .dat.

![](/tutorials/utilities/rom-management/romulus/consoles/romulus4.png)

### Para ir más lejos

Como podéis ver en las capturas de pantalla anteriores, es posible personalizar el fichero .dat para obtener una única región, uno o varios idiomas, etc., en función de vuestras necesidades.  
Dependiendo del sistema, estas opciones pueden ser más o menos numerosas, pero si no tocáis a nada, obtendréis el fichero .dat completo.

## Añadir un fichero .dat

* Pulsad en el icono "Añadir DAT" y seleccionad los ficheros .dat que queréis añadir a Romulus. No es necesario descomprimir los archivos si están en formato .zip.

![](/tutorials/utilities/rom-management/romulus/consoles/romulus5.png)

>Arrastrar y soltar también funciona para añadir uno o más ficheros.
{.is-info}

* Validad con `SI` en las 2 ventanas siguientes.

![](/tutorials/utilities/rom-management/romulus/consoles/romulus6.png)

![](/tutorials/utilities/rom-management/romulus/consoles/romulus7.png)

* El fichero ha sido añadido a la base de datos de Romulus.

![](/tutorials/utilities/rom-management/romulus/consoles/romulus8.png)

Todas las columnas son bastante autoexplicativas, pero aquí hay una breve descripción de cada una:

* **Descripción:** Descripción del fichero DAT en la base de datos (puede modificarse).
* **Versión:** Versión del fichero DAT (para ficheros No-Intro, es la fecha y hora del archivo).
* **Sets propios:** Muestra el número de sets propios y el número total de sets posibles (útil sólo para arcade).
* **Roms poseídos:** Muestra el número de roms poseídos y el número total de roms en el fichero.
* **Modo de archivo:** No es relevante para consolas, pero indica el modo en el que se almacenarán las roms (para detalles de otros modos, ver [Romulus for the Arcade](./romulus-for-arcade)).
* **Común:** No se utiliza.
* **Añadido el:** Indica la fecha en la que se añadió el fichero a la base de datos.
* **Última exploración:** Indica la última vez que realizó una exploración con este fichero.
* **Nombre:** Indica el nombre del fichero.
* **Ruta:** Ruta vinculada al árbol de base de datos.

## Configurar el fichero .dat añadido

### Propiedades del fichero

* Pulsad el botón derecho del ratón en el fichero que queréis configurar y seleccionad **Propiedades**.

![](/tutorials/utilities/rom-management/romulus/consoles/romulus9.png)

* Para consolas, verificad que el **Modo de fichero** está ajustado a **Not Split** (para más detalles sobre otros modos, consulte [Romulus for the Arcade](./romulus-for-arcade)) y luego pulsad **OK**.

![](/tutorials/utilities/rom-management/romulus/consoles/romulus10.png)

### Definir la carpeta para las roms

* Haced doble click en el fichero elegido para bascular en el modo `Scanner`, y pulsad en **Scan**:

![](/tutorials/utilities/rom-management/romulus/consoles/romulus11.png)

* En la siguiente ventana, hay que rellenar las siguientes informaciones:

![](/tutorials/utilities/rom-management/romulus/consoles/romulus12.png)

**A**: Seleccionad una carpeta vacía donde se van a copiar las roms válidas encontradas durante el escaneo para construir el romset.  
**B**: Opcionalmente, definid la carpeta de copia de seguridad. Las roms no validadas presentes en el romset durante el escaneo se copiarán en esta carpeta.  
**C**: Eventualmente, aquí es donde se debe definir el archivo Header para los archivos .dat que lo necesitan.

Una vez definida toda esta información, pulsad en **Cerrar**.

## Contruir el romset

### Presentación de la página Scanner

La página **Scanner** es la página más importante de Romulus. Aquí es donde puede ver la lista de sets y de roms presentes en el fichero .dat y que constituyen el romset.

![](/tutorials/utilities/rom-management/romulus/consoles/romulus13.png)

Detalle importante:

![](/tutorials/utilities/rom-management/romulus/consoles/romulus14.png)

Estos tres símbolos indican el estado del romset. La carpeta **verde** indica cuántos juegos están completos y disponibles, la carpeta **amarilla** indica cuántos juegos están incompletos y la carpeta **roja** indica cuántos juegos faltan (estos símbolos también se muestran en la lista, delante de cada juego, para ver el estado de cada juego).  
Podéis filtrar la lista de juegos haciendo clic en estos iconos para mostrar sólo algunos de los conjuntos de juegos. Por defecto, las tres carpetas están activas para mostrar toda la información. Si, por ejemplo, sólo queréis ver las juegos incompletos o que faltan, pulsad en la carpeta verde para ocultar todos las roms completas.

### Construcción del romset

Pulsad **Reconstruire por batch todos los profiles seleccionados** :

![](/tutorials/utilities/rom-management/romulus/consoles/romulus15.png)

>También se puede utilizar el botón **Reconstruir** (a la izquierda del botón que se recomienda) pero es menos práctico.
{.is-info}

Se abrirá una ventana que os pide seleccionar la carpeta que contiene las roms a comparar con el fichero .dat. Atención, esta carpeta no es la misma que carpeta de roms de destino configurada anteriormente. Es la carpeta que contiene los ficheros que queréis validar con Romulus.

![](/tutorials/utilities/rom-management/romulus/consoles/romulus16.png)

En el siguiente ejemplo, la carpeta _NES en bulk_ contiene roms de NES de origen desconozido. Pulsad **OK** una vez seleccionado este directorio, y se abrirá la siguiente ventana:

![](/tutorials/utilities/rom-management/romulus/consoles/romulus17.png)

Marcad **Sí para todos** y validad con **OK**, en este momento comienza la construcción del romset:

![](/tutorials/utilities/rom-management/romulus/consoles/romulus18.png)

Todo lo que tenéis que hacer es esperar hasta el final del proceso (que puede ser largo).

![](/tutorials/utilities/rom-management/romulus/consoles/romulus19.png)

Los juegos considerados conformes se ponen en verde y se copian en el directorio romset de destino... Para los más valientes, sólo os queda lanzaros a la búsqueda del fullset (conjunto de juegos completo) en internet, Romulus se encargará de validarlo y corregirlo para que funcione en vuestro emulador, tal y como os hemos mostrado en esta sección de la documentación.