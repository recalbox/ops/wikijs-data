---
title: Libretro Mesen
description: 
published: true
date: 2024-07-05T15:03:24.525Z
tags: libretro, mesen, nes
editor: markdown
dateCreated: 2023-09-07T06:45:22.239Z
---

Libretro Mesen es un emulador de la NES y Famicom de gran precisión y un lector NSF.

## ![](/emulators/license.svg) Licencia

El core posee la siguiente licencia [**GPLv3**](https://github.com/SourMesen/Mesen/blob/master/README.md).

## ![](/emulators/compatibility.png) Compatibilidad

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | 
| ❌ | ❌ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Funcionalidades

| Funcionalidad | ¿Soportada? |
| :---: | :---: |
| Reinicio | ✔ |
| Capturas de pantalla | ✔ |
| Grabaciones | ✔ |
| Grabaciones instantáneas | ✔ |
| Rebobinado | ✔ |
| Netplay | ✔ |
| Optiones del core | ✔ |
| RetroAchievements | ✔ |
| Cheats RetroArch | ✔ |
| Controles | ✔ |
| Remapping | ✔ |
| Softpatching | ✔ |

## ![](/emulators/bios.svg) BIOS

>**No se necesitan bios.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Extensiones de roms soportadas

Las roms deben tener las extensiones siguientes:

* .nes
* .unf
* .unif
* .zip
* .7z

Este sistema soporta las roms comprimidas en formato .zip/.7z. No obstante, se trata sólo de un archivo.

Los ficheros contenidos en los .zip/.7z deben tener las extensiones citadas previamente.
Además, cada fichero .zip/.7z sólo debe contener un fichero rom al interior.

### Emplazamiento

Disponed vuestras roms como en la siguiente imagen: 

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 nes
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.zip**

>Se aconsejan las roms del projecto **No-Intro**.
{.is-success}

>Para más información, consultad [este tutorial](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

>Para conservar las configuraciones personalizadas cuando se produce una actualización del sistema Recalbox, os aconsejamos utilizar las [Sobrecargas de configuración](./../../../advanced-usage/configuration-override).
{.is-info}

### Acceder a las opciones

Podéis configurar las opciones del core de dos formas diferentes.

* Via el Menú RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via el fichero `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Opciones del core

| Opción | Valor de la opción | Variable (para la sobrecarga) | Valores posibles |
| --- | --- | --- | --- |
| NTSC filter | `Désactivé` ✅ / `Composite (Blargg)` / `S-Video (Blargg)` / `RGB (Blargg)` / `Monochrome (Blargg)` / `Bisqwit 2x` / `Bisqwit 4x` / `Bisqwit 8x` | `mesen_ntsc_filter` | `Disabled` / `Composite (Blargg)` / `S-Video (Blargg)` / `RGB (Blargg)` / `Monochrome (Blargg)` / `Bisqwit 2x` / `Bisqwit 4x` / `Bisqwit 8x` |
| Palette | `Default` ✅ / `Composite Direct (by FirebrandX)` / `Nes Classic` / `Nestopia (RGB)` / `Original Hardware (by FirebrandX)` / `PVM Style (by FirebrandX)` / `Sony CXA2025AS` / `Unsaturated v6 (by FirebrandX)` / `YUV v3 (by FirebrandX)` / `Wavebeam (by nakedarthur)` / `Custom` / `Raw` | `mesen_palette` | `Default` / `Composite Direct (by FirebrandX)` / `Nes Classic` / `Nestopia (RGB)` / `Original Hardware (by FirebrandX)` / `PVM Style (by FirebrandX)` / `Sony CXA2025AS` / `Unsaturated v6 (by FirebrandX)` / `YUV v3 (by FirebrandX)` / `Wavebeam (by nakedarthur)` / `Custom` / `Raw` |
| Overclock | `None` ✅ / `Low` / `Medium` / `High` / `Very High` | `mesen_overclock` | `None` / `Low` / `Medium` / `High` / `Very High` |
| Overclock Type | `Before NMI (Recommended)` ✅ / `After NMI` | `mesen_overclock_type` | `Before NMI (Recommended)` / `After NMI` |
| Region | `Auto` ✅ / `NTSC` / `PAL` / `Dendy` | `mesen_region` | `Auto` / `NTSC` / `PAL` / `Dendy` |
| Left Overscan | `None` ✅ / `4px` / `8px` / `12px` / `16px` | `mesen_overscan_left` | `None` / `4px` / `8px` / `12px` / `16px` |
| Right Overscan | `None` ✅ / `4px` / `8px` / `12px` / `16px` | `mesen_overscan_right` | `None` / `4px` / `8px` / `12px` / `16px` |
| Top Overscan | `None` ✅ / `4px` / `8px` / `12px` / `16px` | `mesen_overscan_top` | `None` / `4px` / `8px` / `12px` / `16px` |
| Bottom Overscan | `None` ✅ / `4px` / `8px` / `12px` / `16px` | `mesen_overscan_bottom` | `None` / `4px` / `8px` / `12px` / `16px` |
| Aspect Ratio | `Auto` ✅ / `No Stretching` / `NTSC` / `PAL` / `4:3` / `16:9` | `mesen_aspect_ratio` | `Auto` / `No Stretching` / `NTSC` / `PAL` / `4:3` / `16:9` |
| Controller Turbo Speed | `Fast` ✅ / `Very Fast` / `Désactivé` / `Slow` / `Normal` | `mesen_controllerturbospeed` | `Fast` / `Very Fast` / `Disabled` / `Slow` / `Normal` |
| Shift A/B/X/Y clockwise | `Désactivé` ✅ / `Activé` | `mesen_shift_buttons_clockwise` | `disabled` / `enabled` |
| Enable HD Packs | `Activé` ✅ / `Désactivé` | `mesen_hd_packs` | `enabled` / `disabled` |
| Remove sprite limit | `Désactivé` ✅ / `Activé` | `mesen_no_sprite_limit` | `disabled` / `enabled` |
| Enable fake stereo effect | `Désactivé` ✅ / `Activé` | `mesen_fake_stereo` | `disabled` / `enabled` |
| Reduce popping on Triangle channel | `Activé` ✅ / `Désactivé` | `mesen_mute_triangle_ultrasonic` | `enabled` / `disabled` |
| Reduce popping on DMC channel | `Activé` ✅ / `Désactivé` | `mesen_reduce_dmc_popping` | `enabled` / `disabled` |
| Swap Square channel duty cycles | `Désactivé` ✅ / `Activé` | `mesen_swap_duty_cycle` | `disabled` / `enabled` |
| Disable Noise channel mode flag | `Désactivé` ✅ / `Activé` | `mesen_disable_noise_mode_flag` | `disabled` / `enabled` |
| Screen rotation | `None` / `90 degrees` ✅ / `180 degrees` / `270 degrees` | `mesen_screenrotation` | `None` / `90 degrees` / `180 degrees` / `270 degrees` |
| Default power-on state for RAM | `All 0s (Default)` ✅ / `All 1s` / `Random Values` | `mesen_ramstate` | `All 0s (Default)` / `All 1s` / `Random Values` |
| FDS: Automatically insert disks | `Désactivé` ✅ / `Activé` | `mesen_fdsautoselectdisk` | `disabled` / `enabled` |
| FDS: Fast forward while loading | `Désactivé` ✅ / `Activé` | `mesen_fdsfastforwardload` | `disabled` / `enabled` |
| Sound Output Sample Rate | `96000` ✅ / `192000` / `384000` / `11025` / `22050` / `44100` / `48000` | `mesen_audio_sample_rate` | `96000` / `192000` / `384000` / `11025` / `22050` / `44100` / `48000` |

## ![](/emulators/external-links.png) Enlaces externos

* **Código fuente utilizado** : [https://github.com/libretro/Mesen/](https://github.com/libretro/Mesen/)
* **Documentación Libretro** : [https://docs.libretro.com/library/mesen/](https://docs.libretro.com/library/mesen/)