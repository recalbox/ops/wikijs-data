---
title: Gestión de la dirección IP y de la configuración de red
description: 
published: true
date: 2024-07-26T06:27:04.308Z
tags: ip, red, dirección, administración
editor: markdown
dateCreated: 2023-09-13T06:36:28.980Z
---

¿Un problemita con la dirección IP de vuestra Recalbox? Aquí encontraréis la solucion.

Aquí tenéis los tutoriales disponibles:

[Definir la dirección IP de la conexión Wi-Fi](static-manual-ip)
[Encontrar la dirección IP de vuestra Recalbox](discover-recalbox-ip)