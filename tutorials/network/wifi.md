---
title: Wi-Fi
description: 
published: true
date: 2023-07-20T10:33:06.200Z
tags: wifi, network, admin
editor: markdown
dateCreated: 2021-08-06T08:27:06.596Z
---

If you want to activate the wifi on your Recalbox or if you have a connection problem, you should find your happiness here.

Here are the available tutorials:

- [Enable Wi-Fi](enable-wifi)
- [Using a static IP address on the Wi-Fi network](/en/tutorials/network/ip/static-manual-ip)
- [Defining the Wi-Fi country code for the Wi-Fi network](wifi-country-code)