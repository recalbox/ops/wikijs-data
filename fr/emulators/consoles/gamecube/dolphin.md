---
title: Dolphin
description: 
published: true
date: 2025-02-06T23:47:36.530Z
tags: 
editor: markdown
dateCreated: 2021-05-21T08:20:48.112Z
---

**Dolphin** est un émulateur pour deux récentes consoles de jeu de Nintendo : la GameCube et la Wii. Il permet aux joueurs sur PC d'apprécier les jeux réalisés pour ces deux consoles en **full HD** (1080p) avec différentes améliorations : compatibilité avec tous les contrôleurs de PC, vitesse turbo, jeu en ligne en réseau, et bien plus encore !

Plus de 200 personnes ont travaillé dur depuis des années pour créer Dolphin. La liste des contributeurs peut être trouvée [sur GitHub](https://github.com/dolphin-emu/dolphin/graphs/contributors).

## ![](/emulators/license.svg) Licence

Ce core est sous licence [**GPLv2**](https://github.com/dolphin-emu/dolphin/blob/master/license.txt).

## ![](/emulators/compatibility.png) Compatibilité

| RPI0/RPI1 | RPI3 | RPI4/PI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ❌ | ❌ | ❌ | ❌ | ❌ | ❌ | ✅ | ❌ |

## ![](/emulators/features.png) Fonctionnalités

| Fonctionnalité | Supporté |
| :---: | :---: |
| Netplay | ✔ |

## ![](/emulators/bios.svg) BIOS

Bien que purement facultatives, il existe tout de même des bios Gamecube par zone géographique, à savoir Europe (EUR), États-Unis (USA) et Japon (JAP).

### Liste des bios optionnels

| Nom de fichier | Description | MD5 | Fourni |
| :---: | :---: | :---: | :---: |
| IPL.bin | BIOS GameCube EUR | 0cdda509e2da83c85bfe423dd87346cc | ❌ |
| IPL.bin | BIOS GameCube JAP | fc924a7c879b661abc37cec4f018fdf3 | ❌ |
| IPL.bin | BIOS GameCube USA | 339848a0b7c2124cf155276c1e79cbd0 | ❌ |

### Emplacement

Placez les bios comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 gamecube
┃ ┃ ┃ ┃ ┣ 📁 EUR
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **IPL.bin**
┃ ┃ ┃ ┃ ┣ 📁 JAP
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **IPL.bin**
┃ ┃ ┃ ┃ ┣ 📁 USA
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **IPL.bin**

## ![](/emulators/isos.svg) Isos

### Extensions supportées

Les isos doivent avoir les extensions suivantes :

* .ciso
* .gc
* .gcm
* .gcz
* .iso
* .m3u
* .rvz

### Emplacement

Placez les isos comme ceci : 

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 gamecube
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.rvz**

>Les isos au format **Redump** sont vivement conseillés.
{.is-success}

>Pour plus d'information sur les isos, rendez-vous sur [ce tutoriel](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/external-links.png) Liens externes

* **Code source utilisé** : [https://github.com/dolphin-emu/dolphin/](https://github.com/dolphin-emu/dolphin/)
* **Site officiel** : [https://www.dolphin-emu.org/](https://www.dolphin-emu.org/)