---
title: Configuration des manettes N64
description: 
published: true
date: 2021-09-04T22:31:51.148Z
tags: n64, configuration, manette
editor: markdown
dateCreated: 2021-05-21T08:34:39.439Z
---

Pour paramétrer une manette N64 dans Recalbox, vous devez utiliser la commande `sdl2-jstest`.

### Ajoutez la configuration de votre contrôleur au système :

Pour configurer votre manette, vous aurez besoin d'informations dessus en utilisant la commande suivante :

```shell
sdl2-jstest -l
```

>Vous pouvez trouver plus d'informations sur l'utilisation de la commande `sdl2-jstest` sur [cette page](./../../../tutorials/controllers/test-configuration/test-your-joystick-with-sdl2-jstest).
{.is-info}

Dans cet exemple, on obtient :

```shell
Joystick (Broadcom Bluetooth Wireless  Joystick                        ) has 6 axes (X, Y, Z, Rz, Hat0X, Hat0Y)
and 12 buttons (Trigger, ThumbBtn, ThumbBtn2, TopBtn, TopBtn2, PinkieBtn, BaseBtn, BaseBtn2, BaseBtn3, BaseBtn4, BaseBtn5, BaseBtn6).
```

Une fois le nom de la manette et chaque bouton identifié, vous devez modifier le fichier `/usr/share/mupen64plus/InputAutoCfg.ini`. Pour cela, utilisez cette commande :

```shell
nano /usr/share/mupen64plus/InputAutoCfg.ini
```

Si ce fichier n'existe pas, il sera crée avec la commande ci-dessus.

À la fin du fichier, ajoutez les informations de configuration, comme ceci :

>Ce modèle est un exemple, avec des informations sur une manette spécifique (nom de la manette, numéro de bouton etc...). Vous devrez l'adapter avec les informations de votre propre manette.
{.is-info}

```ini
[Broadcom Bluetooth Wireless  Joystick                        ]
plugged = True
plugin = 2
mouse = False
AnalogDeadzone = 4096,4096
AnalogPeak = 32768,32768
DPad R = hat(0 Right)
DPad L = hat(0 Left)
DPad D = hat(0 Down)
DPad U = hat(0 Up)
Start = button(9)
Z Trig = button(7)
B Button = button(2)
A Button = button(1)
C Button R = axis(3+)
C Button L = axis(3-)
C Button D = axis(2+)
C Button U = axis(2-)
R Trig = button(5)
L Trig = button(4)
Mempak switch = button(6)
Rumblepak switch = 
X Axis = axis(0-,0+)
Y Axis = axis(1-,1+)
```

Une fois que votre configuration est correcte, faites `Ctrl + X` pour quitter nano, acceptez d'écraser le fichier avec `Y` et appuyez sur `Entrée` pour quitter. Vous pouvez maintenant démarrer un jeu N64, et tester votre configuration.

Votre configuration est correcte, vous l'aimez ? Super ^^ Mais, il y a un "problème". Lorsque vous mettez à jour votre Recalbox, tous ces fichiers de configuration sont également mis à jour. Ils seront donc réinitialisés. Si vous ne voulez pas le faire après chaque mise à jour, faites une sauvegarde de votre fichier `InputAutoCfg.ini`.

Vous pouvez également ajouter votre configuration à Recalbox. Vous devez consulter ce point, et poster votre propre configuration en commentaire. Nous l'ajouterons ensuite au système lors d'une prochaine mise à jour de Recalbox.

### Ajouter des commandes spéciales

Mupen64plus ne supporte pas les combinaisons de boutons, comme les émulateurs RetroArch, pour lancer une commande spéciale. Mais vous pouvez affecter les boutons inutilisés à une commande spécifique, comme sauvegarder/charger une sauvegarde instantanée, changer les emplacements de sauvegarde, etc...

Pour ce faire, dans un premier temps, vous devez identifier tous les boutons inutilisés dans votre configuration. Comme vu précédemment, connectez-vous en [SSH](./../../../tutorials/system/access/root-access-terminal-cli) et utilisez la commande `sdl2-jstest`, puis notez le numéro de code de vos boutons inutilisés.

>Vous pouvez trouver plus d'informations sur l'utilisation de la commande `sdl2-jstest` sur [cette page](./../../../tutorials/controllers/test-configuration/test-your-joystick-with-sdl2-jstest).
{.is-info}

Maintenant, toujours via SSH, modifiez votre fichier `mupen64plus.cfg` avec cette commande :

```shell
nano /recalbox/configs/mupen64/mupen64plus.cfg
```

Ensuite, allez dans la section intitulée `[CoreEvents]`. La bonne section, c'est celle-là :

```ini
# Joystick event string for stopping the emulator
Joy Mapping Stop = ""
# Joystick event string for switching between fullscreen/windowed modes
Joy Mapping Fullscreen = ""
# Joystick event string for saving the emulator state
Joy Mapping Save State = ""
# Joystick event string for loading the emulator state
Joy Mapping Load State = ""
# Joystick event string for advancing the save state slot
Joy Mapping Increment Slot = ""
# Joystick event string for taking a screenshot
Joy Mapping Screenshot = ""
# Joystick event string for pausing the emulator
Joy Mapping Pause = ""
# Joystick event string for muting/unmuting the sound
Joy Mapping Mute = ""
# Joystick event string for increasing the volume
Joy Mapping Increase Volume = ""
# Joystick event string for decreasing the volume
Joy Mapping Decrease Volume = ""
# Joystick event string for fast-forward
Joy Mapping Fast Forward = ""
# Joystick event string for pressing the game shark button
Joy Mapping Gameshark = ""
```

Il faut garder à l'esprit que Mupen64plus identifie `player 1` en tant que `J0`, `player 2` en `J1` etc. Ainsi, par exemple, si vous voulez ajouter le bouton numéro 10 du joueur 1 à la commande "_save savestates_", vous devez éditer votre fichier comme ceci :

```ini
Joy Mapping Save State = "J0B10"
```

Et si vous voulez ajouter le bouton 5 du joueur 2 à la commande "_load savestates_", vous devez éditer votre fichier comme ceci:

```ini
Joy Mapping Load State = "J1B5"
```

Une fois que votre configuration est correcte, faites `Ctrl + X` pour quitter nano, acceptez d'écraser le fichier avec `Y` et appuyez sur `Entrée` pour quitter.