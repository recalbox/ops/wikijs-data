---
title: Play Singe games
description: 
published: true
date: 2024-11-10T13:33:15.713Z
tags: daphne, singe, 8.0+
editor: markdown
dateCreated: 2021-08-08T07:01:34.697Z
---

## What are Singe Games?

The term `SINGE` (from the name of the dragon in Dragon's Lair) is the name of an extension for the Daphne emulator that allows anyone to create their own FMV: (full motion video).

It allows you to run the famous American Laser Games (ALG) that were all the rage in the arcades in their day. It allows you to play WoW Action Max and some other arcade games.

## List of Singe games

Here is the list of ALG Singe games:

* Crime Patrol
* Drug Wars
* MadDog McCree
* MadDog McCree 2
* The Last Bounty Hunter
* Time Traveler v2.0
* Who Shot Johnny Rock

Here is the list of WoW Action Max Singe games:

* 38 Ambush Alley
* Blue Thunder
* HydroSub 2021
* Pops Ghostly
* Sonic Fury

Here is the list of Arcade Singe games:

* Ninja Hayate
* Space Pirates
* TimeGal
* Time Traveler

## Games installation

Until Recalbox 8.0, games were separated into 2 locations into the system per game. Since Recalbox 8.1, it's not the case anymore out of the WoW Action Max games (a common folder for the 5 games in addition to their own folder).

To make it simple, files which were on `/roms/daphne/singe/foldername/gamename` go into the same folder than the game itself, namely `/roms/daphne/gamename.daphne`.

## Location of files per game

Since it is particularly difficult to place the right files in the right places, you will see below how to put the necessary files for each game in the Recalbox tree.

>Everything below is an example of working romset, yours can work with some differences.
{.is-info}

## {.tabset}

### American Laser Games

#### Crime Patrol

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 crimepatrol.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 addlg___.ttf
┃ ┃ ┃ ┃ ┃ ┣ 🗒 badge3.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 badge4.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 break.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 ...
┃ ┃ ┃ ┃ ┃ ┣ 📁 cdrom
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 a_cpintro.dat
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 a_cpintro.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 a_cpintro.ogg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 b_airterrorist.dat
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 ...
┃ ┃ ┃ ┃ ┃ ┣ 🗒 ...
┃ ┃ ┃ ┃ ┃ ┣ 🗒 crimepatrol.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 crimepatrol.txt
┃ ┃ ┃ ┃ ┃ ┣ 🗒 ...


#### Drug Wars

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 drugwars.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 addlg___.ttf
┃ ┃ ┃ ┃ ┃ ┣ 🗒 badge3.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 badge4.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 break.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 ...
┃ ┃ ┃ ┃ ┃ ┣ 📁 cdrom
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 a_dwintro.dat
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 a_dwintro.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 a_dwintro.ogg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 a_partner01.dat
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 ...
┃ ┃ ┃ ┃ ┃ ┣ 🗒 ...
┃ ┃ ┃ ┃ ┃ ┣ 🗒 drugwars.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 drugwars.txt
┃ ┃ ┃ ┃ ┃ ┣ 🗒 ...

#### MadDog McCree

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 maddog.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 arcadecoin.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 bottlebreak.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 bullet.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 choice.easy.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 ...
┃ ┃ ┃ ┃ ┃ ┣ 📁 cdrom
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 a_bank.dat
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 a_bank.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 a_bank.ogg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 a_barrel-front.dat
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 ...
┃ ┃ ┃ ┃ ┃ ┣ 🗒 ...
┃ ┃ ┃ ┃ ┃ ┣ 🗒 maddog.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 maddog.txt
┃ ┃ ┃ ┃ ┃ ┣ 🗒 ...

#### MadDog McCree 2

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 maddog2.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 arcadecoin.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 bottlebreak.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 bullet.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 cdrom-globals.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 ...
┃ ┃ ┃ ┃ ┃ ┣ 📁 cdrom
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 a_trailer.dat
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 a_trailer.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 a_trailer.ogg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 b_aintro.dat
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 ...
┃ ┃ ┃ ┃ ┃ ┣ 🗒 ...
┃ ┃ ┃ ┃ ┃ ┣ 🗒 maddog2.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 maddog2.txt
┃ ┃ ┃ ┃ ┃ ┣ 🗒 ...

#### MadDog II: Typing Edition

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 typing-md2.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 box.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 bullet01.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 bullet02.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 bullet03.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 bullet04.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 bullet05.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 bullet06.wav
┃ ┃ ┃ ┃ ┃ ┣ 📁 cdrom
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 a_trailer.dat
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 a_trailer.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 a_trailer.ogg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 b_aintro.dat
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 ...
┃ ┃ ┃ ┃ ┃ ┣ 🗒 clak.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 coin.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 copying
┃ ┃ ┃ ┃ ┃ ┣ 🗒 gambler.ttf
┃ ┃ ┃ ┃ ┃ ┣ 🗒 hurry3.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 m39.ttf
┃ ┃ ┃ ┃ ┃ ┣ 🗒 maddog2te.cfg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 panel.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 panel2.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 panel3.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 panel4.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 panel5.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 panel6.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 pause.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 pistol.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 shot.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 star.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 te-globals.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 te-hitbox-train.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 te-hscore.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 te-service.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 te-setuplevel.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 te-setuptype.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 te-toolbox.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 te-typewriter.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 typebackspace.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 typecarriage.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 typesingle.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 typewriterding2.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 typing-md2.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 typing-md2.txt

#### Space Pirates

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 spacepirates.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 ca.ttf
┃ ┃ ┃ ┃ ┃ ┣ 📁 cdrom
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 a_trailer.dat
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 a_trailer.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 a_trailer.ogg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 b_dc01.dat
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 ...
┃ ┃ ┃ ┃ ┃ ┣ 🗒 cdrom-globals.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 cdrom-setuplevel.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 cdrom-shipbattle.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 ...
┃ ┃ ┃ ┃ ┃ ┣ 🗒 spacepirates.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 spacepirates.txt
┃ ┃ ┃ ┃ ┃ ┣ 🗒 ...

#### The Last Bounty Hunter

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 lbh.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 bottlebreak.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 bullet2.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 captured.png
┃ ┃ ┃ ┃ ┃ ┣ 📁 cdrom
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 a_trailer.dat
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 a_trailer.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 a_trailer.ogg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 b_armybase.dat
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 ...
┃ ┃ ┃ ┃ ┃ ┣ 🗒 cdrom-globals.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 ...
┃ ┃ ┃ ┃ ┃ ┣ 🗒 lastbountyhunter.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 lastbountyhunter.txt
┃ ┃ ┃ ┃ ┃ ┣ 🗒 ...

#### Who Shot Johnny Rock?

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 johnnyrock.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 anchorsteamnf.ttf
┃ ┃ ┃ ┃ ┃ ┣ 🗒 cashregister.wav
┃ ┃ ┃ ┃ ┃ ┣ 📁 cdrom
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 a_intro.dat
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 a_intro.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 a_intro.ogg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 a_start.dat
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 ...
┃ ┃ ┃ ┃ ┃ ┣ 🗒 cdrom-globals.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 cdrom-hitbox-cans.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 ...
┃ ┃ ┃ ┃ ┃ ┣ 🗒 johnnyrock.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 johnnyrock.txt
┃ ┃ ┃ ┃ ┃ ┣ 🗒 ...

### WoW Action Max

#### 38 Ambush Alley

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 38ambushalley.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 38ambushalley.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 38ambushalley.txt
┃ ┃ ┃ ┃ ┃ ┣ 🗒 video_38ambushalley.m2v
┃ ┃ ┃ ┃ ┃ ┣ 🗒 video_38ambushalley.ogg
┃ ┃ ┃ ┃ ┣ 📁 actionmax.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 emulator.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 font_bluestone.ttf
┃ ┃ ┃ ┃ ┃ ┣ 🗒 font_chemrea.ttf
┃ ┃ ┃ ┃ ┃ ┣ 🗒 font_led_real.ttf
┃ ┃ ┃ ┃ ┃ ┣ 🗒 framework.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 sound_actionmax.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 sound_asteadyaimiscritical.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 sound_badhit.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 sound_gameover.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 sound_getreadyforaction.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 sound_goodhit.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 sound_gunshot.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 sprite_38ambushalley.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 sprite_actionmax_orig.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 sprite_actionmax.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 sprite_bullet.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 sprite_crosshair.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 sprite_lightoff.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 sprite_lighton.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 video_actionmaxintro.m2v
┃ ┃ ┃ ┃ ┃ ┣ 🗒 video_actionmaxintro.ogg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 video_menu.m2v
┃ ┃ ┃ ┃ ┃ ┣ 🗒 video_menu.ogg

>The file `38ambushalley.txt` can be named `frame_38ambushalley.txt`, do not hesitate to rename it.
>
>The `actionmax.daphne` folder won't be visible in EmulationStation, which is normal.
{.is-info}

>Be sure the file `38ambushalley.txt` have the following content to work:
>```ini
>.
>
>1       ../actionmax.daphne/video_actionmaxintro.m2v
>355     video_38ambushlley.m2v
>31835   ../actionmax.daphne/video_menu.m2v
>```
{.is-info}

#### Blue Thunder

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 bluethunder.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 bluethunder.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 bluethunder.txt
┃ ┃ ┃ ┃ ┃ ┣ 🗒 video_bluethunder.m2v
┃ ┃ ┃ ┃ ┃ ┣ 🗒 video_bluethunder.ogg
┃ ┃ ┃ ┃ ┣ 📁 actionmax.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 emulator.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 font_bluestone.ttf
┃ ┃ ┃ ┃ ┃ ┣ 🗒 font_chemrea.ttf
┃ ┃ ┃ ┃ ┃ ┣ 🗒 font_led_real.ttf
┃ ┃ ┃ ┃ ┃ ┣ 🗒 framework.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 sound_actionmax.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 sound_asteadyaimiscritical.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 sound_badhit.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 sound_gameover.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 sound_getreadyforaction.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 sound_goodhit.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 sound_gunshot.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 sprite_actionmax.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 sprite_bluethunder.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 sprite_bullet.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 sprite_crosshair.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 sprite_lightoff.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 sprite_lighton.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 video_actionmaxintro.m2v
┃ ┃ ┃ ┃ ┃ ┣ 🗒 video_actionmaxintro.ogg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 video_menu.m2v
┃ ┃ ┃ ┃ ┃ ┣ 🗒 video_menu.ogg

>The file `bluethunder.txt` can be named `frame_bluethunder.txt`, feel free to rename it.
>
>The `actionmax.daphne` folder won't be visible in EmulationStation, which is normal.
{.is-info}

>Be sure the file `bluethunder.txt` have the following content to work:
>```ini
>.
>
>1       ../actionmax.daphne/video_actionmaxintro.m2v
>355     video_bluethunder.m2v
>27950   ../actionmax.daphne/video_menu.m2v
>```
{.is-info}

#### HydroSub 2021

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 hydrosub2021.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 hydrosub2021.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 hydrosub2021.txt
┃ ┃ ┃ ┃ ┃ ┣ 🗒 video_hydrosub2021.m2v
┃ ┃ ┃ ┃ ┃ ┣ 🗒 video_hydrosub2021.ogg
┃ ┃ ┃ ┃ ┣ 📁 actionmax.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 emulator.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 font_bluestone.ttf
┃ ┃ ┃ ┃ ┃ ┣ 🗒 font_chemrea.ttf
┃ ┃ ┃ ┃ ┃ ┣ 🗒 font_led_real.ttf
┃ ┃ ┃ ┃ ┃ ┣ 🗒 framework.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 sound_actionmax.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 sound_asteadyaimiscritical.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 sound_badhit.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 sound_gameover.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 sound_getreadyforaction.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 sound_goodhit.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 sound_gunshot.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 sprite_actionmax.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 sprite_bullet.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 sprite_crosshair.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 sprite_hydrosub2021.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 sprite_lightoff.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 sprite_lighton.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 video_actionmaxintro.m2v
┃ ┃ ┃ ┃ ┃ ┣ 🗒 video_actionmaxintro.ogg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 video_menu.m2v
┃ ┃ ┃ ┃ ┃ ┣ 🗒 video_menu.ogg

>The file `hydrosub2021.txt` can be named `frame_hydrosub2021.txt`, do not hesitate to rename it.
>
>The `actionmax.daphne` folder won't be visible in EmulationStation, which is normal.
{.is-info}

>Be sure the file `hydrosub2021.txt` have the following content to work:
>```ini
>.
>
>1       ../actionmax.daphne/video_actionmaxintro.m2v
>355     video_hydrosub2021.m2v
>28820   ../actionmax.daphne/video_menu.m2v
>```
{.is-info}

#### Pops Ghostly

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 popsghostly.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 popsghostly.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 popsghostly.txt
┃ ┃ ┃ ┃ ┃ ┣ 🗒 video_popsghostly.m2v
┃ ┃ ┃ ┃ ┃ ┣ 🗒 video_popsghostly.ogg
┃ ┃ ┃ ┃ ┣ 📁 actionmax.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 emulator.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 font_bluestone.ttf
┃ ┃ ┃ ┃ ┃ ┣ 🗒 font_chemrea.ttf
┃ ┃ ┃ ┃ ┃ ┣ 🗒 font_led_real.ttf
┃ ┃ ┃ ┃ ┃ ┣ 🗒 framework.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 sound_actionmax.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 sound_asteadyaimiscritical.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 sound_badhit.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 sound_gameover.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 sound_getreadyforaction.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 sound_goodhit.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 sound_gunshot.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 sprite_actionmax.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 sprite_bullet.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 sprite_crosshair.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 sprite_lightoff.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 sprite_lighton.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 sprite_popsghostly.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 video_actionmaxintro.m2v
┃ ┃ ┃ ┃ ┃ ┣ 🗒 video_actionmaxintro.ogg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 video_menu.m2v
┃ ┃ ┃ ┃ ┃ ┣ 🗒 video_menu.ogg

>The file `popsghostly.txt` can be named `frame_popsghostly.txt`, do not hesitate to rename it.
>
>The `actionmax.daphne` folder won't be visible in EmulationStation, which is normal.
{.is-info}

>Be sure the file `popsghostly.txt` have the following content to work:
>```ini
>.
>
>1       ../actionmax.daphne/video_actionmaxintro.m2v
>355     video_popsghostly.m2v
>31835   ../actionmax.daphne/video_menu.m2v
>```
{.is-info}

#### Sonic Fury

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 sonicfury.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 sonicfury.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 sonicfury.txt
┃ ┃ ┃ ┃ ┃ ┣ 🗒 video_sonicfury.m2v
┃ ┃ ┃ ┃ ┃ ┣ 🗒 video_sonicfury.ogg
┃ ┃ ┃ ┃ ┣ 📁 actionmax.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 emulator.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 font_bluestone.ttf
┃ ┃ ┃ ┃ ┃ ┣ 🗒 font_chemrea.ttf
┃ ┃ ┃ ┃ ┃ ┣ 🗒 font_led_real.ttf
┃ ┃ ┃ ┃ ┃ ┣ 🗒 framework.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 sound_actionmax.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 sound_asteadyaimiscritical.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 sound_badhit.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 sound_gameover.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 sound_getreadyforaction.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 sound_goodhit.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 sound_gunshot.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 sprite_actionmax.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 sprite_bullet.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 sprite_crosshair.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 sprite_lightoff.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 sprite_lighton.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 sprite_sonicfury.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 video_actionmaxintro.m2v
┃ ┃ ┃ ┃ ┃ ┣ 🗒 video_actionmaxintro.ogg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 video_menu.m2v
┃ ┃ ┃ ┃ ┃ ┣ 🗒 video_menu.ogg

>The file `sonicfury.txt` can be named `frame_sonicfury.txt`, don't hesitate to rename it.
>
>The `actionmax.daphne` folder won't be visible in EmulationStation, which is normal.
{.is-info}

>Be sure the file `sonicfury.txt` have the following content to work:
>```ini
>.
>
>1       ../actionmax.daphne/video_actionmaxintro.m2v
>355     video_sonicfury.m2v
>26645   ../actionmax.daphne/video_menu.m2v
>```
{.is-info}

### Arcade

#### Ninja Hayate

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 hayate.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 1up.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 action.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdown.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowleft.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowright.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowup.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 bonus.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 button.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 freeplay.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 getready.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 hayate.cfg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 hayate.m2v
┃ ┃ ┃ ┃ ┃ ┣ 🗒 hayate.ogg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 hayate.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 hayate.txt
┃ ┃ ┃ ┃ ┃ ┣ 🗒 insertcoin.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 LICENSE.TXT
┃ ┃ ┃ ┃ ┃ ┣ 🗒 livesleft.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 num00.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 num01.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 num02.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 num03.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 num04.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 num05.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 num06.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 num07.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 num08.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 num09.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 player1.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 presstart.ttf
┃ ┃ ┃ ┃ ┃ ┣ 🗒 prompt.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 right.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 tick.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 topscore.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 victory.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 wrong.wav

#### Time Gal

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 timegal.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 action.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdown.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowleft.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowright.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowup.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 blip.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 button.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 freeplay.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 gal.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 getready.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 insertcoin.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 LICENSE.TXT
┃ ┃ ┃ ┃ ┃ ┣ 🗒 line1.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 line2.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 line3.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 line4.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 line5.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 line6.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 line100.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 line101.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 line102.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 line103.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 line104.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 line105.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 line106.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 line107.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 line108.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 line109.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 line110.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 line111.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 line112.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 line113.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 line114.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 line115.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 line116.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 line117.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 num00.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 num01.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 num02.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 num03.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 num04.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 num05.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 num06.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 num07.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 num08.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 num09.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 player1.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 presstart.ttf
┃ ┃ ┃ ┃ ┃ ┣ 🗒 question.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 right.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 ring01.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 ring02.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 selarrow.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 select.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 teedo.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 timegal.cfg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 timegal.m2v
┃ ┃ ┃ ┃ ┃ ┣ 🗒 timegal.ogg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 timegal.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 timegal.txt
┃ ┃ ┃ ┃ ┃ ┣ 🗒 topscore.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 wrong.wav

#### Time Traveler

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 timetraveler.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 action.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdown.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowleft.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowright.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowup.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 coin.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 COPYING.txt
┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 death.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 ds-digib.ttf
┃ ┃ ┃ ┃ ┃ ┣ 🗒 ds-digib.ttf.new
┃ ┃ ┃ ┃ ┃ ┣ 📁 dvd
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 vts_01_1.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 vts_01_1.ogg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 vts_02_1.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 vts_02_1.ogg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 vts_03_1.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 vts_03_1.ogg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 vts_04_1.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 vts_04_1.ogg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 vts_05_1.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 vts_05_1.ogg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 vts_06_1.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 vts_06_1.ogg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 vts_07_1.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 vts_07_1.ogg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 vts_08_1.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 vts_08_1.ogg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 vts_09_1.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 vts_09_1.ogg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 vts_10_1.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 vts_10_1.ogg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 vts_11_1.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 vts_11_1.ogg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 vts_12_1.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 vts_12_1.ogg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 vts_13_1.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 vts_13_1.ogg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 vts_14_1.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 vts_14_1.ogg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 vts_15_1.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 vts_15_1.ogg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 vts_16_1.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 vts_16_1.ogg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 vts_17_1.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 vts_17_1.ogg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 vts_18_1.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 vts_18_1.ogg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 dvd-globals.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 dvd-helltrader.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 dvd-hscore.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 dvd-map.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 dvd-play.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 dvd-service.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 dvd-setuplevel.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 dvd-shufflelevel.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 dvd-toolbox.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 htt.cfg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 m39.ttf
┃ ┃ ┃ ┃ ┃ ┣ 🗒 question.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 right.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 timetraveler.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 timetraveler.txt
┃ ┃ ┃ ┃ ┃ ┣ 🗒 victory.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 wrong.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 yellowbox.png

>The file `timetraveler.singe` can be named `dvd-timetraveler.singe`, do not hesitate to rename it.
{.is-info}
