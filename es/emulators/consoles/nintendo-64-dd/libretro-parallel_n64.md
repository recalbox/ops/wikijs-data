---
title: Libretro ParaLLEl_n64
description: 
published: true
date: 2024-07-05T14:58:20.080Z
tags: libretro, 64dd, n64dd, parallel n64
editor: markdown
dateCreated: 2023-09-07T06:42:34.093Z
---

**Libretro ParaLLEl_n64** está basado en Mupen64 Plus y ha sido especialemente reescrito/optimizado para Libretro.

## ![](/emulators/license.svg) Licencia

El core posee la siguiente licencia [**GPLv3**].

## ![](/emulators/compatibility.png) Compatibilidad

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | 
| ❌ | ✅ | ✅ | ✅ | ✅ | ❌ | ✅ | ✅ |

## ![](/emulators/features.png) Funcionalidades



## ![](/emulators/bios.svg) BIOS

### Lista de BIOS obligatorias

| Nombre del fichero | Descripción | MD5 | ¿Preinstalada? |
| :---: | :---: | :---: | :---: |
| IPL.n64 | BIOS N64DD | 8d3d9f294b6e174bc7b1d2fd1c727530 | ❌ |

### Emplazamiento

Disponed vuestras bios como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 Mupen64plus
┃ ┃ ┃ ┃ ┣ 🗒 **IPL.n64**

## ![](/emulators/roms.png) Roms

### Extensiones de roms soportadas

Las roms deben tener las extensiones siguientes:

* .bin
* .ndd

### Emplazamiento

Disponed vuestras roms como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 64dd
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.ndd**

>Se aconsejan las roms del proyecto **No-Intro**.
{.is-success}

>Para más información sobre las roms, consultad [este tutorial](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

>Para conservar las configuraciones personalizadas cuando se produce una actualización del sistema Recalbox, os aconsejamos utilizar las [Sobrecargas de configuración](./../../../advanced-usage/configuration-override).
{.is-info}

### Acceder a las opciones

Podéis configurar las opciones del core de dos formas diferentes.

* Via el Menú RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via el fichero `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Opciones del core

| Opción | Valor de la opción | Variable (para la sobrecarga) | Valores posibles |
| --- | --- | --- | --- |
| CPU Core | `dynamic_recompiler` ✅ / `cached_interpreter` / `pure_interpreter` | `parallel-n64-cpucore` | `dynamic_recompiler` / `cached_interpreter` / `pure_interpreter` |
| Audio Buffer Size (restart) | `2048` ✅ / `1024` | `parallel-n64-audio-buffer-size` | `2048` / `1024` |
| Analog Deadzone (percent) | `0` / `5` / `10` / `15` ✅ / `20` / `25` / `30` | `parallel-n64-astick-deadzone` | `0` / `5` / `10` / `15` / `20` / `25` / `30` |
| Analog Sensitivity (percent) | `50` / `55` / `60` / `65` / `70` / `75` / `80` / `85` / `90` / `95` / `100` ✅ / `105` / `110` / `115` / `120` / `125` / `130` / `135` / `140` / `145` / `150` / `200` | `parallel-n64-astick-sensitivity` | `50` / `55` / `60` / `65` / `70` / `75` / `80` / `85` / `90` / `95` / `100` / `105` / `110` / `115` / `120` / `125` / `130` / `135` / `140` / `145` / `150` / `200` |
| Player 1 Pak | `none` / `memory` ✅ / `rumble` | `parallel-n64-pak1` | `none` / `memory` / `rumble` |
| Player 2 Pak | `none` ✅ / `memory` / `rumble` | `parallel-n64-pak2` | `none` / `memory` / `rumble` |
| Player 3 Pak | `none` ✅ / `memory` / `rumble` | `parallel-n64-pak3` | `none` / `memory` / `rumble` |
| Player 4 Pak | `none` ✅ / `memory` / `rumble` | `parallel-n64-pak4` | `none` / `memory` / `rumble` |
| Enable Expansion Pak RAM | `Activé` ✅ / `Désactivé` | `parallel-n64-disable_expmem` | `enabled` / `disabled` |
| GFX Accuracy (restart) | `veryhigh` ✅ / `high` / `medium` / `low` | `parallel-n64-gfxplugin-acccuracy` | `veryhigh` / `high` / `medium` / `low` |
| ParaLLEl Synchronous RDP | `Activé` ✅ / `Désactivé` | `parallel-n64-parallel-rdp-synchronous` |  |
| (ParaLLEl-RDP) Crop pixel border pixels | `0` ✅ / `2` / `4` / `6` / `8` / `10` / `12` / `14` / `16` / `18` / `20` / `22` / `24` / `26` / `28` / `30` / `32` / `34` / `36` / `38` / `40` / `42` / `44` / `46` / `48` / `50` / `52` / `54` / `56` / `58` / `60` / `62` / `64` | `parallel-n64-parallel-rdp-overscan` | `0` / `2` / `4` / `6` / `8` / `10` / `12` / `14` / `16` / `18` / `20` / `22` / `24` / `26` / `28` / `30` / `32` / `34` / `36` / `38` / `40` / `42` / `44` / `46` / `48` / `50` / `52` / `54` / `56` / `58` / `60` / `62` / `64` |
| (ParaLLEl-RDP) VI divot filter | `Activé` ✅ / `Désactivé` | `parallel-n64-parallel-rdp-divot-filter` | `enabled` / `disabled` |
| (ParaLLEl-RDP) VI gamma dither | `Activé` ✅ / `Désactivé` | `parallel-n64-parallel-rdp-gamma-dither` | `enabled` / `disabled` |
| (ParaLLEl-RDP) VI AA | `Activé` ✅ / `Désactivé` | `parallel-n64-parallel-rdp-vi-aa` | `enabled` / `disabled` |
| (ParaLLEl-RDP) VI bilinear | `Activé` ✅ / `Désactivé` | `parallel-n64-parallel-rdp-vi-bilinear` | `enabled` / `disabled` |
| (ParaLLEl-RDP) VI dither filter | `Activé` ✅ / `Désactivé` | `parallel-n64-parallel-rdp-vi-dither-filter` | `enabled` / `disabled` |
| (ParaLLEl-RDP) Upscaling factor (restart) | `1x` ✅ / `2x` / `4x` / `8x` | `parallel-n64-parallel-rdp-upscaling` | `1x` / `2x` / `4x` / `8x` |
| (ParaLLEl-RDP) Downsampling | `Désactivé` ✅ / `1/2` / `1/4` / `1/8` | `parallel-n64-parallel-rdp-downsampling` | `disabled` / `1/2` / `1/4` / `1/8` |
| (ParaLLEl-RDP) Use native texture LOD when upscaling | `Désactivé` ✅ / `Activé` | `parallel-n64-native-texture-lod` | `disabled` / `enabled` |
| (ParaLLEl-RDP) Use native resolution for TEX_RECT | `Activé` ✅ / `Désactivé` | `parallel-n64-paral
lel-rdp-native-tex-rect` | `enabled` / `disabled` |

| Send audio lists to HLE RSP | `Désactivé` ✅ / `Activé` | `parallel-n64-send_allist_to_hle_rsp` | `disabled` / `enabled` |
| GFX Plugin | `auto` ✅ / `glide64` / `gln64` / `rice` / `angrylion` | `parallel-n64-gfxplugin` | `auto` / `glide64` / `gln64` / `rice` / `angrylion` |
| RSP Plugin | `auto` ✅ / `hle` / `cxd4` | `parallel-n64-rspplugin` | `auto` / `hle` / `cxd4` |
| Resolution (restart) | `320x240` / `640x480` ✅ / `960x720` / `1280x960` / `1440x1080` / `1600x1200` / `1920x1440` / `2240x1680` / `2880x2160` / `5760x4320` | `parallel-n64-screensize` | `320x240` / `640x480` / `960x720` / `1280x960` / `1440x1080` / `1600x1200` / `1920x1440` / `2240x1680` / `2880x2160` / `5760x4320` |
| Aspect ratio hint (reinit) | `normal` ✅ / `widescreen` | `parallel-n64-aspectratiohint` | `normal` / `widescreen` |
| (Glide64) Texture Filtering | `automatic` ✅ / `N64 3-point` / `bilinear` / `nearest` | `parallel-n64-filtering` | `automatic` / `N64 3-point` / `bilinear` / `nearest` |
| (Angrylion) Dithering | `Activé` ✅ / `Désactivé` | `parallel-n64-dithering` | `enabled` / `disabled` |
| (Glide64) Polygon Offset Factor | `-5.0` / `-4.5` / `-4.0` / `-3.5` / `-3.0` ✅ / `-2.5` / `-2.0` / `-1.5` / `-1.0` / `-0.5` / `0.0` / `0.5` / `1.0` / `1.5` / `2.0` / `2.5` / `3.0` / `3.5` / `4.0` / `4.5` / `5.0` | `parallel-n64-polyoffset-factor` | `-5.0` / `-4.5` / `-4.0` / `-3.5` / `-3.0` / `-2.5` / `-2.0` / `-1.5` / `-1.0` / `-0.5` / `0.0` / `0.5` / `1.0` / `1.5` / `2.0` / `2.5` / `3.0` / `3.5` / `4.0` / `4.5` / `5.0` |
| (Glide64) Polygon Offset Units | `-5.0` / `-4.5` / `-4.0` / `-3.5` / `-3.0` ✅ / `-2.5` / `-2.0` / `-1.5` / `-1.0` / `-0.5` / `0.0` / `0.5` / `1.0` / `1.5` / `2.0` / `2.5` / `3.0` / `3.5` / `4.0` / `4.5` / `5.0` | `parallel-n64-polyoffset-units` | `-5.0` / `-4.5` / `-4.0` / `-3.5` / `-3.0` / `-2.5` / `-2.0` / `-1.5` / `-1.0` / `-0.5` / `0.0` / `0.5` / `1.0` / `1.5` / `2.0` / `2.5` / `3.0` / `3.5` / `4.0` / `4.5` / `5.0` |
| (Angrylion) VI Overlay | `Filtered` ✅ / `AA+Blur` / `AA+Dedither` / `AA only` / `Unfiltered` / `Depth` / `Coverage` | `parallel-n64-angrylion-vioverlay` | `Filtered` / `AA+Blur` / `AA+Dedither` / `AA only` / `Unfiltered` / `Depth` / `Coverage` |
| (Angrylion) Thread sync level | `Low` ✅ / `Medium` / `High` | `parallel-n64-angrylion-sync` | `Low` / `Medium` / `High` |
| (Angrylion) Multi-threading | `all threads` ✅ / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `21` / `22` / `23` / `24` / `25` / `26` / `27` / `28` / `29` / `30` / `31` / `32` / `33` / `34` / `35` / `36` / `37` / `38` / `39` / `40` / `41` / `42` / `43` / `44` / `45` / `46` / `47` / `48` / `49` / `50` / `51` / `52` / `53` / `54` / `55` / `56` / `57` / `58` / `59` / `60` / `61` / `62` / `63` | `parallel-n64-angrylion-multithread` | `all threads` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `21` / `22` / `23` / `24` / `25` / `26` / `27` / `28` / `29` / `30` / `31` / `32` / `33` / `34` / `35` / `36` / `37` / `38` / `39` / `40` / `41` / `42` / `43` / `44` / `45` / `46` / `47` / `48` / `49` / `50` / `51` / `52` / `53` / `54` / `55` / `56` / `57` / `58` / `59` / `60` / `61` / `62` / `63` |
| (Angrylion) Hide overscan | `Désactivé` ✅ / `Activé` | `parallel-n64-angrylion-overscan` | `disabled` / `enabled` |
| VI Refresh (Overclock) | `auto` ✅ / `1500` / `2200` | `parallel-n64-virefresh` | `auto` / `1500` / `2200` |
| Buffer Swap | `Désactivé` ✅ / `Activé` | `parallel-n64-bufferswap` | `disabled` / `enabled` |
| Framerate (restart) | `original` ✅ / `fullspeed` | `parallel-n64-framerate` | `original` / `fullspeed` |
| Independant C-button Controls | `Désactivé` ✅ / `Activé` | `parallel-n64-alt-map` | `disabled` / `enabled` |
| (Glide64) Vertex cache VBO (restart) | `Désactivé` ✅ / `Activé` | `parallel-n64-vcache-vbo` | `disabled` / `enabled` |
| Boot Device | `Default` / `64DD IPL` ✅ | `parallel-n64-boot-device` | `Default` / `64DD IPL` |
| 64DD Hardware | `Désactivé` / `Activé` ✅ | `parallel-n64-64dd-hardware` | `disabled` / `enabled` |

## ![](/emulators/external-links.png) Enlaces externos

* **Código fuente utilizado** : [https://github.com/libretro/parallel-n64/](https://github.com/libretro/parallel-n64/)
* **Documentación Libretro** : -
