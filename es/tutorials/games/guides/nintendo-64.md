---
title: Nintendo 64
description: 
published: true
date: 2024-07-26T07:04:17.187Z
tags: n64, nintendo, 64, juegos
editor: markdown
dateCreated: 2023-09-14T07:56:03.562Z
---

Con los tutoriales de esta sección, podréis averiguar cómo cambiar el sonido a la salida de auriculares si el sistema se niega, o qué hacer si un juego no arranca.

Aqui tenéis la lista de tutoriales disponibles:

[Compatibilidad de roms con los cores](game-core-compatibility)
[No hay sonido en la salida jack con mupen64plus](no-sound-on-jack-with-mupen64plus)
[Regresar a EmulationStation](return-to-emulationstation-with-mupen64plus)
[Texturas de alta resolución](high-resolution-textures)
[Varios packs de texturas de alta resolución diferentes sobre el mismo juego](load-differents-high-resolution-texture-packs-on-same-game)