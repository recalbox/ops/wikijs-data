---
title: Configuration de l'extension YouTube
description: Ou comment pouvoir voir les vidéos de YouTube sans problème !
published: true
date: 2023-02-21T19:57:16.287Z
tags: configuration, kodi, extensions, youtube
editor: markdown
dateCreated: 2022-12-20T21:07:21.534Z
---

## Introduction

Dans Kodi, l'extension Youtube est déjà pré-installée. Cependant, vous devez le configurer pour pouvoir vous identifier et retrouver tout votre compte (vidéos favorites, abonnements, etc.).

## Procédure

### Pré-requis

- Avoir un compte Google / YouTube.
- Avoir Recalbox 9.0. Le faire sur une version précédente **peut** fonctionner mais n'est pas garanti à 100%.

### Configuration

#### Création des clés API / OAuth

##### Création du projet

1. Rendez-vous sur https://console.cloud.google.com/ et cliquez sur `Nouveau Projet`. Ce bouton se trouve en haut à gauche de la fenêtre.

>Si vous avez déjà des projets, cliquez sur la liste des projets.
>{.is-info}

2. Cliquez sur `Nouveau Projet`.

3. Renseignez le nom du projet et cliquez sur `Créer`. Vous pouvez lui donner le nom que vous souhaitez.

![](/tutorials/kodi/youtube/ytb1fr.png){.full-width}

##### Ajout de la bibliothèque YouTube

4. Une fois le projet crée, cliquez sur l'icône avec les 3 traits horizontaux pour ouvrir le menu, puis allez dans `API et services` > `Bibliothèque`.

![](/tutorials/kodi/youtube/ytb2fr.png){.full-width}

5. Sur cette page, sélectionnez le projet créé précédemment puis recherchez `YouTube Data API v3` et cliquez dessus.

![](/tutorials/kodi/youtube/ytb3fr.png){.full-width}

6. Sur la page de la bibliothèque, cliquez sur `Activer`.

![](/tutorials/kodi/youtube/ytb4fr.png){.full-width}

##### Génération de la clé API

7. Ouvrez le menu de l'étape 4 et allez dans `API et services` > `Identifiants`.

![](/tutorials/kodi/youtube/ytb5fr.png){.full-width}

8. Dans la liste des identifiants, en haut de page, cliquez sur `Créer des identifiants` > `Clé API`.

![](/tutorials/kodi/youtube/ytb6fr.png){.full-width}

9. Une popup sera visible au bout de quelques secondes vous donnant une clé API généré. Copiez cette clé API et gardez-là de côté.

![](/tutorials/kodi/youtube/ytb7fr.png){.full-width}

##### Écran de consentement OAuth

10. À gauche, cliquez sur `Écran de consentement OAuth`.

![](/tutorials/kodi/youtube/ytb8fr.png){.full-width}

11. Indiquez le type d'utilisateur `Externes` et cliquez sur `Créer`.

![](/tutorials/kodi/youtube/ytb9fr.png){.full-width}

12. Ici, renseignez le `nom de l'application`, `l'adresse e-mail d'assistance utilisateur` ainsi qu'une `adresse e-mail` tout en bas de page et cliquez sur `Enregistrer et continuer`.

![](/tutorials/kodi/youtube/ytb10fr.png){.full-width}

>Les 2 adresses e-mails peuvent être différentes.
>{.is-info}

13. Retournez dans `Écran de consentement OAuth` et créez un utilisateur test en cliquant sur `Add users`, puis en renseignant une adresse e-mail.

![](/tutorials/kodi/youtube/ytb11fr.png){.full-width}

14. Dans la même page, n'oubliez pas de cliquer sur `PUBLIER L'APPLICATION`.

![](/tutorials/kodi/youtube/ytb12fr.png){.full-width}

##### Génération de la clé OAuth

15. À gauche, cliquez sur `Identifiants` puis, en haut de page, cliquez sur `Créer des identifiants` et sélectionnez cette fois-ci `ID client OAuth`.

![](/tutorials/kodi/youtube/ytb13fr.png){.full-width}

16. Vous devez configurer le type d'application afin d'obtenir l'ID client OAuth. Sélectionnez `Périphériques d'entrée TV et limitée` et cliquez sur `Créer`.

![](/tutorials/kodi/youtube/ytb14fr.png){.full-width}

>Le nom donné à l'application sur la même page peut être modifié comme être laissé tel quel.
>{.is-info}

![](/tutorials/kodi/youtube/ytb15fr.png){.full-width}

17. Vous obtiendrez l'`ID client` ainsi que le `code secret du client`. Gardez-les aussi de côté.

![](/tutorials/kodi/youtube/ytb16fr.png){.full-width}

#### Configuration de YouTube dans Kodi

>Les étapes suivantes peuvent différer si vous démarrez Kodi ou Youtube pour la première fois.
>{.is-info}

1. Lancez Kodi.

2. Allez dans `Extensions` > `YouTube`.

![](/tutorials/kodi/youtube/ytb17fr.png){.full-width}

3. Dans la liste visible à l'écran, choisissez `Paramètres` tout en bas.

![](/tutorials/kodi/youtube/ytb18fr.png){.full-width}

4. Dans les paramètres, choisissez à gauche `API`.

![](/tutorials/kodi/youtube/ytb19fr.png){.full-width}

5. Au milieu, modifiez chaque ligne comme suit :

    1. Clé de l'API : la clé API que vous avez généré en premier.

    2. ID de l'API : l'ID client que vous avez obtenu en second. Ne rentrez pas la partie `.apps.googleusercontent.com`, elle ne sert pas.

    3. API secret : le code secret du client que vous avez eu en dernier.

6. Une fois ces 3 informations fournies, cliquez sur `OK` de la fenêtre des paramètres.

7. Une popup va vous demander si vous souhaitez lancer l'assistance de démarrage. Répondez `Oui`.

![](/tutorials/kodi/youtube/ytb20fr.png){.full-width}

8. Sélectionnez votre langue et votre région.

![](/tutorials/kodi/youtube/ytb21fr.png){.full-width}

9. Sélectionnez si vous souhaitez utiliser votre géolocalisation.

![](/tutorials/kodi/youtube/ytb22fr.png){.full-width}

10. Choisissez `Suggestions de vidéos`. Si une liste de vidéos est visible, tout devrait fonctionner après que votre identification sera faite.

![](/tutorials/kodi/youtube/ytb23fr.png){.full-width}

#### Identification

1. Choisissez `Connexion`. Une popup vous indiquera de vous identifier 2 fois.

![](/tutorials/kodi/youtube/ytb24fr.png){.full-width}

2. Allez sur https://google.com/device et donnez le premier code que Kodi vous donnera.

![](/tutorials/kodi/youtube/ytb25fr.png){.full-width}

3. Sur votre navigateur, choisissez votre compte YouTube et validez les autorisations.

![](/tutorials/kodi/youtube/ytb26fr.png){.full-width}

4. Une fois la première validation effectuée, vous devez retourner sur la même adresse avec un second code que Kodi vous fournit toujours et suivre la même procédure.

![](/tutorials/kodi/youtube/ytb27fr.png){.full-width}

>Vous allez avoir une fenêtre vous indiquant que la page est dangereuse. Continuez malgré tout pour pouvoir finir de vous identifier.
>{.is-info}

5. Une fois ces 2 validations effectuées, vous êtes connecté et pouvez profiter de YouTube sur Kodi sans publicité !

![](/tutorials/kodi/youtube/ytb28fr.png){.full-width}