---
title: Libretro PX68k
description: 
published: true
date: 2024-06-22T23:24:47.436Z
tags: libretro, x68000, px68k
editor: markdown
dateCreated: 2021-05-21T08:18:07.836Z
---



## ![](/emulators/license.svg) Licence

Ce core est sous licence [**GPLv2**](https://github.com/libretro/px68k-libretro/blob/master/COPYING).

## ![](/emulators/compatibility.png) Compatibilité

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Fonctionnalités

| Fonctionnalité | Supporté |
| :---: | :---: |
| Redémarrage  | ✔ |
| Captures d'écran | ✔ |
| Options du core | ✔ |
| Contrôles | ✔ |
| Remapping | ✔ |
| Contrôle du disque | ✔ |

## ![](/emulators/bios.svg) BIOS

### Liste des bios obligatoires

| Nom de fichier | Description | MD5 | Fourni |
| :---: | :---: | :---: | :---: |
| iplrom.dat | BIOS X68000 | 7fd4caabac1d9169e289f0f7bbf71d8e | ✅ |
| cgrom.dat | Fichier police | cb0a5cfcf7247a7eab74bb2716260269 | ✅ |

### Emplacement

Placez les BIOS comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 keropi
┃ ┃ ┃ ┃ ┣ 🗒 **iplrom.dat**
┃ ┃ ┃ ┃ ┣ 🗒 **cgrom.dat**

### Liste des bios optionnels

| Nom de fichier | Description | MD5 | Fourni |
| :---: | :---: | :---: | :---: |
| iplrom30.dat | BIOS X68000 #2 | f373003710ab4322642f527f567e020a | ❌ |
| iplromco.dat | BIOS X68000 #3 | cc78d4f4900f622bd6de1aed7f52592f | ❌ |
| iplromxv.dat | BIOS X68000 #4 | 0617321daa182c3f3d6f41fd02fb3275 | ❌ |

### Emplacement

Placez les bios comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 keropi
┃ ┃ ┃ ┃ ┣ 🗒 **iplrom30.dat**
┃ ┃ ┃ ┃ ┣ 🗒 **iplromco.dat**
┃ ┃ ┃ ┃ ┣ 🗒 **iplromxv.dat**

## ![](/emulators/roms.png) Roms

### Extensions supportées

Les roms doivent avoir les extensions suivantes :

* .dim
* .img
* .d88
* .88d
* .hdm
* .dup
* .2hd
* .xdf
* .hdf
* .cmd
* .m3u
* .zip
* .7z

Ce système supporte les roms compressées au format .zip/.7z. Attention toutefois, il ne s'agit que d'une archive.

Les fichiers contenus dans les .zip/.7z doivent correspondre aux extensions citées précédemment.
De plus, chaque fichier .zip/.7z ne doit contenir qu'une seule rom compressée.

### Emplacement

Placez les roms comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 x68000
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.zip**

>Les roms au format **TOSEC** sont vivement conseillées.
{.is-success}

>Pour plus d'information sur les roms, rendez-vous sur [ce tutoriel](/fr/tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuration avancée de l'émulateur

>Pour pouvoir conserver vos configurations personnalisées lors d'une mise à jour, nous vous conseillons d'utiliser notre fonctionnalité [Surcharges de configuration](./../../../advanced-usage/configuration-override).
{.is-info}

### Accéder aux options

Vous pouvez configurer diverses options de deux façons différentes.

* Via le menu RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via le fichier `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Options du core

| Options | Valeur des options | Variable (pour surcharge) | Valeurs des variables |
| --- | --- | --- | --- |
| Taille de la police du menu | `Normal` ✅ / `Grande` | `px68k_menufontsize` | `normal` / `large` |
| Vitesse du CPU | `10MHz` ✅ / `16MHz` / `25MHz` / `33MHz (OC)` / `66MHz (OC)` / `100MHz (OC)` / `150MHz (OC)` / `200MHz (OC)` | `px68k_cpuspeed` | `10MHz` / `16MHz` / `25MHz` / `33MHz (OC)` / `66MHz (OC)` / `100MHz (OC)` / `150MHz (OC)` / `200MHz (OC)` |
| Taille de la RAM (Redémarrage) | `1MB` / `2MB` / `3MB` / `4MB` ✅ / `5MB` / `6MB` / `7MB` / `8MB` / `9MB` / `10MB` / `11MB` / `12MB` | `px68k_ramsize` | `1MB` / `2MB` / `3MB` / `4MB` / `5MB` / `6MB` / `7MB` / `8MB` / `9MB` / `10MB` / `11MB` / `12MB` |
| Utiliser l'analogique | `Désactivé` ✅ / `Activé` | `px68k_analog` | `disabled` / `enabled` |
| Type de manette du joueur 1 | `Défaut` ✅ / `CPSF-MD (8 Boutons)` / `CPSF-SFC (8 Boutons)` | `px68k_joytype1` | `Default` / `CPSF-MD (8 Buttons)` / `CPSF-SFC (8 Buttons)` |
| Type de manette du joueur 2 | `Défaut` ✅ / `CPSF-MD (8 Boutons)` / `CPSF-SFC (8 Boutons)` | `px68k_joytype2` | `Default` / `CPSF-MD (8 Buttons)` / `CPSF-SFC (8 Buttons)` |
| Mappage de la manette du J1 | `Défaut` ✅ / `XF1` / `XF2` / `XF3` / `XF4` / `XF5` / `OPT1` / `OPT2` / `F1` / `F2` | `px68k_joy1_select` | `Default` / `XF1` / `XF2` / `XF3` / `XF4` / `XF5` / `OPT1` / `OPT2` / `F1` / `F2` |
| Volume ADPCM | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` ✅ | `px68k_adpcm_vol` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` |
| Volume OPM | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` ✅ / `13` / `14` / `15` | `px68k_opm_vol` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` |
| Volume Mercury | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` ✅ / `14` / `15` | `px68k_mercury_vol` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` |
| Échange de disques sur le lecteur | `FDD1` ✅ / `FDD0` | `px68k_disk_drive` | `FDD1` / `FDD0` |
| Enregistrer les chemins d'accès aux disquettes | `Désactivé` / `Activé` ✅ | `px68k_save_fdd_path` | `disabled` / `enabled` |
| Enregistrer les chemins d'accès aux disques durs | `Désactivé` / `Activé` ✅ | `px68k_save_hdd_path` | `disabled` / `enabled` |
| Vibrer la manette pendant le lecture des disquettes | `Désactivé` ✅ / `Activé` | `px68k_rumble_on_disk_read` | `disabled` / `enabled` |
| Manette/souris | `Mouse` ✅ / `Joystick` | `px68k_joy_mouse` | `Mouse` / `Joystick` |
| Echange des boutons | `BOUTON1 BOUTON2` ✅ / `BOUTON2 BOUTON1` | `px68k_vbtn_swap` | `TRIG1 TRIG2` / `TRIG2 TRIG1` |
| Mode sans attente | `Désactivé` ✅ / `Activé` | `px68k_no_wait_mode` | `disabled` / `enabled` |
| Saut d'images | `Toutes les images` ✅ / `1/2 image` / `1/3 image` / `1/4 image` / `1/5 image` / `1/6 image` / `1/8 image` / `1/16 image` / `1/32 image` / `1/60 image` / `Saut d'image automatique` | `px68k_frameskip` | `Full Frame` / `1/2 Frame` / `1/3 Frame` / `1/4 Frame` / `1/5 Frame` / `1/6 Frame` / `1/8 Frame` / `1/16 Frame` / `1/32 Frame` / `1/60 Frame` / `Auto Frame Skip` |
| Pousser la vidéo avant l'audio | `Désactivé` ✅ / `Activé` | `px68k_push_video_before_audio` | `disabled` / `enabled` |
| Ajuster les fréquences d'images | `Désactivé` / `Activé` ✅ | `px68k_adjust_frame_rates` | `disabled` / `enabled` |
| Hack de désynchronisation de l'audio | `Désactivé` ✅ / `Activé` | `px68k_audio_desync_hack` | `disabled` / `enabled` |

## ![](/emulators/external-links.png) Liens externes

* **Code source utilisé** : [https://github.com/libretro/px68k-libretro/](https://github.com/libretro/px68k-libretro/)
* **Documentation Libretro** : [https://docs.libretro.com/library/px68k/](https://docs.libretro.com/library/px68k/)