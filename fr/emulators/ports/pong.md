---
title: Pong
description: 
published: true
date: 2023-09-19T21:51:04.064Z
tags: ports, gong, 8.0+
editor: markdown
dateCreated: 2021-11-02T09:58:57.425Z
---

![](/emulators/ports/pong.svg){.align-center}

## Fiche technique

* **Développeur** : Dan Zaidan
* **Année de sortie** : 2018

## Présentation

Pong est un des premiers jeux vidéo d'arcade et le premier jeu vidéo d'arcade de sport. Il a été imaginé par l'Américain Nolan Bushnell et développé par Allan Alcorn, et la société Atari le commercialise à partir de novembre 1972. Bien que d'autres jeux vidéo aient été inventés précédemment, comme Computer Space, Pong est le premier à devenir populaire.

Le jeu est inspiré du tennis de table en vue de dessus, et chaque joueur s'affronte en déplaçant la raquette virtuelle de haut en bas, via un bouton rotatif, de façon à garder la balle dans le terrain de jeu. Le joueur peut changer la direction de la balle en fonction de l'endroit où celle-ci tape sur la raquette, alors que sa vitesse augmente graduellement au cours de la manche. Un score est affiché pour la partie en cours et des bruitages accompagnent la frappe de la balle sur les raquettes.

Pong est à l'origine un exercice demandé par Bushnell à Alcorn en guise d'entraînement. Une version similaire a été créée précédemment par Ralph Baer pour la console de jeu Odyssey de Magnavox, mais son existence reste peu connue. Surpris par la qualité du résultat, Bushnell et Ted Dabney, fondateurs d'Atari, décident de commercialiser le jeu dès 1972. La copie du concept entraîne d'ailleurs une poursuite en justice de Magnavox contre Atari pour violation de brevet en 1976.

Mise sur le marché fin 1972, la borne d'arcade est un succès : elle est vendue à près de 8 000 exemplaires l'année suivante et engrange jusqu'à 40 millions de dollars de chiffres d'affaires en 1975, dépassant toutes les prédictions de Bushnell et Dabney. Ce succès incite de nombreuses sociétés à se lancer dans le jeu vidéo en copiant le concept, notamment sur console de salon. À la suite des bons chiffres de la borne d'arcade et de l'engouement généré par les concurrents pour le jeu de salon, Pong est porté sur une console de salon dédiée, sous le nom Home Pong à partir de 1975, commercialisée par Sears puis directement par Atari un an après. Ce double succès est considéré comme l'évènement précurseur de l'industrie du jeu vidéo, avec une forte augmentation de l'offre, des centaines de consoles de salon reprenant le concept.

## Émulateurs

[Libretro Gong](libretro-gong)