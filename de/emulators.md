---
title: 5. 🛠️ EMULATOREN
description: 
published: true
date: 2023-08-31T12:59:08.035Z
tags: 
editor: markdown
dateCreated: 2021-06-30T16:08:51.204Z
---

Du bist jetzt im anspruchsvolleren Teil der gesamten Dokumentation angelangt, der alle in Recalbox enthaltenen Spielsysteme betrifft: 

* [Arcade](arcade)
* [Konsolen](consoles)
* [Fantasy](fantasy)
* [Spiel-Engines](engines)
* [Computer](computers)
* [Notebooks](handhelds)
* [Ports](ports)

Hier ist die [Liste der eingeschlossenen Spiele](included-games) in der neuesten Version von Recalbox.