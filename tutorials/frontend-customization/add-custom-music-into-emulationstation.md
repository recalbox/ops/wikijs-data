---
title: Ajouter une musique personnalisée à EmulationStation
description: 
published: true
date: 2021-09-04T18:05:19.044Z
tags: emulationstation, musique, personnalisée
editor: markdown
dateCreated: 2021-05-21T08:07:04.585Z
---

Vous pouvez ajouter vos propres musiques pour y être jouées dans EmulationStation.

## Procédure

* Vous devez avoir un accès à votre système :
  * Via [WinSCP](./../system/access/network-access-winscp)
  * Via [Cyberduck](./../system/access/network-access-cyberduck)
  * Via [MobaXTerm](./../system/access/network-access-mobaxterm)
* Une fois connecté, vous pouvez mettre vos musiques dans le répertoire `/recalbox/share/music`
* Une fois mis en place, votre musique sera lue aléatoirement dans EmulationStation sans avoir besoin de redémarrer.

## Voir aussi

Mettez vos musiques favorites sur EmulationStation [en suivant le pas à pas de Fabrice](https://www.youtube.com/watch?v=uu40MDzBdJM).

![https://www.youtube.com/watch?v=uu40MDzBdJM](/tutorials/frontend-customization/add-music-into-emulationstation/changer-la-musique-sur-es.png)

![Rendez-vous 2:42 pour les explications.](/tutorials/frontend-customization/add-music-into-emulationstation/ajouter-des-musiques-sur-emulationstation.png)