---
title: Clrmamepro
description: 
published: true
date: 2023-09-16T21:29:00.053Z
tags: clrmamepro
editor: markdown
dateCreated: 2023-09-16T21:29:00.053Z
---

En este tutorial, os enseñaremos cómo utilizar Clrmamepro para extraer (reconstruir) o escanear un romset de arcade en sólo unos clics. 

## I - Antes de empezar

### 1 - Elementos necesarios

Primero necesitas :

* El software Clrmamepro [aquí](https://mamedev.emulab.it/clrmamepro/).
* Un [romset MAME](./../../../advanced-usage/arcade-in-recalbox) que corresponda a tu emulador.
* Samples (opcionales dependiendo del romset). 
* Los [CHDs](./../../../advanced-usage/arcade-in-recalbox) (opcional dependiendo del romset).
* Un fichero .DAT o .XML en la carpeta [share](./../../../basic-usage/file-management)/bios de vuestra Recalbox.

### 2 - Definiciones

* **Set**: Fichero comprimido (.zip/.7z/.rar) que contiene juegos, drivers y bios.
* **Rom**: Fichero contenido en un set.

![](/tutorials/utilities/rom-management/clrmamepro/clrmamepro1.png)

* **¿Qué es un fichero .dat?**: Los ficheros dat son ficheros de texto que contienen información sobre los juegos emulados con una versión específica del emulador MAME.  En estos ficheros dat, encontramos información para juegos particulares como: nombre de la rom, año, fabricante, información de fusión, rom padre o clon, tamaño de la rom, CRC para cada rom. CRC es un algoritmo utilizado para comprobar la integridad de un fichero. clrmamepro lo utiliza para verificar las rom-sets.
* **ClrmamePro**: Este software te permite comprobar y reconstruir tus romsets arcade basándose en un fichero con información en formato xml o dat.

### 3 - Instalación

#### Windows

Descargad ClrmamePro para [Windows](http://mamedev.emulab.it/clrmamepro/#downloads)

#### macOS

Descargad ClrmamePro para [macOS](http://www.emulab.it/)

#### Linux

Descargad ClrmamePro para Linux con [Wine](https://doc.ubuntu-fr.org/wine).

>El fichero zip es una versión portátil.
{.is-info}

* Instaladlo.
* Obtened el fichero dat para el emulador arcade que os interesa.
* Lanzad Clrmamepro:
  * Ejecutad clrmamepro haciendo click derecho sobre el icono.
  * Luego "Ejecutar como administrador".

>**Trucos bajo Windows** :
>
>* Id al ejecutable cmpro64.exe o cmpro32.exe
>* A continuación, pulsad con el botón derecho del ratón
>* Seleccionad _Propiedades_.
>* Id a la pestaña _Compatibilidad_.
>* Marcad la casilla "_Ejecutar como administrador"_.
>* Validad con Ok.
>
>El programa se ejecutará a partir de ahora y para siempre como administrador.
{.is-success}

## II - Utilización

### 1 - Descriptivo de funciones

![](/tutorials/utilities/rom-management/clrmamepro/clrmamepro2.png){.full-width}

### 2 - Rebuilder (reconstruire)

* Arrancad clrmamepro.

![](/tutorials/utilities/rom-management/clrmamepro/clrmamepro3.png){.full-width}

* Para seleccionar vuestro fichero .dat pulsad "Add DatFile".

![](/tutorials/utilities/rom-management/clrmamepro/clrmamepro4.png){.full-width}

* Sélectionnad vuestro fichero + "abrir".

![](/tutorials/utilities/rom-management/clrmamepro/clrmamepro5.png){.full-width}

* Una vez seleccionado, podéis especificar una carpeta o subcarpeta. Al seleccionar "ok" clrmamepro creará automáticamente una carpeta "NEW DATFILES".

![](/tutorials/utilities/rom-management/clrmamepro/clrmamepro6.png){.full-width}

* Seleccionad vuestro fichero.dat + "Load/Update" para cargarlo

![](/tutorials/utilities/rom-management/clrmamepro/clrmamepro7.png){.full-width}

* Clrmamepro os permite crear o actualizar la configuración. Os aconsejamos que pulséis en "Predeterminado".

![](/tutorials/utilities/rom-management/clrmamepro/clrmamepro8.png){.full-width}

* Clrmamepro va leer y cargar vuestro fichero y después deberíais encontraros en el menú general.
  * Para extraier un romset pulsad en "Rebuilder"

![](/tutorials/utilities/rom-management/clrmamepro/clrmamepro9.png)

* En la "source" hay que indicar el directorio de vuestro romset MAME.

![](/tutorials/utilities/rom-management/clrmamepro/clrmamepro10.png){.full-width}

* Seleccionad el directorio donde se encuentra vuestro romset + "OK".

![](/tutorials/utilities/rom-management/clrmamepro/clrmamepro11.png){.full-width}

* En "destination" debéis indicar un directorio para recuperar el romset extraído.

![](/tutorials/utilities/rom-management/clrmamepro/clrmamepro12.png){.full-width}

* Seleccionad el directorio de destino + "OK".

![](/tutorials/utilities/rom-management/clrmamepro/clrmamepro13.png){.full-width}

* **En el menú Rebuilder, podéis configurar** :

  * 1 - [**El tipo de romset**](./../../../advanced-usage/arcade-in-recalbox) que vais a crear **(Merge Options)** :

    * Non-merged
    * Split
    * Merged

  * 2 - **Las opciones** : (Os aconsejamos dejar los valores por defecto)

    * **Compress Files** : opciones de compresión (.zip/.7z/.rar).
    * **Recompress Files** : opcoines de recompresion, si queréis o no re-comprimir los ficheros.
    * **Show Statistics** : para ver los resultados al final del Rebuild.
    * **Remove Matched Sourcefiles** : es esta opción está activa, clrmamepro extraerá y borrará los ficheros corespondientes con el fichero .DAT de vuestro Romset MAME.
    * **Systems** y **Advanced** : no toquéis aquí por el momento.

* Por último, iniciad la extracción haciendo clic en "Reconstruir".

![](/tutorials/utilities/rom-management/clrmamepro/clrmamepro14.png){.full-width}

* Dejad funcionar clrmamepro hasta el final.

![](/tutorials/utilities/rom-management/clrmamepro/clrmamepro15.png){.full-width}

* ¡Y ya está! Os recomendamos escanear la carpeta de destino para aseguraros de todo está bien🙂.

![](/tutorials/utilities/rom-management/clrmamepro/clrmamepro16.png){.full-width}

### 3 - Scanner (Verificación)

* Arrancad clrmamepro.

![](/tutorials/utilities/rom-management/clrmamepro/clrmamepro3.png){.full-width}

* Para seleccionar el fichero .dat, pulsad en "Add DatFile".

![](/tutorials/utilities/rom-management/clrmamepro/clrmamepro4.png){.full-width}

* Seleccionad el fichero + "Abrir".

![](/tutorials/utilities/rom-management/clrmamepro/clrmamepro5.png){.full-width}

* Una vez seleccionado, podéis especificar una carpeta o subcarpeta. Al seleccionar "ok", clrmamepro creará automáticamente una carpeta "NEW DATFILES".

![](/tutorials/utilities/rom-management/clrmamepro/clrmamepro6.png){.full-width}

* Seleccionad vuestro fichero .dat + "Load/Update" para cargarlo.

![](/tutorials/utilities/rom-management/clrmamepro/clrmamepro7.png){.full-width}

* Clrmamepro os permite crear o actualizar la configuración. Os aconsejamos pulsad en "Predeterminado".

![](/tutorials/utilities/rom-management/clrmamepro/clrmamepro8.png){.full-width}

* Clrmamepro leerá y cargará el fichero y al final volveréis al menú principal. Para indicar dónde está vuestra romset, pulsad en "Configuración".

![](/tutorials/utilities/rom-management/clrmamepro/clrmamepro17.png)

* La ruta de vuestro romset en "ROM-Paths" haciendo click en "Add".

![](/tutorials/utilities/rom-management/clrmamepro/clrmamepro18.png){.full-width}

* Seleccionad el directorio donde se encuentra el romset que queréis scanear + "OK". 

![](/tutorials/utilities/rom-management/clrmamepro/clrmamepro19.png){.full-width}

* Para Mame, si el romset contiene samples, hay que especificar su ruta de la misma forma dentro de "Sample-Paths".

![](/tutorials/utilities/rom-management/clrmamepro/clrmamepro20.png){.full-width}

* Grabad las rutas pulsado en "Save As Def.". 

![](/tutorials/utilities/rom-management/clrmamepro/clrmamepro21.png){.full-width}

* Clrmamepro os confirma que la operación se ha tenido en cuenta, pulsad en "Aceptar".

![](/tutorials/utilities/rom-management/clrmamepro/clrmamepro22.png){.full-width}

* Podéis cerrar esta ventana.

![](/tutorials/utilities/rom-management/clrmamepro/clrmamepro23.png){.full-width}

* Una vez de vuelta al menú principal, pulsad en "Scanner".

![](/tutorials/utilities/rom-management/clrmamepro/clrmamepro24.png)

* En el menú **Scanner** hay que configurar:

  * 1 - **Los elementos a  scanear (You want to scan)** :

    * Sets (bios/juegos/drivers)
    * Roms (fichas que componen cada Set)
    * Samples (Drivers complementarios para ciertos juegos Mame)
    * [CHDs](./../../../advanced-usage/arcade-in-recalbox) (que habéis añadido a vuestro romset)

  * 2 - [**El tipo de romset**](./../../../advanced-usage/arcade-in-recalbox) que poseéis **(You prefer)** :

    * Non-merged
    * Split
    * Merged

  * 3 - **Los mensajes durante y después del scan(Prompts)** :

    * Ask Before Fixing
    * Add/Show Statistics

  * 4 - **Comprobar/Arreglar** : Clrmamepro comprobará lo que hay en la columna de la izquierda "comprobar". Si queréis que clrmamepro haga correcciones, podéis marcar las casillas de la columna derecha "Fix". Tened cuidado con la opción "fix", clrmamepro eliminará las carpetas y ficheros que no correspondan con vuestro DAT y los colocará en el "backup".

  * 5 - **Opciones** : No toquéis aquí por el momento

Lanzad finalmente el scan pulsado en "New Scan"

![](/tutorials/utilities/rom-management/clrmamepro/clrmamepro25.png){.full-width}

* En esta ventana se van a mostrar los diferentes problemas de vuestro set. (Ejemplo: missing rom) Podéis exportar una lista de todo lo que os falta dentro de la pestaña "Miss List" para poder completarlo más adelante.

![](/tutorials/utilities/rom-management/clrmamepro/clrmamepro26.png){.full-width}

* Por último, veréis los resultados del escáner en la ventana de "estadísticas". La parte más importante es la sección "Missing" de la parte de abajo, donde no falta nada, por lo que todo es perfecto.

![](/tutorials/utilities/rom-management/clrmamepro/clrmamepro27.png){.full-width}

#### A - Set Information

En la ventana que lista los juegos que faltan, hay un botón en la parte inferior izquierda llamado **Set Information**. Haciendo clic en él, podéis separar los juegos que queréis conservar de los que no. A continuación os mostraremos un ejemplo para borrar todos los clones.

* En el campo de texto **Select Sets**, introduce `%c=?*`.
* Pulsad el botón **Invert** de la derecha.
* Cerrad la ventana.
* Con la ventana **Scanner** todavía abierta, pulsad en **Advanced**.
* En esta nueva ventana, marcad la casilla **Mark disabled sets as unneeded**.
* Cerrad la ventana.

Ahora podéis volver a escanear vuestras roms, lo que verificará unicamente las roms padre.

Para saber más sobre esta opción y otras posibilidades de filtrado, visitad [esta página](https://www.emulab.it/forum/index.php?topic=3565.0).