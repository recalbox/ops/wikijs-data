---
title: Libretro-same_cdi
description: 
published: true
date: 2024-06-30T15:44:25.047Z
tags: libretro, cdi, 9.1+, same_cdi
editor: markdown
dateCreated: 2023-09-06T09:07:12.726Z
---

**Libretro-same_cdi** es un core derivado de MAME. La parte "same" significa S(ingle) A(rcade) M(achine) E(mulator) (emulador simple de máquinas recreativas).

## ![](/emulators/license.svg) Licencia

El core posee la siguiente licencia **[GPLv2](https://github.com/libretro/same_cdi/blob/master/COPYING)**.

## ![](/emulators/compatibility.png) Compatibilidad

 | RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
 | ❌ | ❌ | ❌ | ❌ | ❌ | ❌ | ✅ | ❌ |

## ![](/emulators/features.png) Funcionalidades

| Funcionalidad | ¿Soportada? |
| :---: | :---: |
| Reinicio | ✔ |
| Capturas de pantalla | ✔ |
| Grabaciones | ✔ |
| Opciones del core | ✔ |
| Controles | ✔ |
| Remappage | ✔ |

## ![](/emulators/bios.svg) BIOS

### Lista de BIOS obligatorias

| Nombre del fichero | Descripción | MD5 | ¿Preinstalada? |
| :---: | :---: | :---: | :---: |
| cdibios.zip |  | 9fd6b277cd877d51426427a33d1d19bb | ❌ |
| cdimono1.zip | CD-i (Mono-I) (PAL) | ce590c4787460161359f2b680136cb60 bbe6636ac11d3744f0710e8e820187e7 | ❌ |
| cdimono2.zip |  | 2ac8cd5bb23287e022ca17d616949255 | ❌ |

### Emplazamiento

Disponed vuestras bios como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 cdi
┃ ┃ ┃ ┃ ┣ 🗒 **cdibios.zip**
┃ ┃ ┃ ┃ ┣ 🗒 **cdimono1.zip**
┃ ┃ ┃ ┃ ┣ 🗒 **cdimono2.zip**

## ![](/emulators/isos.svg) Isos

### Extensiones de roms soportadas

Las roms deben tener las extensiones siguientes:

* .chd
* .cue
* .iso

### Emplazamiento

Disponed vuestras isos como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 cdi
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.chd**

>Se aconsejan las isos del projecto **Redump**.
{.is-success}

>Para más información, consultad [este tutorial](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

>Para conservar las configuraciones personalizadas cuando se produce una actualización del sistema Recalbox, os aconsejamos utilizar las [Sobrecargas de configuración](./../../../advanced-usage/configuration-override).
{.is-info}

### Acceder a las opciones

Podéis configurar las opciones del core de dos formas diferentes.

* Via el Menú RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via el fichero `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Opciones del core

| Opción | Valor de la opción | Variable (para la sobrecarga) | Valores posibles |
| --- | --- | --- | --- |
| Read configuration | `Désactivé` ✅ / `Activé` | `same_cdi_read_config` | `disabled` / `enabled` |
| Write configuration | `Désactivé` ✅ / `Activé` | `same_cdi_write_config` | `disabled` / `enabled` |
| Save state naming | `game` ✅ / `system` | `same_cdi_saves` | `game` / `system` |
| NVRAM Saves per game | `Activé` ✅ / `Désactivé` | `same_cdi_nvram_saves` | `enabled` / `disabled` |
| Auto save/load states | `Désactivé` ✅ / `Activé` | `same_cdi_auto_save` | `disabled` / `enabled` |
| Enable in-game mouse | `Désactivé` ✅ / `Activé` | `same_cdi_mouse_enable` | `disabled` / `enabled` |
| Lightgun mode | `none` ✅ / `touchscreen` / `lightgun` | `same_cdi_lightgun_mode` | `none` / `touchscreen` / `lightgun` |
| Profile Buttons according to games (Restart) | `Activé` ✅ / `Désactivé` | `same_cdi_buttons_profiles` | `enabled` / `disabled` |
| Enable throttle | `Désactivé` ✅ / `Activé` | `same_cdi_throttle` | `disabled` / `enabled` |
| Enable cheats | `Désactivé` ✅ / `Activé` | `same_cdi_cheats_enable` | `disabled` / `enabled` |
| Main CPU Overclock | `default` ✅ / `30` / `31` / `32` / `33` / `34` / `35` / `36` / `37` / `38` / `39` / `40` / `41` / `42` / `43` / `44` / `45` / `46` / `47` / `48` / `49` / `50` / `51` / `52` / `53` / `54` / `55` / `60` / `65` / `70` / `75` / `80` / `85` / `90` / `95` / `100` / `105` / `110` / `115` / `120` / `125` / `130` / `135` / `140` / `145` / `150` | `same_cdi_cpu_overclock` | `default` / `30` / `31` / `32` / `33` / `34` / `35` / `36` / `37` / `38` / `39` / `40` / `41` / `42` / `43` / `44` / `45` / `46` / `47` / `48` / `49` / `50` / `51` / `52` / `53` / `54` / `55` / `60` / `65` / `70` / `75` / `80` / `85` / `90` / `95` / `100` / `105` / `110` / `115` / `120` / `125` / `130` / `135` / `140` / `145` / `150` |
| Alternate render method | `Désactivé` ✅ / `Activé` | `same_cdi_alternate_render` | `disabled` / `enabled` |
| Boot from CLI | `Désactivé` ✅ / `Activé` | `same_cdi_boot_from_cli` | `disabled` / `enabled` |
| Resolution | `640x480` ✅ / `640x360` / `800x600` / `800x450` / `960x720` / `960x540` / `1024x768` / `1024x576` / `1280x960` / `1280x720` / `1600x1200` / `1600x900` / `1440x1080` / `1920x1080` / `1920x1440` / `2560x1440` / `2880x2160` / `3840x2160` | `same_cdi_altres` | `640x480` / `640x360` / `800x600` / `800x450` / `960x720` / `960x540` / `1024x768` / `1024x576` / `1280x960` / `1280x720` / `1600x1200` / `1600x900` / `1440x1080` / `1920x1080` / `1920x1440` / `2560x1440` / `2880x2160` / `3840x2160` |
| MAME INI Paths | `Désactivé` ✅ / `Activé` | `same_cdi_mame_paths_enable` | `disabled` / `enabled` |
| MAME Joystick 4-way simulation | `disabled` ✅ / `4way` / `strict` / `qbert` | `same_cdi_mame_4way_enable` | `disabled` / `4way` / `strict` / `qbert` |

## ![](/emulators/external-links.png) Enlaces externos

* **Código fuente utilizado** : [https://github.com/libretro/same_cdi/](https://github.com/libretro/same_cdi/)
* **Documentación Libretro** : [https://docs.libretro.com/library/same_cdi/](https://docs.libretro.com/library/same_cdi/)