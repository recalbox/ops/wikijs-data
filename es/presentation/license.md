---
title: Licencia
description: Esta página contiene una descripción de los aspectos jurídicos y otras cosas aburridas pero importantes.
published: true
date: 2023-09-24T20:44:20.626Z
tags: recalbox, licencia
editor: markdown
dateCreated: 2023-04-17T11:15:21.171Z
---

Recalbox utiliza varios paquetes con diversos tipos de licencias (GPL, BSD, MIT, etc.) y Recalbox en sí mismo posee una licencia libre.

A continuación se muestra la licencia utilizada por Recalbox :

>La redistribución y la utilización del código RECALBOX o de cualquier otra obra derivada está autorizado bajo reserva del cumplimiento de las siguientes condiciones :
>
>Las redistribuciones no pueden ser vendidas sin autorización, ni ser utilizadas dentro de otro producto o para una actividad comercial.
>Las redistribuciones modificadas a partir de las fuentes originales deben incluir el código fuente completo, incluido el código fuente de todos los componentes utilizados por la distribución binaria que haya sido construida a partir de estas fuentes modificadas.
>Las redistribuciones deben reproducir la nota sobre derechos de autor descrita aquí arriba, la lista de condiciones de esta página y la nota de no-responsabilidad de acuerdo con esta documentación y/o los otros documentos incluidos en la distribución.
>ESTE PROGRAMA INFORMÁTICO SE PONE A DISPOSICIÓN DEL PÚBLICO POR LOS PROPIETARIOS DE LOS DERECHOS DE AUTOR Y LOS COLABORADORES "TAL CUAL" Y SIN GARANTÍAS. TODA GARANTÍA EXPRESA O IMPLÍCITA, INCLUIDAS PERO SIN ESTAR LIMITADAS, LAS GARANTÍAS IMPLÍCITAS DE CALIDAD MERCANTIL ET DE ADECUACIÓN A USOS PARTICULARES, SON RECHAZADAS. EN NINGÚN CASO, EL PROPIETARIO DE LOS DERECHOS DE AUTOR O LOS COLABORADORES NO SON RESPONSABLES DE CUALQUIER DAÑO DIRECTO, INDIRECTO, ACCÉSORIO, ESPECIAL, EJEMPLAR O CONSECUTIVO (INLUIDOS, PERO SIN LIMITARSE A ELLOS, EL APROVISIONAMIENTO DE BIENES O DE SERVICIOS DE REPUESTO; LA PÉRDIDA DE UTILIZACIÓN, DE DATOS O DE BENEFICIOS; O LA INTERRUPCIÓN DE LA ACTIVIDAD), CUALQUIERA QUE SEA LA CAUSA Y SEGÚN CUALQUIER TEORÍA DE RESPONSABILIDAD, QUE SEA CONTRACTUAL, DE RESPONSABILIDAD ESTRICTA O DELICTUAL (INCLUIDO LA NÉGLIGENCIA U OTRO) RESULTADO DE CUALQUIER FORMA DE UTILIZACIÓN DE ESTE PROGRAMA INFORMÁTICO, INCLUSO CUANDO HAN SIDO INFORMADOS DE LA POSIBILIDAD DE TALES DAÑOS.

>Aunque no se trata de una obligación legal encadrada por esta licencia, pedimos a toda persona que desee crear obras derivadas, versiones modificadas o "forks" de Recalbox de no utilizar código fuente que no se encuentre disponible en la rama `master`.
{.is-info}