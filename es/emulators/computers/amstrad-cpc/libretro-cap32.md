---
title: Libretro Cap32
description: 
published: true
date: 2024-07-29T18:26:26.121Z
tags: libretro, amstrad, cpc, cap32
editor: markdown
dateCreated: 2023-09-11T07:40:16.908Z
---

## ![](/emulators/license.svg) Licencia

Este núcleo está bajo la licencia [**GPLv2**](https://github.com/libretro/libretro-cap32/blob/master/COPYING.txt).

## ![](/emulators/compatibility.png) Compatibilidad

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Características

| Característica | Soportado |
| :---: | :---: |
| Redémarrage  | ✔ |
| Capturas d'écran | ✔ |
| Sauvegardes | ✔ |
| Sauvegardes instantanées | ✔ |
| Rembobinage | ✔ |
| Netplay | ✔ |
| Options du core | ✔ |
| RetroAchievements | ✔ |
| Contrôles | ✔ |
| Remapping | ✔ |
| Contrôle du disque | ✔ |

## ![](/emulators/bios.svg) BIOS

>**No se requiere BIOS.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Extensiones soportadas

Las roms deben tener las siguientes extensiones:

* .bin
* .cdt
* .cpr
* .dsk
* .m3u
* .sna
* .tap
* .voc
* .zip
* .7z

Este sistema soporta roms comprimidas en formato .zip/.7z. Sin embargo, se trata solo de un archivo.

Los archivos contenidos en los .zip/.7z deben corresponder a las extensiones mencionadas anteriormente.
Además, cada archivo .zip/.7z debe contener solo una rom comprimida.

>Los juegos con la extensión .bin deben estar obligatoriamente descomprimidos!
{.is-info}

### Ubicación

Coloca las roms de la siguiente manera:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 amstradcpc
┃ ┃ ┃ ┃ ┣ 🗒 **juego.zip**

>Se recomiendan encarecidamente las roms en formato **TOSEC**.
{.is-success}

>Para más información sobre las roms, visita [este tutorial](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

>Para poder conservar tus configuraciones personalizadas durante una actualización, te recomendamos utilizar nuestra funcionalidad [Sobrecargas de configuración](./../../../advanced-usage/configuration-override).
{.is-info}

### Acceder a las opciones

Puedes configurar diversas opciones de dos maneras diferentes.

* A través del menú RetroArch:

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* A través del archivo `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Opciones del core

| Opciones | Valores de las opciones | Variable (para sobrecarga) | Valores de las variables |
| --- | --- | --- | --- |
| Controls > User 1 Controller Config | `auto` ✅ / `qaop` / `incentive` / `joystick_port1` | `cap32_retrojoy0` | `auto` / `qaop` / `incentive` / `joystick_port1` |
| Controls > User 2 Controller Config | `auto` ✅ / `qaop` / `incentive` / `joystick_port1` / `joystick_port2` | `cap32_retrojoy1` | `auto` / `qaop` / `incentive` / `joystick_port1` / `joystick_port2` |
| Controls > Combo Key | `select` / `y` ✅ / `b` / `Désactivé` | `cap32_combokey` | `select` / `y` / `b` / `disabled` |
| Controls > Use internal Remap DB | `Activé` ✅ / `Désactivé` | `cap32_db_mapkeys` | `enabled` / `disabled` |
| Light Gun > Input | `Désactivé` ✅ / `phaser` / `gunstick` | `cap32_lightgun_input` | `disabled` / `phaser` / `gunstick` |
| Light Gun > Show Crosshair | `Désactivé` ✅ / `Activé` | `cap32_lightgun_show` | `disabled` / `enabled` |
| Model | `6128` ✅ / `464` / `664` / `6128+ (experimental)` | `cap32_model` | `6128` / `464` / `664` / `6128+ (experimental)` |
| Advanced > Autorun | `Activé` ✅ / `Désactivé` | `cap32_autorun` | `enabled` / `disabled` |
| Advanced > Ram size | `64` / `128` ✅ / `192` / `512` / `576` | `cap32_ram` | `64` / `128` / `192` / `512` / `576` |
| Video > Green Phosphor blueish | `5` / `10` / `15` ✅ / `20` / `30` | `cap32_advanced_green_phosphor` | `5` / `10` / `15` / `20` / `30` |
| Video > Monitor Intensity | `5` / `6` / `7` / `8` ✅ / `9` / `10` / `11` / `12` / `13` / `14` / `15` | `cap32_scr_intensity` | `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` |
| Video Advanced > Color Depth | `16bit` ✅ / `24bit` | `cap32_gfx_colors` | `16bit` / `24bit` |
| Video Advanced > Crop Screen Borders | `Désactivé` ✅ / `Activé` | `cap32_scr_crop` | `disabled` / `enabled` |
| Status Bar | `onloading` / `Activé` / `Désactivé` | `cap32_statusbar` | `onloading` / `enabled` / `disabled` |
| Keyboard Transparency | `Désactivé` ✅ / `Activé` | `cap32_` | `disabled` / `enabled` |
| Floppy Sound | `Activé` ✅ / `Désactivé` | `cap32_floppy_sound` | `enabled` / `disabled` |
| Monitor Type | `color` ✅ / `green` / `white` | `cap32_scr_tube` | `color` / `green` / `white` |
| PC Language | `english` ✅ / `french` / `spanish` | `cap32_lang_layout` | `english` / `french` / `spanish` |

## ![](/emulators/external-links.png) Enlaces externos

* **Código fuente utilizado** : [https://github.com/libretro/libretro-cap32/](https://github.com/libretro/libretro-cap32/)
* **Documentación Libretro** : [https://docs.libretro.com/library/caprice32/](https://docs.libretro.com/library/caprice32/)
