---
title: Backup / Duplicación de la tarjeta SD
description: Cómo duplicar o realizar una copia de seguridad de vuestra tarjeta SD
published: true
date: 2024-07-26T06:29:43.280Z
tags: clone, backup, duplicación, copia de seguridad, tarjeta sd
editor: markdown
dateCreated: 2023-09-01T15:00:37.647Z
---

Si utilizáis una tarjeta SD, encontraréis en estas páginas formas de realizar un backup o copia de seguridad o cómo clonar vuestro dispositivo de almacenamiento. 

> Las tarjetas SD suelen fallar más que los discos duros, así que se recomienda no utilizarlas para almacenar muchos juegos siempre y cuando podáis enchufar un disco externo a vuestro sistema Recalbox.
{.is-warning}

Aquí tenéis la lista de tutoriales disponibles:

[Duplicación de vuestra tarjeta SD](clone-sd-card)