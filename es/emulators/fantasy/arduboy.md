---
title: Arduboy
description: 
published: true
date: 2024-07-26T08:21:48.756Z
tags: fantasy, arduboy, 9.1+, consolas
editor: markdown
dateCreated: 2023-09-04T07:03:29.448Z
---

![](/emulators/fantasy/arduboy.svg){.align-center}

## Detalles técnicos

* **Desarrollador**: Kevin Bates
* **Año de lanzamiento**: 2015
* **CPU**: ATMega32u4 a 8 MHz
* **RAM**: 2,5KB RAM
* **Pantalla**: pantalla OLED monocroma de 1,3 pulgadas con resolución de 128 x 64 píxeles
* **Almacenamiento**: 32KB Flash
* **Sonido**: Altavoz Piezo de 2 canales

## Presentación

El arduboy es una videoconsola portátil de 8 bits basada en la plataforma de código abierto Arduino.

Inicialmente fue un proyecto de Kickstarter de Kevin Bates que vio la luz a principios de 2015 y obtuvo más de 400.000 dólares con más de 7.000 contribuyentes.

## Emuladores

[Libretro Arduous](libretro-arduous)