---
title: Libretro Mesen_S
description: 
published: true
date: 2024-07-13T13:50:17.077Z
tags: libretro, snes, super nintendo, mesen
editor: markdown
dateCreated: 2023-09-08T08:23:31.347Z
---



## ![](/emulators/license.svg) Licencia

El core posee la siguiente licencia [**GPLv3**](https://github.com/SourMesen/Mesen-S/blob/master/README.md).

## ![](/emulators/compatibility.png) Compatibilidad

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ❌ | ❌ | ✅&nbsp;🐌 | ✅ | ✅&nbsp;🐌 | ✅&nbsp;🐌 | ✅ | ✅&nbsp;🐌 |

🐌  Basses performances mais jouable.

## ![](/emulators/features.png) Funcionalidades

| Funcionalidad | ¿Soportada? |
| :---: | :---: |
| Reinicio | ✔ |
| Capturas de pantalla | ✔ |
| Grabaciones | ✔ |
| Grabaciones instantáneas | ✔ |
| Rebobinado | ✔ |
| Netplay | ✔ |
| Opciones del core | ✔ |
| RetroAchievements | ✔ |
| Cheats RetroArch | ✔ |
| Cheats nativos | ✔ |
| Controles | ✔ |
| Remapping | ✔ |
| Multi-Ratón | ✔ |
| Softpatching | ✔ |
| Crop Overscan | ✔ |

## ![](/emulators/bios.svg) BIOS

>**No se necesitan bios.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Extensiones de roms soportadas

Las roms deben tener las extensiones siguientes:

* .fig
* .gd3
* .sfc
* .smc
* .swc
* .zip
* .7z

Este sistema soporta las roms comprimidas en formato .zip/.7z. No obstante, se trata sólo de un archivo.

Los ficheros contenidos en los .zip/.7z deben tener las extensiones citadas previamente.
Además, cada fichero .zip/.7z sólo debe contener un fichero rom al interior.

### Emplazamiento

Disponed vuestras roms como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 snes
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.zip**

>Se aconsejan las roms del proyecto **No-Intro**.
{.is-success}

>Para más información sobre las roms, consultad [este tutorial](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

>Para conservar las configuraciones personalizadas cuando se produce una actualización del sistema Recalbox, os aconsejamos utilizar las [Sobrecargas de configuración](./../../../advanced-usage/configuration-override).
{.is-info}

### Acceder a las opciones

Podéis configurar las opciones del core de dos formas diferentes.

* Via el Menú RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via el fichero `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Opciones del core

| Opción | Valor de la opción | Variable (para la sobrecarga) | Valores posibles |
| --- | --- | --- | --- |
| NTSC Filter | `Désactivé` ✅ / `Composite (Blargg)` / `S-Video (Blargg)` / `RGB (Blargg)` / `Monochrome (Blargg)` | `mesen-s_ntsc_filter` | `Disabled` / `Composite (Blargg)` / `S-Video (Blargg)` / `RGB (Blargg)` / `Monochrome (Blargg)` |
| Region | `Auto` ✅ / `NTSC` / `PAL` | `mesen-s_region` | `Auto` / `NTSC` / `PAL` |
| Game Boy Model | `Auto` ✅ / `Game Boy` / `Game Boy Color` / `Super Game Boy` | `mesen-s_gbmodel` | `Auto` / `Game Boy` / `Game Boy Color` / `Super Game Boy` |
| Use SGB2 | `Activé` ✅ / `Désactivé` | `mesen-s_sgb2` | `enabled` / `disabled` |
| Vertical Overscan | `None` ✅ / `8px` / `16px` | `mesen-s_overscan_vertical` | `None` / `8px` / `16px` |
| Horizontal Overscan | `None` ✅ / `8px` / `16px` | `mesen-s_overscan_horizontal` | `None` / `8px` / `16px` |
| Aspect Ratio | `Auto` ✅ / `No Stretching` / `NTSC` / `PAL` / `4:3` / `16:9` | `mesen-s_aspect_ratio` | `Auto` / `No Stretching` / `NTSC` / `PAL` / `4:3` / `16:9` |
| Blend Hi-Res Modes | `Désactivé` ✅ / `Activé` | `mesen-s_blend_high_res` | `disabled` / `enabled` |
| Cubic Interpolation (Audio) | `Désactivé` ✅ / `Activé` | `mesen-s_cubic_interpolation` | `disabled` / `enabled` |
| Overclock | `None` ✅ / `Low` / `Medium` / `High` / `Very High` | `mesen-s_overclock` | `None` / `Low` / `Medium` / `High` / `Very High` |
| Overclock Type | `Before NMI` ✅ / `After NMI` | `mesen-s_overclock_type` | `Before NMI` / `After NMI` |
| Super FX Clock Speed | `100%` ✅ / `200%` / `300%` / `400%` / `500%` / `1000%` | `mesen-s_superfx_overclock` | `100%` / `200%` / `300%` / `400%` / `500%` / `1000%` |
| Default power-on state for RAM | `Random Values (Default)` ✅ / `All 0s` / `All 1s` | `mesen-s_ramstate` | `Random Values (Default)` / `All 0s` / `All 1s` |
| Use HLE coprocessor emulation | `Désactivé` ✅ / `Activé` | `mesen-s_hle_coprocessor` | `disabled` / `enabled` |

## ![](/emulators/external-links.png) Enlaces externos

* **Código fuente utilizado** : [https://github.com/libretro/Mesen-S/](https://github.com/libretro/Mesen-S/)
* **Documentación Libretro** : [https://docs.libretro.com/library/mesen-s/](https://docs.libretro.com/library/mesen-s/)