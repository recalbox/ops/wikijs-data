---
title: Regresar a EmulationStation
description: 
published: true
date: 2023-09-20T21:09:41.967Z
tags: emulationstation, mupen64plus
editor: markdown
dateCreated: 2023-09-20T21:09:41.967Z
---

## Explicaciones

Recalbox no ha preconfigurado Mupen64plus a diferencia de lo que ocurre con otros emuladores. Por lo tanto, se debe aplicar una configuración manual.

El modo de vídeo de la N64 se define en el fichero [recalbox.conf] (./../../../../basic-usage/getting-started/recalboxconf-file).

Por defecto, se utiliza el modo de vídeo: `n64.videomode=DMT 4 HDMI`. Este modo muestra una resolución de **640x480**. Establecemos esta resolución por defecto porque la Raspberry Pi 2 no puede emular la N64 a una resolución mayor con una buena tasa de refresco. Así que **640x480** es la resolución máxima utilizada por vuestra Recalbox por defecto.

Si vuestra pantalla no puede cambiar de resolución (por ejemplo con la pantalla táctil oficial de la Pi), obtendréis bordes negros alrededor de la imagen porque no se estirará para ajustarse a la pantalla.

## Instrucciones

En este caso, debéis aplicar las 2 etapes siguientes:

* Reemplazad la línea anterior por esta otra:

```ini
n64.videomode=default
```

* Modificad el fichero `/recalbox/share/system/configs/mupen64/mupen64plus.cfg` para cambiar la resolución de salida a la resolución nativa de vuestra pantalla (por ejemplo **800x480**) :

```ini
[Video-General]
# Use fullscreen mode if True, or windowed mode if False
Fullscreen = False
# Width of output window or fullscreen width
ScreenWidth = 800
# Height of output window or fullscreen height
ScreenHeight = 480
```

Pero no todas las pantallas tienen la misma compatibilidad de modo de vídeo. Por ejemplo, cuando iniciáis una partida de N64 con el core mupen64plus, podéis obtener una pantalla negra y retornar a EmulationStation. En este caso, hay que determinar los modos de vídeo que son compatibles con vuestra pantalla.
  
* Conectaros via [SSH](./../../../../tutorials/system/access/root-access-terminal-cli), y escribid estos comandos uno detrás del otro:

`tvservice -m DMT`
`tvservice -m CEA`

* Estos comandos mostraran algo como esto:

```shell
[root@RECALBOX ~]# tvservice -m DMT
Group DMT has 16 modes:
           mode 4: 640x480 @ 60Hz 4:3, clock:25MHz progressive 
           mode 5: 640x480 @ 72Hz 4:3, clock:31MHz progressive 
           mode 6: 640x480 @ 75Hz 4:3, clock:31MHz progressive 
           mode 8: 800x600 @ 56Hz 4:3, clock:36MHz progressive 
           mode 9: 800x600 @ 60Hz 4:3, clock:40MHz progressive 
           mode 10: 800x600 @ 72Hz 4:3, clock:50MHz progressive 
           mode 11: 800x600 @ 75Hz 4:3, clock:49MHz progressive 
           mode 16: 1024x768 @ 60Hz 4:3, clock:65MHz progressive 
           mode 17: 1024x768 @ 70Hz 4:3, clock:75MHz progressive 
           mode 18: 1024x768 @ 75Hz 4:3, clock:78MHz progressive 
           mode 21: 1152x864 @ 75Hz 4:3, clock:108MHz progressive 
           mode 32: 1280x960 @ 60Hz 4:3, clock:108MHz progressive 
           mode 35: 1280x1024 @ 60Hz 5:4, clock:108MHz progressive 
           mode 36: 1280x1024 @ 75Hz 5:4, clock:135MHz progressive 
  (prefer) mode 57: 1680x1050 @ 60Hz 16:10, clock:119MHz progressive 
           mode 58: 1680x1050 @ 60Hz 16:10, clock:146MHz progressive 
```

```shell
[root@RECALBOX ~]# tvservice -m CEA
Group CEA has 5 modes:
           mode 1: 640x480 @ 60Hz 4:3, clock:25MHz progressive 
           mode 2: 720x480 @ 60Hz 4:3, clock:27MHz progressive 
           mode 3: 720x480 @ 60Hz 16:9, clock:27MHz progressive 
  (native) mode 4: 1280x720 @ 60Hz 16:9, clock:74MHz progressive 
           mode 16: 1920x1080 @ 60Hz 16:9, clock:148MHz progressive 
```

* Esta salida nos muestra todos los modos de vídeo que se pueden utilizar junto con vuestra pantalla. Hay que encontrar un modo de vídeo, con una resolución igual o superior a 640x480, y a continuación definir este modo dentro del fichero [recalbox.conf.](./../../../../basic-usage/getting-started/recalboxconf-file).
* Si elejimos este modo de vídeo:

```shell
Group CEA has 5 modes:
           mode 1: 640x480 @ 60Hz 4:3, clock:25MHz progressive
```

* Habrá que modificar el fichero [recalbox.conf](./../../../../basic-usage/getting-started/recalboxconf-file) de la siguiente forma:

  * modo de vídeo por defecto :

```ini
n64.videomode=DMT 4 HDMI
```

  * nuevo modo de vídeo:

```ini
n64.videomode=CEA 1 HDMI
```

## Con una pantalla CRT

Con las pantalla CRT, se debe especificar el siguiente modo de vídeo:

```ini
n64.videomode=default
```