---
title: Libretro Picodrive
description: 
published: true
date: 2024-07-12T13:19:23.908Z
tags: libretro, picodrive, sg-1000
editor: markdown
dateCreated: 2024-07-12T13:19:23.908Z
---

**Libretro PicoDrive** es un emulador open-source Sega **8**/**16** bit y **32X**. Fue diseñado para dispositivos portátiles en la arquitectura **ARM**.

## ![](/emulators/license.svg) Licencia

Este core está bajo licencia [**no comercial**](https://github.com/libretro/picodrive/blob/master/COPYING).

## ![](/emulators/compatibility.png) Compatibilidad

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Funcionalidades

| Funcionalidad | Soportado |
| :---: | :---: |
| Reinicio | ✔ |
| Guardados | ✔ |
| Guardados instantáneos | ✔ |
| Rebobinado | ✔ |
| Netplay | ✔ |
| Opciones del core | ✔ |
| RetroAchievements | ✔ |
| Cheats RetroArch | ✔ |
| Controles | ✔ |
| Remapeo | ✔ |
| Control del disco | ✔ |

## ![](/emulators/bios.svg) BIOS

> **Ningún BIOS es requerido.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Extensiones soportadas

Las roms deben tener las siguientes extensiones:

* .bin
* .sc
* .sg
* .zip
* .7z

Este sistema soporta roms comprimidas en formato .zip/.7z. Sin embargo, solo se trata de un archivo.

Los archivos contenidos en los .zip/.7z deben corresponder a las extensiones mencionadas anteriormente.
Además, cada archivo .zip/.7z debe contener solo una rom comprimida.

### Ubicación

Coloca las roms de la siguiente manera:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 sg1000
┃ ┃ ┃ ┃ ┣ 🗒 **juego.zip**

> Las roms en formato **No-Intro** son altamente recomendadas.
{.is-success}

> Para más información sobre las roms, consulta [este tutorial](./../../../tutorials/games/generalities/isos-and-roms).
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

> Para conservar tus configuraciones personalizadas durante una actualización, te recomendamos usar nuestra funcionalidad [Sobrecargas de configuración](./../../../advanced-usage/configuration-override).
{.is-info}

### Acceder a las opciones

Puedes configurar varias opciones de dos maneras diferentes.

* A través del menú RetroArch:

┣ 📁 Menú RetroArch
┃ ┣ 📁 Opciones del core
┃ ┃ ┣ 🧩 Nombre_opción

* A través del archivo `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Opciones del core

| Opciones | Valor de las opciones | Variable (para sobrecarga) | Valores de las variables |
| --- | --- | --- | --- |
| Región | `Auto` ✅ / `Japan NTSC` / `Japan PAL` / `US` / `Europe` | `picodrive_region` | `Auto` / `Japan NTSC` / `Japan PAL` / `US` / `Europe` |
| Tipo de Master System | `Auto` ✅ / `Game Gear` / `Master System` | `picodrive_smstype` | `Auto` / `Game Gear` / `Master System` |
| Mapeo de ROM de Master System | `Auto` ✅ / `Sega` / `Codemasters` / `Korea` / `Korea MSX` / `Korea X-in-1` / `Korea 4-Pak` / `Korea Janggun` / `Korea Nemesis` | `picodrive_smsmapper` | `Auto` / `Sega` / `Codemasters` / `Korea` / `Korea MSX` / `Korea X-in-1` / `Korea 4-Pak` / `Korea Janggun` / `Korea Nemesis` |
| Sega CD RAM Cart | `Desactivado` ✅ / `Activado` | `picodrive_ramcart` | `desactivado` / `activado` |
| Relación de aspecto proporcionada por el core | `PAR` ✅ / `4:3` / `CRT` | `picodrive_aspect` | `PAR` / `4:3` / `CRT` |
| Filtro de fantasma de LCD | `Desactivado` ✅ / `Débil` / `Normal` | `picodrive_ggghost` | `desactivado` / `débil` / `normal` |
| Renderer | `Accurate` ✅ / `Good` / `Fast` | `picodrive_renderer` | `accurate` / `good` / `fast` |
| Sample Rate (Hz) | `16000` / `22050` / `32000` / `44100` ✅ | `picodrive_sound_rate` | `16000` / `22050` / `32000` / `44100` |
| Filtrado FM | `Desactivado` ✅ / `Activado` | `picodrive_fm_filter` | `desactivado` / `activado` |
| Unidad de sonido FM de Master System | `Desactivado` ✅ / `Activado` | `picodrive_smsfm` | `desactivado` / `activado` |
| Ruido de DAC de Mega Drive FM | `Desactivado` ✅ / `Activado` | `picodrive_dacnoise` | `desactivado` / `activado` |
| Filtro de audio | `Desactivado` ✅ / `Low-pass` | `picodrive_audio_filter` | `desactivado` / `low-pass` |
| Filtro Low-pass % | `5` / `10` / `15` / `20` / `25` / `30` / `35` / `40` / `45` / `50` / `55` / `60` ✅ / `65` / `70` / `75` / `80` / `85` / `90` / `95` | `picodrive_lowpass_range` | `5` / `10` / `15` / `20` / `25` / `30` / `35` / `40` / `45` / `50` / `55` / `60` / `65` / `70` / `75` / `80` / `85` / `90` / `95` |
| Dispositivo de entrada 1 | `3 Button Pad` ✅ / `6 Button Pad` / `None` | `picodrive_input1` | `3 Button Pad` / `6 Button Pad` / `None` |
| Dispositivo de entrada 2 | `3 Button Pad` ✅ / `6 Button Pad` / `None` | `picodrive_input2` | `3 Button Pad` / `6 Button Pad` / `None` |
| Recompiladores dinámicos | `Activado` ✅ / `Desactivado` | `picodrive_drc` | `desactivado` / `activado` |
| Frameskip | `Desactivado` ✅ / `Auto` / `Manual` | `picodrive_frameskip` | `desactivado` / `auto` / `manual` |
| Umbral de Frameskip (%) | `15` / `18` / `21` / `24` / `27` / `30` / `33` ✅ / `36` / `39` / `42` / `45` / `48` / `51` / `54` / `57` / `60` | `picodrive_frameskip_threshold` | `15` / `18` / `21` / `24` / `27` / `30` / `33` / `36` / `39` / `42` / `45` / `48` / `51` / `54` / `57` / `60` |
| Sin límite de sprites | `Desactivado` ✅ / `Activado` | `picodrive_sprlim` | `desactivado` / `activado` |
| Overclock de 68K | `Desactivado` ✅ / `+25%` / `+50%` / `+75%` / `+100%` / `+200%` / `+400%` | `picodrive_overclk68k` | `desactivado` / `+25%` / `+50%` / `+75%` / `+100%` / `+200%` / `+400%` |

## ![](/emulators/external-links.png) Enlaces externos

* **Código fuente utilizado**: [https://github.com/libretro/picodrive/](https://github.com/libretro/picodrive/)
* **Documentación de Libretro**: [https://docs.libretro.com/library/picodrive/](https://docs.libretro.com/library/picodrive/)
