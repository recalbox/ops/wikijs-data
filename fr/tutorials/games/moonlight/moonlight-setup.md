---
title: Mise en route de Moonlight
description: 
published: true
date: 2023-05-08T20:47:37.196Z
tags: moonlight, mise en route
editor: markdown
dateCreated: 2021-05-21T08:38:29.815Z
---

## Introduction

Moonlight est une version open-source de la technologie Gamestream de NVidia.

Si votre PC répond aux exigences, vous pouvez diffuser la plupart de vos jeux en streaming sur votre Recalbox. D'un autre côté, Recalbox lit la configuration des pads depuis EmulationStation et la convertit vers Moonlight.

### Fonctionnalités de Moonlight sur Recalbox

* Diffusez des jeux en streaming sur votre réseau local ou via Internet
* Jusqu'à 4 joueurs supportés
* Jusqu'à 1080p/60fps
* Décodage H264 accéléré par le matériel sur n'importe quelle version du Raspberry Pi
* Supporte le clavier et la souris
* Jusqu'à GFE 3,12

### Pré-requis

Configuration requise pour Recalbox et le PC pour profiter de Moonlight :

* Une manette configurée dans EmulationStation
* Compte Steam (optionnel) ou jeux supportés de façon autonome (voir [https://shield.nvidia.com/game-stream](https://shield.nvidia.com/game-stream))
* Un GPU compatible Nvidia (voir [http://www.geforce.com/geforce-experience/system-requirements](http://www.geforce.com/geforce-experience/system-requirements))
* Une connexion Ethernet est fortement recommandée, le WiFi n'est pas assez fiable.

## Utilisation de Moonlight

Recalbox offre un scrap plus agréable, la possibilité de diffuser en continu à partir de plusieurs PC du réseau et une petite option pour trouver les hôtes GFE disponibles.

Vous devez utiliser 3 commandes pour configurer et démarrer Moonlight.

>Indiquez bien les chemins entiers comme indiqué !
{.is-warning}

### Trouver des hôtes

En premier lieu, vous devez trouver les hôtes disponibles avec cette commande :

```shell
/recalbox/scripts/moonlight/Moonlight.sh find
```

### Associer votre Recalbox à un des hôtes trouvés

Vous devez ensuite associer votre Recalbox à l'hôte souhaité avec cette commande :

```shell
/recalbox/scripts/moonlight/Moonlight.sh pair <host>
```

>Remplacez `<host>` par le nom d'hôte indiqué par la première commande.
{.is-info}

>Une invite côté Windows va s'afficher. Il faudra entrer le code PIN qui est affiché à la fin de l'exécution de la commande d'association.
{.is-info}

### Initialiser Moonlight

Une fois l'association à l'hôte souhaité effectuée, vous n'avez plus qu'à lancer la commande suivante pour finir :

```shell
/recalbox/scripts/moonlight/Moonlight.sh init <host>
```

>Remplacez `<host>` par le nom d'hôte indiqué par la première commande.
{.is-info}

## Quitter un jeu (forçage)

Pour quitter un jeu, vous pouvez utiliser la combinaison de touches CTRL + ALT + SHIFT + Q (ou A).

## Démonstration

Voici une démo pour trouver des hôtes GFE, les connecter et les lancer :


```shell
# /recalbox/scripts/moonlight/Moonlight.sh find
Listing available GFE servers :
GFE Host WIN10(192.168.2.137) NVIDIA GeForce GTX 980 running GFE 3.23.0.74
You can now run the following commands:
  /recalbox/scripts/moonlight/Moonlight.sh pair WIN10
  /recalbox/scripts/moonlight/Moonlight.sh init WIN10

# ./Moonlight.sh pair WIN10
Hostname mode
WIN10(192.168.2.137) /recalbox/share/system/configs/moonlight/moonlight-WIN10.conf | /recalbox/share/system/configs/moonlight/keydir-WIN10
When PIN is asked on your PC, enter the number ----> 3863 <----
GFE Host WIN10(192.168.2.137) NVIDIA GeForce GTX 980 running GFE 3.23.0.74

# /recalbox/scripts/moonlight/Moonlight.sh init WIN10
Hostname mode
Adding and scraping Brutal Legend ...
Adding and scraping Sacred Citadel ...
Adding and scraping Just Cause 3 ...
Adding and scraping Street Fighter V ...
Adding and scraping Just Cause 2 Multiplayer ...
Adding and scraping Tales of Zestiria ...
Adding and scraping Grand Theft Auto V ...
Adding and scraping Hell Yeah! Wrath of the Dead Rabbit ...
Adding and scraping Ultra Street Fighter IV ...
Adding and scraping Diablo III ...
Adding and scraping Pro Evolution Soccer 2017 ...
Adding and scraping Bionic Commando Rearmed ...
Adding and scraping Just Cause 2 ...
Adding and scraping Pro Evolution Soccer 2016 ...
Adding and scraping Broforce ...
Adding and scraping DmC: Devil May Cry ...
Adding and scraping Naruto Shippuden: Ultimate Ninja Storm 4 ...
Adding and scraping Steam ...
```

## ANNEXE

>Utilisateurs avancés uniquement : vous pouvez diffuser en continu à partir d'un ordinateur distant sur Internet.
{.is-warning}

* Vous devrez modifier `/recalbox/scripts/moonlight/Moonlight.sh` pour :
  * Spécifier l'adresse IP de l'hôte distant,
  * Définir la même adresse IP dans moonlight.conf,
  * Et configurer la redirection de port selon [cette page](https://github.com/moonlight-stream/moonlight-android/wiki/Setup-Guide#streaming-over-the-internet)