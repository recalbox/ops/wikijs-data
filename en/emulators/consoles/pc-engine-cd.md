---
title: TurboGrafx CD
description: NEC TurboGrafx CD
published: true
date: 2021-10-03T21:36:41.694Z
tags: consoles, nec, turbografx cd
editor: markdown
dateCreated: 2021-07-28T05:58:15.475Z
---

![](/emulators/consoles/pcenginecd-us.svg){.align-center}

## Technical data

* **Manufacturer**: NEC
* **Year of release**: 1987
* **Units sold**: 10 millions
* **CPU**: HuC6280 8-bit 7,16 Mhz
* **RAM**: 8 Kbytes
* **Picture Processor Unit**: 8-bit
* **Video RAM**: 64 Kbytes
* **Media**: HU-Card, SHU-Card, CD-ROM, Super-CD-ROM

## Presentation

The **PC-Engine,** known as **TurboGrafx-16** in North America, is an 8-bit video game console manufactured by NEC and marketed from 1987 in Japan. Outside of its native country and North America, the console line had a relatively confidential existence, as it was not widely distributed.

Ahead of its time, the PC-Engine has an 8-bit central processor and a 16-bit graphics processor, so it is considered a fourth generation console. Designed to be modular and upgradeable, it is also the first game console in history to use CD-ROM support, thanks to an optional drive, from December 1988. After the first original model, it was declined in many versions to finally accept four different game formats: HuCard, CD-ROM², Super CD-ROM² and Arcade CD-ROM².

## Emulators

[Libretro Mednafen_SuperGrafx](libretro-mednafen_supergrafx)
[Libretro Mednafen_PCE_FAST](libretro-mednafen_pce_fast)