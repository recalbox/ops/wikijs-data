---
title: Game Boy
description: Nintendo Game Boy
published: true
date: 2024-07-26T08:32:19.903Z
tags: nintendo, game, boy, gameboy, portátiles
editor: markdown
dateCreated: 2023-09-04T08:03:32.302Z
---

![](/emulators/handheld/gameboy.svg){.align-center}

## Detalles técnicos

* **Fabricante**: Nintendo
* **Año de lanzamiento**: 1989
* **Cantidad vendida**: 118,69 millones
* **Juego más vendido**: Tetris
* **Procesador**: Zilog Z80 de 8 bits a 4,194304MHz
* **RAM**: 8kB
* **VRAM**: 8k
* **Vídeo**: PPU (integrado en el procesador)
* **Resolución**: 160x144 píxeles, 4 niveles de gris
* **Sonido**: 4 canales estéreo
* **Tamaño del cartucho**: 32kB - 1MB

## Presentación

La **Game Boy** es una videoconsola portátil de 8 bits de cuarta generación desarrollada y fabricada por Nintendo. Lanzada en Japón el 21 de abril de 1989, luego en Norteamérica en octubre de 1989 y finalmente en Europa el 28 de septiembre de 1990, fue la primera consola portátil de la gama Game Boy. Fue diseñada por Gunpei Yokoi y Nintendo Research & Development 1, el mismo equipo que creó la serie Game and Watch, así como muchos juegos de éxito para la Nintendo Entertainment System.

A pesar del lanzamiento de consolas portátiles técnicamente más avanzadas, la Game Boy fue un gran éxito. Se convirtió en la tercera consola más vendida de la historia, con 119 millones de unidades vendidas, por detrás de la PlayStation 2 de Sony y la Nintendo DS. A pesar de sus gráficos minimalistas, la Game Boy consiguió hacerse un nombre gracias a sus muchas bazas: pequeño tamaño, bajo precio, batería de larga duración y un catálogo de juegos rico y variado.

## Emuladores

[Libretro Gambatte](libretro-gambatte)
[Libretro Mesen_S](libretro-mesen_s)
[Libretro SameBoy](libretro-sameboy)
[Libretro TGBDual](libretro-tgbdual)
[Libretro bsnes](libretro-bsnes)
[Libretro mGBA](libretro-mgba)