---
title: Libretro GenesisPlusGX Wide
description: Megadrive/Genesis emulator with wide screen support
published: true
date: 2024-07-24T13:45:59.336Z
tags: genesis, libretro, genesisplusgx wide, 8.0+
editor: markdown
dateCreated: 2021-06-22T12:13:24.636Z
---

**Libretro GenesisPlusGX Wide** is an open-source Sega 8/16-bit emulator focused on accuracy and portability.

It emulate Megadrive games with widescreen support.

Unlike other emulators, **Libretro GenesisPlusGX Wide** will not stretch the image to fill the screen, but will display game graphics outside the original 4/3 area.

An impressive example on Street of Rage with the Wide core:

![](/emulators/consoles/megadrive/libretro-genesisplusgxwide/169.png)

And the original version:

![](/emulators/consoles/megadrive/libretro-genesisplusgxwide/43.png)

## ![](/emulators/license.svg) License

This core is under [**non-commercial**](https://github.com/libretro/Genesis-Plus-GX/blob/master/LICENSE.txt) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Features

| Feature | Supported |
| :---: | :---: |
| Restart | ✔ |
| Saves | ✔ |
| States | ✔ |
| Rewind | ✔ |
| Netplay | ✔ |
| Core Options | ✔ |
| RetroAchievements | ✔ |
| RetroArch Cheats | ✔ |
| Controls | ✔ |
| Remapping | ✔ |
| Multi-Mouse | ✔ |

## ![](/emulators/bios.svg) Bios

### List of optional bios

| Nom de fichier | Description | MD5 | Fourni |
| :---: | :---: | :---: | :---: |
| bios_MD.bin | BIOS MegaDrive (bootrom) | 45e298905a08f9cfb38fd504cd6dbc84 | ❌ |

### Location

Put the bios like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 megadrive
┃ ┃ ┃ ┃ ┣ 🗒 **bios_MD.bin**

## ![](/emulators/roms.png) Roms

### Supported extensions

Roms must have the following extensions:

* .68k
* .bin
* .en
* .md
* .mdx
* .smd
* .zip
* .7z

This system supports compressed roms in .zip/.7z format. However, be careful, it is only an archive.

The files contained in the .zip/.7z must correspond to the extensions mentioned above.
Moreover, each .zip/.7z file must contain only one compressed rom.

### Location

Place the roms like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 megadrive
┃ ┃ ┃ ┃ ┣ 🗒 **game.zip**

>The roms using **No-Intro** format roms are highly recommended.
{.is-success}

>For more information about roms, go to [this tutorial](./../../../tutorials/games/generalities/isos-and-roms)!
{.is-info}

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an update, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Accessing to options

You can configure various options in two different ways.

* Via the RetroArch menu:

┣ 📁 RetroArch menu
┃ ┣ 📁 Core options
┃ ┃ ┣ 🧩 Name_option

* Via the `retroarch-core-options.cfg` file:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core options

| Options | Options values | Variables (for configuration override) | Variables values |
| --- | --- | --- | --- |
| System Hardware | `Auto` ✅ / `SG-1000` / `SG-1000 II` / `SG-1000 II + RAM Ext.` / `Mark III` / `Master System` / `Master System II` / `Game Gear` / `Mega Drive/Genesis` | `genesis_plus_gx_system_hw` | `auto` / `sg-1000` / `sg-1000 II` / `sg-1000 II + ram ext.` / `mark-III` / `master system` / `master system II` / `game gear` / `mega drive / genesis` |
| System Region | `Auto` ✅ / `NTSC-U` / `PAL` / `NTSC-J` | `genesis_plus_gx_region_detect` | `auto` / `ntsc-u` / `pal` / `ntsc-j` |
| System Boot ROM | `Disabled` ✅ / `Enabled` | `genesis_plus_gx_bios` | `disabled` / `enabled` |
| CD System BRAM (Requires Restart) | `Per-BIOS` ✅ / `Per-Game` | `genesis_plus_gx_bram` | `per bios` / `per game` |
| CD Backup Cart BRAM (Requires Restart) | `Per-Cart` ✅ / `Per-Game` | `genesis_plus_gx_cart_bram` | `per cart` / `per game` |
| CD Backup Cart BRAM Size (Requires Restart) | `Disabled` ✅ / `128 kbit` / `256 kbit` / `512 kbit` / `1 Mbit` / `2 Mbit` / `4 Mbit` ✅ | `genesis_plus_gx_cart_size` | `disabled` / `128k` / `256k` / `512k` / `1meg` / `2meg` / `4meg` |
| CD add-on (MD mode) (Requires Restart) | `Auto` ✅ / `Sega/Mega CD` / `MegaSD` / `None` | `genesis_plus_gx_add_on` | `auto` / `sega/mega cd` / `megasd` / `none` |
| Cartridge Lock-On | `Disabled` ✅ / `Game Genie` / `Action Replay (Pro)` / `Sonic & Knuckles` | `genesis_plus_gx_lock_on` | `disabled` / `game genie` / `action replay (pro)` / `sonic & knuckles` |
| Core-Provided Aspect Ratio | `Auto` ✅ / `NTSC PAR` / `PAL PAR` / `4:3` / `Uncorrected` | `genesis_plus_gx_aspect_ratio` | `auto` / `NTSC PAR` / `PAL PAR` / `4:3` / `Uncorrected` |
| Borders | `Disabled` ✅ / `Top/Bottom` / `Left/Right` / `Full` | `genesis_plus_gx_overscan` | `disabled` / `top/bottom` / `left/right` / `full` |
| Extra columns to draw in H40 for widescreen | `10` ✅ / `0` / `2` / `4` / `6` / `8` / `12` / `14` / `16` / `18` / `20` / `22` / `24` | `genesis_plus_gx_wide_h40_extra_columns` | `10` / `0` / `2` / `4` / `6` / `8` / `12` / `14` / `16` / `18` / `20` / `22` / `24` |
| Fix VPD DMA boundary bug | `Disabled` ✅ / `Enabled` | `genesis_plus_gx_wide_vdp_fix_dma_boundary_bug` | `disabled` / `enabled` |
| Hide Master System Side Borders | `Disabled` ✅ / `Left Border Only` / `Left & Right Borders` | `genesis_plus_gx_left_border` | `disabled` / `left border` / `left & right borders` |
| Game Gear Extended Screen | `Disabled` ✅ / `Enabled` | `genesis_plus_gx_gg_extra` | `disabled` / `enabled` |
| Blargg NTSC Filter | `Disabled` ✅ / `Monochrome` / `Composite` / `S-Video` / `RGB` | `genesis_plus_gx_blargg_ntsc_filter` | `disabled` / `monochrome` / `composite` / `svideo` / `rgb` |
| LCD Ghosting Filter | `Disabled` ✅ / `Enabled` | `genesis_plus_gx_lcd_filter` | `disabled` / `enabled` |
| Interlaced Mode 2 Output | `Single Field` ✅ / `Double Field` | `genesis_plus_gx_render` | `single field` / `double field` |
| Frameskip | `Disabled` ✅ / `Auto` / `Manual` | `genesis_plus_gx_frameskip` | `disabled` / `auto` / `manual` |
| Frameskip Threshold (%) | `15` / `18` / `21` / `24` / `27` / `30` / `33` / `36` / `39` / `42` / `45` / `48` / `51` / `54` / `57` / `60` | `genesis_plus_gx_frameskip_threshold` | `15` / `18` / `21` / `24` / `27` / `30` / `33` ✅ / `36` / `39` / `42` / `45` / `48` / `51` / `54` / `57` / `60` |
| Master System FM (YM2413) | `Auto` ✅ / `Disabled` / `Enabled` | `genesis_plus_gx_ym2413` | `auto` / `disabled` / `enabled` |
| Master System FM (YM2413) Core | `MAME` ✅ / `Nuked` | `genesis_plus_gx_ym2413_core` | `mame` / `nuked` |
| Mega Drive / Genesis FM | `MAME (YM2612)` ✅ / `MAME (ASIC YM3438)` / `MAME (Enhanced YM3438)` / `Nuked (YM2612)` / `Nuked (YM3438)` | `genesis_plus_gx_ym2612` | `mame (ym2612)` / `mame (asic ym3438)` / `mame (enhanced ym3438)` / `nuked (ym2612)` / `nuked (ym3438)` |
| Sound Output | `Stereo` ✅ / `Mono` | `genesis_plus_gx_sound_output` | `stereo` / `mono` |
| Audio Filter | `Disabled` ✅ / `Low-Pass` / `EQ` | `genesis_plus_gx_audio_filter` | `disabled` / `low-pass` / `EQ` |
| Low-Pass Filter % | `5` / `10` / `15` / `20` / `25` / `30` / `35` / `40` / `45` / `50` / `55` / `60` ✅ / `65` / `70` / `75` / `80` / `85` / `90` / `95` | `genesis_plus_gx_lowpass_range` | `5` / `10` / `15` / `20` / `25` / `30` / `35` / `40` / `45` / `50` / `55` / `60` / `65` / `70` / `75` / `80` / `85` / `90` / `95` |
| PSG Preamp Level | `0` / `5` / `10` / `15` / `20` / `25` / `30` / `35` / `40` / `45` / `50` / `55` / `60` / `65` / `70` / `75` / `80` / `85` / `90` / `95` / `100` / `105` / `110` / `115` / `120` / `125` / `130` / `135` / `140` / `145` / `150` ✅ / `155` / `160` / `165` / `170` / `175` / `180` / `185` / `190` / `195` / `200` | `genesis_plus_gx_psg_preamp` | `0` / `5` / `10` / `15` / `20` / `25` / `30` / `35` / `40` / `45` / `50` / `55` / `60` / `65` / `70` / `75` / `80` / `85` / `90` / `95` / `100` / `105` / `110` / `115` / `120` / `125` / `130` / `135` / `140` / `145` / `150` / `155` / `160` / `165` / `170` / `175` / `180` / `185` / `190` / `195` / `200` |
| FM Preamp Level | `0` / `5` / `10` / `15` / `20` / `25` / `30` / `35` / `40` / `45` / `50` / `55` / `60` / `65` / `70` / `75` / `80` / `85` / `90` / `95` / `100` ✅ / `105` / `110` / `115` / `120` / `125` / `130` / `135` / `140` / `145` / `150` / `155` / `160` / `165` / `170` / `175` / `180` / `185` / `190` / `195` / `200` | `genesis_plus_gx_fm_preamp` | `0` / `5` / `10` / `15` / `20` / `25` / `30` / `35` / `40` / `45` / `50` / `55` / `60` / `65` / `70` / `75` / `80` / `85` / `90` / `95` / `100` / `105` / `110` / `115` / `120` / `125` / `130` / `135` / `140` / `145` / `150` / `155` / `160` / `165` / `170` / `175` / `180` / `185` / `190` / `195` / `200` |
| CD-DA Volume | `0` / `5` / `10` / `15` / `20` / `25` / `30` / `35` / `40` / `45` / `50` / `55` / `60` / `65` / `70` / `75` / `80` / `85` / `90` / `95` / `100` ✅ | `genesis_plus_gx_cdda_volume` | `0` / `5` / `10` / `15` / `20` / `25` / `30` / `35` / `40` / `45` / `50` / `55` / `60` / `65` / `70` / `75` / `80` / `85` / `90` / `95` / `100` |
| PCM Volume | `0` / `5` / `10` / `15` / `20` / `25` / `30` / `35` / `40` / `45` / `50` / `55` / `60` / `65` / `70` / `75` / `80` / `85` / `90` / `95` / `100` ✅ | `genesis_plus_gx_pcm_volume` | `0` / `5` / `10` / `15` / `20` / `25` / `30` / `35` / `40` / `45` / `50` / `55` / `60` / `65` / `70` / `75` / `80` / `85` / `90` / `95` / `100` |
| EQ Low | `0` / `5` / `10` / `15` / `20` / `25` / `30` / `35` / `40` / `45` / `50` / `55` / `60` / `65` / `70` / `75` / `80` / `85` / `90` / `95` / `100` ✅ | `genesis_plus_gx_wide_audio_eq_low` | `0` / `5` / `10` / `15` / `20` / `25` / `30` / `35` / `40` / `45` / `50` / `55` / `60` / `65` / `70` / `75` / `80` / `85` / `90` / `95` / `100` |
| EQ Mid | `0` / `5` / `10` / `15` / `20` / `25` / `30` / `35` / `40` / `45` / `50` / `55` / `60` / `65` / `70` / `75` / `80` / `85` / `90` / `95` / `100` ✅ | `genesis_plus_gx_wide_audio_eq_mid` | `0` / `5` / `10` / `15` / `20` / `25` / `30` / `35` / `40` / `45` / `50` / `55` / `60` / `65` / `70` / `75` / `80` / `85` / `90` / `95` / `100` |
| EQ High | `0` / `5` / `10` / `15` / `20` / `25` / `30` / `35` / `40` / `45` / `50` / `55` / `60` / `65` / `70` / `75` / `80` / `85` / `90` / `95` / `100` ✅ | `genesis_plus_gx_wide_audio_eq_high` | `0` / `5` / `10` / `15` / `20` / `25` / `30` / `35` / `40` / `45` / `50` / `55` / `60` / `65` / `70` / `75` / `80` / `85` / `90` / `95` / `100` |
| Light Gun Input | `Light Gun` ✅ / `Touchscreen` | `genesis_plus_gx_gun_input` | `lightgun` / `touchscreen` |
| Show Light Gun Crosshair | `Disabled` ✅ / `Enabled` | `genesis_plus_gx_gun_cursor` | `disabled` / `enabled` |
| Invert Mouse Y-Axis | `Disabled` ✅ / `Enabled` | `genesis_plus_gx_invert_mouse` | `disabled` / `enabled` |
| Remove Per-Line Sprite Limit | `Disabled` ✅ / `Enabled` | `genesis_plus_gx_no_sprite_limit` | `disabled` / `enabled` |
| Enhanced per-tile vertical scroll | `Disabled` ✅ / `Enabled` | `genesis_plus_gx_enhanced_vscroll` | `disabled` / `enabled` |
| Enhanced per-tile vertical scroll limit | `2` / `3` / `4` / `5` / `6` / `7` / `8` ✅ / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` | `genesis_plus_gx_enhanced_vscroll_limit` | `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` |
| CPU Speed | `100%` ✅ / `125%` / `150%` / `175%` / `200%` | `genesis_plus_gx_overclock` | `100%` / `125%` / `150%` / `175%` / `200%` |
| System Lock-Ups | `Enabled` ✅ / `Disabled` | `genesis_plus_gx_force_dtack` | `enabled` / `disabled` |
| 68K Address Error | `Enabled` ✅ / `Disabled` | `genesis_plus_gx_addr_error` | `enabled` / `disabled` |
| CD access time |  `Enabled` ✅ / `Disabled` | `genesis_plus_gx_cd_latency` | `enabled` / `disabled` |
| PSG Tone Channel 0 Volume % | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `genesis_plus_gx_psg_channel_0_volume` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
| PSG Tone Channel 1 Volume % | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `genesis_plus_gx_psg_channel_1_volume` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
| PSG Tone Channel 2 Volume % | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `genesis_plus_gx_psg_channel_2_volume` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
| PSG Tone Channel 3 Volume % | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `genesis_plus_gx_psg_channel_3_volume` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
| Mega Drive / Genesis FM Channel 0 Volume % | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `genesis_plus_gx_md_channel_0_volume` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
| Mega Drive / Genesis FM Channel 1 Volume % | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `genesis_plus_gx_md_channel_1_volume` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
| Mega Drive / Genesis FM Channel 2 Volume % | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `genesis_plus_gx_md_channel_2_volume` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
| Mega Drive / Genesis FM Channel 3 Volume % | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `genesis_plus_gx_md_channel_3_volume` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
| Mega Drive / Genesis FM Channel 4 Volume % | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `genesis_plus_gx_md_channel_4_volume` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
| Mega Drive / Genesis FM Channel 5 Volume % | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `genesis_plus_gx_md_channel_5_volume` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
| Master System FM (YM2413) Channel 0 Volume % | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `genesis_plus_gx_sms_fm_channel_0_volume` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
| Master System FM (YM2413) Channel 1 Volume % | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `genesis_plus_gx_sms_fm_channel_1_volume` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
| Master System FM (YM2413) Channel 2 Volume % | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `genesis_plus_gx_sms_fm_channel_2_volume` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
| Master System FM (YM2413) Channel 3 Volume % | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `genesis_plus_gx_sms_fm_channel_3_volume` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
| Master System FM (YM2413) Channel 4 Volume % | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `genesis_plus_gx_sms_fm_channel_4_volume` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
| Master System FM (YM2413) Channel 5 Volume % | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `genesis_plus_gx_sms_fm_channel_5_volume` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
| Master System FM (YM2413) Channel 6 Volume % | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `genesis_plus_gx_sms_fm_channel_6_volume` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
| Master System FM (YM2413) Channel 7 Volume % | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `genesis_plus_gx_sms_fm_channel_7_volume` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
| Master System FM (YM2413) Channel 8 Volume % | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `genesis_plus_gx_sms_fm_channel_8_volume` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/libretro/Genesis-Plus-GX-Wide/](https://github.com/libretro/Genesis-Plus-GX-Wide/)