---
title: Input
description: 
published: true
date: 2021-08-08T18:28:16.226Z
tags: compatibility, input
editor: markdown
dateCreated: 2021-08-05T20:52:11.914Z
---

### USB Encoder

| Device | Status | Latest version used | Comments |
| :---: | :---: | :---: | :---: | :---: |
| DragonRise | ✅ | 4.0.5 |  |
| [Juyao](http://www.juyao999.com/h-pd-65-0_304_12_-1.html) | ✅ | 4.0.0 | usbhid needed (test necessary for 4.1.0) |
| [XCSOURCE 2 Players USB Encoder](https://www.amazon.fr/XCSOURCE-Joueurs-Encodeur-Manette-AC491/dp/B01LAUYMQ6?tag=httpwwwrecalb-21) | ✅ | 4.0.0 | USBHID needed |
| [Xin-mo](http://www.xin-mo.com/) | ✅ | 4.0.0 |  |
| [ZERO-DELAY-PCBA](https://www.focusattack.com/ps3-pc-zero-delay-usb-encoder-pcb-japan-style-controls/) | ✅ | 4.0.2 | [Buy](https://www.banggood.com/Game-DIY-Arcade-Set-Kits-Replacement-Parts-USB-Encoder-to-PC-Joystick-and-Buttons-p-1039974.html?akmClientCountry=FR&cur_warehouse=CN&ID=228) |

### USB Keyboard Encoder

| Device | Status | Latest version used | Comments |
| :---: | :---: | :---: | :---: |
| iPAC2 | ✅ | 4.0.0 |  |
| mini pac | ✅ | 4.0.2 |  |
| Ultra Slim Rii | ❌ | 4.0.5 |  |

### Self-powered USB Hub

| Device | Status | Comments |
| :---: | :---: | :---: |
| AmazonBasics Hub USB 3.0 4 ports | ❌ | [Buy](https://www.amazon.fr/gp/product/B00E0NHKEM/ref=oh_aui_detailpage_o01_s00?ie=UTF8&psc=1??tag=httpwwwrecalb-21) |
| Hub USB 2.0 Plugable | ✅ | [Buy](https://www.amazon.fr/gp/product/B00BMGP0RE/ref=oh_aui_detailpage_o09_s00?ie=UTF8&psc=1) |
