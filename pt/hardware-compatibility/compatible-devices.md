---
title: Compatibilidade dos periféricos
description: 
published: true
date: 2021-11-28T14:30:51.862Z
tags: compatibilidade, periféricos
editor: markdown
dateCreated: 2021-11-28T14:30:51.862Z
---

Este projeto e realizado com a ajuda de vários membros da comunidade para compartilhar todos os dispositivos que funcionam "plug and play" e dispositivos que não funcionam.

Sinta-se à vontade para fazer quaisquer acréscimos ou alterações para criar um banco de dados de compatibilidade para o Recalbox!