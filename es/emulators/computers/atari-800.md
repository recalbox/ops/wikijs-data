---
title: Atari 800
description: 
published: true
date: 2024-07-26T12:45:44.901Z
tags: atari, 800, atari-800, ordenadores
editor: markdown
dateCreated: 2023-09-02T18:37:48.800Z
---

![](/emulators/computers/atari800.svg){.align-center}

## Especificaciones técnicas

* **Fabricante**: Atari, Inc.
* **Año de lanzamiento**: 1979
* **Sistema operativo**: Atari OS
* **Procesador**: MOS Technology 6502B a 1,8 Mhz
* **RAM**: 8kB base, 48 kB max.
* **ROM**: 8KB (Atari Basic)
* **Chip de sonido**: 4× osciladores con mezcla de ruido
* **Pantalla**: 320x192 monocromo, 160x96 con 128 colores

## Presentación

En diciembre de 1978, **Atari** presentó los **Atari 400** y **800**. Estos ordenadores no estuvieron disponibles hasta finales de 1979 debido a problemas de producción.

El Atari **800** es un ordenador de 8 bits basado en la **MOS Technology 6502A** a 7,70 MHz y capaz de albergar hasta 48Kb de RAM. Utiliza un teclado QWERTY estándar con 62 teclas convencionales y 4 teclas especiales a la derecha del teclado.

Se diferenciaba de otros ordenadores por sus capacidades gráficas y de sonido. Era capaz de mostrar 128 colores en la pantalla utilizando el procesador gráfico **CTIA**, y hasta 256 colores utilizando el procesador gráfico superior **GTIA** en versiones posteriores del ordenador. El 400 fue el primer ordenador capaz de mostrar simultáneamente 4 objetos programables en pantalla llamados "Player-missiles" (también conocidos como "Sprints" en los ordenadores **Commodore**). En aquella época, la mayoría de los ordenadores sólo mostraban pantallas monocromas o de 8 colores de forma primitiva. Los gráficos estaban soportados por un chip personalizado llamado "**ANTIC**" (**CTIA/GTIA**). Este chip estaba diseñado para funcionar como coprocesador y asumir la carga de trabajo del procesador principal para mostrar los gráficos y los colores en la pantalla.

El equipo que desarrolló los chips personalizados en el **400** y el **800** estaba dirigido por **Jay Miner** quien, tras dejar Atari, dirigió los equipos que desarrollaron los chips personalizados que rodeaban al procesador **Motorola MC68000** que proporcionaría el ordenador más avanzado de la época, ¡el **Amiga 1000**!

El sonido lo proporcionaba otro chip llamado "**POKEY**" y producía 4 voces para proporcionar un sonido más realista en comparación con otros ordenadores del mercado. Se podía conectar en serie una unidad de cinta, una unidad de disco de 5 1/4", un módem o una impresora a través de conectores especiales tipo D de 13 patillas. El **Atari 800** también podía alojar hasta 4 joysticks tipo D de 9 patillas en los puertos de la parte frontal de la máquina. Los joysticks utilizados eran del tipo estándar utilizado en el **Atari 2600 VCS**.

Aunque el 800 podía conectarse a un televisor estándar con el cable de antena, incluía un conector DIN de 5 patillas en el lado derecho para conectar un monitor en color de alta resolución.

El ordenador se lanzó originalmente con 16K de RAM, pero podía ampliarse fácilmente a 48K añadiendo módulos de **RAM** de 16K en las ranuras previstas bajo la tapa, que podía retirarse por la parte superior.

El **Atari 800** era uno de los pocos ordenadores que no utilizaba una versión del **BASIC** de **Microsoft**, sino que utilizaba una versión propia, lo que hacía más complicada la conversión de programas escritos en **BASIC**. Por alguna extraña razón, **BASIC** no estaba incluido en la **ROM**, sino que tenía que cargarse instalando un cartucho en una de las 2 ranuras situadas bajo la trampilla de la parte frontal de la máquina. Tenía que ser insertado en la ranura de la izquierda como la mayoría de los cartuchos en el **800**. Algunos cartuchos sólo utilizaban la ranura de la derecha. Además, los cartuchos **Atari VCS** no se podían insertar ahí. El **800** tenía un modulador de RF incorporado, sin coste adicional, que podía conectarse directamente a cualquier televisor.

## Emuladores

[Libretro Atari800](libretro-atari800)