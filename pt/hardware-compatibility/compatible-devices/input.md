---
title: Entrada
description: Codificadores USB
published: true
date: 2021-11-28T23:05:42.261Z
tags: input, compatibilidade, entrada
editor: markdown
dateCreated: 2021-11-28T23:05:42.261Z
---

### Codificadores USB

| Periférico | Status | Última versão testada | Comentários |
| :---: | :---: | :---: | :---: | :---: |
| DragonRise | ✅ | 4.0.5 |  |
| [Juyao](http://www.juyao999.com/h-pd-65-0_304_12_-1.html) | ✅ | 4.0.0 | Precisa do usbhid (teste necessário para 4.1.0) |
| [XCSOURCE 2 Joueurs Encodeur USB](https://www.amazon.fr/XCSOURCE-Joueurs-Encodeur-Manette-AC491/dp/B01LAUYMQ6?tag=httpwwwrecalb-21) | ✅ | 4.0.0 | Besoin du USBHID |
| [Xin-mo](http://www.xin-mo.com/) | ✅ | 4.0.0 |  |
| [ZERO-DELAY-PCBA](https://www.focusattack.com/ps3-pc-zero-delay-usb-encoder-pcb-japan-style-controls/) | ✅ | 4.0.2 | [Comprado](https://www.banggood.com/Game-DIY-Arcade-Set-Kits-Replacement-Parts-USB-Encoder-to-PC-Joystick-and-Buttons-p-1039974.html?akmClientCountry=FR&cur_warehouse=CN&ID=228) |

### Codificadores de teclado USB

| Periférico | Status | Última versão testada | Comentários |
| :---: | :---: | :---: | :---: |
| iPAC2 | ✅ | 4.0.0 |  |
| mini pac | ✅ | 4.0.2 |  |
| Ultra Slim Rii | ❌ | 4.0.5 |  |


### Hub USB com alimentação própria

| Periférico | Status | Comentários |
| :---: | :---: | :---: |
| AmazonBasics Hub USB 3.0 4 ports | ❌ | [Comprado](https://www.amazon.fr/gp/product/B00E0NHKEM/ref=oh_aui_detailpage_o01_s00?ie=UTF8&psc=1??tag=httpwwwrecalb-21) |
| Hub USB 2.0 Plugable | ✅ | [Comprado](https://www.amazon.fr/gp/product/B00BMGP0RE/ref=oh_aui_detailpage_o09_s00?ie=UTF8&psc=1) |
