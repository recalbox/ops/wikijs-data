---
title: Nintendo Entertainment System
description: Nintendo Entertainment System
published: true
date: 2021-10-03T20:37:33.514Z
tags: system, consoles, nintendo, nes, entertainment
editor: markdown
dateCreated: 2021-05-21T08:00:36.652Z
---

![](/emulators/consoles/nes.svg){.align-center}

## Fiche technique

* **Fabricant** : Nintendo
* **Année de sortie** : 1983
* **Quantités vendues** : 61.91 millions
* **Meilleur jeu vendu** : Super Mario Bros.
* **Processeur** : 8-bit MOS 6502 @ 1.79MHz
* **RAM** : 2kB / VRAM : 2kB
* **Vidéo** : 8-Bit PPU (Picture Processing Unit)
* **Résolution** : 256 x 240 pixels, 25 couleurs (sur une palette de 64 couleurs)
* **Puce sonore** : PSG sound (2 Square Waves, 1 Triangle Wave, 1 White Noise)
* **Taille des cartouches** : 8kB - 1MB

## Présentation

La **Nintendo Entertainment System**, par abréviation **NES**, également couramment appelée **Nintendo** en France, est une console de jeux vidéo de génération 8 bits fabriquée par l'entreprise japonaise Nintendo et distribuée à partir de 1985 (1987 en Europe). Son équivalent japonais est la **Family Computer** ou **Famicom** sortie quelques années avant, en 1983. En Corée du Sud, la NES porta le nom de **Hyundai Comboy** et en Inde, celui de **Tata Famicom**.

La console connut un succès mondial, ce qui aida à redynamiser l'industrie du jeu vidéo après le krach du jeu vidéo de 1983, et ce qui fixa les normes pour les consoles suivantes, du game design aux procédures de gestion.

Super Mario Bros. fut le jeu le plus vendu sur la console. Son succès fut tel que ce jeu justifiait bien souvent l'achat de la console à lui tout seul, devenant ainsi un killer game.

La Nintendo Entertainment System est la 13e console de jeux vidéo la plus vendue de tous les temps avec 61,91 millions d'unités vendues.

## Émulateurs

[Libretro Nestopia](libretro-nestopia)
[Libretro FCEUmm](libretro-fceumm)
[Libretro FCEUnext](libretro-fceunext)
[Libretro Quicknes](libretro-quicknes)
[Libretro Mesen](libretro-mesen)
[Libretro FBNeo](libretro-fbneo)