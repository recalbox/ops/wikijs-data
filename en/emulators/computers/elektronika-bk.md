---
title: Elektronika BK
description: 
published: true
date: 2021-08-09T22:12:48.368Z
tags: computers, elektronika, bk, 7.2+
editor: markdown
dateCreated: 2021-07-28T20:14:14.856Z
---

![](/emulators/computers/elektronikabk.svg){.align-center}

## Fiche technique

* **Manufacturer**: Elektronika
* **Year of release**: 1984
* **OS**: Focal
* **CPU**: K1801VM1 @ 3MHz
* **RAM**: 32 KB
* **ROM**: 8 KB
* **GPU**: K1801VP1-037
* **Sound**: Covox DACs
* **Resolution**: 512×256 (monochrome) / 256×256 (4 colors)

## Presentation

The Elektronika BK is a series of 16-bit PFP-11 compatible fanless home computers developed under the Elektronika brand by NPO Scientific Center, the leading Soviet team of the time. It was also the predecessor of the more powerful UKNC and DVK computers.

Released in 1984 (developed in 1983), the computer was based on the K1801BM1 (the Soviet LSI-11 compatible processor) and was the only "official" computer to have been mass produced.

They were sold around 600 - 650 rubles. It was expensive but affordable, which made it one of the most popular computers in the Soviet Union, despite the fact that it contains several problems. Later, in the 90s, their powerful processor and simple, easy-to-program design made them popular as demonstration machines. BK (БК) is Russian for "бытовой компьютер" - family computer. The machines were also used for a short time as cash registers, for example, in department stores GUM.

Although the BK-0010 was one of the cheapest Soviet computers and speed (as well as memory, graphics, etc.) differed little from the simple 8-bit models, it was one of the first fully 16-bit home computers in the world (unlike the TI-99 / 4A, BK had controllers with the same width of data bus). The Intellivision used a very similar, all-16-bit processor, the General Instrument CP1600, and with the Keyboard Component or ECS extensions, it was transformed into a fully 16-bit home computer. The IBM PC and PCjr were 8/16-bit computers, as their 8088 processor has an 8-bit data bus and a 16-bit internal bus.

## Emulators

[Libretro BK](libretro-bk)