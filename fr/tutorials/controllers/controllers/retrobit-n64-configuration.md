---
title: Configuration de la manette N64 Retrobit
description: 
published: true
date: 2023-12-14T00:06:27.362Z
tags: n64, manette, retrobit
editor: markdown
dateCreated: 2023-04-19T08:57:16.716Z
---

## Introduction

Il existe plusieurs manettes N64 dans le commerce, et RetroArch ne sait pas utiliser les boutons C correctement. Ce tutoriel va vous indiquer comment se servir de la manette Tribute64 de chez retrobit.

![](/tutorials/controllers/controllers/retrobit-tribute64.png)

## Utilisation

Malheureusement, RetroArch ne connait pas les 4 boutons C d'origine et se limite à A / B / X / Y. Nous allons voir comment y remédier au maximum des possibilitées.

### Allumage / configuration

La manette a son propre dongle USB inséparable. Vous devez le brancher sur votre recalbox et utiliser la combinaison de touches `START` + `HAUT` pour l'allumer.

La configuration de base est fournie dans recalbox, il n'y a pas besoin de la remapper. Néanmoins, si ça doit arriver, suivez les points suivants :

- Le bouton A dans EmulationStation doit correspondre au bouton C-Bas de la manette
- Le bouton B dans EmulationStation doit correspondre au bouton A de la manette
- Le bouton X dans EmulationStation doit correspondre au bouton C-Gauche de la manette
- Le bouton Y dans EmulationStation doit correspondre au bouton B de la manette
- Le bouton Select dans EmulationStation doit correspondre au bouton gris de gauche au-dessus de Start de la manette
- Le bouton Hotkey dans EmulationStation doit correspondre au bouton gris de droite au-dessus de Start de la manette

> Utilisez la manette que sur les jeux N64 et 64DD. Elle n'est pas faite pour un autre système.
> {.is-info}

Une fois ceci effectué, vous devez créer 1 fichier de surcharge de configuration. Celle-ci va permettre d'avoir tous les boutons fonctionnels.

### Surcharges de configuration

>Ceci ne fonctionne que pour les cores Libretro.
{.is-info}

Vous devez créer une fichier texte vide nommé `.retroarch.cfg` dans `/recalbox/share/roms/n64` et y insérez le contenu suivant :

```ini
input_player1_b_btn = "2" # A
input_player1_a_btn = "15" # No affectation
input_player1_y_btn = "15" # No affectation
input_player1_y_btn = "1" # B
input_player1_start_btn = "9" # Start
input_player1_select_btn = "8" # Select
input_player1_up_btn = "h0up" # D-pad up
input_player1_down_btn = "h0down" # D-pad down
input_player1_left_btn = "h0left" # D-pad left
input_player1_right_btn = "h0right" # D-pad right
input_player1_l_btn = "4" # L
input_player1_r_btn = "5" # R
input_player1_l2_btn = "6" # Z
input_player1_r_x_plus_btn = "10" # C-right
input_player1_r_x_minus_btn = "0" # C-left
input_player1_r_y_plus_btn = "3" # C-down
input_player1_r_y_minus_btn = "11" # C-up
input_player1_l_x_plus_axis = "+0" # Stick right
input_player1_l_x_minus_axis = "-0" # Stick left
input_player1_l_y_plus_axis = "+1" # Stick down
input_player1_l_y_minus_axis = "-1" # Stick up
input_player1_menu_toggle_btn = "12" # Unknown
```

Bon jeu !