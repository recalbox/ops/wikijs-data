---
title: FAQ
description: 
published: true
date: 2023-09-20T09:48:15.275Z
tags: faq, apple, macintosh, 9.0+
editor: markdown
dateCreated: 2023-09-20T09:48:15.275Z
---

Estas son las preguntas relacionadas con el uso del sistema Macintosh.

## Configurar la pantalla del sistema a dos colores

Algunos juegos antiguos de Macintosh os pedirán configurar la pantalla del sistema en dos colores (blanco y negro).
Sin embargo, puede ocurrir que el fichero BIOS del MinivMac no incluya la utilidad para configurar los colores de la pantalla.
Para solucionar este problema, hay que modificar el fichero MinivMacBootv2.dsk utilizando el emulador Mini vMac.

### Necesitaréis:

 - Un PC para realizar las operaciones
 - Ficheros MinivMacBootv2.dsk y MacII.ROM
 - Disquetes de instalación para **System 7.5**.
 - El emulador Mini vMac (disponible aquí: https://www.gryphel.com/c/minivmac/dnld_mii.html)
 
### Pasos a seguir

1. Iniciad el Mini vMac. 2. Si el Mini vMac muestra el mensaje "No se puede localizar MacII.ROM", arrastrad el fichero al directorio donde habéis extraído el Mini vMac.
![minivmac-rom-missing.png](/tutoriales/juegos/guías/macintosh/minivmac-rom-missing.png){.full-width}

2. Si se detecta el fichero, se escuchará un pequeño pitido. Abrid el fichero "MinivMacBootv2.dsk". Podéis arrastrar y soltar el fichero en el emulador, o abrir el fichero a través del menú "Fichero > Abrir imagen de disco".

3. Insertad el primer disquete de instalación.  Se abrirá el contenido del disquete.
![minivmac-install-disk1.png](/tutorials/games/guides/macintosh/minivmac-install-disk1.png){.full-width}

4. Haced click en el fichero e "Instalar". Se iniciará el programa de instalación. Pulsad en "Continuar".
minivmac-install-step1.png](/tutorials/games/guides/macintosh/minivmac-install-step1.png){.full-width}

5. Para instalar la herramienta, seleccionad "Instalación personalizada" en el menú desplegable de la parte superior izquierda.
minivmac-install-step2.png](/tutorials/games/guides/macintosh/minivmac-install-step2.png){.full-width}

6. Buscad "Panel de control" y pulsad en el triángulo de la izquierda para desplegar las opciones. A continuación, seleccionad "Monitores". Por último, pulsad "Instalar".
![minivmac-install-step3.png](/tutorials/games/guides/macintosh/minivmac-install-step3.png){.full-width}

7. El programa de instalación os pedirá insertar el disquete 3. Arrastrad y soltad el fichero del disquete 3 en el emulador o abre el archivo.
![minivmac-install-step4.png](/tutoriales/juegos/guías/macintosh/minivmac-install-step4.png){.full-width}

8. El programa indica que la instalación se ha realizado correctamente. Os pide reiniciar. Pulsad "Reiniciar". El emulador puede tardar de 1 a 2 minutos en volver. Si aparece el icono de un disquete durante el reinicio, abrid de nuevo el fichero "MinivMacBootv2.dsk".
minivmac-install-step5.png](/tutorials/games/guides/macintosh/minivmac-install-step5.png){.full-width}

9. Podéis comprobar que la utilidad se ha instalado correctamente pulsando Apple > Paneles de control > Monitores. Si está bien instalado, podéis apagar la máquina virtual (Apple > Apagar).
![minivmac-install-step6.png](/tutorials/games/guides/macintosh/minivmac-install-step6.png){.full-width}

10. Ahora podéis copiar y reemplazar el fichero MinivMacBootv2.dsk de vuestra Recalbox, situado en la carpeta bios. Cuando el juego os pida cambiar a dos colores, abrid el panel de control "Monitores" y pulsad en "Blanco y negro".
minivmac-install-step7.png](/tutorials/games/guides/macintosh/minivmac-install-step7.png){.full-width}