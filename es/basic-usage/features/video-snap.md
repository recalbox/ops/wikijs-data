---
title: Video Snaps
description: 
published: true
date: 2023-08-25T14:22:26.985Z
tags: video, snaps
editor: markdown
dateCreated: 2023-08-25T14:22:26.985Z
---

Los "snaps vídeo" son ni más ni menos que la présentación de un pequeño vídeo de gameplay en lugar de vuestro "scrap" habitual dentro de la lista de juegos.

Podéis ver une ejemplo de lo que hablamos [aquí](https://www.youtube.com/watch?v=r5oabVrfQpk).

>Para aquellos que nunca han utilizado esta funcionalidad, hay que advertir que los vídeos ocupan un tamaño importante en el disco duro, aunqué se encuentren en formato MP4 _(de 1 a 6 MB)_.
>Para disfrutar plenamente de esta funcionalidad, se recomienda disponer de un disco de gran tamaño, de 1 o 2 TB!
{.is-warning}

## Configuración

Para obtener los vídeos de los juegos, debéis (re)scrapar vuestras colecciones con ayuda de Skraper o del scraper interno de Recalbox.

### Con el escraper interno

Consultad [esta página](./../../basic-usage/features/internal-scraper) de la documentación.

### A través del programa Skraper

Podéis consultar [este vídeo](https://www.youtube.com/watch?v=G388Gc6kkRs) que os enseñará a utilizar el programa Skraper.