---
title: Et pour les utilisateurs avancés
description: Comment rajouter des jeux "lightgun" soi même comme un grand ;-)
published: true
date: 2024-07-25T13:03:36.639Z
tags: lightgun, 7.2+, utilisateurs avancés
editor: markdown
dateCreated: 2021-05-21T08:12:18.092Z
---

## Comment aller plus loin ?

### L'idée de base...

Le principe est d'avoir un fichier unique de configuration regroupant toutes les informations nécessaires pour les jeux "Lightgun" de tous les systèmes & émulateurs. 

Ainsi, on pourra faire de l'héritage, des regroupements de jeux mais aussi gérer des particularités sans avoir un fichier par jeu ou par système comme on peut le faire déjà mais sans pouvoir regrouper des systèmes par exemple. Ce qui est possible avec ce xml unique. 

De plus, on pourra, dans les versions suivantes, rajouter des aides en lignes, des messages lors du chargement des jeux, etc...

Un autre avantage par rapport aux surcharges classiques : on peut rajouter des fonctions dans le python du Configgen pour aller chercher/identifier si un matériel est présent comme la Mayflash dolphin bar ou savoir sur quel board/pc on se trouve. On peut décider de configurer ainsi entre 1 à 3 joueurs Lightgun à ce jour.

>Pour rappel, ceci n'est possible que pour Libretro pour l'instant. Rien ne nous empêcherait de le faire pour d'autres émulateurs avec le fichier xml que l'on a déjà.
{.is-info}

### Le fichier XML de configuration

En fait, la configuration des jeux est dans un fichier xml (pour info, c'est un ".cfg" en fait).

Le fichier est organisé ainsi en terme de structure (ici il est vide sans configurations pour montrer la structure xml au complet) :

```xml
<?xml version="1.0"?>
<root>
	<emulatorOptions>
	  <option />
 	</emulatorOptions>
  <coreOptions>
	  <option />
  </coreOptions>
  <system>
 		<emulatorList>
      <emulator />
    </emulatorList>
		<emulatorOptions>
			<option />
		</emulatorOptions>
    <coreOptions>
			<option />
    </coreOptions>
    <gameList>
      <game>
        <emulatorOptions>
          <option />
        </emulatorOptions> 
        <coreOptions>
          <option />
        </coreOptions>
      </game>
    </gameList>
  </system>
</root>
```

### Les Tags XML

**&lt;root>** : c'est la base du XML où tout va se retrouver ainsi qu'une version pour suivre les évolution de ce fichier

```xml
<?xml version="1.0"?>
<root>
	<!-- internal version of the file to help follow-up of update, same version until release -->
	<version>1.0.6 - 18-03-2024</version>
</root>
```

**&lt;emulatorOptions>** : c'est la partie dans &lt;root> et commune de surchage de `retroarchcustom.cfg` que l'on va avoir pour tous les systèmes, dans le cas du lightgun, on va y mettre les touches de base comme select/exit/start/trigger.

```xml
	<!-- MANDATORY inputs (for retroarchcustom.cfg): comon part to use for all systems (using dolphinbar and until 2 wiimotes) -->
	<emulatorOptions>
        <option name="input_player1_gun_start" value="enter" />
        <option name="input_player2_gun_start" value="enter" />
        <option name="input_player1_gun_select" value="escape" />
        <option name="input_player2_gun_select" value="escape" />
        <!-- force selecte and start for consoles games -->
        <option name="input_player1_select" value="escape" />
        <option name="input_player2_select" value="escape" />
        <option name="input_player1_start" value="enter" />
        <option name="input_player2_start" value="enter" />
        <!-- for many it's necessary to move in menu or else -->
        <option name="input_player1_gun_dpad_up" value="up" />
        <option name="input_player1_gun_dpad_down" value="down" />
        <option name="input_player1_gun_dpad_right" value="right" />
        <option name="input_player1_gun_dpad_left" value="left" />
        <option name="input_player2_gun_dpad_up" value="up" />
        <option name="input_player2_gun_dpad_down" value="down" />
        <option name="input_player2_gun_dpad_right" value="right" />
        <option name="input_player2_gun_dpad_left" value="left" />
        <!-- index to manage distinction between guns -->
        <option name="input_player1_mouse_index" value="GUNP1" />
        <option name="input_player2_mouse_index" value="GUNP2" />
        <!-- wiimotes home button for exit game -->
		    <option name="input_exit_emulator" value="lsuper" />
	  	  <option name="input_enable_hotkey" value="lsuper" />
        <!-- set on fullscreen for don't see cursor mouse on x86(-64) -->
	  	  <option name="video_fullscreen" value="true" />
        <!-- to have help using overlay switch and finally ask to press 2 times home to exit -->
        <option name="input_overlay_next" value="lsuper" />
        <option name="quit_press_twice" value="true" />
 	</emulatorOptions>
```

**&lt;coreOptions>** : c'est la partie dans `&lt;root>` et commune de surchage de `retroarch-core-options.cfg` que l'on va avoir. Dans notre cas, ce n'est pas utilisé de manière commune mais cela serait possible.

```xml
    <!-- OPTIONAL options (for retroarch-core-options.cfg): comon part to use for all systems (using dolphinbar and until 2 wiimotes) -->
  	<coreOptions>
        <!-- only for more log on test -->
        <!-- <option name="libretro_log_level" value="2" />
        <option name="log_verbosity" value="true" /> -->
  	</coreOptions>
```

**&lt;option>** : ce sont les valeurs de base de configuration toujours composées de 2 attributs `name` et `value` :

```xml
<option name="input_player1_gun_start" value="enter" />
```

**&lt;system>** : c'est la partie qui regroupe toute la configuration d'un système. On y retrouve les tags `&lt;emulatorList>`, `&lt;emulator>`, `&lt;emulatorOptions>`, `&lt;coreOptions>`, `&lt;gameList>` et `&lt;game>`.

```xml
  <!-- ********************************************************* Nintendo Entertainment System ************************************************************** -->
	<system name="nes">
		<!-- MANDATORY: emulator and core selected for lightgun -->
		<emulatorList name="libretro">
			<emulator priority="1" name="libretro" core="fceumm" />
		</emulatorList>
    <!-- MANDATORY: inputs common to this system -->
		<emulatorOptions>
      	<!-- gamepad -->
      	<option name="input_libretro_device_p0" value="1" />
      	<!-- lightgun -->
      	<option name="input_libretro_device_p1" value="258" />
      	<!-- Wiimote button B -->
		  	<option name="input_player2_gun_trigger_mbtn" value="1" />
        <!-- Set index to GUNP1 on player 2 to have first wiimote as player 1 -->
        <option name="input_player2_mouse_index" value="GUNP1" />
		</emulatorOptions>
		<!-- MANDATORY: options common to this system -->
		<coreOptions>
		  	<option name="fceumm_zapper_mode" value="clightgun" />
		  	<option name="fceumm_zapper_tolerance" value="6" />
      	<option name="fceumm_show_crosshair" value="enabled" />
		</coreOptions>
        <gameList>
            <!-- list of games using lightgun -->
            <!-- see 'duckhunt' game to see more explainations to know how to setup game part -->
            <game name="3in1supergun" tested="true" />
```

**&lt;emulatorList>** : ce tag liste la liste des émulateurs compatibles, généralement limité à un seul :

```xml
    	<!-- MANDATORY: emulator and core selected for lightgun-->
    	<emulatorList>
      		<emulator priority="1" name="libretro" core="flycast" />
    	</emulatorList>
```

**&lt;emulator>** : ce tag a pour valeur la priorité, le nom de l'émulateur et core à utiliser. il se trouve toujours dans `&lt;emulatorList>` comme ci-dessus.

```xml
<emulator priority="1" name="libretro" core="flycast" />
```

**&lt;emulatorOptions>** : ce tag permet de surcharger `retroarchcustom.cfg` en plus de la partie `&lt;emulatorOptions>` au niveau des `&lt;system>`, `&lt;gameList>` et/ou `&lt;game>`.

```xml
<system>
.....
		<emulatorOptions>
      	<!-- gamepad -->
      	<option name="input_libretro_device_p0" value="1" />
      	<!-- lightgun -->
      	<option name="input_libretro_device_p1" value="258" />
      	<!-- Wiimote button B -->
		  	<option name="input_player2_gun_trigger_mbtn" value="1" />
        <!-- Set index to GUNP1 on player 2 to have first wiimote as player 1 -->
        <option name="input_player2_mouse_index" value="GUNP1" />
.....
```

```xml
.....
        <gameList>
            <emulatorOptions>
                <!-- no reload, this game is a free fire but need secondary input for grenade and more -->
                <option name="input_player1_a_mbtn" value="2" />
                <option name="input_player2_a_mbtn" value="2" />     
            </emulatorOptions>
            <game name="alien3thegun" tested="true" />
.....
```

```xml
.....            
            <game name="gunbuster" tested="true">
                <name>gunbuster</name>
                <emulatorOptions>
                    <!-- no reload free fire game but ned secondary input for grenade and more -->
                    <!-- move players with dpad on wiimote -->
                    <option name="input_player1_a" value="2" />
                    <option name="input_player1_gun_offscreen_shot_mbtn" value="nul" />
                    <option name="input_player2_a" value="2" />
                    <option name="input_player2_gun_offscreen_shot_mbtn" value="nul" />
                </emulatorOptions>
            </game>
.....
```

**&lt;coreOptions>**: ce tag permet de surcharger `retroarch-core-options.cfg`  en plus de la partie `&lt;coreOptions>` au niveau des `&lt;system>`, `&lt;gameList>` et/ou `&lt;game>`. 

```xml
        <!-- options common to this system-->
        <coreOptions>
            <option name="genesis_plus_gx_gun_input" value="lightgun" />
            <option name="genesis_plus_gx_gun_cursor" value="enabled" />
        </coreOptions>
```

**&lt;gameList>** : ce tag est la racine des groupes de jeux. Cela permet d'avoir une configuration distinct pour une partie des jeux dans un même système, on peut donc avoir plusieurs sections `&lt;gameList>` dans le même système.

```xml
        <gameList>
            <emulatorOptions>
                <!-- no reload, these games have a free fire but need secondary input for grenade and more -->
                <option name="input_player1_a_mbtn" value="2" />
                <option name="input_player2_a_mbtn" value="2" />     
            </emulatorOptions>
            <game name="alien3thegun" tested="true" />
            <game name="beastbusters" tested="true" />
            <game name="dragongun" tested="true" />
        </gameList>

```

 **&lt;game>** : ce tag qui est dans `&lt;gameList>` est pour définir la configuration du jeu en particulier.

```xml
            <game name="gunbuster" tested="ok">
                <emulatorOptions>
                    <!-- no reload free fire game but ned secondary input for grenade and more -->
                    <!-- move players with dpad on wiimote -->
                    <option name="input_player1_a" value="2" />
                    <option name="input_player1_gun_offscreen_shot_mbtn" value="nul" />
                    <option name="input_player2_a" value="2" />
                    <option name="input_player2_gun_offscreen_shot_mbtn" value="nul" />
                </emulatorOptions>
            </game>
```


>**&lt;game>** possède les attributs `name` (pour reconnaitre le jeu lancé) et `tested` pour savoir si on a déjà testé cette rom en particulier et connaitre son statut (2 valeurs possibles) :
>* **true** -> testé avec la configuration défini.
>* **false** -> testé mais pas fonctionnel, donc ne sera pas configuré pour fonctionner en mode lightgun pour l'instant (on a de l'espoir).

>Il faut toujours mettre le nom **sans espace/caractères spéciaux et en minuscule** pour le parsing de la "gamelist" d'un système par Recalbox. Attention, c’est le nom du jeu dans son plus simple appareil, c’est ni le nom du fichier de la rom ni le nom avec la zone géographique, etc… il faut rester le plus simple possible pour que la correspondance se fasse à coup sûr. **Faire un scrap permettra d'être sur d'avoir un jeu avec un nom identifiable.**
{.is-warning}

### L’héritage

En fait dans ce paragraphe, nous souhaitons vous expliquer que dans l'ordre la configuration se fera ainsi : 

`common` -> `system` -> `gameList` -> `game`

.... On vous laisse nous dire à quoi sera égale `input_player2_gun_trigger_mbtn` dans l'exemple suivant

```xml
<?xml version="1.0"?>
<root>
	<emulatorOptions>
	  <option name="input_player2_gun_trigger_mbtn" value="1" />
 	</emulatorOptions>
  <coreOptions>
  </coreOptions>
  <system>
		<emulatorList>
			<emulator></emulator>
		</emulatorList>
    <emulatorOptions>
      <string name="input_player2_gun_trigger_mbtn" value="2" />
    </emulatorOptions>
    <coreOptions>
    </coreOptions>
    <gameList>
      <emulatorOptions>
        <string name="input_player2_gun_trigger_mbtn" value="1" />
      </emulatorOptions>
      <coreOptions>
      </coreOptions>
      <game>
        <emulatorOptions>
          <option name="input_player2_gun_trigger_mbtn" value="2" />
        </emulatorOptions>
        <coreOptions>
        </coreOptions>
      </game>
    </gameList>
  </system>
</root>
```

### Gestion des index pour le multi-joueurs

Nous avons du gérer les index des "mouse" et le préciser de manière commune ou plus précise par jeu. Par exemple quand le joueur 1 est finalement sur le port 2, et le joueur 2 sur le port 3. Nous avons donc des mots clés comme `GUNP1`, `GUNP2` ou `GUNP3`.

```xml
.....
     <emulatorOptions>
        <!-- index to manage distinction between guns -->
        <option name="input_player1_mouse_index" value="GUNP1" />
        <option name="input_player2_mouse_index" value="GUNP2" />
        .....
```

```xml
        <gameList> <!-- games using justifier -->
            <emulatorOptions>
                <!-- lightguns always on port 2 -->
                <!-- For Justifier device (blue cross) -->
                <string name="input_libretro_device_p2" value="516" />
                <string name="input_player2_mouse_index" value="GUNP1" />
                <!-- Wiimote button B or Z -->
                <string name="input_player2_gun_trigger_mbtn" value="1" />
                <!-- For Justifier(P2) device (pink cross) -->
                <string name="input_libretro_device_p3" value="772" />
                <string name="input_player3_mouse_index" value="GUNP2" />
                <!-- Wiimote button B or Z -->
                <string name="input_player3_gun_trigger_mbtn" value="1" />
                <!-- Common inputs part replicated for this game using player 3 -->
                <string name="input_player3_gun_start" value="enter" />
                <string name="input_player3_gun_select" value="escape" />
                <!-- Common inputs part replicated to force select and start for consoles games -->
                <string name="input_player3_select" value="escape" />
                <string name="input_player3_start" value="enter" />
            </emulatorOptions>
            <game name="lethalenforcers" tested="true" />
        </gameList>
```

## Mais plus simplement...

En fait, le fichier xml de configuration n'est pas trouvable dans votre “share” mais il est dans le système et accessible uniquement en SSH : `/recalbox/share_init/system/.emulationstation/lightgun.cfg`.

>Pour pouvoir modifier ce fichier, il faudra monter la [partition du système](./../../../tutorials/system/access/remount-partition-with-write-access) en lecture/écriture.
{.is-warning}

### Comment rajouter un jeu/rom ?

Il suffit de trouver le système, exemple pour la NES :

![](/basic-usage/features/lightgun/advanced-users/advancedusers2.png)

Et de rajouter dans la section &lt;gameList> le tag &lt;game> puis &lt;name> ainsi :

```xml
           <game name="monjeu2" tested="true" />
```


>RAPPEL: Il faut toujours mettre le nom **sans espace/caractères spéciaux et en minuscule** pour le parsing de la "gamelist" d'un système par Recalbox. Attention, c’est le nom du jeu dans son plus simple appareil, c’est ni le nom du fichier de la rom ou le nom avec la zone géographique, etc… il faut rester le plus simple possible pour que la correspondance se fasse à coup sûr. **Faire un scrap permettra d'être sur d'avoir un jeu avec un nom identifiable.**
{.is-warning}

### Pour vérifier que c’est bien pris en compte

* Si le fichier est mal formaté, vous revenez directement à la liste des jeux.
* Si le nom n’est pas trouvé/reconnu, vous ne pourrez pas activer le mode Lightgun.
* Si vous êtes vraiment en mode Lightgun, vous devez pouvoir sortir du jeu avec la touche HOME de la Wiimote.

### Pour rajouter des configurations à un jeu

Le mieux est de regarder ce que l'on a fait pour d'autres jeux… En général, vous allez avoir besoin de modifier les paramètres correspondants aux boutons de tir primaire et secondaire, voire désactiver des touches (voir ci-après) mais cela peut être encore plus compliqué… Si c'est le cas, il faudra aller voir les manuels des jeux de l'époque et passer beaucoup de temps dans les menu de RetroArch… donc maitriser l'art de la surcharge !

```xml
        <game name="ninjaassault" tested="true">
            <emulatorOptions>
                <!-- for this game start p1 for P2 aux_c -->
                <string name="input_player1_gun_start" value="nul" />
                <string name="input_player1_gun_aux_b" value="enter" />
                <!-- for this game trigger -->
                <string name="input_player1_gun_trigger_mbtn" value="nul" />
                <string name="input_player1_gun_aux_a_mbtn" value="1" />
                <!-- for this game real offscreen reload -->
                <string name="input_player1_gun_offscreen_shot_mbtn" value="nul" />
            </emulatorOptions>
        </game>
```

### Pour rajouter un nouveau système

Dans ce cas, il faut vraiment connaitre le core à utiliser, les paramètres de RetroArch et en général configurer aussi le "device" dans `&lt;emulatorOptions>` ainsi que les paramètres "lightgun" du core dans `&lt;coreOptions>`.

```xml
......
<system name="megadrive">
    <!-- MANDATORY: emulator and core selected for lightgun -->
    <emulatorList name="libretro">
        <emulator priority="1" name="libretro" core="genesisplusgx" />
    </emulatorList>
    <!-- MANDATORY: inputs common to this system -->
    <emulatorOptions>
        <!-- Wiimote button B or Z -->
        <option name="input_player2_gun_trigger_mbtn" value="1" />
        <!-- Wiimote button A or c - aux when it's necessary as jump, rocket, etc... -->
        <option name="input_player2_gun_aux_a_mbtn" value="2" />
        <!-- <option name="input_player2_gun_aux_a_mbtn" value="2" /> -->
        <!-- gamepad always on port 1-->
        <option name="input_libretro_device_p1" value="1" />
    </emulatorOptions>
    <!-- MANDATORY: options common to this system -->
    <coreOptions>
        <option name="genesis_plus_gx_gun_input" value="lightgun" />
        <option name="genesis_plus_gx_gun_cursor" value="enabled" />
    </coreOptions>
    .....
```

**Bon courage !!!**