---
title: Games compatibility list
description: 
published: true
date: 2021-08-19T19:13:52.852Z
tags: lightgun, games, 7.2+
editor: markdown
dateCreated: 2021-08-19T19:13:52.852Z
---

## Introduction

Here you can have the list of games recognized as lightgun by system.

This list contains all games currently recognized as using lightgun. Only games with ✅ compatibility are listed in the LightGun virtual system in EmulationStation. The games marked with ❌ do not work or perhaps badly and those marked with ❔ have not been tested for various reasons (only work partially and not at the beginning of the game, etc.).

The core compatibility list does not take into account whether it is a portable version or not, it just indicates whether the necessary emulator is included or not.

This list will be updated for each new version if necessary.

## Nintendo Entertainment System

### Emulator

The required emulator is `Libretro FCEUmm`.

### Compatibility

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

### Games

| Game name | Compatibility | Notes |
| :--- | :--- | :--- |
| 3 In 1 Supergun | ✅ | - |
| Asder - 20 In 1 | ✅ | - |
| Adventures of Bayou Billy | ❔ | - |
| Baby Boomer | ✅ | - |
| Barker Bill's Trick Shooting | ✅ | - |
| Blood of Jurassic | ❔ | - |
| Chiller | ✅ | - |
| Cobra Mission | ❔ | - |
| Crime Busters | ❔ | - |
| Day Dreamin' Davey | ❔ | - |
| Duck Hunt | ✅ | - |
| Freedom Force | ✅ | - |
| Gotcha | ✅ | - |
| Gumshoe | ✅ | - |
| Hit Marmot | ❔ | - |
| Hogan's Alley | ✅ | - |
| Laser Invasion | ❔ | - |
| Lethal Weapon | ❔ | - |
| Lightgun Games 2 in 1 - Cosmocop + Cyber Monster | ❔ | - |
| Lightgun Games 2 in 1 - Tough Cop + Super Tough Cop | ❔ | - |
| Lone Ranger | ❔ | - |
| Master Shooter | ❔ | - |
| Mechanized Attack | ✅ | - |
| Operation Wolf | ✅ | - |
| Russian Roulette | ❔ | - |
| Super Russian Roulette | ❔ |
| Shootingrange | ✅ | - |
| Space Shadow | ❔ | - |
| Strike Wolf | ❔ | - |
| Totheearth | ✅ | - |
| Track & Field II | ❔ | - |
| Wild Gunman | ✅ | - |

## Super Nintendo

### Emulator

The required emulator is `Libretro snes9x`.

### Compatibility

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

### Games

| Game name | Compatibility | Notes |
| :--- | :--- | :--- |
| Battle Clash | ✅ | - |
| Bazooka Blitzkrieg | ✅ | - |
| Lamborghini American Challenge | ✅ | - |
| Lethal Enforcers | ✅ | - |
| Metal Combat - Falcon's Revenge | ✅ | - |
| Nintendo Scope 6 | ✅ | - |
| Operation Thunderbolt | ✅ | - |
| Revolution X | ❌ | - |
| Super Scope 6 | ✅ | - |
| T2 - The Arcade Game | ✅ | - |
| Tin Star | ✅ | - |
| Yoshi's Safari | ✅ | - |
| X Zone | ✅ | - |

## Megadrive

### Emulator

The required emulator is `Libretro GenesisPlusGX`.

### Compatibility

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

### Games

| Game name | Compatibility | Notes |
| :--- | :--- | :--- |
| Bodycount | ✅ | - |
| Menacer - 6-game Cartridge | ✅ | - |
| T2 - The Arcade Game | ✅ | - |

## Mega-CD

### Emulator

The required emulator is `Libretro GenesisPlusGX`.

### Compatibility

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

### Games

| Game name | Compatibility | Notes |
| :--- | :--- | :--- |
| Corpse Killer | ✅ | - |
| Crime Patrol | ✅ | - |
| Ground Zero Texas | ❌ | - |
| Lethal Enforcers | ✅ | - |
| Lethal Enforcers II: Gun Fighters | ✅ | - |
| Mad Dog McCree | ✅ | - |
| Mad Dog II: The Lost Gold | ✅ | - |
| Snatcher | ❔ | - |
| Who Shot Johnny Rock ? | ✅ | - |

## Master System

### Emulator

The required emulator is `Libretro GenesisPlusGX`.

### Compatibility

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

### Games

| Game name | Compatibility | Notes |
| :--- | :--- | :--- |
| Assault City (USA, Europe, Brazil) (Light Phaser) | ✅ | - |
| Bank Panic | ❌ | - |
| Gangster Town | ✅ | - |
| Laser Ghost | ✅ | You need to click on 1 or 2 to start in Light Phaser mode from sega game pad in control 2 |
| Marksman Shooting & Trap Shooting | ✅ | - |
| Marksman Shooting & Trap Shooting & Safari Hunt | ✅ | - |
| Missile Defense 3-D | ✅ | - |
| Operation Wolf | ✅ | - |
| Rambo III | ✅ | - |
| Rescue Mission | ✅ | - |
| Shooting Gallery ~ Shooting G. | ✅ | - |
| Space Gun | ✅ | - |
| Wanted | ❔ | - |

## Dreamcast

### Emulator

The required emulator is `Libretro Flycast`.

### Compatibility

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ❌ | ❌ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

### Games

| Game name | Compatibility | Notes |
| :--- | :--- | :--- |
| Confidential Mission | ✅ | - |
| Death Crimson 2 | ✅ | - |
| Death Crimson OX | ✅ | - |
| House of the Dead 2 | ✅ | - |
| Virtua Cop 2 | ✅ | - |

## Playstation 1

### Emulator

The required emulator is `Libretro Mednafen_PSX`.

### Compatibility

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ❌ | ❌ | ❌ | ✅ | ❌ | ✅ | ✅ | ✅ |

### Games

| Game name | Compatibility | Notes |
| :--- | :--- | :--- |
| Area 51 | ✅ | - |
| Crypt Killer | ✅ | - |
| Die Hard Trilogy | ✅ | - |
| Die Hard Trilogy 2 | ✅ | - |
| Elemental Gearbolt | ❔ | - |
| Extreme Ghostbusters: The Ultimate Invasion | ✅ | In this game, need to press - and + on wiimote to start/pause game (corresponding to Gcon45 A & B Buttons) |
| Game Paradise 2 | ❔ | - |
| Ghoul Panic | ✅ | - |
| Gunfighter: The Legend of Jesse James | ✅ | - |
| Gunfighter II: Revenge of Jesse James | ✅ | - |
| Guntu: Western Front June, 1944: Tetsu no Kioku | ❔ | - |
| Judge Dredd | ✅ | - |
| Lethal Enforcers | ✅ | - |
| Maximum Force | ✅ | - |
| Mighty Hits Special | ✅ | - |
| Moorhen 3: Chicken Chase | ✅ | - |
| Moorhuhn 2: Die Jagd Geht Weiter | ❔ | - |
| Moorhuhn 3 | ✅ | - |
| Moorhuhn X | ❔ | - |
| Omega Assault | ❔ | - |
| Point Blank | ✅ | - |
| Point Blank 2 | ✅ | - |
| Point Blank 3 | ✅ | - |
| Project: Horned Owl | ❔ | - |
| Puffy no P.S. I Love You | ❔ |
| Rescue Shot | ✅ | - |
| Rescue Shot Bubibo | ✅ | - |
| Rescue Shot Bubibo & Biohazard: Gun Survivor: GunCon Taiou Shooting Taikenban | ✅ | - |
| Resident Evil: Survivor | ✅ | - |
| Revolution X | ❔ | - |
| Serofans | ❔ | - |
| Simple 1500 Series Vol. 24: The Gun Shooting | ❔ | - |
| Simple 1500 Series Vol. 63: The Gun Shooting 2 | ❔ | - |
| Time Crisis | ✅ | - |
| Time Crisis: Project Titan | ✅ | - |

## Saturn

### Emulator

The required emulator is `Libretro Mednafen_Saturn`.

### Compatibility

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ❌ | ❌ | ❌ | ❌ | ❌ | ❌ | ✅ | ✅ |

### Games

| Game name | Compatibility | Notes |
| :--- | :--- | :--- |
| Area 51 | ✅ | - |
| Chaos Control | ✅ | - |
| Corpse Killer: Graveyard Edition | ❌ | - |
| Crypt Killer | ✅ | - |
| Death Crimson | ✅ | - |
| Die Hard Trilogy | ✅ | - |
| House of the Dead | ✅ | - |
| Maximum Force | ✅ | - |
| Mechanical Violator Hakaider: Last Judgement | ✅ | - |
| Mighty Hits | ✅ | - |
| Policenauts | ❔ | - |
| Revolution X | ❌ | - |
| Scud: The Disposable Assassin | ✅ | - |
| Virtua Cop | ✅ | - |
| Virtua Cop 2 | ✅ | - |

## 3DO

### Emulator

The required emulator is `Libretro Opera`.

### Compatibility

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ❌ | ❌ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

### Games

| Game name | Compatibility | Notes |
| :--- | :--- | :--- |
| Burning Soldier | ❌ | - |
| Corpse Killer | ✅ | - |
| Creature Shock | ❌ | - |
| Crime Patrol | ✅ | - |
| CyberDillo | ❌ | - |
| Demolition Man | ❔ | - |
| Drug Wars | ✅ | - |
| Killingtime | ❌ | - |
| Mad Dog McCree | ✅ | - |
| Mad Dog II: The Lost Gold | ✅ | - |
| Shootout at Old Tucson | ❔ | - |
| Space Pirates | ✅ | - |
| The Last Bounty Hunter | ✅ | - |
| Who Shot Johnny Rock ? | ✅ | - |

## Atomiswave

### Emulator

The required emulator is `Libretro Flycast`.

### Compatibility

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ❌ | ❌ | ❌ | ✅ | ✅ | ✅ | ✅ | ✅ |

### Games

| Game name | Compatibility | Notes |
| :--- | :--- | :--- |
| Extreme Hunting | ✅ | - |
| Extreme Hunting 2 | ❌ | - |
| Ranger Mission | ✅ | - |
| Sega Clay Challenge | ✅ | - |
| Sports Shooting USA | ✅ | - |

## Naomi

### Emulator

The required emulator is `Libretro Flycast`.

### Compatibility

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ❌ | ❌ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

### Games

| Game name | Compatibility | Notes |
| :--- | :--- | :--- |
| Death Crimson OX | ✅ | - |
| House of the Dead 2 | ✅ | - |
| Gun Survivor 2: Biohazard Code:Veronica | ❌ | - |
| Mazan: Flash of the Blade | ✅ | - |
| Ninja Assault | ✅ | - |

## Naomi GD

### Emulator

The required emulator is `Libretro Flycast`.

### Compatibility

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ❌ | ❌ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

### Games

| Game name | Compatibility | Notes |
| :--- | :--- | :--- |
| Confidential Mission | ✅ | - |
| Lupin the 3rd: The Shooting | ✅ | - |
| Maze of the King | ✅ | - |

## Mame

### Emulator

The required emulator is `Libretro MAME`.

### Compatibility

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ❌ | ❌ | ❌ | ❌ | ❌ | ❌ | ✅ | ✅ |

### Games

| Game name | Compatibility | Notes |
| :--- | :--- | :--- |
| Alien3: The Gun | ❌ | - |
| Area 51 (R3000) | ✅ | - |
| Area 51 / Maximum Force Duo v2.0 | ✅ | - |
| Bang! | ✅ | - |
| Bank Panic | ❌ | - |
| Beast Busters | ✅ | - |
| Beast Busters 2nd Nightmare | ✅ | - |
| Born to Fight | ✅ | - |
| Bubble Trouble | ✅ | - |
| CarnEvil | ❌ | - |
| Catch-22 | ❌ | - |
| Cheyenne | ✅ | - |
| Chiller | ✅ | - |
| Clay Pigeon | ✅ | - |
| Claybuster | ✅ | - |
| Combat Hawk | ✅ | - |
| Combat School | ✅ | - |
| Crackshot | ✅ | - |
| Critter Crusher | ✅ | - |
| Crossbow | ✅ | - |
| Crypt Killer | ✅ | - |
| Cycle Shooting | ✅ | - |
| Deer Hunting USA | ✅ | - |
| Desert Gun | ✅ | - |
| Dragon Gun | ✅ | - |
| Egg Venture | ✅ | - |
| Freedom Force | ❔ | - |
| Ghost Hunter | ✅ | - |
| Great Guns | ✅ | - |
| Golgo 13 | ✅ | - |
| Golgo 13 Kiseki No Dandou | ✅ | - |
| Golly! Ghost! | ✅ | - |
| Gun Bullet | ❔ | - |
| Gunbuster | ✅ | - |
| Hit'n Miss | ✅ | - |
| Invasion - The Abductors | ✅ | - |
| Jurassic Park | ❌ | - |
| Laser Ghost | ❌ | - |
| Lethal Enforcers | ✅ | - |
| Lethal Enforcers II: Gun Fighters | ✅ | - |
| Lethal Justice | ✅ | - |
| Line of Fire / Bakudan Yarou | ❔ | - |
| Lord of Gun | ❔ | - |
| Mallet Madnes | ✅ | - |
| Maximum Force | ✅ | - |
| Mechanized Attack | ❔ | - |
| Mobile Suit Gundam Final Shooting | ❔ | - |
| Night Stocker | ❔ | - |
| N.Y. Captor | ✅ | - |
| Oh! Bakyuuun | ✅ | - |
| One Shot One Kill | ❌ | - |
| Operation Thunderbolt | ❌ | - |
| Operation Wolf | ✅ | - |
| Point Blank | ✅ | - |
| Point Blank 2 | ✅ | - |
| Point Blank 3 | ✅ | - |
| Police Trainer | ✅ | - |
| Rail Chase | ✅ | - |
| Rapid Fire | ✅ | - |
| Revolution X | ❌ | - |
| Sharpshooter | ✅ | - |
| Shooting Gallery | ❌ | - |
| Space Gun | ❌ | - |
| Steel Gunner | ✅ | - |
| Steel Gunner 2 | ✅ | - |
| Target Hits | ✅ | - |
| Terminator 2 - Judgment Day | ❌ | - |
| Tickee Tickats | ✅ | - |
| Time Crisis | ✅ | - |
| Trophy Hunting - Bear & Moose | ✅ | - |
| Tut's Tomb | ✅ | - |
| Under Fire | ✅ | - |
| Virtua Cop 2 | ✅ | - |
| Vs. Duck Hunt | ✅ | - |
| Vs. Freedom Force | ✅ | - |
| Vs. Hogan's Alley | ✅ | - |
| Who Dunit | ✅ | - |
| Wild Gunman | ❔ | - |
| Wild Pilot | ❌ | - |
| Wing Shooting Championship | ✅ | - |
| Zero Point | ✅ | - |
| Zero Point 2 | ✅ | - |
| Zombie Raid | ✅ | - |

## Mame 2003-Plus

### Emulator

The required emulator is `Libretro MAME 2003 Plus`.

### Compatibility

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

### Games

| Game name | Compatibility | Notes |
| :--- | :--- | :--- |
| Alien 3: The Gun | ✅ | - |
| Area 51 (R3000) | ❌ | - |
| Area 51 / Maximum Force Duo v2.0 | ❌ | - |
| Bang! | ✅ | - |
| Beast Busters | ✅ | - |
| Born To Fight | ✅ | - |
| Bubble Trouble - Golly Ghost 2 | ❌ | - |
| Cheyenne | ✅ | - |
| Chiller | ✅ | - |
| Clay Pigeon | ✅ | - |
| Crossbow | ✅ | - |
| Crypt Killer | ✅ | - |
| Deer Hunting USA | ✅ | - |
| Dragon Gun | ✅ | - |
| Egg Venture | ✅ | - |
| Ghost Hunter | ✅ | - |
| Golgo 13 | ✅ | - |
| Golgo 13 Kiseki no Dandou | ✅ | - |
| Golly! Ghost! | ✅ | - |
| Great Guns | ✅ | - |
| Gunbuster | ✅ | - |
| Hit'n Miss | ✅ | - |
| Invasion - The Abductors | ❌ | - |
| Jurassic Park | ✅ | - |
| Lethal Enforcers | ✅ | - |
| Lethal Enforcers II: Gun Fighters | ✅ | - |
| Lethal Justice | ✅ | - |
| Mallet Madnes | ❔ | - |
| Maximum Force v1.05 | ✅ | - |
| N.Y. Captor | ✅ | - |
| Oh! Bakyuuun | ❌ | - |
| One Shot One Kill | ✅ | - |
| Operation Wolf | ✅ | - |
| Operation Wolf 3 | ✅ | - |
| Point Blank | ✅ | - |
| Rail Chase | ✅ | - |
| Rapid Fire v1.1 | ✅ | - |
| Steel Gunner | ✅ | - |
| Target Hits | ✅ | - |
| Tickee Tickats | ✅ | - |
| Time Crisis | ✅ | - |
| Trophy Hunting - Bear & Moose v1.0 | ✅ | - |
| Tut's Tomb | ✅ | - |
| Under Fire | ✅ | - |
| Virtua Cop 2 | ❌ | - |
| Vs. Duck Hunt | ✅ | - |
| Vs. Freedom | ✅ | - |
| Vs. Hogan's Alley | ✅ | - |
| Who Dunit | ✅ | - |
| Wild Pilot | ✅ | - |
| Wing Shooting Championship v2.00 | ✅ | - |
| Zero Point | ✅ | - |
| Zero Point 2 | ✅ | - |
| Zombie Raid | ✅ | - |

## FinalBurn Neo

### Emulator

The required emulator is `Libretro FBNeo`.

### Compatibility

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

### Games

| Game name | Compatibility | Notes |
| :--- | :--- | :--- |
| Alien3: The Gun | ❌ | - |
| Area 51 (R-3000) | ❌ | - |
| Area 51 / Maximum Force Duo v2.0 | ❌ | - |
| Bang! | ✅ | - |
| Bank Panic | ❌ | - |
| Beast Busters | ✅ | - |
| Born to Fight | ✅ | - |
| Bubble Trouble | ❔ | - |
| Cheyenne | ✅ | - |
| Chiller | ✅ | - |
| Clay Pigeon | ✅ | - |
| Crossbow | ✅ | - |
| Crypt Killer | ✅ | - |
| Deer Hunting USA | ✅ | - |
| Dragon Gun | ✅ | - |
| Egg Venture | ✅ | - |
| Ghost Hunter | ✅ | - |
| Golgo 13 | ✅ | - |
| Golgo 13 Kiseki no Dandou | ✅ | - |
| Golly! Ghost! | ✅ | - |
| Great Guns | ✅ | - |
| Gunbuster | ✅ | - |
| Hit'n Miss | ✅ | - |
| Invasion - The Abductors | ❌ | - |
| Jurassic Park | ✅ | - |
| Lethal Enforcers II: Gun Fighters | ✅ | - |
| Lethal Justice | ✅ | - |
| Line of Fire / Bakudan Yarou | ✅ | - |
| Lord of Gun | ❌ | - |
| Mechanized Attack | ✅ | - |
| Mobile Suit Gundam Final Shooting | ❌ | - |
| Operation Thunderbolt | ✅ | - |
| Operation Wolf | ✅ | - |
| Operation Wolf 3 | ✅ | - |
| Point Blank | ✅ | - |
| Rail Chase | ✅ | - |
| Space Gun | ✅ | - |
| Steel Gunner | ✅ | - |
| Steel Gunner 2 | ✅ | - |
| Trophy Hunting - Bear & Moose v1.0 | ✅ | - |
| Turkey Hunting USA v1.0 | ✅ | - |
| Wing Shooting Championship v2.00 | ✅ | - |
| Zero Point | ✅ | - |
| Zero Point 2 | ✅ | - |
| Zombie Raid | ✅ | - |