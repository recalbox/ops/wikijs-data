---
title: Libretro Arduous
description: 
published: true
date: 2024-06-23T00:10:26.028Z
tags: libretro, 9.1+, arduous
editor: markdown
dateCreated: 2023-06-17T22:27:46.074Z
---

**Libretro Arduous** est un port de l'**Arduous**.

## ![](/emulators/license.svg) Licence

Ce core est sous licence [**GPLv3**](https://github.com/libretro/arduous/blob/main/COPYING).

## ![](/emulators/compatibility.png) Compatibilité

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Fonctionnalités

| Fonctionnalité | Supporté |
| :---: | :---: |

## ![](/emulators/bios.svg) BIOS

>**Aucun bios n'est requis.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Extensions supportées

Les roms doivent avoir l'extension :

* .hex

### Emplacement

Placez les roms comme ceci : 

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 arduboy
┃ ┃ ┃ ┃ ┣ 🗒  **jeu.hex**

## ![](/emulators/advanced-configuration.png) Configuration avancée de l'émulateur

>**Ce core n'a pas d'option.**
{.is-success}

## ![](/emulators/external-links.png) Liens externes

* **Code source utilisé** : [https://github.com/libretro/arduous/](https://github.com/libretro/arduous/)