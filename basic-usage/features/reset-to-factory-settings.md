---
title: Reset factory
description: 
published: true
date: 2024-11-11T05:29:36.792Z
tags: reset, factory, settings, 7.2+
editor: markdown
dateCreated: 2021-06-29T09:28:19.304Z
---

## Overview

A new option named `Reset to factory settings` that allows to reset Recalbox to the same state after doing a brand new clean install. This is very helpful to get a new installation without doing it manually. 


>This option **does not touch** user files likes bios, roms and game saves!
{.is-success}

## How to use

This option is located in EmulationStation. To do this, just press `START`, go to `ADVANCED SETTINGS` and select `RESET TO FACTORY SETTINGS`.

![](/basic-usage/features/resetfactory1-en.png){.full-width}

Once the option chosen, two warning windows are displayed.

![](/basic-usage/features/resetfactory2-en.png){.full-width}

![](/basic-usage/features/resetfactory3-en.png){.full-width}

>Once these two windows passed, **there is no way to go back**.
{.is-danger}

When the reset factory workflow is done, the Recalbox install is like new !