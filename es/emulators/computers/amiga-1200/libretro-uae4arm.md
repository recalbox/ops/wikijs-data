---
title: Libretro UAE4ARM
description: 
published: true
date: 2023-09-11T07:23:35.585Z
tags: libretro, amiga-1200, amiga, 1200, uae4arm, 8.0+
editor: markdown
dateCreated: 2023-09-11T07:23:35.585Z
---



## ![](/emulators/license.svg) Licencia

El core posee la siguiente licencia [**GPLv2**](https://github.com/libretro/PUAE/blob/master/COPYING).

## ![](/emulators/compatibility.png) Compatibilidad

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ❌ | ❌ | ✅ |

## ![](/emulators/features.png) Funcionalidades



## ![](/emulators/bios.svg) BIOS

### Lista de BIOS obligatorias

Encontraréis los ficheros de bios con el nombre indicado en la columna **Descripción**. Tendréis que renombrarlos para que tengan el nombre indicado en la columna **Nombre del fichero**.

| Nombre del fichero | Descripción | MD5 | ¿Preinstalada? |
| :---: | :---: | :---: | :---: |
| kick40068.A1200 | Kickstart v3.1 r40.068 (1993-12)(Commodore)(A1200)\[!\].rom | 646773759326fbac3b2311fd8c8793ee | ❌ |

### Emplazamiento

Disponed vuestras bios como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 🗒 **kick40068.A1200**

## ![](/emulators/roms.png) Roms

### Extensiones de roms soportadas

Las roms deben tener las extensiones siguientes:

* .adf
* .adz
* .ipf
* .lha
* .lhz
* .lzx
* .zip
* .rp9
* .dms
* .fdi
* .hdf
* .hdz
* .m3u

### Emplazamiento

Disponed vuestras roms como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 amiga1200
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.ipf**

>Se aconsejan las roms del proyecto **TOSEC**.
{.is-success}

>Para más información sobre las roms, consultad [este tutorial](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

>Para conservar las configuraciones personalizadas cuando se produce una actualización del sistema Recalbox, os aconsejamos utilizar las [Sobrecargas de configuración](./../../../advanced-usage/configuration-override).
{.is-info}

### Acceder a las opciones

Podéis configurar las opciones del core de dos formas diferentes.

* Via el Menú RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via el fichero `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Opciones del core

| Opción | Valor de la opción | Variable (para la sobrecarga) | Valores posibles |
| --- | --- | --- | --- |
| Model | `Auto` ✅ / `A500` / `A600` / `A1200` / `CDTV` / `CD32` | `uae4arm_model` | `Auto` / `A500` / `A600` / `A1200` / `CDTV` / `CD32` |
| Fast Mem | `None` ✅ / `1 MB` / `2 MB` / `4 MB` / `8 MB` | `uae4arm_fastmem` | `None` / `1 MB` / `2 MB` / `4 MB` / `8 MB` |
| Internal resolution | `640x270` ✅ / `320x240` / `320x256` / `320x262` / `640x240` / `640x256` / `640x262` / `640x270` / `768x270` | `uae4arm_resolution` | `640x270` / `320x240` / `320x256` / `320x262` / `640x240` / `640x256` / `640x262` / `640x270` / `768x270` |
| Leds on screen | `Activé` ✅ / `Désactivé` | `uae4arm_leds_on_screen` | `enabled` / `disabled` |
| Floppy speed | `100` ✅ / `200` / `400` / `800` | `uae4arm_floppy_speed` | `100` / `200` / `400` / `800` |
| Line doubling (de-interlace) | `Désactivé` ✅ / `Activé` | `uae4arm_linedoubling` | `disabled` / `enabled` |
| whdload mode | `files` ✅ / `hdfs` | `uae4arm_whdloadmode` | `files` / `hdfs` |
| fast copper | `Désactivé` ✅ / `Activé` | `uae4arm_fastcopper` | `disabled` / `enabled` |

## ![](/emulators/external-links.png) Enlaces externos

* **Código fuente utilizado** : [https://github.com/Chips-fr/uae4arm-rpi/](https://github.com/Chips-fr/uae4arm-rpi/)
* **Documentación Libretro** : -