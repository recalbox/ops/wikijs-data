---
title: WASM-4
description: 
published: true
date: 2024-07-26T08:23:17.004Z
tags: fantasy, 9.1+, wasm4, consolas
editor: markdown
dateCreated: 2023-09-04T07:05:20.012Z
---

![](/emulators/fantasy/wasm4.svg){.align-center}

## Detalles técnicos

* **Desarrollador**: Bruno Garcia
* **Fecha de lanzamiento**: 2021
* **Pantalla**: 160x160 píxeles, 4 colores personalizables a 60 Hz
* **Memoria**: 64 KB de RAM lineal
* **Límite de tamaño del cartucho**: 64 KB
* **Entradas**: teclado, ratón, pantalla táctil, hasta 4 joysticks
* **Audio**: 2 canales de onda de pulso, 1 canal de onda triangular, 1 canal de ruido
* **Almacenamiento**: 1024 bytes

## Presentación

WebAssembly, abreviado wasm, es un estándar de la World Wide Web para el desarrollo de aplicaciones. Está diseñado para sustituir a JavaScript con un rendimiento superior. El estándar consta de bytecode, su representación textual y un entorno de ejecución en un sandbox compatible con JavaScript. Puede ejecutarse dentro y fuera de un navegador web. WebAssembly está normalizado por el World Wide Web Consortium.

Dado que WebAssembly sólo especifica un lenguaje de bajo nivel, el código de bytes suele generarse compilando un lenguaje de nivel superior. Entre los primeros lenguajes compatibles se encuentran Rust con el proyecto/módulo wasm-bindgen (crate), así como C y C++, compilados con Emscripten (basado en LLVM). Muchos otros lenguajes de programación disponen ahora de compiladores WebAssembly, como C#, Go, Java, Lua, Python, Ruby, Fortran y Pascal.

Los navegadores web compilan el bytecode wasm en el lenguaje máquina del host en el que se utilizan antes de ejecutarlo.

## Emuladores

[Libretro Wasm4](libretro-wasm4)