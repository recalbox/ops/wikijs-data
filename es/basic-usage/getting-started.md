---
title: Primeros pasos
description: 
published: true
date: 2023-09-03T13:09:57.334Z
tags: primeros pasos
editor: markdown
dateCreated: 2023-09-03T13:09:57.334Z
---

Si eres un nuevo usuario o un usuario principiante, que utiliza Recalbox por primera vez, podréis encontrar en las siguientes páginas toda la información básica necesaria.