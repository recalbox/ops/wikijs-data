---
title: 🕹 Arcade
description: 
published: true
date: 2023-09-01T09:39:47.818Z
tags: arcade, tutorial, recreativas, maquinas
editor: markdown
dateCreated: 2023-09-01T09:39:47.818Z
---

Aquí encontrarás tutoriales relacionados con la arcade, como el hardware utilizado, las conexiones necesarias, etc.

En esta serie de tutoriales, presentaremos las diferentes formas de interactuar, desde un punto de vista de hardware, con RecalBox a través de las conexiones expuestas por la Raspberry Pi.
Nos centraremos principalmente en aquellos que dispongan de un GPIO.
El objetivo es divertirse mientras se construye una máquina arcade para:

- Controlar el volumen a través de un botón.
- Exponer un botón de encendido/apagado.
- Exponer botones para acceder a atajos de las funciones de RecalBox (captura de pantalla, guardar, ...)

Estos tutoriales no están dirigidos a expertos en electrónica y/o desarrollo. Intentaremos ser lo más didácticos posible y explicar cada paso que tomemos en nuestras construcciones de manera clara. Los componentes electrónicos utilizados en estos tutoriales se describirán en el próximo capítulo.