---
title: Recalbox RGB JAMMA - Manual 
description: Recalbox RGB JAMMA Manual y F.A.Q.
published: true
date: 2024-05-19T14:45:08.414Z
tags: rgb, jamma
editor: markdown
dateCreated: 2024-02-04T18:04:04.102Z
---



> English version available [here](../../en/hardware-compatibility/recalbox-rgb-jamma-new).
{.is-info}


# Presentación

Recalbox RGB JAMMA: ¡disfrutad de todas las funciones de Recalbox en vuestro terminal JAMMA!
![rgb_jamma_v5_main_01.jpg](/compatibility/jamma/rgb_jamma_v5_main_01.jpg)

Antes de empezar, ¿qué es JAMMA?

Introducido por primera vez en los años 80, JAMMA estandarizó la conexión de diferentes señales en las máquinas recreativas de la época. De la noche a la mañana, cambiar de juego en una máquina recreativa se convirtió en un juego de niños.

Para más información, echad un vistazo al vídeo de la asociación HFS, que explica la historia y el funcionamiento de jamma:

[![jamma_cqlb.jpeg](/compatibility/jamma/jamma_cqlb.jpeg)](https://www.youtube.com/watch?v=jOBZ0h9bA0Y)

Este formato ha sobrevivido hasta nuestros días, y aún se utiliza en un gran número de máquinas recreativas de tubos catódicos.

Si no tenéis una máquina compatible con JAMMA, o si no tenéis pensado comprar una en un futuro próximo, el Recalbox RGB JAMMA no es para vosotros...

Sin embargo, su hermano mayor, Recalbox RGB DUAL, os permite experimentar las emociones de los juegos de antaño en vuestra televisión CRT, es compatible con la Rasbperry Pi 4/3/Zero2 y está disponible en nuestra tienda online.

# Preparación

El esquema de los pines del Recalbox RGB Jamma es compatible con los estándares JAMMA, JAMMA+ y CHAMMA.

![jamma_pinout_(4).png](/compatibility/jamma/jamma_pinout_(4).png)

_Los botones se identifican según el standar JAMMA:_

![jamma-layout.png](/compatibility/jamma/jamma-layout.png)

Los botones TEST y SERVICE se encuentran dentro de vuetra máquina, a menudo directamente sobre la fuente de alimentación.

## Instalación dentro de la máquina de arcade

### Ensamblaje

#### Pi + Recalbox RGB JAMMA

Enganchad el Raspberry Pi en la Recalbox RGB JAMMA, y enganchad los separadores de plástico suministrados en las dos placas de circuito impreso.

![assembly1.jpg](/compatibility/jamma/assembly1.jpg){.decorate}

![assembly2.jpg](/compatibility/jamma/assembly2.jpg){.decorate}

#### Ventilador

El ventilador Recalbox RGB JAMMA se utiliza para enfriar la Raspberry Pi. Ha sido testeado para enfriar la Raspberry de la forma más eficiente posible, incluso cuando se utiliza la carcasa oficial. **Este ventilador reemplaza el ventilador situado en vuestra Rasberry Pi.**

Utilizad el Pin +5V y GND para obtener la máxima potencia, o +3V y GND para limitar el ruido del ventilador.

![fan_(5).png](/compatibility/jamma/fan_(5).png){.center}

Ventiladores compatibles :
- Talla de 40x40x20mm o de 40x40x10mm
- 5V
- 1W max (0.3W suffisent)
- Tornillos y tuercas M3

> Si instaláis vuestro propio ventilador, colocadlo de forma que impulse el aire hacia abajo.
> 
{.is-info}

> No hay que añadir un ventilador directamente en el Rasbperry, no es necesario, la ventilación de la Recalbox RGB JAMMA enfría todo el sistema directamente.
{.is-warning}


#### Carcasa (opcional)

Depués de haber enganchado el Raspberry PI encima del Recalbox RGB JAMMA, sólo os queda colocar el conjunto dentro de la carcasa y basta con cerrarla usando los tornillos M2.5x15mm propocinados con la misma.

![case-assembly1.jpg](/compatibility/jamma/case-assembly1.jpg){.decorate}

![case-assembly1-1.jpg](/compatibility/jamma/case-assembly1-1.jpg){.decorate}

## Tensión de alimentación

El voltaje de la fuente de alimentación del Recalbox RGB JAMMA + Raspberry Pi 5 debe estar entre 5.05V y 5.20V.

Si vuestra máquina ya está configurada para ser compatible con la mayoría de PCBs / Multi Sistemas, no hay problema, podéis hacer funcionar Recalbox RGB JAMMA tal cual.

Una ventana emergente os advertirá durante la ejecución si el voltaje se vuelve demasiado bajo cuando el Raspberry Pi se utiliza a tope.

Si aparece dicha ventana emergente, necesitáis aumentar el voltaje en unos milivoltios (entre 5,05V y 5,2V, **recomendamos 5,1V**).

Para ello, utiliza un potenciómetro situado en la fuente de alimentación. 
Aseguráos de obtener la máxima información posible sobre vuestra fuente de alimentación, equiparos correctamente (destornillador de plástico o de cerámica), y pedid ajuda externa si sois principiantes y no estáis seguros de cómo proceder.

## Instalación de la imagen

Descargad la imagen de Recalbox que se encuentra disponible en la sección Jamma de [recalbox.com/recalbox-rgb-jamma](https://www.recalbox.com/fr/recalbox-rgb-jamma/).

Sin descomprimir la imagen, grabadla en vuestra tarjeta SD usando [logiciel Raspberry Pi Imager](https://www.raspberrypi.com/software/), usando como origen la imagen descargada y como destino el lector de tarjetas SD de vuestro ordenador (consultad [cette vidéo](https://www.recalbox.com/static/newrpiimager-image.mp4-processed-ffa17b13a7bbc9a682f419b3c9d9a451.mp4)).

Una vez que la imagen se ha escrito correctamente en vuestra tarjeta SD, metedla dentro de vuestro Raspberry Pi, conectad vuestra Recabox RGB JAMMA a la toma JAMMA y... ¡encended vuestra máquina recreativa!

La instalación y detección de la Recalbox RGB JAMMA se iniciará automáticamente.

# VIDEO

## Pixel Perfect 15Khz

Todos los juegos, ya sean de arcade o de consola, se lanzan en su resolución y frecuencia originales. Algunos juegos tienen resoluciones "exóticas", como Mortal Kombat (`400x254@54.815170Hz`) o R-Type (`384x256@55.017606Hz`).
Los modos de vídeo generados por la Recalbox RGB JAMMA se acercan lo más posible a estas resoluciones originales, por lo que algunos juegos pueden requerir el ajuste de vuestro monitor arcade.

### Super Resolución

Para producir una señal de vídeo de la mejor calidad posible, a veces es necesario utilizar superresoluciones, que multiplican la resolución horizontal por un número entero. La pantalla Recalbox RGB JAMMA muestra el factor de multiplicación horizontal cuando se activa una súper resolución.

![metal-slug-screen.png](/compatibility/jamma/metal-slug-screen.png)


## 31kHz support / JVS / Naomi

Si tenéis una pantalla de 31kHz (un NUC por ejemplo), activad el modo 31kHz en el menú principal para cambiar Recalbox a 31kHz.

No obstante, Recalbox se lanza por defecto a 15kHz. Existen dos posibilidades:

* Enchufad el Recalbox RGB JAMMA en una máquina recreativa de 15kHz una primera vez para cambiar la frecuencia usando el menú de EmulationStation:

![option-31khz.png](/compatibility/jamma/option-31khz.png){.decorate}

* Añadid la línea `options.screen.31kHz = 1` dentro del fichero `RECALBOX/crt/recalbox-crt-options.cfg`.


Cuando lanzáis un juego de 15kHz en 480p, las líneas del juego simplemente se duplican. Podéis activar los "scanlines" para simular las líneas de barrido originales de una pantalla de 240p. Disponéis de 3 opciones para activar "scanlines" más o menos pronunciadas:

![scanlines.png](/compatibility/jamma/scanlines.png)

##  Soporte Multisync

El modo Multisync es adecuado para las consolas que detectan la frecuencia de la señal de vídeo y que son capaces de cambiar automáticamente de modo en consecuencia. Los juegos de 240p se lanzan a 15kHz y los de 480p a 31kHz. ¡De esta forma podréis tener TODOS vuestros juegos en sus resoluciones originales!

El multisync se activa directamente desde el menú Recalbox RGB JAMMA:
![multisync.png](/compatibility/jamma/multisync.png)

Una vez seleccionado el modo Multisync, podéis elegir la resolución de los menús, en función de vuestas preferencias, y limitar la conmutación de vuestro terminal:

- eligid 240p si soléis jugar a juegos de 15kHz en lugar de a juegos de 31kHz.
- eligid 480p si soléis jugar a juegos de 31kHz en lugar de a juegos de 15kHz.

![menu-resolution.png](/compatibility/jamma/menu-resolution.png)
> Comprobad que vuestro reproductor es compatible con el modo MultiSync antes de activarlo.
{.is-warning}

Por defecto, los juegos de Atomiswave, Naomi y Dreamcast se lanzarán en 480p. Pero algunos de los juegos de la época fueron reescalados en estas plataformas, y puede que algunos de vosotros queráis descubrirlos en 240p :) Para ello, basta con activar la opción de selección de resolución en el menú y elegir la resolución de cada juego de forma individual antes de lanzarlo.

![select-resolution.png](/compatibility/jamma/select-resolution.png)

Cada vez que lanzaréis el juego, se os preguntará para seleccionar la resolución que quéreis utilizar.
![select-game-rez.png](/compatibility/jamma/select-game-rez.png)

### Modo TATE (vertical)

Para activar el modo TATE, nada mas sencillo, todo se encuetra dentro del menú TATE (START -> TATE).
![tate-settings.png](/compatibility/jamma/tate-settings.png)

Seleccionad el sentido de la rotación (DERECHA = 90° en el sentido de las ajugas de un reloj). Una vez seleccionada la rotación, EmulationStation se recarga y la pantalla se gira!
![tate-rotation.png](/compatibility/jamma/tate-rotation.png)

Es posible regrupar los juegos que utilizan el modo TATE mediante la opción "DISPLAY ONLY TATE GAMES IN GAMELIST"

> Pensad a re-scrapear vuestros juegos para que Recalbox detecte correctamente el modo TATE !
{.is-success}

Si vuestro theme es compatible con el modo TATE, se adaptara automáticamente a las nuevas dimensiones de vuestra pantalla!

![tate-theme.png](/compatibility/jamma/tate-theme.png){.decorate}


# CONTROLES

## Jamma + Jamma+ + Chamma

Cada terminal tiene su propia configuración. La Recalbox RGB JAMMA soporta automáticamente diferentes combinaciones de botones en el jamma o kick harness.
Los KickHarness CPS1 y CPS2 son intercambiables sin necesidad de configuración.

* Configuración JAMMA y Kick Harness

| Botones del JAMMA | Botones del kick harness |
|----------------------|--------------------------|
| 1 2                  |                          |
| 1 2 3                |                          |
| 1 2 3                | 4                        |
| 1 2 3                | 4 5                      |
| 1 2 3                | 4 5 6                    |
| 1 2 3 4              | 5 6                      |

* Configuración JAMMA+ y Kick Harness

| Botones del JAMMA | Botones del kick harness |
|----------------------|--------------------------|
| 1 2 3 4 5            | 6                        |

* Configuración CHAMMA y Kick Harness

| Botones del JAMMA | Botones del kick harness |
|----------------------|--------------------------|
| 1 2 3 4 5 6          |                          |

### Tierra en el pin 27/E

Si tenéis la masa para J1 y J2 en los pines 27/E del JAMMA, tendréis que activar la masa común en la configuración.
![27asground.png](/compatibility/jamma/27asground.png)

## CPS1 and CPS2 Kick Harness

![cps1and2.png](/compatibility/jamma/cps1and2.png){.center}

## Input Lag

El input lag del Recalbox RGB JAMMA es extremadamente bajo, menos de un milisegundo.

Sin embargo, la latencia global de los juegos y de los diferentes sistemas se ve afectada por otros elementos además del input lag.

Hay dos opciones disponibles en Recalbox RGB JAMMA para reducir la latencia tanto como sea posible:

![reducelagoptions.png](/compatibility/jamma/reducelagoptions.png){.decorate}

* `Reduce latency` activará numerosas opciones de retroarch y otros emuladores para reducir la latencia (retardo automático de frames, sondeo tardío, etc.).
* `run ahead` permite precalcular frames para ahorrar un frame de latencia.

Estas dos opciones activadas juntas, combinadas con el bajísimo input lag de los componentes Recalbox RGB JAMMA, reducen drásticamente la latencia y se acercan mucho a la latencia del mister o de los juegos originales.

Sin embargo, estas opciones podrían tener un impacto negativo en un puñado de juegos, así que tenedlo presente si notáis que ciertos juegos se ralentizan.

Encontraréis más información en [nuestro informe completo sobre el input lag en Recalbox RGB JAMMA](https://www.recalbox.com/fr/blog/2023-07-19-input-lag-and-latency/).


## NeoGeo layout selection

Podéis configurar el mapeado que se aplicará automáticamente al arrancar un juego NeoGeo.

Si tenéis una NeoGeo de 4 botones, introducid el número de botones en las opciones del menú Recalbox RGB JAMMA y utilizad el mapeado estándar de NeoGeo:

![panel_4btn.png](/compatibility/jamma/panel_4btn.png){.decorate}

![4buttons_(1).png](/compatibility/jamma/4buttons_(1).png)

Para paneles de 6 botones, podéis elegir entre dos mapeados. Line, que utiliza el mapeado de líneas de los paneles NeoGeo, y Square, que utiliza el mapeado de los controladores Neogeo (ajustable en el menú Recalbox RGB JAMMA).

![neogeolayout.png](/compatibility/jamma/neogeolayout.png){.decorate}

![neogeo-layout_(1).png](/compatibility/jamma/neogeo-layout_(1).png)

## Modo de 4 jugadores

El modo de 4 jugadores se activa directamente desde el menú Recalbox RGB JAMMA.

Los jugadores 3 y 4 se asignan al Kick Harness CPS2:

![4playerscps2.png](/compatibility/jamma/4playerscps2.png){.center}

El sistema virtual que contiene los juegos para 3+ jugadores se activa automáticamente cuando se activa el modo para 4 jugadores.

## Voltaje 5/12V compatible Coin Support+

Recalbox RGB JAMMA es compatible con monederos mecánicos o electrónicos (+3V, +5V, +12V) en su peine JAMMA.
Recalbox detectará automáticamente el nivel lógico del monedero (Normalmente Abierto, o Normalmente Cerrado) para detectar la inserción de monedas.


## Botones Test + Service 

Los botones SERVICE y TEST situados en el interior del terminal tienen dos usos principales:

- acceder al menú de servicio de vuestros juegos arcade (compatible con fbneo, mame, naomi, atomiswave)
- salir de un juego con la combinación SERVICE + TEST. Práctico cuando estás en modo Gamecenter pero quieres cambiar de juego.


## Start Button Patterns (Coin, HK, Turbo, Volume)

Hay una serie de atajos disponibles utilizando el botón de inicio. Estos atajos se pueden activar y desactivar desde el menú Recalbox RGB JAMMA.

![start-patterns.png](/compatibility/jamma/start-patterns.png){.decorate}

* `START + BTN1 = CREDIT`: activa los créditos con el atajo START + BTN1 para cada jugador.
* `START + BTN = HK`: activa los atajos START + BTN = HOTKEY + BTN. Útil para abrir menús retroarch o ajustar el sonido con START + UP/DOWN.
* `START 3 SEC = EXIT`: sale de los juegos con una pulsación larga en START. Si está desactivado, podéis salir de los juegos con la combinación TEST+SERVICE.
* `START + BTN 5SEC = AUTO DISPARO`: activa el autodisparo en un botón con START + BTN. Desactívalo de la misma forma. Un reinicio resetea los modos turbo.
* `START + UP/DOWN = VOLUMEN`: ajusta el volumen sonoro.
* `START + LEFT/RIGHT = MUESTRA/OCULTA CLONES`: muestra u oculta los clones de un juego de arcade.

## Los atajos

![instruction_space_(2).png](/compatibility/jamma/instruction_space_(2).png)


# AUDIO

## Sound control con el joystick

Utilizad los atajos START + UP y DOWN del joystick para subir o bajar el sonido.


## Outputs

* La toma JACK se utiliza para emitir sonido estéreo sin amplificar. Para utilizar con un amplificador externo.
* El amplificador MONO se utiliza para emitir sonido en los pines de audio del JAMMA. Se puede desactivar en las opciones.
* La toma de audio de 4 pines también se puede utilizar para emitir una señal estéreo sin amplificar.

![4pin_audio.png](/compatibility/jamma/4pin_audio.png){.center}

## Mono Amp BOOST

El sonido del amplificador mono se puede ajustar de varias maneras:

* mediante el mando giratorio del Recalbox RGB JAMMA
* desde la interfaz (START + UP/DOWN)
* También es posible realzar el sonido aumentando la ganancia del amplificador desde el menú. Aumenta la ganancia paso a paso para evitar sorpresas desagradables :)

![monoboost.png](/compatibility/jamma/monoboost.png){.decorate}

# SYSTEM

## Automatic Systems by Editor

Cuando añadís un set completo de FBNeo o Mame, Recalbox puede mostrar automáticamente los sistemas que corresponden a cada editor arcade.
Los sistemas se encuentran activados por defecto por Recalbox para Recalbox RGB JAMMA.

Para activar o desactivar estos sistemas virtuales, id al menú ARCADE.

![virtual-editors.png](/compatibility/jamma/virtual-editors.png){.decorate}

## Auto Boot on game

Para arrancar directamente un juego al encender el terminal, activad el "arranque directo sobre un juego" en el menú AJUSTES AVANZADOS -> AJUSTES DE ARRANQUE.

A continuación, id hasta el juego que deseáis iniciar automáticamente, pulsad INICIO y seleccionad INICIAR ESTE JUEGO.

Cada vez que lancéis el juego, se iniciará automáticamente. Mantened pulsado START para salir y volver al menú.

![bootongame.png](/compatibility/jamma/bootongame.png){.decorate}


## Modo Gamecenter

El modo GameCenter os permite arrancar el kiosco como en una sala recreativa: sólo se lanzara un juego, y los créditos deben añadirse usando la ranura para monedas, no hay accesos directos disponibles y sólo se puede salir del juego usando la combinación SERVICE + TEST (dos botones dentro del terminal, accesibles sólo abriendo la compuerta usando las llaves de la máquina de arcade).

Si a esto le añadís el arranque automático sobre un juego, vuestro kiosco se comportará al 100% como una máquina de arcade de la época.

Para activar el modo GameCenter, simplemente desactivad el "start button patterns" (los atajos de botones) dentro del menú Recalbox RGB JAMMA:

![gamecenter.png](/compatibility/jamma/gamecenter.png){.decorate}


## System Update

Las actualizaciones de Recalbox son frecuentes, y estamos orgullosos de ello. Los errores se solucionan lo antes posible, para que podáis beneficiar rápidamente de las últimas funciones.

Recalbox os ofrecerá automáticamente actualizaciones si vuestro sistema está conectado a internet, por cable o wifi.


# TIPS

### Alimentation grise Astro City

Las fuentes de alimentación grises Sega Astro City 400-5198-01**Y** (y sólo este modelo) causan problemas en sistemas que no consumen suficiente energía a 5V en el arranque.

![psu400-5198-01y.jpg](/compatibility/jamma/psu400-5198-01y.jpg){.decorate}

Si este es el caso, podéis conectar una resistencia de 8Ω 25W a los dos pads +5V y GND situados en la fuente de alimentación de la Recalbox RGB JAMMA. Esto extraerá la corriente necesaria para asegurar que la fuente de alimentación no se dispare en el arranque.

Utilizad pines y cables Dupont en lugar de soldar los cables directamente a la placa.

Aseguráos de aislar correctamente vuestra resistencia para evitar que provoque cortocircuitos (su contorno de cobre es conductor).

![resistor.png](/compatibility/jamma/resistor.png){.decorate}

Aquí podéis ver cómo la resistencia ha sido aislada con plástico anti electricidad estática:

![pxl_20240210_203627364~2.jpg](/compatibility/jamma/pxl_20240210_203627364~2.jpg){.decorate}

# Troubleshoting / Recherche de panne

* **Mi Raspberry Pi no arranca, y sobre el Recalbox RGB JAMMA sólo veo encendido un led arriba a la derecha.**

![ouvlo.jpg](/compatibility/jamma/ouvlo.jpg){.decorate}{.center}

Si el LED de encendido se ilumina, pero el LED `+5V RPi` del sistema OUVLO permanece apagado, el sistema de protección está impidiendo que la corriente fluya al Raspberry Pi. 
La alimentación de tu terminal es o demasiado alta (por encima de 5,25V) o demasiado baja.

Hara falta ajustarla con un destornillador adecuado, entre 5,1V y 5,2V sin carga.

# F.A.Q

* **¿Necesito añadir un ventilador o una almohadilla térmica al Raspberry Pi?**
No, el ventilador de la Recalbox RGB JAMMA es suficiente para obtener una refrigeración adecuada.

* **¿Necesito añadir una fuente de alimentación USB?**
No 

* **¿Recalbox RGB JAMMA es compatible con los terminales JVS?**
No directamente. Necesitaréis una PCB para adaptar el JAMMA al JVS de tu terminal. Una vez adaptado, Recalbox RGB JAMMA es totalmente compatible con los mandos y pantallas de los terminales JVS (31kHz o multysinc).

* ¿Dónde puedo comprar un Recalbox RGB JAMMA?
En [la tienda oficial Recalbox](https://shop.recalbox.com) (según disponibilidad y fecha de lanzamiento)

* **¿Cómo se escrpean los juegos ?**
Podéis hacer un scrapping de vuestros juegos para recuperar sus imágenes y videos y toda una serie de informaciones adicionales directamente desde el menú SCRAPER de Recalbox. Recordad que si sois [patron](https://www.patreon.com/recalbox) dispondréis de varias ventajas:
  - Patron de nivel 1 : funcionamiento del auto scrap, que os permite hacer un scrapping de forma automática cuando pasáis sobre un juego de vuestra lista de juegos.
  - Patron de nivel 2 : adémas de lo anterior, tenéis a vuestra disposición varios servidores de scrapping propios a Recalbox, los cuales permiten obtener una velocidad de scrapping mucho más importante que la velocidad por defecto. Podréis efectuar un scrapping completo de vuestros juegos en un tiempo record !
  
- **¿Puedo lanzar los juegos en 480i ?**
Sólo los juegos compatibles con 480i pueden lanzarse en 480i. No es posible forzar un modo 480i si la resolución nativa del juego es de 240p.
En los Raspberry Pi 4/400 y 3, el modo 480i es automático para los juegos compatibles (Dreamcast, Atomiswave, etc.).
En los Raspberry Pi 5, el modo 480i no se soporta actualmente, y vuestros juegos 480i se lanzarán en 240p si utilizáis una pantalla de 15kHz, o bien en 480p si utilizáis una pantalla de 31kHz (o multisync).

- **Puedo overclockear mi Raspberry Pi ?**
Podéis overclockear cualquier Raspberry Pi desde el menú Recalbox -> Opciones avanzadas -> Overclock.
Os recomendamos encarecidamente que ventiléis adecuadamente vuestro Raspberry Pi si hacéis overclock.
También debéis tener en cuenta que el overclocking implica un mayor consumo de energía y, por tanto, una mayor caída del voltaje. Desactivad el overclocking si aparecen alertas de tensión insuficiente.

- **¿Qué puede pasar si aumento demasiado la tensión en la alimentación?**
El Raspberry Pi y el Recalbox RGB JAMMA funcionan hasta un voltaje de 5,25V. Por encima de este voltaje, el sistema OUVLO del Recalbox RGB JAMMA cortará la alimentación para proteger los circuitos.

- **¿Cómo puedo fabricar mis cables de 4 jugadores ?**
Podéis pedir todos estos componentes (conectores, etc.) a smallcab y apoyaros en el esquema que figura más arriba en esta misma página.

- **¿Qué juegos y sets de arcade nos aconsejáis?**
Las versiones de romsets arcade compatibles con los emuladores de Recalbox se encuentran disponibles en [la página arcade](https://wiki.recalbox.com/fr/emulators/arcade).
Recomendamos comenzar con un set completo de FBNEO, que contiene la mayoría de los juegos 2D de arcade, y completar más adelante con los juegos que os faltan a partir de un fullset de MAME.

- **¿Puedo enchufar mandos/joysticks adicionales?**
Claro. Si queréis conectar mandos adicionales, os recomendamos utilizar mandos USB, aunque el Bluetooth también es compatible y se puede activar fácilmente desde las opciones de Recalbox.
Recuerdad reconfigurar los botones la primera vez que conectéis un mando.

- **¿Puedo hacer funcionar los juegos 240p (15kHz) en una pantalla que soporta sólo 31kHz ?**
Los juegos en 240p, 224p y en otras resoluciones se lanzarán en 31kHz en vuestra pantalla, la cual no soporta los modos 15kHz. No obstante, el filtro Scanlines de Recalbox RGB JAMMA os permitirá acercaros al máximo posible al renderizado equivalente de una pantalla de 15kHz.

- **¿Cúal es el soporte de pistolas (guncon, ir gun, sinden)?**
Estas pistolas no están soportadas por el momento.

- **¿Es peligroso cambiar de frecuencias en los monitores multisync ?**
No, siempre y cuando vuestro decodificador no cambie de modo docenas de veces por minuto, en principio no hay riesgo. Sin embargo, algunas personas están de acuerdo en que el relé que hace ese ruido de clic cuando se cambia la resolución es el punto débil de los decks automáticos multifrecuencia. 
Depende de vosotros decidir si queréis usarlo o no con vuestro Recalbox RGB JAMMA. Recalbox no se hace responsable si vuestro deck se estropea.

- **¿Puedo suprimir los videos de introducción?**
Claro. Desde menu -> Opciones avanzadas -> Opciones de arranque.

- **¿Puedo añadir mi propio vídeo de introducción?**
Claro, como se indica [en esta página](https://wiki.recalbox.com/fr/basic-usage/features/boot-videos)

- **¿Cómo se hacen las actualizaciones de Recalbox?**
Simplemente conectad vuestra Recalbox a internet mediante un cable ethernet o wifi. El sistema os pedirá si queréis actualizar al arrancar Recabox siempre y cuando exista una actualización disponible.

- **¿Que tamaño de tarjeta SD nos recomendáis?**
Para una experiencia arcade al 100%, basta con 128 GB (juegos + informaciones de scrapping).
Para una experiencia arcade y consolas de 8/16/32 bits (sin juegos de CD), es mejor una tarjeta SD de 256 GB.

- **¿Cómo se utiliza un Recalbox RGB JAMMA con una máquina recreativa de tipo Naomi ?**
Para conectar un Recalbox RGB JAMMA a una Naomi NUC, necesitarás un adaptador de JAMMA a JVS. Existen varios disponibles en internet.

- **¿Se puede estropear Recalbox RGB JAMMA si lo enchufo al revés en la toma JAMMA??**
Hemos aplicado varias medidas de seguridad para evitar daños en el Recalbox RGB JAMMA o en el Raspberry Pi cuando se enchufa al revés. Sin embargo, tened cuidado al enchufarlo para evitar cualquier riesgo durante su instalación.

- **¿Debo aumentar la tensión de mi alimentación cuando veo un mensaje de"undervoltage"?**
Por naturaleza, la longitud de los cables del arnés JAMMA provoca una importante caída de tensión. Por lo tanto, es normal que haya que aumentar la tensión, sin carga, a entre 5,05 V y 5,2 V en función de los PCB que instalen en el terminal.

- **¿Son compatibles las fichas (jetons) con las máquinas que funcionan con monedas?**
Las fichas tienen un tamaño muy similar al de las monedas de 100 yenes y son compatibles con muchas máquinas de monedas mecánicas y electrónicas. Sin embargo, son sobre todo elementos de coleccionistas y no se garantiza la compatibilidad con las máquinas de monedas.

- **¿Puedo apagar mi máquina recreativa sin pasar por el menú de Recalbox?**
Si, pero intentad no apagar de forma salvaje el sistema sobre todo si cambiáis opciones o añadis/borráis elementos de los favoritos.

- **¿Recalbox RGB JAMMA es compatible con mi panel de 1, 2, 3, 4, 5 o 6 botones ?**
Sí, consultad más arriba en la sección de controladores los diferentes cableados compatibles.

- **¿Recalbox RGB JAMMA es compatible con máquinas que utilizan una pantalla HDMI??**
No.

- **¿Puedo personalizar la disposición de los botones?**
Podéis elegir diferentes opciones de disposición para la NeoGeo, por ejemplo. Sin embargo, no podréis reconfigurar los botones para cada sistema y/o juegos. Hay una razón para ello: queremos ofrecerte una experiencia plug and play. Como la disposición de los botones en los paneles es estándar, la configuración de los mappings se adapta a cada juego y a cada sistema automáticamente.

- **¿Recalbox RGB JAMMA es compatible con las máquinas Viewlix/chewlix?**
No, no si la pantalla necesita una entrada de vídeo HDMI.

- **¿Recalbox RGB JAMMA es compatible con otros sistemas de emulación que no son Recalbox?**
No.

- **¿Con qué Raspberry Pis es compatible el Recalbox RGB JAMMA?**
Con los Raspberry Pi 3/4/400 et 5.

- **¿Recalbox RGB JAMMA es compatibles con los clones de Raspberry Pi (Orange Pi etc...)?**
No.

- **¿Puedo continuar utilizando el ventilador oficial del Raspberry Pi 5?**
No, el ventilador del Recalbox RGB JAMMA remplaza y es suficiente para ventilar el Raspberry Pi.

- **¿Puedo ajustar la geometría de la imagen desde Recalbox RGB JAMMA?**
Es posible ajustar la posición de la imagen para cada modo de visualización en la pantalla de calibración del menú Recalbox RGB JAMMA. Sin embargo, el tamaño vertical y horizontal de la imagen deben ajustarse directamente desde el menu OSD de vuestra pantalla.

- **¿Recalbox RGB JAMMA funciona con todos los tipos de máquinas de arcade que utilizan el estandar JAMMA?**
Si vuestra máquina está equipada con una toma JAMMA, ¡el Recalbox RGB JAMMA es compatible!

- **¿Puedo aumentar el sonido del Recalbox RGB JAMMA?**
Existen varias posibilidades para ajustar el sonido:
  - desde vuestro amplificador de potencia.
  - desde el potenciómetro del Recalbox RGB JAMMA.
  - desde el menú de opciones de sonido.
  - desde el atajo START + UP/DOWN, si está habilitado en las opciones.

- **El sonido satura**
  - Poned el sonido al 90% máximo en las opciones de sonido de EmulationStation (INICIO -> OPCIONES DE SONIDO).
  - Girad el potenciómetro de sonido Mono en el Recalbox RGB JAMMA al máximo.
  - Ajustad el amplificador de vuestra máquina en consecuencia.
  
- **¿Puedo conectar mi Recalbox RGB JAMMA a un supergun?**
Si.

- **No consigo grabar mis favoritos**
Para que vuestros favoritos se queden grabados la próxima vez que encendáis vuestra Recalbox, hay que apagar el sistema de forma ordenada (START -> SALIR -> APAGAR)

- **Tengo interferencias de vídeo**
  - Los niveles RGB del Recalbox RGB JAMMA están cerca de los niveles de los PCB originales, y a menudo son más altos que algunas soluciones multisistema basadas en Raspberry Pi. Pensad en ajustar los niveles de los potenciómetros de vuestra placa.
  - No olvidéis verificar que vuestra máquina de arcade está correctamente conectada a la toma de tierra.
