---
title: Wolfenstein 3D
description: 
published: true
date: 2021-08-08T23:47:57.168Z
tags: ports, wolfenstein3d
editor: markdown
dateCreated: 2021-06-29T19:33:41.665Z
---

![](/emulators/ports/wolfenstein3d.png){.align-center}

## Technical data

* **Created by**: id Software
* **Year of release**: May 5, 1992
* **Genre**: First person shooter
* **Engine**: Wolfenstein Engine
* **Operating system**: MS-DOS 5.0 or higher
* **CPU**: IBM 286 Computer
* **Storage**: 8 MB
* **RAM**: 528k de mémoire conventionnelle
* **Resolution**: 320x200
* **Sound**: Sound Blaster and AdLib or 100% compatible sound card
* **Developper**: id Software
* **Editor**: Apogee Software (FormGen for Spear of Destiny)
* **Director**: Tom Hall
* **Designers**: John Romero, Tom Hall
* **Programmers**: John Carmack, John Romero
* **Artists**: Adrian Carmack
* **Compositor**: Robert Prince (DOS)
* **PEGI**: 12+

## Presentation

**Wolfenstein 3D** is a first-person shooter video game developed by id Software and published by Apogee Software on May 5, 1992 for MS-DOS. It is based on the games Castle Wolfenstein and Beyond Castle Wolfenstein developed by Muse Software in the 1980s. The player embodies the spy B.J. Blazkowicz who tries to escape from a Nazi castle, in which he is imprisoned, during the Second World War. To do this, he must explore several levels to find the elevator or the boss and fight enemies using a knife, a gun and other weapons he can find.

Wolfenstein 3D is the second major game published by id Software after the Commander Keen series. Its development began in 1991 when John Carmack was trying to create a powerful 3D engine that he experimented with in Hovertank 3D and Catacomb 3-D. During a design meeting, the studio decided to give a more violent direction to its next title and the programmer John Romero suggested to make a remake of Castle Wolfenstein using the 3D engine developed by John Carmack. The game is designed by John Romero and Tom Hall who benefits from the help of Adrian Carmack for the artworks and Bobby Prince for the sound effects and the music. The game was published as shareware by Apogee in 1992 in two series of three episodes. An additional episode, Spear of Destiny, was then published by FormGen the same year.

Upon its release, Wolfenstein 3D was a critical and commercial success, winning numerous awards and selling over 200,000 copies by the end of 1992. It is also known as the ancestor of the famous game Doom, and can therefore claim the title of the first "Doom-like game", since it helped popularize this genre and define its basic principles, which would later be copied in many titles. It also demonstrated the viability of shareware distribution.

## History

Wolfenstein 3D takes place during the Second World War. The player plays the American spy B.J. Blazkowicz in his fight against the Nazis. In the original version of the game, divided into three episodes, the hero first tries to escape from Wolfenstein Castle after being captured during a mission to recover the plans of a secret operation. After escaping, he manages to recover and then foil the plans of his enemies who are trying to create an army of mutants. He then infiltrates the Führerbunker under the Reich Chancellery where he finally confronts Adolf Hitler, in a robotic suit equipped with four Gatling guns.

The second trilogy, called The Nocturnal Missions, is a prequel to the first one in which B.J. Blazkowicz tries to thwart the Nazi plans to manufacture chemical weapons. Its first episode follows a chase in the weapons research complex of the scientist responsible for developing these chemical weapons. The hero then travels to Erlangen Castle, in order to steal the plans for these weapons, and then to Offenbach Castle where he confronts the Nazi general responsible for this operation.

The additional episode, published under the title Spear of Destiny, is also a prequel to the original game. The player still plays as B.J. Blazkowicz in his attempt to retrieve the Holy Lance that the Nazis have seized. In the two expansions of Spear of Destiny, Return to Danger and Ultimate Challenge, the hero must also recover the Holy Lance that the Nazis have stolen again in order to use it to build a nuclear weapon or summon demons.

## Emulators

[Libretro ECWolf](libretro-ecwolf)