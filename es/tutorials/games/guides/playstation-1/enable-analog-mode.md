---
title: Activar el modo analog
description: 
published: true
date: 2023-09-21T09:05:02.358Z
tags: ps1, mode, analogue
editor: markdown
dateCreated: 2023-09-21T09:05:02.358Z
---

## Introducción

En algunos juegos, necesitaréis un mando analógico de PlayStation.

## Utilización

* Id al menú RetroArch pulsando `Hotkey + B`.
* Id a `Keys` > `Port 1 Keys`.

(/tutorials/games/guides/playstation-1/enable-analog/psxanalog1.png){.full-width}

* En la opción `Device Type`, desplazaros hasta ver `analog`.

![](/tutorials/games/guides/playstation-1/enable-analog/psxanalog2.png){.full-width}

* Atrás "2 veces" y pulsad "Reanudar".

¡Disfrutad del juego!