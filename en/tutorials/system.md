---
title: 🎛️ System
description: 
published: true
date: 2021-08-06T15:04:33.963Z
tags: tutorial, system
editor: markdown
dateCreated: 2021-08-06T07:56:33.595Z
---

You will find everything about the base, i.e. the system: how to access it or how to modify certain options, for example.

Here are the available categories:

[Access](access)
[Backups and clone](backups-and-clone)
[Installation](installation)
[Modification](modification)