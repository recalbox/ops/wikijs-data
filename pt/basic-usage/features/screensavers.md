---
title: Protetor de tela
description: 
published: true
date: 2021-08-17T00:14:56.467Z
tags: 
editor: markdown
dateCreated: 2021-08-17T00:13:07.865Z
---

## Introdução

No EmulationStation, é possível definir o protetor de tela a ser usado, e alguns deles têm opções detalhadas em suas próprias páginas.

## Utilização

* Na lista de sistemas, pressione o botão `START` e vá para `CONFIGURAÇÕES DA INTERFACE` e depois para `PROTETOR DE TELA`.
* Aqui você terá várias opções:

  * `ATIVAR PROTETOR DE TELA`: permite que você configure após quantos minutos o protetor de tela será ligado. Você pode escolher entre 0 e 30 minutos.
  * `TIPO DO PROTETOR DE TELA`: Permite que você escolha qual protetor de tela deseja aplicar. Várias opções estão disponíveis.
  * `SISTEMAS PARA EXIBIR NA DEMONSTRAÇÃO`: Permite que você escolha quais sistemas o protetor de tela irá exibir. Válido somente para demonstrações de jogos e clipes de jogos.

## {.tabset}

### Modo de demonstração

O modo de demonstração é um protetor de tela que executa seus jogos em segundo plano.

O modo demonstração extrai de uma lista de sistemas (por padrão há todos os consoles de 8/16 bits, exceto os portáteis), e lança um jogo aleatoriamente de um sistema escolhido. Ele mantém um histórico interno para evitar rodar o mesmo sistema duas vezes seguidas ou rodar o mesmo jogo duas vezes.

![](/basic-usage/features/screensavers/demomode1.png)

#### Ativação

Para ativar este recurso, é muito fácil:

* No EmulationStation, abra o menu com o botão `START`.
* Vá para `CONFIGURAÇÕES DA INTERFACE` > `PROTETOR DE TELA`.
* Aqui, vá para `TIPO DO PROTETOR DE TELA` e escolha `MODO DEMONSTRAÇÃO`.

#### Configuração

Você pode configurar os sistemas a serem utilizados para demonstrações de jogos:

* No EmulationStation, abra o menu com o botão `START`.
* Vá para `CONFIGURAÇÕES DA INTERFACE` > `PROTETOR DE TELA`.
* Aqui, vá para `SISTEMAS PARA EXIBIR NA DEMONSTRAÇÃO` e selecione os sistemas dos quais os jogos serão utilizados.

### Modo gameclip (clipes de jogos)

O modo gameclip utilizará os vídeos de scrape dos seus jogos para exibir na tela como um protetor de tela.

O modo gameclip extrai de uma lista de sistemas (por padrão há todos os consoles de 8/16 bits, exceto portáteis), e inicia um vídeo aleatório em um sistema escolhido. Ele mantém um histórico interno para evitar iniciar o mesmo sistema duas vezes seguidas ou iniciar o mesmo vídeo duas vezes.

#### Ativação

Para ativar esta função, nada poderia ser mais fácil:

* No EmulationStation, abra o menu com o botão `START`.
* Vá para `CONFIGURAÇÕES DA INTERFACE` > `PROTETOR DE TELA`.
* Aqui, vá para `TIPO DO PROTETOR DE TELA` e escolha `GAMECLIP`.

#### Configuração

Você pode configurar os sistemas a serem utilizados no modo gameclip:

* No EmulationStation, abra o menu com o botão `START`.
* Vá para `CONFIGURAÇÕES DA INTERFACE` > `PROTETOR DE TELA`.
* Aqui, vá para `SISTEMAS PARA EXIBIR NA DEMONSTRAÇÃO` e selecione os sistemas dos quais os jogos serão utilizados.
