---
title: Flashback
description: 
published: true
date: 2024-07-26T08:29:15.207Z
tags: ports, flashback
editor: markdown
dateCreated: 2023-09-04T12:37:18.073Z
---

![](/emulators/ports/flashback.jpg){.align-center}

## Ficha técnica

* **Creado por :** Delphine Software International
* **Año de lanzamiento :** 1992
* **Género:** Plataformas
* **Plataforma(s) :** Amiga, Acorn Archimedes, Mega Drive/Genesis, MS-DOS, NEC PC-9801, Super NES, Sega CD, FM Towns, 3DO, CD-i, Atari Jaguar, Mac OS, iPhone OS, Symbian, Maemo, Dreamcast, Nintendo Switch, PlayStation 4.
* **Diseño :** Paul Cuisset
* **Compositores:** Jean Baudlot, Fabrice Visserot, Raphaël Gesqua (versión Amiga)
* **PEGI** : 12

## Sinopsis

![](/emulators/ports/flashback_01.jpg)

Corre el año 2142. El eminente científico Conrad B. Hart sufre amnesia y se encuentra en el corazón de la selva cuando recupera la conciencia. No muy lejos de él hay una curiosa caja que genera una imagen holográfica suya. La imagen le dice que vaya a Nueva Washington, donde le espera un hombre llamado Ian. De camino a la ciudad, Conrad ignora que los alienígenas se han infiltrado entre la población y que tendrá que desbaratar un increíble complot interplanetario mientras intenta encontrar los holocubos que contienen fragmentos de su memoria...

Es un escenario que recuerda al cuento Souvenirs à vendre (Podemos recordarlo por usted al por mayor) de Philip K. Dick publicado en 1966 y adaptado a la pantalla en 1990 por Paul Verhoeven bajo el título Total Recall, sólo dos años antes del estreno de Flashback.

## Emuladores

[Libretro REminiscence](libretro-reminiscence)