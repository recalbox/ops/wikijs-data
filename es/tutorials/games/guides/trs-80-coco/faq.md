---
title: FAQ
description: 
published: true
date: 2023-09-21T13:27:25.415Z
tags: faq
editor: markdown
dateCreated: 2023-09-21T13:27:25.415Z
---

## Algunas informaciones útil...

### ¿Qué máquinas puedo utilizar con el Ordenador en color TRS-80?

Por defecto, la máquina utilizada será la Color Computer 2 PAL (CoCo 2 PAL).

Dependiendo de los subdirectorios, la máquina a utilizar se seleccionará automáticamente según estos criterios:

```text
"/coco/": "coco",
"/coco-pal/": "coco",
"/cocous/": "cocous",
"/coco-ntsc/": "cocous",
"/coco2/": "coco2b",
"/coco2b/": "coco2b",
"/coco2-pal/": "coco2b",
"/coco2b-pal/": "coco2b",
"/coco2us/": "coco2bus",
"/coco2bus/": "coco2bus",
"/coco2-ntsc/": "coco2bus",
"/coco2b-ntsc/": "coco2bus",
"/mx/": "mx1600",
"/mx1600/": "mx1600",
```

### ¿Admite el ordenador TRS-80 Color el suavizado?

Sí, el core XRoar admite suavizado (en EmulationStation, `START` > `GAME OPTIONS` > `SMOOTHING`).

### ¿Beneficia el Ordenador a Color TRS-80 de las sobrecargas de configuración?

Sí, pero las sobrecargas de configuración no se sobrecargan entre sí.

La primera que se encuentra en esta secuencia es la única que se tiene en cuenta: `rom.ext.config` > `<carpeta>.config` > `<padre>.config` > ... La sintaxis del fichero de configuración no permite gestionar las sobrecargas de forma completa sin impactar el rendimiento del sistema.

Para generar un fichero de configuración, podéis utilizar este comando SSH: `xroar --config-all > .config`.

### ¿Se tienen en cuenta los 2 joysticks?

Sí, los joysticks 0 y 1 se tienen en cuenta automáticamente, sea cual sea el orden que los mismos en EmulationStation.

### Hemos podido imprimir con el Ordenador a Color TRS-80. ¿Se puede volver a hacer?

Sí, cuando la máquina 'imprima', el texto impreso irá al fichero `/recalbox/share/saves/trs80coco/printer/rom-extension.txt`.