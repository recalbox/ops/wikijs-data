---
title: Z-Machine
description: 
published: true
date: 2023-08-06T10:13:41.377Z
tags: 9.0+, z-machine, infocom, engines
editor: markdown
dateCreated: 2023-02-26T18:25:33.858Z
---

![](/emulators/fantasy/zmachine.svg){.align-center}

## Techincal data

* **Developer:** Joel Berez and Marc Blank
* **Year of release:** 1979
* **Device:** keyboard
* **Display:** 640x480 pixels

## Présentation

The Z-machine is a virtual machine that was developed by Joel Berez and Marc Blank in 1979 and used by Infocom for its text adventure games. Infocom compiled game code to files containing Z-machine instructions (called story files or Z-code files) and could therefore port its text adventures to a new platform simply by writing a Z-machine implementation for that platform. With the large number of incompatible home computer systems in use at the time, this was an important advantage over using native code or developing a compiler for each system.

The "Z" of Z-machine stands for Zork, Infocom's first adventure game. Z-code files usually have names ending in .z1, .z2, .z3, .z4, .z5, .z6, .z7, or .z8, where the number is the version number of the Z-machine on which the file is intended to be run, as given by the first byte of the story file.

This is a modern convention, however. Infocom itself used extensions of .dat (Data) and .zip (ZIP = Z-machine Interpreter Program), but the latter clashed with the widespread use of .zip for PKZIP-compatible archive files starting in the 1990s, after Activision closed Infocom. Infocom produced six versions of the Z-machine. Files using versions 1 and 2 are very rare. Only two version 1 files are known to have been released by Infocom and only two of version 2. Version 3 covers the majority of Infocom's released games. Later versions had more capabilities, culminating in some graphic support in version 6.

The compiler (called Zilch) that Infocom used to produce its story files has never been released, although documentation of the language used (ZIL, a Lisp-family language similar to MDL) still exists, and an open-source replacement "ZILF" has been written. After Mediagenic moved Infocom to California in 1989, Computer Gaming World stated that "ZIL ... is functionally dead", and reported rumors of a "completely new parser that may never be used". In May 1993, Graham Nelson released the first version of his Inform compiler, which also generates Z-machine story files as its output, even though the Inform source language is quite different from ZIL.

Inform has become popular in the interactive fiction community. A large proportion of interactive fiction is in the form of Z-machine story files. Demand for the ability to create larger game files led Nelson to specify versions 7 and 8 of the Z-machine, though version 7 is rarely used. Because of the way addresses are handled, a version 3 story file can be up to 128K in length, a version 5 story can be up to 256K in length, and a version 8 story can be up to 512k in length. Though these sizes may seem small by today's computing standards, for text-only adventures, these are large enough for elaborate games.

During the 1990s, Graham Nelson drew up a Z-Machine Standard based on detailed studies of the existing Infocom files. The standard also includes extensions used by his newer versions, as well as links to the "Blorb" resource format used by Infocom, and a "Quetzal" savefile format. In 2006, Nelson expanded Z-machine to the 32-bit Glulx format for Inform 7. The Interactive Fiction Technology Foundation, founded 2016, manages all these standards.

## Emulators

[Frotz](frotz)