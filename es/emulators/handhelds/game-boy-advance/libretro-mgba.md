---
title: Libretro mGBA
description: 
published: true
date: 2024-07-25T22:10:35.926Z
tags: libretro, gameboy, mgba, advance, gba
editor: markdown
dateCreated: 2023-09-11T13:06:59.430Z
---

Libretro **mGBA** es un emulador para ejecutar juegos de Game Boy Advance.  
Pretende ser más rápido y preciso que muchos de los emuladores de Game Boy Advance existentes, además de añadir funciones de las que carecen otros emuladores. También es compatible con juegos de Game Boy y Game Boy Color.

## ![](/emulators/license.svg) Licencia

El core posee la siguiente licencia [**MPLv2.0**](https://github.com/libretro/mgba/blob/master/LICENSE).

## ![](/emulators/compatibility.png) Compatibilidad

 | RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | 
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | 

## ![](/emulators/features.png) Funcionalidades

| Funcionalidad | ¿Soportada? |
| :---: | :---: |
| Reinicio | ✔ |
| Grabaciones | ✔ |
| Grabaciones instantáneas | ✔ |
| Rebobinado | ✔ |
| Opciones del core | ✔ |
| RetroAchievements | ✔ |
| Cheats RetroArch | ✔ |
| Controles | ✔ |
| Remapping | ✔ |
| Vibración | ✔ |
| Softpatching | ✔ |

## ![](/emulators/bios.svg) BIOS

### Lista de BIOS opcionales

| Nombre del fichero | Descripción | MD5 | ¿Preinstalada? |
| :---: | :---: | :---: | :---: |
| gba_bios.bin | BIOS Game Boy Advance | a860e8c0b6d573d191e4ec7db1b1e4f6 | ❌ |

### Emplazamiento

Disponed vuestras bios como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 gba
┃ ┃ ┃ ┃ ┣ 🗒 **gba_bios.bin**

## ![](/emulators/roms.png) Roms

### Extensiones de roms soportadas

Las roms deben tener las extensiones siguientes:

* .gba
* .zip
* .7z

Este sistema soporta las roms comprimidas en formato .zip/.7z. No obstante, se trata sólo de un archivo.

Los ficheros contenidos en los .zip/.7z deben tener las extensiones citadas previamente.
Además, cada fichero .zip/.7z sólo debe contener un fichero rom al interior.

### Emplazamiento

Disponed vuestras roms como en la siguiente imagen: 

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 gba
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.zip**

>Se aconsejan las roms del proyecto **No-Intro**.
{.is-success}

>Para más información sobre las roms, consultad [este tutorial](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

>Para conservar las configuraciones personalizadas cuando se produce una actualización del sistema Recalbox, os aconsejamos utilizar las [Sobrecargas de configuración](./../../../advanced-usage/configuration-override).
{.is-info}

### Acceder a las opciones

Podéis configurar las opciones del core de dos formas diferentes.

* Via el Menú RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via el fichero `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Opciones del core

| Opción | Valor de la opción | Variable (para la sobrecarga) | Valores posibles |
| --- | --- | --- | --- |
| Modèle de Game Boy (Redémarrage requis) | `Détection automatique` ✅ / `Game Boy` / `Super Game Boy` / `Game Boy Color` / `Game Boy Advance` | `mgba_gb_model` | `Autodetect` / `Game Boy` / `Super Game Boy` / `Game Boy Color` / `Game Boy Advance` |
| Utiliser le fichier BIOS si trouvé (Redémarrage requis) | `Activé` ✅ / `Désactivé` | `mgba_use_bios` | `ON` / `OFF` |
| Ignorer l'intro du BIOS (Redémarrage requis) | `Désactivé` ✅ / `Activé` | `mgba_skip_bios` | `OFF` / `ON` |
| Palette Game Boy par défaut | `Niveaux de gris` ✅ / `DMG Green` / `GB Pocket` / `GB Light` / `GBC Brown ↑` / `GBC Red ↑A` / `GBC Dark Brown ↑B` / `GBC Pale Yellow ↓` / `GBC Orange ↓A` / `GBC Yellow ↓B` / `GBC Blue ←` / `GBC Dark Blue ←A` / `GBC Gray ←B` / `GBC Green →` / `GBC Dark Green →A` / `GBC Reverse →B` / `SGB 1-A` / `SGB 1-B` / `SGB 1-C` / `SGB 1-D` / `SGB 1-E` / `SGB 1-F` / `SGB 1-G` / `SGB 1-H` / `SGB 2-A` / `SGB 2-B` / `SGB 2-C` / `SGB 2-D` / `SGB 2-E` / `SGB 2-F` / `SGB 2-G` / `SGB 2-H` / `SGB 3-A` / `SGB 3-B` / `SGB 3-C` / `SGB 3-D` / `SGB 3-E` / `SGB 3-F` / `SGB 3-G` / `SGB 3-H` / `SGB 4-A` / `SGB 4-B` / `SGB 4-C` / `SGB 4-D` / `SGB 4-E` / `SGB 4-F` / `SGB 4-G` / `SGB 4-H` | `mgba_gb_colors` | `Grayscale` / `DMG Green` / `GB Pocket` / `GB Light` / `GBC Brown ↑` / `GBC Red ↑A` / `GBC Dark Brown ↑B` / `GBC Pale Yellow ↓` / `GBC Orange ↓A` / `GBC Yellow ↓B` / `GBC Blue ←` / `GBC Dark Blue ←A` / `GBC Gray ←B` / `GBC Green →` / `GBC Dark Green →A` / `GBC Reverse →B` / `SGB 1-A` / `SGB 1-B` / `SGB 1-C` / `SGB 1-D` / `SGB 1-E` / `SGB 1-F` / `SGB 1-G` / `SGB 1-H` / `SGB 2-A` / `SGB 2-B` / `SGB 2-C` / `SGB 2-D` / `SGB 2-E` / `SGB 2-F` / `SGB 2-G` / `SGB 2-H` / `SGB 3-A` / `SGB 3-B` / `SGB 3-C` / `SGB 3-D` / `SGB 3-E` / `SGB 3-F` / `SGB 3-G` / `SGB 3-H` / `SGB 4-A` / `SGB 4-B` / `SGB 4-C` / `SGB 4-D` / `SGB 4-E` / `SGB 4-F` / `SGB 4-G` / `SGB 4-H` |
| Palettes de Game Boy prédéfinies par le matériel (Redémarrage requis) | `Préréglage Game Boy par défaut` / `Préréglages de Game Boy Color uniquement` / `Préréglages de Super Game Boy uniquement` / `Tous les préréglages disponibles` | `mgba_gb_colors_preset` | `0` / `1` / `2` / `3` |
| Utiliser les bordures de Super Game Boy (Redémarrage requis) | `Activé` ✅ / `Désactivé` | `mgba_sgb_borders` | `ON` / `OFF` |
| Correction colorimétrique | `Désactivé` ✅ / `Game Boy Advance` / `Game Boy Color` / `Auto` | `mgba_color_correction` | `OFF` / `GBA` / `GBC` / `Auto` |
| Interframe Blending | `Désactivé` ✅ / `Simple` / `Intelligent` / `Rémanence LCD (réaliste)` / `Rémanence LCD (rapide)` | `mgba_interframe_blending` | `OFF` / `mix` / `mix_smart` / `lcd_ghosting` / `lcd_ghosting_fast` |
| Filtre passe-bas | `Désactivé` ✅ / `Activé` | `mgba_audio_low_pass_filter` | `disabled` / `enabled` |
| Niveau du filtre | `5%` / `10%` / `15%` / `20%` / `25%` / `30%` / `35%` / `40%` / `45%` / `50%` / `55%` / `60%` / `65%` / `70%` / `75%` / `80%` / `85%` / `90%` / `95%` | `mgba_audio_low_pass_range` | `5` / `10` / `15` / `20` / `25` / `30` / `35` / `40` / `45` / `50` / `55` / `60` / `65` / `70` / `75` / `80` / `85` / `90` / `95` |
| Autoriser les entrées directionnelles opposées | `Désactivé` ✅ / `Activé` | `mgba_allow_opposing_directions` | `no` / `yes` |
| Niveau de capteur solaire | `Utiliser le capteur de l'appareil si disponible` / `0` ✅ / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` | `mgba_solar_sensor_level` | `sensor` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` |
| Vibration du Game Boy Player (Redémarrage requis) | `Désactivé` ✅ / `Activé` | `mgba_force_gbp` | `OFF` / `ON` |
| Suppression de la boucle inactive | `Supprimer celles connues` ✅ / `Détecter et supprimer` / `Ne pas supprimer` | `mgba_idle_optimization` | `Remove Known` / `Detect and Remove` / `Don't Remove` |
| Saut d'images | `Désactivé` ✅ / `Auto` / `Auto (seuil)` / `Intervalle fixe` | `mgba_frameskip` | `disabled` / `auto` / `auto_threshold` / `fixed_interval` |
| Seuil de saut d'images (%) | `15` / `18` / `21` / `24` / `27` / `30` / `33` ✅ / `36` / `39` / `42` / `45` / `48` / `51` / `54` / `57` / `60` | `mgba_frameskip_threshold` | `15` / `18` / `21` / `24` / `27` / `30` / `33` / `36` / `39` / `42` / `45` / `48` / `51` / `54` / `57` / `60` |
| Intervalle de saut d'images | `0` ✅ / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` | `mgba_frameskip_interval` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` |

## ![](/emulators/external-links.png) Enlaces externos

* **Código fuente utilizado** : [https://github.com/libretro/mgba/](https://github.com/libretro/mgba/)
* **Documentación Libretro** : [https://docs.libretro.com/library/mgba/](https://docs.libretro.com/library/mgba/)
* **Código fuente oficial** : [https://github.com/mgba-emu/mgba](https://github.com/mgba-emu/mgba/)
* **Página web oficial** : [https://mgba.io/](https://mgba.io/)