---
title: Ajouter des thèmes à l'interface
description: 
published: true
date: 2024-12-11T03:13:05.460Z
tags: emulationstation, thèmes
editor: markdown
dateCreated: 2021-05-21T08:07:10.543Z
---

**Envie d'un nouveau thème pour votre interface Recalbox ? Vous êtes au bon endroit.**

## Ou trouver un thème
L'ensemble des thèmes connus disponibles sont présentés sur cette page

La majorité des liens vous dirigerons vers le [forum](https://forum.recalbox.com/category/14/themes-interface) sur lequel vous pouvez télécharger les thèmes créés par la communauté.
N’hésitez pas à échanger avec leurs auteurs et laisser un commentaire sur les posts respectifs.

Encore un grand merci à eux pour leur travail et leur partage.

---

## Ajouter un thème à Recalbox

Assurez-vous simplement d'utiliser un thème compatible avec votre version de Recalbox (donc un thème à jour avec une version à jour).

---

Vous pouvez ajouter manuellement le thème de votre choix en suivant ces étapes :

* Décompressez l'archive téléchargée
* Copiez le dossier contenant le thème que vous souhaitez utiliser sur votre interface de deux façons différentes :
  * Soit depuis le réseau : `\\recalbox\share\themes\`  
  * Soit sur la carte SD ou sur votre stockage externe le cas échéant :`SHARE/themes/`

> Attention, la totalité des dossiers de votre thème doit être dans un unique dossier et non à la racine du dossier `themes` **sans espaces**.
Cela donne donc une arborescence qui ressemble à la suivante :
📁 recalbox
┣ 📁 share
┃ ┣ 📁 themes
┃ ┃ ┣ 📁 Hyperwall-720p
{.is-warning}

* Vous pouvez maintenant changer le thème dans les `Options de l'interface > Theme` sans redémarrer.
* Certains thèmes disposent d'options de personnalisation. Vous les trouverez dans les `Options de l'interface > Configuration du theme`.

---

> Un tutoriel vidéo est disponible sur YouTube via ce lien : [Changez le thème de Recalbox](https://youtu.be/U185lY2tKTg).
{.is-info}

## Liste des thèmes et compatibilité
| |HDMI PI/PC/OXu4|RGB Dual/JAMMA|Anbernic RG353|GPICase|OGoA|OGoS|16/9|4/3|TATE|
|-|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
|  Artbook CRT	|⚠️|✅|✅|❌|:grey_question:|:grey_question:|❌|✅|✅|
|  ARTFLIX NX		|✅|⚠️|⚠️|❌|:grey_question:|:grey_question:|✅|:grey_question:|❌|
|  Authentica		|✅|⚠️|⚠️|❌|:grey_question:|:grey_question:|✅|:grey_question:|❌|
|  Blue Box 	  |✅|⚠️|⚠️|❌|:grey_question:|:grey_question:|✅|:grey_question:|❌|
|  Bresson 	    |✅|⚠️|⚠️|❌|:grey_question:|:grey_question:|✅|:grey_question:|❌|
|  ColorLines 	|✅|⚠️|⚠️|❌|:grey_question:|:grey_question:|✅|:grey_question:|❌|
|  DarkMachines |✅|⚠️|⚠️|❌|:grey_question:|:grey_question:|✅|:grey_question:|❌|
|  Dark Slide   |✅|⚠️|⚠️|❌|:grey_question:|:grey_question:|✅|:grey_question:|❌|
|  Dashboard X  |✅|✅|✅|❌|:grey_question:|:grey_question:|✅|✅|✅|
|  Dreambox		  |✅|⚠️|⚠️|❌|:grey_question:|:grey_question:|✅|:grey_question:|❌|
|  Epic Noir	  |✅|⚠️|⚠️|❌|:grey_question:|:grey_question:|✅|:grey_question:|❌|
|  FFArts			  |✅|⚠️|⚠️|❌|:grey_question:|:grey_question:|✅|:grey_question:|❌|
|  GameBoy		  |⚠️|✅|✅|❌|:grey_question:|:grey_question:|❌|✅|❌|
|  Hyperwall	  |✅|⚠️|⚠️|❌|:grey_question:|:grey_question:|✅|:grey_question:|❌|
|  Metal Slide  |✅|⚠️|⚠️|❌|:grey_question:|:grey_question:|✅|:grey_question:|❌|
|  Midnight			|✅|✅|✅|❌|:grey_question:|:grey_question:|✅|✅|✅|
|  Mister				|⚠️|✅|✅|✅|:grey_question:|:grey_question:|❌|✅|✅|
|  Muskashi			|✅|❌|❌|❌|❌|❌|✅|❌|❌|
|	 Next Pixel 	|✅|❌|❌|❌|:grey_question:|:grey_question:|✅|✅|❌|
|	 Retro Switch |✅|⚠️|⚠️|❌|:grey_question:|:grey_question:|✅|:grey_question:|❌|
|	 Unifiedspin	|✅|⚠️|⚠️|❌|:grey_question:|:grey_question:|✅|:grey_question:|❌|
|	 Uniflyered		|✅|⚠️|⚠️|❌|:grey_question:|:grey_question:|✅|:grey_question:|❌|

Légende :

✅ Optimisé : fonctionne parfaitement, et a été optimisé pour la plateforme en question
⚠️ Compatible : fonctionne mais n'est pas optimisé, certains éléments ne sont pas mis en page tel qui devraient ou les tailles de police ne sont pas optimales mais ces problèmes restent mineurs et n’empêchent pas le bon fonctionnement du thème
❌ Non compatible : ne fonctionne pas ou la mise en page du thème n'est pas du tout adapté (éléments ou textes qui se chevauchent, textes illisibles, images déformées...)
:grey_question: En attente de test : Et oui ça prend du temps, ne pas hésiter à participer  [ici](https://forum.recalbox.com/topic/24510)


## Les thèmes

### Artbook CRT
Auteur : **Beudbeud**  

- Theme optimisé pour RGB Dual et RGB JAMMA
- Résolution 4/3 Yoko et Tate - 240p/480p
- Prise en charge de tous les systèmes et systèmes virtuels pour recalbox 9.2

|HDMI PI/PC/OXu4|RGB Dual/JAMMA|Anbernic RG353|GPICase|OGoA|OGoS|16/9|4/3|TATE|
|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
|⚠️|✅|✅|❌|:grey_question:|:grey_question:|❌|✅|✅|

![9.2_artbookcrt_1.jpg](/tutorials/frontend-customization/add-themes-into-emulationstation/9.2_artbookcrt_1.jpg)
![9.2_artbookcrt_2.jpg](/tutorials/frontend-customization/add-themes-into-emulationstation/9.2_artbookcrt_2.jpg)

> Présentation et téléchargement : **[Forum recalbox](https://forum.recalbox.com/topic/33153/recalbox-9-theme-artbook-crt-240p)** ou **[github](https://github.com/beudbeud/recalbox-artbookcrt)**
{.is-info}


### ARTFLIX NX
Auteur : **Fagnerpc**

- ⚠️ Manque certains systèmes mais utilisable
- ⚠️ Manque l'ensemble des systèmes virtuels arcade
- ⚠️ Ne prend pas en compte l'option des drapeaux dans les gamelist
- Résolution disponible : 540p - 720p
- Options : choix de la résolution

|HDMI PI/PC/OXu4|RGB Dual/JAMMA|Anbernic RG353|GPICase|OGoA|OGoS|16/9|4/3|TATE|
|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
|✅|⚠️|⚠️|❌|:grey_question:|:grey_question:|✅|:grey_question:|❌|

![8.0.1_artflix_nx_-_1.jpg](/tutorials/frontend-customization/add-themes-into-emulationstation/8.0.1_artflix_nx_-_1.jpg)
![8.0.1_artflix_nx_-_2.jpg](/tutorials/frontend-customization/add-themes-into-emulationstation/8.0.1_artflix_nx_-_2.jpg)
> Présentation et téléchargement : [Forum recalbox](https://forum.recalbox.com/topic/25918)
{.is-info}

### Authentica 
Auteur : **Butch Games**

- ⚠️ Quelques visuels/background manquant mais utilisable
- Options : couleurs, vues systèmes, vues gamelist, clips, régions

|HDMI PI/PC/OXu4|RGB Dual/JAMMA|Anbernic RG353|GPICase|OGoA|OGoS|16/9|4/3|TATE|
|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
|✅|⚠️|⚠️|❌|:grey_question:|:grey_question:|✅|:grey_question:|❌|

![8.0.1_authentica_1.jpg](/tutorials/frontend-customization/add-themes-into-emulationstation/8.0.1_authentica_1.jpg)
![8.0.1_authentica_2.jpg](/tutorials/frontend-customization/add-themes-into-emulationstation/8.0.1_authentica_2.jpg)
> Présentation et téléchargement : [Forum recalbox](https://forum.recalbox.com/topic/25888) 
{.is-info}

### Blue Box
Auteur : **Butch Games**

- ⚠️ Manque certains systèmes mais utilisable
- ⚠️ Manque l'ensemble des systèmes virtuels arcade
- Options : couleurs, icones, menus, vues systèmes, vues gamelist, clips, régions

|HDMI PI/PC/OXu4|RGB Dual/JAMMA|Anbernic RG353|GPICase|OGoA|OGoS|16/9|4/3|TATE|
|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
|✅|⚠️|⚠️|❌|:grey_question:|:grey_question:|✅|:grey_question:|❌|

![8.1.1_blue_box_1.jpg](/tutorials/frontend-customization/add-themes-into-emulationstation/8.1.1_blue_box_1.jpg)
![8.1.1_blue_box_2.jpg](/tutorials/frontend-customization/add-themes-into-emulationstation/8.1.1_blue_box_2.jpg)
> Présentation et téléchargement : [Forum recalbox](https://forum.recalbox.com/topic/26947)
{.is-info}

### Bresom
Auteur : **lemerle**

- ⚠️ Manque certains systèmes mais utilisable
- ⚠️ Manque l'ensemble des systèmes virtuels arcade
- Options : choix de la résolution

|HDMI PI/PC/OXu4|RGB Dual/JAMMA|Anbernic RG353|GPICase|OGoA|OGoS|16/9|4/3|TATE|
|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
|✅|⚠️|⚠️|❌|:grey_question:|:grey_question:|✅|:grey_question:|❌|

![8.1.1_bresom_1.jpg](/tutorials/frontend-customization/add-themes-into-emulationstation/8.1.1_bresom_1.jpg)
![8.1.1_bresom_2.jpg](/tutorials/frontend-customization/add-themes-into-emulationstation/8.1.1_bresom_2.jpg)
> Présentation et téléchargement : [Forum recalbox](https://forum.recalbox.com/topic/29334)
{.is-info}

### ColorLines
Auteur : **Butch Games**

- ⚠️ Manque certains systèmes mais utilisable
- ⚠️ Manque l'ensemble des systèmes virtuels arcade
- ⚠️ Bug d'affichage sur liste des jeux
- Options : couleurs, icones, playlist, vues gamelist, clips, régions

|HDMI PI/PC/OXu4|RGB Dual/JAMMA|Anbernic RG353|GPICase|OGoA|OGoS|16/9|4/3|TATE|
|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
|✅|⚠️|⚠️|❌|:grey_question:|:grey_question:|✅|:grey_question:|❌|

![8.0.1_colorlines_1.jpg](/tutorials/frontend-customization/add-themes-into-emulationstation/8.0.1_colorlines_1.jpg)
![8.0.1_colorlines_2.jpg](/tutorials/frontend-customization/add-themes-into-emulationstation/8.0.1_colorlines_2.jpg)
> Présentation et téléchargement : [Forum recalbox](https://forum.recalbox.com/topic/25363)
{.is-info}

### DARK MACHINES
Auteur : **Jorge Rosa**

- ⚠️ Manque certains systèmes mais utilisable
- ⚠️ Manque l'ensemble des systèmes virtuels arcade

|HDMI PI/PC/OXu4|RGB Dual/JAMMA|Anbernic RG353|GPICase|OGoA|OGoS|16/9|4/3|TATE|
|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
|✅|⚠️|⚠️|❌|:grey_question:|:grey_question:|✅|:grey_question:|❌|

![9.1_dark_machines_1.jpg](/tutorials/frontend-customization/add-themes-into-emulationstation/9.1_dark_machines_1.jpg)
![9.1_dark_machines_2.jpg](/tutorials/frontend-customization/add-themes-into-emulationstation/9.1_dark_machines_2.jpg)
> Présentation et téléchargement : [Forum recalbox](https://forum.recalbox.com/topic/30269)
{.is-info}

### Dark Slide
Auteur : **Butch Games**
- Tous les systèmes et sytèmes virtuels par genre
- ⚠️ 33/51 systèmes virtuels arcade
- Options : couleurs, icones, vues systèmes, vues gamelist, clips, régions

|HDMI PI/PC/OXu4|RGB Dual/JAMMA|Anbernic RG353|GPICase|OGoA|OGoS|16/9|4/3|TATE|
|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
|✅|⚠️|⚠️|❌|:grey_question:|:grey_question:|✅|:grey_question:|❌|

![9.1_dark_slide_1.jpg](/tutorials/frontend-customization/add-themes-into-emulationstation/9.1_dark_slide_1.jpg)
![9.1_dark_slide_2.jpg](/tutorials/frontend-customization/add-themes-into-emulationstation/9.1_dark_slide_2.jpg)
> Présentation et téléchargement : [Forum recalbox](https://forum.recalbox.com/topic/30607)
{.is-info}


### Dashboard X
Auteur : **Serviettzky**

- Theme compatible et optimisé à la fois pour version HDMI, RGB Dual/JAMMA
- Résolution 16/9 + 4/3 - Yoko et Tate - HD/240p/480p
- Prise en charge de tous les systèmes et systèmes virtuels pour recalbox 9.2
- ⚠️ Manque l'ensemble des systèmes virtuels par genre


|HDMI PI/PC/OXu4|RGB Dual/JAMMA|Anbernic RG353|GPICase|OGoA|OGoS|16/9|4/3|TATE|
|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
|✅|✅|✅|❌|:grey_question:|:grey_question:|✅|✅|✅|

![9.2_dashboard_x_1.jpg](/tutorials/frontend-customization/add-themes-into-emulationstation/9.2_dashboard_x_1.jpg)
![9.2_dashboard_x_2.jpg](/tutorials/frontend-customization/add-themes-into-emulationstation/9.2_dashboard_x_2.jpg)

> Présentation et téléchargement : **[Forum recalbox](https://forum.recalbox.com/topic/34488/recalbox-9-2-th%C3%A8me-dashboard_x)** ou **[github](https://github.com/serviettzky/dashboard-X)**
{.is-info}

### Dreambox
Auteur : **Butch Games**

- ⚠️ Manque certains systèmes mais utilisable
- ⚠️ Manque l'ensemble des systèmes virtuels arcade
- ⚠️ Bug d'affichage sur liste des jeux
- Options : icones, vues systèmes, vues gamelist, clips, régions

|HDMI PI/PC/OXu4|RGB Dual/JAMMA|Anbernic RG353|GPICase|OGoA|OGoS|16/9|4/3|TATE|
|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
|✅|⚠️|⚠️|❌|:grey_question:|:grey_question:|✅|:grey_question:|❌|

![8.0.1_dreambox_1.jpg](/tutorials/frontend-customization/add-themes-into-emulationstation/8.0.1_dreambox_1.jpg)
![8.0.1_dreambox_2.jpg](/tutorials/frontend-customization/add-themes-into-emulationstation/8.0.1_dreambox_2.jpg)
> Présentation et téléchargement : [Forum recalbox](https://forum.recalbox.com/topic/23272)
{.is-info}

### Epic Noir
Auteur : **Chicuelo adapté par Malixx**

- ⚠️ Manque certains systèmes mais utilisable
- ⚠️ Manque l'ensemble des systèmes virtuels arcade
- Options : icones, résolutions

|HDMI PI/PC/OXu4|RGB Dual/JAMMA|Anbernic RG353|GPICase|OGoA|OGoS|16/9|4/3|TATE|
|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
|✅|⚠️|⚠️|❌|:grey_question:|:grey_question:|✅|:grey_question:|❌|

![9.1_epic_noir_1.jpg](/tutorials/frontend-customization/add-themes-into-emulationstation/9.1_epic_noir_1.jpg)
![9.1_epic_noir_2.jpg](/tutorials/frontend-customization/add-themes-into-emulationstation/9.1_epic_noir_2.jpg)
> Présentation et téléchargement : [Forum recalbox](https://forum.recalbox.com/topic/30440)
{.is-info}

### FFArts 
Auteur : **fpowergamesretro**

- ⚠️ Manque certains systèmes mais utilisable
- ⚠️ Manque l'ensemble des systèmes virtuels arcade
- ⚠️ Bug d'affichage sur liste des jeux
- Options : couleurs, icones, menus, vues systèmes, vues gamelist,  régions

|HDMI PI/PC/OXu4|RGB Dual/JAMMA|Anbernic RG353|GPICase|OGoA|OGoS|16/9|4/3|TATE|
|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
|✅|⚠️|⚠️|❌|:grey_question:|:grey_question:|✅|:grey_question:|❌|

![8.1.1_ffart_1.jpg](/tutorials/frontend-customization/add-themes-into-emulationstation/8.1.1_ffart_1.jpg)
![8.1.1_ffart_2.jpg](/tutorials/frontend-customization/add-themes-into-emulationstation/8.1.1_ffart_2.jpg)
> Présentation et téléchargement : [Forum recalbox](https://forum.recalbox.com/topic/29668)
{.is-info}

### GAMEBOY
Auteur : **Beudbeud**  

- Theme optimisé pour ecran 4/3 mais fonctionne très bien en 16/9 
- Résolution 4/3 Yoko - 240p/480p
- Prise en charge de tous les systèmes et systèmes virtuels pour recalbox 9.2

|HDMI PI/PC/OXu4|RGB Dual/JAMMA|Anbernic RG353|GPICase|OGoA|OGoS|16/9|4/3|TATE|
|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
|⚠️|✅|✅|❌|:grey_question:|:grey_question:|❌|✅|❌|

![9.2_gameboy_1.jpg](/tutorials/frontend-customization/add-themes-into-emulationstation/9.2_gameboy_1.jpg)
![9.2_gameboy_2.jpg](/tutorials/frontend-customization/add-themes-into-emulationstation/9.2_gameboy_2.jpg)

> Présentation et téléchargement : **[Forum recalbox](https://forum.recalbox.com/topic/34372/recalbox-9-2-theme-gameboy-240p-480p/)** ou **[github](https://github.com/beudbeud/recalbox-gameboy)**
{.is-info}

### Hyperwall
Auteur : **Butch Games mis à jour par Aldébaran**

- ⚠️ Manque certains systèmes mais utilisable
- ⚠️ Manque l'ensemble des systèmes virtuels arcade
- ⚠️ Bug d'affichage sur liste des jeux
- Options : icones, vues systèmes, vues gamelist, régions

|HDMI PI/PC/OXu4|RGB Dual/JAMMA|Anbernic RG353|GPICase|OGoA|OGoS|16/9|4/3|TATE|
|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
|✅|⚠️|⚠️|❌|:grey_question:|:grey_question:|✅|:grey_question:|❌|

![8.0.1_hyperwall_1.jpg](/tutorials/frontend-customization/add-themes-into-emulationstation/8.0.1_hyperwall_1.jpg)
![8.0.1_hyperwall_2.jpg](/tutorials/frontend-customization/add-themes-into-emulationstation/8.0.1_hyperwall_2.jpg)
> Présentation et téléchargement : [Forum recalbox](https://forum.recalbox.com/topic/24597)
{.is-info}

### Metal Slide de Butch Games
Auteur : **Butch Games**

- ⚠️ Manque certains systèmes mais utilisable
- ⚠️ Manque l'ensemble des systèmes virtuels arcade
- Options : couleurs, icones, menus, playlist, vues gamelist, clips, régions

|HDMI PI/PC/OXu4|RGB Dual/JAMMA|Anbernic RG353|GPICase|OGoA|OGoS|16/9|4/3|TATE|
|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
|✅|⚠️|⚠️|❌|:grey_question:|:grey_question:|✅|:grey_question:|❌|

![8.0.1_metal_slide_1.jpg](/tutorials/frontend-customization/add-themes-into-emulationstation/8.0.1_metal_slide_1.jpg)
![8.0.1_metal_slide_20.jpg](/tutorials/frontend-customization/add-themes-into-emulationstation/8.0.1_metal_slide_20.jpg)
> Présentation et téléchargement : [Forum recalbox](https://forum.recalbox.com/topic/26314)
{.is-info}

### Midnight
Auteur : **BOUNITOS**

- Theme compatible et optimisé à la fois pour version HDMI, RGB Dual/JAMMA
- Résolution 16/9 + 4/3 - Yoko et Tate - HD/240p/480p
- Prise en charge de tous les systèmes et systèmes virtuels pour recalbox 9.2
- Option : Shaders CRT, Icones, Gameclips
- ⚠️ Thème gourmand (Resolution de l'interface en 720p conseillé sur pi4 )

|HDMI PI/PC/OXu4|RGB Dual/JAMMA|Anbernic RG353|GPICase|OGoA|OGoS|16/9|4/3|TATE|
|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
|✅|✅|✅|❌|:grey_question:|:grey_question:|✅|✅|✅|

![9.2_midnight_2.jpg](/tutorials/frontend-customization/add-themes-into-emulationstation/9.2_midnight_2.jpeg)
![9.2_midnight_1.jpg](/tutorials/frontend-customization/add-themes-into-emulationstation/9.2_midnight_1.jpg)

> Présentation et téléchargement : **[Forum recalbox](https://forum.recalbox.com/topic/32569/recalbox-9-2-theme-midnight)** ou **[Page dédiée](https://bounar.fr/Themes_es/Midnight/)** ou **[gitlab](https://gitlab.com/BenoitBounar/Theme-Midnight)**
{.is-info}


### MISTER
Auteur : **Beudbeud**  

- Theme optimisé pour ecran 4/3 mais fonctionne très bien en 16/9 
- Résolution 4/3 YOKO et TATE - 240p/480p
- Prise en charge de tous les systèmes et systèmes virtuels pour recalbox 9.2

|HDMI PI/PC/OXu4|RGB Dual/JAMMA|Anbernic RG353|GPICase|OGoA|OGoS|16/9|4/3|TATE|
|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
|⚠️|✅|✅|✅|:grey_question:|:grey_question:|❌|✅|✅|

![9.2_mister_1.jpg](/tutorials/frontend-customization/add-themes-into-emulationstation/9.2_mister_1.jpg)
![9.2_mister_2.jpg](/tutorials/frontend-customization/add-themes-into-emulationstation/9.2_mister_2.jpg)

> Présentation et téléchargement : **[Forum recalbox](https://forum.recalbox.com/topic/34364/recalbox-9-2-theme-mister-240p/)** ou **[github](https://github.com/beudbeud/recalbox-mister)**
{.is-info}

### Muskashi
Auteur : **Airdream**

- ⚠️ Manque certains systèmes mais utilisable
- ⚠️ Manque l'ensemble des systèmes virtuels arcade et genre
- Résolution 16/9 - 720p/1080p HDMI
- Options : vues systèmes

|HDMI PI/PC/OXu4|RGB Dual/JAMMA|Anbernic RG353|GPICase|OGoA|OGoS|16/9|4/3|TATE|
|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
|✅|❌|❌|❌|❌|❌|✅|❌|❌|

![9.2_mukashi_1](/tutorials/frontend-customization/add-themes-into-emulationstation/9.2_mukashi_1.jpg)
![9.2_mukashi_2](/tutorials/frontend-customization/add-themes-into-emulationstation/9.2_mukashi_2.jpg)
> Présentation et téléchargement : [Forum recalbox](https://forum.recalbox.com/topic/14323)
{.is-info}


### Next Pixel
Auteur : **mYSt**

- ⚠️ Manque certains systèmes mais utilisable
- ⚠️ Manque l'ensemble des systèmes virtuels arcade
- Ne prend pas en compte l'option des drapeaux dans les gamelist
- Résolution 4/3 - 240p/480p/768p et 16/9 - 720p/1080p 
-	Pour OGoA / OGoS prendre 16/9 480p
- Option : couleurs, icones, vues systèmes, vues gamelist, régions
-	Le thème utilise aussi des mix/scrap spécifiquements désignés pour Next Pixel (cf. documentation du thème)
- ⚠️ Thème gourmand (Resolution de l'interface en 720p conseillé sur pi4 )

|HDMI PI/PC/OXu4|RGB Dual/JAMMA|Anbernic RG353|GPICase|OGoA|OGoS|16/9|4/3|TATE|
|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
|✅|❌|❌|❌|:grey_question:|:grey_question:|✅|✅|❌|

![8.0.1_next_pixel_1.jpg](/tutorials/frontend-customization/add-themes-into-emulationstation/8.0.1_next_pixel_1.jpg)
![8.0.1_next_pixel_2.jpg](/tutorials/frontend-customization/add-themes-into-emulationstation/8.0.1_next_pixel_2.jpg)
> Présentation et téléchargement : [Forum recalbox](https://forum.recalbox.com/topic/16064)
{.is-info}


### Retro Switch
Auteur : **Dwayne Hurst adapté par Butch Games**
- ⚠️ Manque certains systèmes mais utilisable
- ⚠️ Manque l'ensemble des systèmes virtuels arcade
- ⚠️ Leger bug d'affichage du logo du système sur la liste des jeux
- Options : Couleur du thème, icones

|HDMI PI/PC/OXu4|RGB Dual/JAMMA|Anbernic RG353|GPICase|OGoA|OGoS|16/9|4/3|TATE|
|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
|✅|⚠️|⚠️|❌|:grey_question:|:grey_question:|✅|:grey_question:|❌|


![8.0.1_retro_switch_1.jpg](/tutorials/frontend-customization/add-themes-into-emulationstation/8.0.1_retro_switch_1.jpg)
![8.0.1_retro_switch_2.jpg](/tutorials/frontend-customization/add-themes-into-emulationstation/8.0.1_retro_switch_2.jpg)
> Présentation et téléchargement : [Forum recalbox](https://forum.recalbox.com/topic/24439)
{.is-info}

### Unifiedspin
Auteur : **Butch Games inspiré des thèmes Unified**
- ⚠️ Manque certains systèmes mais utilisable
- ⚠️ Manque l'ensemble des systèmes virtuels arcade
- ⚠️ Bug d'affichage sur liste des jeux
- Option : icones, vues systèmes, vues gamelist, clips, régions

|HDMI PI/PC/OXu4|RGB Dual/JAMMA|Anbernic RG353|GPICase|OGoA|OGoS|16/9|4/3|TATE|
|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
|✅|⚠️|⚠️|❌|:grey_question:|:grey_question:|✅|:grey_question:|❌|

![8.0.1_unifiedspin_10.jpg](/tutorials/frontend-customization/add-themes-into-emulationstation/8.0.1_unifiedspin_10.jpg)
![8.0.1_unifiedspin_2.jpg](/tutorials/frontend-customization/add-themes-into-emulationstation/8.0.1_unifiedspin_2.jpg)
> Présentation et téléchargement : [Forum recalbox](https://forum.recalbox.com/topic/24433)
{.is-info}

### Uniflyered
Auteur : **Dwayne Hurst adapté par Butch Games**
- ⚠️ Manque certains systèmes mais utilisable
- ⚠️ Manque l'ensemble des systèmes virtuels arcade
- Options : icones, playlist, régions

|HDMI PI/PC/OXu4|RGB Dual/JAMMA|Anbernic RG353|GPICase|OGoA|OGoS|16/9|4/3|TATE|
|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
|✅|⚠️|⚠️|❌|:grey_question:|:grey_question:|✅|:grey_question:|❌|

![8.0.1_uniflyered_1.jpg](/tutorials/frontend-customization/add-themes-into-emulationstation/8.0.1_uniflyered_1.jpg)
![8.0.1_uniflyered_2.jpg](/tutorials/frontend-customization/add-themes-into-emulationstation/8.0.1_uniflyered_2.jpg)
> Présentation et téléchargement : [Forum recalbox](https://forum.recalbox.com/topic/22527)
{.is-info}