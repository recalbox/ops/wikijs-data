---
title: Amiga 600 & 1200 & AmigaCD32 emulators
description: 
published: true
date: 2021-09-04T23:16:33.085Z
tags: amiga-cd32, amiga-1200, amiga-600, amiga
editor: markdown
dateCreated: 2021-08-08T06:12:29.179Z
---

## Subsystems

In Recalbox, the Amiga machines have been divided into 3 virtual subsystems:  

* Amiga 600: All _OCS/ECS_ machines from the A1000 to the latest A600
* Amiga 120to: All _AGA_ machines, from the 1200
* Amiga CD32 : The only real console of the Amiga series     
  * CDTV is not yet supported, although the Recalbox theme and CDTV configurations are ready. It will be added later as a new subsystem, as soon as Amiberry fully supports it.

## Supported "Roms"

Here is a list of supported file formats, sorted by subsystem:

* Amiga600 & Amiga1200:

*  * **ADF** (`*.adf`) disks, the most popular disk format (cannot incorporate disk protectors). Can be zipped or 7-zipped.
*  **IPF** (`*.ipf`) disks, the most popular format for non-intro dumps (can include disk protections). Can be zipped.
*  **WHD** (folders, `*.lha/lzh/lz`, `*.zip`), the well-known format for hard disk games. WHD folders are reconfigured using the internal `*.slave` file; however it is strongly recommended to store them in `lha` or `zip` format. Unzipped WHDs are not faster and even worse, they can make EmulationStation extremely slow.)
  * **HDD FS**, hard disk on file system. Folders must end with `.hd' extensions. Rarely used. For real Amiga fans who have backed up their Amiga HD. Can be zipped, but not recommended.
  **HDF** (`*.hdf`), Hard disk images in a single file. Can be zipped (read/write).
  * **RP9** (`*.rp9`), Amiga Forever all-in-one packages.

* **Amiga CD32** :

  * **ISO** (`.bin`, `*.iso`, with or without `*.cue`), CD dumps. Can be zipped.

Other CD image formats may be supported but have not been tested by the Recalbox team.

When running zipped/7-zipped files, our configurator tries to identify the type of rom by looking for specific extensions inside the archive. 

* `ADF/IPF/BIN/ISO` are fast to identify.
* `WHD` are fast, but LHA` should be preferred.
* `HDDFS` may take longer to check and may lead to misinterpretations. If you use `HDDFS`, leave them as normal files and folders and create the root folder name ending in `.hd` for quick identification.

## Priority settings

As you can see, this new Amiberry supports all Amiga roms formats. To make things easier for everyone, we have done a huge amount of work to automatically configure the emulator based on what you want to boot.

Recalbox builds a default machine configuration per subsystem. However, in some situations, you may want to override the automatically generated settings.

Recalbox generates configuration files in `/tmp/amiga/conf`. The most important one is the `uaeconfig.uae` file, in which you will find all the keys defining the machine, the hard disks, etc.

I can't describe all the keys here. If you are curious, I recommend that you take a look at the appropriate Amiberry and UAE documentation.

You have two ways to overwrite settings:

### Per system:

Create a file named `/recalbox/share/system/configs/amiberry/<subsystem>.uae` (`/recalbox/share/system/configs/amiberry/amiga1200.uae` to override the Amiga1200)

* Set the key you want to bypass.
* For example, you can create a configuration file with the following content to disable the LED display in the lower right corner:

```ini
show_leds=false
```

### Per set

Create a file named exactly like your "rom" (or folder) but ending with `.uae`.

For example, in your rom folder, if you have a file named `Aladdin (AGA).ipf`, you can create an `Aladdin (AGA).uae` and replace the keys in that file.

Recalbox generates the configuration in the following order:

* Create the default configuration for the rom type and subsystem.
* Load `/recalbox/share/configs/<subsystem>.uae` if the file exists, and add/overwrite the configuration keys.
* Load `<game>.uae` if the file exists, and add/overwrite the configuration keys.

## In the game

As in other emulators:

* `Hotkey + START` exit the emulator, 
* `Hotkey + B` launch the Amiberry interface.