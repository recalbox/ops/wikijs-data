---
title: GSPlus
description: 
published: true
date: 2024-07-24T21:49:17.874Z
tags: gsplus, apple-2gs
editor: markdown
dateCreated: 2021-07-29T15:48:31.638Z
---

**GSPlus** is an emulator created on the remains of KEGS and GSPort to emulate the Apple ]GS (Apple 2GS).

## ![](/emulators/license.svg) License

This core is under [**GPLv2**](https://github.com/digarok/gsplus/blob/master/LICENSE.txt) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Features



## ![](/emulators/bios.svg) BIOS

### List of mandatory bios

| Filename | Description | MD5 | Provided |
| :---: | :---: | :---: | :---: |
| apple2gs1.rom | - | 20a0334c447cb069a040ae5be1d938df | ❌ |
| apple2gs3.rom | - | ba89edf2729a28a17cd9e0f7a0ac9a39 | ❌ |

### Location

Put the bios like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 🗒 apple2gs.rom

## ![](/emulators/roms.png) Roms

### Supported extensions

Roms must have the extension:

* .2mg
* .po
* .hdv

### Location

Put the roms like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 apple2gs
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.2mg**

>The roms using **TOSEC** format are highly recommended.
{.is-success}

>For more information about roms, go to [this tutorial](./../../../tutorials/games/generalities/isos-and-roms)!
{.is-info}

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/digarok/gsplus/](https://github.com/digarok/gsplus/)
* **Documentation**: [http://apple2.gs/plus/](http://apple2.gs/plus/)