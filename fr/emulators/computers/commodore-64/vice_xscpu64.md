---
title: Vice_xscpu64
description: 
published: true
date: 2024-07-25T16:10:46.024Z
tags: commodore-64, c64, vice, xscpu64, 9.1+
editor: markdown
dateCreated: 2024-07-25T15:59:07.642Z
---



## ![](/emulators/license.svg) Licence

Ce core est sous licence [**GPLv2**](https://sourceforge.net/p/vice-emu/code/HEAD/tree/trunk/vice/COPYING).

## ![](/emulators/compatibility.png) Compatibilité

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Fonctionnalités



## ![](/emulators/bios.svg) BIOS

### Liste des bios optionnels

| Nom de fichier | Description | MD5 | Fourni |
| :---: | :---: | :---: | :---: |
| scpu-dos-1.4.bin | CMD SuperCPU Kernal 1.4 | cda2fcd2e1f0412029383e51dd472095 | ❌ |
| scpu-dos-2.04.bin | CMD SuperCPU Kernal 2.04 | b2869f8678b8b274227f35aad26ba509 | ❌ |


### Emplacement

Placez les BIOS comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 vice
┃ ┃ ┃ ┃ ┣ 📁 SCPU64
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **scpu-dos-1.4.bin**
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **scpu-dos-2.04.bin**

## ![](/emulators/roms.png) Roms

### Extensions supportées

Les roms doivent avoir les extensions suivantes :

* .bin
* .cmd
* .crt
* .d1m
* .d2m
* .d4m
* .d64
* .d6z
* .d67
* .d71
* .d7z
* .d80
* .d81
* .d82
* .d8z
* .d90
* .dhd
* .g41
* .g4z
* .g64
* .g6z
* .m3u
* .nbz
* .nib
* .p00
* .p64
* .prg
* .t64
* .tap
* .tcrt
* .vfl
* .vsf
* .x64
* .x6z
* .zip
* .7z
* .gz

Ce système supporte les roms compressées au format .zip/.7z. Attention toutefois, il ne s'agit que d'une archive.

Les fichiers contenus dans les .zip/.7z doivent correspondre aux extensions citées précédemment.  
De plus, chaque fichier .zip/.7z ne doit contenir qu'une seule rom compressée.

### Emplacement

Placez les roms comme ceci : 

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 c64
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.zip**

>Les roms au format **No-Intro** sont vivement conseillées.
{.is-success}

>Pour plus d'information sur les roms, rendez-vous sur [ce tutoriel](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/external-links.png) Liens externes

* **Code source utilisé** : [https://sourceforge.net/projects/vice-emu/](https://sourceforge.net/projects/vice-emu/)