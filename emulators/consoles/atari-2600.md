---
title: Atari 2600
description: 
published: true
date: 2025-01-05T23:39:58.690Z
tags: atari-2600, consoles, atari, 2600
editor: markdown
dateCreated: 2021-07-27T16:47:02.943Z
---

![](/emulators/consoles/atari2600.svg){.align-center}

## Technical data

* **Manufacturer**: Atari Corporation
* **Year of release**: 1977
* **Units sold**: 30 million
* **Best-selling game**: Pac-Man
* **CPU**: 8-bit MOS 6507 @ 1.19 MHz
* **RAM**: 128 bytes
* **Audio / Video Processor**: TIA
* **Resolution**: 160 x 192 pixels, 4 colors per line (128 colors palette)
* **Sound chip**: 2 channels of 1-bit monaural with 4-bit volume control
* **Cart size**: 2kB - 32kB

## Presentation

In 1976, Fairchild Camera and Instrument released the [**Channel F**](./channelf) system, the first home console with a cartridge support. A year later, followed the Studio II (created by RCA), the second home console in history... And finally, in October 1977, Atari released the **Atari VCS** (Video Computer System) with 9 games available.

In 1982, Atari accepted that a dozen of third party developers would release games on the console. Financially, Atari came out with profits but playfully, it was a disaster... Although there were still some quality titles, a lot of crap was released on the **Atari VCS**...

In 1983, more and more companies were producing games and accessories; and what had to happen happened: There were too many products for the demand and many developers closed their doors. It was a **crash for the video game world**.

In 1984, the price of the Atari 2600 dropped to less than a third of its original price. The years 1984 and 1985 were really poor as far as video games were concerned and only a few games were released, including Cosmic Commuter and Ghostbusters. However, the console still sold well because it had become very cheap and had a huge game library.

In 1986 the [**NES**](./nintendo-entertainment-system) from Nintendo was released, which was a huge success. Atari then decided to release the **Atari 2600** **Jr**, a redesigned Atari 2600.

In 1987 and 1988, some excellent games were released such as Pac-Man Jr. The **Atari VCS** was renamed some time later **Atari 2600** and dominated the console market for several years, until the early 80's.

## Emulators

[Libretro Stella](libretro-stella)
[Libretro Stella2014](libretro-stella2014)