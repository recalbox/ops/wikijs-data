---
title: Libretro 2048
description: 
published: true
date: 2024-07-25T22:49:06.888Z
tags: libretro, 2048
editor: markdown
dateCreated: 2023-09-11T14:31:47.177Z
---

Libretro 2048 es una adaptación del juego 2048.

## ![](/emulators/license.svg) Licencia

El core posee la siguiente licencia [**domaine public**](https://github.com/libretro/libretro-2048/blob/master/COPYING).

## ![](/emulators/compatibility.png) Compatibilidad

 | RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | 
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | 

## ![](/emulators/features.png) Funcionalidades

| Funcionalidad | ¿Soportada? |
| :---: | :---: |
| Reinicio | ✔ |
| Capturas de pantalla | ✔ |
| Grabaciones | ✔ |
| Grabaciones instantáneas | ✔ |
| Rebobinado | ✔ |
| Netplay | ✔ |
| Controles | ✔ |
| Remapping | ✔ |

## ![](/emulators/bios.svg) BIOS

>**No necesita bios.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Extensiones de roms soportadas

Las roms deben tener las extensiones siguientes:

* .game

### Emplazamiento

Disponed vuestras roms como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 ports
┃ ┃ ┃ ┃ ┣ 📁 2048
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **2048.game**

Cette rom est fournie dans votre Recalbox.

## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

>**Este core no tiene opciones.**
{.is-success}

## ![](/emulators/external-links.png) Enlaces externos

* **Código fuente utilizado** : [https://github.com/libretro/libretro-2048/](https://github.com/libretro/libretro-2048)
* **Código fuente oficial** : [https://github.com/gabrielecirulli/2048/](https://github.com/gabrielecirulli/2048)
* **Página web oficial** : [https://play2048.co**/**](https://play2048.co/)​