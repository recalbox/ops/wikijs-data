---
title: Useful tools list
description: 
published: true
date: 2021-08-06T23:41:41.899Z
tags: useful, tools, list
editor: markdown
dateCreated: 2021-08-06T23:41:41.899Z
---

Before starting the Recalbox adventure, here is a list of utilities you should have on your PC that can help you.

>MacOS and Linux users, if you have equivalents, edit this page to add them.
{.is-info}

## MicroSD formatting utilities

* [SDFormatter](https://www.sdcard.org/downloads/formatter_4/) : software to format SD/SDHC/SDXC.
* [Minitool Partition wizard free](http://www.partitionwizard.com/free-partition-manager.html) : software to format in different formats.

## Utility to flash an image 

* **(Recommended)** [Raspberry Pi Imager](https://www.raspberrypi.org/software/) : tool available on all systems (Windows, Linux, macOS, ...) allowing to flash an `.img` file on storage devices (microsd card/hard disk).
* [WIN32 Disk Imager](https://sourceforge.net/projects/win32diskimager/) : allows you to create an image of your microSD.
* Balena [Etcher](https://etcher.io/): cross-platform tool to flash an `.img` file on storage devices (microsd card/hard disk).

## Utilities to decompress your archives

* [7zip](http://www.7-zip.org/download.html) : ideal to decompress your romsets in 7zip.001, 7zip.002, etc.

## Utilities to manage your Recalbox remotely

## {.tabset}
## Windows
* [MobaXTerm](https://mobaxterm.mobatek.net/)
* [WINSCP](http://winscp.net/)
* [Putty](https://www.chiark.greenend.org.uk/~sgtatham/putty/latest.html) 
* [Notepad++](https://notepad-plus-plus.org/downloads/)
\
See [this category](./../system/access) to configure these utilities.

### macOS
* **Terminal** provided by default (Applications > Utilities).
* **TextEdit** provided by default.

### Linux
* **Terminal** provided by default.

## Utility to check the digital signatures of bios

## {.tabset}
### Windows
* [winmd5free](http://www.winmd5.com/)
* [md5sum](http://esrg.sourceforge.net/utils_win_up/md5sum/)

### macOS
* Command **md5sum** via terminal.

### Linux
* Command **md5sum** via terminal.

## Utilities to manage your roms locally on your PC

* [Universal XML Editor - Simply edit your xml files](https://github.com/Universal-Rom-Tools/Universal-XML-Editor/releases) (local) 
* [Universal ROM Cleaner - Clean your romset with one click](https://forum.recalbox.com/topic/2344/soft-universal-rom-cleaner-nettoyez-vos-romset-d-une-main-clean-your-romset-with-one-hand) (local)

## Utilities to manage CD romsets (iso, img, bin)

For Sega CD, PC-Engine CD and PSX.

* [Daemon Tools Lite](https://www.daemon-tools.cc/fra/products/dtLite) : Mount CD images on your PC ; sound [tutorial](./../games/guides/playstation-1/convert-disc-files-to-bin+cue)
* [IMGBurn](https://www.imgburn.com/) : Allows you to create an image in bin+cue format ; sound [tutorial](./../games/guides/playstation-1/convert-disc-files-to-bin+cue)
* [Monkey Audio](http://www.monkeysaudio.com/download.html) : Allows to decompress ape files in wav.
* [emctools](https://app.box.com/s/l8x54nof3k53myk6yueaz2d1p02x8d5t) : Allows to decompress bin.ec format.
* [psx2psp](https://github.com/recalbox/recalbox-os/wiki/Gestion-multidisc-PSX) : Allows to merge several CDs.
* [SegaCD Cue Maker](http://www.racketboy.com/downloads/SegaCueMaker.zip) : Allows you to create missing cues for your Sega CD bin files only.

## Utility to rename the internal name of the rom

* [N64 Rom Renamer](https://www.romhacking.net/utilities/791/) **:** By Einstein II.