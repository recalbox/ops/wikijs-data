---
title: Dolphin
description: 
published: true
date: 2024-06-23T00:09:53.964Z
tags: wii, dolphin
editor: markdown
dateCreated: 2021-05-21T08:28:13.045Z
---

Dolphin est un émulateur pour deux récentes consoles de jeu de Nintendo : la GameCube et la Wii. Il permet aux joueurs sur PC d'apprécier les jeux réalisés pour ces deux consoles en **full HD** (1080p) avec différentes améliorations : compatibilité avec tous les contrôleurs de PC, vitesse turbo, jeu en ligne en réseau, et bien plus encore !

Plus de 200 personnes ont travaillé dur depuis des années pour créer Dolphin. La liste des contributeurs peut être trouvée [sur GitHub](https://github.com/dolphin-emu/dolphin/graphs/contributors).

## ![](/emulators/license.svg) Licence

Ce core est sous licence [**GPLv2**](https://github.com/dolphin-emu/dolphin/blob/master/license.txt).

## ![](/emulators/compatibility.png) Compatibilité

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ❌ | ❌ | ❌ | ❌ | ❌ | ❌ | ✅ | ❌ |

## ![](/emulators/features.png) Fonctionnalités

| Fontionnalité | Supporté |
| :---: | :---: |
| Netplay | ✔ |

## ![](/emulators/bios.svg) BIOS

>**Aucun bios n'est requis.**
{.is-success}

## ![](/emulators/isos.svg) Isos

### Extensions supportées

Les isos doivent avoir les extensions suivantes :

* .ciso
* .iso
* .rvz
* .wbfs

### Emplacement

Placez les isos comme ceci : 

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 wii
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.iso**

>Les isos au format **Redump** sont vivement conseillés.
{.is-success}

>Pour plus d'information sur les isos, rendez-vous sur [ce tutoriel](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

Dolphin fonctionne avec les roms dans les formats suivants :

* **GCM**/**ISO** (dumps non compressés ~4,7 GB pour un jeu de Wii)
* **WBFS** (non pris en charge par Dolphin 3.0 ou plus ancien)
* **WAD** (mini jeux wiiware)

>Notez que WBFS et CISO sont des formats de compression à perte : ils retirent des informations non utilisées sur les disques pour les rendre plus petits. Ainsi nous vous recommandons d'essayer de dumper vos jeux au format GCM/ISO si vous avez des problèmes avec un dump WBFS.
{.is-warning}

## ![](/emulators/advanced-configuration.png) Configuration avancée des émulateurs

>Pour pouvoir conserver vos configurations personnalisées lors d'une mise à jour, nous vous conseillons d'utiliser notre fonctionnalité [Surcharges de configuration](./../../../advanced-usage/configuration-override).
{.is-info}

### Options de Dolphin

## ![](/emulators/external-links.png) Liens externes

* **Code source utilisé** : [https://github.com/dolphin-emu/dolphin/](https://github.com/dolphin-emu/dolphin)
* **Site officiel** : [https://www.dolphin-emu.org/](https://www.dolphin-emu.org/)