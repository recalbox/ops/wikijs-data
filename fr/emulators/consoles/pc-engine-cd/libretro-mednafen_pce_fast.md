---
title: Libretro Mednafen_PCE_FAST
description: 
published: true
date: 2024-07-24T15:43:54.054Z
tags: libretro, mednafen, pce fast, pc-engine cd
editor: markdown
dateCreated: 2021-05-21T08:25:02.738Z
---

**Libretro Mednafen_PCE_FAST** est un port libretro de Mednafen PCE Fast avec le module PC Engine SuperGrafx retiré.

## ![](/emulators/license.svg) Licence

Ce core est sous licence [**GPLv2**](https://github.com/libretro/beetle-pce-fast-libretro/blob/master/COPYING).

## ![](/emulators/compatibility.png) Compatibilité

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Fonctionnalités

| Fonctionnalité | Supporté |
| :---: | :---: |
| Redémarrage | ✔ |
| Sauvegardes | ✔ |
| Sauvegardes instantanées | ✔ |
| Rembobinage | ✔ |
| Netplay | ✔ |
| Options du core | ✔ |
| RetroAchievements | ✔ |
| Controls | ✔ |
| Remapping | ✔ |

## ![](/emulators/bios.svg) BIOS

### Liste des bios obligatoires

| Nom de fichier | Description | MD5 | Fourni |
| :---: | :---: | :---: | :---: |
| syscard3.pce | PC-Engine Super CD-ROM^2 System Card v3.0 | ff1a674273fe3540ccef576376407d1d 1e47a1780aaa7e277a3896c1ba00e317 38179df8f4ac870017db21ebcbf53114 | ❌ |

### Emplacement

Placez les BIOS comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 pcenginecd
┃ ┃ ┃ ┃ ┣ 🗒 **syscard3.pce**

### Liste des bios optionnels

| Nom de fichier | Description | MD5 | Fourni |
| :---: | :---: | :---: | :---: |
| syscard1.pce | PC-Engine CD-ROM System Card v1.0 | 9f770275393b8627cf9d24e5c56d2ab9 | ❌ |
| syscard2.pce | PC-Engine CD-ROM^2 System Card v2.0 | 424f1d6bf93259bf255afa7d1dc9f721 | ❌ |
| syscard2u.pce | TurboGrafx-CD System Card | 94279f315e8b52904f65ab3108542afe | ❌ |
| syscard3u.pce | TurboGrafx-CD Super System Card | 0754f903b52e3b3342202bdafb13efa5 | ❌ |
| gexpress.pce | Game Express CD Card | 6d2cb14fc3e1f65ceb135633d1694122 | ❌ |

### Emplacement

Placez les BIOS comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 pcenginecd
┃ ┃ ┃ ┃ ┣ 🗒 **syscard1.pce**
┃ ┃ ┃ ┃ ┣ 🗒 **syscard2.pce**
┃ ┃ ┃ ┃ ┣ 🗒 **syscard2u.pce**
┃ ┃ ┃ ┃ ┣ 🗒 **syscard3u.pce**
┃ ┃ ┃ ┣ 📁 pcengine
┃ ┃ ┃ ┃ ┣ 🗒 **gexpress.pce**

## ![](/emulators/isos.svg) Isos

### Extensions supportées

Les isos doivent avoir l'extension suivante :

* .cue/.bin
* .ccd/.img
* .chd
* .m3u
* .toc

### Emplacement

Placez les isos comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 pcenginecd
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.cue**
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.bin**

>Les isos au format **Redump** sont vivement conseillés.
{.is-success}

>Pour plus d'information sur les isos, rendez-vous sur [ce tutoriel](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuration avancée de l'émulateur

>Pour pouvoir conserver vos configurations personnalisées lors d'une mise à jour, nous vous conseillons d'utiliser notre fonctionnalité [Surcharges de configuration](./../../../advanced-usage/configuration-override).
{.is-info}

### Accéder aux options

Vous pouvez configurer diverses options de deux façons différentes.

* Via le menu RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via le fichier `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Options du core

| Options | Valeur des options | Variable (pour surcharge) | Valeurs des variables |
| --- | --- | --- | --- |
| Palette de couleurs | `RVB` ✅ / `Composite` | `pce_color_palette` | `RGB` / `Composite` |
| Saut d'iamges | `Désactivé` ✅ / `Auto` / `Manuel` | `pce_fast_frameskip` | `disabled` / `auto` / `manual` |
| Seuil de saut d'images (%) | `15` / `18` / `21` / `24` / `27` / `30` / `33 (par défaut)` ✅ / `36` / `39` / `42` / `45` / `48` / `51` / `54` / `57` / `60` | `pce_fast_frameskip_threshold` | `15` / `18` / `21` / `24` / `27` / `30` / `33` / `36` / `39` / `42` / `45` / `48` / `51` / `54` / `57` / `60` |
| Surbalayage horizontal (Mode largeur 352 uniquement) | `300` / `302` / `304` / `306` / `308` / `310` / `312` / `314` / `316` / `318` / `320` / `322` / `324` / `326` / `328` / `330` / `332` / `334` / `336` / `338` / `340` / `342` / `344` / `346` / `348` / `350` / `352 (par défaut)` ✅ | `pce_hoverscan` | `300` / `302` / `304` / `306` / `308` / `310` / `312` / `314` / `316` / `318` / `320` / `322` / `324` / `326` / `328` / `330` / `332` / `334` / `336` / `338` / `340` / `342` / `344` / `346` / `348` / `350` / `352` |
| Ligne de balayage initiale | `0` / `1` / `2` / `3 (par défaut)` ✅ / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `21` / `22` / `23` / `24` / `25` / `26` / `27` / `28` / `29` / `30` / `31` / `32` / `33` / `34` / `35` / `36` / `37` / `38` / `39` / `40` | `pce_initial_scanline` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `21` / `22` / `23` / `24` / `25` / `26` / `27` / `28` / `29` / `30` / `31` / `32` / `33` / `34` / `35` / `36` / `37` / `38` / `39` / `40` |
| Dernière ligne de balayage | `208` / `209` / `210` / `211` / `212` / `213` / `214` / `215` / `216` / `217` / `218` / `219` / `220` / `221` / `222` / `223` / `224` / `225` / `226` / `227` / `228` / `229` / `230` / `231` / `232` / `233` / `234` / `235` / `236` / `237` / `238` / `239` / `240` / `241` / `242 (par défaut)` ✅ | `pce_last_scanline` | `208` / `209` / `210` / `211` / `212` / `213` / `214` / `215` / `216` / `217` / `218` / `219` / `220` / `221` / `222` / `223` / `224` / `225` / `226` / `227` / `228` / `229` / `230` / `231` / `232` / `233` / `234` / `235` / `236` / `237` / `238` / `239` / `240` / `241` / `242` |
| Volume du canal audio PSG 0 (%) | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `pce_fast_sound_channel_0_volume` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
| Volume du canal audio PSG 1 (%) | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `pce_fast_sound_channel_1_volume` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
| Volume du canal audio PSG 2 (%) | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `pce_fast_sound_channel_2_volume` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
| Volume du canal audio PSG 3 (%) | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `pce_fast_sound_channel_3_volume` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
| Volume du canal audio PSG 4 (%) | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `pce_fast_sound_channel_4_volume` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
| Volume du canal audio PSG 5 (%) | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `pce_fast_sound_channel_5_volume` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
| Sensibilité de la souris | `0.25` / `0.50` / `0.75` / `1.00` / `1.25` ✅ / `1.50` / `1.75` / `2.00` / `2.25` / `2.50` / `2.75` / `3.00` / `3.25` / `3.50` / `3.75` / `4.00` / `4.25` / `4.50` / `4.75` / `5.00` | `pce_mouse_sensitivity` | `0.25` / `0.50` / `0.75` / `1.00` / `1.25` / `1.50` / `1.75` / `2.00` / `2.25` / `2.50` / `2.75` / `3.00` / `3.25` / `3.50` / `3.75` / `4.00` / `4.25` / `4.50` / `4.75` / `5.00` |
| Désactiver la réinitialisation logicielle (RUN+SELECT) | `Désactivé` ✅ / `Activé` | `pce_disable_softreset` | `disabled` / `enabled` |
| Type de manette par défaut pour le joueur 1 | `2 touches` ✅ / `6 touches` | `pce_fast_default_joypad_type_p1` | `2 Buttons` / `6 Buttons` |
| Type de manette par défaut pour le joueur 2 | `2 touches` ✅ / `6 touches` | `pce_fast_default_joypad_type_p2` | `2 Buttons` / `6 Buttons` |
| Type de manette par défaut pour le joueur 3 | `2 touches` ✅ / `6 touches` | `pce_fast_default_joypad_type_p3` | `2 Buttons` / `6 Buttons` |
| Type de manette par défaut pour le joueur 4 | `2 touches` ✅ / `6 touches` | `pce_fast_default_joypad_type_p4` | `2 Buttons` / `6 Buttons` |
| Type de manette par défaut pour le joueur 5 | `2 touches` ✅ / `6 touches` | `pce_fast_default_joypad_type_p5` | `2 Buttons` / `6 Buttons` |
| Activer/désactiver le turbo | `Désactivé` ✅ / `Activé` | `pce_turbo_toggling` | `disabled` / `enabled` |
| Touche de raccourci turbo alternative | `Désactivé` ✅ / `Activé` | `pce_turbo_toggle_hotkey` | `disabled` / `enabled` |
| Délai du turbo | `1` / `2` / `3 (par défaut)` ✅ / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` | `pce_turbo_delay` | `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` |
| Mise en cache des images CD (Redémarrage requis) | `Désactivé` ✅ / `Activé` | `pce_fast_cdimagecache` | `disabled` / `enabled` |
| BIOS CD (Redémarrage requis) | `Games Express` / `System Card 1` / `System Card 2` / `System Card 3` ✅ / `System Card 2 US` / `System Card 3 US` | `pce_fast_cdbios` | `Games Express` / `System Card 1` / `System Card 2` / `System Card 3` / `System Card 2 US` / `System Card 3 US` |
| Vitesse du CD | `x1` ✅ / `x2` / `x4` / `x8` | `pce_cdspeed` | `1` / `2` / `4` / `8` |
| Volume ADPCM (%) | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ / `110` / `120` / `130` / `140` / `150` / `160` / `170` / `180` / `190` / `200` | `pce_adpcmvolume` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` / `110` / `120` / `130` / `140` / `150` / `160` / `170` / `180` / `190` / `200` |
| Volume CDDA (%) | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ / `110` / `120` / `130` / `140` / `150` / `160` / `170` / `180` / `190` / `200` | `pce_cddavolume` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` / `110` / `120` / `130` / `140` / `150` / `160` / `170` / `180` / `190` / `200` |
| Volume PSG (%) | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ / `110` / `120` / `130` / `140` / `150` / `160` / `170` / `180` / `190` / `200` | `pce_cdpsgvolume` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` / `110` / `120` / `130` / `140` / `150` / `160` / `170` / `180` / `190` / `200` |
| Aucune limite de sprites | `Désactivé` ✅ / `Activé` | `pce_nospritelimit` | `disabled` / `enabled` |
| Multiplicateur d'overclocking du processeur (Redémarrage requis) | `1` ✅ / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `20` / `30` / `40` / `50` | `pce_ocmultiplier` | `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `20` / `30` / `40` / `50` |

## ![](/emulators/external-links.png) Liens externes

* **Code source utilisé** : [https://github.com/libretro/beetle-pce-fast-libretro/](https://github.com/libretro/beetle-pce-fast-libretro/)
* **Documentation Libretro** : [https://docs.libretro.com/library/beetle_pce_fast/](https://docs.libretro.com/library/beetle_pce_fast/)