---
title: FAQ
description: 
published: true
date: 2023-12-21T07:34:49.979Z
tags: ps1, faq
editor: markdown
dateCreated: 2023-09-21T12:44:30.245Z
---

## Multi-disco

En primer lugar, veamos las distintas posibilidades que ofrece Recalbox para cambiar de disco.

### Modo PSP

Agrupa todos los discos en un único archivo eboot.pbp. Podéis cambiar los discos desde el menú de Retroarch.

#### Ventajas:

* Se pueden usar atajos del mando para cambiar los discos.
* Se puede crear una grabación que se comparte entre todos los discos.

#### Desventajas:

* Los ficheros `.SBI` no se pueden usar en los juegos multidisco.
* No permite cambiar a un disco parcheado (con corrección de protección LibCrypt).
* Lleva un cierto tiempo cambiar todos los juegos multi-discos al format `.PBP`.

### Modo PCSX-reARMed

El disco "siguiente" se tiene que buscar desde el menú Retroarch a través del sistema de ficheros.

#### Ventajas:

* Vuestros juegos están en formato `.BIN` / `.CUE`.

#### Desventajas:

* De momento no funciona.
* Es feo y lento.
* Los ficheros grabados no son compartidos entre los diferentes discos.
* No se pueden usar los atajos del mando para cambiar de disco.

Como resultado, los juegos con LibCrypt y protección multi-disco son un verdadero dolor de cabeza en estos dos modos.

### Modo definitivo

Basta con crear un fichero `.M3U` para cada juego multi-disco !. Este fichero contiene los nombres de varios ficheros. Por ejemplo:

![M3U](/tutorials/games/guides/playstation-1/m3u1.png)

Al final con el nuevo fichero `.M3U` obtendríamos algo así:

![Dossier PSX](/tutorials/games/guides/playstation-1/m3u2.png)

Como podéis observar en la imagen anterior, hay varios ficheros `.CUE` / `.BIN` / `.SBI` por disco y un sólo fichero `.M3U`, para el juego Final Fantasy IX.

#### Ventajas :

* Vuestro juegos permanecen en el formato de origen.
* Se pueden utilizar los atajos del mando para cambiar de disco.
* Se pueden utilizar archivos "SBI".
* Sólo hay un único fichero de grabación para todos los diferentes discos.

#### Desventajas :

* No hay ninguna, sólo venjatas.

## Juegos multipista

Algunos juegos tienen varias pistas por disco. Esto significa que el archivo `.CUE` posee varias entradas (una por pista).

Ejemplo de un fichero `.CUE` multipista:

![Cue à pistes multiples](/tutorials/games/guides/playstation-1/cue1.png)

Al final deberíamos tener lo siguiente:

![Jeu multi-pistes](/tutorials/games/guides/playstation-1/cue2.png)

## Extensiones

### Extensión `.BIN` : caso particular

Los ficheros de rom con la extensión `.BIN` también son compatibles, pero requieren un archivo `.CUE` para iniciarse.

### ¿Cómo funciona la extensión `.CUE`?

Un fichero `.CUE` es, simplemente, un fichero de texto cuya extensión ha cambiado de `.TXT` a `.CUE` . Debe tener el mismo nombre que el fichero `.BIN` al que acompaña y contiene la información necesaria para lanzar el fichero `.BIN`.

### Extensión `.M3U`: gestión de juegos multi-CD

Se pueden cargar simultáneamente varios CDs del mismo juego desde RetroArch creando un archivo de « lista de reproducción » `.M3U` (texto plano `.TXT` con extensión `.M3U`).

>Una lista de reproducción .M3U puede utilizarse con cualquiera de las extensiones de fichero utilizadas en los juegos multi-disco, sin importar si se trata de un juego de cartucho o de CD.
{.is-warning}

## Pasos para gestionar un juego multi-CD

Debéis crear manualmente un nuevo fichero de texto llamado `<nombre_del_juego.m3u`.

>Tenéis dos ejemplo aquí abajo que utilizan las extensiones `.BIN` / `.CUE` y `.CHD`.
{.is-info}

### Ejemplo con Final Fantasy VII en `.CUE`

Creamos el fichero `Final Fantasy (France).m3u` con el siguiente contenido:

```
Final Fantasy VII (France) (Disc 1).cue
Final Fantasy VII (France) (Disc 2).cue
Final Fantasy VII (France) (Disc 3).cue
```

### Ejemplo con Final Fantasy VII en `.CHD`

Creamos el fichero `Final Fantasy (France).m3u` con el siguiente contenido:

```
Final Fantasy VII (France) (Disc 1).chd
Final Fantasy VII (France) (Disc 2).chd
Final Fantasy VII (France) (Disc 3).chd
```

## Cambiar de disco durante una partida

Si necesitáis cambiar el disco mientras el juego está en curso, haced lo siguiente:

1. Abrid la bandeja de CD "virtual": `Botón HK + Joystick izquierdo (Arriba)`.
2. Cambiad el disco: `Tecla HK + Joystick izquierdo (Dirección derecha o izquierda)`.
3. Cerrad la bandeja de CD "virtual": `Tecla HK + Joystick izquierdo (Arriba)`

Esto funciona si tienes tu milti-disc configurado en un archivo `.M3U`.

## Preguntas variadas

### ¿Qué debo hacer con los ficheros `.ECM` y `.APE`?

Hay que descomprimirlos. Consultad esta [guía](https://www.epforums.org/showthread.php?57757-ECM-And-APE-Guide) para hacerlo.

### ¿Cómo activo el Dualshock (análogos)?

* En el juego, id al menú RetroArch pulsando `Hotkey` + `B`.
* Seleccionad `Keys` > `Port 1 Keys`.

![](/tutorials/games/guides/playstation-1/enable-analog/psxanalog1.png){.full-width}

* Seleccionad `analog` en la opción `Type de périphérique` (se activa en todos vuestros mandos).

![](/tutorials/games/guides/playstation-1/enable-analog/psxanalog2.png){.full-width}

### No tengo el archivo `.CUE`, ¿qué hago?

 Si sólo tenéis un fichero `.BIN` y no el .`CUE`, podéis generarlo de varias maneras:

* Online: [http://nielsbuus.dk/pg/psx_cue_maker/](http://nielsbuus.dk/pg/psx_cue_maker/)
* Con este programa gratuito: [CueMaker](https://github.com/thorst/CueMaker/releases/)

También podéis obtenerlo [del proyecto Redump](http://redump.org/cues/psx/) buscando vuestro juego en cuestión.

## Glosario

* .ape: fichero comprimido para el arcficherohivo .wav.
* .bin: datos de juegos y pistas de música.
* .ccd/.img/.sub: clonación de ficheros de CD.
* .cue: fichero en el que se definen las pistas del disco. Uno por cada dato .bin.
* .ecm: fichero comprimido para el archivo .bin.
* .pbp: fichero PSP para juegos PSX.
* .ppf: fichero de parche para juegos con protección LibCrypt.
* .sbi: fichero que contiene la información de protección necesaria para ejecutar juegos protegidos en emuladores. Deben tener el mismo nombre que el fichero .cue.
* .wav: fichero de pista de música. Debes cambiarle el nombre a .bin.

## Enlaces útiles

* [Guide ECM-APE](https://www.epforums.org/showthread.php?57757-ECM-And-APE-Guide)
* [Liste des jeux PSX](https://psxdatacenter.com/pal_list.html)
* [Fichiers .sbi](http://psxdatacenter.com/sbifiles.html)
* [Générateur de fichier .cue](http://nielsbuus.dk/pg/psx_cue_maker/)