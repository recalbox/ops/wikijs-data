---
title: Generalidades
description: 
published: true
date: 2024-07-26T06:55:16.442Z
tags: generalidades
editor: markdown
dateCreated: 2023-09-07T17:18:23.705Z
---

En esta sección se describen algunos puntos generales relaciondos con los juegos y los sistemas que los ejecutan.

Aquí tenéis las categorías disponibles:

[Las roms y las isos](isos-and-roms)

Aquí tenéis los tutoriales disponibles:

[Configuration de shaders](shaders-configuration)
[Dónde encontrar roms 100% legales](where-to-find-100-legal-roms)
[Etiquetas (tags) utilizadas en los nombres de las roms](tags-used-in-rom-names)
[Gestión de juegos multi-discos con el formato .M3U](multidisc-management-with-m3u)