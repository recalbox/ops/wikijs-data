---
title: Libretro FBNeo
description: 
published: true
date: 2021-10-05T07:35:04.508Z
tags: genesis, libretro, fbneo
editor: markdown
dateCreated: 2021-07-30T22:42:25.063Z
---



## ![](/emulators/license.svg) License

This core is under [**non-commercial**](https://github.com/finalburnneo/FBNeo/blob/master/src/license.txt) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Features

| Feature | Supported |
| :---: | :---: |
| Saves | ✔ |
| States | ✔ |
| Rewind | ✔ |
| Netplay | ✔ |
| RetroAchievements | ✔ |
| RetroArch Cheats | ✔ |
| Native Cheats | ✔ |
| Controllers | ✔ |
| Multi-Mouse | ✔ |
| Subsystem | ✔ |

## ![](/emulators/bios.svg) BIOS

>**No bios is required.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Supported extensions

Roms must have the extension:

* .zip

### Location

Put the roms like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 megadrive
┃ ┃ ┃ ┃ ┣ 🗒 **game.zip**

>For more information about roms, go to [this tutorial](./../../../tutorials/games/generalities/isos-and-roms)!
{.is-info}

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Accessing to options

You can configure various options in two different ways.

* Via RetroArch menu:

┣ 📁 RetroArch Menu
┃ ┣ 📁 Core options
┃ ┃ ┣ 🧩 Name_option

* Via the file `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core options

| Options | Options values | Variables (for configuration override) | Variables values |
| --- | --- | --- | --- |
| Use 32-bits color depth when available | `Disabled` / `Enabled` ✅ / | `fbneo-allow-depth-32` | `disabled` / `enabled` |
| Vertical mode | `Disabled` / `Enabled` / `alternate` | `fbneo-vertical-mode` | `disabled` / `enabled` |
| Allow patched romsets | `Disabled` / `Enabled` ✅ | `fbneo-allow-patched-romsets` | `disabled` / `enabled` |
| Analog Speed | `25%` / `30%` / `35%` / `40%` / `45%` / `50%` / `55%` / `60%` / `65%` / `70%` / `75%` / `80%` / `85%` / `90%` / `95%` / `100%` ✅ / `105%` / `110%` / `115%` / `120%` / `125%` / `130%` / `135%` / `140%` / `145%` / `150%` / `155%` / `160%` / `165%` / `170%` / `175%` / `180%` / `185%` / `190%` / `195%` / `200%` / `205%` / `210%` / `215%` / `220%` / `225%` / `230%` / `235%` / `240%` / `245%` / `250%` / `255%` / `260%` / `265%` / `270%` / `275%` / `280%` / `285%` / `290%` / `295%` / `300%` / `305%` / `310%` / `315%` / `320%` / `325%` / `330%` / `335%` / `340%` / `345%` / `350%` / `355%` / `360%` / `365%` / `370%` / `375%` / `380%` / `385%` / `390%` / `395%` | `fbneo-analog-speed` | `25%` / `30%` / `35%` / `40%` / `45%` / `50%` / `55%` / `60%` / `65%` / `70%` / `75%` / `80%` / `85%` / `90%` / `95%` / `100%` / `105%` / `110%` / `115%` / `120%` / `125%` / `130%` / `135%` / `140%` / `145%` / `150%` / `155%` / `160%` / `165%` / `170%` / `175%` / `180%` / `185%` / `190%` / `195%` / `200%` / `205%` / `210%` / `215%` / `220%` / `225%` / `230%` / `235%` / `240%` / `245%` / `250%` / `255%` / `260%` / `265%` / `270%` / `275%` / `280%` / `285%` / `290%` / `295%` / `300%` / `305%` / `310%` / `315%` / `320%` / `325%` / `330%` / `335%` / `340%` / `345%` / `350%` / `355%` / `360%` / `365%` / `370%` / `375%` / `380%` / `385%` / `390%` / `395%` |
| No crosshair with lightgun device | `Disabled` / `Enabled` ✅ | `fbneo-lightgun-hide-crosshair` | `disabled` / `enabled` |
| Enable cyclone | `Disabled` ✅ / `Enabled` | `fbneo-cyclone` | `disabled` / `enabled` |
| CPU clock | `25%` / `30%` / `35%` / `40%` / `45%` / `50%` / `55%` / `60%` / `65%` / `70%` / `75%` / `80%` / `85%` / `90%` / `95%` / `100%` ✅ / `105%` / `110%` / `115%` / `120%` / `125%` / `130%` / `135%` / `140%` / `145%` / `150%` / `155%` / `160%` / `165%` / `170%` / `175%` / `180%` / `185%` / `190%` / `195%` / `200%` / `205%` / `210%` / `215%` / `220%` / `225%` / `230%` / `235%` / `240%` / `245%` / `250%` / `255%` / `260%` / `265%` / `270%` / `275%` / `280%` / `285%` / `290%` / `295%` / `300%` / `305%` / `310%` / `315%` / `320%` / `325%` / `330%` / `335%` / `340%` / `345%` / `350%` / `355%` / `360%` / `365%` / `370%` / `375%` / `380%` / `385%` / `390%` / `395%` | `fbneo-cpu-speed-adjust` | `25%` / `30%` / `35%` / `40%` / `45%` / `50%` / `55%` / `60%` / `65%` / `70%` / `75%` / `80%` / `85%` / `90%` / `95%` / `100%` / `105%` / `110%` / `115%` / `120%` / `125%` / `130%` / `135%` / `140%` / `145%` / `150%` / `155%` / `160%` / `165%` / `170%` / `175%` / `180%` / `185%` / `190%` / `195%` / `200%` / `205%` / `210%` / `215%` / `220%` / `225%` / `230%` / `235%` / `240%` / `245%` / `250%` / `255%` / `260%` / `265%` / `270%` / `275%` / `280%` / `285%` / `290%` / `295%` / `300%` / `305%` / `310%` / `315%` / `320%` / `325%` / `330%` / `335%` / `340%` / `345%` / `350%` / `355%` / `360%` / `365%` / `370%` / `375%` / `380%` / `385%` / `390%` / `395%` |
| Frameskip | `Disabled` ✅ / `Fixed` / `Auto` / `Manual` | `fbneo-frameskip-type` | `disabled` / `Fixed` / `Auto` / `Manual` |
| Frameskip Threshold (%) | `15` / `18` / `21` / `24` / `27` / `30` / `33` ✅ / `36` / `39` / `42` / `45` / `48` / `51` / `54` / `57` / `60` | `fbneo-frameskip-manual-threshold` | `15` / `18` / `21` / `24` / `27` / `30` / `33` / `36` / `39` / `42` / `45` / `48` / `51` / `54` / `57` / `60` |
| Fixed Frameskip | `No skipping` ✅ / `Skip rendering of 1 frames out of 2` / `Skip rendering of 2 frames out of 3` / `Skip rendering of 3 frames out of 4` / `Skip rendering of 4 frames out of 5` / `Skip rendering of 5 frames out of 6` | `fbneo-fixed-frameskip` | `0` / `1` / `2` / `3` / `4` / `5` |
| Samplerate | `44100` / `48000` ✅ | `fbneo-samplerate` | `44100` / `48000` |
| Sample Interpolation | `Disabled` / `2-point 1st order` / `4-point 3rd order` ✅ | `fbneo-sample-interpolation` | `disabled` / `2-point 1st order` / `4-point 3rd order` |
| FM Interpolation | `Disabled` / `4-point 3rd order` ✅ | `fbneo-fm-interpolation` | `disabled` / `4-point 3rd order` |
| LowPass Filter | `Disabled` ✅ / `Enabled` | `fbneo-lowpass-filter` | `disabled` / `enabled` |

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/finalburnneo/FBNeo/](https://github.com/finalburnneo/FBNeo/)
* **Libretro documentation**: [https://docs.libretro.com/library/fbneo/](https://docs.libretro.com/library/fbneo/)