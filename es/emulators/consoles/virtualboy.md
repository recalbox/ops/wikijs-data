---
title: Virtual Boy
description: Nintendo Virtual Boy
published: true
date: 2024-07-26T08:07:14.410Z
tags: nintendo, virtual boy, consolas
editor: markdown
dateCreated: 2023-09-03T20:31:55.128Z
---

![](/emulators/consoles/virtualboy.svg){.align-center}

## Detalles técnicos

* **Fabricante**: Nintendo
* **Año de lanzamiento**: 1995
* **Cantidad vendida**: 800.000
* **Juego más vendido**: VB Wario Land
* **Procesador**: 32Bit NEC V810 RISC @ 20MHz
* **RAM**: 1Mbits DRAM 512kb P-SRAM
* **Color**: 4 colores / 32 niveles de intensidad
* **Resolución**: 384 x 224 píxeles
* **Pantalla**: 2 LED luminosos RTI
* **Sonido**: 16 bits estéreo

## Presentación

El **Virtual Boy** es una videoconsola creada por Nintendo y lanzada en 1995 en Japón y Estados Unidos, en forma de visiocasco 3D.

Intentando innovar con una pantalla estereoscópica, la consola fue finalmente un fracaso comercial. A pesar de varias reducciones de precio, las ventas no despegaron y se canceló el lanzamiento de la consola en Europa. Nintendo abandonó el proyecto en 1996.

Con 800.000 unidades vendidas, la Virtual Boy fue la consola menos vendida en la historia de las videoconsolas de Nintendo.

Sin embargo, el mando fue muy popular entre los usuarios por su diseño ergonómico y, a pesar de ello, resultaba muy agradable de usar.

## Emuladores

[Libretro Mednafen_VB](libretro-mednafen_vb)