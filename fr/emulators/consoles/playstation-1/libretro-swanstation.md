---
title: Libretro Swanstation
description: 
published: true
date: 2025-02-07T20:36:58.448Z
tags: libretro, ps1, playstation 1, psx, swanstation
editor: markdown
dateCreated: 2021-05-21T08:25:45.876Z
---



## ![](/emulators/license.svg) Licence

Ce core est sous licence [**GPLv3**](https://github.com/libretro/swanstation/blob/master/LICENSE).

## ![](/emulators/compatibility.png) Compatibilité

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ❌ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Fonctionnalités

| Fonctionnalité | Supporté |
| :---: | :---: |
|  |  |

## ![](/emulators/bios.svg) BIOS

### Liste des bios obligatoires

| Nom de fichier | Description | MD5 | Fourni |
| :---: | :---: | :---: | :---: |
| scph5500.bin | BIOS PS1 japonais - requis pour les jeux japonais | 8dd7d5296a650fac7319bce665a6a53c | ❌ |
| scph5501.bin | BIOS PS1 américain - requis pour les jeux américains | 490f666e1afb15b7362b406ed1cea246 | ❌ |
| scph5502.bin | BIOS PS1 européen - requis pour les jeux européens | 32736f17079d0b2b7024407c39bd3050 | ❌ |

### Emplacement

Placez les BIOS comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 🗒 **scph5500.bin**
┃ ┃ ┃ ┣ 🗒 **scph5501.bin**
┃ ┃ ┃ ┣ 🗒 **scph5502.bin**

### Liste des bios optionnels

| Nom de fichier | Description | MD5 | Fourni |
| :---: | :---: | :---: | :---: |
| ps1_rom.bin | BIOS PS1 - Fourni avec les dernières mises à jour PS3 | 81bbe60ba7a3d1cea1d48c14cbcc647b | ❌ |

### Emplacement

Placez les BIOS comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 🗒 **ps1_rom.bin**

## ![](/emulators/isos.svg) Isos

### Extensions supportées

Les isos doivent avoir les extensions suivantes :

* .bin/.cue
* .bin/.toc
* .img/.ccd/.sub
* .chd
* .ecm
* .exe
* .img
* .iso
* .m3u
* .mds
* .pbp
* .psf
* .psexe

### Emplacement

Placez les isos comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 psx
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.bin**
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.cue**

>Les isos au format **Redump** sont vivement conseillés.
{.is-success}

>Pour plus d'information sur les isos, rendez-vous sur [ce tutoriel](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-features.png) Configuration avancée de l'émulateur

>Pour pouvoir conserver vos configurations personnalisées lors d'une mise à jour, nous vous conseillons d'utiliser notre fonctionnalité [Surcharges de configuration](./../../../advanced-usage/configuration-override).
{.is-info}

### Accéder aux options

Vous pouvez configurer diverses options de deux façons différentes.

* Via le menu RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via le fichier `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Options du core

| Options | Valeurs des options | Variable (pour surcharge) | Valeurs des variables |
| --- | --- | --- | --- |
| Console Region | `Auto-Detect` ✅ / `NTSC-J (Japan)` / `NTSC-U (US, Canada)` / `PAL (Europe, Australia)` | `swanstation_Console_Region` | `Auto` / `NTSC-J` / `NTSC-U` / `PAL` |
| NTSC-J BIOS (Restart) | `SCPH5500` ✅ / `PSP` / `PS3` | `swanstation_BIOS_PathNTSCJ` | `scph5500.bin` / `psxonpsp660.bin` / `ps1_rom.bin` |
| NTSC-U BIOS (Restart) | `SCPH5501` ✅ / `PSP` / `PS3` | `swanstation_BIOS_PathNTSCU` | `scph5501.bin` / `psxonpsp660.bin` / `ps1_rom.bin` |
| PAL BIOS (Restart) | `SCPH5502` ✅ / `PSP` / `PS3` | `swanstation_BIOS_PathPAL` | `scph5502.bin` / `psxonpsp660.bin` / `ps1_rom.bin` |
| Fast Boot | `Activé` / `Désactivé` ✅ | `swanstation_BIOS_PatchFastBoot` | `true` / `false` |
| CD-ROM Region Check | `Activé` / `Désactivé` ✅ | `swanstation_CDROM_RegionCheck` | `enabled` / `disabled` |
| CD-ROM Read Thread | `Activé` ✅ / `Désactivé` | `swanstation_CDROM_ReadThread` | `true` / `false` |
| Apply Image Patches | `Activé` / `Désactivé` ✅ | `swanstation_CDROM_LoadImagePatches` | `true` / `false` |
| Preload CD-ROM Image To RAM | `Activé` / `Désactivé` ✅ | `swanstation_CDROM_LoadImageToRAM` | `true` / `false` |
| Pre-cache CHD Images To RAM | `Activé` / `Désactivé` ✅ | `swanstation_CDROM_PreCacheCHD` | `true` / `false` |
| Mute CD Audio | `Activé` / `Désactivé` ✅ | `swanstation_CDROM_MuteCDAudio` | `true` / `false` |
| CD-ROM Read Speedup | `None (Double Speed)` ✅ / `2x (Quad Speed)` / `3x (6x Speed)` / `4x (8x Speed)` / `5x (10x Speed)` / `6x (12x Speed)` / `7x (14x Speed)` / `8x (16x Speed)` / `9x (18x Speed)` / `10x (20x Speed)` | `swanstation_CDROM_ReadSpeedup` | `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` |
| CD-ROM Seek Speedup | `Infinite/Instantaneous` / `None (Double Speed)` ✅ / `2x (Quad Speed)` / `3x (6x Speed)` / `4x (8x Speed)` / `5x (10x Speed)` / `6x (12x Speed)` / `7x (14x Speed)` / `8x (16x Speed)` / `9x (18x Speed)` / `10x (20x Speed)` | `swanstation_CDROM_SeekSpeedup` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` |
| CPU Execution Mode | `Interpreter` / `Cached Interpreter` / `Recompiler` ✅ | `swanstation_CPU_ExecutionMode` | `Interpreter` / `CachedInterpreter` / `Recompiler` |
| GPU Renderer | `Hardware (Auto)` / `Harware (OpenGL)` ✅ / `Hardware (Vulkan)` / `Software` | `swanstation_GPU_Renderer` | `Auto` / `OpenGL` / `Vulkan` / `Software` |
| Internal Resolution Scale | `1x` ✅ / `2x` / `3x (for 720p)` / `4x` / `5x (for 1080p)` / `6x (for 1440p)` / `7x` / `8x` / `9x (for 4K)` / `10x` / `11x` / `12x` / `13x` / `14x` / `15x` / `16x` | `swanstation_GPU_ResolutionScale` | `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` |
| Use Software Renderer For Readbacks | `Activé` / `Désactivé` ✅ | `swanstation_GPU_UseSoftwareRendererForReadbacks` | `true` / `false` |
| Multisample Antialiasing | `Désactivé` ✅ / `2x MSAA` / `4x MSAA` / `8x MSAA` / `16x MSAA` / `32x MSAA` / `2x SSAA` / `4x SSAA` / `8x SSAA` / `16x SSAA` / `32x SSAA` | `swanstation_GPU_MSAA` | `1` / `2` / `4` / `8` / `16` / `32` / `2-ssaa` / `4-ssaa` / `8-ssaa` / `16-ssaa` / `32-ssaa` |
| True Color Rendering | `Activé` / `Désactivé` ✅ | `swanstation_GPU_TrueColor` | `true` / `false` |
| Scaled Dithering | `Activé` ✅ / `Désactivé` | `swanstation_GPU_ScaledDithering` | `true` / `false` |
| Disable Interlacing | `Activé` / `Désactivé` ✅ | `swanstation_GPU_DisableInterlacing` | `true` / `false` |
| Force NTSC Timings | `Activé` / `Désactivé` ✅ | `swanstation_GPU_ForceNTSCTimings` | `true` / `false` |
| Force 4:3 For 24-Bit Display | `Activé` / `Désactivé` ✅ | `swanstation_Display_Force4_3For24Bit` | `true` / `false` |
| Chroma Smoothing For 24-Bit Display | `Activé` / `Désactivé` ✅ | `swanstation_GPU_ChromaSmoothingFor24Bit` | `true` / `false` |
| Texture Filtering | `Nearest-Neighbor` ✅ / `Bilinear` / `Bilinear (No Edge Blending)` / `JINC2` / `JINC2 (No Edge Blending)` / `xBR` / `xBR (No Edge Blending)` | `swanstation_GPU_TextureFilter` | `Nearest` / `Bilinear` / `BilinearBinAlpha` / `JINC2` / `JINC2BinAlpha` / `xBR` / `xBRBinAlpha` |
| Widescreen Hack | `Activé` / `Désactivé` ✅ | `swanstation_GPU_WidescreenHack` | `true` / `false` |
| PGXP Geometry Correction | `Activé` / `Désactivé` ✅ | `swanstation_GPU_PGXPEnable` | `true` / `false` |
| Core Provided Aspect Ratio | `4:3` / `16:9` / `19:9` / `20:9` / `Custom` / `Auto (Game Native)` / `PAR 1:1` | `swanstation_Display_AspectRatio` | `4:3` / `16:9` / `19:9` / `20:9` / `Custom` / `Auto` / `PAR 1:1` |
| Crop Mode | `None` / `Only Overscan Area` ✅ / `All Borders` | `swanstation_Display_CropMode` | `None` / `Overscan` / `Borders` |
| Downsampling | `Désactivé` ✅ / `Box (Downsample 3D/Smooth All)` / `Adaptive (Preserve 3D/Smooth 2D)` | `swanstation_GPU_DownsampleMode` | `Disabled` / `Box` / `Adaptive` |
| Display OSD Messages | `Activé` ✅ / `Désactivé` | `swanstation_Display_ShowOSDMessages` | `true` / `false` |
| Display Active Start Offset | `-30` / `-29` / `-28` / `-27` / `-26` / `-25` / `-24` / `-23` / `-22` / `-21` / `-20` / `-19` / `-18` / `-17` / `-16` / `-15` / `-14` / `-13` / `-12` / `-11` / `-10` / `-9` / `-8` / `-7` / `-6` / `-5` / `-4` / `-3` / `-2` / `-1` / `0` ✅ / `+1` / `+2` / `+3` / `+4` / `+5` / `+6` / `+7` / `+8` / `+9` / `+10` / `+11` / `+12` / `+13` / `+14` / `+15` / `+16` / `+17` / `+18` / `+19` / `+20` / `+21` / `+22` / `+23` / `+24` / `+25` / `+26` / `+27` / `+28` / `+29` / `+30` | `swanstation_Display_ActiveStartOffset` | `-30` / `-29` / `-28` / `-27` / `-26` / `-25` / `-24` / `-23` / `-22` / `-21` / `-20` / `-19` / `-18` / `-17` / `-16` / `-15` / `-14` / `-13` / `-12` / `-11` / `-10` / `-9` / `-8` / `-7` / `-6` / `-5` / `-4` / `-3` / `-2` / `-1` / `0` / `+1` / `+2` / `+3` / `+4` / `+5` / `+6` / `+7` / `+8` / `+9` / `+10` / `+11` / `+12` / `+13` / `+14` / `+15` / `+16` / `+17` / `+18` / `+19` / `+20` / `+21` / `+22` / `+23` / `+24` / `+25` / `+26` / `+27` / `+28` / `+29` / `+30` |
| Display Active End Offset | `-30` / `-29` / `-28` / `-27` / `-26` / `-25` / `-24` / `-23` / `-22` / `-21` / `-20` / `-19` / `-18` / `-17` / `-16` / `-15` / `-14` / `-13` / `-12` / `-11` / `-10` / `-9` / `-8` / `-7` / `-6` / `-5` / `-4` / `-3` / `-2` / `-1` / `0` ✅ / `+1` / `+2` / `+3` / `+4` / `+5` / `+6` / `+7` / `+8` / `+9` / `+10` / `+11` / `+12` / `+13` / `+14` / `+15` / `+16` / `+17` / `+18` / `+19` / `+20` / `+21` / `+22` / `+23` / `+24` / `+25` / `+26` / `+27` / `+28` / `+29` / `+30` | `swanstation_Display_ActiveEndOffset` | `-30` / `-29` / `-28` / `-27` / `-26` / `-25` / `-24` / `-23` / `-22` / `-21` / `-20` / `-19` / `-18` / `-17` / `-16` / `-15` / `-14` / `-13` / `-12` / `-11` / `-10` / `-9` / `-8` / `-7` / `-6` / `-5` / `-4` / `-3` / `-2` / `-1` / `0` / `+1` / `+2` / `+3` / `+4` / `+5` / `+6` / `+7` / `+8` / `+9` / `+10` / `+11` / `+12` / `+13` / `+14` / `+15` / `+16` / `+17` / `+18` / `+19` / `+20` / `+21` / `+22` / `+23` / `+24` / `+25` / `+26` / `+27` / `+28` / `+29` / `+30` |
| Display Line Start Offset | `-30` / `-29` / `-28` / `-27` / `-26` / `-25` / `-24` / `-23` / `-22` / `-21` / `-20` / `-19` / `-18` / `-17` / `-16` / `-15` / `-14` / `-13` / `-12` / `-11` / `-10` / `-9` / `-8` / `-7` / `-6` / `-5` / `-4` / `-3` / `-2` / `-1` / `0` ✅ / `+1` / `+2` / `+3` / `+4` / `+5` / `+6` / `+7` / `+8` / `+9` / `+10` / `+11` / `+12` / `+13` / `+14` / `+15` / `+16` / `+17` / `+18` / `+19` / `+20` / `+21` / `+22` / `+23` / `+24` / `+25` / `+26` / `+27` / `+28` / `+29` / `+30` | `swanstation_Display_LineStartOffset` | `-30` / `-29` / `-28` / `-27` / `-26` / `-25` / `-24` / `-23` / `-22` / `-21` / `-20` / `-19` / `-18` / `-17` / `-16` / `-15` / `-14` / `-13` / `-12` / `-11` / `-10` / `-9` / `-8` / `-7` / `-6` / `-5` / `-4` / `-3` / `-2` / `-1` / `0` / `+1` / `+2` / `+3` / `+4` / `+5` / `+6` / `+7` / `+8` / `+9` / `+10` / `+11` / `+12` / `+13` / `+14` / `+15` / `+16` / `+17` / `+18` / `+19` / `+20` / `+21` / `+22` / `+23` / `+24` / `+25` / `+26` / `+27` / `+28` / `+29` / `+30` |
| Display Line End Offset | `-30` / `-29` / `-28` / `-27` / `-26` / `-25` / `-24` / `-23` / `-22` / `-21` / `-20` / `-19` / `-18` / `-17` / `-16` / `-15` / `-14` / `-13` / `-12` / `-11` / `-10` / `-9` / `-8` / `-7` / `-6` / `-5` / `-4` / `-3` / `-2` / `-1` / `0` ✅ / `+1` / `+2` / `+3` / `+4` / `+5` / `+6` / `+7` / `+8` / `+9` / `+10` / `+11` / `+12` / `+13` / `+14` / `+15` / `+16` / `+17` / `+18` / `+19` / `+20` / `+21` / `+22` / `+23` / `+24` / `+25` / `+26` / `+27` / `+28` / `+29` / `+30` | `swanstation_Display_LineEndOffset` | `-30` / `-29` / `-28` / `-27` / `-26` / `-25` / `-24` / `-23` / `-22` / `-21` / `-20` / `-19` / `-18` / `-17` / `-16` / `-15` / `-14` / `-13` / `-12` / `-11` / `-10` / `-9` / `-8` / `-7` / `-6` / `-5` / `-4` / `-3` / `-2` / `-1` / `0` / `+1` / `+2` / `+3` / `+4` / `+5` / `+6` / `+7` / `+8` / `+9` / `+10` / `+11` / `+12` / `+13` / `+14` / `+15` / `+16` / `+17` / `+18` / `+19` / `+20` / `+21` / `+22` / `+23` / `+24` / `+25` / `+26` / `+27` / `+28` / `+29` / `+30` |
| Memory Card 1 Type | `No Memory Card` / `Libretro` ✅ / `Shared Between All Games` / `Separate Card Per Game (Game Code)` / `Separate Card Per Game (Game Title)` | `swanstation_MemoryCards_Card1Type` | `None` / `Libretro` / `Shared` / `PerGame` / `PerGameTitle` |
| Memory Card 2 Type | `No Memory Card` ✅ / `Shared Between All Games` / `Separate Card Per Game (Game Code)` / `Separate Card Per Game (Game Title)` | `swanstation_MemoryCards_Card2Type` | `None` / `Shared` / `PerGame` / `PerGameTitle` |
| Use Single Card For Playlist | `Activé` ✅ / `Désactivé` | `swanstation_MemoryCards_UsePlaylistTitle` | `true` / `false` |
| Multitap Mode | `Désactivé` ✅ / `Enable on Port 1 Only` / `Enable on Port 2 Only` / `Enable on Ports 1 and 2` | `swanstation_ControllerPorts_MultitapMode` | `Disabled` / `Port1Only` / `Port2Only` / `BothPorts` |
| DualShock Analog Mode Combo | `L1 + R1 + L3 + R3` / `L1 + L2 + R1 + R2 + Start + Select` / `L1 + R1 + Select` ✅ / `L1 + R1 + Start` / `L1 + R1 + L3` / `L1 + R1 + R3` / `L2 + R2 + Select` / `L2 + R2 + Start` / `L2 + R2 + L3` / `L2 + R2 + R3` / `L3 + R3` | `swanstation_Controller_AnalogCombo` | `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` |
| DualShock Enable Rumble | `Activé` ✅ / `Désactivé` | `swanstation_Controller_EnableRumble` | `true` / `false` |
| CD-ROM Async Readahead | `Disabled (Synchronous)` / `1 Sector (7KB / 2ms)` / `2 Sectors (13KB / 4ms)` / `3 Sectors (20KB / 6ms)` / `4 Sectors (27KB / 8ms)` / `5 Sectors (33KB / 10ms)` / `6 Sectors (40KB / 12ms)` / `7 Sectors (47KB / 14ms)` / `8 Sectors (53KB / 16ms)` / `9 Sectors (60KB / 18ms)` / `10 Sectors (67KB / 20ms)` / `11 Sectors (73KB / 22ms)` / `12 Sectors (80KB / 24ms)` / `13 Sectors (87KB / 26ms)` / `14 Sectors (93KB / 28ms)` / `15 Sectors (100KB / 30ms)` / `16 Sectors (107KB / 32ms)` / `17 Sectors (113KB / 34ms)` / `18 Sectors (120KB / 36ms)` / `19 Sectors (127KB / 38ms)` / `20 Sectors (133KB / 40ms)` / `21 Sectors (140KB / 42ms)` / `22 Sectors (147KB / 44ms)` / `23 Sectors (153KB / 46ms)` / `24 Sectors (160KB / 48ms)` / `25 Sectors (167KB / 50ms)` / `26 Sectors (173KB / 52ms)` / `27 Sectors (180KB / 54ms)` / `28 Sectors (187KB / 56ms)` / `29 Sectors (193KB / 58ms)` / `30 Sectors (200KB / 60ms)` / `31 Sectors (207KB / 62ms)` / `32 Sectors (213KB / 64ms)` | `swanstation_CDROM_ReadaheadSectors` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `21` / `22` / `23` / `24` / `25` / `26` / `27` / `28` / `29` / `30` / `31` / `32` |
| CPU Overclocking | `10%` / `15%` / `20%` / `25%` / `30%` / `35%` / `40%` / `45%` / `50%` / `55%` / `60%` / `65%` / `70%` / `75%` / `80%` / `85%` / `90%` / `95%` / `100% (Default)` ✅ / `105%` / `110%` / `115%` / `120%` / `125%` / `130%` / `135%` / `140%` / `145%` / `150%` / `155%` / `160%` / `165%` / `170%` / `175%` / `180%` / `185%` / `190%` / `195%` / `200%` / `205%` / `210%` / `215%` / `220%` / `225%` / `230%` / `235%` / `240%` / `245%` / `250%` / `255%` / `260%` / `265%` / `270%` / `275%` / `280%` / `285%` / `290%` / `295%` / `300%` / `325%` / `350%` / `375%` / `400%` / `425%` / `450%` / `475%` / `500%` / `525%` / `550%` / `575%` / `600%` / `625%` / `650%` / `675%` / `700%` / `725%` / `750%` / `775%` / `800%` / `825%` / `850%` / `875%` / `900%` / `925%` / `950%` / `975%` / `1000%`| `swanstation_CPU_Overclock` | `10` / `15` / `20` / `25` / `30` / `35` / `40` / `45` / `50` / `55` / `60` / `65` / `70` / `75` / `80` / `85` / `90` / `95` / `100` / `105` / `110` / `115` / `120` / `125` / `130` / `135` / `140` / `145` / `150` / `155` / `160` / `165` / `170` / `175` / `180` / `185` / `190` / `195` / `200` / `205` / `210` / `215` / `220` / `225` / `230` / `235` / `240` / `245` / `250` / `255` / `260` / `265` / `270` / `275` / `280` / `285` / `290` / `295` / `300` / `325` / `350` / `375` / `400` / `425` / `450` / `475` / `500` / `525` / `550` / `575` / `600` / `625` / `650` / `675` / `700` / `725` / `750` / `775` / `800` / `825` / `850` / `875` / `900` / `925` / `950` / `975` / `1000` |
| Apply Compatibility Settings | `Activé` ✅ / `Désactivé` | `swanstation_Main_ApplyGameSettings` | `true` / `false` |
| Log Level | `None` / `Error` / `Warning` / `Performance` / `Success` / `Information` ✅ / `Developer` / `Profile` / `Debug` / `Trace` | `swanstation_Logging_LogLevel` | `None` / `Error` / `Warning` / `Performance` / `Success` / `Information` / `Developer` / `Profile` / `Debug` / `Trace` |
| CPU Recompiler ICache | `Activé` / `Désactivé` ✅ | `swanstation_CPU_RecompilerICache` | `true` / `false` |
| CPU Recompiler Block Linking | `Activé` / `Désactivé` | `swanstation_CPU_RecompilerBlockLinking` | `true` / `false` |
| CPU Recompiler Fast Memory Access | `Désactivé (Slower)` / `LUT (Faster)` ✅ / `MMAP (Hardware, Fastest, 64-Bit Only)` | `swanstation_CPU_FastmemMode` | `Disabled` / `LUT` / `MMAP` |
| CPU Recompiler Fast Memory Access Rewrite | `Activé` / `Désactivé` ✅ | `swanstation_CPU_FastmemRewrite` | `true` / `false` |
| Enable VRAM Write Texture Replacement | `Activé` / `Désactivé` ✅ | `swanstation_TextureReplacements_EnableVRAMWriteReplacements` | `true` / `false` |
| Internal Run-Ahead | `0 Frames (Désactivé)` ✅ / `1 Frame` / `2 Frames` / `3 Frames` / `4 Frames` / `5 Frames` / `6 Frames` / `7 Frames` / `8 Frames` / `9 Frames` / `10 Frames` | `swanstation_Main_RunaheadFrameCount` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` |
| Enable 8MB RAM (Dev Console) | `Activé` / `Désactivé` ✅ | `swanstation_Console_Enable8MBRAM` | `true` / `false` |
| Use Old MDEC Routines | `Activé` / `Désactivé` ✅ | `swanstation_Hacks_UseOldMDECRoutines` | `true` / `false` |
| Use Alternative Audio Hook (Restart) | `Activé` ✅ / `Désactivé` | `swanstation_Audio_FastHook` | `true` / `false` |

## ![](/emulators/external-links.png) Liens externes

* **Code source utilisé** : [https://github.com/libretro/swanstation/](https://github.com/libretro/swanstation/)