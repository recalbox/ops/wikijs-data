---
title: Trier et mettre à jour ses roms
description: 
published: true
date: 2024-07-23T22:48:56.762Z
tags: roms, trier, mettre à jour
editor: markdown
dateCreated: 2021-05-21T08:48:38.192Z
---

Pour trier et mettre à jour vos jeux, vous aurait besoin d'un logiciel, d'un fichier dat et pour certaines consoles un fichier header.

>Cette étape n'est pas à négliger car les roms ont des mises à jour par certains groupes qui permettent d'améliorer les roms en supprimant des bugs présents sur la rom ou pour mieux la faire tourner sur émulateur.
>
>Ces mises à jour ne sont pas fréquentes mais reste néanmoins indispensables pour leurs bons fonctionnements sur émulateur et est essentiel en Netplay. 
{.is-warning}

## Les logiciels

Vous avez plusieurs logiciels pour trier et mettre à jour vos roms.

* [Clrmamepro](https://mamedev.emulab.it/clrmamepro/)
* [Romulus](https://romulus.dats.site/)
* [Universal Rom Cleaner](https://github.com/Universal-Rom-Tools/Universal-ROM-Cleaner/releases)

>Pour les débutants, il est conseillé d'utiliser Romulus qui est plus facile d'utilisation.
{.is-info}

## Les fichiers dat

Un fichier dat est un fichier de référence pour le catalogage des jeux par système (console). Ce fichier peut avoir l'extension .dat (d'où son nom) ou .xml.

Avec ce fichier, vous pouvez vérifier avec les logiciels adéquats si votre dump de rom ou d'iso est correcte.

Concernant l'arcade, vous pourrez télécharger ces fichiers sur ces sites:

* **AdvanceMAME** : dans votre Recalbox, dans `/recalbox/share/arcade/advancemame/advancemame.dat`.
* **Atomiswave** : dans votre Recalbox, dans `/recalbox/share/arcade/libretro/atomiswave.dat`.
* **FBNeo** : dans votre Recalbox, dans `/recalbox/share/arcade/libretro/fbneo.dat`.
* **MAME** : dans votre Recalbox, dans `/recalbox/share/arcade/libretro/mame.dat`.
* **MAME 2000** : dans votre Recalbox, dans `/recalbox/share/arcade/libretro/mame2000.dat`.
* **MAME 2003 Plus** : dans votre Recalbox, dans `/recalbox/share/arcade/libretro/mame2003-plus.dat`.
* **MAME 2003** : dans votre Recalbox, dans `/recalbox/share/arcade/libretro/mame2003.dat`.
* **MAME 2010** : dans votre Recalbox, dans `/recalbox/share/arcade/libretro/mame2010.dat`.
* **MAME 2015** : dans votre Recalbox, dans `/recalbox/share/arcade/libretro/mame2015.dat`.
* **Model3** : dans votre Recalbox, dans `/recalbox/share/arcade/supermodel/supermodel.dat`.
* **Naomi** : dans votre Recalbox, dans `/recalbox/share/arcade/libretro/naomi.dat`.
* **Naomi GD-Rom** : dans votre Recalbox, dans `/recalbox/share/arcade/libretro/naomigd.dat`.
* **Naomi 2** : dans votre Recalbox, dans `/recalbox/share/arcade/libretro/naomi2.dat`.
* **Neo Geo** : dans votre Recalbox, dans `/recalbox/share/arcade/libretro/fbneo-non-arcade/FinalBurn Neo (ClrMame Pro XML, Neogeo only).dat`.

>Les fichiers dats listés dessus sont accessibles uniquement via un accès réseau.
{.is-info}

Pour les consoles de salon ou portables ainsi que les ordinateurs, vous pourrez télécharger ces ficheirs sur ces sites :

* **No-Intro** (consoles à cartouches) : [https://datomatic.no-intro.org/?page=download](https://datomatic.no-intro.org/?page=download)
* **Redump** (consoles à disque) : [https://redump.org/downloads/](https://redump.org/downloads/)
* **TOSEC** (ordinateurs) : [https://www.tosecdev.org/](https://www.tosecdev.org/)

Les deux sources suivantes sont listés pour la culture uniquement :

* **Trurip** : [https://database.trurip.org/](https://database.trurip.org/)
* **GoodSets/Goodtools** : [https://cowering.blogspot.com/](https://cowering.blogspot.com/)

Veillez à prendre le fichier dat correspondant au groupe de rom que vous aurez choisi !

## Les fichiers header

Pour certaines consoles, il vous faudra un fichier header (en-tête) en compléments du fichier dat.

Voici la liste des consoles concernées :

* Atari - 7800
* Atari - Lynx
* Nintendo - Family Computer Disk System
* Nintendo - Nintendo Entertainment System

## Les préférences de tri

### National ou international ?

* **National** : si vous voulez jouer uniquement avec les personnes du même pays que vous : gardez uniquement les roms de votre pays puis privilégiez les roms USA et Japon pour les manquants.
* **International** : si vous voulez jouer avec des personnes des autres pays : gardez une rom de chaque pays.

Voici un exemple international :

* Super Street Fighter II (Europe)
* Super Street Fighter II - The New Challengers (Japan)
* Street Fighter II Plus - Champion Edition (Japan, Korea, Asia)

### Les Retroachievements

Certains trophées Retroachievements ont besoin de la rom USA pour valider les retroachievements et certains jeux n'existent qu'en version japonaise.  

Vous trouverez plus d'informations [ici](./../../../../basic-usage/features/retroachievements).

### Les jeux et les régions

Selon la région, les jeux peuvent avoir des différences plus au moins flagrantes. De plus, certains jeux n'existent qu'en version japonaise.

Voici quelques exemples de ces différences :

* Boss différent.
* Couleur de sang différente (rouge sur une version USA qui passe en vert pour une version Europe).
* Fps plus ou moins élevé.
* Fréquence (MHz) plus ou moins élevé.
* Nombre de joueurs différents.

### Les codes et tags dans les roms

Pour savoir à quoi correspondent les codes et tags dans les roms (uniquement pour les roms cartouches et disquettes), vous trouverez plus d'informations [ici](./../tags-used-in-rom-names).