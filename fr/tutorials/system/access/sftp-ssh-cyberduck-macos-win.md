---
title: Accès SFTP/SSH via Cyberduck \(macOS / Windows\)
---



## Connexion réseau SSH avec Cyberduck <a id="connexion-reseau-ssh-avec-cyberduck"></a>

Voici comment vous connecter sur macOS et Windows en réseau à Recalbox en SFTP avec le logiciel Cyberduck.

Vous pourrez ainsi avoir accès facilement aux différents fichiers de configuration comme par exemple `/boot/recalbox-user-config.txt`.   
Si vous êtes un habitué **des outils du shell**, ce tutoriel n'est pas fait pour vous , il s'adresse **aux utilisateurs de macOS** qui ont besoin **d'une interface graphique.**


>**Informations :**
>
>**Cyberduck** est un logiciel gratuit de transfert de fichiers sur macOS et Windows.  
>Il gère de nombreux protocoles : FTP, FTP-SSL, SFTP, WebDAV, Swift, S3, Google Cloud Storage, Windows Azure Storage, Dropbox, Google Drive, Backblaze B2 Cloud Storage, Backspace Cloud Files.
{.is-info}

* Pour commencer, [téléchargez](https://cyberduck.io/) Cyberduck.
* Il faut que votre Recalbox soit connecté à votre réseau local. Vous pouvez trouver l’adresse ip de votre Recalbox via le menu `OPTIONS RÉSEAU`**,** l'adresse IP est requise \(le nom de réseau ne suffit pas\).

![](http://i.imgur.com/j6JPi37.png.jpg)

* Ouvrez Cyberduck et cliquez sur **«** Ouvrir une connexion » et renseignez les informations suivantes :
  * Protocole : SFTP \(SSH File Transfert Protocol\)
  * Port : 22
  * Serveur : renseignez l’adresse IP de votre Recalbox
  * Nom d’utilisateur : root
  * Mot de passe : recalboxroot 
* Cliquez sur le bouton `Connecter` et autorisez les empreintes inconnues.

![](http://i.imgur.com/8u6bNOe.png)

* Pour enregistrer la configuration dans un signet, cliquez sur l'icône de signet en haut à gauche, puis sur le bouton `+`  en bas à gauche

![](http://i.imgur.com/ePuP7Ez.png)

* Profitez-en pour renommer la connexion avant de fermer la fenêtre

![](http://i.imgur.com/WexZz6G.png)

* Voilà le résultat :

![](http://i.imgur.com/kvgMoPt.png)

Voilà, votre signet est sauvegardé.

Le dossier **recalbox-user-config.txt** se trouve dans ****`/boot/` .  
Le dossier **partagé** de Recalbox dans `/recalbox/share/`

## **Mettre les droits en écriture sur la carte SD** <a id="mettre-les-droits-en-ecriture-sur-la-carte-sd"></a>

Pour les bidouilleurs avertis, vous devez passer les droits de la SD en écriture, pour modifier le fameux fichier `recalbox-user-config.txt`. 

* Dans le menu de Cyberduck, utilisez l’option `Envoyer une commande`

![](http://i.imgur.com/1KWkCrH.png)

* Et insérez la commande suivante **:**

`mount -o remount,rw /boot` 

* Voilà, vous pouvez modifier le fichier `recalbox-user-config.txt`. Il vous est conseillé de **faire une sauvegarde du fichier d’origine** sur votre ordinateur, puis de remplacer celui de la carte SD par le fichier modifié.


>**Information :**
>
>Au prochain redémarrage de la Recalbox, les droits repasseront en lecture seule.
{.is-info}

**Source :** [https://forum.recalbox.com/topic/7341/tutoriel-se-connecter-en-ssh-pour-les-nuls-avec-cyberduck-pour-config-txt-osx-et-win](https://forum.recalbox.com/topic/7341/tutoriel-se-connecter-en-ssh-pour-les-nuls-avec-cyberduck-pour-config-txt-osx-et-win)

