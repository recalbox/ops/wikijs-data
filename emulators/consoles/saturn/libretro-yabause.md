---
title: Libretro Yabause
description: 
published: true
date: 2024-07-24T18:54:05.893Z
tags: libretro, saturn, yabause
editor: markdown
dateCreated: 2021-08-03T10:39:39.501Z
---

**Libretro Yabause** is a port of the **Yabause** emulator to Libretro supporting the Sega Saturn.

## ![](/emulators/license.svg) License

This core is under [**GPLv2**](https://github.com/libretro/yabause/blob/master/yabause/COPYING).

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ❌ | ❌ | ❌ | ❌ | ❌ | ❌ | ✅ | ❌ |

## ![](/emulators/features.png) Features

| Feature | Supported |
| :---: | :---: |
| Restart  | ✔ |
| Screenshots | ✔ |
| Saves | ✔ |
| States | ✔ |
| Core Options | ✔ |
| RetroAchievements | ✔ |
| RetroArch Cheats | ✔ |
| Controls | ✔ |
| Remapping | ✔ |
| Disk Control | ✔ |

## ![](/emulators/bios.svg) BIOS

### List of mandatory bios

| Filename | Description | MD5 | Provided |
| :---: | :---: | :---: | :---: |
| saturn_bios.bin | Saturn BIOS | af5828fdff51384f99b3c4926be27762 | ❌ |

### Location

Put the bios like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 saturn
┃ ┃ ┃ ┃ ┣ 🗒 **saturn_bios.bin**

## ![](/emulators/isos.svg) Isos

### Supported extensions

Isos must have the extension:

* .cue/.bin
* .m3u
* .img/.ccd
* .iso
* .mds/.mdf
* .chd

### Location

Put the isos like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 saturn
┃ ┃ ┃ ┃ ┣ 🗒 **game.bin**
┃ ┃ ┃ ┃ ┣ 🗒 **game.cue**

>The isos using **Redump** format are highly recommended.
{.is-success}

>For more information about isos, go to [this tutorial](./../../../tutorials/games/generalities/isos-and-roms)!
{.is-info}

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Accessing to options

You can configure various options in two different ways.

* Via RetroArch menu:

┣ 📁 RetroArch Menu
┃ ┣ 📁 Core options
┃ ┃ ┣ 🧩 Name_option

* Via the file `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core options

| Options | Options values | Variables (for configuration override) | Variables values |
| --- | --- | --- | --- |
| Frameskip | `Disabled` ✅ / `Enabled` | `yabause_frameskip` | `disabled` / `enabled` |
| Force HLE BIOS (restart) | `Disabled` ✅ / `Enabled` | `yabause_force_hle_bios` | `disabled` / `enabled` |
| Addon Cartridge (restart) | `None` ✅ / `1M_ram` / `4M_ram` | `yabause_addon_cartridge` | `None` / `1M_ram` / `4M_ram` |
| 6Player Adaptor on Port 1 | `Disabled` ✅ / `Enabled` | `yabause_multitap_port1` | `disabled` / `enabled` |
| 6Player Adaptor on Port 2 | `Disabled` ✅ / `Enabled` | `yabause_multitap_port2` | `disabled` / `enabled` |
| Number of Threads | `1` / `2` / `4` ✅ / `8` / `16` / `32` | `yabause_numthreads` | `1` / `2` / `4` / `8` / `16` / `32` |

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/libretro/yabause/](https://github.com/libretro/yabause/)
* **Libretro documentation**: [https://docs.libretro.com/library/yabause/](https://docs.libretro.com/library/yabause/)