---
title: ColecoVision
description: 
published: true
date: 2024-07-26T07:46:59.823Z
tags: coleco, colecovision, consolas
editor: markdown
dateCreated: 2023-09-03T15:51:34.573Z
---

![](/emulators/consoles/colecovision.svg){.align-center}

## Datos técnicos

* **Fabricante :** Coleco
* **Año de lanzamiento :** 1982
* **Cantidad vendida :** 2+ millones
* **Juego más vendido :** Donkey Kong
* **CPU :** Zilog Z80 a 3,58 MHz
* **RAM :** 1kB
* **VRAM:** 16kB
* **Video:** Texas Instruments TMS9928A
* **Resolución :** 256x192, 16 colores
* **Chip de sonido:** Texas Instruments SN76489A
* **Tamaño del cartucho :** 8kB - 32kB

## Presentación

La **ColecoVision** es una videoconsola de segunda generación comercializada por Coleco (Connecticut Leather Company), lanzada en agosto de 1982. Presentaba gráficos y mandos similares a los de los juegos arcade de la época.

ColecoVision salió a la venta en el verano de 1982 por 399 dólares. El catálogo de lanzamiento incluía doce títulos y diez más. Finalmente, se lanzaron unos 400 títulos en cartucho durante su vida útil. La mayoría de los títulos de su catálogo eran conversiones de juegos arcade.

Coleco comercializó un módulo que hacía compatible la consola con la Atari 2600, dotándola de la mayor biblioteca de juegos de todas las consolas de la época. El módulo provocó acciones legales por parte de Atari, pero ésta no pudo impedir su venta. Que conste que, gracias a este módulo, los propietarios de cartuchos franceses de VCS2600 descubrieron por fin sus juegos en 128 colores, en lugar de la coloración sospechosamente simple de 8 colores de la versión en blanco y negro de los mismos, en VCS2600 SECAM.

Otro módulo, lanzado en el verano de 1983, transformaba la consola en un auténtico ordenador, el Coleco Adam, con teclado y reproductor de casetes.

En las Navidades de 1982, Coleco había vendido 500.000 consolas, sobre todo gracias a la calidad de los juegos suministrados. Mientras que Atari hizo fortuna con Space Invaders, ColecoVision fue la primera consola en albergar Donkey Kong. Los derechos se negociaron por 250.000 dólares.

Las ventas superaron rápidamente el millón a principios de 1983, antes del crack de los videojuegos de 1983 en Estados Unidos. A finales del primer trimestre de 1984, las ventas se habían reducido a un total de 2 millones. Comercializada en Europa con el nombre de CBS ColecoVision, la consola estuvo disponible en Francia en el verano de 1983 a un precio de 1.790 francos. El módulo Adam se puso brevemente a la venta a principios de 1984.

Coleco Industries consideró la posibilidad de retirarse de la industria del videojuego en junio de 1985 y la ColecoVision se dejó de fabricar oficialmente en octubre de 1985.

## Emuladores

[Libretro FBNeo](libretro-fbneo)
[Libretro GearColeco](libretro-gearcoleco)
[Libretro blueMSX](libretro-bluemsx)