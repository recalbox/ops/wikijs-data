---
title: Atari ST
description: 
published: true
date: 2024-07-26T12:46:14.440Z
tags: atari, st, atari-st, ordenadores
editor: markdown
dateCreated: 2023-09-02T18:39:27.244Z
---

![](/emulators/computers/atarist.svg){.align-center}

## Detalles técnicos

* **Fabricante**: Atari Corporation
* **Año de lanzamiento**: 1985
* **Sistema operativo**: Atari TOS
* **Procesador**: Motorola 68000 CPU de 16/32 bits a 8 MHz
* **RAM**: 512 KB o 1 MB
* **Unidad de disco**: disquetera de 3½" de una cara
* **Chip de sonido**: Yamaha YM2149 de onda cuadrada a 3 voces.
* **Pantalla**
  * **Baja resolución**: 320 × 200 (16 colores), paleta de 512 colores.
  * **Resolución media**: 640 × 200 (4 colores), paleta de 512 colores

## Presentación

El **Atari ST** es una familia de ordenadores personales diseñada por la empresa estadounidense Atari, cuyo éxito comercial marcó la segunda mitad de los años ochenta y principios de los noventa. Tuvo tanto éxito para el gran público (videojuegos) como para el uso profesional (tratamiento de textos, autoedición y, sobre todo, MAO).

El microordenador Atari ST pasará a la historia de la informática como la máquina que permitió el desarrollo de la música asistida por ordenador y la democratización del estándar MIDI. En 2010, la revista especializada Sound on Sound lo incluyó entre los 25 productos destacados responsables de los cambios en la grabación musical. Esta máquina sigue considerándose una referencia en este campo, gracias a su robustez y extrema precisión para la secuenciación MIDI.

## Emuladores

[Hatari](hatari)
[Libretro Hatari](libretro-hatari)