---
title: Romulus for Arcade
description: Usage of Romulus Rom Manager for Arcade systems (MAME, FinalBurn Neo, ...)
published: true
date: 2021-08-07T13:15:30.888Z
tags: arcade, romulus, mame, fbneo
editor: markdown
dateCreated: 2021-08-07T13:08:30.114Z
---

>Please refer to the [Romulus for Consoles](./romulus-for-consoles) part to know the basic functioning of Romulus (adding and setting a .dat, rebuilding a romset, etc.).
{.is-info}

## File modes

The Arcade romsets contain many games so some of them have a common base. We speak then of "Parent / Clone" games, "Parent" for the main game (the 1st game published for example) and clones for all the derivatives of this game (another language, another region, another name, ...).

Moreover, unlike console romsets where the standard is "1 game = 1 rom", arcade games have several roms to make up the game.

In Romulus, there are 3 possible modes for the assembly of these games (these same modes are similar in many romset managers).

### Detail of the 3 modes

![main set = parent / clone set = clone](/tutorials/utilities/rom-management/romulus/arcade/romulus1.png)

**Split / Not Merged** : All roms needed for the game are in the game file or folder (more storage space is used because common roms are present in each game).
* Split / Merged** : All the roms needed for the Parent game are in the game file or folder. The file or folder of the Clone game contains only the particular roms composing it (but to work, the clone game needs roms present in the parent game).
**Not Split** : All the roms of the parent or clone games are in the same file or folder.

>For a use of the games in Recalbox, it is advised to use the **Split / Merged** mode.
{.is-info}

## Arcade games in Romulus

![](/tutorials/utilities/rom-management/romulus/arcade/romulus2.png)

* **Blue highlighted line**: The empty "**Clone of**" column indicates that it is a parent game. The green icon on the left indicates that the game is complete (yellow icon = incomplete game, red icon = absent game).
**A** : In this table you will find the details of the selected game. It is the list of the roms composing the game. The green dot indicates that the rom is present and conforms to the .dat file (red dot = missing rom).
* **B** : The column "**Clone of**" is not empty, these are clone games. The name of the parent game is indicated in this column.

## Sample and CHD files

![](/tutorials/utilities/rom-management/romulus/arcade/romulus3.png)

When configuring the .dat file for the arcade (see [Configure the .dat file added](./romulus-for-consoles#configure-the-added-dat-file)), it is sometimes necessary to define other directories in addition to the one for the roms: a directory for the **Samples** (Audio files necessary for some games) and a directory for the **CHD** (Complementary CD image of the rom for the operation of the most recent games).