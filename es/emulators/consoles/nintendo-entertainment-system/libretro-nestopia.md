---
title: Libretro Nestopia
description: 
published: true
date: 2024-07-05T15:05:32.158Z
tags: libretro, nestopia, nes
editor: markdown
dateCreated: 2023-09-07T06:46:15.660Z
---



## ![](/emulators/license.svg) Licencia

El core posee la siguiente licencia [**GPLv2**](https://github.com/libretro/nestopia/blob/master/COPYING).

## ![](/emulators/compatibility.png) Compatibilidad

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | 
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | 

## ![](/emulators/features.png) Funcionalidades

| Funcionalidad | ¿Soportada? |
| :---: | :---: |
| Reinicio | ✔ |
| Capturas de pantalla | ✔ |
| Grabaciones | ✔ |
| Grabaciones instantáneas | ✔ |
| Rebobinado | ✔ |
| Netplay | ✔ |
| Optiones del core | ✔ |
| Cheats RetroArch | ✔ |
| Controles | ✔ |
| Remapping | ✔ |
| Softpatching | ✔ |

## ![](/emulators/bios.svg) BIOS

>**No se necesitan bios.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Extensiones de roms soportadas

Las roms deben tener las extensiones siguientes:

* .nes
* .unf
* .unif
* .zip
* .7z

Este sistema soporta las roms comprimidas en formato .zip/.7z. No obstante, se trata sólo de un archivo.

Los ficheros contenidos en los .zip/.7z deben tener las extensiones citadas previamente.
Además, cada fichero .zip/.7z sólo debe contener un fichero rom al interior.

### Emplazamiento

Disponed vuestras roms como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 nes
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.zip**

>Se aconsejas las roms del projecto **No-Intro**.
{.is-success}

>Para más informacion sobre las roms, consultad [este tutorial](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

>Para conservar las configuraciones personalizadas cuando se produce una actualización del sistema Recalbox, os aconsejamos utilizar las [Sobrecargas de configuración](./../../../advanced-usage/configuration-override).
{.is-info}


### Acceder a las opciones

Podéis configurar las opciones del core de dos formas diferentes.

* Via el Menú RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via el fichero `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Opciones del core

| Opción | Valor de la opción | Variable (para la sobrecarga) | Valores posibles |
| --- | --- | --- | --- |
 | Région du système | `Auto` ✅ / `NTSC` / `PAL` / `Famicom` / `Dendy` | `nestopia_favored_system` | `auto` / `ntsc` / `pal` / `famicom` / `dendy` |
| Insertion automatique du FDS | `Désactivé` / `Activé` ✅ | `nestopia_fds_auto_insert` | `disabled` / `enabled` |
| Filtre NTSC de Blargg | `Désactivé` ✅ / `Vidéo composite` / `S-Video` / `Péritel RVB (SCART)` / `Monochrome` | `nestopia_blargg_ntsc_filter` | `disabled` / `composite` / `svideo` / `rgb` / `monochrome` |
| Palette | `CXA2025AS` ✅ / `Consommateur` / `Canonique` / `Alternative` / `RGB` / `PAL` / `FBx composite direct` / `FBx style PVM D93` / `FBx matériel NTSC` / `FBx NES Classic (corrigée)` / `Données brutes` / `Personnalisée` | `nestopia_palette` | `cxa2025as` / `consumer` / `composite` / `alternate` / `rgb` / `pal` / `composite-direct-fbx` / `pvm-style-d93-fbx` / `ntsc-hardware-fbx` / `nes-classic-fbx-fs` / `raw` / `custom` |
| Mask Overscan (Top Vertical) | `0` / `4` / `8` ✅ / `12` / `16` / `20` / `24` | `nestopia_overscan_v_top` | `0` / `4` / `8` / `12` / `16` / `20` / `24` |
| Mask Overscan (Bottom Vertical) | `0` / `4` / `8` ✅ / `12` / `16` / `20` / `24` | `nestopia_overscan_v_bottom` | `0` / `4` / `8` / `12` / `16` / `20` / `24` |
| Mask Overscan (Left Horizontal) | `0` ✅ / `4` / `8` / `12` / `16` / `20` / `24` | `nestopia_overscan_h_left` | `0` / `4` / `8` / `12` / `16` / `20` / `24` |
| Mask Overscan (Right Horizontal) | `0` ✅ / `4` / `8` / `12` / `16` / `20` / `24` | `nestopia_overscan_h_right` | `0` / `4` / `8` / `12` / `16` / `20` / `24` |
| Rapport d'aspect préféré | `Auto` ✅ / `NTSC` / `PAL` / `4:3` | `nestopia_aspect` | `auto` / `ntsc` / `pal` / `4:3` |
| Distorsion sonore du Game Genie | `Désactivé` ✅ / `Activé` | `nestopia_genie_distortion` | `disabled` / `enabled` |
| Volume du canal carré 1 (%) | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `nestopia_audio_vol_sq1` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
| Volume du canal carré 2 (%) | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `nestopia_audio_vol_sq2` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
| Volume du canal triangle (%) | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `nestopia_audio_vol_tri` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
| Volume du canal de bruit (%) | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `nestopia_audio_vol_noise` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
| Volume du canal DPCM (%) | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `nestopia_audio_vol_dpcm` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
| Volume du canal FDS (%) | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `nestopia_audio_vol_fds` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
| Volume du canal MMC5 (%) | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `nestopia_audio_vol_mmc5` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
| Volume du canal VRC6 (%) | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `nestopia_audio_vol_vrc6` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
| Volume du canal VRC7 (%) | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `nestopia_audio_vol_vrc7` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
| Volume du canal N163 (%) | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `nestopia_audio_vol_n163` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
| Volume du canal S5B (%) | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `nestopia_audio_vol_s5b` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
| Audio Output | `mono` / `stereo` ✅ | `nestopia_audio_type` | `mono` / `stereo` |
| 4 Player Adapter | `Auto` ✅ / `NTSC` / `Famicom` | `nestopia_select_adapter` | `auto` / `ntsc` / `famicom` |
| Touches décalées dans le sens horaire | `Désactivé` ✅ / `Activé` | `nestopia_button_shift` | `disabled` / `enabled` |
| Appareil Arkanoid | `Souris` ✅ / `Pointeur` | `nestopia_arkanoid_device` | `mouse` / `pointer` |
| Périphérique Zapper | `Pistolet` ✅ / `Souris` / `Pointeur` | `nestopia_zapper_device` | `lightgun` / `mouse` / `pointer` |
| Afficher le viseur | `Désactivé` / `Activé` ✅ | `nestopia_show_crosshair` | `disabled` / `enabled` |
| Mask Overscan (Vertical) | `Activé` ✅ / `Désactivé` | `nestopia_overscan_v` | `enabled` / `disabled` |
| Mask Overscan (Horizontal) | `Activé` / `Désactivé` ✅ | `nestopia_overscan_h` | `enabled` / `disabled` |
| Preferred Aspect Ratio | `Auto` ✅ / `NTSC` / `PAL` / `4:3` | `nestopia_aspect` | `auto` / `ntsc` / `pal` / `4:3` |
| Game Genie Sound Distortion | `Désactivé` ✅ / `Activé` | `nestopia_genie_distortion` | `disabled` / `enabled` |
| System Region | `Auto` ✅ / `NTSC` / `PAL` / `Famicom` / `Dendy` | `nestopia_favored_system` | `auto` / `ntsc` / `pal` / `famicom` / `dendy` |
| RAM Power-on State | `0x00` ✅ / `0xFF` / `Random` | `nestopia_ram_power_state` | `0x00` / `0xFF` / `random` |
| Shift Buttons Clockwise | `Désactivé` ✅ / `Activé` | `nestopia_button_shift` | `disabled` / `enabled` |
| Vitesse d'impulsion du turbo | `2` ✅ / `3` / `4` / `5` / `6` / `7` / `8` / `9` | `nestopia_turbo_pulse` | `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` |
| Supprimer la limite de sprites | `Désactivé` ✅ / `Activé` | `nestopia_nospritelimit` | `disabled` / `enabled` |
| Vitesse du processeur (overclock) | `1x` ✅ / `2x` | `nestopia_overclock` | `1x` / `2x` |
| État de la RAM à la mise sous tension | `0x00` ✅ / `0xFF` / `Aléatoire` | `nestopia_ram_power_state` | `0x00` / `0xFF` / `random` |

## ![](/emulators/external-links.png) Enlaces externos

* **Código fuente utilizado** : [https://github.com/libretro/nestopia/](https://github.com/libretro/nestopia/)
* **Documentación Libretro** : [https://docs.libretro.com/library/nestopia\_ue/](https://docs.libretro.com/library/nestopia_ue/)