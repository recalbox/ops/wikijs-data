---
title: Libretro ECWolf
description: 
published: true
date: 2024-07-13T14:06:13.805Z
tags: libretro, wolfenstein3d, ecwolf, wolfenstein 3d
editor: markdown
dateCreated: 2023-09-12T06:35:04.582Z
---

**Freero ECWolf** es un motor de juego de puerto fuente para Wolfenstein 3D. Este port es cortesía de phcoder.

ECWolf es un port del motor Wolfenstein 3D basado en Wolf4SDL. Combina el motor original de Wolfenstein 3D con la experiencia de usuario de ZDoom para crear el port de Wolf3D más fácil de usar y modificar.

Al igual que ZDoom, ECWolf pretende dar soporte a todos los juegos que utilizan el motor Wolfenstein 3D, incluyendo Blake Stone (que llegará en ECWolf 3.0), Corridor 7, Operation Body Count, Rise of the Triad y Super 3D Noah's Ark. ECWolf también soportará Macintosh Wolfenstein 3D con todas sus misiones creadas por el usuario (próximamente en ECWolf 2.0).

* Un único binario ejecuta todos los juegos compatibles. (Wolfenstein 3D, Spear of Destiny, ...)
* Soporte completo para modos de alta resolución con corrección de la relación de aspecto, incluyendo soporte para pantalla panorámica.
* Esquemas de control modernos (WASD + ratón).
* Mapa automático estilo Mac Wolf / S3DNA / ROTT.
* Ubicaciones de copia de seguridad ilimitadas.
* En realidad está basado en el motor Wolf3D en lugar de recrearlo o forzarlo en un motor más moderno.
* Software renderizado usando la misma difusión de rayos de 8 bits.

ECWolf puede ejecutar los siguientes contenidos:

* Wolfenstein 3D
* Lanza del Destino
* Super 3D Noah's Ark

## ![](/emulators/license.svg) Licencia

El core posee la siguiente licencia [**GPLv2**](https://github.com/libretro/ecwolf/blob/master/docs/license-gpl.txt).

## ![](/emulators/compatibility.png) Compatibilidad

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | 
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | 

## ![](/emulators/features.png) Funcionalidades



## ![](/emulators/bios.svg) BIOS

>**No necesita bios.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Extensiones de roms soportadas

Las roms deben tener las extensiones siguientes:

* .zip
* .pk3

### Emplazamiento

Disponed vuestras roms como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 ports
┃ ┃ ┃ ┃ ┣ 📁 Wolfenstein 3D
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **ecwolf.pk3**

## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

>Para conservar las configuraciones personalizadas cuando se produce una actualización del sistema Recalbox, os aconsejamos utilizar las [Sobrecargas de configuración](./../../../advanced-usage/configuration-override).
{.is-info}

### Acceder a las opciones

Podéis configurar las opciones del core de dos formas diferentes.

* Via el Menú RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via el fichero `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Opciones del core

| Opción | Valor de la opción | Variable (para la sobrecarga) | Valores posibles |
| --- | --- | --- | --- |
| Internal resolution | `320x200` ✅ / `320x240` / `400x240` / `420x240` / `480x270` / `640x360` / `640x400` / `640x480` / `800x500` / `960x540` / `960x600` / `1024x768` / `1280x720` / `1280x800` / `1600x1000` / `1920x1200` / `2240x1400` / `2560x1600` | `ecwolf-resolution` | `320x200` / `320x240` / `400x240` / `420x240` / `480x270` / `640x360` / `640x400` / `640x480` / `800x500` / `960x540` / `960x600` / `1024x768` / `1280x720` / `1280x800` / `1600x1000` / `1920x1200` / `2240x1400` / `2560x1600` |
| Analog deadzone | `0%` / `5%` / `10%` / `15%` ✅ / `20%` / `25%` / `30%` | `ecwolf-analog-deadzone` | `0` / `5` / `10` / `15` / `20` / `25` / `30` |
| Refresh rate (FPS) | `7` / `7.8` / `8.8` / `10` / `14` / `17.5` / `25` / `30` / `35` ✅ / `50` / `60` / `70` / `72` / `75` / `90` / `100` / `119` / `120` / `140` / `144` / `240` / `244` / `300` / `360` | `ecwolf-fps` | `7` / `7.8` / `8.8` / `10` / `14` / `17.5` / `25` / `30` / `35` / `50` / `60` / `70` / `72` / `75` / `90` / `100` / `119` / `120` / `140` / `144` / `240` / `244` / `300` / `360` |
| Preferred palette format (Restart) | `RGB565 (16-bit)` ✅ / `XRGB8888 (24-bit)` | `ecwolf-palette` | `rgb565` / `xrgb8888` |
| Always run | `Désactivé` ✅ / `Activé` | `ecwolf-alwaysrun` | `disabled` / `enabled` |
| Screen size | `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `Largest with statusbar` ✅ / `Without statusbar` | `ecwolf-viewsize` | `4` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `21` |
| Show map as overlay | `Désactivé` ✅ / `Activé` / `On + Normal` | `ecwolf-am-overlay` | `off` / `on` / `both` |
| Rotate map | `Désactivé` ✅ / `Activé` / `Overlay only` | `ecwolf-am-rotate` | `off` / `on` / `overlay_only` |
| Textures walls in automap | `Désactivé` ✅ / `Activé` | `ecwolf-am-drawtexturedwalls` | `disabled` / `enabled` |
| Textured floors in automap | `Désactivé` ✅ / `Activé` | `ecwolf-am-drawtexturedfloors` | `disabled` / `enabled` |
| Textured Overlay in automap | `Désactivé` ✅ / `Activé` | `ecwolf-am-texturedoverlay` | `disabled` / `enabled` |
| Show level ratios in automap | `Désactivé` ✅ / `Activé` | `ecwolf-am-showratios` | `disabled` / `enabled` |
| Pause game in automap | `Désactivé` ✅ / `Activé` | `ecwolf-am-pause` | `disabled` / `enabled` |
| Volume of music | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` ✅ | `ecwolf-music-volume` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` |
| Volume of digitized sound effects | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` ✅ | `ecwolf-digi-volume` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` |
| Volume of Adlib sound effects | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` ✅ | `ecwolf-adlib-volume` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` |
| Volume of Speaker sound effects | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` ✅ | `ecwolf-speaker-volume` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` |
| Analog move and strafe sensitivity | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` ✅ | `ecwolf-analog-move-sensitivity` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` |
| Analog turn sensitivity | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` ✅ | `ecwolf-analog-turn-sensitivity` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` |
| Order of lookup for effects | `Digitized, Adlib, Speaker` ✅ / `Digitized, Adlib` / `Digitized only` / `Adlib only` / `Speaker only` | `ecwolf-effects-priority` | `digi-adlib-speaker` / `digi-adlib` / `digi-speaker` / `digi` / `adlib` / `speaker` |
| Aspect | `Auto` ✅ / `16:9` / `4:3` / `16:10` / `17:10` / `5:4` / `21:9` | `ecwolf-aspect` | `auto` / `16:9` / `4:3` / `16:10` / `17:10` / `5:4` / `21:9` |
| Invulnerability | `Désactivé` ✅ / `Activé` | `ecwolf-invulnerability` | `disabled` / `enabled` |
| Dynamic FPS | `Désactivé` ✅ / `Activé` | `ecwolf-dynamic-fps` | `disabled` / `enabled` |
| Store files in memory | `Désactivé` ✅ / `Activé` | `ecwolf-memstore` | `disabled` / `enabled` |
| Horizontal panning speed in automap | `0` / `1` / `2` / `3` / `4` / `5` ✅ / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` | `ecwolf-panx-adjustment` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` |
| Vertical panning speed in automap | `0` / `1` / `2` / `3` / `4` / `5` ✅ / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` | `ecwolf-paxy-adjustment` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` |

## Creación de mods

* ¡Crea mods sin trabajar con el código fuente!
  * Los mods funcionan con todas las plataformas soportadas, incluyendo Windows, macOS y Linux.
* Hay soporte para texturas arbitrarias de alta resolución, planos y sprites.
* Pushwalls móviles simultáneos ilimitados.
* Cosas ilimitadas.
* Tamaño de mapa ilimitado (aunque limitaciones técnicas restringen GAMEMAPS a 181x181).
* Utiliza scripts y formatos de datos similares a ZDoom.
  * Las utilidades de edición de Doom funcionan con ECWolf (excepto para niveles).

Tened en cuenta que hasta ECWolf 2.0, aunque no se excluyen cambios radicales, no se preservará necesariamente la compatibilidad hacia atrás de los mods. Leed [la wiki](http://maniacsvault.net/ecwolf/wiki/Version_compatibility) para más información.

## ![](/emulators/external-links.png) Enlaces externos

* **Código fuente utilizado** : [https://github.com/libretro/ecwolf/](https://github.com/libretro/ecwolf)
* **Página web oficial** : [http://maniacsvault.net/ecwolf/](http://maniacsvault.net/ecwolf/)
* **Foro oficial** : [https://forum.drdteam.org/viewforum.php?f=174](https://forum.drdteam.org/viewforum.php?f=174)
* **Documentación oficial** : [http://maniacsvault.net/ecwolf/wiki/Main_Page](http://maniacsvault.net/ecwolf/wiki/Main_Page)