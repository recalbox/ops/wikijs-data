---
title: Libretro QuickNES
description: 
published: true
date: 2024-06-22T23:26:23.795Z
tags: libretro, nes, quicknes
editor: markdown
dateCreated: 2021-05-21T08:24:36.957Z
---



## ![](/emulators/license.svg) Licence

Ce core est sous licence [**LGPLv2.1+**](https://github.com/kode54/QuickNES/blob/master/COPYING).

## ![](/emulators/compatibility.png) Compatibilité

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Fonctionnalités

| Fonctionnalité | Supporté |
| :---: | :---: |
| Redémarrage | ✔ |
| Captures d'écran | ✔ |
| Sauvegardes | ✔ |
| Sauvegardes instantanées | ✔ |
| Rembobinage | ✔ |
| Netplay | ✔ |
| Options du core | ✔ |
| RetroAchievements | ✔ |
| Controls | ✔ |
| Remapping | ✔ |
| Softpatching | ✔ |

## ![](/emulators/bios.svg) BIOS

>**Aucun bios n'est requis.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Extensions supportées

Les roms doivent avoir l'extension suivante :

* .nes
* .unf
* .unif
* .zip
* .7z

Ce système supporte les roms compressées au format .zip/.7z. Attention toutefois, il ne s'agit que d'une archive.

Les fichiers contenus dans les .zip/.7z doivent correspondre aux extensions citées précédemment.
De plus, chaque fichier .zip/.7z ne doit contenir qu'une seule rom compressée.

### Emplacement

Placez les roms comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 nes
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.zip**

>Les roms au format **No-Intro** sont vivement conseillées.
{.is-success}

>Pour plus d'information sur les roms, rendez-vous sur [ce tutoriel](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuration avancée de l'émulateur

>Pour pouvoir conserver vos configurations personnalisées lors d'une mise à jour, nous vous conseillons d'utiliser notre fonctionnalité [Surcharges de configuration](./../../../advanced-usage/configuration-override).
{.is-info}

### Accéder aux options

Vous pouvez configurer diverses options de deux façons différentes.

* Via le menu RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via le fichier `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Options du core

| Options | Valeur des options | Variable (pour surcharge) | Valeurs des variables |
| --- | --- | --- | --- |
| Rapport d'aspect | `PAR (rapport d'aspect des pixels)` ✅ / `4:3` | `quicknes_aspect_ratio_par` | `PAR` / `4:3` |
| Afficher le surbalayage horizontal | `Activé` ✅ / `Désactivé` | `quicknes_use_overscan_h` | `enabled` / `disabled` |
| Afficher le surbalayage vertical | `Désactivé` ✅ / `Activé` | `quicknes_use_overscan_v` | `disabled` / `enabled` |
| Palette de couleurs | `default` ✅ / `Réelle par AspiringSquire` / `Console virtuelle Nintendo` / `PPU RVG Nintendo` / `YUV-V3 de FBX` / `Finale non saturée de FBX` / `Sony CXA2025AS US` / `PAL` / `Finale 2 de BMF` / `Finale 3 de BMF` / `Lisse de FBX` / `Composite directe de FBX` / `Style PVM D93 de FBX` / `Matériel NTSC de FBX` / `NES Classic (corrigée) de FBX` / `NESCAP de RGBSource` / `Wavebeam de nakedarthur` | `quicknes_palette` | `default` / `asqrealc` / `nintendo-vc` / `rgb` / `yuv-v3` / `unsaturated-final` / `sony-cxa2025as-us` / `pal` / `bmf-final2` / `bmf-final3` / `smooth-fbx` / `composite-direct-fbx` / `pvm-style-d93-fbx` / `ntsc-hardware-fbx` / `nes-classic-fbx-fs` / `nescap` / `wavebeam` |
| Aucune limite de sprites | `Désactivé` ✅ / `Activé` | `quicknes_no_sprite_limit` | `disabled` / `enabled` |
| Mode audio | `Non linéaire` ✅ / `Linéaire` / `Panoramique stéréo` | `quicknes_audio_nonlinear` | `nonlinear` / `linear` / `stereo panning` |
| Préréglage de l'égaliseur audio | `Default` ✅ / `Famicom` / `TV` / `Plat` / `Net` / `Métallique` | `quicknes_audio_eq` | `default` / `famicom` / `tv` / `flat` / `crisp` / `tinny` |
| Activer le turbo | `None` ✅ / `Joueur 1` / `Joueur 2` / `Joueur 1 & 2` | `quicknes_turbo_enable` | `none` / `player 1` / `player 2` / `both` |
| Largeur d'impulsion du turbo (en images) | `1` / `2` / `3` ✅ / `5` / `10` / `15` / `30` / `60` | `quicknes_turbo_pulse_width` | `1` / `2` / `3` / `5` / `10` / `15` / `30` / `60` |
| Autoriser les directions opposées | `Désactivé` ✅ / `Activé` | `quicknes_up_down_allowed` | `disabled` / `enabled` |

## ![](/emulators/external-links.png) Liens externes

* **Code source utilisé** : [https://github.com/libretro/QuickNES_Core/](https://github.com/libretro/QuickNES_Core/)
* **Documentation Libretro** : [https://docs.libretro.com/library/quicknes/](https://docs.libretro.com/library/quicknes/)