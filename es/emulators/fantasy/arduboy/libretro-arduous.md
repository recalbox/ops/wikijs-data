---
title: Libretro Arduous
description: 
published: true
date: 2024-07-14T19:15:48.772Z
tags: libretro, 9.1+, arduous
editor: markdown
dateCreated: 2023-09-11T06:10:35.750Z
---

**Libretro Arduous** est un port de l'**Arduous**.

## ![](/emulators/license.svg) Licencia

El core posee la siguiente licencia [**GPLv3**](https://github.com/libretro/arduous/blob/main/COPYING).

## ![](/emulators/compatibility.png) Compatibilidad

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | 
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Funcionalidades

| Funcionalidad | ¿Soportada? |
| :---: | :---: |

## ![](/emulators/bios.svg) BIOS

>**No necesita bios.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Extensiones de roms soportadas

Las roms deben tener las extensiones siguientes:

* .hex

### Emplazamiento

Disponed vuestras roms como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 arduboy
┃ ┃ ┃ ┃ ┣ 🗒  **jeu.hex**

## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

>**Este core no tiene opciones.**
{.is-success}

## ![](/emulators/external-links.png) Enlaces externos

* **Código fuente utilizado** : [https://github.com/libretro/arduous/](https://github.com/libretro/arduous/)