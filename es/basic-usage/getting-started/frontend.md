---
title: EmulationStation
description: 
published: true
date: 2024-11-17T15:58:25.373Z
tags: frontend
editor: markdown
dateCreated: 2023-09-04T14:55:00.224Z
---

## Presentación

Cuando Recalbox arranca, entráis en la interfaz de un programa que se llama **EmulationStation** (ES).

Esta interfaz os permite navegar entre los diferentes sistemas y juegos, además de ejecutarlos, y a través de sus menús permite configurar diferentes opciones de la interfaz y actualizar vuestra Recalbox.

### Lista de sistemas

La pantalla de inicio es la pantalla que lista los diferentes sistemas. Muestra todas las consolas y sistemas disponibles.

![](/basic-usage/getting-started/es1.png){.full-width}

Cuando seleccionáis un sistema, la pantalla cambia y accedéis a la lista de todos los juegos de dicho sistema.

### Lista de juegos

La lista de juegos presenta todas vuestras roms en la parte izquierda y detalles de cada juego en la parte de la derecha.

Cuando se lanza un juego, consultad la sección [Durante el juego](./../../basic-usage/getting-started/special-commands/in-game) si queréis conocer las opciones disponibles durante el juego.

## Opciones del menú de EmulationStation

Cuando pulsáis el botón `START`, aparece un menú que permite ajustar la configuración de vuestra Recalbox.

![](/basic-usage/getting-started/es2.png){.full-width}

### Kodi Media Center

* Permite arrancar **Kodi Media Center** (anteriormente conocido como `XBMC`).

Podéis lanzar Kodi desde la pantalla de inicio pulsado el botón `X` de vuestro mando.

Para salir de Kodi, seleccionad `QUIT` dentro del mismo, y volveréis a la interfaz de EmulationStation.

Los mandos se encuentran soportados por Kodi por defecto, pero si lo preferís, podéis utilizar el HDMI-CEC (protocolo que permite usar el mando de vuestra TV para controlar Kodi) o una aplicación para teléfonos móviles de control a distancia de Kodi.

Para más información sobre Kodi, consultad los siguientes tutoriales:

[Kodi Media Center](./../features/kodi-media-center)
[Utilización de un mando infrarojos](./../features/kodi-media-center/remote-control-usage)

### Parámetros del sistema

En este menú accedéis a las informaciones básicas de vuestra Recalbox.

* **Versión de Recalbox** : la versión de Recalbox actualmente instalada. 
* **Espacio de disco** : espacio de almacenamiento utilizado/disponible.  
* **Media de almacenamiento** : lista los medias de almacenamiento disponibles:
  * Partición share interna (1er disco duro utilizado por el sistema para el primer arranque).
  * Cualquier otro almacenamiento externo (todos los otros discos duros y/o llaves USB detectados).
* **Idioma** : el idioma de los menús de EmulationStation. 
* **Teclado** : el tipo de teclado que queréis utilizar.

### Actualización

Este menú propone activar o no las actualizaciones. Podéis eligir si queréis instalar únicamente las versiones estables o si preferís las versiones betas de Recalbox.

* **Comprobar las actualizaciones** : comprueba si una actualización se encuentra disponible.
* **Actualización disponbile** : os indica si existe o no una actualización del sistema disponible.
* **Novedades de la actualización** : os indica la lista de novedades presentes en la nueva versión disponible.
* **Lanzar la actualización** : ejecuta el proceso de actualización.

### Opciones de los juegos

Este menú propone las siguientes configuraciones:

* **Formato de los juegos** :
  * Auto
  * Configuración RetroArch
  * El del core 
  * No configurar
  * Pixels cuadrados
  * Propio a RetroArch
  * 1/1
  * 16/10
  * 16/15
  * 16/9
  * 19/12
  * 19/14
  * 2/1
  * 21/9
  * 3/2
  * 3/4
  * 30/17
  * 32/9
  * 4/1
  * 4/3
  * 5/4
  * 6/5
  * 7/9
  * 8/3
  * 8/7
  * 9/16 
* **Suavizar los juegos** : aplica mediante un shader un efecto a los gráficos de los juegos para suavizar las esquinas de los sprites, para que el conjunto sea más agradable visualmente en las televisiones modernas. 
* **Rebobinado** : permite ir hacia atrás en los juegos (rebobinar).

>El rebobinado ralentiza ciertos emuladores (PS1, Dreamcast, ...).
>Podéis desactivar esta opción globalmente aquí y activarla en cada emulador desde el menú de [Parámetros avanzados](#parametres-avances). 
{.is-warning}

* **Softpatching**: permite aplicar uno o varios patches a un juego. Para más información, consultad su [página dedicada](../.../features/softpatching).
* **Mostrar los saves states al lanzar un juego**: os permite continuar el juego seleccionado un save-state.
* **Grabación/Carga auto** : permite grabar el juego al salir de él y recuperar la última grabación cuando se lanza, todo de forma automática. 
* **Apretard dos veces para quitar el juego** : sirve para confirmar que realmente queremos salir.
* **Ajustar a la escala (pixel perfecto)** : muestra los juegos en su resolución de origen, sin agrandarla.
* **Shaders** : aplica un filtro en la imagen del juego para dar un efecto en particular (blanco y negro, imagén con líneas verticales como los antiguos monitores CRT, otros colores, etc).
* **Shaders predefinidos** : podéis configurar fácilmente los _shaders_ disponibles en los diferentes sistemas:
  * Los shaders **scanlines** activan "scanlines" para simular una pantalla CRT. 
  * Los shaders **retro** son un paquete de shaders que contiene el mejor shader existente para cada sistema. Los shaders de este paquete son elegidos por la comunidad Recalbox y os aportan una experiencia en juego muy similar a la experiencia original, para cada sistema !
  Para obtener más información, consultad el tutorial [Configuración de shaders](./../../tutorials/games/generalities/shaders-configuration).

>También podéis cambiar de _shader_ durante el juego utilizando vuestro mando. Haced uso de los [Commandos especiales](./special-commands/in-game) `Hotkey` + `R2` o `Hotkey` + ` L2` para cambiar de shader hacia delante o hacia atrás.
{.is-info}

* **Shaders avanzados**: podéis aplicar un filtro a la imagen de vuestro juego para darle un efecto especial, como un aspecto más retro.
* **Modo HD** : 
* **Modo panorámico (16/9)** : 
* **Habilitar el driver Vulkan**: habilita el driver gráfico Vulkan. Sólo es compatible con Atomiswave, Dreamcast, Naomi, Naomi 2 (sólo PC), Naomi GD y PSP (libretro-ppsspp).
* **Modo Game Boy**: permite ejecutar juegos de la consola Game Boy en modo Super Game Boy.
* **Opciones de Retroachievements** : configuración de Retroachievements (Sistema de recompensas/trofeos). 
* **Options NetPlay** : configuración del NetPlay (juego online).
* **No mostrar los juegos preinstalados** : permite esconder los juegos que vienen preinstalados con Recalbox.

### Descarga de contenidos

Esta opción os permite descargar juegos de forma 100% legal para los siguientes sistemas:

* TIC-80
* Uzebox
* Vectrex
* WASM-4

### Configuración de mandos/controladores

Aquí podéis configurar vuestro mando de juego.

* **Configurar un mando** : permite mapear los botones de vuestro mando a acciones.
* **Asociar un mando Bluetooth** : permite asociar un mando inhalámbrico a vuestra Recalbox.
* **Olvidar los mandos Bluetooth** : Rompe la asociación de todos los mandos inhalámbricos.
* **Activar el emparejamiento al inicio**: permite emparejar nuevos mandos inhalámbricos al inicio.
* **Piloto** : permite cambiar el driver Linux de detección de mandos. Normalmente no hay que tocar esta opción si conseguís configurar vuestro mando y si éste funciona correctamente.
* **Mostrar siempre el OSD de los mandos**: muestra un pequeño icono de joystick en la parte superior izquierda de la pantalla, indicando visualmente su utilización y indicando qué joystick está en qué posición cuando se utilizan varios a la vez. En algunos mandos, también puede verse el nivel de batería.
* **Tipo de OSD de los mandos**: permite elegir la imagen de los mandos.

### Opciones de la interfaz

Este menú propone modificar la configuración de la interfaz en sí misma.

* **Salvapantallas**: Permite cambiar el salvapantallas con diferentes opciones.
* **Themes**: una selección de temas creados por la comunidad (estos nuevos temas no están disponibles por defecto con Recalbox).
* **Configuración de themes**: algunos de los temas instalados tienen opciones de configuración.
* **Ordenación del sistema**: permite cambiar la ordenación por defecto del sistema.
* **Selección rápida de sistema**: permite cambiar de un sistema a otro utilizando las teclas izquierda o derecha dentro de las listas de juego.
* **Ventana emergente de ayuda**: muestra una ventana emergente de ayuda en la parte superior derecha de la pantalla para cada opción pulsando el botón `Y`.
* **Invertir botones de validación/cancelación**: invierte los botones de validación y cancelación de tus joysticks.
* **Reloj del menú**: Muestra la hora actual en los menús de EmulationStation.
* **Configuración de las ventanas emergentes**: permite cambiar las opciones de las ventanas emergentes de cambio de música y de NetPlay.


>Los popups que informan de las actualizaciones no son configurables y siempre se muestran.
{.is-info}

* **Filtros de juego**: permite aplicar ciertos filtros a determinadas acciones:
  * **Mostrar sólo la última versión (beta)**: permite mostrar el juego más reciente. Por ejemplo, si tienes un juego marcado como `Rev1` y otro como `Rev3`, sólo se mostrará `Rev3` en la lista de juegos.
  * **Mostrar sólo favoritos**: permite mostrar sólo los juegos favoritos en los sistemas.
  * **Mostrar juegos ocultos**: muestra los juegos que han sido ocultados.
  * **Ocultar juegos de Mahjong y Casino**: permite ocultar juegos arcade como mahjong y casino.
  * **Ocultar juegos para adultos**: Oculta los juegos para adultos. Funciona después de desechar tus juegos.
  * **Ocultar juegos preinstalados**: permite ocultar los juegos suministrados con Recalbox.
  * **Mostrar sólo juegos con 3+ jugadores**: sólo muestra los juegos que se pueden jugar con 3 o más jugadores.
  * **Mostrar sólo juegos Yoko (horizontal)**: sólo muestra los juegos que se juegan en una pantalla horizontal.
  * **Mostrar sólo juegos de Tate (vertical)**: sólo muestra juegos jugados en una pantalla vertical.
  * **Ocultar los no-juegos**: oculta las roms que no son juegos.
* **Mostrar por nombre de fichero**: permite mostrar los juegos por su nombre de fichero.
* **Actualizar lista de juegos**: si habéis añadido o eliminado juegos, deberéis actualizar la lista de juegos para que los cambios surtan efecto.

### Configuración Arcade

Este menú permite modificar las opciones relativas a los juegos arcade.

* **Habilitar vista mejorada**: habilita una vista mejorada para los sistemas arcade.
* **Ocultar clones predeterminados**: permite ocultar los juegos arcade reconocidos como clones.
* **Ocultar bios**: oculta las bios de los juegos arcade.
* **Ocultar juegos no funcionales**: oculta los juegos reconocidos como no funcionales.
* **Usar siempre nombres oficiales**: muestra los nombres oficiales en lugar de los nombres de archivos o desechos.
* **Sistemas virtuales de fabricantes**: permite elegir los fabricantes de juegos acade que se desean añadir a la lista de sistemas.
* **Sistema arcade todo en uno**: te permite cambiar algunas opciones adicionales de arcade.

### Ajustes de Tate

Este menú te permite cambiar las opciones de los juegos que se pueden jugar con una pantalla a la vertical.

* **Activar el sistema virtual tate**: permite mostrar el sistema virtual tate en la lista de sistemas.
* **Mostrar sólo juegos tate en las listas de juegos**: permite mostrar en los sistemas sólo los juegos que se pueden jugar en pantalla vertical.
* **Rotación de juegos**: permite elegir la rotación que se aplicará a los juegos tate.
* **Rotación completa del sistema**: rota toda la interfaz.


### Opciones de sonido

Este menú le ofrece las siguientes opciones:

* **Volumen del sistema**: ajusta el volumen de todo el sistema.
* **Volumen de la música**: ajusta el volumen del sonido en la interfaz.
* **Modo de audio**: permite elegir qué sonidos quieres escuchar en la interfaz.
* **Salida de audio**: selecciona la salida de audio.
* **Asociar dispositivos de audio Bluetooth**: permite asociar un altavoz o unos auriculares Bluetooth.


### Opciones de red

Este menú ofrece las siguientes opciones:

* **Estado**: muestra si está conectado o no.
* **Dirección IP**: permite obtener la dirección IP de tu Recalbox.
* **Activar wifi**: permite activar o desactivar el wifi.
* **wifi ssid**: permite introducir el nombre de tu red wifi.
* **wifi key**: permite introducir tu contraseña wifi.
* **Network Name**: permite cambiar el nombre de tu Recalbox en tu red local.
* **Conexión automática WPS**: permite activar la conexión WPS.

### Scrapeador

Para cada juego, puedes obtener muchas informaciones (carátula, resumen, etc...) que se mostrarán en el menú de selección de juegos de EmulationStation, en la parte de la derecha de la pantalla.

Pulsad `START` y id a `SCRAPER`. Seguid las instrucciones en pantalla. Encontraréis más información sobre el scraper interno [aquí](./../features/internal-scraper).

### Parámetros avanzados

* **Overclock** : configura el overclocking.
  * AUCUN
  * HIGH
  * TURBO
  * EXTREM
  * NOLIMIT

>Los ajustes TURBO y EXTREMO pueden causar daños irreversibles en los Raspberry Pis si no se realizan en las condiciones adecuadas (disipación de calor, ventilación, fuente apropiada, etc.). La garantía del hardware puede quedar anulada.
{.is-danger}

* **Parámetros de arranque** : permite configurar algunos ajustes de arranque.
* **Sistemas virtuales**: permite mostrar sistemas virtuales en la lista de sistemas.
* **CRT Recalbox**: permite modificar algunas opciones relativas a la utilización de una pantalla CRT, como RGB Dual.
* **Resoluciones**: permite elegir varias resoluciones para aplicar. Esta opción sólo está disponible para PC.
* **Configuración avanzada del emulador**: permite configurar las opciones del emulador independientemente unas de otras. 
* **Opciones de Kodi**: permite activar/desactivar Kodi, iniciar automáticamente Kodi cuando se inicia Recalbox o activar el acceso directo para iniciar Kodi desde la lista del sistema.
* **Gestión de dispositivos**: permite seleccionar el dispositivo utilizado si tiene un script de apagado seguro.
* **Mostrar los FPS**: permite mostrar la tasa de fotogramas (fotogramas por segundo) en la interfaz y en los juegos.
* **Gestor web Recalbox**: permite activar o desactivar el gestor web Recalbox.
* **Actualización del gestor de arranque**: le permite actualizar el gestor de arranque de su Raspberry Pi 4 / 400 / 5.
* **Restaurar configuración de fábrica**: restablece tu Recalbox al mismo estado que si fuera una nueva instalación. Esto no afecta a tus datos personales de ninguna manera.

### Comprobación de bios

Este menú os permite gestionar las bios requeridas por algunos de los emuladores de Recalbox. Más información en [esta página](./../features/bios-manager).

### Licencia open-source

Aquí podéis consultar el contrato de licencia open-source de Recalbox.

### Salir

Menú para **apagar Recalbox adecuadamente**.

* Apagar
* Apagado rápido
* Reiniciar

## Controles

#### Los botones del mando en la interfaz de EmulationStation :

* `A` → Seleccionar
* `B` → Volver
* `Y` → Favoritos si estás en la lista de juegos, lanza los clips vídeo si estás en la lista de sistema.
* `X` → Lanza Kodi o el lobby Netplay si te encuentras en la lista de sistemas.
* `Start` → Menú
* `Select` → Opciones para salir si estás dentro de la lista de sistemas, muestra los favoritos del sistema si estás en la lista de juegos.
* `R` → En la lista de juegos avance rápido, en la lista de sistemas abre la función global de búsqueda.
* `L` → En la lista de jeugos retroceso rápido.

## Los favoritos

Podéis establecer un juego como favorito pulsando la tecla `Y` en el juego. El juego se colocará entonces al principio de la lista con un ☆ delante de su nombre.

>El sistema debe apagarse limpiamente utilizando el menú de EmulationStation para que la lista de juegos favoritos se guarde correctamente y puedas volver a encontrarla la próxima vez que arranques.
{.is-warning}