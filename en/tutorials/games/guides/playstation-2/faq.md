---
title: FAQ
description: 
published: true
date: 2021-11-09T20:50:15.881Z
tags: faq, ps2, 8.0+
editor: markdown
dateCreated: 2021-10-12T20:55:09.421Z
---

## Is there a set of key combinations for PCSX2?

Yes, here is the list of these combinations:

- `HK` + `Up`/`Down`: select slot
- `HK` + `X`/`Y`: load/save state
- `HK` + `Left`: toggle slow mode
- `HK` + `Right`: toggle fast mode
- `HK` + `L1`: cycle in ratio (4:3, 16:9, fullscreen stretch)
- `HK` + `R1`: screenshot
- `Select` + `Start`: quit PCSX2.