---
title: Libretro PokeMini
description: 
published: true
date: 2024-07-25T00:30:38.563Z
tags: libretro, pokémon, mini, pokémini
editor: markdown
dateCreated: 2021-07-27T16:06:02.784Z
---

**Libretro PokeMini** is an emulator for the Nintendo Pokémon Mini handheld system developed in C and C++.

It is written by **JustBurn**.

## ![](/emulators/license.svg) License

This core is under [**GPLv3**](https://github.com/libretro/PokeMini/blob/master/LICENSE) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI3 | RPI4/400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Features

| Feature | Supported |
| :---: | :---: |
| Restart | ✔ |
| Saves | ✔ |
| States | ✔ |
| Rewind | ✔ |
| Core Options | ✔ |
| Retroachievements | ✔ |
| Controls | ✔ |
| Remapping | ✔ |
| Vibration | ✔ |

## ![](/emulators/bios.svg) BIOS

### List of optional bios

| Filename | Description | MD5 | Provided |
| :---: | :---: | :---: | :---: |
| bios.min | Pokémon Mini BIOS | 1e4fb124a3a886865acb574f388c803d | ❌ |

### Location

Put the bios like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 pokemini
┃ ┃ ┃ ┃ ┣ 🗒 **bios.min**

## ![](/emulators/roms.png) Roms

### Supported extensions

Roms must have the extension:

* .min
* .zip

This system supports compressed roms in .zip format. However, be careful, it is only an archive.

The files contained in the .zip must correspond to the extensions mentioned above.
Moreover, each .zip file must contain only one compressed rom.

### Location

Put the roms like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 pokemini
┃ ┃ ┃ ┃ ┣ 🗒 **game.zip**

>The roms using **No-Intro** format are highly recommended.
{.is-success}

>For more information about roms, go to [this tutorial](./../../../tutorials/games/generalities/isos-and-roms)!
{.is-info}

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Accessing to options

You can configure various options in two different ways.

* Via RetroArch menu:

┣ 📁 RetroArch Menu
┃ ┣ 📁 Core options
┃ ┃ ┣ 🧩 Name_option

* Via the file `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core options

| Options | Options values | Variables (for configuration override) | Variables values |
| --- | --- | --- | --- |
| Video Scale (Restart) | `1x` / `2x` / `3x` / `4x` ✅ / `5x` / `6x` | `pokemini_video_scale` | `1x` / `2x` / `3x` / `4x` / `5x` / `6x` |
| 60Hz Mode | `Disabled` / `Enabled` ✅ | `pokemini_60hz_mode` | `disabled` / `enabled` |
| LCD Filter | `Dot Matrix` ✅ / `Scanlines` / `None` | `pokemini_lcdfilter` | `dotmatrix` / `scanline` / `none` |
| LCD Mode | `Analog` ✅ / `3 Shades` / `2 Shades` | `pokemini_lcdmode` | `analog` / `3shades` / `2shades` |
| LCD Contrast | `0` / `16` / `32` / `48` / `64` ✅ / `80` / `96` | `pokemini_lcdcontrast` | `0` / `16` / `32` / `48` / `64` / `80` / `96` |
| LCD Brightness | `-80` / `-60` / `-40` / `-20` / `0` ✅ / `20` / `40` / `60` / `80` | `pokemini_lcdbright` | `-80` / `-60` / `-40` / `-20` / `0` / `20` / `40` / `60` / `80` |
| Palette | `Default` ✅ / `Old` / `Black & White` / `Green` / `Inverted Green` / `Red` / `Inverted Red` / `Blue LCD` / `LED Backlight` / `Girl Power` / `Blue` / `Inverted Blue` / `Sepia` / `Inverted Black & White` | `pokemini_palette` | `Default` / `Old` / `Monochrome` / `Green` / `Green Vector` / `Red` / `Red Vector` / `Blue LCD` / `LEDBacklight` / `Girl Power` / `Blue` / `Blue Vector` / `Sepia` / `Monochrome Vector` |
| Piezo Filter | `Enabled` ✅ / `Disabled` | `pokemini_piezofilter` | `enabled` / `disabled` |
| Low Pass Filter | `Désactivé` ✅ / `Activé` | `pokemini_lowpass_filter` | `disabled` / `enabled` |
| Low Pass Filter Level (%) | `5` / `10` / `15` / `20` / `25` / `30` / `35` / `40` / `45` / `50` / `55` / `60` ✅ / `65` / `70` / `75` / `80` / `85` / `90` / `95` | `pokemini_lowpass_range` | `5` / `10` / `15` / `20` / `25` / `30` / `35` / `40` / `45` / `50` / `55` / `60` / `65` / `70` / `75` / `80` / `85` / `90` / `95` |
| Screen Shake Level | `0` / `1` / `2` / `3` ✅ | `pokemini_screen_shake_lv` | `0` / `1` / `2` / `3` |
| Controller Rumble Level | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` ✅ | `pokemini_rumblelvl` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` |
| Turbo Button Period | `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` ✅ / `19` / `20` / `21` / `22` / `23` / `24` / `25` / `26` / `27` / `28` / `29` / `30` / `31` / `32` / `33` / `34` / `35` / `36` / `37` / `38` / `39` / `40` / `41` / `42` / `43` / `44` / `45` / `46` / `47` / `48` / `49` / `50` / `51` / `52` / `53` / `54` / `55` / `56` / `57` / `58` / `59` / `60` / `61` / `62` / `63` / `64` / `65` / `66` / `67` / `68` / `69` / `70` / `71` / `72` / `73` / `74` / `75` / `76` / `77` / `78` / `79` / `80` / `81` / `82` / `83` / `84` / `85` / `86` / `87` / `88` / `89` / `90` / `91` / `92` / `93` / `94` / `95` / `96` / `97` / `98` / `99` / `100` / `101` / `102` / `103` / `104` / `105` / `106` / `107` / `108` / `109` / `110` / `111` / `112` / `113` / `114` / `115` / `116` / `117` / `118` / `119` / `120` | `pokemini_turbo_period` | `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `21` / `22` / `23` / `24` / `25` / `26` / `27` / `28` / `29` / `30` / `31` / `32` / `33` / `34` / `35` / `36` / `37` / `38` / `39` / `40` / `41` / `42` / `43` / `44` / `45` / `46` / `47` / `48` / `49` / `50` / `51` / `52` / `53` / `54` / `55` / `56` / `57` / `58` / `59` / `60` / `61` / `62` / `63` / `64` / `65` / `66` / `67` / `68` / `69` / `70` / `71` / `72` / `73` / `74` / `75` / `76` / `77` / `78` / `79` / `80` / `81` / `82` / `83` / `84` / `85` / `86` / `87` / `88` / `89` / `90` / `91` / `92` / `93` / `94` / `95` / `96` / `97` / `98` / `99` / `100` / `101` / `102` / `103` / `104` / `105` / `106` / `107` / `108` / `109` / `110` / `111` / `112` / `113` / `114` / `115` / `116` / `117` / `118` / `119` / `120` |

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/libretro/PokeMini/](https://github.com/libretro/PokeMini/)
* **Libretro Documentation**: [https://docs.libretro.com/library/pokemini/](https://docs.libretro.com/library/pokemini/)
* **Official source code**: [https://sourceforge.net/projects/pokemini/](https://sourceforge.net/projects/pokemini/)
* **Official documentation**: [https://sourceforge.net/p/pokemini/wiki/Home/](https://sourceforge.net/p/pokemini/wiki/Home/)