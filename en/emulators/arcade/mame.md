---
title: Mame
description: Multiple Arcade Machine Emulator
published: true
date: 2021-08-09T07:33:26.401Z
tags: arcade, mame
editor: markdown
dateCreated: 2021-07-04T18:35:13.795Z
---

![](/emulators/arcade/mame.svg){.align-center}

## History

MAME, an acronym for Multiple Arcade Machine Emulator, is a PC emulation software that aims to faithfully reproduce the functioning of arcade games and systems in order to preserve the history of video games.
It also integrates other emulators and versions, such as MESS and SDLMAME or MEWUI.
The first public release dates from February 1997 with version 0.1; we are now at v0.230 (March 2021). Today it brews more than 4000 unique games and more than 9000 romsets, with a constant contribution of new titles.
It spreads over a base of games from 1975 to nowadays.

MAME contains various components:

* several hardware emulators imitating the behavior of the processors and motherboards of the supported arcade games;
* a control emulator simulating the different devices (joystick) by keyboards or other peripherals;
* an emulator to simulate and redirect the audio and video outputs of the arcade terminals to the respective outputs of the computer.

## Emulators

[Libretro MAME](libretro-mame)
[Libretro MAME2000](libretro-mame-2000)
[Libretro MAME2003](libretro-mame-2003)
[Libretro MAME2003_Plus](libretro-mame-2003-plus)
[Libretro MAME2010](libretro-mame-2010)
[Libretro MAME2015](libretro-mame-2015)
[AdvanceMAME](advance-mame)