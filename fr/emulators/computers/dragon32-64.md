---
title: Dragon32/64
description: 
published: true
date: 2021-11-16T07:44:20.924Z
tags: dragon32, dragon64, ordinateurs, dragon data, tano corp, 8.0+
editor: markdown
dateCreated: 2021-08-21T09:40:08.815Z
---

![](/emulators/computers/dragon.svg){.align-center}

## Fiche technique

* **Fabricant** : Dragon Data
* **Année de sortie** : 1982
* **Système d'exploitation** : Microsoft Extended BASIC
* **Processeur** : Motorola 6809E @ 0.89 MHz
* **RAM** : 32/64 KB
* **Résolution** : 256 x 192 maximum, 8 couleurs
* **Son** : 1 voix, 5 octaves avec le Basic / 4 voix, 7 octaves avec le code machine

## Présentation

Les Dragon 32 et Dragon 64 sont des ordinateurs personnels construits dans les années 1980. Ces ordinateurs étaient très similaires au TRS-80 Color Computer (CoCo), ils étaient produits pour le marché européen par Dragon Data, Ltd., à Port Talbot au Pays de Galles, et pour le marché américain par Tano de La Nouvelle-Orléans en Louisiane. Les numéros de modèle constituent la principale différence entre les deux machines, 32 Ko de RAM pour le Dragon 32 et 64 Ko de RAM pour le Dragon 64.

Au début des années 1980, le marché de l'ordinateur personnel était en plein essor. De nouvelles machines furent lancées presque tous les mois. En Août 1982, Dragon rejognit la mêlée avec le Dragon 32; Dragon 64 suivit un an plus tard. Les ordinateurs se vendirent très bien au départ qui ont attiré l'intérêt de plusieurs développeurs de logiciels indépendants, notamment Microdeal. Un magazine (dragon user) a commencé à être publié peu de temps après le lancement de la machine.

Sur le marché encombré de l'ordinateur personnels, où la qualité des jeux étaient un facteur important, les Dragon ont souffert à cause de leurs capacités graphiques inférieures aux machines contemporaines comme le Sinclair ZX Spectrum et BBC Micro.

Le Dragon est également incapable d'afficher facilement les minuscules, et incapable d'afficher les caractères accentuées. La machine ne pouvait guère dépasser le stade de console de jeux, excluant le marché éducatif et "semi-professionnel".

En raison de ces limitations, le Dragon n'a pas été un grand succès commercial, et la production des dragon prit fin en Juin 1984.

## Émulateurs

[XRoar](xroar)