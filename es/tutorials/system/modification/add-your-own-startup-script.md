---
title: Añadir vuestro propio script al inicio de Recalbox
description: 
published: true
date: 2023-09-14T06:59:22.902Z
tags: script, inicio, añadir
editor: markdown
dateCreated: 2023-09-14T06:59:22.902Z
---

## Introducción

Seguid las siguientes etapas para crear un script de arranque que se ejecutará automáticamente al inicio de Recalbox.

El script debería tener dos funciones, una que será ejecutada al arranque (paramétro `start`) y otra que se ejecutará al apagado de Recalbox (`stop`).

## Utilización

Debéis crear un fichero llamado `custom.sh` dentro del directorio `/recalbox/share/system`.

Este fichero puede contener:

* Código shell
* Puede utilizar otros ficheros como ficheros Python por ejemplo.

## Test

Para probar que todo funciona bien, ejecutad el comando `/etc/init.d/S99custom start`.