---
title: Libretro Atari800
description: 
published: true
date: 2024-07-16T19:16:36.981Z
tags: libretro, atari, 800, atari-800, atari800
editor: markdown
dateCreated: 2023-09-11T08:21:26.501Z
---



## ![](/emulators/license.svg) Licencia

El core posee la siguiente licencia [**GPLv2**](https://github.com/atari800/atari800/blob/master/COPYING).

## ![](/emulators/compatibility.png) Compatibilidad

 | RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | 
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | 

## ![](/emulators/features.png) Funcionalidades



## ![](/emulators/bios.svg) BIOS

### Lista de BIOS obligatorias

| Nombre del fichero | Descripción | MD5 | ¿Preinstalada? |
| :---: | :---: | :---: | :---: |
| ATARIBAS.ROM | BASIC interpreter | 0bac0c6a50104045d902df4503a4c30b | ❌ |
| ATARIOSA.ROM | Atari 400/800 PAL | eb1f32f5d9f382db1bbfb8d7f9cb343a | ❌ |
| ATARIOSB.ROM | Atari 400/800 NTSC | a3e8d617c95d08031fe1b20d541434b2 | ❌ |
| ATARIXL.ROM | Atari XL/XE OS | 06daac977823773a3eea3422fd26a703 | ❌ |

### Emplazamiento

Disponed vuestras bios como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 atari800
┃ ┃ ┃ ┃ ┣ 🗒 **ATARIBAS.ROM**
┃ ┃ ┃ ┃ ┣ 🗒 **ATARIOSA.ROM**
┃ ┃ ┃ ┃ ┣ 🗒 **ATARIOSB.ROM**
┃ ┃ ┃ ┃ ┣ 🗒 **ATARIXL.ROM**

## ![](/emulators/roms.png) Roms

### Extensiones de roms soportadas

Las roms deben tener las extensiones siguientes:

* .atr
* .atx
* .bin
* .car
* .cas
* .com
* .dcm
* .rom
* .xex
* .xfd
* .zip

Este sistema soporta las roms comprimidas en formato .zip/.7z. No obstante, se trata sólo de un archivo.

Los ficheros contenidos en los .zip/.7z deben tener las extensiones citadas previamente.
Además, cada fichero .zip/.7z sólo debe contener un fichero rom al interior.

### Emplazamiento

Disponed vuestras roms como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 atari800
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.zip**

>Se aconsejan las roms del proyecto **TOSEC**.
{.is-success}

>Para más información sobre las roms, consultad [este tutorial](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

>Para conservar las configuraciones personalizadas cuando se produce una actualización del sistema Recalbox, os aconsejamos utilizar las [Sobrecargas de configuración](./../../../advanced-usage/configuration-override).
{.is-info}

### Acceder a las opciones

Podéis configurar las opciones del core de dos formas diferentes.

* Via el Menú RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via el fichero `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Opciones del core

| Opción | Valor de la opción | Variable (para la sobrecarga) | Valores posibles |
| --- | --- | --- | --- |
| Video Standard | `NTSC` ✅ / `PAL` | `atari800_ntscpal` | `NTSC` / `PAL` |
| Hi-Res Artifacting | `None` ✅ / `blue/brown 1` / `blue/brown 2` / `GTIA` / `CTIA` | `atari800_artifacting` | `none` / `blue/brown 1` / `blue/brown 2` / `GTIA` / `CTIA` |
| Internal resolution | `336x240` ✅ / `320x240` / `384x240` / `384x272` / `384x288` / `400x300` | `atari800_resolution` | `336x240` / `320x240` / `384x240` / `384x272` / `384x288` / `400x300` |
| Controller Hacks | `none` ✅ / `Dual Stick` / `Swap Ports` | `atari800_opt2` | `none` / `enabled` / `Swap Ports` |
| Activate Paddle Mode | `Désactivé` ✅ /  `Activé` | `paddle_active` | `disabled` / `enabled` |
| Paddle Movement Speed | `1` / `2` / `3` ✅ / `4` / `5` / `6` / `7` / `8` / `9` | `paddle_movement_speed` | `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` |
| Digital Joystick Sensitivity | `5%` / `10%` / `15%` / `20%` / `25%` / `30%` / `35%` / `40%` / `45%` / `50%` / `55%` / `60%` / `65%` / `70%` / `75%` / `80%` / `85%` / `90%` / `95%` / `100%` ✅ | `pot_digital_sensitivity` | `5` / `10` / `15` / `20` / `25` / `30` / `35` / `40` / `45` / `50` / `55` / `60` / `65` / `70` / `75` / `80` / `85` / `90` / `95` / `100` |
| Analog Joystick Sensitivity | `5%` / `10%` / `15%` / `20%` / `25%` / `30%` / `35%` / `40%` / `45%` / `50%` / `55%` / `60%` / `65%` / `70%` / `75%` / `80%` / `85%` / `90%` / `95%` / `100%` ✅ | `pot_analog_sensitivity` | `5` / `10` / `15` / `20` / `25` / `30` / `35` / `40` / `45` / `50` / `55` / `60` / `65` / `70` / `75` / `80` / `85` / `90` / `95` / `100` |
| Analog Joystick Deadzone | `0%` / `3%` / `5%` / `7%` / `10%` / `13%` / `15%` ✅ / `17%` / `20%` / `23%` / `25%` / `27%` / `30%` | `pot_analog_deadzone` | `0` / `3` / `5` / `7` / `10` / `13` / `15` / `17` / `20` / `23` / `25` / `27` / `30` |
| Retroarch Keyboard type | `poll` ✅ / `callback` | `atari800_keyboard` | `poll` / `callback` |
| Atari Keyboard Defines | `informational` ✅ | `keyboard_defines` | `informational` |
| Atari System | `Atari 400/800 (OS B)` ✅ / `Atari 800XL (64K)` / `Atari 130XE (128K)` / `Modern XL/XE(320K Comby Shop)` / `Modern XL/XE(576K)` / `Modern XL/XE(1088K)` / `Atari 5200 Super System` | `atari800_system` | `400/800 (OS B)` / `800XL (64K)` / `130XE (128K)` / `Modern XL/XE(320K CS)` / `Modern XL/XE(576K)` / `Modern XL/XE(1088K)` / `5200` |
| Internal BASIC (hold OPTION on boot) | `Désactivé` ✅ / `Activé` | `atari800_internalbasic` | `disabled` / `enabled` |
| SIO Acceleration | `Désactivé` / `Activé` ✅ | `atari800_sioaccel` | `disabled` / `enabled` |
| Boot from Cassette (Reboot) | `Désactivé` ✅ / `Activé` | `atari800_cassboot` | `disabled` / `enabled` |
| Autodetect Atari Cartridge Type (Restart) | `Désactivé` ✅ / `Activé` | `atari800_opt1` | `disabled` / `enabled` |


## ![](/emulators/external-links.png) Enlaces externos

* **Código fuente utilizado** : [https://github.com/atari800/atari800/](https://github.com/atari800/atari800/)
* **Documentación Libretro** : [https://docs.libretro.com/library/atari800/](https://docs.libretro.com/library/atari800/)