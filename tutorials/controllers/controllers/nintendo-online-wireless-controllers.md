---
title: Manettes Nintendo Switch sans fil
description: 
published: true
date: 2021-09-04T22:51:35.399Z
tags: switch, nintendo, 7.2+, manette, sans-fil
editor: markdown
dateCreated: 2021-05-21T08:35:04.419Z
---

Nous allons voir ici quelles sont les manettes Nintendo sans fil officielles qui sont compatibles et comment s'en servir.

## Quelles sont les manettes concernées ?

Les manettes Nintendo Switch utilisables nativement dans Recalbox sont les suivantes :

* [Joy-Cons](joy-cons-usage)

![](/tutorials/controllers/controllers/switch-joycons/nintendo_switch_joy-con_controllers.png)

* [Nintendo Switch Online - Super Nintendo](snes-controller-usage)

![](/tutorials/controllers/controllers/switch-joycons/snes-controller.jpg)

Dans les rubriques suivantes, vous trouverez comment synchroniser et utiliser ces manettes dans Recalbox.

## Et les manettes Nintendo Switch Online - NES ?

Ces manettes ne peuvent pas être utilisées dans Recalbox, chacune de ces manettes est reconnue comme un Joy-Con droit.