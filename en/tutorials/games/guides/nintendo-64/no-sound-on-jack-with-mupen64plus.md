---
title: No sound on jack with mupen64plus
description: 
published: true
date: 2021-10-03T22:29:56.807Z
tags: n64, nintendo, 64, sound, jack
editor: markdown
dateCreated: 2021-08-08T13:19:27.510Z
---

If you are using the headphone jack of your Raspberry Pi as a sound output and sound is not available with the N64 emulator `mupen64plus`, please follow the steps below.

* Connect via [SSH](./../../../system/access/root-access-terminal-cli) 
* Modify the `mupen64plus` configuration file with the following command:

`nano /recalbox/share/system/configs/mupen64/mupen64plus.cfg`

* Find the following line:

```text
OUTPUT_PORT = 1
```

**Change this line to this:**

```text
OUTPUT_PORT = 0
```

* Now you just have to save the file with `CTRL + X` then `Y` to confirm, and validate with `Enter`.
* Reboot your Raspberry Pi with the `reboot` command.

Once rebooted, the sound should be available.

>This modification can also be done from [WinSCP](./../../../system/access/network-access-winscp), [Cyberduck](./../../../system/access/network-access-cyberduck) or [MobaXTerm](./../../../system/access/network-access-mobaxterm) if you are allergic to the terminal
{.is-info}