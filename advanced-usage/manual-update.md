---
title: Manual and/or offline update
description: How to update Recalbox manually, and without internet connection
published: true
date: 2024-09-14T15:02:15.892Z
tags: upgrade, update, offline
editor: markdown
dateCreated: 2021-06-17T10:15:35.483Z
---

## When to use the manual update?

You may need in some cases to use a manual update of your Recalbox:
- To update a recalbox that does not have an internet connection.
- When the team asks you to test a *development* image to verify a bug fix.
- When you have a system problem (very rare).

>The following examples will be used with the Raspberry Pi 4 image, you will have to adapt these examples to your board.
> {.is-info}

To do this, you will have to get two files:
- The **image file** of Recalbox, named `recalbox-rpi4_64.img.xz`.
- The **checksum** file which contains the checksum of the image, allowing to automatically check its integrity. It has the same name as the image, but with a `.sha1` extension so in our example `recalbox-rpi4_64.img.xz.sha1`.

## Download the official image

To update your Recalbox to the latest version available in offline mode, go to https://www.recalbox.com/download/stable/allimages/ :

Then download the image corresponding to your board, and the corresponding checksum `sha1`:

![download.png](/advanced-usage/manual-update/download.png)

## Copy the files to the SD card

There is 2 ways to proceed:

## {.tabset}
### SD card on your PC

Now that you have the two files `recalbox-rpi4_64.img.xz` and `recalbox-rpi4_64.img.xz.sha1`, you just have to put them in the `update` directory of the partition named `RECALBOX` of the SD card where Recalbox is installed (location is also on `/boot/update` when your Recalbox is running):

![images-in-boot-partition.png](/advanced-usage/manual-update/images-in-boot-partition.png)

### SD card on your Recalbox still on

You can also put the two files in your Recalbox while still running. For this, you need to follow those steps:

- Mount the [boot partition on read/write](/tutorials/system/access/remount-partition-with-write-access).
- Put the two files `recalbox-rpi4_64.img.xz` and `recalbox-rpi4_64.img.xz.sha1` into the folder `/boot/update`.
- Restart.

## Launch the installation

The installation will start automatically the next time you start your Recalbox. To verify that the installation has been successful, you can display the menu with the `Start` button and see the version change.