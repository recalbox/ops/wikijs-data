---
title: Romsets for Recalbox
description: 
published: true
date: 2021-08-07T15:37:04.559Z
tags: romsets
editor: markdown
dateCreated: 2021-08-07T15:34:18.038Z
---

**Legend:**

  🥇    ==> Set to use
  🥈    ==> To complete (for hack roms, translation, etc...)
  🥉    ==> As a last resort
  🎮    ==> Recommended set for Netplay
  ❌    ==> No set available for this group
  ❎    ==> Set exists but **"Not recommended "**
  ❓    ==> Unknown

## Arcade

>For arcade, the romset is different depending on the core you are using.
{.is-info}

### Atomiswave

| Core | Romset | .dat | Architecture |
| :---: | :---: | :---: | :---: |
| **Flycast** | MAME 0.135 or higher, 0.230 recommended |  | x86_64 |

### FinalBurn

| Core | Romset | .dat | Architecture |
| :---: | :---: | :---: | :---: |
| **FBNeo** | FinalBurn Neo v1.0.0.01 |  | Pi2 / Pi3 / Odroid / x86_84 |
| **PiFBA** | fba 0.2.96.71 |  | Pi0 / Pi0w / Pi1 |

### Laserdisc (Daphne)

* Some games can be directly and legally downloaded by the DaphneLoader tool of the [Windows distribution of Daphne](http://www.daphne-emu.com/site3/index_hi.php). 
* For others (Dragon's Lair, Dragon's Lair II, Space Ace, ...), you must prove that you have a valid license of the game (DVD version for example).

Once downloaded, copy the necessary files (ROM and laser disc image files) to your Recalbox, as explained below.

### Mame

| Core | Romset | .dat | Architecture |
| :---: | :---: | :---: | :---: |
| AdvanceMame | MAME 0.106 |  | All Pi |
| Mame2000 | MAME 0.37b5 |  | Pi1 |
| Mame2003 | MAME 0.78 |  | All |
| Mame2003Plus  | MAME 0.78-0.188 |  | All |
| Mame2010 | MAME 0.139 |  | Pi2 / Pi3 / x86 / x86_64 |
| Mame2015 | MAME 0.160 |  | Pi3 / x86 / x86_64 |
| Mame | MAME 0.230 |  | X86 / x86_64 |

### Neo-Geo

| Core | Romset | .dat | Architecture |
| :---: | :---: | :---: | :---: |
| **FBNeo** | FinalBurn Neo v1.0.0.01 |  | All platforms |
| **Mame** | See romsets [above](./romsets-for-recalbox#mame). |  | All platforms |

### Naomi

| Core | Romset | .dat | Architecture |
| :---: | :---: | :---: | :---: |
| **Flycast** | MAME 0.135 or higher, 0.230 recommended |  | x86_64 |

### Naomi GD-ROM

| Core | Romset | .dat | Architecture |
| :---: | :---: | :---: | :---: |
| **Flycast** | MAME 0.135 or higher, 0.230 recommended |  | x86_64 |

>For more information on Arcade roms, please visit [this page](./../../../advanced-usage/arcade-in-recalbox).
{.is-info}

### Sega Model 3

| Core | Romset | .dat | Architecture |
| :---: | :---: | :---: | :---: |
| **Supermodel** | MAME 0.135 or higher, 0.230 recommended |  | x86_64 |

## Fantasy

| Platform | No-Intro | Tosec | Redump | Trurip |
| :---: | :---: | :---: | :---: | :---: |
| **EasyRPG** |  |  |  |  |
| **Lutro** |  |  |  |  |
| **Openbor** |  |  |  |  |
| **Solarus** |  |  |  |  |
| **Tic-80** |  |  |  |  |
| **Uzebox** |  |  |  |  |

## Consoles

| Platform | No-Intro | TOSEC | Redump | Trurip |
| :---: | :---: | :---: | :---: | :---: |
| **32x** | 🥇 🎮 | 🥈 | ❌ | 🥉 |
| **Amiga CD32** | ❌ | 🥈 | 🥇 | 🥉 |
| **Amiga CDTV** | ❌ | 🥈 | 🥇 | 🥉 |
| **Amstrad GX4000** | ❌ | 🥇 | ❌ | 🥈 |
| **Atari 2600** | 🥇 🎮 | 🥈 | ❌ | ❌ |
| **Atari 5200** | 🥇 | 🥈 | ❌ | 🥉 |
| **Atari 7800** | 🥇 | 🥈 | ❌ | 🥉 |
| **Atari Jaguar** | 🥇 🎮 | 🥈 | ❌ | 🥉 |
| **ColecoVision** | 🥇 | 🥈 | ❌ | 🥉 |
| **Dreamcast** | ❌ | 🥈 | 🥇 | 🥉 |
| **Fairchild Channel F** | 🥇 | 🥈 | ❌ | 🥉 |
| **Famicom Disk System** | 🥇 🎮 | 🥈 | ❌ | 🥉 |
| **GameCube** | ❌ | 🥈 | 🥇 🎮 | 🥉 |
| **Intellivision** | 🥇 | 🥈 | ❌ | 🥉 |
| **Master System** | 🥇 🎮 | 🥈 | ❌ | 🥉 |
| **Genesis** | 🥇 🎮 | 🥈 | ❌ | 🥉 |
| **Multivision** | ❌ | 🥇 | ❌ | ❌ |
| **NeoGeo CD** | ❌ | ❌ | 🥇 | 🥇 |
| **NES** | 🥇 🎮 | 🥈 | ❌ | ❌ |
| **Nintendo 64** | 🥇 | 🥈 | ❌ | ❌ |
| **Nintendo 64DD** | 🥇 | 🥈 | ❌ | ❌ |
| **Odyssey 2** | 🥇 | 🥈 | ❌ | 🥉 |
| **Panasonic 3DO** | ❌ | 🥈 | 🥇 | 🥉 |
| **Pc-Engine** | 🥇 🎮 | 🥈 | ❌ | 🥉 |
| **Pc-Engine CD** | ❌ | 🥈 | 🥇 | 🥉 |
| **PCFX** | ❌ | 🥉 | 🥇 | 🥈 |
| **PlayStation 1** | ❌ | 🥉 | 🥇 | 🥈 |
| **Satellaview** | 🥇 | 🥉 | ❌ | ❌ |
| **Saturn** | ❌ | 🥉 | 🥇 | 🥈 |
| **Sega CD** | ❌ | 🥉 | 🥇 | 🥈 |
| **Sega SG1000** | 🥇 🎮 | 🥉 | ❌ | ❌ |
| **Super Famicom** | 🥇 | 🥈 | ❌ | 🥉 |
| **SuFami Turbo** | 🥇 | 🥈 | ❌ | ❌ |
| **Super Nintendo** | 🥇 🎮 | 🥈 | ❌ | ❌ |
| **SuperGrafx** | 🥇 🎮 | 🥈 | ❌ | 🥉 |
| **Vectrex** | 🥇 | 🥈 | ❌ | 🥉 |
| **Virtual Boy** | 🥇 | 🥈 | ❌ | 🥉 |
| **Wii** | ❌ | 🥈 | 🥇 🎮 | ❌ |

## Handhelds

| Platform | No-Intro | Tosec | Redump | Trurip |
| :---: | :---: | :---: | :---: | :---: |
| **Game and Watch** | ❌ | ❌ | ❌ | ❌ |
| **Game Boy** | 🥇 | 🥈 | ❌ | 🥉 |
| **Game Boy Color** | 🥇 | 🥈 | ❌ | 🥉 |
| **Game Boy Advance** | 🥇 | 🥈 | ❌ | 🥉 |
| **Game Gear** | 🥇 | 🥈 | ❌ | 🥉 |
| **Lynx** | 🥇 | 🥈 | ❌ | 🥉 |
| **Nintendo DS** | 🥇 | ❌ | ❌ | ❌ |
| **NeoGeo Pocket** | 🥇 | 🥈 | ❌ | 🥉 |
| **NeoGeo Pocket Color** | 🥇 | 🥈 | ❌ | 🥉 |
| **Palm OS** | 🥈 | ❌ | 🥇 | 🥉 |
| **Pokémini** | 🥇 | 🥈 | ❌ | 🥉 |
| **PSP** | ❌ | ❌ | 🥇 | 🥈 |
| **Wonderswan** | 🥇 | 🥈 | ❌ | 🥉 |
| **Wonderswan Color** | 🥇 | 🥈 | ❌ | 🥉 |

## Computers

| Platform | No-Intro | Tosec | Redump | Trurip |
| :---: | :---: | :---: | :---: | :---: |
| **Amiga 600** | ❌ | 🥇 | ❌ | ❌ |
| **Amiga 1200** | ❌ | 🥇 | ❌ | ❌ |
| **Amstrad CPC** | 🥈 | 🥇 | ❌ | 🥉 |
| **Apple II** | ❌ | 🥇 | ❌ | 🥈 |
| **Apple II Gs** | ❌ | 🥇 | ❌ | 🥈 |
| **Atari 800** | ❌ | 🥇 | ❌ | 🥈 |
| **Atari St** | 🥈 | 🥇 | ❌ | 🥉 |
| **Commodore 64** | 🥈 | 🥇 | ❌ | 🥉 |
| **MSX 1** | 🥈 | 🥇 | ❌ | 🥉 |
| **MSX 2** | 🥈 | 🥇 | ❌ | 🥉 |
| **MSX Turbo R** | 🥈 | 🥇 | ❌ | 🥉 |
| **NEC Pc8801** | ❌ | 🥈 | 🥇 | 🥉 |
| **NEC Pc9801** | ❌ | 🥈 | 🥇 | 🥉 |
| **Oric Atmos** | ❌ | 🥇 | ❌ | ❌ |
| **Samcoupé** | ❌ | 🥇 | ❌ | 🥈 |
| **Sharp X1** | ❌ | 🥇 | ❌ | 🥈 |
| **Sharp X68000** | ❌ | 🥈 | 🥇 | 🥉 |
| **Sinclair ZXspectrum** | 🥇 | 🥈 | ❌ | 🥉 |
| **Sinclair ZX81** | ❌ | 🥇 | ❌ | 🥈 |
| **Spectravideo SV-318** | ❌ | 🥇 | ❌ | 🥈 |
| **Thomson MO/TO** | ❌ | 🥇 | ❌ | 🥈 |

## Ports

| Platform |  |  |
| :---: | :---: | :---: |
| **2048** |  |  |
| **Cavestory** |  |  |
| **Craft** |  |  |
| **DinoThawr** |  |  |
| **Doom** |  |  |
| **Flashback** |  |  |
| **MrBoom** |  |  |
| **Outrun** |  |  |
| **Quake I** |  |  |
| **Rick Dangerous** |  |  |
| **Sigil** |  |  |
| **Wolfenstein** |  |  |