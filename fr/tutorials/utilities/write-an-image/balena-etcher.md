---
title: Balena Etcher
description: 
published: true
date: 2023-09-17T07:06:34.856Z
tags: balena, etcher
editor: markdown
dateCreated: 2021-05-21T08:42:43.189Z
---

> N'utilisez Balena Etcher qu'en tout dernier recours, celui-ci flashe Recalbox de manière non correcte et peut rendre votre installation non opérationnelle !
>
> Il est très vivement recommandé d'utiliser [Raspberry Pi Imager](./../../../tutorials/utilities/write-an-image/raspberry-pi-imager) en premier lieu pour flasher Recalbox, même pour installer Recalbox sur une autre board qu'un Raspberry Pi.
{.is-danger}

[BalenaEtcher](https://www.balena.io/etcher/) est un programme est très simple d'utilisation. Vous pouvez flasher le fichier image sur le support désiré (par exemple, une carte microSD pour un Raspberry Pi). L'opération de flash d'une image ne prend que quelques secondes de manipulation.

* Choisir l’image ISO à flasher :

  * Cliquer sur `Select Image` et chercher le fichier à flasher. Le nom de l’image apparaît alors sous la première icône et un petit changement permet de modifier ce choix.

* Connecter un média de stockage :

	* La seconde icône en forme de stockage externe s’illumine.
  * Sélectionner un média de stockage.

Etcher valide automatiquement ce qu’il détecte comme un périphérique USB. Il est possible de changer de destination en appuyant sur le petit `Change`. Le programme indique la capacité du dispositif et signale si celui-ci est trop petit pour accepter l’image.

>Pour l'installation sur disque dur :
>
>* Allez dans `Paramètres` (Roue crantée en haut à droite).
>* Désactiver la fonction "unsafe mode".
>* Revenez au menu d'accueil.
>* Sélectionner un disque dur.
{.is-warning}

![Etcher](/tutorials/utilities/image-write/balena-etcher/balena1.jpg)

* Appuyer sur le bouton `Start ou Flash` pour lancer la procédure. Etcher se charge de tout, formatage, préparation des partitions, copie des fichiers, etc.

![](/tutorials/utilities/image-write/balena-etcher/balena2.png)

* Le programme indique le travail en cours, il précise également le temps nécessaire au déroulement de l’opération et la vitesse de transfert des données.

![](/tutorials/utilities/image-write/balena-etcher/balena3.png)

* Il valide ensuite l’image.

![](/tutorials/utilities/image-write/balena-etcher/balena4.png)

* Et enfin signale le bon – ou le mauvais – déroulement de l’opération. Vous donne une clé pour vérifier l’intégrité de l’archive et vous propose de recommencer l’opération soit avec la même image en un clic, soit avec une image différente.

![](/tutorials/utilities/image-write/balena-etcher/balena5.png)

* Vous pouvez éjecter le support de stockage.