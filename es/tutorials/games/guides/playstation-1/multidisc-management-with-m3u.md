---
title: Gestión de multi-discos con el formato .M3U
description: 
published: true
date: 2023-09-21T11:58:56.192Z
tags: ps1, m3u, gestion, multidiscos
editor: markdown
dateCreated: 2023-09-21T11:58:56.192Z
---

El contenido ha sido transferido [a esta otra página](./../../generalities/multidisc-management-with-m3u).