---
title: Libretro O2EM
description: 
published: true
date: 2024-07-14T17:19:43.607Z
tags: libretro, o2em, videopac
editor: markdown
dateCreated: 2023-09-08T09:39:05.949Z
---

Libretro O2EM es un emulador multi-plateforma open source para la Odyssey2 / Videopac +.

## ![](/emulators/license.svg) Licencia

El core posee la siguiente licencia [**artistique**](https://sourceforge.net/projects/o2em/).

## ![](/emulators/compatibility.png) Compatibilidad

 | RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | 
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | 

## ![](/emulators/features.png) Funcionalidades

| Funcionalidad | ¿Soportada? |
| :---: | :---: |
| Reinicio | ✔ |
| Grabaciones instantáneas | ✔ |
| Rebobinado | ✔ |
| Opciones del core | ✔ |
| RetroAchievements | ✔ |
| Controles | ✔ |
| Remapping | ✔ |

## ![](/emulators/bios.svg) BIOS

### Lista de BIOS obligatorias

| Nombre del fichero | Descripción | MD5 | ¿Preinstalada? |
| :---: | :---: | :---: | :---: |
| o2rom.bin | BIOS Odyssey2 - modèle G7000 | 562d5ebf9e030a40d6fabfc2f33139fd | ❌ |
| c52.bin | BIOS Videopac+ Français - modèle G7000 | f1071cdb0b6b10dde94d3bc8a6146387 | ❌ |

### Emplazamiento

Disponed vuestras bios como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 o2em
┃ ┃ ┃ ┃ ┣ 🗒 **o2rom.bin**
┃ ┃ ┃ ┃ ┣ 🗒 **c52.bin**

## ![](/emulators/roms.png) Roms

### Extensiones de roms soportadas

Las roms deben tener las extensiones siguientes:

* .bin
* .zip
* .7z

Este sistema soporta las roms comprimidas en formato .zip/.7z. No obstante, se trata sólo de un archivo.

Los ficheros contenidos en los .zip/.7z deben tener las extensiones citadas previamente.
Además, cada fichero .zip/.7z sólo debe contener un fichero rom al interior.

### Emplazamiento

Disponed vuestras roms como en la siguiente imagen: 

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 o2em
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.zip**

>Se aconsejan las roms del proyecto **No-Intro**.
{.is-success}

>Para más información sobre las roms, consultad [este tutorial](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

>Para conservar las configuraciones personalizadas cuando se produce una actualización del sistema Recalbox, os aconsejamos utilizar las [Sobrecargas de configuración](./../../../advanced-usage/configuration-override).
{.is-info}

### Acceder a las opciones

Podéis configurar las opciones del core de dos formas diferentes.

* Via el Menú RetroArch :


┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via el fichero `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Opciones del core

| Opción | Valor de la opción | Variable (para la sobrecarga) | Valores posibles |
| --- | --- | --- | --- |
| Emulated Hardware (Restart) | `Odyssey 2 (NTSC)` ✅ / `Videopac G7000 (European)` / `Videopac+ G7400 (European)` / `Videopac+ G7400 (French)` | `o2em_bios` | `o2rom.bin` / `c52.bin` / `g7400` / `jopac.bin` |
| Console Region (Restart) | `Auto` ✅ / `NTSC` / `PAL` | `o2em_region` | `auto` / `NTSC` / `PAL` |
| Swap Gamepads | `Désactivé` ✅ / `Activé` | `o2em_swap_gamepads` | `disabled` / `enabled` |
| Virtual KBD Transparency | `0%` ✅ / `25%` / `50%` / `75%` | `o2em_vkbd_transparency` | `0` / `25` / `50` / `75` |
| Crop Overscan | `Désactivé` ✅ / `Activé` | `o2em_crop_overscan` | `disabled` / `enabled` |
| Interframe Blending | `Désactivé` ✅ / `Simple` / `Ghosting (65%)` / `Ghosting (75%)` / `Ghosting (85%)` / `Ghosting (95%)` | `o2em_mix_frames` | `disabled` / `mix` / `ghost_65` / `ghost_75` / `ghost_85` / `ghost_95` |
| Audio Volume | `0%` / `10%` / `20%` / `30%` / `40%` / `50%` ✅ / `60%` / `70%` / `80%` / `90%` / `100%` | `o2em_audio_volume` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
 | Voice Volume | `0%` / `5%` / `10%` / `15%` / `20%` / `25%` / `30%` / `35%` / `40%` / `45%` / `50%` / `55%` / `60%` / `65%` / `70%` ✅ / `75%` / `80%` / `85%` / `90%` / `95%` / `100%` | `o2em_voice_volume` | `0` / `5` / `10` / `15` / `20` / `25` / `30` / `35` / `40` / `45` / `50` / `55` / `60` / `65` / `70` / `75` / `80` / `85` / `90` / `95` / `100` |
| Audio Filter | `Désactivé` ✅ / `Activé` | `o2em_low_pass_filter` | `disabled` / `enabled` |
| Audio Filter Level | `5%` / `10%` / `15%` / `20%` / `25%` / `30%` / `35%` / `40%` / `45%` / `50%` / `55%` / `60%` ✅ / `65%` / `70%` / `75%` / `80%` / `85%` / `90%` / `95%` | `o2em_low_pass_range` | `5` / `10` / `15` / `20` / `25` / `30` / `35` / `40` / `45` / `50` / `55` / `60` / `65` / `70` / `75` / `80` / `85` / `90` / `95` |

## ![](/emulators/external-links.png) Enlaces externos

* **Código fuente utilizado** : [https://github.com/libretro/libretro-o2em/](https://github.com/libretro/libretro-o2em/)
* **Documentación Libretro** : [https://docs.libretro.com/library/o2em/](https://docs.libretro.com/library/o2em/)