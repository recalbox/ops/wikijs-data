---
title: CDi
description: Philips CDi
published: true
date: 2025-01-05T23:36:15.554Z
tags: consoles, philips, cdi, 8.1+
editor: markdown
dateCreated: 2022-02-03T17:32:30.145Z
---

![](/emulators/consoles/cdi.svg){.align-center}

## Technical data

* **Manufacturer**: Philips, Magnavox
* **Year of Release:** 1991
* **OS:** CD-RTOS (Compact Disc Real Time Operating System)
* **CPU:** 16-bit CISC (680070) @ 15.5MHz
* **RAM:** 1 MB
* **VRAM:** 1 MB
* **ROM:** 512 KB
* **GPU:** SCC66470, later MCD 212
* **Sound chip:** 16-bit Stereo Sound with 44.1 KHz Sound Sampling Rate
* **Resolution:** 384x280 to 768x560 (16.7 million colours)

## Presentation

The Compact Disc-Interactive (CD-I, later CD-i) is a digital optical disc data storage format that was mostly developed and marketed by Dutch company Philips. It was created as an extension of CDDA and CD-ROM and specified in the Green Book, co-developed by Philips and Sony, to combine audio, text and graphics. The two companies initially expected to impact the education/training, point of sale, and home entertainment industries, but CD-i eventually became best known for its video games.

CD-i media physically have the same dimensions as CD, but with up to 744 MB of digital data storage, including up to 72 minutes of full motion video. CD-i players were usually standalone boxes that connect to a standard television; some less common setups included integrated CD-i television sets and expansion modules for personal computers. Most players were created by Philips; the format was licensed by Philips and Microware for use by other manufacturers, notably Sony who released professional CD-i players under the "Intelligent Discman" brand. Unlike CD-ROM drives, CD-i players are complete computer systems centered around dedicated Motorola 68000-based microprocessors and its own operating system called CD-RTOS, which is an acronym for "Compact Disc – Real Time Operating System".

Media released on the format included video games and "edutainment" and multimedia reference titles, such as interactive encyclopedias and museum tours – which were popular before public Internet access was widespread – as well as business software. Philips's CD-i system also implemented Internet features, including subscriptions, web browsing, downloading, e-mail, and online play. Philips's aim with its players was to introduce interactive multimedia content for the general public by combining features of a CD player and game console, but at a lower price than a personal computer with a CD-ROM drive.

Authoring kits for the format were released first in 1988, and the first player aimed for home consumers, Philips's CDI 910/205, at the end of 1991, initially priced around US$1,000 (equivalent to $1,900 in 2020), and capable of playing interactive CD-i discs, Audio CDs, CD+G (CD+Graphics), Photo CDs and Video CDs (VCDs), though the latter required an optional "Digital Video Card" to provide MPEG-1 decoding. Initially marketed to consumers as "home entertainment systems", and in later years as a "gaming platform", CD-i did not manage to find enough success in the market, and was mostly abandoned by Philips in 1996. The format continued to be supported for licensees for a few more years after.

## Emulators

[Libretro-cdi2015](libretro-cdi2015)
[Libretro Same_cdi](libretro-same_cdi)