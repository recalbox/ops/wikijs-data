---
title: Libretro FreeIntv
description: 
published: true
date: 2021-10-08T14:02:27.104Z
tags: libretro, intellivision, freeintv
editor: markdown
dateCreated: 2021-07-30T21:16:15.656Z
---



## ![](/emulators/license.svg) License

This core is under [**GPLv3**](https://github.com/libretro/FreeIntv/blob/master/LICENSE) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Features

| Feature | Supported |
| :---: | :---: |
| Screenshots | ✔ |
| RetroAchievements | ✔ |
| Controls | ✔ |

## ![](/emulators/bios.svg) BIOS

### List of mandatory bios

| Filename | Description | MD5 | Provided |
| :---: | :---: | :---: | :---: |
| exec.bin | Execution ROM | 62e761035cb657903761800f4437b8af | ❌ |
| grom.bin | Graphic ROM | 0cd5946c6473e42e8e4c2137785e427f | ❌ |

### Location

Put the bios like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 🗒 **exec.bin**
┃ ┃ ┃ ┣ 🗒 **grom.bin**

## ![](/emulators/roms.png) Roms

### Supported extensions

Roms must have the extension:

* .bin
* .int
* .rom
* .zip
* .7z

This system supports compressed roms in .zip/.7z format. However, be careful, it is only an archive.

The files contained in the .zip/.7z must correspond to the extensions mentioned above.
Moreover, each .zip/.7z file must contain only one compressed rom.

### Location

Put the roms like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 intellivision
┃ ┃ ┃ ┃ ┣ 🗒 **game.zip**

>The roms using **No-Intro** format are highly recommended.
{.is-success}

>For more information about roms, go to [this tutorial](./../../../tutorials/games/generalities/isos-and-roms)!
{.is-info}

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>**This core have no otion.**
{.is-success}

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/libretro/FreeIntv/](https://github.com/libretro/FreeIntv/)
* **Libretro documentation**: [https://docs.libretro.com/library/freeintv/](https://docs.libretro.com/library/freeintv/)