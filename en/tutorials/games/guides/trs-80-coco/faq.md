---
title: FAQ
description: 
published: true
date: 2021-11-09T20:51:10.683Z
tags: color, faq, trs-80, computer, coco, 8.0+
editor: markdown
dateCreated: 2021-09-09T17:38:52.961Z
---

## Some useful information...

## What machines can I use with the TRS-80 Color Computer?

By default, the machine used will be the Color Computer 2 PAL (CoCo 2 PAL).

Depending on the subdirectories, the machine to be used will be automatically selected according to these criteria:

```text
"/coco/": "coco",
"/coco-pal/": "coco",
"/cocous/": "cocous",
"/coco-ntsc/": "cocous",
"/coco2/": "coco2b",
"/coco2b/": "coco2b",
"/coco2-pal/": "coco2b",
"/coco2b-pal/": "coco2b",
"/coco2us/": "coco2bus",
"/coco2bus/": "coco2bus",
"/coco2-ntsc/": "coco2bus",
"/coco2b-ntsc/": "coco2bus",
"/mx/": "mx1600",
"/mx1600/": "mx1600",
```

### Does the TRS-80 Color Computer support smoothing of games?

Yes, the XRoar core supports smoothing (in EmulationStation, `START` > `GAME SETTINGS` > `SMOOTH GAMES`).

### Does the TRS-80 Color Computer benefit from configuration overrides?

Yes! But the configuration overrides do not override each other.

The first one found in order is the one that is taken into account: `rom.ext.config` > `<folder>.config` > `<parent>.config` > ... The syntax of the configuration file does not allow to manage overloads without making a gas factory.

To generate a config file, you can use this SSH command: `xroar --config-all > .config`

### Are the 2 joysticks taken into account ?

Yes, joysticks 0 and 1 are automatically taken into account whatever the order chosen in EmulationStation.

### We could print with the TRS-80 Color Computer. Is this something you can do again?

Yes! When the machine "prints", the printed text will go into the `/recalbox/share/saves/trs80coco/printer/rom-extension.txt`