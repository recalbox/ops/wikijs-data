---
title: Libretro Mednafen_PSX_HW
description: 
published: true
date: 2021-10-17T12:26:51.837Z
tags: libretro, ps1, mednafen, playstation 1, psx hw
editor: markdown
dateCreated: 2021-08-01T23:19:14.348Z
---

[Beetle PSX](https://github.com/libretro/beetle-psx-libretro) is a fork of the [Mednafen](https://mednafen.github.io/) PSX module for Libretro, it currently runs on Linux, macOS and Windows, this core is available in `mednafen-psx-hw` version which requires OpenGL 3.3 for the OpenGL renderer.

## ![](/emulators/license.svg) License

This core is under [**GPLv2**](https://github.com/libretro/beetle-psx-libretro/blob/master/COPYING) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ❌ | ❌ | ❌ | ❌ | ❌ | ❌ | ✅ | ✅ |

## ![](/emulators/features.png) Features

| Feature | Supported |
| :---: | :---: |
| Restart | ✔ |
| Saves | ✔ |
| States | ✔ |
| Core Options | ✔ |
| RetroAchievements | ✔ |
| RetroArch Cheats | ✔ |
| Controls | ✔ |
| Remapping | ✔ |
| Multi-Mouse | ✔ |
| Rumble | ✔ |
| Disk Control | ✔ |

## ![](/emulators/bios.svg) BIOS

### List of mandatory bios

| Filename | Description | MD5 | Provided |
| :---: | :---: | :---: | :---: |
| scph5500.bin | Japanese PS1 BIOS - required for japanese games | 8dd7d5296a650fac7319bce665a6a53c | ❌ |
| scph5501.bin | American PS1 BIOS - required for american games | 490f666e1afb15b7362b406ed1cea246 | ❌ |
| scph5502.bin | European PS1 BIOS - required for european games | 32736f17079d0b2b7024407c39bd3050 | ❌ |

### Location

Put the bios like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 🗒 **scph5500.bin**
┃ ┃ ┃ ┣ 🗒 **scph5501.bin**
┃ ┃ ┃ ┣ 🗒 **scph5502.bin**

## ![](/emulators/isos.svg) Isos

### Supported extensions

Isos must have the extension:

* .bin/.cue
* .bin/.toc
* .m3u
* .img/.ccd/.sub
* .exe
* .pbp
* .chd

### Location

Put the isos like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 psx
┃ ┃ ┃ ┃ ┣ 🗒 **game.bin**
┃ ┃ ┃ ┃ ┣ 🗒 **game.cue**

>The isos using **Redump** format are highly recommended.
{.is-success}

>For more information about isos, go to [this tutorial](./../../../tutorials/games/generalities/isos-and-roms)!
{.is-info}

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Accessing to options

You can configure various options in two different ways.

* Via RetroArch menu:

┣ 📁 RetroArch Menu
┃ ┣ 📁 Core options
┃ ┃ ┣ 🧩 Name_option

* Via the file `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core options

| Options | Options values | Variables (for configuration override) | Variables values |
| --- | --- | --- | --- |
| Renderer (Restart) | `Hardware (auto)` ✅ / `Hardware (OpenGL)` / `Hardware (Vulkan)` / `Software` | `beetle_psx_hw_renderer` | `hardware` / `hardware_gl` / `hardware_vk` / `software` |
| Software Framebuffer | `Enabled` ✅ / `Disabled` | `beetle_psx_hw_renderer` | `enabled` / `disabled` |
| Internal GPU Resolution | `1x (Native)` ✅ / `2x` / `4x` / `8x` / `16x` | `beetle_psx_hw_internal_resolution` | `1x(native)` / `2x` / `4x` / `8x` / `16x` |
| Internal Color Depth | `16 bpp (Native)` ✅ / `32 bpp` | `beetle_psx_hw_depth` | `16bpp(native)` / `32 bpp` |
| Dithering Pattern | `1x (Native)` ✅ / `Internal Resolution` / `Disabled` | `beetle_psx_hw_dither_mode` | `1x(native)` / `internal resolution` / `disabled` |
| Texture Filtering | `Nearest` ✅ / `SABR` / `xBR` / `Bilinear` / `3-Point` / `JINC2` | `beetle_psx_hw_filter` | `nearest` / `SABR` / `xBR` / `bilinear` / `3-point` / `JINC2` |
| Adaptive Smoothing | `Disabled` ✅ / `Enabled` | `psx_hw_adaptive_smoothing` | `disabled` / `enabled` |
| Supersampling (Downsample to Native Resolution) | `Disabled` ✅ / `Enabled` | `psx_hw_super_sampling` | `disabled` / `enabled` |
| Multi-Sampled Anti Aliasing | `1x (Default)` ✅ / `2x` / `4x` / `8x` / `16x` | `psx_hw_msaa` | `1x` / `2x` / `4x` / `8x` / `16x` |
| MDEC YUV Chroma Filter | `Disabled` ✅ / `Enabled` | `psx_hw_mdec_yuv` | `disabled` / `enabled` |
| Track Textures | `Disabled` ✅ / `Enabled` | `beetle_psx_hw_track_textures` | `disabled` / `enabled` |
| Dump Textures | `Disabled` ✅ / `Enabled` | `beetle_psx_hw_dump_textures` | `disabled` / `enabled` |
| Replace Textures | `Disabled` ✅ / `Enabled` | `beetle_psx_hw_replace_textures` | `disabled` / `enabled` |
| Wireframe Mode (Debug) | `Disabled` ✅ / `Enabled` | `beetle_psx_hw_wireframe` | `disabled` / `enabled` |
| Display Full VRAM (Debug) | `Disabled` ✅ / `Enabled` | `beetle_psx_hw_dispaly_vram` | `disabled` / `enabled` |
| PGXP Operation Mode | `Disabled` ✅ / `Memory Only` / `Memory + CPU (Buggy)` |`beetle_psx_hw_pgxp_mode` | `disabled` / `memory only` / `memory + CPU` |
| PGXP Vertex Cache | `Disabled` ✅ / `Enabled` | `beetle_psx_hw_pgxp_vertex` | `disabled` / `enabled` |
| PGXP Perspective Correct Texturing | `Disabled` ✅ / `Enabled` | `beetle_psx_hw_pgxp_texture` | `disabled` / `enabled` |
| Display Internal FPS | `Disabled` ✅ / `Enabled` | `beetle_psx_hw_display_internal_fps` | `disabled` / `enabled` |
| Line-to-Quad Hack | `Default` ✅ / `Aggressive` / `Disabled` | `beetle_psx_hw_line_render` | `default` / `aggressive` / `disabled` |
| Frame Duping (Speedup) | `Disabled` ✅ / `Enabled` | `psx_hw_frame_duping` | `disabled` / `enabled` |
| CPU Dynarec | `Disabled (Beetle Interpreter)` ✅ / `Max Performance` / `Cycle Timing Check` / `Lightrec Interpreter` | `beetle_psx_hw_cpu_dynarec` | `disabled` / `execute` / `execute_once` / `run_interpreter` |
| Dynarec Code Invalidation | `Full` ✅ / `DMA Only (Slightly Faster)` | `beetle_psx_hw_dynarec_invalidate` | `full` / `dma` |
| Dynarec DMA/GPU Event Cycles | `128 (Default)` ✅ / `256` / `384` / `512` / `640` / `768` / `896` / `1024` | `beetle_psx_hw_dynarec_eventcycles` | `128` / `256` / `384` / `512` / `640` / `768` / `896` / `1024` |
| CPU Frequency Scaling (Overclock) | `50%` / `60%` / `70%` / `80%` / `90%` / `100% (Native) ✅` / `110%` / `120%` / `130%` / `140%` / `150%` / `160%` / `170%` / `180%` / `190%` / `200%` / `210%` / `220%` / `230%` / `240%` / `250%` / `260%` / `270%` / `280%` / `290%` / `300%` / `310%` / `320%` / `330%` / `340%` / `350%` / `360%` / `370%` / `380%` / `390%` / `400%` / `410%` / `420%` / `430%` / `440%` / `450%` / `460%` / `470%` / `480%` / `490%` / `500%` / `510%` / `520%` / `530%` / `540%` / `550%` / `560%` / `570%` / `580%` / `590%` / `600%` / `610%` / `620%` / `630%` / `640%` / `650%` / `660%` / `670%` / `680%` / `690%` / `700%` / `710%` / `720%` / `730%` / `740%` / `750%` | `beetle_psx_hw_cpu_freq_scale` | `50%` / `60%` / `70%` / `80%` / `90%` / `100%(native)` / `110%` / `120%` / `130%` / `140%` / `150%` / `160%` / `170%` / `180%` / `190%` / `200%` / `210%` / `220%` / `230%` / `240%` / `250%` / `260%` / `270%` / `280%` / `290%` / `300%` / `310%` / `320%` / `330%` / `340%` / `350%` / `360%` / `370%` / `380%` / `390%` / `400%` / `410%` / `420%` / `430%` / `440%` / `450%` / `460%` / `470%` / `480%` / `490%` / `500%` / `510%` / `520%` / `530%` / `540%` / `550%` / `560%` / `570%` / `580%` / `590%` / `600%` / `610%` / `620%` / `630%` / `640%` / `650%` / `660%` / `670%` / `680%` / `690%` / `700%` / `710%` / `720%` / `730%` / `740%` / `750%` |
| GTE Overclock | `Disabled` ✅ / `Enabled` | `beetle_psx_hw_gte_overclock` | `disabled` / `enabled` |
| GPU Resterizer Overclock | `1x (Native)` ✅ / `2x` / `4x` / `8x` / `16x` / `32x` | `beetle_psx_hw_gpu_overclock` | `1x(native)` / `2x` / `4x` / `8x` / `16x` / `32x` |
| Skip BIOS | `Disabled` ✅ / `Enabled` | `beetle_psx_hw_skip_bios` | `disabled` / `enabled` |
| Core-Reported FPS Timing | `Progressive Rate (Default)` ✅ / `Force Interlaced Rate` / `Allow Automatic Toggling` | `beetle_psx_hw_core_timing_fps` | `force_progressive` / `force_interlaced` / `auto_toggle` |
| Core Aspect Ratio | `Corrected` ✅ / `Uncorrected` / `Force 4:3` / `Force NTSC` | `beetle_psx_hw_aspect_ratio` | `corrected` / `uncorrected` / `4:3` / `ntsc` |
| Widescreen Mode Hack | `Disabled` ✅ / `Enabled` | `beetle_psx_hw_widescreen_hack` | `disabled` / `enabled` |
| PAL (European) Video Timing Override | `Disabled` ✅ / `Enabled` | `beetle_psx_hw_pal_video_timing_override` | `disabled` / `enabled` |
| Crop Horizontal Overscan | `Enabled` ✅ / `Disabled` | `beetle_psx_hw_crop_overscan` | `enabled` / `disabled` |
| Additional Cropping | `Disabled` ✅ / `1px` / `2px` / `3px` / `4px` / `5px` / `6px` / `7px` / `8px` / `9px` / `10px` / `11px` / `12px` / `13px` / `14px` / `15px` / `16px` / `17px` / `18px` / `19px` / `20px` | `psx_hw_image_crop` | `disabled` / `1px` / `2px` / `3px` / `4px` / `5px` / `6px` / `7px` / `8px` / `9px` / `10px` / `11px` / `12px` / `13px` / `14px` / `15px` / `16px` / `17px` / `18px` / `19px` / `20px` |
| Offset Cropped Image | `-12px` / `-11px` / `-10px` / `-9px` / `-8px` / `-7px` / `-6px` / `-5px` / `-4px` / `-3px` / `-2px` / `-1px` / `Disabled` ✅ / `+1px` / `+2px` / `+3px` / `+4px` / `+5px` / `+6px` / `+7px` / `+8px` / `+9px` / `+10px` / `+11px` / `+12px` | `psx_hw_image_offset` | `-12px` / `-11px` / `-10px` / `-9px` / `-8px` / `-7px` / `-6px` / `-5px` / `-4px` / `-3px` / `-2px` / `-1px` / `disabled` / `+1px` / `+2px` / `+3px` / `+4px` / `+5px` / `+6px` / `+7px` / `+8px` / `+9px` / `+10px` / `+11px` / `+12px` |
| Horizontal Image Offset (GPU Cycles) | `-40` / `-39` / `-38` / `-37` / `-36` / `-35` / `-34` / `-33` / `-32` / `-31` / `-30` / `-29` / `-28` / `-27` / `-26` / `-25` / `-24` / `-23` / `-22` / `-21` / `-20` / `-19` / `-18` / `-17` / `-16` / `-15` / `-14` / `-13` / `-12` / `-11` / `-10` / `-9` / `-8` / `-7` / `-6` / `-5` / `-4` / `-3` / `-2` / `-1` / `0` ✅ / `+1` / `+2` / `+3` / `+4` / `+5` / `+6` / `+7` / `+8` / `+9` / `+10` / `+11` / `+12` / `+13` / `+14` / `+15` / `+16` / `+17` / `+18` / `+19` / `+20` / `+21` / `+22` / `+23` / `+24` / `+25` / `+26` / `+27` / `+28` / `+29` / `+30` / `+31` / `+32` / `+33` / `+34` / `+35` / `+36` / `+37` / `+38` / `+39` / `+40` | `beetle_psx_hw_image_offset_cycles` | `-40` / `-39` / `-38` / `-37` / `-36` / `-35` / `-34` / `-33` / `-32` / `-31` / `-30` / `-29` / `-28` / `-27` / `-26` / `-25` / `-24` / `-23` / `-22` / `-21` / `-20` / `-19` / `-18` / `-17` / `-16` / `-15` / `-14` / `-13` / `-12` / `-11` / `-10` / `-9` / `-8` / `-7` / `-6` / `-5` / `-4` / `-3` / `-2` / `-1` / `0` / `+1` / `+2` / `+3` / `+4` / `+5` / `+6` / `+7` / `+8` / `+9` / `+10` / `+11` / `+12` / `+13` / `+14` / `+15` / `+16` / `+17` / `+18` / `+19` / `+20` / `+21` / `+22` / `+23` / `+24` / `+25` / `+26` / `+27` / `+28` / `+29` / `+30` / `+31` / `+32` / `+33` / `+34` / `+35` / `+36` / `+37` / `+38` / `+39` / `+40` |
| Initial Scanlines (NTSC) | `0` ✅ / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `21` / `22` / `23` / `24` / `25` / `26` / `27` / `28` / `29` / `30` / `31` / `32` / `33` / `34` / `35` / `36` / `37` / `38` / `39` / `40` | `psx_hw_initial_scanlines` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `21` / `22` / `23` / `24` / `25` / `26` / `27` / `28` / `29` / `30` / `31` / `32` / `33` / `34` / `35` / `36` / `37` / `38` / `39` / `40` |
| Last Scanlines (NTSC) | `210` / `211` / `212` / `213` / `214` / `215` / `216` / `217` / `218` / `219` / `220` / `221` / `222` / `223` / `224` / `225` / `226` / `227` / `228` / `229` / `230` / `231` / `232` / `233` / `234` / `235` / `236` / `237` / `238` / `239` ✅ | `psx_hw_last_scanlines` | `210` / `211` / `212` / `213` / `214` / `215` / `216` / `217` / `218` / `219` / `220` / `221` / `222` / `223` / `224` / `225` / `226` / `227` / `228` / `229` / `230` / `231` / `232` / `233` / `234` / `235` / `236` / `237` / `238` / `239` |
| Initial Scanlines - PAL | `0` ✅ / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `21` / `22` / `23` / `24` / `25` / `26` / `27` / `28` / `29` / `30` / `31` / `32` / `33` / `34` / `35` / `36` / `37` / `38` / `39` / `40` | `beetle_psx_hw_initial_scanline_pal` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `21` / `22` / `23` / `24` / `25` / `26` / `27` / `28` / `29` / `30` / `31` / `32` / `33` / `34` / `35` / `36` / `37` / `38` / `39` / `40` |
| Last Scanlines - PAL | `230` / `231` / `232` / `233` / `234` / `235` / `236` / `237` / `238` / `239` / `240` / `241` / `242` / `243` / `244` / `245` / `246` / `247` / `248` / `249` / `250` / `251` / `252` / `253` / `254` / `255` / `256` / `257` / `258` / `259` / `260` / `261` / `262` / `263` / `264` / `265` / `266` / `267` / `268` / `269` / `270` / `271` / `272` / `273` / `274` / `275` / `276` / `277` / `278` / `279` / `280` / `281` / `282` / `283` / `284` / `285` / `286` / `287` ✅ | `beetle_psx_hw_last_scanline_pal` | `230` / `231` / `232` / `233` / `234` / `235` / `236` / `237` / `238` / `239` / `240` / `241` / `242` / `243` / `244` / `245` / `246` / `247` / `248` / `249` / `250` / `251` / `252` / `253` / `254` / `255` / `256` / `257` / `258` / `259` / `260` / `261` / `262` / `263` / `264` / `265` / `266` / `267` / `268` / `269` / `270` / `271` / `272` / `273` / `274` / `275` / `276` / `277` / `278` / `279` / `280` / `281` / `282` / `283` / `284` / `285` / `286` / `287` |
| CD Access Method (Restart) | `Synchronous` ✅ / `Asynchronous` / `Pre-Cache` | `beetle_psx_hw_cd_access_method` | `sync` / `async` / `precache` |
| CD Loading Speed | `2x (Native)` ✅ / `4x` / `6x` / `8x` / `10x` / `12x` / `14x` | `beetle_psx_hw_cd_fastload` | `2x(native)` / `4x` / `6x` / `8x` / `10x` / `12x` / `14x` |
| Memory Card 0 Method (Restart) | `Libretro` ✅ / `Mednafen` | `beetle_psx_hw_use_mednafen_memcard0_method` | `libretro` / `mednafen` |
| Shared Memory Card 1 (Restart) | `Enabled` ✅ / `Disabled` | `beetle_psx_hw_shared_memory_cards` | `enabled` / `disabled` |
| Analog Self-Calibration | `Disabled` ✅ / `Enabled` | `beetle_psx_hw_analog_calibration` | `disabled` / `enabled` |
| Enable DualShock Analog Mode Toggle | `Disabled` ✅ / `Enabled` | `beetle_psx_hw_analog_toggle` | `disabled` / `enabled` |
| Port 1: Multitap Enable | `Disabled` ✅ / `Enabled` | `beetle_psx_hw_enable_multitap_port1` | `disabled` / `enabled` |
| Port 2: Multitap Enable | `Disabled` ✅ / `Enabled` | `beetle_psx_hw_enable_multitap_port2` | `disabled` / `enabled` |
| Gun Input Mode | `Light Gun` ✅ / `Touchscreen` | `beetle_psx_hw_gun_input_mode` | `lightgun` / `touchscreen` |
| Gun Cursor | `Cross` ✅ / `Dot` / `Disabled` | `beetle_psx_hw_gun_cursor` | `cross` / `dot` / `off` |
| Mouse Sensitivity | `5%` / `10%` / `15%` / `20%` / `25%` / `30%` / `35%` / `40%` / `45%` / `50%` / `55%` / `60%` / `65%` / `70%` / `75%` / `80%` / `85%` / `90%` / `95%` / `100%` ✅ / `105%` / `110%` / `115%` / `120%` / `125%` / `130%` / `135%` / `140%` / `145%` / `150%` / `155%` / `160%` / `165%` / `170%` / `175%` / `180%` / `185%` / `190%` / `195%` / `200%` | `beetle_psx_hw_mouse_sensitivity` | `5%` / `10%` / `15%` / `20%` / `25%` / `30%` / `35%` / `40%` / `45%` / `50%` / `55%` / `60%` / `65%` / `70%` / `75%` / `80%` / `85%` / `90%` / `95%` / `100%` / `105%` / `110%` / `115%` / `120%` / `125%` / `130%` / `135%` / `140%` / `145%` / `150%` / `155%` / `160%` / `165%` / `170%` / `175%` / `180%` / `185%` / `190%` / `195%` / `200%` |
| NegCon Twist Responses | `Linear` ✅ / `Quadratic` / `Cubic` | `beetle_psx_hw_negcon_response` | `linear` / `quadratic` / `cubic` |
| NegCon Twist Deadzone | `0%` ✅ / `5%` / `10%` / `15%` / `20%` / `25%` / `30%` | `beetle_psx_hw_negcon_deadzone` | `5%` / `10%` / `15%` / `20%` / `25%` / `30%` |
| Memory Card Left Index | `0` ✅ / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `21` / `22` / `23` / `24` / `25` / `26` / `27` / `28` / `29` / `30` / `31` / `32` / `33` / `34` / `35` / `36` / `37` / `38` / `39` / `40` / `41` / `42` / `43` / `44` / `45` / `46` / `47` / `48` / `49` / `50` / `51` / `52` / `53` / `54` / `55` / `56` / `57` / `58` / `59` / `60` / `61` / `62` / `63` | `beetle_psx_hw_memcard_left_index` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `21` / `22` / `23` / `24` / `25` / `26` / `27` / `28` / `29` / `30` / `31` / `32` / `33` / `34` / `35` / `36` / `37` / `38` / `39` / `40` / `41` / `42` / `43` / `44` / `45` / `46` / `47` / `48` / `49` / `50` / `51` / `52` / `53` / `54` / `55` / `56` / `57` / `58` / `59` / `60` / `61` / `62` / `63` |
| Memory Card Right Index | `0` / `1` ✅ / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `21` / `22` / `23` / `24` / `25` / `26` / `27` / `28` / `29` / `30` / `31` / `32` / `33` / `34` / `35` / `36` / `37` / `38` / `39` / `40` / `41` / `42` / `43` / `44` / `45` / `46` / `47` / `48` / `49` / `50` / `51` / `52` / `53` / `54` / `55` / `56` / `57` / `58` / `59` / `60` / `61` / `62` / `63` | `beetle_psx_hw_memcard_right_index` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `21` / `22` / `23` / `24` / `25` / `26` / `27` / `28` / `29` / `30` / `31` / `32` / `33` / `34` / `35` / `36` / `37` / `38` / `39` / `40` / `41` / `42` / `43` / `44` / `45` / `46` / `47` / `48` / `49` / `50` / `51` / `52` / `53` / `54` / `55` / `56` / `57` / `58` / `59` / `60` / `61` / `62` / `63` |

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/libretro/beetle-psx-libretro/](https://github.com/libretro/beetle-psx-libretro/)
* **Libretro documentation**: [https://docs.libretro.com/library/beetle_psx_hw/](https://docs.libretro.com/library/beetle_psx_hw/)