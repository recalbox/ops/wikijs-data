---
title: Libretro Retro8
description: 
published: true
date: 2024-07-24T20:51:26.072Z
tags: libretro, pico-8, retro8, 7.2+
editor: markdown
dateCreated: 2021-07-04T18:13:05.217Z
---

Libretro Retro8 is a core developed to run games, which have been created from this same emulator.

## ![](/emulators/license.svg) License

This core is under [**GPLv3**](https://github.com/libretro/retro8/blob/master/LICENSE) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Features

| Feature | Supported |
| :---: | :---: |

## ![](/emulators/bios.svg) BIOS

>**No bios is required.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Supported extensions

Roms must have the extension:

* .p8
* .png
* .zip
* .7z

### Location

Put your roms like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 pico8
┃ ┃ ┃ ┃ ┣ 🗒 **game.png**

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>**This core have no option.**
{.is-success}

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://gitlab.com/recalbox/packages/libretro/libretro-retro8/](https://gitlab.com/recalbox/packages/libretro/libretro-retro8/)
* **Official source code**: [https://github.com/Jakz/retro8/](https://github.com/Jakz/retro8/)
* **Pico-8 forum**: [https://www.lexaloffle.com/bbs/?cat=7](https://www.lexaloffle.com/bbs/?cat=7)