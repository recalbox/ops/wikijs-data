---
title: 2. Les variables
description: 
published: true
date: 2024-04-13T20:38:50.516Z
tags: themes, 9.2+
editor: markdown
dateCreated: 2024-01-24T00:36:11.256Z
---

## Introduction

Afin de rendre les thèmes plus dynamiques, il existe des variables que vous pouvez utiliser. Ces variables peuvent être utilisées n'importe où dans les balises.

En dehors des variables `$system`, `$language` et `$country`, toutes les autres variables sont présentes depuis Recalbox 9.2.

## Liste des variables

### Variable `$system`

La variable `$system` permet de rendre le nom du système dynamique.

Exemple :

```xml
<include>
  ./$system/custom.xml
</include>
```

### Variable `$language`

La variable `$language` permet d'utiliser le code de la langue choisie par l'utilisateur.

Exemple :

```xml
<include>
  ./data/$language/texts.xml
</include>
```

### Variable `$country`

La variable `$country` permet d'utiliser le pays choisi par l'utilisateur.

Exemple :

```xml
<include>
  ./data/$country/logo.svg
</include>
```

### Variable `$fullname`

La variable `$fullname` utilise le nom complet du système en cours.

Exemple :

```xml
<text name="systemCurrent" text="$fullname"></text>
```

### Variable `$type`

La variable `$type` permet d'utiliser le type de système parmi cette liste :

- arcade : système arcade
- computer : micro-ordinateur
- console : console de salon
- engine : moteur de jeu
- fantasy : console fantasie
- handheld : console portable
- port : les ports
- virtual : système virtuel sauf arcade
- virtual-arcade : système virtuel arcade par constructeur

Exemple :

```xml
<text name="systemGameName" text="$type"></text>
```

### Variable `$pad`

La variable `$pad` indique le niveau de nécessité d'un pad ou d'une manette pour profiter de ce système parmi cette liste :

- mandatory : obligatoire
- recommended : fortement recommandé
- optionnal : optionnel, on peut s'en passer
- no : pas requis

Exemple :

```xml
<image>
  <path path="./data/assets/pad-$pad.svg"></path>
</image>
```

### Variable `$keyboard`

La variable `$keyboard` indique le niveau de nécessité d'un clavier pour profiter de ce système parmi cette liste :

- mandatory : obligatoire
- recommended : fortement recommandé
- optionnal : optionnel, on peut s'en passer
- no : pas requis

```xml
<image>
  <path path="./data/assets/keyboard-$keyboard.svg"></path>
</image>
```

### Variable `$mouse`

La variable `$mouse` indique le niveau de nécessité d'une souris pour profiter de ce système parmi cette liste :

- mandatory : obligatoire
- recommended : fortement recommandé
- optionnal : optionnel, on peut s'en passer
- no : pas requis

```xml
<image>
  <path path="./data/assets/mouse-$mouse.svg"></path>
</image>
```

### Variable `$releaseyear`

La variable `$releaseyear` indique l'année de sortie du système.

Exemple :

```xml
<text name="date" extra="true">
  <text>Date de sortie du système : $releaseyear</text>
</text>
```

### Variable `$netplay`

La variable `$netplay` indique par `yes` ou `no` si le système est compatible avec le Netplay ou pas.

Exemple :

```xml
<text name="netplay" extra="true">
  <text>Netplay disponible pour le système : $netplay</text>
</text>
```

### Variable `$lightgun`

La variable `$lightgun` indique par `yes` ou `no` si le système a des jeux jouables au lightgun.

```xml
<text name="lightgun" extra="true">
  <text>Jeu lightgun : $lightgun</text>
</text>
```

### Variable `$crt`

La variable `$crt` indique par `yes` ou `no` si un adaptateur CRT est actif ou pas.

```xml
<text name="crt" extra="true">
  <text>CRT actif : $crt</text>
</text>
```

### Variable `$jamma`

La variable `$jamma` indique par `yes` ou `no` si un adaptateur Jamma est actif ou pas.

```xml
<text name="jamma" extra="true">
  <text>Jamma actif : $jamma</text>
</text>
```

### Variable `$tate`

La variable `$tate` indique par `yes` ou `no` si l'écran est retourné à 90° ou -90°.

```xml
<text name="tate" extra="true">
  <text>Tate actif : $tate</text>
</text>
```

### Variable `$overscan`

La variable `$overscan` indique par `yes` ou `no` si un adaptateur CRT sans adaptateur Jamma est actif ou pas.

```xml
<text name="overscan" extra="true">
  <text>Overscan actif : $overscan</text>
</text>
```

### Variable `$resolution`

La variable `$resolution` permet de contenir des informations pour une certaine résolution d'écran.

Liste des valeurs :

- `qvga` : inférieur ou égal à 288 pixels de hauteur.
- `vga` : inférieur ou égal à 576 pixels de hauteur.
- `hd` : inférieur ou égal à 920 pixels de hauteur.
- `fhd` : supérieur à 920 pixels de hauteur.

```xml
<text name="resolution" extra="true">
  <text>Résolution : $resolution</text>
</text>
```