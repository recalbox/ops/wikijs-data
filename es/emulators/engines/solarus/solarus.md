---
title: Solarus
description: 
published: true
date: 2024-07-14T19:21:41.308Z
tags: solarus
editor: markdown
dateCreated: 2023-09-11T06:35:46.264Z
---

![](/emulators/fantasy/solarusarpg.svg){.align-center}

**Solarus** es un core standalone que permite jugar a juegos homebrew del estilo Action-RPG 2D.

## ![](/emulators/license.svg) Licencia

El core posee la siguiente licencia [**GPLv3**](https://www.solarus-games.org/fr/about/legal).

## ![](/emulators/compatibility.png) Compatibilidad

 | RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | 
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Funcionalidades

| Funcionalidad | ¿Soportada? |
| :---: | :---: |
| Reinicio | ✔ |
| Capturas de pantalla | ✔ |
| Grabaciones | ✔ |
| Opciones del core | ✔ |
| Controles | ✔ |
| Remapping | ✔ |
| Vibración | ✔ |
| Disk Control | ✔ |

## ![](/emulators/bios.svg) BIOS

>**No necesita bios.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Extensiones de roms soportadas

Las roms deben tener las extensiones siguientes:

* .solarus

### Emplazamiento

Disponed vuestras roms como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 solarus
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.solarus**

### Lista de juegos

Posee un catálogo de juegos compatible con el motor:  
[https://www.solarus-games.org/fr/games](https://www.solarus-games.org/fr/games)

![](/emulators/fantasy/solarusgamelist.png){.align-center}

## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

>**Este core no tiene opciones.**
{.is-success}

## ![](/emulators/external-links.png) Enlaces externos

* **Código fuente utilizado** : [https://gitlab.com/bkg2k/solarus](https://gitlab.com/bkg2k/solarus)
* **Página oficial** : [https://solarus-games.org/fr/](https://solarus-games.org/fr)
* **Juegos Homebrew** : [https://www.solarus-games.org/fr/games/](https://www.solarus-games.org/fr/games)