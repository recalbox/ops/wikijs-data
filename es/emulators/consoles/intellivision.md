---
title: Intellivision
description: Mattel Intellivision
published: true
date: 2024-07-26T07:49:55.186Z
tags: mattel, intellivision, consolas
editor: markdown
dateCreated: 2023-09-03T19:22:34.267Z
---

![](/emulators/consoles/intellivision.svg){.align-center}

## Datos técnicos

* **Fabricante:** Mattel Electronics
* **Año de lanzamiento :** 1979
* **Cantidad vendida :** 3+ millones
* **Juego más vendido:** Las Vegas Poker & Blackjack
* **Procesador:** GI CP1610 @ 894.886 kHz
* **RAM:** 1kB
* **Vídeo:** General Instrument AY-3-8900-1
* **Resolución:** 159x96, 16 colores
* **Sonido:** General Instrument AY-3-8914
* **Tamaño del cartucho:** 8kB - 32kB

## Presentación

La **Intellivision** es una consola de videojuegos producida por Mattel y lanzada al mercado en 1979. El desarrollo de la consola comenzó en 1978, menos de un año después de la introducción de su principal competidora, la Atari 2600.

## Emuladores

[Libretro FreeIntv](libretro-freeintv)