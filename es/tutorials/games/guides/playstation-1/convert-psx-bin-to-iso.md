---
title: Convertir las imágenes de disco .BIN en .ISO
description: 
published: true
date: 2023-09-21T09:39:08.507Z
tags: ps1, bin, iso, convertir
editor: markdown
dateCreated: 2023-09-21T09:39:08.507Z
---

## Introducción

Vamos a explicaros una solución para obtener un único fichero en formato .ISO por juego. Todos los juegos funcionarán de esta forma siempre y cuando los ficheros .BIN descargados también funcionen.

>Un problema recurrente con las roms para la PSX es que los archivos están a menudo en formatos .BIN y .CUE en vez de en .ISO, e incluso en formato .BIN.ECM.
>
>Una de las desventajas de estos archivos es que a menudo hay varios de ellos, 2 o 3 archivos y a veces docenas. Esto puede distorsionar la calidad de la presentación en la interfaz de EmulationStation (EmulationStation puede mostrar varias veces el mismo juego, con lo que también se distorsiona el número total de juegos del sistema).  
>  
>Este problema de visualización de EmulationStation se produce cuando:
>
>* No habéis scrapeados vuestras roms.
>* Si habéis configurado mal el software de scraping.
>
>No obstante, las roms en formato .BIN y .CUE se pueden lanzar correctamente.
{.is-info}

En primer lugar, el fichero ISO es una imagen binaria del disco y contendrá todo el juego en un único fichero.

### El fichero BIN

El fichero .BIN también es una imagen de disco, con un formato muy similar al ISO. El inconveniente es que los ficheros BIN pueden ser varios, a veces muchísimos, y suelen ir acompañados de un único fichero en formato CUE.

### El fichero CUE

El fichero .CUE es un pequeño fichero de texto que acompaña a los archivos .BIN. Se puede abrir con cualquier editor de texto y su único propósito es ayudar a indexar los ficheros BIN a los que acompaña. Indica al emulador dónde están los ficheros BIN, cuáles son sus nombres, su orden y, en ocasiones, las diferentes pistas de audio que contienen.

### El fichero .BIN.ECM

El fichero .BIN.ECM es más raro; es el resultado de un fichero BIN comprimido.   
Veremos cómo descomprimirlo en la última sección.

---

Lo primero que hay que tener en cuenta es que un juego suele estar formado por un solo fichero BIN y un solo fichero CUE. En este caso, el fichero CUE (texto), cuya función es indexar los distintos ficheros BIN que lo acompañan, no sirve para nada. Los usuarios Recalbox lo saben: en este caso, basta con borrar el archivo CUE, y el archivo BIN es autónomo (como una ISO), y se puede dejar tal cual, y funciona.

Podéis dejar estos BIN únicos en vuestro sistema sin convertirlos o bien podéis evitaros problemas y convertir todas vuestras roms, como queráis.

El segundo punto importante es no renombrar nunca los archivos BIN.
Si abrís un fichero CUE con un editor de texto, veréis que enumera todos los archivos .BIN. Si renombráis los ficheros BIN, los nombres no coincidirán más.

Si habéis modificado los nombres de los BIN, deberéis renombrar todos los nombres escritos en el fichero CUE, pero os podéis evitar este trabajo extra simplemente no renombrando los ficheros BIN. Una vez que hayáis obtenido el fichero en formato ISO, seréis libres de renombrarlo como os plazca.

## Convertir un fichero BIN en ISO

Ahora veremos cómo convertir los ficheros `.BIN` ficheros `.ISO`. Para ello, usaremos [**IsoBuster**](http://www.isobuster.com/fr/), una versión demo gratuita.

* Abrid IsoBuster.
* Id al menú `Fichero` y pulsad en `Abrir Archivo de Imagen`.
* Buscad la localización del juego que deseáis convertir a BIN/CUE.
* Como no se pueden seleccionar varios ficheros `.BIN` para incorporarlos a IsoBuster, el truco consiste en usar el archivo `.CUE`, que a su vez indexa todos los ficheros `.BIN` que componen el juego. En nuestro ejemplo no es interesante porque solamente hay un fichero `.BIN`, pero el principio sería el mismo con 3 o 30 archivos `.BIN`: abrid  el fichero `.CUE`.

Si vuestra rom está en 1 fichero BIN y habéis borrado el fichero `.CUE`, no os preocupéis, el proceso funciona igual seleccionando directamente el fichero `.BIN`.

>Pueden darse casos especiales, como con Rayman, que consta de sólo un _Rayman.bin_ y sólo un _Rayman.cue_.
>
>El fichero `.CUE` es mucho más completo y también indexa las pistas de audio del juego. No todos los fichero `.CUE` son así de completos, pero es una buena costumbre trabajar siempre con el fichero `.CUE`.
{.is-warning}

>Aqui tenéis [una base de datos](https://www.google.fr/search?q=cue_and_sbis) que contiene todos los ficheros CUE de todas las roms PSX.
{.is-info}

* En la columna de la izquierda, pulsad con el botón derecho del ratón en el [CD] raíz, luego, en los menús, pulsad en **Extraer CD** y pulsad en **Datos brutos (`.BIN`, `.ISO`)**.
* Podéis dar un nombre a vuestro fichero de exportación, el nombre del juego que quieras.  Normalmente, en la mayoría de los programas windows, cuando no escribimos una extensión de fichero (`.ISO`) pero que hemos seleccionado `.ISO` justo debajo, el fichero se grabará con la extensión `.ISO`. Con IsoBuster, esto no es así, el fichero no tendrá ninguna extensión si no la escribimos explícitamente. Podéis añadirla más tarde renombrando el fichero generado, pero para evitar errores, se aconseja escribir la extensión `.ISO` en el nombre del fichero.
* La conversión tardará unos segundos.
* Si habéis seleccionado un `.CUE', pulsad `No'. De todas formas, como el fichero `.CUE` se está utilizando en el programa, no se puede sobrescribir.

Vuestro juego se encuentra ahora en el formato ISO, que se habrá generado a partir de los ficheros originales `.BIN` y `.CUE`.

## BIN comprimido (BIN.ECM)

Terminaremos con el caso más raro de los BIN comprimidos, en formato `.BIN.ECM`.
Para eliminar la extensión `.ECM` y obtener un `.BIN`, es necesario utilizar un programa de descompresión. Existen varios métodos para hacerlo:

## {.tabset}
### Windows
* Descargad el programa `Un-ECM.exe` [en esta página](https://archive.org/details/ECMToolsV1.0).
* Extraed el programa Un-ECM.exe en un directorio del mismo nombre, y acordaros de la ruta de instalación.
* Pulsad con el botón derecho del ratón sobre vuestro fichero `.BIN.ECM` y seleccionad `Abrir con`.
* En la lista de programas, hacia abajo pulsad en `Encontrar otra aplicación en este PC` y abridlo con el programa `Un-ECM.exe`.
* Podéis marcar la casilla `Abrir siempre con esta aplicación` para que, en el futuro, al hacer doble clic en un fichero .BIN.ECM se descomprima automáticamente.

### macOS
* Con Homebrew, podéis instalar `ecm` y `unecm` vía [SSH](./../../../../tutorials/system/access/root-access-terminal-cli) :

`brew install ecm`

* Para comprimir:

`ecm jeu.bin`

* Para descomprimir:

`unecm jeu.bin.ecm`

### Linux

* Existen varios métodos en línea de comandos, que consisten en arrastrar los ficheros al terminal. Podéis encontrar tutoriales adecuados buscando los términos "BIN.ECM" y "Un-ECM" + el nombre de vuestro sistema operativo.

El fichero BIN resultante se puede utilizar tal cual, o se puede convertir en un fichero ISO como se ha descrito anteriormente.