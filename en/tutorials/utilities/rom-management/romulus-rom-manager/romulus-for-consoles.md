---
title: Romulus for Consoles
description: Usage of Romulus Rom Manager for consoles (Romsets No-Intro particularly)
published: true
date: 2021-08-07T13:14:03.671Z
tags: romulus, consoles
editor: markdown
dateCreated: 2021-08-07T13:14:03.671Z
---

## Before starting

To sort the roms, Romulus needs a .dat file. This file will act as a catalog listing the list and description of each rom file in order to determine whether or not your file matches the file expected by the romset.

The Dat files most suitable for consoles are those from the **NO-INTRO** team. There are many other sources for these files but they will not be discussed here.

Go to [DAT-o-MATIC](https://datomatic.no-intro.org/?page=download) to download the No-Intro .dat files.

### DAT-o-MATIC

Here is an example of how to download a No-Intro .dat file. As the Dat-O-Matic site evolves regularly, it is possible that the method presented is no longer completely exhaustive.

#### 1. Home screen

![](/tutorials/utilities/rom-management/romulus/consoles/romulus1.png){.full-width}

* A : List of all systems with a .dat file available

#### 2. Download a .dat file

![](/tutorials/utilities/rom-management/romulus/consoles/romulus2.png){.full-width}

* **A** : Choose the system
* **B** : Click on **Prepare** to display the following screen

![](/tutorials/utilities/rom-management/romulus/consoles/romulus3.png)

* **A** : Start the download

### Header file

In order to work with Romulus, some systems will need an additional file named **Header** (for example, it is the case for the Nintendo NES). Think of downloading it at the same time as the .dat file.

![](/tutorials/utilities/rom-management/romulus/consoles/romulus4.png){.full-width}

### To go further

As you can see on the screenshots above, it is possible to customize the .dat file in order to obtain a single region, one or more languages, ... according to your needs.  
Depending on the system, these options can be more or less numerous but if you don't touch anything, you get the complete .dat.

## Add a .dat file

* Click on the icon "Add DATs" and select the .dat file(s) to add in Romulus. It is not necessary to decompress the files if they are in .zip format.

![](/tutorials/utilities/rom-management/romulus/consoles/romulus5.png)

>Drag and drop also works to add one or more files.
{.is-info}

* Validate by clicking on `YES` for the next 2 windows.

![](/tutorials/utilities/rom-management/romulus/consoles/romulus6.png){.full-width}

![](/tutorials/utilities/rom-management/romulus/consoles/romulus7.png){.full-width}

* The file is now added to the database.

![](/tutorials/utilities/rom-management/romulus/consoles/romulus8.png){.full-width}

The set of columns is quite self-explanatory but here is a brief description of each of them:

* **Description :** Description of the DAT file in your database (can be modified).
* **Version :** Version of the DAT file (for No-Intro files, it is the date and time of the file).
* **Owned sets:** Shows the number of owned sets and the total number of possible sets (useful for the arcade only).
* **Roms owned:** Shows the number of roms owned and the total number of roms in the file.
* **File mode :** Not relevant for consoles, but indicates the mode in which the roms will be stored (for details on other modes, see [Romulus for the Arcade](./romulus-for-arcade)).
* **Common:** Not used.
* **Added on:** Indicates the date you added the file to your database.
* **Last Scan:** Indicates the last time you performed a scan with this file.
* **Name:** Indicates the name of the file.
* **Path:** Path related to your database tree.

## Configure the added .dat file

### Properties of the file

* Right click on the file to configure and choose **Properties**.

![](/tutorials/utilities/rom-management/romulus/consoles/romulus9.png){.full-width}

* For consoles, make sure that the **File Mode** is set to **Not Split** (for more details on other modes, see [Romulus for the Arcade](./romulus-for-arcade)) then validate with **OK**.

![](/tutorials/utilities/rom-management/romulus/consoles/romulus10.png){.full-width}

### Define the folder for the roms

* Double-click on the chosen file to switch to the Scanner mode, then click on **Scan** :

![](/tutorials/utilities/rom-management/romulus/consoles/romulus11.png)

* In the window that opens, you need the following information:

![](/tutorials/utilities/rom-management/romulus/consoles/romulus12.png){.full-width}

**A** : Select an empty folder in which the valid roms found during the scan will be copied in order to build the romset.  
**B**: Eventually, define the backup folder. The non-validated roms present in the romset during a scan will be copied into this folder.  
**C** : Eventually, it is here that the Header file must be defined for the concerned .dat.

Once all the information is defined, click on **Close**.

## Build the romset

### Presentation of the Scanner page

The **Scanner** page is the most important page of Romulus. It is here that we see the list of sets and roms present in the .dat file and which will constitute the romset.

![](/tutorials/utilities/rom-management/romulus/consoles/romulus13.png){.full-width}

Important point of detail:

![](/tutorials/utilities/rom-management/romulus/consoles/romulus14.png)

These three symbols indicate the status of the romset. The **green** folder indicates how many games are complete and available, the **yellow** folder indicates how many games are incomplete and the **red** folder indicates how many games are missing (these symbols are also displayed in the list, in front of each game, to have the status of each game).  
It is possible to filter the list of games by clicking on these symbols to display only a part of the games. By default, all three folders are active in order to display all information. If, for example, you want to see only the incomplete or missing games, you have to click on the green folder to hide all the complete games.

### Building the romset

Click on **Batch build all selected profiles** :

![](/tutorials/utilities/rom-management/romulus/consoles/romulus15.png)

>It is also possible to use the **Reconstruct** command (to the left of the recommended one) but it is less convenient.
{.is-info}

A window opens and asks you to select the folder in which the roms to be compared to the .dat file are located. This folder must be different from the previously configured destination folder.

![](/tutorials/utilities/rom-management/romulus/consoles/romulus16.png){.full-width}

For the example, the folder _NES in bulkc_ contains unknown NES roms. Click on **OK** once the folder is selected, the following window opens:

![](/tutorials/utilities/rom-management/romulus/consoles/romulus17.png){.full-width}

Check **Yes for all** and validate with **OK**, the construction starts:

![](/tutorials/utilities/rom-management/romulus/consoles/romulus18.png){.full-width}

All you have to do is wait until the end of the process (which can be long).

![](/tutorials/utilities/rom-management/romulus/consoles/romulus19.png){.full-width}

The games found to be compliant have turned green and have been copied to the romset directory... All that remains is to start looking for the fullset for the most courageous.