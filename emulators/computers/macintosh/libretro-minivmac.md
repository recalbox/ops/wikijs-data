---
title: Libretro MinivMac
description: 
published: true
date: 2024-07-24T22:11:53.166Z
tags: libretro, 9.0+, minivmac
editor: markdown
dateCreated: 2023-02-26T18:04:41.164Z
---



## ![](/emulators/license.svg) License

This core is under [**GPLv2**](https://github.com/libretro/libretro-minivmac/blob/master/minivmac/COPYING.txt) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ❌ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Features

| Feature | Supported |
| :---: | :---: |
| Screenshots | ✔ |
| Core Options | ✔ |
| Controls | ✔ |
| Remapping | ✔ |

## ![](/emulators/bios.svg) BIOS

### List of optional bios

| Filename | Description | MD5 | Provided |
| :---: | :---: | :---: | :---: |
| MacII.ROM | - | 66223be1497460f1e60885eeb35e03cc | ❌ |
| MinivMacBootv2.dsk | System 7.5.5 Boot Disk | a249565f03b98d004ee7f019570069cd | ❌ |

### Location

Put the bios like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 macintosh
┃ ┃ ┃ ┃ ┣ 🗒 **MacII.ROM**
┃ ┃ ┃ ┃ ┣ 🗒 **MinivMacBootv2.dsk**

## ![](/emulators/roms.png) Roms

### Supported extensions

Roms must have the extension:

* .cmd
* .dsk
* .hvf
* .img
* .zip

This system supports compressed roms in .zip format. However, be careful, it is only an archive.

The files contained in the .zip must correspond to the extensions mentioned above.
Moreover, each .zip file must contain only one compressed rom.

### Location

Put the roms like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 macintosh
┃ ┃ ┃ ┃ ┣ 🗒 **game.zip**

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Accessing to options

You can configure various options in two different ways.

* Via RetroArch menu:

┣ 📁 RetroArch Menu
┃ ┣ 📁 Core options
┃ ┃ ┣ 🧩 Name_option

* Via the file `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core options

| Options | Options values | Variables (for configuration override) | Variables values |
| --- | --- | --- | --- |
| Status Bar | `Désactivé` ✅ / `Activé` | `minivmac_Statusbar` | `disabled` / `enabled` |
| Keyboard Type | `Callback` ✅ / `Poll` | `minivmac_kbdtype` | `Callback` / `Poll` |

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/libretro/libretro-minivmac/](https://github.com/libretro/libretro-minivmac/)
* **Libretro documentation**: [https://docs.libretro.com/library/minivmac/](https://docs.libretro.com/library/minivmac/)