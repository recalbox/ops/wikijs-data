---
title: FAQ Recalbox 8
description: 
published: true
date: 2023-09-26T12:49:07.840Z
tags: faq
editor: markdown
dateCreated: 2023-08-31T13:41:59.768Z
---

En esta FAQ, encontraréis los problemas más frecuentes. Intentaremos corregirlos en las futuras versiones :)

## Problemas conocidos

### Pantalla negra en los Raspberry Pi 3 o 4

Si tenéis un problema de pantalla negra en RPi4 ou RPi3, podéis intentar corregirlo siguiendo las siguientes etapas:

* Meted vuestra tarjeta SD en vuestro PC
* En la partición `RECALBOX`, abrid el fichero `cmdline.txt`.
* Añadir el siguiente texto al final de la línea existente: `video=HDMI-A-1:D video=HDMI-A-2:D`.
* Al final el contenido del fichero debe parecese a esto:
```
dwc_otg.fiq_fix_enable=1 sdhci-bcm2708.sync_after_dma=0 dwc_otg.lpm_enable=0 console=tty3 consoleblank=0 loglevel=3 elevator=deadline vt.global_cursor_default=0 logo.nologo splash label=RECALBOX rootwait fastboot noswap video=HDMI-A-1:D video=HDMI-A-2:D
```
* Grabad el fichero modificado.
* Desmontad con seguridad la tarjeta SD de vuestro PC.
* Probad a lanzar Recalbox de nuevo en vuestro Raspberry Pi con esta modificación.

### RPI3

| Lugar donde se produce el problema | Descripción | ¿Corregido? |
| --- | --- | --- |
| General | No hay imagen en ciertas pantallas HDMI y DSI | ✅ |

### RPI4

| Lugar donde se produce el problema | Description | ¿Corregido? |
| --- | --- | --- |
| Kodi | Mensaje de error al arrancar | ✅ |
| Kodi | Mando no reconocido, es necesario reasignar el mando en las opciones de Kodi | ✅ |
| Kodi | Problemas para leer el contenido de Netflix | ❌ |

### PC

| Lugar donde se produce el problema | Description | ¿Corregido? |
| --- | --- | --- |
| Kodi | Mensaje de error al arrancar | ✅ |
| Kodi |  Mando no reconocido, es necesario reasignar el mando en las opciones de Kodi | ✅ |
| PCSX2 | Problemas con el DPAD con rotación 1/4 de vuelta (izquier por abajo, etc.) | ✅ |
| Dolphin | Imposible salir del juego con el mando (hay que utilizar en un teclado ALT + F4) | ❌ |

### Odroid GO Advanced es un ladrillo (inutilizable depuès de aplicar la actualización)

La primera versión de la image 8.0.1 que fue publicada por Recalbox estaba corrompida. Unos días despues se remplazó por una nueva imagen que funciona bien.

Para los usuarios que instalaron la versión corrompida, aquí tenéis el proceso para desbloquar vuestra consola:

1. Descargad el fichero http://upgrade.recalbox.com/latest/odroidgo2/boot.tar.xz,
2. Utilizando un programa de descompresión que soporte la extensión XZ y TAR (Easy 7 zip por ejemplo), abrid dicho fichero.
3. Insertad vuestra tarjeta SD de la Odroid en vuestro PC.
4. Copiad los 3 ficheros que contiene el directorio boot del zip (linux, recalbox y uInitrd) en vuestra tarjeta SD, dentro del directorio boot igualmente. Reescribiréis así los ficheros corrompidos de la tarjeta SD.
5. Desmontad la tarjeta SD de vuestro PC con seguridad e insertadla en vuestra Odroid.