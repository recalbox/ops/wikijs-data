---
title: Libretro vecx
description: 
published: true
date: 2024-07-14T17:17:01.821Z
tags: libretro, vectrex, vecx
editor: markdown
dateCreated: 2023-09-08T09:35:30.880Z
---



## ![](/emulators/license.svg) Licencia

El core posee la siguiente licencia [**GPLv3**](https://github.com/libretro/libretro-vecx/blob/master/LICENSE.md).

## ![](/emulators/compatibility.png) Compatibilidad

 | RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | 

## ![](/emulators/features.png) Funcionalidades

| Funcionalidad | ¿Soportada? |
| :---: | :---: |
| Capturas de pantalla | ✔ |
| Grabaciones | ✔ |
| Grabaciones instantáneas | ✔ |
| Rebobinado | ✔ |
| Opciones del core | ✔ |
| RetroAchievements | ✔ |
| Mandos | ✔ |

## ![](/emulators/bios.svg) BIOS

>**No necesita bios.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Extensiones de roms soportadas

Las roms deben tener las extensiones siguientes:

* .bin
* .vec
* .zip
* .7z

Este sistema soporta las roms comprimidas en formato .zip/.7z. No obstante, se trata sólo de un archivo.

Los ficheros contenidos en los .zip/.7z deben tener las extensiones citadas previamente.
Además, cada fichero .zip/.7z sólo debe contener un fichero rom al interior.

### Emplazamiento

Disponed vuestras roms como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 vectrex
┃ ┃ ┃ ┃ ┣ 🗒 **juego.zip**

>Se aconsejan las roms del proyecto **No-Intro**.
{.is-success}

>Para más información sobre las roms, consultad [este tutorial](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

>Para conservar las configuraciones personalizadas cuando se produce una actualización del sistema Recalbox, os aconsejamos utilizar las [Sobrecargas de configuración](./../../../advanced-usage/configuration-override).
{.is-info}

### Acceder a las opciones

Podéis configurar las opciones del core de dos formas diferentes.

* Via el Menú RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via el fichero `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Opciones del core

| Opción | Valor de la opción | Variable (para la sobrecarga) | Valores posibles |
| --- | --- | --- | --- |
| Use Hardware Rendering | `Software` / `Hardware` ✅ | `vecx_use_hw` | `Software` / `Hardware` |
| Internal Resolution Multiplier | `1` ✅ / `2` / `3` / `4` | `vecx_res_multiplier` | `1` / `2` / `3` / `4` |
| Hardware Rendering Resolution | `434x540` ✅ / `515x640` / `580x720` / `618x768` / `824x1024` ✅ / `845x1050` / `869x1080` / `966x1200` / `1159x1440` / `1648x2048` | `vecx_res_hw` | `434x540` / `515x640` / `580x720` / `618x768` / `824x1024` / `845x1050` / `869x1080` / `966x1200` / `1159x1440` / `1648x2048` |
| Line brightness | `1` / `2` / `3` / `4` ✅ / `5` / `6` / `7` / `8` / `9` | `vecx_line_brightness` | `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` |
| Line width | `1` / `2` / `3` / `4` ✅ / `5` / `6` / `7` / `8` / `9` | `vecx_line_width` | `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` |
 | Line color | `White` ✅ / `Green` / `Cyan` / `Yellow` / `Magenta` / `Red` / `Blue` | `vecx_line_color` | `White` / `Green` / `Cyan` / `Yellow` / `Magenta` / `Red` / `Blue` |
| Bloom brightness | `0` / `1` / `2` / `3` / `4` ✅ / `5` / `6` / `7` / `8` / `9` | `vecx_bloom_brightness` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` |
| Bloom width | `2px` / `3px` / `4px` / `6px` / `8px` ✅ / `10px` / `12px` / `14px` / `16px` | `vecx_bloom_width` | `2px` / `3px` / `4px` / `6px` / `8px` / `10px` / `12px` / `14px` / `16px` |
| Scale vector display horizontally | `0.845` / `0.85` / `0.855` / `0.86` / `0.865` / `0.87` / `0.875` / `0.88` / `0.885` / `0.89` / `0.895` / `0.90` / `0.905` / `0.91` / `0.915` / `0.92` / `0.925` / `0.93` / `0.935` / `0.94` / `0.945` / `0.95` / `0.955` / `0.96` / `0.965` / `0.97` / `0.975` / `0.98` / `0.985` / `0.99` / `0.995` / `1` ✅ / `1.005` / `1.01` | `vecx_scale_x` | `0.845` / `0.85` / `0.855` / `0.86` / `0.865` / `0.87` / `0.875` / `0.88` / `0.885` / `0.89` / `0.895` / `0.90` / `0.905` / `0.91` / `0.915` / `0.92` / `0.925` / `0.93` / `0.935` / `0.94` / `0.945` / `0.95` / `0.955` / `0.96` / `0.965` / `0.97` / `0.975` / `0.98` / `0.985` / `0.99` / `0.995` / `1` / `1.005` / `1.01` |
| Scale vector display vertically | `0.845` / `0.85` / `0.855` / `0.86` / `0.865` / `0.87` / `0.875` / `0.88` / `0.885` / `0.89` / `0.895` / `0.90` / `0.905` / `0.91` / `0.915` / `0.92` / `0.925` / `0.93` / `0.935` / `0.94` / `0.945` / `0.95` / `0.955` / `0.96` / `0.965` / `0.97` / `0.975` / `0.98` / `0.985` / `0.99` / `0.995` / `1` ✅ / `1.005` / `1.01` | `vecx_scale_y` | `0.845` / `0.85` / `0.855` / `0.86` / `0.865` / `0.87` / `0.875` / `0.88` / `0.885` / `0.89` / `0.895` / `0.90` / `0.905` / `0.91` / `0.915` / `0.92` / `0.925` / `0.93` / `0.935` / `0.94` / `0.945` / `0.95` / `0.955` / `0.96` / `0.965` / `0.97` / `0.975` / `0.98` / `0.985` / `0.99` / `0.995` / `1` / `1.005` / `1.01` |
| Horizontal shift | `-0.03` / `-0.025` / `-0.02` / `-0.015` / `-0.01` / `-0.005` / `0` ✅ / `0.005` / `0.01` / `0.015` / `0.02` / `0.025` / `0.03` | `vecx_shift_x` | `-0.03` / `-0.025` / `-0.02` / `-0.015` / `-0.01` / `-0.005` / `0` / `0.005` / `0.01` / `0.015` / `0.02` / `0.025` / `0.03` |
| Vertical shift | `-0.035` / `-0.03` / `-0.025` / `-0.02` / `-0.015` / `-0.01` / `-0.005` / `0` ✅ / `0.005` / `0.01` / `0.015` / `0.02` / `0.025` / `0.03` / `0.035` / `0.04` / `0.045` / `0.05` / `0.055` / `0.06` / `0.065` / `0.07` / `0.075` / `0.08` / `0.085` / `0.09` / `0.095` / `0.1` | `vecx_shift_y` | `-0.035` / `-0.03` / `-0.025` / `-0.02` / `-0.015` / `-0.01` / `-0.005` / `0` / `0.005` / `0.01` / `0.015` / `0.02` / `0.025` / `0.03` / `0.035` / `0.04` / `0.045` / `0.05` / `0.055` / `0.06` / `0.065` / `0.07` / `0.075` / `0.08` / `0.085` / `0.09` / `0.095` / `0.1` |

## ![](/emulators/external-links.png) Enlaces externos
* **Código fuente utilizado** : [https://github.com/libretro/libretro-vecx/](https://github.com/libretro/libretro-vecx/)
* **Documentación Libretro** : [https://docs.libretro.com/library/vecx/](https://docs.libretro.com/library/vecx/)