---
title: Neo-Geo Pocket
description: 
published: true
date: 2021-08-09T12:54:29.133Z
tags: snk, neo-geo, handhelds, pocket, ngp
editor: markdown
dateCreated: 2021-07-06T07:53:54.119Z
---

![](/emulators/handheld/ngp.svg){.align-center}

## Technical data

* **Manufacturer**: SNK
* **Year of release**: 1999
* **Units sold**: 2 millions, including color version
* **CPUs**: Toshiba TLCS900H @ 6.144 MHz, Z80 @ 3.072 MHz
* **RAM**: 12K for 900H, 4K for Z80
* **ROM**: 64 Kbytes
* **Picture Processor Unit**: 16-bit
* **Resolution**: 160x152 (256×256 virtual screen)
* **Media**: ROM cartridge 4 MB max

## Presentation

The **Neo-Geo Pocket** is SNK's first handheld console. It was released in Japan at the end of 1998, and was stopped in 1999, when the Neo-Geo Pocket Color arrived, because of lower than expected sales of the monochrome Neo-Geo Pocket.

This system was only released in the Japanese and Hong Kong markets. Although it had a short life, some important games were released on it such as Samurai Shodown, and King of Fighters R-1.

The Neo-Geo Pocket can play a lot of the new color games. There are however notable exceptions, such as Sonic the Hedgehog Pocket Adventure or SNK vs. Capcom: Match of the Millennium and The Last Blade. The Neo-Geo Pocket Color is fully backwards compatible.

## Emulators

[Libretro FBNeo](libretro-fbneo)
[Libretro Mednafen_NGP](libretro-mednafen_ngp)
[Libretro-RACE](libretro-race)