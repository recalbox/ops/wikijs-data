---
title: Configuration of customized buttons
description: 
published: true
date: 2021-08-07T13:40:17.472Z
tags: configuration, customization, buttons
editor: markdown
dateCreated: 2021-08-07T13:20:50.507Z
---

You'll get everything about specific controller configurations.

Here are the available tutorials:

[Create a custom configuration per emulator](create-custom-configuration-per-emulator)
[How to customize controller mapping](how-to-customize-controller-mapping)
[N64 controller configuration](n64-controller-configuration)
