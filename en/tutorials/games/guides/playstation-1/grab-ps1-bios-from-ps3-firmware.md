---
title: Grab the ps1_rom.bin bios from the PS3 firmware
description: 
published: true
date: 2021-10-14T19:01:17.060Z
tags: bios, ps3, ps1
editor: markdown
dateCreated: 2021-10-14T19:01:17.060Z
---

## What is the bios named ps1_rom.bin?

This bios is a file that is provided in the updates of the PS3.

Its advantages :

* Universal, suitable for games of any origin.
* It's the only bios you can get legally without extracting the bios of your original console.

This tutorial will show you how to get this bios.

## How to get this bios ?

* You must first download and unzip the [RCPS3](https://rpcs3.net/) emulator, a PS3 game emulator. This one exists for Windows and Linux.

* Once unzipped, you must go to [this page](https://www.playstation.com/en-us/support/hardware/ps3/system-software/) and download the PS3 firmware.

* Once you have downloaded the PS3UPDAT.PUP file, open the RCPS3 emulator.

* Open the `File` menu and select `Install Firmware`.

![](/tutorials/games/guides/playstation-1/ps1-bios-from-ps3/ps1biosps3-1.png){.full-width}

* In the new window, look for the PS3UPDAT.PUP file and open it.

![](/tutorials/games/guides/playstation-1/ps1-bios-from-ps3/ps1biosps3-2.png){.full-width}

* You need to let RCPS3 work for a few minutes.

![](/tutorials/games/guides/playstation-1/ps1-bios-from-ps3/ps1biosps3-3.png){.full-width}

* When finished, go to the directory where you unzipped RCPS3 and go to the `/dev_flash/ps1_emu/` folder. You will find the bios file to grab.

![](/tutorials/games/guides/playstation-1/ps1-bios-from-ps3/ps1biosps3-4.png){.full-width}