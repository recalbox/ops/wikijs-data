---
title: Defining the IP address for the Wi-Fi network
description: The IP address changes too much times? Set network parameters to solve this. 
published: true
date: 2023-07-20T10:29:12.281Z
tags: wifi, network, address, ip, admin
editor: markdown
dateCreated: 2021-08-06T08:36:33.206Z
---

With Recalbox, it is possible to define the IP address to use with the Wi-Fi connection, this is can be done with the [recalbox.conf file](./../../../basic-usage/getting-started/recalboxconf-file). 

>Recalbox allows managing network parameters for specific use cases, a mistake in this part could drive to conflict or other issues, the best practice is using your router to do this tuning. Use this tutorial with intent. 
{.is-warning} 

Open the `recalbox.conf` file and find these lines:

```ini
## Wifi - static IP
## if you want a static IP address, you must set all 4 values (ip, netmask, gateway and nameservers)
## if any value is missing or all lines are commented out, it will fall back to the
## default of DHCP. For nameservers, you must set at least 1 nameserver.
;wifi.ip=manual ip address (ex: 192.168.1.2)
;wifi.netmask=network mask (ex: 255.255.255.0)
;wifi.gateway=ip address of gateway (ex: 192.168.1.1)
;wifi.nameservers=ip address of domain name servers, space separated (ex: 192.168.1.1 8.8.8.8)
```

Remove the symbol `;` beginning of last three lines and update values as desired. Here is an example:

```ini
wifi.ip=192.168.1.10
wifi.netmask=255.255.255.0
wifi.gateway=192.168.1.254
wifi.nameservers=8.8.8.8 8.8.4.4
```

Restart your Recalbox to acknowledge new parameters using the command `reboot`. 