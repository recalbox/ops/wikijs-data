---
title: Libretro Snes9X 2010
description: 
published: true
date: 2024-06-23T11:21:54.417Z
tags: libretro, snes, super nintendo, snes9x 2010
editor: markdown
dateCreated: 2021-05-21T08:27:28.475Z
---



## ![](/emulators/license.svg) Licence

Ce core est sous licence [**non commerciale**](https://github.com/libretro/snes9x/blob/master/LICENSE).

## ![](/emulators/compatibility.png) Compatibilité

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Fonctionnalités

| Fonctionnalité | Supporté |
| :---: | :---: |
| Redémarrage | ✔ |
| Captures d'écran | ✔ |
| Sauvegardes | ✔ |
| Sauvegardes instantanées | ✔ |
| Rembobinage | ✔ |
| Netplay | ✔ |
| Options du core | ✔ |
| RetroAchievements | ✔ |
| Cheats RetroArch | ✔ |
| Controls | ✔ |
| Remapping | ✔ |
| Multi-Mouse | ✔ |
| Softpatching | ✔ |
| Crop Overscan | ✔ |

## ![](/emulators/bios.svg) BIOS

>**Aucun bios n'est requis.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Extensions supportées

Les roms doivent avoir l'extension suivante :

* .fig
* .gd3
* .sfc
* .smc
* .swc
* .zip
* .7z

Ce système supporte les roms compressées au format .zip/.7z. Attention toutefois, il ne s'agit que d'une archive.

Les fichiers contenus dans les .zip/.7z doivent correspondre aux extensions citées précédemment.
De plus, chaque fichier .zip/.7z ne doit contenir qu'une seule rom compressée.

### Emplacement

Placez les roms comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 snes
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.zip**

>Les roms au format **No-Intro** sont vivement conseillées.
{.is-success}

>Pour plus d'information sur les roms, rendez-vous sur [ce tutoriel](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuration avancée de l'émulateur

>Pour pouvoir conserver vos configurations personnalisées lors d'une mise à jour, nous vous conseillons d'utiliser notre fonctionnalité [Surcharges de configuration](./../../../advanced-usage/configuration-override).
{.is-info}

### Accéder aux options

Vous pouvez configurer diverses options de deux façons différentes.

* Via le menu RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via le fichier `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Options du core

| Options | Valeurs des options | Variable (pour surcharge) | Valeurs des variables |
| --- | --- | --- | --- |
| Console Region (Restart) | `Auto` ✅ / `NTSC` / `PAL` | `snes9x_2010_region` | `auto` / `ntsc` / `pal` |
| Preferred Aspect Ratio | `4:3` / `Uncorrected` / `Auto` ✅ / `NTSC` / `PAL` | `snes9x_2010_aspect` | `4:3` / `uncorrected` / `auto` / `ntsc` / `pal` |
| Set Autofire Pulse | `Medium` ✅ / `Fast` / `Slow` | `snes9x_2010_turbodelay` | `medium` / `fast` / `slow` |
| Blargg NTSC Filter | `Désactivé` ✅ / `Monochrome` / `RF` / `Composite` / `S-Video` / `RGB` | `snes9x_2010_blargg` | `disabled` / `monochrome` / `rf` / `composite` / `s-video` / `rgb` |
| Frameskip | `Désactivé` ✅ / `Auto` / `Manual` | `snes9x_2010_frameskip` | `disabled` / `auto` / `manual` |
| Frameskip Threshold (%) | `15` / `18` / `21` / `24` / `27` / `30` / `33` ✅ / `36` / `39` / `42` / `45` / `48` / `51` / `54` / `57` / `60` | `snes9x_2010_frameskip_threshold` | `15` / `18` / `21` / `24` / `27` / `30` / `33` / `36` / `39` / `42` / `45` / `48` / `51` / `54` / `57` / `60` |
| SuperFX Overclock | `5 MHz (Underclock)` / `8 MHz (Underclock)` / `9 MHz (Underclock)` / `10 MHz (Default)` ✅ / `11 MHz` / `12 MHz` / `13 MHz` / `14 MHz` / `15 MHz` / `20 MHz` / `30 MHz` / `40 MHz` | `snes9x_2010_overclock` | `5 MHz (Underclock)` / `8 MHz (Underclock)` / `9 MHz (Underclock)` / `10 MHz (Default)` / `11 MHz` / `12 MHz` / `13 MHz` / `14 MHz` / `15 MHz` / `20 MHz` / `30 MHz` / `40 MHz` |
| Reduce Slowdown (Hack, Unsafe) | `Disabled` ✅ / `Light` / `Compatible` / `Max` | `snes9x_2010_overclock_cycles` | `disabled` / `light` / `compatible` / `max` |
| Reduce Flickering (Hack, Unsafe) | `Disabled` ✅ / `Enabled` | `snes9x_2010_reduce_sprite_flicker` | `disabled` / `enabled` |
| Block Invalid VRAM Access (Restart) | `Activé` ✅ / `Désactivé` | `snes9x_2010_block_invalid_vram_access` | `enabled` / `disabled` |

## ![](/emulators/external-links.png) Liens externes

* **Code source utilisé** : [https://github.com/libretro/snes9x2010/](https://github.com/libretro/snes9x2010/)
* **Documentation Libretro** : [https://docs.libretro.com/library/snes9x_2010/](https://docs.libretro.com/library/snes9x_2010/)