---
title: Libretro Flycast-Next
description: 
published: true
date: 2024-07-24T08:15:47.494Z
tags: libretro, naomi, 9.2+, flycast-next
editor: markdown
dateCreated: 2024-07-24T07:51:22.129Z
---

**Libretro Flycast** is a cross-platform **Sega Dreamcast** emulator capable of emulating the following systems:

* **NAOMI M1** 
* **NAOMI M2** 
* **NAOMI M4** 

## ![](/emulators/license.svg) License

This core is under [**GPLv2**](https://github.com/flyinghead/flycast/blob/master/LICENSE) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ❌ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Features

| Feature | Supported |
| :---: | :---: |
| Restart | ✔ |
| Screenshots | ✔ |
| Saves | ✔ |
| Core Options | ✔ |
| RetroArch Cheats | ✔ |
| Controls | ✔ |
| Remapping | ✔ |
| Rumble | ✔ |
| Disk Control | ✔ |

## ![](/emulators/bios.svg) BIOS

### Mandatory bios list

| Filename | Description | MD5 | Provided |
| :---: | :---: | :---: | :---: |
| naomi.zip | MAME format since Romset MAME 0.154 | eb4099aeb42ef089cfe94f8fe95e51f6 | ❌ |

### Optional bios list

| Filename | Description | MD5 | Provided |
| :---: | :---: | :---: | :---: |
| airlbios.zip | Naomi Airline Pilots deluxe Bios from MAME | 3f348c88af99a40fbd11fa435f28c69d | ❌ |
| hod2bios.zip | Naomi The House of the Dead 2 Bios from MAME | 9c755171b222fb1f4e1439d5b709dbf1 | ❌ |

### Location

Put your bios like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 dc
┃ ┃ ┃ ┃ ┣ 🗒 **naomi.zip**
┃ ┃ ┃ ┃ ┣ 🗒 **airlbios.zip**
┃ ┃ ┃ ┃ ┣ 🗒 **hod2bios.zip**

## ![](/emulators/roms.png) Roms

Flycast is based on the Romset of **Mame** but also on **nullDC** formats for its Naomi part.

### Mame Romset

Only Naomi romsets **from a MAME romset 0.135 or higher** are compatible !   
We recommend the latest romset 0.235 which will bring its share of additional compatibility!
For more info on the current romset version: [MameDev](https://www.mamedev.org/release.html).

>The dat file to sort your arcade roms is available in the folder `/recalbox/share/bios/dc/`
{.is-info}

### Romset NullDC

These roms are compatible with Flycast but less reliable than roms from a Mame Romset.

>The **NullDC** roms are in the format: _`.bin + .lst`_.
{.is-info}

Example for the game `cfield.zip` :

* `/recalbox/share/roms/naomi/cfield/...`
* `/recalbox/share/roms/naomi/cfield/cfield.bin`
* `/recalbox/share/roms/naomi/cfield/cfield.lst`

### Location

Put your roms like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 naomi
┃ ┃ ┃ ┃ ┣ 🗒 **game.zip**

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Accessing to options

You can configure various options in two different ways.

* Via RetroArch menu:

┣ 📁 RetroArch Menu
┃ ┣ 📁 Core options
┃ ┃ ┣ 🧩 Name_option

* Via the file `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core options

| Options | Options values | Variables (for configuration override) | Variables values |
| --- | --- | --- | --- |
| Region | `Japon` / `États-Unis` ✅ / `Europe` / `Par défaut` | `reicast_region` | `Japan` / `USA` / `Europe` / `Default` |
| Enable DSP | `Disabled` / `Enabled` ✅ | `reicast_enable_dsp` | `disabled` / `enabled` |
| Force Windows CE Mode | `Disabled` ✅ / `Enabled` | `reicast_force_wince` | `disabled` / `enabled` |
| Allow NAOMI Service Buttons | `Disabled` ✅ / `Enabled` | `reicast_allow_service_buttons` | `disabled` / `enabled` |
| Set NAOMI Games to Free Play | `Disabled` / `Enabled` ✅ | `reicast_force_freeplay` | `disabled` / `enabled` |
| Internal Resolution | `320x240` / `640x480` ✅ / `800x600` / `960x720` / `1024x768` / `1280x960` / `1440x1080` / `1600x1200` / `1920x1440` / `2560x1920` / `2880x2160` / `3200x2400` / `3840x2880` / `4480x3360` / `5120x3840` / `5760x4320` / `6400x4800` / `7040x5280` / `7680x5760` / `8320x6240` / `8960x6720` / `9600x7200` / `10240x7680` / `10880x8160` / `11520x8640` / `12160x9120` / `12800x9600` | `reicast_internal_resolution` | `320x240` / `640x480` / `800x600` / `960x720` / `1024x768` / `1280x960` / `1440x1080` / `1600x1200` / `1920x1440` / `2560x1920` / `2880x2160` / `3200x2400` / `3840x2880` / `4480x3360` / `5120x3840` / `5760x4320` / `6400x4800` / `7040x5280` / `7680x5760` / `8320x6240` / `8960x6720` / `9600x7200` / `10240x7680` / `10880x8160` / `11520x8640` / `12160x9120` / `12800x9600` |
| Screen Orientation | `Horizontal` ✅ / `Vertical` | `reicast_screen_rotation` | `horizontal` / `vertical` |
| Alpha Sorting | `Per-Strip (fast, least accurate)` / `Per-Triangle (normal)` ✅ / `Per-Pixel (accurate, but slower)` | `reicast_alpha_sorting` | `per-strip (fast, least accurate)` / `per-triangle (normal)` / `per-pixel (accurate)` |
| Full framebuffer emulation | `Disabled` ✅ / `Enabled` | `reicast_emulate_framebuffer` | `disabled` / `enabled` |
| Enable RTT (Render To Texture) Buffer | `Disabled` ✅ / `Enabled` | `reicast_enable_rttb` | `disabled` / `enabled` |
| Mipmapping | `Disabled` / `Enabled` ✅ | `reicast_mipmapping` | `disabled` / `enabled` |
| Fog Effects | `Disabled` / `Enabled` ✅ | `reicast_fog` | `disabled` / `enabled` |
| Volume Modifier | `Disabled` / `Enabled` ✅ | `reicast_volume_modifier_enable` | `disabled` / `enabled` |
| Anisotropic Filter | `Disabled` / `2` / `4` ✅ / `8` / `16` | `reicast_anisotropic_filtering` | `off` / `2` / `4` / `8` / `16` |
| Texture Filtering | `Default` ✅ / `Force Nearest-Neighbor` / `Force Linear` | `reicast_texture_filtering` | `0` / `1` / `2` |
| Delay Frame Swapping | `Disabled` ✅ / `Enabled` | `reicast_delay_frame_swapping` | `disabled` / `enabled` |
| Detect Frame Rate Changes | `Disabled` ✅ / `Enabled` | `reicast_detect_vsync_swap_interval` | `disabled` / `enabled` |
| PowerVR2 Post-processing Filter | `Disabled` ✅ / `Enabled` | `reicast_pvr2_filtering` | `disabled` / `enabled` |
| Texture Upscaling (xBRZ) | `Disabled` ✅ / `x2` / `x4` / `x6` | `reicast_texupscale` | `disabled` / `2` / `4` / `6` |
| Texture Upscaling Max. Filtered Size | `256` / `512` / `1024` | `reicast_` | `256` / `512` / `1024` |
| Native Depth Interpolation | `Disabled` ✅ / `Enabled` | `reicast_native_depth_interpolation` | `disabled` / `enabled` |
| Threaded Rendering | `Disabled` / `Enabled` ✅ | `reicast_thraded_rendering` | `disabled` / `enabled` |
| Auto Skip Frame | `Disabled` ✅ / `Normal` / `Maximal` | `reicast_auto_skip_frame` | `disabled` / `some` / `more` |
| Frame Skipping | `Disabled` ✅ / `1` / `2` / `3` / `4` / `5` / `6` | `reicast_frame_skipping` | `disabled` / `1` / `2` / `3` / `4` / `5` / `6` |
| Widescreen Cheats (Restart Required) | `Disabled` ✅ / `Enabled` | `reicast_widescreen_cheats` | `disabled` / `enabled` |
| Widescreen Hack | `Disabled` ✅ / `Enabled` | `reicast_widescreen_hack` | `disabled` / `enabled` |
| SH4 CPU under/overclock | `100 MHz` / `110 MHz` / `120 MHz` / `130 MHz` / `140 MHz` / `150 MHz` / `160 MHz` / `170 MHz` / `180 MHz` / `190 MHz` / `200 MHz` / `210 MHz` / `220 MHz` / `230 MHz` / `240 MHz` / `250 MHz` / `260 MHz` / `270 MHz` / `280 MHz` / `290 MHz` / `300 MHz` | `reicast_sh4clock` | `100` / `110` / `120` / `130` / `140` / `150` / `160` / `170` / `180` / `190` / `200` / `210` / `220` / `230` / `240` / `250` / `260` / `270` / `280` / `290` / `300`  |
| Load Custom Textures | `Disabled` ✅ / `Enabled` | `reicast_custom_textures` | `disabled` / `enabled` |
| Dump Textures | `Disabled` ✅ / `Enabled` | `reicast_dump_textures` | `disabled` / `enabled` |
| Analog Stick Deadzone | `0%` / `5%` / `10%` / `15%` ✅ / `20%` / `25%` / `30%` | `reicast_analog_stick_deadzone` | `0%` / `5%` / `10%` / `15%` / `20%` / `25%` / `30%` |
| Trigger Deadzone | `0%` ✅ / `5%` / `10%` / `15%` / `20%` / `25%` / `30%` | `reicast_trigger_deadzone` | `0%` / `5%` / `10%` / `15%` / `20%` / `25%` / `30%` |
| Digital Triggers | `Disabled` ✅ / `Enabled` | `reicast_digital_triggers` | `disabled` / `enabled` |
| Broadcast Digital Outputs | `Disabled` ✅ / `Enabled` | `reicast_network_output` | `disabled` / `enabled` |
| Gun Crosshair 1 Display | `Disabled` ✅ / `White` / `Red` / `Green` / `Blue` | `reicast_lightgun1_crosshair` | `disabled` / `White` / `Red` / `Green` / `Blue` |
| Gun Crosshair 2 Display | `Disabled` ✅ / `White` / `Red` / `Green` / `Blue` | `reicast_lightgun2_crosshair` | `disabled` / `White` / `Red` / `Green` / `Blue` |
| Gun Crosshair 3 Display | `Disabled` ✅ / `White` / `Red` / `Green` / `Blue` | `reicast_lightgun3_crosshair` | `disabled` / `White` / `Red` / `Green` / `Blue` |
| Gun Crosshair 4 Display | `Disabled` ✅ / `White` / `Red` / `Green` / `Blue` | `reicast_lightgun4_crosshair` | `disabled` / `White` / `Red` / `Green` / `Blue` |
| VMU Sounds | `Disabled` ✅ / `Enabled` | `reicast_vmu_sound` | `disabled` / `enabled` |

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/flyinghead/flycast/](https://github.com/flyinghead/flycast/)