---
title: Libretro Potator
description: 
published: true
date: 2024-06-22T23:10:56.680Z
tags: 
editor: markdown
dateCreated: 2021-05-21T08:32:18.456Z
---

**Libretro Potator** est un émulateur de supervision Watara basé sur la version Normmatt.

## ![](/emulators/license.svg) Licence

Ce core est sous licence [**Domaine Public**](https://github.com/libretro/potator/blob/master/LICENSE).

## ![](/emulators/compatibility.png) Compatibilité

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Fonctionnalités



## ![](/emulators/bios.svg) BIOS

>**Aucun bios n'est requis.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Extensions supportées

Les roms doivent avoir les extensions suivantes :

* .sv
* .zip
* .7z

Ce système supporte les roms compressées au format .zip/.7z. Attention toutefois, il ne s'agit que d'une archive.

Les fichiers contenus dans les .zip/.7z doivent correspondre aux extensions citées précédemment.
De plus, chaque fichier .zip/.7z ne doit contenir qu'une seule rom compressée.

### Emplacement

Placez les roms comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 supervision
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.zip**

>Les roms au format **No-Intro** sont vivement conseillées.
{.is-success}

>Pour plus d'information sur les roms, rendez-vous sur [ce tutoriel](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuration avancée de l'émulateur

>Pour pouvoir conserver vos configurations personnalisées lors d'une mise à jour, nous vous conseillons d'utiliser notre fonctionnalité [Surcharges de configuration](./../../../advanced-usage/configuration-override).
{.is-info}

### Accéder aux options

Vous pouvez configurer diverses options de deux façons différentes.

* Via le menu RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via le fichier `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Options du core

| Options | Valeur des options | Variable (pour surcharge) | Valeurs des variables |
| --- | --- | --- | --- |
| Internal Palette | `Greyscale` ✅ / `Potator Amber` / `Potator Green` / `Potator Blue` / `Potator BGB` / `Potator Wataroo` / `Game Boy DMG` / `Game Boy Pocket` / `Game Boy Light` / `Blossom Pink` / `Bubbles Blue` / `Buttercup Green` / `Digivice` / `Game.com` / `GameKing` / `Game Master` / `Golden Wild` / `Greenscale` / `Hokage Orange` / `Labo Fawn` / `Legendary Super Saiyan` / `Microvision` / `Million Live Gold` / `Odyssey Gold` / `Shiny Sky Blue` / `Slime Blue` / `TI-83` / `Travel Wood` / `Virtual Boy` | `potator_palette` | `default` / `potator_amber` / `potator_green` / `potator_blue` / `potator_bgb` / `potator_wataroo` / `gb_dmg` / `gb_pocket` / `gb_light` / `blossom_pink` / `bubbles_blue` / `buttercup_green` / `digivice` / `game_com` / `gameking` / `game_master` / `golden_wild` / `greenscale` / `hokage_orange` / `labo_fawn` / `legendary_super_saiyan` / `microvision` / `million_live_gold` / `odyssey_gold` / `shiny_sky_blue` / `slime_blue` / `ti_83` / `travel_wood` / `virtual_boy` |
| LCD Ghosting | `Disabled` ✅ / `1 Frame` / `2 Frames` / `3 Frames` / `4 Frames` / `5 Frames` / `6 Frames` / `7 Frames` / `8 Frames` | `potator_lcd_ghosting` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` |
| Frameskip | `Désactivé` ✅ / `Auto` / `Manual` | `potator_frameskip` | `disabled` / `auto` / `manual` |
| Frameskip Threshold (%) | `15` / `18` / `21` / `24` / `27` / `30` / `33` ✅ / `36` / `39` / `42` / `45` / `48` / `51` / `54` / `57` / `60` | `potator_frameskip_threshold` | `15` / `18` / `21` / `24` / `27` / `30` / `33` / `36` / `39` / `42` / `45` / `48` / `51` / `54` / `57` / `60` |

## ![](/emulators/external-links.png) Liens externes

* **Code source utilisé** : [https://github.com/libretro/potator/](https://github.com/libretro/potator/)