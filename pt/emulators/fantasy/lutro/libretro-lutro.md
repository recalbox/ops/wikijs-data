---
title: Libretro Lutro
description: 
published: true
date: 2021-10-19T23:23:33.778Z
tags: libretro, lutro
editor: markdown
dateCreated: 2021-10-19T23:23:33.778Z
---

**Lutro** é uma estrutura de jogos lua experimental que segue o API LÖVE. Os jogos de Lutro podem ser jogados com LibRetro / RetroArch através do núcleo Lutro.

## ![](/emulators/license.svg) Licença

Este núcleo está licenciado sob [**MIT**](https://github.com/libretro/libretro-lutro/blob/master/LICENSE).

## ![](/emulators/compatibility.png) Compatibilidade

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Recursos

| Recurso | Suportado |
| :---: | :---: |
| Controles | ✔ |
| Remapear | ✔ |

## ![](/emulators/bios.svg) BIOS

>**Não são necessárias BIOS.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Extensões suportadas

As ROMs devem ter a extensão :

* .lutro
* .lua

### Local

Coloque os roms desta maneira:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 lutro
┃ ┃ ┃ ┃ ┣ 🗒 **jogo.lutro**

## ![](/emulators/advanced-configuration.png) Configuração avançada do emulador

>**Este núcleo não tem opção**.
{.is-success}

## ![external-links.png](/emulators/external-links.png) Links externos

* **Código fonte utilizado** : [https://github.com/libretro/libretro-lutro](https://github.com/libretro/libretro-lutro)
* **Documentação do Libretro** : [https://docs.libretro.com/library/lutro/](https://docs.libretro.com/library/lutro/)