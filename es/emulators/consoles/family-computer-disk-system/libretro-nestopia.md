---
title: Libretro Nestopia
description: 
published: true
date: 2024-07-29T13:46:20.226Z
tags: libretro, famicom, family computer disk system, nestopia
editor: markdown
dateCreated: 2023-09-06T11:19:39.220Z
---

## ![](/emulators/license.svg) Licencia

Este núcleo está bajo la licencia [**GPLv2**](https://github.com/libretro/nestopia/blob/master/COPYING).

## ![](/emulators/compatibility.png) Compatibilidad

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Funcionalidades

| Funcionalidad | Soportado |
| :---: | :---: |
| Reinicio | ✔ |
| Capturas de pantalla | ✔ |
| Guardados | ✔ |
| Guardados instantáneos | ✔ |
| Rebobinado | ✔ |
| Netplay | ✔ |
| Opciones del núcleo | ✔ |
| Trucos de RetroArch | ✔ |
| Controles | ✔ |
| Remapeo | ✔ |
| Softpatching | ✔ |

## ![](/emulators/bios.svg) BIOS

### Lista de BIOS obligatorios

| Nombre de archivo | Descripción | MD5 | Proporcionado |
| :---: | :---: | :---: | :---: |
| disksys.rom | BIOS Family Computer Disk System | ca30b50f880eb660a320674ed365ef7a | ❌  |

### Ubicación

Coloque los BIOS de la siguiente manera:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 fds
┃ ┃ ┃ ┃ ┣ 🗒 **disksys.rom**

## ![](/emulators/roms.png) Roms

### Extensiones soportadas

Los roms deben tener las siguientes extensiones:

* .fds
* .zip
* .7z

Este sistema soporta roms comprimidos en formato .zip/.7z. Sin embargo, solo se trata de un archivo.

Los archivos contenidos en los .zip/.7z deben corresponder a las extensiones mencionadas anteriormente.
Además, cada archivo .zip/.7z no debe contener más de una rom comprimida.

### Ubicación

Coloque los roms de la siguiente manera:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 fds
┃ ┃ ┃ ┃ ┣ 🗒 **juego.zip**

>Se recomienda encarecidamente el uso de roms en formato **No-Intro**.
{.is-success}

>Para más información sobre los roms, visite [este tutorial](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

>Para poder conservar sus configuraciones personalizadas durante una actualización, le recomendamos utilizar nuestra funcionalidad [Sobrecargas de configuración](./../../../advanced-usage/configuration-override).
{.is-info}

### Acceder a las opciones

Puede configurar diversas opciones de dos maneras diferentes.

* A través del menú RetroArch:

┣ 📁 Menú RetroArch
┃ ┣ 📁 Opciones del núcleo
┃ ┃ ┣ 🧩 Name_option

* A través del archivo `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Opciones del núcleo

| Opciones | Valor de las opciones | Variable (para sobrecarga) | Valores de las variables |
| --- | --- | --- | --- |
| Región del sistema | `Auto` ✅ / `NTSC` / `PAL` / `Famicom` / `Dendy` | `nestopia_favored_system` | `auto` / `ntsc` / `pal` / `famicom` / `dendy` |
| Inserción automática del FDS | `Desactivado` / `Activado` ✅ | `nestopia_fds_auto_insert` | `disabled` / `enabled` |
| Filtro NTSC de Blargg | `Desactivado` ✅ / `Vídeo compuesto` / `S-Video` / `Péritel RVB (SCART)` / `Monocromo` | `nestopia_blargg_ntsc_filter` | `disabled` / `composite` / `svideo` / `rgb` / `monochrome` |
| Paleta | `CXA2025AS` ✅ / `Consumidor` / `Canónico` / `Alternativo` / `RGB` / `PAL` / `FBx compuesto directo` / `FBx estilo PVM D93` / `FBx hardware NTSC` / `FBx NES Classic (corregida)` / `Datos en bruto` / `Personalizada` | `nestopia_palette` | `cxa2025as` / `consumer` / `composite` / `alternate` / `rgb` / `pal` / `composite-direct-fbx` / `pvm-style-d93-fbx` / `ntsc-hardware-fbx` / `nes-classic-fbx-fs` / `raw` / `custom` |
| Máscara de overscan (Vertical superior) | `0` / `4` / `8` ✅ / `12` / `16` / `20` / `24` | `nestopia_overscan_v_top` | `0` / `4` / `8` / `12` / `16` / `20` / `24` |
| Máscara de overscan (Vertical inferior) | `0` / `4` / `8` ✅ / `12` / `16` / `20` / `24` | `nestopia_overscan_v_bottom` | `0` / `4` / `8` / `12` / `16` / `20` / `24` |
| Máscara de overscan (Horizontal izquierdo) | `0` ✅ / `4` / `8` / `12` / `16` / `20` / `24` | `nestopia_overscan_h_left` | `0` / `4` / `8` / `12` / `16` / `20` / `24` |
| Máscara de overscan (Horizontal derecho) | `0` ✅ / `4` / `8` / `12` / `16` / `20` / `24` | `nestopia_overscan_h_right` | `0` / `4` / `8` / `12` / `16` / `20` / `24` |
| Relación de aspecto preferida | `Auto` ✅ / `NTSC` / `PAL` / `4:3` | `nestopia_aspect` | `auto` / `ntsc` / `pal` / `4:3` |
| Distorsión sonora del Game Genie | `Desactivado` ✅ / `Activado` | `nestopia_genie_distortion` | `disabled` / `enabled` |
| Volumen del canal cuadrado 1 (%) | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `nestopia_audio_vol_sq1` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
| Volumen del canal cuadrado 2 (%) | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `nestopia_audio_vol_sq2` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
| Volumen del canal triángulo (%) | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `nestopia_audio_vol_tri` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
| Volumen del canal de ruido (%) | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `nestopia_audio_vol_noise` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
| Volumen del canal DPCM (%) | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `nestopia_audio_vol_dpcm` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
| Volumen del canal FDS (%) | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `nestopia_audio_vol_fds` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
| Volumen del canal MMC5 (%) | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `nestopia_audio_vol_mmc5` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
| Volumen del canal VRC6 (%) | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `nestopia_audio_vol_vrc6` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
| Volumen del canal VRC7 (%) | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `nestopia_audio_vol_vrc7` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
| Volumen del canal N163 (%) | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `nestopia_audio_vol_n163` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
| Volumen del canal S5B (%) | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `nestopia_audio_vol_s5b` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
| Salida de audio | `mono` / `estéreo` ✅ | `nestopia_audio_type` | `mono` / `stereo` |
| Adaptador de 4 jugadores | `Auto` ✅ / `NTSC` / `Famicom` | `nestopia_select_adapter` | `auto` / `ntsc` / `famicom` |
| Teclas desplazadas en el sentido de las agujas del reloj | `Desactivado` ✅ / `Activado` | `nestopia_button_shift` | `disabled` / `enabled` |
| Dispositivo Arkanoid | `Ratón` ✅ / `Puntero` | `nestopia_arkanoid_device` | `mouse` / `pointer` |
| Dispositivo Zapper | `Pistola` ✅ / `Ratón` / `Puntero` | `nestopia_zapper_device` | `lightgun` / `mouse` / `pointer` |
| Mostrar la mira | `Desactivado` / `Activado` ✅ | `nestopia_show_crosshair` | `disabled` / `enabled` |
| Velocidad de pulso del turbo | `2` ✅ / `3` / `4` / `5` / `6` / `7` / `8` / `9` | `nestopia_turbo_pulse` | `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` |
| Eliminar el límite de sprites | `Desactivado` ✅ / `Activado` | `nestopia_nospritelimit` | `disabled` / `enabled` |
| Velocidad del procesador (overclock) | `1x` ✅ / `2x` | `nestopia_overclock` | `1x` / `2x` |
| Estado de la RAM al encender | `0x00` ✅ / `0xFF` / `Aleatorio` | `nestopia_ram_power_state` | `0x00` / `0xFF` / `random` |

## ![](/emulators/external-links.png) Enlaces externos

* **Código fuente utilizado** : [https://github.com/libretro/nestopia/](https://github.com/libretro/nestopia/)
* **Documentación Libretro** : [https://docs.libretro.com/library/nestopia\_ue/](https://docs.libretro.com/library/nestopia_ue/)
