---
title: Atari ST
description: 
published: true
date: 2021-10-03T16:13:26.731Z
tags: atari, st, atari-st, ordinateurs
editor: markdown
dateCreated: 2021-05-21T07:56:13.419Z
---

![](/emulators/computers/atarist.svg){.align-center}

## Fiche technique

* **Fabricant** : Atari Corporation
* **Année de sortie** : 1985
* **Système d'exploitation** : Atari TOS
* **Processeur** : Motorola 68000 16-/32-bit CPU @ 8 MHz
* **RAM** : 512KB ou 1MB
* **Lecteur** : lecteur de disquette simple face 3½"
* **Puce sonore** : Yamaha YM2149 3-voice squarewave
* **Affichage** :
  * **Basse résolution** : 320 × 200 (16 couleurs), palette de 512 couleurs
  * **Moyenne résolution** : 640 × 200 (4 couleurs), palette de 512 couleurs

## Présentation

Les **Atari ST** forment une famille d'ordinateurs personnels conçus par la firme américaine Atari dont le succès commercial a marqué la deuxième moitié des années 1980 et le début des années 1990. Le succès fut autant grand public (jeux vidéo) que professionnel (traitement de texte, PAO et surtout MAO).

Le micro-ordinateur Atari ST marque plus particulièrement l’histoire informatique comme la machine ayant permis l'essor de la musique assistée par ordinateur et la démocratisation de la norme MIDI. En 2010, le magazine spécialisé Sound on Sound le classe dans les 25 produits marquants et responsables des changements de l'enregistrement musical. Cette machine est considérée encore aujourd'hui comme une référence dans le domaine en raison de sa robustesse et de son extrême précision pour le séquençage MIDI.

## Émulateurs

[Libretro Hatari](libretro-hatari)