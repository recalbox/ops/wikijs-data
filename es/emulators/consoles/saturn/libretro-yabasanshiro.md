---
title: Libretro YabaSanshiro
description: 
published: true
date: 2024-07-12T13:08:04.249Z
tags: libretro, saturn, yabasanshiro
editor: markdown
dateCreated: 2023-09-07T20:43:31.325Z
---

**Libretro YabaSanshiro** es una adaptación del emulador **Yabause** para el sistema Libretro que emula la Sega Saturn.

## ![](/emulators/license.svg) Licencia

El core posee la siguiente licencia [**GPLv2**](https://github.com/libretro/yabause/blob/master/yabause/COPYING).

## ![](/emulators/compatibility.png) Compatibilidad

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | 
| ❌ | ❌ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Funcionalidades



## ![](/emulators/bios.svg) BIOS

### Lista de BIOS obligatorias

| Nombre del fichero | Descripción | MD5 | ¿Preinstalada? |
| :---: | :---: | :---: | :---: |
| saturn_bios.bin | BIOS Saturn | af5828fdff51384f99b3c4926be27762 | ❌ |

### Emplazamiento

Disponed vuestras bios como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 saturn
┃ ┃ ┃ ┃ ┣ 🗒 **saturn_bios.bin**

## ![](/emulators/isos.svg) Isos

### Extensiones de roms soportadas

Las isos deben tener las extensiones siguientes:

* .bin/.cue
* .img/.ccd
* .iso
* .mds/.mdf
* .chd

### Emplazamiento

Disponed vuestras isos como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 saturn
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.bin**
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.cue**

>Se aconsejan las isos del proyecto **Redump**.
{.is-success}

>Para más información sobre las isos, consultad [este tutorial](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

>Para conservar las configuraciones personalizadas cuando se produce una actualización del sistema Recalbox, os aconsejamos utilizar las [Sobrecargas de configuración](./../../../advanced-usage/configuration-override).
{.is-info}

### Acceder a las opciones

Podéis configurar las opciones del core de dos formas diferentes.

* Via el Menú RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via el fichero `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Opciones del core

| Opción | Valor de la opción | Variable (para la sobrecarga) | Valores posibles |
| --- | --- | --- | --- |
| Force HLE BIOS (restart) | `Désactivé` ✅ / `Activé` | `yabasanshiro_force_hle_bios` | `disabled` / `enabled` |
| Auto-frameskip | `Activé` ✅ / `Désactivé` | `yabasanshiro_frameskip` | `enabled` / `disabled` |
| Addon Cartridge (restart) | `1M_extended_ram` / `4M_extended_ram` ✅ | `yabasanshiro_addon_cart` | `1M_extended_ram` / `4M_extended_ram` |
| System Language (restart) | `english` ✅ / `deutsch` / `french` / `spanish` / `italian` / `japanese` | `yabasanshiro_system_language` | `english` / `deutsch` / `french` / `spanish` / `italian` / `japanese` |
| 6Player Adaptor on Port 1 | `Désactivé` ✅ / `Activé` | `yabasanshiro_multitap_port1` | `disabled` / `enabled` |
| 6Player Adaptor on Port 2 | `Désactivé` ✅ / `Activé` | `yabasanshiro_multitap_port2` | `disabled` / `enabled` |
| SH2 Core (restart) | `dynarec` ✅ / `interpreter` | `yabasanshiro_sh2coretype` | `dynarec` / `interpreter` |
| Polygon Mode (restart) | `perspective_correction` ✅ / `gpu_tesselation` / `cpu_tesselation` | `yabasanshiro_polygon_mode` | `perspective_correction` / `gpu_tesselation` / `cpu_tesselation` |
| Resolution Mode (restart) | `original` ✅ / `2x` / `4x` / `720p` / `1080p` / `4k` | `yabasanshiro_resolution_mode` | `original` / `2x` / `4x` / `720p` / `1080p` / `4k` |
| RGB resolution mode | `original` ✅ / `2x` / `720p` / `1080p` / `Fit_to_emulation` | `yabasanshiro_rbg_resolution_mode` | `original` / `2x` / `720p` / `1080p` / `Fit_to_emulation` |
| RGB use compute shader for RGB | `Désactivé` ✅ / `Activé` | `yabasanshiro_rbg_use_compute_shader` | `disabled` / `enabled` |

## ![](/emulators/external-links.png) Enlaces externos

* **Código fuente utilizado** : [https://github.com/libretro/yabause/](https://github.com/libretro/yabause/)
* **Documentación Libretro** : [https://docs.libretro.com/library/yabasanshiro/](https://docs.libretro.com/library/yabasanshiro/)