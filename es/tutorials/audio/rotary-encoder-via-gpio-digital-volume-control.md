---
title: Codificador giratorio a través de los GPIO (control de volumen digital)
description: 
published: true
date: 2023-09-17T20:34:46.658Z
tags: gpio, digital, volumen, giratorio, codificador, controlador
editor: markdown
dateCreated: 2023-09-17T20:34:46.658Z
---

Esta pequeña guía y el script python que se presenta han sido adaptados del [proyecto de Savetheclocktower](https://gist.github.com/savetheclocktower/9b5f67c20f6c04e65ed88f2e594d43c1) (en inglés) que se encuentra disponible en GitHub.

## Propósito

Este script es para aquellos que quieren un butón físico de volumen en un proyecto Recalbox, tales como en las máquinas de arcade. Este script es útil para máquinas de arcade con altavoces que no tienen su propio botón de volumen físico, o que tendrían dificultades para mover el botón de control del altavoz cerca del usuario.

Este script utiliza un codificador rotatorio estándar de 5 pines y ha sido probado sobre el codificador de Adafruit. Se necesitan cinco cables para este codificador rotatorio: tres para la parte del botón (A, B y tierra), y dos para la parte de la tecla (común y tierra). Aquí hay una referencia para los pines GPIO de Raspberry Pi.

| Descripción | BCM # | Board # |
| :--- | :--- | :--- |
| bouton A | GPIO 26 | 37 |
| bouton B | GPIO 19 | 35 |
| bouton de la terre | pin de la terre en-dessous du GPIO 26 | 39 |
| touche commun | GPIO 13 | 33 |
| touche de la terre | pin de la terre opposé au GPIO 13 | 34 |

Podéis utilizar los pines que queráis; simplemente actualizad el script `volume-monitor.sh` si los cambiais. Si no disponéis de un interruptor pulsador en vuestro codificador rotatorio, dejad los pines desocupados. Se puede utilizar cualquier pin de tierra, los pines de tierra se indican únicamente en relación con su proximidad al conjuto, pero cualquier pin de tierra es válido.

### Demonio de volumen

El script que se presenta más abajo funciona de la siguiente manera: utiliza los pines especificados, y cuando el mando se gira en un sentido u otro, utiliza los estados de los pines A y B para saber si el mando se ha girado a la izquierda o a la derecha. Así se sabe si debe subir o bajar el volumen del sistema como respuesta al evento, lo que se hace con el programa de línea de comandos **Amixer**.

1. Para instalar el script en vuestra Recalbox: hay que montar la partición sistema [en modo lectura-escritura](./../../tutorials/system/access/remount-partition-with-write-access) vía SSH.
2. Cread vuestro script `volume-monitor.py` dentro de `/recalbox/scripts` usando nano:

```shell
nano /recalbox/scripts/volume-monitor.py
```

3. Copiad y pegad la secuencia de comandos que se muestra a continuación en el fichero y, a continuación, guardad dicho fichero utilizando `Ctrl` + `X` :


```python
#!/usr/bin/env python2

"""
The daemon responsible for changing the volume in response to a turn or press
of the volume knob.
The volume knob is a rotary encoder. It turns infinitely in either direction.
Turning it to the right will increase the volume; turning it to the left will
decrease the volume. The knob can also be pressed like a button in order to
turn muting on or off.
The knob uses two GPIO pins and we need some extra logic to decode it. The
button we can just treat like an ordinary button. Rather than poll
constantly, we use threads and interrupts to listen on all three pins in one
script.
"""

import os
import signal
import subprocess
import sys
import threading

from RPi import GPIO
from multiprocessing import Queue

DEBUG = False

# SETTINGS
# ========

# The two pins that the encoder uses (BCM numbering).
GPIO_A = 26   
GPIO_B = 19

# The pin that the knob's button is hooked up to. If you have no button, set
# this to None.
GPIO_BUTTON = 13 

# The minimum and maximum volumes, as percentages.
#
# The default max is less than 100 to prevent distortion. The default min is
# greater than zero because if your system is like mine, sound gets
# completely inaudible _long_ before 0%. If you've got a hardware amp or
# serious speakers or something, your results will vary.
VOLUME_MIN = 60
VOLUME_MAX = 96

# The amount you want one click of the knob to increase or decrease the
# volume. I don't think that non-integer values work here, but you're welcome
# to try.
VOLUME_INCREMENT = 1

# (END SETTINGS)
# 


# When the knob is turned, the callback happens in a separate thread. If
# those turn callbacks fire erratically or out of order, we'll get confused
# about which direction the knob is being turned, so we'll use a queue to
# enforce FIFO. The callback will push onto a queue, and all the actual
# volume-changing will happen in the main thread.
QUEUE = Queue()

# When we put something in the queue, we'll use an event to signal to the
# main thread that there's something in there. Then the main thread will
# process the queue and reset the event. If the knob is turned very quickly,
# this event loop will fall behind, but that's OK because it consumes the
# queue completely each time through the loop, so it's guaranteed to catch up.
EVENT = threading.Event()

def debug(str):
  if not DEBUG:
    return
  print(str)

class RotaryEncoder:
  """
  A class to decode mechanical rotary encoder pulses.
  Ported to RPi.GPIO from the pigpio sample here: 
  http://abyz.co.uk/rpi/pigpio/examples.html
  """

  def __init__(self, gpioA, gpioB, callback=None, buttonPin=None, buttonCallback=None):
    """
    Instantiate the class. Takes three arguments: the two pin numbers to
    which the rotary encoder is connected, plus a callback to run when the
    switch is turned.

    The callback receives one argument: a `delta` that will be either 1 or -1.
    One of them means that the dial is being turned to the right; the other
    means that the dial is being turned to the left. I'll be damned if I know
    yet which one is which.
    """

    self.lastGpio = None
    self.gpioA    = gpioA
    self.gpioB    = gpioB
    self.callback = callback

    self.gpioButton     = buttonPin
    self.buttonCallback = buttonCallback

    self.levA = 0
    self.levB = 0

    GPIO.setmode(GPIO.BCM)
    GPIO.setup(self.gpioA, GPIO.IN, pull_up_down=GPIO.PUD_UP)
    GPIO.setup(self.gpioB, GPIO.IN, pull_up_down=GPIO.PUD_UP)

    GPIO.add_event_detect(self.gpioA, GPIO.BOTH, self._callback)
    GPIO.add_event_detect(self.gpioB, GPIO.BOTH, self._callback)

    if self.gpioButton:
      GPIO.setup(self.gpioButton, GPIO.IN, pull_up_down=GPIO.PUD_UP)
      GPIO.add_event_detect(self.gpioButton, GPIO.FALLING, self._buttonCallback, bouncetime=500)


  def destroy(self):
    GPIO.remove_event_detect(self.gpioA)
    GPIO.remove_event_detect(self.gpioB)
    GPIO.cleanup()

  def _buttonCallback(self, channel):
    self.buttonCallback(GPIO.input(channel))

  def _callback(self, channel):
    level = GPIO.input(channel)
    if channel == self.gpioA:
      self.levA = level
    else:
      self.levB = level

    # Debounce.
    if channel == self.lastGpio:
      return

    # When both inputs are at 1, we'll fire a callback. If A was the most
    # recent pin set high, it'll be forward, and if B was the most recent pin
    # set high, it'll be reverse.
    self.lastGpio = channel
    if channel == self.gpioA and level == 1:
      if self.levB == 1:
        self.callback(1)
    elif channel == self.gpioB and level == 1:
      if self.levA == 1:
        self.callback(-1)

class VolumeError(Exception):
  pass

class Volume:
  """
  A wrapper API for interacting with the volume settings on the RPi.
  """
  MIN = VOLUME_MIN
  MAX = VOLUME_MAX
  INCREMENT = VOLUME_INCREMENT

  def __init__(self):
    # Set an initial value for last_volume in case we're muted when we start.
    self.last_volume = self.MIN
    self._sync()

  def up(self):
    """
    Increases the volume by one increment.
    """
    return self.change(self.INCREMENT)

  def down(self):
    """
    Decreases the volume by one increment.
    """
    return self.change(-self.INCREMENT)

  def change(self, delta):
    v = self.volume + delta
    v = self._constrain(v)
    return self.set_volume(v)

  def set_volume(self, v):
    """
    Sets volume to a specific value.
    """
    self.volume = self._constrain(v)
    output = self.amixer("set 'PCM' unmute {}%".format(v))
    self._sync(output)
    return self.volume

  def toggle(self):
    """
    Toggles muting between on and off.
    """
    if self.is_muted:
      output = self.amixer("set 'PCM' unmute")
    else:
      # We're about to mute ourselves, so we should remember the last volume
      # value we had because we'll want to restore it later.
      self.last_volume = self.volume
      output = self.amixer("set 'PCM' mute")

    self._sync(output)
    if not self.is_muted:
      # If we just unmuted ourselves, we should restore whatever volume we
      # had previously.
      self.set_volume(self.last_volume)
    return self.is_muted

  def status(self):
    if self.is_muted:
      return "{}% (muted)".format(self.volume)
    return "{}%".format(self.volume)

  # Read the output of `amixer` to get the system volume and mute state.
  #
  # This is designed not to do much work because it'll get called with every
  # click of the knob in either direction, which is why we're doing simple
  # string scanning and not regular expressions.
  def _sync(self, output=None):
    if output is None:
      output = self.amixer("get 'PCM'")

    lines = output.readlines()
    if DEBUG:
      strings = [line.decode('utf8') for line in lines]
      debug("OUTPUT:")
      debug("".join(strings))
    last = lines[-1].decode('utf-8')

    # The last line of output will have two values in square brackets. The
    # first will be the volume (e.g., "[95%]") and the second will be the
    # mute state ("[off]" or "[on]").
    i1 = last.rindex('[') + 1
    i2 = last.rindex(']')

    self.is_muted = last[i1:i2] == 'off'

    i1 = last.index('[') + 1
    i2 = last.index('%')
    # In between these two will be the percentage value.
    pct = last[i1:i2]

    self.volume = int(pct)

  # Ensures the volume value is between our minimum and maximum.
  def _constrain(self, v):
    if v < self.MIN:
      return self.MIN
    if v > self.MAX:
      return self.MAX
    return v

  def amixer(self, cmd):
    p = subprocess.Popen("amixer {}".format(cmd), shell=True, stdout=subprocess.PIPE)
    code = p.wait()
    if code != 0:
      raise VolumeError("Unknown error")
      sys.exit(0)

    return p.stdout

if __name__ == "__main__":

  gpioA = GPIO_A
  gpioB = GPIO_B
  gpioButton = GPIO_BUTTON

  v = Volume()

  def on_press(value):
    v.toggle()
    print("Toggled mute to: {}".format(v.is_muted))
    EVENT.set()

  # This callback runs in the background thread. All it does is put turn
  # events into a queue and flag the main thread to process them. The
  # queueing ensures that we won't miss anything if the knob is turned
  # extremely quickly.
  def on_turn(delta):
    QUEUE.put(delta)
    EVENT.set()

  def consume_queue():
    while not QUEUE.empty():
      delta = QUEUE.get()
      handle_delta(delta)

  def handle_delta(delta):
    if v.is_muted:
      debug("Unmuting")
      v.toggle()
    if delta == 1:
      vol = v.up()
    else:
      vol = v.down()
    print("Set volume to: {}".format(vol))

  def on_exit(a, b):
    print("Exiting...")
    encoder.destroy()
    sys.exit(0)

  debug("Volume knob using pins {} and {}".format(gpioA, gpioB))

  if gpioButton != None:
    debug("Volume button using pin {}".format(gpioButton))

  debug("Initial volume: {}".format(v.volume))

  encoder = RotaryEncoder(GPIO_A, GPIO_B, callback=on_turn, buttonPin=GPIO_BUTTON, buttonCallback=on_press)
  signal.signal(signal.SIGINT, on_exit)

  while True:
    # This is the best way I could come up with to ensure that this script
    # runs indefinitely without wasting CPU by polling. The main thread will
    # block quietly while waiting for the event to get flagged. When the knob
    # is turned we're able to respond immediately, but when it's not being
    # turned we're not looping at all.
    # 
    # The 1200-second (20 minute) timeout is a hack; for some reason, if I
    # don't specify a timeout, I'm unable to get the SIGINT handler above to
    # work properly. But if there is a timeout set, even if it's a very long
    # timeout, then Ctrl-C works as intended. No idea why.
    EVENT.wait(1200)
    consume_queue()
EVENT.clear()
```

4. Marcad el fichero como un fichero ejecutable:

```shell
chmod +x /recalbox/scripts/volume-monitor.py
```

5. Para que el script de control del volumen arranque con vuestro sistema Recalbox, proceded como sigue:

```shell
touch ~/custom.sh && chmod u+x ~/custom.sh
```

6. Abrid el script custom.sh usando nano:

```shell
nano ~/custom.sh
```

7. Copiad y pegad lo que sigue dentro del fichero custom.sh y grabad la modificación con Ctrl+X:

```shell
python /recalbox/scripts/volume-monitor.py
```

8. Reiniciad vuestra Recalbox utilizando el comando de reinicio:

```shell
reboot
```

9. Disfrutad de vuestro nuevo control de volumen.

### Modificaciones en el script

Todos estos cambios se encuentran en la sección "Settings" del script `Volume-monitor.py`.

>Por favor, esperad 3 segundos después de que aparezca el menú de Emulación ANTES de tocar el codificador rotatorio. Girad el codificador antes puede bloquear el script y hacer que el codificador no responda.
{.is-info}

Si queréis cambiar los dos pines por defecto utilizados por el codificador, cambiad las siguientes líneas en el script. Verificad bien que utilizáis los códigos de numeración BCM.

![](/tutorials/audio/gpio-rotative-coder.png)

`GPIO_A = 26`

`GPIO_B = 19`

Si queréis cambiar el pin al que está conectado el pulsador, modificad la línea correspondiente como aquí abajo. Comprobad que utilizáis los códigos de numeración del BCM. Si no disponéis de ningún pulsador, poned este parámetro a 'None'.

`GPIO_BUTTON = 13`

Si quieres cambiar el rango (es decir, el min & max) de volumen de la Raspberry Pi, editad la línea correspondiente en el script como aquí abajo. Las cifras se expresan en porcentaje. El max por defecto es menor que 100 para evitar distorsiones. El mínimo por defecto es mayor que cero porque si vuestro sistema es como el mío, el sonido se vuelve completamente inaudible mucho antes del 0%. Si tenéis un amplificador de hardware o altavoces de calidad o lo que sea, esto puedo cambiar.

`VOLUME_MIN = 60`

`VOLUME_MAX = 96`

Si queréis que el volumen del sistema cambie más rápidamente y que sea más sensible, cambiad la línea correspondiente en el script como aquí abajo. El valor predeterminado es 1, aumentad a 2 para duplicar la velocidad de cambio de volumen.

`VOLUME_INCREMENT = 1`

### Cómo desinstalar el script

Repetid los pasos 7 y 8 anteriores, pero eliminando la línea añadida en el paso 8 o comentándola añadiendo una almohadilla. Por ejemplo, la línea quedaría como sigue:

```shell
#python /recalbox/scripts/volume-monitor.py
```

## Agradecimientos

* **Substring** por su ayuda en hacer posible esta guía y la modificación de Recalbox.  
* **A todos los demás desarrolladores de Recalbox** por hacer posible este maravilloso proyecto.  
* **savetheclocktower** por el código original del proyecto y la ayuda con la conversión a Python2.