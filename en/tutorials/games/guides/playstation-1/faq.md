---
title: FAQ
description: 
published: true
date: 2021-09-23T23:23:42.732Z
tags: ps1, faq
editor: markdown
dateCreated: 2021-08-08T14:28:21.609Z
---

## Multi-disk

First of all, we will see the different possibilities offered by Recalbox to change the disc.

### PSP mode

Put all disks in one eboot.pbp file. You can change the disk in the Retroarch menu.

#### Advantages:

* Controller shortcuts can be used to change disks.
* There is a backup file for all disks.

#### Disadvantages:

* .sbi files cannot be used in multi-disk games.
* Does not allow to switch to a patched disk (with LibCrypt protection fix).
* It takes some time to change all multi-disk sets to eboot.pbp.

### PCSX-reARMed mode

The following disk is searched in the Retroarch menu by the file system.

#### Advantages:

* Your games are in `.BIN` / `.CUE` format.

#### Disadvantages:

* It doesn't work now.
* It is ugly and slow.
* Saved files are not shared between disks.
* Controller shortcuts cannot be used to switch disks.

Therefore, games with LibCrypt and multi-disk protection are a real pain in the ass in these two modes.

### Final mode

Just create a `.M3U` file for each multi-disk game. This file will contain the names of the `.CUE` files. For example :

![M3U](/tutorials/games/guides/playstation-1/m3u1.png)

The last aspect should be as follows:

![PSX folder](/tutorials/games/guides/playstation-1/m3u2.png)

As you can see, there are `.CUE` / `.BIN` / `.SBI` per disk and one `.M3U` per multi-disk game.

#### Advantages :

* Your games are in `.BIN` / `.CUE` format.
* The HANDSET shortcuts can be used to change discs.
* The `.SBI` files can be used.
* There is a backup file for all disks.

#### Disadvantages :

* You have to modify the es_systems.cfg file (only once).
* You must hide the `.CUE` files in the Recalbox menu.

## Multi-track games

Some games have multiple tracks per disk. This means that the `.CUE` file requires multiple entries (one per track).

Example of a multi-track `.CUE`:

![Multi-track Cue](/tutorials/games/guides/playstation-1/cue1.png)

The last aspect should be the following:

![Multi-track game](/tutorials/games/guides/playstation-1/cue2.png)

## Extensions

### `.bin` extension: special case

Rom files with the `.bin` extension are also supported but require a `.cue` file to be started.

### `.cue' extension: how does it work?

A `.cue` file is, quite simply, a text file whose extension has been changed from `.txt` to `.cue` . It must have the same name as the `.bin` file it accompanies and it contains the information necessary to launch the `.bin` file.

### .m3u` extension: manage multi-CD games

Multiple CDs of the same game can be loaded simultaneously from EmulationStation into RetroArch by creating a `.m3u` playlist file (plain text `.txt` with a `.m3u` extension).

>A .m3u playlist can only be used with games that have **one `.cue` file and only one `.bin` per disk**.
>Any multi-disc game that has multiple `.bin` files for a single disc will not work (it is recommended to convert these discs to a single `.bin` file).
{.is-warning}

## Steps to manage a multi-CD game

1. Rename the `.cue` files of each disk in the game with the appropriate extension `.cd1`, `.cd2`, etc... so that EmulationStation lists only the `.m3u` files and not all the individual disks.
2. Create a new text file and name it `Final Fantasy VII (France).m3u`.

### Example with Final Fantasy VII :

We rename :

| File to rename | Renamed file |
| :---: | :---: |
| Final Fantasy VII (France) (Disc 1).cue | Final Fantasy VII (France).cd1 |
| Final Fantasy VII (France) (Disc 2).cue | Final Fantasy VII (France).cd2 |
| Final Fantasy VII (France) (Disc 3).cue | Final Fantasy VII (France).cd3 |

One creates the file `Final Fantasy (France).m3u` :

```
Final Fantasy VII (France) (Disc 1).cd1
Final Fantasy VII (France) (Disc 2).cd2
Final Fantasy VII (France) (Disc 3).cd3
```

### Example with games in `.chd` format

 | File to rename | Renamed file |
 | :---: | :---: |
 | Final Fantasy VII (France) (Disc 1).chd | Final Fantasy VII (France).cd1 |
 | Final Fantasy VII (France) (Disc 2).chd | Final Fantasy VII (France).cd2 |
 | Final Fantasy VII (France) (Disc 3).chd | Final Fantasy VII (France).cd3 |
    
The `Final Fantasy VII (France).m3u` file will be identical to the previous example.

With this configuration, EmulationStation will automatically launch CD1 each time the game is launched.

## Changing discs during the game

If you need to change the disk while the game is running, you should do the following:

1. Open the "virtual" CD tray: `HK key + Left Joystick (Up direction)`
2. Change the disc: `HK key + Left Joystick (Right or Left direction)`
3. Close the "virtual" CD tray: `HK key + Left Joystick (Up)`

This works if you have your milti-disc set in a `.M3U` file.

## Is there a menu for DuckStation options?
 
Yes, but you have to plug in a keyboard and press `F12` to access and use the menus. These menus are not usable with a joystick at this time.

## Miscellaneous

### What do I do with the `.ECM` and `.APE` files?

You have to unzip them. See this [guide](https://www.epforums.org/showthread.php?57757-ECM-And-APE-Guide) to do so.

### How do I activate the Dualshock (analogs)?

* In the game, go to the RetroArch menu by pressing `Hotkey` + `B`.
* Select `Controls` > `Port 1 Controls`.

![](/tutorials/games/guides/playstation-1/enable-analog/psxanalog1-en.png){.full-width}

* Select `analog` in the `Device Type` option (same for all the pads you want).

![](/tutorials/games/guides/playstation-1/enable-analog/psxanalog2-en.png){.full-width}

### I don't have the `.cue` file, how do I do it?

 If you only have a `.bin` file and no .`cue` file, you can generate it in several ways:

* Online: [http://nielsbuus.dk/pg/psx_cue_maker/](http://nielsbuus.dk/pg/psx_cue_maker/)
* With this freeware : [CueMaker](https://github.com/thorst/CueMaker/releases/)

You can also get it [from Redump](http://redump.org/cues/psx/).

## Glossary

* .ape: compressed file for the .wav file.
* .bin: game data and music tracks.
* .ccd/.img/.sub: Cloning of CD files. You have to transform them into .cue/.bin (with IsoBuster for example).
* .cue: File in which the tracks on the disc are defined. One per .bin data.
* .ecm: compressed file for the .bin file.
* .pbp : PSP file for PSX games.
* .ppf: patch file for games with LibCrypt protection.
* .sbi: file that contains protection information and that is needed to run protected games in emulators. They must have the same name as the .cue file.
* .wav: music track file. You must rename it to .bin.

## Useful links

* [ECM-APE Guide](https://www.epforums.org/showthread.php?57757-ECM-And-APE-Guide)
* [PSX games list](https://psxdatacenter.com/pal_list.html)
* [.sbi files](http://psxdatacenter.com/sbifiles.html)
* [Cue File Generator](http://nielsbuus.dk/pg/psx_cue_maker/)