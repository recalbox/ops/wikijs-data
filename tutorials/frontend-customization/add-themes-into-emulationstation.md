---
title: Add themes into frontend
description: 
published: true
date: 2024-07-26T07:25:26.443Z
tags: emulationstation, themes
editor: markdown
dateCreated: 2021-08-06T13:02:38.541Z
---

Want to try a new style for your frontend? You are at the right place.

Feel free to click on the links for more information and to leave a comment on the respective posts to the different authors.

Thanks again to them for their work and sharing.

---

## Add a theme to the frontend

You can download themes created by the community in the [forum](https://forum.recalbox.com/category/14/themes-interface), just make sure you use a theme that is compatible with your version of Recalbox (i.e. an up-to-date theme with an up-to-date version).

You can manually add the theme of your choice by following these steps:

* Unzip the downloaded archive
* Copy the folder containing the theme you want to use to your frontend in 2 different ways:
  * Either from the network: `\\Recalbox\share\themes`
  * From the SD card or external storage if you have one: `SHARE/themes/`

> Be careful, all the folders of your theme must be in a single folder and not in the root of the `themes` folder.
This results in a tree structure that looks like the following:
📁 recalbox
┣ 📁 share
┃ ┣ 📁 themes
┃ ┃ ┣ 📁 Hyperwall 720p
{.is-warning}

You can now change the theme in the frontend options without rebooting.

A video tutorial is available on YouTube [here](https://youtu.be/U185lY2tKTg).

## Compatibility

|  | GPICase | PI / OXu4 | PC | OGoA | OGoS | 16/9 | 4/3 | RGB Dual |
| - | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ARTFLIX | ❔ | ✅ | ✅ | ❔ | ❔ | ✅ | ❔ | ❔ |
| Authentica | ❔ | ✅ | ✅ | ❔ | ✅ | ✅ | ❔ | ❔ |
| Blue Box | ❔ | ✅ | ✅ | ❔ | ✅ | ✅ | ❔ | ❔ |
| Bresson | ❔ | ✅ | ✅ | ✅ | ✅ | ✅ | ❔ | ❔ |
| ColorLines | ❔ | ✅ | ✅ | ❔ | ✅ | ✅ | ❔ | ❔ |
| Dreambox | ⚠️ | ✅ | ✅ | ⚠️ | ⚠️ | ✅ | ⚠️ | ❔ |
| FFArts | ❔ | ✅ | ✅ | ❔ | ❔ | ✅ | ❔ | ❔ |
| FL Power Games Retro | ❔ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ❔ |
| Hyperwall | ⚠️ | ✅ | ✅ | ⚠️ | ⚠️ | ✅ | ⚠️ | ❔ |
| Metal Slide | ❔ | ✅ | ✅ | ❔ | ✅ | ✅ | ❔ | ❔ |
| Mukashi | ❌ | ✅ | ✅ | ⚠️ | ⚠️ | ✅ | ⚠️ | ❔ |
| Next Pixel | ✅ | ✅ | ✅ | ⚠️ |⚠️ |  ✅ | ✅ | ❔ |
| Recalbox-goa2-colored | ✅ | ⚠️ | ⚠️ | ✅ | ✅ | ✅ | ✅ | ❔ |
| Recalbox Next Colored | ⚠️ | ✅ | ✅ | ⚠️ | ⚠️ | ✅ | ✅ | ❔ |
| Retro Switch | ⚠️ | ✅ | ✅ | ⚠️ | ⚠️ | ✅ | ⚠️ | ❔ |
| Unifiedspin | ❌ | ✅ | ✅ | ⚠️ | ⚠️ | ✅ | ⚠️ | ❔ |
| Uniflyered | ❔ | ✅ | ✅ | ❔ | ✅ | ✅ | ❔ | ❔ |

Legend:

✅ Optimized: works perfectly, and has been optimized for the platform in question
⚠️ Compatible: works but is not optimized, some elements are not laid out as they should or the font sizes are not optimal but these problems are minor and do not prevent the theme from working properly
❌ Not compatible: does not work or the layout of the theme is not adapted at all (overlapping elements or texts, unreadable texts, distorted images...)
❔: test waiting: this takes time, don't hesitate to participate [here](https://forum.recalbox.com/topic/24510)

## Themes

### ARTFLIX NX by Fagnerpc

- Some systems are missing but it can be used
- No flag in game lists
- Available resolution: 540p - 720p
- Options: resolution choice

![8.0.1_artflix_nx_-_1.jpg](/tutorials/frontend-customization/add-themes-into-emulationstation/8.0.1_artflix_nx_-_1.jpg){.full-width}
![8.0.1_artflix_nx_-_2.jpg](/tutorials/frontend-customization/add-themes-into-emulationstation/8.0.1_artflix_nx_-_2.jpg){.full-width}

Detailed presentation by clicking [here](https://forum.recalbox.com/topic/25918)

### Authentica by Butch Games

- Some systems are missing but it can be used
- Available resolution: 720p
- Options: colors, system views, gamelists views, clips, regions

![8.0.1_authentica_1.jpg](/tutorials/frontend-customization/add-themes-into-emulationstation/8.0.1_authentica_1.jpg){.full-width}
![8.0.1_authentica_2.jpg](/tutorials/frontend-customization/add-themes-into-emulationstation/8.0.1_authentica_2.jpg){.full-width}

Detailed presentation by clicking [here](https://forum.recalbox.com/topic/25888)

### Blue Box by Butch Games

- All systems are available
- Available resolution: 720p
- Options: colors, icons, menus, system views, gamelists views, clips, regions

![8.1.1_blue_box_1.jpg](/tutorials/frontend-customization/add-themes-into-emulationstation/8.1.1_blue_box_1.jpg){.full-width}
![8.1.1_blue_box_2.jpg](/tutorials/frontend-customization/add-themes-into-emulationstation/8.1.1_blue_box_2.jpg){.full-width}

Detailed presentation by clicking [here](https://forum.recalbox.com/topic/26947)

### Bresom by lemerle

- Some systems are missing but it can be used
- No flag in game lists
- Available resolution: 540p - 720p
- Options: resolution choice

![8.1.1_bresom_1.jpg](/tutorials/frontend-customization/add-themes-into-emulationstation/8.1.1_bresom_1.jpg){.full-width}
![8.1.1_bresom_2.jpg](/tutorials/frontend-customization/add-themes-into-emulationstation/8.1.1_bresom_2.jpg){.full-width}

Detailed presentation by clicking [here](https://forum.recalbox.com/topic/29334)

### ColorLines by Butch Games

- All systems are available
- Available resolution: 720p
- Options: colors, icons, playlists, gamelists views, clips, regions

![8.0.1_colorlines_1.jpg](/tutorials/frontend-customization/add-themes-into-emulationstation/8.0.1_colorlines_1.jpg){.full-width}
![8.0.1_colorlines_2.jpg](/tutorials/frontend-customization/add-themes-into-emulationstation/8.0.1_colorlines_2.jpg){.full-width}

Detailed presentation by clicking [here](https://forum.recalbox.com/topic/25363)

### Dreambox by Butch Games

- Some systems are missing but it can be used
- No flag in game lists
- Available resolution: 720p
- Options: icons, system views, gamelist views, clips, regions

![8.0.1_dreambox_1.jpg](/tutorials/frontend-customization/add-themes-into-emulationstation/8.0.1_dreambox_1.jpg){.full-width}
![8.0.1_dreambox_2.jpg](/tutorials/frontend-customization/add-themes-into-emulationstation/8.0.1_dreambox_2.jpg){.full-width}

Detailed presentation by clicking [here](https://forum.recalbox.com/topic/23272)

### FFArts de fpowergamesretro

- Some systems are missing but it can be used
- No flag in game lists
- Options: colors, icons, menus, system views, gamelist views, regions

![8.1.1_ffart_1.jpg](/tutorials/frontend-customization/add-themes-into-emulationstation/8.1.1_ffart_1.jpg){.full-width}
![8.1.1_ffart_2.jpg](/tutorials/frontend-customization/add-themes-into-emulationstation/8.1.1_ffart_2.jpg){.full-width}

Detailed presentation by clicking [here](https://forum.recalbox.com/topic/29668)

### FLPower Games Retro by fpowergamesretro

- Some systems are missing but it can be used
- No flag in game lists
- Options : icons, menu, system views, gamelist views

![8.0.1_flpower_games_retro_1.jpg](/tutorials/frontend-customization/add-themes-into-emulationstation/8.0.1_flpower_games_retro_1.jpg){.full-width}
![8.0.1_flpower_games_retro_2.jpg](/tutorials/frontend-customization/add-themes-into-emulationstation/8.0.1_flpower_games_retro_2.jpg){.full-width}

Detailed presentation by clicking [here](https://forum.recalbox.com/topic/26256)

### Hyperwall by Butch Games updated by Aldébaran

- Some systems are missing but it can be used
- Available resolution: 720p
- Options: icons, system views, gamelist views, regions

![8.0.1_hyperwall_1.jpg](/tutorials/frontend-customization/add-themes-into-emulationstation/8.0.1_hyperwall_1.jpg){.full-width}
![8.0.1_hyperwall_2.jpg](/tutorials/frontend-customization/add-themes-into-emulationstation/8.0.1_hyperwall_2.jpg){.full-width}

Detailed presentation by clicking [here](https://forum.recalbox.com/topic/24597)

### Metal Slide by Butch Games

- All systems are available
- Available resolution: 720p
- Options : colors, icons, menus, playlist, gamelist views, clips, regions

![8.0.1_metal_slide_1.jpg](/tutorials/frontend-customization/add-themes-into-emulationstation/8.0.1_metal_slide_1.jpg){.full-width}
![8.0.1_metal_slide_20.jpg](/tutorials/frontend-customization/add-themes-into-emulationstation/8.0.1_metal_slide_20.jpg){.full-width}

Présentation détaillée en cliquant [ici](https://forum.recalbox.com/topic/26314)

### Mukashi by mrseguin and airdream

- Some systems are missing but it can be used
- No flag in game lists
- Available resolution : 720p - 1080p (beware very heavy at the moment when loading)
- Options: logos, system views

![8.0.1_mukashi_1.jpg](/tutorials/frontend-customization/add-themes-into-emulationstation/8.0.1_mukashi_1.jpg){.full-width}
![8.0.1_mukashi_2.jpg](/tutorials/frontend-customization/add-themes-into-emulationstation/8.0.1_mukashi_2.jpg){.full-width}

Detailed presentation by clicking [here](https://forum.recalbox.com/topic/14323)

### Next Pixel by mYSt

- Some systems are missing but it can be used
- No flag in game lists
- Available resolutions: 480p - 720p - 1080p et aussi pour le 4/3 en 240p et 768p
- For OGoA / OGoS take 16/9 480p
- For the Pi and some PCs do not exceed 720p
- Options: colors, icons, system views, gamelist views, regions
- The theme also uses mix/scrap specifically designed for Next Pixel (see theme documentation)

![8.0.1_next_pixel_1.jpg](/tutorials/frontend-customization/add-themes-into-emulationstation/8.0.1_next_pixel_1.jpg){.full-width}
![8.0.1_next_pixel_2.jpg](/tutorials/frontend-customization/add-themes-into-emulationstation/8.0.1_next_pixel_2.jpg){.full-width}

Detailed presentation by clicking [here](https://forum.recalbox.com/topic/16064)

### Recalbox-goa2-colored by Aldébaran

- This is the official theme for handheld boards with 8 new background colors (pink, orange, yellow, blue, green, purple, red et grey)

![8.1.1_recalbox-goa2-colored_1.jpg](/tutorials/frontend-customization/add-themes-into-emulationstation/8.1.1_recalbox-goa2-colored_1.jpg){.full-width}
![8.1.1_recalbox-goa2-colored_2.jpg](/tutorials/frontend-customization/add-themes-into-emulationstation/8.1.1_recalbox-goa2-colored_2.jpg){.full-width}

Detailed presentation by clicking [here](https://forum.recalbox.com/topic/26228)

### Recalbox-next-colored by Aldébaran

- This is the official theme with 8 new background colors (pink, orange, yellow, blue, green, purple, red et grey)

![8.0.1_recalbox-next-colored_1.jpg](/tutorials/frontend-customization/add-themes-into-emulationstation/8.0.1_recalbox-next-colored_1.jpg){.full-width}
![8.0.1_recalbox-next-colored_2.jpg](/tutorials/frontend-customization/add-themes-into-emulationstation/8.0.1_recalbox-next-colored_2.jpg){.full-width}

Detailed presentation by clicking [here](https://forum.recalbox.com/topic/26136)

### Retro Switch by Dwayne Hurst adapted by Butch Games

- Some systems are missing but it can be used
- No flag in game lists
- Available resolutions: 720p
- Options: double theme (DarkSwitch et LightSwitch), icons, regions

![8.0.1_retro_switch_1.jpg](/tutorials/frontend-customization/add-themes-into-emulationstation/8.0.1_retro_switch_1.jpg){.full-width}
![8.0.1_retro_switch_2.jpg](/tutorials/frontend-customization/add-themes-into-emulationstation/8.0.1_retro_switch_2.jpg){.full-width}

Detailed presentation by clicking [here](https://forum.recalbox.com/topic/24439)

### Unifiedspin by Butch Games inspired by Unified themes

- All systems are available, virtual or not, on 9.1
- No flag in game lists
- Available resolution: 720p
- Options: icons, system views, gamelist views, clips, regions

![8.0.1_unifiedspin_10.jpg](/tutorials/frontend-customization/add-themes-into-emulationstation/8.0.1_unifiedspin_10.jpg){.full-width}
![8.0.1_unifiedspin_2.jpg](/tutorials/frontend-customization/add-themes-into-emulationstation/8.0.1_unifiedspin_2.jpg){.full-width}

Detailed presentation by clicking [here](https://forum.recalbox.com/topic/24433)

### Uniflyered by Dwayne Hurst adapted by Butch Games

- All systems are available
- Available resolution: 480p
- Options: icons, playlist, regions

![8.0.1_uniflyered_1.jpg](/tutorials/frontend-customization/add-themes-into-emulationstation/8.0.1_uniflyered_1.jpg){.full-width}
![8.0.1_uniflyered_2.jpg](/tutorials/frontend-customization/add-themes-into-emulationstation/8.0.1_uniflyered_2.jpg){.full-width}

Detailed presentation by clicking [here](https://forum.recalbox.com/topic/22527)