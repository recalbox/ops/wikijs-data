---
title: GameCube
description: Nintendo GameCube
published: true
date: 2024-07-29T13:54:42.087Z
tags: nintendo, gamecube, gc, consolas
editor: markdown
dateCreated: 2023-09-03T15:58:19.595Z
---

![](/emulators/consoles/gamecube.svg){.align-center}

## Ficha técnica

* **Fabricante** : Nintendo
* **Año de lanzamiento** : 2001
* **Cantidades vendidas** : 21.74 millones
* **Mejor juego vendido** : Super Smash Bros. Melee
* **Procesador** : 128-bit IBM Gekko PowerPC @ 485 MHz
* **RAM** : 43 MB total non-unified RAM
* **Procesador de video** : 162 MHz "Flipper" LSI
* **Resolución** : 640×480 480i or 480p - 60 Hz
* **Chip de sonido** : 16-bit Dolby Pro Logic II
* **Media** : 1.5 GB miniDVD

## Presentación

La (o el) **Nintendo GameCube** es una consola de videojuegos de salón del fabricante japonés Nintendo, lanzada en 2001 (2002 en Europa), desarrollada en asociación con IBM, NEC y ATI. Fue la penúltima de las consolas de videojuegos de sexta generación.

## Emuladores

[Dolphin](dolphin)
