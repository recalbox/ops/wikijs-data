---
title: Game Boy Color
description: Nintendo Game Boy Color
published: true
date: 2024-07-26T08:32:53.937Z
tags: color, gameboy, gbc, portátiles
editor: markdown
dateCreated: 2023-09-04T08:09:42.169Z
---

![](/emulators/handheld/gameboycolor.svg){.align-center}

## Detalles técnicos

* **Fabricante**: Nintendo
* **Año de lanzamiento**: 1998
* **Cantidad vendida**: 118,69 millones
* **Juego más vendido**: Pokemon Oro y Plata
* **Procesador**: Zilog Z80 de 8 bits a 8,33 MHz
* **RAM**: 32kB
* **VRAM**: 26kB
* **Vídeo**: PPU (integrado en el procesador)
* **Resolución**: 160 x 144 píxeles, 56 colores (de una paleta de 32768 colores)
* **Sonido**: 4 canales estéreo
* **Tamaño del cartucho**: 256kB - 8MB

## Presentación

La **Game Boy Color** es la videoconsola portátil que sucede a la Game Boy. Creada por Nintendo, cuenta con una pantalla en color sólo ligeramente más grande que la de la Game Boy. Sin embargo, su procesador es el doble de rápido y su memoria el doble de grande. Es retrocompatible con todos los juegos de la primera generación de Game Boy.

La Game Boy Color está disponible en varios colores: translúcido, violeta, azul, verde, fucsia, amarillo y violeta translúcido. También se produjeron ediciones limitadas: negro translúcido, verde translúcido, azul noche, azul translúcido, naranja claro, verde y dorado y otra docena de variantes específicas, así como 7 versiones "Pokémon".

Salió a la venta a finales de 1998 en Japón, Estados Unidos, Europa y Australia.

Desde el lanzamiento de la Game Boy, Nintendo se había concentrado en los juegos y no había lanzado una verdadera nueva generación (los modelos Pocket y Light eran meras evoluciones de la Game Boy). Se previó una segunda generación en 1998, coincidiendo con el décimo aniversario del primer modelo. Pero en realidad, esta consola se creó principalmente para evitar que **Bandai** se hiciera con el monopolio de los juegos portátiles con su WonderSwan, porque mientras tanto, Nintendo Research & Development 1 (R&D1) ya estaba trabajando en la Game Boy Advance. La principal expectativa de los jugadores de la época era disponer de una pantalla en color, como ocurría en las máquinas de la competencia desde hacía mucho tiempo (Game Gear, Atari Lynx, etc.).

## Emuladores

[Libretro Gambatte](libretro-gambatte)
[Libretro Mesen_S](libretro-mesen_s)
[Libretro SameBoy](libretro-sameboy)
[Libretro TGBDual](libretro-tgbdual)
[Libretro bsnes](libretro-bsnes)
[Libretro mGBA](libretro-mgba)

