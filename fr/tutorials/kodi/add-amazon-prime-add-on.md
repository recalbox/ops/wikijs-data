---
title: Ajouter l'extension Amazon Prime
description: 
published: true
date: 2024-09-19T09:02:50.930Z
tags: kodi, extensions, amazon
editor: markdown
dateCreated: 2022-12-27T21:26:42.051Z
---

## Introduction

Il existe pour Kodi une extension permettant de lire les vidéos Amazon Prime. Ce tutoriel décrit comment l'installer.

>L'extension n'est pas compatible actuellement avec le Raspberry Pi 4 et 5 car le module de vidéo WideVine pour systèmes ARM 64 bits n'existe pas encore.
>{.is-info}

## Pré-requis

Vous devez vérifier plusieurs points :

1. Ne pas avoir de double identification sur Amazon. La plupart du temps, l'identification via Kodi ne fonctionne pas à cause de cette demande supplémentaire.

2. Activer l'installation depuis un fichier zip dans Kodi :
    * Allez dans `Paramètres` > `Système` > `Extensions`.
    * Activez l'option `Sources inconnues`. Validez lors de la demande de confirmation.

## Installation

1. Lancez Kodi.

2. Vous devez installer en premier un fichier zip contenant les informations du dépôt où se trouve l'extension Amazon Prime. Allez à l'adresse suivante et téléchargez le dernier `Plugin Repository` :

https://github.com/Sandmann79/xbmc/releases

3. Transférez le fichier zip téléchargé dans votre Recalbox à l'emplacement désiré dans le share.

4. Dans Kodi, allez dans `Extensions` > `Installer d'un fichier Zip` et sélectionnez le fichier zip que vous avez téléchargé. Confirmez les messages de confirmation si vous en avez.



5. Une fois installé, toujours dans `Extensions`, allez dans `Installer d'un dépôt`.



6. Trouvez l'extension dans `Sandmann79s Repository` > `Extensions Vidéos` > `Amazon VOD`. Ouvrez l'extension pour l'installer. Confirmez les dépendances nécessaires à son installation.



7. Une fois installé, l'extension doit être visible dans la liste des extensions installées.



## Utilisation

Vous allez devoir vous identifier avant de pouvoir utiliser l'extension.

1. Ouvrez l'extension.

2. Vous allez automatiquement avoir la fenêtre des options ouverte. À gauche, mettez-vous sur `Connection` et sélectionnez `Sign In...`.



3. Vous allez devoir renseigner sur 2 fenêtres à la suite votre adresse email et votre mot de passe.

>Vous allez sûrement avoir des fenêtres où vous devrez retaper votre mot de passe ainsi qu'un captcha. C'est tout à fait normal, Amazon fait ses vérifications, même si celles-ci peuvent être trop répétitives.
>{.is-info}

4. Une fois que votre identification sera effectuée, vous aurez une fenêtre vous l'indiquant.



5. Maintenant, vous pouvez profiter d'Amazon Prime.