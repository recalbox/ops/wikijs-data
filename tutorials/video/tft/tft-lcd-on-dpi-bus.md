---
title: Configurez votre petit écran TFT sur le bus DPI
description: 
published: true
date: 2021-09-04T22:24:39.967Z
tags: tft, dpi, bus, écran, petit
editor: markdown
dateCreated: 2021-05-21T08:44:23.314Z
---

## Configurez votre petit écran TFT sur DPI

### DPI (INTERFACE D'AFFICHAGE PARALLÈLE)

Source : [https://www.raspberrypi.org/documentation/hardware/raspberrypi/dpi/README.md](https://www.raspberrypi.org/documentation/hardware/raspberrypi/dpi/README.md)

Une interface RGB parallèle jusqu'à 24 bits est disponible sur toutes les cartes Raspberry Pi avec l'en-tête à 40 voies (A+, B+, Pi2, Pi3, Zero) et le module de calcul. Cette interface permet de connecter des écrans RGB parallèles au GPIO Raspberry Pi soit en RGB24 (8 bits pour le rouge, le vert et le bleu) ou RGB666 (6 bits par couleur) ou RGB565 (5 bits rouge, 6 vert et 5 bleu).

Cette interface est contrôlée par le micrologiciel GPU et peut être programmée par un utilisateur via des paramètres config.txt spéciaux et en activant la bonne superposition de l'arbre de périphériques Linux.

En envoyant les données via cette interface parallèle, vous n'avez aucun problème avec la bande passante du bus et la vitesse d'affichage (FPS). Mais il utilise presque toutes les broches GPIO.

Ce mode est très utile, vous pouvez régler très finement vos paramètres d'affichage : timings, résolutions, etc.

Ce mode s'accompagne de nouveaux overlays, qui permettent de produire un signal RGB grâce au VGA666 (obtenez l'adaptateur VGA passif 666 pour Raspberry-Pi B+ : [code](https://github.com/fenlogic/vga666) et [matériel](https://www.banggood.com/VGA-666-Adapter-Board-For-Raspberry-Pi-3-Model-B-2B-B-A-p-1071309.html)).

```ini
dtoverlay=vga666
```

Vous pourrez utiliser l'écran TFT [5"](https://www.adafruit.com/product/1596) ou [7"](https://www.adafruit.com/product/2354) d'Adafruit grâce au [panneau TFT Kippah DPI d'Adafruit pour Raspberry Pi avec support tactile](https://www.adafruit.com/product/2453).

```ini
dtoverlay=dpi24
```

#### Mise en place

* Vous devez ouvrir le fichier [recalbox-user-config.txt](./../../../tutorials/system/modification/configtxt-configuration) avec nano ou vim et ajoutez les lignes suivantes :

```ini
dtoverlay=dpi24
```

### Écran TFT Geekwrom HD 3,5 pouces 800x480

Cet écran est de petite taille et offre une résolution de 800x480. Ses spécifications sont assez bonnes :

![](/tutorials/video/tft/geekwrom3-5.png)

Cet écran étonnant peut se trouver sur [ce site chinois](https://www.banggood.com/Geekwrom-HD-3_5-Inch-TFT-Display-Shield-800x480-For-Raspberry-Pi-3B-2B-With-2-Keys-And-Remote-IR-p-1069730.html) pour un prix inférieur à 50€.

Il peut être utilisé tel quel, sans le programme fbcp. Fbcp n'est plus nécessaire car le mode DPI utilise directement le GPU.

Pour obtenir le support de cet écran, il suffit d'ajouter les lignes suivantes à votre fichier [recalbox-user-config.txt](./../../../tutorials/system/modification/configtxt-configuration)

#### Mise en place

* Vous devez ouvrir le fichier [recalbox-user-config.txt](./../../../tutorials/system/modification/configtxt-configuration) avec nano ou vim et ajoutez les lignes suivantes :

```ini
# 3.5 HD tft screen 800x480
dtoverlay=dpi24
overscan_left=0
overscan_right=0
overscan_top=0
overscan_bottom=0

# Banggood
framebuffer_width=800
framebuffer_height=480
dtparam=spi=off
dtparam=i2c_arm=off

enable_dpi_lcd=1
display_default_lcd=1
dpi_output_format=0x6f015
dpi_group=2
dpi_mode=87
hdmi_timings=480 0 16 16 24 800 0 4 2 2 0 0 0 60 0 32000000 6
display_rotate=3
```

* Sauvegardez le fichier et redémarrez votre Recalbox. Vous pouvez maintenant visionner les incroyables images affichées sur cet écran.

![Ecran TFT Geekwrom HD 3,5 pouces 800x480](/tutorials/video/tft/geekwrom-result.jpg)

### Écran TFT 5" et 7" d'Adafruit

Pour utiliser ces écrans de [5"](https://www.adafruit.com/product/1596) et [7"](https://www.adafruit.com/product/2353), vous aurez besoin du [panneau TFT Kippah DPI d'Adafruit pour Raspberry Pi avec support tactile](https://www.adafruit.com/product/2453) pour les faire fonctionner.

#### Mise en place

* Vous devez ouvrir le fichier [recalbox-user-config.txt](./../../../tutorials/system/modification/configtxt-configuration) avec nano ou vim et ajoutez les lignes suivantes :

```ini
#3.5 HD tft screen 800x480
dtoverlay=dpi24
overscan_left=0
overscan_right=0
overscan_top=0
overscan_bottom=0

#Adafruit
framebuffer_width=800
framebuffer_height=480
dtparam=spi=off
dtparam=i2c_arm=off

enable_dpi_lcd=1
display_default_lcd=1
dpi_group=2
dpi_mode=87
dpi_output_format=0x6f005
hdmi_timings=800 0 40 48 88 480 0 13 3 32 0 0 0 60 0 32000000 6
display_rotate=3
```

### Ecran 4.0" HyperPixel

Merci glook pour ce tuto : [https://forum.recalbox.com/topic/17854/ecran-hyperpixel-4-pouces/4](https://forum.recalbox.com/topic/17854/ecran-hyperpixel-4-pouces/4)

Cet écran est disponible [ici](https://shop.pimoroni.com/products/hyperpixel-4?variant=12569539706963) avec ou sans écran tactile.

Commencez par lire le Github [ici](https://github.com/pimoroni/hyperpixel4) (anglais). Vous aurez besoin de suivre ces étapes :

#### Mise en place

En fonction de votre board, le code source à utiliser sera différent. Consultez la partie [installation manuelle](https://github.com/pimoroni/hyperpixel4#manual-installation) pour savoir quelle code source vous devez utiliser.

Une fois le bon code source choisi, voici les étapes à suivre :

* Téléchargez le code source en cliquand sur le gros bouton vert `Code` et choisissez de télécharger au format `.zip`.
* Décompressez l'archive et placez le répertoire extrait dans `/recalbox/share/system/`.
* Connectez-vous via [SSH](./../../../tutorials/system/access/root-access-terminal-cli) et allez dans le répertoire que vous venez de mettre dans votre Recalbox.
* Montez la [partition de démarrage](./../../../tutorials/system/access/remount-partition-with-write-access) en lecture/écriture.
* Compilez hyperpixel4.dts en hyperpixel4.dtbo :

```shell
dtc -@ -I dts –O dtb –o dist/hyperpixel4.dtbo src/hyperpixel4.dts
```

* Copiez le fichier `hyperpixel4.dtbo` dans `/boot/overlays/`.

```shell
cd dist
cp hyperpixel4.dtbo /boot/overlays/
```

* Modifiez le fichier [recalbox-user-config.txt](./../../../tutorials/system/modification/configtxt-configuration) avec nano ou vim et ajoutez les lignes suivantes :

```ini
# Paramètres de l'écran LCD HyperPixel
dtoverlay=hyperpixel4
overscan_left=0
overscan_right=0
overscan_top=0
overscan_bottom=0
framebuffer_width=800
framebuffer_height=480
enable_dpi_lcd=1
display_default_lcd=1
dpi_group=2
dpi_mode=87
dpi_output_format=0x7f216
display_rotate=3
hdmi_timings=480 0 10 16 59 800 0 15 113 15 0 0 0 60 0 32000000 6
```

* Assurez-vous que l'i2c et le spi sont désactivés :

```ini
dtparam=i2c_arm=off
dtparam=spi=off
```

* Copiez `hyperpixel4-init` situé dans le répertoire `/dist` vers `/usr/bin/` sur Recalbox.

```shell
cp -R /dist/hyperpixel4-init /usr/bin/
```

* Assurez-vous d'exécuter ce programme au démarrage dans Recalbox, en ajoutant `hyperpixel4-init` en ligne de commande dans l'un des scripts d'init du fichier `/etc/init.d`.