---
title: Acceso root a través del terminal
description: Cómo acceder a Recalbox a través de SSH
published: true
date: 2024-09-18T15:21:12.618Z
tags: root, terminal, ssh, acceso
editor: markdown
dateCreated: 2023-09-01T14:53:09.450Z
---

Para obtener un terminal con acceso de root en Recalbox, existen dos opciones:

>Nunca expongáis vuestra Recalbox a internet. En particular el puerto 22. Todo el mundo conoce la contraseña de root por defecto y os podrían hackear.
{.is-danger}

## SSH

Conectándose vía ssh, con las siguientes informaciones de identificación predefinidas:

* Dirección IP de vuestra Recalbox : `192.168.x.x`
* Usuario : `root`
* Contraseña : `recalboxroot`

## {.tabset}
### Windows

Podéis utilizar estos programas:

* [Putty](https://www.chiark.greenend.org.uk/~sgtatham/putty/latest.html)
* [MobaXterm](https://mobaxterm.mobatek.net/)
* PowerShell

### macOS

El sistema Mac proporciona un terminal por defecto. Abrid la carpeta `Aplicaciones` y dirigíros hacia la carpeta `Utilidades` para lanzarlo.

La línea de comandos que hay que ejecutar es: `ssh root@adresse-ip-de-votre-Recalbox`

### Linux

Linux proporciona por defecto un terminal o una consola.

Línea de comandos que hay que ejecutar: `ssh root@adresse-ip-de-votre-Recalbox`

## Acceso directo

Desde la interfaz con un teclado:

* Pulsad `F4` para salir de EmulationStation.
* Pulsad `Alt + F2` para obtener un terminal.
* Utilizad los mismos identificadores que anteriormente.

>Si no conocéis la dirección IP de vuestra Recalbox, también podéis utilizar el nombre `recalbox.local`, e.g. `ssh root@recalbox.local`
{.is-info}

## Cambiar la contraseña por defecto

Podéis cambiar la contraseña por defecto con:

```sh
mount -o remount,rw /
passwd
```

>El cambio de contraseña no sobrevivirá a las actualizaciones.
{.is-warning}
