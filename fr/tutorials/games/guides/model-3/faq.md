---
title: FAQ
description: 
published: true
date: 2024-07-25T22:45:21.923Z
tags: model3, faq
editor: markdown
dateCreated: 2024-07-25T22:45:21.923Z
---

## Erreur « Network board not present »

Pour pallier à cet erreur au lancement de certains jeux comme Daytona, voici la marche à suivre :

Lancez le jeu et appuyez sur `L3` pour accéder au menu test.

![](/tutorials/games/guides/model-3/networkerror1.png)

Déplacez-vous avec les boutons `X` ou `Y` jusqu'à vous placer sur `GAME ASSIGNMENTS` et validez avec `A` ou `B`.

![](/tutorials/games/guides/model-3/networkerror2.png)

Placez-vous sur `LINK ID` et validez avec `A` ou `B` pour passer de `MASTER` à `SINGLE`.

![](/tutorials/games/guides/model-3/networkerror3.png)

Revenez aux menus précédents jusqu'à quitter le menu test.