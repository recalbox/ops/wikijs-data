---
title: 🌐 Red
description: 
published: true
date: 2024-07-26T06:24:52.868Z
tags: tutorial, red
editor: markdown
dateCreated: 2023-09-01T09:14:16.838Z
---

Aquí se encuentran todos los tutoriales relacionados con la red internet o local, tal como el wifi, bluetooth y las carpetas compartidas.

Aquí tenéis la lista de categorías disponibles:

[Bluetooth](bluetooth)
[Compartir la red](share)
[Gestión de la dirección IP y de la configuración de red](ip)
[Wifi](wifi)

Aquí tenéis la lista de tutoriales disponibles:

[Esperad a la red antes de lanzar Kodi](wait-for-network-access-before-booting-kodi)
[Interactuar con el emulador a través de un mapping GPIO](send-commands-to-emulators-with-gpio)