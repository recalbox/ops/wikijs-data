---
title: Libretro RACE
description: 
published: true
date: 2024-06-22T23:28:42.045Z
tags: libretro, color, neo-geo, pocket, race, ngpc
editor: markdown
dateCreated: 2021-05-21T08:31:31.053Z
---

**Libretro RACE** est un émulateur de SNK Neo Geo Pocket et Neo Geo Pocket Color.

Il a été écrit par :

* Judge_
* Flavor
* Akop Karapetyan
* theelf
* frangarcj
* negativeExponent

Il a été modifié par **theelf** pour fonctionner sur la PSP.
Il est probablement dérivé de la version Akop Karapetyan PSP de RACE ([http://psp.akop.org/race](http://psp.akop.org/race)).

## ![](/emulators/license.svg) Licence

Cet émulateur est sous licence [**GPLv2**](https://github.com/libretro/RACE/blob/master/license.txt).

## ![](/emulators/compatibility.png) Compatibilité

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Fonctionnalités

| Fonctionnalité | Supporté |
| :---: | :---: |
| Redémarrage | ✔ |
| Captures d'écran | ✔ |
| Sauvegardes | ✔ |
| Sauvegardes instantanées | ✔ |
| Rembobinage | ✔ |
| Netplay | (pas d'émulation du câble link) |
| Options du core | ✔ |
| RetroAchievements | ✔ |
| Controls | ✔ |
| Remapping | ✔ |

## ![](/emulators/bios.svg) BIOS

>**Aucun bios n'est requis.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Extensions supportées

La rom doit avoir l'extension :

* .ngpc
* .zip
* .7z

Ce système supporte les roms compressées au format .zip/.7z. Attention toutefois, il ne s'agit que d'une archive.

Les fichiers contenus dans les .zip/.7z doivent correspondre aux extensions citées précédemment.
De plus, chaque fichier .zip/.7z ne doit contenir qu'une seule rom compressée.

### Emplacement

Placez les roms comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 ngpc
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.zip**

>Les roms au format **No-Intro** sont vivement conseillées.
{.is-success}

>Pour plus d'information sur les roms, rendez-vous sur [ce tutoriel](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuration avancée de l'émulateur

>Pour pouvoir conserver vos configurations personnalisées lors d'une mise à jour, nous vous conseillons d'utiliser notre fonctionnalité [Surcharges de configuration](./../../../advanced-usage/configuration-override).
{.is-info}

### Accéder aux options

Vous pouvez configurer diverses options de deux façons différentes.

* Via le menu RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via le fichier `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Options du core

| Options | Valeur des options | Variable (pour surcharge) | Valeurs des variables |
| --- | --- | --- | --- |
| Language | `Japanese` / `English` ✅ | `race_language` | `japanese` / `english` |
| Dark Filter Level (percent) | `0` ✅ / `5` / `10` / `15` / `20` / `25` / `30` / `35` / `40` / `45` / `50` | `race_dark_filter_level` | `0` / `5` / `10` / `15` / `20` / `25` / `30` / `35` / `40` / `45` / `50` |
| Frameskip | `Désactivé` ✅ / `Auto` / `Manual` | `race_frameskip` | `disabled` / `auto` / `manual` |
| Frameskip Threshold (%) | `15` / `18` / `21` / `24` / `27` / `30` / `33` ✅ / `36` / `39` / `42` / `45` / `48` / `51` / `54` / `57` / `60` | `race_frameskip_threshold` | `15` / `18` / `21` / `24` / `27` / `30` / `33` / `36` / `39` / `42` / `45` / `48` / `51` / `54` / `57` / `60` |

## ![](/emulators/external-links.png) Liens externes

* **Code source utilisé :** [https://github.com/libretro/RACE/](https://github.com/libretro/RACE/)
* **Documentation Libretro :**[ https://docs.libretro.com/library/race/](https://docs.libretro.com/library/race/)
* **Code source officiel :** [https://github.com/TheFlav/RACE-NGPC-Emulator-PSP/](https://github.com/TheFlav/RACE-NGPC-Emulator-PSP/)
* **Documentation officielle** : [http://psp.akop.org/race/doc.htm](http://psp.akop.org/race/doc.htm)
* **Site officiel :** [http://psp.akop.org/race.htm](http://psp.akop.org/race.htm)