---
title: Contribuir para o projeto
description: Como contribuir para o projeto Recalbox
published: true
date: 2021-06-24T23:45:05.895Z
tags: contribute, help, open source
editor: markdown
dateCreated: 2021-06-24T23:45:05.895Z
---

Recalbox é um projeto *open source* (de código aberto) gratuito, e ficaríamos felizes em contar com você entre nossos colaboradores.

## Perfis procurados
Se você tem habilidades e gostaria de usá-las para melhorar o projeto, você pode nos falar sobre isso em nosso [Discord](https://discord.com/invite/NbQFbGM).

Estamos sempre em busca de perfis que possam nos ajudar:
- **Designer gráfico**: Recalbox, website, redes sociais
- **UX Designer**: ux recalbox, web, ui
- **Desenvolvedor**: bash, c, c++, python (python 2 > 3)
- **Tradutor**: tradução e revisão do Recalbox, dos sites, da Wiki
- **Usuários avançados**: relatórios detalhados de erros

## Relatórios de erros
A primeira forma de participação é relatar em detalhes os erros (*bugs*) que você encontra no Recalbox.

Os **[*Issues*](https://gitlab.com/recalbox/recalbox/-/issues)** do gitlab são bilhetes de solicitação de correções de *bugs*, ou melhorias, que permitem aos usuários, desenvolvedores e testadores discutir o tópico em um só lugar.

Quando comunicar um *bug*, não hesite em ser o mais específico possível. Descreva o contexto, acrescente capturas de tela (*screenshots*) e, se for o caso, explique como você trabalhou em torno do *bug*.

Vá para https://gitlab.com/recalbox/recalbox/-/issues !

## Desenvolvimento

As fontes do Recalbox estão disponíveis em https://gitlab.com/recalbox/recalbox

O desenvolvimento do Recalbox requer a leitura de um mínimo de documentação sobre como funcionam as bases do sistema: ***buildroot***.


Uma vez entendido como funciona, você pode começar fazendo sua própria imagem do Recalbox como indicado no [README.md](https://gitlab.com/recalbox/recalbox/-/blob/master/README.md) do projeto.

Então será necessário conhecer o básico dos comandos de **bash** e linux.

Dependendo de suas habilidades, você pode começar sobre diferentes tópicos:
- Atualização de pacotes em buildroot (bash, compilação)
- Adicionar um pacote em buildroot (bash, compilação)
- Adicionar um emulador autônomo (bash, compilação, c/c++, python)
- Adicionando características ao EmulationStation (c++)


Não estamos necessariamente procurando pessoas super técnicas, mas sim alguém motivado, autônomo e capaz de trabalhar em equipe no modelo de código aberto (open source). :D

## Traduções

### Wiki


Se você quiser participar da **tradução da Wiki,** visite a página [Contribuir para a Wiki](.../.../contribute)


### Interface

Se você quiser participar da **tradução da interface do EmulationStation**, você pode entrar na página do projeto no [poeditor.com](https://poeditor.com/join/project/hEp5Khj4Ck). São feitas atualizações regulares do arquivo do idioma de origem, que lhe traremos para traduzir para os idiomas de sua escolha.

### **Vídeos do *Youtube***

Se você quiser participar da **tradução de legendas dos vídeo tutoriais do Recalbox**, entre em contato com a equipe em nossas **[redes sociais, Discord ou fórum](./../../presentation/useful-links).**
