---
title: Libretro Mesen
description: 
published: true
date: 2021-10-08T11:31:37.574Z
tags: libretro, mesen, nes
editor: markdown
dateCreated: 2021-08-01T20:58:26.895Z
---



## ![](/emulators/license.svg) License

This core is under [**GPLv3**](https://github.com/SourMesen/Mesen/blob/master/README.md).

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ❌ | ❌ | ❌ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Features

| Feature | Supported |
| :---: | :---: |
| Restart | ✔ |
| Screenshots | ✔ |
| Saves | ✔ |
| States | ✔ |
| Rewind | ✔ |
| Netplay | ✔ |
| Core Options | ✔ |
| RetroAchievements | ✔ |
| RetroArch Cheats | ✔ |
| Controls | ✔ |
| Remapping | ✔ |
| Softpatching | ✔ |

## ![](/emulators/bios.svg) BIOS

>**No bios is required.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Supported extensions

Roms must have the extension:

* .nes
* .unf
* .unif
* .zip
* .7z

This system supports compressed roms in .zip/.7z format. However, be careful, it is only an archive.

The files contained in the .zip/.7z must correspond to the extensions mentioned above.
Moreover, each .zip/.7z file must contain only one compressed rom.

### Location

Put the roms like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 nes
┃ ┃ ┃ ┃ ┣ 🗒 **game.zip**

>The roms using **No-Intro** format are highly recommended.
{.is-success}

>For more information about roms, go to [this tutorial](./../../../tutorials/games/generalities/isos-and-roms)!
{.is-info}

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Accessing to options

You can configure various options in two different ways.

* Via RetroArch menu:

┣ 📁 RetroArch Menu
┃ ┣ 📁 Core options
┃ ┃ ┣ 🧩 Name_option

* Via the file `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core options

| Options | Options values | Variables (for configuration override) | Variables values |
| --- | --- | --- | --- |
| NTSC filter | `Disabled` ✅ / `Composite (Blargg)` / `S-Video (Blargg)` / `RGB (Blargg)` / `Monochrome (Blargg)` / `Bisqwit 2x` / `Bisqwit 4x` / `Bisqwit 8x` | `mesen_ntsc_filter` | `Disabled` / `Composite (Blargg)` / `S-Video (Blargg)` / `RGB (Blargg)` / `Monochrome (Blargg)` / `Bisqwit 2x` / `Bisqwit 4x` / `Bisqwit 8x` |
| Palette | `Default` ✅ / `Composite Direct (by FirebrandX)` / `Nes Classic` / `Nestopia (RGB)` / `Original Hardware (by FirebrandX)` / `PVM Style (by FirebrandX)` / `Sony CXA2025AS` / `Unsaturated v6 (by FirebrandX)` / `YUV v3 (by FirebrandX)` / `Wavebeam (by nakedarthur)` / `Custom` / `Raw` | `mesen_palette` | `Default` / `Composite Direct (by FirebrandX)` / `Nes Classic` / `Nestopia (RGB)` / `Original Hardware (by FirebrandX)` / `PVM Style (by FirebrandX)` / `Sony CXA2025AS` / `Unsaturated v6 (by FirebrandX)` / `YUV v3 (by FirebrandX)` / `Wavebeam (by nakedarthur)` / `Custom` / `Raw` |
| Overclock | `None` ✅ / `Low` / `Medium` / `High` / `Very High` | `mesen_overclock` | `None` / `Low` / `Medium` / `High` / `Very High` |
| Overclock Type | `Before NMI (Recommended)` ✅ / `After NMI` | `mesen_overclock_type` | `Before NMI (Recommended)` / `After NMI` |
| Region | `Auto` ✅ / `NTSC` / `PAL` / `Dendy` | `mesen_region` | `Auto` / `NTSC` / `PAL` / `Dendy` |
| Left Overscan | `None` ✅ / `4px` / `8px` / `12px` / `16px` | `mesen_overscan_left` | `None` / `4px` / `8px` / `12px` / `16px` |
| Right Overscan | `None` ✅ / `4px` / `8px` / `12px` / `16px` | `mesen_overscan_right` | `None` / `4px` / `8px` / `12px` / `16px` |
| Top Overscan | `None` ✅ / `4px` / `8px` / `12px` / `16px` | `mesen_overscan_top` | `None` / `4px` / `8px` / `12px` / `16px` |
| Bottom Overscan | `None` ✅ / `4px` / `8px` / `12px` / `16px` | `mesen_overscan_bottom` | `None` / `4px` / `8px` / `12px` / `16px` |
| Aspect Ratio | `Auto` ✅ / `No Stretching` / `NTSC` / `PAL` / `4:3` / `16:9` | `mesen_aspect_ratio` | `Auto` / `No Stretching` / `NTSC` / `PAL` / `4:3` / `16:9` |
| Controller Turbo Speed | `Fast` ✅ / `Very Fast` / `Disabled` / `Slow` / `Normal` | `mesen_controllerturbospeed` | `Fast` / `Very Fast` / `Disabled` / `Slow` / `Normal` |
| Shift A/B/X/Y clockwise | `Disabled` ✅ / `Enabled` | `mesen_shift_buttons_clockwise` | `disabled` / `enabled` |
| Enable HD Packs | `Enabled` ✅ / `Disabled` | `mesen_hd_packs` | `enabled` / `disabled` |
| Remove sprite limit | `Disabled` ✅ / `Enabled` | `mesen_no_sprite_limit` | `disabled` / `enabled` |
| Enable fake stereo effect | `Disabled` ✅ / `Enabled` | `mesen_fake_stereo` | `disabled` / `enabled` |
| Reduce popping on Triangle channel | `Enabled` ✅ / `Disabled` | `mesen_mute_triangle_ultrasonic` | `enabled` / `disabled` |
| Reduce popping on DMC channel | `Enabled` ✅ / `Disabled` | `mesen_reduce_dmc_popping` | `enabled` / `disabled` |
| Swap Square channel duty cycles | `Disabled` ✅ / `Enabled` | `mesen_swap_duty_cycle` | `disabled` / `enabled` |
| Disable Noise channel mode flag | `Disabled` ✅ / `Enabled` | `mesen_disable_noise_mode_flag` | `disabled` / `enabled` |
| Screen rotation | `None` / `90 degrees` ✅ / `180 degrees` / `270 degrees` | `mesen_screenrotation` | `None` / `90 degrees` / `180 degrees` / `270 degrees` |
| Default power-on state for RAM | `All 0s (Default)` ✅ / `All 1s` / `Random Values` | `mesen_ramstate` | `All 0s (Default)` / `All 1s` / `Random Values` |
| FDS: Automatically insert disks | `Disabled` ✅ / `Enabled` | `mesen_fdsautoselectdisk` | `disabled` / `enabled` |
| FDS: Fast forward while loading | `Disabled` ✅ / `Enabled` | `mesen_fdsfastforwardload` | `disabled` / `enabled` |
| Sound Output Sample Rate | `96000` ✅ / `192000` / `384000` / `11025` / `22050` / `44100` / `48000` | `mesen_audio_sample_rate` | `96000` / `192000` / `384000` / `11025` / `22050` / `44100` / `48000` |

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/SourMesen/Mesen/](https://github.com/SourMesen/Mesen/)
* **Libretro documentation**: [https://docs.libretro.com/library/mesen/](https://docs.libretro.com/library/mesen/)