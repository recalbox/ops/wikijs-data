---
title: EasyRPG
description: 
published: true
date: 2021-08-08T23:59:10.235Z
tags: fantasy, easyrpg
editor: markdown
dateCreated: 2021-06-29T18:24:59.025Z
---

![](/emulators/fantasy/easyrpg.svg){.align-center}

## Technical data

* **Manufacturer**: -
* **Year of release**: -

## Presentation

EasyRPG is a community project that aims to create a free, cross-platform, multilingual role-playing game (RPG) creation tool, inspired by a commercial product called RPG Maker.

The project is composed of several parts:

* [EasyRPG Player](https://easyrpg.org/player/) EasyRPG Player is a program that allows you to play games created with RPG Maker 2000 and 2003 It aims to be a free cross-platform RPG Maker 2000/2003 interpreter. The main goal is to play all games created with them as the original game interpreter (RPG_RT) does.
* [RTP Replacement](https://easyrpg.org/rtp-replacement/)
* [Tools](https://easyrpg.org/tools/)
* [EasyRPG Editor](https://easyrpg.org/editor/)

Their main goals:

* Free as in freedom: every component of the project will be released under a free license.
* Multi-platform: they want EasyRPG to reach as many platforms as possible.
* Multi-language: the more languages supported, the better.

**Contribute to this project**

This is a free and open source project (FLOSS). Anyone can help, from programmers or artists to testers and translators.

## Emulators

[Libretro EasyRPG](libretro-easyrpg)