---
title: Supermodel
description: 
published: true
date: 2024-11-10T18:12:42.596Z
tags: model-3, supermodel
editor: markdown
dateCreated: 2021-05-21T08:14:25.486Z
---

Le Supermodel émule la plateforme d'arcade Sega Model 3.  
Il utilise **OpenGL 2.1** et **SDL**, et peut fonctionner sous Windows, Linux et macOS.

Il s'agit d'une sortie publique anticipée de Supermodel. Il s'agit d'un document très préliminaire, la version alpha du logiciel.
Le développement a débuté en janvier 2011 et s'est concentré sur les aspects de rétro-ingénierie du Model 3 encore inconnu.
Par conséquent, de nombreuses caractéristiques importantes, telles que une interface utilisateur appropriée, ne sont pas encore mises en œuvre et la compatibilité des jeux est encore faible.

## ![](/emulators/license.svg) Licence

Ce core est sous licence [**GPLV3**](https://www.supermodel3.com/About.html).

## ![](/emulators/compatibility.png) Compatibilité

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ❌ | ❌ | ❌ | ❌ | ❌ | ❌ | ✅ | ❌ |

## ![](/emulators/features.png) Fonctionnalités

| Fonctionnalité | Supporté |
| :---: | :---: |
| Pause | ✔︎ |
| Sauvegardes instantanées | ✔︎ |
| Captures d'écran | ✔︎ |

## ![](/emulators/bios.svg) BIOS

>**Aucun bios n'est requis.**
{.is-success}

## ![](/emulators/roms.png) Roms

Model3 se base sur le romset de Mame indiqué [ici](../).

>Vous devez obligatoirement passer votre romset MAME dans ClrMamePro pour avoir un romset Model3 fonctionnel.
{.is-info}

>Afin de trier vos roms arcade, le fichier dat est disponible dans le dossier `/recalbox/share/arcade/supermodel/`.
{.is-success}

### Emplacement

Placez les roms comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 model3
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.zip**

## ![](/emulators/advanced-configuration.png) Configuration avancée de l'émulateur

>Pour pouvoir conserver vos configurations personnalisées lors d'une mise à jour, nous vous conseillons d'utiliser notre fonctionnalité [Surcharges de configuration](./../../../advanced-usage/configuration-override).
{.is-info}

### Le fichier de configuration de Supermodel  :

```text
#
#  ####                                                      ###           ###
# ##  ##                                                      ##            ##
# ###     ##  ##  ## ###   ####   ## ###  ##  ##   ####       ##   ####     ##
#  ###    ##  ##   ##  ## ##  ##   ### ## ####### ##  ##   #####  ##  ##    ##
#    ###  ##  ##   ##  ## ######   ##  ## ####### ##  ##  ##  ##  ######    ##
# ##  ##  ##  ##   #####  ##       ##     ## # ## ##  ##  ##  ##  ##        ##
#  ####    ### ##  ##      ####   ####    ##   ##  ####    ### ##  ####    ####
#                 ####
#
#                       A Sega Model 3 Arcade Emulator.
#                Copyright 2011 Bart Trzynadlowski, Nik Henson


# ----------------------------- CONTROLLERS -------------------------------- #
# The rate at which analog control values increase/decrease
# when controlled by a key.  Valid range is 1-100, with 1
# being the least sensitive (slowest response) on default [25]
sensitivity=25

# Specifies the saturation, the position at which the joystick
# is interpreted as being in its most extreme position,
# as a % of the total range of the axis, from 0-200.
# A value of 200 means that the range will be halved, on default [100].
saturation=100

# Specifies the dead zone as a percentage,
# 0-99, of the total range of the axis.
# Within the dead zone, the joystick is inactive on default [2].
deadzone=2

# ------------------------------- AUDIO ------------------------------------- #

# Master Volume in %
sound-volume=100

# Set Music volume in %
music-volume=100

# Swap left and right audio channels
# [1] enable
# [0] disable
flip-stereo=0

# Relative front/rear balance in %
balance=0

# Disable sound board emulation sound effects
# [1] enable
# [0] disable
no-sound=0

# Disable Digital Sound Board MPEG music
# [1] enable
# [0] disable
no-dsb=0

# Select your sound engine:
# [0] New SCSP engine based on MAME [Default]
# [1] Legacy SCSP engine
sound-engine=0

# ------------------------------- VIDEO ------------------------------------- #

# set your graphics resolution automatically with [auto]
# or set on this format [1280,1024] or [none] if needed
resolution=auto

# Disable 60 Hz frame rate lock
# [1] disable
# [0] enable
no-throttle=0

# Use 8 texture maps for decoding
# [1] enable
# [0] disable
multi-texture=0

# Enable proper quad rendering
# [1] enable
# [0] disable
quad-rendering=0

# Show Crosshairs for gun games
# [0] disable
# [1] enable for 1 player
# [2] enable for 2 players
crosshairs=1

# -------------------------------- CORE ------------------------------------- #

# Set graphics multi threading in gpu or cpu
# [0] gpu-multi-threaded > Run graphics rendering in separate thread [Default]
# [1] no-gpu-thread > Run graphics rendering in main thread
# [2] no-threads > Disable multi-threading entirely
multi-threading=0

# The PowerPC frequency in MHz.
# The default is value [50] but [70] for best performance.
ppc-frequency=70

# Enable menu services on games [L3=test, R3=service]
# [1] enable
# [0] disable
service-button=1

# Log level information in Supermodel.log
# Default value : info
# add value : error
log-level=info
```

**Emplacement du fichier** : `/recalbox/share/system/configs/model3/ConfigModel3.ini`

### Options du core

## ![](/emulators/external-links.png) Liens externes

* **Forum Officiel** : [https://www.supermodel3.com/](https://www.supermodel3.com/)
* **Source Forge Officiel** : [https://sourceforge.net/projects/model3emu/](https://sourceforge.net/projects/model3emu/)