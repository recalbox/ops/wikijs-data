---
title: Dragon32/64
description: 
published: false
date: 2021-11-09T20:30:40.994Z
tags: computers, dragon32, dragon64, dragon data, tano corp, 8.0+
editor: markdown
dateCreated: 2021-09-15T08:42:21.516Z
---

![](/emulators/computers/dragon.svg){.align-center}

## Technical data

* **Manufacturer**: Dragon Data, Ltd / Tano Corp
* **Year of release**: 1982
* **OS**: Microsoft Extended BASIC
* **CPU**: Motorola 6809E @ 0.89 MHz
* **RAM**: 32/64 KB
* **Resolution**: 256 x 192 maximum, 8 colours
* **Sound**: 1 voice, 5 octaves with the Basic / 4 voices, 7 octaves with machine code

## Presentation

The Dragon 32 and Dragon 64 are home computers that were built in the 1980s. The Dragons are very similar to the TRS-80 Color Computer, and were produced for the European market by Dragon Data, Ltd., initially in Swansea, Wales before moving to Port Talbot, Wales (until 1984) and by Eurohard S.A. in Casar de Cáceres, Spain (from 1984 to 1987), and for the US market by Tano of New Orleans, Louisiana. The model numbers reflect the primary difference between the two machines, which have 32 and 64 kilobytes (32,768 and 65,536 bytes) of RAM, respectively.

Dragon Data entered the market in August 1982 with the Dragon 32. The Dragon 64 followed a year later. The computers sold well initially and attracted the interest of independent software developers including Microdeal. A companion magazine, Dragon User, began publication shortly after the microcomputer's launch.

Despite this initial success, there were two technical impediments to the Dragon's acceptance. The graphics capabilities trailed behind other computers such as the ZX Spectrum and BBC Micro. This was significant for the games market. Additionally, as a cost-cutting measure, the hardware-supported text modes only included upper case characters. This restricted system's appeal to the educational market.

Dragon Data collapsed in June 1984. It was acquired by the Spanish company Eurohard S.A., which moved the factory from Wales to Cáceres and released the Dragon 200 (a Dragon 64 with a new case that allowed a monitor to be placed on top) and the Dragon 200-E (an enhanced Dragon 200 with both upper and lower case characters and a Spanish keyboard), but ultimately filed for bankruptcy in 1987. The remaining stock from Eurohard was purchased by a Spanish electronics hobbyist magazine and given away to those who paid for a three-year subscription, until 1992.

In the United States it was possible to purchase the Tano Dragon new in box until early 2017 from California Digital, a retailer that purchased the remaining stock

## Emulators

[XRoar](xroar)