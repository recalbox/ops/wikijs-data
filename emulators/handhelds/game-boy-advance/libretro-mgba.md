---
title: Libretro mGBA
description: 
published: true
date: 2024-07-24T23:16:00.051Z
tags: libretro, gameboy, mgba, advance, gba
editor: markdown
dateCreated: 2021-07-14T06:44:49.514Z
---

**Libretro mGBA** is an emulator for running Game Boy Advance games.  
It aims to be faster and more accurate than many existing Game Boy Advance emulators, as well as adding features that other emulators lack. It also supports Game Boy and Game Boy Color games.

## ![](/emulators/license.svg) License

This core is under [**MPLv2.0**](https://github.com/libretro/mgba/blob/master/LICENSE) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Features

| Feature | Supported |
| :---: | :---: |
| Restart | ✔ |
| Saves | ✔ |
| States | ✔ |
| Rewind | ✔ |
| Core Options | ✔ |
| RetroAchievements | ✔ |
| RetroArch Cheats | ✔ |
| Controls | ✔ |
| Remapping | ✔ |
| Rumble | ✔ |
| Softpatching | ✔ |

## ![](/emulators/bios.svg) BIOS

### List of optional bios

| Filename | Description | MD5 | Provided |
| :---: | :---: | :---: | :---: |
| gba_bios.bin | Game Boy Advance BIOS | a860e8c0b6d573d191e4ec7db1b1e4f6 | ❌ |

### Location

Put your bios like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 gba
┃ ┃ ┃ ┃ ┣ 🗒 **gba_bios.bin**

## ![](/emulators/roms.png) Roms

### Supported extensions

Roms must have the extension:

* .gba
* .zip
* .7z

This system supports compressed roms in .zip/.7z format. However, be careful, it is only an archive.

The files contained in the .zip/.7z must correspond to the extensions mentioned above.
Moreover, each .zip/.7z file must contain only one compressed rom.

### Location

Put your roms like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 gba
┃ ┃ ┃ ┃ ┣ 🗒 **game.zip**

>The roms using **No-Intro** format are highly recommended.
{.is-success}

>For more information about roms, go to [this tutorial](./../../../tutorials/games/generalities/isos-and-roms)!
{.is-info}

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Accessing to options

You can configure various options in two different ways.

* Via RetroArch menu:

┣ 📁 RetroArch Menu
┃ ┣ 📁 Core options
┃ ┃ ┣ 🧩 Name_option

* Via the file `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core options

| Options | Options values | Variables (for configuration override) | Variables values |
| --- | --- | --- | --- |
| Game Boy Model (Restart) | `Autodetect` ✅ / `Game Boy` / `Super Game Boy` / `Game Boy Color` / `Game Boy Advance` | `mgba_gb_model` | `Autodetect` / `Game Boy` / `Super Game Boy` / `Game Boy Color` / `Game Boy Advance` |
| Use BIOS File if Found (Restart) | `Enabled` ✅ / `Disabled` | `mgba_use_bios` | `ON` / `OFF` |
| Skip BIOS Intro (Restart) | `Disabled` ✅ / `Enabled` | `mgba_skip_bios` | `OFF` / `ON` |
| Default Game Boy Palette | `Grayscale` ✅ / `DMG Green` / `GB Pocket` / `GB Light` / `GBC Brown ↑` / `GBC Red ↑A` / `GBC Dark Brown ↑B` / `GBC Pale Yellow ↓` / `GBC Orange ↓A` / `GBC Yellow ↓B` / `GBC Blue ←` / `GBC Dark Blue ←A` / `GBC Gray ←B` / `GBC Green →` / `GBC Dark Green →A` / `GBC Reverse →B` / `SGB 1-A` / `SGB 1-B` / `SGB 1-C` / `SGB 1-D` / `SGB 1-E` / `SGB 1-F` / `SGB 1-G` / `SGB 1-H` / `SGB 2-A` / `SGB 2-B` / `SGB 2-C` / `SGB 2-D` / `SGB 2-E` / `SGB 2-F` / `SGB 2-G` / `SGB 2-H` / `SGB 3-A` / `SGB 3-B` / `SGB 3-C` / `SGB 3-D` / `SGB 3-E` / `SGB 3-F` / `SGB 3-G` / `SGB 3-H` / `SGB 4-A` / `SGB 4-B` / `SGB 4-C` / `SGB 4-D` / `SGB 4-E` / `SGB 4-F` / `SGB 4-G` / `SGB 4-H` | `mgba_gb_colors_preset` | `Grayscale` / `DMG Green` / `GB Pocket` / `GB Light` / `GBC Brown ↑` / `GBC Red ↑A` / `GBC Dark Brown ↑B` / `GBC Pale Yellow ↓` / `GBC Orange ↓A` / `GBC Yellow ↓B` / `GBC Blue ←` / `GBC Dark Blue ←A` / `GBC Gray ←B` / `GBC Green →` / `GBC Dark Green →A` / `GBC Reverse →B` / `SGB 1-A` / `SGB 1-B` / `SGB 1-C` / `SGB 1-D` / `SGB 1-E` / `SGB 1-F` / `SGB 1-G` / `SGB 1-H` / `SGB 2-A` / `SGB 2-B` / `SGB 2-C` / `SGB 2-D` / `SGB 2-E` / `SGB 2-F` / `SGB 2-G` / `SGB 2-H` / `SGB 3-A` / `SGB 3-B` / `SGB 3-C` / `SGB 3-D` / `SGB 3-E` / `SGB 3-F` / `SGB 3-G` / `SGB 3-H` / `SGB 4-A` / `SGB 4-B` / `SGB 4-C` / `SGB 4-D` / `SGB 4-E` / `SGB 4-F` / `SGB 4-G` / `SGB 4-H` |
| Hardware Preset Game Boy Palettes (Restart) | `Default Game Boy preset` / `Game Boy Color presets only` / `Super Game Boy presets only` / `Any available presets` | `mgba_gb_colors_preset` | `0` / `1` / `2` / `3` |
| Use Super Game Boy Borders (Restart) | `Enabled` ✅ / `Disabled` | `mgba_sgb_borders` | `ON` / `OFF` |
| Color Correction | `Disabled` ✅ / `Game Boy Advance` / `Game Boy Color` / `Auto` | `mgba_color_correction` | `OFF` / `GBA` / `GBC` / `Auto` |
| Interframe Blending | `Disabled` ✅ / `Simple` / `Smart` / `LCD Ghosting (Accurate)` / `LCD Ghosting (Fast)` | `mgba_interframe_blending` | `OFF` / `mix` / `mix_smart` / `lcd_ghosting` / `lcd_ghosting_fast` |
| Audio Filter | `Disabled` ✅ / `Enabled` | `mgba_audio_low_pass_filter` | `disabled` / `enabled` |
| Audio Filter Level | `5%` / `10%` / `15%` / `20%` / `25%` / `30%` / `35%` / `40%` / `45%` / `50%` / `55%` / `60%` ✅ / `65%` / `70%` / `75%` / `80%` / `85%` / `90%` / `95%` | `mgba_audio_low_pass_range` | `5` / `10` / `15` / `20` / `25` / `30` / `35` / `40` / `45` / `50` / `55` / `60` / `65` / `70` / `75` / `80` / `85` / `90` / `95` |
| Allow Opposing Directional Input | `Disabled` ✅ / `Enabled` | `mgba_allow_opposing_directions` | `no` / `yes` |
| Solar Sensor Level | `Use device sensor if available` / `0` ✅ / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` | `mgba_solar_sensor_level` | `sensor` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` |
| Game Boy Player Rumber (Restart) | `Disabled` ✅ / `Enabled` | `mgba_force_gbp` | `disabled` / `enabled` |
| Idle Loop Removal | `Remove Known` ✅ / `Detect and Remove` / `Don't Remove` | `mgba_idle_optimization` | `Remove Known` / `Detect and Remove` / `Don't Remove` |
| Frameskip | `Disabled` ✅ / `Auto` / `Auto (Threshold)` / `Fixed Interval` | `mgba_frameskip` | `disabled` / `auto` / `auto_threshold` / `fixed_interval` |
| Frameskip Threshold (%) | `15` / `18` / `21` / `24` / `27` / `30` / `33` ✅ / `36` / `39` / `42` / `45` / `48` / `51` / `54` / `57` / `60` | `mgba_frameskip_threshold` | `15` / `18` / `21` / `24` / `27` / `30` / `33` / `36` / `39` / `42` / `45` / `48` / `51` / `54` / `57` / `60` |
| Frameskip Interval | `0` ✅ / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` | `mgba_frameskip_interval` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` |

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/libretro/mgba/](https://github.com/libretro/mgba/)
* **Libretro documentation**: [https://docs.libretro.com/library/mgba/](https://docs.libretro.com/library/mgba/)
* **Official source code**: [https://github.com/mgba-emu/mgba](https://github.com/mgba-emu/mgba)
* **Official website**: [https://mgba.io/](https://mgba.io/)