---
title: Game Gear
description: Sega Game Gear
published: true
date: 2024-07-26T08:33:11.888Z
tags: sega, gamegear, gg, portátiles
editor: markdown
dateCreated: 2023-09-04T08:13:26.968Z
---

![](/emulators/handheld/gamegear.svg){.align-center}

## Detalles técnicos

* **Fabricante**: Sega
* **Año de lanzamiento**: 1990
* **Cantidad vendida**: 11 millones
* **Procesador**: Zilog Z80 de 8 bits a 3,58 MHz
* **RAM**: 8kB
* **VRAM**: 16kB
* **Vídeo**: Sega VDP (Texas Instruments TMS9918 modificado)
* **Resolución**: 160 x 146 píxeles, 32 colores (paleta de 4096 colores)
* **Sonido**: TI SN76489 PSG
* **Tamaño del cartucho**: 32kB - 1MB
* **Pantalla**: 8,128cm (3,2 pulgadas), LCD retroiluminada

## Presentación

La **Game Gear** es una videoconsola portátil en color, lanzada en 1990 y producida por Sega para competir con la Game Boy de Nintendo, aparecida un año antes.

En términos de hardware, la Game Gear es tan similar a su hermana mayor la Master System (de la que es la adaptación a consola portátil, con algunas diferencias) que su biblioteca de juegos está formada en gran parte por conversiones de juegos realizados para esta última, con la única diferencia notable de la menor resolución para adaptarse a la pantalla de la consola portátil.

El Master Gear Converter permite utilizar los cartuchos de Master System directamente en la Game Gear sin necesidad de emular el hardware de la consola doméstica.

La Game Gear nunca tuvo el éxito esperado, en parte debido a una serie de defectos importantes:

* escasa duración de las pilas: 6 pilas LR6 proporcionan 4 horas de uso
* frágil conector de CA
* precio casi el doble que la Game Boy, pero justificado por la pantalla en color retroiluminada. No fue hasta 1998 cuando Nintendo lanzó una consola en color, la Game Boy Color, y hasta 2003 para una consola portátil en color retroiluminada, la Game Boy Advance SP.
* los componentes de la máquina envejecieron muy mal, sobre todo los condensadores, que perdieron su capacidad, provocando una pérdida de sonido y de brillo en la pantalla.

## Emuladores

[Libretro FBNeo](libretro-fbneo)
[Libretro Gearsystem](libretro-gearsystem)
[Libretro GenesisPlusGX](libretro-genesisplusgx)