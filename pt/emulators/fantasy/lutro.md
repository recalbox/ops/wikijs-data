---
title: Lutro
description: Lua engine
published: true
date: 2021-10-19T23:18:24.036Z
tags: fantasy, lutro
editor: markdown
dateCreated: 2021-10-19T23:12:37.088Z
---

![](/emulators/fantasy/lutro.svg){.align-center}

## Ficha técnica

* **Fabricante** : -
* **Ano de lançamento** : -

## Apresentação

Lutro é uma estrutura de jogos Lua experimental para Libretro seguindo o API LÖVE.

Lutro é um renderizador e implementa apenas um subconjunto do API LÖVE. Ele visa a portabilidade através do libretro API e conta com as dependências.

## Emuladores

[Libretro Lutro](libretro-lutro)