---
title: Libretro Arduous
description: 
published: true
date: 2024-07-24T20:49:59.411Z
tags: libretro, 9.1+, arduous
editor: markdown
dateCreated: 2023-09-27T06:24:37.441Z
---

**Libretro Arduous** is a port of the **Arduous**.

## ![](/emulators/license.svg) License

This core is under [**GPLv3**](https://github.com/libretro/arduous/blob/main/COPYING) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Features

| Feature | Supported |
| :---: | :---: |

## ![](/emulators/bios.svg) BIOS

>**No bios is required.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Supported extensions

Roms must have the extension:

* .hex

### Location

Put the roms like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 arduboy
┃ ┃ ┃ ┃ ┣ 🗒  **jeu.hex**

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>**This core have no option.**
{.is-success}

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/libretro/arduous/](https://github.com/libretro/arduous/)