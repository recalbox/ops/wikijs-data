---
title: « share » partition is not visible on Windows
description: 
published: true
date: 2024-04-14T12:10:33.159Z
tags: share, partition, roms, windows, sd card, visibility
editor: markdown
dateCreated: 2022-04-24T20:52:04.097Z
---

Sometimes, for an unknown reason, Windows refuses to display one of the 2 partitions that Windows can read. There is a method to force Windows to show the partition.

## Prerequisite

You must have :

- Run at least once Recalbox to complete its installation
- Your storage support plugged into your PC
- Started Windows

## Procedure

Most of the time, it's the partition containing the games which doesn't want to show. It can be the other partition but it's less often.

- On Windows 10+ desktop, do a right click on Start button and select `Disk Management`.

![](/tutorials/system/access/share1.png){.full-width}

- A new window will be visible. On the bottom of this window, find the partition to force to show (`RECALBOX` or `SHARE`) and do a right click on it to choose `Assign a letter to the drive` option.

![](/tutorials/system/access/share2.png){.full-width}

## Windows XP and 7

For Windows older than Windows 10, there is still a way to access the share partition. You must first find the update with the term `KB955704` (Microsoft no longer provides it on its site, you'll have to find it yourself).