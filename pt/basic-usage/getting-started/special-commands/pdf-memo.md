---
title: Guia PDF
description: Baixe o Guia de comandos especiais em PDF
published: true
date: 2021-07-15T23:53:35.052Z
tags: 
editor: markdown
dateCreated: 2021-07-15T23:51:31.327Z
---

## Link para Download

[Formato PDF](https://mega.nz/file/kg9EECbQ#rbaEmbs22wUFk20mlKATh7c8e0n5-wKJVsiQ4ZjHhqI)

## Visão geral

![](/basic-usage/getting-started/special-commands/pdf1-br.jpg){.full-width}

![](/basic-usage/getting-started/special-commands/pdf2-br.jpg){.full-width}

![](/basic-usage/getting-started/special-commands/pdf3-br.jpg){.full-width}

![](/basic-usage/getting-started/special-commands/pdf4-br.jpg){.full-width}