---
title: PCSX2
description: 
published: true
date: 2024-06-23T00:04:18.887Z
tags: playstation 2, ps2, pcsx2, 8.0+
editor: markdown
dateCreated: 2021-06-28T14:39:40.876Z
---



## ![](/emulators/license.svg) Licence

Ce core est sous licence [**GPLv2**](https://github.com/PCSX2/pcsx2/blob/master/COPYING.GPLv2).

## ![](/emulators/compatibility.png) Compatibilité

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ❌ | ❌ | ❌ | ❌ | ❌ | ❌ | ✅ | ❌ |

## ![](/emulators/features.png) Fonctionnalités



## ![](/emulators/bios.svg) BIOS

### Liste des bios obligatoires

| Nom de fichier | Description | MD5 | Fourni |
| :---: | :---: | :---: | :---: |
| ps2-0230a-20080220.bin | SCPH-90001/SCPH-90010 (Version 5.0 02/20/08 A) | 21038400dc633070a78ad53090c53017 | ❌ |
| ps2-0230e-20080220.bin | SCPH-90002/SCPH-90003/SCPH-90004/SCPH-90008 (Version 5.0 02/20/08 E) | dc69f0643a3030aaa4797501b483d6c4 | ❌ |
| ps2-0230h-20080220.bin | SCPH-90005/SCPH-90006/SCPH-90007 (Version 5.0 02/20/08 J) | 30d56e79d89fbddf10938fa67fe3f34e | ❌ |
| ps2-0230j-20080220.bin | SCPH-90000 (Version 5.0 02/20/08 J) | 80ac46fa7e77b8ab4366e86948e54f83 | ❌ |

### Emplacement

Placez les BIOS comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 ps2
┃ ┃ ┃ ┃ ┣ 🗒 **ps2-0230a-20080220.bin**
┃ ┃ ┃ ┃ ┣ 🗒 **ps2-0230e-20080220.bin**
┃ ┃ ┃ ┃ ┣ 🗒 **ps2-0230h-20080220.bin**
┃ ┃ ┃ ┃ ┣ 🗒 **ps2-0230j-20080220.bin**
┃ ┃ ┃ ┃ ┣ 🗒 **ps2-0250e-20100415.bin**

## ![](/emulators/isos.svg) Isos

### Extensions supportées

Les isos doivent avoir les extensions suivantes :

* .bin
* .chd
* .cso
* .gz
* .iso
* .img
* .mdf
* .nrg

### Emplacement

Placez les isos comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 ps2
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.iso**

>Les isos au format **Redump** sont vivement conseillés.
{.is-success}

>Pour plus d'information sur les isos, rendez-vous sur [ce tutoriel](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuration avancée de l'émulateur

>Pour pouvoir conserver vos configurations personnalisées lors d'une mise à jour, nous vous conseillons d'utiliser notre fonctionnalité [Surcharges de configuration](./../../../advanced-usage/configuration-override).
{.is-info}

## ![](/emulators/external-links.png) Liens externes

* **Code source utilisé** : [https://github.com/pcsx2/pcsx2/](https://github.com/pcsx2/pcsx2/)