---
title: Web Manager
description: 
published: true
date: 2024-11-11T05:27:35.364Z
tags: web, manager
editor: markdown
dateCreated: 2021-06-29T09:09:55.117Z
---

## To connect

Simply type in your web browser its name:

## {.tabset}

### Windows

[http://recalbox/](http://recalbox/)

If that doesn't work, you can use [the IP address of your Recalbox](./../../tutorials/network/ip/discover-recalbox-ip).

### Linux and macOS

[http://recalbox.local/](http://recalbox.local/)

If this does not work, you can use [the IP address of your Recalbox](./../../tutorials/network/ip/discover-recalbox-ip).

## Interface

When you are on the main page, you can:

* Modify the options in the `recalbox.conf` file.
* Add/remove roms.
* Create a support archive.
* Monitor your board - temperature, core and storage usage.
* View the current system and, if applicable, the current game on the home page.
* Manage your captures (screenshots or videos).
* Manage your Patreon key and associated options.

## Disabling the web manager

By default, the web manager starts automatically. If you want, you can disable it.

* In EmulationStation, press the `START` button.
* Go to `ADVANCED SETTINGS`.
* Set the `RECALBOX WEB MANAGER` option to OFF.