---
title: Testear vuestro joystick con sdl2-jstest
description: 
published: true
date: 2023-09-18T11:43:36.743Z
tags: sdl2-jstest, joystick, testear
editor: markdown
dateCreated: 2023-09-18T11:43:36.743Z
---

Si vuestro mando presenta problemas, existe un comando que permite obtener varias informaciones útiles.

## Descripción

Para testear vuestro mando, hay que:

* Conectarse vía [SSH](./../../../tutorials/system/access/root-access-terminal-cli)
* Ejecutar el siguiente comando:

```shell
sdl2-jstest
```

## Listar los detalles de vuestros mandos

Comando a utilizar:

```shell
sdl2-jstest --list
```

Resultado:

```shell
Joystick Name: 'DragonRise Inc.   Generic   USB  Joystick'
Joystick Path: '/dev/input/event0'
Joystick GUID: 030000001008000001e5000010010000
Joystick Number: 0
Number of Axes: 2
Number of Buttons: 10
Number of Hats: 0
Number of Balls: 0
GameController:
not a gamepad
Axis code 0: 0
Axis code 1: 1
Button code 0: 288
Button code 1: 289
Button code 2: 290
Button code 3: 291
Button code 4: 292
Button code 5: 293
Button code 6: 294
Button code 7: 295
Button code 8: 296
Button code 9: 297​
```

## Testear vuestro mando

Comando a utilizar:

```shell
sdl2-jstest -e 0
```

Aquí, 0 indica el número de vuestro mando tal y como ha sido identificado con el comando `sdl2-jstest --list`.

Encontraréis un ejemplo de un mando N64 si vistáis [esta página](https://forum.recalbox.com/topic/9016/a-lire-manettes-n64).