---
title: Modifier les fichiers de configuration de démarrage
description: 
published: true
date: 2024-08-02T09:38:40.909Z
tags: démarrage, configuration
editor: markdown
dateCreated: 2021-05-21T08:41:28.712Z
---

>Ceci ne s'applique pas aux PC.
{.is-info}

## Explications

Ce fichier est quelque peu spécifique. Il permet de définir des paramètres de démarrage de tout le système. Il est fort utilisé pour certaines configurations.

Ces fichiers sont localisés dans le système à cette adresse :

`/boot/config.txt`
`/boot/recalbox-user-config.txt`

>Il est _**FORTEMENT NON RECOMMANDÉ**_ d'ajouter des données dans le fichier `config.txt` (sauf avis contraire) sous peine de tout perdre lors de la prochaine mise à jour. Si vous devez ajouter une configuration, modifiez le fichier `recalbox-user-config.txt` pour tout retrouver en cas de mise à jour.
{.is-danger}

## Utilisation

## {.tabset}

### config.txt

* Connectez-vous [via SSH](./../../../tutorials/system/access/root-access-terminal-cli)
* Montez la [partition de démarrage](./../../../tutorials/system/access/remount-partition-with-write-access) en lecture-écriture.
* Utilisez la commande suivante pour modifier le fichier :

```shell
nano /boot/config.txt
```

### recalbox-user-config.txt

* Connectez-vous [via SSH](./../../../tutorials/system/access/root-access-terminal-cli)
* Montez la [partition de démarrage](./../../../tutorials/system/access/remount-partition-with-write-access) en lecture-écriture.
* Utilisez la commande suivante pour modifier le fichier :

```shell
nano /boot/recalbox-user-config.txt
```

### cmdline.txt

* Connectez-vous [via SSH](./../../../tutorials/system/access/root-access-terminal-cli)
* Montez la [partition de démarrage](./../../../tutorials/system/access/remount-partition-with-write-access) en lecture-écriture.
* Utilisez la commande suivante pour modifier le fichier :

```shell
nano /boot/cmdline.txt
```