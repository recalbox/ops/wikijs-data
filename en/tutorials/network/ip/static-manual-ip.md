---
title: Manually define an IP address
description: 
published: true
date: 2021-10-11T19:34:46.289Z
tags: address, ip, manual
editor: markdown
dateCreated: 2021-08-06T08:36:33.206Z
---

With Recalbox, it is possible to change the IP address manually in the [recalbox.conf](./../../../basic-usage/getting-started/recalboxconf-file) file.

Open the `recalbox.conf` file and find these lines:

```ini
## Wifi - static IP
## if you want a static IP address, you must set all 4 values (ip, netmask, gateway and nameservers)
## if any value is missing or all lines are commented out, it will fall back to the
## default of DHCP. For nameservers, you must set at least 1 nameserver.
;wifi.ip=manual ip address (ex: 192.168.1.2)
;wifi.netmask=network mask (ex: 255.255.255.0)
;wifi.gateway=ip address of gateway (ex: 192.168.1.1)
;wifi.nameservers=ip address of domain name servers, space separated (ex: 192.168.1.1 8.8.8.8)
```

You must remove the `;` in front of the last three lines, or the configuration will not work, and fill them in with the desired information, for example:

```ini
wifi.ip=192.168.1.10
wifi.netmask=255.255.255.0
wifi.gateway=192.168.1.254
wifi.nameservers=8.8.8.8 8.8.4.4
```