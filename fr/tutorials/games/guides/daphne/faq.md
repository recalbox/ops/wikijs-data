---
title: FAQ
description: 
published: true
date: 2024-07-01T13:26:25.818Z
tags: daphne, faq
editor: markdown
dateCreated: 2021-09-15T07:35:30.089Z
---

## Où puis-je trouver les jeux ?

* Certains jeux peuvent être directement et légalement téléchargés par l'outil DaphneLoader de la [distribution Windows de Daphne](http://www.daphne-emu.com/site3/index_hi.php).
* Pour d'autres (Dragon's Lair, Dragon's Lair II, Space Ace, ...), vous devez prouver que vous possédez une licence valide du jeu (version DVD par exemple).

Une fois téléchargé, copiez les fichiers nécessaires (ROM et fichiers d'image du laserdisc) sur votre Recalbox, comme expliqué sur les 3 pages dédiées :

* [Jouer aux jeux Daphné](./play-daphne-games)
* [Jouer aux jeux Singe](./play-singe-games)
* [Jouer aux jeux Singe 2](./play-singe-2-games)

## Comment quitter le jeu ?

Pour quitter le jeu, seul le bouton `Hotkey` est suffisant.

Cependant, étant un système arcade, vous devez utiliser le bouton `Select` pour mettre des crédits. Et si le bouton `Hotkey` se trouve sur `Select`, comment ça se passe ?

Sur les manettes n'ayant pas de bouton `Hotkey` dédié, vous pouvez utiliser le bouton `R1` pour quitter le jeu et continuer à utiliser le bouton `Select` pour ajouter des crédits.

## Configuration de base

Configuration de base requise pour le fonctionnement des jeux.

### Qu'est-ce que le fichier `.commands` ?

Comme expliqué précédemment, ce fichier doit être créé dans le répertoire du jeu, avec le même nom que le fichier ROM mais avec l'extension `.commands`. Il vous permet de passer des paramètres additionnels à l'émulateur pour un jeu spécifique.

La plupart de temps, il contient l'option `-bank`, qui définit la configuration du jeu (aussi connu sous le nom DIP switch) :

* Nombre de crédits pour une partie
* Nombre de vies pour un crédit
* Niveau de difficulté
* Etc...

>Des valeurs recommandées pour les jeux les plus courants sont décrites plus loin, mais vous pouvez consulter le site [LaserDisc Game Tech Center](http://www.dragons-lair-project.com/tech/) pour obtenir les informations complètes.
>
>Veuillez aussi noter que, pour un même jeu, les DIP switch peuvent varier d'une version de ROM à une autre. D'autres paramètres sont aussi possibles. Vérifiez le [wiki de Daphne](http://www.daphne-emu.com/mediawiki/index.php/CmdLine) pour plus de détails.
{.is-info}

Exemple pour Dragon's Lair (DLE 2.x):
  
`-bank 1 00110111 -bank 0 10011000`   
  
La page [suivante](http://www.dragons-lair-project.com/tech/dips/dle20.asp) nous indique que :

* Le son est toujours présent, avec la voix du narrateur (même en dehors d'une partie)
* 1 pièce = 1 crédit
* 1 crédit = 5 vies
* Pas de test ou diagnostique
* Mode de jeu standard

### Jeux supportés et configuration recommandée

Pour chaque jeu, le tableau suivant décrit :

* Le niveau de compatibilité de l'émulateur. 
* Le fichier ROM recommandé pour une meilleure expérience. 
* Le contenu recommandé du fichier `.commands` : pour une difficulté normale, 5 vies, crédit minimum, etc...

| Nom du jeu | Compatibilité | ROM recommandée | commands |
| :--- | :--- | :--- | :---: |
| Astron Belt | ⭐️⭐️ | astron.zip | `-bank 0 00000000 -bank 1 01000000` |
| Badlands | ⭐️ | badlands.zip | `-bank 0 00000000 -bank 1 10000010` |
| Bega's Battle | ⭐️ | bega.zip | `-bank 0 00000000 -bank 1 00000001` |
| Cliff Hanger |  | cliff.zip | `-bank 0 00000000 -bank 1 01000000 -bank 2 00000000 -bank 3 01111000` |
| Cobra Command | ⭐️⭐️ | cobraconv.zip | `-bank 0 11010000 -bank 1 00010001` |
| Dragon's Lair | ⭐️ | dle21.zip | `-bank 1 00110111 -bank 0 10011000` |
| Dragon's Lair II | ⭐️⭐️ | lair2.zip |  |
| Esh's Aurunmilla | ⭐️⭐️ | esh.zip |  |
| Galaxy Ranger | ⭐️ | galaxy.zip | `-bank 0 00000000 -bank 1 00000000` |
| GP World | ⭐️ | gpworld.zip |  |
| Interstellar Laser Fantasy | ⭐️ | interstellar.zip | `-bank 0 00100001 -bank 1 00000000` |
| M.A.C.H. 3 | ⭐️ | mach3.zip | `-bank 0 01000100` |
| Road Blaster | ⭐️ | roadblaster.zip | `-bank 0 00000000 -bank 1 00000000` |
| Space Ace | ⭐️⭐️ | sae.zip | `-bank 1 01100111 -bank 0 10011000` |
| Super Don Quix-Ote | ⭐️ | sdq.zip | `-bank 0 00100001 -bank 1 00000000` |
| Thayer's Quest |  | tq.zip | `-bank 0 00010000` |
| Us Vs Them | ⭐️ | uvt.zip | `-bank 0 00000000` |

## Boutons du joystick

>Un seul joystick est supporté, seulement le joueur 1.
{.is-info}

* Utilisez le stick gauche pour les déplacements et les boutons standards pour les actions.

>Pour la plupart des jeux, seul le bouton B est utilisé (A pour les contrôleurs Xbox, X pour Playstation).
{.is-info}

## Overlays

* Comment puis-je ajouter des overlays ?

À partir de Recalbox 9.1, vous pouvez avoir des overlays pour vos jeux Daphné. Créez un répertoire nommé `bezels` dans le dossier de vos jeux Daphné et placez-y les overlays correspondants avec le même nom. Par exemple, le jeu nommé `lair.daphne` aura son overlay nommé `lair.png`.

![](/tutorials/games/guides/daphne/ace.png)

![](/tutorials/games/guides/daphne/lair.png)