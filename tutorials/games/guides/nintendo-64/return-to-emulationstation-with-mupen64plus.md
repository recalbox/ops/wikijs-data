---
title: Back to EmulationStation with mupen64plus
description: 
published: true
date: 2022-02-13T12:59:45.075Z
tags: emulationstation, mupen64plus
editor: markdown
dateCreated: 2021-08-08T13:26:15.908Z
---

## Explanations

Mupen64plus is not auto-configured by Recalbox like other emulators. So you have to do it manually.

The video mode of the N64 is defined in your [recalbox.conf file](./../../../../basic-usage/getting-started/recalboxconf-file).

By default we use the video mode: `n64.videomode=DMT 4 HDMI`. This mode displays a resolution of **640x480**. We set this resolution by default, because the Raspberry Pi 2 cannot emulate the N64 at a higher resolution with a good refresh rate. So **640x480** is the maximum resolution to use on your Recalbox.

If your screen cannot change resolution (for example with the official Pi touchscreen), you will have black borders around the image because it will not be stretched to fit the screen.

## Instructions

In this case you have to do these 2 steps:

* Set the same line in `recalbox.conf` to this:

```ini
n64.videomode=default
```

* Modify the `/recalbox/share/system/configs/mupen64/mupen64plus.cfg` file to change the output resolution to the native resolution of your screen (here **800x480**):

```ini
[Video-General]
# Use fullscreen mode if True, or windowed mode if False
Fullscreen = False
# Width of output window or fullscreen width
ScreenWidth = 800
# Height of output window or fullscreen height
ScreenHeight = 480
```

But not all screens have the same video mode compatibility. So when you try to start a game of N64 with a mupen64plus core, you may get a black screen with a return to EmulationStation. In this case, you need to determine which video modes are compatible with your own screen.
  
* Log in via [SSH](./../../../system/access/root-access-terminal-cli), then type these 2 commands separately:

`tvservice -m DMT`
`tvservice -m CEA`

* The commands will return something like this:

```shell
[root@RECALBOX ~]# tvservice -m DMT
Group DMT has 16 modes:
           mode 4: 640x480 @ 60Hz 4:3, clock:25MHz progressive 
           mode 5: 640x480 @ 72Hz 4:3, clock:31MHz progressive 
           mode 6: 640x480 @ 75Hz 4:3, clock:31MHz progressive 
           mode 8: 800x600 @ 56Hz 4:3, clock:36MHz progressive 
           mode 9: 800x600 @ 60Hz 4:3, clock:40MHz progressive 
           mode 10: 800x600 @ 72Hz 4:3, clock:50MHz progressive 
           mode 11: 800x600 @ 75Hz 4:3, clock:49MHz progressive 
           mode 16: 1024x768 @ 60Hz 4:3, clock:65MHz progressive 
           mode 17: 1024x768 @ 70Hz 4:3, clock:75MHz progressive 
           mode 18: 1024x768 @ 75Hz 4:3, clock:78MHz progressive 
           mode 21: 1152x864 @ 75Hz 4:3, clock:108MHz progressive 
           mode 32: 1280x960 @ 60Hz 4:3, clock:108MHz progressive 
           mode 35: 1280x1024 @ 60Hz 5:4, clock:108MHz progressive 
           mode 36: 1280x1024 @ 75Hz 5:4, clock:135MHz progressive 
  (prefer) mode 57: 1680x1050 @ 60Hz 16:10, clock:119MHz progressive 
           mode 58: 1680x1050 @ 60Hz 16:10, clock:146MHz progressive 
```

```shell
[root@RECALBOX ~]# tvservice -m CEA
Group CEA has 5 modes:
           mode 1: 640x480 @ 60Hz 4:3, clock:25MHz progressive 
           mode 2: 720x480 @ 60Hz 4:3, clock:27MHz progressive 
           mode 3: 720x480 @ 60Hz 16:9, clock:27MHz progressive 
  (native) mode 4: 1280x720 @ 60Hz 16:9, clock:74MHz progressive 
           mode 16: 1920x1080 @ 60Hz 16:9, clock:148MHz progressive 
```

* Now we have all the video modes that can be used on your screen. We have to find a video mode, with a resolution equal or lower than 640x480, and then define this mode in the [recalbox.conf file](./../../../../basic-usage/getting-started/recalboxconf-file).
* If I choose this video mode :

```shell
Group CEA has 5 modes:
           mode 1: 640x480 @ 60Hz 4:3, clock:25MHz progressive
```

* The [recalbox.conf file](./../../../../basic-usage/getting-started/recalboxconf-file). must be modified as follows:

  * default video mode:

```ini
n64.videomode=DMT 4 HDMI
```

  * new video mode:

```ini
n64.videomode=CEA 1 HDMI
```

## With a CRT monitor

If you are using a CRT monitor, you must change your video mode as follows:  

```ini
n64.videomode=default
```