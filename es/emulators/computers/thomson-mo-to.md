---
title: Thomson MO/TO
description: 
published: true
date: 2024-07-26T12:52:25.184Z
tags: thomson, mo5, to8, ordenadores
editor: markdown
dateCreated: 2023-09-03T12:54:27.144Z
---

![Modelo TO8](/emulators/computers/thomson.svg){.align-center}

## Datos técnicos

* **Fabricante**: Thomson
* **Año de lanzamiento**: 1986
* **Procesador**: Motorola 6809E a 1 MHz
* **RAM**: 256kB
* **ROM**: 80kB
* **Resolución**: 8 modos de visualización, paleta de 4096 colores.
  * **Baja resolución**: 160 × 200 (16, 5 ó 2 colores)
  * **Resolución media**: 320 × 200 (16, 4, 3 ó 2 colores)
  * **Alta resolución**: 640 × 200 (2 colores)
* **Soportes**: Cartucho, disquetera externa de 3½" y lector de casetes

## Presentación

La gama MO/TO de Thomson es una gama de microordenadores de 8 bits: MO5, MO6, TO7, TO7/70, TO8, TO8D, TO9, TO9+.

## Emuladores

[Libretro Theodore](libretro-theodore)