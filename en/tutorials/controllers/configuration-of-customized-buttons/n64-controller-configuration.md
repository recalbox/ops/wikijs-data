---
title: N64 controller configuration
description: 
published: true
date: 2021-08-07T13:37:53.420Z
tags: n64, configuration, controller
editor: markdown
dateCreated: 2021-08-07T13:37:53.420Z
---

To set up an N64 controller in Recalbox, you must use the `sdl2-jstest` command.

### Add your controller configuration to the system:

To configure your controller, you will need information about it using the following command:

```shell
sdl2-jstest -l
```

>You can find more information on using the `sdl2-jstest` command on [this page](./../test-configuration/test-your-joystick-with-sdl2-jstest).
{.is-info}

In this example, we get:

```shell
Joystick (Broadcom Bluetooth Wireless Joystick ) has 6 axes (X, Y, Z, Rz, Hat0X, Hat0Y)
and 12 buttons (Trigger, ThumbBtn, ThumbBtn2, TopBtn, TopBtn2, PinkieBtn, BaseBtn, BaseBtn2, BaseBtn3, BaseBtn4, BaseBtn5, BaseBtn6).
```

Once you have identified the name of the controller and each button, you need to modify the file `/usr/share/mupen64plus/InputAutoCfg.ini`. To do this, use this command:

```shell
nano /usr/share/mupen64plus/InputAutoCfg.ini
```

If this file does not exist, it will be created with the above command.

At the end of the file, add the configuration information, like this:

>This template is an example, with information about a specific controller (controller name, button number etc...). You will have to adapt it with the information of your own controller.
{.is-info}

```ini
[Broadcom Bluetooth Wireless  Joystick                        ]
plugged = True
plugin = 2
mouse = False
AnalogDeadzone = 4096,4096
AnalogPeak = 32768,32768
DPad R = hat(0 Right)
DPad L = hat(0 Left)
DPad D = hat(0 Down)
DPad U = hat(0 Up)
Start = button(9)
Z Trig = button(7)
B Button = button(2)
A Button = button(1)
C Button R = axis(3+)
C Button L = axis(3-)
C Button D = axis(2+)
C Button U = axis(2-)
R Trig = button(5)
L Trig = button(4)
Mempak switch = button(6)
Rumblepak switch = 
X Axis = axis(0-,0+)
Y Axis = axis(1-,1+)
```

Once your configuration is correct, press `Ctrl + X` to quit nano, agree to overwrite the file with `Y` and press `Enter` to quit. You can now start an N64 game, and test your configuration.

Your configuration is correct, do you like it? Great ^^ But, there is one "problem". When you update your Recalbox, all these configuration files are also updated. So they will be reset. If you don't want to do this after every update, make a backup of your `InputAutoCfg.ini` file.

You can also add your configuration to Recalbox. You should check this out, and post your own configuration as a comment. We will then add it to the system in a future update of Recalbox.

### Adding special commands

Mupen64plus does not support button combinations, like the RetroArch emulators, to run a special command. But you can assign unused buttons to a specific command, like save/load a snapshot, change save locations, etc...

To do this, first you have to identify all unused buttons in your configuration. As seen before, connect to [SSH](./../../system/access/root-access-terminal-cli) and use the command `sdl2-jstest`, then write down the code number of your unused buttons.

>You can find more information about using the `sdl2-jstest` command on [this page](./../test-configuration/test-your-joystick-with-sdl2-jstest).
{.is-info}

Now, still via SSH, modify your `mupen64plus.cfg` file with this command:

```shell
nano /recalbox/configs/mupen64/mupen64plus.cfg
```

Then go to the section called `[CoreEvents]`. The right section is this one:

```ini
# Joystick event string for stopping the emulator
Joy Mapping Stop = ""
# Joystick event string for switching between fullscreen/windowed modes
Joy Mapping Fullscreen = ""
# Joystick event string for saving the emulator state
Joy Mapping Save State = ""
# Joystick event string for loading the emulator state
Joy Mapping Load State = ""
# Joystick event string for advancing the save state slot
Joy Mapping Increment Slot = ""
# Joystick event string for taking a screenshot
Joy Mapping Screenshot = ""
# Joystick event string for pausing the emulator
Joy Mapping Pause = ""
# Joystick event string for muting/unmuting the sound
Joy Mapping Mute = ""
# Joystick event string for increasing the volume
Joy Mapping Increase Volume = ""
# Joystick event string for decreasing the volume
Joy Mapping Decrease Volume = ""
# Joystick event string for fast-forward
Joy Mapping Fast Forward = ""
# Joystick event string for pressing the game shark button
Joy Mapping Gameshark = ""
```

Keep in mind that Mupen64plus identifies `player 1` as `J0`, `player 2` as `J1` and so on. So, for example, if you want to add player 1's button number 10 to the "_save savestates_" command, you would edit your file like this:

```ini
Joy Mapping Save State = "J0B10"
```

And if you want to add player 2's button 5 to the "_load savestates_" command, you need to edit your file like this:

```ini
Joy Mapping Load State = "J1B5
```

Once your configuration is correct, do `Ctrl + X` to exit nano, agree to overwrite the file with `Y` and press `Enter` to exit.