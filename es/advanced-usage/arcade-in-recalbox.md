---
title: El Arcade en Recalbox
description: 
published: true
date: 2023-09-13T19:37:12.536Z
tags: arcade
editor: markdown
dateCreated: 2023-08-30T14:19:21.226Z
---

![](/advanced-usage/arcade/arcade-recalbox.png)

## I - Las máquinas recreativas

Los años 70 han marcado el principio de la historia de las máquinas de arcade!!
Cada máquina utiliza un material específico y único, a menudo con varios procesadores, chips de sonido y de vídeo muy especializados, y siempre en la punta tecnológica de la informática de la época.

Las primeras máquinas de arcade sólo permitían jugar al juego para el que habían sido concebidas (Pac-man, Space Invaders, Pong, etc). Cuando un juego quedaba anticuado, era complicado reutilizar el mueble.

Los sistemas de arcade en 1986 evolucionan y se basan en placa madre que soporta un cartucho con el juego o una [**PCB**](https://fr.wikipedia.org/wiki/Circuit_imprimé) (conectividad [**JAMMA**](https://fr.wikipedia.org/wiki/JAMMA)).  
Las máquinas de arcade se convierten en máquinas genéricas y el material se aproxima a las consolas vídeo de salón o a los ordenadores de alta gama.

Esto permite reducir los costes de producción, el tiempo de creación y facilita las actualizaciones de juegos y la gestión de los operadores de máquinas recreativas y de las salas de juegos.

No obstante muchas máquinas recreativas continúan a utilizar un material específico y optimizado para un juego en concreto: juegos de coches, juegos con pistolas ópticas, juegos de bailar...

## II - MAME

[**MAME**](https://fr.wikipedia.org/wiki/MAME) (Multiple Arcade Machine Emulator) es un emulador polivalente.

El objetivo del projecto MAME consiste en conservar todos los juegos de las máquinas de arcade.
Muchas máquinas recreativas no se fabrican más. El projecto MAME impide que estos juegos realizados con hardware obsoleto se pierdan en el olvido.

Para realizar esto, el projecto documenta el material de las máquinas recreativas y su funcionamiento interno.

### 1 - La documentación de MAME

La disposición pública de la documentación de MAME permite principalmente validar la exactitud de las emulaciones (¿cómo probar sino que la máquina emulada corresponde fidelmente a la original?).
Con el paso del tiempo, MAME ha absorbido (en 2015) un projecto hermano llamdo [**MESS**](https://fr.wikipedia.org/wiki/MESS) (Multi Emulator Super System). Gracias a este projecto, MAME documenta ahora también una gran variedad de ordenadores retro, de consolas de juegos, de calculadoras, además de los juegos de las máquinas recreativas que era su objetivo inicial.

La primera versión pública de MAME (0.1), creada por Nicola SALMORIA, salió a la luz el 5 de febrero de 1997.
El 21 de octubre de 2010, la versión 0.140 del emulador hace funcionar más de 4510 juegos. 
El 4 de abril de 2020, la version 0.220 toma en cuenta más de 6000 juegos de recreativas y ordenadores.

MAME está continuamente evolucionando. Para que los juegos funcionen, es necesario hacer corresponder su romset con la versión utilizada del emulador.

### 2 - El principio de documentación de MAME

MAME documenta todas estas fabulosas máquinas recreativas mediante el pricipio de [**DUMPS**](https://fr.wikipedia.org/wiki/Dump).
El dump consiste en la transferencia de los datos contenidos en los diferentes componentes hardware de la máquina (placa madre, PCB, cartuchos) hacia un formato digital que puede ser explotado pour los emuladores.

![](/advanced-usage/arcade/board-to-zip.png)

Para las máquinas recreativas más modernas, este dump se acompaña de un sorporte de datos en CD-ROM, DVD o en casette, llamado CHD (Compressed Hunks of Data ou morceaux de données compressés).  
Sin estos ficheros CHD la emulación sera imperfecta o no funcionará en absoluto.

![](/advanced-usage/arcade/rom-and-chd.png)

Desgraciadamente, este proceso no es perfecto y a menudo produce versiones de juegos llamadas ”BAD DUMPS”, donde la emulación resulta imperfecta o incluso imposible.

## III - LOS ROMSETS

### 1 - Contenido de un romset

Como podéis imaginar con la explicación de más arriba, en un romset MAME encontramos muchísimas cosas. En efecto, el hecho de haber querido documentar todas las máquinas recreativas del planeta junto con ordenadores y placas madres nos lleva a preguntarnos ¿qué tenemos en un ROMSET?¿qué necesitamos para jugar?

Gracias al programa [**CLRMAMEPRO**](./../tutorials/utilities/rom-management/clrmamepro), es posible procesar un romset MAME y ordernarlo y filtrarlo, siempre y cuando se disponga de una referencia: el fichero [**.dat**](https://fr.wikipedia.org/wiki/Index.dat) correspondiente.
Este fichero contiene la lista completa de metadatos que permite de procesar el romset para hacerlo funcionar con vuestra versión del emulador.
_**Es importante comparar vuestro romset con el fichero .dat correspondiente utilizando el programa clrmamepro para aseguraros que vuestras roms funcionan. Si buscáis los juegos uno a uno en internet os encontraréis que no funcionan por culpa de problemas de compatibilidad con vuestra version del emulador MAME**_

Dentro del projecto MAME, existen varias familias de romsets:

* **STANDAR:** es el romset más grande (55Go). Contiene juegos de recreativas, flippers, consolas, máquinas de meter dinero, calculadoras, incluso tamagotchis...
* **MECHANICAL:** consiste en máquinas electromecánicas que son muy complejas a emular porque necesitan movimientos mécanicos.
* **DEVICE:** se trata de ficheros de configuración que acompañan a las máquinas recreativas y que son necesarios para su emulación.
* **BIOS:** algunos sistemas se encuentran clasificados por BIOS (unos 70 aproximadamente).

### 2 - Las BIOS / los Drivers

Algunas ROMs de un romset pueden necesitar ficheros de BIOS o de drivers, como ocurre con los juegos de [**Neo-Geo**](https://fr.wikipedia.org/wiki/Neo-Geo_MVS). 

* Ejemplo **Neo-Geo**: si queréis jugar a los juegos Neo-Geo, debéis copiar el fichero de "BIOS"(en este caso el fichero neogeo.zip) en el mismo directorio que el juego.
* Ejemplo [**CPS**](https://fr.wikipedia.org/wiki/CP_System) : para utilizar algunos juegos CPS, debéis copiar el fichero "driver" (en este caso el fichero qsound_hle.zip) en la misma carpeta que el juego.

Por supuesto, si utilizáis subcarpetas para vuestros juegos (por género o por hardware por ejemplo), debéis copiar estos ficheros dentro de cada subcarpeta que contiene juegos de la Neo-Geo o de CPS.

Normalmente estos ficheros son pequeños, así que no cuesta nada copiarlos todos en cada uno de vuestras subcarpetas.

Ahora os preguntaréis: ¿dónde puede conseguir estos ficheros de BIOS o de drivers?
Nada más sencillo: dentro de vuestro propio romset MAME!

### 3 - Los ficheros CHD

Como por las BIOS o DRIVERS, los ficheros CHD deben acompañar la rom que lo necesita pero atención porque la forma de almacenarlos es diferente.

El fichero CHD (no comprimido) no posee el mismo nombre que la ROM pero debe almacenarse en una carpeta (no comprimida) que utilice el mismo nombre exacto de la ROM.

Los ficheros CHD a menudo no se encuentran en los romsets porque son voluminosos y se deben descargar por separado.

**Ejemplo ROM+CHD : sfiii3**

![](/advanced-usage/arcade/roms-chd.png)

### 4 - Los tipos de ficheros en un romset

* **PARENT:** la ROM del juego original que el equipo de MAME ha decidido llamar "version original". A la excepción de los ficheros ROM BIOS, los ficheros ROM para estos juegos contienen todo lo necesario para que el emulador los ejecute correctamente. El conjunto de ficheros contenidos en la ROM son considerados como la revisión más reciente, y cuando existen varias versiones regionales, se utiliza la revisión "world" (internacional) o "US" (americana) como referencia.
* **CLONES:** se trata de una variante de la ROM PARENT o de una versión diferente (por ejemplo, Street Fighter II Tubo se considera como una variante de Street Fighter II Champion Edition). Dentro de este grupo se encuentra también las versiones bootleg / homebrew / hack...
* **BIOS:** la ROM común que todos los juegos de una misma máquina recreativa utilizan (como Neo-Geo MVS). Estos ficheros se utilizan para arrancar la máquina y permiten efectuar tareas básicas de inicialización cuando se enchufa la corriente.

### 5 - Tipo de romsets: split / merged / non-merged

**Existen tres tipos de romsets MAME:**

* **Split:** [dividido] los ficheros comunes entre parents y clones se encuentra únicamente en el zip parent, así que para utilizar el clone tienes que tener en posesión las ROMS parent y clone.
* **Non-merged:** [no-fusionado] todos los ficheros necesarios para hacer funcionar un clone se encuentran en su propio zip.
* **Merged:** [fusionado] tanto el parent como el clone se encuentran dentro del mismo zip.

**Aquí abajo se presenta un ejemplo de ROM dependiendo del tipo de romset utilizado:** en rojo se muestran los ficheros parent, en amarillo los ficheros del clone y en blanco los demás ficheros necesarios para el funcionamiento del juego.

Insistimos en el hecho que en el caso de un romset **split** el directorio **clone** no puede funcionar sin el directorio **parent**.

![](/advanced-usage/arcade/rom-types.jpg){.full-width}

## IV - El Arcade y Recalbox

Los diferentes tipos de emuladores de Arcade presentes en Recalbox hacen referencia a **diferentes romsets MAME**.

_**Es importante comparar vuestro romset con el fichero .dat correspondiente utilizando el programa clrmamepro para aseguraros que vuestras roms funcionan. Si buscáis los juegos uno a uno en internet os encontraréis que no funcionan por culpa de problemas de compatibilidad con vuestra version del emulador MAME**_

Encontraréis todas las versiones de todos los romset de Recalbox en [esta página](./../emulators/arcade).

## V - GLOSARIO

**A**

* **AtomisWave :** sistema de máquinas recreativas (arcade) creado por Sammy con una arquitectura similar a la Naomi de Sega.

**B**

* **Boot :** es la fase de arranque de un sistema, como por ejemplo un ordenador o una consola.
* **Bootleg:** copias de juegos no oficiales (no permitidas por el creador o el distribuidor). Existen en formato PCB (para máquinas recreativas) y en cartuchos. Generalmente se vendían mucho más baratas que los juegos originales y se distinguen claramente por la falta del logo del distribuidor en las placas madres o en los cartuchos. Es muy común que los juegos de bootlegs tengan fallos o pequeños bugs, músicas diferentes, sonidos que faltan en relación con el juego original, freezes aleatórios, etc.

**C**

* **Placa madre:** se trata de una lámina de silicio que contiene los componentes informáticos (hardware) necesarios para hacer funcionar el juego. Por ejemplo en el sistema MVS o cualquier otro sistema que utilice cartuchos o discos, en la placa base se encuetran las unidades de cálculo, los procesadores de sonido y de gráficos etc que permiten ejecutar el programa que ejecuta el juego.
* **Chihiro :** nombre de un sistema de arcade distribuido por SEGA en 2002. La arquitectura de esta plataforma se base en la arquitectura de la console Xbox de Microsoft. Entre la lista de juegos disponibles, los más populares son: Outrun 2, House of the Dead 3 y Virtua Cop 3.
* **CPS 1 :** acrónimo de Capcom Play System 1. Nombre de un sistema de arcade creado por Capcom que vió la luz en 1988. Entre los juegos de este sistema se encuentra Street Fighters II, el más famoso !
* **CPS 2 :** acrónimo de Capcom Play System 2. Nombre de un sistema de arcade creado por Capcom que vió la luz en 1993. Entre los juegos de este sistema se encuentra : Gigawing, Marvel Vs Capcom, la serie de Street Fighters Zéro o Alpha, Vampire Savior o Progear no Arashi.
* **CPS 3 :** acrónimo de Capcom Play System 3. Nombre de un sistema de arcade creado por Capcom que vió la luz en 1996. Se trata de un sistema muy poderoso para la gestión de gráficos en 2D. En este sistema han salido los juegos Jojo’s Bizarre Adventure, Warzard, y la serie de Street Fighters III.  
* **Crystal System :** [sistema de recreativas](https://fr.wikipedia.org/wiki/Syst%C3%A8me_d%27arcade) con formato JAMMA que vió la luz en 2001, fabricado y comercializado por la empresa BrezzaSoft. El Crystal System se crea con el objetivo de reemplazar en el mercado el sistema Neo-Geo. Casi la totalidad de los empleados de BrezzaSoft vienen de SNK después de que ésta haya caido en bancarrota. Este sistema propone juegos en 2D muy avanzados y muy agradables. El Crystal System se convertirá en un desastre comercial y no alcanzará sus expectativas, con una vída útil en el mercado muy corta. Muy pocos juegos salen al mercado, pero entre ellos cabe citar Evolution Soccer, un juego de football creado a partir de Super Sidekicks, y Crystal of the Kings, un Beat them all de tipo medieval en la línea de Golden Axe o Knights of the Round.

**D**

* **DECO Cassette System :** [máquina recreativa](https://fr.wikipedia.org/wiki/Borne_d%27arcade) desarrolada por Data East. Creada en 1980, fue el primer sistema de arcade que permitía cambiar de juego. El poseedor compraba la cabina de la máquina, mientras que los juegos se distribuían en casettes audio. Bastaba con insertar la casette en la cabina junto con un dispositivo de seguridad (se trata de una de las primeras formas de gestión digital de derechos de autor, para impedir la copia no autorizada).
* **Dump / Dumping / Dumper :** Técnica que implica transferir los datos contenidos en una o varias ROMs de un juego de arcade al disco duro de una computadora (imagen de ROM) para luego ponerlos a disposición en Internet y usarlos en emuladores como MAME.

**E**

* **Emulation :** proceso que permite reproducir en una computadora o consola el comportamiento de otro dispositivo, gracias a lo que se llama un emulador. Por ejemplo, el emulador Kawaks permite jugar a la Neo Geo en su PC

**H**

* **Hyper Neo-Geo 64 :** fue creado por la compañía SNK y lanzado al mercado en septiembre de 1997, con el propósito de suceder al sistema Neo-Geo MVS que tenía en ese momento siete años de antigüedad. Técnicamente, como su nombre indica, el Hyper Neo-Geo 64 está equipado con un microprocesador de 64 bits que permite ejecutar juegos en 3D, mostrando 4096 colores simultáneamente de una paleta de 16,7 millones de colores, en una resolución de 640 x 480 píxeles. Entre los 7 juegos lanzados: Fatal Fury, Wild Ambition, Samurai Shodown 64, Samurai Shodown 64, Warriors Rage..

**I**

* **Image de ROM :** copia de las ROMs (Read Only Memory), los componentes electrónicos que se encuentran en los cartuchos y tarjetas de arcade y que contienen el juego. El acto de copiar un juego de arcade al disco duro de una computadora se llama "dumpear" un juego y requiere hardware especializado. Las copias/imagenes de las ROMs luego se ponen en Internet en forma de archivos generalmente comprimidos en formato .zip, legibles por emuladores, y luego son descargados por ustedes y por mí. Por razones de simplicidad, las imágenes de las ROMs simplemente se llaman "roms" en la mayoría de los sitios de emulación.

**J**

* **JAMMA :** estándar de conectividad para todos los juegos de arcade, con el propósito de que funcionen en todas las máquinas recreativas. Fue creado a mediados de los años 80 por la «Japanese Amusement Machine Manufacturers Association». Técnicamente, este estándar define las conexiones para: dos joysticks de ocho direcciones, dos botones de inicio, y seis botones de control (tres por jugador), además del soporte de sonido (mono).

**K**

* **Konami Sytem 573 :** el System 573 es una serie de tarjetas de sistema de arcade de Konami basadas en la PlayStation original. El hardware fue utilizado principalmente para las series de juegos de música arcade de Konami, más comúnmente la serie Dance Dance Revolution introducida en 1998.
* **Konami Viper :** Konami lanza la Konami Viper en 1998 para salas de arcade. Esta máquina recreativa utiliza el sistema JAMMA+PCB. Entre los 13 juegos lanzados: Jurrasic Park 3, GTI Club 2, Silent Scope EX, Thrill Drive II…

**L**

* **Lindbergh :** nombre de un sistema de arcade creado por SEGA lanzado en 2006. La arquitectura y los componentes de este sistema son casi idénticos a los de una PC, lo que contrasta con los otros sistemas creados por SEGA anteriormente. Entre los juegos lanzados en Lindbergh se encuentran Virtua Fighter 5, Virtua Tennis 3, House of The Dead 4, After Burner Climax... Lindbergh también es el nombre de uno de los pioneros de la aviación, el estadounidense Charles Lindbergh (1902-1974). Fue la primera persona en realizar un vuelo en solitario a través del océano Atlántico en avión.

**M**

* **MAME :** un famoso emulador de arcade que emula más de 6000 sistemas y máquinas recreativas. MAME también es considerado como un verdadero museo virtual del juego de arcade
* **Model 1 :** nombre de un sistema de arcade creado por SEGA lanzado en 1992. Una verdadera avanzada tecnológica, en este sistema se lanzarán Virtua Racing y Virtua Fighter 1.
* **Model 2 :** nombre de un sistema de arcade creado por SEGA lanzado en 1993. Algunos de los juegos más populares publicados en este sistema son: Daytona USA, House of the Dead, Sega Rally Championship y Virtua Fighter 2.
* **Model 3 :** nombre de un sistema de arcade creado por SEGA lanzado en 1996, que se convirtió en el sistema más potente de su época. Algunos de los juegos más populares publicados en este sistema son: Daytona USA 2, Scud Race, Sega Rally 2 y Virtua Fighter 3.

**N**

* **Namco System 246 :** el System 246 es un sistema de videojuegos destinado a salas de arcade, basado en la PlayStation 2. Fue creado por la compañía Namco en 2001 y experimentó varias evoluciones: el System 256, el Super System 256 y el System 147.
* **Naomi et Naomi GD :** sistema de videojuegos en cartucho y GD-ROM para máquinas recreativas, lanzado en 1998 como sucesor del sistema Sega Model 3. El acrónimo de Naomi significa "New Arcade Operation Machine Idea" (literalmente "nueva idea de máquina de arcade") y también se traduce como "belleza" en japonés.
* **Neo-geo MVS :** (Multi Video System) es un sistema de videojuegos para máquinas recreativas compatible con JAMMA, destinado a salas de arcade y creado por la empresa japonesa SNK en 1990.
* **Nintendo Playchoice 10 :** basado en el sistema Nintendo Entertainment (NES), la placa base PlayChoice 10 permitía instalar hasta 10 cartuchos ROM a la vez. A diferencia de la mayoría de los otros juegos de arcade, las monedas agregaban tiempo en lugar de créditos, lo que te permitía cambiar de juego sobre la marcha cuando lo desearas.

**P**

* **PCB :** las iniciales de «Printed Circuit Board» («Placa de circuito impreso» en francés). Una placa diseñada para agrupar componentes electrónicos. En los juegos arcade, una PCB es un circuito impreso que contiene un juego y se conecta a la máquina recreativa a través del conector JAMMA.
* **PGM :** las iniciales de «Poly Game Master», desarrollado por IGS (International Games System). Este sistema de arcade es similar al MVS en términos de rendimiento, apariencia y uso.
* **Pinball :** flipper.

**R**

* **ROM :** siglas de "**Read Only Memory"** tambien llamada « memoria muerta ».

**S**

* **Sega Hikaru :** nombre de un sistema creado por la compañía Sega en 1999. El Hikaru fue concebido originalmente para albergar el juego Brave Fire Fighters. Tiene la capacidad de mostrar en pantalla gráficos complejos que representan movimientos de fuego o agua. Fue el primer sistema de arcade en ofrecer sombreado de Phong en pantalla. Este sistema solo tuvo seis juegos y fue rápidamente abandonado debido a que su explotación resultaba demasiado costosa en comparación con la Naomi2.
* **Sega Mega Play:** sistema de arcade basado en la consola doméstica Sega Megadrive. Al insertar monedas, compras créditos como en un juego de arcade estándar
* **Sega Mega-Tech :** sistema de arcade basado en la consola doméstica Sega Megadrive. Al insertar una moneda, compras tiempo; el juego termina cuando se agota tu tiempo, y puedes agregar monedas adicionales durante el juego para obtener tiempo extra.
* **Set :** en el lenguage de máquinas de arcade, un set=juego, o BIOS compuesta de varias mémorias muertas (o ROMs).
* **ST-V Titan** (acrónimo de Sega Titan Video) : sistema arcade de Sega lanzado en 1994, con una arquitectura idéntica a la de la consola Sega Saturn (a excepción de que el ST-V utilizaba cartuchos). Uno de los objetivos era facilitar la vida de los programadores, lograr conversiones más rápidas de arcade a consola y obtener conversiones lo más fieles posible.

**T**

* **Triforce :** nombre de un sistema de arcade desarrollado en conjunto por Nintendo, SEGA y Namco, lanzado en 2002. La arquitectura de este sistema se basa en la de la consola GameCube de Nintendo. Entre los juegos publicados, los más populares son: F-Zero AX, Virtua Striker 4 y Mario Kart Arcade GP. Nota: el nombre Triforce representa la alianza entre las 3 compañías, pero también es una referencia a la serie The Legend Of Zelda.

## VI - Fuentes:

* [http://www.system16.com/](http://www.system16.com/)
* [https://fr.wikipedia.org/wiki/Wikip%C3%A9dia:Accueil_principal](https://fr.wikipedia.org/wiki/Wikip%C3%A9dia:Accueil_principal)