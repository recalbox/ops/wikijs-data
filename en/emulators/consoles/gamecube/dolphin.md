---
title: Dolphin
description: 
published: true
date: 2021-10-11T21:26:01.115Z
tags: gamecube, gc, dolphin
editor: markdown
dateCreated: 2021-07-30T21:12:10.769Z
---

**Dolphin** is an emulator for two recent Nintendo game consoles: the GameCube and the Wii. It allows PC gamers to enjoy games made for these two consoles in **full HD** (1080p) with various enhancements: compatibility with all PC controllers, turbo speed, online network play, and much more!

More than 200 people have been working hard for years to create Dolphin. The list of contributors can be found [on GitHub](https://github.com/dolphin-emu/dolphin/graphs/contributors).

## ![](/emulators/license.svg) License

This core is under [**GPLv2**](https://github.com/dolphin-emu/dolphin/blob/master/license.txt) licnse.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/PI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ❌ | ❌ | ❌ | ❌ | ❌ | ❌ | ❌ | ✅ |

## ![](/emulators/features.png) Features

| Feature | Supported |
| :---: | :---: |
| Netplay | ✔ |

## ![](/emulators/bios.svg) BIOS

Although purely optional, there are still Gamecube bios by geographical area, namely Europe (EUR), United States (USA) and Japan (JAP).

### List of optional bios

| Filename | Description | MD5 | Provided |
| :---: | :---: | :---: | :---: |
| IPL.bin | BIOS GameCube EUR | 0cdda509e2da83c85bfe423dd87346cc | ❌ |
| IPL.bin | BIOS GameCube JAP | fc924a7c879b661abc37cec4f018fdf3 | ❌ |
| IPL.bin | BIOS GameCube USA | 339848a0b7c2124cf155276c1e79cbd0 | ❌ |

### Location

Put the bios like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 gamecube
┃ ┃ ┃ ┃ ┣ 📁 EUR
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **IPL.bin**
┃ ┃ ┃ ┃ ┣ 📁 JAP
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **IPL.bin**
┃ ┃ ┃ ┃ ┣ 📁 USA
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **IPL.bin**

## ![](/emulators/isos.svg) Isos

### Supported extensions

Isos must have the extension:

* .ciso
* .gc
* .gcm
* .gcz
* .iso
* .rvz

### Location

Put the isos like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 gamecube
┃ ┃ ┃ ┃ ┣ 🗒 **game.iso**

>The isos using **Redump** format are highly recommended.
{.is-success}

>For more information about isos, go to [this tutorial](./../../../tutorials/games/generalities/isos-and-roms)!
{.is-info}

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Core options

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/dolphin-emu/dolphin/](https://github.com/dolphin-emu/dolphin/)
* **Official website**: [https://www.dolphin-emu.org/](https://www.dolphin-emu.org/)