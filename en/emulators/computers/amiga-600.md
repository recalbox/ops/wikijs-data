---
title: Amiga 600
description: 
published: true
date: 2021-08-09T13:51:31.607Z
tags: amiga-600, amiga, computers, 600, commodore
editor: markdown
dateCreated: 2021-07-28T18:51:23.641Z
---

![](/emulators/computers/amiga600.svg){.align-center}

## Technical data

* **Manufacturer**: Commodore
* **Year of release**: 1992
* **OS**: AmigaOS 2.05
* **CPU**: Motorola 68000 @ 7.09 (PAL) or 7.16 MHz (NTSC)
* **RAM**: 1 MB (expandable to 6 MB officially)
* **VRAM**: 1024 KB
* **ROM**: 512 KB Kickstart 2.05
* **GPU**: Commodore's Enhanced Chip Set
* **Sound chip**: 4 DMA-driven 8-bit channels
* **Résolution**: 320x200 to 1280x512 (4096 colors at lower display resolutions)

## Presentation

The **A600**, also called **Amiga 600**, was the last computer of the A500 line. Launched in late 1991, it was essentially a refurbished A500. Commodore International's intention was to boost sales of the A500 before the new, more sophisticated A1200 became available.

The most notable feature of the A600 was its small size. Without a numeric keypad, the A600 was only 35 cm long by 24 cm deep, 8 cm high and weighed approximately 3 kg.

It came with the Kickstart/Workbench v2 which was much more practical and attractive than its predecessor. It was targeted at the entry level, while the highest end of the range at the time was the A3000. The A600 used the Motorola 68000 processor clocked at 7.09 MHz (PAL) or 7.16 MHz (NTSC).

The standard memory was 1 MB. However, many people increased it to a maximum of 2 MB of chip RAM. Up to 4 MB of fast RAM could be added, using the PCMCIA port.

The original design did not allow a change of processor, as the 68000 was soldered to its motherboard. Despite this, upgrades including the Motorola 68010, 68020 (up to 25 MHz), and 68030 (up to 50 MHz) were offered by expansion card manufacturers. Up to 32 MB of fast RAM could be added on some cards.

Its most interesting connectors are perhaps the PCMCIA Type II port, and the internal IDE interface (for the 20 or 40 MB 2.5" hard disk). The model with the built-in 20 or 40 MB IDE hard drive was sold in the 1990s at almost twice the price of a standard A600, and called the "A600HD". The rest of the peripherals are similar to the A500.

In the 2010s, accelerator cards based on 680202 or 680303 are still produced. The presence of the PCMCIA port, allowing the addition of a Compact Flash card via an adapter, makes it one of the most popular Amiga models for retrogaming, along with the Amiga 1200.

## Emulators

[Amiberry](amiberry)
[Libretro PUAE](libretro-puae)