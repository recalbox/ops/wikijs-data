---
title: Surcharges RetroArch
description: Exemples de clefs pour surcharger la configuration retroarch.
published: true
date: 2021-10-03T22:17:25.224Z
tags: retroarch, surcharge
editor: markdown
dateCreated: 2021-05-21T07:52:56.503Z
---

Voici une liste non exhaustive des clefs pouvant être altérées dans un fichier de surcharge de type `.retroarch.cfg`, ces modifications ne s'effectue que si l'émulateur est un core RetroArch.

>Un \* après la valeur signifie que le paramètre peut être surchargé également via un .recalbox.conf
{.is-info}

## Audio

* `audio_enable = "true"` : activer ou désactiver l'audio.
* `audio_volume = "0.000000"` : régler le gain du volume, 0= volume par défaut par défaut.

## Menu RetroArch

* `quick_menu_show_save_content_dir_overrides = "false"` : montrer ou cacher l'option Remplacement de configuration pour le répertoire.
* `quick_menu_show_save_core_overrides = "false"` : montrer ou cacher l'option Remplacement de configuration pour le core.
* `quick_menu_show_save_game_overrides = "false"` : montrer ou cacher l'option Remplacement de configuration pour le jeu.

>Les Remplacements de configuration sont une fonctionnalité de RetroArch, similaire aux surcharges, mais qui retiennent beaucoup d'informations, dans le contexte de Recalbox, mieux vaut préférer les surcharges si possible.
{.is-info}

## Debug

* `fps_show = "true"` : montrer les FPS en jeu.
* `menu_driver = "ozone"` : choisir le menu retroarch, de base ozone, sauf sur GPi Case, où rgui est utilisé.
* `menu_enable_widgets = "true"` : activer les popups en jeu, si mit à false, affichera les notifications en tant que texte jaune.

## Dossiers

* `recording_output_directory = ""` : dossier où vont les enregistrements vidéos.
* `savefile_directory = ""` : où enregistrer/charger les sauvegardes.
* `savestate_directory = ""` : où enregistrer/charger les sauvegardes instantanées.
* `screenshot_directory = ""` : dossier où vont les captures d'écran.

## Service de traduction

* `ai_service_enable = "true"` : \* activer ou désactiver le service de traduction.
* `ai_service_mode = "0"` : _mode du service de traduction, 0: image, 1: voix.
* `ai_service_source_lang = "0"` : \* langue à lire pour le service de traduction, 0 = non spécifié.
* `ai_service_target_lang = "1"` : \* langue dans laquelle traduire, 1= Anglais, 3= Français.
* `ai_service_url =` : \* lien internet du service à utiliser.

## Overlays

* `aspect_ratio_index = "23"` : \* index du ratio, 23 = Custom.
* `input_overlay = ""` : lien vers le fichier de configuration de l'overlay.
* `input_overlay_enable = "true"` :  activation de l'overlay.
* `input_overlay_hide_in_menu = "true"` : overlay masqué dans le menu RetroArch.

### Coordonnées à définir avec l'overlay

* `custom_viewport_height = ""`
* `custom_viewport_width = ""`
* `custom_viewport_x = ""`
* `custom_viewport_y = ""`
* `video_message_pos_x = "0.050000"`
* `video_message_pos_y = "0.050000"`

## Netplay

* `netplay_nickname = ""` : \* pseudo Netplay.

## Rotation de l'écran

* `video_rotation = "1"` : pivote le rendu vidéo, 0= normal, 1=90°, 2=180°, 3=270°, attention à l'aspect ratio.

## Joystick et Pad directionnel

Pour dissocie/associe le Pad directionnel à un des joysticks :

* `input_player1_analog_dpad_mode = "0"` : dissocie.
* `input_player1_analog_dpad_mode = "1"` : associe au joystick gauche.
* `input_player1_analog_dpad_mode = "2"` : associe au joystick droite.
* `input_player1_analog_dpad_mode = "3"` : associe de force au joystick gauche
* `input_player1_analog_dpad_mode = "4"` : associe de force au joystick droit

## Remapping des hotkeys

>Les réglages pour changer les hotkeys sont dépendant du mapping de la manette dans Recalbox. Si la manette change, la configuration surchargée grâce à ces lignes peut ne plus marcher.
{.is-warning}

Pour avoir la valeur numérique pour chaque touche de votre manette, il faut regarder dans le fichier `/recalbox/share/system/configs/retroarch/retroarchcustom.cfg` la valeur de la clef voulue suivant ce tableau :

| Nom de la touche | Clé concernée dont il faut prendre la valeur |
| :--- | :--- |
| A | input_player1_a_btn |
| B | input_player1_b_btn |
| X | input_player1_x_btn |
| Y | input_player1_y_btn |
| Haut | input_player1_up_btn |
| Bas | input_player1_down_btn |
| Gauche | input_player1_left_btn |
| Droite | input_player1_right_btn |
| Select | input_player1_select_btn |
| Start | input_player1_start_btn |
| L | input_player1_l_btn |
| L2 | input_player1_l2_btn |
| L3 | input_player1_l3_btn |
| R | input_player1_r_btn |
| R2 | input_player1_r2_btn |
| R3 | input_player1_r3_btn |
| Stick Gauche en Haut | input_player1_l_y_minus_axis |
| Stick Gauche en Bas | input_player1_l_y_plus_axis |
| Stick Gauche à Gauche | input_player1_l_x_minus_axis |
| Stick Gauche à Droite | input_player1_l_x_plus_axis |
| Stick Droit en Haut | input_player1_r_y_minus_axis |
| Stick Droit en Bas | input_player1_r_y_plus_axis |
| Stick Droit à Gauche | input_player1_r_x_minus_axis |
| Stick Droit à Droite | input_player1_r_x_plus_axis |

>Les modifications à faire aux valeurs qui suivent sont à faire dans le fichier de surcharge.
>
>Le fichier précédent ne sert qu'a observer le mapping actuel.
{.is-warning}

* `input_enable_hotkey_btn =` :  Touche Hotkey.
* `input_screenshot_btn =` : Touche pour faire une capture d'écran.
* `input_exit_emulator_btn =` : Touche pour quitter le jeu.
* `input_load_state_btn =` : Touche pour charger une save state.
* `input_save_state_btn =` : Touche pour faire une sauvegarde instantanée.
* `input_menu_toggle_btn =` : Touche pour accéder au menu RetroArch.
* `input_reset_btn =` : Touche pour redémarrer le jeu.
* `input_ai_service_btn =` : Touche pour traduire l'écran actuel.

Par exemple, `The legend of Zelda: Link's Awakening` sur Game Boy nécessite d'utiliser `Start+Select+A+B` pour sauvegarder. Si votre manette n'a pas de bouton home, et que la hotkey est sur `Select`, la sauvegarde naturelle du jeu sera impossible. Mettre la touche `input_enable_hotkey_btn` sur la touche R de votre manette pour ce jeu uniquement peut se faire. La valeur de la clef `input_player1_r_btn` vaut 4 pour la touche R, alors il vous faudra entrer `input_enable_hotkey_btn = 4` dans la surcharge du jeu.