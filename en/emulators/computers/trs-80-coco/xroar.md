---
title: XRoar
description: 
published: false
date: 2021-11-09T20:34:12.929Z
tags: color, trs-80, computer, xroar, trs-80-coco, 8.0+
editor: markdown
dateCreated: 2021-09-12T08:41:53.472Z
---



## ![](/emulators/license.svg) License

This core is under [**GPL-3.0**](https://gitlab.com/recalbox/packages/standalone/xroar/-/blob/master/COPYING.GPL) and [**LGPL-3.0**](https://gitlab.com/recalbox/packages/standalone/xroar/-/blob/master/COPYING.LGPL) licenses.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ❌ | ✅ |

## ![](/emulators/features.png) Features

| Fonctionnalité | Supporté |
| :---: | :---: |


## ![](/emulators/bios.svg) BIOS

### List of mandatory bios

| Filename | Description | MD5 | Provided |
| :---: | :---: | :---: | :---: |
| bas11.rom | CoCo 1 BASIC | c73fb4bff9621c5ab17f6220b20db82f | ❌ |
| bas13.rom | CoCo 2 BASIC | c2fc43556eb6b7b25bdf5955bd9df825 | ❌ |
| extbas10.rom | CoCo 1 Extended BASIC | fda72f415afe99b36f953bb9bc1253da | ❌ |
| extbas11.rom | CoCo 2 Extended BASIC | 21070aa0496142b886c562bf76d7c113 | ❌ |

### Location

Put the bios like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 trs80coco
┃ ┃ ┃ ┃ ┣ 🗒 **bas11.rom**
┃ ┃ ┃ ┃ ┣ 🗒 **bas13.rom**
┃ ┃ ┃ ┃ ┣ 🗒 **extbas10.rom**
┃ ┃ ┃ ┃ ┣ 🗒 **extbas11.rom**

### List of optional bios

| Filename | Description | MD5 | Provided |
| :---: | :---: | :---: | :---: |
| bas10.rom | CoCo 1 BASIC | a74f3d95b395dad7cdca19d560eeea74 | ❌ |
| bas12.rom | CoCo 2 BASIC | c933316c7d939532a13648850c1c2aa6 | ❌ |
| disk10.rom | RSDos 1.0 | a64b3ef9efcc066b18d35b134068d1cc | ❌ |
| disk11.rom | RSDos 1.1 | 8cab28f4b7311b8df63c07bb3b59bfd5 | ❌ |
| hdbdw3bck.rom | RSDos Becker | ef750d93d24d7bc87a8ecd6e5a15a845 | ❌ |
| mx1600bas.rom | MX1600 BASIC | 88d1504e93366f498105a846cdbf7fb7 | ❌ |

### Location

Put the bios like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 trs80coco
┃ ┃ ┃ ┃ ┣ 🗒 **bas10.rom**
┃ ┃ ┃ ┃ ┣ 🗒 **bas12.rom**
┃ ┃ ┃ ┃ ┣ 🗒 **disk10.rom**
┃ ┃ ┃ ┃ ┣ 🗒 **disk11.rom**
┃ ┃ ┃ ┃ ┣ 🗒 **hdbdw3bck.rom**
┃ ┃ ┃ ┃ ┣ 🗒 **mx1600bas.rom**

## ![](/emulators/roms.png) Roms

### Supported extensions

Roms must have the extension:

* .cas
* .wav
* .bas
* .asc
* .dmk
* .jvc
* .os9
* .dsk
* .vdk
* .rom
* .ccc

### Location

Put the roms like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 trs80coco
┃ ┃ ┃ ┃ ┣ 🗒  **game.dsk**

>For more information about roms, go to [this tutorial](./../../../tutorials/games/generalities/isos-and-roms)!
{.is-info}

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://gitlab.com/recalbox/packages/standalone/xroar/](https://gitlab.com/recalbox/packages/standalone/xroar/)
* **Official website**: [https://www.6809.org.uk/xroar/](https://www.6809.org.uk/xroar/)