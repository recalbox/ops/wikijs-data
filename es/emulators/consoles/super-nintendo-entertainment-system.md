---
title: Super Nintendo Entertainment System
description: Super Nintendo Entertainment System
published: true
date: 2024-07-26T08:06:29.651Z
tags: nintendo, snes, super nintendo, consolas
editor: markdown
dateCreated: 2023-09-03T20:24:55.146Z
---

![](/emulators/consoles/snes.svg){.align-center}

## Detalles técnicos

* **Fabricante**: Nintendo
* **Año de lanzamiento**: 1990
* **Cantidad vendida**: 49,10 millones
* **Juego más vendido**: Super Mario World
* **Procesador**: 65C816 a 3,58 MHz 16 bits modificados
* **RAM**: 128kB / VRAM: 64kB
* **Vídeo**: 16 bits PPU (Picture Processor Unit)
* **Resolución**: 256x224, 256 colores (paleta de 32768 colores)
* **Chip de sonido**: 8 bits Sony SPC700, 8 canales
* **Tamaño del cartucho** : 256kB - 6MB

## Presentación

La **Super Nintendo Entertainment System** (comúnmente abreviada como **Super NES** o **SNES** o **Super Nintendo**), o **Super Famicom** en Japón, es una videoconsola del fabricante japonés Nintendo que salió a la venta en noviembre de 1990. En Norteamérica, la consola salió a la venta con un aspecto decididamente diferente. En Corea del Sur, la Super Nintendo fue distribuida por Hyundai Electronics con el nombre de **Super Comboy**.

En 1997 y 1998, Nintendo decidió renovar el diseño lanzando el modelo SNS-101, a veces llamado **Super NES 2** en Estados Unidos, y **Super Famicom Jr.** en Japón.

## Emuladores

[Libretro Mesen_s](libretro-mesen_s)
[Libretro Snes9x](libretro-snes9x)
[Libretro Snes9x_2002](libretro-snes9x-2002)
[Libretro Snes9x_2005](libretro-snes9x-2005)
[Libretro Snes9x_2010](libretro-snes9x-2010)
[Libretro bsnes](libretro-bsnes)
[Libretro bsnes HD](libretro-bsnes-hd)
[Libretro-supafaust](libretro-supafaust)
[piSNES](pisnes)