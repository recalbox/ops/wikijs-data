---
title: Sharp X68000
description: 
published: true
date: 2024-07-26T12:51:44.885Z
tags: sharp, x68000, ordenadores
editor: markdown
dateCreated: 2023-09-02T19:01:55.437Z
---

![](/emulators/computers/sharpx68000.svg){.align-center}

## Datos técnicos

* **Fabricante**: Sharp
* **Año de lanzamiento**: 1987
* **Sistema operativo**: Human68k
* **Procesador**: Motorola 68000 a 10 MHz
* **RAM**: 1-4 MB (hasta 12 MB)
* **VRAM**: 1056 KB
* **ROM**: 1 MB (128 KB BIOS, 768 KB Generador de caracteres)
* **Procesador gráfico**: Chipset Sharp-Hudson modificado
* **Chip de sonido**: Yamaha YM2151
* **Resolución**: 512x512 (65536 colores)

## Presentación

El **Sharp X68000**, comúnmente conocido como **X68000** o **X68k**, es un ordenador personal de Sharp Corporation lanzado en Japón en 1987. El primer modelo utilizaba un microprocesador Motorola 68000 de 10 MHz, con 1 megabyte de RAM y sin disco duro. El último modelo, lanzado en 1993, tenía un Motorola 68030 a 25 MHz, con 4 MB de RAM y un disco duro SCSI opcional de 80 MB. Los modelos básicos tenían dos unidades de disquete de 5"¼.

Este ordenador ejecuta un sistema operativo llamado Human68k, desarrollado conjuntamente por el propio fabricante, es decir, Sharp y Hudson Soft. Human68k funciona según el mismo principio que MS-DOS, que se basa en comandos introducidos por el usuario. Los archivos ejecutables terminan con la extensión `.X'. Se han lanzado tres versiones principales de este sistema operativo.

Al igual que los ordenadores personales occidentales como el Amiga y el Atari ST, la arquitectura del X68000 lo acercaba a los sistemas arcade de la época. ¡Como resultado, varios juegos arcade famosos se desarrollaron en este ordenador, como Bubble Bobble, Columns, Final Fight, Ys, Castlevania, Street Fighter, Ghosts'n Goblins, After Burner y Parodius Da!

El sistema CPS-1 es un derivado autónomo del X68000. De ahí las versiones casi perfectas en píxeles de Final Fight y Ghouls'n Ghosts. Casi, porque en Final Fight, por ejemplo, el X68000 no puede mostrar tantos sprites simultáneos como la versión CPS1.

## Emuladores

[Libretro PX68k](libretro-px68k)