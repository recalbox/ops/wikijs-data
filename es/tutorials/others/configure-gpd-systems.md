---
title: Configurar los sistemas GPD
description: 
published: true
date: 2023-09-18T06:40:15.425Z
tags: gpd, sistemas
editor: markdown
dateCreated: 2023-09-18T06:40:15.425Z
---

## Introducción

El hardware GPD no siempre se reconoce correctamente (por ejemplo, la pantalla gira de forma no deseada, etc.). Sin embargo, existe un procedimiento para hacerlo compatible dependiendo de su referencia de hardware.

Este tutorial se aplica a las siguientes referencias:

- GPD Pocket
- GPD Pocket 2
- GPD Pocket 3
- GPD P2 Max
- GPD MicroPC
- PC Win 2
- GPD Win Max
- Topjoy Falcon

>¡Este procedimiento debe aplicarse después de **CADA** actualización así como después de una instalación !
{.is-warning}

## Procedimiento

### Requisitos previos

- Haber instalado Recalbox en una de las referencias listadas arriba.
- Estar cómodo con los comandos SSH y la transferencia de archivos.

### Primera instalación

1. Debéis obtener un script específico para poder instalar todo lo necesario para que el hardware funcione correctamente. Id a https://github.com/wimpysworld/umpc-ubuntu, pulsad el botón verde y haced clic en `Download ZIP`.

   Descomprimid el contenido, abrid el fichero `umpc-ubuntu.sh` y modificad la 4ª línea según el modelo que poseas:

| Modèle | Valeur |
| :---: | :---: |
| GPD Pocket | `gpd-pocket` |
| GPD Pocket 2 | `gpd-pocket2` |
| GPD Pocket 3 | `gpd-pocket3` |
| GPD P2 Max | `gpd-p2-max` |
| GPD MicroPC | `gpd-micropc` |
| GPD Win 2 | `gpd-win2` |
| GPD Win Max | `gpd-win-max` |
| Topjoy Falcon | `topjoy-falcon` |

`UMPC="valeur-de-votre-modele"`

Grabad el fichero con vuestra modificación.

2. Desplazad el directorio descomprido (que debe tener como nombre `umpc-ubuntu-master`) dentro del directorio `/recalbox/share/system/`. Al final debéis tener el siguiente directorio `/recalbox/share/system/umpc-ubuntu-master/` con los ficheros descomprimidos dentro.

3. Ahora hay que conectarse [vía SSH](../system/access/root-access-terminal-cli) y ejecutar uno detrás de otro los siguientes comandos:

```ssh
mount -o remount,rw /
mount -o remount,rw /boot
cd umpc-ubuntu-master
bash umpc-ubuntu.sh enable
```

>Puede haber un error relacionado con update-grub. Si ocurre es normal, no os preocupéis.
{.is-info}

4. Abrid el fichero `/boot/EFI/BOOT/grub.cfg` y en la línea 9, añadid al final de la misma:

```ini
fbcon=rotate:1 video=eDP-1:800x1280 drm.edid_firmware=eDP-1:edid/gpd-win-max-edid.bin
```

>No olvidéis de poner un espacio en blanco entre `loglevel=0` y `fbcon=rotate:1` !
{.is-warning}

>También podéis hacer lo mismo en la línea 15, para que se aplique también cuando seleccionéis la opción de la línea 15 al arranque.
{.is-info}

Ahora sólo tenéis que reiniciar y disfrutar de vuestro sistema.

### Actualización

En cada actualización, tenéis que llevar a cabo los pasos 3 y 4 anteriores.