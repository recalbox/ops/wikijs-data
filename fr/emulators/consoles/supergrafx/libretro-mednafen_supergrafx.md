---
title: Libretro Mednafen_SuperGrafx
description: 
published: true
date: 2024-06-22T11:09:45.431Z
tags: libretro, mednafen, supergrafx
editor: markdown
dateCreated: 2021-05-21T08:27:54.664Z
---



## ![](/emulators/license.svg) Licence

Ce core est sous licence [**GPLv2**](https://github.com/libretro/beetle-supergrafx-libretro/blob/master/COPYING).

## ![](/emulators/compatibility.png) Compatibilité

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Fonctionnalités

| Fonctionnalité | Supporté |
| :---: | :---: |
| Redémarrage  | ✔ |
| Captures d'écran | ✔ |
| Sauvegardes | ✔ |
| Sauvegardes instantanées | ✔ |
| Rembobinage | ✔ |
| Netplay | ✔ |
| Options du core | ✔ |
| RetroAchievements | ✔ |
| Cheats RetroArch | ✔ |
| Contrôles | ✔ |
| Remapping | ✔ |
| Multi-Mouse | ✔ |

## ![](/emulators/bios.svg) BIOS

### Liste des bios optionnels

| Nom de fichier | Description | MD5 | Fourni |
| :---: | :---: | :---: | :---: |
| gexpress.pce | Game Express CD Card | 6d2cb14fc3e1f65ceb135633d1694122 | ❌ |

### Emplacement

Placez le BIOS comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 pcengine
┃ ┃ ┃ ┃ ┣ 🗒 **gexpress.pce**

## ![](/emulators/roms.png) Roms

### Extensions supportées

Les roms doivent avoir l'extension suivante :

* .pce/.sgx
* .zip
* .7z

Ce système supporte les roms compressées au format .zip/.7z. Attention toutefois, il ne s'agit que d'une archive.

Les fichiers contenus dans les .zip/.7z doivent correspondre aux extensions citées précédemment.
De plus, chaque fichier .zip/.7z ne doit contenir qu'une seule rom compressée.

### Emplacement

Placez les roms comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 supergrafx
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.zip**

>Les roms au format **No-Intro** sont vivement conseillées.
{.is-success}

>Pour plus d'information sur les roms, rendez-vous sur [ce tutoriel](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuration avancée de l'émulateur

>Pour pouvoir conserver vos configurations personnalisées lors d'une mise à jour, nous vous conseillons d'utiliser notre fonctionnalité [Surcharges de configuration](./../../../advanced-usage/configuration-override).
{.is-info}

### Accéder aux options

Vous pouvez configurer diverses options de deux façons différentes.

* Via le menu RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via le fichier `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Options du core

| Options | Valeur des options | Variable (pour surcharge) | Valeurs des variables |
| --- | --- | --- | --- |
| Palette de couleurs | `RVB` ✅ / `Composite` | `sgx_palette` | `RGB` / `Composite` |
| Rapport d'aspect | `Auto` ✅ / `6:5` / `4:3` | `sgx_aspect_ratio` | `auto` / `6:5` / `4:3` |
| Surbalayage horizontal (Mode largeur 352 uniquement) | `300` / `302` / `304` / `306` / `308` / `310` / `312` / `314` / `316` / `318` / `320` / `322` / `324` / `326` / `328` / `330` / `332` / `334` / `336` / `338` / `340` / `342` / `344` / `346` / `348` / `350` / `352 (par défaut)` ✅ | `sgx_hoverscan` | `300` / `302` / `304` / `306` / `308` / `310` / `312` / `314` / `316` / `318` / `320` / `322` / `324` / `326` / `328` / `330` / `332` / `334` / `336` / `338` / `340` / `342` / `344` / `346` / `348` / `350` / `352` |
| Ligne de balayage initial | `0` / `1` / `2` / `3 (par défaut)` ✅ / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `21` / `22` / `23` / `24` / `25` / `26` / `27` / `28` / `29` / `30` / `31` / `32` / `33` / `34` / `35` / `36` / `37` / `38` / `39` / `40` | `sgx_initial_scanline` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `21` / `22` / `23` / `24` / `25` / `26` / `27` / `28` / `29` / `30` / `31` / `32` / `33` / `34` / `35` / `36` / `37` / `38` / `39` / `40` |
| Dernière ligne de balayage | `208` / `209` / `210` / `211` / `212` / `213` / `214` / `215` / `216` / `217` / `218` / `219` / `220` / `221` / `222` / `223` / `224` / `225` / `226` / `227` / `228` / `229` / `230` / `231` / `232` / `233` / `234` / `235` / `236` / `237` / `238` / `239` / `240` / `241` / `242 (par défaut)` ✅ | `sgx_last_scanline` | `208` / `209` / `210` / `211` / `212` / `213` / `214` / `215` / `216` / `217` / `218` / `219` / `220` / `221` / `222` / `223` / `224` / `225` / `226` / `227` / `228` / `229` / `230` / `231` / `232` / `233` / `234` / `235` / `236` / `237` / `238` / `239` / `240` / `241` / `242` |
| Sensibilité de la souris | `0.25` / `0.50` / `0.75` / `1.00` / `1.25` ✅ / `1.50` / `1.75` / `2.00` / `2.25` / `2.50` / `2.75` / `3.00` / `3.25` / `3.50` / `3.75` / `4.00` / `4.25` / `4.50` / `4.75` / `5.00` | `sgx_mouse_sensitivity` | `0.25` / `0.50` / `0.75` / `1.00` / `1.25` / `1.50` / `1.75` / `2.00` / `2.25` / `2.50` / `2.75` / `3.00` / `3.25` / `3.50` / `3.75` / `4.00` / `4.25` / `4.50` / `4.75` / `5.00` |
| Autoriser les directions opposées | `Désactivé` ✅ / `Activé` | `sgx_up_down_allowed` | `disabled` / `enabled` |
| Désactiver la réinitialisation logicielle (RUN+SELECT) | `Désactivé` ✅ / `Activé` | `sgx_disable_softreset` | `disabled` / `enabled` |
| Contrôleur multitap à 5 ports | `Activé` ✅ / `Désactivé` | `sgx_multitap` | `enabled` / `disabled` |
| Type de manette par défaut pour le joueur 1 | `2 touches` ✅ / `6 touches` | `sgx_default_joypad_type_p1` | `2 Buttons` / `6 Buttons` |
| Type de manette par défaut pour le joueur 2 | `2 touches` ✅ / `6 touches` | `sgx_default_joypad_type_p2` | `2 Buttons` / `6 Buttons` |
| Type de manette par défaut pour le joueur 3 | `2 touches` ✅ / `6 touches` | `sgx_default_joypad_type_p3` | `2 Buttons` / `6 Buttons` |
| Type de manette par défaut pour le joueur 4 | `2 touches` ✅ / `6 touches` | `sgx_default_joypad_type_p4` | `2 Buttons` / `6 Buttons` |
| Type de manette par défaut pour le joueur 5 | `2 touches` ✅ / `6 touches` | `sgx_default_joypad_type_p5` | `2 Buttons` / `6 Buttons` |
| Mode des touches de raccourci du turbo | `Désactivé` ✅ / `On/Off Toggle` / `Dedicated Turbo Buttons` | `sgx_turbo_toggle` | `disabled` / `switch` / `dedicated` |
| Touches de raccourci turbo alternatives | `X/Y` ✅ / `L3/R3` | `sgx_turbo_toggle_hotkey` | `disabled` / `enabled` |
| Délai du turbo | `1` / `2` / `3` ✅ / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` | `sgx_turbo_delay` | `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` |
| Mise en cache des images CD (Redémarrage requis) | `Désactivé` ✅ / `Activé` | `sgx_cdimagecache` | `disabled` / `enabled` |
| BIOS CD (Redémarrage requis) | `System Card 3` ✅ / `Games Express` / `System Card 1` / `System Card 2` / `System Card 2 US` / `System Card 3 US` | `sgx_cdbios` | `System Card 3` / `Games Express` / `System Card 1` / `System Card 2` / `System Card 2 US` / `System Card 3 US` |
| Détecter les CD de Games Express (Redémarrage requis) | `Activé` ✅ / `Désactivé` | `sgx_detect_gexpress` | `enabled` / `disabled` |
| Vitesse du CD | `1` ✅ / `2` / `4` / `8` | `sgx_cdspeed` | `1` / `2` / `4` / `8` |
| Volume ADPCM % | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ / `110` / `120` / `130` / `140` / `150` / `160` / `170` / `180` / `190` / `200` | `sgx_adpcmvolume` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` / `110` / `120` / `130` / `140` / `150` / `160` / `170` / `180` / `190` / `200` |
| Volume CDDA % | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ / `110` / `120` / `130` / `140` / `150` / `160` / `170` / `180` / `190` / `200` | `sgx_cddavolume` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` / `110` / `120` / `130` / `140` / `150` / `160` / `170` / `180` / `190` / `200` |
| Volume PSG % | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ / `110` / `120` / `130` / `140` / `150` / `160` / `170` / `180` / `190` / `200` | `sgx_cdpsgvolume` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` / `110` / `120` / `130` / `140` / `150` / `160` / `170` / `180` / `190` / `200` |
| Forcer l'émulation de la SuperGrafx (Redémarrage requis) | `Désactivé` ✅ / `Activé` | `sgx_forcesgx` | `disabled` / `enabled` |
| Aucune limite de sprites | `Désactivé` ✅ / `Activé` | `sgx_nospritelimit` | `disabled` / `enabled` |
| Multiplicateur d'overclocking du processeur (Redémarrage requis) | `1` ✅ / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `20` / `30` / `40` / `50` | `sgx_ocmultiplier` | `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `20` / `30` / `40` / `50` |

## ![](/emulators/external-links.png) Liens externes

* **Code source utilisé** : [https://github.com/libretro/beetle-supergrafx-libretro/](https://github.com/libretro/beetle-supergrafx-libretro/)
* **Documentation Libretro** : [https://docs.libretro.com/library/beetle_sgx/](https://docs.libretro.com/library/beetle_sgx/)