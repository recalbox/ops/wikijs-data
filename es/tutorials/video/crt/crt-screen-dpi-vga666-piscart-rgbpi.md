---
title: Configurad vuestra pantalla CRT a través del bus DPI (VGA666 / PiScart / RGBPi) (obsoleto)
description: 
published: true
date: 2023-09-10T12:32:23.519Z
tags: dpi, bus, vga666, piscart, rgbpi, cathodique, ecran
editor: markdown
dateCreated: 2023-09-10T12:32:23.519Z
---


>**Este artículo no está actualizado!**
Consultad [Configurad vuestra pantalla CRT en los Raspberry Pi 4 / 400 / 3
](./recalbox-on-crt-with-scart-dac)
{.is-warning}

## ¿Qué es el bus DPI?

Se puede encontrar una explicación detallada en [el siguiente enlace](https://www.raspberrypi.org/documentation/hardware/raspberrypi/dpi/README.md).

Todas las placas madres Raspberry Pi con el cabezal de 40 pines (A+, B+, Pi2, Pi3, Zero), junto con el Compute Module, disponen de una interfaz RGB paralela de hasta 24 bits. Esta interfaz permite conectar pantallas RGB paralelas al GPIO de la Raspberry Pi, ya sea en RGB24 (8 bits para rojo, verde y azul), RGB666 (6 bits por color) o RGB565 (5 bits para rojo, 6 para verde y 5 para azul).

Esta interfaz está controlada por el firmware de la GPU y puede ser programada por un usuario a través de parámetros especiales config.txt y habilitando la superposición correcta del árbol de dispositivos Linux.

Tened en cuenta que existen diferentes formas de representar los valores de color en los pines de salida DPI en modo 565, 666 o 24 bits (consultad la siguiente tabla y la parte `output_format` del parámetro `dpi_output_format` más abajo):

![GPIOs utilizados dependiendo del modo de representación](/tutorials/video/crt/dpi_output.png)

Tened en cuenta que todos los otros dispositivos periféricos superpuestos que usan pines GPIO conflictivos se deben deshabilitar. En el fichero [recalbox-user-config.txt](./../../../tutorials/system/modification/configtxt-configuration), aseguráos de comentar o invertir cualquier parámetro dtp que habilite I2C o SPI :

```ini
dtparam=i2c_arm=off
dtparam=spi=off
```

## El Gert VGA666

El Gert VGA 666 (6 bits por canal de color, de ahí su nombre 666) es un add-on / placa madre de expansión para el modelo Raspberry Pi B + (no funcionará con el modelo A / B, ya que se requieren los pines GPIO adicionales en el modelo B +). Esta es una creación de hardware de código abierto recientemente hecha pública por Gert van Loo, uno de los ingenieros de hardware que contribuyeron al diseño original de Raspberry Pi (también uno de los arquitectos del chip BCM2835 en el corazón de la Raspberry Pi) y alguien con quien muchos de vosotros podéis haber hablado en los Raspberry Jams o en los multiples foros de Raspberry Pi.

Es un método limpio y muy útil de utilizar una pantalla/monitor VGA con el Raspberry Pi y es mucho más barato que un adaptador HDMI a VGA o similar. La conexión VGA se maneja de forma nativa en hardware por los pines GPIO (utilizando una interfaz paralela) y utiliza aproximadamente la misma carga de CPU que la conexión HDMI a bordo. Es capaz de mostrar vídeo VGA 1080p60 sin carga de CPU.

También es posible manejar esta interfaz al mismo tiempo que la conexión HDMI, por lo que también es posible una configuración de doble pantalla. Este complemento no era posible en los modelos Raspberry Pi A y B, porque no se habían colocado todos los pines necesarios en el cabezal GPIO. ¡Otra brillante mejora que ha hecho posible el modelo B+!

![Le module Gert VGA666](/tutorials/video/crt/vga666-1.jpg)

En primer lugar tenemos que abrir el fichero [recalbox-user-config.txt](./../../../tutorials/system/modification/configtxt-configuration) :

Tened en cuenta que para que el Gert VGA 666 funcione correctamente, hay que desactivar SPI y I2C. Par ello, añadid las siguientes líneas al final del fichero anterior:

```ini
dtparam=spi=off
dtparam=i2c_arm=off
dtoverlay=vga666 
enable_dpi_lcd=1 
display_default_lcd=1 
```

También necestáis especificar la resolución de vuestro monitor (esto no se aplica a las pantallas de televisión). Continuad editando el fichero y añadid a continuación una de las siguientes configuraciones:

```ini
#For 1920x1080 60Hz
dpi_group=2
dpi_mode=82
#For 1280x1024 60Hz
dpi_group=2
dpi_mode=35
#For 1024x768 60Hz
dpi_group=2
dpi_mode=16
#For 800x600 60Hz
dpi_group=2
dpi_mode=9
```

![VGA666 en un monitor de PC](/tutorials/video/crt/vga666-2.jpg)

Si no encontráis la resolución que queréis entre la lista de aquí arriba, podéis consultar [este enlace](https://www.raspberrypi.org/documentation/configuration/config-txt/video.md).

Para las pantallas de television, la configuración sera diferente. Debemos utilizar el `dpi_mode` 87 personalizado y debemos corregir los `hdmi_timings` para configurar el modmo personalizado de 15KHz. Este modo personalizado se aplicará a la interfaz de EmulationStation. Se pueden utilizar varios `hdmi_timings` diferentes:

```ini
dtparam=i2c_arm=off
dtparam=spi=off
 
# Enable VGA666
dtoverlay=vga666
enable_dpi_lcd=1
display_default_lcd=1
 
dpi_group=2
dpi_mode=87
 
#hdmi_timings=506 1 8 48 56 240 1 3 10 6 0 0 0 60 0 9600000 1
#hdmi_timings=512 1 16 48 64 288 1 3 5 6 0 0 0 50 0 9600000 1
 
# Custom 15kHz mode
hdmi_timings=506 1 8 44 52 240 1 6 10 6 0 0 0 60 0 9600000 1
#hdmi_timings=320 1 17 33 34 224 1 14 8 18 0 0 0 60 0 6400000 1
#hdmi_timings=960 0 173 8 0 160 0 40 10 0 0 0 0 60 0 19200000 8
#hdmi_timings=320 1 25 30 30 240 1 9 3 10 0 0 0 60 0 6400000 1
#hdmi_timings=1920 1 52 208 260 240 1 6 10 6 0 0 0 60 0 38400000 1
```

Se pueden utilizar varios `hdmi_timings` diferentes al mismo tiempo. A continuación tenéis una lista de estos timings, que os permitira ahorra tiempo en la construcción de vuestra configuración.

```ini
OK
hdmi_timings=996 1 24 96 120 240 1 3 10 6 0 0 0 60 0 19200000 1
hdmi_timings=506 1 8 44 52 240 1 6 10 6 0 0 0 60 0 9600000 1
hdmi_timings=336 1 10 30 34 252 1 15 3 42 0 0 0 50 0 6400000 1 #with borders
hdmi_timings 480 1 29 35 66 234 1 4 10 18 0 0 0 60 0 9600000 1
hdmi_timings=990 1 22 94 116 240 1 6 10 6 0 0 0 60 0 19200000 1
hdmi_timings=820 1 42 260 100 240 1 6 10 6 0 0 0 60 0 19200000 1
Perfect
hdmi_timings=320 1 12 32 44 240 1 6 10 6 0 0 0 60 0 6400000 1
hdmi_timings=506 1 8 44 52 240 1 6 10 6 0 0 0 60 0 9600000 1
hdmi_timings=996 1 24 96 120 240 1 3 10 6 0 0 0 60 0 19200000 1
hdmi_timings=1920 1 52 208 260 240 1 6 10 6 0 0 0 60 0 38400000 1
hdmi_timings=512 1 16 48 64 288 1 3 5 6 0 0 0 50 0 9600000 1
OK
hdmi_timings=996 1 24 96 120 240 1 3 10 6 0 0 0 60 0 19200000 1
hdmi_timings=506 1 8 44 52 240 1 6 10 6 0 0 0 60 0 9600000 1
hdmi_timings=336 1 10 30 34 252 1 15 3 42 0 0 0 50 0 6400000 1 #with borders
hdmi_timings 480 1 29 35 66 234 1 4 10 18 0 0 0 60 0 9600000 1
hdmi_timings=990 1 22 94 116 240 1 6 10 6 0 0 0 60 0 19200000 1
hdmi_timings=820 1 42 260 100 240 1 6 10 6 0 0 0 60 0 19200000 1
Perfect
hdmi_timings=320 1 12 32 44 240 1 6 10 6 0 0 0 60 0 6400000 1
hdmi_timings=506 1 8 44 52 240 1 6 10 6 0 0 0 60 0 9600000 1
hdmi_timings=996 1 24 96 120 240 1 3 10 6 0 0 0 60 0 19200000 1
hdmi_timings=1920 1 52 208 260 240 1 6 10 6 0 0 0 60 0 38400000 1
hdmi_timings=512 1 16 48 64 288 1 3 5 6 0 0 0 50 0 9600000 1
OK
hdmi_timings=996 1 24 96 120 240 1 3 10 6 0 0 0 60 0 19200000 1
hdmi_timings=506 1 8 44 52 240 1 6 10 6 0 0 0 60 0 9600000 1
hdmi_timings=336 1 10 30 34 252 1 15 3 42 0 0 0 50 0 6400000 1 #with borders
hdmi_timings 480 1 29 35 66 234 1 4 10 18 0 0 0 60 0 9600000 1
hdmi_timings=990 1 22 94 116 240 1 6 10 6 0 0 0 60 0 19200000 1
hdmi_timings=820 1 42 260 100 240 1 6 10 6 0 0 0 60 0 19200000 1
Perfect
hdmi_timings=320 1 12 32 44 240 1 6 10 6 0 0 0 60 0 6400000 1
hdmi_timings=506 1 8 44 52 240 1 6 10 6 0 0 0 60 0 9600000 1
hdmi_timings=996 1 24 96 120 240 1 3 10 6 0 0 0 60 0 19200000 1
hdmi_timings=1920 1 52 208 260 240 1 6 10 6 0 0 0 60 0 38400000 1
hdmi_timings=512 1 16 48 64 288 1 3 5 6 0 0 0 50 0 9600000 1
```

A continuación, debemos indicar a Recalbox cómo se visualiza EmulationStation y la resolucón por defecto de los emuladores:

* Abrid el fichero [recalbox.conf](./../../../basic-usage/getting-started/recalboxconf-file)
* Buscad la línea `global.videomode`.
* Indicad el valor `DMT 87 HDMI` como aquí abajo:

```ini
global.videomode=DMT 87 HDMI
```

![VGA666 con una pequeña pantalla de televisión](/tutorials/video/crt/vga666-3.png)

RetroArch permite una configuración básica específica para la salida de vídeo. Puedes crear un fichero de configuración por core. Para ello, cread los ficheros de configuración en el directorio `/share/system/configs/retroarch`.

Utilizamos el aspect ratio personalizado "23" para modificar la visual de RetroArch.

Para la snes | snes.cfg

```ini
aspect_ratio_index = "23"
custom_viewport_width = "900"
custom_viewport_height = "224"
custom_viewport_x = "58"
custom_viewport_y = "13"
```

Para la megadrive | megadrive.cfg

```ini
aspect_ratio_index = "23"
custom_viewport_width = "900"
custom_viewport_height = "224"
custom_viewport_x = "58"
custom_viewport_y = "13"
```

Para la nes | nes.cfg

```ini
aspect_ratio_index = "23"
custom_viewport_width = "900"
custom_viewport_height = "224"
custom_viewport_x = "58"
custom_viewport_y = "13"
```

Para la fba | fba_libretro.cfg

```ini
aspect_ratio_index = "23"
custom_viewport_width = "900"
custom_viewport_height = "224"
custom_viewport_x = "58"
custom_viewport_y = "13"
```

Para mame | mame.cfg

```ini
aspect_ratio_index = "23"
custom_viewport_width = "900"
custom_viewport_height = "224"
custom_viewport_x = "58"
custom_viewport_y = "13"
```

![Détails de l'image](/tutorials/video/crt/vga666-4.png)

En los emuladores, es probable que necesitéis ajustar manualmente las variables personalizadas de la ventana de visualización. Para ello, debes iniciar el menú RetroArch pulsando `Hotkey + B`.

## RGBPi : câble SCARt/Peritel para las pantallas CRT

Disfrutad de la sencillez de un cable listo para conectar a vuestro televisor CRT y a vuestra RaspberryPi con nuestro sistema operativo personalizado,  con resoluciones RGB y PixelPerfect para todos los juegos, con sonido estéreo mejorado y con una nueva función de conmutación automática, compatible por el momento con los modelos 2B, 3B, 3B+...

Esta nueva función de conmutación automática en el canal AV en 4:3, con un RGB de 18 bits (sin conversiones) y con una señal CSync limpia (generada en la placa de circuitos y enviada a travès del cable SCART) ofrece la mejor calidad de señal posible y la menor pérdida, junto con dos canales de audio filtrados mejorados, todo ello preconfigurado.

![Adaptateur RGB-Pi](/tutorials/video/crt/rgbpi1.png)

![Détails de l&apos;adaptateur RGB-Pi
](/tutorials/video/crt/rgbpi2.png)

Si queréis utilizarlo con Recalbox, podéis utilizar el mismo procedimiento que en el caso del VGA666, con unas pequeñas modificaciones. Aquí tenéis el contenido del fichero para el RGBPi [recalbox-user-config.txt](./../../../tutorials/system/modification/configtxt-configuration) :

```ini
hdmi_timings=320 1 10 30 40 240 1 3 4 6 0 0 0 60 0 6400000 1

## RGB-Pi Custom Configuration
## IMPORTANT! hdmi_timings must be always in the first line
## hdmi_timings=<h_active_pixels> <h_sync_polarity <h_front_porch> <h_sync_pulse
> <h_back_porch> <v_active_lines> <v_sync_polarity> <v_front_porch> <v_sync_puls
e> <v_back_porch> <v_sync_offset_a> <v_sync_offset_b> <pixel_rep> <frame_rate> <
interlaced> <pixel_freq> <aspect_ratio>
## Usable Pixel clock are : 4800000 - 6400000 - 9600000 - 19200000 and anything 
over 38400000

disable_overscan=1
hdmi_force_hotplug=1
config_hdmi_boost=0
dtparam=audio=on
display_rotate=0
audio_pwm_mode=2
disable_audio_dither=1
hdmi_drive=2
boot_delay=3
disable_splash=1
avoid_safe_mode=1
dtoverlay=pwm-2chan,pin=18,func=2,pin2=19,func2=2
framebuffer_depth=32
framebuffer_ignore_alpha=1
#dtoverlay=rgb-pi
#dpi24 is equal to rgb-pi
dtoverlay=dpi24
enable_dpi_lcd=1
display_default_lcd=1
dpi_output_format=6
dpi_group=2
dpi_mode=87
kernel=zImage
overscan_scale=0
avoid_warnings=1
gpu_mem_256=64
gpu_mem_512=128
gpu_mem_1024=256
dtparam=i2c_vc=on
dtoverlay=i2c-gpio,i2c_gpio_sda=10,i2c_gpio_scl=11
```

## Pi2Scart

El Pi2SCART es una placa madre de circuito impreso que añade una salida de audio/vídeo de 15Khz a vuestra Raspberry Pi a través de una toma SCART. La PI2SCART se conecta simplemente a la interfaz GPIO de la Raspberry. El sonido se recoge de la toma de auriculares a través de un cable de audio. El PI2SCART no necesita alimentación adicional para funcionar.

![Module Pi2Scart branché sur un Pi](/tutorials/video/crt/rgbpi3.png)

Como los dos sistemas precedentes, el Pi2Scart utiliza el bus DPI y los pines GPIO para funcionar. Utiliza el overlay VGA66 y el `dpi_mode 87`. Para hacerlo funcionar con Recalbox, debéis modificar el fichero [recalbox-user-config.txt](./../../../tutorials/system/modification/configtxt-configuration) y añadir las siguientes líneas:

```ini
disable_audio_dither=1
dtparam=audio=on
dtoverlay=vga666
enable_dpi_lcd=1
display_default_lcd=1
dpi_group=2
dpi_mode=87
hdmi_timings=320 1 16 30 34 240 1 2 3 22 0 0 0 60 0 6400000 1 #240p
```

>Este ajuste de resolución corresponde a una resolución común a todos los emuladores, que puede no adaptarse perfectamente a tu pantalla CRT. Depende de vosotros si queréis ajustarla directamente desde vuestra pantalla (menú de fábrica, potenciómetro) o a través de Recalbox (overscan o a través de RetroArch). En este último caso (RetroArch), podéis anular la configuración básica para definir una resolución de vídeo específica.
{.is-warning}

>En el caso de pruebas en un terminal o bartop, no olvidéis modificar la opción `controllers.gpio.enabled` para activar los comandos GPIO, cambiando su valor de 1 a 0.
{.is-warning}

RetroArch permite una configuración básica específica para la salida de vídeo. Podéis crear un fichero de configuración por core. Para ello, cread los ficheros de configuración en el directorio `/share/system/configs/retroarch`.

Utilizamos el aspect ratio personalizado "23" para modificar la visual de RetroArch.

Para la snes | snes.cfg

```ini
aspect_ratio_index = "23"
custom_viewport_width = "900"
custom_viewport_height = "224"
custom_viewport_x = "58"
custom_viewport_y = "13"
```