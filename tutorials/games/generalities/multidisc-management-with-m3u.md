---
title: Multi-disc management with .M3U
description: 
published: true
date: 2024-07-23T22:51:22.383Z
tags: management, multidisc, m3u
editor: markdown
dateCreated: 2022-12-27T11:02:17.835Z
---

## Introduction

Multiple CDs of the same game can be loaded simultaneously from EmulationStation into RetroArch by creating a `.M3U` playlist file (plain text `.TXT` with a `.M3U` extension).

An `.M3U` playlist can be used with any game extension:
- `.BIN` / `.IMG` / `.CCD` / `.SUB`,
- `.BIN` / `.GDI` / `.RAW`,
- `.CHD`,
- `.CDI`,
- etc.

## Usage

There is some examples below. This applies to all systems with multiple discs / cartridges / floppy discs per game and supports the `.M3U` file extension.

## {.tabset}

### `.BIN` / `.CUE` format

* Create a new text file called `Final Fantasy VII (France).m3u` which contains the following information:

```text
Final Fantasy VII (France) (Disc 1).cue
Final Fantasy VII (France) (Disc 2).cue
Final Fantasy VII (France) (Disc 3).cue
```

### `.CHD` format

* Create a new text file named `Final Fantasy VII (France).m3u` which contains the following information:

```text
Final Fantasy VII (France) (Disc 1).chd
Final Fantasy VII (France) (Disc 2).chd
Final Fantasy VII (France) (Disc 3).chd
```

### `.BIN` / `.IMG` / `.CCD` / `.SUB` format

* Create a new text file named `Final Fantasy VII (France).m3u` which contains the following information:

```text
Final Fantasy VII (France) (Disc 1).ccd
Final Fantasy VII (France) (Disc 2).ccd
Final Fantasy VII (France) (Disc 3).ccd
```

### `.BIN` / `.GDI` / `.RAW` format

* Create a new text file named `Final Fantasy VII (France).m3u` which contains the following information:

```text
Final Fantasy VII (France) (Disc 1).gdi
Final Fantasy VII (France) (Disc 2).gdi
Final Fantasy VII (France) (Disc 3).gdi
```

### `.CDI` format

* Create a new text file named `Grandia II (USA)[Zorlon].m3u` which contains the following information:
 
 ```text
 Grandia II (USA)(CD 1 of 3)[Zorlon].cdi
 Grandia II (USA)(CD 2 of 3)[Zorlon].cdi
 Grandia II (USA)(CD 3 of 3)[Zorlon].cdi
 ```

## Changing discs during the game

If you need to change the disc while the game is running, you have to do the following:

* Open the "virtual" CD tray: Key `HK` + Left Joystick (Up direction)
* Change the disc: `HK` key + Left Joystick (Right or Left direction)
* Close the "virtual" CD tray: `HK` key + Left Joystick (Up Direction)

## The Dolphin case
 
Depending on your operating system (most often Windows), you may see a rejection message like this:
 
![M3U file error message with Dolphin](/tutorials/games/generalities/m3u-dolphin-fr.png){.center}

In this case, open Notepad++ editor ([install it](https://notepad-plus-plus.org/downloads/) before if you don't have it) and open your `.M3U` file. and in the menus go to `Edit` > `Convert Line Breaks` and choose `Convert to UNIX Format (LF)`. Once done, save your file and it should be read correctly by Dolphin.