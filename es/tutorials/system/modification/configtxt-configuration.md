---
title: Modifcar los ficheros de boot de Recalbox
description: 
published: true
date: 2024-08-05T13:38:20.985Z
tags: configuración, inicio, arranque
editor: markdown
dateCreated: 2023-09-14T07:16:55.842Z
---

>Esto no se aplica a las PC.
{.is-info}

## Explicaciones

Este fichero es algo específico. Se utiliza para definir parámetros de arranque para todo el sistema. Se utiliza mucho para ciertas configuraciones.

Estos ficheros se encuentran en el sistema en:

`/boot/config.txt`
`/boot/recalbox-user-config.txt`

>_**NO ES RECOMENDABLE**_ añadir datos al fichero `config.txt` (a menos que se le indique lo contrario), porque se perderán todas las modificaciones en la próxima actualización del sistema. Si necesitáis añadir una configuración aquí, modificad el fichero `recalbox-user-config.txt` el cual no será sobreescrito al actualizar.
{.is-danger}

## Utilización

## {.tabset}

### config.txt

* Conectaros [via SSH](./../../../tutorials/system/access/root-access-terminal-cli)
* Montad la [partición de arranque](./../../../tutorials/system/access/remount-partition-with-write-access) en modo lectura-escritura.
* Modificad el fichero mediante el siguiente comando:

```shell
nano /boot/config.txt
```

### recalbox-user-config.txt

* Conectaros [via SSH](./../../../tutorials/system/access/root-access-terminal-cli)
* Montad la [partición de arranque](./../../../tutorials/system/access/remount-partition-with-write-access) en modo lectura-escritura.
* Modificad el fichero mediante el siguiente comando:

```shell
nano /boot/recalbox-user-config.txt
```

### cmdline.txt

* Conectaros [via SSH](./../../../tutorials/system/access/root-access-terminal-cli)
* Montad la [partition de arranque](./../../../tutorials/system/access/remount-partition-with-write-access) en modo lectura-escritura.
* Modificad el fichero mediante el siguiente comando:

```shell
nano /boot/cmdline.txt
```