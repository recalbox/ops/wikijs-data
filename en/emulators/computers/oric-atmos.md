---
title: Oric Atmos
description: 
published: true
date: 2021-08-09T14:54:23.026Z
tags: computers, tangerine, oric, oric-atmos
editor: markdown
dateCreated: 2021-07-28T21:13:32.618Z
---

![](/emulators/computers/oricatmos.svg){.align-center}

## Technical data

* **Developer**: Tangerine Computer Systems
* **OS**: Tangerine/Microsoft Extended Basic v1.0
* **Year of release**: 1982
* **Units sold**: 210000
* **CPU**: MOS 6502A @ 1 MHz
* **Memory**: 16 KB / 48 KB
* **Storage**: Tape recorder, 300 and 2400 baud
* **Graphics**: 40×28 text characters / 240×200 pixels, 8 colors

## Presentation

The **Oric Atmos** is the second computer of the Oric firm. Marketed from 1984, it takes the base of the Oric-1 by replacing its keyboard by a model offering a more conventional touch allowing a faster input, and by correcting some bugs of its ROM. It will be elected computer of the year in 1984.

The major problem of the Oric-1 was its "semi-mechanical pivot" keyboard. The keyboards of consumer microcomputers were far from matching those of professional models because for cost reasons they usually used a simple plastic membrane (like on the ZX-81) with rubber keys on top. The keyboard of the Oric-1 was called semi-mechanical because its keys were made of rigid plastic, which was an undeniable progress, but it was still less practical than a real mechanical keyboard. The Oric-Atmos solves the first problem of the Oric-1 thanks to its mechanical keyboard. It also solves some bugs in its read-only memory affecting the cassette loading as well as the integrated Basic (the Tangerine Basic produced by Microsoft goes from version 1.0 to 1.1), but which will turn out to be double-edged, as some Oric-1 programs no longer run on the Atmos. Apart from the appearance of the case, the rest of the machine is almost identical.

## Emulators

[Oricutron](oricutron)