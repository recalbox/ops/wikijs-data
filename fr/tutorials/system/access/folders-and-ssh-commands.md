---
title: Dossiers et commandes SSH
description: Quelques commandes bien utiles...
---



Vous trouverez ci-dessous quelques commandes vous permettant d'effectuer quelques actions spécifiques si cela vous est nécessaire.

## Commandes SSH <a id="ssh-commands"></a>

**Vous connecter à votre Recalbox :**

```text
ssh root@[IP address of Recalbox]
```

**Monter la partition de base en mode lecture/écriture :**

```text
mount -o remount,rw / 
```

**Monter la partition de démarrage en lecture/écriture :**

```text
mount -o remount,rw /boot
```

**Arrêter EmulationStation :**

```text
es stop
```

**Démarrer EmulationStation :**

```text
es start
```

**Redémarrer EmulationStation :**

```text
es restart
```

**Changer le mot de passe utilisateur :**

```text
passwd
```

**Redémarrer votre Recalbox :**

```text
reboot
```

**Éteindre votre Recalbox :**

```text
poweroff
```

**Faire une capture d'écran :**

```text
raspi2png
```

Il prendra une capture d'écran et créera un fichier sous `/recalbox/share/screenshots/` sous le nom **screenshot-date-du-screenTheure-du-screen.png**

Vous pouvez également modifier en un nom personnalisé votre capture avec `-p filename.png`

**Informations sur le réseau :**

```text
ifconfig
```

**Vérifiez la température du CPU :**

```text
cat /sys/class/thermal/thermal_zone0/temp  
```

Ensuite, divisez le résultat par 1000 pour obtenir la température en Celsius \(par exemple : 54034 devient 54°C\).

## Fichiers <a id="files"></a>

**Configuration spécifique au Raspberry Pi, comme les paramètres d'overscan, l'overclocking, le mode vidéo par défaut, etc... :**

```text
nano /boot/recalbox-user-config.txt  
```

**Fichier de configuration Recalbox :**

```text
nano /recalbox/share/system/recalbox.conf
```

