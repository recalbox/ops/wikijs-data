---
title: Play Singe 2 games
description: 
published: true
date: 2024-09-17T15:58:38.059Z
tags: daphne, 8.1+, singe 2
editor: markdown
dateCreated: 2022-07-22T19:38:49.201Z
---

## What are Singe 2 Games?

The term `SINGE` (from the name of the dragon in Dragon's Lair) is the name of an extension for the Daphne emulator that allows anyone to create their own FMV: (full motion video).

Compared to version 1, Singe 2 games have some advantages:

* Any resolution/format for games (Daphne/singe was limited to m2v and was experimental at 1080p)
* Adaptable overlay (Daphne Singe was limited)
* No maximum length limitation (was 65536 frames)
* Better controls (all buttons of the pad usable)

## List of Singe 2 games

Here is the list of Singe 2 games:

* A Nightmare on Elm Street
* Altered Carbon
* Arcade Xperience Vol. 1
* Arcade Xperience Vol. 2
* Arcade Xperience Vol. 3
* Asterix
* Astroboy
* Chantze's Stone HD
* Cliff Hanger
* Conan: the Boy in the Future
* Cracke
* Crime Patrol HD
* Daitarn 3
* Danmachi
* Dragon's Lair II: Time Warp
* Dragon's Lair TV Show
* Dragon Trainer
* Drug Wars HD
* Esh's Arunmilla
* Fire and Ice
* Freedom Fighter
* Friday the 13th
* MadDog McCree HD
* MadDog II HD
* Ninja Hayate HD
* Œil pour œil
* Platoon (Prototype)
* Princess Mononoke
* Puss in Boots
* Rollercoaster
* Samurai Jack
* Scrat
* Secret of Nimh
* Space Pirates HD
* Star Blazers
* Starship Troopers
* Sucker Punch
* Sugar Rush
* Survival
* The Last Bounty Hunter HD
* Time Gal HD
* Titan AE
* Triad Stone HD
* Triad Stone
* Tron
* Venus Wars
* Who Shot Johnny Rock? HD (Enhanced)
* Who Shot Johnny Rock? HD

## Games installation

Installation of Singe 2 games is very easy and is described for each game below.

Since Recalbox 9.2, most of the Singe v2 games (those with `Cfg` / `Fonts` / `Overlay` / `Script` / `Sounds` / `Video` sub-directories) are using now a common framework. This framework must be placed in `/recalbox/share/roms/daphne/Framework`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 Framework
┃ ┃ ┃ ┃ ┃ ┣ 📁 Fonts
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m39.ttf
┃ ┃ ┃ ┃ ┃ ┣ 🗒 framework.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 globals.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 hscore.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 main.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 service.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 toolbox.singe

You can find that framework [here](https://github.com/DirtBagXon/hypseus_singe_data/tree/master/00-singe2/Framework) with non-copyrighted files for your games (no `.M2V` or `.OGG`) on the same repository.

## Location of files per game

Since it is particularly difficult to place the right files in the right places, you will see below how to put the necessary files for each game in the Recalbox tree.

>Everything below is an example of working romset, yours can work with some differences.
{.is-info}

## {.tabset}

### A-E

#### A Nightmare on Elm Street
 
┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 elmstreet.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 elmstreet.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 elmstreet.txt
┃ ┃ ┃ ┃ ┃ ┣ 📁 Cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 default.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 game.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hscore.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s1.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s2.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s3.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s4.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s5.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s6.cfg
┃ ┃ ┃ ┃ ┃ ┣ 📁 Fonts
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 font.ttf
┃ ┃ ┃ ┃ ┃ ┣ 📁 Overlay
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action2.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdl.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdr.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowright.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel1.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel2.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowul.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowup.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowur.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barnone.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 bonus.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 buttons.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 death.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk1.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk2.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk3.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 diskA.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 freeplay.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 getready.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hold.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hright.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hup.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 insertcoin.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 live.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 📁 Lores
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdl.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdr.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowright.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowul.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowup.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowur.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barnone.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 bonus.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 buttons.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 death.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk3.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 diskA.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 freeplay.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 getready.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hold.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hright.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hup.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 insertcoin.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 live.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m0.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m3.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m4.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m5.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m6.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m7.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mash.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 minus.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 nomoves.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num00.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num01.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num02.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num03.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num04.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num05.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num06.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num07.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num08.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num09.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 ok.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 perfect.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 scene.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 score.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 secret.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 shoot.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 skip.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 special.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 stick.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tbronze.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tgold.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 time.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tno.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 topscore.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 total.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tplat.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tsilver.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m0.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m1.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m2.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m3.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m4.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m5.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m6.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m7.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mash.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 📁 Minimal
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdl.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdr.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowright.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowul.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowup.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowur.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barnone.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 bonus.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 buttons.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 death.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk3.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 diskA.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 freeplay.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 getready.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hold.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hright.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hup.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 insertcoin.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 live.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m0.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m3.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m4.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m5.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m6.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m7.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mash.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 minus.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 nomoves.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num00.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num01.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num02.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num03.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num04.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num05.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num06.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num07.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num08.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num09.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 perfect.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 scene.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 score.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 skip.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 special.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 stick.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tbronze.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tgold.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tno.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 topscore.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 total.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tplat.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tsilver.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 minus.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 nomoves.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num00.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num01.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num02.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num03.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num04.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num05.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num06.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num07.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num08.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num09.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 ok.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 perfect.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 scene.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 score.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 secret.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 shoot.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 skip.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 special.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 stick.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tbronze.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tgold.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 time.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tno.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 topscore.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 total.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tplat.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tsilver.png
┃ ┃ ┃ ┃ ┃ ┣ 📁 Script
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 addons.singe
┃ ┃ ┃ ┃ ┃ ┣ 📁 Sounds
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 clear.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 coin.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 death.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 music.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 music2.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 right.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 roll.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 victory.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 wrong.wav
┃ ┃ ┃ ┃ ┃ ┣ 📁 Video
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 freddy.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 freddy.ogg

#### Altered Carbon

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 carbon.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 carbon.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 carbon.txt
┃ ┃ ┃ ┃ ┃ ┣ 📁 Cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 default.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 game.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hscore.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s1.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s2.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s3.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s4.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s5.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s6.cfg
┃ ┃ ┃ ┃ ┃ ┣ 📁 Fonts
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 font.ttf
┃ ┃ ┃ ┃ ┃ ┣ 📁 Overlay
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdl.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdr.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowright.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel1.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel2.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowul.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowup.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowur.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barnone.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 bonus.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 buttons.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 death.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk1.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk2.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk3.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 diskA.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 freeplay.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 getready.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hold.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hright.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hup.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 insertcoin.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 live.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 📁 Lores
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdl.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdr.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowright.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowul.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowup.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowur.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barnone.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 bonus.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 buttons.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 death.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk3.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 diskA.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 freeplay.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 getready.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hold.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hright.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hup.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 insertcoin.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 live.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m0.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m3.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m4.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m5.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m6.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m7.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mash.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 minus.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 nomoves.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num00.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num01.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num02.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num03.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num04.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num05.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num06.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num07.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num08.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num09.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 perfect.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 scene.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 score.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 skip.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 special.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 stick.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tbronze.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tgold.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tno.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 topscore.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 total.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tplat.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tsilver.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m0.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m1.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m2.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m3.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m4.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m5.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m6.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m7.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mash.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 📁 Minimal
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action3.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action4.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdl.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdr.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowright.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowul.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowup.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowur.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barnone.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 bonus.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 buttons.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 death.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk3.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 diskA.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 freeplay.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 getready.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hold.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hright.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hup.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 insertcoin.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 live.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m0.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m3.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m4.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m5.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m6.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m7.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mash.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 minus.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 nomoves.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num00.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num01.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num02.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num03.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num04.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num05.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num06.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num07.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num08.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num09.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 ok.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 perfect.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 scene.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 score.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 shoot.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 skip.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 special.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 stick.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tbronze.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tgold.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tno.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 topscore.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 total.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tplat.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tsilver.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 minus.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 nomoves.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num00.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num01.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num02.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num03.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num04.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num05.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num06.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num07.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num08.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num09.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 perfect.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 scene.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 score.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 skip.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 special.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 stick.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tbronze.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tgold.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tno.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 topscore.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 total.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tplat.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tsilver.png
┃ ┃ ┃ ┃ ┃ ┣ 📁 Script
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 addons.singe
┃ ┃ ┃ ┃ ┃ ┣ 📁 Sounds
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 clear.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 coin.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 death.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 right.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 roll.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 victory.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 wrong.wav
┃ ┃ ┃ ┃ ┃ ┣ 📁 Video
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 carbon.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 carbon.ogg

#### Arcade Xperience Vol. 1

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 arcadex1.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 arcadex1.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 arcadex1.txt
┃ ┃ ┃ ┃ ┃ ┣ 📁 Cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 default.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 game.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hscore.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s1.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s2.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s3.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s4.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s5.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s6.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s10.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s20.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s30.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s40.cfg
┃ ┃ ┃ ┃ ┃ ┣ 📁 Fonts
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 font.ttf
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 fontlo.ttf
┃ ┃ ┃ ┃ ┃ ┣ 📁 Overlay
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action2.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action3.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action4.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdl.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdr.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowright.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel1.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel2.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowul.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowup.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowur.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barnone.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 bonus.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 buttons.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 death.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk1.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk2.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk3.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 diskA.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 freeplay.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 getready.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hold.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hright.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hup.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 insertcoin.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 live.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 loopl.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 loopr.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 📁 Lores
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action3.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action4.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdl.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdr.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowright.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowul.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowup.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowur.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barnone.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 bonus.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 buttons.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 death.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk3.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 diskA.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 freeplay.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 getready.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hold.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hright.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hup.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 insertcoin.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 live.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 loopl.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 loopr.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m0.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m3.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m4.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m5.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m6.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m7.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mash.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 minus.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 nomoves.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num00.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num01.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num02.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num03.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num04.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num05.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num06.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num07.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num08.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num09.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 perfect.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 scene.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 score.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 shoot.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 skip.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 special.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 stick.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tbronze.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tgold.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tno.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 topscore.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 total.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tplat.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tsilver.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m0.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m1.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m2.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m3.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m4.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m5.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m6.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m7.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mash.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 📁 Minimal
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action3.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action4.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdl.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdr.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowright.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowul.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowup.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowur.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barnone.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 bonus.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 buttons.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 death.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk3.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 diskA.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 freeplay.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 getready.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hold.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hright.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hup.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 insertcoin.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 live.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 loopl.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 loopr.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m0.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m3.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m4.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m5.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m6.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m7.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mash.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 minus.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 nomoves.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num00.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num01.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num02.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num03.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num04.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num05.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num06.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num07.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num08.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num09.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 perfect.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 scene.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 score.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 shoot.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 skip.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 special.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 stick.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tbronze.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tgold.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tno.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 topscore.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 total.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tplat.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tsilver.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 minus.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 nomoves.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num00.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num01.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num02.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num03.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num04.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num05.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num06.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num07.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num08.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num09.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 ok.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 perfect.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 scene.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 score.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 shoot.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 skip.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 special.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 stick.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tbronze.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tgold.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 time.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tno.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 topscore.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 total.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tplat.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tsilver.png
┃ ┃ ┃ ┃ ┃ ┣ 📁 Script
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 addons.singe
┃ ┃ ┃ ┃ ┃ ┣ 📁 Sounds
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 clear.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 coin.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 death.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 right.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 roll.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 victory.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 wrong.wav
┃ ┃ ┃ ┃ ┃ ┣ 📁 Video
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arcadex1.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arcadex1.ogg

#### Arcade Xperience Vol. 2

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 arcadex2.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 arcadex2.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 arcadex2.txt
┃ ┃ ┃ ┃ ┃ ┣ 📁 Cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 default.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 game.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hscore.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s1.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s2.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s3.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s4.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s5.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s6.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s10.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s20.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s30.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s40.cfg
┃ ┃ ┃ ┃ ┃ ┣ 📁 Fonts
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 font.ttf
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 fontlo.ttf
┃ ┃ ┃ ┃ ┃ ┣ 📁 Overlay
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action2.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action3.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action4.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdl.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdr.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowright.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel1.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel2.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowul.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowup.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowur.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barnone.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 bonus.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 buttons.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 death.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk1.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk2.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk3.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 diskA.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 freeplay.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 getready.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hold.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hright.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hup.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 insertcoin.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 live.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 loopl.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 loopr.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 📁 Lores
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action3.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action4.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdl.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdr.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowright.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowul.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowup.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowur.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barnone.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 bonus.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 buttons.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 death.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk3.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 diskA.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 freeplay.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 getready.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hold.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hright.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hup.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 insertcoin.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 live.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 loopl.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 loopr.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m0.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m3.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m4.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m5.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m6.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m7.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mash.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 minus.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 nomoves.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num00.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num01.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num02.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num03.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num04.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num05.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num06.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num07.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num08.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num09.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 perfect.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 scene.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 score.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 shoot.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 skip.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 special.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 stick.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tbronze.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tgold.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tno.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 topscore.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 total.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tplat.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tsilver.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m0.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m1.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m2.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m3.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m4.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m5.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m6.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m7.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mash.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 📁 Minimal
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action3.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action4.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdl.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdr.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowright.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowul.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowup.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowur.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barnone.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 bonus.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 buttons.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 death.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk3.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 diskA.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 freeplay.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 getready.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hold.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hright.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hup.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 insertcoin.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 live.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 loopl.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 loopr.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m0.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m3.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m4.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m5.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m6.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m7.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mash.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 minus.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 nomoves.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num00.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num01.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num02.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num03.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num04.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num05.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num06.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num07.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num08.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num09.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 perfect.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 scene.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 score.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 shoot.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 skip.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 special.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 stick.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tbronze.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tgold.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tno.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 topscore.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 total.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tplat.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tsilver.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 minus.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 nomoves.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num00.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num01.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num02.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num03.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num04.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num05.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num06.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num07.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num08.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num09.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 ok.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 perfect.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 scene.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 score.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 shoot.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 skip.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 special.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 stick.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tbronze.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tgold.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 time.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tno.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 topscore.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 total.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tplat.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tsilver.png
┃ ┃ ┃ ┃ ┃ ┣ 📁 Script
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 addons.singe
┃ ┃ ┃ ┃ ┃ ┣ 📁 Sounds
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 clear.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 coin.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 death.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 right.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 roll.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 victory.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 wrong.wav
┃ ┃ ┃ ┃ ┃ ┣ 📁 Video
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arcadex2.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arcadex2.ogg

#### Arcade Xperience Vol. 3

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 arcadex3.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 arcadex3.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 arcadex3.txt
┃ ┃ ┃ ┃ ┃ ┣ 📁 Cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 default.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 game.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hscore.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s1.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s2.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s3.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s4.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s5.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s6.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s10.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s20.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s30.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s40.cfg
┃ ┃ ┃ ┃ ┃ ┣ 📁 Fonts
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 font.ttf
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 fontlo.ttf
┃ ┃ ┃ ┃ ┃ ┣ 📁 Overlay
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action2.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action3.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action4.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdl.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdr.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowright.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel1.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel2.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowul.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowup.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowur.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barnone.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 bonus.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 buttons.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 death.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk1.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk2.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk3.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 diskA.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 freeplay.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 getready.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hold.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hright.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hup.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 insertcoin.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 live.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 loopl.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 loopr.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 📁 Lores
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action3.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action4.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdl.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdr.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowright.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowul.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowup.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowur.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barnone.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 bonus.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 buttons.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 death.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk3.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 diskA.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 freeplay.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 getready.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hold.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hright.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hup.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 insertcoin.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 live.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 loopl.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 loopr.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m0.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m3.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m4.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m5.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m6.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m7.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mash.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 minus.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 nomoves.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num00.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num01.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num02.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num03.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num04.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num05.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num06.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num07.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num08.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num09.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 perfect.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 scene.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 score.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 shoot.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 skip.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 special.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 stick.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tbronze.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tgold.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tno.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 topscore.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 total.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tplat.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tsilver.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m0.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m1.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m2.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m3.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m4.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m5.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m6.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m7.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mash.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 📁 Minimal
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action3.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action4.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdl.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdr.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowright.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowul.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowup.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowur.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barnone.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 bonus.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 buttons.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 death.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk3.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 diskA.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 freeplay.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 getready.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hold.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hright.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hup.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 insertcoin.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 live.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 loopl.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 loopr.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m0.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m3.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m4.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m5.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m6.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m7.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mash.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 minus.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 nomoves.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num00.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num01.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num02.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num03.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num04.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num05.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num06.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num07.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num08.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num09.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 perfect.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 scene.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 score.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 shoot.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 skip.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 special.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 stick.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tbronze.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tgold.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tno.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 topscore.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 total.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tplat.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tsilver.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 minus.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 nomoves.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num00.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num01.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num02.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num03.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num04.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num05.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num06.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num07.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num08.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num09.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 ok.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 perfect.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 scene.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 score.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 shoot.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 skip.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 special.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 stick.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tbronze.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tgold.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 time.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tno.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 topscore.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 total.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tplat.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tsilver.png
┃ ┃ ┃ ┃ ┃ ┣ 📁 Script
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 addons.singe
┃ ┃ ┃ ┃ ┃ ┣ 📁 Sounds
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 clear.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 coin.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 death.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 right.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 roll.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 victory.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 wrong.wav
┃ ┃ ┃ ┃ ┃ ┣ 📁 Video
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arcadex3.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arcadex3.ogg

#### Asterix

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 asterix.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 asterix.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 asterix.txt
┃ ┃ ┃ ┃ ┃ ┣ 📁 Cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 default.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 game.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hscore.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s1.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s2.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s3.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s4.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s5.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s6.cfg
┃ ┃ ┃ ┃ ┃ ┣ 📁 Fonts
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 font.ttf
┃ ┃ ┃ ┃ ┃ ┣ 📁 Overlay
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowright.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel1.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel2.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowup.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barnone.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 bonus.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 buttons.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 death.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk1.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk2.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk3.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 diskA.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 freeplay.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 insertcoin.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 live.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 📁 Lores
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowright.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowup.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barnone.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 bonus.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 buttons.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 death.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk3.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 diskA.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 freeplay.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 insertcoin.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 live.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m0.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m3.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m4.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m5.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m6.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m7.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mash.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 minus.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 nomoves.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num00.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num01.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num02.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num03.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num04.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num05.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num06.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num07.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num08.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num09.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 perfect.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 scene.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 score.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 skip.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 special.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 stick.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 topscore.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 total.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m0.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m1.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m2.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m3.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m4.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m5.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m6.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m7.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mash.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 📁 Minimal
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action3.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action4.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdl.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdr.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowright.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowul.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowup.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowur.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barnone.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 bonus.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 buttons.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 death.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk3.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 diskA.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 freeplay.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 getready.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hold.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hright.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hup.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 insertcoin.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 live.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m0.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m3.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m4.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m5.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m6.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m7.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mash.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 minus.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 nomoves.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num00.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num01.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num02.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num03.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num04.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num05.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num06.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num07.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num08.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num09.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 ok.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 perfect.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 scene.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 score.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 shoot.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 skip.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 special.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 stick.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tbronze.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tgold.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tno.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 topscore.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 total.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tplat.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tsilver.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 minus.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 nomoves.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num00.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num01.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num02.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num03.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num04.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num05.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num06.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num07.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num08.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num09.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 perfect.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 scene.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 score.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 skip.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 special.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 stick.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 topscore.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 total.png
┃ ┃ ┃ ┃ ┃ ┣ 📁 Script
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 addons.singe
┃ ┃ ┃ ┃ ┃ ┣ 📁 Sounds
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 clear.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 coin.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 death.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 music.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 music2.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 right.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 roll.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 victory.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 wrong.wav
┃ ┃ ┃ ┃ ┃ ┣ 📁 Video
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 asterix.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 asterix.ogg

#### Astroboy

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 astroboy.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 astroboy.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 astroboy.txt
┃ ┃ ┃ ┃ ┃ ┣ 📁 Cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 default.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 game.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hscore.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s1.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s2.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s3.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s4.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s5.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s6.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s10.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s20.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s30.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s40.cfg
┃ ┃ ┃ ┃ ┃ ┣ 📁 Fonts
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 font.ttf
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 fontlo.ttf
┃ ┃ ┃ ┃ ┃ ┣ 📁 Overlay
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action2.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action3.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action4.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdl.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdr.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowright.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel1.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel2.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowul.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowup.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowur.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barnone.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 bonus.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 buttons.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 death.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk1.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk2.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk3.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 diskA.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 freeplay.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 getready.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hold.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hright.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hup.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 insertcoin.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 live.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 📁 Lores
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdl.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdr.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowright.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowul.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowup.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowur.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barnone.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 bonus.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 buttons.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 death.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk3.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 diskA.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 freeplay.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 getready.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hold.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hright.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hup.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 insertcoin.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 live.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m0.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m3.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m4.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m5.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m6.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m7.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mash.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 minus.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 nomoves.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num00.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num01.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num02.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num03.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num04.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num05.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num06.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num07.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num08.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num09.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 perfect.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 scene.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 score.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 shoot.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 skip.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 special.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 stick.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tbronze.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tgold.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tno.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 topscore.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 total.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tplat.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tsilver.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m0.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m1.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m2.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m3.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m4.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m5.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m6.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m7.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mash.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 📁 Minimal
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action3.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action4.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdl.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdr.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowright.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowul.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowup.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowur.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barnone.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 bonus.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 buttons.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 death.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk3.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 diskA.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 freeplay.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 getready.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hold.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hright.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hup.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 insertcoin.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 live.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 loopl.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 loopr.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m0.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m3.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m4.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m5.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m6.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m7.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mash.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 minus.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 nomoves.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num00.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num01.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num02.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num03.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num04.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num05.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num06.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num07.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num08.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num09.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 perfect.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 scene.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 score.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 shoot.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 skip.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 special.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 stick.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tbronze.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tgold.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tno.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 topscore.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 total.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tplat.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tsilver.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 minus.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 nomoves.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num00.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num01.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num02.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num03.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num04.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num05.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num06.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num07.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num08.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num09.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 perfect.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 scene.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 score.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 shoot.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 skip.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 special.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 stick.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tbronze.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tgold.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tno.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 topscore.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 total.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tplat.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tsilver.png
┃ ┃ ┃ ┃ ┃ ┣ 📁 Script
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 addons.singe
┃ ┃ ┃ ┃ ┃ ┣ 📁 Sounds
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 clear.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 coin.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 death.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 right.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 roll.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 victory.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 wrong.wav
┃ ┃ ┃ ┃ ┃ ┣ 📁 Video
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 astroboy.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 astroboy.ogg

#### Chantze's Stone HD

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 chantze-hd.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 chantze-hd.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 chantze-hd.txt
┃ ┃ ┃ ┃ ┃ ┣ 🗒 triad-hd.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 triad-hd.txt
┃ ┃ ┃ ┃ ┃ ┣ 📁 Cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 default.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 default2.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 game.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 game2.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hscore.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hscore2.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s1.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s2.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s3.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s4.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s5.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s6.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s10.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s20.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s30.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s40.cfg
┃ ┃ ┃ ┃ ┃ ┣ 📁 Fonts
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 font.ttf
┃ ┃ ┃ ┃ ┃ ┣ 📁 Overlay
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowright.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel1.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel2.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowup.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barnone.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 bonus.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 buttons.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 death.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk1.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk2.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk3.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 diskA.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 freeplay.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 insertcoin.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 live.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 📁 Lores
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowright.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowup.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barnone.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 bonus.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 buttons.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 death.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk3.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 diskA.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 freeplay.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 insertcoin.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 live.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m0.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m3.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m4.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m5.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m6.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m7.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mash.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 minus.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 nomoves.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num00.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num01.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num02.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num03.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num04.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num05.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num06.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num07.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num08.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num09.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 ok.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 perfect.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 scanlines.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 scene.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 score.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 skip.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 special.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 stick.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 topscore.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 total.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m0.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m1.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m2.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m3.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m4.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m5.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m6.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m7.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mash.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 📁 Minimal
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowright.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowup.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barnone.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 bonus.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 buttons.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 chantze.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 death.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk3.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 diskA.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 freeplay.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 insertcoin.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 live.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m0.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m3.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m4.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m5.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m6.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m7.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mash.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 minus.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 nomoves.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num00.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num01.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num02.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num03.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num04.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num05.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num06.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num07.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num08.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num09.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 ok.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 perfect.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 scanlines.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 scene.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 score.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 skip.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 special.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 stick.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 topscore.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 total.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 minus.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 nomoves.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num00.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num01.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num02.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num03.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num04.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num05.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num06.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num07.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num08.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num09.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 ok.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 perfect.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 scanlines.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 scene.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 score.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 skip.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 special.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 stick.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 topscore.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 total.png
┃ ┃ ┃ ┃ ┃ ┣ 📁 Script
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 addons.singe
┃ ┃ ┃ ┃ ┃ ┣ 📁 Sounds
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 clear.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 coin.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 death.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 music1.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 music2.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 right.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 roll.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 victory.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 wrong.wav
┃ ┃ ┃ ┃ ┃ ┣ 📁 Video
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 chantze.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 chantze.ogg

#### Cliff Hanger

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 cliff.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 cliff.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 cliff.txt
┃ ┃ ┃ ┃ ┃ ┣ 📁 Cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 default.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 game.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hscore.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s1.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s2.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s3.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s4.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s5.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s6.cfg
┃ ┃ ┃ ┃ ┃ ┣ 📁 Fonts
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 font.ttf
┃ ┃ ┃ ┃ ┃ ┣ 📁 Overlay
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action2.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowright.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel1.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel2.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowup.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barnone.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 bonus.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 buttons.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 death.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk1.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk2.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk3.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 diskA.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 freeplay.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 getready.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 insertcoin.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 live.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 📁 Lores
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowright.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowup.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barnone.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 bonus.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 buttons.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 death.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk3.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 diskA.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 freeplay.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 getready.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 insertcoin.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 live.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 minus.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 nomoves.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num00.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num01.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num02.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num03.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num04.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num05.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num06.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num07.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num08.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num09.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 perfect.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 scene.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 score.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 skip.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 special.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 stick.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 topscore.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 total.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 📁 Minimal
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action3.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action4.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdl.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdr.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowright.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowul.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowup.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowur.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barnone.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 bonus.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 buttons.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 death.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk3.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 diskA.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 feet.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 freeplay.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 getready.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hold.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hright.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hup.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 insertcoin.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 live.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m0.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m3.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m4.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m5.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m6.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m7.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mash.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 minus.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 nomoves.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num00.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num01.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num02.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num03.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num04.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num05.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num06.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num07.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num08.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num09.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 ok.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 perfect.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 scene.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 score.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 secret.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 shoot.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 skip.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 special.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 stick.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tbronze.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tgold.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tno.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 topscore.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 total.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tplat.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tsilver.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 minus.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 nomoves.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num00.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num01.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num02.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num03.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num04.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num05.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num06.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num07.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num08.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num09.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 perfect.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 scene.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 score.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 skip.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 special.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 stick.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 topscore.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 total.png
┃ ┃ ┃ ┃ ┃ ┣ 📁 Script
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 addons.singe
┃ ┃ ┃ ┃ ┃ ┣ 📁 Sounds
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 clear.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 coin.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 death.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 music.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 music2.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 right.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 roll.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 victory.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 wrong.wav
┃ ┃ ┃ ┃ ┃ ┣ 📁 Video
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 cliff.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 cliff.ogg

#### Conan: the Boy in the Future

>This game doesn't use the framework described on top of this page.
{.is-info}

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 conan.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 conan.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 conan.txt
┃ ┃ ┃ ┃ ┃ ┣ 📁 Cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 game.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 save.cfg
┃ ┃ ┃ ┃ ┃ ┣ 📁 Fonts
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 font.ttf
┃ ┃ ┃ ┃ ┃ ┣ 📁 Overlay
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdl.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdr.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowright.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel2.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowul.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowup.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowur.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 bonus.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 complete.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk1.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk2.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk3.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 diskA.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 freeplay.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 getready.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 insertcoin.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 live.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m0.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m1.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m2.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m3.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m4.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m5.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m6.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mash.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 nomoves.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num00.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num01.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num02.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num03.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num04.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num05.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num06.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num07.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num08.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num09.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 ok.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 scene.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 score.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 secret.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 skip.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 topscore.png
┃ ┃ ┃ ┃ ┃ ┣ 📁 Script
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 globals.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hscore.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 main.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 map.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 service.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 toolbox.singe
┃ ┃ ┃ ┃ ┃ ┣ 📁 Sounds
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 clear.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 coin.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 death.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 right.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 victory.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 wrong.wav
┃ ┃ ┃ ┃ ┃ ┣ 📁 Video
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 conan.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 conan.ogg

#### Cracke

>This game doesn't use the framework described on top of this page.
{.is-info}

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 cracke.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 action.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 action2.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 action3.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 action4.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 addons.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdl.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdown.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdr.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowleft.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowright.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel2.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowul.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowup.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowur.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 barfull.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 barnone.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 bonus.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 buttons.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 clear.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 coin.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 cracke.m2v
┃ ┃ ┃ ┃ ┃ ┣ 🗒 cracke.ogg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 cracke.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 cracke.txt
┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 death.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 death.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 default.cfg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk1.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk2.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk3.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 diskA.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 font.ttf
┃ ┃ ┃ ┃ ┃ ┣ 🗒 framework.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 freeplay_alt.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 freeplay.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 game.cfg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 getready.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 globals.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 hdown.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 hleft.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 hold.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 hright.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 hscore.cfg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 hscore.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 hup.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 insertcoin.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 level.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 live.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 m0.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 m1.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 m2.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 m3.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 m4.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 m5.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 m6.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 m7.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 main.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 mash.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 mfull.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 minus.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 nomoves.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 num00.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 num01.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 num02.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 num03.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 num04.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 num05.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 num06.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 num07.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 num08.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 num09.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 ok.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 perfect.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 right.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 roll.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 s1.cfg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 s2.cfg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 s3.cfg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 s4.cfg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 s5.cfg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 s6.cfg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 save.cfg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 scene.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 score.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 secret.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 service.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 shoot.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 skip.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 special.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 stick.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 tbronze.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 tgold.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 tno.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 toolbox.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 topscore.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 total.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 tplat.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 tsilver.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 victory.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 wrong.wav

#### Crime Patrol HD

>This game doesn't use the framework described on top of this page.
{.is-info}

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 crimepatrol-hd.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 crimepatrol-hd.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 crimepatrol-hd.txt
┃ ┃ ┃ ┃ ┃ ┣ 📁 Cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 crimepatrol.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 crimepatrol.cfg.def
┃ ┃ ┃ ┃ ┃ ┣ 📁 Fonts
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 default.ttf
┃ ┃ ┃ ┃ ┃ ┣ 📁 Overlay
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 badge3.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 badge4.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 bullets00.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 bullets01.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 bullets02.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 bullets03.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 bullets04.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 bullets05.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 bullets06.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 bullets07.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 bullets08.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 bullets09.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 bullets10.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 bullets11.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 bullets12.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 crosshaira.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 crosshairb.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 crosshairc.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 crosshaird.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 crosshaire.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hit.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hit1.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hit2.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hit3.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hit4.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hit5.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num0.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num1.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num2.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num3.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num4.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num5.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num6.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num7.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num8.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num9.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 pistol.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 shot.png
┃ ┃ ┃ ┃ ┃ ┣ 📁 Script
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 globals.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitbox-airdrugs.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitbox-airports.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitbox-bankrobbery.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitbox-cars.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitbox-druglab1.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitbox-druglab2.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitbox-gang.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitbox-hangar.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitbox-nuclearplant.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitbox-store.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitbox-stripbar.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitbox-warehouse.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitmap-delta.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitmap-practice.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitmap-rookie.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitmap-swat.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitmap-undercover.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hscore.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 manage.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 service.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 setuplevel.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 toolbox.singe
┃ ┃ ┃ ┃ ┃ ┣ 📁 Sounds
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 clear.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 coin.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 death.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 music.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 music2.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 right.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 roll.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 victory.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 wrong.wav
┃ ┃ ┃ ┃ ┃ ┣ 📁 Video
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 crimepatrol.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 crimepatrol.ogg

#### Daitarn 3

>This game doesn't use the framework described on top of this page.
{.is-info}

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 daitarn.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 daitarn.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 daitarn.txt
┃ ┃ ┃ ┃ ┃ ┣ 📁 Cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 game.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 save.cfg
┃ ┃ ┃ ┃ ┃ ┣ 📁 Fonts
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 font.ttf
┃ ┃ ┃ ┃ ┃ ┣ 📁 Overlay
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdl.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdr.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowright.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel2.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowul.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowup.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowur.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 bonus.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 complete.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk1.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk2.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk3.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 diskA.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 freeplay.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 getready.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 insertcoin.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 live.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m0.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m1.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m2.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m3.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m4.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m5.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m6.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mash.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 nomoves.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num00.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num01.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num02.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num03.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num04.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num05.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num06.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num07.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num08.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num09.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 ok.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 scene.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 score.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 secret.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 skip.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 topscore.png
┃ ┃ ┃ ┃ ┃ ┣ 📁 Script
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 globals.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hscore.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 main.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 map.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 save.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 service.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 toolbox.singe
┃ ┃ ┃ ┃ ┃ ┣ 📁 Sounds
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 clear.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 coin.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 death.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 right.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 victory.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 wrong.wav
┃ ┃ ┃ ┃ ┃ ┣ 📁 Video
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 daitarn3.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 daitarn3.wav

#### Danmachi

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 danmachi.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 Danmachi.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 Danmachi.txt
┃ ┃ ┃ ┃ ┃ ┣ 📁 Cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 default.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 default2.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 game.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 game2.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hscore.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hscore2.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s1.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s2.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s3.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s4.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s5.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s6.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s10.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s20.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s30.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s40.cfg
┃ ┃ ┃ ┃ ┃ ┣ 📁 Fonts
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 font.ttf
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 fontlo.ttf
┃ ┃ ┃ ┃ ┃ ┣ 📁 Overlay
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action2.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action3.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action4.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdl.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdr.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowright.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel1.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel2.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowul.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowup.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowur.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barnone.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 bonus.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 buttons.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 death.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk1.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk2.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk3.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 diskA.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 freeplay.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 getready.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hold.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hright.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hup.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 insertcoin.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 live.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 loopl.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 loopr.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 📁 Lores
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action3.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action4.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdl.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdr.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowright.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowul.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowup.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowur.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barnone.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 bonus.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 buttons.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 death.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk3.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 diskA.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 freeplay.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 getready.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hold.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hright.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hup.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 insertcoin.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 live.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 loopl.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 loopr.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m0.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m3.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m4.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m5.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m6.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m7.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mash.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 minus.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 nomoves.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num00.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num01.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num02.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num03.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num04.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num05.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num06.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num07.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num08.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num09.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 ok.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 perfect.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 scene.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 score.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 shoot.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 skip.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 special.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 stick.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tbronze.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tgold.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 time.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tno.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 topscore.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 total.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tplat.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tsilver.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m0.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m1.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m2.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m3.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m4.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m5.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m6.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m7.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mash.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 📁 Minimal
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action3.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action4.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdl.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdr.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowright.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowul.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowup.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowur.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barnone.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 bonus.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 buttons.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 death.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk3.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 diskA.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 freeplay.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 getready.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hold.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hright.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hup.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 insertcoin.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 live.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 loopl.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 loopr.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m0.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m3.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m4.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m5.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m6.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m7.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mash.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 minus.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 nomoves.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num00.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num01.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num02.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num03.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num04.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num05.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num06.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num07.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num08.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num09.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 perfect.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 scene.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 score.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 shoot.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 skip.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 special.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 stick.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tbronze.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tgold.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tno.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 topscore.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 total.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tplat.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tsilver.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 minus.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 nomoves.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num00.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num01.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num02.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num03.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num04.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num05.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num06.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num07.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num08.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num09.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 ok.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 perfect.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 scene.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 score.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 shoot.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 skip.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 special.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 stick.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tbronze.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tgold.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 time.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tno.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 topscore.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 total.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tplat.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tsilver.png
┃ ┃ ┃ ┃ ┃ ┣ 📁 Script
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 addons.singe
┃ ┃ ┃ ┃ ┃ ┣ 📁 Sounds
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 clear.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 coin.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 death.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 right.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 roll.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 victory.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 wrong.wav
┃ ┃ ┃ ┃ ┃ ┣ 📁 Video
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 Danmachi.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 Danmachi.ogg

#### Dragon's Lair II: Time Warp

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 dl2e.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 dl2e.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 dl2e.txt
┃ ┃ ┃ ┃ ┃ ┣ 📁 Cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 default.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 game.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hscore.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s1.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s1p.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s2.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s2p.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s3.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s3p.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s4.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s4p.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s5.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s5p.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s6.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s6p.cfg
┃ ┃ ┃ ┃ ┃ ┣ 📁 Fonts
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 font.ttf
┃ ┃ ┃ ┃ ┃ ┣ 📁 Overlay
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowright.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel1.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel2.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowup.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barnone.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 bonus.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 buttons.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 death.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk1.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk2.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk3.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 diskA.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 freeplay.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 insertcoin.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 live.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 📁 Lores
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowright.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowup.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barnone.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 bonus.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 buttons.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 death.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk3.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 diskA.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 freeplay.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 insertcoin.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 live.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 minus.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 nomoves.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num00.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num01.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num02.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num03.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num04.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num05.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num06.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num07.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num08.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num09.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 perfect.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 scene.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 score.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 skip.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 special.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 stick.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 topscore.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 total.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 📁 Minimal
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowright.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowup.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barnone.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 bonus.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 buttons.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 death.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk3.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 diskA.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 freeplay.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 getready.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 insertcoin.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 live.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 minus.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 nomoves.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num00.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num01.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num02.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num03.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num04.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num05.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num06.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num07.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num08.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num09.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 perfect.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 scene.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 score.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 skip.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 special.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 stick.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 topscore.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 total.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 minus.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 nomoves.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num00.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num01.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num02.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num03.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num04.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num05.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num06.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num07.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num08.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num09.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 perfect.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 scene.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 score.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 skip.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 special.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 stick.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 topscore.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 total.png
┃ ┃ ┃ ┃ ┃ ┣ 📁 Script
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 addons.singe
┃ ┃ ┃ ┃ ┃ ┣ 📁 Sounds
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 clear.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 coin.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 death.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 prompt.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 right.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 roll.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 victory.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 wrong.wav
┃ ┃ ┃ ┃ ┃ ┣ 📁 Video
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 dl2e.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 dl2e.ogg

#### Dragon's Lair TV Show

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 dltv.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 dltv.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 dltv.txt
┃ ┃ ┃ ┃ ┃ ┣ 📁 Cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 game.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 save.cfg
┃ ┃ ┃ ┃ ┃ ┣ 📁 Fonts
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 font.ttf
┃ ┃ ┃ ┃ ┃ ┣ 📁 Overlay
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdl.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdr.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowright.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel2.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowul.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowup.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowur.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 bonus.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 complete.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk1.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk2.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk3.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 diskA.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 freeplay.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 getready.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 insertcoin.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 live.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m0.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m1.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m2.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m3.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m4.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m5.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m6.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mash.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 minus.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 nomoves.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num00.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num01.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num02.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num03.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num04.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num05.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num06.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num07.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num08.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num09.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 ok.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 scene.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 score.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 secret.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 skip.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 topscore.png
┃ ┃ ┃ ┃ ┃ ┣ 📁 Script
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 main.singe
┃ ┃ ┃ ┃ ┃ ┣ 📁 Sounds
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 clear.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 coin.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 death.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 right.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 victory.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 wrong.wav
┃ ┃ ┃ ┃ ┃ ┣ 📁 Video
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 dltv.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 dltv.ogg

#### Dragon Trainer

>This game doesn't use the framework described on top of this page.
{.is-info}

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 dragon.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 action.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdl.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdown.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdr.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowleft.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowright.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowul.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowup.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowur.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 bonus.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 clear.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 coin.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 complete.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 death.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk1.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk2.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk3.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 diskA.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 dragon.cfg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 dragon.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 dragon.txt
┃ ┃ ┃ ┃ ┃ ┣ 🗒 freeplay.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 globals.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 hscore.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 insertcoin.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 level.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 live.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 m0.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 m1.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 m2.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 m3.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 m39.ttf
┃ ┃ ┃ ┃ ┃ ┣ 🗒 m4.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 m5.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 m6.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 main.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 map.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 mash.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 mfull.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 nomoves.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 num00.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 num01.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 num02.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 num03.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 num04.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 num05.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 num06.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 num07.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 num08.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 num09.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 ok.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 right.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 save.cfg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 scene.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 score.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 secret.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 service.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 service.ttf
┃ ┃ ┃ ┃ ┃ ┣ 🗒 setupgame.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 skip.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 toolbox.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 victory.wav
┃ ┃ ┃ ┃ ┃ ┣ 📁 video
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 death.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 death.ogg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 intro01.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 intro01.ogg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 intro02.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 intro02.ogg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level01.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level01.ogg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level02.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level02.ogg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level03.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level03.ogg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level04.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level04.ogg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level05.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level05.ogg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level06.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level06.ogg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level07.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level07.ogg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level08.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level08.ogg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level09.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level09.ogg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level10.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level10.ogg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level11.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level11.ogg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 menus.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 menus.ogg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 title.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 title.ogg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 wrong.wav

#### Drug Wars HD

>This game doesn't use the framework described on top of this page.
{.is-info}

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 drugwars-hd.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 drugwars-hd.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 drugwars-hd.txt
┃ ┃ ┃ ┃ ┃ ┣ 📁 Cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 drugwars.cfg
┃ ┃ ┃ ┃ ┃ ┣ 📁 Fonts
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 default.ttf
┃ ┃ ┃ ┃ ┃ ┣ 📁 Overlay
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 badge3.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 bullets00.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 bullets01.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 bullets02.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 bullets03.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 bullets04.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 bullets05.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 bullets06.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 bullets07.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 bullets08.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 bullets09.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 bullets10.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 bullets11.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 bullets12.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 crosshaira.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 crosshairb.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 crosshairc.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 crosshaird.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 crosshaire.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hit.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hit1.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hit2.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hit3.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hit4.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hit5.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num0.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num1.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num2.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num3.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num4.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num5.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num6.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num7.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num8.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num9.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 pistol.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 shot.png
┃ ┃ ┃ ┃ ┃ ┣ 📁 Script
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 globals.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitbox-bar.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitbox-base.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitbox-beach.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitbox-boathouse.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitbox-buschase.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitbox-carchase.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitbox-courtroom.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitbox-drugdealer.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitbox-final.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitbox-helicopter.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitbox-hideout.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitbox-nightvision.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitbox-practice.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitbox-shipyard.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitbox-town.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitmap-3panels.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitmap-sierra.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hscore.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 manage.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 service.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 setuplevel.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 toolbox.singe
┃ ┃ ┃ ┃ ┃ ┣ 📁 Sounds
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 break.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 coin.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 gunempty.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 gunhit.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 gunreload.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 gunshot.wav
┃ ┃ ┃ ┃ ┃ ┣ 📁 Video
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 drugwars.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 drugwars.ogg

#### Esh's Arunmilla

>This game doesn't use the framework described on top of this page.
{.is-info}

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 aurunmilla.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 action.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 action2.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 action3.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 action4.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 action5.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdl.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdown.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdr.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowleft.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowright.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel2.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowul.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowup.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowur.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 aurunmilla.m2v
┃ ┃ ┃ ┃ ┃ ┣ 🗒 aurunmilla.ogg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 aurunmilla.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 aurunmilla.txt
┃ ┃ ┃ ┃ ┃ ┣ 🗒 bonus.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 buttons.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 clear.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 coin.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 cong.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 death.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 default.cfg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk1.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk2.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk3.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 diskA.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 font.ttf
┃ ┃ ┃ ┃ ┃ ┣ 🗒 freeplay.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 game.cfg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 getready.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 hold.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 insertcoin.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 level.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 live.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 m0.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 m1.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 m2.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 m3.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 m4.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 m5.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 m6.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 m7.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 main.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 mash.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 mfull.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 nomoves.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 num00.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 num01.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 num02.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 num03.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 num04.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 num05.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 num06.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 num07.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 num08.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 num09.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 ok.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 player.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 prompt.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 right.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 save.cfg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 scene.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 score.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 secret.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 shoot.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 skip.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 special.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 stick.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 tbronze.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 tgold.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 tno.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 topscore.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 tplat.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 tsilver.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 victory.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 wrong.wav

### F-P

#### Fire and Ice

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 fireandice.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 fireandice.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 fireandice.txt
┃ ┃ ┃ ┃ ┃ ┣ 📁 Cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 default.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 default2.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 game.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 game2.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hscore.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hscore2.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s1.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s2.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s3.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s4.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s5.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s6.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s10.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s20.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s30.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s40.cfg
┃ ┃ ┃ ┃ ┃ ┣ 📁 Fonts
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 font.ttf
┃ ┃ ┃ ┃ ┃ ┣ 📁 Overlay
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdl.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdr.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowright.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel1.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel2.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowul.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowup.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowur.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barnone.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 bonus.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 buttons.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 death.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk1.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk2.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk3.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 diskA.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 freeplay.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 getready.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hold.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hright.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hup.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 insertcoin.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 live.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 📁 Lores
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdl.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdr.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowright.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowul.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowup.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowur.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barnone.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 bonus.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 buttons.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 death.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk3.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 diskA.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 freeplay.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 getready.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hold.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hright.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hup.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 insertcoin.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 live.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m0.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m3.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m4.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m5.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m6.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m7.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mash.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 minus.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 nomoves.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num00.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num01.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num02.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num03.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num04.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num05.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num06.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num07.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num08.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num09.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 perfect.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 scene.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 score.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 shoot.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 skip.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 special.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 stick.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tbronze.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tgold.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tno.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 topscore.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 total.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tplat.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tsilver.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m0.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m1.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m2.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m3.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m4.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m5.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m6.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m7.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mash.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 📁 Minimal
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdl.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdr.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowright.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowul.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowup.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowur.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barnone.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 bonus.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 buttons.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 death.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk3.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 diskA.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 freeplay.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 getready.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hold.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hright.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hup.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 insertcoin.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 live.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m0.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m3.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m4.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m5.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m6.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m7.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mash.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 minus.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 nomoves.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num00.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num01.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num02.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num03.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num04.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num05.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num06.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num07.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num08.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num09.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 ok.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 perfect.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 scene.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 score.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 secret.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 shoot.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 skip.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 special.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 stick.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tbronze.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tgold.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tno.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 topscore.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 total.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tplat.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tsilver.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 minus.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 nomoves.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num00.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num01.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num02.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num03.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num04.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num05.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num06.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num07.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num08.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num09.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 perfect.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 scene.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 score.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 shoot.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 skip.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 special.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 stick.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tbronze.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tgold.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tno.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 topscore.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 total.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tplat.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tsilver.png
┃ ┃ ┃ ┃ ┃ ┣ 📁 Script
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 addons.singe
┃ ┃ ┃ ┃ ┃ ┣ 📁 Sounds
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 clear.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 coin.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 death.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 right.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 roll.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 victory.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 wrong.wav
┃ ┃ ┃ ┃ ┃ ┣ 📁 Video
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 fireandice.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 fireandice.ogg

#### Freedom Fighter

>This game doesn't use the framework described on top of this page.
{.is-info}

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 freedomfighter.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 action.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdl.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdown.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdr.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowleft.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowright.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowul.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowup.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowur.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 bonus.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 bullet.png
┃ ┃ ┃ ┃ ┃ ┣ 📁 cdrom
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 boom.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 boom.ogg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 death.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 death.ogg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 end.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 end.ogg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 intro.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 intro.ogg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level01.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level01.ogg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level02.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level02.ogg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level03.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level03.ogg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level04.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level04.ogg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level05.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level05.ogg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level06.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level06.ogg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level07.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level07.ogg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level08.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level08.ogg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 morte_robot.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 morte_robot.ogg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 title.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 title.ogg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 clear.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 coin.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 crosshair.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 death.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk1.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk2.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk3.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 diskA.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 freedomfighter.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 freedomfighter.txt
┃ ┃ ┃ ┃ ┃ ┣ 🗒 game.cfg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 globals.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 hscore.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 level.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 live.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 m0.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 m1.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 m2.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 m3.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 m4.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 m5.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 m6.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 m39.ttf
┃ ┃ ┃ ┃ ┃ ┣ 🗒 main_mouse.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 map.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 mash.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 mfull.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 miss.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 nomoves.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 num00.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 num01.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 num02.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 num03.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 num04.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 num05.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 num06.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 num07.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 num08.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 num09.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 ok.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 reload.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 save.cfg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 scene.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 score.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 service.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 shoot.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 shot.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 skip.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 toolbox.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 victory.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 wrong.wav

#### Friday the 13th

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 friday13.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 friday13.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 friday13.txt
┃ ┃ ┃ ┃ ┃ ┣ 📁 Cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 default.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 game.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hscore.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s1.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s2.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s3.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s4.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s5.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s6.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s10.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s20.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s30.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s40.cfg
┃ ┃ ┃ ┃ ┃ ┣ 📁 Fonts
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 font.ttf
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 fontlo.ttf
┃ ┃ ┃ ┃ ┃ ┣ 📁 Overlay
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdl.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdr.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowright.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel1.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel2.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowul.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowup.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowur.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barnone.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 bonus.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 buttons.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 death.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk1.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk2.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk3.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 diskA.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 freeplay.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 getready.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hold.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hright.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hup.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 insertcoin.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 live.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 📁 Lores
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdl.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdr.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowright.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowul.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowup.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowur.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barnone.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 bonus.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 buttons.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 death.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk3.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 diskA.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 freeplay.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 getready.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hold.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hright.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hup.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 insertcoin.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 live.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m0.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m3.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m4.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m5.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m6.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m7.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mash.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 minus.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 nomoves.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num00.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num01.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num02.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num03.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num04.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num05.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num06.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num07.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num08.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num09.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 perfect.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 scene.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 score.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 skip.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 special.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 stick.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tbronze.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tgold.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tno.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 topscore.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 total.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tplat.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tsilver.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m0.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m1.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m2.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m3.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m4.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m5.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m6.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m7.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mash.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 📁 Minimal
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdl.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdr.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowright.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowul.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowup.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowur.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barnone.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 bonus.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 buttons.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 death.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk3.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 diskA.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 freeplay.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 getready.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hold.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hright.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hup.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 insertcoin.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 live_1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 live.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m0.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m3.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m4.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m5.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m6.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m7.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mash.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 minus.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 nomoves.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num00.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num01.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num02.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num03.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num04.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num05.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num06.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num07.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num08.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num09.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 perfect.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 scene.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 score.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 skip.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 special.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 stick.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tbronze.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tgold.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 topscore.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 total.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tplat.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tsilver.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 minus.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 nomoves.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num00.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num01.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num02.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num03.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num04.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num05.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num06.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num07.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num08.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num09.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 perfect.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 scene.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 score.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 skip.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 special.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 stick.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tbronze.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tgold.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tno.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 topscore.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 total.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tplat.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tsilver.png
┃ ┃ ┃ ┃ ┃ ┣ 📁 Script
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 addons.singe
┃ ┃ ┃ ┃ ┃ ┣ 📁 Sounds
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 clear.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 coin.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 death.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 music.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 music2.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 right.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 roll.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 victory.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 wrong.wav
┃ ┃ ┃ ┃ ┃ ┣ 📁 Video
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 f13.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 f13.ogg

#### MadDog McCree HD

>This game doesn't use the framework described on top of this page.
{.is-info}

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 maddog-hd.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 maddog-hd.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 maddog-hd.txt
┃ ┃ ┃ ┃ ┃ ┣ 📁 Cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 maddog.cfg
┃ ┃ ┃ ┃ ┃ ┣ 📁 Fonts
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 gambler.ttf
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m39.ttf
┃ ┃ ┃ ┃ ┃ ┣ 📁 Overlay
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 bullet.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 bullet2.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 credits.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 crosshaira.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 crosshairb.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 crosshairc.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 crosshaird.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 crosshaire.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hit.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hit1.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hit2.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hit3.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hit4.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hit5.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hole.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num0.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num1.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num2.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num3.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num4.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num5.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num6.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num7.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num8.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num9.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 pistol.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 reload.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 shot.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 star.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 star2.png
┃ ┃ ┃ ┃ ┃ ┣ 📁 Script
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 framework.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitbox-highscore.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitbox-lvlbank.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitbox-lvlbottle.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitbox-lvlcanyon.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitbox-lvlcliff.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitbox-lvlcorral.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitbox-lvlfuse.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitbox-lvlhideout.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitbox-lvlhouse.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitbox-lvlmad.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitbox-lvlplateau.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitbox-lvlpond.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitbox-lvlsaloon.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitbox-lvlsheriff.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitbox-lvlstart.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitbox-lvltut.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitbox-showdowns.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitmap-hideoutsigns.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitmap-mine.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitmap-signpost.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 service.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 toolbox.singe
┃ ┃ ┃ ┃ ┃ ┣ 📁 Sounds
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 clear.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 coin.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 death.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 music.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 music2.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 right.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 roll.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 victory.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 wrong.wav
┃ ┃ ┃ ┃ ┃ ┣ 📁 Video
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 maddog.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 maddog.ogg

#### MadDog II HD

>This game doesn't use the framework described on top of this page.
{.is-info}

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 maddog2-hd.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 maddog2-hd.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 maddog2-hd.txt
┃ ┃ ┃ ┃ ┃ ┣ 📁 Cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 maddog2.cfg
┃ ┃ ┃ ┃ ┃ ┣ 📁 Fonts
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 gambler.ttf
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m39.ttf
┃ ┃ ┃ ┃ ┃ ┣ 📁 Overlay
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 bullet.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 credits.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 crosshaira.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 crosshairb.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 crosshairc.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 crosshaird.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 crosshaire.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 dr.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hit.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hole.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num0.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num1.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num2.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num3.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num4.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num5.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num6.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num7.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num8.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num9.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 pistol.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 reload.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 shot.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 star.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 ul.png
┃ ┃ ┃ ┃ ┃ ┣ 📁 Script
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 globals.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitbox-beaver.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitbox-bonnie.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitbox-finalbeaver.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitbox-finalbonnie.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitbox-item1.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitbox-item2.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitbox-maddog.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitbox-prof1.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitbox-prof2.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitbox-prospector1.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitbox-showdows.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitbox-start.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitbox-town1.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitbox-town2.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hscore.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 service.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 setuplevel.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 toolbox.singe
┃ ┃ ┃ ┃ ┃ ┣ 📁 Sounds
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arcadecoin.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 bottlebreak.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 coin.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 empty.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 music.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 reload.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 shot.wav
┃ ┃ ┃ ┃ ┃ ┣ 📁 Video
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 maddog2.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 maddog2.ogg

#### Ninja Hayate HD

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 hayate-hd.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 hayate-hd.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 hayate-hd.txt
┃ ┃ ┃ ┃ ┃ ┣ 🗒 hayate-psx.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 hayate-psx.txt
┃ ┃ ┃ ┃ ┃ ┣ 📁 Cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 default.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 default2.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 game.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 game2.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hscore.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hscore2.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s1.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s2.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s3.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s4.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s5.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s6.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s10.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s20.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s30.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s40.cfg
┃ ┃ ┃ ┃ ┃ ┣ 📁 Fonts
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 default.ttf
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 font.ttf
┃ ┃ ┃ ┃ ┃ ┣ 📁 Overlay
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdl.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdr.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowright.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel1.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel2.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowul.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowup.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowur.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barnone.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 bonus.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 buttons.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 death.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk1.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk2.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk3.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 diskA.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 freeplay.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 insertcoin.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 live.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 📁 Lores
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdl.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdr.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowright.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowul.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowup.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowur.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barnone.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 bonus.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 buttons.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 death.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk3.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 diskA.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 freeplay.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 insertcoin.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 live.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 minus.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 nomoves.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num00.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num01.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num02.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num03.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num04.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num05.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num06.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num07.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num08.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num09.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 perfect.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 scanlines.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 scene.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 score.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 skip.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 special.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 stick.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 topscore.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 total.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 📁 Minimal
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdl.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdr.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowright.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowul.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowup.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowur.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barnone.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 bonus.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 buttons.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 death.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk3.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 diskA.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 freeplay.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 insertcoin.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 live.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 minus.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 nomoves.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num00.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num01.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num02.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num03.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num04.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num05.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num06.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num07.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num08.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num09.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 perfect.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 scanlines.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 scene.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 score.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 skip.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 special.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 stick.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 topscore.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 total.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 minus.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 nomoves.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num00.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num01.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num02.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num03.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num04.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num05.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num06.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num07.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num08.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num09.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 perfect.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 scanlines.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 scene.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 score.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 skip.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 special.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 stick.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 topscore.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 total.png
┃ ┃ ┃ ┃ ┃ ┣ 📁 Script
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 addons.singe
┃ ┃ ┃ ┃ ┃ ┣ 📁 Sounds
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 clear.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 coin.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 death.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 prompt.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 right.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 roll.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 victory.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 wrong.wav
┃ ┃ ┃ ┃ ┃ ┣ 📁 Video
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hayate.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hayate.ogg

#### Œil pour œil

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 oeil.daphne
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 oeil.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 oeil.txt
┃ ┃ ┃ ┃ ┃ ┣ 📁 Cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 default.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 game.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hscore.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s1.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s2.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s3.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s4.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s5.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s6.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 save.cfg
┃ ┃ ┃ ┃ ┃ ┣ 📁 Fonts
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 font.ttf
┃ ┃ ┃ ┃ ┃ ┣ 📁 Overlay
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action2.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action3.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action4.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdl.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdr.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowright.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel1.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel2.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowul.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowup.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowur.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barnone.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 bonus.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 buttons.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 death.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk1.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk2.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk3.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 diskA.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 freeplay.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 getready.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hold.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hright.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hup.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 insertcoin.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 live.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 📁 Lores
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action3.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action4.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdl.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdr.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowright.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowul.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowup.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowur.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barnone.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 bonus.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 buttons.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 death.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk3.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 diskA.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 freeplay.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 getready.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hold.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hright.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hup.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 insertcoin.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 live.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m0.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m3.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m4.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m5.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m6.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m7.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mash.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 minus.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 nomoves.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num00.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num01.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num02.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num03.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num04.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num05.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num06.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num07.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num08.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num09.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 ok.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 perfect.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 scene.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 score.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 secret.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 shoot.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 skip.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 special.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 stick.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tbronze.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tgold.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tno.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 topscore.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 total.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tplat.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tsilver.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m0.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m1.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m2.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m3.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m4.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m5.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m6.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m7.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mash.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 📁 Minimal
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action3.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action4.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdl.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdr.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowright.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowul.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowup.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowur.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barnone.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 bonus.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 buttons.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 death.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk3.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 diskA.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 freeplay.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 getready.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hold.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hright.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hup.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 insertcoin.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 live.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m0.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m3.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m4.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m5.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m6.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m7.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mash.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 minus.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 nomoves.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num00.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num01.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num02.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num03.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num04.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num05.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num06.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num07.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num08.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num09.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 ok.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 perfect.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 scene.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 score.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 secret.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 shoot.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 skip.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 special.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 stick.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tbronze.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tgold.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tno.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 topscore.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 total.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tplat.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tsilver.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 minus.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 nomoves.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num00.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num01.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num02.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num03.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num04.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num05.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num06.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num07.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num08.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num09.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 ok.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 perfect.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 scene.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 score.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 secret.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 shoot.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 skip.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 special.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 stick.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tbronze.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tgold.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tno.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 topscore.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 total.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tplat.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tsilver.png
┃ ┃ ┃ ┃ ┃ ┣ 📁 Script
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 addons.singe
┃ ┃ ┃ ┃ ┃ ┣ 📁 Sounds
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 clear.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 coin.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 death.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 right.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 roll.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 victory.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 wrong.wav
┃ ┃ ┃ ┃ ┃ ┣ 📁 Video
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 oeil.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 oeil.ogg

#### Platoon (Prototype)

>This game doesn't use the framework described on top of this page.
{.is-info}

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 platoon.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 arcaden.ttf
┃ ┃ ┃ ┃ ┃ ┣ 🗒 bullet2.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 coin.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 crosshaira.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 crosshaira_old.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 crosshairb.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 crosshairc.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 crosshaird.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 crosshaire.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 empty.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 hat2.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 hat3.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 hscore.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 m39.ttf
┃ ┃ ┃ ┃ ┃ ┣ 🗒 manage.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 menu-marquee.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 menu-poster.jpg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 num0.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 num1.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 num2.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 num3.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 num4.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 num5.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 num6.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 num7.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 num8.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 num9.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 pistol2.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 platoon-globals.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 platoon-hitbox-bravo.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 platoon-hitbox-charlie.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 platoon-hitbox-delta.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 platoon-hitbox-lima.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 platoon-hitbox-tango.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 platoon-hitbox-zulu.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 platoon-setuplevel.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 platoon.cfg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 platoon.m2v
┃ ┃ ┃ ┃ ┃ ┣ 🗒 platoon.ogg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 platoon.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 platoon.txt
┃ ┃ ┃ ┃ ┃ ┣ 🗒 reload.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 reload2.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 score.ttf
┃ ┃ ┃ ┃ ┃ ┣ 🗒 service.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 shot.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 toolbox.singe

#### Princess Mononoke

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 mononoke.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 mononoke.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 mononoke.txt
┃ ┃ ┃ ┃ ┃ ┣ 📁 Cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 default.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 game.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hscore.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s1.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s2.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s3.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s4.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s5.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s6.cfg
┃ ┃ ┃ ┃ ┃ ┣ 📁 Fonts
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 font.ttf
┃ ┃ ┃ ┃ ┃ ┣ 📁 Overlay
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdl.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdr.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowright.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel1.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel2.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowul.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowup.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowur.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barnone.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 bonus.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 buttons.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 death.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk1.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk2.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk3.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 diskA.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 freeplay.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 getready.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hold.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hright.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hup.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 insertcoin.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 live.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 📁 Lores
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdl.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdr.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowright.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowul.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowup.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowur.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barnone.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 bonus.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 buttons.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 death.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk3.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 diskA.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 freeplay.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 getready.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hold.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hright.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hup.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 insertcoin.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 live.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m0.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m3.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m4.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m5.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m6.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m7.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mash.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 minus.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 nomoves.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num00.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num01.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num02.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num03.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num04.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num05.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num06.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num07.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num08.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num09.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 perfect.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 scene.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 score.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 shoot.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 skip.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 special.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 stick.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tbronze.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tgold.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tno.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 topscore.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 total.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tplat.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tsilver.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m0.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m1.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m2.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m3.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m4.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m5.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m6.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m7.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mash.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 📁 Minimal
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdl.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdr.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowright.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowul.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowup.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowur.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barnone.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 bonus.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 buttons.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 death.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk3.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 diskA.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 freeplay.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 getready.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hold.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hright.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hup.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 insertcoin.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 live.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m0.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m3.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m4.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m5.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m6.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m7.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mash.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 minus.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 nomoves.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num00.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num01.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num02.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num03.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num04.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num05.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num06.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num07.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num08.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num09.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 perfect.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 scene.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 score.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 shoot.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 skip.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 special.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 stick.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tbronze.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tgold.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tno.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 topscore.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 total.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tplat.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tsilver.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 minus.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 nomoves.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num00.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num01.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num02.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num03.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num04.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num05.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num06.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num07.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num08.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num09.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 perfect.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 scene.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 score.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 shoot.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 skip.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 special.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 stick.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tbronze.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tgold.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tno.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 topscore.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 total.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tplat.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tsilver.png
┃ ┃ ┃ ┃ ┃ ┣ 📁 Script
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 addons.singe
┃ ┃ ┃ ┃ ┃ ┣ 📁 Sounds
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 clear.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 coin.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 death.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 right.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 roll.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 victory.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 wrong.wav
┃ ┃ ┃ ┃ ┃ ┣ 📁 Video
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mononoke.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mononoke.ogg

#### Puss in Boots

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 pussinboots.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 pussinboots.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 pussinboots.txt
┃ ┃ ┃ ┃ ┃ ┣ 📁 Cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 game.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 save.cfg
┃ ┃ ┃ ┃ ┃ ┣ 📁 Fonts
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 font.ttf
┃ ┃ ┃ ┃ ┃ ┣ 📁 Overlay
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdl.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdr.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowright.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel2.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowul.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowup.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowur.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 bonus.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 complete.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk1.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk2.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk3.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 diskA.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 freeplay.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 getready.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 insertcoin.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 live.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m0.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m1.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m2.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m3.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m4.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m5.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m6.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mash.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 nomoves.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num00.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num01.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num02.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num03.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num04.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num05.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num06.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num07.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num08.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num09.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 ok.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 scene.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 score.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 secret.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 skip.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 topscore.png
┃ ┃ ┃ ┃ ┃ ┣ 📁 Script
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 globals.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hscore.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 main.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 map.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 service.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 toolbox.singe
┃ ┃ ┃ ┃ ┃ ┣ 📁 Sounds
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 clear.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 coin.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 death.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 right.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 victory.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 wrong.wav
┃ ┃ ┃ ┃ ┃ ┣ 📁 Video
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 pussinboots.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 pussinboots.ogg

### Q-Z

#### Rollercoaster

>This game is playable only with a keyboard!
{.is-info}

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 rollercoaster.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 rollercoaster.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 rollercoaster.txt
┃ ┃ ┃ ┃ ┃ ┣ 📁 Fonts
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 FreeLicense.txt
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 PrintChar21.ttf
┃ ┃ ┃ ┃ ┃ ┣ 📁 Images
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 01234.jpg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 02550.jpg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 02818.jpg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 04002.jpg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 04627.jpg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 04750.jpg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 05890.jpg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 07343.jpg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 11000.jpg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 15362.jpg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 21130.jpg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 23100.jpg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 28322.jpg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 33900.jpg
┃ ┃ ┃ ┃ ┃ ┣ 📁 Sounds
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 beep3.wav
┃ ┃ ┃ ┃ ┃ ┣ 📁 Video
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 00000_00030.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 01234_33900.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 06726_06959.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 06726_06959.ogg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 08300_08488.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 08300_08488.ogg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 10960_11107.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 10960_11107.ogg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 11818_11907.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 11818_11907.ogg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 12169_12227.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 12169_12227.ogg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 14038_14100.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 14038_14100.ogg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 14724_14855.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 14724_14855.ogg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 16185_16223.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 16185_16223.ogg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 18722_18807.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 18722_18807.ogg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 19453_19799.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 19453_19799.ogg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 19801_20007.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 19801_20007.ogg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 30032_30214.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 30032_30214.ogg

#### Samurai Jack

>This game doesn't use the framework described on top of this page.
{.is-info}

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 jack.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 action.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 action2.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 action3.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdown.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowleft.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowright.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowup.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 coin.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 complete.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 cross.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 death.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 freeplay.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 globals.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 hscore.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 insertcoin.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 jack.cfg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 jack.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 jack.txt
┃ ┃ ┃ ┃ ┃ ┣ 🗒 level.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 live.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 m0.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 m1.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 m2.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 m3.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 m39.ttf
┃ ┃ ┃ ┃ ┃ ┣ 🗒 m4.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 m5.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 main.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 map.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 mash.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 nomoves.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 num00.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 num01.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 num02.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 num03.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 num04.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 num05.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 num06.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 num07.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 num08.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 num09.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 opt.ttf
┃ ┃ ┃ ┃ ┃ ┣ 🗒 right.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 scene.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 secret.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 service.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 skip.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 toolbox.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 victory.wav
┃ ┃ ┃ ┃ ┃ ┣ 📁 video
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 death.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 death.ogg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 intro01.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 intro01.ogg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 intro02.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 intro02.ogg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level01.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level01.ogg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level02.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level02.ogg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level03.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level03.ogg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level04.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level04.ogg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level05.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level05.ogg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 menus.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 menus.ogg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 title.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 title.ogg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 wrong.wav

#### Scrat

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 scrat.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 scrat.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 scrat.txt
┃ ┃ ┃ ┃ ┃ ┣ 📁 Cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 default.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 game.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hscore.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s1.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s2.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s3.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s4.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s5.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s6.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 save.cfg
┃ ┃ ┃ ┃ ┃ ┣ 📁 Fonts
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 font.ttf
┃ ┃ ┃ ┃ ┃ ┣ 📁 Overlay
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action2.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action3.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action4.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdl.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdr.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowright.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel1.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel2.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowul.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowup.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowur.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barnone.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 bonus.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 buttons.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 death.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk1.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk2.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk3.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 diskA.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 freeplay.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 getready.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hold.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hright.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hup.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 insertcoin.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 live.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 loopl.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 loopr.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 📁 Lores
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action3.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action4.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdl.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdr.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowright.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowul.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowup.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowur.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barnone.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 bonus.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 buttons.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 death.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk3.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 diskA.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 freeplay.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 getready.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hold.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hright.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hup.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 insertcoin.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 live.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m0.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m3.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m4.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m5.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m6.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m7.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mash.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 minus.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 nomoves.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num00.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num01.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num02.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num03.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num04.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num05.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num06.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num07.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num08.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num09.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 ok.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 perfect.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 scene.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 score.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 secret.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 shoot.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 skip.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 special.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 stick.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tbronze.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tgold.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 time.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tno.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 topscore.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 total.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tplat.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tsilver.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m0.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m1.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m2.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m3.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m4.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m5.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m6.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m7.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mash.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 📁 Minimal
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action3.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action4.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdl.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdr.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowright.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowul.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowup.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowur.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barnone.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 bonus.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 buttons.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 death.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk3.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 diskA.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 freeplay.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 getready.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hold.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hright.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hup.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 insertcoin.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 live.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m0.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m3.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m4.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m5.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m6.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m7.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mash.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 minus.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 nomoves.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num00.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num01.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num02.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num03.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num04.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num05.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num06.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num07.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num08.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num09.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 ok.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 perfect.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 scene.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 score.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 secret.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 shoot.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 skip.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 special.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 stick.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tbronze.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tgold.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 time.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tno.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 topscore.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 total.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tplat.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tsilver.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 minus.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 nomoves.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num00.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num01.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num02.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num03.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num04.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num05.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num06.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num07.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num08.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num09.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 ok.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 perfect.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 scene.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 score.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 secret.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 shoot.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 skip.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 special.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 stick.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tbronze.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tgold.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tno.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 topscore.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 total.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tplat.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tsilver.png
┃ ┃ ┃ ┃ ┃ ┣ 📁 Script
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 addons.singe
┃ ┃ ┃ ┃ ┃ ┣ 📁 Sounds
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 clear.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 coin.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 death.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 right.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 roll.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 victory.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 wrong.wav
┃ ┃ ┃ ┃ ┃ ┣ 📁 Video
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 scrat.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 scrat.ogg

#### Secret of Nimh

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 secretofnimh.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 secretofnimh.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 secretofnimh.txt
┃ ┃ ┃ ┃ ┃ ┣ 📁 Cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 default.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 game.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hscore.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s1.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s2.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s3.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s4.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s5.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s6.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s10.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s20.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s30.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s40.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 save.cfg
┃ ┃ ┃ ┃ ┃ ┣ 📁 Fonts
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 font.ttf
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 fontlo.ttf
┃ ┃ ┃ ┃ ┃ ┣ 📁 Overlay
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowright.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel1.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel2.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowup.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barnone.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 bonus.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 buttons.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 death.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk1.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk2.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk3.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 diskA.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 freeplay.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 getready.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 insertcoin.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 live.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 📁 Lores
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowright.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowup.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barnone.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 bonus.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 buttons.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 death.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk3.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 diskA.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 freeplay.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 getready.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 insertcoin.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 live.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 minus.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 nomoves.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num00.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num01.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num02.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num03.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num04.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num05.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num06.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num07.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num08.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num09.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 ok.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 perfect.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 scene.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 score.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 shoot.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 skip.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 special.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 stick.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tbronze.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tgold.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 time.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tno.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 topscore.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 total.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tplat.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tsilver.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 📁 Minimal
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowright.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowup.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barnone.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 bonus.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 buttons.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 death.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk3.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 diskA.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 freeplay.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 getready.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 insertcoin.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 live.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 minus.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 nomoves.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num00.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num01.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num02.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num03.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num04.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num05.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num06.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num07.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num08.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num09.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 perfect.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 scene.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 score.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 skip.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 special.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 stick.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tbronze.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tgold.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tno.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 topscore.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 total.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tplat.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tsilver.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 minus.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 nomoves.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num00.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num01.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num02.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num03.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num04.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num05.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num06.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num07.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num08.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num09.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 ok.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 perfect.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 scene.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 score.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 shoot.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 skip.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 special.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 stick.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tbronze.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tgold.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 time.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tno.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 topscore.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 total.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tplat.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tsilver.png
┃ ┃ ┃ ┃ ┃ ┣ 📁 Script
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 addons.singe
┃ ┃ ┃ ┃ ┃ ┣ 📁 Sounds
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 clear.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 coin.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 death.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 music.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 music2.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 prompt.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 right.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 roll.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 victory.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 wrong.wav
┃ ┃ ┃ ┃ ┃ ┣ 📁 Videos
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 SecretOfNimh.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 SecretOfNimh.ogg

#### Space Pirates HD

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 spacepirates-hd.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 spacepirates-hd.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 spacepirates-hd.txt
┃ ┃ ┃ ┃ ┃ ┣ 📁 Cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 spacepirates.cfg
┃ ┃ ┃ ┃ ┃ ┣ 📁 Fonts
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 ca.ttf
┃ ┃ ┃ ┃ ┃ ┣ 📁 Overlay
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 canon.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 crosshaira.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 crosshairb.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 crosshairc.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 crosshaird.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 crosshaire.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 f00.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 f01.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 f02.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 f03.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 f04.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 f05.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 f06.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 f07.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 f08.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 f09.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hit.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hit1.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hit2.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hit3.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hit4.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hit5.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 p00.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 p01.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 p02.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 pistol.png
┃ ┃ ┃ ┃ ┃ ┣ 📁 Script
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 globals.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitbox-band.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitbox-door1.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitbox-door2.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitbox-door2b.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitbox-door3.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitbox-fellina.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitbox-fellinapuzzle.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitbox-firepractice.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitbox-mountain.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitbox-practice.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitbox-reaper.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitbox-scrapyard.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitbox-scrapyard2.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitbox-ship.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitbox-socketpuzzle.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitbox-tallon2.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hscore.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 service.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 setuplevel.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 shipbattle.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 toolbox.singe
┃ ┃ ┃ ┃ ┃ ┣ 📁 Sounds
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 coin.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sp_credit2.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sp_empty.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sp_erase.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sp_gunshot.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sp_niceshot.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sp_ohwell.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sp_reload01b.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tick.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tock.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 wrong.wav
┃ ┃ ┃ ┃ ┃ ┣ 📁 Video
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 spacepirates.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 spacepirates.ogg

#### Starblazers

>This game doesn't use the framework described on top of this page.
{.is-info}

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 starblazers.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 action.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdl.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdown.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdr.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowleft.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowright.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowul.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowup.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowur.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 bonus.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 clear.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 coin.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 complete.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 crosshair.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 death.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk1.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk2.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk3.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 diskA.png
┃ ┃ ┃ ┃ ┃ ┣ 📁 dvd
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 choice.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 choice.ogg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 death.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 death.ogg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 intro01.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 intro01.ogg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 intro02.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 intro02.ogg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level01.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level01.ogg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level02.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level02.ogg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level03.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level03.ogg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level04.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level04.ogg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level05.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level05.ogg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level06.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level06.ogg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level07.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level07.ogg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level08.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level08.ogg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 title.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 title.ogg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 game.cfg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 globals.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 hscore.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 level.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 live.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 m0.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 m1.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 m2.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 m3.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 m39.ttf
┃ ┃ ┃ ┃ ┃ ┣ 🗒 m4.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 m5.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 m6.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 main.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 map.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 mash.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 mfull.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 nomoves.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 num00.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 num01.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 num02.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 num03.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 num04.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 num05.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 num06.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 num07.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 num08.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 num09.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 ok.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 right.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 save.cfg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 scene.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 score.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 secret.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 service.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 skip.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 starblazers.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 starblazers.txt
┃ ┃ ┃ ┃ ┃ ┣ 🗒 starblazers_cross.png
┃ ┃ ┃ ┃ ┃ ┣ 📁 startup.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 setupgame.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 setupgameblank.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 toolbox.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 victory.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 wrong.wav

#### Starship Troopers

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 starshiptroopers.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 StarshipTroopers.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 StarshipTroopers.txt
┃ ┃ ┃ ┃ ┃ ┣ 📁 Cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 default.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 game.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hscore.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s1.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s2.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s3.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s4.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s5.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s6.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s10.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s20.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s30.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s40.cfg
┃ ┃ ┃ ┃ ┃ ┣ 📁 Fonts
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 font.ttf
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 fontlo.ttf
┃ ┃ ┃ ┃ ┃ ┣ 📁 Overlay
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action2.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowright.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel1.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel2.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowup.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barnone.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 bonus.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 buttons.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 death.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk1.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk2.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk3.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 diskA.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 freeplay.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 getready.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hold.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hright.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hup.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 insertcoin.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 live.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 📁 Lores
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowright.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowup.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barnone.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 bonus.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 buttons.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 death.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk3.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 diskA.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 freeplay.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 getready.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hold.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hright.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hup.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 insertcoin.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 live.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m0.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m3.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m4.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m5.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m6.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m7.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mash.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 minus.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 nomoves.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num00.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num01.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num02.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num03.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num04.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num05.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num06.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num07.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num08.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num09.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 ok.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 perfect.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 scene.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 score.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 secret.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 shoot.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 skip.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 special.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 stick.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tbronze.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tgold.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 time.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tno.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 topscore.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 total.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tplat.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tsilver.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m0.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m1.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m2.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m3.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m4.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m5.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m6.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m7.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mash.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 📁 Minimal
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowright.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowup.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barnone.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 bonus.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 buttons.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 death.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk3.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 diskA.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 freeplay.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 getready.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hold.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hright.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hup.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 insertcoin.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 live.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m0.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m3.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m4.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m5.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m6.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m7.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mash.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 minus.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 nomoves.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num00.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num01.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num02.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num03.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num04.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num05.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num06.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num07.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num08.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num09.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 ok.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 perfect.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 scene.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 score.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 secret.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 shoot.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 skip.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 special.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 stick.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tbronze.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tgold.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tno.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 topscore.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 total.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tplat.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tsilver.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 minus.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 nomoves.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num00.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num01.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num02.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num03.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num04.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num05.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num06.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num07.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num08.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num09.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 ok.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 perfect.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 scene.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 score.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 secret.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 shoot.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 skip.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 special.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 stick.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tbronze.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tgold.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 time.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tno.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 topscore.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 total.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tplat.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tsilver.png
┃ ┃ ┃ ┃ ┃ ┣ 📁 Script
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 addons.singe
┃ ┃ ┃ ┃ ┃ ┣ 📁 Sounds
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 clear.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 coin.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 death.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 music.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 music2.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 right.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 roll.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 victory.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 wrong.wav
┃ ┃ ┃ ┃ ┃ ┣ 📁 Videos
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 starshiptroopers.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 starshiptroopers.ogg

#### Sucker Punch

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 suckerpunch.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 suckerpunch.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 suckerpunch.txt
┃ ┃ ┃ ┃ ┃ ┣ 📁 Cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 default.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 game.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hscore.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s1.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s2.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s3.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s4.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s5.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s6.cfg
┃ ┃ ┃ ┃ ┃ ┣ 📁 Fonts
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 font.ttf
┃ ┃ ┃ ┃ ┃ ┣ 📁 Overlay
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdl.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdr.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowright.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel1.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel2.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowul.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowup.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowur.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barnone.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 bonus.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 buttons.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 death.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk1.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk2.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk3.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 diskA.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 freeplay.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 getready.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hold.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hright.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hup.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 insertcoin.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 live.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 📁 Lores
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdl.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdr.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowright.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowul.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowup.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowur.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barnone.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 bonus.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 buttons.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 death.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk3.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 diskA.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 freeplay.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 getready.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hold.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hright.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hup.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 insertcoin.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 live.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m0.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m3.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m4.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m5.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m6.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m7.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mash.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 minus.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 nomoves.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num00.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num01.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num02.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num03.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num04.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num05.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num06.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num07.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num08.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num09.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 perfect.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 scene.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 score.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 skip.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 special.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 stick.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tbronze.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tgold.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tno.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 topscore.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 total.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tplat.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tsilver.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m0.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m1.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m2.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m3.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m4.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m5.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m6.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m7.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mash.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 📁 Minimal
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdl.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdr.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowright.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowul.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowup.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowur.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barnone.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 bonus.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 buttons.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 death.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk3.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 diskA.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 freeplay.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 getready.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hold.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hright.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hup.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 insertcoin.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 live.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m0.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m3.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m4.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m5.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m6.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m7.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mash.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 minus.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 nomoves.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num00.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num01.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num02.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num03.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num04.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num05.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num06.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num07.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num08.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num09.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 perfect.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 scene.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 score.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 skip.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 special.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 stick.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tbronze.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tgold.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tno.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 topscore.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 total.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tplat.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tsilver.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 minus.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 nomoves.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num00.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num01.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num02.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num03.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num04.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num05.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num06.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num07.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num08.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num09.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 perfect.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 scene.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 score.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 skip.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 special.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 stick.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tbronze.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tgold.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tno.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 topscore.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 total.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tplat.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tsilver.png
┃ ┃ ┃ ┃ ┃ ┣ 📁 Script
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 addons.singe
┃ ┃ ┃ ┃ ┃ ┣ 📁 Sounds
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 clear.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 coin.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 death.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 right.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 roll.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 victory.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 wrong.wav
┃ ┃ ┃ ┃ ┃ ┣ 📁 Video
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 suckerpunch.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 suckerpunch.ogg

#### Sugar Rush

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 sugar_rush.daphne
┃ ┃ ┃ ┃ ┃ ┣ 📁 Cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 default.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 game.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hscore.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s1.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s2.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s3.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s4.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s5.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s6.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s10.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s20.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s30.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s40.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 save.cfg
┃ ┃ ┃ ┃ ┃ ┣ 📁 Fonts
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 font.ttf
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 fontlo.ttf
┃ ┃ ┃ ┃ ┃ ┣ 📁 Overlay
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action2.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowright.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel1.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel2.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowup.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barnone.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 bonus.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 buttons.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 death.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk1.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk2.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk3.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 diskA.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 freeplay.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 getready.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hold.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hright.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hup.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 insertcoin.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 live.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 📁 Lores
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowright.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowup.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barnone.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 bonus.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 buttons.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 death.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk3.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 diskA.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 freeplay.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 getready.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hold.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hright.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hup.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 insertcoin.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 live.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m0.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m3.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m4.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m5.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m6.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m7.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mash.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 minus.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 nomoves.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num00.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num01.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num02.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num03.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num04.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num05.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num06.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num07.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num08.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num09.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 ok.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 perfect.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 scene.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 score.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 shoot.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 skip.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 special.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 stick.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tbronze.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tgold.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 time.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tno.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 topscore.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 total.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tplat.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tsilver.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m0.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m1.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m2.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m3.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m4.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m5.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m6.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m7.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mash.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 📁 Minimal
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowright.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowup.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barnone.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 bonus.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 buttons.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 death.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk3.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 diskA.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 freeplay.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 getready.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hold.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hright.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hup.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 insertcoin.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 live.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m0.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m3.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m4.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m5.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m6.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m7.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mash.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 minus.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 nomoves.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num00.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num01.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num02.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num03.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num04.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num05.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num06.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num07.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num08.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num09.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 perfect.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 scene.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 score.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 shoot.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 skip.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 special.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 stick.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tbronze.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tgold.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tno.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 topscore.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 total.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tplat.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tsilver.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 minus.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 nomoves.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num00.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num01.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num02.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num03.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num04.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num05.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num06.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num07.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num08.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num09.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 ok.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 perfect.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 scene.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 score.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 secret.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 shoot.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 skip.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 special.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 stick.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tbronze.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tgold.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 time.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tno.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 topscore.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 total.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tplat.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tsilver.png
┃ ┃ ┃ ┃ ┃ ┣ 📁 Script
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 addons.singe
┃ ┃ ┃ ┃ ┃ ┣ 📁 Sounds
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 clear.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 coin.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 coin2.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 death.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 music.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 music2.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 right.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 roll.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 victory.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 wrong.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 Sugar_Rush.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 Sugar_Rush.txt
┃ ┃ ┃ ┃ ┃ ┣ 📁 Videos
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 Sugar_Rush.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 Sugar_Rush.ogg

#### Survival

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 survival.daphne
┃ ┃ ┃ ┃ ┃ ┣ 📁 Bezel
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 Survival.png
┃ ┃ ┃ ┃ ┃ ┣ 📁 Cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 default.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 game.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hscore.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s1.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s2.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s3.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s4.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s5.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s6.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s10.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s20.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s30.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s40.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 save.cfg
┃ ┃ ┃ ┃ ┃ ┣ 📁 Fonts
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 font.ttf
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 fontlo.ttf
┃ ┃ ┃ ┃ ┃ ┣ 📁 Overlay
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action2.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action3.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action4.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdl.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdr.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowright.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel1.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel2.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowul.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowup.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowur.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barnone.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 bonus.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 buttons.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 death.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk1.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk2.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk3.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 diskA.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 freeplay.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 getready.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hold.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hright.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hup.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 insertcoin.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 live.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 📁 Lores
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action3.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action4.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdl.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdr.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowright.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowul.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowup.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowur.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barnone.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 bonus.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 buttons.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 death.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk3.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 diskA.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 freeplay.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 getready.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hold.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hright.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hup.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 insertcoin.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 live.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m0.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m3.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m4.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m5.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m6.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m7.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mash.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 minus.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 nomoves.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num00.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num01.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num02.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num03.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num04.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num05.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num06.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num07.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num08.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num09.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 ok.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 perfect.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 scene.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 score.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 shoot.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 skip.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 special.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 stick.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tbronze.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tgold.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 time.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tno.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 topscore.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 total.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tplat.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tsilver.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m0.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m1.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m2.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m3.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m4.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m5.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m6.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m7.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mash.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 📁 Minimal
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action3.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action4.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdl.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdr.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowright.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowul.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowup.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowur.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barnone.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 bonus.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 buttons.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 death.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk3.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 diskA.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 freeplay.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 getready.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hold.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hright.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hup.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 insertcoin.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 live.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m0.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m3.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m4.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m5.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m6.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m7.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mash.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 minus.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 nomoves.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num00.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num01.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num02.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num03.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num04.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num05.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num06.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num07.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num08.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num09.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 ok.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 perfect.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 scene.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 score.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 secret.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 shoot.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 skip.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 special.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 stick.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tbronze.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tgold.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tno.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 topscore.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 total.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tplat.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tsilver.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 minus.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 nomoves.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num00.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num01.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num02.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num03.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num04.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num05.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num06.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num07.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num08.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num09.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 ok.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 perfect.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 scene.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 score.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 shoot.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 skip.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 special.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 stick.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tbronze.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tgold.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 time.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tno.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 topscore.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 total.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tplat.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tsilver.png
┃ ┃ ┃ ┃ ┃ ┣ 📁 Script
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 addons.singe
┃ ┃ ┃ ┃ ┃ ┣ 📁 Sounds
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 clear.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 coin.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 coin2.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 death.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 right.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 roll.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 victory.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 wrong.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 Survival.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 Survival.txt
┃ ┃ ┃ ┃ ┃ ┣ 📁 Videos
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 survival.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 survival.ogg

#### The Last Bounty Hunter HD

>This game doesn't use the framework described on top of this page.
{.is-info}

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 lbh-hd.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 lbh-hd.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 lbh-hd.txt
┃ ┃ ┃ ┃ ┃ ┣ 📁 Cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 lbh.cfg
┃ ┃ ┃ ┃ ┃ ┣ 📁 Fonts
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 helldorado.ttf
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m39.ttf
┃ ┃ ┃ ┃ ┃ ┣ 📁 Overlay
┃ ┃ ┃ ┃ ┃ ┣ 🗒 bullet.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 captured.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 credits.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 crosshaira.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 crosshairb.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 crosshairc.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 crosshaird.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 crosshaire.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 dead.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 dr.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hit.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hole.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num0.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num1.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num2.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num3.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num4.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num5.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num6.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num7.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num8.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num9.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 pistol.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 reload.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 shot.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 star.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 star2.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 ul.png
┃ ┃ ┃ ┃ ┃ ┣ 📁 Script
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 globals.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitbox-armybase.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitbox-bonus.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitbox-cactus1.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitbox-cactus2.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitbox-dan1.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitbox-dan2.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitbox-final.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitbox-harry1.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitbox-harry2.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitbox-harry3.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitbox-loco1.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitbox-loco2.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitbox-loco3.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitbox-showdows.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitmap-poster.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hscore.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 manage.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 service.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 setuplevel.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 toolbox.singe
┃ ┃ ┃ ┃ ┃ ┣ 📁 Sounds
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 bottlebreak.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 coin.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 empty.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 metal.ting.01.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 reload.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 shot.wav
┃ ┃ ┃ ┃ ┃ ┣ 📁 Video
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 lbh.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 lbh.ogg

#### Time Gal HD

>This game doesn't use the framework described on top of this page.
{.is-info}

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 timegal-hd.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 action.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 action2.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 action3.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdl.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdown.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdr.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowleft.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowright.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel2.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowul.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowup.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowur.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 bonus.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 clear.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 coin.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 death.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk1.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk2.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk3.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 diskA.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 font.ttf
┃ ┃ ┃ ┃ ┃ ┣ 🗒 freeplay.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 game.cfg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 getready.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 insertcoin.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 level.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 live.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 m0.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 m1.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 m2.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 m3.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 m4.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 m5.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 m6.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 mash.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 mfull.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 nomoves.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 num00.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 num01.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 num02.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 num03.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 num04.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 num05.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 num06.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 num07.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 num08.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 num09.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 ok.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 presstart.ttf
┃ ┃ ┃ ┃ ┃ ┣ 🗒 right.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 save.cfg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 scene.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 score.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 secret.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 skip.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 timegal-hd.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 timegal-hd.txt
┃ ┃ ┃ ┃ ┃ ┣ 🗒 timegal.m2v
┃ ┃ ┃ ┃ ┃ ┣ 🗒 timegal.ogg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 topscore.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 victory.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 wrong.wav

#### Titan AE

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 titanae.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 titanae.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 titanae.txt
┃ ┃ ┃ ┃ ┃ ┣ 📁 Cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 default.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 game.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hscore.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s1.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s2.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s3.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s4.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s5.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s6.cfg
┃ ┃ ┃ ┃ ┃ ┣ 📁 Fonts
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 font.ttf
┃ ┃ ┃ ┃ ┃ ┣ 📁 Overlay
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdl.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdr.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowright.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel1.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel2.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowul.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowup.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowur.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barnone.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 bonus.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 buttons.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 death.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk1.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk2.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk3.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 diskA.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 freeplay.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hold.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hright.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hup.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 insertcoin.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 live.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 📁 Lores
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdl.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdr.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowright.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowul.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowup.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowur.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barnone.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 bonus.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 buttons.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 death.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk3.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 diskA.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 freeplay.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hold.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hright.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hup.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 insertcoin.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 live.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m0.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m3.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m4.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m5.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m6.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m7.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mash.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 minus.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 nomoves.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num00.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num01.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num02.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num03.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num04.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num05.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num06.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num07.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num08.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num09.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 perfect.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 scene.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 score.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 shoot.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 skip.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 special.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 stick.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tbronze.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tgold.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tno.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 topscore.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 total.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tplat.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tsilver.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m0.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m1.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m2.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m3.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m4.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m5.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m6.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m7.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mash.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 📁 Minimal
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action3.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action4.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdl.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdr.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowright.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowul.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowup.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowur.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barnone.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 bonus.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 buttons.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 death.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk3.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 diskA.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 freeplay.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 getready.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hold.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hright.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hup.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 insertcoin.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 live.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m0.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m3.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m4.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m5.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m6.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m7.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mash.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 minus.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 nomoves.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num00.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num01.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num02.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num03.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num04.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num05.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num06.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num07.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num08.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num09.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 perfect.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 scene.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 score.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 shoot.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 skip.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 special.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 stick.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tbronze.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tgold.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tno.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 topscore.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 total.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tplat.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tsilver.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 minus.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 nomoves.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num00.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num01.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num02.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num03.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num04.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num05.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num06.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num07.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num08.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num09.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 perfect.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 scene.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 score.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 shoot.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 skip.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 special.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 stick.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tbronze.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tgold.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tno.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 topscore.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 total.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tplat.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tsilver.png
┃ ┃ ┃ ┃ ┃ ┣ 📁 Script
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 addons.singe
┃ ┃ ┃ ┃ ┃ ┣ 📁 Sounds
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 clear.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 coin.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 death.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 music.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 music2.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 right.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 roll.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 victory.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 wrong.wav
┃ ┃ ┃ ┃ ┃ ┣ 📁 Video
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 titanae.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 titanae.ogg

#### Triad Stone HD

>This game doesn't use the framework described on top of this page.
{.is-info}

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 triad-hd.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 action.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 addons.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdown.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowleft.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowright.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel1.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel2.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowup.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 barfull.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 barnone.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 bonus.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 buttons.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 chantze.m2v
┃ ┃ ┃ ┃ ┃ ┣ 🗒 chantze.ogg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 clear.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 coin.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 death.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 death.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 default.cfg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 default2.cfg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk1.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk2.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk3.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 diskA.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 font.ttf
┃ ┃ ┃ ┃ ┃ ┣ 🗒 framework.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 freeplay.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 game.cfg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 game2.cfg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 globals.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 hscore.cfg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 hscore.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 hscore2.cfg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 insertcoin.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 level.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 live.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 m0.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 m1.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 m2.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 m3.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 m4.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 m5.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 m6.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 m7.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 main.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 mash.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 mfull.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 minus.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 nomoves.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 num00.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 num01.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 num02.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 num03.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 num04.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 num05.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 num06.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 num07.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 num08.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 num09.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 ok.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 perfect.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 right.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 roll.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 s1.cfg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 s10.cfg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 s2.cfg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 s20.cfg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 s3.cfg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 s30.cfg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 s4.cfg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 s40.cfg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 s5.cfg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 s50.cfg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 s6.cfg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 s60.cfg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 scanlines.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 scene.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 score.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 service.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 service.ttf
┃ ┃ ┃ ┃ ┃ ┣ 🗒 skip.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 special.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 stick.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 toolbox.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 topscore.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 total.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 triad_HD.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 triad_HD.txt
┃ ┃ ┃ ┃ ┃ ┣ 🗒 victory.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 wrong.wav

>This game required to have the game `Chantze's Stone HD` in addition!
{.is-info}

#### Triad Stone

>This game doesn't use the framework described on top of this page.
{.is-info}

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 triad.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 action.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 action2.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 action3.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 action4.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 action5.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 addons.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdl.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdown.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdr.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowleft.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowright.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel2.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowul.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowup.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowur.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 blip02.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 bonus.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 buttons.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 clear.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 coin.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 death.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 death.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 default.cfg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk1.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk2.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk3.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 diskA.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 font.ttf
┃ ┃ ┃ ┃ ┃ ┣ 🗒 framework.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 freeplay.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 game.cfg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 getready.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 globals.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 hdown.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 hleft.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 hold.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 hright.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 hscore.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 hup.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 insertcoin.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 level-0.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 level-1.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 level.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 live.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 m0.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 m1.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 m2.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 m3.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 m4.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 m5.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 m6.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 m7.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 main.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 map.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 map.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 map2.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 mapblock.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 maptriad.m2v
┃ ┃ ┃ ┃ ┃ ┣ 🗒 maptriad.ogg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 mash.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 mfull.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 minus.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 nomoves.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 num00.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 num01.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 num02.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 num03.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 num04.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 num05.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 num06.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 num07.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 num08.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 num09.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 ok.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 opt.ttf
┃ ┃ ┃ ┃ ┃ ┣ 🗒 perfect.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 player.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 powerprompt.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 powerupblip.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 prompt.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 right.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 roll.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 save.cfg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 scene.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 score.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 secret.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 service.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 shoot.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 skip.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 special.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 stick.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 sword.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 swordright.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 tap.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 tbronze.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 tgold.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 tno.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 toolbox.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 topscore.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 total.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 tplat.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 triad.m2v
┃ ┃ ┃ ┃ ┃ ┣ 🗒 triad.ogg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 triad.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 triad.txt
┃ ┃ ┃ ┃ ┃ ┣ 🗒 trophy.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 tsilver.png

#### Tron

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 tron.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 tron.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 tron.txt
┃ ┃ ┃ ┃ ┃ ┣ 📁 Cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 default.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 game.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hscore.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s1.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s2.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s3.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s4.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s5.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s6.cfg
┃ ┃ ┃ ┃ ┃ ┣ 📁 Fonts
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 font.ttf
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 fontlo.ttf
┃ ┃ ┃ ┃ ┃ ┣ 📁 Overlay
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdl.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdr.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowright.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel1.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel2.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowul.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowup.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowur.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barnone.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 bonus.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 buttons.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 death.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk1.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk2.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk3.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 diskA.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 freeplay.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 getready.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hold.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hright.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hup.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 insertcoin.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 live.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 📁 Lores
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdl.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdr.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowright.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowul.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowup.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowur.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barnone.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 bonus.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 buttons.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 death.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk3.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 diskA.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 freeplay.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 getready.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hold.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hright.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hup.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 insertcoin.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 live.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m0.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m3.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m4.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m5.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m6.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m7.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mash.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 minus.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 nomoves.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num00.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num01.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num02.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num03.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num04.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num05.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num06.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num07.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num08.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num09.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 perfect.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 scene.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 score.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 shoot.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 skip.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 special.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 stick.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tbronze.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tgold.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tno.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 topscore.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 total.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tplat.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tsilver.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m0.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m1.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m2.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m3.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m4.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m5.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m6.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m7.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mash.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 📁 Minimal
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdl.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdr.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowright.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowul.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowup.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowur.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barnone.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 bonus.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 buttons.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 death.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk3.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 diskA.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 freeplay.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 getready.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hold.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hright.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hup.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 insertcoin.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 live.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m0.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m3.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m4.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m5.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m6.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m7.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mash.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 minus.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 nomoves.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num00.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num01.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num02.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num03.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num04.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num05.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num06.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num07.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num08.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num09.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 perfect.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 scene.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 score.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 shoot.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 skip.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 special.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 stick.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tbronze.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tgold.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tno.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 topscore.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 total.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tplat.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tsilver.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 minus.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 nomoves.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num00.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num01.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num02.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num03.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num04.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num05.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num06.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num07.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num08.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num09.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 perfect.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 scene.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 score.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 shoot.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 skip.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 special.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 stick.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tbronze.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tgold.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tno.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 topscore.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 total.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tplat.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tsilver.png
┃ ┃ ┃ ┃ ┃ ┣ 📁 Script
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 addons.singe
┃ ┃ ┃ ┃ ┃ ┣ 📁 Sounds
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 clear.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 coin.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 death.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 right.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 roll.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 victory.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 wrong.wav
┃ ┃ ┃ ┃ ┃ ┣ 📁 Video
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tron.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tron.ogg

#### Venus Wars

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 venuswars.daphne
┃ ┃ ┃ ┃ ┃ ┣ 📁 Cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 default.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 default2.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 game.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 game2.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hscore.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hscore2.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s1.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s2.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s3.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s4.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s5.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s6.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s10.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s20.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s30.cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 s40.cfg
┃ ┃ ┃ ┃ ┃ ┣ 📁 Fonts
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 font.ttf
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 fontlo.ttf
┃ ┃ ┃ ┃ ┃ ┣ 📁 Overlay
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action2.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action3.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action4.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdl.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdr.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowright.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel1.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel2.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowul.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowup.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowur.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barnone.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 bonus.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 buttons.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 death.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk1.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk2.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk3.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 diskA.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 freeplay.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 getready.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hold.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hright.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hup.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 insertcoin.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 live.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 loopl.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 loopr.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 📁 Lores
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action3.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action4.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdl.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdr.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowright.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowul.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowup.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowur.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barnone.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 bonus.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 buttons.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 death.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk3.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 diskA.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 freeplay.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 getready.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hold.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hright.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hup.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 insertcoin.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 live.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 loopl.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 loopr.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m0.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m3.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m4.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m5.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m6.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m7.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mash.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 minus.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 nomoves.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num00.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num01.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num02.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num03.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num04.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num05.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num06.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num07.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num08.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num09.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 ok.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 perfect.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 scene.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 score.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 shoot.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 skip.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 special.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 stick.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tbronze.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tgold.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 time.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tno.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 topscore.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 total.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tplat.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tsilver.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m0.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m1.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m2.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m3.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m4.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m5.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m6.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m7.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mash.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 📁 Minimal
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action3.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action4.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdl.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdr.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowright.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowsel2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowul.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowup.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowur.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 barnone.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 bonus.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 buttons.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 death.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 disk3.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 diskA.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 freeplay.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 getready.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hold.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hright.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hup.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 insertcoin.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 level.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 live.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 loopl.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 loopr.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m0.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m1.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m2.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m3.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m4.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m5.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m6.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 m7.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mash.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 mfull.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 minus.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 nomoves.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num00.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num01.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num02.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num03.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num04.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num05.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num06.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num07.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num08.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num09.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 ok.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 perfect.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 scene.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 score.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 secret.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 shoot.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 skip.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 special.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 stick.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tbronze.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tgold.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tno.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 topscore.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 total.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tplat.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tsilver.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 minus.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 nomoves.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num00.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num01.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num02.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num03.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num04.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num05.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num06.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num07.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num08.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num09.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 ok.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 perfect.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 scene.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 score.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 shoot.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 skip.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 special.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 stick.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tbronze.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tgold.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 time.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tno.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 topscore.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 total.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tplat.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tsilver.png
┃ ┃ ┃ ┃ ┃ ┣ 📁 Script
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 addons.singe
┃ ┃ ┃ ┃ ┃ ┣ 📁 Sounds
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 clear.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 coin.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 coin2.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 death.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 music.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 music2.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 right.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 right1.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 roll.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 victory.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 wrong.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 Venus_Wars.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 Venus_Wars.txt
┃ ┃ ┃ ┃ ┃ ┣ 📁 Videos
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 Venus_Wars.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 Venus_Wars.ogg

#### Who Shot Johnny Rock? HD (Enhanced)

>This game doesn't use the framework described on top of this page.
{.is-info}

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 johnnyrocknoir.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 anchorsteamnf.ttf
┃ ┃ ┃ ┃ ┃ ┣ 🗒 break.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 cashregister.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 coin.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 coinstable.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 crosshaira.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 crosshairb.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 crosshairc.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 crosshaird.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 crosshaire.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 empty.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 globals.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 gunempty.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 gunhit.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 gunreload.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 gunshot.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 hit.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitbox-cans.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitbox-casino.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitbox-elevator.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitbox-final1.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitbox-final2.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitbox-final3.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitbox-final4.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitbox-garage1.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitbox-garage2.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitbox-mansion.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitbox-office.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitbox-pool1.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitbox-pool2.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitbox-pool3.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitbox-poolballs.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitbox-random.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitbox-warehouse1.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitbox-warehouse2.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitbox-warehouse3.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitbox-warehouse4.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitbox-warehouse5.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitbox-wareintro.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitmap-city.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitmap-items.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitmap-license.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitmap-pool.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitmap-store.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 hscore.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 johnnyrock.cfg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 johnnyrock.m2v
┃ ┃ ┃ ┃ ┃ ┣ 🗒 johnnyrock.ogg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 johnnyrocknoir.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 johnnyrocknoir.txt
┃ ┃ ┃ ┃ ┃ ┣ 🗒 m39.ttf
┃ ┃ ┃ ┃ ┃ ┣ 🗒 manage.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 panel.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 recoil.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 service.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 setuplevel.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 setuprandom.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 shot.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 tommygun.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 tommyshot.wav
┃ ┃ ┃ ┃ ┃ ┣ 🗒 toolbox.singe

#### Who Shot Johnny Rock? HD

>This game doesn't use the framework described on top of this page.
{.is-info}

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 johnnyrock-hd.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 johnnyrock-hd.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 johnnyrock-hd.txt
┃ ┃ ┃ ┃ ┃ ┣ 📁 Cfg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 johnnyrock.cfg
┃ ┃ ┃ ┃ ┃ ┣ 📁 Fonts
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 anchorsteamnf.ttf
┃ ┃ ┃ ┃ ┃ ┣ 📁 Overlay
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 crosshaira.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 crosshairb.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 crosshairc.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 crosshaird.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 crosshaire.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hit.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hit1.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hit2.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hit3.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hit4.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hit5.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num0.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num1.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num2.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num3.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num4.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num5.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num6.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num7.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num8.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 num9.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 panel.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 shot.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tommygun.png
┃ ┃ ┃ ┃ ┃ ┣ 📁 Script
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 globals.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitbox-cans.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitbox-casino.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitbox-elevator.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitbox-final1.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitbox-final2.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitbox-final3.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitbox-final4.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitbox-garage1.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitbox-garage2.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitbox-mansion.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitbox-office.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitbox-pool1.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitbox-pool2.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitbox-pool3.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitbox-poolballs.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitbox-random.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitbox-warehouse1.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitbox-warehouse2.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitbox-warehouse3.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitbox-warehouse4.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitbox-warehouse5.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitbox-wareintro.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitmap-city.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitmap-items.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitmap-license.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitmap-pool.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hitmap-store.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 hscore.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 manage.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 service.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 setuplevel.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 setuprandom.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 toolbox.singe
┃ ┃ ┃ ┃ ┃ ┣ 📁 Sounds
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 break.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 cashregister.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 coin.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 coinstable.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 credit.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 empty.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 gunempty.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 gunhit.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 gunreload.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 gunshot.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 recoil.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 tommyshot.wav
┃ ┃ ┃ ┃ ┃ ┣ 📁 Video
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 johnnyrock.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 johnnyrock.ogg
