---
title: Libretro MinivMac
description: 
published: true
date: 2024-06-22T11:38:26.148Z
tags: libretro, 9.0+, minivmac
editor: markdown
dateCreated: 2023-02-24T17:36:00.699Z
---



## ![](/emulators/license.svg) Licence

Ce core est sous licence [**GPLv2**](https://github.com/libretro/libretro-minivmac/blob/master/minivmac/COPYING.txt).

## ![](/emulators/compatibility.png) Compatibilité

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ❌ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Fonctionnalités

| Fonctionnalité | Supporté |
| :---: | :---: |
| Captures d'écran | ✔ |
| Options du core | ✔ |
| Contrôles | ✔ |
| Remapping | ✔ |

## ![](/emulators/bios.svg) BIOS

### Liste des bios optionnels

| Nom de fichier | Description | MD5 | Fourni |
| :---: | :---: | :---: | :---: |
| MacII.ROM | - | 66223be1497460f1e60885eeb35e03cc | ❌ |
| MinivMacBootv2.dsk | System 7.5.5 Boot Disk | a249565f03b98d004ee7f019570069cd | ❌ |

### Emplacement

Placez les bios comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 macintosh
┃ ┃ ┃ ┃ ┣ 🗒 **MacII.ROM**
┃ ┃ ┃ ┃ ┣ 🗒 **MinivMacBootv2.dsk**

## ![](/emulators/roms.png) Roms

### Extensions supportées

Les roms doivent avoir les extensions suivantes :

* .cmd
* .dsk
* .hvf
* .img
* .zip

Ce système supporte les roms compressées au format .zip. Attention toutefois, il ne s'agit que d'une archive.

Les fichiers contenus dans les .zip doivent correspondre aux extensions citées précédemment.
De plus, chaque fichier .zip ne doit contenir qu'une seule rom compressée.

### Emplacement

Placez les roms comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 macintosh
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.zip**

## ![](/emulators/advanced-configuration.png) Configuration avancée de l'émulateur

>Pour pouvoir conserver vos configurations personnalisées lors d'une mise à jour, nous vous conseillons d'utiliser notre fonctionnalité [Surcharges de configuration](./../../../advanced-usage/configuration-override).
{.is-info}

### Accéder aux options

Vous pouvez configurer diverses options de deux façons différentes.

* Via le menu RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via le fichier `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Options du core

| Options | Valeur des options | Variable (pour surcharge) | Valeurs des variables |
| --- | --- | --- | --- |
| Status Bar | `Désactivé` ✅ / `Activé` | `minivmac_Statusbar` | `disabled` / `enabled` |
| Keyboard Type | `Callback` ✅ / `Poll` | `minivmac_kbdtype` | `Callback` / `Poll` |

## ![](/emulators/external-links.png) Liens externes

* **Code source utilisé** : [https://github.com/libretro/libretro-minivmac/](https://github.com/libretro/libretro-minivmac/)
* **Documentation Libretro** : [https://docs.libretro.com/library/minivmac/](https://docs.libretro.com/library/minivmac/)