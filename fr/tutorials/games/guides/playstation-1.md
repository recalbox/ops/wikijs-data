---
title: Playstation 1
description: 
published: true
date: 2021-12-12T16:54:03.600Z
tags: sony, ps1, playstation 1
editor: markdown
dateCreated: 2021-05-21T08:37:33.579Z
---

Ici, vous pourrez découvrir plusieurs tutoriels liés à la PlayStation.

Voici les tutoriels disponibles :

[Activer le mode analogue](enable-analog-mode)
[Convertir les images disque .BIN en .ISO](convert-psx-bin-to-iso)
[Convertir les images disque en .BIN + .CUE](convert-disc-files-to-bin+cue)
[Convertir une sauvegarde .GME en .SRM](convert-gme-save-to-srm)
[Dumpez vos propres jeux rapidement](dump-you-own-games-quickly)
[FAQ](faq)
[Gestion des cartes mémoire](memory-cards-management)
[Gestion des multi-disques au format .M3U](multidisc-management-with-m3u)
[Gestion des multi-disques au format .PBP](multidisc-management-with-pbp)
[Récupérer le bios ps1_rom.bin depuis le firmware PS3](grab-ps1-bios-from-ps3-firmware)
[Utiliser vos sauvegardes ePSXe (.MCR) dans RetroArch (.SRM)](use-epsxe-mcr-saves-into-srm)