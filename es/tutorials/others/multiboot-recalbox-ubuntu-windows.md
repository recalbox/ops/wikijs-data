---
title: Multiboot Recalbox / Ubuntu / Windows
description: Arrancad Recalbox / Ubuntu / Windows en cualquier order y momento !
published: false
date: 2023-09-18T21:05:08.302Z
tags: windows, arranque, multiboot, multiarranque, ubuntu, orden
editor: markdown
dateCreated: 2023-09-18T10:12:10.551Z
---

![](/tutorials/others/multiboot/0000.png){.full-width}

## Introducción

En el mundo de la informática existen varios sistemas operativos (SO), cada uno con sus propias ventajas e inconvenientes, y es posible que tengas que utilizar distintos SO en una misma estación de trabajo.

## ¿Por qué?

Para poder elegir qué SO utilizar de forma rápida y sencilla.

## Requisitos previos

## Un PC con Windows 10 con una cuenta de "Administrador" de Windows y al menos un procesador de doble núcleo de 2GHz y 4Gb de RAM.

* Una versión para PC de la llave USB Recalbox ya configurada y funcionando.

* Descargar Ubuntu en versión LTS (preferiblemente) [aquí](https://ubuntu.com/download/desktop) y grabar el archivo .ISO descargado en un DVD o Llave USB.

>LTS => Long Time Support => En resumen, actualizaciones soportadas durante al menos 5 años.
{.is-info}

>Si su disco Windows no es un SSD sino un HDD: ejecute una desfragmentación para obtener el máximo espacio cuando reduzca su partición Windows.
{.is-warning}

>En raras ocasiones. Puede ocurrir que el arranque seleccionado no tenga éxito. No se preocupe, reinicie su PC. Y vuelva a seleccionar su Boot.
{.is-info}

## 1 - Reducción de la partition Windows 10

Let's Start the Party ;-)

Inicie su ordenador en Windows, luego en el menú de inicio, escriba `Computer Management` en el cuadro de búsqueda y haga clic en `Computer Management`.

![](/tutorials/others/multiboot/1000.png){.full-width}

Se abrirá la ventana Gestión de ordenadores. Haga clic en `Gestión de discos` en la columna de la izquierda.

![](/tutorials/others/multiboot/1001.png){.full-width}

Haga clic con el botón derecho en la partición (C:) que es la partición principal de Windows. Aparecerá un menú emergente, haga clic en `Reducir volumen...`.

![](/tutorials/others/multiboot/1002.png){.full-width}

Aparecerá una nueva ventana pidiéndole que espere (el tiempo de espera variará en función de su PC).

![](/tutorials/others/multiboot/1003.png){.full-width}

A continuación aparece la ventana `Reducir C:`. Se le pedirá que introduzca el tamaño de su partición Windows (espacio libre en su disco duro). El tamaño mínimo a liberar para una instalación Linux es de 32GB. Para facilitar su uso, le sugerimos que lo reduzca en 100GB en mi ejemplo en un disco duro de 500GB. 

Por lo tanto, tienes que introducir el siguiente valor en MB, es decir, `100000`, y hacer clic en `Reducir` para iniciar el proceso, que tardará más o menos tiempo dependiendo de si tu PC está equipado con un SSD o HDD.

![](/tutorials/others/multiboot/1004.png){.full-width}

Una vez finalizado el proceso, verás aquí una partición `Unallocated` de `97.66GB`.

![](/tutorials/others/multiboot/1005.png){.full-width}

Eso es todo, la primera etapa de la reducción de la partición se ha completado. Antes de apagar el PC, inserte la llave USB o el CD de instalación de Ubuntu.

## 2 - Instalar Ubuntu

Arranca tu sistema desde tu DVD o USB de Ubuntu y espera pacientemente hasta que aparezca la siguiente pantalla. A continuación, seleccione el idioma `Español` y haga clic en `Instalar Ubuntu`.

![](/tutorials/others/multiboot/2001.png){.full-width}

A continuación, en la pantalla `Disposición del teclado`, déjelo como se indica a continuación y haga clic en `Continuar`.

![](/tutorials/others/multiboot/2002.png){.full-width}

Esto le llevará a la pantalla `Actualizaciones y otro software`. Deberías marcar la casilla `Instalar software de terceros...` antes de pulsar `Continuar`. Si eres nuevo en Linux, esto te ayudará a automatizar el proceso.

![](/tutorials/others/multiboot/2003.png){.full-width}

En la pantalla `Tipo de instalación`, deje marcada la opción `Instalar Ubuntu junto con Windows 10`. El software de instalación detectará automáticamente el espacio no asignado recuperado anteriormente en el apartado "Reducir partición de Windows". A continuación, haga clic en "Instalar ahora".

![](/tutorials/others/multiboot/2004.png){.full-width}

Aparecerá la ventana "¿Debo aplicar los cambios a los discos? Haga clic en `Continuar` para permitir que el software de instalación escriba las nuevas particiones en su disco en el área previamente no asignada. Este paso es necesario para completar la instalación de Ubuntu.

![](/tutorials/others/multiboot/2005.png){.full-width}

En la ventana "Dónde estás", haz clic en tu zona horaria o deja París por defecto y haz clic en "Continuar".

![](/tutorials/others/multiboot/2006.png){.full-width}

En la ventana "¿Quién eres?", te dejamos que introduzcas la información que desees. Necesitarás recordar esto para los siguientes pasos.

>Esta cuenta será tu cuenta de administrador por defecto en Ubuntu.
{.is-warning}

![](/tutorials/others/multiboot/2007.png){.full-width}

Comenzará el proceso de instalación, que durará más o menos tiempo, dependiendo de su PC y de su disco duro. Lea los distintos mensajes durante la fase de instalación para familiarizarse con las características de Ubuntu.

![](/tutorials/others/multiboot/2007b.png){.full-width}

Una vez finalizada la instalación, la interfaz le pedirá que haga clic en Reiniciar ahora. Haga clic en "Reiniciar ahora".

![](/tutorials/others/multiboot/2008.png){.full-width}

La interfaz le pide que retire el medio de instalación. Para ello, desconecte la llave USB o extraiga el DVD de instalación de Ubuntu de la unidad y, a continuación, pulse `Intro` en el teclado.

![-](/tutorials/others/multiboot/2008b.png){.full-width}

Una vez que su PC se haya reiniciado, accederá directamente a la interfaz de selección de SO llamada GRUB, como se muestra a continuación.
Tiene tres opciones:

1) No tocar nada Ubuntu se iniciará automáticamente (preferible al principio);
2) Usa las flechas arriba y abajo de tu teclado para detener el temporizador;
3) Sección Windows para volver a Windows antes de continuar (o para tomar un descanso).

![](/tutorials/others/multiboot/2009.png){.full-width}

Llegará a la pantalla de inicio de sesión, seleccione su única cuenta e introduzca su contraseña.

![](/tutorials/others/multiboot/2010.png){.full-width}

Su instalación de Ubuntu se ha completado y verá la siguiente pantalla: su escritorio de Ubuntu.

![](/tutorials/others/multiboot/2011.png){.full-width}

## 3 - Preparar el software necesario en Ubuntu

Inicie su PC en Ubuntu. Identifíquese y obtenga el software como se muestra a continuación.

Haga clic en el icono `Ubuntu Software` en la barra de menú a la izquierda de su escritorio.

![](/tutorials/others/multiboot/3001.png){.full-width}

Es posible que te pidan actualizaciones, así que déjate guiar y actualiza hasta que finalmente reinicies el ordenador. La clave es volver atrás y hacer clic en el icono `Ubuntu Software` al final de estas actualizaciones para llegar a la ventana `Ubuntu Software` de abajo (equivalente a una AppStore, PlayStore o Microsoft Store). Haga clic en la lupa de búsqueda y escriba `Grub Cutomizer`.

![](/tutorials/others/multiboot/3002.png){.full-width}

![](/tutorials/others/multiboot/3004.png){.full-width}

A continuación, haz clic en el icono `Grub Cutomizer` y en `Install` en la siguiente ventana, e introduce tu contraseña para confirmar.

![](/tutorials/others/multiboot/3005.png){.full-width}

Y salga de la ventana `Ubuntu Software`. A continuación, haga clic en "Mostrar aplicaciones" en la esquina inferior izquierda del escritorio.

![](/tutorials/others/multiboot/3007.png){.full-width}

A continuación verá el menú de búsqueda de sus aplicaciones.

![](/tutorials/others/multiboot/3008.png){.full-width}

Visualmente o utilizando el menú de búsqueda, encuentra los iconos `Grub Customizer` y `Terminal` pulsando con el botón derecho del ratón sobre cada uno de ellos para añadirlos a tus favoritos.

![](/tutorials/others/multiboot/3009.png){.full-width}

Aparecerán en la barra lateral izquierda de su escritorio.

![](/tutorials/others/multiboot/3011.png){.full-width}

## 4 - Copia de seguridad de su GRUB actual

>¡Haga una copia del archivo grub.cfg original antes de hacer cualquier cambio!
{.is-warning}

Haz click en el icono del escritorio con tu nombre de usuario para abrir una ventana del explorador, luego `Otras ubicaciones`, luego `Ordenador`, luego `Boot` y finalmente `grub`.

![](/tutorials/others/multiboot/4000.png){.full-width}

Guarde una copia del archivo `grub.cfg` en una memoria USB, disco duro externo o enviándoselo por correo electrónico.

## 5 - Organizando el GRUB (arranque)

>A partir de este punto, puede haber diferencias entre su menú y los menús que aparecen a continuación debido a la configuración de los distintos PCs. Por ello vamos a explicarte la filosofía de las operaciones a realizar según el siguiente ejemplo para llegar al menú que deseas tener.
{.is-info}

En primer lugar, vamos a cambiar el orden de inicio que desees al arrancar tu ordenador.

Haz clic en el icono `Grub Customizer`, introduce tu contraseña y espera a que se abra la ventana que aparece a continuación.

![](/tutorials/others/multiboot/4001.png){.full-width}

A continuación encontrará las correspondencias con el menú de inicio de su PC:

![](/tutorials/others/multiboot/5000.png){.full-width}

Verá que la estructura de árbol es la misma. Los iconos de directorio representan los submenús y las ruedas dentadas representan las diferentes puestas en marcha posibles.

Puede organizar las puestas en marcha en el orden que desee.

>Sin ninguna acción por tu parte, al final del temporizador, el arranque situado al principio de la lista será el arranque por defecto de tu PC.
{.is-info}

En nuestro ejemplo, realizaremos las siguientes operaciones para obtener la siguiente lista al arrancar el PC:

* Windows 10
* Ubuntu
* Opciones avanzadas para Ubuntu
  * Ubuntu, con Linux 5.8.0-38-generic
  * Ubuntu, con Linux 5.8.0-38-generic (recovery mode)
  * Ubuntu, con Linux 5.4.0-42-generic
  * Ubuntu, con Linux 5.4.0-42-generic (recovery mode)
  * Memory test (memtest86+)
  * Memory test (memtest86+, serial console 115200)

Para obtener esta estructura, primero selecciona la línea `Windows 10 (on /dev/sha1)` y haz clic con el botón derecho del ratón, luego haz clic en `Rename`. Borra los caracteres sobrantes para obtener `Windows 10` y pulsa `Enter` en el teclado para terminar de escribir.

Con la línea de Windows 10 seleccionada, usa las flechas arriba/abajo en la barra de menú de la ventana `Grub Customizer` de abajo para poner la línea de Windows 10 en la 1ª línea.

![](/tutorials/others/multiboot/5002.png){.full-width}

Obtendrás la siguiente clasificación:

![](/tutorials/others/multiboot/5004.png){.full-width}

Uitlizad la misma función para poner `Memory test (memtest86+)` y `Memory test (memtest86+, serial console 115200)` dentro del submenú `Options avancées pour Ubuntu`.

>Si hay inicio(s) que no quieres que aparezcan al arrancar el PC, te aconsejamos que los pongas en un submenú que puedes crear con las flechas izquierda/derecha.
{.is-info}

![](/tutorials/others/multiboot/5003.png){.full-width}

>¡No borres el/los boot(s) que no vayas a utilizar! Pueden usarse para solucionar problemas.
{.is-warning}

Una vez realizados todos estos cambios, debería tener el menú de arranque configurado como se muestra a continuación:

![](/tutorials/others/multiboot/5005.png){.full-width}

Tras una última comprobación, haz clic en el icono `Guarda tu configuración` y espera a que se complete la actualización de la configuración antes de salir del `Grub Customiser`.

Ahora reinicie su PC para ver la nueva pantalla de inicio:

![](/tutorials/others/multiboot/5008.png){.full-width}

## 6 - Añade la línea de arranque Recalbox

>En tu bios, asegúrate de que tu PC arranca primero en el HDD. Si este no es el caso, ¡haz el cambio!
{.is-warning}

>RECORDATORIO: Conecta tu llave USB Recalbox en tu PC. ¡Esta llave debe haber sido arrancada con éxito al menos una vez en este PC antes del inicio de este tutorial!
{.is-warning}

Enciende tu PC con la llave USB Recalbox conectada y arranca Ubuntu. Una vez que Ubuntu haya arrancado, deberías ver los tres iconos USB `RECALBOX` / `OVERLAY` / `SHARE` en el menú lateral izquierdo del escritorio de Ubuntu. Estos tres iconos corresponden a las 3 particiones `RECALBOX` / `OVERLAY` / `SHARE` de tu memoria USB.

![](/tutorials/others/multiboot/6001.png){.full-width}

Vuelve al `Customiser Grub` y haz click en el icono ![] (/tutorials/others/multiboot/6002a.png) para abrir una nueva ventana del Editor de Entradas como sigue:

* Nombre: introduzca el texto `Recalbox
* Tipo: seleccione `Chain Loader` en el menú desplegable.
* `Partition`: seleccione la partición de su unidad USB llamada `Recalbox`.
* Secuencia de arranque: esta casilla se rellena automáticamente cuando se selecciona la partición.

Esto es lo que deberías ver en la pantalla:

![](/tutorials/others/multiboot/6003.png){.full-width}

Dependiendo de su PC, la línea `Partition` puede ser diferente y del tipo `/dev/sd__(RECALBOX, vfat)`.

Del mismo modo, dependiendo de su PC, la 1ª línea de la `Secuencia de arranque` puede ser diferente y del tipo `set root='(hd_,_)'` :

* El primer número es el del disco en cuestión en su tabla de particiones (la numeración de los discos empieza por 0 para el primero),
* El segundo número es el de la partición en cuestión (la numeración de la partición empieza por 1 para la primera).

Tal como está, la `Secuencia de Arranque` no le permitirá arrancar Recalbox en su llave USB. Para corregir el problema, mantenga su 1ra línea del tipo `set root='(hd_,_)'` luego reemplace las siguientes líneas con el texto de abajo:

```shell
search --no-floppy --fs-uuid --set=root C8E4-A108
set gfxpayload=auto
linux /boot/linux label=RECALBOX console=tty3 quiet loglevel=0
initrd /boot/initrd.gz
```

>Tenga cuidado con la parte `C8E4-A108`, que probablemente tendrá un nombre diferente en su instalación. ¡Utilice la misma serie de letras y números que en su instalación!
{.is-warning}

En mi caso, obtengo la siguiente ventana con `Type` que cambia automáticamente de `Chain Loader` a `Other` (lo cual es normal).

![](/tutorials/others/multiboot/6005.png){.full-width}

Haz clic en `Validar` y saca el inicio de Recalbox que se acaba de crear entre Windows 10 y Ubuntu para obtener el siguiente resultado:

![](/tutorials/others/multiboot/6006.png){.full-width}

Haga clic en `Guardar` para guardar los cambios. Cuando reinicies tu PC, obtendrás la pantalla de inicio que se muestra a continuación. En este punto, ya puedes arrancar en cualquiera de los tres SO: Windows 10, Recalbox o Ubuntu.

![](/tutorials/others/multiboot/6007.png){.full-width}

Esto marca el final de la configuración básica de Multiboot Grub. Puedes parar aquí o puedes continuar personalizando gráficamente tu pantalla de inicio y añadir características interesantes.

## 7 - Añadir un tema Recalbox al Grub

En primer lugar, vaya a Ubuntu, abra la página actual en él y descargar el archivo de abajo. Por defecto, el archivo se guardará en el directorio `Downloads` de tu cuenta de Ubuntu.

[Recalbox.zip](https://mega.nz/file/zAIUBTpT#dz7umAN-q39Xd_agltjb5_5cle-GX9TSY4XGFjqgf9g)

Inicie el `Grub Customizer` y vaya a la pestaña `Appearance Settings`. Haga clic en el icono `+` para añadir un nuevo tema. En el ejemplo, el archivo se subió automáticamente a `/home/user/Downloads/`. Para que aparezca tu archivo .zip, haz click en `Archive Files` y selecciona `All Files` y luego haz doble click en el archivo Recalbox.zip para seleccionar el nuevo tema.

![](/tutorials/others/multiboot/7001.png){.full-width}

Por último, haga clic en el icono `Guardar` del Personalizador de Grub y cierre el Personalizador de Grub.

![](/tutorials/others/multiboot/7002.png){.full-width}

Reinicia tu ordenador para ver tu nuevo theme instalado.

![](/tutorials/others/multiboot/7003.png){.full-width}

>La imagen de arriba está en 4:3 porque no puedo darte una captura de pantalla de la puesta en marcha en el formato original 16:9 1080p. Por esta razón, todas las imágenes de bios que siguen estarán siempre en 4:3 (a diferencia de tu pantalla si tienes una pantalla 16:9).
{.is-info}

>Falta el icono Recalbox. Esto es normal en esta etapa. Para añadirlo, sigue el tutorial de abajo...
{.is-warning}

## 8 - Personalización manual de GRUB (opcional)

>A partir de este punto, si vuelves al `Grub Customizer`, ¡simplemente reabriendo la aplicación automáticamente recarga la configuración de Grub en la etapa que dejaste en §7 arriba!
{.is-warning}

Comencemos haciendo una copia de seguridad de su archivo original `grub.cfg` recuperando su archivo desde `/boot/grub/grub.cfg`. Para hacer esta copia, haga clic en el icono `Archivo` en la barra lateral izquierda de su ordenador (equivalente al Explorador de Archivos en Windows) y siga los pasos 1, 2, 3 y 4 para encontrar su archivo grub.cfg.

![](/tutorials/others/multiboot/8000.png){.full-width}

Haga clic con el botón derecho en el archivo `grub.cfg` y seleccione copiar y luego pegar, idealmente en un medio externo como una memoria USB, o también puede enviárselo a usted mismo por correo electrónico.

>¡Este paso le permitirá recuperar información importante que puede haber borrado inadvertidamente!
{.is-warning}

### Añade un icono para tu línea Recalbox a la pantalla de inicio de Grub

Ahora vamos a escribir en el archivo `grub.cfg`. Para ello, abra una ventana de Terminal haciendo clic en el icono de Terminal en la barra lateral izquierda del escritorio.

![](/tutorials/others/multiboot/8001.png){.full-width}

Escribid esta línea y pulsad `Enter` en vuestro teclado.

```shell
sudo gedit /boot/grub/grub.cfg
```

Se os pedirá vuestra contraseña. Escribidla y pulsad `Enter`.

![](/tutorials/others/multiboot/8100.png){.full-width}

>Cuando introduzca su contraseña en un terminal Linux, los caracteres no aparecerán, lo cual es normal en Linux.
{.is-info}

Se abrirá la siguiente ventana de modificación del fichero grub.cfg y podrá cerrar la ventana del terminal permanentemente.

![](/tutorials/others/multiboot/8101.png){.full-width}

En el fichero `grub.cfg`, reemplazad :

```ini
menuentry "Recalbox"{
```

Por:

```ini
menuentry "Recalbox" --class recalbox {
```

Como se muestra en esta imagen:

![](/tutorials/others/multiboot/8102.png){.full-width}

Haz clic en `Guardar` y reinicia tu ordenador para que aparezca el icono de Recalbox al arrancar el ordenador, o continúa con el resto del tutorial para añadir las otras funciones que se muestran a continuación antes de reiniciar.

![](/tutorials/others/multiboot/8103.png){.full-width}

### Añadir la función Detener y/o Reiniciar al arranque del PC

Siga los pasos de la sección anterior para editar el fichero `grub.cfg`.

A continuación, copie la siguiente línea al final del fichero `grub.cfg` para añadir la función **Reiniciar ordenador** al menú de arranque.

```shell
### BEGIN Restart-Option ###
menuentry "Restart" --class restart {reboot}
### END Restart-Option ###
```

Si quieres añadir la función **Apagar ordenador** al menú de inicio, copia la línea de abajo al final del archivo grub.cfg:

```shell
### BEGIN Shutdown-Option ###
menuentry "Shutdown" --class shutdown {halt}
### END Shutdown-Option ###
```

Podéis grabar estas modificaciones y reiniciar el ordenador.

El resultado final:

![](/tutorials/others/multiboot/8201.png){.full-width}

Por ejemplo, sin sobrescribir ninguna imagen, en un ordenador portátil, el resultado es el siguiente:

![](/tutorials/others/multiboot/0000.png){.full-width}