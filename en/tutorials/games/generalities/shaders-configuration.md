---
title: Shaders configuration
description: 
published: true
date: 2021-08-08T13:51:39.053Z
tags: configuration, shaders
editor: markdown
dateCreated: 2021-08-07T15:45:18.211Z
---

## Introduction

Shaders are filters that can modify the image you get when you use your emulators. It is often used to get a more "retro" image, close to the image we played on back then.

The different types of files you have in Recalbox are :

* **GLSL** : the shader itself.
* **GLSLP** : the preset shader. Can combine several shaders.
/recalbox/share/system/configs/shaders/**xxx.cfg** : the shader pack configuration files.

The shader configuration file (`.CFG`) references a shader preset (`.GLSLP`) for each Recalbox system. Each `.GLSLP` file references one or more shaders (`.GLSL`).

## Predefined shaders

With predefined shaders, you can set up shaders for all your systems in one option.

Predefined shaders are created by the community, and are optimized with each new version of Recalbox.

Here is the list of predefined shaders:

* `NONE`: no active shader.
* `SCANLINES`: enable scan lines on all emulators.
* `RETRO`: selects the "best" shaders for each system, chosen by the community, which will give you the closest gaming experience to the original.

You can set the shader pack you want to activate in the `GAME SETTINGS` menu in EmulationStation.

## Custom Shaders

There are 2 ways to set shaders in Recalbox.

### Global shader

You can choose to use one shader for all systems you play. To do this, follow these steps:

* In EmulationStation, open the menu with `START`.
* Go to `GAME SETTINGS` > `SHADERS`.
* Choose the desired shader and it will be applied to the launch of each game.

### Shader by system

>This is for more experienced users.
{.is-info}

You can combine the power of shader packs with the extensive customization of recalbox.conf. The `global.shaderset` can define a basic shader for each emulator. You can then change the shader preset for a system with the `systemname.shader` option. The default shader presets are defined in `/recalbox/share_init/shaders/`

For example, if I want:

* `RETRO` shaders on all emulators,
* no shaders for my Gameboy,
* an existing shader preset for the Megadrive,
* and a custom shader preset for my Snes

```ini
# Set the retro shader set for all emulators, now i know that i have the best selection
global.shaderset=retro
# But my gameboy seem better for me with no shaders
gb.shaders=
# We use the already existing 4xbr_retro.glslp shader for megadrive
megadrive.shaders=/recalbox/share_init/shaders/4xbr_retro.glslp
# And i want to apply my own shader preset on snes
snes.shaders=/recalbox/share/shaders/custom/snes.glslp
```

You can also change shaders in game using your controller.  
Use the special commands `Hotkey + R2` or `Hotkey + L2` to go to the next or previous shader.