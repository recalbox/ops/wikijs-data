---
title: Libretro Lutro
description: 
published: true
date: 2024-07-14T19:17:13.882Z
tags: libretro, lutro
editor: markdown
dateCreated: 2023-09-11T06:15:18.022Z
---

**Lutro** es un framework experimental de juegos en lua que sigue la API de LÖVE. Los juegos Lutro se pueden jugar con LibRetro / RetroArch a través del kernel Lutro.

## ![](/emulators/license.svg) Licencia

El core posee la siguiente licencia [**MIT**](https://github.com/libretro/libretro-lutro/blob/master/LICENSE).

## ![](/emulators/compatibility.png) Compatibilidad

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | 
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Funcionalidades

| Funcionalidad | ¿Soportada? |
| :---: | :---: |
| Controles | ✔ |
| Remappage | ✔ |

## ![](/emulators/bios.svg) BIOS

>**No necesita bios.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Extensions supportées

Les roms doivent avoir l'extension :

* .lutro
* .lua

### Extensiones de roms soportadas

Las roms deben tener las extensiones siguientes:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 lutro
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.lutro**

## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

>**Este core no tiene opciones.**
{.is-success}

## ![](/emulators/external-links.png) Enlaces externos

* **Código fuente utilizado** : [https://github.com/libretro/libretro-lutro](https://github.com/libretro/libretro-lutro)
* **Documentación Libretro** : [https://docs.libretro.com/library/lutro/](https://docs.libretro.com/library/lutro/)