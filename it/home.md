---
title: Benvenuti nella Wiki di Recalbox!
description: La pagina di presentazione di Recalbox.
published: true
date: 2024-06-02T16:54:13.124Z
tags: first start, how to, welcome, home
editor: markdown
dateCreated: 2021-06-17T18:56:45.279Z
---

![welcome.png](/home/welcome.png)

## Che cos'è Recalbox?

[Recalbox](https://www.recalbox.com) è la **console definitiva per retro-gaming** che ti permette di rigiocare a tutti i giochi di console e computers della tua infanzia.

Con [Recalbox](https://www.recalbox.com), (ri)scopri e (ri)gioca a tutti i titoli che hanno formato il mondo dei videogiochi con estrema facilità!

[Recalbox](https://www.recalbox.com) può essere installato su degli economici microcomputer come il Raspberry Pi, ed anche sul tuo PC (vecchio o nuovo)!

## Voglio installare Recalbox! Da dove comincio?

Per installare recalbox, vai sulla pagina [Preparazione e Installazione](./basic-usage/preparation-and-installation).

Per imparare di più rigurado i sistemi supportati, visita la sezione sulla [Compatibilità Hardware](./hardware-compatibility).

## Sto cercando una pagina specifica!

Avvantaggiati con la funzione cerca della wiki!
Puoi semplicemente scrivere una parola nel box di ricerca...
![search-text.png](/home/search-text.png)
Oppure, puoi cercare con i tag.
![search-tags.png](/home/search-tags.png)

## Sono un novellino, da dove comincio?

* Raccomandiamo di leggere bene la documentazione:
  * [Uso basilare](./basic-usage), per capire le **basi per usare** [Recalbox](https://www.recalbox.com).
  * [Compatibilità Emulatori](./hardware-compatibility/emulators-compatibility).
  * [Compatibilità Hardware](./hardware-compatibility/compatible-devices).
  * Puoi consultare anche le sezioni sugli [Emulatori](./emulators) per avere più informazioni riguardo quelli presenti in [Recalbox](https://www.recalbox.com).
  * Se alcuni **termini** sono **troppo tecnici** per te, il [glossario](./basic-usage/glossary) può essere un **ottimo aiuto**.
* Per un utilizzo più avanzato di Recalbox:
  * Riferisciti ai [tutorials](./tutorials).
  * Visita la sezione [Uso Avanzato](./advanced-usage).
* Puoi anche consulatare queste sezioni per informazioni aggiuntive: 
  * Il [sito](https://www.recalbox.com) di Recalbox.
  * Il [forum](https://forum.recalbox.com/).
  * Il [Discord](https://discord.gg/NbQFbGM).

## Qualche consiglio per un nuovo utente?

[Recalbox](https://www.recalbox.com) è un progetto **open-source**, ed è anche **gratuito**!

Il team che lavora su Recalbox lo fanno nel loro tempo libero, quindi il progetto e la sua documentazione si evolve e modifica in base a quante persone lavorano su di esso.

Il **miglior consiglio** che possiamo darti è:

* **sii curioso.**
* **leggi la documentazioni.**
* **leggi i [tutorials](./tutorials).**
* **contribuisci alla wiki, sia nel creare contenuto che nella sua traduzione** (vedi [Come contribuire](/en/contribute))
* **fai delle ricerche anche sul [forum](https://forum.recalbox.com/).**
* **fai sempre un  backup** della cartella **share** prima di fare qualcosa di cui non sei veramente sicuro.