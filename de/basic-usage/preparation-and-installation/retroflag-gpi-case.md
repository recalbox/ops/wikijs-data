---
title: RetroFlag GPi Gehäuse
description: 
published: true
date: 2021-06-24T11:19:40.008Z
tags: 
editor: markdown
dateCreated: 2021-06-24T11:19:40.008Z
---

## Vorbereitung

### Benötigte Komponenten

* 1 x RetroFlag GPi-Gehäuse
* 1 x Raspberry Pi Zero W
* 1 x SD-Karte
* 1 x SD-Karten Adapter
* 1 x Kühlkörper (für dein Raspberry Pi Zero W Board)
* 3 X Batterien

### Empfohlene Batterien

* [Duracell Rechargeable AA 2500 mAh Mignon Akku Batterien HR6, 4er Pack](https://www.amazon.de/dp/B00E3DVQFS/) 
* [Panasonic eneloop pro, Ready-to-Use Ni-MH Akku, AA Mignon, 4er Pack, min. 2500 mAh, 500 Ladezyklen, mit extrastarker Leistung und geringer Selbstentladung](https://www.amazon.de/gp/product/B00JWC40JY/)

## Beschreibung des Gpi Gehäuses

![](/basic-usage/preparation-and-installation/gpicase1.png)

![](/basic-usage/preparation-and-installation/gpicase2.png)

![](/basic-usage/preparation-and-installation/gpicase3.png)

## Flashen und loslegen!

>_**Setze das GPi Case nicht im Batteriebetrieb auf!**_   
>Verwende für Installationen und Tests immer das USB-Kabel als Stromversorgung.
{.is-warning}

![GPi Case](/basic-usage/preparation-and-installation/gpicase4.png)

Einfach das Image Flashen und loslegen, so einfacht ist das! Recalbox erkennt automatisch alles, was benötigt wird, und installiert es dann.

So betreibst du dein GPi Case:

* Flashe einfach die neueste Version von Recalbox für Raspberry Pi1 / Pi0 auf deine SD-Karte, stecke sie in den Einschub deines GPI und schalte ihn ein! 
* Warte dann ein paar Sekunden und du solltest folgendes Bild auf Deinem Bildschirm sehen:

![Image d'installation](/basic-usage/preparation-and-installation/gpicase5.png)

* Warte noch ein paar Sekunden mehr (bei SD-Karten mit hoher Kapazität kann es länger dauern), bevor EmulationStation mit dem gewohnten Bildschirm startet:

![EmulationStation démarre...](/basic-usage/preparation-and-installation/gpicase6.png)

* Sobald das erledigt ist, kannst du dich an einem optimierten Theme erfreuen, das dein Spielerlebnis bereichert, flüssig läuft und Spaß macht!

## Was ist anders als bei einer normalen Recalbox?

Wenn Recalbox die Komponenten des GPi Case erkennt und installiert, ändert es auch einige Einstellungen, damit du schnell loslegen kannst.  
  
Natürlich sind alle Einstellungen auch hier über das `START`-Menü verfügbar, und du weiterhin alles, was du willst, neu konfigurieren.

### Im Folgenden findest du eine Liste der geänderten Standardeinstellungen:

* Alle Videomodi (videomode) sind auf `default` eingestellt, um unnötige Änderungen der HDMI-Auflösung zu vermeiden.
* GPI-Hostname ist auf **RECALBOXGPI** eingestellt.
* Kodi und die X-Taste sind deaktiviert.
* Virtuelle Steuerung ist deaktiviert
* Updates sind deaktiviert
* Netplay ist deaktiviert
* Bluetooth ist deaktiviert
* XArcade- und PS3-Treiber sind deaktiviert
* Musik-Popup ist deaktiviert
* Die XBOX360-Konfiguration wurde durch die GPi-Case-Konfiguration ersetzt (ja, der GPI-Controller ist technisch gesehen auch ein XBOX360-Controller!)
* Unser optimiertes `recalbox-gpicase` Theme ist bereits nach `/recalbox/share/themes` kopiert und aktiviert

### Zusätzlich sind installiert:

* dpi-pizero-gpicase.dtbo: GPi-Bildschirmtreiber 
* pwm-audio-pizero-gpicase.dtbo: GPi-Sound-Treiber 
* Ein spezielles Shutdown-Skript für das GPi, ursprünglich von Retroflag erstellt und stark modifiziert, um das GPi schnell und sicher herunterzufahren.

## Spiele hinzufügen

![](/basic-usage/preparation-and-installation/gpicase7.png)

Sobald dein GPi-Case mit dem Internet verbunden ist, kannst du Spiele wie gewohnt über die SAMBA-Freigabe (`recalboxgpi\share` via Datei-Explorer) oder über den WebManager (`http://recalboxgpi` via Browser) hinzufügen

Viel Spaß mit deinen Lieblingsspielen!

## Was man wissen sollte

### Welche Art von Spielen kann ich auf dem GPI ausführen?

>Du kannst jedes Spiel von 8/16-Bit-Konsolen und Computern bis hin zu Arcade-Spielen ausführen.  
>_**Ja, wirklich!**_
{.is-info}

Für Arcade gibt es **PiFBA**, eine für ARM optimierte Version von FBA, die **Romset 0.2.96.37** benötigt.  
**FBNeo** (früher FBAlpha) und **MAME2000**, bis hin zu **MAME2003plus**, laufen ebenfalls sehr gut und viele Games sind gut spielbar.

Bei einigen **GBA/SNES**-Spielen sowie bei einigen Arcade-Spielen kann es zu Verzögerungen oder Slowdowns kommen. In diesem Fall kannst du versuchen, die Option `ZURÜCKSPULEN` in den erweiterten Optionen des Emulators zu deaktivieren. Allerdings laufen einige Spiele auch mit optimierten Optionen nicht mit voller Geschwindigkeit.
 
>Es wird nicht empfohlen, Computerspiele zu installieren, da diese zum Starten oft eine Tastatur und/oder Maus benötigen.
{.is-info}

### Kann ich meinen Pi0 übertakten?

Leider ist der Raspberry Pi0 nicht einfach zu übertakten. Außerdem enthält die GPi-Cartridge keine Lüftungsschlitze, um die heiße Luft abzuführen (zumindest nicht das zum Zeitpunkt der Erstellung dieser Seite verwendete GPi v1-Gehäuse).

_**Das Spielen mit den O/C-Optionen erfolgt weiterhin auf eigene Gefahr!**_

>Wenn dein GPI nach der Auswahl einer O/C-Option nicht stabil läuft, bis zu dem Punkt, an dem du die Optionen nicht mehr ändern kannst, stecke die SD-Karte in deinen PC und entferne die O/C-Zeilen aus der Datei `recalbox-oc-config.txt`. Diese Datei befindet sich in der Partition `RECALBOX`.
{.is-danger}

Du kannst einen kleinen Kühlkörper aus Kupfer hinzufügen, wie in der Abbildung unten gezeigt. Die Dicke sollte dabei 2 mm nicht überschreiten.

![](/basic-usage/preparation-and-installation/gpicase8.jpg)

### Wie lange halten die Batterien?

Die ersten Tests haben gezeigt, dass du mit einer Akkuladung ca. 2 Stunden Spielzeit hast.

Vergiss nicht, dass dein GPi Case einen USB-Stecker hat. Verwende also im Auto oder zu Hause das USB-Kabel.

>Wenn die Rückseite des Gehäuses sehr heiß wird, empfehlen wir dir, das GPi Case auszuschalten und es kurz abkühlen zu lassen.
{.is-info}

### Wie kann ich meine Spiele scrapen?

* **Interner Scraper**: Du kannst weiterhin den internen Scraper verwenden. Dieser ist so konfiguriert, dass er große Dateien zur Anzeige in einer normalen Recalbox scraped. Die Verwendung über WLAN ist allerdings langsam und ineffizient. 
* **[Externer Scraper](https://www.skraper.net/)**: **Wir empfehlen dringend die Verwendung eines externen Scrapers**. Damit geht es schneller und die Ergebnisse sind besser.

>Um Platz zu sparen und die Geschwindigkeit von EmulationStation mit dem **externen Scraper** zu erhöhen, kannst Du auch die maximale Bildgröße im Scraper auf 170x170 einstellen.
{.is-info}

## Fehlersuche und -behebung

* **Wenn das Display schwarz bleibt oder flackert, wenn du das Gehäuse berührst oder die Konsole hinlegst:**
  * Prüfe alle Anschlüsse
  * Hebe den RPi im Gehäuse an, um ihn näher an die gedruckten Schaltungen zu bringen: Spiele etwas mit den 4 Stiften, die den Pi 0 stützen, und schraube sie leicht heraus, bis der Pi 0 perfekt parallel zum Gehäuse steht. Je nach Gehäuse kann es ausreichen, die 2 goldenen Stifte an der Oberseite herauszuschrauben.

>Dieser Fehler tritt auf, wenn es ein Problem mit der Verarbeitung des Gehäuses gibt. Die Verbindung zwischen dem Pi0 und dem Gehäuse ist dann nicht korrekt ausgeführt.
{.is-info}

* **Stabilitätsprobleme lösen:**

  * **1. Die Micro-SD**

  Überprüfe die microSD.  
  Einige Modelle verursachen Probleme mit dem Pi, ganz zu schweigen von der schlechten Qualität der chinesischen µSD-Karten.  
  Teste mit einer anderen SD, vorzugsweise von einer anderen (besseren) Marke.  

  * **2. Batterien**

  _**Setze das GPi Gehäuse nicht im Batteriebetrieb auf!**_   
  Verwende für die Installation und die ersten Tests immer das USB-Kabel.  
  Wenn es mit dem Kabel funktioniert und mit den Batterien nicht, sind entweder die Batterien entladen oder sie liefern trotz des GPi-Regulators nicht genug Strom.  
  Die meisten Akkus sind 1,2V, nicht 1,5V.  

  * **3. Der Raspberry Pi0**

  Es scheint, dass einige Modelle Probleme mit den Standard Clocktimings haben, die in der Pi-Firmware konfiguriert sind.  
  Öffne die Datei `recalbox-oc-config.txt` in der Partition `RECALBOX` und füge die folgenden Zeilen am Ende der Datei ein:

  ```ini
  arm_freq=1000
  gpu_freq=500
  core_freq=500
  sdram_freq=500
  sdram_schmoo=0x02000020
  over_voltage=6
  sdram_over_voltage=2
  ```

  Dies erzwingt die Konfiguration der Taktraten mit einer leichten Übertaktung der GPU als Bonus.  

  * **4. Zusammenbau**

	Obwohl die Montage des Pi relativ einfach ist, kann es vorkommen, dass die Montage zu unzureichenden Kontakten führt. Öffne dann das Gehäuse, reinige die Kontakte im Pi und im GPi-Gehäuse mit einer Reinigungslösung auf Alkoholbasis und baue alles wieder zusammen.
	Überprüfe auch, ob alles richtig sitzt. Wenn der Power-Button nur schwer zu Bewegen ist, oder sogar blockiert, ist der Zusammenbau nicht korrekt durchegführt worden.


  * **5. Zu guter Letzt**

  Wenn der Pi trotz allem nicht funktioniert oder instabil läuft, versuche es mit einem anderen Pi, um herauszufinden, ob der Pi oder das GPi-Gehäuse das Problem verursacht. Wir empfehlen auch, immer einen Ersatz-Pi zu haben.  
   Wenn das GPi Case defekt ist, kannst du versuchen, es umzutauschen oder zurückzugeben. Falls es Probleme mit dem Verkäufer gibt, gib alle Verfahren an, die du befolgt hast, um deinen guten Willen bezüglich der Inkriminierung des GPi-Cases zu beweisen.

* Wenn du dich in einer der folgenden Situationen befindest:

  * Beim ersten Start ist auch nach längerem Warten nichts auf dem Bildschirm zu sehen.
  * Du kannst das WLAN nicht konfigurieren. Setze dich mit uns auf dem Discord-Server oder im Forum in Verbindung und gib uns die folgenden Dateien, die sich in der Partition `RECALBOX` deiner SD-Karte befinden:

    * `config.txt`
    * `recalbox-backup.conf`
    * `hardware.log`

>Bei anderen Problemen schreibe uns hier an:  
>[Forum](https://forum.recalbox.com/) oder [Gitlab issues](https://gitlab.com/recalbox/recalbox/issues)
{.is-info}