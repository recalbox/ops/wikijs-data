---
title: Naomi 2
description: 
published: true
date: 2024-07-23T21:37:15.150Z
tags: arcade, naomi 2
editor: markdown
dateCreated: 2024-06-21T10:30:31.897Z
---

![](/emulators/arcade/naomi2.svg){.align-center}

## Fiche technique

* **Processeur** : Hitachi SH-4 32-bit RISC (@ 200 MHz 360 MIPS / 1.4 GFLOPS)
* **Vidéo** : Processeur PowerVR 2 à 100 MHz fabriqué par Nec
* **Audio** : Processeur ARM7 Yamaha XG AICA RISC 32 bits
* **RAM** : 56 Mo
* **Média** : Cartouche (PCB de ROMS), 168 MB maximum

## Présentation

Le Naomi 2 est l'acronyme de _New Arcade Operation Machine Idea_ (littéralement « nouvelle idée de machine d'arcade »), Naomi se traduit aussi par « beauté » en japonais.

L'architecture matérielle du Naomi 2 est semblable à celle de son ainée, les jeux sont donc compatibles.

Le système offre des performances graphiques améliorées, bénéficiant de l'augmentation de la capacité graphique du matériel originel du Naomi. Il est maintenant animé par deux puces graphiques PowerVR CLX2, une puce VideoLogic Elan, la quantité de mémoire graphique a été doublée passant de 16 Mo à 32 Mo (soit quatre fois plus que la Dreamcast), permettant l'affichage de plus de détails graphiques.

Avec le Naomi 2, Sega propose dès l'achat son lecteur de GD-ROM en option. Les Dimm boards et GD-Roms, quelles que soient leurs versions (Naomi, Naomi 2), sont entièrement compatibles avec la Naomi ou la Naomi 2 (si le BIOS est adéquat, bien sûr).

Le Naomi 2 tout comme son prédécesseur peut être connecté en réseau, on parle alors de Naomi 2 Satellite Terminal.

Le système Naomi 2 s'impose à l'époque comme une référence de puissance en arcade, profitant du savoir-faire de Sega dans ce domaine.

## Émulateurs

[Libretro Flycast-Next](libretro-flycast-next)