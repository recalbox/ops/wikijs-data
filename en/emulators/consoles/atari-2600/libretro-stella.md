---
title: Libretro Stella
description: 
published: true
date: 2021-10-06T23:26:42.398Z
tags: libretro, atari-2600, atari, 2600, stella
editor: markdown
dateCreated: 2021-07-30T16:25:28.775Z
---

**Libretro Stella** is an Atari multiplatform emulator.

## ![](/emulators/license.svg) License

This core is under [**GPLv2**](https://github.com/stella-emu/stella/blob/master/License.txt) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Features

| Feature | Supported |
| :---: | :---: |
| Restart | ✔ |
| Screenshots | ✔ |
| States | ✔ |
| Rewind | ✔ |
| Netplay | ✔ |
| RetroAchievements | ✔ |
| Controls | ✔ |
| Remapping | ✔ |

## ![](/emulators/bios.svg) BIOS

>**No bios is required.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Supported extensions

Roms must have the extension:

* .a26
* .bin
* .zip
* .7z

This system supports compressed roms in .zip/.7z format. However, be careful, it is only an archive.

The files contained in the .zip/.7z must correspond to the extensions mentioned above.
Moreover, each .zip/.7z file must contain only one compressed rom.

### Location

Put the roms like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 atari2600
┃ ┃ ┃ ┃ ┣ 🗒 **game.zip**

>The roms using **No-Intro** format are highly recommended.
{.is-success}

>For more information about roms, go to [this tutorial](./../../../tutorials/games/generalities/isos-and-roms)!
{.is-info}

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Accessing to options

You can configure various options in two different ways.

* Via RetroArch menu:

┣ 📁 RetroArch Menu
┃ ┣ 📁 Core options
┃ ┃ ┣ 🧩 Name_option

* Via the file `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core options

| Options | Options values | Variables (for configuration override) | Variables values |
| --- | --- | --- | --- |
| Console display | `auto` ✅ / `ntsc` / `pal` / `secam` / `ntsc50` / `pal60` / `secam60` | `stella_console` | `auto` / `ntsc` / `pal` / `secam` / `ntsc50` / `pal60` / `secam60` |
| Palette colors | `standard` ✅ / `z26` / `user` / `custom` | `stella_palette` | `standard` / `z26` / `user` / `custom` |
| TV effects | `Disabled` ✅ / `composite` / `s-video` / `rgb` / `badly adjusted` | `stella_filter` | `disabled` / `composite` / `s-video` / `rgb` / `badly adjusted` |
| NTSC aspect % | `par` ✅ / `100` / `101` / `102` / `103` / `104` / `105` / `106` / `107` / `108` / `109` / `110` / `111` / `112` / `113` / `114` / `115` / `116` / `117` / `118` / `119` / `120` / `121` / `122` / `123` / `124` / `125` / `50` / `75` / `76` / `77` / `78` / `79` / `80` / `81` / `82` / `83` / `84` / `85` / `86` / `87` / `88` / `89` / `90` / `91` / `92` / `93` / `94` / `95` / `96` / `97` / `98` / `99` | `stella_ntsc_aspect` | `par` / `100` / `101` / `102` / `103` / `104` / `105` / `106` / `107` / `108` / `109` / `110` / `111` / `112` / `113` / `114` / `115` / `116` / `117` / `118` / `119` / `120` / `121` / `122` / `123` / `124` / `125` / `50` / `75` / `76` / `77` / `78` / `79` / `80` / `81` / `82` / `83` / `84` / `85` / `86` / `87` / `88` / `89` / `90` / `91` / `92` / `93` / `94` / `95` / `96` / `97` / `98` / `99` |
| PAL aspect % | `par` ✅ / `100` / `101` / `102` / `103` / `104` / `105` / `106` / `107` / `108` / `109` / `110` / `111` / `112` / `113` / `114` / `115` / `116` / `117` / `118` / `119` / `120` / `121` / `122` / `123` / `124` / `125` / `50` / `75` / `76` / `77` / `78` / `79` / `80` / `81` / `82` / `83` / `84` / `85` / `86` / `87` / `88` / `89` / `90` / `91` / `92` / `93` / `94` / `95` / `96` / `97` / `98` / `99` | `stella_pal_aspect` | `par` / `100` / `101` / `102` / `103` / `104` / `105` / `106` / `107` / `108` / `109` / `110` / `111` / `112` / `113` / `114` / `115` / `116` / `117` / `118` / `119` / `120` / `121` / `122` / `123` / `124` / `125` / `50` / `75` / `76` / `77` / `78` / `79` / `80` / `81` / `82` / `83` / `84` / `85` / `86` / `87` / `88` / `89` / `90` / `91` / `92` / `93` / `94` / `95` / `96` / `97` / `98` / `99` |
| Crop horizontal overscan | `Disabled` ✅ / `Enabled` | `stella_crop_hoverscan` | `disabled` / `enabled` |
| Stereo sound | `auto` ✅ / `Disabled` / `Enabled` | `stella_stereo` | `auto` / `off` / `on` |
| Phosphor mode | `auto` ✅ / `Disabled` / `Enabled` | `stella_phosphor` | `auto` / `off` / `on` |
| Phosphor blend % | `60` ✅ / `65` / `70` / `75` / `80` / `85` / `90` / `95` / `100` / `0` / `5` / `10` / `15` / `20` / `25` / `30` / `35` / `40` / `45` / `50` / `55` | `stella_phosphor_blend` | `60` / `65` / `70` / `75` / `80` / `85` / `90` / `95` / `100` / `0` / `5` / `10` / `15` / `20` / `25` / `30` / `35` / `40` / `45` / `50` / `55` |
| Paddle joypad sensitivity | `3` ✅ / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `1` / `2` | `stella_paddle_joypad_sensitivity` | `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `1` / `2` |

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/stella-emu/stella/](https://github.com/stella-emu/stella/)
* **Libretro documentation**: [https://docs.libretro.com/library/stella/](https://docs.libretro.com/library/stella/)