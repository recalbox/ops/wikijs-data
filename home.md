---
title: Welcome to the Recalbox wiki!
description: How to start with Recalbox?
published: true
date: 2024-06-02T16:53:40.126Z
tags: 
editor: markdown
dateCreated: 2021-05-21T20:27:59.059Z
---

![welcome.png](/home/welcome.png)

## What is Recalbox?

[Recalbox](https://www.recalbox.com) is the **ultimate retro-gaming console** that lets you replay all the games from the consoles and computers of your childhood.

With [Recalbox](https://www.recalbox.com), (re)discover and (re)play the titles that have shaped the world of video games with ease!

[Recalbox](https://www.recalbox.com) can be easily installed on inexpensive microcomputers like the Raspberry Pi, and even on your PC (modern or not)!

## I want to install Recalbox! Where do I start?

To install Recalbox, go to the [Preparation and Installation](./basic-usage/preparation-and-installation) page.

To learn more about supported systems, visit the [hardware compatibility](./hardware-compatibility) section.

## I'm looking for a specific page!

Take advantage of the search function at the top of the page!
You can either simply fill in your search in the box...
![search-text.png](/home/search-text.png)
Or, you can search by tag.
![search-tags.png](/home/search-tags.png)

## I'm a newbie, where do I start?

* We strongly encourage you to look at the documentation:
  * [Basic usage](./basic-usage), to understand the **basics of using** [Recalbox](https://www.recalbox.com).
  * [Emulator compatibility](./hardware-compatibility/emulators-compatibility).
  * [Device compatibility](./hardware-compatibility/compatible-devices).
  * You can consult the [Emulators](./emulators) section to have more information about the ones present in [Recalbox](https://www.recalbox.com).
  * If some **terms** are **too technical** for you, the [glossary](./basic-usage/glossary) can be a **valuable tool**.
* For more advanced utilization of Recalbox:
  * Refer to the [tutorials](./tutorials).
  * Visit the [Advanced Usage](./advanced-usage) section.
* You can also check these sections for additional information: 
  * The Recalbox [website](https://www.recalbox.com).
  * The [forum](https://forum.recalbox.com/).
  * The [Discord](https://discord.gg/NbQFbGM).

## Any advice for a new user?

[Recalbox](https://www.recalbox.com) is an **open-source** project, and is also **free**!

The people who work on it do so on their own time, so the project and its documentation evolve as fast as people contribute to it.

The **best advice** we can give you is:

* **be curious.**
* **read the documentation.**
* **read the [tutorials](./tutorials).**
* **contribute to the wiki, whether it's creating content or translating** (see [How to contribute](/en/contribute))
* **do some searching on the [forum](https://forum.recalbox.com/).**
* **always make a backup** of your **share** folder before doing anything you're not sure about.