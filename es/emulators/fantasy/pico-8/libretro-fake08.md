---
title: Libretro Fake08
description: 
published: true
date: 2024-07-13T09:43:13.582Z
tags: libretro, 9.2+, fake08
editor: markdown
dateCreated: 2024-07-13T09:43:13.582Z
---

## ![](/emulators/license.svg) Licencia

Este core está bajo licencia [**MIT**](https://github.com/jtothebell/fake-08/blob/master/LICENSE.MD).

## ![](/emulators/compatibility.png) Compatibilidad

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Funcionalidades

| Funcionalidad | Soportado |
| :---: | :---: |

## ![](/emulators/bios.svg) BIOS

>**No se requiere BIOS.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Extensiones soportadas

Las roms deben tener la extensión:

* .p8
* .png
* .zip
* .7z

### Ubicación

Coloca las roms de esta manera:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 pico8
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.png**

## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

## ![](/emulators/external-links.png) Enlaces externos

* **Código fuente utilizado** : [https://github.com/jtothebell/fake-08/](https://github.com/jtothebell/fake-08/)
* **Foro Pico-8** : [https://www.lexaloffle.com/bbs/?cat=7](https://www.lexaloffle.com/bbs/?cat=7)
