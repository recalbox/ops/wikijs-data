---
title: DOS
description: 
published: true
date: 2021-10-03T16:42:54.951Z
tags: dos, msdos, ordinateurs
editor: markdown
dateCreated: 2021-05-21T07:56:30.768Z
---

![](/emulators/computers/dos.svg){.align-center}

## Fiche technique

* **Fabricant** : IBM
* **Année de sortie** : 1981

## Présentation

On appelle généralement **DOS** _(disk operating system)_ le système d'exploitation **PC-DOS**, ainsi que la variante **MS-DOS** vendue par Microsoft pour les compatibles PC. Il en existe des clones postérieurs, tels **DR-DOS** de Digital Research et **FreeDOS**.

Jusqu'au début des années 1990, DOS était le type de système le plus utilisé sur compatibles PC. Ce système en ligne de commande était rudimentaire : pas de multitâche, pas de mémoire virtuelle, gestion du seul mode segmenté 16 bits du microprocesseur x86.

Il existe d'autres systèmes sans relation directe qui contiennent le mot DOS _(AMSDOS, AmigaDOS, Apple DOS, ProDOS, DOS sur mainframe)_, mais leur nom est éclipsé.

## Émulateurs

[DOSBox](dosbox)
[Libretro DOSBox_Pure](libretro-dosbox-pure)