---
title: TFT
description: 
published: true
date: 2021-08-06T23:12:46.211Z
tags: tft
editor: markdown
dateCreated: 2021-08-06T17:11:00.726Z
---

If you use a TFT screen, you should find your happiness here.

Here are the available tutorials:

[Configure a TFT HDMI screen](hdmi-tft-screen)
[Configure a small TFT screen on DPI bus](tft-lcd-on-dpi-bus)
[Configure a small TFT screen on SPI bus](tft-lcd-on-spi-bus)
[Connect a LCD display with I2C to your Recalbox](lcd-display-with-i2c)
[Second miniTFT screen for scraps and videos](second-minitft-or-ogst-for-scraps-and-videos)