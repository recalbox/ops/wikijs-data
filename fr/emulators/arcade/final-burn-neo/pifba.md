---
title: PiFBA
description: 
published: true
date: 2024-06-23T11:29:19.082Z
tags: finalburn, neo, pifba
editor: markdown
dateCreated: 2021-05-21T08:13:10.800Z
---

**PiFBA** est Final Burn Alpha 2x pour le Raspberry Pi porté par Squid.  
Cela émule les anciens jeux d'arcade en utilisant des roms basés sur MAME pour CPS1, CPS2, Neogeo, Toaplan et de nombreux autres jeux.

Il devrait prendre en charge les versions les plus récentes des versions MAME de ces ROMs, mais malheureusement, il n'y a pas de version définitive à utiliser. Une liste complète des jeux pris en charge se trouve dans le programme frontal.

Un certain nombre de jeux ne fonctionneront pas correctement, certains sont si volumineux qu'ils semblent manquer de mémoire.

>A n'utiliser que si vous avez un Pi0/1 ou si vous voulez améliorez les performances d'un jeu particulier ramant sur FBNeo.
>piFBA est le mieux optimisé des émulateurs FBA sur la Recalbox pour le Pi0/1 mais est compatible avec beaucoup moins de jeux que FBNeo.
{.is-warning}

## ![](/emulators/license.svg) Licence

Ce core est sous licence **GPLv2**.

## ![](/emulators/compatibility.png) Compatibilité

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ❌ | ❌ | ❌ | ❌ | ❌ | ❌ |

## ![](/emulators/features.png) Fonctionnalités

| Fonctionnalité | Supporté |
| :---: | :---: |
| Sauvegardes | ✔ |
| Netplay | ✔ |
| RetroAchievements | ✔ |

## ![](/emulators/bios.svg) Bios

### Liste des bios

En fonction des jeux, certains bios seront nécessaires et seront à placer dans le même répertoire que les jeux.

### Emplacement

Placez les **bios** comme ceci : 

┣ 📁 recalbox  
┃ ┣ 📁 share  
┃ ┃ ┣ 📁 roms  
┃ ┃ ┃ ┣ 📁 fba  
┃ ┃ ┃ ┃ ┣ 🗒 bios.zip  

## ![](/emulators/roms.png) Roms

* Basé sur le romset MAME : **FBA 0.2.96.71** (Avril 2007) lui-même basé sur le set MAME 0.114
* Poids : 3.62GB
* Romsets émulés : 684 (Pas de clone dans ce set)
* Liste de compatibilité : [/recalbox/share/roms/fba/clrmamepro/piFBA_gamelist.txt](https://gitlab.com/recalbox/recalbox/-/raw/master/board/recalbox/fsoverlay/recalbox/share_init/bios/fba/piFBA_gamelist.txt)
* Fichier Dat : [/recalbox/share/roms/fba/clrmamepro/fba_029671_od_release_10_working_roms.dat](https://gitlab.com/recalbox/recalbox/-/raw/master/board/recalbox/fsoverlay/recalbox/share_init/bios/fba/fba_029671_od_release_10_working_roms.dat)

Certaines roms importantes peuvent nécessiter une conversion au format '.fba' avant de fonctionner.
FBACache_windows.zip est inclus pour ce faire, qui fonctionne sous Windows uniquement.

Un fichier DAT clrmamepro / romcenter est inclus 'fba_029671_clrmame_dat.zip' qui prend en charge la plupart des ROMs qui fonctionnent et peuvent générer des ROMs compatibles à partir des versions récentes de MAME.

## ![](/emulators/advanced-configuration.png) Configuration avancée des émulateurs

>Pour pouvoir conserver vos configurations personnalisées lors d'une mise à jour, nous vous conseillons d'utiliser notre fonctionnalité [Surcharges de configuration](./../../../advanced-usage/configuration-override).
{.is-info}

## ![](/emulators/external-links.png) Liens externes

* **Code source utilisé** : [https://github.com/recalbox/pifba/](https://github.com/recalbox/pifba)
* **Code source original**: [https://github.com/squidrpi/pifba/](https://github.com/squidrpi/pifba)