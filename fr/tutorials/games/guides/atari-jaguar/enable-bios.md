---
title: Activation du bios
description: 
published: true
date: 2023-08-13T23:23:55.647Z
tags: bios, atari, jaguar
editor: markdown
dateCreated: 2021-06-06T22:01:13.648Z
---

## Introduction

Certains jeux ne démarreront pas avec l'émulateur Jaguar. Pour contourner ce problème, il faut activer le bios dans les options du core.

## Utilisation

### Via le jeu

* Lancez un jeu qui démarre correctement pour l'Atari Jaguar.
* Allez dans le menu de RetroArch en faisant `Hotkey` + `B`.
* Allez dans `Options`.

![](/tutorials/games/guides/atari-jaguar/jaguarenablebios1.png){.full-width}

* Activez l'option `Bios`.

![](/tutorials/games/guides/atari-jaguar/jaguarenablebios2.png){.full-width}

* Fermez le menu RetroArch (faites « Retour » 1 fois et allez dans `Reprendre`) et quittez complètement l'émulateur.
* Lancez le jeu qui ne démarrait pas.

### Via une surcharge de configuration

* Créez un fichier nommé `.core.cfg` et mettez le contenu suivant dedans :

```ini
virtualjaguar_bios=enabled
```

* Placez ce fichier dans `/recalbox/share/roms/jaguar`.
* Lancez le jeu qui ne démarrait pas.

Bon jeu !