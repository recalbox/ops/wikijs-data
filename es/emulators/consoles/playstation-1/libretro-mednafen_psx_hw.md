---
title: Libretro Mednafen_PSX_HW
description: 
published: true
date: 2024-07-06T19:49:48.629Z
tags: libretro, ps1, mednafen, playstation 1, psx hw
editor: markdown
dateCreated: 2023-09-07T16:21:04.895Z
---

[Beetle PSX](https://github.com/libretro/beetle-psx-libretro) es un fork del emulador [Mednafen](https://mednafen.github.io/) PSX para Libretro, actualmente funciona en Linux, macOS y Windows. Este núcleo está disponible en la versión `mednafen-psx-hw` que requiere OpenGL 3.3 para el motor de renderizado OpenGL.

## ![](/emulators/license.svg) Licencia

El core posee la siguiente licencia [**GPLv2**](https://github.com/libretro/beetle-psx-libretro/blob/master/COPYING).

## ![](/emulators/compatibility.png) Compatibilidad

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | 
| ❌ | ❌ | ❌ | ❌ | ❌ | ❌ | ✅ | ❌ |

## ![](/emulators/features.png) Funcionalidades

| Funcionalidad | ¿Soportada? |
| :---: | :---: |
| Reinicio | ✔ |
| Capturas de pantalla | ✔ |
| Grabaciones | ✔ |
| Grabaciones instantáneas | ✔ |
| Opciones del core | ✔ |
| RetroAchievements | ✔ |
| Cheats RetroArch | ✔ |
| Controles | ✔ |
| Remapping | ✔ |
| Multi-Ratón | ✔ |
| Vibración | ✔ |
| Control de disco | ✔ |

## ![](/emulators/bios.svg) BIOS

### Lista de BIOS obligatorias

| Nombre del fichero | Descripción | MD5 | ¿Preinstalada? |
| :---: | :---: | :---: | :---: |
| scph5500.bin | BIOS PS1 japonais - requis pour les jeux japonais | 8dd7d5296a650fac7319bce665a6a53c | ❌ |
| scph5501.bin | BIOS PS1 américain - requis pour les jeux américains | 490f666e1afb15b7362b406ed1cea246 | ❌ |
| scph5502.bin | BIOS PS1 européen - requis pour les jeux européens | 32736f17079d0b2b7024407c39bd3050 | ❌ |

### Emplazamiento

Disponed vuestras bios como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 🗒 **scph5500.bin**
┃ ┃ ┃ ┣ 🗒 **scph5501.bin**
┃ ┃ ┃ ┣ 🗒 **scph5502.bin**

## ![](/emulators/isos.svg) Isos

### Extensiones de roms soportadas

Las isos deben tener las extensiones siguientes:

* .bin/.cue
* .bin/.toc
* .m3u
* .img/.ccd/.sub
* .exe
* .pbp
* .chd

### Emplazamiento

Disponed vuestras isos como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 psx
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.bin**
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.cue**

>Se aconsejan las isos del proyecto **Redump**.
{.is-success}

>Para más información sobre las isos, consultad [este tutorial](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

>Para conservar las configuraciones personalizadas cuando se produce una actualización del sistema Recalbox, os aconsejamos utilizar las [Sobrecargas de configuración](./../../../advanced-usage/configuration-override).
{.is-info}

### Acceder a las opciones

Podéis configurar las opciones del core de dos formas diferentes.

* Via el Menú RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via el fichero `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Opciones del core

| Opción | Valor de la opción | Variable (para la sobrecarga) | Valores posibles |
| --- | --- | --- | --- |
| Internal GPU Resolution | `1x (Native)` ✅ / `2x` / `4x` / `8x` / `16x` | `beetle_psx_hw_internal_resolution` | `1x(native)` / `2x` / `4x` / `8x` / `16x` |
| Internal Color Depth | `16 bpp (Native)` ✅ / `32 bpp` | `beetle_psx_hw_depth` | `16bpp(native)` / `32 bpp` |
| Dithering Pattern | `1x (Native)` ✅ / `Internal Resolution` / `Désactivé` | `beetle_psx_hw_dither_mode` | `1x(native)` / `internal resolution` / `disabled` |
| Texture Filtering | `Nearest` ✅ / `SABR` / `xBR` / `Bilinear` / `3-Point` / `JINC2` | `beetle_psx_hw_filter` | `nearest` / `SABR` / `xBR` / `bilinear` / `3-point` / `JINC2` |
| Wireframe Mode (Debug) | `Désactivé` ✅ / `Activé` | `beetle_psx_hw_wireframe` | `disabled` / `enabled` |
| Software Framebuffer | `Activé` ✅ / `Désactivé` | `beetle_psx_hw_renderer` | `enabled` / `disabled` |
| PAL (European) Video Timing Override | `Désactivé` ✅ / `Activé` | `beetle_psx_hw_pal_video_timing_override` | `disabled` / `enabled` |
| Crop Overscan | `Activé` ✅ / `Désactivé` | `beetle_psx_hw_crop_overscan` | `enabled` / `disabled` |
| Additional Cropping | `Désactivé` ✅ / `1px` / `2px` / `3px` / `4px` / `5px` / `6px` / `7px` / `8px` / `9px` / `10px` / `11px` / `12px` / `13px` / `14px` / `15px` / `16px` / `17px` / `18px` / `19px` / `20px` | `psx_hw_image_crop` | `disabled` / `1px` / `2px` / `3px` / `4px` / `5px` / `6px` / `7px` / `8px` / `9px` / `10px` / `11px` / `12px` / `13px` / `14px` / `15px` / `16px` / `17px` / `18px` / `19px` / `20px` |
| Horizontal Image Offset (GPU Cycles) | `-40` / `-39` / `-38` / `-37` / `-36` / `-35` / `-34` / `-33` / `-32` / `-31` / `-30` / `-29` / `-28` / `-27` / `-26` / `-25` / `-24` / `-23` / `-22` / `-21` / `-20` / `-19` / `-18` / `-17` / `-16` / `-15` / `-14` / `-13` / `-12` / `-11` / `-10` / `-9` / `-8` / `-7` / `-6` / `-5` / `-4` / `-3` / `-2` / `-1` / `0` ✅ / `+1` / `+2` / `+3` / `+4` / `+5` / `+6` / `+7` / `+8` / `+9` / `+10` / `+11` / `+12` / `+13` / `+14` / `+15` / `+16` / `+17` / `+18` / `+19` / `+20` / `+21` / `+22` / `+23` / `+24` / `+25` / `+26` / `+27` / `+28` / `+29` / `+30` / `+31` / `+32` / `+33` / `+34` / `+35` / `+36` / `+37` / `+38` / `+39` / `+40` | `beetle_psx_hw_image_offset_cycles` | `-40` / `-39` / `-38` / `-37` / `-36` / `-35` / `-34` / `-33` / `-32` / `-31` / `-30` / `-29` / `-28` / `-27` / `-26` / `-25` / `-24` / `-23` / `-22` / `-21` / `-20` / `-19` / `-18` / `-17` / `-16` / `-15` / `-14` / `-13` / `-12` / `-11` / `-10` / `-9` / `-8` / `-7` / `-6` / `-5` / `-4` / `-3` / `-2` / `-1` / `0` / `+1` / `+2` / `+3` / `+4` / `+5` / `+6` / `+7` / `+8` / `+9` / `+10` / `+11` / `+12` / `+13` / `+14` / `+15` / `+16` / `+17` / `+18` / `+19` / `+20` / `+21` / `+22` / `+23` / `+24` / `+25` / `+26` / `+27` / `+28` / `+29` / `+30` / `+31` / `+32` / `+33` / `+34` / `+35` / `+36` / `+37` / `+38` / `+39` / `+40` |
| GPU Resterizer Overclock | `1x (Native)` ✅ / `2x` / `4x` / `8x` / `16x` / `32x` | `beetle_psx_hw_gpu_overclock` | `1x(native)` / `2x` / `4x` / `8x` / `16x` / `32x` |
| Core Aspect Ratio | `Corrected` ✅ / `Uncorrected` / `Force 4:3` / `Force NTSC` | `beetle_psx_hw_aspect_ratio` | `corrected` / `uncorrected` / `4:3` / `ntsc` |
| Initial Scanlines - PAL | `0` ✅ / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `21` / `22` / `23` / `24` / `25` / `26` / `27` / `28` / `29` / `30` / `31` / `32` / `33` / `34` / `35` / `36` / `37` / `38` / `39` / `40` | `beetle_psx_hw_initial_scanline_pal` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `21` / `22` / `23` / `24` / `25` / `26` / `27` / `28` / `29` / `30` / `31` / `32` / `33` / `34` / `35` / `36` / `37` / `38` / `39` / `40` |
| Last Scanlines - PAL | `230` / `231` / `232` / `233` / `234` / `235` / `236` / `237` / `238` / `239` / `240` / `241` / `242` / `243` / `244` / `245` / `246` / `247` / `248` / `249` / `250` / `251` / `252` / `253` / `254` / `255` / `256` / `257` / `258` / `259` / `260` / `261` / `262` / `263` / `264` / `265` / `266` / `267` / `268` / `269` / `270` / `271` / `272` / `273` / `274` / `275` / `276` / `277` / `278` / `279` / `280` / `281` / `282` / `283` / `284` / `285` / `286` / `287` ✅ | `beetle_psx_hw_last_scanline_pal` | `230` / `231` / `232` / `233` / `234` / `235` / `236` / `237` / `238` / `239` / `240` / `241` / `242` / `243` / `244` / `245` / `246` / `247` / `248` / `249` / `250` / `251` / `252` / `253` / `254` / `255` / `256` / `257` / `258` / `259` / `260` / `261` / `262` / `263` / `264` / `265` / `266` / `267` / `268` / `269` / `270` / `271` / `272` / `273` / `274` / `275` / `276` / `277` / `278` / `279` / `280` / `281` / `282` / `283` / `284` / `285` / `286` / `287` |
| Initial Scanlines (NTSC) | `0` ✅ / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `21` / `22` / `23` / `24` / `25` / `26` / `27` / `28` / `29` / `30` / `31` / `32` / `33` / `34` / `35` / `36` / `37` / `38` / `39` / `40` | `psx_hw_initial_scanlines` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `21` / `22` / `23` / `24` / `25` / `26` / `27` / `28` / `29` / `30` / `31` / `32` / `33` / `34` / `35` / `36` / `37` / `38` / `39` / `40` |
| Last Scanlines (NTSC) | `210` / `211` / `212` / `213` / `214` / `215` / `216` / `217` / `218` / `219` / `220` / `221` / `222` / `223` / `224` / `225` / `226` / `227` / `228` / `229` / `230` / `231` / `232` / `233` / `234` / `235` / `236` / `237` / `238` / `239` ✅ | `psx_hw_last_scanlines` | `210` / `211` / `212` / `213` / `214` / `215` / `216` / `217` / `218` / `219` / `220` / `221` / `222` / `223` / `224` / `225` / `226` / `227` / `228` / `229` / `230` / `231` / `232` / `233` / `234` / `235` / `236` / `237` / `238` / `239` |
| Display Internal FPS | `Désactivé` ✅ / `Activé` | `beetle_psx_hw_display_internal_fps` | `disabled` / `enabled` |
| Display Full VRAM (Debug) | `Désactivé` ✅ / `Activé` | `beetle_psx_hw_dispaly_vram` | `disabled` / `enabled` |
| Analog Self-Calibration | `Désactivé` ✅ / `Activé` | `beetle_psx_hw_analog_calibration` | `disabled` / `enabled` |
| Enable DualShock Analog Mode Toggle | `Désactivé` ✅ / `Activé` | `beetle_psx_hw_analog_toggle` | `disabled` / `enabled` |
| DualShock Analog Mode Combo | `L1 + L2 + R1 + R2 + Start + Select` / `L1 + R1 + Select` ✅ / `L1 + R1 + Start` / `L1 + R1 + L3` / `L1 + R1 + R3` / `L2 + R2 + Select` / `L2 + R2 + Start` / `L2 + R2 + L3` / `L2 + R2 + R3` / `L3 + R3` | `beetle_psx_hw_analog_toggle_combo` | `l1+l2+r1+r2+start+select` / `l1+r1+select` / `l1+r1+start` / `l1+r1+l3` / `l1+r1+r3` / `l2+r2+select` / `l2+r2+start` / `l2+r2+l3` / `l2+r2+r3` / `l3+r3` |
| DualShock Analog Mode Combo Hold Delay | `0 Second Delay` / `1 Second Delay` ✅ / `2 Second Delay` / `3 Second Delay` / `4 Second Delay` / `5 Second Delay` | `beetle_psx_hw_analog_toggle_hold` | `0` / `1` / `2` / `3` / `4` / `5` |
| Port 1: Multitap Enable | `Désactivé` ✅ / `Activé` | `beetle_psx_hw_enable_multitap_port1` | `disabled` / `enabled` |
| Port 2: Multitap Enable | `Désactivé` ✅ / `Activé` | `beetle_psx_hw_enable_multitap_port2` | `disabled` / `enabled` |
| Gun Input Mode | `Light Gun` ✅ / `Touchscreen` | `beetle_psx_hw_gun_input_mode` | `lightgun` / `touchscreen` |
| Gun Cursor | `Cross` ✅ / `Dot` / `Désactivé` | `beetle_psx_hw_gun_cursor` | `cross` / `dot` / `off` |
| Port 1: Gun Crosshair Color | `Red` ✅ / `Blue` / `Green` / `Orange` / `Yellow` / `Cyan` / `Pink` / `Purple` / `Black` / `White` | `beetle_psx_hw_crosshair_color_p1` | `red` / `blue` / `green` / `orange` / `yellow` / `cyan` / `pink` / `purple` / `black` / `white` |
| Port 2: Gun Crosshair Color | `Blue` ✅ / `Red` / `Green` / `Orange` / `Yellow` / `Cyan` / `Pink` / `Purple` / `Black` / `White` | `beetle_psx_hw_crosshair_color_p2` | `blue` / `red` / `green` / `orange` / `yellow` / `cyan` / `pink` / `purple` / `black` / `white` |
| Mouse Sensitivity | `5%` / `10%` / `15%` / `20%` / `25%` / `30%` / `35%` / `40%` / `45%` / `50%` / `55%` / `60%` / `65%` / `70%` / `75%` / `80%` / `85%` / `90%` / `95%` / `100%` ✅ / `105%` / `110%` / `115%` / `120%` / `125%` / `130%` / `135%` / `140%` / `145%` / `150%` / `155%` / `160%` / `165%` / `170%` / `175%` / `180%` / `185%` / `190%` / `195%` / `200%` | `beetle_psx_hw_mouse_sensitivity` | `5%` / `10%` / `15%` / `20%` / `25%` / `30%` / `35%` / `40%` / `45%` / `50%` / `55%` / `60%` / `65%` / `70%` / `75%` / `80%` / `85%` / `90%` / `95%` / `100%` / `105%` / `110%` / `115%` / `120%` / `125%` / `130%` / `135%` / `140%` / `145%` / `150%` / `155%` / `160%` / `165%` / `170%` / `175%` / `180%` / `185%` / `190%` / `195%` / `200%` |
| neGcon Twist Response | `Linear` ✅ / `Quadratic` / `Cubic` | `beetle_psx_hw_negcon_response` | `linear` / `quadratic` / `cubic` |
| neGcon Twist Deadzone | `0%` ✅ / `5%` / `10%` / `15%` / `20%` / `25%` / `30%` | `beetle_psx_hw_negcon_deadzone` | `5%` / `10%` / `15%` / `20%` / `25%` / `30%` |
| Memory Card 0 Method (Restart Required) | `Libretro` ✅ / `Mednafen` | `beetle_psx_hw_use_mednafen_memcard0_method` | `libretro` / `mednafen` |
| Enable Memory Card 1 (Restart Required) | `Activé` ✅ / `Désactivé` | `beetle_psx_hw_enable_memcard_1` | `enabled` / `disabled` |
| Shared Memory Cards (Restart Required) | `Activé` ✅ / `Désactivé` | `beetle_psx_hw_shared_memory_cards` | `enabled` / `disabled` |
| Memory Card Left Index | `0` ✅ / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `21` / `22` / `23` / `24` / `25` / `26` / `27` / `28` / `29` / `30` / `31` / `32` / `33` / `34` / `35` / `36` / `37` / `38` / `39` / `40` / `41` / `42` / `43` / `44` / `45` / `46` / `47` / `48` / `49` / `50` / `51` / `52` / `53` / `54` / `55` / `56` / `57` / `58` / `59` / `60` / `61` / `62` / `63` | `beetle_psx_hw_memcard_left_index` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `21` / `22` / `23` / `24` / `25` / `26` / `27` / `28` / `29` / `30` / `31` / `32` / `33` / `34` / `35` / `36` / `37` / `38` / `39` / `40` / `41` / `42` / `43` / `44` / `45` / `46` / `47` / `48` / `49` / `50` / `51` / `52` / `53` / `54` / `55` / `56` / `57` / `58` / `59` / `60` / `61` / `62` / `63` |
| Memory Card Right Index | `0` / `1` ✅ / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `21` / `22` / `23` / `24` / `25` / `26` / `27` / `28` / `29` / `30` / `31` / `32` / `33` / `34` / `35` / `36` / `37` / `38` / `39` / `40` / `41` / `42` / `43` / `44` / `45` / `46` / `47` / `48` / `49` / `50` / `51` / `52` / `53` / `54` / `55` / `56` / `57` / `58` / `59` / `60` / `61` / `62` / `63` | `beetle_psx_hw_memcard_right_index` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `21` / `22` / `23` / `24` / `25` / `26` / `27` / `28` / `29` / `30` / `31` / `32` / `33` / `34` / `35` / `36` / `37` / `38` / `39` / `40` / `41` / `42` / `43` / `44` / `45` / `46` / `47` / `48` / `49` / `50` / `51` / `52` / `53` / `54` / `55` / `56` / `57` / `58` / `59` / `60` / `61` / `62` / `63` |
| PGXP Operation Mode | `Désactivé` ✅ / `Memory Only` / `Memory + CPU (Buggy)` |`beetle_psx_hw_pgxp_mode` | `disabled` / `memory only` / `memory + CPU` |
| PGXP 2D Geometry Tolerance | `Désactivé` ✅ / `0px` / `1px` / `2px` / `3px` / `4px` / `5px` / `6px` / `7px` / `8px` | `beetle_psx_hw_pgxp_2d_tol` | `disabled` / `0px` / `1px` / `2px` / `3px` / `4px` / `5px` / `6px` / `7px` / `8px` |
| PGXP Primitive Culling | `Désactivé` ✅ / `Activé` | `beetle_psx_hw_pgxp_nclip` |  `disabled` / `enabled` |
| PGXP Vertex Cache | `Désactivé` ✅ / `Activé` | `beetle_psx_hw_pgxp_vertex` | `disabled` / `enabled` |
| PGXP Perspective Correct Texturing | `Désactivé` ✅ / `Activé` | `beetle_psx_hw_pgxp_texture` | `disabled` / `enabled` |
| Line-to-Quad Hack | `Default` ✅ / `Aggressive` / `Désactivé` | `beetle_psx_hw_line_render` | `default` / `aggressive` / `disabled` |
| Widescreen Mode Hack | `Désactivé` ✅ / `Activé` | `beetle_psx_hw_widescreen_hack` | `disabled` / `enabled` |
| Widescreen Mode Hack Aspect Ratio | `16:9` ✅ / `16:10` / `18:9` / `19:9` / `20:9` / `21:9` / `32:9` | `beetle_psx_hw_widescreen_hack_aspect_ratio` | `16:9` / `16:10` / `18:9` / `19:9` / `20:9` / `21:9` / `32:9` |
| CPU Frequency Scaling (Overclock) | `50%` / `60%` / `70%` / `80%` / `90%` / `100% (Native) ✅` / `110%` / `120%` / `130%` / `140%` / `150%` / `160%` / `170%` / `180%` / `190%` / `200%` / `210%` / `220%` / `230%` / `240%` / `250%` / `260%` / `270%` / `280%` / `290%` / `300%` / `310%` / `320%` / `330%` / `340%` / `350%` / `360%` / `370%` / `380%` / `390%` / `400%` / `410%` / `420%` / `430%` / `440%` / `450%` / `460%` / `470%` / `480%` / `490%` / `500%` / `510%` / `520%` / `530%` / `540%` / `550%` / `560%` / `570%` / `580%` / `590%` / `600%` / `610%` / `620%` / `630%` / `640%` / `650%` / `660%` / `670%` / `680%` / `690%` / `700%` / `710%` / `720%` / `730%` / `740%` / `750%` | `beetle_psx_hw_cpu_freq_scale` | `50%` / `60%` / `70%` / `80%` / `90%` / `100%(native)` / `110%` / `120%` / `130%` / `140%` / `150%` / `160%` / `170%` / `180%` / `190%` / `200%` / `210%` / `220%` / `230%` / `240%` / `250%` / `260%` / `270%` / `280%` / `290%` / `300%` / `310%` / `320%` / `330%` / `340%` / `350%` / `360%` / `370%` / `380%` / `390%` / `400%` / `410%` / `420%` / `430%` / `440%` / `450%` / `460%` / `470%` / `480%` / `490%` / `500%` / `510%` / `520%` / `530%` / `540%` / `550%` / `560%` / `570%` / `580%` / `590%` / `600%` / `610%` / `620%` / `630%` / `640%` / `650%` / `660%` / `670%` / `680%` / `690%` / `700%` / `710%` / `720%` / `730%` / `740%` / `750%` |
| GTE Overclock | `Désactivé` ✅ / `Activé` | `beetle_psx_hw_gte_overclock` | `disabled` / `enabled` |
| Skip BIOS | `Désactivé` ✅ / `Activé` | `beetle_psx_hw_skip_bios` | `disabled` / `enabled` |
| Override BIOS (Restart Required) | `Désactivé` ✅ / `PSP PS1 BIOS` / `PS3 PS1 BIOS` | `beetle_psx_hw_override_bios` | `disabled` / `psxonpsp` / `ps1_rom` |
| Dynarec DMA/GPU Event Cycles | `128 (Default)` ✅ / `256` / `384` / `512` / `640` / `768` / `896` / `1024` | `beetle_psx_hw_dynarec_eventcycles` | `128` / `256` / `384` / `512` / `640` / `768` / `896` / `1024` |
| Dynarec SPU Samples | `1 (Default)` ✅ / `4` / `16` | `beetle_psx_hw_dynarec_spu_samples` | `1` / `4` / `16` |
| Renderer (Restart) | `Hardware (auto)` ✅ / `Hardware (OpenGL)` / `Hardware (Vulkan)` / `Software` | `beetle_psx_hw_renderer` | `hardware` / `hardware_gl` / `hardware_vk` / `software` |
| CPU Dynarec | `Désactivé (Beetle Interpreter)` ✅ / `Max Performance` / `Cycle Timing Check` / `Lightrec Interpreter` | `beetle_psx_hw_cpu_dynarec` | `disabled` / `execute` / `execute_once` / `run_interpreter` |
| Dynarec Code Invalidation | `Full` ✅ / `DMA Only (Slightly Faster)` | `beetle_psx_hw_dynarec_invalidate` | `full` / `dma` |
| Core-Reported FPS Timing | `Progressive Rate (Default)` ✅ / `Force Interlaced Rate` / `Allow Automatic Toggling` | `beetle_psx_hw_core_timing_fps` | `force_progressive` / `force_interlaced` / `auto_toggle` |
| CD Access Method (Restart) | `Synchronous` ✅ / `Asynchronous` / `Pre-Cache` | `beetle_psx_hw_cd_access_method` | `sync` / `async` / `precache` |
| CD Loading Speed | `2x (Native)` ✅ / `4x` / `6x` / `8x` / `10x` / `12x` / `14x` | `beetle_psx_hw_cd_fastload` | `2x(native)` / `4x` / `6x` / `8x` / `10x` / `12x` / `14x` |

## ![](/emulators/external-links.png) Enlaces externos

* **Código fuente utilizado** : [https://github.com/libretro/beetle-psx-libretro/](https://github.com/libretro/beetle-psx-libretro/)
* **Documentación Libretro** : [https://docs.libretro.com/library/beetle_psx_hw/](https://docs.libretro.com/library/beetle_psx_hw/)