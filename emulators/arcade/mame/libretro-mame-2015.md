---
title: Libretro MAME 2015
description: 
published: true
date: 2024-07-24T07:04:49.568Z
tags: libretro, mame, mame2015
editor: markdown
dateCreated: 2021-07-05T23:29:52.097Z
---



## ![](/emulators/license.svg) License

This core is under **MAME non-commercial** license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ❌ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Features

| Feature | Supported |
| :---: | :---: |
| Saves |  |
| States |  |
| Rewind |  |
| Netplay |  |
| RetroAchievements |  |
| RetroArch Cheats |  |
| Native Cheats |  |
| Controllers |  |
| Multi-Mouse |  |
| Rumble |  |
| Sensors |  |
| Camera |  |
| Location |  |
| Subsystem |  |

## ![](/emulators/bios.svg) BIOS

BIOS romsets are not required when using "Full Non-Merged" arcade romsets. For "Split" and "Non-Merged" romsets, place the BIOS in the same directory as the game romset.

>**Note**:  
>Place the bios in the directory: `/recalbox/share/roms/mame/`   
>or in its subdirectory if you use several cores of Mame: `/recalbox/share/roms/mame/Mame2015/`
>Games and their bios must be in the same directory or sub-directory.
{.is-warning}

## ![](/emulators/roms.png) Roms

* Based on romset: MAME 0.160 (March 2015)
* DatFile: You can find the dat file in your recalbox at location `/recalbox/share/arcade/libretro/mame2015.dat`.

### Location

Put your roms like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 mame
┃ ┃ ┃ ┃ ┣ 🗒 **game.zip**
┃ ┃ ┃ ┃ ┣ 📁 **game**
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **game.chd**

BIOS romsets are not necessary when you want to use a subdirectory (useful if you want to have a mame set for another core).

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 mame
┃ ┃ ┃ ┃ ┣ 📁 Mame 2015
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **game.zip**
┃ ┃ ┃ ┃ ┃ ┣ 📁 **game**
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 **game.chd**

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Accessing to options

You can configure various options in two different ways.

* Via RetroArch menu:

┣ 📁 RetroArch Menu
┃ ┣ 📁 Core options
┃ ┃ ┣ 🧩 Name_option

* Via the file `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core options

| Options | Options values | Variables (for configuration override) | Variables values |
| --- | --- | --- | --- |
| Read configuration | `Disabled` ✅ / `Enabled` | `mame2015_read_config` | `disabled` / `enabled` |
| Auto save/load states | `Disabled` ✅ / `Enabled` | `mame2015_auto_save` | `disabled` / `enabled` |
| Enable in-game mouse | `Disabled` ✅ / `Enabled` | `mame2015_mouse_enable` | `disabled` / `enabled` |
| Enable throttle | `Disabled` ✅ / `Enabled` | `mame2015_throttle` | `disabled` / `enabled` |
| Enable cheats | `Disabled` ✅ / `Enabled` | `mame2015_cheats_enable` | `disabled` / `enabled` |
| Hide nag screen | `Disabled` ✅ / `Enabled` | `mame2015_hide_nagscreen` | `disabled` / `enabled` |
| Hide gameinfo screen | `Disabled` ✅ / `Enabled` | `mame2015_hide_infoscreen` | `disabled` / `enabled` |
| Hide warnings screen | `Disabled` ✅ / `Enabled` | `mame2015_hide_warnings` | `disabled` / `enabled` |
| Alternate render method | `Disabled` ✅ / `Enabled` | `mame2015_alternate_renderer` | `disabled` / `enabled` |
| Boot to OSD | `Disabled` ✅ / `Enabled` | `mame2015_boot_to_osd` | `disabled` / `enabled` |
| Boot from CLI | `Disabled` ✅ / `Enabled` | `mame2015_boot_from_cli` | `disabled` / `enabled` |

## ![](/emulators/external-links.png) External links

* **Progettosnaps**: [https://www.progettosnaps.net/](https://www.progettosnaps.net/)
* **Source code used**: [https://github.com/libretro/mame2015-libretro/](https://github.com/libretro/mame2015-libretro)