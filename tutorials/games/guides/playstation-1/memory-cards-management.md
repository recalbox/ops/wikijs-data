---
title: Gestion des cartes mémoire
description: 
published: true
date: 2021-09-08T13:01:10.322Z
tags: carte, ps1, gestion, mémoire
editor: markdown
dateCreated: 2021-05-21T08:48:00.985Z
---

## Activer la seconde carte mémoire

### Intérêt

Cela vous permet de disposer d'une seconde carte mémoire en jeu qui est commune à tous les jeux (donc unique).

### Étapes

* Lancez n'importe quel jeu PSX.
* Allez dans le menu de RetroArch avec `Hotkey + B` et allez dans `Options`.

![](/tutorials/games/guides/playstation-1/memory-card-management/psxbios1.png){.full-width}

* Activez l'option `Enable second memory card`.

![](/tutorials/games/guides/playstation-1/memory-card-management/psxbios2.png){.full-width}

* Ressortez du menu.

>Cette manipulation est à faire sur chaque jeu pour lesquels vous souhaitez avoir accès à cette carte mémoire. Il est recommandé de quitter le jeu et de le relancer ensuite.
{.is-warning}

Désormais, en jeu, vous verrez une carte mémoire utilisable dans le slot 2. Dans le dossier `/share/saves/psx/`, vous verrez un fichier nommé `pcsx-card2.mcd` qui est le fichier de carte mémoire.

## Accéder à l'éditeur de carte mémoire

### Intérêt

Cela vous permet de gérer vos sauvegardes de jeu (copier ou supprimer) si vous en avez la nécessité.

### Étapes

* Lancez un jeu.
* Allez dans le menu de RetroArch avec `Hotkey + B` et allez dans `Options`.

![](/tutorials/games/guides/playstation-1/memory-card-management/psxbios1.png){.full-width}

* Activez l'option `Show Bios Bootlogo`.

![](/tutorials/games/guides/playstation-1/memory-card-management/psxbios3.png){.full-width}

* Retournez en jeu et quittez complètement le jeu.
* Relancez le jeu et pendant la séquence de boot, éjectez le jeu avec `Hotkey + Stick gauche vers le haut`.
* Vous devriez arriver dans le bios de la console pour gérer vos sauvegardes.

![](/tutorials/games/guides/playstation-1/memory-card-management/psxbios4.png){.full-width}

Et si vous avez activé la seconde carte mémoire comme vu plus haut, vous devriez pouvoir copier vos données entre cartes.

![](/tutorials/games/guides/playstation-1/memory-card-management/psxbios5.png){.full-width}