---
title: Beim Projekt mitmachen
description: 
published: true
date: 2021-06-21T13:38:19.126Z
tags: 
editor: markdown
dateCreated: 2021-06-21T13:38:19.126Z
---

Recalbox ist ein kostenloses Open-Source-Projekt, und wir würden uns freuen, wenn auch Du dazu beiträgst.

## Gesuchte Profile
Wenn du über Fertigkeiten verfügst und sie zur Verbesserung des Projekts einsetzen möchtest, kannst du uns das auf unserem Discord mitteilen.

Wir sind immer auf der Suche nach geeigneten Leuten, die uns helfen können:
- **Grafikdesigner** : Recalbox, Website, soziale Netzwerke
- **UX Designer**: UX Recalbox, Web, UI
- **Entwickler**: bash, c, c++, python (python 2 > 3)
- **Übersetzer**: Übersetzung und Korrektur von Recalbox, Webseiten, Wiki
- **Aufmerksame Benutzer**: detaillierte Fehlerberichte

## Fehlerberichte
Die erste Möglichkeit der Teilnahme besteht darin, die Fehler, die du auf Recalbox findest, detailliert an uns zu melden.

Gitlab **[Issues](https://gitlab.com/recalbox/recalbox/-/issues)** sind Tickets für Fehler oder Erweiterungsanfragen, die es Benutzern, Entwicklern und Testern ermöglichen, das Thema an einem Ort zu diskutieren.

Wenn du einen Fehler meldest, zögere nicht, so präzise wie möglich zu sein. Erläutere den Zusammenhang, füge Screenshots hinzu und erkläre, wie du den Fehler behoben hast, falls dies der Fall ist.

## Entwicklung

Recalbox-Quellen sind auf [GitLab](https://gitlab.com/recalbox/recalbox) verfügbar.

Das Entwickeln für Recalbox erfordert Verständnis über die Funktionsweise der Systemgrundlagen: **buildroot**.


Sobald Du verstanden hast, wie es funktioniert, kannst Du damit beginnen, Dein eigenes Recalbox-Image zu erstellen, wie in der [README.md](https://gitlab.com/recalbox/recalbox/-/blob/master/README.md) des Projekts beschrieben.

Weiterhin ist es notwendig, die Grundlagen der **bash** und der Linux-Befehle zu kennen.

Je nach deinen Fähigkeiten kannst du mit verschiedenen Themen beginnen:
- Pakete in buildroot aktualisieren (bash, kompilieren)
- Pakete in buildroot hinzufügen (bash, kompilieren)
- Eigenständige Emulatoren hinzufügen (bash, kompilieren, c/c++, python)
- Funktionen zu EmulationStation hinzufügen (c++)

Wir suchen nicht unbedingt nach supertechnischen Leuten, sondern eher nach jemandem, der motiviert und selbstständig ist und weiß, wie man in einem Team auf Basis des Open-Source-Modells arbeitet.

## Übersetzungen

### Wiki

Wenn du dich an der **Übersetzung des Wikis** beteiligen möchtest, besuche die Seite [Zum Recalbox Wiki beitragen](./contribute)

### Oberfläche

Wenn du dich an der **Übersetzung der EmulationStation-Oberfläche** beteiligen möchtest, kannst du der Projektseite unter [poeditor.com](https://poeditor.com/join/project/hEp5Khj4Ck) beitreten. Es werden regelmäßig Aktualisierungen der Quellsprachdatei vorgenommen, die wir dir zur Übersetzung in die Sprachen deiner Wahl zur Verfügung stellen werden.

### **YouTube Videos**

Wenn du dich an der **Übersetzung von Untertiteln für die Tutorial-Videos von Recalbox** beteiligen möchtest, kontaktiere bitte das Team auf unseren **[sozialen Netzwerken, Discord oder im Forum](./presentation/useful-links).**
