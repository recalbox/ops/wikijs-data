---
title: 7. 📋 A PROPOS DE RECALBOX
description: Comment contribuer, les licences, les liens utiles...
published: true
date: 2023-07-18T11:54:04.198Z
tags: 
editor: markdown
dateCreated: 2021-05-21T07:48:58.198Z
---

Vous trouverez ici la présentation générale de Recalbox, qui sommes-nous, les licences d'utilisations ainsi que la possibilité de contribuer au projet de différentes manières !