---
title: Libretro Handy
description: 
published: true
date: 2024-06-22T10:02:16.531Z
tags: libretro, atari, lynx, handy
editor: markdown
dateCreated: 2021-05-21T08:29:01.997Z
---

**Libretro Handy** est un émulateur de système de jeu vidéo Atari Lynx qui peut être utilisé comme noyau de libretro.
Handy était le nom original du projet Lynx qui a été lancé chez Epyx puis terminé par Atari.

## ![](/emulators/license.svg) Licence

Ce core est sous licence [**zlib**](https://sourceforge.net/projects/handy/).

## ![](/emulators/compatibility.png) Compatibilité

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Fonctionnalités

| Fonctionnalité | Supporté |
| :---: | :---: |
| Redémarrage | ✔ |
| Captures d'écran | ✔ |
| Sauvegardes instantanées | ✔ |
| Rembobinage | ✔ |
| Netplay (State based) | ✔ (pas d'émulation du câble link) |
| Options du core | ✔ |
| RetroAchievements | ✔ |
| Controls | ✔ |
| Remapping | ✔ |

## ![](/emulators/bios.svg) BIOS

### Liste des bios obligatoires

| Nom de fichier | Description | MD5 | Fourni |
| :---: | :---: | :---: | :---: |
| lynxboot.img | Image Boot Lynx | fcd403db69f54290b51035d82f835e7b | ❌  |

### Emplacement

Placez le BIOS comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 lynx
┃ ┃ ┃ ┃ ┣ 🗒 **lynxboot.img**

## ![](/emulators/roms.png) Roms

### Extensions supportées

La rom doit avoir l'extension :

* .lnx
* .o
* .zip
* .7z

Ce système supporte les roms compressées au format .zip/.7z. Attention toutefois, il ne s'agit que d'une archive.

Les fichiers contenus dans les .zip/.7z doivent correspondre aux extensions citées précédemment.
De plus, chaque fichier .zip/.7z ne doit contenir qu'une seule rom compressée.

### Emplacement

Placez les roms comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 lynx
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.lnx**

>Les roms au format **No-Intro** sont vivement conseillées.
{.is-success}

>Pour plus d'information sur les roms, rendez-vous sur [ce tutoriel](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuration avancée de l'émulateur

>Pour pouvoir conserver vos configurations personnalisées lors d'une mise à jour, nous vous conseillons d'utiliser notre fonctionnalité [Surcharges de configuration](./../../../advanced-usage/configuration-override).
{.is-info}

### Accéder aux options

Vous pouvez configurer diverses options de deux façons différentes.

* Via le menu RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via le fichier `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Options du core

| Options | Valeurs des options | Variable (pour surcharge) | Valeurs des variables |
| --- | --- | --- | --- |
| Video Refresh Rate | `50Hz` / `60Hz` ✅ / `75Hz` / `100Hz` / `125Hz` | `handy_refresh_rate` | `50` / `60` / `75` / `100` / `125` |
| Rotation d'écran | `Auto` ✅ / `Désactivé` / `Horaire` / `Bottom` / `Anti-horaire` | `handy_rot` | `Auto` / `None` / `90` / `180` / `270` |
| Profondeur des couleurs (Redémarrage requis) | `Milliers (16 bits)` ✅ / `Millions (24 bits)` | `handy_gfx_colors` | `16bit` / `24bit` |
| Filtre de rémanence LCD | `Désactivé` ✅ / `2 Frames` / `3 Frames` / `4 Frames` | `handy_lcd_ghosting` | `disabled` / `2frames` / `3frames` / `4frames` |
| CPU Overclock Multiplier | `1` ✅ / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `20` / `30` / `40` / `50` | `handy_overclock` | `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `20` / `30` / `40` / `50` |
| Saut d'images | `Désactivé` ✅ / `Auto` / `Manuel` | `handy_frameskip` | `disabled` / `auto` / `manual` |
| Seuil de saut d'images (%) | `15` / `18` / `21` / `24` / `27` / `30` / `33` ✅ / `36` / `39` / `42` / `45` / `48` / `51` / `54` / `57` / `60` | `handy_frameskip_threshold` | `15` / `18` / `21` / `24` / `27` / `30` / `33` / `36` / `39` / `42` / `45` / `48` / `51` / `54` / `57` / `60` |

## ![](/emulators/external-links.png) Liens externes

* **Code source utilisé** : [https://github.com/libretro/libretro-handy](https://github.com/libretro/libretro-handy)
* **Documentation Libretro** : [https://docs.libretro.com/library/handy/](https://docs.libretro.com/library/handy/)
* **Code source officiel** : [http://handy.sourceforge.net/download.htm](http://handy.sourceforge.net/download.htm)
* **Site officiel** :  [http://handy.sourceforge.net/](http://handy.sourceforge.net/)