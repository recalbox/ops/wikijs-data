---
title: FAQ Recalbox 9
description: 
published: true
date: 2023-08-31T13:42:55.532Z
tags: faq
editor: markdown
dateCreated: 2023-07-18T07:37:55.233Z
---

# GPiCase2W et Raspberry Pi Zero (pas 2)

Pour installer Recalbox 9 sur **GPiCase2W** et **Raspberry Pi Zero premier du nom**, voici la marche à suivre:
- Téléchargez [l'image pour Raspberry Pi Zero / 1](https://upgrade.recalbox.com/latest/download/rpi1/recalbox-rpi1.img.xz) ou installez la depuis Rpi Imager.
- Procédez a l'installation sur votre RPi Zero, dans votre GPiCase2W, l'écran va être en tout petit.
- Après l'installation éteignez votrre GPiCase2W et récupérez la carte SD.
- Dans la partition `RECALBOX`, remplacez le fichier `recalbox-user-config.txt` par le fichier suivant: [recalbox-user-config.txt](https://recalbox-public-assets.s3.fr-par.scw.cloud/recalbox-user-config.txt)
- Remettez la carte SD dans votre GPiCase2W et profitez !

![gpi.jpg](/faq9/gpi.jpg)

## Pas de vidéo d'introduction
Les vidéos d'introduction peuvent maintenant être sautées en appuyant sur un bouton ou une direction sur un contrôleur.

Certains utilisateurs les sautaient automatiquement parce que le contrôleur USB envoyait des rapports d'entrée aléatoires lors du démarrage de Recalbox.

Veuillez vérifier si le fait de débrancher votre contrôleur avant de démarrer résout le problème, et nous vérifierons si nous pouvons résoudre ce problème dans une prochaine version.


## Shinobi redémarre lorsque l'on rembobine sur master system

Modifiez les informations du jeu et sélectionnez un autre core pour éviter ce problème.
