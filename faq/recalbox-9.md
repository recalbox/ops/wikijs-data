---
title: FAQ Recalbox 9
description: Answers to frequently asked questions for Recalbox 9
published: true
date: 2023-07-18T07:32:54.979Z
tags: 
editor: markdown
dateCreated: 2023-07-18T07:32:54.979Z
---

## GPiCase2W and Raspberry Pi Zero (not 2)

To install Recalbox 9 on **GPiCase2W** and **Raspberry Pi Zero first of the name**, here are the steps to follow:
- Download [the image for Raspberry Pi Zero / 1](https://upgrade.recalbox.com/latest/download/rpi1/recalbox-rpi1.img.xz) or install it from Rpi Imager.
- Proceed to the installation on your RPi Zero, in your GPiCase2W, the screen will be very small.
- After the installation turn off your GPiCase2W and get the SD card.
- In the `RECALBOX` partition, replace the `recalbox-user-config.txt` file with the following file: [recalbox-user-config.txt](https://recalbox-public-assets.s3.fr-par.scw.cloud/recalbox-user-config.txt)
- Put the SD card back in your GPiCase2W and enjoy!

![gpi.jpg](/faq9/gpi.jpg)

## No intro video
Intro videos can now be skipped while pressing a button or a direction on a controller.

Some user have it automatically skipped because the USB controller sends random input reports while starting recalbox.

Please check if unplugging your controller before booting fix the issue, and we will check if we can fix that a in future version.

## Shinobi restarts when rewind on master system

Edit game informations and select an other core to avoid this issue.


