---
title: Libretro Gong
description: 
published: true
date: 2025-01-04T06:27:55.480Z
tags: libretro, gong, 8.0+
editor: markdown
dateCreated: 2021-11-02T10:14:44.201Z
---

Libretro Gong is an open source implementation of Pong written by Dan Zeitan in 2018.

## ![](/emulators/license.svg) License

This core is under [**GPLv3**](https://github.com/libretro/gong/blob/master/COPYING) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Features

| Feature | Supported |
| :---: | :---: |
| Core Options | ✔ |

## ![](/emulators/bios.svg) BIOS

>**No bios is required.**
{.is-success}

## ![](/emulators/roms.png) Roms

The Gong core does not involve the use of extensions. Just load and start the core.

### Location

Put your roms like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 ports
┃ ┃ ┃ ┃ ┣ 📁 Gong
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **gong.c**

This rom is included in your Recalbox.

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Accessing to options

You can configure various options in two different ways.

* Via RetroArch menu:

┣ 📁 RetroArch Menu
┃ ┣ 📁 Core options
┃ ┃ ┣ 🧩 Name_option

* Via the file `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core options

| Options | Options values | Variables (for configuration override) | Variables values |
| --- | --- | --- | --- |
| Player 2 | `CPU` ✅ / `Human` | `gong_player2` | `CPU` / `Human` |
| Video Refresh Rate (Restart) | `60` ✅ / `70` / `72` / `75` / `100` / `119` / `120` / `140` / `144` | `gong_refresh` | `60` / `70` / `72` / `75` / `100` / `119` / `120` / `140` / `144` |

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/libretro/gong/](https://github.com/libretro/gong/)
* **Official source code**: [https://danzaidan.itch.io/pong-in-c](https://danzaidan.itch.io/pong-in-c)