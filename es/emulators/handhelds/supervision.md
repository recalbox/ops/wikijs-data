---
title: Supervision
description: 
published: true
date: 2024-07-26T08:34:50.089Z
tags: watara, supervision, 7.2+, portátiles
editor: markdown
dateCreated: 2023-09-04T12:22:32.324Z
---

![](/emulators/handheld/supervision.svg){.align-center}

## Datos técnicos

* **Fabricante**: Watara
* **Año de lanzamiento**: 1992
* **Procesador**: 65SC02 de 8 bits a 4 MHz
* **RAM**: 8kB
* **VRAM**: 8kB
* **Pantalla**: 160x160 píxeles, 4 tonos de gris
* **Sonido**: 2 canales tonales y 1 canal de ruido más salida de audio estéreo DMA adicional

## Presentación

La **Watara Supervision** es una consola portátil monocroma diseñada en Hong Kong y presentada en 1992 como competidora de Game Boy. Fue distribuida en Francia por AudioSonic.

Los principales argumentos de la Supervision para competir con la Game Boy eran su pantalla más grande (6 × 6 cm) y su bajo precio de lanzamiento. Sin embargo, la consola no tuvo el éxito esperado, principalmente por su limitada biblioteca de software y la calidad de la pantalla. Cabe destacar la baja resolución de la pantalla, que, unida a su tamaño, la convierte en la más horrible y arcaica (cf. TI-80/81 o los primeros teléfonos móviles Nokia), y también que sus cartuchos se parecen mucho a los de Mega Duck/Cougar Boy, que también son superiores a los de Watara Supervision.

## Emuladores

[Libretro Potator](libretro-potator)