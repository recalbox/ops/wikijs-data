---
title: GameHat configuration
description: 
published: true
date: 2021-08-06T15:30:24.779Z
tags: configuration, gamehat
editor: markdown
dateCreated: 2021-08-06T15:30:24.779Z
---

## Configuration

For all those who want to use Recalbox with the GameHat, here is how to do it:

* Flash the Recalbox image to your SD card.
* From a PC/Mac:
  * Plug in the SD card
  * Add these settings to the `recalbox-user-config.txt` file in the root of the card:

```ini
hdmi_force_hotplug=1
avoid_warnings=1
max_usb_current=1
hdmi_group=2
hdmi_mode=1
hdmi_mode=87
hdmi_cvt 640 480 60 6 0 0 0
hdmi_drive=2
display_rotate=0
```

* Connect a gamepad (wired) and fill in the wifi settings.
* When you are connected to your network, via the web interface (or SSH)
  * Modify the `recalbox.conf` file like this:
    * Change the line `controllers.gpio.enabled=0` to `controllers.gpio.enabled=1`.
    * Change the line `controllers.gpio.args=map=1,2` to `controllers.gpio.args=map=5 gpio=5,6,13,19,21,4,26,12,23,20,16,18,-1`
* Unplug the controller and restart your Recalbox.
* After rebooting, press a button and configure!

## External links

* Link to the GameHat Wiki: [https://www.waveshare.com/wiki/Game\_HAT](https://www.waveshare.com/wiki/Game_HAT)