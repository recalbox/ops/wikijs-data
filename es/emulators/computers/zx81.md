---
title: ZX81
description: 
published: true
date: 2024-07-26T12:53:47.392Z
tags: sinclair, spectrum, zx81, ordenadores
editor: markdown
dateCreated: 2023-09-03T13:07:27.552Z
---

![](/emulators/computers/zx81.svg){.align-center}

## Especificaciones técnicas

* **Desarrollador**: Sinclair Research
* **Fabricante**: Timex Corporation
* **Año de lanzamiento**: 1981
* **Cantidades vendidas**: 1,5 millones
* **Procesador**: Z80 a 3,25 MHz
* **Memoria**: 1 KB (64 KB máximo 56 KB utilizables)
* **Almacenamiento**: Grabador/reproductor de casetes externo de 250 baudios
* **Pantalla**: Monocromo en canal UHF
* **Gráficos**: 24 líneas x 32 caracteres o 64 x 48 píxeles modo gráfico

## Presentación

El **Sinclair ZX81** fue un ordenador personal de 8 bits diseñado por Sinclair Research y comercializado por Timex Corporation en marzo de 1981. La carcasa era negra con un teclado de membrana; el aspecto distintivo de la máquina era obra del diseñador industrial Rick Dickinson.

Sucesor del ZX80, este ordenador de bajo coste se basaba en un microprocesador Zilog Z80A a 3,25 MHz y disponía de 1 kB de RAM para programas y pantalla. La visualización era en blanco y negro (24 líneas de 32 caracteres) en un televisor estándar. El ordenador no tenía salida de sonido.

El objetivo al diseñar el ZX81 era producir un ordenador con un rendimiento modesto, pero a un coste razonable. Esto significaba reducir el número de componentes y optimizar su uso. Hay dos características destacables a este respecto:

* la presencia de un circuito integrado dedicado, el ULA. Diseñado específicamente para el ZX81, Diseñado específicamente para el ZX81, sustituyó a dieciocho circuitos TTL estándar del ZX80 original ;
* pero quizá el aspecto más ingenioso fue la técnica utilizada para el control de vídeo, que corría a cargo del propio microprocesador. Así, la frecuencia de 3,25 MHz sólo se justificaba para permitir que el Z80 fuera compatible con la frecuencia de barrido horizontal de los televisores.

La mitad del tiempo de máquina (49,92%: 192×2×52 µs / (625×64 µs)) se dedicaba a trazar los puntos de las matrices de caracteres en la pantalla. El código del programa de usuario sólo se procesaba durante los avances de línea del CRT y mientras se escaneaban las zonas en blanco de la parte superior e inferior de la pantalla.

No obstante, el comando RÁPIDO liberaba al microprocesador del control del vídeo. La pantalla de TV se cubría entonces de nieve hasta que un comando LENTO reactivaba la visualización. Las operaciones se realizaban entonces el doble de rápido. Nada impedía realizar operaciones de PRINT o PLOT durante el modo RÁPIDO, pero el resultado final no se mostraba hasta que se restablecía el modo LENTO.

La programación se realizaba en BASIC Sinclair tecleando las instrucciones impresas en las teclas del teclado. BASIC ofrecía la posibilidad de programar indirectamente en lenguaje máquina utilizando los comandos PEEK y POKE. La máquina se ofreció como kit de montaje por 490 FF en 1981.

Disponía de numerosos periféricos, como una ampliación de memoria de 16 kB, una impresora térmica compacta y un teclado que se pegaba al teclado original. Se comercializaban programas informáticos, incluidos numerosos juegos (Flight Simulator, Cobalt Simulator, Mazzog). Los programas y datos podían guardarse en una grabadora de casete.

Su precio (imbatible en la época sin las ampliaciones) y, sobre todo, su manual, que enseñaba los fundamentos de la programación BASIC de forma muy didáctica, hicieron de él un gran éxito, a pesar de su escaso rendimiento, su BASIC incompleto, su baja resolución semigráfica (pero accesible en BASIC) y su falta de color. Recibió el nombre de "El Iniciador" y permitió a muchos futuros informáticos dar sus primeros pasos a bajo coste y con poco riesgo.

La versión americana del ZX81 fue el **Timex Sinclair 1000**, cuya diferencia fundamental con el ZX81 era que disponía de 2 kB de RAM.

## Emuladores

[Libretro 81](libretro-81)