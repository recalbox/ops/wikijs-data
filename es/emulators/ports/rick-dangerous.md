---
title: Rick Dangerous
description: 
published: true
date: 2024-07-26T08:31:09.932Z
tags: ports, rick dangerous
editor: markdown
dateCreated: 2023-09-04T12:49:08.580Z
---

![](/emulators/ports/rick-peligroso.png){.align-center}

## Ficha técnica

* **Creado por :** Core Design
* **Año de lanzamiento :** 1989
* **Género :** Plataformas, Morir y reintentar
* **Sistema operativo:** Amiga 500, Amstrad CPC, Atari ST, Commodore 64, DOS y ZX Spectrum.
* **Programación y diseño:** Simon Phipps
* **Gráficos:** Simon Phipps, Terry Lloyd
* **Música:** Dave Pridmore
* **Diseño de niveles:** Rob Toone, Bob Churchill
* **Programación para Amstrad y Spectrum:** Dave Pridmore
* **Programación C64:** Stu Gregg
* **Editor**: Rainbird Software
**PEGI**: 3

## Presentación

**Rick Dangerous** es un videojuego desarrollado por Core Design y publicado por Rainbird Software en 1989. Rick Dangerous, el primer juego original y el primer éxito de Core Design, se lanzó en Amiga 500, Amstrad CPC, Atari ST, Commodore 64, DOS y ZX Spectrum. El juego adquirió estatus de culto y posteriormente se lanzaron muchas versiones amateur: ports, clones y remakes.

Mucho antes que Lara Croft, en los años 80 y principios de los 90, Rick Dangerous era el Indiana Jones de los videojuegos, huyendo de rocas rodantes, evitando trampas, desde Sudamérica hasta una base de misiles futurista, pasando por Egipto y el castillo Schwarzendumpf.

Se trata de un juego de plataformas con gráficos de dibujos animados en el que el jugador asume el papel de Rick Dangerous, una especie de Indiana Jones en busca de la tribu perdida de Goolus en el Amazonas. Pero su avión se estrella, y ahí empieza el juego. La acción se desarrolla en 1945.

## Emuladores

[Libretro XRick](libretro-xrick)