---
title: Raspberry PI hats
description: 
published: true
date: 2023-09-19T08:27:16.085Z
tags: rpi, hats, compatible, 8.0+
editor: markdown
dateCreated: 2023-09-19T08:27:16.085Z
---

# Raspberry PI HATs compatibles con Recalbox

## Controladores PoE

Los controladores PoE proporcionan alimentación a la Raspberry PI a través del conector ethernet. ¡No hay necesidad de una fuente de alimentación mini-USB o USB-C!

Para alimentar la RPI, necesitaréis un switch ethernet compatible con la norma IEEE 802.3af o la IEEE 802.3at.

### Waveshare PoE hat (b)

Este hat alimenta el RPI a través de ethernet y tiene una pantalla oled monocromática y un ventilador DC programable (encendido/apagado). Es posible forzar la activación del ventilador mediante un interruptor en la tarjeta.

Este hat es compatible con RPI 3b+ y RPI 4.

El programa [recalbox-wpaf] (/es/advanced-usage/fan-controller) soporta este hat y permite controlar el ventilador (encendido/apagado dependiendo de la temperatura de la CPU).

### Raspberry PI PoE+

Este hat alimenta el RPI a través de ethernet y tiene un ventilador de velocidad variable (PWM).

Este hat es compatible con RPI 3b+ y RPI4.

El programa [recalbox-wpaf] (/en/advanced-usage/fan-controller) soporta este hat y permite controlar el ventilador (activar/desactivar dependiendo de la temperatura de la CPU).

## Cartas con ventilador

Este tipo de cartas tienen un ventilador para refrigerar la CPU.

### Ventilador Pimoroni SHIM

Esta carta se acopla a una parte del conector GPIO de la Raspberry PI. Tiene un ventilador silencioso, un LED RGB y un botón. La función de ventilador está soportada por el programa [recalbox-wpaf] (/es/advanced-usage/fan-controller).

