---
title: Amstrad CPC
description: 
published: true
date: 2021-08-09T13:56:41.993Z
tags: computers, amstrad, cpc
editor: markdown
dateCreated: 2021-07-28T19:04:02.647Z
---

![](/emulators/computers/amstradcpc.svg){.align-center}

## Technical data

* **Manufacturer**: Amstrad
* **Year of release**: 1984
* **OS**: AMSDOS with Locomotive BASIC
* **CPU**: Zilog Z80A @ 4 MHz
* **RAM**: 64 KB
* **Sound chip**: General Instrument AY-3-8910

## Presentation

The **Amstrad CPC** is an 8-bit personal computer produced by Amstrad in the 1980s. CPC stands for Colour Personal Computer, although a version with a monochrome monitor was available.

The Amstrad CPC sold about three million units worldwide, including about one million in France.

## Emulators

[Libretro CrocoDS](libretro-crocods)
[Libretro Cap32](libretro-cap32)