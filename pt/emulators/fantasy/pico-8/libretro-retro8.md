---
title: Libretro Retro8
description: 
published: true
date: 2021-10-20T00:01:36.728Z
tags: libretro, pico-8, retro8, 7.2+
editor: markdown
dateCreated: 2021-10-20T00:01:36.728Z
---

O Libretro Retro8 é um núcleo desenvolvido para rodar jogos, os quais foram criados a partir deste mesmo emulador.

## ![](/emulators/license.svg) Licença

Este núcleo está sob licença [**GPLv3**](https://github.com/libretro/retro8/blob/master/LICENSE).

## ![](/emulators/compatibility.png) Compatibilidade

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ 

## ![](/emulators/features.png) Recursos

| Recurso | Suportado |
| :---: | :---: |

## ![](/emulators/bios.svg) BIOS

> **Não é necessária nenhuma BIOS.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Extensões suportadas

As Roms deve ter a extensão:

* .p8
* .png
* .zip
* .7z

### Local

Coloque suas roms assim:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 pico8
┃ ┃ ┃ ┃ ┣ 🗒 **jogo.png**

## ![](/emulators/advanced-configuration.png) Configuração avançada do emulador

> **Este núcleo não tem opção.**
{.is-success}

## ![external-links.png](/emulators/external-links.png) Links externos

* **Código fonte utilizado** : [https://github.com/libretro/retro8/](https://github.com/libretro/retro8/)
* **Código fonte oficial** : [https://github.com/Jakz/retro8/](https://github.com/Jakz/retro8/)
* **Fórum do Pico-8** : [https://www.lexaloffle.com/bbs/?cat=7](https://www.lexaloffle.com/bbs/?cat=7)