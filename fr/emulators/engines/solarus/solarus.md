---
title: Solarus
description: 
published: true
date: 2024-06-23T00:15:45.369Z
tags: solarus
editor: markdown
dateCreated: 2021-05-21T08:28:44.575Z
---

![](/emulators/fantasy/solarusarpg.svg){.align-center}

**Solarus** est un core standalone qui permet de jouer à des jeux homebrew au style Action-RPG 2D.

## ![](/emulators/license.svg) Licence

Ce core est sous licence [**GPLv3**](https://www.solarus-games.org/fr/about/legal).

## ![](/emulators/compatibility.png) Compatibilité

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Fonctionnalités

| Fonctionnalité | Supporté |
| :---: | :---: |
| Redémarrage | ✔︎ |
| Captures d'écran | ✔︎ |
| Sauvegardes | ✔︎ |
| Options du core | ✔︎ |
| Contrôles | ✔︎ |
| Remapping | ✔︎ |
| Vibration | ✔︎ |
| Disk Control | ✔︎ |

## ![](/emulators/bios.svg) BIOS

>**Aucun bios n'est requis.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Extensions supportées

Les roms doivent avoir l'extension :

* .solarus

### Emplacement

Placez les roms comme ceci : 

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 solarus
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.solarus**

### Liste des jeux

Il dispose d'un catalogue de jeux compatible avec le moteur :  
[https://www.solarus-games.org/fr/games](https://www.solarus-games.org/fr/games)

![](/emulators/fantasy/solarusgamelist.png){.align-center}

## ![](/emulators/advanced-configuration.png) Configuration avancée de l'émulateur

>**Ce core n'a pas d'option.**
{.is-success}

## ![](/emulators/external-links.png) Liens externes

* **Code source utilisé** : [https://gitlab.com/bkg2k/solarus](https://gitlab.com/bkg2k/solarus)
* **Site officiel** : [https://solarus-games.org/fr/](https://solarus-games.org/fr)
* **Jeux Homebrew** : [https://www.solarus-games.org/fr/games/](https://www.solarus-games.org/fr/games)