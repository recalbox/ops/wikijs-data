---
title: Jouer en multi-joueurs (3 à 4 joueurs)
description: 
published: true
date: 2024-01-07T23:27:27.365Z
tags: nintendo, super, multi-joueurs
editor: markdown
dateCreated: 2021-05-21T08:48:19.606Z
---

Il est possible de jouer jusqu’à 4 sur l'émulateur SNES sur Recalbox.

Si vous souhaitez pouvoir profiter du multi-joueur sur SNES, vous aurez besoin d'une manette par joueur, correctement configurée.

* Lancez un jeu SNES, comme Micro Machines par exemple.
* Quand le jeu démarre, allez dans le menu de RetroArch en faisant `Hotkey + B`.
* Faites « Retour » 2 fois puis allez dans `Réglages` > `Configuration`.

![](/tutorials/games/guides/snes/multitap/snesmultitap1.png){.full-width}

* Activez le paramètre `Sauvegarder la configuration en quittant`.

![](/tutorials/games/guides/snes/multitap/snesmultitap2.png){.full-width}

* Faires « Retour » 2 fois et allez dans `Menu principal` > `Menu rapide` > `Touches` > `Touches du port 2`.

![](/tutorials/games/guides/snes/multitap/snesmultitap3.png){.full-width}

* Sur l'option `Type de périphérique`, faites défiler jusqu'à avoir le texte `Multitap`affiché.

![](/tutorials/games/guides/snes/multitap/snesmultitap4.png){.full-width}

* Faites « Retour » 1 fois et allez dans `Gérer les fichiers de remappage`. Ici, sélectionnez pour quel cas vous enregistrez l'option multitap (pour le jeu, pour le dossier de jeux, etc.).

* Faites « Retour » 2 fois et sélectionnez `Redémarrer`.

Et voilà. Le jeu fonctionne, et vous êtes le roi de la soirée !