---
title: Vérifier l'empreinte MD5 d'une rom ou bios
description: 
published: true
date: 2021-09-04T21:40:02.358Z
tags: rom, md5, bios, empreinte
editor: markdown
dateCreated: 2021-05-21T08:42:17.746Z
---

Pour vérifier le hash d'une ou plusieurs roms manuellement depuis votre ordinateur :

## {.tabset}
### Windows
Télécharger ce logiciel [md5sum](http://esrg.sourceforge.net/utils_win_up/md5sum/).​

### Linux
* Depuis un `terminal` `md5sum /path/to/file` .
* Remplacer `/path/to/file` par le chemin de votre fichier.

### macOS
* Ouvrir `Terminal` .
* Saisir `md5sum /path/to/file` (remplacez `/path/to/file` par le chemin de votre fichier).
* Clic droit sur votre fichier, choisir `Copier`.
* Puis `Coller` dans la fenêtre du `Terminal`.