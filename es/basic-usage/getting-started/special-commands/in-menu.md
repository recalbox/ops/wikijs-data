---
title: Dentro del menú
description: Desde el menú de EmulationStation, podéis ejecutar órdenes especiales
published: true
date: 2023-09-03T13:26:03.782Z
tags: comandos especiales, menú
editor: markdown
dateCreated: 2023-09-03T13:26:03.782Z
---

## Función de búsqueda

Desde el menú de todos los sitemas se puede activar la **función de búsqueda**.

* `R1`

## Menú de apagado

Desde el menú de todos los sitemas, para apagar correctamente el sistema:

* `Select` > `Quitar`

## Lanzar los vídeos de juegos

Si habéis elegido utilizar como pantalla de reposo los vídeos de los juegos scrapados, los podéis lanzar desde la lista de todos los sistemas sin tener que esperar que se cumpla el tiempo de inactividad configurado en la interfaz.

* `Y`