---
title: 🗃️ Utilidades
description: 
published: true
date: 2024-07-26T07:30:25.654Z
tags: tutorial, utilidades, programas
editor: markdown
dateCreated: 2023-09-01T09:35:22.039Z
---

Aquí encontraréis una lista de utilidades relacionadas con vuestras roms, scraps, etc.

Lista de categorías disponibles:

[Conversión de roms](rom-conversion)
[Flasher una imagen](write-an-image)
[Gestión de los ficheros dat](dat-management)
[Gestión de roms](rom-management)
[Gestión de scrapes](scrap-management)

Tutoriales disponibles:

[Actualización del bootloader del Raspberry Pi 4](update-rpi4-bootloader)
[Lista de utilidades interesantes](useful-tools-list)