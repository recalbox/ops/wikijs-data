---
title: OpenBOR
description: 
published: true
date: 2024-07-24T20:59:26.200Z
tags: openbor
editor: markdown
dateCreated: 2021-07-04T18:00:35.947Z
---



## ![](/emulators/license.svg) License

This core is under [**BSD-3**](https://github.com/DCurrent/openbor/blob/master/LICENSE) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Features



## ![](/emulators/bios.svg) BIOS

>**No bios is required.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Supported extensions

Roms must have the extension:

* .pak

### Location

Put your roms like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 openbor
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.pak**

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>**This core have no option.**
{.is-success}

## ![](/emulators/external-links.png) External links

* **Source code used** : [https://gitlab.com/Bkg2k/openbor/](https://gitlab.com/Bkg2k/openbor/)
* **Official Senile Team website** : [https://www.senileteam.com/](https://www.senileteam.com/)
* **Official community** : [http://www.chronocrash.com/forum/](http://www.chronocrash.com/forum/)