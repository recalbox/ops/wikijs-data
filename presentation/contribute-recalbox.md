---
title: Contribuer au projet
description: Comment contribuer au projet Recalbox
published: true
date: 2021-09-04T16:42:37.964Z
tags: open source, aide, projet, contribuer
editor: markdown
dateCreated: 2021-05-21T07:51:25.637Z
---

Recalbox est un projet open source et gratuit, et nous serions heureux de vous compter parmi nos contributeurs.

## Profils recherchés
Si vous avez des compétences et que vous aimeriez les utiliser pour améliorer le projet, vous pouvez nous en parler sur notre discord.

Nous recherchons toujours des profils qui peuvent nous aider :
- **Graphiste** : recalbox, site web, réseaux sociaux
- **UX Designer** : ux recalbox, web, ui
- **Développeur** : bash, c, c++, python (python 2 > 3)
- **Traducteur** : traduction et correction de Recalbox, des sites, du wiki
- **Utilisateurs avertis** : rapports de bugs détaillés

## Rapport de bugs

La première forme de participation est de remonter les bugs que vous trouvez sur Recalbox de façon détaillée.

Les **[Issues](https://gitlab.com/recalbox/recalbox/-/issues)** gitlab sont des tickets de bug ou de demande d’amélioration qui permettent aux utilisateurs, aux développeurs et aux testeurs d’échanger autour du sujet en un seul endroit.

Lorsque vous remontez un bug, n'hésitez pas à être le plus précis possible. Décrire le contexte, ajouter des captures d’écrans, et pourquoi pas expliquer comment vous avez contourné le bug si tel est le cas.

Rendez vous sur https://gitlab.com/recalbox/recalbox/-/issues !

## Developpement

Les sources de Recalbox sont disponibles sur https://gitlab.com/recalbox/recalbox

Le développement sur Recalbox nécessite de lire un minimum de documentation sur le fonctionnement des fondations du système : **buildroot**.

Un fois le fonctionnement de ce dernier compris, vous pouvez commencer par vous faire votre propre image de Recalbox comme indiqué dans le [README.md](https://gitlab.com/recalbox/recalbox/-/blob/master/README.md) du project.

Ensuite, il sera nécessaire de connaitre les bases du **bash** et des commandes linux.

Suivant vos compétences, vous pouvez commencer sur différents sujets :

- Mise à jour de package dans buildroot (bash, compilation)
- Ajout de package dans buildroot (bash, compilation)
- Ajout d'un émulateur standalone (bash, compilation, c/c++, python)
- Ajout de fonctionnalités dans EmulationStation (c++)

Nous ne cherchons pas forcément des personnes super pointues techniquement, mais plutôt quelqu'un de motivé, autonome et sachant travailler en équipe sur le modèle open source :D

## Traductions

### Wiki

Si vous voulez participer à la **traduction du wiki**, visitez la page [Contribuer au wiki](../../contribute)

### Interface

Si vous voulez participer à la **traduction de l'interface EmulationStation**, vous pouvez rejoindre la page du projet sur [poeditor.com](https://poeditor.com/join/project/hEp5Khj4Ck). Des mises à jour régulières du fichier langue source sont effectuées auquel nous vous amenons à traduire dans les langues de votre choix.

### Vidéos Youtube

Si vous voulez participer à la **traduction des sous-titres pour les vidéos tutoriels** de Recalbox, veuillez vous rapprocher de l'équipe sur nos **[réseaux sociaux, Discord ou forum](./../../presentation/useful-links).**