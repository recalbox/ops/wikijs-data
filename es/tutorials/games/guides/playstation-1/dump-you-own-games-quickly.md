---
title: Volcad vuestros propios juegos rápidamente
description: 
published: true
date: 2023-09-21T10:57:09.177Z
tags: ps1, dump, juegos, volcar
editor: markdown
dateCreated: 2023-09-21T10:57:09.177Z
---

¿Todavía tenéis vuestros propios juegos físicos y queréis realizar una copia (dump) para poder jugar con ellos en Recalbox? Aquí os contamos cómo hacer.

El siguiente tutorial os permitirá crear una copia de seguridad de vuestros juegos en formato `.BIN` + `.CUE`.

## Requisitos previos

Necesitaréis algunas cosas antes de empezar:

* El software [ImgBurn](https://www.imgburn.com/) - la traducción al francés se suministra por separado y su instalación se explica en la lista de idiomas disponibles en la página de descargas.
* Una ubicación para grabar la copia de seguridad.

## Procedimiento

* Poned el juego en el lector de CD.
* Abrid ImgBurn.
* Eligid `Crear una imagen desde un disco` y apuntad a la unidad de CD que contiene vuestro disco original.
* Elegid dónde poner los archivos `.BIN` + `.CUE`, en vuestro SSD, en vuestro disco duro o en vuestra tarjeta SD.

>La extensión del fichero por defecto será `.ISO`, aseguráos de cambiarla a `.BIN`.
{.is-warning}

*Si existe la posibilidad de cambiar la velocidad de la copia, intentad mantenerla en 2X (por encima de esto, la unidad puede leer ciertos datos demasiado rápido o puede no corregir un sector del disco a tiempo si necesita ser releído).

## Observaciones

* Estos discos ya tienen más de 15-20 años, por lo que es muy posible que se produzcan errores de lectura (un CD no es eterno, puede rayarse, volverse un poco más opaco en determinados lugares con el paso del tiempo, o pueden tener marcas diversas debido a su manipulación a lo largo del tiempo -huellas, polvo, etc.-).
* Con los juegos europeos, sobre todo los más populares, y a partir de 1997, Sony introdujo sucesivas protecciones (anticopia, antilectura en ISO, antichip, etc.), por lo que se necesitan equipos especializados para extraer los datos correctamente de estos discos y el proceso puede ser largo y complejo para los particulares o la gente no especializada.
* En el proyecto Redump, se ha aportado una posible solución con los ficheros SBI (SubChannel Information), que son en realidad descripciones de las pistas ocultas y las protecciones de los discos. Compartir estos ficheros SBI es legal, ya que no contienen código directo del CD, sino sólo información aparte (como los parches).
* Es posible recuperar estos ficheros SBI a partir de los ficheros de cada disco descritos en Redump, en la sección SBI.
* Aquí tenéis el catálogo de todos los juegos protegidos en SONY PlayStation http://redump.org/discs/libcrypt/2/

## Créditos

* Zet_sensei en el [Discord](https://discord.com/channels/438014472049917953/439141063425392640/848505646391361577).