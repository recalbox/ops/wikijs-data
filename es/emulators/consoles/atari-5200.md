---
title: Atari 5200
description: 
published: true
date: 2024-07-26T07:44:21.934Z
tags: atari, 5200, atari-5200, consolas
editor: markdown
dateCreated: 2023-09-03T15:42:41.380Z
---

![](/emulators/consoles/atari5200.svg){.align-center}

## Datos técnicos

* **Fabricante:** Atari Corporation
* **Año de lanzamiento:** 1982
* **Procesador:** 6502C a 1,79 MHz
* **RAM:** 16 Kbytes
* **Procesador de audio/vídeo:** TIA
* **Resolución:** 320 x 192 píxeles, 16 colores en pantalla
* **Chip de sonido:** 4 voces
* **Tamaño del cartucho:** 2kB - 32kB

## Presentación

El Atari 5200 es esencialmente un Atari 400 sin teclado. Esto demuestra que los diseños de Atari pueden adaptarse rápidamente al mercado.

La consola incorpora un nuevo y revolucionario joystick con un stick analógico y teclas de función (start, pause, reset). Otras innovaciones son el disparo automático y los cuatro puertos de joystick.

El diseño no centrado del joystick resultó ser poco beneficioso y poco fiable, lo que molestó a muchos compradores. Aunque la 5200 cosechó un gran número de seguidores gracias a una biblioteca de juegos de gran calidad, se enfrentó a la nueva competencia de la [**Colecovision**](./colecovision) y a una economía cada vez más inestable.

La cuestión de qué sistema sería superior al otro se volvió irrelevante cuando el mercado de los juegos se hundió en 1983-1984, acabando con ambas consolas en su perfección.

## Emuladores

[Libretro A5200](libretro-a5200)
[Libretro Atari800](libretro-atari800)