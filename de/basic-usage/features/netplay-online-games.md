---
title: Netplay (Online-Spiel)
description: 
published: true
date: 2022-08-19T14:46:49.559Z
tags: netplay, online, spiele
editor: markdown
dateCreated: 2022-08-17T00:25:23.881Z
---

![](/basic-usage/features/netplay/logo-recalbox_netplay.png)

## Einführung

Netplay ist eine Funktion, die den vernetzten Mehrspielermodus mit einigen Emulatoren über Peer-to-Peer-Netzwerke (P2P) ermöglicht, um die Netzwerklatenz zu reduzieren und ein optimales Spielerlebnis zu gewährleisten, indem mehrere Instanzen, auf denen derselbe Emulationskern und Inhalt läuft, kontinuierlich synchronisiert werden.
Sie haben auch die Möglichkeit, Personen im Zuschauermodus teilnehmen zu lassen (zusätzliche Latenzzeit).
Online-Gaming auf unseren alten Konsolen, die diese Fähigkeit noch gar nicht kannten!

## Empfehlung

### Verbindung

* Schließen Sie Ihren Rechner über ein RJ45-Ethernet-Kabel an (eine WiFi-Verbindung ist weitaus weniger stabil).
* Haben Sie eine Box, die UPNP beherrscht, oder öffnen Sie ihren Port manuell.
* Wenn sich die Recalbox hinter dem Router befindet, ist alles in Ordnung (einfaches NAT).
* Wenn sich die Recalbox hinter zwei Routern befindet, wird Netplay nicht durchgelassen (doppelt NAT).

### Roms

* **Für Cartridge-Konsolen:**
  * Verwenden Sie Roms aus **No-Intro-Romsets** (Dumps, die näher an den Originalspielen sind).
* **Für CD-Konsolen:**
  * Verwenden Sie Roms aus **Redump-Romsets** (Dumps, die näher an den Originalspielen sind).
  
### System

- Wenn Sie einen Raspberry Pi0, Pi1, Pi2 , Pi3-b und Pi3-b+ verwenden, wird eine **Übertaktung** (auf 1300 MHZ) **empfohlen oder ist sogar erforderlich**.
- Bei Raspberry Pi4, x86_64 (64bit), x86 (32bit) PCs und Odroid ist eine Übertaktung nicht erforderlich.