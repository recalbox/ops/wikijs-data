---
title: Softpatching
description: Play with patched games on-the-fly!
published: true
date: 2024-11-09T22:46:00.594Z
tags: 8.1+, softpatching
editor: markdown
dateCreated: 2022-05-19T22:58:45.585Z
---

## What is softpatching ?

« Sofpatching » allows you to apply a patch on a game without creating a new rom. You can play a game with or without the patch depending on chosen options.

Patches range from a little cheat to a complete translation of a game (typically Japanese) into another language.

> You can use multiple patches at the same time per game.
{.is-info}

## How it works?

Its usage is very simple and is done in 2 steps:

### Installation of the patch

First, you need to put your patch in the same directory as the game to patch. The patch must have the same name as the rom except for its file extension.

> A patch can have the file extension `.UPS`, `.IPS` or `.BPS`.
{.is-info}

### Patch activation

Your patch is in the correct location with the correct filename? Now you need to define what to do with patches. Do you want them to apply everytime or do you want it to ask each time you start a game?

For this, in EmulationStation, in the systems list, press `Start` and go to `GAME OPTIONS`. You will have an option named `SOFTPATCHING` with 3 possibles choices:

- `AUTO` : will apply the patch everytime automatically
- `SELECT` : will ask you if you would like to apply the patch
- `DISABLED` : will ignore all patches completely

Once your choice is made, you only need to run the game to play with the patch.

### Patches actiovation

Since Recalbox 9, vous can have multiple patchs for a game !

In the same folder than the game which have multiple patchs, add a folder with the game name followed by « -patches ». Example:

┣ 📁 nes
┃ ┣ 📁 Legend of Zelda, The (USA) (Rev 1)-patches
┃ ┃ ┣ 🗒 ZeldaC.ips
┃ ┃ ┣ 🗒 Zelda - The Legend of Link (v3-12-20).ips
┃ ┃ ┣ 🗒 An other patch.ups
┃ ┣ 🗒 Legend of Zelda, The (USA) (Rev 1).zip

Once this is done and with the condition to choose the `SELECT` choice, when you start the game, you will have a message asking you which patch(s) to apply to your game!

## Using several patches at the same time

You can use several patches at the same time. To do this, you need to name the corresponding patches in a certain way:

- Patches that are to be applied at the same time must have the same name
- The extensions of each patch have a number at the end, indicating the order of application.

For example, you have the `rom.bin` set to which you have several patches to apply. You can name them like this:

- rom.ips
- rom.ips1
- rom.ips2

Here, `rom.ips` will be the first patch to be applied, `rom.ips1` will be the second patch to be applied, and so on.

Also, to make this as automatic as possible, in the interface menu, do `START`, go to `GAME OPTIONS` and set the `SOFTPATCHING` option to `AUTO`.

## Apply a single patch automatically at each startup

You also have the option of always wanting to launch a game with the same patch. To do this, there are two things to do:

- In the interface, do `START`, go to `GAME OPTIONS` and for the `SOFTPATCHING` option, choose `LAUNCH LAST`.
- Still in the interface, on the game you want to patch all the time, do `START` then `MODIFY GAME` and on the `SOFTPATCHING` option, choose the patch to apply.

With this, you won't have a window to choose whether you want to apply a patch or not, and the game will always launch patched.

## List of compatible emulators

There is a list of compatibles emulators with patches:

- ColecoVision
  - Libretro GearColeco
- Famicom Disk System
  - Libretro FCEUmm
- Game Boy / Game Boy Color
  - Libretro bsnes
  - Libretro Gambatte
  - Libretro Mesen_S
  - Libretro mGBA
  - Libretro TGB Dual
- Game Boy Advance
  - Libretro mGBA
  - Libretro Meteor
- Master System
  - Libretro Gearsystem
- Megadrive/Genesis
  - Libretro GenesisPlus GX
  - Libretro GenesisPlus GX Wide
  - Libretro Picodrive
- Neo Geo Pocket / Neo Geo Pocket Color
  - Libretro Mednafen_NGP
- NES
  - Libretro FCEUmm
  - Libretro FCEUNext
  - Libretro Mesen
  - Libretro Nestopia
  - Libretro QuickNES
- Nintendo 64
  - Libretro Mupen64Plus NX
- Satellaview
  - Libretro bsnes
  - Libretro Snes9x
- SNES
  - Libretro bsnes
  - Libretro bsnes HD
  - Libretro Mesen_S
  - Libretro Snes9x
  - Libretro Snes9x 2002
  - Libretro Snes9x 2005
  - Libretro Snes9x 2010
- WonderSwan/Color
  - Libretro Mednafen_WSWAN

## Patches links

GohanSenior has put together a selection of almost a thousand French translation patches for 8-bit and 16-bit games!

To take advantage of these patches, you'll need to:

- use the No-Intro romsets
- download the patch file at https://github.com/GohanSenior/Pack-Trans-EN-Sets-No-Intro-Softpatching-Recalbox/archive/refs/heads/main.zip (link to github: https://github.com/GohanSenior/Pack-Trans-EN-Sets-No-Intro-Softpatching-Recalbox)
- uncompress in your roms folder
- activate softpatching in the Recalbox options
- Select the patch when Recalbox asks you how to launch your game

And there you have it, a thousand games translated into French!

Don't hesitate to contact him on our discord to thank him or to report any problems you may encounter!

You can find other working patches from the following web sites:

- https://traf.romhack.org/
- https://terminus.romhack.net/