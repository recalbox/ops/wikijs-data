---
title: Connectar un mando IPEGA
description: 
published: true
date: 2023-09-19T09:22:42.300Z
tags: ipega, mando
editor: markdown
dateCreated: 2023-09-19T09:22:42.300Z
---

## Trucos para los controladores IPEGA

* Conéctaros vía [SSH](./../../../tutoriales/sistema/acceso/root-access-terminal-cli) a Recalbox y montad la [partición del sistema](./../../../tutoriales/sistema/acceso/remount-partition-with-write-access) en modo lectura/escritura.
* Buscad la referencia de vuestro controlador usando el comando :

```shell
evtest
```

Ejemplo : `PG-9055`

* Modificad `ATTRS{name}=="PG-9055"` en función de la referencia de vuestro mando y copiad/pegad el conjunto de estos comandos en la sesisón de SSH, y a continuación validad con la tecla `Enter`.

```text
echo 'SUBSYSTEM=="input", ATTRS{name}=="PG-9055", MODE="0666", ENV{ID_INPUT_JOYSTICK}="1"'>>/etc/udev/rules.d/99-joysticks-exotics.rules
reboot
```

## Conexión en Bluetooth

* Reiniciad el controlador IPEGA si lo estáis utilizando con otro dispositivo.
* Ponedlo en modo `Home+X`.
* Ejecutad la búsqueda de controladores bluetooth desde el menú Emulationstation pulsando `START` > `HANDSET SETTINGS`.

Podéis comprobar si vuestro IPEGA aparece en el fichero `/etc/udev/rules.d/99-joysticks-exotics.rules`.

Si no es así, podéis [abrir una incidencia](https://gitlab.com/recalbox/recalbox/-/issues) para que sea añadido en la próxima actualización de Recalbox.