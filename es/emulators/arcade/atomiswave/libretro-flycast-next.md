---
title: Libretro Flycast-Next
description: 
published: true
date: 2024-07-29T11:09:18.257Z
tags: libretro, atomiswave, 9.2+, flycast-next
editor: markdown
dateCreated: 2024-07-29T11:09:18.257Z
---

**Libretro Flycast** es un emulador **Sega Dreamcast multiplataforma** capaz de emular el **Sammy Atomiswave.**

## ![](/emulators/license.svg) Licencia

Este núcleo está bajo la licencia [**GPLv2**](https://github.com/libretro/flycast/blob/master/LICENSE).

## ![](/emulators/compatibility.png) Compatibilidad

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ❌ | ❌ | ✅ | ✅ | ✅ | ✅  🐌 |✅ | ✅ |

🐌  Bajo rendimiento pero jugable

## ![](/emulators/features.png) Funcionalidades

| Funcionalidad | Soportado |
| :---: | :---: |
| Reinicio | ✔ |
| Capturas de pantalla | ✔ |
| Guardados | ✔ |
| Opciones del núcleo | ✔ |
| Trucos RetroArch | ✔ |
| Controles | ✔ |
| Remapeo | ✔ |
| Vibración | ✔ |
| Control de disco | ✔ |

## ![](/emulators/bios.svg) BIOS

### Lista de bios obligatorios

| Nombre de archivo | Descripción | MD5 | Proporcionado |
| :---: | :---: | :---: | :---: |
| awbios.zip | Bios «Atomiswave» de Mame | 0ec5ae5b5a5c4959fa8b43fcf8687f7c | ❌ |

### Ubicación

Coloca los bios de la siguiente manera:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 dc
┃ ┃ ┃ ┃ ┣ 🗒 **awbios.zip**

## ![](/emulators/roms.png) Roms

Libretro Flycast se basa en el Romset de **MAME** pero también en formatos **NullDC** para su parte Atomiswave.

### Extensiones soportadas

Las roms deben tener las siguientes extensiones:

* .zip
* .7z
* .bin/.lst
* .dat

## Romset Mame

Solo las roms Atomiswave **provenientes de un RomSet MAME 0.135 o superior** son compatibles.
Nosotros recomendamos el **RomSet Mame 0.235** que traerá su lote de compatibilidades adicionales.
Para más información sobre la versión del Romset en curso, consulta la página [MameDev](https://www.mamedev.org/release.html).

>Para ordenar tus roms arcade, el **archivo dat** está disponible en el directorio: `/recalbox/share/bios/dc/`
{.is-info}

### Ubicación

Coloca las roms de la siguiente manera:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 atomiswave
┃ ┃ ┃ ┃ ┣ 🗒 **juego.zip**

## Romset NullDC

Estas roms son compatibles con Flycast pero **menos fiables** que las roms **provenientes de un set MAME.**

>Las Roms **NullDC** están en formato: `_.bin + .lst_`
{.is-info}

### Ubicación

Coloca las roms de la siguiente manera:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 atomiswave
┃ ┃ ┃ ┃ ┣ 📁 **juego**
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **juego.bin**
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **juego.lst**

## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

>Para poder conservar tus configuraciones personalizadas durante una actualización, te recomendamos utilizar nuestra funcionalidad [Sobrecargas de configuración](./../../../advanced-usage/configuration-override).
{.is-info}

### Acceder a las opciones

Puedes configurar diversas opciones de dos maneras diferentes.

* Vía el Menú RetroArch:

┣ 📁 Menu RetroArch
┃ ┣ 📁 Opciones del núcleo
┃ ┃ ┣ 🧩 Name_option

* Vía el archivo `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Opciones del núcleo

| Opciones | Valor de las opciones | Variable (para sobrecarga) | Valores de las variables |
| --- | --- | --- | --- |
| Región | `Japón` / `Estados Unidos` ✅ / `Europa` / `Por defecto` | `reicast_region` | `Japan` / `USA` / `Europe` / `Default` |
| Activar el DSP | `Desactivado` / `Activado` ✅ | `reicast_enable_dsp` | `disabled` / `enabled` |
| Permitir los botones de servicio NAOMI | `Desactivado` ✅ / `Activado` | `reicast_allow_service_buttons` | `disabled` / `enabled` |
| Definir los juegos NAOMI en Free Play | `Desactivado` / `Activado` ✅ | `reicast_force_freeplay` | `disabled` / `enabled` |
| Emulación del adaptador de alta velocidad | `Desactivado` ✅ / `Activado` | `reicast_emulate_bba` | `disabled` / `enabled` |
| Activar el UPnP | `Desactivado` / `Activado` ✅ | `reicast_upnp` | `disabled` / `enabled` |
| Resolución interna | `320x240` / `640x480` ✅ / `800x600` / `960x720` / `1024x768` / `1280x960` / `1440x1080` / `1600x1200` / `1920x1440` / `2560x1920` / `2880x2160` / `3200x2400` / `3840x2880` / `4480x3360` / `5120x3840` / `5760x4320` / `6400x4800` / `7040x5280` / `7680x5760` / `8320x6240` / `8960x6720` / `9600x7200` / `10240x7680` / `10880x8160` / `11520x8640` / `12160x9120` / `12800x9600` | `reicast_internal_resolution` | `320x240` / `640x480` / `800x600` / `960x720` / `1024x768` / `1280x960` / `1440x1080` / `1600x1200` / `1920x1440` / `2560x1920` / `2880x2160` / `3200x2400` / `3840x2880` / `4480x3360` / `5120x3840` / `5760x4320` / `6400x4800` / `7040x5280` / `7680x5760` / `8320x6240` / `8960x6720` / `9600x7200` / `10240x7680` / `10880x8160` / `11520x8640` / `12160x9120` / `12800x9600` |
| Orientación de la pantalla | `Horizontal` ✅ / `Vertical` | `reicast_screen_rotation` | `horizontal` / `vertical` |
| Ordenación alfa | `Por tira (rápido, menos preciso)` / `Por triángulo (normal)` ✅ / `Por píxel (preciso, pero el más lento)` | `reicast_alpha_sorting` | `per-strip (fast, least accurate)` / `per-triangle (normal)` / `per-pixel (accurate)` |
| Emulación completa del framebuffer | `Desactivado` ✅ / `Activado` | `reicast_emulate_framebuffer` | `disabled` / `enabled` |
| Activar el buffer de RTT (renderizado a textura) | `Desactivado` ✅ / `Activado` | `reicast_enable_rttb` | `disabled` / `enabled` |
| Mipmapping | `Desactivado` / `Activado` ✅ | `reicast_mipmapping` | `disabled` / `enabled` |
| Efectos de niebla | `Desactivado` / `Activado` ✅ | `reicast_fog` | `disabled` / `enabled` |
| Modificador del volumen | `Desactivado` / `Activado` ✅ | `reicast_volume_modifier_enable` | `disabled` / `enabled` |
| Filtrado anisotrópico | `Desactivado` / `2` / `4` ✅ / `8` / `16` | `reicast_anisotropic_filtering` | `off` / `2` / `4` / `8` / `16` |
| Filtrado de texturas | `Por defecto` ✅ / `Forzar al más cercano` / `Forzar lineal` | `reicast_texture_filtering` | `0` / `1` / `2` |
| Retrasar el intercambio de imágenes | `Desactivado` ✅ / `Activado` | `reicast_delay_frame_swapping` | `disabled` / `enabled` |
| Detectar cambios en la frecuencia de imágenes | `Desactivado` ✅ / `Activado` | `reicast_detect_vsync_swap_interval` | `disabled` / `enabled` |
| Filtro de posprocesamiento PowerVR2 | `Desactivado` ✅ / `Activado` | `reicast_pvr2_filtering` | `disabled` / `enabled` |
| Ampliación de texturas (xBRZ) | `Desactivado` ✅ / `x2` / `x4` / `x6` | `reicast_texupscale` | `disabled` / `2` / `4` / `6` |
| Factor máximo del filtro de ampliación de texturas | `256` / `512` / `1024` | `reicast_` | `256` / `512` / `1024` |
| Interpolación de profundidad nativa | `Desactivado` ✅ / `Activado` | `reicast_native_depth_interpolation` | `disabled` / `enabled` |
| Renderizado en múltiples hilos | `Desactivado` / `Activado` ✅ | `reicast_thraded_rendering` | `disabled` / `enabled` |
| Salto de imágenes automático | `Desactivado` ✅ / `Normal` / `Máximo` | `reicast_auto_skip_frame` | `disabled` / `some` / `more` |
| Salto de imágenes | `Desactivado` ✅ / `1` / `2` / `3` / `4` / `5` / `6` | `reicast_frame_skipping` | `disabled` / `1` / `2` / `3` / `4` / `5` / `6` |
| Trucos de pantalla ancha (Reinicio requerido) | `Desactivado` ✅ / `Activado` | `reicast_widescreen_cheats` | `disabled` / `enabled` |
| Hack de pantalla ancha | `Desactivado` ✅ / `Activado` | `reicast_widescreen_hack` | `disabled` / `enabled` |
| SH4 CPU under/overclock | `100 MHz` / `110 MHz` / `120 MHz` / `130 MHz` / `140 MHz` / `150 MHz` / `160 MHz` / `170 MHz` / `180 MHz` / `190 MHz` / `200 MHz` / `210 MHz` / `220 MHz` / `230 MHz` / `240 MHz` / `250 MHz` / `260 MHz` / `270 MHz` / `280 MHz` / `290 MHz` / `300 MHz` | `reicast_sh4clock` | `100` / `110` / `120` / `130` / `140` / `150` / `160` / `170` / `180` / `190` / `200` / `210` / `220` / `230` / `240` / `250` / `260` / `270` / `280` / `290` / `300`  |
| Cargar texturas personalizadas | `Desactivado` ✅ / `Activado` | `reicast_custom_textures` | `disabled` / `enabled` |
| Importar texturas | `Desactivado` ✅ / `Activado` | `reicast_dump_textures` | `disabled` / `enabled` |
| Zona muerta del stick analógico | `0%` / `5%` / `10%` / `15%` ✅ / `20%` / `25%` / `30%` | `reicast_analog_stick_deadzone` | `0%` / `5%` / `10%` / `15%` / `20%` / `25%` / `30%` |
| Zona muerta de los gatillos | `0%` ✅ / `5%` / `10%` / `15%` / `20%` / `25%` / `30%` | `reicast_trigger_deadzone` | `0%` / `5%` / `10%` / `15%` / `20%` / `25%` / `30%` |
| Gatillos digitales | `Desactivado` ✅ / `Activado` | `reicast_digital_triggers` | `disabled` / `enabled` |
| Difundir salidas digitales | `Desactivado` ✅ / `Activado` | `reicast_network_output` | `disabled` / `enabled` |
| Mostrar la mira del arma 1 | `Desactivado` ✅ / `Blanco` / `Rojo` / `Verde` / `Azul` | `reicast_lightgun1_crosshair` | `disabled` / `White` / `Red` / `Green` / `Blue` |
| Mostrar la mira del arma 2 | `Desactivado` ✅ / `Blanco` / `Rojo` / `Verde` / `Azul` | `reicast_lightgun2_crosshair` | `disabled` / `White` / `Red` / `Green` / `Blue` |
| Mostrar la mira del arma 3 | `Desactivado` ✅ / `Blanco` / `Rojo` / `Verde` / `Azul` | `reicast_lightgun3_crosshair` | `disabled` / `White` / `Red` / `Green` / `Blue` |
| Mostrar la mira del arma 4 | `Desactivado` ✅ / `Blanco` / `Rojo` / `Verde` / `Azul` | `reicast_lightgun4_crosshair` | `disabled` / `White` / `Red` / `Green` / `Blue` |
| Sonidos VMU | `Desactivado` ✅ / `Activado` | `reicast_vmu_sound` | `disabled` / `enabled` |
| Mostrar la pantalla del VMU 1 | `Desactivado` ✅ / `Activado` | `reicast_vmu1_screen_display` | `disabled` / `enabled` |
| Posición de la pantalla del VMU 1 | `Arriba a la izquierda` ✅ / `Arriba a la derecha` / `Abajo a la izquierda` / `Abajo a la derecha` | `reicast_vmu1_screen_position` | `Upper Left` / `Upper Right` / `Lower Left` / `Lower Right` |
| Tamaño de la pantalla del VMU 1 | `1x` ✅ / `2x` / `3x` / `4x` / `5x` | `reicast_vmu1_screen_size_mult` | `1x` / `2x` / `3x` / `4x` / `5x` |
| Color de los píxeles encendidos de la pantalla del VMU 1 | `Por defecto activado` ✅ / `Por defecto desactivado` / `Negro` / `Azul` / `Azul claro` / `Verde` / `Cian` / `Cian azul` / `Verde claro` / `Verde cian` / `Cian claro` / `Rojo` / `Púrpura` / `Púrpura claro` / `Amarillo` / `Gris` / `Púrpura claro (2)` / `Verde claro (2)` / `Verde claro (3)` / `Cian claro (2)` / `Rojo claro (2)` / `Magenta` / `Púrpura claro (3)` / `Naranja claro` / `Naranja` / `Púrpura claro (4)` / `Amarillo claro` / `Amarillo claro (2)` / `Blanco` | `reicast_vmu1_pixel_on_color` | `DEFAULT_ON 00` / `DEFAULT_OFF 01` / `BLACK 02` / `BLUE 03` / `LIGHT_BLUE 04` / `GREEN 05` / `CYAN 06` / `CYAN_BLUE 07` / `LIGHT_GREEN 08` / `CYAN_GREEN 09` / `LIGHT_CYAN 10` / `RED 11` / `PURPLE 12` / `LIGHT_PURPLE 13` / `YELLOW 14` / `GREY 15` / `LIGHT_PURPLE_2 16` / `LIGHT_GREEN_2 17` / `LIGHT_GREEN_3 18` / `LIGHT_CYAN_2 19` / `LIGHT_RED_2 20` / `MAGENTA 21` / `LIGHT_PURPLE_3 22` / `LIGHT_ORANGE 23` / `ORANGE 24` / `LIGHT_PURPLE_4 25` / `LIGHT_YELLOW 26` / `LIGHT_YELLOW_2 27` / `WHITE 28` |
| Color de los píxeles apagados de la pantalla del VMU 1 | `Por defecto desactivado` ✅ / `Por defecto activado` / `Negro` / `Azul` / `Azul claro` / `Verde` / `Cian` / `Cian azul` / `Verde claro` / `Verde cian` / `Cian claro` / `Rojo` / `Púrpura` / `Púrpura claro` / `Amarillo` / `Gris` / `Púrpura claro (2)` / `Verde claro (2)` / `Verde claro (3)` / `Cian claro (2)` / `Rojo claro (2)` / `Magenta` / `Púrpura claro (3)` / `Naranja claro` / `Naranja` / `Púrpura claro (4)` / `Amarillo claro` / `Amarillo claro (2)` / `Blanco` | `reicast_vmu1_pixel_off_color` | `DEFAULT_OFF 00` / `DEFAULT_ON 01` / `BLACK 02` / `BLUE 03` / `LIGHT_BLUE 04` / `GREEN 05` / `CYAN 06` / `CYAN_BLUE 07` / `LIGHT_GREEN 08` / `CYAN_GREEN 09` / `LIGHT_CYAN 10` / `RED 11` / `PURPLE 12` / `LIGHT_PURPLE 13` / `YELLOW 14` / `GREY 15` / `LIGHT_PURPLE_2 16` / `LIGHT_GREEN_2 17` / `LIGHT_GREEN_3 18` / `LIGHT_CYAN_2 19` / `LIGHT_RED_2 20` / `MAGENTA 21` / `LIGHT_PURPLE_3 22` / `LIGHT_ORANGE 23` / `ORANGE 24` / `LIGHT_PURPLE_4 25` / `LIGHT_YELLOW 26` / `LIGHT_YELLOW_2 27` / `WHITE 28` |
| Opacidad de la pantalla del VMU 1 | `10%` / `20%` / `30%` / `40%` / `50%` / `60%` / `70%` / `80%` / `90%` / `100%` | `reicast_vmu1_screen_opacity` | `10%` / `20%` / `30%` / `40%` / `50%` / `60%` / `70%` / `80%` / `90%` / `100%` |
| Mostrar la pantalla del VMU 2 | `Desactivado` ✅ / `Activado` | `reicast_vmu2_screen_display` | `disabled` / `enabled` |
| Posición de la pantalla del VMU 2 | `Arriba a la izquierda` ✅ / `Arriba a la derecha` / `Abajo a la izquierda` / `Abajo a la derecha` | `reicast_vmu2_screen_position` | `Upper Left` / `Upper Right` / `Lower Left` / `Lower Right` |
| Tamaño de la pantalla del VMU 2 | `1x` ✅ / `2x` / `3x` / `4x` / `5x` | `reicast_vmu2_screen_size_mult` | `1x` / `2x` / `3x` / `4x` / `5x` |
| Color de los píxeles encendidos de la pantalla del VMU 2 | `Por defecto activado` ✅ / `Por defecto desactivado` / `Negro` / `Azul` / `Azul claro` / `Verde` / `Cian` / `Cian azul` / `Verde claro` / `Verde cian` / `Cian claro` / `Rojo` / `Púrpura` / `Púrpura claro` / `Amarillo` / `Gris` / `Púrpura claro (2)` / `Verde claro (2)` / `Verde claro (3)` / `Cian claro (2)` / `Rojo claro (2)` / `Magenta` / `Púrpura claro (3)` / `Naranja claro` / `Naranja` / `Púrpura claro (4)` / `Amarillo claro` / `Amarillo claro (2)` / `Blanco` | `reicast_vmu2_pixel_on_color` | `DEFAULT_ON 00` / `DEFAULT_OFF 01` / `BLACK 02` / `BLUE 03` / `LIGHT_BLUE 04` / `GREEN 05` / `CYAN 06` / `CYAN_BLUE 07` / `LIGHT_GREEN 08` / `CYAN_GREEN 09` / `LIGHT_CYAN 10` / `RED 11` / `PURPLE 12` / `LIGHT_PURPLE 13` / `YELLOW 14` / `GREY 15` / `LIGHT_PURPLE_2 16` / `LIGHT_GREEN_2 17` / `LIGHT_GREEN_3 18` / `LIGHT_CYAN_2 19` / `LIGHT_RED_2 20` / `MAGENTA 21` / `LIGHT_PURPLE_3 22` / `LIGHT_ORANGE 23` / `ORANGE 24` / `LIGHT_PURPLE_4 25` / `LIGHT_YELLOW 26` / `LIGHT_YELLOW_2 27` / `WHITE 28` |
| Color de los píxeles apagados de la pantalla del VMU 2 | `Por defecto desactivado` ✅ / `Por defecto activado` / `Negro` / `Azul` / `Azul claro` / `Verde` / `Cian` / `Cian azul` / `Verde claro` / `Verde cian` / `Cian claro` / `Rojo` / `Púrpura` / `Púrpura claro` / `Amarillo` / `Gris` / `Púrpura claro (2)` / `Verde claro (2)` / `Verde claro (3)` / `Cian claro (2)` / `Rojo claro (2)` / `Magenta` / `Púrpura claro (3)` / `Naranja claro` / `Naranja` / `Púrpura claro (4)` / `Amarillo claro` / `Amarillo claro (2)` / `Blanco` | `reicast_vmu2_pixel_off_color` | `DEFAULT_OFF 00` / `DEFAULT_ON 01` / `BLACK 02` / `BLUE 03` / `LIGHT_BLUE 04` / `GREEN 05` / `CYAN 06` / `CYAN_BLUE 07` / `LIGHT_GREEN 08` / `CYAN_GREEN 09` / `LIGHT_CYAN 10` / `RED 11` / `PURPLE 12` / `LIGHT_PURPLE 13` / `YELLOW 14` / `GREY 15` / `LIGHT_PURPLE_2 16` / `LIGHT_GREEN_2 17` / `LIGHT_GREEN_3 18` / `LIGHT_CYAN_2 19` / `LIGHT_RED_2 20` / `MAGENTA 21` / `LIGHT_PURPLE_3 22` / `LIGHT_ORANGE 23` / `ORANGE 24` / `LIGHT_PURPLE_4 25` / `LIGHT_YELLOW 26` / `LIGHT_YELLOW_2 27` / `WHITE 28` |
| Opacidad de la pantalla del VMU 2 | `10%` / `20%` / `30%` / `40%` / `50%` / `60%` / `70%` / `80%` / `90%` / `100%` | `reicast_vmu2_screen_opacity` | `10%` / `20%` / `30%` / `40%` / `50%` / `60%` / `70%` / `80%` / `90%` / `100%` |
| Mostrar la pantalla del VMU 3 | `Desactivado` ✅ / `Activado` | `reicast_vmu3_screen_display` | `disabled` / `enabled` |
| Posición de la pantalla del VMU 3 | `Arriba a la izquierda` ✅ / `Arriba a la derecha` / `Abajo a la izquierda` / `Abajo a la derecha` | `reicast_vmu3_screen_position` | `Upper Left` / `Upper Right` / `Lower Left` / `Lower Right` |
| Tamaño de la pantalla del VMU 3 | `1x` ✅ / `2x` / `3x` / `4x` / `5x` | `reicast_vmu3_screen_size_mult` | `1x` / `2x` / `3x` / `4x` / `5x` |
| Color de los píxeles encendidos de la pantalla del VMU 3 | `Por defecto activado` ✅ / `Por defecto desactivado` / `Negro` / `Azul` / `Azul claro` / `Verde` / `Cian` / `Cian azul` / `Verde claro` / `Verde cian` / `Cian claro` / `Rojo` / `Púrpura` / `Púrpura claro` / `Amarillo` / `Gris` / `Púrpura claro (2)` / `Verde claro (2)` / `Verde claro (3)` / `Cian claro (2)` / `Rojo claro (2)` / `Magenta` / `Púrpura claro (3)` / `Naranja claro` / `Naranja` / `Púrpura claro (4)` / `Amarillo claro` / `Amarillo claro (2)` / `Blanco` | `reicast_vmu3_pixel_on_color` | `DEFAULT_ON 00` / `DEFAULT_OFF 01` / `BLACK 02` / `BLUE 03` / `LIGHT_BLUE 04` / `GREEN 05` / `CYAN 06` / `CYAN_BLUE 07` / `LIGHT_GREEN 08` / `CYAN_GREEN 09` / `LIGHT_CYAN 10` / `RED 11` / `PURPLE 12` / `LIGHT_PURPLE 13` / `YELLOW 14` / `GREY 15` / `LIGHT_PURPLE_2 16` / `LIGHT_GREEN_2 17` / `LIGHT_GREEN_3 18` / `LIGHT_CYAN_2 19` / `LIGHT_RED_2 20` / `MAGENTA 21` / `LIGHT_PURPLE_3 22` / `LIGHT_ORANGE 23` / `ORANGE 24` / `LIGHT_PURPLE_4 25` / `LIGHT_YELLOW 26` / `LIGHT_YELLOW_2 27` / `WHITE 28` |
| Color de los píxeles apagados de la pantalla del VMU 3 | `Por defecto desactivado` ✅ / `Por defecto activado` / `Negro` / `Azul` / `Azul claro` / `Verde` / `Cian` / `Cian azul` / `Verde claro` / `Verde cian` / `Cian claro` / `Rojo` / `Púrpura` / `Púrpura claro` / `Amarillo` / `Gris` / `Púrpura claro (2)` / `Verde claro (2)` / `Verde claro (3)` / `Cian claro (2)` / `Rojo claro (2)` / `Magenta` / `Púrpura claro (3)` / `Naranja claro` / `Naranja` / `Púrpura claro (4)` / `Amarillo claro` / `Amarillo claro (2)` / `Blanco` | `reicast_vmu3_pixel_off_color` | `DEFAULT_OFF 00` / `DEFAULT_ON 01` / `BLACK 02` / `BLUE 03` / `LIGHT_BLUE 04` / `GREEN 05` / `CYAN 06` / `CYAN_BLUE 07` / `LIGHT_GREEN 08` / `CYAN_GREEN 09` / `LIGHT_CYAN 10` / `RED 11` / `PURPLE 12` / `LIGHT_PURPLE 13` / `YELLOW 14` / `GREY 15` / `LIGHT_PURPLE_2 16` / `LIGHT_GREEN_2 17` / `LIGHT_GREEN_3 18` / `LIGHT_CYAN_2 19` / `LIGHT_RED_2 20` / `MAGENTA 21` / `LIGHT_PURPLE_3 22` / `LIGHT_ORANGE 23` / `ORANGE 24` / `LIGHT_PURPLE_4 25` / `LIGHT_YELLOW 26` / `LIGHT_YELLOW_2 27` / `WHITE 28` |
| Opacidad de la pantalla del VMU 3 | `10%` / `20%` / `30%` / `40%` / `50%` / `60%` / `70%` / `80%` / `90%` / `100%` | `reicast_vmu3_screen_opacity` | `10%` / `20%` / `30%` / `40%` / `50%` / `60%` / `70%` / `80%` / `90%` / `100%` |
| Mostrar la pantalla del VMU 4 | `Desactivado` ✅ / `Activado` | `reicast_vmu4_screen_display` | `disabled` / `enabled` |
| Posición de la pantalla del VMU 4 | `Arriba a la izquierda` ✅ / `Arriba a la derecha` / `Abajo a la izquierda` / `Abajo a la derecha` | `reicast_vmu4_screen_position` | `Upper Left` / `Upper Right` / `Lower Left` / `Lower Right` |
| Tamaño de la pantalla del VMU 4 | `1x` ✅ / `2x` / `3x` / `4x` / `5x` | `reicast_vmu4_screen_size_mult` | `1x` / `2x` / `3x` / `4x` / `5x` |
| Color de los píxeles encendidos de la pantalla del VMU 4 | `Por defecto activado` ✅ / `Por defecto desactivado` / `Negro` / `Azul` / `Azul claro` / `Verde` / `Cian` / `Cian azul` / `Verde claro` / `Verde cian` / `Cian claro` / `Rojo` / `Púrpura` / `Púrpura claro` / `Amarillo` / `Gris` / `Púrpura claro (2)` / `Verde claro (2)` / `Verde claro (3)` / `Cian claro (2)` / `Rojo claro (2)` / `Magenta` / `Púrpura claro (3)` / `Naranja claro` / `Naranja` / `Púrpura claro (4)` / `Amarillo claro` / `Amarillo claro (2)` / `Blanco` | `reicast_vmu4_pixel_on_color` | `DEFAULT_ON 00` / `DEFAULT_OFF 01` / `BLACK 02` / `BLUE 03` / `LIGHT_BLUE 04` / `GREEN 05` / `CYAN 06` / `CYAN_BLUE 07` / `LIGHT_GREEN 08` / `CYAN_GREEN 09` / `LIGHT_CYAN 10` / `RED 11` / `PURPLE 12` / `LIGHT_PURPLE 13` / `YELLOW 14` / `GREY 15` / `LIGHT_PURPLE_2 16` / `LIGHT_GREEN_2 17` / `LIGHT_GREEN_3 18` / `LIGHT_CYAN_2 19` / `LIGHT_RED_2 20` / `MAGENTA 21` / `LIGHT_PURPLE_3 22` / `LIGHT_ORANGE 23` / `ORANGE 24` / `LIGHT_PURPLE_4 25` / `LIGHT_YELLOW 26` / `LIGHT_YELLOW_2 27` / `WHITE 28` |
| Color de los píxeles apagados de la pantalla del VMU 4 | `Por defecto desactivado` ✅ / `Por defecto activado` / `Negro` / `Azul` / `Azul claro` / `Verde` / `Cian` / `Cian azul` / `Verde claro` / `Verde cian` / `Cian claro` / `Rojo` / `Púrpura` / `Púrpura claro` / `Amarillo` / `Gris` / `Púrpura claro (2)` / `Verde claro (2)` / `Verde claro (3)` / `Cian claro (2)` / `Rojo claro (2)` / `Magenta` / `Púrpura claro (3)` / `Naranja claro` / `Naranja` / `Púrpura claro (4)` / `Amarillo claro` / `Amarillo claro (2)` / `Blanco` | `reicast_vmu4_pixel_off_color` | `DEFAULT_OFF 00` / `DEFAULT_ON 01` / `BLACK 02` / `BLUE 03` / `LIGHT_BLUE 04` / `GREEN 05` / `CYAN 06` / `CYAN_BLUE 07` / `LIGHT_GREEN 08` / `CYAN_GREEN 09` / `LIGHT_CYAN 10` / `RED 11` / `PURPLE 12` / `LIGHT_PURPLE 13` / `YELLOW 14` / `GREY 15` / `LIGHT_PURPLE_2 16` / `LIGHT_GREEN_2 17` / `LIGHT_GREEN_3 18` / `LIGHT_CYAN_2 19` / `LIGHT_RED_2 20` / `MAGENTA 21` / `LIGHT_PURPLE_3 22` / `LIGHT_ORANGE 23` / `ORANGE 24` / `LIGHT_PURPLE_4 25` / `LIGHT_YELLOW 26` / `LIGHT_YELLOW_2 27` / `WHITE 28` |
| Opacidad de la pantalla del VMU 4 | `10%` / `20%` / `30%` / `40%` / `50%` / `60%` / `70%` / `80%` / `90%` / `100%` | `reicast_vmu4_screen_opacity` | `10%` / `20%` / `30%` / `40%` / `50%` / `60%` / `70%` / `80%` / `90%` / `100%` |

## ![external-links.png](/emulators/external-links.png) Enlaces externos

* **Código fuente utilizado** : [https://github.com/flyinghead/flycast](https://github.com/flyinghead/flycast)
