---
title: Recalbox para vuestro codificador de teclado USB
description: 
published: true
date: 2023-09-18T11:58:31.894Z
tags: usb, teclado, codificador
editor: markdown
dateCreated: 2023-09-18T11:53:23.099Z
---

## Introducción

Xarcade2jstick ha sido parcheado para soportar codificadores de teclado. Puede que necestéis reconfigurar algunas teclas del codificador, ya que desafortunadamente los dispositivos Xarcade de X-gaming no han sido mapeados como los controladores MAME normales.

## Añadir vuestro codificador de teclado

Por ahora, sólo se han incluido la primera generación de IPAC. Si tenéis otro codificador de teclado, seguid las siguientes etapas:

* Conectaros vía [SSH](./../../../tutorials/system/access/root-access-terminal-cli).
* Descubrid el nombre del periférico asociado a vuestro codificador utilizando el comando `ls /dev/input/by-id`. Normalmente, el resultado que nos interesa aparece seguido de un kbd. Por ejemplo : _usb-Ultimarc_IPAC_2_Ultimarc_IPAC_2_9-if01-event-kbd_

>Un mismo codificador puede tener varios nombres posibles, así que probadlos todos, por ejemplo :
>
>* usb-Cypress_I-PAC_Arcade_Control_Interface-event-kbd -> funciona
>* usb-Cypress_I-PAC_Arcade_Control_Interface-if01-event-kbd -> no funciona
{.is-info}

* Montad la [paritición del sistema](./../../../tutorials/system/access/remount-partition-with-write-access) en modo lectura-escritura.
* Cread un fichero vacío con el mismo nombre que vuestro periférico con el siguiente comando:

```shell
touch /recalbox/share_init/system/configs/xarcade2jstick/devicename
```

Para el ejemplo anterior:

```shell
touch /recalbox/share_init/system/configs/xarcade2jstick/usb-Ultimarc_IPAC_2_Ultimarc_IPAC_2_9-if01-event-kbd
```

* Modificad [recalbox.conf](./../../../basic-usage/getting-started/recalboxconf-file) y añadid lo siguiente en una nueva línea:

```ini
controllers.xarcade.enabled=1
```

* Reiniciad Recalbox escribiendo `reboot`

A partir de este momento, Recalbox se encuentra configurado para este tipo de periféricos.