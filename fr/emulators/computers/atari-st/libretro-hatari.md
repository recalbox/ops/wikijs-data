---
title: Libretro Hatari
description: 
published: true
date: 2024-06-23T11:47:49.175Z
tags: libretro, atari-st, hatari
editor: markdown
dateCreated: 2021-05-21T08:15:36.781Z
---



## ![](/emulators/license.svg) Licence

Ce core est sous licence [**GPLv2**](https://github.com/libretro/hatari/blob/master/readme.txt).

## ![](/emulators/compatibility.png) Compatibilité

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Fonctionnalités

| Fonctionnalité | Supporté |
| :---: | :---: |
| Options du core | ✔ |
| Contrôles | ✔ |
| Remapping | ✔ |

## ![](/emulators/bios.svg) BIOS

### Liste des bios obligatoires

| Nom de fichier | Description | MD5 | Fourni |
| :---: | :---: | :---: | :---: |
| emutos.img | Bios open-source EMUTOS. Compatible à 99% avec toutes les machines Atari. | e2c861c588fca2d0cf6be3df3aaf05f2 | ✅ |
| st.img | Bios Atari ST/Mega-ST (pas STE !) | 0087e2707c57efa2106a0aa7576655c0 036c5ae4f885cbf62c9bed651c6c58a8 0604dbb85928f0598d04144a8b554bbe 091a4d95f6c600231a3f1391a309ac26 143343f7b8e0b1162af206fe8f46b002 25789a649faff0a1176dc7d9b98105c0 332fe3803a7e20cd625b27a69f07ae69 41b7dae4e24735f330b63ad923a0bfbc 4a0d4f282c3f2a0196681adf88862dd4 5491b3755992feaf743ceecaa3b18f34 52248cc70ae48b3050e197e270917130 696ba04b3bc963aee2ba769298e342de 736adb2dc835df4d323191fdc8926cc9 7c040857bdcfcd7d748ca82205463efa 800d48825be45ec7f39ba7bb2c3f0b59 886b9b6bb6afa287211a594e16f6ca67 91ce7c1903006da75c823ff3c938e5d2 91fb61080d27d0376e1a3da409727468 a622cc35d8d78703905592dfaa4d2ccb a7dc40dc5c1086bce1a8f3d44fd29051 b2873004a408b8db36321f98daafa251 b2a8570de2e850c5acf81cb80512d9f6 b9e37f0f0a700fc673049794870bdb85 c1c57ce48e8ee4135885cee9e63a68a2 c87a52c277f7952b41c639fc7bf0a43b c9093f27159e7d13ac0d1501a95e53d4 d0f682ee6237497004339fb02172638b d6521785627d20c51edc566808a6bf28 e0444d8c51a8093a3f09665d8099c0af e690bec90d902024beed549d22150755 f6a8fa31d0d1bac1a08d47502244408d | ❌ |
| ste.img | Bios Atari STE | 0604dbb85928f0598d04144a8b554bbe 1cbc4f55295e469fc8cd72b7efdea1da 30f69d70fe7c210300ed83f991b12de9 332fe3803a7e20cd625b27a69f07ae69 4759f2c7bace6c2f440d27acd2ab6ed4 4a0d4f282c3f2a0196681adf88862dd4 6033f2b9364edfed321c6931a8181fd2 61168619b5109d314638c9409f44bca2 6f9471098a521214fad1e2c6f2dd3d08 886b9b6bb6afa287211a594e16f6ca67 94a75c1c65408d9f974b0463e15a3b11 992bac38e01633a529121a2a65f0779e a0982e760f9807d82667ff5a69e78f6b a4cfd3c7412002dd693f69a15d4d961c a7dc40dc5c1086bce1a8f3d44fd29051 b2873004a408b8db36321f98daafa251 bc7b224d0dc3f0cc14c8897db89c5787 c9093f27159e7d13ac0d1501a95e53d4 e690bec90d902024beed549d22150755 ed5fbaabe0219092df74c6c14cea3f8e f3f92cd4e2c331aef8970a156be03b7c febb00ba8784798293a7ae709a1dafcb | ❌ |

### Emplacement

Placez les BIOS comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 atarist
┃ ┃ ┃ ┃ ┣ 🗒 **emutos.img**
┃ ┃ ┃ ┃ ┣ 🗒 **st.img**
┃ ┃ ┃ ┃ ┣ 🗒 **ste.img**

### Liste des bios optionnels

| Nom de fichier | Description | MD5 | Fourni |
| :---: | :---: | :---: | :---: |
| falcon.img | Bios Atari Falcon 30/40 | 919fd0f045299f101cf7757e5e058c41 941baac767269649f92f7918dd5740b8 9e880168d0a004f7f5e852be758f39e4 b2ecabc15b65afef96331be46f2a2cd4 b338bacb2fc453bab61bcc1e2cf5076b e5ea0f216fb446f1c4a4f476bc5f03d4 ed2647936ce4bd283c4d7dfd7ae09d1c | ❌ |
| megaste.img | Bios Atari Mega-STE | 0604dbb85928f0598d04144a8b554bbe 1c92855316a33faee602b8007f22d2cb 332fe3803a7e20cd625b27a69f07ae69 4689ef3694ac86e7289923190e6cd10d 4a0d4f282c3f2a0196681adf88862dd4 61b620ad951815a25cb37895c81a947c 7449b131681f1dfe62ebed1392847057 7aeabdc25f8134590e25643a405210ca 7cdd45b6aac66a21bfb357d9334e46db 7e87d8fe7e24e0b4c55ad1b7955beae3 886b9b6bb6afa287211a594e16f6ca67 a7dc40dc5c1086bce1a8f3d44fd29051 b2873004a408b8db36321f98daafa251 c9093f27159e7d13ac0d1501a95e53d4 e690bec90d902024beed549d22150755 | ❌ |
| tos.img | TOS Image Boot | 036c5ae4f885cbf62c9bed651c6c58a8 0604dbb85928f0598d04144a8b554bbe 066f39a7ea5789d5afd59dd7b3104fa6 091a4d95f6c600231a3f1391a309ac26 143343f7b8e0b1162af206fe8f46b002 1c92855316a33faee602b8007f22d2cb 25789a649faff0a1176dc7d9b98105c0 27ac71b59d59f65571b972e0f894054a 2a8e39c60317b921eabd7e25b5d74395 30f69d70fe7c210300ed83f991b12de9 332fe3803a7e20cd625b27a69f07ae69 41b7dae4e24735f330b63ad923a0bfbc 4a0d4f282c3f2a0196681adf88862dd4 52248cc70ae48b3050e197e270917130 5491b3755992feaf743ceecaa3b18f34 5d65c6a384d847a0b8013d5e22ec0887 61b620ad951815a25cb37895c81a947c 696ba04b3bc963aee2ba769298e342de 6f9471098a521214fad1e2c6f2dd3d08 736adb2dc835df4d323191fdc8926cc9 7449b131681f1dfe62ebed1392847057 7c040857bdcfcd7d748ca82205463efa 7cdd45b6aac66a21bfb357d9334e46db 7e87d8fe7e24e0b4c55ad1b7955beae3 800d48825be45ec7f39ba7bb2c3f0b59 91ce7c1903006da75c823ff3c938e5d2 941baac767269649f92f7918dd5740b8 94a75c1c65408d9f974b0463e15a3b11 992bac38e01633a529121a2a65f0779e 9e880168d0a004f7f5e852be758f39e4 a0982e760f9807d82667ff5a69e78f6b a4cfd3c7412002dd693f69a15d4d961c a7dc40dc5c1086bce1a8f3d44fd29051 b2873004a408b8db36321f98daafa251 b2a8570de2e850c5acf81cb80512d9f6 b9e37f0f0a700fc673049794870bdb85 bc7b224d0dc3f0cc14c8897db89c5787 c1c57ce48e8ee4135885cee9e63a68a2 d0f682ee6237497004339fb02172638b d41ae4efaa2c97f632dbe75d24d51bea d6521785627d20c51edc566808a6bf28 e5ea0f216fb446f1c4a4f476bc5f03d4 e690bec90d902024beed549d22150755 ed2647936ce4bd283c4d7dfd7ae09d1c ed5fbaabe0219092df74c6c14cea3f8e f3f92cd4e2c331aef8970a156be03b7c f62bc9777f28bc6876422738758db365 febb00ba8784798293a7ae709a1dafcb | ❌ |
| tt.img | Bios Atari TT | 066f39a7ea5789d5afd59dd7b3104fa6 19cd81e1c3049bf408ad426dc5c3b2e0 2a8e39c60317b921eabd7e25b5d74395 ac6ca0f0ed42a1603dfd409c4bf8eb89 dd1010ec566efbd71047d6c4919feba5 | ❌ |

### Emplacement

Placez les BIOS comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 atarist
┃ ┃ ┃ ┃ ┣ 🗒 **falcon.img**
┃ ┃ ┃ ┃ ┣ 🗒 **megaste.img**
┃ ┃ ┃ ┃ ┣ 🗒 **tos.img**
┃ ┃ ┃ ┃ ┣ 🗒 **tt.img**

## ![](/emulators/roms.png) Roms

### Extensions supportées

Les roms doivent avoir les extensions suivantes :

* .dim
* .gem
* .ide
* .ipf
* .m3u
* .msa
* .st
* .stx
* .vhd
* .zip
* .7z

Ce système supporte les roms compressées au format .zip/.7z. Attention toutefois, il ne s'agit que d'une archive.

Les fichiers contenus dans les .zip/.7z doivent correspondre aux extensions citées précédemment.  
De plus, chaque fichier .zip/.7z ne doit contenir qu'une seule rom compressée.

### Emplacement

Placez les roms comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 atarist
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.zip**

>Les roms au format **No-Intro** sont vivement conseillées.
{.is-success}

>Pour plus d'information sur les roms, rendez-vous sur [ce tutoriel](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuration avancée de l'émulateur
 
>Pour pouvoir conserver vos configurations personnalisées lors d'une mise à jour, nous vous conseillons d'utiliser notre fonctionnalité [Surcharges de configuration](./../../../advanced-usage/configuration-override).
{.is-info}

### Accéder aux options

Vous pouvez configurer diverses options de deux façons différentes.

* Via le menu RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via le fichier `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Options du core

| Options | Valeur des options | Variable (pour surcharge) | Valeurs des variables |
| --- | --- | --- | --- |
| Enable second joystick | `Activé` ✅ / `Désactivé` | `hatari_twojoy` | `true` / `false` |
| Disable mouse | `Désactivé` ✅ / `Activé` | `hatari_nomouse` | `false` / `true` |
| Disable keyboard | `Désactivé` ✅ / `Activé` | `hatari_nokeys` | `false` / `true` |
| Fast floppy access | `Activé` ✅ / `Désactivé` | `hatari_fastfdc` | `true` / `false` |
| Polarized audio filter | `Désactivé` ✅ / `Activé` | `hatari_polarized_filter` | `false` / `true` |
| High resolution | `Activé` ✅ / `Désactivé` | `hatari_video_hires` | `true` / `false` |
| Crop overscan | `Désactivé` ✅ / `Activé` | `hatari_video_crop_overscan` | `false` / `true` |
| Frameskip | `Désactivé` ✅ / `1` / `2` / `3` / `4` / `auto (max 5)` / `auto (max 10)` | `hatari_frameskips` | `0` / `1` / `2` / `3` / `4` / `5` / `10` |

## ![](/emulators/external-links.png) Liens externes

* **Code source utilisé** : [https://github.com/libretro/hatari/](https://github.com/libretro/hatari/)
* **Documentation Libretro** : [https://docs.libretro.com/library/hatari/](https://docs.libretro.com/library/hatari/)