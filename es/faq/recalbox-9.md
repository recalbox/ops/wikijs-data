---
title: FAQ Recalbox 9
description: 
published: true
date: 2023-08-31T13:50:29.650Z
tags: faq
editor: markdown
dateCreated: 2023-08-31T13:50:29.650Z
---

# GPiCase2W y Raspberry pi Zero (no el 2!)

Para instalar Recalbox 9 en la **GPiCase2W** y con el **Raspberry Pi Zero 1**, las etapas son las siguientes

- Descargad [l'image pour Raspberry Pi Zero / 1](https://upgrade.recalbox.com/latest/download/rpi1/recalbox-rpi1.img.xz) o instaladla directamente desde el programa `Rpi Imager`.
- Ejecutad la instalación en vuestro RPi Zero, conectado a vuestra GPiCase2W. La pantalla será muy pequeña.
- Una vez instalado, apagad vuestra GPiCase2W y recuperad vuestra tarjeta SD.
- En la partición `RECALBOX`, reemplazad el fichero `recalbox-user-config.txt` por el siguiente: [recalbox-user-config.txt](https://recalbox-public-assets.s3.fr-par.scw.cloud/recalbox-user-config.txt)
- Volved a meter la tarjeta SD en vuestra GPiCase2W y ya no os queda más que disfrutar!.

![gpi.jpg](/faq9/gpi.jpg)

## No hay vídeo de introducción

Los vídeos de introducción pueden saltarse si se pulsa un botón o una dirección del mando de juegos.

Algunos usuarios los saltan automáticamente porque el controlador USB envía eventos de entrada aleatorios cuando Recalbox arranca.

Comprobad que si désenchufáis el mando se resuelve el problema, y nosotros intentaremos evitar este comportamiento en las próximas versiones.

## Shinobi se reinicia cuando rebobino en la Master System

Modificad las informaciones del juego en el menu de EmulationStation y seleccionad otro core.
