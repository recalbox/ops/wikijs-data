---
title: Integer scale (Pixel Perfect)
description: 
published: true
date: 2021-08-06T15:18:54.116Z
tags: integer, scale, pixel, perfect
editor: markdown
dateCreated: 2021-08-06T12:50:54.118Z
---

## Purpose

Activate the "Pixel Perfect" image scaling mode which is to get the exact rendering of the games on a modern screen.

## Steps

* Go to the main menu of EmulationStation by pressing the `START` button.
* Go to the `GAME SETTINGS` section
* Under the `SHADERS SET` option, select `SCANLINES` or `RETRO`.
* Turn on the `INTEGER SCALE (PIXEL PERFECT)` option.

>In this mode, the image may no longer be full screen!
{.is-warning}