---
title: Libretro QuickNES
description: 
published: true
date: 2024-07-05T15:06:35.348Z
tags: libretro, nes, quicknes
editor: markdown
dateCreated: 2023-09-07T06:53:51.170Z
---



## ![](/emulators/license.svg) Licencia

El core posee la siguiente licencia [**LGPLv2.1+**](https://github.com/kode54/QuickNES/blob/master/COPYING).

## ![](/emulators/compatibility.png) Compatibilidad

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | 
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | 

## ![](/emulators/features.png) Funcionalidades

| Funcionalidad | ¿Soportada? |
| :---: | :---: |
| Reinicio | ✔ |
| Capturas de pantalla | ✔ |
| Grabaciones | ✔ |
| Grabaciones instantáneas | ✔ |
| Rebobinado | ✔ |
| Opciones del core | ✔ |
| RetroAchievements | ✔ |
| Controles | ✔ |
| Remapping | ✔ |
| Softpatching | ✔ |

## ![](/emulators/bios.svg) BIOS

>**No necesita bios.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Extensiones de roms soportadas

Las roms deben tener las extensiones siguientes:

* .nes
* .unf
* .unif
* .zip
* .7z

Este sistema soporta las roms comprimidas en formato .zip/.7z. No obstante, se trata sólo de un archivo.

Los ficheros contenidos en los .zip/.7z deben tener las extensiones citadas previamente.
Además, cada fichero .zip/.7z sólo debe contener un fichero rom al interior.

### Emplazamiento

Disponed vuestras roms como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 nes
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.zip**

>Se aconsejan las roms del proyecto **No-Intro**.
{.is-success}

>Para más información sobre las roms, consultad [este tutorial](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

>Para conservar las configuraciones personalizadas cuando se produce una actualización del sistema Recalbox, os aconsejamos utilizar las [Sobrecargas de configuración](./../../../advanced-usage/configuration-override).
{.is-info}

### Acceder a las opciones

Podéis configurar las opciones del core de dos formas diferentes.

* Via el Menú RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via el fichero `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Opciones del core

| Opción | Valor de la opción | Variable (para la sobrecarga) | Valores posibles |
| --- | --- | --- | --- |
| Rapport d'aspect | `PAR (rapport d'aspect des pixels)` ✅ / `4:3` | `quicknes_aspect_ratio_par` | `PAR` / `4:3` |
| Afficher le surbalayage horizontal | `Activé` ✅ / `Désactivé` | `quicknes_use_overscan_h` | `enabled` / `disabled` |
| Afficher le surbalayage vertical | `Désactivé` ✅ / `Activé` | `quicknes_use_overscan_v` | `disabled` / `enabled` |
| Palette de couleurs | `default` ✅ / `Réelle par AspiringSquire` / `Console virtuelle Nintendo` / `PPU RVG Nintendo` / `YUV-V3 de FBX` / `Finale non saturée de FBX` / `Sony CXA2025AS US` / `PAL` / `Finale 2 de BMF` / `Finale 3 de BMF` / `Lisse de FBX` / `Composite directe de FBX` / `Style PVM D93 de FBX` / `Matériel NTSC de FBX` / `NES Classic (corrigée) de FBX` / `NESCAP de RGBSource` / `Wavebeam de nakedarthur` | `quicknes_palette` | `default` / `asqrealc` / `nintendo-vc` / `rgb` / `yuv-v3` / `unsaturated-final` / `sony-cxa2025as-us` / `pal` / `bmf-final2` / `bmf-final3` / `smooth-fbx` / `composite-direct-fbx` / `pvm-style-d93-fbx` / `ntsc-hardware-fbx` / `nes-classic-fbx-fs` / `nescap` / `wavebeam` |
| Aucune limite de sprites | `Désactivé` ✅ / `Activé` | `quicknes_no_sprite_limit` | `disabled` / `enabled` |
| Mode audio | `Non linéaire` ✅ / `Linéaire` / `Panoramique stéréo` | `quicknes_audio_nonlinear` | `nonlinear` / `linear` / `stereo panning` |
| Préréglage de l'égaliseur audio | `Default` ✅ / `Famicom` / `TV` / `Plat` / `Net` / `Métallique` | `quicknes_audio_eq` | `default` / `famicom` / `tv` / `flat` / `crisp` / `tinny` |
| Activer le turbo | `None` ✅ / `Joueur 1` / `Joueur 2` / `Joueur 1 & 2` | `quicknes_turbo_enable` | `none` / `player 1` / `player 2` / `both` |
| Largeur d'impulsion du turbo (en images) | `1` / `2` / `3` ✅ / `5` / `10` / `15` / `30` / `60` | `quicknes_turbo_pulse_width` | `1` / `2` / `3` / `5` / `10` / `15` / `30` / `60` |
| Autoriser les directions opposées | `Désactivé` ✅ / `Activé` | `quicknes_up_down_allowed` | `disabled` / `enabled` |


## ![](/emulators/external-links.png) Enlaces externos

* **Código fuente utilizado** : [https://github.com/libretro/QuickNES_Core/](https://github.com/libretro/QuickNES_Core/)
* **Documentación Libretro** : [https://docs.libretro.com/library/quicknes/](https://docs.libretro.com/library/quicknes/)