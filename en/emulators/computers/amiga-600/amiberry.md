---
title: Amiberry
description: 
published: true
date: 2021-10-11T17:08:57.721Z
tags: amiga-600, amiga, amiberry, 600
editor: markdown
dateCreated: 2021-07-29T15:28:30.648Z
---

**Amiberry** is a core for ARM optimized for Amiga.

## ![](/emulators/license.svg) License

This core isunder [**GPLv3**](https://github.com/midwan/amiberry/blob/master/COPYING) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ❌ | ❌ |

## ![](/emulators/features.png) Features



## ![](/emulators/bios.svg) BIOS

### List of mandatory bios

You will find the bios with the name indicated in the column **Description** that you will have to rename with the name indicated in the column **Filename**.

| Filename | Description | MD5 | Provided |
| :---: | :---: | :---: | :---: |
| kick33180.A500.rom | Kickstart v1.2 rev 33.180 (1986)(Commodore)(A500-A1000-A2000).rom | 85ad74194e87c08904327de1a9443b7a | ❌ |
| kick34005.A500.rom | Kickstart v1.3 rev 34.5 (1987)(Commodore)(A500-A1000-A2000-CDTV).rom | 82a21c1890cae844b3df741f2762d48d | ❌ |
| kick37175.A500.rom | Kickstart v2.04 rev 37.175 (1991)(Commodore)(A500+).rom | dc10d7bdd1b6f450773dfb558477c230 | ❌ |
| kick40063.A600.rom | Kickstart v3.1 rev 40.63 (1993)(Commodore)(A500-A600-A2000)[!].rom | e40a5dfb3d017ba8779faba30cbd1c8e | ❌ |
| kick02019.AROS.ext.rom | AROS Free BIOS | 4fa7042d73d0f77bf5723cf8430989bf | ✅ |
| kick02019.AROS.rom | AROS Free BIOS | 186d23cccd9fbd6336d0f988b39dce4d | ✅ |

### Location

Put the bios like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 🗒 **kick33180.A500.rom**
┃ ┃ ┃ ┣ 🗒 **kick34005.A500.rom**
┃ ┃ ┃ ┣ 🗒 **kick37175.A500.rom**
┃ ┃ ┃ ┣ 🗒 **kick40063.A600.rom**
┃ ┃ ┃ ┣ 🗒 **kick02019.AROS.ext.rom**
┃ ┃ ┃ ┣ 🗒 **kick02019.AROS.rom**

## ![](/emulators/roms.png) Roms

>Amiga emulation is available in two flavors: Amiga 600 and Amiga 1200.  
>Some games can be very temperamental so try both systems if a game doesn't work.
>The games can be used either in ADF disk format or in WHDLoad format, using in this case a complementary UAE file.
{.is-info}

### Supported extensions

Roms must have the extension:

* .adf
* .ipf
* .rp9
* .whd
* .zip

>Other disk image formats _could_ be supported but have not been tested by the Recalbox team.
>
>You can find more information about these formats on [this page](./../../../tutorials/games/guides/amiga/faq).
{.is-info}

### Location

Put the roms like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 amiga600
┃ ┃ ┃ ┃ ┣ 🗒 **game.bin**
┃ ┃ ┃ ┃ ┣ 🗒 **game.lst**

>The roms using **TOSEC** format are highly recommended.
{.is-success}

>For more information about roms, go to [this tutorial](./../../../tutorials/games/generalities/isos-and-roms)!
{.is-info}

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://gitlab.com/recalbox/packages/standalone/amiberry/](https://gitlab.com/recalbox/packages/standalone/amiberry)
* **Official website**: [https://blitterstudio.com/amiberry/](https://blitterstudio.com/amiberry/)