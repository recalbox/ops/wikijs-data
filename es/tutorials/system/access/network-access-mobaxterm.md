---
title: Acceso en red via MobaXTerm
description: 
published: true
date: 2024-07-26T06:31:26.414Z
tags: mobaxterm, red, acceso
editor: markdown
dateCreated: 2023-09-13T19:58:09.630Z
---

Con MobaXTerm, podéis:

* Realizar una conexión SFTP
* Utilizar líneas de comandos SSH

## Requisitos previos

Necestáis descargar [MobaXTerm](https://mobaxterm.mobatek.net/). No tenéis que instalarlo, hay una versión portable sin instalación.

## Configuración

### Connexión via SFTP

* Lanzad MobaXTerm para encontraros con la siguiente ventana.

![](/tutorials/system/access/mobaxterm1.png){.full-width}

* Haced click en "Sesión" en la parte superior de la ventana.
* En esta ventana, haced click en `SFTP` para ir a esta ventana.

![](/tutorials/system/access/mobaxterm2.png){.full-width}

* En el campo `Remote host`, indicad [la dirección IP](./../../../tutorials/network/ip/discover-recalbox-ip) de vuestra Recalbox, y en el campo `Username`, indicad `root`y pulsado sobre OK más abajo.
* Aparecerá una ventana para preguntaros la contraseña.

![](/tutorials/system/access/mobaxterm3.png){.full-width}

* En el campo visible, escribid `recalboxroot` y pulsad OK. En la siguiente ventana podéis grabar la contraseña para no tener que escribirla la próxima vez.

>Veréis una ventana pidiéndoos la creación de una contraseña maestra. Esto os permitirá grabar todas las conexiones realizadas a MobaXTerm.
{.is-info}

*Una vez validadas todas las ventanas, estaréis conectados.

![](/tutorials/system/access/mobaxterm4.png){.full-width}

>Cuando cerréis la conexión, la información de la sesión se guardará para que puedas conectarte más fácilmente en otro momento.
{.is-success}

### Connexión via SSH

Una vez registrada la conexión, podéis crear otra conexión para conectaros a través de SSH.

* En el panel de la izquierda, pulsad con el botón derecho del ratón sobre su conexión y seleccionad `Duplicar sesión`.
* En la conexión duplicada, pulsad con el botón derecho y seleccionad "Editar sesión".
* En la parte superior de la nueva ventana, pulsad sobre "SSH".
* Introducid la misma dirección IP, marcad la casilla `Especificar nombre de usuario` y meted el mismo nombre de usuario que antes (es decir, `root`), a continuación, pulsad Aceptar.
* La conexión SSH ha sido creada.

![](/tutorials/system/access/mobaxterm5.png){.full-width}

## Utilización

Cuando os conectáis por SFTP, podéis ver:

* Los ficheros de vuestro PC a un lado,
* Los ficheros de vuestra Recalbox al otro lado.

Por defecto, os conectaréis dentro del directorio `/recalbox/share/system`.