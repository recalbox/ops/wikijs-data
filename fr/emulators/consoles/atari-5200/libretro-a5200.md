---
title: Libretro A5200
description: 
published: true
date: 2024-06-14T14:53:57.174Z
tags: libretro, atari, 5200, atari-5200, 8.1+, a5200
editor: markdown
dateCreated: 2022-07-22T10:05:53.565Z
---



## ![](/emulators/license.svg) Licence

Ce core est sous licence [**GPLv2**](https://github.com/libretro/a5200/blob/master/License.txt).

## ![](/emulators/compatibility.png) Compatibilité

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Fonctionnalités

| Fonctionnalité | Supporté |
| :---: | :---: |

## ![](/emulators/bios.svg) BIOS

### Liste des bios obligatoires

| Nom de fichier | Description | MD5 | Fourni |
| :---: | :---: | :---: | :---: |
| 5200.rom | 5200 - Requis | 281f20ea4320404ec820fb7ec0693b38 | ❌ |

### Emplacement

Placez les bios comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 atari5200
┃ ┃ ┃ ┃ ┣ 🗒 **5200.rom**

## ![](/emulators/roms.png) Roms

### Extensions supportées

Les roms doivent avoir les extensions suivantes :

* .a52
* .atr
* .atr.gz
* .bas
* .bin
* .car
* .cdm
* .xex
* .xfd
* .xfd.gz
* .zip

Ce système supporte les roms compressées au format .zip. Attention toutefois, il ne s'agit que d'une archive.

Les fichiers contenus dans les .zip doivent correspondre aux extensions citées précédemment.
De plus, chaque fichier .zip ne doit contenir qu'une seule rom compressée.

### Emplacement

Placez les roms comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 atari5200
┃ ┃ ┃ ┃ ┣ 🗒  **jeu.zip**

>Les roms au format **No-Intro** sont vivement conseillées.
{.is-success}

>Pour plus d'information sur les roms, rendez-vous sur [ce tutoriel](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuration avancée de l'émulateur

>Pour pouvoir conserver vos configurations personnalisées lors d'une mise à jour, nous vous conseillons d'utiliser notre fonctionnalité [Surcharges de configuration](./../../../advanced-usage/configuration-override).
{.is-info}

### Accéder aux options

Vous pouvez configurer diverses options de deux façons différentes.

* Via le menu RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via le fichier `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Options du core

| Options | Valeur des options | Variable (pour surcharge) | Valeurs des variables |
| --- | --- | --- | --- |
| BIOS (Restart) | `Official` ✅ / `Internal (Altirra OS)` | `a5200_bios` | `official` / `internal` |
| Interframe Blending | `Désactivé` ✅ / `Simple` / `Ghosting (65%)` / `Ghosting (75%)` / `Ghosting (85%)` / `Ghosting (95%)` | `a5200_mix_frames` | `disabled` / `mix` / `ghost_65` / `ghost_75` / `ghost_85` / `ghost_95` |
| Hi-Res Artifacting Mode | `None` ✅ / `blue/brown 1` / `blue/brown 2` / `GTIA` / `CTIA` | `a5200_artifacting_mode` | `none` / `blue/brown 1` / `blue/brown 2` / `GTIA` / `CTIA` |
| High Fidelity POKEY (Restart) | `Désactivé` / `Activé` ✅ | `a5200_enable_new_pokey` | `disabled` / `enabled` |
| Audio Filter | `Désactivé` ✅ / `Activé` | `a5200_low_pass_filter` | `disabled` / `enabled` |
| Audio Filter Level | `5%` / `10%` / `15%` / `20%` / `25%` / `30%` / `35%` / `40%` / `45%` / `50%` / `55%` / `60%` ✅ / `65%` / `70%` / `75%` / `80%` / `85%` / `90%` / `95%` | `a5200_low_pass_range` | `5` / `10` / `15` / `20` / `25` / `30` / `35` / `40` / `45` / `50` / `55` / `60` / `65` / `70` / `75` / `80` / `85` / `90` / `95` |
| Controller Hacks | `Désactivé` ✅ / `Dual Stick` / `Swap Ports` | `a5200_input_hack` | `disabled` / `dual_sticks` / `swap_ports` |
| Pause acts as Reset | `Désactivé` ✅ / `Activé` | `a5200_pause_is_reset` | `disabled` / `enabled` |
| Digital Joystick Sensitivity | `Auto` ✅ / `5%` / `7%` / `10%` / `12%` / `15%` / `17%` / `20%` / `22%` / `25%` / `27%` / `30%` / `32%` / `35%` / `37%` / `40%` / `42%` / `45%` / `47%` / `50%` / `52%` / `55%` / `57%` / `60%` / `62%` / `65%` / `67%` / `70%` / `72%` / `75%` / `77%` / `80%` / `82%` / `85%` / `87%` / `90%` / `92%` / `95%` / `97%` / `100%` | `a5200_digital_sensitivity` | `auto` / `5` / `7` / `10` / `12` / `15` / `17` / `20` / `22` / `25` / `27` / `30` / `32` / `35` / `37` / `40` / `42` / `45` / `47` / `50` / `52` / `55` / `57` / `60` / `62` / `65` / `67` / `70` / `72` / `75` / `77` / `80` / `82` / `85` / `87` / `90` / `92` / `95` / `97` / `100` |
| Analog Joystick Sensitivity | `Auto` ✅ / `5%` / `7%` / `10%` / `12%` / `15%` / `17%` / `20%` / `22%` / `25%` / `27%` / `30%` / `32%` / `35%` / `37%` / `40%` / `42%` / `45%` / `47%` / `50%` / `52%` / `55%` / `57%` / `60%` / `62%` / `65%` / `67%` / `70%` / `72%` / `75%` / `77%` / `80%` / `82%` / `85%` / `87%` / `90%` / `92%` / `95%` / `97%` / `100%` | `a5200_analog_sensitivity` | `auto` / `5` / `7` / `10` / `12` / `15` / `17` / `20` / `22` / `25` / `27` / `30` / `32` / `35` / `37` / `40` / `42` / `45` / `47` / `50` / `52` / `55` / `57` / `60` / `62` / `65` / `67` / `70` / `72` / `75` / `77` / `80` / `82` / `85` / `87` / `90` / `92` / `95` / `97` / `100` |
| Analog Joystick Response | `Linear` ✅ / `Quadratic` | `a5200_analog_response` | `linear` / `quadratic` |
| Analog Joystick Deadzone | `0` / `3` / `5` / `7` / `10` / `13` / `15` ✅ / `17` / `20` / `23` / `25` / `27` / `30` | `a5200_analog_deadzone` | `0` / `3` / `5` / `7` / `10` / `13` / `15` / `17` / `20` / `23` / `25` / `27` / `30` |
| Analog Device | `Analog Stick` ✅ / `Mouse` | `a5200_analog_device` | `analog_stick` / `mouse` |

## ![](/emulators/external-links.png) Liens externes

* **Code source utilisé** : [https://github.com/libretro/a5200/](https://github.com/libretro/a5200/)