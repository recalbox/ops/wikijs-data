---
title: Libretro MAME 2003
description: 
published: true
date: 2024-06-29T17:34:43.315Z
tags: libretro, mame, mame2003
editor: markdown
dateCreated: 2023-09-05T12:43:05.791Z
---

**Libretro MAME2003** es un emulador más reciente que imame4all. Tiene muchos más juegos funcionales.

## ![](/emulators/license.svg) Licencia

El core posee la licencia [MAME no-commercial](https://github.com/libretro/mame2003-libretro/blob/master/LICENSE.md).

## ![](/emulators/compatibility.png) Compatibilidad

 | RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
 | :---: | :----: | :---: | :---: | :---: | :---: | :---: | :---: |
 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Funcionalidades

| Funcionalidad | ¿Soportada? |
| :---: | :---: |
| Grabaciones | ✔ |
| Grabaciones instantáneas | Depende del juego |
| Rebobinado | ✔ |
| Netplay | ✔ |
| Cheats nativos | ✔ |
| Mandos | ✔ |
| Multi-Ratón | ✔ |

## ![](/emulators/bios.svg) BIOS

Las romsets de BIOS no son necesarias si utilizáis las romsets de arcade "Full Non-Merged". Para las romset de tipo "Split" y "Non-Merged", disponed las BIOS necesarias en la misma carpeta que la romset del juego.


>**Aviso** :
>Poned las bios/devices dentro del directorio : `/recalbox/share/roms/mame/`   
>o dentro de una subcarpeta si utilizáis un directorio diferente por core Mame, por ejemplo: `/recalbox/share/roms/mame/Mame2003/`
Los juegos junto con ssus bios deben encontrarse dentro del mismo directorio o subdirectorio.

{.is-warning}

## ![](/emulators/roms.png) Roms

* Basado en el romset: MAME 0.78 (Décembre 2003)
* Talla : 28 GB
* Romsets emulados : 4 721 (incluant les clones, etc...)
* Sets Activos: 4721
* Padres : 1042
* Clones : 2039
* Autres : 1624
* BIOS : 16
* CHDs : 30
* Versión CHDs : 3
* Samples : 2013
* Fichier dat : `/recalbox/share/arcade/libretro/mame2003.dat`.

### Emplazamiento

Disponed vuestras roms como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 mame
┃ ┃ ┃ ┃ ┣ 🗒 **juego.zip**
┃ ┃ ┃ ┃ ┣ 📁 **juego**
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **juego.chd**

También podéis utilizar el subdirectorio mame2003, si queréis clasificar vuestros fullsets por el core que las hace funcionar. Si utilizáis este subdirectorio vuestros juegos se lanzarán automáticamente bajo el emulador mame2003.


┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 mame
┃ ┃ ┃ ┃ ┣ 📁 mame2003
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **juego.zip**
┃ ┃ ┃ ┃ ┃ ┣ 📁 **juego**
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 **juego.chd**

## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

>Para conservar las configuraciones personalizadas cuando se produce una actualización del sistema Recalbox, os aconsejamos utilizar las [Sobrecargas de configuración](./../../../advanced-usage/configuration-override).
{.is-info}


### Accéder a las opciones

Podéis configurar las opciones del core de dos formas diferentes.

* Via el Menú RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via el fichero `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg** 

### Opciones del core

| Opción | Valor de la opción | Variable (para la sobrecarga) | Valores posibles |
| --- | --- | --- | --- |
| Skip Disclaimer | `Désactivé` ✅ / `Activé` | `mame2003_skip_disclaimer` | `disabled` / `enabled` |
| Skip Warnings | `Désactivé` ✅ / `Activé` | `mame2003_skip_warnings` | `disabled` / `enabled` |
| Display MAME Menu | `Désactivé` ✅ / `Activé` | `mame2003_display_setup` | `disabled` / `enabled` |
| Legacy Remapping | `Activé` ✅ / `Désactivé` | `mame2003_mame_remapping` | `enabled` / `disabled` |
 | Autosave Hiscore | `default` ✅ / `recursively` / `disabled` | `mame2003_autosave_highscore` | `default` / `recursively` / `disabled` |
 | NVRAM Bootstraps * | `Activé` ✅ / `Désactivé` | `mame2003_nvram_bootstraps` | `enabled` / `disabled` |
| Locate System Files Within a Subfolder | `Activé` ✅ / `Désactivé` | `mame2003_core_sys_subfolder` | `enabled` / `disabled` |
| Locate Save Files Within a Subfolder | `Activé` ✅ / `Désactivé` | `mame2003_core_save_subfolder` | `enabled` / `disabled` |
| Mouse Device | `mouse` ✅ / `pointer` / `Désactivé` | `mame2003_mouse_device` | `mouse` / `pointer` / `disabled` |
| Input Interface | `simultaneous` ✅ / `retropad` / `keyboard` | `mame2003_input_interface` | `simultaneous` / `retropad` / `keyboard` |
 | 4-Way Joystick Emulation on 8-Way Joysticks * | `Désactivé` ✅ / `Activé` | `mame2003_four_way_emulation` | `disabled` / `enabled` |
 | Show Lightgun Crosshairs * | `Activé` ✅ / `Désactivé` | `mame2003_crosshair_enabled` | `enabled` / `disabled` |
| Map Right Analog Stick as Buttons | `Activé` ✅ / `Désactivé` | `mame2003_rstick_to_btns` | `enabled` / `disabled` |
 | Dip Switch/Cheat Input Ports * | `Désactivé` ✅ / `Activé` | `mame2003_cheat_input_ports` | `disabled` / `enabled` |
 | Use Samples | `Activé` ✅ / `Désactivé` | `mame2003_use_samples` | `enabled` / `disabled` || DCS Speedhack | `Activé` ✅ / `Désactivé` | `mame2003_dcs_speedhack` | `enabled` / `disabled` |
 | Sample Rate | `8000 KHz` / `11025 KHz` / `22050 KHz` / `30000 KHz` / `44100 KHz` / `48000 KHz` ✅ | `mame2003_sample_rate` | `8000` / `11025` / `22050` / `30000` / `44100` / `48000` |
 | Bypass Timing Skew | `Désactivé` ✅ / `Activé` | `mame2003_machine_timing` | `disabled` / `enabled` |
| Brightness | `0.2` / `0.3` / `0.4` / `0.5` / `0.6` / `0.7` / `0.8` / `0.9` / `1.0` ✅ / `1.1` / `1.2` / `1.3` / `1.4` / `1.5` / `1.6` / `1.7` / `1.8` / `1.9` / `2.0` | `mame2003_brightness` | `0.2` / `0.3` / `0.4` / `0.5` / `0.6` / `0.7` / `0.8` / `0.9` / `1.0` / `1.1` / `1.2` / `1.3` / `1.4` / `1.5` / `1.6` / `1.7` / `1.8` / `1.9` / `2.0` |
| Gamma Correction | `0.5` / `0.6` / `0.7` / `0.8` / `0.9` / `1.0` ✅ / `1.1` / `1.2` / `1.3` / `1.4` / `1.5` / `1.6` / `1.7` / `1.8` / `1.9` / `2.0` | `mame2003_gamma` | `0.5` / `0.6` / `0.7` / `0.8` / `0.9` / `1.0` / `1.1` / `1.2` / `1.3` / `1.4` / `1.5` / `1.6` / `1.7` / `1.8` / `1.9` / `2.0` |
| TATE Mode | `Désactivé` ✅ / `Activé` | `mame2003_tate_mode` | `disabled` / `enabled` |
| Frameskip | `Désactivé` ✅ / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `9` / `10` / `11` / `auto` / `auto aggressive` / `auto max` | `mame2003_frameskip` | `disabled` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `9` / `10` / `11` / `auto` / `auto_aggressive` / `auto_max` |
| Display Artwork | `Activé` ✅ / `Désactivé` | `mame2003_display_artwork` | `enabled` / `disabled` |
| Resolution Multiplier | `1` ✅ / `2` | `mame2003_art_resolution` | `1` / `2` |
 | Resolution * | `640x480` / `1024x768` ✅ / `1280x960` / `1440x1080` / `1600x1200` / `original` | `mame2003_vector_resolution` | `640x480` / `1024x768` / `1280x960` / `1440x1080` / `1600x1200` / `original` |
 | Antialiasing * | `Activé` ✅ / `Désactivé` | `mame2003_vector_antialias` | `enabled` / `disabled` |
 | Beam Width * | `1` / `1.2` ✅ / `1.4` / `1.6` / `1.8` / `2` / `2.5` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` | `mame2003_vector_beam_width` | `1` / `1.2` / `1.4` / `1.6` / `1.8` / `2` / `2.5` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` |
 | Transculency * | `Activé` ✅ / `Désactivé` | `mame2003_vector_translucency` | `enabled` / `disabled` |
 | Flicker * | `0` / `10` / `20` ✅ / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` | `mame2003_vector_flicker` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
 | Intensity * | `0.5` / `1` / `1.5` ✅ / `2` / `2.5` / `3` | `mame2003_vector_intensity` | `0.5` / `1` / `1.5` / `2` / `2.5` / `3` |
 | Specify Sega ST-V BIOS * | `default` ✅ / `japan` / `japana` / `us` / `japan_b` / `taiwan` / `europe` | `mame2003_stv_bios` | `default` / `japan` / `japana` / `us` / `japan_b` / `taiwan` / `europe` |
 | CPU Clock Scale | `default` ✅ / `25%` / `30%` / `35%` / `40%` / `45%` / `50%` / `55%` / `60%` / `65%` / `70%` / `75%` / `80%` / `85%` / `90%` / `95%` / `105%` / `110%` / `115%` / `120%` / `125%` / `200%` / `250%` / `300%` | `mame2003_cpu_clock_scale` | `default` / `25` / `30` / `35` / `40` / `45` / `50` / `55` / `60` / `65` / `70` / `75` / `80` / `85` / `90` / `95` / `105` / `110` / `115` / `120` / `125` / `200` / `250` / `300` |
 
 * Aparece únicamente si el juego suporta estas opciones.

## ![](/emulators/external-links.png) Enlaces externos

* **Progettosnaps** : [https://www.progettosnaps.net/](https://www.progettosnaps.net/)
* **Código fuente utilizado** : [https://github.com/libretro/mame2003-libretro/](https://github.com/libretro/mame2003-libretro)
* **Documentación Libretro** : [https://docs.libretro.com/library/mame_2003/](https://docs.libretro.com/library/mame_2003/)