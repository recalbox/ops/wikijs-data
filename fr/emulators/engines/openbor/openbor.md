---
title: OpenBOR
description: 
published: true
date: 2024-06-23T00:15:28.915Z
tags: openbor
editor: markdown
dateCreated: 2021-05-21T08:28:32.096Z
---



## ![](/emulators/license.svg) Licence

Ce core est sous licence [**BSD-3**](https://github.com/DCurrent/openbor/blob/master/LICENSE).

## ![](/emulators/compatibility.png) Compatibilité

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Fonctionnalités



## ![](/emulators/bios.svg) BIOS

>**Aucun bios n'est requis.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Extensions supportées

Les roms doivent avoir l'extension :

* .pak

### Emplacement

Placez les roms comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 openbor
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.pak**

## ![](/emulators/advanced-configuration.png) Configuration avancée de l'émulateur

>**Ce core n'a pas d'option.**
{.is-success}

## ![](/emulators/external-links.png) Liens externes

* **Code source utilisé** : [https://gitlab.com/Bkg2k/openbor/](https://gitlab.com/Bkg2k/openbor/)
* **Site officiel de la Senile Team** : [https://www.senileteam.com/](https://www.senileteam.com/)
* **Communauté officielle** : [http://www.chronocrash.com/forum/](http://www.chronocrash.com/forum/)