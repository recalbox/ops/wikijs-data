---
title: Dumpez vos propres jeux rapidement
description: 
published: true
date: 2023-08-13T23:54:50.189Z
tags: ps1, dump, jeux
editor: markdown
dateCreated: 2021-05-30T23:35:27.163Z
---

Vous avez vos propres jeux physiques et souhaitez en créer une copie (dump) pour pouvoir y jouer sur Recalbox ? C'est quelque chose de possible.

Le tutoriel suivant vous permet de créer une copie de sauvegarde de vos jeux au format `.BIN` + `.CUE`.

## Pré-requis

Il vous faudra quelques petits pré-requis avant de vous y mettre :

* Le logiciel [ImgBurn](https://www.imgburn.com/) - la traduction française est fourni séparément et son installation est expliqué dans la liste des langues disponibles sur la page de téléchargement.
* Un emplacement de sauvegarde.

## Procédure

* Mettez votre jeu dans votre lecteur CD.
* Ouvrez ImgBurn.
* Choisissez `Créer une image à partir d'un disque` et pointer le lecteur de CD avec votre disque original.
* Choisissez ensuite où mettre les fichiers `.BIN` + `.CUE` sur votre SSD ou disque dur ou carte SD.

>L'extension de fichier défini par défaut est le `.ISO`, faites attention de le changer pour obtenir du `.BIN`.
{.is-warning}

* Pour la vitesse de copie, si vous avez la possibilité de la régler, essayez au maximum de rester en 2X (au-dessus, il est possible que le lecteur lise trop vite certaines données ou ne corrige pas à temps si nécessaire de relire un secteur du disque).

## Remarques

* Les disques ont plus de 15-20 ans, les erreurs de lecture, c'est fort possible que cela arrive (un CD n'est pas éternel, il peut être rayé, un peu plus opaque dans le temps à certains endroits, ou des traces divers par l'utilisateur ou sa manipulation dans le temps - traces de doigts, poussières, autres).
* Après, avec les jeux européens, surtout les plus populaires et à partir de 1997, Sony va implanter des protections successives (anti-copie, anti-relecture sur ISO, anti-puce etc), normalement il faut du matériel spécialisé pour extraire correctement les données et le processus peut être long pour les particuliers ou les personnes non-sépcialisées.
* Chez Redump, une solution possible a été fournie avec des fichiers SBI (SubChannel Information), qui sont en fait des decriptions des pistes cachées et les protections sur les disques. Les proposer reste légal, car ils ne contiennent pas de code direct venant du CD, mais uniquement des informations séparées (comme un patch).
* Il est possible de récupérer ces fichiers SBI sur les fiches de chaque disque décrit chez Redump, dans la section SBI.
* Voici le catalogue de tous les jeux protégés sur SONY PlayStation http://redump.org/discs/libcrypt/2/

## Crédits

* Zet_sensei sur [Discord](https://discord.com/channels/438014472049917953/439141063425392640/848505646391361577).