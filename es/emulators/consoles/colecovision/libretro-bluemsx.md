---
title: Libretro blueMSX
description: 
published: true
date: 2024-06-30T15:53:44.563Z
tags: libretro, bluemsx, colecovision
editor: markdown
dateCreated: 2023-09-06T09:33:32.423Z
---

## ![](/emulators/license.svg) Licencia

El core posee la siguiente licencia [**GPLv2**](https://github.com/libretro/blueMSX-libretro/blob/master/license.txt).

## ![](/emulators/compatibility.png) Compatibilidad

 | RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Funcionalidades

| Funcionalidad | ¿Soportada? |
| :---: | :---: |
| Reinicio | ✔ |
| Capturas de pantalla | ✔ |
| Opciones del core | ✔ |
| RetroAchievements | ✔ |
| Controles | ✔ |
| Remapping | ✔ |
| Controles del disco | ✔ |

## ![](/emulators/bios.svg) BIOS

### Lista de BIOS obligatorias

| Nombre del fichero | Descripción | MD5 | ¿Preinstalada? |
| :---: | :---: | :---: | :---: |
| czz50-1.rom | Requis | 1de922acdd742d31349c2801e9768c35 | ✅  |
| czz50-2.rom | Requis | 72b089dc55b7fe7ffb5028f365e8c045 | ✅  |
| coleco.rom | Requis | 2c66f5911e5b42b8ebe113403548eee7 | ✅  |
| coleco.rom | Requis | 2c66f5911e5b42b8ebe113403548eee7 | ✅  |
| SVI603.ROM | Requis | c60a2e85572c0ccb69505a7646d5c1b6 | ✅  |

### Emplazamiento

Disponed vuestras bios como en la siguiente imagen:

┣ 📁 recalbox  
┃ ┣ 📁 share  
┃ ┃ ┣ 📁 bios  
┃ ┃ ┃ ┣ 📁 Machines  
┃ ┃ ┃ ┃ ┣ 📁 COL - Bit Corporation Dina
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **czz50-1.rom**
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **czz50-2.rom**
┃ ┃ ┃ ┃ ┣ 📁 COL - ColecoVision with Opcode Memory Extension
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **coleco.rom**
┃ ┃ ┃ ┃ ┣ 📁 COL - ColecoVision
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **coleco.rom**
┃ ┃ ┃ ┃ ┣ 📁 COL - Spectravideo SVI-603 Coleco
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **SVI603.ROM**

## ![](/emulators/roms.png) Roms

### Extensiones de roms soportadas

Las roms deben tener las extensiones siguientes:

* .col

### Emplazamiento

Disponed vuestros juegos como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 colecovision
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.col**

>Se aconsejan las roms del projecto **No-Intro**.
{.is-success}

>Para más información, consultad [este tutorial](./../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

>Para conservar las configuraciones personalizadas cuando se produce una actualización del sistema Recalbox, os aconsejamos utilizar las [Sobrecargas de configuración](./../../../advanced-usage/configuration-override).
{.is-info}

### Acceder a las opciones

Podéis configurar las opciones del core de dos formas diferentes.

* Via el Menú RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via el fichero `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Opciones del core

| Opción | Valor de la opción | Variable (para la sobrecarga) | Valores posibles |
| --- | --- | --- | --- |
| Machine Type (Restart) | `Auto` ✅ / `MSX` / `MSXturboR` / `MSX2` / `MSX2+` / `SEGA - SG-1000` / `SEGA - SC-3000` / `SEGA - SF-7000` / `SVI - Spectravideo SVI-318` / `SVI - Spectravideo SVI-328` / `SVI - Spectravideo SVI-328 MK2` / `ColecoVision` / `Coleco (Spectravideo SVI-603)` | `bluemsx_msxtype` | `Auto` / `MSX` / `MSXturboR` / `MSX2` / `MSX2+` / `SEGA - SG-1000` / `SEGA - SC-3000` / `SEGA - SF-7000` / `SVI - Spectravideo SVI-318` / `SVI - Spectravideo SVI-328` / `SVI - Spectravideo SVI-328 MK2` / `ColecoVision` / `Coleco (Spectravideo SVI-603)` |
| Crop Overscan | `Désactivé` ✅ / `Activé` / `MSX2` | `bluemsx_overscan` | `disabled` / `enabled` / `MSX2` |
| VDP Sync Type (Restart) | `Auto` / `50Hz` / `60Hz` ✅ | `bluemsx_vdp_synctype` | `auto` / `50 Hz` / `60 Hz` |
| No Sprite Limit | `Désactivé` ✅ / `Activé` | `bluemsx_nospritelimits` | `disabled` / `enabled` |
| Sound YM2413 Enable (Restart) | `Activé` ✅ / `Désactivé` | `bluemsx_ym2413_enable` | `enabled` / `disabled` |
| Cart Mapper Type (Restart) | `Auto` ✅ / `Normal` / `mirrored` / `basic` / `0x4000` / `0xC000` / `ascii8` / `ascii8sram` / `ascii16` / `ascii16sram` / `ascii16nf` / `konami4` / `konami4nf` / `konami5` / `konamisynth` / `korean80` / `korean90` / `korean126` / `MegaFlashRomScc` / `MegaFlashRomSccPlus` / `msxdos2` / `scc` / `sccexpanded` / `sccmirrored` / `sccplus` / `snatcher` / `sdsnatcher` / `SegaBasic` / `SG1000` / `SG1000Castle` / `SG1000RamA` / `SG1000RamB` / `SC3000` | `bluemsx_cartmapper` | `Auto` / `Normal` / `mirrored` / `basic` / `0x4000` / `0xC000` / `ascii8` / `ascii8sram` / `ascii16` / `ascii16sram` / `ascii16nf` / `konami4` / `konami4nf` / `konami5` / `konamisynth` / `korean80` / `korean90` / `korean126` / `MegaFlashRomScc` / `MegaFlashRomSccPlus` / `msxdos2` / `scc` / `sccexpanded` / `sccmirrored` / `sccplus` / `snatcher` / `sdsnatcher` / `SegaBasic` / `SG1000` / `SG1000Castle` / `SG1000RamA` / `SG1000RamB` / `SC3000` |
| Auto Rewind Cassette | `Activé` ✅ / `Désactivé` | `bluemsx_auto_rewind_cas` | `enabled` / `disabled` |

## ![](/emulators/external-links.png) Enlaces externos

* **Código fuente utilizado** : [https://github.com/libretro/blueMSX-libretro/](https://github.com/libretro/blueMSX-libretro/)
* **Documentación Libretro** : [https://docs.libretro.com/library/bluemsx/](https://docs.libretro.com/library/bluemsx/)