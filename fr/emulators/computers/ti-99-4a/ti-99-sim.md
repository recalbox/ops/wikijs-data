---
title: TI-99/Sim
description: 
published: true
date: 2024-06-23T11:54:47.255Z
tags: ti-99/4a, ti-99/sim, 8.0+
editor: markdown
dateCreated: 2021-08-21T18:10:38.233Z
---



## ![](/emulators/license.svg) Licence

Ce core est sous licence [**GPL-2.0**](https://gitlab.com/recalbox/packages/standalone/ti99sim/-/blob/master/doc/COPYING).

## ![](/emulators/compatibility.png) Compatibilité

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Fonctionnalités

| Fonctionnalité | Supporté |
| :---: | :---: |


## ![](/emulators/bios.svg) BIOS

### Liste des bios obligatoires

| Nom de fichier | Description | MD5 | Fourni |
| :---: | :---: | :---: | :---: |
| TI-994A.ctg | System cartridge | 412ecbf991edcb68edd0e76c2caa4a59 | ❌ |

### Emplacement

Placez les bios comme ceci : 

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 ti994a
┃ ┃ ┃ ┃ ┣ 🗒  **TI-994A.ctg**

### Liste des bios optionnels

| Nom de fichier | Description | MD5 | Fourni |
| :---: | :---: | :---: | :---: |
| spchrom.bin | Speech synthesis | 7adcaf64272248f7a7161cfc02fd5b3f | ❌ |
| ti-disk.ctg | Disk Operating System | 04714f43347cefb2a051a77116344b3f | ❌ |

### Emplacement

Placez les bios comme ceci : 

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 ti994a
┃ ┃ ┃ ┃ ┣ 🗒  **spchrom.bin**
┃ ┃ ┃ ┃ ┣ 🗒  **ti-disk.ctg**

## ![](/emulators/roms.png) Roms

### Extensions supportées

La rom doit avoir l'extension :

* .ctg

### Emplacement

Placez les roms comme ceci : 

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 ti994a
┃ ┃ ┃ ┃ ┣ 🗒  **jeu.ctg**

>Pour plus d'information sur les roms, rendez-vous sur [ce tutoriel](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuration avancée de l'émulateur

>Pour pouvoir conserver vos configurations personnalisées lors d'une mise à jour, nous vous conseillons d'utiliser notre fonctionnalité [Surcharges de configuration](./../../../advanced-usage/configuration-override).
{.is-info}

## ![](/emulators/external-links.png) Liens externes

* **Code source utilisé** : [https://gitlab.com/recalbox/packages/standalone/ti99sim/](https://gitlab.com/recalbox/packages/standalone/ti99sim/)
* **Site web officiel** : [https://www.mrousseau.org/programs/ti99sim/](https://www.mrousseau.org/programs/ti99sim/)