---
title: Jugar mediante una PSP con el mando de juegos FuSa
description: 
published: true
date: 2023-09-19T09:28:01.622Z
tags: psp, fusa, mando, controlador
editor: markdown
dateCreated: 2023-09-19T09:28:01.622Z
---

Con firmwares personalizados como **pro CFW** y **ME CFW**, pero también gracias a la flexibilidad natural de la PSP y al hardware semiabierto, con la ayuda del controlador FuSa (una aplicación homebrew para la PSP), puedes conseguir uno de los mejores controladores de SNES para tu Raspberry Pi.

## ¿Qué necesitamos?

* Una PSP modelo 1xxx, 2xxx o 3xxx (preferiblemente modelos 2xxx y 3xxx)
* Una Memory Stick Pro Duo de 1 GB o superior
* Un cable mini USB - USB A
* Firmware personalizado (Pro o ME / LME)
* Un PC o su Raspberry Pi para transferir archivos a través de la red
* La aplicación [FuSa controller] (http://foosa.do.am/load/fusa_gamepad_version_03/3-1-0-33)

## Para empezar

Descarga el zip del gamepad FuSa, luego enciende tu PSP con la tarjeta de memoria en la consola, y si es un modelo 3000, lanza tu CFW (los modelos más antiguos no necesitan lanzarlo ya que estos modelos ya arrancan con el CFW cargado), conecta el cable USB a tu PSP y PC, descomprime el zip en `/PSP/GAMES/` y desconéctala de forma segura de tu PC.

Ahora lanza la aplicación y conéctala a tu Raspberry Pi, la pantalla de tu PSP debería estar apagada pero el LED de encendido debería permanecer normalmente encendido.

## Disposición recomendada

Para una experiencia consistente dentro y fuera de EmulationStation, incluyendo en Kodi y todos los núcleos RetroArch, y debido a que todos los mandos de Playstation en Japón utilizan la misma disposición que un mando de SNES pero con diferentes símbolos, sugiero utilizar esta configuración de botones más mi pequeña sugerencia para los botones de volumen y el botón home en Kodi.

PSP layout](/tutorials/controllers/use-psp-as-controller/psp-config.png)

## Notas

* PSP Street no es compatible.
* Si tu PSP está en buenas condiciones, es mucho mejor que un mando genérico o un gamepad de 360. Los botones son muy sensibles y el D-Pad es ideal para juegos retro.
* La Raspberry Pi NO es capaz de cargar completamente una PSP.
* Los botones de volumen no funcionan en EmulationStation o RetroArch.
* Puedes acceder a una configuración más detallada en los archivos .ini de FuSa.