---
title: 3DO
description: 
published: true
date: 2024-07-26T07:43:39.780Z
tags: panasonic, 3do, consolas
editor: markdown
dateCreated: 2023-09-03T15:35:08.497Z
---

![](/emulators/consoles/3do.svg){.align-center}

## Datos técnicos

* **Fabricante:** Panasonic, Sanyo, GoldStar
* **Año de lanzamiento:** 1993
* **Sistema operativo:** Sistema operativo multitarea de 32 bits 
* **Procesador:** RISC de 32 bits (ARM60) a 12,5 MHz
* **RAM:** 2 MB
* **VRAM**:1 MB
* **ROM :** 1 MB
* **Procesador gráfico:** 2 coprocesadores de vídeo acelerados a 25 MHz
* **Chip de sonido:** Sonido estéreo de 16 bits con frecuencia de muestreo de sonido de 44,1 KHz
* **Resolución:** 640x480 (16,7 millones de colores)

## Presentación

La **3DO Interactive Multiplayer** fue originalmente el nombre dado a la primera videoconsola desarrollada por Panasonic según el estándar **3DO** establecido por The 3DO Company. Este modelo y los posteriores se suelen abreviar como **3DO**.

En 1993, Trip Hawkins dejó Electronic Arts para fundar 3DO (Three Dimensional Objects). Su objetivo era crear la tecnología para una consola de 32 bits y vender la licencia a diversos fabricantes, imponiendo así un estándar.

La tecnología 3DO fue concebida sobre el papel por Dave Needle y Robert J. Mical, que habían formado parte de los equipos de desarrollo del Commodore Amiga y el Atari Lynx: debido al gran número de antiguos ingenieros de Commodore, la empresa recibió en sus inicios el apodo de The Ex-Commodore West entre los profesionales de la informática.

Los primeros en suscribir la licencia fueron Matsushita y Goldstar, seguidos de Samsung, AT&T y Toshiba. La primera 3DO comercializada fue la Panasonic FZ-1, en abril de 1993, seguida de la Goldstar GDO y del modelo Sanyo. Creative fabricó una tarjeta ISA para PC: esta tarjeta, llamada 3DO Blaster, permitía utilizar medios 3DO en un PC con Windows 3.1. En el CES de invierno, a finales de 1993, sólo se mostró un prototipo de la 3DO de AT&T.

Debido a la época de los videojuegos, en la que lo "multimedia" estaba de moda, se creó una gran variedad de títulos para esta consola. Además de juegos, había títulos educativos, CD-ROM enciclopédicos, títulos pornográficos y mucho más.

En 1994, las ventas de los distintos modelos no alcanzaron los objetivos y Trip Hawkins vendió sus acciones de la empresa a Matsushita. El sistema 3DO se dejó de fabricar en 1995. El sistema 3DO M2 de 64 bits, más avanzado, que debía sustituir al 3DO estándar, no salió al mercado, a pesar de las expectativas. La empresa 3DO vendió todos los derechos del estándar M2 a Matsushita, que lo utilizó en sistemas arcade y expositores interactivos.

Tras el fracaso de su hardware, 3DO se recicló como editora de juegos. A pesar de algunos éxitos, como Meridian 59 y Army Men, las malas decisiones repercutieron en la producción y, en 2002, aunque la facturación de 3DO ascendió a 53,7 millones de dólares, la empresa registró pérdidas de 47,3 millones y se declaró en quiebra. 

## Emuladores

[Libretro Opera](libretro-opera)
