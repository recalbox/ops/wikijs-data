---
title: Libretro UAE4ARM
description: 
published: true
date: 2024-06-23T11:28:25.086Z
tags: libretro, amiga-cdtv, amiga, uae4arm, cdtv, 8.0+
editor: markdown
dateCreated: 2021-07-29T15:19:38.261Z
---



## ![](/emulators/license.svg) Licence

Ce core est sous licence **GPLv2**.

## ![](/emulators/compatibility.png) Compatibilité

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ❌ | ✅ |

## ![](/emulators/features.png) Fonctionnalités

| Fonctionnalité | Supporté |
| :---: | :---: |
| Redémarrage | ✔ |
| Captures d'écran | ✔ |
| Sauvegardes | ✔ |
| Sauvegardes instantanées | ✔ |
| Rembobinage | ✔ |
| Options du core | ✔ |
| Controls | ✔ |
| Remapping | ✔ |
| Multi-Mouse | ✔ |
| Contrôle du disque | ✔ |
| LEDs | ✔ |

## ![](/emulators/bios.svg) BIOS

### Liste des bios obligatoires

Vous trouverez les bios avec le nom indiqué dans la colonne **Description** qu'il vous faudra renommer avec le nom indiqué dans la colonne **Nom de fichier**.

| Nom de fichier | Description | MD5 | Fourni |
| :---: | :---: | :---: | :---: |
| kick34005.CDTV | CDTV Extended-ROM v1.0 (1991)(Commodore)(CDTV)\[!\] | 89da1838a24460e4b93f4f0c5d92d48d d98112f18792ee3714df16a6eb421b89 d1145ab3a0f89340f94c9e734762c198 | ❌ |

### Emplacement

Placez les BIOS comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 🗒 **kick34005.CDTV**

## ![](/emulators/roms.png) Isos

### Extensions supportées

Les isos doivent avoir les extensions suivantes :

* .bin/.cue
* .iso
* .mds/.mdf
* .img/.ccd
* .nrg
* .chd
* .m3u
* .uae

### Emplacement

Placez les isos comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 cd32
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.cue**
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.bin**

>Les isos au format **Redump** sont vivement conseillés.
{.is-success}

>Pour plus d'information sur les isos, rendez-vous sur [ce tutoriel](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuration avancée de l'émulateur

>Pour pouvoir conserver vos configurations personnalisées lors d'une mise à jour, nous vous conseillons d'utiliser notre fonctionnalité [Surcharges de configuration](./../../../advanced-usage/configuration-override).
{.is-info}

### Accéder aux options

Vous pouvez configurer diverses options de deux façons différentes.

* Via le menu RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via le fichier `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Options du core

| Options | Valeur des options | Variable (pour surcharge) | Valeurs des variables |
| --- | --- | --- | --- |
| Model | `Auto` ✅ / `A500` / `A600` / `A1200` / `CDTV` / `CD32` | `uae4arm_model` | `Auto` / `A500` / `A600` / `A1200` / `CDTV` / `CD32` |
| Fast Mem | `None` ✅ / `1 MB` / `2 MB` / `4 MB` / `8 MB` | `uae4arm_fastmem` | `None` / `1 MB` / `2 MB` / `4 MB` / `8 MB` |
| Internal resolution | `640x270` ✅ / `320x240` / `320x256` / `320x262` / `640x240` / `640x256` / `640x262` / `640x270` / `768x270` | `uae4arm_resolution` | `640x270` / `320x240` / `320x256` / `320x262` / `640x240` / `640x256` / `640x262` / `640x270` / `768x270` |
| Leds on screen | `Activé` ✅ / `Désactivé` | `uae4arm_leds_on_screen` | `enabled` / `disabled` |
| Floppy speed | `100` ✅ / `200` / `400` / `800` | `uae4arm_floppy_speed` | `100` / `200` / `400` / `800` |
| Line doubling (de-interlace) | `Désactivé` ✅ / `Activé` | `uae4arm_linedoubling` | `disabled` / `enabled` |
| whdload mode | `files` ✅ / `hdfs` | `uae4arm_whdloadmode` | `files` / `hdfs` |
| fast copper | `Désactivé` ✅ / `Activé` | `uae4arm_fastcopper` | `disabled` / `enabled` |

## ![](/emulators/external-links.png) Liens externes

* **Code source utilisé** : [https://github.com/Chips-fr/uae4arm-rpi/](https://github.com/Chips-fr/uae4arm-rpi/)
* **Documentation Libretro** : -