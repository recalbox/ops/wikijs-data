---
title: Libretro uzem
description: 
published: true
date: 2024-06-23T00:16:03.668Z
tags: libretro, uzebox, uzem
editor: markdown
dateCreated: 2021-05-21T08:28:56.440Z
---

**Libretro Uzem** est un émulateur officiel de l'Uzebox (une console de jeu open source 8 bits rétro-minimaliste). L'Uzebox est un système minimal basé sur un microcontrôleur AVR ATmega644.

## ![](/emulators/license.svg) Licence

Ce core est sous licence [**GPLv3**](https://github.com/Uzebox/uzebox/blob/master/gpl-3.0.txt).

## ![](/emulators/compatibility.png) Compatibilité

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Fonctionnalités

| Fonctionnalité | Supporté |
| :---: | :---: |
| Sauvegardes instantanées | ✔ |
| Rembobinage | ✔ |

## ![](/emulators/bios.svg) BIOS

>**Aucun bios n'est requis.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Extensions supportées

Les roms doivent avoir l'extension :

* .uze
* .zip
* .7z

Les fichiers contenus dans les .zip/.7z doivent correspondre aux extensions citées précédemment.

### Emplacement

Placez les roms comme ceci : 

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 uzebox
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.zip**

## ![](/emulators/advanced-configuration.png) Configuration avancée de l'émulateur

>**Ce core n'a pas d'option.**
{.is-success}

## ![](/emulators/external-links.png) Liens externes

* **Code source utilisé** : [https://github.com/libretro/libretro-uzem/](https://github.com/libretro/libretro-uzem)
* **Code source officiel** : [https://github.com/Uzebox/uzebox/](https://github.com/Uzebox/uzebox)
* **Site officiel** : [http://belogic.com/uzebox/index.asp](http://belogic.com/uzebox/index.asp)
* **Forum officiel** : [http://uzebox.org/forums/](http://uzebox.org/forums/)
* **Wiki officiel** : [http://uzebox.org/wiki/index.php?title=Hello_World](http://uzebox.org/wiki/index.php?title=Hello_World)