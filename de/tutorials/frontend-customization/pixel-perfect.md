---
title: Integer Scale (Pixel Perfect)
description: 
published: true
date: 2022-06-04T13:49:31.525Z
tags: integer, scale, pixel perfect, einstellung
editor: markdown
dateCreated: 2022-06-04T13:49:31.525Z
---

## Zweck

Aktiviere den "Pixel Perfect"-Bildskalierungsmodus, um eine exakte Darstellung der Spiele auf einem modernen Bildschirm zu erhalten..

## Schritte

* Rufe das Hauptmenü von EmulationStation auf, indem Du die Taste `START` drückst.
* Gehe zum Abschnitt `SPIELEINSTELLUNGEN`
* Unter `SHADER`, wähle `SCANLINES` oder `RETRO`.
* Aktiviere dann `INTEGER SCALE (PIXEL PERFECT)`.

>In diesem Modus wird das Bild möglicherweise nicht mehr als Vollbild angezeigt.!
{.is-warning}