---
title: 4. 🔧 COMPATIBILITÀ HARDWARE
description: 
published: true
date: 2023-09-01T15:09:00.184Z
tags: 
editor: markdown
dateCreated: 2021-06-19T14:51:13.411Z
---

Nelle pagine seguenti, puoi verificare se il tuo hardware è stato testato ed è supportato da Recalbox, nonché se un sistema specifico o un emulatore è compatibile con il tuo hardware.