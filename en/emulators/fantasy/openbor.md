---
title: OpenBOR
description: 
published: true
date: 2021-08-09T00:02:25.839Z
tags: fantasy, openbor
editor: markdown
dateCreated: 2021-06-30T09:08:36.305Z
---

![](/emulators/fantasy/openbor.svg){.align-center}

## Technical data

* **Manufacturer**: Senile Team, OpenBOR Team and the community
* **Year of release**: 2004

## Presentation

OpenBOR is a side-scrolling game engine based on royalty-free sprites.  
Since its humble beginnings in 2004, it has become the most versatile, easy to use and powerful engine of its kind you will find anywhere.

OpenBOR is optimized for side-scrolling beat em up style games (Double Dragon, Streets of Rage, Final Fight), but easily adapts to any type of game style and functionality you can imagine.

Want to try your hand at creating games? Open one of the already existing modules and tinker - the community is here to help you.  
Ready to make something of your own? Gather some images and let's go! Feeling ambitious? Dive into the built-in scripting engine and graphics suite to create a masterpiece that rivals the wackiest AAA productions. 

OperBOR is the open source suite for Beats of Rage.

In 2004, Senile Team released Beats of Rage, a customizable video game engine, a free DOS beat-'em-up inspired by SEGA's Streets of Rage series and using sprites from SNK Playmore's King of Fighters series.  
With Beats of Rage, users can create their own beat 'em up games - no programming experience required!  
Beats of Rage was first released for DOS in November 2003. It was only announced by word of mouth, but it quickly gained popularity.  
Senile Team quickly released an editor's package allowing anyone interested to create a module for the BOR engine.  
Ports to many other systems (Dreamcast, Linux, Windows, GP32, GP2X, GP2X Wiz, Mac OS X, OpenDingux Dingoo A320 & GCW-Zero) soon followed.  
Beats of Rage is no longer maintained, distributed or supported by Senile Team.

In 2005, Kirby2000 asked Senile Team to open the source code to BOR. They agreed and OpenBOR was born. The development of the engine was continued by the community, and still is to this day.

To learn more, visit the OpenBOR community at ChronoCrash.com. You will also find dozens of already finished game modules to download and play.

## Emulators

[OpenBOR](openbor)