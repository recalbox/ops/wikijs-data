---
title: Connectez votre Recalbox à un CRT avec composite
description: 
published: true
date: 2024-08-25T19:53:34.715Z
tags: crt, composite, écran
editor: markdown
dateCreated: 2021-05-21T08:43:12.538Z
---

Si vous souhaitez connecter votre Recalbox sur un écran CRT, vous aurez besoin d'un câble mini-jack vers RCA comme celui-ci :

![RPi Audio/Video Socket](/tutorials/video/crt/rpi_av_socket.jpg)

>Sur les câbles de caméscopes, la sortie vidéo pourrait être sur la prise audio droite (rouge).
>
>De plus, le type de câble nécessaire n'étant pas vraiment répandu, il est préférable de tester au multimètre que le câble utilisé correspond bien au schéma proposé ci-dessus (une inversion de la masse et de la vidéo sur le jack entraine une image noir & blanc sautillante, signe d'un câble inadapté).
{.is-warning}

## Configuration

### Ficher config.txt

>Ce fichier est remplacé à chaque mise à jour. Vous devrez refaire les manipulations dans ce fichier à chaque fois !
{.is-warning}

* Modifiez le fichier [config.txt](./../../../tutorials/system/modification/configtxt-configuration) en trouvant la ligne suivante (il y en a 2, retenez celle de votre board) :

```ini
dtoverlay=vc4-kms-v3d,cma-512
```

Et remplacez par ceci :

```ini
dtoverlay=vc4-kms-v3d,composite=1
```

* Trouvez la ligne suivante et commentez-là en mettant un '#' devant, comme ceci :

```ini
#hdmi_enable_4kp60=1
```

### Fichier recalbox-user-config.txt

* Modifiez le fichier [recalbox-user-config.txt](./../../../tutorials/system/modification/configtxt-configuration) et ajoutez le `sdtv_mode` supporté (une seule ligne parmi les suivantes) :

```ini
sdtv_mode=0    # NTSC normal
sdtv_mode=1    # NTSC japonais – no pedesta
sdtv_mode=2    # PAL normal
sdtv_mode=3    # PAL brésilien – 525/60 rather than 625/50, different subcarrier
```

* Pour avoir un meilleur son par la sortie composite, activez le pilote audio expérimental pour le Raspberry Pi :

```ini
audio_pwm_mode=2
```

* Ce mode peut créer un ralentissement global de Recalbox. Si c'est le cas, passer la ligne en commentaire et forcer la sortie audio sur le jack :

```ini
#audio_pwm_mode=2
hdmi_drive=1
```

* Finalement, ajoutez `hdmi_ignore_hotplug=1` pour forcer le composite. Votre ficher recalbox-user-config.txt devrait ressembler à ça :

```ini
sdtv_mode=2
hdmi_ignore_hotplug=1
audio_pwm_mode=2
```

## Tout est lent avec le mode composite

`audio_pwm_mode=2` améliore le son mais peut ralentir le système en mode composite, en particulier sur Pi zéro. Dans ce cas, utilisez le mode suivant :

```ini
audio_pwm_mode=0
```