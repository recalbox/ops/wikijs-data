---
title: Libretro TGBDual
description: 
published: true
date: 2024-06-23T11:58:24.817Z
tags: libretro, color, gameboy, tgbdual, gbc
editor: markdown
dateCreated: 2021-05-21T08:30:39.124Z
---

**Libretro Tgbdual** est un émulateur qui permet d'exécuter des jeux Game Boy avec prise en charge du câble de liaison de jeu.

## ![](/emulators/license.svg) Licence

Ce core est sous licence [**GPLv2**](https://github.com/libretro/tgbdual-libretro/blob/master/docs/COPYING-2.0.txt).

## ![](/emulators/compatibility.png) Compatibilité

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Fonctionnalités

| Fonctionnalité | Supporté |
| :---: | :---: |
| Redémarrage | ✔ |
| Captures d'écran | ✔ |
| Sauvegardes | ✔ |
| Sauvegardes instantanées | ✔ |
| Rembobinage | ✔ |
| Netplay | ✔ |
| Options du core | ✔ |
| Controls | ✔ |
| Remapping | ✔ |
| Softpatching | ✔ |

## ![](/emulators/bios.svg) BIOS

>**Aucun bios n'est requis.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Extensions supportées

La rom doit avoir l'extension :

* .bin
* .gbc
* .sgb
* .zip
* .7z

Ce système supporte les roms compressées au format .zip/.7z. Attention toutefois, il ne s'agit que d'une archive.

Les fichiers contenus dans les .zip/.7z doivent correspondre aux extensions citées précédemment.
De plus, chaque fichier .zip/.7z ne doit contenir qu'une seule rom compressée.

### Emplacement

Placez les roms comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 gbc
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.zip**

>Les roms au format **No-Intro** sont vivement conseillées.
{.is-success}

>Pour plus d'information sur les roms, rendez-vous sur [ce tutoriel](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuration avancée de l'émulateur

>Pour pouvoir conserver vos configurations personnalisées lors d'une mise à jour, nous vous conseillons d'utiliser notre fonctionnalité [Surcharges de configuration](./../../../advanced-usage/configuration-override).
{.is-info}

### Accéder aux options

Vous pouvez configurer diverses options de deux façons différentes.

* Via le menu RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via le fichier `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Options du core

| Options | Valeur des options | Variable (pour surcharge) | Valeurs des variables |
| --- | --- | --- | --- |
| Link cable emulation (reload) | `Désactivé` ✅ / `Activé` | `tgbdual_gblink_enable` | `disabled` / `enabled` |
| Screen layout | `left-right` ✅ / `top-down` | `tgbdual_screen_placement` | `left-right` / `top-down` |
| Switch player screens | `normal` ✅ / `switched` | `tgbdual_switch_screens` | `normal` / `switched` |
| Show player screens | `both players` ✅ / `player 1 only` / `player 2 only` | `tgbdual_single_screen_mp` | `both players` / `player 1 only` / `player 2 only` |
| Audio output | `Game Boy #1` ✅ / `Game Boy #2` | `tgbdual_audoi_output` | `Game Boy #1` / `Game Boy #2` |

## ![](/emulators/external-links.png) Liens externes

* **Code source utilisé** : [https://github.com/libretro/tgbdual-libretro/](https://github.com/libretro/tgbdual-libretro)
* **Documentation Libretro** : [https://docs.libretro.com/library/tgb\_dual/](https://docs.libretro.com/library/tgb_dual/)
* **Code source officiel** : [http://shinh.skr.jp/tgbdualsdl/](http://shinh.skr.jp/tgbdualsdl/)
* **Site officiel** : [http://gigo.retrogames.com/download.html\#tgb-dual](http://shinh.skr.jp/tgbdualsdl/)