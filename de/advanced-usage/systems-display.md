---
title: Darstellung der Systeme
description: Verwendung und Anpassung der Systemdarstellung. (Recalbox v6.1 und höher)
published: true
date: 2021-06-30T09:22:38.002Z
tags: 
editor: markdown
dateCreated: 2021-06-30T09:22:38.002Z
---

## Wie die Darstellung funktioniert

Die Anzeige der Systeme im Recalbox-Menü wird über die Datei `es_systems.cfg` geregelt. Diese Datei befindet sich im Ordner `/recalbox/share_init/system/.emulationstation/es_systems.cfg`.

Sie enthält die Namen der verschiedenen Systeme, die von deiner Version von Recalbox unterstützt werden. Sie ist wie folgt aufgebaut:

```xml
<?xml version="1.0"?>
<systemList>
  <system>
    <fullname>Panasonic 3DO</fullname>
    <name>3do</name>
    <path>/recalbox/share/roms/3do</path>
    <extension>.iso .ISO .cue .CUE .chd .CHD</extension>
    <command>python /usr/lib/python2.7/site-packages/configgen/emulatorlauncher.pyc %CONTROLLERSCONFIG% -system %SYSTEM% -rom %ROM% -emulator %EMULATOR% -core %CORE% -ratio %RATIO% %NETPLAY%</command>
    <platform>3do</platform>
    <theme>3do</theme>
    <emulators>
      <emulator name="libretro">
        <cores>
          <core>4do</core>
        </cores>
      </emulator>
    </emulators>
  </system>
  <system>
      [. . .]
  </system>
  [. . .]
</systemList>
```

Die Anzeige der Systeme erfolgt in der Reihenfolge, in der sie in dieser Datei aufgeführt sind. Sie enthält auch die Konfiguration dieser Systeme.

## Ändern der Anzeigereihenfolge

>Du solltest die ursprüngliche Datei `es_systems.cfg` (die sich im Verzeichnis `share_init` befindet) **NICHT VERÄNDERN**. Wenn es später Probleme mit den von dir vorgenommenen Änderungen gibt, ist diese Datei die einzige Quelle, um Recalbox wieder zum Laufen zu bringen.
{.is-warning}

Das Ändern der Anzeigereihenfolge sollte nur über die Datei `es_systems.cfg` im Verzeichnis `/recalbox/share/system/.emulationstation/` erfolgen

Anfänglich ist diese Datei nicht vorhanden. Du musst entweder die Originaldatei kopieren oder eine neue Datei erstellen. Sobald das geschehen ist, kannst du die Systeme in eine beliebige Reihenfolge bringen. Die Systemkonfiguration wird immer noch aus der ursprünglichen Datei `es_systems.cfg` übernommen, aber die Reihenfolge der Systeme wird entsprechend der Reihenfolge in der neuen Datei festgelegt.

Wenn in der neuen Datei ein System fehlt oder falsch eingetragen ist, hat die Originaldatei Vorrang. Für die neue Datei gibt es nur 2 Werte, die zwingend erforderlich sind: "**fullname**" und "**platform**", alle anderen sind optional. Die Datei sollte mindestens wie folgt aufgebaut sein:

```xml
<?xml version="1.0"?>
<systemList>
  <system>
    <fullname>Nintendo Entertainment System</fullname>
    <platform>nes</platform>
  </system>
  <system>
    <fullname>Family Computer Disk System</fullname>
    <platform>fds</platform>
  </system>
  <system>
    <fullname>Super Nintendo Entertainment System</fullname>
    <platform>snes</platform>
  </system>
  <system>
    <fullname>Satellaview</fullname>
    <platform>satellaview</platform>
  </system>
  [. . .]
</systemList>
```

## Hinzufügen eines "benutzerdefinierten" Systems

>Du solltest die ursprüngliche Datei `es_systems.cfg` (die sich im Verzeichnis `share_init` befindet) **NICHT VERÄNDERN**. Wenn es später Probleme mit den von dir vorgenommenen Änderungen gibt, ist diese Datei die einzige Quelle, um Recalbox wieder zum Laufen zu bringen.
{.is-warning}

Dadurch wird kein neuer Emulator zu Recalbox hinzugefügt, sondern ein neuer Systemeintrag in das Auswahlmenü aufgenommen.

Es ist möglich, die Änderung der Systemreihenfolge und das Hinzufügen eines oder mehrerer benutzerdefinierter Systeme zu kombinieren.

Wie beim Ändern der Systemreihenfolge sollte das Hinzufügen eines benutzerdefinierten Systems nur über die Datei `es_systems.cfg` im folgenden Verzeichnis `/recalbox/share/system/.emulationstation/` erfolgen.

Anfänglich ist diese Datei nicht vorhanden. Du musst entweder die Originaldatei kopieren oder eine neue Datei erstellen. Sobald das geschehen ist, kannst du ein "neue Systeme" hinzufügen.

Wenn in der neuen Datei ein System falsch eingetragen ist, hat die Originaldatei Vorrang. Für die neue Datei sind alle Werte obligatorisch. Um also ein neues System zu erstellen, ist es am einfachsten, von einem bestehenden System (und entsprechend den ROMs, die du einbinden willst) auszugehen und nur das zu ändern, was unbedingt notwendig ist:

- _**« fullname »**_: Hier wird der Name des neuen Systems angegeben.
- _**« path »**_: Hier wird das Verzeichnis angegeben, das die ROMs des neuen Systems enthält.
- _**« theme »**_: Hier wird angegeben, welches Theme verwendet werden soll. Es ist notwendig, vorher das neue Theme zu erstellen (Logo, Hintergrund, etc.)

**Alle anderen Einträge müssen unverändert bleiben.** 

Hier ist ein Beispiel, wie du ein SNES-basiertes System so hinzufügst, dass es nur übersetzte Roms enthält:

```xml
<?xml version="1.0"?>
<systemList>
  <system>
    <fullname>Super Nintendo Fan Trad</fullname>
    <name>snes</name>
    <path>/recalbox/share/roms/snestrad</path>
    <extension>.smc .sfc .mgd .zip .7z</extension>
    <command>python /usr/lib/python2.7/site-packages/configgen/emulatorlauncher.pyc %CONTROLLERSCONFIG% -system %SYSTEM% -rom %ROM% -emulator %EMULATOR% -core %CORE% -ratio %RATIO% %NETPLAY%</command>
    <platform>snes</platform>
    <theme>snestrad</theme>
    <emulators>
      <emulator name="libretro">
        <cores>
          <core>snes9x2005</core>
          <core>snes9x2010</core>
          <core>snes9x2002</core>
        </cores>
      </emulator>
    </emulators>
  </system>
</systemList>
```