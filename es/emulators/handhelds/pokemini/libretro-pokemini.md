---
title: Libretro PokeMini
description: 
published: true
date: 2024-07-25T22:45:37.348Z
tags: libretro, pokémon, mini, pokémini
editor: markdown
dateCreated: 2023-09-11T14:11:49.545Z
---

**Libretro PokeMini** es un emulador de la console portátil Nintendo Pokémon Mini desarrollado en C y C++.

Ha sido escrito por **JustBurn**.

## ![](/emulators/license.svg) Licencia

El core posee la siguiente licencia [**GPLv3**](https://github.com/libretro/PokeMini/blob/master/LICENSE).

## ![](/emulators/compatibility.png) Compatibilidad

 | RPI0/RPI1 | RPI3 | RPI4/400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | 
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | 

## ![](/emulators/features.png) Funcionalidades

| Funcionalidad | ¿Soportada? |
| :---: | :---: |
| Reinicio | ✔ |
| Grabaciones | ✔ |
| Grabaciones instantáneas | ✔ |
| Rebobinado | ✔ |
| Opciones del core | ✔ |
| Retroachievements | ✔ |
| Controles | ✔ |
| Remapping | ✔ |
| Vibración | ✔ |

## ![](/emulators/bios.svg) BIOS

### Lista de BIOS opcionales

| Nombre del fichero | Descripción | MD5 | ¿Preinstalada? |
| :---: | :---: | :---: | :---: |
| bios.min | BIOS Pokémon Mini | 1e4fb124a3a886865acb574f388c803d | ❌ |

### Emplazamiento

Disponed vuestras bios como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 pokemini
┃ ┃ ┃ ┃ ┣ 🗒 **bios.min**

## ![](/emulators/roms.png) Roms

### Extensiones de roms soportadas

Las roms deben tener las extensiones siguientes:

* .min
* .zip

Este sistema soporta las roms comprimidas en formato .zip/.7z. No obstante, se trata sólo de un archivo.

Los ficheros contenidos en los .zip/.7z deben tener las extensiones citadas previamente.
Además, cada fichero .zip/.7z sólo debe contener un fichero rom al interior.

### Emplazamiento

Disponed vuestras roms como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 pokemini
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.zip**

>Se aconsejan las roms del proyecto **No-Intro**.
{.is-success}

>Para más información sobre las roms, consultad [este tutorial](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

>Para conservar las configuraciones personalizadas cuando se produce una actualización del sistema Recalbox, os aconsejamos utilizar las [Sobrecargas de configuración](./../../../advanced-usage/configuration-override).
{.is-info}

### Acceder a las opciones

Podéis configurar las opciones del core de dos formas diferentes.

* Via el Menú RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via el fichero `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Opciones del core

| Opción | Valor de la opción | Variable (para la sobrecarga) | Valores posibles |
| --- | --- | --- | --- |
| Échelle vidéo (redémarrer) | `1x` / `2x` / `3x` / `4x` ✅ / `5x` / `6x` | `pokemini_video_scale` | `1x` / `2x` / `3x` / `4x` / `5x` / `6x` |
| 60Hz Mode | `Désactivé` / `Activé` ✅ | `pokemini_60hz_mode` | `disabled` / `enabled` |
| Filtre LCD | `Matrice de Points` ✅ / `Lignes de Balayage` / `Aucun` | `pokemini_lcdfilter` | `dotmatrix` / `scanline` / `none` |
| Mode LCD | `Analogique` ✅ / `3 Nuances` / `2 Nuances` | `pokemini_lcdmode` | `analog` / `3shades` / `2shades` |
| Contraste LCD | `0` / `16` / `32` / `48` / `64` ✅ / `80` / `96` | `pokemini_lcdcontrast` | `0` / `16` / `32` / `48` / `64` / `80` / `96` |
| Luminosité de l'écran LCD | `-80` / `-60` / `-40` / `-20` / `0` ✅ / `20` / `40` / `60` / `80` | `pokemini_lcdbright` | `-80` / `-60` / `-40` / `-20` / `0` / `20` / `40` / `60` / `80` |
| Palette | `Défaut` ✅ / `Vieux` / `Noir et Blanc` / `Vert` / `Vert Inversé` / `Rouge` / `Rouge Inversé` / `LCD Bleu` / `Rétro-éclairage LED` / `Pouvoir des Filles` / `Bleu` / `Bleu Inversé` / `Sépia` / `Noir et Blanc Inversé` | `pokemini_palette` | `Default` / `Old` / `Monochrome` / `Green` / `Green Vector` / `Red` / `Red Vector` / `Blue LCD` / `LEDBacklight` / `Girl Power` / `Blue` / `Blue Vector` / `Sepia` / `Monochrome Vector` |
| Filtre Piézo | `Activé` ✅ / `Désactivé` | `pokemini_piezofilter` | `enabled` / `disabled` |
| Low Pass Filter | `Désactivé` ✅ / `Activé` | `pokemini_lowpass_filter` | `disabled` / `enabled` |
| Low Pass Filter Level (%) | `5` / `10` / `15` / `20` / `25` / `30` / `35` / `40` / `45` / `50` / `55` / `60` ✅ / `65` / `70` / `75` / `80` / `85` / `90` / `95` | `pokemini_lowpass_range` | `5` / `10` / `15` / `20` / `25` / `30` / `35` / `40` / `45` / `50` / `55` / `60` / `65` / `70` / `75` / `80` / `85` / `90` / `95` |
| Niveau de tremblement de l'écran | `0` / `1` / `2` / `3` ✅ | `pokemini_screen_shake_lv` | `0` / `1` / `2` / `3` |
| Niveau du rumble du contrôleur | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` ✅ | `pokemini_rumble_lv` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` |
| Période du bouton Turbo | `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` ✅ / `19` / `20` / `21` / `22` / `23` / `24` / `25` / `26` / `27` / `28` / `29` / `30` / `31` / `32` / `33` / `34` / `35` / `36` / `37` / `38` / `39` / `40` / `41` / `42` / `43` / `44` / `45` / `46` / `47` / `48` / `49` / `50` / `51` / `52` / `53` / `54` / `55` / `56` / `57` / `58` / `59` / `60` / `61` / `62` / `63` / `64` / `65` / `66` / `67` / `68` / `69` / `70` / `71` / `72` / `73` / `74` / `75` / `76` / `77` / `78` / `79` / `80` / `81` / `82` / `83` / `84` / `85` / `86` / `87` / `88` / `89` / `90` / `91` / `92` / `93` / `94` / `95` / `96` / `97` / `98` / `99` / `100` / `101` / `102` / `103` / `104` / `105` / `106` / `107` / `108` / `109` / `110` / `111` / `112` / `113` / `114` / `115` / `116` / `117` / `118` / `119` / `120` | `pokemini_turbo_period` | `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `21` / `22` / `23` / `24` / `25` / `26` / `27` / `28` / `29` / `30` / `31` / `32` / `33` / `34` / `35` / `36` / `37` / `38` / `39` / `40` / `41` / `42` / `43` / `44` / `45` / `46` / `47` / `48` / `49` / `50` / `51` / `52` / `53` / `54` / `55` / `56` / `57` / `58` / `59` / `60` / `61` / `62` / `63` / `64` / `65` / `66` / `67` / `68` / `69` / `70` / `71` / `72` / `73` / `74` / `75` / `76` / `77` / `78` / `79` / `80` / `81` / `82` / `83` / `84` / `85` / `86` / `87` / `88` / `89` / `90` / `91` / `92` / `93` / `94` / `95` / `96` / `97` / `98` / `99` / `100` / `101` / `102` / `103` / `104` / `105` / `106` / `107` / `108` / `109` / `110` / `111` / `112` / `113` / `114` / `115` / `116` / `117` / `118` / `119` / `120` |

## ![](/emulators/external-links.png) Enlaces externos

* **Código fuente utilizado** : [https://github.com/libretro/PokeMini/](https://github.com/libretro/PokeMini/)
* **Documentación Libretro** : [https://docs.libretro.com/library/pokemini/](https://docs.libretro.com/library/pokemini/)
* **Código fuente oficial** : [https://sourceforge.net/projects/pokemini/](https://sourceforge.net/projects/pokemini/)
* **Documentación oficial** : [https://sourceforge.net/p/pokemini/wiki/Home/](https://sourceforge.net/p/pokemini/wiki/Home/)