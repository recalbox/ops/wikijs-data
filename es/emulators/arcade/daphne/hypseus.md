---
title: Hypseus
description: 
published: true
date: 2024-07-29T12:06:33.844Z
tags: daphne, singe, hypseus
editor: markdown
dateCreated: 2023-09-05T08:02:12.870Z
---

**Hypseus** para la emulación **de juegos de arcade en laserdisc**, es un fork de Daphne escrito por Jeffrey Clark para aportarle algunas mejoras:

* Actualización del decodificador MPEG2
* Soporte para SDL2
* Etc...

## ![](/emulators/license.svg) Licencia

Este núcleo está bajo la licencia [**GPLv3**](https://github.com/DirtBagXon/hypseus-singe/blob/master/LICENSE).

**Hypseus Singe**, Being Retro Orientated This Humble Emulator Regenerates
Copyright (C) 2021-2024 [DirtBagXon](https://github.com/DirtBagXon/hypseus-singe)

**Hypseus**, Multiple Arcade Laserdisc Emulator
Copyright (C) 2016 [Jeffrey Clark](https://github.com/h0tw1r3)

**Daphne**, the First Ever Multiple Arcade Laserdisc Emulator
Copyright (C) 1999-2013 [Matt Ownby](http://www.daphne-emu.com/site3/statement.php)

## ![](/emulators/compatibility.png) Compatibilidad

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Características

| Característica | Soportado |
| :---: | :---: |

## ![](/emulators/bios.svg) BIOS

>**No se requiere ningún BIOS.**
{.is-success}

## ![](/emulators/roms.png) Roms

### ¿Cómo jugar a juegos de laserdisc en Recalbox?

* Para agregar un juego de laserdisc, necesitas los dos componentes de dicho juego:

  * La imagen del laserdisc original, que contiene los contenidos de video y audio.
  * El archivo ROM, que es el programa del juego.

**La ROM** es un simple archivo ZIP que debe ser copiado en el subdirectorio `roms/daphne/`. Este contiene archivos en `.bin`.

* La imagen del disco láser está compuesta por varios archivos:

  * **Uno o más archivos de video** (`*.m2v`)
  * **Uno o más archivos de audio** (`*.ogg`)
  * **Uno o más archivos de índice** (`*.dat`): pueden ser creados por el emulador en el primer lanzamiento (operación lenta)
  * **Un archivo de marco** (`.txt`): para asociar los números de marco con los archivos de video.

* Todos estos archivos deben ser copiados en un directorio con una extensión `.daphne`, bajo el directorio `.daphne`.
* La primera línea de tu archivo `.txt` es una ruta y debe ser . (solo el carácter punto), modifícala si no es así.
* También puedes crear un archivo con la extensión `.commands` para pasar al emulador parámetros específicos de un juego. Ver más abajo para más detalles.
* El directorio, el archivo de marco y el archivo de comandos deben tener el mismo nombre que el archivo ROM! (solo las extensiones son diferentes).

### Ubicación

Coloca las roms de la siguiente manera:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 **juego.daphne**
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **juego.command** (opcional)
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **juego.txt**
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **juego.dat**
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **juego.m2v**
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **juego.ogg**
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **...**
┃ ┃ ┃ ┃ ┣ 📁 **roms**
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **juego.zip**

## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

>**Este núcleo no tiene opciones.**
{.is-success}

## ![](/emulators/external-links.png) Enlaces externos

* **Código fuente utilizado**: [https://github.com/DirtBagXon/hypseus-singe](https://github.com/DirtBagXon/hypseus-singe)
* **Sitio oficial**: [http://www.daphne-emu.com/site3/index_hi.php](http://www.daphne-emu.com/site3/index_hi.php)
* **FAQ**: [http://www.daphne-emu.com/site3/FAQ_USA.php](http://www.daphne-emu.com/site3/FAQ_USA.php)
* **Wiki del núcleo**: [https://www.daphne-emu.com:9443/mediawiki/index.php/Main_Page]
