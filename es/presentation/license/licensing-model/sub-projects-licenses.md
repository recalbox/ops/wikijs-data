---
title: Licencias para projectos relacionados
description: 
published: true
date: 2023-08-31T14:53:39.874Z
tags: licencia, projectos
editor: markdown
dateCreated: 2023-08-31T14:53:39.874Z
---

## Proyectos software

Recalbox no se compone sólo de su repositorio principal. También mantenemos una serie de otros proyectos de código abierto incluidos en Recalbox o como parte de la infraestructura general de Recalbox.

He aquí un resumen de las licencias de los principales proyectos:

* El [fork Recalbox de EmulationStation](https://gitlab.com/recalbox/recalbox-emulationstation) está licenciado bajo la licencia MIT.
* [El gestor web] de Recalbox (https://gitlab.com/recalbox/recalbox-manager) está licenciado bajo un acuerdo de "todos los derechos reservados".
* La API de Recalbox está licenciada bajo los términos de la licencia MIT.
* La licencia de Recalbox [website](https://gitlab.com/recalbox/www.recalbox.com) es "todos los derechos reservados".

## Obras creativas

También mantenemos y distribuimos [themes](https://gitlab.com/recalbox/recalbox-themes) para Recalbox, que están liberados bajo las siguientes licencias:

| Themes | Licencia |
| :---: | :---: |
| recalbox-goa2 | [CreativeCommons BY-NC-ND 4.0](https://creativecommons.org/licenses/by-nc-nd/4.0/) |
| recalbox-next | [CreativeCommons BY-NC-ND 4.0](https://creativecommons.org/licenses/by-nc-nd/4.0/) |