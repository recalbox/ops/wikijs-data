---
title: Glossary
description: List of terms related to Recalbox.
published: true
date: 2022-05-18T16:08:51.771Z
tags: help, glossary, definition
editor: markdown
dateCreated: 2021-08-03T22:10:53.729Z
---

This section gives the definition of technical terms related to Recalbox, if you have a word you don't understand, this page should help you.

## A

* **Arcade**: The arcade generally refers to video game machines where you insert coins to get credits. Due to their nature (a large number of different machines, where consoles usually have a defined hardware on which a large number of games will run), it is important to understand how to run the associated emulators, as putting files without knowing exactly what you are doing will usually not work. Also, in the context of Recalbox, Neogeo games are emulated in the same way as arcade titles. 
* **API**: Application Programming Interface.

## B

* **Big-Endian**: is the most common way to store [binary](https://techlib.fr/definition/binary.html) files . It places the most significant (or largest) value first, followed by less significant values. For example, the big-endian representation of the [integer](https://techlib.fr/definition/integer.html) number 123 puts the hundreds value (1) first, followed by the tens value (2), then the units value (3) or [123]. 
* **Bios**: Files representing programs read by the original system, required by some emulators to work.
* **Buildroot**: A set of Makefiles and patches that simplifies and automates the process of building a complete, bootable Linux environment for an embedded system, while using cross-compilation to allow the building of multiple target platforms on a single Linux-based development system. Buildroot can automatically create the required cross-compile toolchain, create a root file system, compile a Linux kernel image, and generate a boot loader for the target embedded system, or it can perform any independent combination of these steps. For example, an already installed cross-compiler toolchain can be used independently, while Buildroot creates only the root file system.

## C

* **Changelog**: Text file containing the changes for each version of a software. The [changelog](https://gitlab.com/recalbox/recalbox/raw/master/CHANGELOG.md) and [release-notes](https://gitlab.com/recalbox/recalbox/raw/master/RELEASE-NOTES.md) files are accessible to everyone.
* **CHD**: Arcade game disk image used by some MAME games, stores a compressed version of the entire contents of an arcade machine's hard drive.
* **Cheats**: In the context of Recalbox, cheats allow to reproduce the behavior of some cheating devices of the original systems, allowing to activate or deactivate pre-entered codes modifying the game behavior.
* **Checksum**: A string calculated from the contents of a file, if two files of the same size have the same checksum, they are probably identical. Used mainly for Netplay.
* **Core**: A program loaded by an emulator (usually RetroArch).
* **CRC**: A type of checksum used for Netplay.
* **Cue** _(or .cue)_: Files describing how raw data is distributed on a disk, mainly used for disk-based consoles, with .bin files.

## D

* **Dat** _(or .dat)_: Files used to check batches of roms to make sure they belong to a given set, mainly used for Arcade and Netplay. 
* **Debugging**: Fixing a computer problem due to a bug, i.e. a malfunction of a computer program, which prevents the proper functioning of an application, software, etc. Example: He debugged his Recalbox by restarting it.
* **Demo mode**: A feature of Recalbox, allowing EmulationStation to launch random games after a set amount of inactivity.
* **DragonBlaze**: The name of Recalbox version 6.0.

## E

* **Endianness**: In [Computer Science](https://techlib.fr/definition/computer_science.html), a term that describes how [date](https://techlib.fr/definition/data.html) is stored. Specifically, it defines which end of a multibyte [Data Type](https://techlib.fr/definition/datatype.html) contains the most significant values. The two endian types are big-endian and small-endian.
* **EmulationStation** _(or ES)_: The front-end (or interface to manage and access games) used in Recalbox.
* **Emulator**: A program that mimics the behavior of a defined hardware.

## F

* **Front-end**:

## G

* **Gamelist** _(or gamelist.xml)_: File specific to each emulated system, listing information for each game.
* **Gitlab**: 
* **GPI**:
* **GPIO** _(or General Purpose Input/Output)_: Contacts accessible on a Raspberry Pi, which can be used for different purposes (mainly to attach buttons and other controls in the case of Recalbox).

## H

* **Hash**: Functionality calculating the checksums of the roms for the Netplay.
* **Hotkey**: Button used for key combinations interpreted by the emulator as shortcuts for features.

## I

* **Interface**:
* **Iso** _(or .iso)_: Image files of a physical disk.
* **Issue**: In the case of Gitlab, an issue represents a bug report or a feature request, it is possible to see the issues or to create one by following [this link](https://gitlab.com/recalbox/recalbox/-/issues).

## K

* **Kodi**: Multimedia player included in Recalbox.

## L

* **Libretro**: Cross-platform API used by Retroarch.
* **License**: A license defines what can be done or not with a software, it is possible to read the license of Recalbox [here](https://gitlab.com/recalbox/recalbox/-/blob/master/LICENSE.md), it is also necessary to take into account that Recalbox is provided with other software, which are also subject to their license.
* **Link-Cable**: A cable used to connect two portable systems together in order to use multiplayer features for supported games, some emulators can mimic this behavior.
* **Little Endian**: Stores the least significant value first, followed by increasingly significant values. For example, the number 123 in little-endian notation is [321]. The text [a beautiful](https://techlib.fr/definition/string.html) "ABC" is represented by [CBA].

## M

* **MAME**: Multiple Arcade Machine Emulator is a free, open-source emulator designed to recreate arcade game system hardware in software on modern personal computers and other platforms.
* **MD5**: a type of checksum, used for BIOS.
* **MITM**: _(or Man In The Middle)_: Setting used for Netplay, using a remote server to synchronize games rather than a direct connection, in return for a loss of reliability and speed.
* **Multitap**: Device allowing to connect more controllers than there are controller ports on a console, allowing to have a 4 players mode on systems with only two original controller ports for supported games, some emulators can imitate this behavior.

## N

* **Netplay**: A technology allowing to play in network, in the case of Recalbox, it allows to play retro games online.
* **No-intro**: Name of a group that reference roms, are specialized in cartridge games and have for objective and vision to keep only the dumps closest to the original ones. **Reference romsets** for systems using **cartridges**, mainly **used for Netplay**.
* **NOOBS**: (New Out Of the Box Software) has a name that may sound pejorative, but this utility allows you to install an operating system reliably and quickly from a wide selection. NOOBS is really just a simple operating system installation manager for the Raspberry Pi, it was designed to simplify this task to the maximum by allowing you to install an OS in just a few clicks.

## O

* **OCR** : Optical Character Recognition** (OCR), or ocherization, refers to computer processes for translating images of printed or typed text into text files.
* **Open-source** : The designation **open source** or "**open source code**", applies to software whose [license](https://en.wikipedia.org/wiki/Software_license) meets criteria precisely established by the [Open Source Initiative](https://en.wikipedia.org/wiki/Open_Source_Initiative), i.e., the possibilities of free redistribution, access to the [source code](https://en.wikipedia.org/wiki/Source_code), and creation of derivative works. This source code is made available to the general public and is generally the result of collaboration between programmers.
* **Operating System**: Also called **OS**, it is a set of programs that directs the use of a computer's resources by application software.
* **Overlays** : Image displayed on top of the game, mainly used for cosmetic reasons.
* **Override**: Files that allow overriding the default configuration for certain games or folders, see [configuration override](./../advanced-usage/configuration-override/).

## P

* **PBP** _(or .pbp)_ : Image files corresponding to physical UMD of the Playstation Portable, or to multi-disc games of the Playstation.
* **Pi:** See **Raspberry Pi**.
* **Plug and Play**: Also known as **PnP**, means "plug and play" or "plug and use". It is a technology that allows operating systems to quickly and automatically recognize compatible peripherals as soon as they are plugged in, without rebooting the computer. Plug and Play allows an implementation requiring a minimum of intervention on behalf of the user, without installation of dedicated software, and thus minimizing the errors of handling and parameter setting.

## R

* **Raspberry Pi** : 
* **Rebuild** : 
* **Recalbox**: The operating system itself, based on Buildroot System From Scratch.
* **Recalbox.conf**: File containing most of the settings for Recalbox and its emulators, see ["The recalbox.conf file"](./../basic-usage/getting-started/recalboxconf-file).
* **Redump**: Name of a group that references roms, specializes in disk-based games.
* **Reloaded**: Name of the version 7 of Recalbox.
* **Retroachievements**: A service to activate a trophy/success system for supported games and emulators.
* **RetroArch**: The Libretro Front-End used by some emulators in Recalbox, see [RetroArch](./../advanced-usage/retroarch/).
* **Rewind**: A setting that allows you to rewind in some emulators, in exchange for a loss of performance.
* **Roms**: Image files of physical games.
* **Romset**: A specific set of roms, mainly used for Netplay and Arcade.

## S

* **Samba**: SMB (Server Message Block), the sharing of folders and printers in a local network is a feature of modern operating systems that allows access to resources of a computer (data folders and printers) from another computer located in the same local network (home or corporate network).
* **Savestates**: A feature that allows you to save the exact state of the emulated machine, allowing a full backup of a game at any time, even if the game does not originally support saves, it is important to note that traditional game saves and savestates can conflict.
* **Scraper**: A program capable of fetching game information from the Internet, and saving it in a format that can be interpreted by EmulationStation.
* **Shaders**: Filters that can alter the visual rendering of a game at the cost of reduced performance, mainly used for cosmetic reasons.
* **Share**: Recalbox partition, accessible from the network.
* **SSH** :
* **Support Archive** _(or Support Archive)_: Webmanager option, allowing to generate a debugging link useful for developers in case of problems.


## T

* **Themes**: Files that define the overall look of EmulationStation.

## U

* **UMD**:

## V

* **Verbose**: For Recalbox, this is a boot mode displaying debugging information for some architectures, useful to understand why the system does not boot correctly.

## W

* **Webmanager**: A Recalbox-enabled network interface, allowing for easy setup, file management and more.

## X

* **Xin-Mo** :