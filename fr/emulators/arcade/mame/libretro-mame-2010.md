---
title: Libretro MAME 2010​
description: 
published: true
date: 2024-06-25T16:24:37.314Z
tags: libretro, mame, mame2010
editor: markdown
dateCreated: 2021-05-21T08:13:45.502Z
---



## ![](/emulators/license.svg) Licence

Ce core est sous licence **MAME non-commerciale**.

## ![](/emulators/compatibility.png) Compatibilité

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ❌ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Fonctionnalités

| Fonctionnalité | Supporté |
| :---: | :---: |
| Redémarrage | ✔ |
| Captures d'écran | ✔ |
| Options du core | ✔ |

## ![](/emulators/bios.svg) BIOS

Les romsets BIOS ne sont pas nécessaires lorsque vous utilisez des romsets d'arcade "Full Non-Merged". Pour les roms "Split" et "Non-Merged", placez le BIOS dans le même répertoire que le romset du jeu.

>**Remarque** :
>Placez les bios dans le répertoire : `/recalbox/share/roms/mame/`   
>ou dans son sous-répertoire si vous utilisez plusieurs cores de Mame : `/recalbox/share/roms/mame/Mame2010/`
Les jeux et leurs bios doivent être dans le même répertoire ou sous-répertoire.
{.is-warning}

## ![](/emulators/roms.png) Roms

* Basé sur le romset : MAME 0.139 (Août 2010)
* Fichier dat : vous trouverez le fichier dat dans votre recalbox dans le répertoire `/recalbox/share/arcade/libretro/mame2010.dat`.

### Emplacement

Placez les roms comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 mame
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.zip**
┃ ┃ ┃ ┃ ┣ 📁 **jeu**
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **jeu.chd**

Vous pouvez aussi opter pour le sous-répertoire déjà présent `mame2010` (utile si vous voulez avoir un set mame pour un autre core). Ceci permettra de lancer le bon émulateur automatiquement.

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 mame
┃ ┃ ┃ ┃ ┣ 📁 mame2010
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **jeu.zip**
┃ ┃ ┃ ┃ ┃ ┣ 📁 **jeu**
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 **jeu.chd**

## ![](/emulators/advanced-configuration.png) Configuration avancée de l'émulateur

>Pour pouvoir conserver vos configurations personnalisées lors d'une mise à jour, nous vous conseillons d'utiliser notre fonctionnalité [Surcharges de configuration](./../../../advanced-usage/configuration-override).
{.is-info}

### Accéder aux options

Vous pouvez configurer diverses options de deux façons différentes.

* Via le menu RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via le fichier `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Options du core

| Options | Valeur des options | Variable (pour surcharge) | Valeurs des variables |
| --- | --- | --- | --- |
| Mouse enabled | `Activé` ✅ / `Désactivé` | `mame_current_mouse_enabled` | `enabled` / `disabled` |
| Video approach 1 Enabled | `Désactivé` ✅ / `Activé` | `mame_current_videoappoach1_enable` | `disabled` / `enabled` |
| Hide nag screen | `Activé` ✅ / `Désactivé` | `mame_current_skip_nagscreen` | `enabled` / `disabled` |
| Hide game infos screen | `Désactivé` ✅ / `Activé` | `mame_current_skip_gameinfos` | `disabled` / `enabled` |
| Hide warning screen | `Désactivé` ✅ / `Activé` | `mame_current_skip_warnings` | `disabled` / `enabled` |
| Core provided aspect ratio | `DAR` ✅ / `PAR` | `mame_current_aspect_ratio` | `DAR` / `PAR` |
| Enable autofire | `Désactivé` ✅ / `button 1` / `button 2` / `R2 to button 1 mapping` / `R2 to button 2 mapping` | `mame_current_turbo_button` | `disabled` / `button 1` / `button 2` / `R2 to button 1 mapping` / `R2 to button 2 mapping` |
| Set autofire pulse speed | `medium` ✅ / `slow` / `fast` | `mame_current_turbo_delay` | `medium` / `slow` / `fast` |
| Set frameskip | `0` ✅ / `1` / `2` / `3` / `4` / `automatic` | `mame_current_frame_skip` | `0` / `1` / `2` / `3` / `4` / `automatic` |
| Set sample rate (Restart) | `48000Hz` ✅ / `44100Hz` / `32000Hz` / `22050Hz` | `mame_current_sample_rate` | `48000Hz` / `44100Hz` / `32000Hz` / `22050Hz` |
| Set brightness | `default` ✅ / `+1%` / `+2%` / `+3%` / `+4%` / `+5%` / `+6%` / `+7%` / `+8%` / `+9%` / `+10%` / `+11%` / `+12%` / `+13%` / `+14%` / `+15%` / `+16%` / `+17%` / `+18%` / `+19%` / `+20%` / `-20%` / `-19%` / `-18%` / `-17%` / `-16%` / `-15%` / `-14%` / `-13%` / `-12%` / `-11%` / `-10%` / `-9%` / `-8%` / `-7%` / `-6%` / `-5%` / `-4%` / `-3%` / `-2%` / `-1%` | `mame_current_adj_brightness` | `default` / `+1%` / `+2%` / `+3%` / `+4%` / `+5%` / `+6%` / `+7%` / `+8%` / `+9%` / `+10%` / `+11%` / `+12%` / `+13%` / `+14%` / `+15%` / `+16%` / `+17%` / `+18%` / `+19%` / `+20%` / `-20%` / `-19%` / `-18%` / `-17%` / `-16%` / `-15%` / `-14%` / `-13%` / `-12%` / `-11%` / `-10%` / `-9%` / `-8%` / `-7%` / `-6%` / `-5%` / `-4%` / `-3%` / `-2%` / `-1%` |
| Set contrast | `default` ✅ / `+1%` / `+2%` / `+3%` / `+4%` / `+5%` / `+6%` / `+7%` / `+8%` / `+9%` / `+10%` / `+11%` / `+12%` / `+13%` / `+14%` / `+15%` / `+16%` / `+17%` / `+18%` / `+19%` / `+20%` / `-20%` / `-19%` / `-18%` / `-17%` / `-16%` / `-15%` / `-14%` / `-13%` / `-12%` / `-11%` / `-10%` / `-9%` / `-8%` / `-7%` / `-6%` / `-5%` / `-4%` / `-3%` / `-2%` / `-1%` | `mame_current_adj_contrast` | `default` / `+1%` / `+2%` / `+3%` / `+4%` / `+5%` / `+6%` / `+7%` / `+8%` / `+9%` / `+10%` / `+11%` / `+12%` / `+13%` / `+14%` / `+15%` / `+16%` / `+17%` / `+18%` / `+19%` / `+20%` / `-20%` / `-19%` / `-18%` / `-17%` / `-16%` / `-15%` / `-14%` / `-13%` / `-12%` / `-11%` / `-10%` / `-9%` / `-8%` / `-7%` / `-6%` / `-5%` / `-4%` / `-3%` / `-2%` / `-1%` |
| Set gamme | `default` ✅ / `+1%` / `+2%` / `+3%` / `+4%` / `+5%` / `+6%` / `+7%` / `+8%` / `+9%` / `+10%` / `+11%` / `+12%` / `+13%` / `+14%` / `+15%` / `+16%` / `+17%` / `+18%` / `+19%` / `+20%` / `-20%` / `-19%` / `-18%` / `-17%` / `-16%` / `-15%` / `-14%` / `-13%` / `-12%` / `-11%` / `-10%` / `-9%` / `-8%` / `-7%` / `-6%` / `-5%` / `-4%` / `-3%` / `-2%` / `-1%` | `mame_current_adj_gamma` | `default` / `+1%` / `+2%` / `+3%` / `+4%` / `+5%` / `+6%` / `+7%` / `+8%` / `+9%` / `+10%` / `+11%` / `+12%` / `+13%` / `+14%` / `+15%` / `+16%` / `+17%` / `+18%` / `+19%` / `+20%` / `-20%` / `-19%` / `-18%` / `-17%` / `-16%` / `-15%` / `-14%` / `-13%` / `-12%` / `-11%` / `-10%` / `-9%` / `-8%` / `-7%` / `-6%` / `-5%` / `-4%` / `-3%` / `-2%` / `-1%` |
| Use external hiscore.dat | `Désactivé` ✅ / `Activé` | `mame-external_hiscore` | `disabled` / `enabled` |

## ![](/emulators/external-links.png) Liens externes

* **Progettosnaps** : [https://www.progettosnaps.net/](https://www.progettosnaps.net/)
* **Code source utilisé** : [https://github.com/libretro/mame2010-libretro/](https://github.com/libretro/mame2010-libretro)
* **Documentation Libretro** : [https://docs.libretro.com/library/mame\_2010/](https://docs.libretro.com/library/mame_2010/)