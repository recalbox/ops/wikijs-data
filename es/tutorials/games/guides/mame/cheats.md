---
title: Códigos de trucos
description: 
published: true
date: 2024-09-29T18:14:25.453Z
tags: mame, códigos, trucos
editor: markdown
dateCreated: 2024-09-29T18:14:25.453Z
---

## Introducción

Con MAME, es posible añadir códigos de trucos.

>Es preferible utilizar un control con los botones `Select` y `Hotkey` separados si desean conservar el uso de las guardas instantáneas.
{.is-info}

## Uso

* Descarguen el archivo de [códigos de trucos](https://www.mamecheat.co.uk/mame_downloads.htm) correspondiente a la versión integrada de MAME en Recalbox.

>Si no encuentran para la versión actual de MAME, tomen la primera versión anterior, funcionará al 100%.
{.is-info}

* Descompriman el archivo. Obtendrán 3 archivos: el único archivo que nos interesa es `cheat.7z`.
* Colocan el archivo `cheat.7z` en el directorio `/recalbox/share/bios/mame` **sin descomprimirlo!** y lancen su juego.

Para acceder a los códigos de trucos, durante el juego:

* Vayan al menú de RetroArch haciendo `Hotkey + B`.
* Vayan a `Opciones del núcleo` > `System`.

![](/tutorials/games/guides/mame/cheatsfr1.png){.full-width}

* En la parte superior de la pantalla de opciones, activen `Cheats` y salgan del juego.

![](/tutorials/games/guides/mame/cheatsfr2.png){.full-width}

* Relancen su juego y vayan al menú de MAME haciendo `Select` + `Y` y vayan a `Cheat`.

![](/tutorials/games/guides/mame/cheatsfr3.png){.full-width}

* Tendrán la lista de cheats para su juego. Solo queda seleccionar los cheats deseados.

![](/tutorials/games/guides/mame/cheatsfr4.png){.full-width}


* Tendrán la lista de los códigos de trucos para su juego. Solo les queda seleccionar los códigos de trucos deseados..

![](/tutorials/games/guides/mame/cheatsfr4.png){.full-width}