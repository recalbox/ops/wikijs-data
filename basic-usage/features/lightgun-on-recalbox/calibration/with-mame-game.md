---
title: Avec un jeu MAME
description: 
published: true
date: 2021-09-04T17:54:09.791Z
tags: lightgun, mame, calibration, 7.2+
editor: markdown
dateCreated: 2021-05-21T08:45:40.192Z
---

## Exemple de calibration dans MAME 2003 plus

1) Lancer le jeu et activer ce menu en pressant L3 sur votre manette.

![](/basic-usage/features/lightgun/calibration/mame1.png){.full-width}

2) Descendre avec votre dpad ou stick gauche dans "Analog Controls" et valider avec la touche `X` _(manette de SNES comme référence bien sûr_)

![](/basic-usage/features/lightgun/calibration/mame2.png){.full-width}

3) Choisir et mettre "Lightgun X sensitivity" et "Lightgun Y sensitivity" à 50 voir 100% (elle peut être à 25% par défaut) avec votre dpad ou votre stick gauche.

![](/basic-usage/features/lightgun/calibration/mame3.png){.full-width}

4) Sortir des menus en validant avec avec la touche `X` _(manette de SNES comme référence)_ pour revenir au jeu. Les paramètres seront effectifs de suite, Enjoy !!! 

5) Au prochain lancement, la configuration sera gardée (gardez bien vos "saves" au chaud !!!)

>Il faut bien garder ces fichiers. Ainsi vous n'aurez pas à refaire la manipulation, cela sera dans vos sauvegardes : 
>
>![](/basic-usage/features/lightgun/calibration/calibration1.png){.full-width}
{.is-warning}