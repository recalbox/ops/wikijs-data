---
title: Nintendo Switch Online wireless controllers
description: 
published: true
date: 2021-08-09T22:28:25.394Z
tags: switch, controller, nintendo, wireless, 7.2+
editor: markdown
dateCreated: 2021-08-07T13:41:44.007Z
---

Here we will see which official Nintendo wireless controllers are compatible and how to use them.

## What controllers are involved?

The Nintendo Switch controllers that can be used natively in Recalbox are the following:

* [Joy-Cons](joy-cons-usage)

![](/tutorials/controllers/controllers/switch-joycons/nintendo_switch_joy-con_controllers.png)

* [Nintendo Switch Online - Super Nintendo](snes-controller-usage)

![](/tutorials/controllers/controllers/switch-joycons/snes-controller.jpg)

In the following sections, you will find out how to synchronize and use these controllers in Recalbox.

## What about the Nintendo Switch Online - NES controllers?

These controllers cannot be used in Recalbox, each of these controllers is recognized as a right Joy-Con.