---
title: Connect a LCD display with I2C to your Recalbox
description: 
published: true
date: 2021-08-06T22:57:55.920Z
tags: display, lcd, screen, i2c
editor: markdown
dateCreated: 2021-08-06T22:57:55.920Z
---

## Prerequisite

An I2c LCD screen like the Hd44780 model in A00 version (ascii support + Japanese characters) or A02 (ascii support + European characters).

![](/tutorials/video/tft/hd44780.jpg)

## Installation

### I2C connection on Raspberry Pi GPIO

Connecting the I2C on a Raspberry Pi

![](/tutorials/video/tft/i2c-gpio.png)

### Activating I2C in Recalbox

* Modify the file [recalbox-user-config.txt](./../../system/modification/configtxt-configuration) with nano or vim and add the following lines:

```ini
#Activate I2C
dtparam=i2c1=on
dtparam=i2c_arm=on
```

* Edit the file `/boot/cmdline.txt` with nano or vim and add the following lines:

```ini
bcm2708.vc_i2c_override=1
```

* Restart your Recalbox

### Check the I2C address

You will need to know the address of your I2C** to run some display scripts. Typically, the address is `0x27` or `0x3f`.

To check this:

* Run the following command; it takes a while!
  * on very old Raspberry models: `i2cdetect -y 0` 
  * on newer Raspberry models (pi4, pi3, pi2): `i2cdetect -y 1`

```shell

     0  1  2  3  4  5  6  7  8  9  a  b  c  d  e  f
00:          -- -- -- -- -- -- -- -- -- -- -- -- --
10: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
20: -- -- -- -- -- -- -- 27 -- -- -- -- -- -- -- --
30: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
40: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
50: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
60: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
```

The address returned by the command in the table tells us: `0x27`

## Display script available for Recalbox.

This script is available in this [discussion](https://forum.recalbox.com/topic/8689/script-clcd-display-on-recalbox).

![](/tutorials/video/tft/hd4470-result.jpg)