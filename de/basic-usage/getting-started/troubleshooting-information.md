---
title: Fehlersuche und -behebung
description: 
published: true
date: 2024-06-02T19:46:41.427Z
tags: 
editor: markdown
dateCreated: 2021-06-24T11:39:14.844Z
---

## Controller

### Mein Controller funktioniert in Emulationstation, aber nicht, wenn ich Emulatoren ausführe

Wir haben so viele Controller-Konfigurationen wie möglich in EmulationStation hinzugefügt, so dass der Gebrauch möglichst unkpmpliziert ist. Jedoch müssen viele von ihnen vor dem ersten Gebrauch konfiguriert werden, damit sie in den Emulatoren funktionieren.

Öffne dazu das Hauptmenü in EmulationStation durch drücken von `START` und gehe dann zu `CONTROLLER EINSTELLUNGEN` > `CONTROLLER KONFIGURIEREN`.

### PS3-Controller blinkt, wird aber nicht gekoppelt

* Schließe den Controller mit einem Kabel an die Recalbox an und warte 10 Sekunden.
* Ziehe den Controller dann vom Kabel ab und drücke die `PS`-Taste.

### Der PS3-Controller scheint tot zu sein

* Du musst den Controller durch Drücken eines kleinen Knopfes auf der Rückseite des Controllers zurücksetzen. Nimm dazu eine Büroklammer.

## Anzeige

### Zusätzlicher schwarzer Rand, Bild zu groß

* Verwende das Bildmenü deines Fernsehers und stelle die Bildgröße auf 1:1 Pixel oder voll ein.

Wenn das nicht funktioniert, versuche, die Overscan-Funktion im Auswahlfeld der Menüsystemeinstellungen zu aktivieren. Weitere Informationen dazu findest du [in dieser Anleitung](./tutorials/video/display-configuration/image-size-settings-overscan-tft).

### Schwarzer Bildschirm auf dem PC-Monitor

Wenn du ein schwarzes Bild auf deinem PC-Monitor (HDMI oder DVI) hast, bearbeite die Datei `recalbox-user-config.txt` und füge ein `#` vor die Zeile `hdmi_drive = 2` ein (wenn es nicht schon vorhanden ist). Weitere Informationen dazu findest Du [in dieser Anleitung](./tutorials/video/lcd/dvi-screen).

## System

### Root-Zugriff

* Du kannst dich über [SSH](./tutorials/system/access/root-access-terminal-cli) mit deiner Recalbox verbinden. Du kannst auch auf ein Terminal zugreifen, indem du EmulationStation mit `F4` beendest und dann `ALT` + `F2` drückst. Weitere Informationen findest du in [diesem Tutorial](./tutorials/system/access/root-access-terminal-cli).