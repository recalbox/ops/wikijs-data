---
title: Libretro Tic80
description: 
published: true
date: 2024-06-23T11:33:08.736Z
tags: libretro, tic-80, tic80
editor: markdown
dateCreated: 2021-05-21T08:28:50.826Z
---

Libretro TIC-80 est un émulateur qui permet d'émuler un ordinateur fantastique pour créer, jouer et partager de petits jeux.

## ![](/emulators/license.svg) Licence

Ce core est sous licence [**MIT**](https://github.com/libretro/TIC-80/blob/master/LICENSE).

## ![](/emulators/compatibility.png) Compatibilité

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Fonctionnalités

| Fonctionnalité | Supporté |
| :---: | :---: |
| Redémarrage | ✔ |
| Sauvegardes | ✔ |
| Options du core | ✔ |
| Netplay | ✔ |
| Native Cheats | ✔ |
| Controls | ✔ |

## ![](/emulators/bios.svg) BIOS

>**Aucun bios n'est requis.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Extensions supportées

Les roms doivent avoir l'extension :

* .fd
* .sap
* .k7
* .m7
* .rom
* .zip
* .7z

Les fichiers contenus dans les .zip/.7z doivent correspondre aux extensions citées précédemment.

### Emplacement

Placez les roms comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 tic80
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.zip**

## ![](/emulators/advanced-configuration.png) Configuration avancée de l'émulateur

>Pour pouvoir conserver vos configurations personnalisées lors d'une mise à jour, nous vous conseillons d'utiliser notre fonctionnalité [Surcharges de configuration](./../../../advanced-usage/configuration-override).
{.is-info}

### Accéder aux options

Vous pouvez configurer diverses options de deux façons différentes.

* Via le Menu RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via le fichier `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Options du core

| Options | Valeur des options | Variable (pour surcharge) | Valeurs des variables |
| --- | --- | --- | --- |
| Crop Border | `Désactivé` ✅ / `Activé` | `tic80_crop_border` | `disabled` / `enabled` |
| Pointer Device | `Mouse` ✅ / `Touchscreen (Pointer API)` / `Left Analog` / `Right Analog` / `D-Pad` | `tic80_pointer_device` | `mouse` / `touchscreen` / `left_analog` / `right_analog` / `dpad` |
| Pointer Speed | `50%` / `60%` / `70%` / `80%` / `90%` / `100%` ✅ / `110%` / `120%` / `130%` / `140%` / `150%` / `160%` / `170%` / `180%` / `190%` / `200%` / `210%` / `220%` / `230%` / `240%` / `250%` / `260%` / `270%` / `280%` / `290%` / `300%` | `tic80_pointer_speed` | `50` / `60` / `70` / `80` / `90` / `100` / `110` / `120` / `130` / `140` / `150` / `160` / `170` / `180` / `190` / `200` / `210` / `220` / `230` / `240` / `250` / `260` / `270` / `280` / `290` / `300` |
| Mouse Cursor | `Désactivé` ✅ / `Dot` / `Cross` / `Arrow` | `tic80_mouse_cursor` | `disabled` / `dot` / `cross` / `arrow` |
| Mouse Cursor Color | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` ✅ | `tic80_mouse_cursor_color` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` |
| Mouse Cursor Hide Delay | `Disabled` / `1` / `2` / `3` / `4` / `5` ✅ / `6` / `7` / `8` / `9` / `10` | `tic80_mouse_hide_delay` | `disabled` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` |
| Gamepad Analog Deadzone | `0%` / `3%` / `6%` / `9%` / `12%` / `15%` ✅ / `18%` / `21%` / `24%` / `27%` / `30%` | `tic80_analog_deadzone` | `0` / `3` / `6` / `9` / `12` / `15` / `18` / `21` / `24` / `27` / `30` |

## ![](/emulators/external-links.png) Liens externes

* **Code source officiel** : [https://github.com/nesbox/TIC-80/](https://github.com/nesbox/TIC-80/)
* **Site officiel** : [https://tic.computer/](https://tic.computer/)
* **Wiki source officiel** : [https://github.com/nesbox/TIC-80/wiki](https://github.com/nesbox/TIC-80/wiki)
* **Base de données Libretro** : [Libretro TIC-80 Database](https://github.com/libretro/libretro-database/blob/master/rdb/TIC-80.rdb)
* **Codes de triche Libretro Tic-80** : [https://github.com/libretro/libretro-database/tree/master/cht/TIC-80/](https://github.com/libretro/libretro-database/tree/master/cht/TIC-80)