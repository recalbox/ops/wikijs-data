---
title: Libretro MAME 2010
description: 
published: true
date: 2021-10-12T00:21:03.011Z
tags: libretro, neo-geo, mame2010
editor: markdown
dateCreated: 2021-08-01T10:39:24.608Z
---



## ![](/emulators/license.svg) License

This core is under **MAME non-commercial** license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Features



## ![](/emulators/bios.svg) BIOS

### List of mandatory bios

| Filename | Description | MD5 | Provided |
| :---: | :---: | :---: | :---: |
| neogeo.zip | Neo-Geo BIOS | e023d0a4b5249fdff2a4620c28963944 8cbdff53661f41ddbe486a98915e1ec9 42e359e394f0be1d61b94ddca0efbe6c c1e0a738d266926f604186aa9d63e4db d12bba59121229ff13702878b415cb7c 2b4ffdf24d5d4f24c4b116c50c4bafc3 005b843b6e70b939c2cca41887cbc371 42e359e394f0be1d61b94ddca0efbe6c 4b9432a4571d03cc8bae9d24d8d6eb40 4cd64313e26537c04cdb561b8f84b0e4 50bbfd6bf297038b085e29265c65723a 76ee7390ccd537cddea1018025ca29a8 84d6c35aad790a77ba3cfc1b319e1061 912c0a56bafe0fba39d0c668b139b250 94748719c0ef31e7375b0c357d82fc89 ab25b9e25052a6064901a7a221037eb6 ae9e5825cde82c58e39dfd48534f7060 b18f83feef474b8a1306ef199cd810a2 b4519df27a352c2a6e42e06d31330d91 bf00272e7150b31156a8995d60bf185d ca66fd463ef7dcab492a6de8ce5f45eb f4a125538dfd7a8044e025fc5188fb88 a3b2c9a69c28ad0bd4ea07877b744bbe | ❌  |

### Location

Put the bios like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 neogeo
┃ ┃ ┃ ┃ ┣ 🗒 **neogeo.zip**

## ![](/emulators/roms.png) Roms

### Supported extensions

Roms must have the extension:

* .zip
* .7z

### Location

Put the roms like this:

┣ 📁recalbox
┃ ┣ 📁share
┃ ┃ ┣ 📁roms
┃ ┃ ┃ ┣ 📁 neogeo
┃ ┃ ┃ ┃ ┣ 🗒 **game.zip**

>For more information about roms, go to [this tutorial](./../../../tutorials/games/generalities/isos-and-roms)!
{.is-info}

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Accessing to options

You can configure various options in two different ways.

* Via RetroArch menu:

┣ 📁 RetroArch Menu
┃ ┣ 📁 Core options
┃ ┃ ┣ 🧩 Name_option

* Via the file `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core options

| Options | Options values | Variables (for configuration override) | Variables values |
| --- | --- | --- | --- |
| Mouse enabled | `Enabled` ✅ / `Disabled` | `mame_current_mouse_enabled` | `enabled` / `disabled` |
| Video approach 1 Enabled | `Disabled` ✅ / `Enabled` | `mame_current_videoappoach1_enable` | `disabled` / `enabled` |
| Hide nag screen | `Enabled` ✅ / `Disabled` | `mame_current_skip_nagscreen` | `enabled` / `disabled` |
| Hide game infos screen | `Disabled` ✅ / `Enabled` | `mame_current_skip_gameinfos` | `disabled` / `enabled` |
| Hide warning screen | `Disabled` ✅ / `Enabled` | `mame_current_skip_warnings` | `disabled` / `enabled` |
| Core provided aspect ratio | `DAR` ✅ / `PAR` | `mame_current_aspect_ratio` | `DAR` / `PAR` |
| Enable autofire | `Disabled` ✅ / `button 1` / `button 2` / `R2 to button 1 mapping` / `R2 to button 2 mapping` | `mame_current_turbo_button` | `disabled` / `button 1` / `button 2` / `R2 to button 1 mapping` / `R2 to button 2 mapping` |
| Set autofire pulse speed | `medium` ✅ / `slow` / `fast` | `mame_current_turbo_delay` | `medium` / `slow` / `fast` |
| Set frameskip | `0` ✅ / `1` / `2` / `3` / `4` / `automatic` | `mame_current_frame_skip` | `0` / `1` / `2` / `3` / `4` / `automatic` |
| Set sample rate (Restart) | `48000Hz` ✅ / `44100Hz` / `32000Hz` / `22050Hz` | `mame_current_sample_rate` | `48000Hz` / `44100Hz` / `32000Hz` / `22050Hz` |
| Set brightness | `default` ✅ / `+1%` / `+2%` / `+3%` / `+4%` / `+5%` / `+6%` / `+7%` / `+8%` / `+9%` / `+10%` / `+11%` / `+12%` / `+13%` / `+14%` / `+15%` / `+16%` / `+17%` / `+18%` / `+19%` / `+20%` / `-20%` / `-19%` / `-18%` / `-17%` / `-16%` / `-15%` / `-14%` / `-13%` / `-12%` / `-11%` / `-10%` / `-9%` / `-8%` / `-7%` / `-6%` / `-5%` / `-4%` / `-3%` / `-2%` / `-1%` | `mame_current_adj_brightness` | `default` / `+1%` / `+2%` / `+3%` / `+4%` / `+5%` / `+6%` / `+7%` / `+8%` / `+9%` / `+10%` / `+11%` / `+12%` / `+13%` / `+14%` / `+15%` / `+16%` / `+17%` / `+18%` / `+19%` / `+20%` / `-20%` / `-19%` / `-18%` / `-17%` / `-16%` / `-15%` / `-14%` / `-13%` / `-12%` / `-11%` / `-10%` / `-9%` / `-8%` / `-7%` / `-6%` / `-5%` / `-4%` / `-3%` / `-2%` / `-1%` |
| Set contrast | `default` ✅ / `+1%` / `+2%` / `+3%` / `+4%` / `+5%` / `+6%` / `+7%` / `+8%` / `+9%` / `+10%` / `+11%` / `+12%` / `+13%` / `+14%` / `+15%` / `+16%` / `+17%` / `+18%` / `+19%` / `+20%` / `-20%` / `-19%` / `-18%` / `-17%` / `-16%` / `-15%` / `-14%` / `-13%` / `-12%` / `-11%` / `-10%` / `-9%` / `-8%` / `-7%` / `-6%` / `-5%` / `-4%` / `-3%` / `-2%` / `-1%` | `mame_current_adj_contrast` | `default` / `+1%` / `+2%` / `+3%` / `+4%` / `+5%` / `+6%` / `+7%` / `+8%` / `+9%` / `+10%` / `+11%` / `+12%` / `+13%` / `+14%` / `+15%` / `+16%` / `+17%` / `+18%` / `+19%` / `+20%` / `-20%` / `-19%` / `-18%` / `-17%` / `-16%` / `-15%` / `-14%` / `-13%` / `-12%` / `-11%` / `-10%` / `-9%` / `-8%` / `-7%` / `-6%` / `-5%` / `-4%` / `-3%` / `-2%` / `-1%` |
| Set gamme | `default` ✅ / `+1%` / `+2%` / `+3%` / `+4%` / `+5%` / `+6%` / `+7%` / `+8%` / `+9%` / `+10%` / `+11%` / `+12%` / `+13%` / `+14%` / `+15%` / `+16%` / `+17%` / `+18%` / `+19%` / `+20%` / `-20%` / `-19%` / `-18%` / `-17%` / `-16%` / `-15%` / `-14%` / `-13%` / `-12%` / `-11%` / `-10%` / `-9%` / `-8%` / `-7%` / `-6%` / `-5%` / `-4%` / `-3%` / `-2%` / `-1%` | `mame_current_adj_gamma` | `default` / `+1%` / `+2%` / `+3%` / `+4%` / `+5%` / `+6%` / `+7%` / `+8%` / `+9%` / `+10%` / `+11%` / `+12%` / `+13%` / `+14%` / `+15%` / `+16%` / `+17%` / `+18%` / `+19%` / `+20%` / `-20%` / `-19%` / `-18%` / `-17%` / `-16%` / `-15%` / `-14%` / `-13%` / `-12%` / `-11%` / `-10%` / `-9%` / `-8%` / `-7%` / `-6%` / `-5%` / `-4%` / `-3%` / `-2%` / `-1%` |
| Use external hiscore.dat | `Disabled` ✅ / `Enabled` | `mame-external_hiscore` | `disabled` / `enabled` |

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/libretro/mame2010-libretro/](https://github.com/libretro/mame2010-libretro/)