---
title: FAQ
description: 
published: true
date: 2023-09-20T20:35:22.899Z
tags: faq, neo-geo cd
editor: markdown
dateCreated: 2023-09-20T20:35:22.899Z
---

## Notas sobre el formato de los juego

### Versiones "normales" y "cdz pached"

FBNeo suporta los 2 formatos, pero otros emuladores de cd Neogeo sólo soportan las versiones cdz parcheadas. Quiza lo mejor sea quedarse con estas últimas.

### Formato Redump

Los ficheros del proyecto redump están en formato `.CUE` con varios ficheros `.BIN` llamados **multibin**. Este core no soporta los multibin y hay que convertir estos juegos para que tengan un único archivo bin.

## Conversion

Podéis hacerlo con el software **CDmage** 1.02.1:

**File** &gt; **Open** &gt; Seleccionad vuestro juego en formato .CUE
**File** &gt; **Save As** &gt; Introducid un nuevo nombre para vuestro juego para no sobreescribir el existente y pulsad `Save'.
* Aseguráos de haber elegido "MODE1/2352" en la lista desplegable de la derecha de la nueva ventana y pulsad `OK`.

Fuentes:

* [https://neo-source.com/index.php?topic=2487.msg26465#msg26465](https://neo-source.com/index.php?topic=2487.msg26465#msg26465)
* [https://github.com/libretro/FBNeo/blob/master/src/burner/libretro/README.md#neogeo-cd-doesnt-work-why-](https://github.com/libretro/FBNeo/blob/master/src/burner/libretro/README.md#neogeo-cd-doesnt-work-why-)