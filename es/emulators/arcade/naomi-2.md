---
title: Naomi 2
description: 
published: true
date: 2024-07-23T21:37:29.026Z
tags: arcade, naomi 2
editor: markdown
dateCreated: 2024-06-23T09:10:01.436Z
---

![](/emulators/arcade/naomi2.svg){.align-center}

## Especificaciones técnicas

* **Procesador**: Hitachi SH-4 RISC de 32 bits (a 200 MHz 360 MIPS / 1,4 GFLOPS)
* **Vídeo**: Procesador PowerVR 2 a 100 MHz fabricado por Nec
* **Audio**: Procesador ARM7 Yamaha XG AICA RISC de 32 bits
* **RAM**: 56 MB
* **Soportes**: Cartucho (PCB de ROMS), 168 MB máximo

## Presentación

El Naomi 2 es el acrónimo de _New Arcade Operation Machine Idea_ (literalmente "nueva idea de máquina arcade"), Naomi también se traduce como "belleza" en japonés.

La arquitectura de hardware de la Naomi 2 es similar a la de su predecesora, por lo que los juegos son compatibles.

El sistema ofrece un rendimiento gráfico mejorado, beneficiándose de la mayor capacidad gráfica del hardware original de la Naomi. Ahora cuenta con dos chips gráficos PowerVR CLX2 y un chip VideoLogic Elan, y la cantidad de memoria gráfica se ha duplicado de 16 MB a 32 MB (cuatro veces más que la Dreamcast), lo que permite mostrar más detalles gráficos.

Con la Naomi 2, Sega ofrece su lector GD-ROM como extra opcional. Las placas Dimm y los GD-Roms, sea cual sea su versión (Naomi, Naomi 2), son totalmente compatibles con el Naomi o el Naomi 2 (si la BIOS es adecuada, claro).

El Naomi 2, al igual que su predecesor, puede conectarse a una red, y se denomina Terminal Satélite Naomi 2.

El sistema Naomi 2 se estableció en su momento como un referente en cuanto a potencia arcade, beneficiándose del saber hacer de Sega en este campo.

## Emuladores

[Libretro Flycast-Next](libretro-flycast-next)