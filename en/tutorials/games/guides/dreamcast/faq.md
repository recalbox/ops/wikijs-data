---
title: FAQ
description: 
published: true
date: 2021-10-16T11:17:16.677Z
tags: card, memory, dreamcast, faq, vmu, multidisc
editor: markdown
dateCreated: 2021-10-16T11:17:16.677Z
---

## Game conversion

### What game extensions can be used with the Dreamcast ?

You have several extensions but we will focus on the following ones:

* `.BIN`/`.CUE`
* `.CHD`
* `.GDI`

### I want to convert my isos from `.BIN`/`.CUE` format to `.CHD`, is it possible ?

Yes, but you may encounter problems with emulators later on. Indeed, the Redump format (`.BIN`/`.CUE`) is a format that stores more information than the game data such as the data added at the time of the creation of the Master Glass in the factory, then at the pressing. This information is not read by emulators and would be useless to them.

However, the Redump format is designed for pure preservation and archiving, but not for playing.

For playing, it is strongly recommended to use the TOSEC format (`.GDI`). This format includes tracks and data readable by Dreamcast players (Yamaha GD-ROM) and is fully understood by emulators.

The `.CHD` format (v4 and v5) was designed to convert from `.GDI` to TOSEC format only.

Making it take the Redump format is a problem, because it is not possible to 're-map' the tracks simply (without going back to third party software, transferring data and shifting the positions written in the `.CUE` file to a `.GDI` file + reformatting the RAW files) as in TOSEC.

When you present a `.BIN`/`.CUE` converted to `.CHD`, the emulator will not understand the origin of the set, and will wait for the tracks to be set as in TOSEC.

There is no better format between the two.

### Is it best to convert my isos from `.GDI` to `.CHD` format?

Yes, this is the best thing to do if you want to convert your isos to `.CHD` format.

## Multidisc games

On **Dreamcast**, there are **some Multidisc games**.

**Example** :

* _Resident Evil Code Veronica_
* _Skies Of Arcadia_
* _Grandia 2_
* _Etc..._

**Here is how to change discs:**

At the end of disk 1 of your game, the game proposes a _save_. You go back to the menu with **Hotkey** (on Reicast it's enough), then you launch the second disk **by recovering its backup**.

Nothing could be easier !

## Access VMU menu

### Objective

The objective is to be able to modify and manipulate the contents of the memory cards. There are 2 ways to proceed.

### {.tabset}
#### Eject the disk

* Launch a Dreamcast game.
* When the game is playing, eject the disk with `Hotkey` + `Left stick up`.
* The console will automatically stop the game and come to the system. From here, you can access the memory cards.
* To resume the game, press `Hotkey` + `Left Stick Up` again.

#### Empty disk

* Create an empty text file in the `/share/roms/dreamcast` folder, which you can rename to your liking with a `.cdi` extension (Dreamcast BIOS.cdi for example).
* Update the list of games in Recalbox.
* Launch the "game" and voilà!