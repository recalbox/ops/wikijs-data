---
title: Libretro Cap32
description: 
published: true
date: 2025-02-25T04:47:23.308Z
tags: libretro, amstrad, cpc, cap32
editor: markdown
dateCreated: 2021-07-29T15:37:10.342Z
---



## ![](/emulators/license.svg) License

This core is under [**GPLv2**](https://github.com/ColinPitrat/caprice32/blob/master/COPYING.txt) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Features

| Feature | Supported |
| :---: | :---: |
| Restart  | ✔ |
| Screenshots | ✔ |
| Saves | ✔ |
| States | ✔ |
| Rewind | ✔ |
| Netplay | ✔ |
| Core Options | ✔ |
| RetroAchievements | ✔ |
| Controls | ✔ |
| Remapping | ✔ |
| Disk Control | ✔ |

## ![](/emulators/bios.svg) BIOS

>**No bios is required.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Supported extensions

Roms must have the extension:

* .bin
* .cdt
* .cpr
* .dsk
* .m3u
* .sna
* .tap
* .voc
* .zip
* .7z

This system supports compressed roms in .zip/.7z format. However, be careful, it is only an archive.

The files contained in the .zip/.7z must correspond to the extensions mentioned above.
Moreover, each .zip/.7z file must contain only one compressed rom.

>Games with extension .bin must be uncompressed!
{.is-info}

### Location

Put the roms like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 amstradcpc
┃ ┃ ┃ ┃ ┣ 🗒 **game.zip**

>The roms using **TOSEC** format are highly recommended.
{.is-success}

>For more information about roms, go to [this tutorial](./../../../tutorials/games/generalities/isos-and-roms)!
{.is-info}

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Accessing to options

You can configure various options in two different ways.

* Via RetroArch menu:

┣ 📁 RetroArch Menu
┃ ┣ 📁 Core options
┃ ┃ ┣ 🧩 Name_option

* Via the file `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core options

| Options | Options values | Variables (for configuration override) | Variables values |
| --- | --- | --- | --- |
| Controls > User 1 Controller Config | `auto` ✅ / `qaop` / `incentive` / `joystick_port1` | `cap32_retrojoy0` | `auto` / `qaop` / `incentive` / `joystick_port1` |
| Controls > User 2 Controller Config | `auto` ✅ / `qaop` / `incentive` / `joystick_port1` / `joystick_port2` | `cap32_retrojoy1` | `auto` / `qaop` / `incentive` / `joystick_port1` / `joystick_port2` |
| Controls > Combo Key | `select` / `y` ✅ / `b` / `Disabled` | `cap32_combokey` | `select` / `y` / `b` / `disabled` |
| Controls > Use internal Remap DB | `Enabled` ✅ / `Disabled` | `cap32_db_mapkeys` | `enabled` / `disabled` |
| Light Gun > Input | `Disabled` ✅ / `phaser` / `gunstick` | `cap32_lightgun_input` | `disabled` / `phaser` / `gunstick` |
| Light Gun > Show Crosshair | `Disabled` ✅ / `Enabled` | `cap32_lightgun_show` | `disabled` / `enabled` |
| Model | `6128` ✅ / `464` / `664` / `6128+ (experimental)` | `cap32_model` | `6128` / `464` / `664` / `6128+ (experimental)` |
| Advanced > Autorun | `Enabled` ✅ / `Disabled` | `cap32_autorun` | `enabled` / `disabled` |
| Advanced > Ram size | `64` / `128` ✅ / `192` / `512` / `576` | `cap32_ram` | `64` / `128` / `192` / `512` / `576` |
| Video > Green Phosphor blueish | `5` / `10` / `15` ✅ / `20` / `30` | `cap32_advanced_green_phosphor` | `5` / `10` / `15` / `20` / `30` |
| Video > Monitor Intensity | `5` / `6` / `7` / `8` / `9` / `10` ✅ / `11` / `12` / `13` / `14` / `15` | `cap32_scr_intensity` | `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` |
| Video Advanced > Color Depth | `16bit` ✅ / `24bit` | `cap32_gfx_colors` | `16bit` / `24bit` |
| Video Advanced > Crop Screen Borders | `Disabled` ✅ / `Enabled` | `cap32_scr_crop` | `disabled` / `enabled` |
| Status Bar | `onloading` ✅ / `Enabled` / `Disabled` | `cap32_statusbar` | `onloading` / `enabled` / `disabled` |
| Keyboard Transparency | `Disabled` ✅ / `Enabled` | `cap32_keyboard_transparency` | `disabled` / `enabled` |
| Floppy Sound | `Enabled` ✅ / `Disabled` | `cap32_floppy_sound` | `enabled` / `disabled` |
| Monitor Type | `color` ✅ / `green` / `white` | `cap32_scr_tube` | `color` / `green` / `white` |
| CPC Language | `english` ✅ / `french` / `spanish` | `cap32_lang_layout` | `english` / `french` / `spanish` |

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/libretro/libretro-cap32/](https://github.com/libretro/libretro-cap32/)
* **Libretro documentation**: [https://docs.libretro.com/library/caprice32/](https://docs.libretro.com/library/caprice32/)