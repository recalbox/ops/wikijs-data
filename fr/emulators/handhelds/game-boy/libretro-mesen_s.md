---
title: Libretro Mesen_S
description: 
published: true
date: 2024-06-23T11:56:42.843Z
tags: libretro, game, boy, gameboy, mesen
editor: markdown
dateCreated: 2021-05-21T08:29:27.861Z
---

**Libretro Mesen-S** est un émulateur SNES multiplateforme pour Windows et Linux construit en C++ et C#.

## ![](/emulators/license.svg) Licence

Ce core est sous licence [**GPLv3**](https://github.com/SourMesen/Mesen-S/blob/master/README.md).

## ![](/emulators/compatibility.png) Compatibilité

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ❌ | ❌ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Fonctionnalités

| Fonctionnalités | Supporté |
| :---: | :---: |
| Redémarrage | ✔ |
| Sauvegardes | ✔ |
| Sauvegardes instantanées | ✔ |
| Rembobinage | ✔ |
| Netplay | ✔ |
| Options du core | ✔ |
| RetroAchievements | ✔ |
| Cheats RetroArch | ✔ |
| Cheats natifs | ✔ |
| Controls | ✔ |
| Remapping | ✔ |
| Multi-Mouse | ✔ |
| Softpatching | ✔ |
| Crop Overscan | ✔ |

## ![](/emulators/bios.svg) BIOS

### Liste des bios optionnels

| Nom de fichier | Description | MD5 | Fourni |
| :---: | :---: | :---: | :---: |
| dmg_boot.bin | BIOS Game Boy | 57f6a79fb41d6b2c8987fa529c3a90f2 | ✅ |

### Emplacement

Placez les BIOS comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 🗒 **dmg_boot.bin**

## ![](/emulators/roms.png) Roms

### Extensions supportées

La rom doit avoir l'extension :

* .gb
* .zip
* .7z

Ce système supporte les roms compressées au format .zip/.7z. Attention toutefois, il ne s'agit que d'une archive.

Les fichiers contenus dans les .zip/.7z doivent correspondre aux extensions citées précédemment.
De plus, chaque fichier .zip/.7z ne doit contenir qu'une seule rom compressée.

### Emplacement

Placez les roms comme ceci : 

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 gb
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.zip**

>Les roms au format **No-Intro** sont vivement conseillées.
{.is-success}

>Pour plus d'information sur les roms, rendez-vous sur [ce tutoriel](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuration avancée de l'émulateur

>Pour pouvoir conserver vos configurations personnalisées lors d'une mise à jour, nous vous conseillons d'utiliser notre fonctionnalité [Surcharges de configuration](./../../../advanced-usage/configuration-override).
{.is-info}

### Accéder aux options

Vous pouvez configurer diverses options de deux façons différentes.

* Via le menu RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via le fichier `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Options du core

| Options | Valeurs des options | Variable (pour surcharge) | Valeurs des variables |
| --- | --- | --- | --- |
| NTSC Filter | `Désactivé` ✅ / `Composite (Blargg)` / `S-Video (Blargg)` / `RGB (Blargg)` / `Monochrome (Blargg)` | `mesen-s_ntsc_filter` | `Disabled` / `Composite (Blargg)` / `S-Video (Blargg)` / `RGB (Blargg)` / `Monochrome (Blargg)` |
| Region | `Auto` ✅ / `NTSC` / `PAL` | `mesen-s_region` | `Auto` / `NTSC` / `PAL` |
| Game Boy Model | `Auto` ✅ / `Game Boy` / `Game Boy Color` / `Super Game Boy` | `mesen-s_gbmodel` | `Auto` / `Game Boy` / `Game Boy Color` / `Super Game Boy` |
| Use SGB2 | `Activé` ✅ / `Désactivé` | `mesen-s_sgb2` | `enabled` / `disabled` |
| Vertical Overscan | `None` ✅ / `8px` / `16px` | `mesen-s_overscan_vertical` | `None` / `8px` / `16px` |
| Horizontal Overscan | `None` ✅ / `8px` / `16px` | `mesen-s_overscan_horizontal` | `None` / `8px` / `16px` |
| Aspect Ratio | `Auto` ✅ / `No Stretching` / `NTSC` / `PAL` / `4:3` / `16:9` | `mesen-s_aspect_ratio` | `Auto` / `No Stretching` / `NTSC` / `PAL` / `4:3` / `16:9` |
| Blend Hi-Res Modes | `Désactivé` ✅ / `Activé` | `mesen-s_blend_high_res` | `disabled` / `enabled` |
| Cubic Interpolation (Audio) | `Désactivé` ✅ / `Activé` | `mesen-s_cubic_interpolation` | `disabled` / `enabled` |
| Overclock | `None` ✅ / `Low` / `Medium` / `High` / `Very High` | `mesen-s_overclock` | `None` / `Low` / `Medium` / `High` / `Very High` |
| Overclock Type | `Before NMI` ✅ / `After NMI` | `mesen-s_overclock_type` | `Before NMI` / `After NMI` |
| Super FX Clock Speed | `100%` ✅ / `200%` / `300%` / `400%` / `500%` / `1000%` | `mesen-s_superfx_overclock` | `100%` / `200%` / `300%` / `400%` / `500%` / `1000%` |
| Default power-on state for RAM | `Random Values (Default)` ✅ / `All 0s` / `All 1s` | `mesen-s_ramstate` | `Random Values (Default)` / `All 0s` / `All 1s` |

## ![](/emulators/external-links.png) Liens externes

* **Code source utilisé** : [https://github.com/libretro/Mesen-S/](https://github.com/libretro/Mesen-S/)
* **Documentation Libretro** : [https://docs.libretro.com/library/mesen-s/](https://docs.libretro.com/library/mesen-s/)