---
title: Libretro PrBoom
description: 
published: true
date: 2025-01-04T06:12:40.921Z
tags: libretro, doom, prboom
editor: markdown
dateCreated: 2021-06-29T21:02:14.048Z
---

**PrBoom** is the culmination of years of work by various people and projects on the Doom source code.  
PrBoom includes the work of all the following projects:

* Original Doom source release
* DosDoom
* [Boom](http://www.teamtnt.com/boompubl/boom2.htm)
* MBF
* [LxDoom](http://lxdoom.linuxgames.com/)
* lSDLDoom

PrBoom is a port of the Doom graphics engine. It can run Doom 1 and Doom 2.

## ![](/emulators/license.svg) License

This core is under [**GPLv2**](https://github.com/libretro/libretro-prboom/blob/master/COPYING) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Features

| Feature | Supported |
| :---: | :---: |
| Restart | ✔ |
| Screenshots | ✔ |
| Saves | ✔ |
| Core Options | ✔ |
| RetroArch Cheats | ✔ |
| Native Cheats | ✔ |
| Controls | ✔ |
| Remapping | ✔ |

## ![](/emulators/bios.svg) BIOS

>Bios is already in directory `/recalbox/share/bios/prboom/prboom.wad`
{.is-info}

## ![](/emulators/roms.png) Roms

>**WADs** files are similar to "roms".
{.is-info}

>Place your Doom roms in the directory `/recalbox/share/roms/ports/doom/`
{.is-warning}

>Use only lower case for your `.wad` files
{.is-warning}

### Some infos about WADs

Even if the files all have the `.wad` extension, there are several kinds:

* **IWAD**: this is a `.wad` that contains all the data and runs by itself.  
  For the most famous ones:

  * `doom1.wad` for shareware versions of **DOOM**.
  * `doom.wad` for commercial versions of **DOOM**.
  * `doom.wad or doomu.wad` for the versions of **The Ultimate DOOM**.
  * `doom2.wad` for the versions of **DOOM II - Hell on Earth**.
  * `tnt.wad` for the versions of **Final DOOM - TNT Evilution**.
  * `plutonia.wad` for the versions of **Final DOOM - The Plutonia Experiment**.
  * `sigil.wad` for the versions of the last episode of **DOOM - SIGIL**.

* **PWAD** : it's an `.wad` 'add-on' that will need an IWAD to work (for example: almost all amateur levels created by the Doom community).

WADs must also be compatible with PrBoom. Some mods go very far in modifying **(like Brutal Doom, Doom Raider, Golden Eye,...)** but **will not** work with PrBoom. You will have to look for the info on the WAD you want to play!  
If a WAD replaces a single level, it won't necessarily be the level 1 of episode 1. You'll have to reach the right level or use a cheat to access it directly.

### Compatibility list of WAD sets

| WAD file | File name - MD5 | Status |
| :--- | :--- | :--- |
| **SIGIL** |  |  |
| SIGIL (v1.21) | `SIGIL_v1_21.WAD` - 743d6323cb2b9be24c258ff0fc350883 | ✅ on Recalbox |
| **MASTER LEVEL for DOOM II (Registered)** |  |  |
| Master Levels for Doom II (Classic Complete) (PlayStation 3) | `MASTERLEVELS.WAD` - 84cb8640f599c4a17c8eb526f90d2b7a | ✅ |
| Master Levels for Doom II | `20x separate WADs files` | ❌ |
| Final Doom (Registered)** |  |  |
| Final Doom - Evilution | `TNT.WAD` - 4e158d9953c79ccf97bd0663244cc6b6 | ✅ |
| Final Doom - The Plutonia Experiment *v1.9* | `PLUTONIA.WAD` - 75c8cf89566741fa9d22447604053bd7 | ✅ |
| Doom II - Hell on Earth (Registered)** |  |  |
| Doom II - Hell on Earth (v1.9) | `DOOM2.WAD` - 25e1459ca71d321525f84628f45ca8cd | ✅ |
| **The Ultimate DOOM (Registered)** |  |  |
| Ultimate Doom, The *"v1,9ud "* | `DOOM.WAD, UDOOM.WAD, DOOMU.WAD` - c4fe9fd920207691a9f493668e0a2083 | ✅ |
| **DOOM (Registered)** |  |  |
| Doom (v1.9) | `DOOM.WAD` - 1cd63c5ddff1bf8ce844237f580e9cf3 | ✅ |
| **DOOM (Shareware)** |  |  |
| Doom (v1.9) (Demo) | `DOOM1.WAD` - f0cefca49926d00903cf57551d901abe | ✅ on Recalbox |

## Play SIGIL :

>The `doom.wad` file in the roms directory is actually the 1.9 Shareware version of the **Doom** game and should be renamed to `doom1.wad`.
{.is-info}

* To play **SIGIL** you must replace this shareware version `doom.wad` with the **Registered** version of **The Ultimate DOOM v1.9**.

>No other version of `doom.wad` will work with **SIGIL**!
{.is-warning}

* To do this:
  * Rename the existing file at the root of `roms\ports\doom.wad` to `doom1.wad` (or delete it).
  * Paste the new `doom.wad` file from **The Ultimate DOOM v1.9** in place of the old one.
  * Relaunch an update of your game list and run **SIGIL**.

>When you launch **SIGIL**, launching the first four episodes will inevitably launch the first episode. To launch **SIGIL**, simply select the SIGIL episode.
{.is-info}

>To get a **Registered** version of **DOOM**, you must have the original floppy disks or CD-Rom, or get them from STEAM or GOG.
{.is-danger}

## Location

Put your roms like this: 

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 ports
┃ ┃ ┃ ┃ ┣ 📁 Doom
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **\*.wad**

This rom is included in your Recalbox.

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Accessing to options

You can configure various options in two different ways.

* Via RetroArch menu:

┣ 📁 RetroArch Menu
┃ ┣ 📁 Core options
┃ ┃ ┣ 🧩 Name_option

* Via the file `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core options

| Options | Options values | Variables (for configuration override) | Variables values |
| --- | --- | --- | --- |
| Internal resolution (restart) | `320x200` ✅ / `640x400` / `960x600` / `1280x800` / `1600x1000` / `1920x1200` / `2240x1400` / `2560x1600` | `prboom-resolution` | `320x200` / `640x400` / `960x600` / `1280x800` / `1600x1000` / `1920x1200` / `2240x1400` / `2560x1600` |
| Mouse active when using Gamepad | `Disabled` ✅ / `Enabled` | `prboom-mouse_on` | `disabled` / `enabled` |
| Look on parent folders for IWADs | `Enabled` ✅ / `Disabled` | `prboom-find_recursive_one` | `enabled` / `disabled` |
| Rumble Effects | `Disabled` ✅ / `Enabled` | `prboom-rumble` | `disabled` / `enabled` |
| Analog Deadzone (percent) | `0` / `5` / `10` / `15` ✅ / `20` / `25` / `30` | `prboom-analog_deadzone` | `0` / `5` / `10` / `15` / `20` / `25` / `30` |
| Cache Size | `8 MB` / `12 MB` / `16 MB` / `24 MB` / `32 MB` / `48 MB` / `64 MB` / `128 MB` / `256 MB` | `prboom-purge_limit` | `8` / `12` / `16` / `24` / `32` / `48` / `64` / `128` / `256` |

## ![](/emulators/external-links.png) External links

* **Code source used** : [https://github.com/libretro/libretro-prboom/](https://github.com/libretro/libretro-prboom)
* **Libretro documentation** :  [https://docs.libretro.com/library/prboom/](https://docs.libretro.com/library/prboom/)
* **Official site** : [http://prboom.sourceforge.net/about.html](http://prboom.sourceforge.net/about.html)