---
title: Pocket Challenge V2
description: 
published: true
date: 2024-07-26T08:34:29.807Z
tags: pocket, challenge, v2, pcv2, benesse, 7.2+, portátiles
editor: markdown
dateCreated: 2023-09-04T12:19:18.622Z
---

![](/emulators/handheld/pcv2.svg){.align-center}

## Datos técnicos

* **Fabricante**: Benesse Corporation
* **Año de lanzamiento**: 2000
* **Procesador**: 16Bits NEC V20
* **RAM**: 512k vídeo
* **Resolución**: 224 x 144 píxeles
* **Sonido**: 4 canales PCM, 32 muestras de 4 bits por canal.

## Presentación

La Pocket Challenge v2 es una consola portátil de Benesse Corporation, una empresa japonesa centrada en la educación. La consola se creó como dispositivo educativo. Benesse Corporation, propietaria de Berlitz Language Schools, ha lanzado varios juegos de aprendizaje de idiomas. Pocket Challenge v2 es totalmente compatible con Bandai WonderSwan, diseñada por Gunpei Yokoi, el mismo genio que creó Nintendo Game & Watch y Nintendo GameBoy. Al poder jugar a juegos tan famosos como Final Fantasy, Gunpay, Klonoa, Mobile Suit Gundam o Pocket Fighters, la Pocket Challenge era una de las consolas educativas más divertidas.

## Emuladores

[Libretro Mednafen_WSwan](libretro-mednafen-wswan)