---
title: Playstation 2
description: Sony PlayStation 2
published: true
date: 2024-08-27T22:05:19.777Z
tags: sony, playstation 2, ps2, consolas
editor: markdown
dateCreated: 2023-09-03T20:12:37.232Z
---

![](/emulators/consoles/playstation2.svg){.align-center}

## Detalles técnicos

* **Fabricante**: Sony Computer Entertainment
* **Año de lanzamiento**: 2000
* **Cantidad vendida**: 158 millones
* **Juego más vendido**: Gran Theft Auto: San Andreas
* **Procesador**: Emotion Engine basado en R5900 a 294-299 MHz
* **Procesador gráfico**: Sintetizador gráfico a 149 MHz
* **RAM**: 32 MB de RDRAM de doble canal PC800 de 32 bits a 400 MHz
* **RDRAM de vídeo**: 4 MB
* **Resolución** : 256x224 a 1920x1080
* **Medios**: DVD, CD

## Presentación

La **PlayStation 2** (abreviada oficialmente **PS2**) es una videoconsola de sexta generación comercializada por Sony Computer Entertainment, filial de Sony. Salió a la venta el 4 de marzo de 2000 en Japón, el 26 de octubre de 2000 en Norteamérica, el 24 de noviembre de 2000 en Europa y el 30 de noviembre de 2000 en Australia. La consola competía con la Dreamcast de Sega, la GameCube de Nintendo y la Xbox de Microsoft.

PlayStation 2 sucedió a PlayStation en la gama PlayStation. Fue un éxito instantáneo, vendiendo un millón de unidades en pocos días en Japón. PlayStation 2 alcanzó un total de 150 millones de unidades vendidas a 31 de enero de 2011, lo que la convierte en la consola doméstica más vendida de la historia de los videojuegos. Sony afirma que hay 10.828 títulos de videojuegos disponibles en la consola y que se han vendido 1.520 millones de copias de estos títulos desde el lanzamiento de la consola. A finales de 2009, tras casi una década en el mercado, Sony explicó que la PlayStation 2 permanecería en el mercado mientras hubiera compradores para sus juegos. A ésta le siguió la PlayStation 3 en 2006.

La PlayStation 2 es hoy la consola más vendida de la historia de los videojuegos.

Más de doce años después de su lanzamiento, Sony anunció oficialmente que dejaba de distribuir la PlayStation 2 en Japón el 28 de diciembre de 2012 (seguía vendiéndose honrosamente, situándose en el Top 10) y en el resto del mundo el 4 de enero de 2013.

Esto marcó un antes y un después en la historia de los videojuegos, y también para el fabricante japonés, que había causado sensación con el lanzamiento de esta consola en marzo de 2000, en un momento en el que incluir de serie la reproducción de DVD (un formato aún novedoso por aquel entonces) en este tipo de dispositivos suponía una gran innovación.

## Emuladores

[Libretro PCSX2](libretro-pcsx2)
[PCSX2](pcsx2)
