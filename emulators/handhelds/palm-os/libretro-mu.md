---
title: Libretro Mu
description: 
published: true
date: 2024-07-25T00:22:07.210Z
tags: libretro, palm, palm-os, mu
editor: markdown
dateCreated: 2021-07-27T15:51:15.340Z
---

**Libretro Mu** is a Palm Os emulator developed in C and C++.

The continuation of the Core is dedicated to Emily (1998-2020).

## ![](/emulators/license.svg) License

This core is under [**Creative Commons Attribution-NonCommercial 3.0 United States**](https://github.com/libretro/Mu#license) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Features



## ![](/emulators/bios.svg) BIOS

### List of mandatory bios

| Filename | Description | MD5 | Provided |
| :---: | :---: | :---: | :---: |
| palmos41-en-m515.rom | Palm OS 4.1 BIOS | 83cb1d1c76e568b916dc2e7c0bf669f6 | ❌ |

### List of optional bios

| Filename | Description | MD5 | Provided |
| :---: | :---: | :---: | :---: |
| bootloader-dbvz.rom | Palm Bootloader | 9da101cd2317830649a31f8fa46debec | ❌ |

### Location

Put the bios like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 🗒 **palmos41-en-m515.rom**
┃ ┃ ┃ ┣ 🗒 **bootloader-dbvz.rom**

## ![](/emulators/roms.png) Roms

### Supported extensions

Roms must have the extension:

* .prc
* .pdb
* .pqa
* .img

### Location

Put the roms like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 palm
┃ ┃ ┃ ┃ ┣ 🗒 **game.prc**

>The roms using **No-Intro** format are highly recommended.
{.is-success}

>For more information about roms, go to [this tutorial](./../../../tutorials/games/generalities/isos-and-roms)!
{.is-info}

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Accessing to options

You can configure various options in two different ways.

* Via RetroArch menu:

┣ 📁 RetroArch Menu
┃ ┣ 📁 Core options
┃ ┃ ┣ 🧩 Name_option

* Via the file `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core options

| Options | Options values | Variables (for configuration override) | Variables values |
| --- | --- | --- | --- |
| CPU Speed | `1.0` ✅ / `1.5` / `2.0` / `2.5` / `3.0` / `0.5` | `palm_emu_cpu_speed` | `1.0` / `1.5` / `2.0` / `2.5` / `3.0` / `0.5` |
| Force Match System Clock | `Disabled` ✅ / `Enabled` | `palm_emu_feature_synced_rtc` | `disabled` / `enabled` |
| Ignore Invalid Behavior | `Disabled` ✅ / `Enabled` | `palm_emu_feature_durable` | `disabled` / `enabled` |
| Use Left Joystick As Mouse | `Disabled` ✅ / `Enabled` | `palm_emu_use_joystick_as_mouse` | `disabled` / `enabled` |
| Disable Graffiti Area | `Disabled` ✅ / `Enabled` | `palm_emu_disable_graffiti` | `disabled` / `enabled` |
| OS Version | `Palm m515/Palm OS 4.1` ✅ / `Tungsten T3/Palm OS 5.2.1` / `Tungsten T3/Palm OS 6.0` / `Palm m500/Palm OS 4.0` | `palm_emu_os_version` | `Palm m515/Palm OS 4.1` / `Tungsten T3/Palm OS 5.2.1` / `Tungsten T3/Palm OS 6.0` / `Palm m500/Palm OS 4.0` |

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/libretro/Mu/](https://github.com/libretro/Mu/)
* **Official source code**: [https://github.com/meepingsnesroms/Mu/](https://github.com/meepingsnesroms/Mu/)
* **Official website**: [https://meepingsnesroms.github.io/](https://meepingsnesroms.github.io/)