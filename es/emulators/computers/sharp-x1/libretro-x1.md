---
title: Libretro X1
description: 
published: true
date: 2024-07-26T12:51:26.825Z
tags: libretro, x1
editor: markdown
dateCreated: 2023-09-11T11:41:44.194Z
---



## ![](/emulators/license.svg) Licencia

El core posee la siguiente licencia [**BSD-3-Clause**](https://github.com/libretro/xmil-libretro/blob/master/LICENSE).

## ![](/emulators/compatibility.png) Compatibilidad

 | RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | 
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | 

## ![](/emulators/features.png) Funcionalidades



## ![](/emulators/bios.svg) BIOS

### Lista de BIOS obligatorias

| Nombre del fichero | Descripción | MD5 | ¿Preinstalada? |
| :---: | :---: | :---: | :---: |
| IPLROM.X1 | - | eeeea1cd29c6e0e8b094790ae969bfa7 59074727a953fe965109b7dbe3298e30 | ❌ |
| IPLROM.X1T | - | 56c28adcf1f3a2f87cf3d57c378013f5 | ❌ |

### Emplazamiento

Disponed vuestras bios como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 xmil
┃ ┃ ┃ ┃ ┣ 🗒 **IPLROM.X1**
┃ ┃ ┃ ┃ ┣ 🗒 **IPLROM.X1T**

## ![](/emulators/roms.png) Roms

### Extensiones de roms soportadas

Las roms deben tener las extensiones siguientes:

* .dx1
* .2d
* .2hd
* .tfd
* .d88
* .88d
* .hdm
* .xdf
* .dup
* .cmd
* .m3u
* .zip
* .7z

Este sistema soporta las roms comprimidas en formato .zip/.7z. No obstante, se trata sólo de un archivo.

Los ficheros contenidos en los .zip/.7z deben tener las extensiones citadas previamente.
Además, cada fichero .zip/.7z sólo debe contener un fichero rom al interior.

### Emplazamiento

Disponed vuestras roms como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 x1
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.zip**

>Se aconsejan las roms del proyecto **TOSEC**.
{.is-success}

>Para más información sobre las roms, consultad [este tutorial](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

>Para conservar las configuraciones personalizadas cuando se produce una actualización del sistema Recalbox, os aconsejamos utilizar las [Sobrecargas de configuración](./../../../advanced-usage/configuration-override).
{.is-info}

### Acceder a las opciones

Podéis configurar las opciones del core de dos formas diferentes.

* Via el Menú RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via el fichero `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Opciones del core

| Opción | Valor de la opción | Variable (para la sobrecarga) | Valores posibles |
| --- | --- | --- | --- |
| Resolution | `LOW` ✅ / `HIGH` | `X1_RESOLUTE` | `LOW` / `HIGH` |
| Boot media | `2HD` ✅ / `2D` | `X1_BOOTMEDIA` | `2HD` / `2D` |
| ROM type | `X1` ✅ / `TURBO` / `TURBOZ` | `X1_ROMTYPE` | `X1` / `TURBO` / `TURBOZ` |
| FPS | `AUTO` ✅ / `60` / `30` / `20` / `15` | `X1_FPS` | `AUTO` / `60` / `30` / `20` / `15` |
| Disp Vsync | `Disabled` ✅ / `Enabled` | `X1_DISPSYNC` | `OFF` / `ON` |
| Raster | `Disabled` ✅ / `Enabled` | `X1_RASTER` | `OFF` / `ON` |
| No wait | `Disabled` ✅ / `Enabled` | `X1_NOWAIT` | `OFF` / `ON` |
| Joy Reverse | `Disabled` ✅ / `Enabled` | `X1_BTN_MODE` | `OFF` / `ON` |
| Joy Rapid | `Disabled` ✅ / `Enabled` | `X1_BTN_RAPID` | `OFF` / `ON` |
| Audio sampling rate | `44100` ✅ / `22050` / `11025` | `X1_AUDIO_RATE` | `44100` / `22050` / `11025` |
| Seek Sound | `Disabled` ✅ / `Enabled` | `X1_SEEKSND` | `OFF` / `ON` |
| Scanlines in low-res | `Disabled` ✅ / `Enabled` | `X1_SCANLINES` | `OFF` / `ON` |
| Key mode | `Keyboard` ✅ / `JoyKey-1` / `JoyKey-2` / `Mouse-Key` | `X1_KEY_MODE` | `Keyboard` / `JoyKey-1` / `JoyKey-2` / `Mouse-Key` |
| FM Board | `Disabled` / `Enabled` ✅ | `X1_FMBOARD` | `OFF` / `ON` |
| Audio buffer (ms) | `100` / `150` / `200` / `250` ✅ / `300` / `350` / `500` / `750` / `1000` | `X1_AUDIO_DELAYMS` | `100` / `150` / `200` / `250` / `300` / `350` / `500` / `750` / `1000` |
| Cpu clock (MHz) | `1` / `2` / `3` / `4` ✅ / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` | `X1_CPU_CLOCK` | `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` |

## ![](/emulators/external-links.png) Enlaces externos

* **Código fuente utilizado** : [https://github.com/libretro/xmil-libretro/](https://github.com/libretro/xmil-libretro/)