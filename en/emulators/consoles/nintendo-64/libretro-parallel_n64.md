---
title: Libretro ParaLLEl_n64
description: 
published: true
date: 2021-10-12T21:43:25.831Z
tags: n64, libretro, nintendo 64, parallel n64
editor: markdown
dateCreated: 2021-08-01T12:17:32.699Z
---

**Libretro ParaLLEl_n64** is based on Mupen64Plus and has been especially rewritten/optimized for Libretro.

## ![](/emulators/license.svg) License

This core is under [**GPLv3**](https://github.com/libretro/mupen64plus-libretro/blob/master/LICENSE) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ❌ | ❌ | ✅ | ✅ | ✅ | ❌ | ❌ | ✅ |

## ![](/emulators/features.png) Features



## ![](/emulators/bios.svg) BIOS

>**No bios is required.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Supported extensions

Roms must have the extension:

* .n64
* .v64
* .z64
* .bin
* .u1
* .zip
* .7z

This system supports compressed roms in .zip/.7z format. However, be careful, it is only an archive.

The files contained in the .zip/.7z must correspond to the extensions mentioned above.
Moreover, each .zip/.7z file must contain only one compressed rom.

### Location

Put the roms like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 n64
┃ ┃ ┃ ┃ ┣ 🗒 **game.zip**

>The roms using **No-Intro** format are highly recommended.
{.is-success}

>For more information about roms, go to [this tutorial](./../../../tutorials/games/generalities/isos-and-roms)!
{.is-info}

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Accessing to options

You can configure various options in two different ways.

* Via RetroArch menu:

┣ 📁 RetroArch Menu
┃ ┣ 📁 Core options
┃ ┃ ┣ 🧩 Name_option

* Via the file `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core options

| Options | Options values | Variables (for configuration override) | Variables values |
| --- | --- | --- | --- |
| CPU Core | `dynamic_recompiler` ✅ / `cached_interpreter` / `pure_interpreter` | `parallel-n64-cpucore` | `dynamic_recompiler` / `cached_interpreter` / `pure_interpreter` |
| Audio Buffer Size (restart) | `2048` ✅ / `1024` | `parallel-n64-audio-buffer-size` | `2048` / `1024` |
| Analog Deadzone (percent) | `0` / `5` / `10` / `15` ✅ / `20` / `25` / `30` | `parallel-n64-astick-deadzone` | `0` / `5` / `10` / `15` / `20` / `25` / `30` |
| Analog Sensitivity (percent) | `50` / `55` / `60` / `65` / `70` / `75` / `80` / `85` / `90` / `95` / `100` ✅ / `105` / `110` / `115` / `120` / `125` / `130` / `135` / `140` / `145` / `150` / `200` | `parallel-n64-astick-sensitivity` | `50` / `55` / `60` / `65` / `70` / `75` / `80` / `85` / `90` / `95` / `100` / `105` / `110` / `115` / `120` / `125` / `130` / `135` / `140` / `145` / `150` / `200` |
| Player 1 Pak | `none` / `memory` ✅ / `rumble` | `parallel-n64-pak1` | `none` / `memory` / `rumble` |
| Player 2 Pak | `none` ✅ / `memory` / `rumble` | `parallel-n64-pak2` | `none` / `memory` / `rumble` |
| Player 3 Pak | `none` ✅ / `memory` / `rumble` | `parallel-n64-pak3` | `none` / `memory` / `rumble` |
| Player 4 Pak | `none` ✅ / `memory` / `rumble` | `parallel-n64-pak4` | `none` / `memory` / `rumble` |
| Enable Expansion Pak RAM | `Enabled` ✅ / `Disabled` | `parallel-n64-disable_expmem` | `enabled` / `disabled` |
| GFX Accuracy (restart) | `veryhigh` ✅ / `high` / `medium` / `low` | `parallel-n64-gfxplugin-acccuracy` | `veryhigh` / `high` / `medium` / `low` |
| Send audio lists to HLE RSP | `Disabled` ✅ / `Enabled` | `parallel-n64-send_allist_to_hle_rsp` | `disabled` / `enabled` |
| GFX Plugin | `auto` ✅ / `glide64` / `gln64` / `rice` / `angrylion` | `parallel-n64-gfxplugin` | `auto` / `glide64` / `gln64` / `rice` / `angrylion` |
| RSP Plugin | `auto` ✅ / `hle` / `cxd4` | `parallel-n64-rspplugin` | `auto` / `hle` / `cxd4` |
| Resolution (restart) | `320x240` / `640x480` ✅ / `960x720` / `1280x960` / `1440x1080` / `1600x1200` / `1920x1440` / `2240x1680` / `2880x2160` / `5760x4320` | `parallel-n64-screensize` | `320x240` / `640x480` / `960x720` / `1280x960` / `1440x1080` / `1600x1200` / `1920x1440` / `2240x1680` / `2880x2160` / `5760x4320` |
| Aspect ratio hint (reinit) | `normal` ✅ / `widescreen` | `parallel-n64-aspectratiohint` | `normal` / `widescreen` |
| (Glide64) Texture Filtering | `automatic` ✅ / `N64 3-point` / `bilinear` / `nearest` | `parallel-n64-filtering` | `automatic` / `N64 3-point` / `bilinear` / `nearest` |
| (Angrylion) Dithering | `Enabled` ✅ / `Disabled` | `parallel-n64-dithering` | `enabled` / `disabled` |
| (Glide64) Polygon Offset Factor | `-5.0` / `-4.5` / `-4.0` / `-3.5` / `-3.0` ✅ / `-2.5` / `-2.0` / `-1.5` / `-1.0` / `-0.5` / `0.0` / `0.5` / `1.0` / `1.5` / `2.0` / `2.5` / `3.0` / `3.5` / `4.0` / `4.5` / `5.0` | `parallel-n64-polyoffset-factor` | `-5.0` / `-4.5` / `-4.0` / `-3.5` / `-3.0` / `-2.5` / `-2.0` / `-1.5` / `-1.0` / `-0.5` / `0.0` / `0.5` / `1.0` / `1.5` / `2.0` / `2.5` / `3.0` / `3.5` / `4.0` / `4.5` / `5.0` |
| (Glide64) Polygon Offset Units | `-5.0` / `-4.5` / `-4.0` / `-3.5` / `-3.0` ✅ / `-2.5` / `-2.0` / `-1.5` / `-1.0` / `-0.5` / `0.0` / `0.5` / `1.0` / `1.5` / `2.0` / `2.5` / `3.0` / `3.5` / `4.0` / `4.5` / `5.0` | `parallel-n64-polyoffset-units` | `-5.0` / `-4.5` / `-4.0` / `-3.5` / `-3.0` / `-2.5` / `-2.0` / `-1.5` / `-1.0` / `-0.5` / `0.0` / `0.5` / `1.0` / `1.5` / `2.0` / `2.5` / `3.0` / `3.5` / `4.0` / `4.5` / `5.0` |
| (Angrylion) VI Overlay | `Filtered` ✅ / `AA+Blur` / `AA+Dedither` / `AA only` / `Unfiltered` / `Depth` / `Coverage` | `parallel-n64-angrylion-vioverlay` | `Filtered` / `AA+Blur` / `AA+Dedither` / `AA only` / `Unfiltered` / `Depth` / `Coverage` |
| (Angrylion) Thread sync level | `Low` ✅ / `Medium` / `High` | `parallel-n64-angrylion-sync` | `Low` / `Medium` / `High` |
| (Angrylion) Multi-threading | `all threads` ✅ / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `21` / `22` / `23` / `24` / `25` / `26` / `27` / `28` / `29` / `30` / `31` / `32` / `33` / `34` / `35` / `36` / `37` / `38` / `39` / `40` / `41` / `42` / `43` / `44` / `45` / `46` / `47` / `48` / `49` / `50` / `51` / `52` / `53` / `54` / `55` / `56` / `57` / `58` / `59` / `60` / `61` / `62` / `63` | `parallel-n64-angrylion-multithread` | `all threads` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `21` / `22` / `23` / `24` / `25` / `26` / `27` / `28` / `29` / `30` / `31` / `32` / `33` / `34` / `35` / `36` / `37` / `38` / `39` / `40` / `41` / `42` / `43` / `44` / `45` / `46` / `47` / `48` / `49` / `50` / `51` / `52` / `53` / `54` / `55` / `56` / `57` / `58` / `59` / `60` / `61` / `62` / `63` |
| (Angrylion) Hide overscan | `Disabled` ✅ / `Enabled` | `parallel-n64-angrylion-overscan` | `disabled` / `enabled` |
| VI Refresh (Overclock) | `auto` ✅ / `1500` / `2200` | `parallel-n64-virefresh` | `auto` / `1500` / `2200` |
| Buffer Swap | `Disabled` ✅ / `Enabled` | `parallel-n64-bufferswap` | `disabled` / `enabled` |
| Framerate (restart) | `original` ✅ / `fullspeed` | `parallel-n64-framerate` | `original` / `fullspeed` |
| Independant C-button Controls | `Disabled` ✅ / `Enabled` | `parallel-n64-alt-map` | `disabled` / `enabled` |
| (Glide64) Vertex cache VBO (restart) | `Disabled` ✅ / `Enabled` | `parallel-n64-vcache-vbo` | `disabled` / `enabled` |
| Boot Device | `Default` ✅ / `64DD IPL` | `parallel-n64-boot-device` | `Default` / `64DD IPL` |
| 64DD Hardware | `Disabled` ✅ / `Enabled` | `parallel-n64-64dd-hardware` | `disabled` / `enabled` |

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/libretro/parallel-n64/](https://github.com/libretro/parallel-n64/)