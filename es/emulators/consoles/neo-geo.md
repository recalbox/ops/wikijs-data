---
title: Neo-Geo
description: SNK Neo-Geo
published: true
date: 2024-07-26T07:55:24.171Z
tags: snk, neo-geo, consolas
editor: markdown
dateCreated: 2023-09-03T19:36:52.210Z
---

![](/emulators/consoles/neogeo.svg){.align-center}

## Detalles técnicos

* **Fabricante**: SNK Corporation
* **Año de lanzamiento**: 1990
* **Cantidad vendida**: 1 millón
* **Juego más vendido**: The King of Fighters '95
* **Procesador**: Motorola 68000 @ 12MHz, Zilog Z80A @ 4MHz
* **RAM**: 64 KB
* **RAM de audio**: 2 KB
* **RAM de vídeo**: 84 KB
* **Resolución** : 320×224 de resolución, 4096 colores de 65536
* **Chip de sonido** : Yamaha YM2610

## Presentación

La **Neo-Geo**, también conocida como **Neo-Geo Advanced Entertainment System** o **Neo-Geo AES**, es una consola de videojuegos lanzada en 1990.

Diseñada por la compañía japonesa SNK, es técnicamente idéntica al sistema arcade Neo-Geo MVS, con el que comparte una biblioteca de juegos común. La consola es conocida por su biblioteca de juegos arcade 2D de alta calidad, muchos de los cuales son juegos de lucha, así como por su elevado precio y su larga vida útil. A la vista de su precio y de sus características en comparación con las consolas rivales de la misma época, algunos observadores la consideran el "Rolls Royce" de las consolas.

La Neo-Geo es famosa por la calidad de sus juegos, que siguen disfrutándose muchos años después de su lanzamiento. Entre los cerca de ciento cincuenta juegos lanzados, destacan varias series:

* Art of Fighting (combate)
* Fatal Fury (lucha)
* King of Fighters (lucha)
* Magical Drop (juego de puzzle)
* Metal Slug (correr y disparar)
* Samurai Shodown (lucha con espadas)

### Neo-Geo MVS

El **Neo-Geo MVS** o **Neo-Geo Multi Video System** es un sistema de videojuegos arcade compatible con JAMMA creado por la compañía japonesa SNK en paralelo con la consola doméstica Neo-Geo AES, que comparte la misma base de hardware. La calidad de los juegos y las acertadas elecciones técnicas hicieron de la Neo-Geo un éxito, uno de los sistemas arcade más famosos y vendidos del mundo.

## Emuladores

[FBA2x](fba2x)
[Libretro FBNeo](libretro-fbneo)
[Libretro Geolith](libretro-geolith)
[Libretro MAME2000](libretro-mame-2000)
[Libretro MAME2003](libretro-mame-2003-plus)
[Libretro MAME2003_Plus](libretro-mame-2003-plus)
[Libretro MAME2010](libretro-mame-2010)
[Libretro MAME2015](libretro-mame-2015)

## Enlaces externos

[Wikipedia : Neo-Geo AES](https://fr.wikipedia.org/wiki/Neo-Geo_AES)
[Wikipedia : Neo-Geo MVS](https://fr.wikipedia.org/wiki/Neo-Geo_MVS)

