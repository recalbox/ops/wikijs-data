---
title: Resolución de problemas
description: 
published: true
date: 2024-06-02T19:48:58.730Z
tags: resolucion de problemas
editor: markdown
dateCreated: 2023-09-03T14:08:56.088Z
---

## Controladores (mandos)

## Mi mando funciona en EmulationStation pero no cuando intento jugar

Intentamos configurar los mandos para que podáis utilizar EmulationStation sin hacer nada, pero para que funcionen correctamente con los emuladores, se deben reconfigurar.

Para ello, abrid el menú de EmulationStation con el botón `START` y dirigíros hacia `CONFIGURACIÓN DE MANDOS` > `CONFIGURAR UN MANDO`.

### El mando PS3 parpadea pero no consigo realizar la asociación

* Enchufad el mando con un cable USB y esperad 10 segundos.
* Desenchufad el mando y apretad el botón `PS`.

### El mando PS3 parece muerto

* Reiniciad el mando a travès de un pequeño botón que se encuentra dentro de un agujerito por la parte de atrás, usando un clip o un alfiler.

## Presentación

### Border negro extraño, imagen demasiado grande

* Utilizando los menús de vuestra televisión, configurad la talla de la imagen a un ratio de 1:1 (o pantalla completa).

Si esto no funciona, intentad activar la sobreexploración (overscan) de la pantalla desde los menús de EmulationStation. Más información en [este tutorial](./../../tutorials/video/display-configuration/image-size-settings-overscan-tft).

### Pantalla negra en los monitores PC

Si se os presenta una pantalla negra en el monitor (HDMI o DVI), modificad el fichero `recalbox-user-config.txt` (que se actualiza al inicio del sistema) y poned un `#` delante de la línea `hdmi_drive = 2` (en caso de que no exista). Encontraréis más información [en este tutorial](./../../tutorials/video/lcd/dvi-screen).

## Sistema

### Acceso root

* Os podéis conectar por [SSH](./../../tutorials/system/access/root-access-terminal-cli) a vuestra Recalbox. Si disponéis de un teclado podéis utilizar `F4` desde la lista de todos los sistemas de EmulationStation, y a continuación pulsad `ALT` + `F2`. Obtendréis información detallada sobre todo esto consultado [este tutorial](./../../tutorials/system/access/root-access-terminal-cli).