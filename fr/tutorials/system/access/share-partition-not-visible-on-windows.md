---
title: La partition « share » n'est pas visible dans Windows
description: 
published: true
date: 2024-04-14T12:10:04.257Z
tags: share, partition, roms, windows, visible, sd card
editor: markdown
dateCreated: 2022-04-24T20:44:56.059Z
---

Quelquefois, pour une raison ignorée, Windows refuse d'afficher une des 2 partitions que Windows peut lire. Il existe toutefois un moyen de pouvoir forcer Windows à les afficher.

## Pré-requis

Vous devez avoir :

- Démarré au moins une fois votre Recalbox pour terminer son installation.
- Le support de stockage souhaité branché sur votre PC
- Démarré Windows

## Procédure

La plupart du temps, c'est la partition contenant les jeux qui ne s'affiche pas. L'inverse est possible mais moins souvent.

- Sur le bureau Windows 10+, faites un clic droit sur le bouton Démarrer et sélectionnez `Gestion des disques`.

![](/tutorials/system/access/share1.png){.full-width}

- Une fenêtre va s'ouvrir. Dans le bas de la fenêtre, trouvez la partition à forcer (`RECALBOX` ou `SHARE`) et faites un clic droit dessus pour choisir l'option `Assigner une lettre au lecteur`.

![](/tutorials/system/access/share2.png){.full-width}

## Windows XP et 7

Pour les Windows datant d'avant Windows 10, il y a quand même un moyen de pouvoir accéder à la partition share. Vous devez avant trouver la mise à jour avec le terme `KB955704` (Microsoft ne le fournit plus sur son site, vous devez le trouver par vous-même).