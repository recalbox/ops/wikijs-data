---
title: Test your joystick with sdl2-jstest
description: 
published: true
date: 2021-08-07T13:28:31.728Z
tags: test, sdl2-jstest, joystick
editor: markdown
dateCreated: 2021-08-07T13:28:31.728Z
---

If you have a problem with a controller, there is a command that allows you to see its properties.

## Description

To test your controller, you must :

* Connect via [SSH](./../../system/access/root-access-terminal-cli)
* Run the following command:

```shell
sdl2-jstest
```

## List the details of your controllers

Command to use:

```shell
sdl2-jstest --list
```

Result:

```shell
Joystick Name: 'DragonRise Inc.   Generic   USB  Joystick'
Joystick Path: '/dev/input/event0'
Joystick GUID: 030000001008000001e5000010010000
Joystick Number: 0
Number of Axes: 2
Number of Buttons: 10
Number of Hats: 0
Number of Balls: 0
GameController:
not a gamepad
Axis code 0: 0
Axis code 1: 1
Button code 0: 288
Button code 1: 289
Button code 2: 290
Button code 3: 291
Button code 4: 292
Button code 5: 293
Button code 6: 294
Button code 7: 295
Button code 8: 296
Button code 9: 297​
```

## Test your controller

Command to use:

```shell
sdl2-jstest -e 0
```

Here, 0 is the number of your controller detected with the command `sdl2-jstest --list`.

You can find an example with an N64 controller by going [here](https://forum.recalbox.com/topic/9016/a-lire-manettes-n64).