---
title: Kodi Media Center
description: 
published: true
date: 2021-07-17T21:31:32.743Z
tags: 
editor: markdown
dateCreated: 2021-07-16T21:35:59.618Z
---

![](/basic-usage/features/kodi/kodilogo.png)

Kodi é um espaço multimídia integrado ao Recalbox.

Você pode se conectar a uma grande variedade de *streams* através de uma conexão Ethernet para desfrutar de filmes, música e muito mais.

As mídias ao vivo e gravadas podem ser acessadas com uma instalação KODI devidamente configurada. Da mesma forma, você pode simplesmente conectar-se à sua própria mídia local via USB ou rede.

>Os controles agora são suportados pelo Kodi, mas se preferir, você pode usar o HDMI CEC ou um aplicativo remoto para smartphone. 
{.is-info}

## Base

### Início e desligamento

Você pode iniciar o **Kodi Media Center** pressionando o botão `X` (no EmulationStation), ou, pressionando `START` + selecionando a primeira entrada do menu (KODI).

Para sair do KODI, navegue até o ícone de energia no canto superior esquerdo do menu principal do KODI e selecione um dos dois:

* "Power Off System" (Desligar Sistema)
* "Reboot". (Reiniciar)

Ambos os itens retornarão ao menu principal do EmulationStation; siga os comandos normais da EmulationStation se desejar desligar o Recalbox.

Você não pode desligar o Recalbox diretamente pelo KODI, você precisa voltar ao EmulationStation se realmente quiser desligar o Recalbox.

### Novos usuários do KODI :

No primeiro início, o KODI tem apenas algumas opções. Você precisa de uma conexão Ethernet, a menos que você esteja usando somente mídia local ou conectada a USB.

A [Wiki do KODI](http://kodi.wiki/view/Main_Page) contém mais informações sobre as especificidades da criação e uso do KODI. O KODI é bastante intuitivo.
Normalmente, ele encontra a mídia USB automaticamente quando você entra nas opções Vídeo ou Música; você deve ver sua mídia USB conectada ou um caminho para ela.

Além disso, o Vídeo, a Música e outras abas têm "Addons" (semelhantes a uma aplicação).  
Existem vários addons que você pode baixar diretamente do registro principal dentro do próprio KODI, e vários outros registros (coleção de addons) que os usuários criaram, então você pode ter que "cavar um pouco mais fundo" para adicionar algum conteúdo impressionante.

* Para obter addons de música ou vídeo, navegue até as abas "Addons", e vá para a aba "Music" ou "Video" e clique nelas, ou,
* Selecione a aba principal clicando nela e você verá todas as mídias ou dispositivos conectados localmente com "Addons" já instalados. Clique em "Addons" e encontre o botão que diz "Get More" (Obter mais). Isto te levará a uma lista de "Addons" aprovados pelo KODI. Basta selecionar qual lhe interessa e clicar em "Instalar".

Isto acontecerá em segundo plano e se você tiver uma conexão Ethernet decente, o download e a instalação normalmente levarão menos de um minuto.

Se você tiver suporte de rede, você precisa configurar estes caminhos nas configurações do sistema de arquivos.  
Novamente, todos eles estão bem documentados na [Wiki](http://kodi.wiki/view/Main_Page) própria do kodi, e há muitos vídeos disponíveis sobre como configurá-lo.

Notas:

* Há algumas opções no arquivo de [configuração](./.../.../basic-usage/getting-started/recalboxconf-file) se você quiser inicializar diretamente no KODI todas as vezes ao invés do EmulationStation (padrão).
* Algumas pessoas vendem dongles ou instruções para obter conteúdo em seu KODI, mas estes são apenas coleções ou diretórios pré-montados que são gratuitos se você mesmo os encontrar.
* Alguns add-ons podem exigir uma assinatura, como Netflix ou Amazon Video, outros podem simplesmente exigir que você crie uma conta, como Pandora.
* Se seus controles enlouquecem quando você entra no KODI, eles não estão devidamente configurados. Reveja a configuração do controle; seção do [Controles no Recalbox](./../../basic-usage/first-use-and-configuration#configurando-um-controle)
* A versão Raspberry Pi do KODI não tem todas as características e complementos, mas a maioria está presente.
* Na primeira vez e toda vez que você inicia o KODI, ele realiza atualizações ou verificações em segundo plano. Isto pode causar o uso irregular ou retardado de algumas características. Normalmente, é preciso esperar um pouco para usar algumas funções. Quanto mais addons você tiver, mais tempo pode levar e alguns podem ser definidos para não serem automatizados.
* A versão atual do KODI Recalbox é "Leia".
* Clique aqui para suporte ao [SMB v2+ no KODI](https://discourse.osmc.tv/t/kodi-and-smbv1/37441/5).

## Opções

O arquivo [recalbox.conf](./../../basic-usage/getting-started/recalboxconf-file) contém algumas opções para o Kodi. Você pode ativar ou desativar o Kodi, ativar o atalho do botão X ou iniciar automaticamente o Kodi na inicialização. Você também pode fazer isto a partir do EmulationStation (`START` > `CONFIGURAÇÕES AVANÇADAS` > `CONFIGURAÇÕES DO KODI`).
 
>Os controle são suportados pelo Kodi. 


> Você pode usar a função **HDMI CEC** de sua TV para controlar a Kodi a partir de seu controle remoto ou **Yatse**, um controle remoto do Kodi por smartphone .
{.is-info}

## Diversos

### Gestão de energia com HDMI CEC

Se você tentar sair ou desligar o Kodi, seu Raspberry sempre enviará um comando de desligamento HDMI para seu receptor A/V + TV. Como resultado, todos os seus "dispositivos" serão desligados.

Para evitar este comportamento, você deve desativar o ajuste "Desligar dispositivo na saída", indo para "Sistema" -> "Ajustes" -> "Sistema" -> "Dispositivos de entrada" -> "Dispositivos" -> "Adaptador CEC".

O adaptador CEC, em alguns casos, permitirá que você controle o KODI através de seu controle remoto habitual de TV se todos eles suportarem CEC. O CEC envia comandos remotos via cabo HDMI VIA para seu IP. [Wiki do CEC no Kodi](http://kodi.wiki/view/CEC)

### Configuração do Yatse

Yatse e algumas outras aplicações são controles remotos para o KODI. Estas aplicações muitas vezes oferecem mais controle, e um controle mais intuitivo do KODI. Isto não é obrigatório, mas se você gosta do KODI, provavelmente irá desfrutar mais da experiência com essa aplicação remota.

Primeiro você deve iniciar o KODI no seu Recalbox.

Após instalar a aplicação Yatse, Yatse procurará em sua rede as instalações KODI disponíveis.

![](/basic-usage/features/kodi/configure_yatse_for_kodi_recalbox_1-300px.png)

Você pode pular esta etapa e ir para a configuração manual. Basta definir o [endereço IP](./.../.../tutorial/network/ip/discover-recalbox-ip) do Recalbox em sua rede e a porta para **8081**.

![](/basic-usage/features/kodi/configure_yatse_for_kodi_recalbox_2-300px.png)

Seu controle Yatse agora está conectado:

![](/basic-usage/features/kodi/configure_yatse_for_kodi_recalbox_3-300px.png)

Se você gosta do Yatse, compre o **Unlocker** para obter novas funcionalidades.

### Configuração de joystick

A configuração entre o joystick e a ação no KODI pode ser alterada no arquivo `/recalbox/share/system/configs/kodi/input.xml`, utilizando as ações e funções listadas aqui: [http://kodi.wiki/view/Keymap#Commands](http://kodi.wiki/view/Keymap#Commands)

Os botões em seu controlador têm este mapeamento:

| Botão do joystick | Função |
| ---: | :--- |
| `A` | Selecionar |
| `B` | Retornar |
| `X` | Parar |
| `Y` | Menu |
| `START` | Pausar |
| `SELECT` | Menu de contexto |
| `CIMA` | Cima |
| `BAIXO` | Baico |
| `DIREITA` | Direita |
| `ESQUERDA` | Esquerda |
| `STICK ESQUERDO PARA CIMA` | Aumentar o volume |
| `STICK ESQUERDO PARA BAIXO` | Reduzir o volume |
| `STICK ESQUERDO PARA ESQUERDA` | Reduzir o volume |
| `STICK ESQUERDO PARA DIREITA` | Aumentar o volume |
| `STICK DIREITO PARA CIMA` | CIMA |
| `STICK DIREITO PARA BAIXO` | BAIXO |
| `STICK DIREITO PARA ESQUERDA` | ESQUERDA |
| `STICK DIREITO PARA DIREITA` | DIREITA |

### Sem som na saída dos fones de ouvido :

Se você configurou o Kodi para utilizar a saída de áudio dos fones de ouvido mas não está obtendo nenhum som, tente colocar estas variáveis de configuração no arquivo `/boot/recalbox-user-config.txt` :

```ini
hdmi_ignore_edid_audio=1
hdmi_force_edid_audio=0
```

Isso forçará o áudio a passar pela saída dos fones de ouvido.


