---
title: Manual Netplay Libretro
description: 
published: true
date: 2021-08-01T16:09:03.540Z
tags: 
editor: markdown
dateCreated: 2021-07-22T22:49:11.852Z
---

## A Sala de Jogo em rede

### Host:

* Na lista de jogos, pressione o botão `X` enquanto seleciona o jogo desejado.
* O menu de senha será exibido.

![](/basic-usage/features/netplay/libretro-netplay1-pt.png){.full-width}

As opções são as seguintes:

* `DEFINIR SENHA DO JOGADOR`: selecione ON se você quiser colocar uma senha de jogador em seu jogo.

  * As pessoas que quiserem participar como jogador precisarão ter a mesma senha de jogador que o host.

* `DEFINIR SENHA DO ESPECTADOR`: selecione ON se você quiser definir uma senha do espectador em seu jogo.

  * As pessoas que quiserem participar como espectadores precisarão ter a mesma senha de espectador que o host.

* `ESCOLHER SENHA DO JOGADOR`: uma seleção de senhas pré-definidas.

  * É claro, você pode personalizar as senhas desta lista.

* `ESCOLHER SENHA DO ESPECTADOR`: uma seleção de senhas pré-definidas.

  * É claro, você pode personalizar as senhas desta lista

Uma vez que você tenha feito suas escolhas:

  * Selecione `START` para iniciar o jogo.
  * Espere pelos jogadores: um pop-up aparece na tela dos usuários do Recalbox que estão conectados para avisá-los de que uma sessão Netplay está disponível.

![](/basic-usage/features/netplay/libretro-netplay2-pt.png){.full-width}

#### Cliente:

* Junte-se a sala de jogos em rede apertando `X` na lista de sistemas. Se o Kodi estiver habilitado, você terá um menu para escolher entre eles.

![](/basic-usage/features/netplay/libretro-netplay3-pt.png){.full-width}

* Selecione o host ao qual você deseja se conectar.

## Informações da Sala

Para cada jogo, você terá informações:

### No lado esquerdo

* **Um sinal** : ✅ (válido) ou ❌ (inválido) em frente ao logotipo do Recalbox ou ao nome da ROM.
* **O logotipo do Recalbox**: se a pessoa usa Recalbox. Entre o sinal e o nome da ROM.
* **O nome da ROM**: o nome exato do arquivo da ROM do host. Após o sinal e o logotipo do Recalbox.

#### No lado direito

#### Resultado básico

* `NOME DO USUÁRIO`: nome do jogador do host
* `PAÍS`: Código do país do host
* `HASH DA ROM`: correspondência da Hash da ROM
* `ARQUIVO ROM`: correspondência do nome do arquivo da ROM
* `NÚCLEO`: emulador usado
* `VERSÃO DO NÚCLEO`: versão do emulador
* `LATÊNCIA`: tempo de resposta
* `RETROARCH VER.`: versão do RetroArch
* `ARQUITETURA DO HOST`: arquitetura do host (você verá o logotipo do Recalbox aqui se o host o usar)

Você verá o resultado na frente de cada item da lista, ✅ (válido) ou ❌ (inválido).

#### Resultado global

Você verá o resultado geral na parte inferior direita da janela, dizendo-lhe as chances de uma conexão bem sucedida:

* **Verde**: você tem a ROM com o hash e o núcleo certos. Todas as chances estão do seu lado para que funcione!

![](/basic-usage/features/netplay/netplay-lobby-ok-pt.png){.full-width}

* **Azul**: hash não corresponde (algumas ROMS não têm hash, como os sistemas de arcade), mas foi encontrado o arquivo certo. Deve funcionar!

![](/basic-usage/features/netplay/netplay-ok-nok-pt.png){.full-width}

* **Vermelho**: arquivo não encontrado, sistema não permitido, núcleo incorreto. Sem chance do jogo on-line funcionar!

![](/basic-usage/features/netplay/netplay-nok-pt.png){.full-width}