---
title: Libretro PrBoom
description: 
published: true
date: 2024-07-25T08:20:49.647Z
tags: libretro, prboom, sigil
editor: markdown
dateCreated: 2021-06-29T22:52:30.786Z
---



## ![](/emulators/license.svg) License

This core is under [**GPLv2**](https://github.com/libretro/libretro-prboom/blob/master/COPYING) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Features



## ![](/emulators/bios.svg) BIOS

>**No bios is required.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Supported extensions

Roms must have the extension:

* .zip

### Location

Put your roms like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 ports
┃ ┃ ┃ ┃ ┣ 📁 Doom
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **SIGIL_v1_21.wad**

This rom is included in your Recalbox.

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Accessing to options

You can configure various options in two different ways.

* Via RetroArch menu:

┣ 📁 RetroArch Menu
┃ ┣ 📁 Core options
┃ ┃ ┣ 🧩 Name_option

* Via the file `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core options

| Options | Options values | Variables (for configuration override) | Variables values |
| --- | --- | --- | --- |
| Internal resolution (restart) | `320x200` ✅ / `640x400` / `960x600` / `1280x800` / `1600x1000` / `1920x1200` / `2240x1400` / `2560x1600` | `prboom-resolution` | `320x200` / `640x400` / `960x600` / `1280x800` / `1600x1000` / `1920x1200` / `2240x1400` / `2560x1600` |
| Mouse active when using Gamepad | `Disabled` ✅ / `Enabled` | `prboom-mouse_on` | `disabled` / `enabled` |
| Look on parent folders for IWADs | `Enabled` ✅ / `Disabled` | `prboom-find_recursive_one` | `enabled` / `disabled` |
| Rumble Effects | `Disabled` ✅ / `Enabled` | `prboom-rumble` | `disabled` / `enabled` |
| Analog Deadzone (percent) | `0` / `5` / `10` / `15` ✅ / `20` / `25` / `30` | `prboom-analog_deadzone` | `0` / `5` / `10` / `15` / `20` / `25` / `30` |

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/libretro/libretro-prboom/](https://github.com/libretro/libretro-prboom)
* **Libretro documentation**:  [https://docs.libretro.com/library/prboom/](https://docs.libretro.com/library/prboom/)
* **Official website**: [http://prboom.sourceforge.net/about.html](http://prboom.sourceforge.net/about.html)