---
title: Libretro TyrQuake
description: 
published: true
date: 2024-06-23T12:09:02.792Z
tags: libretro, quake 1, tyrquake
editor: markdown
dateCreated: 2021-05-21T08:33:30.472Z
---

## ![](/emulators/license.svg) Licence

Ce core est sous licence [**GPLv2**](https://github.com/libretro/tyrquake/blob/master/gnu.txt).

## ![](/emulators/compatibility.png) Compatibilité

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Fonctionnalités



## ![](/emulators/bios.svg) BIOS

>**Aucun bios n'est requis.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Extensions supportées

Les roms doivent avoir l'extension :

* .pak

### Emplacement

Placez les roms comme ceci : 

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 ports
┃ ┃ ┃ ┃ ┣ 📁 Quake
┃ ┃ ┃ ┃ ┃ ┣ 📁 id1
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 **pak0.pak**

Cette rom est fournie dans votre Recalbox.

## ![](/emulators/advanced-configuration.png) Configuration avancée de l'émulateur

>Pour pouvoir conserver vos configurations personnalisées lors d'une mise à jour, nous vous conseillons d'utiliser notre fonctionnalité [Surcharges de configuration](./../../../advanced-usage/configuration-override).
{.is-info}

### Accéder aux options

Vous pouvez configurer diverses options de deux façons différentes.

* Via le Menu RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via le fichier `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Options du core

| Options | Valeur des options | Variable (pour surcharge) | Valeurs des variables |
| --- | --- | --- | --- |
| Internal resolution(restart) | `320x200` ✅ / `320x240` / `320x480` / `360x200` / `360x240` / `360x400` / `360x480` / `400x224` / `400x240` / `480x272` / `512x224` / `512x240` / `512x384` / `512x512` / `640x224` / `640x240` / `640x448` / `640x400` / `640x480` / `720x576` / `800x480` / `800x600` / `960x720` / `960x600` / `1024x768` / `1280x720` / `1280x800` / `1600x900` / `1600x1000` / `1600x1200` / `1920x1080` / `1920x1200` | `tyrquake_resolution` | `320x240` / `320x480` / `360x200` / `360x240` / `360x400` / `360x480` / `400x224` / `400x240` / `480x272` / `512x224` / `512x240` / `512x384` / `512x512` / `640x224` / `640x240` / `640x448` / `640x400` / `640x480` / `720x576` / `800x480` / `800x600` / `960x720` / `960x600` / `1024x768` / `1280x720` / `1280x800` / `1600x900` / `1600x1000` / `1600x1200` / `1920x1080` / `1920x1200` |
| Framerate (restart) | `Auto` ✅ / `10fps` / `15fps` / `20fps` / `25fps` / `30fps` / `40fps` / `50fps` / `60fps` / `72fps` / `75fps` / `90fps` / `100fps` / `119fps` / `120fps` / `144fps` / `155fps` / `160fps` / `165fps` / `180fps` / `200fps` / `240fps` / `244fps` / `300fps` / `360fps` | `tyrquake_framerate` | `auto` / `10` / `15` / `20` / `25` / `30` / `40` / `50` / `60` / `72` / `75` / `90` / `100` / `119` / `120` / `144` / `155` / `160` / `165` / `180` / `200` / `240` / `244` / `300` / `360` |
| Colored Lighting (restart) | `Désactivé` ✅ / `Activé` | `tyrquake_colored_lighting` | `disabled` / `enabled` |
| Rumble | `Désactivé` ✅ / `Activé` | `tyrquake_rumble` | `disabled` / `enabled` |
| Invert Y Axis | `Désactivé` ✅ / `Activé` | `tyrquake_invert_y_axis` | `disabled` / `enabled` |
| Analog Deadzone (percent) | `0` / `5` / `10` / `15` ✅ / `20` / `25` / `30` | `tyrquake_analog_deadzone` | `0` / `5` / `10` / `15` / `20` / `25` / `30` |

## ![](/emulators/external-links.png) Liens externes

* **Code source utilisé** : [https://github.com/libretro/tyrquake/](https://github.com/libretro/tyrquake/)
* **Documentation Libretro** : [https://docs.libretro.com/library/tyrquake/](https://docs.libretro.com/library/tyrquake/)