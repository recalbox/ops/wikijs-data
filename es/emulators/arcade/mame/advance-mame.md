---
title: ​AdvanceMAME
description: 
published: true
date: 2024-06-29T17:38:44.402Z
tags: mame, advancemame
editor: markdown
dateCreated: 2023-09-05T15:29:55.373Z
---

**AdvanceMAME** es un emulador standalone, añadido inicialmente para la configuración de pantallas CRT. Está basado en el romset MAME 0.106.

## ![](/emulators/license.svg) Licencia

El core posee la siguiente licencia [**GPLv2**](https://github.com/amadvance/advancemame/blob/master/COPYING).

## ![](/emulators/compatibility.png) Compatibilidad

 | RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
 | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ❌ | ✅ |

## ![](/emulators/features.png) Funcionalidades

| Funcionalidad | ¿Soportada? |
| :---: | :---: |
| Saves |  |
| States |  |
| Rewind |  |
| Netplay |  |
| RetroAchievements |  |
| RetroArch Cheats |  |
| Native Cheats |  |
| Controllers |  |
| Multi-Mouse |  |
| Rumble |  |
| Sensors |  |
| Camera |  |
| Location |  |
| Subsystem |  |

## ![](/emulators/bios.svg) BIOS

Las romsets de BIOS no son necesarias si utilizáis las romsets de arcade "Full Non-Merged". Para las romset de tipo "Split" y "Non-Merged", disponed las BIOS necesarias en la misma carpeta que la romset del juego.

>**Aviso** :
>Poned las bios dentro del directorio : `/recalbox/share/roms/mame/`   
>o dentro de una subcarpeta si utilizáis un directorio diferente por core Mame, por ejemplo: `/recalbox/share/roms/mame/AdvanceMame/`
Los juegos junto con sus bios deben encontrarse dentro del mismo directorio o subdirectorio.
{.is-warning}

## ![](/emulators/roms.png) Roms

* Basado en el romset: MAME 0.106 (Mai 2006)
* Fichero dat : el fichero dat se encuentra dentro de vuestra recalbox en el directorio `/recalbox/share/arcade/advancemame/advancemame.dat`.

### Emplazamiento

Disponed vuestras roms como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 mame
┃ ┃ ┃ ┃ ┣ 🗒 **juego.zip**

También podéis utilizar el subdirectorio `advancemame`, si queréis clasificar vuestros fullsets por el core que las hace funcionar. Si utilizáis este subdirectorio vuestros juegos se lanzarán automáticamente bajo el emulador `advancemame`.


┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 mame
┃ ┃ ┃ ┃ ┣ 📁 advancemame
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **juego.zip**

## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

>Para conservar las configuraciones personalizadas cuando se produce una actualización del sistema Recalbox, os aconsejamos utilizar las [Sobrecargas de configuración](./../../../advanced-usage/configuration-override).
{.is-info}
>
>Para obtener más información sobre las roms, consultad [este tutorial](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/external-links.png) Enlaces externos

* **Progettosnaps** : [https://www.progettosnaps.net/](https://www.progettosnaps.net/)
* **Código fuente utilizado** : [https://github.com/amadvance/advancemame/](https://github.com/amadvance/advancemame)
* **Página oficial** : [http://www.advancemame.it/](http://www.advancemame.it/)