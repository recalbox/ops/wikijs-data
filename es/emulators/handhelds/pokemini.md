---
title: Pokémon mini
description: Nintendo Pokémon mini
published: true
date: 2024-07-26T08:34:40.774Z
tags: nintendo, pokémon, mini, pokémini, portátiles
editor: markdown
dateCreated: 2023-09-04T12:20:54.783Z
---

![](/emulators/handheld/pokemini.svg){.align-center}

## Detalles técnicos

* **Fabricante**: Nintendo
* **Año de lanzamiento**: 2001
* **Juego más vendido**: Pokémon Party mini
* **Procesador**: S1C88 a 4 MHz
* **RAM**: 4 kB (compartida con el subsistema de vídeo)
* **Pantalla** : LCD monocromo de 96 x 64 píxeles
* **Sonido**: sonido PWM de 1 canal con 3 niveles de sonido
* **Tamaño del cartucho**: 512 kB

## Presentación

La **Pokémon mini** es una videoconsola portátil diseñada y distribuida por Nintendo y basada en la franquicia Pokémon. Es la videoconsola de cartucho más pequeña producida por Nintendo. Tiene una pantalla en blanco y negro y cuenta con reloj, vibrador y sensor de golpes. También dispone de conexión por infrarrojos para juegos multijugador.

La consola está diseñada para niños pequeños. Los juegos oficiales son sencillos y cortos, y todos tienen temática Pokémon. Nintendo ha anunciado juegos ajenos a la franquicia Pokémon, en particular adaptaciones de Game and Watch, pero nunca han salido a la venta.

La consola ha sido rediseñada para que puedan desarrollarse en ella juegos de aficionados. Sólo hay diez juegos disponibles en esta consola. Japón es el único país que tiene todos los juegos, Europa sólo tiene cinco y Estados Unidos sólo cuatro.

## Emuladores

[Libretro Pokémini](libretro-pokemini)