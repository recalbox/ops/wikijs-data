---
title: Libretro XRick
description: 
published: true
date: 2024-07-25T23:03:03.678Z
tags: libretro, rick dangerous, xrick
editor: markdown
dateCreated: 2023-09-12T06:27:01.136Z
---

**Free Xrick** es una implementación de código abierto del juego "Rick Dangerous".

## ![](/emulators/license.svg) Licencia

El core posee la siguiente licencia [**GPLv3**](https://github.com/libretro/xrick-libretro/blob/master/README).

## ![](/emulators/compatibility.png) Compatibilidad

 | RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | 
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | 

## ![](/emulators/features.png) Funcionalidades

| Funcionalidad | ¿Soportada? |
| :---: | :---: |
| Capturas de pantalla | ✔ |
| Controles | ✔ |

## ![](/emulators/bios.svg) BIOS

>**No necesita bios.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Extensiones de roms soportadas

Las roms deben tener las extensiones siguientes:

* .zip

### Emplazamiento

Disponed vuestras roms como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 ports
┃ ┃ ┃ ┃ ┣ 📁 Rick Dangerous
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **Rick Dangerous.zip**

Esta rom se encuentra por defecto en vuestra Recalbox.

## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

>Para conservar las configuraciones personalizadas cuando se produce una actualización del sistema Recalbox, os aconsejamos utilizar las [Sobrecargas de configuración](./../../../advanced-usage/configuration-override).
{.is-info}

### Acceso a las opciones
 
 Podéis configurar las opciones del core de dos formas diferentes.
 
 * A través del menu RetroArch :
 
 ┣ 📁 Menu RetroArch
 ┃ ┣ 📁 Options du core
 ┃ ┃ ┣ 🧩 Name_option
 
 * A través del fichero `retroarch-core-options.cfg`:
 
 ┣ 📁 recalbox
 ┃ ┣ 📁 share
 ┃ ┃ ┣ 📁 system
 ┃ ┃ ┃ ┣ 📁 configs
 ┃ ┃ ┃ ┃ ┣ 📁 retroarch
 ┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**
 
 ### Opciones del core
 
| Opción | Valor de la opción | Variable (para la sobrecarga) | Valores posibles |
 | --- | --- | --- | --- |
 | Crop Borders | `Activé` ✅ / `Désactivé` | `xrick_crop_borders` | `enabled` / `disabled` |
 | Trainer Mode | `Désactivé` ✅ / `Activé` | `xrick_cheat1` | `disabled` / `enabled` |
 | Invulnerability Mode | `Désactivé` ✅ / `Activé` | `xrick_cheat2` | `disabled` / `enabled` |
 | Expose Mode | `Désactivé` ✅ / `Activé` | `xrick_cheat3` | `disabled` / `enabled` |

## ![](/emulators/external-links.png) Enlaces externos

* **Código fuente utilizado** : [https://github.com/libretro/xrick-libretro/](https://github.com/libretro/xrick-libretro)
* **Código fuente oficial** : [https://github.com/r-type/xrick-libretro/](https://github.com/r-type/xrick-libretro)
* **Página web oficial** : [http://www.bigorno.net/xrick/](http://www.bigorno.net/xrick/)