---
title: FAQ
description: 
published: true
date: 2024-11-04T20:22:21.971Z
tags: system, famicom, disk, faq, face
editor: markdown
dateCreated: 2021-08-08T07:11:26.239Z
---

## Change disk side

### Introduction

Games are written on both sides of a floppy disk, and you have to turn it over to read the other side.

### Usage

Its use depends on the emulator you're using.

#### Libretro FBNeo

The `L3` and `R3` buttons allow for the first one to eject the disk (`L3`) and for the second to change the side and read the new side (`R3`). So you have to press the combinaison `L3` then `R3` in a row to change the side.

#### Others system emulators

The `L1` and `R1` buttons allow for the first one to eject the disk (`R1`) and for the second to change the side (`L1`). So you have to press the combination `R1` then `L1` then `R1` (eject + swap + read) in a row to change the side.