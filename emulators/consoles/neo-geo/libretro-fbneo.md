---
title: Libretro FBNeo
description: 
published: true
date: 2024-07-24T13:54:01.501Z
tags: libretro, fbneo, neo-geo
editor: markdown
dateCreated: 2021-08-01T09:30:51.925Z
---

**FinalBurn Neo**, an emulator for arcade games and some consoles. It is based on the FinalBurn emulators and the old versions of [MAME](https://www.mamedev.org/) (emulating less arcade cabinets) but it works on the same principles.

FB Neo (a.k.a. FinalBurn Neo) is an arcade emulator that supports the following platforms:

* Capcom CPS-1
* Capcom CPS-2
* Capcom CPS-3
* Cave
* Games based on Data East DEC-0, DEC-8, Cassette System, DECO IC16 and DECO-32
* Hardware based on Galaxian
* Irem M62, M63, M72, M90, M92 and M107 hardware
* Kaneko 16
* Konami
* Namco Mappy, System 86, System 1 & 2 and others
* Neo-Geo
* NMK16
* Pacman-based hardware
* PGM
* Psikyo 68EC020 and SH-2 based hardware
* Sega System 1, System 16 (and similar), System 18, X-Board and Y-Board
* Seta-Sammy-Visco (SSV) based hardware
* Super Kaneko Nova System
* Toaplan 1
* Toaplan 2
* Taito F2, F3, X, Z and others
* Various drivers for other hardware
* FB Neo also contains working drivers for the following systems:
  * Sega Megadrive/Genesis
  * ColecoVision
  * Sega SG-1000
  * Sega Master System
  * Game Gear
  * MSX-1
  * ZX Spectrum
  * PC-Engine
  * TurboGrafx 16
  * SuperGrafx
  * Fairchild Channel F.

## ![](/emulators/license.svg) License

This core is under [**non-commercial**](https://github.com/finalburnneo/FBNeo/blob/master/src/license.txt) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Features

| Feature | Supported |
| :---: | :---: |
| Saves | ✔ |
| States | ✔ |
| Rewind | ✔ |
| Netplay | ✔ |
| RetroAchievements | ✔ |
| RetroArch Cheats | ✔ |
| Native Cheats | ✔ |
| Controllers | ✔ |
| Multi-Mouse | ✔ |
| Subsystem | ✔ |

## ![](/emulators/bios.svg) Bios

### List of mandatory bios

| Filename | Description | MD5 | Provided |
| :---: | :---: | :---: | :---: |
| neogeo.zip | Neo-Geo BIOS | e023d0a4b5249fdff2a4620c28963944 8cbdff53661f41ddbe486a98915e1ec9 42e359e394f0be1d61b94ddca0efbe6c c1e0a738d266926f604186aa9d63e4db d12bba59121229ff13702878b415cb7c 2b4ffdf24d5d4f24c4b116c50c4bafc3 005b843b6e70b939c2cca41887cbc371 42e359e394f0be1d61b94ddca0efbe6c 4b9432a4571d03cc8bae9d24d8d6eb40 4cd64313e26537c04cdb561b8f84b0e4 50bbfd6bf297038b085e29265c65723a 76ee7390ccd537cddea1018025ca29a8 84d6c35aad790a77ba3cfc1b319e1061 912c0a56bafe0fba39d0c668b139b250 94748719c0ef31e7375b0c357d82fc89 ab25b9e25052a6064901a7a221037eb6 ae9e5825cde82c58e39dfd48534f7060 b18f83feef474b8a1306ef199cd810a2 b4519df27a352c2a6e42e06d31330d91 bf00272e7150b31156a8995d60bf185d ca66fd463ef7dcab492a6de8ce5f45eb f4a125538dfd7a8044e025fc5188fb88 a3b2c9a69c28ad0bd4ea07877b744bbe | ❌ |

### Location

Put your bios like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 neogeo
┃ ┃ ┃ ┃ ┣ 🗒 **bios.zip**

## ![](/emulators/roms.png) Roms

### Supported extensions

All FinalBurn Neo games use the following game formats:

* .zip
* .7z

### Location

Put your roms like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 neogeo
┃ ┃ ┃ ┃ ┣ 🗒 **game.zip**

>Games can be run also from `/recalbox/share/roms/fbneo` but running them from the folder `neogeo` can optimize its usage to be in the right system.
{.is-info}

>For more information about roms, go to [this tutorial](./../../../tutorials/games/generalities/isos-and-roms)!
{.is-info}

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Accessing to options

You can configure various options in two different ways.

* Via RetroArch menu:

┣ 📁 RetroArch Menu
┃ ┣ 📁 Core options
┃ ┃ ┣ 🧩 Name_option

* Via the file `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core options

| Options | Options values | Variables (for configuration override) | Variables values |
| --- | --- | --- | --- |
| Use 32-bits color depth when available | `Disabled` / `Enabled` ✅ / | `fbneo-allow-depth-32` | `disabled` / `enabled` |
| Vertical mode | `Disabled` ✅ / `Enabled` / `alternate` | `fbneo-vertical-mode` | `disabled` / `enabled` / `alternate` |
| Force 60Hz | `Disabled` ✅ / `Enabled` | `fbneo-force-60hz` | `disabled` / `enabled` |
| Allow patched romsets | `Disabled` / `Enabled` ✅ | `fbneo-allow-patched-romsets` | `disabled` / `enabled` |
| Analog Speed | `25%` / `30%` / `35%` / `40%` / `45%` / `50%` / `55%` / `60%` / `65%` / `70%` / `75%` / `80%` / `85%` / `90%` / `95%` / `100%` ✅ / `105%` / `110%` / `115%` / `120%` / `125%` / `130%` / `135%` / `140%` / `145%` / `150%` / `155%` / `160%` / `165%` / `170%` / `175%` / `180%` / `185%` / `190%` / `195%` / `200%` / `205%` / `210%` / `215%` / `220%` / `225%` / `230%` / `235%` / `240%` / `245%` / `250%` / `255%` / `260%` / `265%` / `270%` / `275%` / `280%` / `285%` / `290%` / `295%` / `300%` / `305%` / `310%` / `315%` / `320%` / `325%` / `330%` / `335%` / `340%` / `345%` / `350%` / `355%` / `360%` / `365%` / `370%` / `375%` / `380%` / `385%` / `390%` / `395%` / `400%` | `fbneo-analog-speed` | `25%` / `30%` / `35%` / `40%` / `45%` / `50%` / `55%` / `60%` / `65%` / `70%` / `75%` / `80%` / `85%` / `90%` / `95%` / `100%` / `105%` / `110%` / `115%` / `120%` / `125%` / `130%` / `135%` / `140%` / `145%` / `150%` / `155%` / `160%` / `165%` / `170%` / `175%` / `180%` / `185%` / `190%` / `195%` / `200%` / `205%` / `210%` / `215%` / `220%` / `225%` / `230%` / `235%` / `240%` / `245%` / `250%` / `255%` / `260%` / `265%` / `270%` / `275%` / `280%` / `285%` / `290%` / `295%` / `300%` / `305%` / `310%` / `315%` / `320%` / `325%` / `330%` / `335%` / `340%` / `345%` / `350%` / `355%` / `360%` / `365%` / `370%` / `375%` / `380%` / `385%` / `390%` / `395%` / `400%` |
| Crosshair emulation | `hide with lightgun device` ✅ / `always hide` / `always show` | `fbneo-lightgun-crosshair-emulation` | `hide with lightgun device` / `always hide` / `always show` |
| CPU clock | `25%` / `30%` / `35%` / `40%` / `45%` / `50%` / `55%` / `60%` / `65%` / `70%` / `75%` / `80%` / `85%` / `90%` / `95%` / `100%` ✅ / `105%` / `110%` / `115%` / `120%` / `125%` / `130%` / `135%` / `140%` / `145%` / `150%` / `155%` / `160%` / `165%` / `170%` / `175%` / `180%` / `185%` / `190%` / `195%` / `200%` / `205%` / `210%` / `215%` / `220%` / `225%` / `230%` / `235%` / `240%` / `245%` / `250%` / `255%` / `260%` / `265%` / `270%` / `275%` / `280%` / `285%` / `290%` / `295%` / `300%` / `305%` / `310%` / `315%` / `320%` / `325%` / `330%` / `335%` / `340%` / `345%` / `350%` / `355%` / `360%` / `365%` / `370%` / `375%` / `380%` / `385%` / `390%` / `395%` / `400%` | `fbneo-cpu-speed-adjust` | `25%` / `30%` / `35%` / `40%` / `45%` / `50%` / `55%` / `60%` / `65%` / `70%` / `75%` / `80%` / `85%` / `90%` / `95%` / `100%` / `105%` / `110%` / `115%` / `120%` / `125%` / `130%` / `135%` / `140%` / `145%` / `150%` / `155%` / `160%` / `165%` / `170%` / `175%` / `180%` / `185%` / `190%` / `195%` / `200%` / `205%` / `210%` / `215%` / `220%` / `225%` / `230%` / `235%` / `240%` / `245%` / `250%` / `255%` / `260%` / `265%` / `270%` / `275%` / `280%` / `285%` / `290%` / `295%` / `300%` / `305%` / `310%` / `315%` / `320%` / `325%` / `330%` / `335%` / `340%` / `345%` / `350%` / `355%` / `360%` / `365%` / `370%` / `375%` / `380%` / `385%` / `390%` / `395%` / `400%` |
| Hiscores | `Disabled` / `Enabled` ✅ | `fbneo-hiscores` | `disabled` / `enabled` |
| Diagnostic Input | `Hold Start` ✅ / `Start + A + B` / `Hold Start + A + B` / `Start + L + R` / `Hold Start + L + R` / `Hold Select` / `Select + A + B` / `Hold Select + A + B` / `Select + L + R` / `Hold Select + L + R` | `fbneo-diagnostic-input` | `Hold Start` / `Start + A + B` / `Hold Start + A + B` / `Start + L + R` / `Hold Start + L + R` / `Hold Select` / `Select + A + B` / `Hold Select + A + B` / `Select + L + R` / `Hold Select + L + R` |
| Neo-Geo mode | `Use bios set in BIOS dipswitch` / `MVS Europe/Asia (English)` / `MVS USA (English - Censored)` / `MVS Japan (Japanese)` / `AES Europe/Asia (English)` / `AES Japan (Japanese)` / `UNIBIOS` | `fbneo-neogeo-mode` | `DIPSWITCH` / `MVS_EUR` / `MVS_USA` / `MVS_JAP` / `AES_EUR` / `AES_JAP` / `UNIBIOS` |
| Memory card mode | `Disabled` / `shared` / `per-game` | `fbneo-memcard-mode` | `disabled` / `shared` / `per-game` |
| Debug Dip 1_1 | `Disabled` ✅ / `Enabled` | `fbneo-debug-dip-1-1` | `disabled` / `enabled` |
| Debug Dip 1_2 | `Disabled` ✅ / `Enabled` | `fbneo-debug-dip-1-2` | `disabled` / `enabled` |
| Debug Dip 1_3 | `Disabled` ✅ / `Enabled` | `fbneo-debug-dip-1-3` | `disabled` / `enabled` |
| Debug Dip 1_4 | `Disabled` ✅ / `Enabled` | `fbneo-debug-dip-1-4` | `disabled` / `enabled` |
| Debug Dip 1_5 | `Disabled` ✅ / `Enabled` | `fbneo-debug-dip-1-5` | `disabled` / `enabled` |
| Debug Dip 1_6 | `Disabled` ✅ / `Enabled` | `fbneo-debug-dip-1-6` | `disabled` / `enabled` |
| Debug Dip 1_7 | `Disabled` ✅ / `Enabled` | `fbneo-debug-dip-1-7` | `disabled` / `enabled` |
| Debug Dip 1_8 | `Disabled` ✅ / `Enabled` | `fbneo-debug-dip-1-8` | `disabled` / `enabled` |
| Debug Dip 2_1 | `Disabled` ✅ / `Enabled` | `fbneo-debug-dip-2-1` | `disabled` / `enabled` |
| Debug Dip 2_2 | `Disabled` ✅ / `Enabled` | `fbneo-debug-dip-2-2` | `disabled` / `enabled` |
| Debug Dip 2_3 | `Disabled` ✅ / `Enabled` | `fbneo-debug-dip-2-3` | `disabled` / `enabled` |
| Debug Dip 2_4 | `Disabled` ✅ / `Enabled` | `fbneo-debug-dip-2-4` | `disabled` / `enabled` |
| Debug Dip 2_5 | `Disabled` ✅ / `Enabled` | `fbneo-debug-dip-2-5` | `disabled` / `enabled` |
| Debug Dip 2_6 | `Disabled` ✅ / `Enabled` | `fbneo-debug-dip-2-6` | `disabled` / `enabled` |
| Debug Dip 2_7 | `Disabled` ✅ / `Enabled` | `fbneo-debug-dip-2-7` | `disabled` / `enabled` |
| Debug Dip 2_8 | `Disabled` ✅ / `Enabled` | `fbneo-debug-dip-2-8` | `disabled` / `enabled` |
| Frameskip | `Disabled` ✅ / `Fixed` / `Auto` / `Manual` | `fbneo-frameskip-type` | `disabled` / `Fixed` / `Auto` / `Manual` |
| Frameskip Threshold (%) | `15` / `18` / `21` / `24` / `27` / `30` / `33` ✅ / `36` / `39` / `42` / `45` / `48` / `51` / `54` / `57` / `60` | `fbneo-frameskip-manual-threshold` | `15` / `18` / `21` / `24` / `27` / `30` / `33` / `36` / `39` / `42` / `45` / `48` / `51` / `54` / `57` / `60` |
| Fixed Frameskip | `No skipping` ✅ / `Skip rendering of 1 frames out of 2` / `Skip rendering of 2 frames out of 3` / `Skip rendering of 3 frames out of 4` / `Skip rendering of 4 frames out of 5` / `Skip rendering of 5 frames out of 6` | `fbneo-fixed-frameskip` | `0` / `1` / `2` / `3` / `4` / `5` |
| Samplerate | `44100` / `48000` ✅ | `fbneo-samplerate` | `44100` / `48000` |
| Sample Interpolation | `Disabled` / `2-point 1st order` / `4-point 3rd order` ✅ | `fbneo-sample-interpolation` | `disabled` / `2-point 1st order` / `4-point 3rd order` |
| FM Interpolation | `Disabled` / `4-point 3rd order` ✅ | `fbneo-fm-interpolation` | `disabled` / `4-point 3rd order` |
| LowPass Filter | `Disabled` ✅ / `Enabled` | `fbneo-lowpass-filter` | `disabled` / `enabled` |

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/libretro/FBNeo/tree/master](https://github.com/libretro/FBNeo/tree/master)
* **Libretro documentation**: [https://docs.libretro.com/library/fbneo/](https://docs.libretro.com/library/fbneo/)
* **Official website**: [https://neo-source.com/](https://neo-source.com/)
* **Core Wiki**: [http://emulation.gametechwiki.com/index.php/FinalBurn_Neo](http://emulation.gametechwiki.com/index.php/FinalBurn_Neo)
* **Officiel wiki**: [https://github.com/finalburnneo/FBNeo/wiki](https://github.com/finalburnneo/FBNeo/wiki)