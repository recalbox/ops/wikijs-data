---
title: Utilisation de votre périphérique XArcade
---



## Dispositifs compatibles et exigences

Les sticks suivants sont supportés :

* X-Arcade Modèle Tankstick
* X-Arcade Dual Stick

## Configurer votre Recalbox

Vous devez modifier recalbox.conf et définir `controllers.xarcade.enabled=1` et redémarrer. Au prochain redémarrage, votre stick sera automatiquement détecté. Essayez vos touches, vous voudrez peut-être remapper votre stick.

## Comment configurer Recalbox via un navigateur web

Connectez votre boîtier à Internet en mode wifi ou filaire. Appuyez sur Start et sélectionnez "Network Settings" pour obtenir votre IP.

Ouvrez n'importe quel navigateur et naviguez vers cette adresse IP ou naviguez vers votre Recalbox.

Dans le menu, recherchez le menu qui vous permet de modifier recalbox.conf. Modifiez les paramètres nécessaires et redémarrez votre Recalbox.

## Utiliser le stick XArcade sur Raspberry Pi 4

Pour utiliser le stick XArcade sur Raspberry Pi 4, vous devez exécuter quelques commandes via SSH. Les commandes sont les suivantes :

```text
mount -o remount,rw /
cp /usr/bin/xarcade2jstick /usr/bin/xarcade2jstick-old
cp xarcade2jstick-rpi4 /usr/bin/xarcade2jstick
chmod 755 /usr/bin/xarcade2jstick
```

