---
title: Configurar vuestra pantallita TFT a travès del bus SPI
description: 
published: true
date: 2023-09-21T14:53:35.722Z
tags: tft, bus, spi, mini, pantalla, pequeña
editor: markdown
dateCreated: 2023-09-14T14:47:20.894Z
---

## Configura tu pantalla TFT capacitiva/resistiva de 2.8" de Adafruit

Esta pantalla se puede encontrar en [este sitio](https://www.adafruit.com/product/1601). Como ya existen overlays para esta pantalla en el directorio `/boot/overlays`, será soportada con una simple configuración.

```ini
pitft28-capacitiva.dtbo
pitft28-resistiva.dtbo
```

Comprueba si tienes la versión capacitiva o resistiva de la pantalla y ajusta la línea `dt-overlay` en el archivo [recalbox-user-config.txt](./../../tutorials/system/modification/configtxt-configuration).

Esta pantalla utiliza el bus SPI para ser manejada. Su baja resolución nos permite obtener un buen frame rate en las frecuencias de emulación. No llega a 60 fps, pero supera el mínimo de 30 fps gracias al programa `fcbp` modificado.

### Modificación para que coincida con la resolución de pantalla

* Necesita abrir el archivo [recalbox-user-config.txt](./../../../tutorials/system/modification/configtxt-configuration).
* Agregue lo siguiente a este archivo:

```ini
dtparam=spi=on
dtparam=i2c1=on
dtparam=i2c_arm=on
hdmi_cvt=320 240 60 1 0 0 0
hdmi_group=2
hdmi_mode=87
# working config for speed
# 900 Mhz -> 48000000
# 1Ghz -> 35000000
#modification of the speed   
dtoverlay=pitft28-resistive,rotate=90,speed=22000000,fps=60
```

Es necesario ajustar la velocidad del bus SPI si overclockeas tu Raspberry Pi para obtener una buena imagen. Estos ajustes han sido probados en una Raspberry Pi1 a 1.1Ghz.

Para empezar, puedes probar esto:

```ini
dtoverlay=pitft28-resistive,rotate=90,speed=32000000,fps=20
```

En el caso de una pantalla capacitiva, basta con sustituirla por lo siguiente:

```ini
dtoverlay=pitft28-capacitive,rotate=90,speed=32000000,fps=20
```

Al reiniciar, la pantalla debería empezar en blanco y, tras unos segundos, volverse negra. Esto es señal de que la pantalla ya es compatible. Pero no obtendrás ninguna imagen hasta que ejecutes `fbcp`.

### Activación de fbcp

* Abrid [recalbox.conf](./../../../basic-usage/getting-started/recalboxconf-file)
* Encontrad la siguiente línea:

```ini
system.fbcp.enabled=0
```

Necesitas deshabilitar los controladores GPIO para usar `fbcp`.

* Cambia esta línea por :

```ini
system.fbcp.enabled=1
```

* Busque la siguiente línea y modifíquela si es necesario:

```ini
controllers.gpio.enabled=0
```

Si estás usando controladores GPIO, puedes en cambio cambiar la configuración de los pines GPIO usando el pin 4 o 5 para establecer el pin usado, evitando los pines usados por tu pantalla. Vea [aquí](./../../../tutoriales/controladores/gpio/gpio-controladores) para más detalles.

* Modifique el archivo [recalbox.conf](./../../../basic-usage/getting-started/recalboxconf-file) como sigue:

```ini
#global.videomode=DMT 87 HDMI
global.videomode=default

#global.ratio=auto
global.ratio=4/3
```

* Guarde el archivo y reinicie.Ahora la pantalla debería funcionar perfectamente. Si tiene algún problema, compruebe primero que el programa `fbcp` funciona con :

```shell
ps aux | grep fbcp
```

Si aparece una línea distinta a la mostrada, todo va bien.

Esta pantalla de 2,8" funciona bien, pero puede ser demasiado pequeña y demasiado cara (35 euros) cuando se trata de clones chinos.Así que vamos a configurar una pantalla TFT de 3,2" de Waveshare, que se puede encontrar en Banggood por menos de 15€.

## Configura tu pantalla TFT capacitiva/resistiva de 3,2" de Waveshare

La pantalla es la siguiente: un módulo de visualización de pantalla táctil LCD TFT de 3,2 pulgadas para la Raspberry Pi B+, B, A+. Su resolución es la misma que la de la pantalla de 2,8": 320x240. Se trata en realidad de una [pantalla Waveshare](https://www.waveshare.com/3.2inch-rpi-lcd-b.htm).

Esta pantalla no está soportada por defecto por los sistemas de archivos del kernel. Necesitaremos añadir otros para que la pantalla funcione. Estos archivos se pueden encontrar [aquí](https://github.com/swkim01/waveshare-dtoverlays).

Consigue los archivos `waveshare35a-overlay.dtb` y `waveshare32b-overlay.dtb` para la pantalla WaveShare 3.2" 320x240 y la pantalla WaveShare 3.5" 320x480 respectivamente. Para los nuevos núcleos de la versión 4.4, tenemos que cambiar el nombre de los archivos dtb a archivos dtbo para que coincidan con el nuevo nombre del árbol de superposición. Renombra `waveshare35a-overlay.dtb` a `waveshare35a.dtbo` y `waveshare32b-overlay.dtb` a `waveshare32b.dtbo` y cópialos en el directorio `/boot/overlays`.

* Abrid [recalbox-user-config.txt](./../../../tutorials/system/modification/configtxt-configuration) con nano o vim y añadir estas líneas:

```ini
#tft screen

#Waveshare 3.2 TFT Screen
#same resolution for hdmi and tft
hdmi_cvt=320 240 60 1 0 0 0
hdmi_group=2                
hdmi_mode=1                 
hdmi_mode=87                

dtparam=spi=on              
dtoverlay=waveshare32b:rotate=270,speed=82000000
```

### Activación de fbcp

* Abrid [recalbox.conf](./../../../basic-usage/getting-started/recalboxconf-file)
* Encontrad la siguiente línea:

```ini
system.fbcp.enabled=0
```

Necesitas deshabilitar los controladores GPIO para usar `fbcp`.

* Cambia esta línea por :

```ini
system.fbcp.enabled=1
```

* Busque la siguiente línea y modifíquela si es necesario:

```ini
controllers.gpio.enabled=0
```

Si estás usando controladores GPIO, puedes en cambio cambiar la configuración de los pines GPIO usando el pin 4 o 5 para establecer el pin usado, evitando los pines usados por tu pantalla. Vea [aquí](./../../../tutoriales/controladores/gpio/gpio-controladores) para más detalles.

* Modifique el archivo [recalbox.conf](./../../../basic-usage/getting-started/recalboxconf-file) como sigue:

```ini
#global.videomode=DMT 87 HDMI
global.videomode=default

#global.ratio=auto
global.ratio=4/3
```

* Guarde el archivo y reinicie.Ahora la pantalla debería funcionar perfectamente. Si tiene algún problema, compruebe primero que el programa `fbcp` funciona con :

```shell
ps aux | grep fbcp
```

Si aparece una línea distinta a la mostrada, todo va bien.

Aquí tenéis un vídeo de una pantalla completamente funcional:

![Waveshare 3,2" résistif TFT](/tutorials/video/tft/waveshare-3-2.jpg)
[https://youtu.be/hcFk_vjQLVo](https://youtu.be/hcFk_vjQLVo)

## ¿Por qué no puedo usar la pantalla TFT capacitiva/resistiva de 3.5" de Waveshare con Recalbox?

Esta pantalla no se puede utilizar para juegos arcade con Recalbox.

El bus SPI no tiene suficiente ancho de banda para manejar esta mayor resolución de 480x320. Si aumentas la velocidad del bus, la pantalla se vuelve inestable (colores, parpadeos). Durante mis pruebas, sólo pude obtener 20-25 FPS. Esta pantalla se puede utilizar con un servidor X con una tasa de fotogramas lenta, pero no en modo arcade, que requiere una tasa de fotogramas más alta.

Como se describe en [este sitio](https://learn.adafruit.com/), no se recomienda usar esta pantalla para jugar. &lt;&lt; Con el doble de píxeles que desplazar por la pantalla, la PiTFT de 3,5" es significativamente más lenta que sus hermanos más compactos y desaconsejamos encarecidamente utilizarla para juegos &lt;&lt;. Ahora ya lo sabe.

Pero si quieres que funcione, puedes hacer lo siguiente:

### Modificar el fichero /boot/recalbox-user-config.txt para que coincida con la resolución de pantalla.

Obtener los ficheros `waveshare35a-overlay.dtb` y `waveshare32b-overlay.dtb` para la pantalla WaveShare 3.2" 320x240 y la pantalla WaveShare 3.5" 320x480 respectivamente. Para los nuevos núcleos de la versión 4.4, tenemos que cambiar el nombre de los archivos dtb a archivos dtbo para que coincidan con el nuevo nombre del árbol de superposición. Renombra `waveshare35a-overlay.dtb` a `waveshare35a.dtbo` y `waveshare32b-overlay.dtb` a `waveshare32b.dtbo` y cópialos en el directorio **/boot/overlays**.

* Necesitas abrir el archivo [recalbox-user-config.txt](./../../../tutorials/system/modification/configtxt-configuration) con nano o vim y añadir las siguientes líneas:

```ini
#tft screen
#Waveshare 3.5 TFT Screen
#same resolution for hdmi and tft
hdmi_cvt=480 320 60 1 0 0 0
hdmi_group=2
hdmi_mode=1
hdmi_mode=87

dtparam=spi=on
dtoverlay=waveshare35a:rotate=270,speed=27000000
# speed=41000000,fps=60 for better FPS, but the colors will look a little weird.
```

### Activación de fbcp

* Abrid [recalbox.conf](./../../../basic-usage/getting-started/recalboxconf-file)
* Encontrad la siguiente línea:

```ini
system.fbcp.enabled=0
```

Necesitas deshabilitar los controladores GPIO para usar `fbcp`.

* Cambia esta línea por :

```ini
system.fbcp.enabled=1
```

* Busque la siguiente línea y modifíquela si es necesario:

```ini
controllers.gpio.enabled=0
```

Si estás usando controladores GPIO, puedes en cambio cambiar la configuración de los pines GPIO usando el pin 4 o 5 para establecer el pin usado, evitando los pines usados por tu pantalla. Vea [aquí](./../../../tutoriales/controladores/gpio/gpio-controladores) para más detalles.

* Modifique el archivo [recalbox.conf](./../../../basic-usage/getting-started/recalboxconf-file) como sigue:

```ini
#global.videomode=DMT 87 HDMI
global.videomode=default

#global.ratio=auto
global.ratio=4/3
```

* Guarde el archivo y reinicie.Ahora la pantalla debería funcionar perfectamente. Si tiene algún problema, compruebe primero que el programa `fbcp` funciona con :

```shell
ps aux | grep fbcp
```

Si aparece una línea distinta a la mostrada, todo va bien.

Aquí tenéis un vídeo de una pantalla funcional de 3,5 pulgadas con problemas de FPS:

![Waveshare 3,5' resistif TFT](/tutorials/video/tft/waveshare-3-5.jpg)
[https://youtu.be/HJXQAEaVhKE](https://youtu.be/HJXQAEaVhKE)

Corriendo a speed=41000000,fps=60; esto es claramente arriesgado, algunas pantallas darán resultados extraños... Funcionará en algunas pantallas dependiendo del controlador del chip integrado.

Según nuestras informaciones, si tenéis la pantalla [LCD 3,5" (C) para Raspberry Pi (480x320 ; 125Mhz)](https://www.waveshare.com/3.5inch-RPi-LCD-C.htm), debería funcionar, pero con la [LCD 3,5" (B) pour Raspberry Pi (480x320 ; IPS)](https://www.waveshare.com/3.5inch-RPi-LCD-B.htm), no podréis obtener 60fps !

![Recalbox 3.5 TFT LCD Raspberry Pi 3 B](/tutorials/video/tft/waveshare-3-5-result.jpg)
[https://www.youtube.com/watch?v=cgznOcvRRqQ](https://www.youtube.com/watch?v=cgznOcvRRqQ)

## ¡Ponga en marcha su pantalla TFT resistiva de 3,5" Waveshare!

Disponible en breve.
Esto es para los valientes: [https://github.com/juj/fbcp-ili9341](https://github.com/juj/fbcp-ili9341)