---
title: Sega Model 3
description: 
published: true
date: 2024-08-19T12:48:51.855Z
tags: arcade, model-3
editor: markdown
dateCreated: 2023-09-01T21:19:01.554Z
---

![](/emulators/arcade/model3.svg){.align-center}

## Especificaciones técnicas

* **Procesador** : IBM 32bits RISC PowerPC 603ev 
  * 66 MHz (Versión 1.0)
  * 100 MHz (Versión 1.5)
  * 166 MHz (versiones 2.0 y 2.1)
* **Procesador gráfico**: 2 × Lockheed Martin Real3D/Pro-1000
* **Renderizado**: 60 millones de píxeles
* **Resolución**: 496 x 384
* **Procesador de sonido**: Motorola 68EC000 sincronizado a 12 MH, 2 × Yamaha SCSP/YM-292F DSP de 128 pasos
* **Interfaz MIDI**
  * 64 voces, 4 canales, máximo 16,5 MB ROM
  * 64 canales PCM
* **RAM**: 1 Mb
* **Tarjeta de sonido opcional** :
  * **DSB1:**
    * **Procesador de sonido** : Zilog Z80
    * **Chip de audio**: NEC uD65654GF102
  * **DSB2:**
    * **CPU**: Motorola 68000
    * **Chip de audio**: NEC uD65654GF102

## Presentación

Con el **Modelo 3**, Sega decidió diseñar un nuevo sistema desde cero. Una vez más, eligieron a la empresa con la que habían establecido una buena relación, ahora llamada división Real3D de Lockheed Martin.

Se les contrató para que ayudaran a desarrollar el nuevo subsistema gráfico, que resultó ser el chipset dual Real3D/Pro-1000, sorprendentemente potente para su época.

## Emuladores

[Supermodel](supermodel)