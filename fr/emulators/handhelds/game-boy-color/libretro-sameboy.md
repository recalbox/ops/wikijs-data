---
title: Libretro SameBoy
description: 
published: true
date: 2024-06-23T11:16:47.415Z
tags: libretro, color, gameboy, sameboy, gbc
editor: markdown
dateCreated: 2021-05-21T08:30:32.612Z
---

**Libretro SameBoy** est un émulateur open source Gameboy (DMG) et Gameboy Color (CGB) extrêmement précis, écrit en C.

* Prend en charge l'émulation Game Boy (DMG); Game Boy Color (CGB) et GBC-Mode; Game Boy Advance (AGB)
* Prend en charge l'émulation précise de haut niveau de Super Game Boy (SGB; NTSC et PAL) et Super Game Boy 2 (SGB2)
* Vous permet de choisir le modèle que vous souhaitez émuler quelle que soit la ROM
* Audio 96 kHz de haute qualité
* Prise en charge de l'économie de batterie
* Enregistrer les états
* Inclut des ROM de démarrage open source pour tous les modèles émulés:

  * Prise en charge complète (et documentation de) toutes les palettes spécifiques au jeu dans la ROM de démarrage CGB / AGB, pour une émulation précise des jeux Game Boy sur une Game Boy Color
  * Prend en charge la sélection manuelle de palette avec des combinaisons de touches, avec 4 nouvelles palettes supplémentaires (direction A + B +)
  * Prend en charge la sélection de palette dans un jeu CGB, le forçant à fonctionner en mode DMG «palettisé», si la ROM le permet
  * Prise en charge des jeux avec un logo non Nintendo dans l'en-tête
  * Pas de longue animation dans la ROM de démarrage DMG

* Quatre paramètres de correction des couleurs
* Trois réglages de filtres audio passe-haut
* Émulation d'horloge en temps réel
* Modes turbo, rembobinage et ralenti
* Précision extrêmement élevée
* Émulation de câble de liaison

## ![](/emulators/license.svg) Licence

Ce core est sous licence [**MIT**](https://github.com/libretro/SameBoy/blob/master/LICENSE).

## ![](/emulators/compatibility.png) Compatibilité

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Fonctionnalités

| Fonctionnalité | Supporté |
| :---: | :---: |
| Redémarrage | ✔ |
| Captures d'écran | ✔ |
| Sauvegardes | ✔ |
| Sauvegardes instantanées | ✔ |
| Rembobinage | ✔ |
| Netplay | ✔ |
| Options du core | ✔ |
| RetroAchievements | ✔ |
| Controls | ✔ |
| Remapping | ✔ |
| Vibration | ✔ |
| Subsystem | ✔ |

## ![](/emulators/bios.svg) BIOS

### Liste des bios optionnels

| Nom de fichier | Description | MD5 | Fourni |
| :---: | :---: | :---: | :---: |
| cgb_boot.bin | BIOS Game Boy Color | b560efd759d87634a03b243f22bba27a | ✅ |

### Emplacement

Placez les BIOS comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 gbc
┃ ┃ ┃ ┃ ┣ 🗒 **cgb_boot.bin**

## ![](/emulators/roms.png) Roms

### Extensions supportées

La rom doit avoir l'extension :

* .bin
* .gbc
* .zip
* .7z

Ce système supporte les roms compressées au format .zip/.7z. Attention toutefois, il ne s'agit que d'une archive.

Les fichiers contenus dans les .zip/.7z doivent correspondre aux extensions citées précédemment.
De plus, chaque fichier .zip/.7z ne doit contenir qu'une seule rom compressée.

### Emplacement

Placez les roms comme ceci : 

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 gbc
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.zip**

>Les roms au format **No-Intro** sont vivement conseillées.
{.is-success}

>Pour plus d'information sur les roms, rendez-vous sur [ce tutoriel](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuration avancée de l'émulateur

>Pour pouvoir conserver vos configurations personnalisées lors d'une mise à jour, nous vous conseillons d'utiliser notre fonctionnalité [Surcharges de configuration](./../../../advanced-usage/configuration-override).
{.is-info}

### Accéder aux options

Vous pouvez configurer diverses options de deux façons différentes.

* Via le menu RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via le fichier `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Options du core

Utilisez ce tableau pour le mode 1 joueur :

| Options | Valeurs des options | Variable (pour surcharge) | Valeurs des variables |
| --- | --- | --- | --- |
| Emulated Model (Requires Restart) | `Auto Detect DMG/CGB` ✅ / `Auto Detect DMG/SGB/CGB` / `Game Boy (DMG-CPU B)` / `Game Boy Color (CPU-CGB C) (Experimental)` / `Game Boy Color (CPU-CGB E)` / `Game Boy Advance` / `Super Game Boy NTSC` / `Super Game Boy PAL` /`Super Game Boy 2` | `sameboy_model` | `Auto` / `Auto (SGB)` / `Game Boy` / `Game Boy Color C` / `Game Boy Color` / `Game Boy Advance` / `Super Game Boy` / `Super Game Boy PAL` / `Super Game Boy 2` |
| Auto Detected SGB Model (Requires Restart) | `Super Game Boy NTSC` ✅ / `Super Game Boy PAL` / `Super Game Boy 2` | `sameboy_auto_sgb_model` | `Super Game Boy` / `Super Game Boy PAL` / `Super Game Boy 2` |
| Real Time Clock Emulation | `Sync to System Clock` ✅ / `Accurate` | `sameboy_rtc` | `sync to system clock` / `accurate` |
| GB Mono Palette | `Greyscale` ✅ / `Lime (Game Boy)` / `Olive (Game Boy Pocket)` / `Teal (Game Boy Light)` | `sameboy_mono_palette` | `greyscale` / `lime` / `olive` / `teal` |
| GBC Color Correction | `Modern - Balanced` ✅ / `Modern - Accurate` / `Modern - Boost Contrast` / `Reduce Contrast` / `Correct Color Curves` / `Harsh Reality (Low Contrast)` / `Désactivé` | `sameboy_color_correction_mode` | `emulate hardware` / `accurate` / `preserve brightness` / `reduce contrast` / `correct curves` / `harsh reality` / `off` |
| Ambient Light Temperature | `1000K (Warmest)` / `1550K` / `2100K` / `2650K` / `3200K` / `3750K` / `4300K` / `4850K` / `5400K` / `5950K` / `6500K (Neutral White)` ✅ / `7050K` / `7600K` / `8150K` / `8700K` / `9250K` / `9800K` / `10350K` / `10900K` / `11450K` / `12000K (Coolest)` | `sameboy_light_temperature` | `1.0` / `0.9` / `0.8` / `0.7` / `0.6` / `0.5` / `0.4` / `0.3` / `0.2` / `0.1` / `0` / `-0.1` / `-0.2` / `-0.3` / `-0.4` / `-0.5` / `-0.6` / `-0.7` / `-0.8` / `-0.9` / `-1.0` |
| Display border | `Always` / `Only for Super Game Boy` ✅ / `Désactivé` | `sameboy_border` | `always` / `Super Game Boy only` / `never` |
| Highpass Filter | `Accurate` ✅ / `Preserve Waveform` / `Désactivé` | `sameboy_high_pass_filter_mode` | `accurate` / `remove dc offset` / `off` |
| Interference Volume | `0%` ✅ / `5%` / `10%` / `15%` / `20%` / `25%` / `30%` / `35%` / `40%` / `45%` / `50%` / `55%` / `60%` / `65%` / `70%` / `75%` / `80%` / `85%` / `90%` / `95%` / `100%` | `sameboy_audio_interference` | `0` / `5` / `10` / `15` / `20` / `25` / `30` / `35` / `40` / `45` / `50` / `55` / `60` / `65` / `70` / `75` / `80` / `85` / `90` / `95` / `100` |
| Rumble Mode | `Always` / `Only for rumble-enabled games` ✅ / `Never` | `sameboy_rumble` | `all games` / `rumble-enabled games` / `never` |

Utilisez ce tableau pour les jeux multi-joueurs :

| Options | Valeurs des options | Variable (pour surcharge) | Valeurs des variables |
| --- | --- | --- | --- |
| Link cable emulation | `Activé` ✅ / `Désactivé` | `sameboy_link` | `enabled` / `disabled` |
| Screen Layout | `Top-Down` ✅ / `Left-Right` | `sameboy_screen_layout` | `top-down` / `left-right` |
| Audio Output | `Game Boy #1` ✅ / `Game Boy #2` | `sameboy_audio_output` | `GameBoy #1` / `GameBoy #2` |
| Emulated model for Game Boy #1 | `Auto Detect DMG/CGB` ✅ / `Auto Detect DMG/SGB/CGB` / `Game Boy (DMG-CPU B)` / `Game Boy Color (CPU-CGB C) (Experimental)` / `Game Boy Color (CPU-CGB E)` / `Game Boy Advance` / `Super Game Boy NTSC` / `Super Game Boy PAL` /`Super Game Boy 2` | `sameboy_model_1` | `Auto` / `Auto (SGB)` / `Game Boy` / `Game Boy Color C` / `Game Boy Color` / `Game Boy Advance` / `Super Game Boy` / `Super Game Boy PAL` / `Super Game Boy 2` |
| Auto Detected SGB Model for Game Boy #1 (Requires Restart) | `Super Game Boy NTSC` ✅ / `Super Game Boy PAL` / `Super Game Boy 2` | `sameboy_auto_sgb_model_1` | `Super Game Boy` / `Super Game Boy PAL` / `Super Game Boy 2` |
| Emulated model for Game Boy #2 | `Auto Detect DMG/CGB` ✅ / `Auto Detect DMG/SGB/CGB` / `Game Boy (DMG-CPU B)` / `Game Boy Color (CPU-CGB C) (Experimental)` / `Game Boy Color (CPU-CGB E)` / `Game Boy Advance` / `Super Game Boy NTSC` / `Super Game Boy PAL` /`Super Game Boy 2` | `sameboy_model_2` | `Auto` / `Auto (SGB)` / `Game Boy` / `Game Boy Color C` / `Game Boy Color` / `Game Boy Advance` / `Super Game Boy` / `Super Game Boy PAL` / `Super Game Boy 2` |
| Auto Detected SGB Model for Game Boy #2 (Requires Restart) | `Super Game Boy NTSC` ✅ / `Super Game Boy PAL` / `Super Game Boy 2` | `sameboy_auto_sgb_model_2` | `Super Game Boy` / `Super Game Boy PAL` / `Super Game Boy 2` |
| GB Mono Palette for Game Boy #1 | `Greyscale` ✅ / `Lime (Game Boy)` / `Olive (Game Boy Pocket)` / `Teal (Game Boy Light)` | `sameboy_mono_palette_1` | `greyscale` / `lime` / `olive` / `teal` |
| GB Mono Palette for Game Boy #2 | `Greyscale` ✅ / `Lime (Game Boy)` / `Olive (Game Boy Pocket)` / `Teal (Game Boy Light)` | `sameboy_mono_palette_2` | `greyscale` / `lime` / `olive` / `teal` |
| GBC Color Correction for Game Boy #1 | `Modern - Balanced` ✅ / `Modern - Accurate` / `Modern - Boost Contrast` / `Reduce Contrast` / `Correct color Curves` / `Harsh Reality (Low Contrast)` / `Désactivé` | `sameboy_color_correction_mode_1` | `emulate hardware` / `accurate` / `preserve brightness` / `reduce contrast` / `correct curves` / `harsh reality` / `off` |
| GBC Color Correction for Game Boy #2 | `Modern - Balanced` ✅ / `Modern - Accurate` / `Modern - Boost Contrast` / `Reduce Contrast` / `Correct color Curves` / `Harsh Reality (Low Contrast)` / `Désactivé` | `sameboy_color_correction_mode_2` | `emulate hardware` / `accurate` / `preserve brightness` / `reduce contrast` / `correct curves` / `harsh reality` / `off` |
| Ambient Light Temperature for Game Boy #1 | `1000K (Warmest)` / `1550K` / `2100K` / `2650K` / `3200K` / `3750K` / `4300K` / `4850K` / `5400K` / `5950K` / `6500K (Neutral White)` ✅ / `7050K` / `7600K` / `8150K` / `8700K` / `9250K` / `9800K` / `10350K` / `10900K` / `11450K` / `12000K (Coolest)` | `sameboy_light_temperature_1` | `1.0` / `0.9` / `0.8` / `0.7` / `0.6` / `0.5` / `0.4` / `0.3` / `0.2` / `0.1` / `0` / `-0.1` / `-0.2` / `-0.3` / `-0.4` / `-0.5` / `-0.6` / `-0.7` / `-0.8` / `-0.9` / `-1.0` |
| Ambient Light Temperature for Game Boy #2 | `1000K (Warmest)` / `1550K` / `2100K` / `2650K` / `3200K` / `3750K` / `4300K` / `4850K` / `5400K` / `5950K` / `6500K (Neutral White)` ✅ / `7050K` / `7600K` / `8150K` / `8700K` / `9250K` / `9800K` / `10350K` / `10900K` / `11450K` / `12000K (Coolest)` | `sameboy_light_temperature_2` | `1.0` / `0.9` / `0.8` / `0.7` / `0.6` / `0.5` / `0.4` / `0.3` / `0.2` / `0.1` / `0` / `-0.1` / `-0.2` / `-0.3` / `-0.4` / `-0.5` / `-0.6` / `-0.7` / `-0.8` / `-0.9` / `-1.0` |
| Highpass Filter for Game Boy #1 | `Accurate` ✅ / `Preserve Waveform` / `Désactivé` | `sameboy_high_pass_filter_mode_1` | `accurate` / `remove dc offset` / `off` |
| Highpass Filter for Game Boy #2 | `Accurate` ✅ / `Preserve Waveform` / `Désactivé` | `sameboy_high_pass_filter_mode_2` | `accurate` / `remove dc offset` / `off` |
| Interference Volume for Game Boy #1 | `0%` ✅ / `5%` / `10%` / `15%` / `20%` / `25%` / `30%` / `35%` / `40%` / `45%` / `50%` / `55%` / `60%` / `65%` / `70%` / `75%` / `80%` / `85%` / `90%` / `95%` / `100%` | `sameboy_audio_interference_1` | `0` / `5` / `10` / `15` / `20` / `25` / `30` / `35` / `40` / `45` / `50` / `55` / `60` / `65` / `70` / `75` / `80` / `85` / `90` / `95` / `100` |
| Interference Volume for Game Boy #2 | `0%` ✅ / `5%` / `10%` / `15%` / `20%` / `25%` / `30%` / `35%` / `40%` / `45%` / `50%` / `55%` / `60%` / `65%` / `70%` / `75%` / `80%` / `85%` / `90%` / `95%` / `100%` | `sameboy_audio_interference_2` | `0` / `5` / `10` / `15` / `20` / `25` / `30` / `35` / `40` / `45` / `50` / `55` / `60` / `65` / `70` / `75` / `80` / `85` / `90` / `95` / `100` |
| Rumble Mode for Game Boy #1 | `Always` / `Only for rumble-enabled games` ✅ / `Never` | `sameboy_rumble_1` | `rumble-enabled games` / `all games` / `never` |
| Rumble Mode for Game Boy #2 | `Always` / `Only for rumble-enabled games` ✅ / `Never` | `sameboy_rumble_2` | `rumble-enabled games` / `all games` / `never` |

## ![](/emulators/external-links.png) Liens externes

* **Code source utilisé** : [https://github.com/libretro/SameBoy/](https://github.com/libretro/SameBoy/)
* **Documentation Libretro** : [https://docs.libretro.com/library/sameboy/](https://docs.libretro.com/library/sameboy/)
* **Code source officiel** : [https://github.com/LIJI32/SameBoy/](https://github.com/LIJI32/SameBoy/)
* **Site officiel** : [https://sameboy.github.io/](https://sameboy.github.io/)