---
title: Joycons usage
description: How to synchronize and use Joy-Cons into Recalbox
published: true
date: 2023-01-18T20:25:52.559Z
tags: joy-con, 7.2+
editor: markdown
dateCreated: 2021-08-07T13:46:54.127Z
---

We will see how to synchronize a pair of Joycons and how to use them separately or together (combined).

## One Joycon per player

On each Joycon, there is a small button to turn on and off the Joycon but this button also allows you to synchronize the Joycon with your Recalbox.

![Sync button on each Joy-Con](/tutorials/controllers/controllers/switch-joycons/joyconconfig1.jpg)

In Recalbox, go to system lists, press **Start** on your usual controller or **Enter** on a keyboard and go to **CONTROLLER SETTINGS**.

![](/tutorials/controllers/controllers/switch-joycons/joyconconfig2-en.png){.full-width}

In this menu, select **PAIR A BLUETOOTH CONTROLLER** and validate. The search for Bluetooth process starts.

![](/tutorials/controllers/controllers/switch-joycons/joyconconfig3-en.png){.full-width}

On the Joycon, press the sync button for a few seconds (usually three seconds) to go in association mode. You should have the four side lights going back and forth.
If leds go off, start again.

![](/tutorials/controllers/controllers/switch-joycons/joyconconfig4-en.png){.full-width}

Once the search for Bluetooth devices is complete, you should see your Joycon in the list.

![](/tutorials/controllers/controllers/switch-joycons/joyconconfig5-en.png){.full-width}

Select the Joycon and validate. Once you get the message **ASSOCIATED CONTROLLER**, press the **SL** and **SR** buttons at the same time. Your Joycon should have 1 side light on. Pairing is successfull

Do the same with the second Joycon by repeating the same steps seen above.

![](/tutorials/controllers/controllers/switch-joycons/joyconconfig6-en.png){.full-width}

Validate the second Joycon to finish its association and press **SL** + **SR** as the last step. Once you have associated your 2 Joycons, they will be visible in the list of controllers.

![](/tutorials/controllers/controllers/switch-joycons/joyconconfig7-en.png){.full-width}

## Joycons in combined mode

To use the two Joycons as one controller, you have to associate both Joycons and, when the leds are flashing, press **L** of the left Joycon and **R** of the right Joycon (L+R) at the same time. Now you have one joystick controller from two Joycons.
If the Joycons were already connected in solo mode, just switch them off, wait about 10 seconds and then switch them on, leds will start blinking and then press **L** + **R** as mentioned before.

![](/tutorials/controllers/controllers/switch-joycons/joyconconfig8-en.png){.full-width}

The joys of playing with Joycons are yours!