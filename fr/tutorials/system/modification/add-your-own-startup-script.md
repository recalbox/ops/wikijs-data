---
title: Ajouter votre propre script au démarrage
description: 
published: true
date: 2023-04-12T17:09:38.807Z
tags: script, ajouter, démarrage
editor: markdown
dateCreated: 2021-05-21T08:41:22.480Z
---

## Introduction

Suivez ces instructions si vous souhaitez créer votre propre script de démarrage qui sera exécuté automatiquement lors du démarrage de Recalbox.

La procédure de démarrage d'un script sera exécutée au démarrage et la procédure d'arrêt à l'arrêt de la Recalbox.

## Utilisation

Vous devez créer un fichier nommé `custom.sh` qui doit se trouver obligatoirement dans `/recalbox/share/system`.

Ce fichier peut contenir :

* Du code shell
* Peut appeler d'autres fichiers tels que des fichiers Python par exemple

## Test

Pour tester si tout fonctionne, exécutez la commande `/etc/init.d/S99custom start`.