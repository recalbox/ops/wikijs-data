---
title: XRoar
description: 
published: true
date: 2024-07-18T18:49:47.087Z
tags: color, xroar, trs-80-coco, 8.0+, trs80, ordenador
editor: markdown
dateCreated: 2023-09-11T11:53:28.459Z
---



## ![](/emulators/license.svg) Licencia

El core posee la siguiente licencia [**GPL-3.0**](https://gitlab.com/recalbox/packages/standalone/xroar/-/blob/master/COPYING.GPL) et [**LGPL-3.0**](https://gitlab.com/recalbox/packages/standalone/xroar/-/blob/master/COPYING.LGPL).

## ![](/emulators/compatibility.png) Compatibilidad

 | RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | 
 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Funcionalidades

| Funcionalidad | ¿Soportada? |
| :---: | :---: |

## ![](/emulators/bios.svg) BIOS

### Lista de BIOS obligatorias

| Nombre del fichero | Descripción | MD5 | ¿Preinstalada? |
| :---: | :---: | :---: | :---: |
| bas11.rom | CoCo 1 BASIC | c73fb4bff9621c5ab17f6220b20db82f | ❌ |
| bas13.rom | CoCo 2 BASIC | c2fc43556eb6b7b25bdf5955bd9df825 | ❌ |
| extbas10.rom | CoCo 1 Extended BASIC | fda72f415afe99b36f953bb9bc1253da | ❌ |
| extbas11.rom | CoCo 2 Extended BASIC | 21070aa0496142b886c562bf76d7c113 | ❌ |

### Emplazamiento

Disponed vuestras bios como en la siguiente imagen: 

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 trs80coco
┃ ┃ ┃ ┃ ┣ 🗒  **bas11.rom**
┃ ┃ ┃ ┃ ┣ 🗒  **bas13.rom**
┃ ┃ ┃ ┃ ┣ 🗒  **extbas10.rom**
┃ ┃ ┃ ┃ ┣ 🗒  **extbas11.rom**

### Lista de BIOS opcionales

| Nombre del fichero | Descripción | MD5 | ¿Preinstalada? |
| :---: | :---: | :---: | :---: |
| bas10.rom | CoCo 1 BASIC | a74f3d95b395dad7cdca19d560eeea74 | ❌ |
| bas12.rom | CoCo 2 BASIC | c933316c7d939532a13648850c1c2aa6 | ❌ |
| disk10.rom | RSDos 1.0 | a64b3ef9efcc066b18d35b134068d1cc | ❌ |
| disk11.rom | RSDos 1.1 | 8cab28f4b7311b8df63c07bb3b59bfd5 | ❌ |
| hdbdw3bck.rom | RSDos Becker | ef750d93d24d7bc87a8ecd6e5a15a845 | ❌ |
| mx1600bas.rom | MX1600 BASIC | 88d1504e93366f498105a846cdbf7fb7 | ❌ |

### Emplazamiento

Disponed vuestras bios como en la siguiente imagen: 

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 trs80coco
┃ ┃ ┃ ┃ ┣ 🗒  **bas10.rom**
┃ ┃ ┃ ┃ ┣ 🗒  **bas12.rom**
┃ ┃ ┃ ┃ ┣ 🗒  **disk10.rom**
┃ ┃ ┃ ┃ ┣ 🗒  **disk11.rom**
┃ ┃ ┃ ┃ ┣ 🗒  **hdbdw3bck.rom**
┃ ┃ ┃ ┃ ┣ 🗒  **mx1600bas.rom**

## ![](/emulators/roms.png) Roms

### Extensiones de roms soportadas

Las roms deben tener las extensiones siguientes:

* .cas
* .wav
* .bas
* .asc
* .dmk
* .jvc
* .os9
* .dsk
* .vdk
* .rom
* .ccc

### Emplazamiento

Disponed vuestras roms como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 trs80coco
┃ ┃ ┃ ┃ ┣ 🗒  **juego.dsk**

>Para más información sobre las roms, consultad [este tutorial](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

>Para conservar las configuraciones personalizadas cuando se produce una actualización del sistema Recalbox, os aconsejamos utilizar las [Sobrecargas de configuración](./../../../advanced-usage/configuration-override).
{.is-info}

## ![](/emulators/external-links.png) Enlaces externos

* **Código fuente utilizado** : [https://gitlab.com/recalbox/packages/standalone/xroar/](https://gitlab.com/recalbox/packages/standalone/xroar/)
* **Página web oficial** : [https://www.6809.org.uk/xroar/](https://www.6809.org.uk/xroar/)