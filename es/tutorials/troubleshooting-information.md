---
title: 🔖 Solución de problemas
description: 
published: true
date: 2024-07-26T07:16:29.010Z
tags: tutorial, problemas, solución
editor: markdown
dateCreated: 2023-09-01T09:32:55.055Z
---

Esta página pertenece a los usuarios más fuertes, para aquellos a los que les gusta escribir en la línea de comandos de SSH. En caso de necesidad, podéis veros en la obligación de ejecutar alguno de estos comandos.

Aquí tenéis la lista de tutoriales disponibles:

[Activar el modo debug](enable-debug-mode)
[Listar los módulos cargados](list-loaded-modules)
[Récuperar las informaciones de vuestros mandos](grab-controllers-informations)
[Script de ayuda para el soporte técnico](support-script)
[Consultar el contenido de los buffers de memoria Linux](display-buffer-memory-linux)