---
title: Amstrad CPC
description: 
published: true
date: 2021-10-03T16:07:51.955Z
tags: amstrad, cpc, ordinateurs
editor: markdown
dateCreated: 2021-05-21T07:55:49.824Z
---

![](/emulators/computers/amstradcpc.svg){.align-center}

## Fiche technique

* **Fabricant** : Amstrad
* **Année de sortie** : 1984
* **Système d'exploitation** : AMSDOS avec Locomotive BASIC
* **Processeur** : Zilog Z80A @ 4 MHz
* **RAM** : 64 KB
* **Puce sonore** : General Instrument AY-3-8910

## Présentation

L'**Amstrad CPC** est un ordinateur personnel 8 bits produit par Amstrad dans les années 1980. CPC est le sigle de Colour Personal Computer, « ordinateur personnel couleur », même si une version dotée d'un moniteur monochrome était disponible.

L'Amstrad CPC s'est vendu à environ trois millions d'exemplaires dans le monde, dont environ un million en France.

## Émulateurs

[Libretro CrocoDS](libretro-crocods)
[Libretro Cap32](libretro-cap32)