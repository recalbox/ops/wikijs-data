---
title: Retroachievements
description: 
published: true
date: 2024-11-11T05:31:15.543Z
tags: retroachievements
editor: markdown
dateCreated: 2021-05-21T07:53:49.009Z
---

## Introduction

La communauté retrogaming a développé une plate-forme [retroachievements.org](http://www.retroachievements.org/)
proposant aux joueurs de relever des défis et d'obtenir des distinctions sur leurs jeux préférés. 

Cette plate-forme supporte la plupart des consoles (NES, SNES, GB, GBC, GBA, Megadrive, PC engine, etc.) et propose des défis pour la plupart des jeux. 

Recalbox permet de connecter un compte [retroachievements.org](http://www.retroachievements.org/) pour prendre part aux défis et se mesurer aux autres joueurs. 

## Création d'un compte

* Rendez-vous sur [retroachievements.org](http://www.retroachievements.org/) et [créer un compte](http://retroachievements.org/createaccount.php) 
* Noter le pseudonyme et le mot de passe car ils seront nécessaires pour activer la fonctionnalité dans Recalbox. 

## Activation du compte dans Recalbox

Rendez-vous dans les paramètres d'EmulationStation.

* Ouvrir le menu d'EmulationStation avec le bouton `START`. 
* Aller dans `OPTIONS DES JEUX` > `OPTIONS DE RETROACHIEVEMENTS`.
* Renseigner les champs comme ci-dessous :

  * `RETROACHIEVEMENTS` : ON
  * `NOM UTILISATEUR` : votre pseudonyme
  * `MOT DE PASSE` : votre mot de passe

En complément vous pouvez choisir d'activer **le mode hardcore**. Cette option permet de jouer aux jeux dans les conditions d'époque en désactivant les fonctionnalités offertent par les émulateurs (sauvegardes, retour arrière, codes de triche, etc.) 

## Compatibilité des émulateurs

Le projet retroachievements est compatible avec la plupart des émulateurs et cores de Recalbox mais tous ne sont pas gérés. Par exemple la Gambe Cube et la Wii ne sont pas du tout prises en charge et sur d'autres consoles seulement certains cores sont compatibles avec la fonctionnalité de retroachievements.

Pour vérifier la compatibilité d'un émulateur veuillez [consulter le tableau de compatibilité](https://docs.retroachievements.org/Emulator-Support-and-Issues/).

## Compatibilité des jeux et des roms

Les titres populaires sont supportés, consulter [la liste des jeux supportés](http://retroachievements.org/gameList.php) pour vérifier si un jeu est pris en charge. 

Pour chaque jeu seulement quelques roms sont supportées. Comme retroachievements.org est un projet américain, naturellement il y a une meilleure compatibilité des roms US mais des roms européennes sont parfois compatibles. Les roms en version "No-Intro" sont à privilégier pour augmenter ses chances d'être compatible. 

La compatibilité d'une ROM peut se vérifier en comparant empreinte du fichier avec la liste des fichiers supportés, cette liste est consultable dans la page de description du jeu du site retroachievements.org. 

## Voir les défis en cours de jeu

Pour visionner les défis et trophées en cours de jeu, rendez-vous dans le menu RetroArch en faisant la combinaison `Hotkey`+`B` puis aller dans le menu `Succès`.

Pour revenir dans la partie appuyer sur `A` pour revenir en arrière puis sélectionner `Reprendre` avec le bouton `B`.
