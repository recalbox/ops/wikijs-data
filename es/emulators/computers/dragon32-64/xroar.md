---
title: XRoar
description: 
published: true
date: 2024-07-29T18:52:28.658Z
tags: dragon32, dragon64, xroar, 8.0+
editor: markdown
dateCreated: 2023-09-11T09:18:32.315Z
---

## ![](/emulators/license.svg) Licencia

Este núcleo está bajo la licencia [**GPL-3.0**](https://gitlab.com/recalbox/packages/standalone/xroar/-/blob/master/COPYING.GPL) y [**LGPL-3.0**](https://gitlab.com/recalbox/packages/standalone/xroar/-/blob/master/COPYING.LGPL).

## ![](/emulators/compatibility.png) Compatibilidad

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Funcionalidades

| Funcionalidad | Soportado |
| :---: | :---: |

## ![](/emulators/bios.svg) BIOS

### Lista de BIOS obligatorios

| Nombre de archivo | Descripción | MD5 | Proporcionado |
| :---: | :---: | :---: | :---: |
| d32.rom | Dragon32 system rom | 3420b96031078a4ef408cad7bf21a33f | ❌ |
| d64rom1.rom | Dragon64 system rom #1 | 5f0bee59710e55f5880e74890912ed78 | ❌ |
| d64rom2.rom | Dragon64 system rom #2 | fd91edce7be5e7c2d88e46b76956a8aa | ❌ |
| d64tano.rom | Tano system rom #1 | be9bc86ee5eb401d0a40d0377f65fefa | ❌ |
| d64tano2.rom | Tano system rom #2 | fd91edce7be5e7c2d88e46b76956a8aa | ❌ |

### Ubicación

Coloca los BIOS de la siguiente manera:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 dragon
┃ ┃ ┃ ┃ ┣ 🗒 **d32.rom**
┃ ┃ ┃ ┃ ┣ 🗒 **d64rom1.rom**
┃ ┃ ┃ ┃ ┣ 🗒 **d64rom2.rom**
┃ ┃ ┃ ┃ ┣ 🗒 **d64tano.rom**
┃ ┃ ┃ ┃ ┣ 🗒 **d64tano2.rom**

### Lista de BIOS opcionales

| Nombre de archivo | Descripción | MD5 | Proporcionado |
| :---: | :---: | :---: | :---: |
| d200rom1.rom | Dragon 200e system rom #1 | be9bc86ee5eb401d0a40d0377f65fefa | ❌ |
| d200rom2.rom | Dragon 200e system rom #2 | fd91edce7be5e7c2d88e46b76956a8aa | ❌ |
| ddos10.rom | Dragon DOS 1.0 | 1c965da49b6c5459b8353630aa1482e7 | ❌ |
| ddos11c.rom | Dragon DOS 1.1 | d8429af1a12f7438a4bf88a5b934cb3a | ❌ |
| ddos12a.rom | Dragon DOS 1.2 | 55e2535dbbed7f1a26b5f263d7c72c63 | ❌ |
| ddos40.rom | Dragon DOS 4.0 | 9ddc388632cd3c376b164ba5cfc64329 | ❌ |
| ddos42.rom | Dragon DOS 4.2 | c956a854cbc4b9d1e69c000f78368668 | ❌ |
| deltados.rom | Delta DOS | 024eac3db20f1b5cf98c30a0e4743201 | ❌ |
| dplus48.rom | DOS Plus 4.8 | ee6f24d893a52b8efea9f787855456b5 | ❌ |
| dplus49b.rom | DOS Plus 4.9 | 56f1b97314e4ca82491c465bb887059e | ❌ |
| dplus50.rom | DOS Plus 5.0 | 35de5d28da507ebb213a26e04241d940 | ❌ |
| sdose6.rom | Super DOS E6 | 9d85e6b7133f915c021156f4b9cdb512 | ❌ |
| sdose8.rom | Super DOS E8 | 167f409b7a4b992faabb784b061ab4c6 | ❌ |

### Ubicación

Coloca los BIOS de la siguiente manera:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 dragon
┃ ┃ ┃ ┃ ┣ 🗒 **d200rom1.rom**
┃ ┃ ┃ ┃ ┣ 🗒 **d200rom2.rom**
┃ ┃ ┃ ┃ ┣ 🗒 **ddos10.rom**
┃ ┃ ┃ ┃ ┣ 🗒 **ddos11c.rom**
┃ ┃ ┃ ┃ ┣ 🗒 **ddos12a.rom**
┃ ┃ ┃ ┃ ┣ 🗒 **ddos40.rom**
┃ ┃ ┃ ┃ ┣ 🗒 **ddos42.rom**
┃ ┃ ┃ ┃ ┣ 🗒 **deltados.rom**
┃ ┃ ┃ ┃ ┣ 🗒 **dplus48.rom**
┃ ┃ ┃ ┃ ┣ 🗒 **dplus49b.rom**
┃ ┃ ┃ ┃ ┣ 🗒 **dplus50.rom**
┃ ┃ ┃ ┃ ┣ 🗒 **sdose6.rom**
┃ ┃ ┃ ┃ ┣ 🗒 **sdose8.rom**

## ![](/emulators/roms.png) Roms

### Extensiones soportadas

La rom debe tener la extensión:

* .cas
* .wav
* .bas
* .asc
* .dmk
* .jvc
* .os9
* .dsk
* .vdk
* .rom
* .ccc

### Ubicación

Coloca las roms de la siguiente manera:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 dragon
┃ ┃ ┃ ┃ ┣ 🗒 **juego.dsk**

>Para más información sobre las roms, visita [este tutorial](./../../../tutorials/games/generalities/isos-and-roms)!
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

>Para poder conservar tus configuraciones personalizadas durante una actualización, te recomendamos utilizar nuestra funcionalidad [Sobrecargas de configuración](./../../../advanced-usage/configuration-override).
{.is-info}

## ![](/emulators/external-links.png) Enlaces externos

* **Código fuente utilizado** : [https://gitlab.com/recalbox/packages/standalone/xroar/](https://gitlab.com/recalbox/packages/standalone/xroar/)
* **Sitio web oficial** : [https://www.6809.org.uk/xroar/](https://www.6809.org.uk/xroar/)
