---
title: Amiberry
description: 
published: true
date: 2024-06-23T00:18:34.572Z
tags: amiga-600, amiga, amiberry, 600
editor: markdown
dateCreated: 2021-05-21T08:14:45.768Z
---

**Amiberry** est un core pour ARM optimisé pour Amiga.

## ![](/emulators/license.svg) Licence

Ce core est sous licence [**GPLv3**](https://github.com/midwan/amiberry/blob/master/COPYING).

## ![](/emulators/compatibility.png) Compatibilité

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ❌ | ✅ |

## ![](/emulators/features.png) Fonctionnalités



## ![](/emulators/bios.svg) BIOS

### Liste des bios obligatoires

Vous trouverez les bios avec le nom indiqué dans la colonne **Description** qu'il vous faudra renommer avec le nom indiqué dans la colonne **Nom de fichier**.

| Nom de fichier | Description | MD5 | Fourni |
| :---: | :---: | :---: | :---: |
| kick33180.A500 | Kickstart v1.2 rev 33.180 (1986)(Commodore)(A500-A1000-A2000).rom | 85ad74194e87c08904327de1a9443b7a | ❌ |
| kick34005.A500 | Kickstart v1.3 rev 34.5 (1987)(Commodore)(A500-A1000-A2000-CDTV).rom | 82a21c1890cae844b3df741f2762d48d | ❌ |
| kick37175.A500 | Kickstart v2.04 rev 37.175 (1991)(Commodore)(A500+).rom | dc10d7bdd1b6f450773dfb558477c230 | ❌ |
| kick40063.A600 | Kickstart v3.1 rev 40.63 (1993)(Commodore)(A500-A600-A2000)\[!\].rom | e40a5dfb3d017ba8779faba30cbd1c8e | ❌ |
| kick02019.AROS.ext | AROS Free BIOS | 4fa7042d73d0f77bf5723cf8430989bf | ✅ |
| kick02019.AROS | AROS Free BIOS | 186d23cccd9fbd6336d0f988b39dce4d | ✅ |

### Emplacement

Placez les BIOS comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 🗒 **kick33180.A500**
┃ ┃ ┃ ┣ 🗒 **kick34005.A500**
┃ ┃ ┃ ┣ 🗒 **kick37175.A500**
┃ ┃ ┃ ┣ 🗒 **kick40063.A600**
┃ ┃ ┃ ┣ 🗒 **kick02019.AROS.ext**
┃ ┃ ┃ ┣ 🗒 **kick02019.AROS**

## ![](/emulators/roms.png) Roms

>L'émulation Amiga est disponible en deux parfums : Amiga 600 et Amiga 1200.  
>Certains jeux peuvent être très capricieux donc essayez les deux systèmes si un jeu ne fonctionne pas.
>Les jeux peuvent être utilisés soit dans un format disque ADF soit dans un format WHDLoad, utilisant dans ce cas un fichier UAE complémentaire.
{.is-info}

### Extensions supportées

Les roms doivent avoir l'extension :

* .adf
* .ipf
* .rp9
* .whd
* .zip

>Les autres formats d'images disque _pourraient_ être supportés mais n'ont pas été testés par l'équipe de Recalbox.
>
>Vous pouvez retrouver plus d'informations sur ces formats sur [cette page](./../../../tutorials/games/guides/amiga/faq).
{.is-info}

### Emplacement

Placez les roms comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 amiga600
┃ ┃ ┃ ┃ ┣ 🗒 **jeux.bin**
┃ ┃ ┃ ┃ ┣ 🗒 **jeux.lst**

>Les roms au format **TOSEC** sont vivement conseillées.
{.is-success}

>Pour plus d'information sur les roms, rendez-vous sur [ce tutoriel](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuration avancée de l'émulateur

>Pour pouvoir conserver vos configurations personnalisées lors d'une mise à jour, nous vous conseillons d'utiliser notre fonctionnalité [Surcharges de configuration](./../../../advanced-usage/configuration-override).
{.is-info}

## ![](/emulators/external-links.png) Liens externes

* **Code source utilisé** : [https://gitlab.com/recalbox/packages/standalone/amiberry/](https://gitlab.com/recalbox/packages/standalone/amiberry)
* **Site officiel** : [https://blitterstudio.com/amiberry/](https://blitterstudio.com/amiberry/)