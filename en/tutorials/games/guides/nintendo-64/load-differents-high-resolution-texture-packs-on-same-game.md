---
title: Multiple high resolution texture packs on the same game
description: 
published: true
date: 2021-08-08T14:10:36.361Z
tags: n64, nintendo, 64, texture, pack
editor: markdown
dateCreated: 2021-08-08T13:23:47.889Z
---

## Information

In the guide "[High-resolution textures](./high-resolution-textures)" you learned how to use the high resolution texture packs.

## General

For some games, different texture packs have been created. The problem is that they all have the same name. If you want to use several versions, follow this guide.

## Sequence

For each version of the high resolution texture, we change the name of the internal rom. When this is done, we will change the names of the high resolution textures.

## What we need

In this guide, we will use **N64 Rom Renamer** by Einstein II (Google is your friend), to change the internal rom name.

You will also need a renaming batch (example on Windows: **AntRenamer**). For this guide, we will use **Better Rename** (macOS).

## Here we go

### Change the internal name of the rom

* Open **N64 Rom Renamer** with administrator rights (it didn't work without it for me).

![](/tutorials/games/guides/nintendo-64/multiple-hires-textures/multiplehirespacks1.png){.full-width}

* Press `F2` or go to `Tools` > `Headereditor`.
* Confirm the warning.

![](/tutorials/games/guides/nintendo-64/multiple-hires-textures/multiplehirespacks2.png)

* In the top left corner, click on the folder icon and select your rom (for our example, we will use Mario Kart 64).

![](/tutorials/games/guides/nintendo-64/multiple-hires-textures/multiplehirespacks3.png){.full-width}

* Here you can change the internal name of the rom.

### Caution

The name **must not be too long**. The total length of letters and spaces must be respected (20 characters). In our case, we see that **MARIOKART64** has nine spaces in addition to its name, which gives us the total possible length for the internal name.

![](/tutorials/games/guides/nintendo-64/multiple-hires-textures/multiplehirespacks4.png){.full-width}

* In our example, we will remove four spaces which will be replaced by four letters (SNES).

![](/tutorials/games/guides/nintendo-64/multiple-hires-textures/multiplehirespacks5.png){.full-width}

* Click `Save changes` to save the changes and then click `Back` to go back.
* The internal name of the rom has been successfully changed.

Then rename the rom file; for example: Mario Kart 64 SNES Edition.ext The rom file name is not important for the textures.

### Modify the high resolution texture pack and the :

Our example file structure of our high resolution pack looks like this:

![](/tutorials/games/guides/nintendo-64/multiple-hires-textures/multiplehirespacks6.png)

The folder and files named **MARIOKART64** are of course now incorrect, after we changed the internal name of the rom. We need to correct this!

* Open **Better Rename**
* Category: Text
* Action: Replace text
* Replace : **MARIOKART64**
* With: **MARIOKART64SNES**

![](/tutorials/games/guides/nintendo-64/multiple-hires-textures/multiplehirespacks7.png){.full-width}

After renaming, you need to check that all names match. If the folder name and file names match, you can copy your texture pack.

>**As a precaution :**
>
>Old folder name: MARIOKART64  
>Old file name in folder: MARIOKART64#......
>
>New folder name: MARIOKART64SNES  
>New file name in folder: MARIOKART64SNES#...
{.is-warning}

Follow these steps for each high-resolution texture pack you want.