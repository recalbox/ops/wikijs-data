---
title: Nintendo 64
description: Nintendo 64
published: true
date: 2024-07-25T19:13:21.355Z
tags: n64, consoles, nintendo, nintendo 64
editor: markdown
dateCreated: 2021-05-21T08:00:30.497Z
---

![](/emulators/consoles/nintendo64.svg){.align-center}

## Fiche technique

* **Fabricant** : Nintendo
* **Année de sortie** : 1996
* **Quantités vendues** : 32.93 millions
* **Meilleur jeu vendu** : Super Mario 64
* **Processeur** : 64-bit NEC VR4300 @ 93.75 MHz
* **Co-processeur** : 64-bit MIPS RISC "Reality Immersion" RCP @ 62.5 MHz
* **RAM** : 4 MB RAMBUS RDRAM (modifiable pour 8 MB)
* **Puce sonore** : Stereo 16-Bit et 48 kHz

## Présentation

Au début des années 1990, avec le succès des consoles **NES** et **Super Nintendo**, la société Nintendo domine le marché mondial du jeu vidéo, malgré la compétition effrénée que lui livre Sega.
L'émergence du CD-ROM a amené Nintendo à conclure un partenariat avec Sony pour que celui-ci développe un lecteur de CD pour la Super Nintendo, le **SNES-CD** plus connue sous le nom de **Nintendo PlayStation**, afin de concurrencer la **PC-Engine** de NEC et le **Mega-CD** de Sega.
Mais un différend amène Nintendo et Sony à abandonner leur projet commun, que Sony recycle alors pour développer sa propre console, la **PlayStation**. Nintendo se tourne alors vers la firme néerlandaise Philips pour poursuivre son projet.

L'annonce de la PlayStation en 1991, ajoutée aux échecs du **Mega-CD** de Sega et du **CD-i** de Philips, persuade alors Nintendo d'enterrer définitivement l'extension CD-ROM de la **Super Nintendo** et de se tourner vers le développement d'une toute nouvelle console.

## Émulateurs

[Libretro mupen64plus_Next](libretro-mupen64plus_next)
[Libretro ParaLLEl_n64](libretro-parallel_n64)
[Mupen64plus GLide64MK2](mupen64plus-glide64mk2)
[Mupen64plus GLideN64](mupen64plus-gliden64)
[Mupen64plus N64_GLES2](mupen64plus-n64_gles2)
[Mupen64plus RICE](mupen64plus-rice)
[Mupen64plus RICE_GLES2](mupen64plus-rice_gles2)