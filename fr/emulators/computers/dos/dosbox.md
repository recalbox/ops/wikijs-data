---
title: DOSBox
description: Pour émuler des jeux DOS
published: true
date: 2024-06-23T11:50:03.876Z
tags: dos, msdos, dosbox
editor: markdown
dateCreated: 2021-05-21T08:16:35.175Z
---

DOSBox est un émulateur capable d'émuler un ordinateur **PC compatible IBM** tournant sous le système d'exploitation **DOS**.  
Plusieurs types de cartes graphiques ou sonores peuvent être émulées.

Cela permet de jouer à d'anciens jeux PC sur Recalbox.

## ![](/emulators/license.svg) Licence

Ce core est sous licence [**GPLv2**](https://gitlab.com/lmerckx/recalbox-dosbox/-/blob/master/COPYING).

## ![](/emulators/compatibility.png) Compatibilité

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Fonctionnalités

| Fonctionnalité | Supporté |
| :---: | :---: |
| Captures d'écran | ✔ |
| Sauvegardes | - |
| options du core | ✔ |
| Controls | ✔ |
| Remapping | ✔ |

## ![](/emulators/bios.svg) BIOS

>**Aucun bios n'est requis.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Extensions supportées

La rom doit avoir l'extension :

* .exe
* .com
* .bat
* .conf

### Emplacement

Placez les roms comme ceci : 

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 dos
┃ ┃ ┃ ┃ ┣ 📁 **jeu**
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **jeu.exe**

## ![](/emulators/advanced-configuration.png) Configuration avancée de l'émulateur

>Pour pouvoir conserver vos configurations personnalisées lors d'une mise à jour, nous vous conseillons d'utiliser notre fonctionnalité [Surcharges de configuration](./../../../advanced-usage/configuration-override).
{.is-info}

## ![](/emulators/external-links.png) Liens externes

* **Code source utilisé** : [https://gitlab.com/lmerckx/recalbox-dosbox/](https://gitlab.com/lmerckx/recalbox-dosbox/)
* **Wiki officiel** : [https://www.dosbox.com/wiki/](https://www.dosbox.com/wiki/)
* **Liste des jeux** : [https://www.dosbox.com/comp_list.php?letter=a](https://www.dosbox.com/comp_list.php?letter=a)
* **Jeux jouables** : [https://www.dosbox.com/comp_list.php?letter=playable](https://www.dosbox.com/comp_list.php?letter=playable)