---
title: Libretro ECWolf
description: 
published: true
date: 2024-06-23T12:10:56.791Z
tags: libretro, ecwolf, wolfenstein 3d
editor: markdown
dateCreated: 2021-05-21T08:33:49.163Z
---

**Libretro ECWolf** est un moteur de jeu de port source pour Wolfenstein 3D. This port is courtesy of phcoder.

ECWolf est un portage du moteur Wolfenstein 3D basé sur Wolf4SDL. Il combine le moteur original Wolfenstein 3D avec l'expérience utilisateur de ZDoom pour créer le port source Wolf3D le plus convivial pour les utilisateurs et les modificateurs.

Comme ZDoom, ECWolf vise à prendre en charge tous les jeux qui utilisent le moteur Wolfenstein 3D, y compris Blake Stone (à venir dans ECWolf 3.0), Corridor 7, Operation Body Count, Rise of the Triad et Super 3D Noah's Ark. ECWolf supportera également Macintosh Wolfenstein 3D avec toutes ses missions créées par l'utilisateur (à venir dans ECWolf 2.0).

* Un binaire unique exécute tous les jeux pris en charge. (Wolfenstein 3D, Spear of Destiny, ...)
* Prise en charge complète des modes haute résolution avec correction du rapport d'aspect, y compris la prise en charge d'un écran large.
* Schémas de contrôle modernes (WASD + souris).
* Automap de style Mac Wolf / S3DNA / ROTT.
* Emplacements de sauvegarde illimités.
* Ceci est en fait basé sur le moteur Wolf3D au lieu d'une recréation ou de forcer dans un moteur plus moderne.
* Logiciel rendu à l'aide de la même diffusion de rayons 8 bits.

ECWolf peut exécuter le contenu suivant :

* Wolfenstein 3D
* Spear of Destiny
* Super 3D Noah's Ark

## ![](/emulators/license.svg) Licence

Ce core est sous licence [**GPLv2**](https://github.com/libretro/ecwolf/blob/master/docs/license-gpl.txt).

## ![](/emulators/compatibility.png) Compatibilité

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Fonctionnalités



## ![](/emulators/bios.svg) BIOS

>**Aucun bios n'est requis.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Extensions supportées

La rom doit avoir l'extension :

* .zip
* .pk3

### Emplacement

Placez les roms comme ceci : 

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 ports
┃ ┃ ┃ ┃ ┣ 📁 Wolfenstein 3D
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **ecwolf.pk3**

## ![](/emulators/advanced-configuration.png) Configuration avancée de l'émulateur

>Pour pouvoir conserver vos configurations personnalisées lors d'une mise à jour, nous vous conseillons d'utiliser notre fonctionnalité [Surcharges de configuration](./../../../advanced-usage/configuration-override).
{.is-info}

### Accéder aux options

Vous pouvez configurer diverses options de deux façons différentes.

* Via le Menu RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via le fichier `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Options du core

| Options | Valeur des options | Variable (pour surcharge) | Valeurs des variables |
| --- | --- | --- | --- |
| Internal resolution | `320x200` ✅ / `320x240` / `400x240` / `420x240` / `480x270` / `640x360` / `640x400` / `640x480` / `800x500` / `960x540` / `960x600` / `1024x768` / `1280x720` / `1280x800` / `1600x1000` / `1920x1200` / `2240x1400` / `2560x1600` | `ecwolf-resolution` | `320x200` / `320x240` / `400x240` / `420x240` / `480x270` / `640x360` / `640x400` / `640x480` / `800x500` / `960x540` / `960x600` / `1024x768` / `1280x720` / `1280x800` / `1600x1000` / `1920x1200` / `2240x1400` / `2560x1600` |
| Analog deadzone | `0%` / `5%` / `10%` / `15%` ✅ / `20%` / `25%` / `30%` | `ecwolf-analog-deadzone` | `0` / `5` / `10` / `15` / `20` / `25` / `30` |
| Refresh rate (FPS) | `7` / `7.8` / `8.8` / `10` / `14` / `17.5` / `25` / `30` / `35` ✅ / `50` / `60` / `70` / `72` / `75` / `90` / `100` / `119` / `120` / `140` / `144` / `240` / `244` / `300` / `360` | `ecwolf-fps` | `7` / `7.8` / `8.8` / `10` / `14` / `17.5` / `25` / `30` / `35` / `50` / `60` / `70` / `72` / `75` / `90` / `100` / `119` / `120` / `140` / `144` / `240` / `244` / `300` / `360` |
| Preferred palette format (Restart) | `RGB565 (16-bit)` ✅ / `XRGB8888 (24-bit)` | `ecwolf-palette` | `rgb565` / `xrgb8888` |
| Always run | `Désactivé` ✅ / `Activé` | `ecwolf-alwaysrun` | `disabled` / `enabled` |
| Screen size | `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `Largest with statusbar` ✅ / `Without statusbar` | `ecwolf-viewsize` | `4` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `21` |
| Show map as overlay | `Désactivé` ✅ / `Activé` / `On + Normal` | `ecwolf-am-overlay` | `off` / `on` / `both` |
| Rotate map | `Désactivé` ✅ / `Activé` / `Overlay only` | `ecwolf-am-rotate` | `off` / `on` / `overlay_only` |
| Textures walls in automap | `Désactivé` ✅ / `Activé` | `ecwolf-am-drawtexturedwalls` | `disabled` / `enabled` |
| Textured floors in automap | `Désactivé` ✅ / `Activé` | `ecwolf-am-drawtexturedfloors` | `disabled` / `enabled` |
| Textured Overlay in automap | `Désactivé` ✅ / `Activé` | `ecwolf-am-texturedoverlay` | `disabled` / `enabled` |
| Show level ratios in automap | `Désactivé` ✅ / `Activé` | `ecwolf-am-showratios` | `disabled` / `enabled` |
| Pause game in automap | `Désactivé` ✅ / `Activé` | `ecwolf-am-pause` | `disabled` / `enabled` |
| Volume of music | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` ✅ | `ecwolf-music-volume` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` |
| Volume of digitized sound effects | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` ✅ | `ecwolf-digi-volume` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` |
| Volume of Adlib sound effects | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` ✅ | `ecwolf-adlib-volume` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` |
| Volume of Speaker sound effects | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` ✅ | `ecwolf-speaker-volume` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` |
| Analog move and strafe sensitivity | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` ✅ | `ecwolf-analog-move-sensitivity` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` |
| Analog turn sensitivity | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` ✅ | `ecwolf-analog-turn-sensitivity` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` |
| Order of lookup for effects | `Digitized, Adlib, Speaker` ✅ / `Digitized, Adlib` / `Digitized only` / `Adlib only` / `Speaker only` | `ecwolf-effects-priority` | `digi-adlib-speaker` / `digi-adlib` / `digi-speaker` / `digi` / `adlib` / `speaker` |
| Aspect | `Auto` ✅ / `16:9` / `4:3` / `16:10` / `17:10` / `5:4` / `21:9` | `ecwolf-aspect` | `auto` / `16:9` / `4:3` / `16:10` / `17:10` / `5:4` / `21:9` |
| Invulnerability | `Désactivé` ✅ / `Activé` | `ecwolf-invulnerability` | `disabled` / `enabled` |
| Dynamic FPS | `Désactivé` ✅ / `Activé` | `ecwolf-dynamic-fps` | `disabled` / `enabled` |
| Store files in memory | `Désactivé` ✅ / `Activé` | `ecwolf-memstore` | `disabled` / `enabled` |
| Horizontal panning speed in automap | `0` / `1` / `2` / `3` / `4` / `5` ✅ / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` | `ecwolf-panx-adjustment` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` |
| Vertical panning speed in automap | `0` / `1` / `2` / `3` / `4` / `5` ✅ / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` | `ecwolf-paxy-adjustment` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` |

## Création de mods

* Créez des mods sans travailler avec le code source !
  * Les mods fonctionnent avec toutes les plates-formes prises en charge, y compris Windows, macOS et Linux.
* Support de texture arbitraire haute résolution, plat et sprite.
* Pushwalls mobiles simultanés illimités.
* Des choses illimitées.
* Taille de la carte illimitée (Bien que les limites techniques limitent le format GAMEMAPS à 181x181).
* Utilise des formats de script et des formats de données similaires à ceux de ZDoom.
  * Les utilitaires d'édition Doom fonctionnent avec ECWolf (sauf pour les niveaux).

Notez que jusqu'à ECWolf 2.0, bien que des changements radicaux ne soient pas exclus, la rétrocompatibilité des mods ne sera pas nécessairement conservée. Consultez [le wiki](http://maniacsvault.net/ecwolf/wiki/Version_compatibility) pour plus d'informations.

## ![](/emulators/external-links.png) Liens externes

* **Code source utilisé** : [https://github.com/libretro/ecwolf/](https://github.com/libretro/ecwolf)
* **Site officiel** : [http://maniacsvault.net/ecwolf/](http://maniacsvault.net/ecwolf/)
* **Forum officiel** : [https://forum.drdteam.org/viewforum.php?f=174](https://forum.drdteam.org/viewforum.php?f=174)
* **Documentation officielle** : [http://maniacsvault.net/ecwolf/wiki/Main_Page](http://maniacsvault.net/ecwolf/wiki/Main_Page)