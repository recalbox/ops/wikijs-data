---
title: Libretro-mojozork
description: 
published: true
date: 2024-06-22T11:34:47.870Z
tags: libretro, z-machine, 9.1+, mojozork
editor: markdown
dateCreated: 2023-06-27T19:22:30.925Z
---

**Libretro-mojozork** est un interpréteur des jeux Infocom et d'autres développeurs pour la **Z-machine**.

## ![](/emulators/license.svg) Licence

Ce core est sous licence [**zlib**](https://github.com/icculus/mojozork/blob/main/LICENSE.txt).

## ![](/emulators/compatibility.png) Compatibilité

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Fonctionnalités

| Fonctionnalité | Supporté |
| :---: | :---: |

## ![](/emulators/bios.svg) BIOS

>**Aucun bios n'est requis.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Extensions supportées

Les roms doivent avoir l'extension :

* .dat
* .zip
* .z1
* .z3
* .7z

Ce système supporte les roms compressées au format .zip/.7z. Attention toutefois, il ne s'agit que d'une archive.

Les fichiers contenus dans les .zip/.7z doivent correspondre aux extensions citées précédemment.
De plus, chaque fichier .zip/.7z ne doit contenir qu'une seule rom compressée.

### Emplacement

Placez les roms comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 zmachine
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.zip**

## ![](/emulators/advanced-configuration.png) Configuration avancée de l'émulateur

> **Ce core n'a pas d'options !**
{.is-success}

## ![](/emulators/external-links.png) Liens externes

* **Code source utilisé** : [https://github.com/icculus/mojozork/](https://github.com/icculus/mojozork/)