---
title: Utiliser un périphérique NVMe de stockage sur Recalbox
description: 
published: true
date: 2025-01-11T01:21:15.554Z
tags: stockage, nvme
editor: markdown
dateCreated: 2024-09-18T15:16:23.769Z
---

## Introduction

Vous pouvez utiliser un périphérique NVMe avec Recalbox pour stocker vos roms via un hat M.2. Ce périphérique se comportera comme un disque dur interne.

![Hat M.2 pour Raspberry Pi 5](/tutorials/system/installation/m.2-dual-hat-for-raspberry-pi-5.jpg)

Vous devez avoir votre support NVMe formaté en exFAT. Aucun formatage n'aura lieu depuis Recalbox.

Il existe plusieurs marques de hat M.2, certains acceptant qu'un seul NVMe et d'autres pouvant en accepter 2. Ce tutoriel est basé sur un hat M.2 acceptant 2 supports NVMe mais en ayant qu'un seul NVMe branché.

>Ce tutoriel concerne principalement le Raspberry Pi 5. Il peut convenir aux disques durs sur PC.
{.is-info}

## Procédure

Suivez les étapes suivantes et vous pourrez vous servir de votre NVMe comme support de stockage de roms supplémentaire.

### Mise à jour du bootloader

En premier lieu, vous devez mettre à jour le bootloader de votre Raspberry Pi 5 en suivant [ce tutoriel](../../utilities/update-rpi4-bootloader)


### Configuration du port pci express

Le standard **PCI Express 3** est compatible avec le Raspberry Pi 5, et permet de **doubler la vitesse de lecture/écriture** sur les périphériques NVMe compatibles.

Cependant, il faut l'activer manuellement. 

Pour cela rendez-vous dans le fichier `recalbox-user-config.txt` de la partition `RECALBOX` ou en utilisant la commande [SSH](../../system/access/root-access-terminal-cli)  `vi /boot/recalbox-user-config.txt`

Ajoutez la ligne suivante au début du fichier :

```
dtparam=pciex1_gen=3
```

### Création du dossier d'accès

Vous devez créer un dossier dans votre share pour accéder au contenu de votre NVMe. Pour cela, utilisez la commande [SSH](../../system/access/root-access-terminal-cli) suivante :

```sh
mkdir -p /recalbox/share/externals/hdd0
```

### Modification du fichier `recalbox-boot.conf`

Vous devez ajouter une ligne dans le fichier `recalbox-boot.conf` de 2 façons différentes :

#### Si votre carte SD est dans votre PC

Dans le lecteur nommé `RECALBOX`, ouvrez le fichier `recalbox-boot.conf` et ajoutez la ligne suivante sur n'importe quelle ligne vide :

```sh
sharenetwork_cmd=mount /dev/nvme0n1p2 /recalbox/share/externals/hdd0
```

#### Si vous agissez dans votre Recalbox allumée

- Connectez-vous via [SSH](../../system/access/root-access-terminal-cli)
- Autorisez la [partition de démarrage](../../system/access/remount-partition-with-write-access) à pouvoir être utilisé en écriture.
- Ouvrez le fichier `/boot/recalbox-boot.conf` et ajoutez la ligne suivante sur n'importe quelle ligne vide :

```ini
sharenetwork_cmd=mount /dev/nvme0n1p2 /recalbox/share/externals/hdd0
```

### Modification du fichier `smb.conf`

Ici, vous êtes obligés de modifier le fichier `/etc/samba/smb.conf` depuis votre Recalbox allumée :

- Connectez-vous via [SSH](../../system/access/root-access-terminal-cli)
- Autorisez la [partition du système](../../system/access/remount-partition-with-write-access) à pouvoir être utilisé en écriture.
- Ouvrez le fichier `/etc/samba/smb.conf` et ajoutez les lignes suivantes à la fin du fichier :

```ini
[nvme]
comment = Recalbox user data
path = /recalbox/share/externals/hdd0
writeable = yes
guest ok = yes
create mask = 0644
directory mask = 0755
force user = root
veto files = /._*/.DS_Store/
delete veto files = yes
```

### Test des modifications

Démarrez ou redémarrez votre Recalbox et regardez :

- Si vous avez le dossier `nvme` dans le partage réseau
- Si le NVMe est listé dans les menus de l'interface (`START` > `RÉGLAGES SYSTÈME` > `MÉDIA DE STOCKAGE`)
- Via SSH, avec la commande `mount`, votre NVMe devrait apparaître :

```sh
# mount
...
/dev/nvme0n1p2 on /recalbox/share/externals/hdd0 type exfat (rw,relatime,fmask=0022,dmark=0022,iocharset=utf8,errors=remount-ro)
...
```

## Que faire si ça ne fonctionne pas ?

Si votre NVMe ne s'affiche pas, vous pouvez vérifier le nom de votre NVMe interne avec cette commande [SSH](../../system/access/root-access-terminal-cli) :

```sh
blkid
```

Vous devriez avoir un résultat ressemblant à celui-ci :

```sh
/dev/nvme0n1p1: LABEL="EFI" UUID="67E3-17ED" TYPE="vfat"
/dev/nvme0n1p2: LABEL="NVMe 256G" UUID="66E9-99AE" TYPE="exfat"
/dev/mmcblk0p1: LABEL="RECALBOX" UUID="2A19-621E" TYPE="vfat"
/dev/mmcblk0p2: LABEL="SHARE" UUID="4140-70D7" TYPE="exfat"
/dev/mmcblk0p3: LABEL="OVERLAY" UUID="007f6e1f-ef13-4abd-9a69-1b70d8317e3b" TYPE="ext4"
```

Ici, `nvme0n1p1` est une petite partition qui peut être crée par certains systèmes qui nous est totalement inutile et trop petite, d'où le fait d'utiliser `nvme0n1p2` dans le reste du tutoriel.

## Remarques

### Mettre le NVMe en lecture seule

Vous pouvez mettre votre NVMe en lecture seule si vous ne voulez pas que Recalbox touche aux fichiers avec cette commande SSH :

```sh
mount -o ro /dev/<votre_nvme> /recalbox/share/externals/hdd0
```

### NVMe formaté en NTFS

Il n'est **vivement pas recommandé** d'avoir votre NVMe formaté en NTFS. Si toutefois, vous souhaitez avoir votre NVMe utilisable en NTFS, il est **TRÈS FORTEMENT** conseillé de les monter en lecture seule. Le format NTFS est malheureusement pas encore bien supporté avec un environnement Linux tel que Recalbox qui peut vous empêcher de pouvoir utiliser votre NVMe pour y écrire des données.