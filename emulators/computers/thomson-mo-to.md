---
title: Thomson MO/TO
description: 
published: true
date: 2021-10-03T17:06:32.654Z
tags: thomson, mo5, to8, ordinateurs
editor: markdown
dateCreated: 2021-05-21T07:57:45.755Z
---

![Modèle TO8](/emulators/computers/thomson.svg){.align-center}

## Fiche technique

* **Fabricant** : Thomson
* **Année de sortie** : 1986
* **Processeur** : Motorola 6809E @ 1 MHz
* **RAM** : 256kB
* **ROM** : 80kB
* **Résolution** : 8 modes d'affichage, palette de 4096 couleurs
  * **Basse résolution** : 160 × 200 (16, 5 ou 2 couleurs)
  * **Moyenne résolution** : 320 × 200 (16, 4, 3 ou 2 couleurs)
  * **Haute résolution** : 640 × 200 (2 couleurs)
* **Média** : Cartouche, lecteur disquette 3½" externe et lecteur cassette

## Présentation

La gamme Thomson MO/TO est une gamme de micro-ordinateurs 8-bits : MO5, MO6, TO7, TO7/70, TO8, TO8D, TO9, TO9+.

## Émulateurs

[Libretro Theodore](libretro-theodore)