---
title: Wii
description: Nintendo Wii
published: true
date: 2025-01-05T22:57:41.962Z
tags: consoles, nintendo, wii
editor: markdown
dateCreated: 2021-07-28T18:17:21.082Z
---

![](/emulators/consoles/wii.svg){.align-center}

## Technical data

* **Manufacturer**: Nintendo
* **Year of release**: 2006
* **Units sold**: 101.63 millions
* **Best-selling game**: Wii Sports
* **CPU**: IBM PowerPC "Broadway" @ 729 MHz
* **RAM**: 88 MB
* **GPU**: ATI "Hollywood" @ 243 MHz
* **Resolution**: 480i/p (PAL/NTSC) or 576i (PAL/SECAM), 4:3 and 16:9
* **sound chip**: Stereo - Dolby Pro Logic II-capable
* **Media**: 4.7/8.54 GB DVD

## Presentation

The **Wii** is a home game console from Japanese manufacturer Nintendo, released in 2006. A seventh-generation console, like the Xbox 360 and PlayStation 3 with which it is in rivalry, the Wii is the best-selling home console of its generation, with 101.63 million units sold in 2016. It is unique in that it uses an accelerometer that can detect the position, orientation and spatial movement of the controller. The Wii is the last Nintendo console not to have high definition.

The Wii marked a turning point in the history of video games by opening up this hobby to a wider audience, thus targeting the whole of society, which partly explains its success.

## Emulators

[Dolphin](dolphin)
[Libretro Dolphin](libretro-dolphin)