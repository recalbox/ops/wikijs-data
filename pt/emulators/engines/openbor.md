---
title: OpenBOR
description: 
published: true
date: 2024-06-02T17:15:52.037Z
tags: openbor, motor de juego
editor: markdown
dateCreated: 2021-10-19T23:39:26.609Z
---

![](/emulators/fantasy/openbor.svg){.align-center}

## Ficha técnica

* **Fabricante:** Senile Team, a equipe OpenBOR e a comunidade
* **Ano de lançamento:** 2004

## Apresentação

OpenBOR é um mecanismo de jogo de rolagem lateral baseado em sprites livres de royalties.
Desde seu início humilde em 2004, ele se tornou o motor mais versátil, fácil de usar e poderoso de seu tipo que você encontrará em qualquer lugar.

O OpenBOR é otimizado para jogos estilo beat em up de rolagem lateral (Double Dragon, Streets of Rage, Final Fight), mas se adapta facilmente a qualquer tipo de estilo de jogo e funcionalidade que você possa imaginar.

Quer experimentar fazer jogos? Abra um dos módulos já existentes e experimente - a comunidade está aqui para ajudar.
Pronto para fazer algo de sua escolha? Pegue algumas fotos e vamos lá! Você está se sentindo ambicioso? Mergulhe no mecanismo de script integrado e na suíte gráfica para criar uma obra-prima que rivaliza com as produções AAA mais malucas.

OperBOR é o pacote de código aberto do Beats of Rage.

Em 2004, o Senile Team lançou o Beats of Rage, um motor de videogame personalizável, beat-'em-up DOS grátis inspirado na série Streets of Rage da SEGA e usando sprites da série King of Fighters da SNK Playmore.
Com o Beats of Rage, os usuários podem criar seus próprios jogos beat 'em up - sem necessidade de experiência em programação!
Beats of Rage foi lançado pela primeira vez para DOS em novembro de 2003. Só foi anunciado boca a boca, mas rapidamente ganhou popularidade.
O Senile Team lançou rapidamente um pacote de edição permitindo que qualquer pessoa interessada em criar um mod para o motor BOR.
Ports para muitos outros sistemas (Dreamcast, Linux, Windows, GP32, GP2X, GP2X Wiz, Mac OS X, OpenDingux Dingoo A320 e GCW-Zero) seguiram rapidamente.
Beats of Rage não é mais mantido, distribuído ou apoiado pelo Senile Team.

Em 2005, Kirby2000 pediu ao Senile Team para abrir o código-fonte para o BOR. Eles aceitaram e o OpenBOR nasceu. O desenvolvimento do motor foi continuado pela comunidade e ainda é até hoje.

Para saber mais, visite a comunidade OpenBOR em ChronoCrash.com. Você também encontrará dezenas de módulos de jogo já concluídos para baixar e jogar.

## Emuladores

[OpenBOR](openbor)