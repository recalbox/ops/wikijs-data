---
title: Lista de programas de gestión de roms
description: 
published: true
date: 2024-07-23T23:08:30.921Z
tags: roms, utilidades, programas, gestión
editor: markdown
dateCreated: 2023-09-16T20:56:59.624Z
---

## ClrmamePro

[https://mamedev.emulab.it/clrmamepro/](https://mamedev.emulab.it/clrmamepro/)

Consultad el [tutorial clrmamepro](./../../../tutorials/utilities/rom-management/clrmamepro)

## Romulus

[https://romulus.dats.site/](https://romulus.dats.site/)

## RomCenter

[http://www.romcenter.com/](http://www.romcenter.com/)

## RomVault

![](/tutorials/utilities/rom-management/romvault.png)

[http://www.romvault.com/](http://www.romvault.com/)

## FAQs

* ¿Dónde puedo encontrar los ficheros .dat?
  * Las siguientes páginas son un buen punto de partida:  
    * [http://datomatic.no-intro.org/](http://datomatic.no-intro.org/) 
    * [http://www.progettosnaps.net/dats/](http://www.progettosnaps.net/dats/)
    * [http://redump.org/](http://redump.org/)