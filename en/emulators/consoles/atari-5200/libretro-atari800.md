---
title: Libretro Atari800
description: 
published: true
date: 2021-10-07T00:01:12.242Z
tags: libretro, atari, atari800, 5200, atari-5200
editor: markdown
dateCreated: 2021-07-30T16:32:06.727Z
---



## ![](/emulators/license.svg) License

This core is under [**GPLv2**](https://github.com/atari800/atari800/blob/master/COPYING) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Features

| Feature | Supported |
| :---: | :---: |
| Screenshots | ✔ |
| Saves | - |
| States | - |
| Core Options | ✔ |
| Native Cheats | - |
| Controls | ✔ |
| Remapping | ✔ |

## ![](/emulators/bios.svg) BIOS

### List of mandatory bios

| Filename | Description | MD5 | Provided |
| :---: | :---: | :---: | :---: |
| 5200.rom | 5200 | 281f20ea4320404ec820fb7ec0693b38 | ❌ |

### Location

Put the bios like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 🗒 **5200.rom**

## ![](/emulators/roms.png) Roms

### Supported extensions

Roms must have the extension:

* .a52
* .atr
* .bas
* .bin
* .car
* .cdm
* .xex
* .xfd
* .atr.gz
* .xfd.gz
* .zip

This system supports compressed roms in .zip/.7z format. However, be careful, it is only an archive.

The files contained in the .zip/.7z must correspond to the extensions mentioned above.
Moreover, each .zip/.7z file must contain only one compressed rom.

### Location

Put the roms like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 atari5200
┃ ┃ ┃ ┃ ┣ 🗒  **game.zip**

>The roms using **No-Intro** format are highly recommended.
{.is-success}

>For more information about roms, go to [this tutorial](./../../../tutorials/games/generalities/isos-and-roms)!
{.is-info}

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Accessing to options

You can configure various options in two different ways.

* Via RetroArch menu:

┣ 📁 RetroArch Menu
┃ ┣ 📁 Core options
┃ ┃ ┣ 🧩 Name_option

* Via the file `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core options

| Options | Options values | Variables (for configuration override) | Variables values |
| --- | --- | --- | --- |
| Atari System | `400/800 (OS B)` ✅ / `800XL (64K)` / `130XE (128K)` / `Modern XL/XE(320K CS)` / `Modern XL/XE(576K)` / `Modern XL/XE(1088K)` / `5200` | `atari800_system` | `400/800 (OS B)` / `800XL (64K)` / `130XE (128K)` / `Modern XL/XE(320K CS)` / `Modern XL/XE(576K)` / `Modern XL/XE(1088K)` / `5200` |
| Video Standard | `NTSC` ✅ / `PAL` | `atari800_ntscpal` | `NTSC` / `PAL` |
| Internal BASIC (hold OPTION on boot) | `Disabled` ✅ / `Enabled` | `atari800_internalbasic` | `disabled` / `enabled` |
| SIO Acceleration | `Disabled` ✅ / `Enabled` | `atari800_sioaccel` | `disabled` / `enabled` |
| Boot from Cassette | `Disabled` ✅ / `Enabled` | `atari800_cassboot` | `disabled` / `enabled` |
| Hi-Res Artifacting | `Disabled` ✅ / `Enabled` | `atari800_artifacting` | `disabled` / `enabled` |
| Autodetect A5200 CartType | `Disabled` ✅ / `Enabled` | `atari800_opt1` | `disabled` / `enabled` |
| Joy hack A5200 for Robotron | `Disabled` ✅ / `Enabled` | `atari800_opt2` | `disabled` / `enabled` |
| Internal resolution | `336x240` ✅ / `320x240` / `384x240` / `384x272` / `384x288` / `400x300` | `atari800_resolution` |  |
| Retroarch Keyboard type | `Disabled` ✅ / `Enabled` | `atari800_keyboard` | `disabled` / `enabled` |

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/libretro/libretro-atari800/](https://github.com/libretro/libretro-atari800/)
* **Libretro documentation**: [https://docs.libretro.com/library/atari800/](https://docs.libretro.com/library/atari800/)