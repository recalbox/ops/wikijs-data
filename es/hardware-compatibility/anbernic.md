---
title: Anbernic
description: 
published: true
date: 2025-01-22T22:16:09.555Z
tags: anbernic, rg353
editor: markdown
dateCreated: 2023-09-19T08:34:28.970Z
---

## Anbernic RG353

![anbernic-rg353m.png](/compatibility/anbernic-rg353m.png)

La videoconsola retro basada en la CPU RK3566 cuenta con un sistema operativo dual (Android y Linux) compatible con docenas de videoconsolas clásicas de los años 70 a 2000. Cuenta con WiFi para juegos multijugador y una salida HDMI para jugar en pantalla grande.

> Sólo el modelo M es compatible. Los otros modelos a lo mejor lo son pero no han sido testeados.
{.is-warning}

### Características :

* **Procesador :** 
  * RK3566, Quad-Core, Cortex-A55 64 bits, frecuencia hasta los 1,8 Ghz
* **GPU :**
  * Acelerador 3D Mali™ G52
* **RAM :**
  * LPDDR4 2 Go
* **Conectores disponibles:** 
  * 2 X USBC
  * 1 X mini HDMI
  * 2 X Ranuras para tarjetas MicroSD (máximo 512 GB)
  * 1 X Toma de cascos
* **Red :**
  * Wi-Fi 2.4G/5G 802.11 a/b/g/n/ac
  * Bluetooth v4.2
* **Batería :**
  * Li-Po, 3200 mAh
* **Alimentación :**
  * Chargeur téléphone de 5W ou 10W
* **Dimensiones :**
  * 14,5 x 7,05 x 1,59 cm
* **Peso :**
  * 232 grammes
  
   ## Anbernic RG351

![](/compatibility/anbernic-rg351.png)

La Anbernic RG351 es una videoconsola portátil basada en Linux creada por Anbernic. La consola utiliza una tarjeta microSD para el almacenamiento y es una consola sólo de ROM digital. Es la sucesora de la RG350 y se ha convertido en una consola portátil líder en juegos retro.

> Sólo el modelo **V** ha sido probado por el equipo de Recalbox. Los otros modelos de RG351 pueden funcionar tambien, pero no han sido certificados.
{.is-warning}

* **Procesador :** 
  * RK3326, Quad-Core, Cortex-A55 64 bits, fréquence jusqu'à 1,5 Ghz
* **GPU :**
  * Courrier G31 MP2
* **RAM :**
  * DDR3L 1 Go
* **Conectores disponibles:** 
  * 2 X USBC
  * 1 X Ranura para tarjeta MicroSD (máximo 512 GB)
  * 1 X Toma de cascos
* **Red :**
  * Wi-Fi 2.4G/5G 802.11 a/b/g/n/ac
  * Bluetooth v4.2
* **Betería :**
  * Li-Po, 3200 mAh
* **Alimentación :**
  * Batería 3500mAh, hasta 8h de utilización