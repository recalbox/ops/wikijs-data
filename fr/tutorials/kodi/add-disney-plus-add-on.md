---
title: Ajouter l'extension Disney+
description: 
published: true
date: 2024-09-19T09:03:00.601Z
tags: kodi, extensions, disney+
editor: markdown
dateCreated: 2022-12-27T21:43:24.057Z
---

## Introduction

Il existe pour Kodi une extension permettant de lire les vidéos Disney+. Ce tutoriel décrit comment l'installer.

>L'extension n'est pas compatible actuellement avec le Raspberry Pi 4 et 5 car le module de vidéo WideVine pour systèmes ARM 64 bits n'existe pas encore.
>{.is-info}

## Pré-requis

Vous devez activer l'installation depuis un fichier zip dans Kodi :

1. Allez dans `Paramètres` > `Système` > `Extensions`.

2. Activez l'option `Sources inconnues`. Validez lors de la demande de confirmation.

## Installation

1. Lancez Kodi.

2. Vous devez installer en premier un fichier zip contenant les informations du dépôt où se trouve l'extension Disney+. Allez à l'adresse suivante et téléchargez le fichier suivant :

https://slyguy.uk/repository.slyguy.zip

3. Transférez le fichier zip téléchargé dans votre Recalbox à l'emplacement désiré dans le share.

4. Dans Kodi, allez dans `Extensions` > `Installer d'un fichier Zip` et sélectionnez le fichier zip que vous avez téléchargé. Confirmez les messages de confirmation si vous en avez.



5. Une fois installé, toujours dans `Extensions`, allez dans `Installer d'un dépôt`.



6. Trouvez l'extension dans `SlyGuy Repository (slyguy.uk)` > `Extensions Vidéos` > `Disney+`. Ouvrez l'extension pour l'installer.



7. Une fois installé, l'extension doit être visible dans la liste des extensions installées.



## Utilisation

Vous allez devoir vous identifier avant de pouvoir utiliser l'extension.

1. Ouvrez l'extension.

2. Sélectionnez `Connexion`. Insérez votre adresse email ainsi que votre mot de passe dans la fenêtre suivante.



3. Une fois que votre identification sera effectuée, vous aurez une fenêtre vous demandant quel profil utiliser.



4. Maintenant, vous pouvez profiter de Disney+.