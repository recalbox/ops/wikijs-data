---
title: Use USB device as storage
description: To put your games and preserve your SD card
published: true
date: 2021-08-06T09:22:40.021Z
tags: usb, storage
editor: markdown
dateCreated: 2021-08-06T09:22:40.021Z
---

## Introduction

You can easily use a USB storage device (USB stick, external self-powered hard drive etc.) to store your roms, personal files etc.

* First, you need to use a device that uses the exFAT file system. Also be sure that the file system you choose is compatible with your PC's operating system. Recalbox will not format your device, it will only create new files on it.

## Optional: synchronize your USB key or disk with your SD card

Recalbox can manually synchronize your USB device with your SD card. This step is optional but must be done before changing your USB storage settings. Here are the steps:

* Start your Recalbox.
* Connect your USB device whenever you want.
* [Connect via SSH](./../access/root-access-terminal-cli).
* Type the following commands:

```shell
cd /recalbox/scripts
./recalbox-sync.sh list
```

* The last command will give you a list of available devices. For example:

```shell
# ./recalbox-sync.sh list
INTERNAL
DEV A80A-27A5 NO_NAME
```

So the name of my USB stick is NO_NAME, its device reference is A80A-27A5. I can synchronize it.

```shell
# ./recalbox-sync.sh sync A80A-27A5
sending incremental file list
./
.keep
bios/
bios/readme.txt
bios/readme.txt

...

sent 111,817,693 bytes received 8,256 bytes 9,723,995.57 bytes/sec
total size is 111,758,686 speedup is 1.00
rsync error: some files/attrs were not transferred (see previous errors) (code 23) at main.c(1178) [sender=3.1.2]
```

Here you go! Don't worry about the errors. Now your USB device contains the copy of the data from your SD card. You can continue with the next step.

## Configure Recalbox to use a USB storage device

* Connect your device to your Recalbox and turn it on. Once in the Recalbox interface, press the `Start` button on your controller, go to `SYSTEM SETTINGS` > `STORAGE DEVICE`. Now select your device from the list, then confirm and wait for the system to reboot. During this reboot phase, Recalbox will create at the root of your device, a new directory named `recalbox` which will contain the whole tree of your `/share` partition.

To add your files (roms, game backups, bios, scrape data etc...) to your storage device, you have two options:

* You can turn off your Recalbox and connect your device to your PC. All you have to do is copy your personal files to your device from your PC. Once the transfer is finished, you just have to plug your device back to your Recalbox, turn it on and play.
* You can also transfer over the local network by keeping your Recalbox turned on. You will need your [IP address](./../../network/ip/discover-recalbox-ip).

With this method, the system (on the SD card) and the share partition (on the device) are separated. So if you were to reinstall your system, you would keep all your user data. You will then just have to reconnect your device to your recalbox, then select it in the system, and play.

## What if, after the Recalbox reboot, the system still doesn't see the hard drive?

Sometimes, after selecting the device in EmulationStation and restarting, Recalbox fails to create the file system on it. So it continues to use the files on the SD card. This happens especially with some hard drives that are slow to initialize.

What you can do:

* Mount the [boot partition](./../access/remount-partition-with-write-access) as read/write.
* Type the following commands:

```shell
cd /boot
nano recalbox-boot.conf
```

* Add this line:

```ini
sharewait=30
```

* Save by `Ctrl` + `X`, `Y` to confirm and `Enter`.
* Type `reboot` and confirm, Recalbox restarts.

If this still doesn't work, increase the value of `sharewait`.