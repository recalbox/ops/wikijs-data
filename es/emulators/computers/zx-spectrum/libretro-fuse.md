---
title: Libretro Fuse
description: 
published: true
date: 2024-07-18T18:56:22.013Z
tags: libretro, zx, spectrum, fuse
editor: markdown
dateCreated: 2023-09-11T12:08:22.155Z
---



## ![](/emulators/license.svg) Licencia

El core posee la siguiente licenci [**GPLv3**](https://github.com/libretro/fuse-libretro/blob/master/LICENSE).

## ![](/emulators/compatibility.png) Compatibilidad

 | RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | 
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | 

## ![](/emulators/features.png) Funcionalidades

| Funcionalidad | ¿Soportada? |
| :---: | :---: |
| Reinicio | ✔ |
| Capturas de pantalla | ✔ |
| Grabaciones instantáneas | ✔ |
| Rebobinado | ✔ |
| Opciones del core | ✔ |
| Controles | ✔ |
| Remapping | ✔ |

## ![](/emulators/bios.svg) BIOS

### Lista de BIOS opcionales

| Nombre del fichero | Descripción | MD5 | ¿Preinstalada? |
| :---: | :---: | :---: | :---: |
| 128p-0.rom | Pentagon 1024K ROM | a249565f03b98d004ee7f019570069cd | ❌ |
| 128p-1.rom | Pentagon 1024K ROM | 6e09e5d3c4aef166601669feaaadc01c | ❌ |
| gluck.rom | Pentagon 1024K ROM | d5869034604dbfd2c1d54170e874fd0a | ❌ |
| trdos.rom | Pentagon 1024K ROM | 0c42e3b9ab8dd91ea096f1d0c07c55e5 0da70a5d2a0e733398e005b96b7e4ba6 14399030d8228ca1b16872ed426a5835 3f8a2a239a10b6694ec79148a292cfb4 4123fd0b5c218ef704770596dc6533e1 48b5da4079ff8394852429e53cfc607f 4a3e2b04982ac6c594adb6793be4d6e7 4da473775c4badcc83ab5d86dc8231de 53e2f417c6996df9af170e147df8e369 5c097b0fb75bb7147104d6e77db0300a 62cbbdca554c8c23676618d4c83ef40e 6459c606ff23a610e504d0387383148a 7031f2610845d39c54c97097afa8fa03 a3242d31a0624a64a56f3a6cb5484e7c a701f54355b53fa93fb8599933d518b7 a92db09c9aa3cfda478a9bf1eec7ff90 aa9f9acf05382aff569dfdadde4ef8f2 b08fa49b5de8448e94936a9d061dc0f5 b4c9634312b796063015450daef13dfa b6d8db853c24621cbd5fef3c892c976c c511eaa8fcc968cc13baf7ad80f3aea3 cc46c7bacbbaf528a864113c76d9b9c9 d6f43c70c003f07d0a694f81ff55db95 dd70541ed6a6e8176e8dace64f9344ad | ❌ |
| 256s-0.rom | Scorpion 256K ROM | b9fda5b6a747ff037365b0e2d8c4379a | ❌ |
| 256s-1.rom | Scorpion 256K ROM | 643861ad34831b255bf2eb64e8b6ecb8 | ❌ |
| 256s-2.rom | Scorpion 256K ROM | d8ad507b1c915a9acfe0d73957082926 | ❌ |
| 256s-3.rom | Scorpion 256K ROM | ce0723f9bc02f4948c15d3b3230ae831 | ❌ |

### Emplazamiento

Disponed vuestras bios como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 fuse
┃ ┃ ┃ ┃ ┣ 🗒 **128p-0.rom**
┃ ┃ ┃ ┃ ┣ 🗒 **128p-1.rom**
┃ ┃ ┃ ┃ ┣ 🗒 **gluck.rom**
┃ ┃ ┃ ┃ ┣ 🗒 **trdos.rom**
┃ ┃ ┃ ┃ ┣ 🗒 **256s-0.rom**
┃ ┃ ┃ ┃ ┣ 🗒 **256s-1.rom**
┃ ┃ ┃ ┃ ┣ 🗒 **256s-2.rom**
┃ ┃ ┃ ┃ ┣ 🗒 **256s-3.rom**

## ![](/emulators/roms.png) Roms

### Extensiones de roms soportadas

Las roms deben tener las extensiones siguientes:

* .csw
* .dsk
* .rzx
* .scl
* .sct
* .szx
* .tap
* .trd
* .tzx
* .z80
* .zip
* .7z

Este sistema soporta las roms comprimidas en formato .zip/.7z. No obstante, se trata sólo de un archivo.

Los ficheros contenidos en los .zip/.7z deben tener las extensiones citadas previamente.
Además, cada fichero .zip/.7z sólo debe contener un fichero rom al interior.

### Emplazamiento

Disponed vuestras roms como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 zxspectrum
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.zip**

>Se aconsejan las roms del proyecto **TOSEC**.
{.is-success}

>Para más información sobre las roms, consultad [este tutorial](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

>Para conservar las configuraciones personalizadas cuando se produce una actualización del sistema Recalbox, os aconsejamos utilizar las [Sobrecargas de configuración](./../../../advanced-usage/configuration-override).
{.is-info}

### Acceder a las opciones

Podéis configurar las opciones del core de dos formas diferentes.

* Via el Menú RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via el fichero `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Opciones del core

| Opción | Valor de la opción | Variable (para la sobrecarga) | Valores posibles |
| --- | --- | --- | --- |
| Model (needs content load) | `Spectrum 48K` ✅ / `Spectrum 48K (NTSC)` / `Spectrum 128K` / `Spectrum +2` / `Spectrum +2A` / `Spectrum +3` / `Spectrum +3e` / `Spectrum SE` / `Timex TC2048` / `Timex TC2068` / `Timex TS2068` / `Spectrum 16K` / `Pentagon 128K` / `Pentagon 512K` / `Pentagon 1024` / `Scorpion 256K` | `fuse_machine` | `Spectrum 48K` / `Spectrum 48K (NTSC)` / `Spectrum 128K` / `Spectrum +2` / `Spectrum +2A` / `Spectrum +3` / `Spectrum +3e` / `Spectrum SE` / `Timex TC2048` / `Timex TC2068` / `Timex TS2068` / `Spectrum 16K` / `Pentagon 128K` / `Pentagon 512K` / `Pentagon 1024` / `Scorpion 256K` |
| Size Video Border | `full` ✅ / `medium` / `small` / `minimum` / `none` | `fuse_size_border` | `full` / `medium` / `small` / `minimum` / `none` |
| Tape Auto Load | `Activé` ✅ / `Désactivé` | `fuse_auto_load` | `enabled` / `disabled` |
| Tape Fast Load | `Activé` ✅ / `Désactivé` | `fuse_fast_load` | `enabled` / `disabled` |
| Tape Load Sound | `Activé` ✅ / `Désactivé` | `fuse_load_sound` | `enabled` / `disabled` |
| Speaker Type | `tv speaker` ✅ / `beeper` / `unfiltered` | `fuse_speaker_type` | `tv speaker` / `beeper` / `unfiltered` |
| AY Stereo Separation | `none` ✅ / `acb` / `abc` | `fuse_ay_stereo_separation` | `none` / `acb` / `abc` |
| Transparent Keyboard Overlay | `Activé` ✅ / `Désactivé` | `fuse_key_ovrlay_transp` | `enabled` / `disabled` |
| Time to Release Key in ms | `100` / `300` / `500` ✅ / `1000` | `fuse_key_hold_time` | `500` / `1000` / `100` / `300` |
| Joypad Left mapping | `<none>` ✅ / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` / `Enter` / `Caps` / `Symbol` / `Space` | `fuse_joypad_left` | `<none>` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` / `Enter` / `Caps` / `Symbol` / `Space` |
| Joypad Right mapping | `<none>` ✅ / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` / `Enter` / `Caps` / `Symbol` / `Space` | `fuse_joypad_right` | `<none>` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` / `Enter` / `Caps` / `Symbol` / `Space` |
| Joypad Up mapping | `<none>` ✅ / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` / `Enter` / `Caps` / `Symbol` / `Space` | `fuse_joypad_up` | `<none>` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` / `Enter` / `Caps` / `Symbol` / `Space` |
| Joypad Down mapping | `<none>` ✅ / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` / `Enter` / `Caps` / `Symbol` / `Space` | `fuse_joypad_down` | `<none>` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` / `Enter` / `Caps` / `Symbol` / `Space` |
| Joypad Start mapping | `<none>` ✅ / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` / `Enter` / `Caps` / `Symbol` / `Space` | `fuse_joypad_start` | `<none>` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` / `Enter` / `Caps` / `Symbol` / `Space` |
| Joypad A button mapping | `<none>` ✅ / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` / `Enter` / `Caps` / `Symbol` / `Space` | `fuse_joypad_a` | `<none>` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` / `Enter` / `Caps` / `Symbol` / `Space` |
| Joypad B button mapping | `<none>` ✅ / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` / `Enter` / `Caps` / `Symbol` / `Space` | `fuse_joypad_b` | `<none>` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` / `Enter` / `Caps` / `Symbol` / `Space` |
| Joypad X button mapping | `<none>` ✅ / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` / `Enter` / `Caps` / `Symbol` / `Space` | `fuse_joypad_x` | `<none>` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` / `Enter` / `Caps` / `Symbol` / `Space` |
| Joypad Y button mapping | `<none>` ✅ / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` / `Enter` / `Caps` / `Symbol` / `Space` | `fuse_joypad_y` | `<none>` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` / `Enter` / `Caps` / `Symbol` / `Space` |
| Joypad L button mapping | `<none>` ✅ / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` / `Enter` / `Caps` / `Symbol` / `Space` | `fuse_joypad_l` | `<none>` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` / `Enter` / `Caps` / `Symbol` / `Space` |
| Joypad R button mapping | `<none>` ✅ / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` / `Enter` / `Caps` / `Symbol` / `Space` | `fuse_joypad_r` | `<none>` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` / `Enter` / `Caps` / `Symbol` / `Space` |
| Joypad L2 button mapping | `<none>` ✅ / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` / `Enter` / `Caps` / `Symbol` / `Space` | `fuse_joypad_l2` | `<none>` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` / `Enter` / `Caps` / `Symbol` / `Space` |
| Joypad R2 button mapping | `<none>` ✅ / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` / `Enter` / `Caps` / `Symbol` / `Space` | `fuse_joypad_r2` | `<none>` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` / `Enter` / `Caps` / `Symbol` / `Space` |
| Joypad L3 button mapping | `<none>` ✅ / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` / `Enter` / `Caps` / `Symbol` / `Space` | `fuse_joypad_l3` | `<none>` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` / `Enter` / `Caps` / `Symbol` / `Space` |
| Joypad R3 button mapping | `<none>` ✅ / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` / `Enter` / `Caps` / `Symbol` / `Space` | `fuse_joypad_r3` | `<none>` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `a` / `b` / `c` / `d` / `e` / `f` / `g` / `h` / `i` / `j` / `k` / `l` / `m` / `n` / `o` / `p` / `q` / `r` / `s` / `t` / `u` / `v` / `w` / `x` / `y` / `z` / `Enter` / `Caps` / `Symbol` / `Space` |


## ![](/emulators/external-links.png) Enlaces externos

* **Código fuente utilizado** : [https://github.com/libretro/fuse-libretro/](https://github.com/libretro/fuse-libretro/)
* **Documentación Libretro** : [https://docs.libretro.com/library/fuse/](https://docs.libretro.com/library/fuse/)