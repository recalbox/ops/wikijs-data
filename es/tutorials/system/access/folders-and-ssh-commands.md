---
title: Directorios y comandos SSH
description: 
published: true
date: 2023-09-13T20:22:14.496Z
tags: ssh, carpetas, comandos, directorios
editor: markdown
dateCreated: 2023-09-13T20:22:14.496Z
---

En esta sección se presentan varios comandos SSH para efectuar varias acciones específicas en caso de necesidad.

## Comandos SSH

Para conectaros a vuestra Recalbox :

```shell
ssh root@[adresse IP address de votre Recalbox]
```

Montar la partición sistema en modo lectura/escritura :

```shell
mount -o remount,rw / 
```

Montar la partición de arranque en modo lectura/escritura :

```shell
mount -o remount,rw /boot
```

Parar EmulationStation :

```shell
es stop
```

Arrancar EmulationStation :

```shell
es start
```

Reiniciar EmulationStation :

```shell
es restart
```

Cambiar la contraseña del usuario :

```shell
passwd
```

Reiniciar Recalbox :

```shell
reboot
```

Apagar Recalbox :

```shell
poweroff
```

Hacer una captura de pantalla :

```shell
raspi2png
```

Este comando creará un fichero dentro de `/recalbox/share/screenshots/` con el nombre `screenshot-date-du-screenTheure-du-screen.png`.

Podéis dar un nombre personalizado a vuestra captura con la opción `-p nombredelfichero.png`

Información sobre las interfaces de red:

```shell
ifconfig
```

Verificación de la temperatura de la CPU:

```shell
cat /sys/class/thermal/thermal_zone0/temp  
```

A continuación, dividid el resultado por 1000 para obtener la temperatura en grados Celsius (por ejemplo: 54034 se convierte en 54°C).

## Ficheros

Configuración específica de la Raspberry Pi, como parámetros de overscan, overclocking, modo de vídeo por defecto, etc... :

```shell
nano /boot/recalbox-user-config.txt  
```

Fichero de configuration Recalbox :

```shell
nano /recalbox/share/system/recalbox.conf
```