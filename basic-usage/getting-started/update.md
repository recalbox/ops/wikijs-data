---
title: Update
description: 
published: true
date: 2024-11-11T07:48:01.452Z
tags: update
editor: markdown
dateCreated: 2021-06-28T23:08:41.955Z
---

## Upgrade process

Here are the **upgrade options** for Recalbox.  We hope this summary will help you and give you the answers you are looking for.

## Depending on your situation...

### Is it possible to upgrade from Recalbox x.x to Recalbox 7?

_**No**, it is not possible to upgrade to the new version of Recalbox 7. A clean install is required. See below for instructions._

### What if I am using a theme other than the one provided by default?

* Make sure you are up to date with the systems, configuration files for your theme.
* Contact the author of your theme or the subject of your theme to check if a new updated version is available.
* Use a custom theme in 720p on Raspberry Pi. In 1080p, your Raspberry Pi will suffer.
* We do not provide support for unofficial Recalbox custom themes. The theme creators are the only ones to support their themes.
* Please use the default Recalbox theme to check your bugs before posting your problem on the forum.

### What if I use OVERLAYS?

If you have created your own overlays, RetroArch has added a new aspect ratio that shifts some overlays:

* Test 22 or 23 on this line in the overlays configurations:

```ini
aspect_ratio_index = 21
```

### Arcade systems

Recalbox regularly updates the cores of the MAME & FinalBurn Neo arcade emulators. As a result, you need to adapt your romsets :

* The global dat files are located in `/recalbox/share/arcade`. You can find more information on [this page](./../../tutorials/games/generalities/isos-and-roms/sort-and-update-your-roms).

* To make a parent-only dat, neogeo parent only etc... you will just have to use this [datutil tutorial](./../../tutorials/utilities/dat-management/datutil) to create them.

* For current versions of arcade romsets, visit [this page](./../../emulators/arcade).

### Your Raspberry Pi in a box

* Make sure you have a good ventilation.
* A good power cable.
* If you have any problems, take your Raspberry Pi out of your case and test Recalbox 7 as is.

### Our advice:

* Have a backup of your share on your PC or external hard drive.
* Use a removable media (USB key or external hard drive), you will quickly understand that a reinstallation is faster.
* Use an external scraper
* Always have 2 microsd cards, 16GB is enough for the system (one for the stable version, another for your tests, or beta tests) where the interest to have its roms on removable media.
* Install heat sinks or fans if necessary:
  * If you overlock your Raspberry Pi.
  * If your Recalbox is in a case.

## Launch the update

>The **Recalbox update** is accessible by pressing `START` > `UPDATES`.
{.is-info}

* Set up your wifi or connect a network cable to your Recalbox.
* Select `UPDATES`.
* Then `START UPDATE`. 

The system will automatically restart after the update is downloaded.