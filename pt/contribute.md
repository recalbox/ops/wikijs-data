---
title: Como contribuir para a Wiki do Recalbox
description: Descubra como você pode contribuir para a Wiki
published: true
date: 2024-06-02T16:51:24.936Z
tags: contribute, edition
editor: markdown
dateCreated: 2021-06-24T00:20:09.840Z
---

## Nomenclatura de documentos

Os documentos seguem uma nomenclatura específica que permite que a Wiki do Recalbox tenha URLs idênticos para as diferentes traduções dos documentos. 

Por exemplo, esta página, no idioma Francês, tem a URL :
`/fr/contribute`
E em Português:
`/pt/contribute`

Isto permite encontrar muito rapidamente a tradução de um documento, ou criar traduções mais facilmente para os colaboradores.

É por isso que as URLs dos documentos aparecem em inglês.

## Regras de formatação

Os documentos estão escritos em [markdown](https://docs.requarks.io/editors/markdown). Markdown é uma ferramenta de formatação de documentos muito simples e acessível. Sinta-se à vontade para usar este documento como um exemplo para começar a escrever o seu próprio.

### Títulos

O título da página é definido nas opções de página. É o único título de nível 1 do documento.
![title-metadata.png](/contribute/title-metadata.png)

No conteúdo das páginas, os títulos começam com o nível 2 (*hashtag* dupla em markdown: `##`):
![double-hash.png](/contribute/double-hash.png)

### Blocos de citação

Para alertar o usuário sobre informações ou elementos importantes, você pode usar blocos de citação:

```markdown
> Esta é uma informação
> {.is-info}
```

> Esta é uma informação
> {.is-info}

## Versão markdown suportada

Para mais informações sobre os recursos de markdown suportados, visite https://docs.requarks.io/editors/markdown.

## Imagens

Ao adicionar imagens a seus documentos, certifique-se de replicar a hierarquia de documentos nas pastas de imagens, omitindo a linguagem.

Por exemplo, para esta página, as imagens serão criadas no diretório `/contribute`.

## Internacionalização

### Procedimento para tradução de uma página

1. Acesse a Wiki com sua conta Discord
2. Vá para a página a traduzir, no idioma padrão da Wiki (FR)
3. Clique no ícone do idioma no topo da página e selecione o idioma para o qual você deseja traduzir a página
![select-lang.png](/contribute/select-lang.png)
4. A tela oferecerá a você para criar uma nova página, clique no botão criar página
5. Clique no editor Markdown
![markdown.png](/contribute/markdown.png)
6. Abra uma nova aba na página original e clique em editar no menu de ações da página
![edit.png](/contribute/edit.png)
7. Acesse o título, as *tags* (etiquetas) e a descrição da página, clicando no botão PÁGINA:
![editpage.png](/contribute/editpage.png)
8. Você pode traduzir estes elementos e adicioná-los no formulário da sua página recém-criada
9. Após fechar o *popup* de título na página original, copie o conteúdo da página e vá para https://www.deepl.com/translator para colar seu texto e obter uma versão pré-traduzida da página.
![deepl.png](/contribute/deepl.png)
10. Agora você só precisa verificar a tradução, as imagens e os links da página recém-criada!
11. Não se esqueça de remover a última linha do texto que é adicionada automaticamente pelo deepl: (traduzido por deepl)
12. Muito obrigado por sua contribuição 🙏


### Links relativos

Exceto em casos excepcionais, os links nas páginas devem ser relativos ao idioma atual.

Por exemplo, nesta página, um link para a página `/pt/home` deve ser preenchido com a URL `./home`  :

```markdown
Visite a [Página inicial](./home)
```

Isto permite criar páginas de tradução que já possuem links válidos.

### Adicionando novos idiomas

Se seu idioma não aparece no menu wiki e você deseja iniciar uma tradução, entre em contato com a equipe em nossas **[redes sociais, Discord ou fórum](./presentation/useful-links).**

## Nível de dificuldade dos tutoriais

A fim de tornar a experiência Recalbox divertida e acessível, quando você cria um tutorial, mencione o nível de dificuldade dele, considerando a seguinte escala:

1.  Qualquer ação que não exija a entrada das configurações avançadas no menu geral ou nos arquivos .conf da Recalbox será considerada como : _nível_ :star:

1. Qualquer ação que exija a configuração das configurações avançadas para fazer alterações que possam causar instabilidades : _nível_:star::star:

1.  Qualquer ação que exija a alteração de entradas no arquivo recalbox.conf: _nível_:star::star::star: (cuidado recomendado).

1.  Qualquer ação que exija manipulações SSH e/ou modificações no arquivo de inicialização (MobaXTerm ou Powershell): _nível_:star::star::star::star:

1.  Qualquer ação que exija um abrir um *issue*, ou fazer um *merge request*: _nível_:star::star::star::star:

1.  Finalmente, qualquer tutorial que tente explicar o uso do clrmamepro será automaticamente classificado como: :scream: _**Aterrorizante**_

Exemplo:

- Adicionar uma ROM, uma BIOS, mudar o idioma: nível :star: 
- Alterar o tema, restaurar as configurações de fábrica, alterar o núcleo padrão: nível :star: :star: 
- Habilitar a tradução inteligente (Retroarch IA): nível :star: :star: :star: 


*Para fazer o emoji de estrela aparecer em seus tutoriais, coloque ":" na frente e atrás da palavra "star" = : star: tudo junto.*


