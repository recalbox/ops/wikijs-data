---
title: Libretro bsnes
description: 
published: true
date: 2024-07-24T23:49:52.452Z
tags: libretro, gbc, bsnes, 8.1+
editor: markdown
dateCreated: 2022-03-01T08:13:41.031Z
---



## ![](/emulators/license.svg) License

This core is under [**GPLv3**](https://github.com/libretro/bsnes-libretro/blob/master/LICENSE.txt) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ❌ | ❌ | ❌ | ❌ | ❌ | ❌ | ✅ | ❌ |

## ![](/emulators/features.png) Features

| Feature | Supported |
| :---: | :---: |
| Restart | ✔ |
| Screenshots | ✔ |
| Saves | ✔ |
| States | ✔ |
| Rewind | ✔ |
| Netplay | ✔ |
| RetroAchievements | ✔ |
| RetroArch Cheats | ✔ |
| Controls | ✔ |
| Remapping | ✔ |
| Subsystems | ✔ |
| Softpatching | ✔ |
| Crop Overscan | ✔ |

## ![](/emulators/bios.svg) BIOS

### List of mandatory bios

| Filename | Description | MD5 | Provided |
| :---: | :---: | :---: | :---: |
| SGB1.sfc | Super Game Boy BIOS | b15ddb15721c657d82c5bab6db982ee9 | ❌ |
| SGB2.sfc | Super Game Boy 2 BIOS | 8ecd73eb4edf7ed7e81aef1be80031d5 | ❌ |

### Location

Put your bios like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 sgb
┃ ┃ ┃ ┃ ┣ 🗒 **SGB1.sfc**
┃ ┃ ┃ ┃ ┣ 🗒 **SGB2.sfc**

## ![](/emulators/roms.png) Roms

### Supported extensions

Roms must have the extension:

* .gb
* .gbc
* .zip
* .7z

This system supports compressed roms in .zip/.7z format. However, be careful, it is only an archive.

The files contained in the .zip/.7z must correspond to the extensions mentioned above.
Moreover, each .zip/.7z file must contain only one compressed rom.

### Location

Put the roms like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 gb
┃ ┃ ┃ ┃ ┣ 🗒 **game.zip**

>The roms using **No-Intro** format are highly recommended.
{.is-success}

>For more information about roms, go to [this tutorial](./../../../tutorials/games/generalities/isos-and-roms)!
{.is-info}

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Accessing to options

You can configure various options in two different ways.

* Via RetroArch menu:

┣ 📁 RetroArch Menu
┃ ┣ 📁 Core options
┃ ┃ ┣ 🧩 Name_option

* Via the file `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core options

| Options | Options values | Variables (for configuration override) | Variables values |
| --- | --- | --- | --- |
| Preferred Aspect Ratio | `Auto` ✅ / `Pixel Perfect` / `4:3` / `NTSC` / `PAL` | `bsnes_aspect_ratio` | `Auto` / `8:7` / `4:3` / `NTSC` / `PAL` |
| Crop Overscan | `8 pixels`✅ / `Disabled` | `bsnes_ppu_show_overscan` | `rrr` / `disabled` |
| Blur Emulation | `Disabled` ✅ / `Enabled` | `bsnes_blur_emulation` | `disabled` / `enabled` |
| Filter | `None` ✅ / `NTSC (RF)` / `NTSC (Composite)` / `NTSC (S-Video)` / `NTSC (RGB)` | `bsnes_blur_emulation` | `None` / `NTSC (RF)` / `NTSC (Composite)` / `NTSC (S-Video)` / `NTSC (RGB)` |
| PPU (Video) - Fast Mode | `Enabled` ✅ / `Disabled` | `bsnes_ppu_fast` | `enabled` / `disabled` |
| PPU (Video) - Deinterlace | `Enabled` ✅ / `Disabled` | `bsnes_ppu_deinterlace` | `enabled` / `disabled` |
| PPU (Video) - No Sprite Limit | `Disabled` ✅ / `Enabled` | `bsnes_ppu_no_sprite_limit` | `disabled` / `enabled` |
| PPU (Video) - No VRAM Blocking | `Disabled` ✅ / `Enabled` | `bsnes_ppu_no_vram_blocking` | `disabled` / `enabled` |
| DSP (Audio) - Fast Mode | `Enabled` ✅ / `Disabled` | `bsnes_dsp_fast` | `enabled` / `disabled` |
| DSP (Audio) - Cubic Interpolation | `Disabled` ✅ / `Enabled` | `bsnes_dsp_cubic` | `disabled` / `enabled` |
| DSP (Audio) - Echo Shadow RAM | `Disabled` ✅ / `Enabled` | `bsnes_dsp_echo_shadow` | `disabled` / `enabled` |
| HD Mode 7 - Scale | `240p (x1)` / `480p (x2)` ✅ / `720p (x3)` / `960p (x4)` / `1200p (x5)` / `1440p (x6)` / `1680p (x7)` / `1920p (x8)` | `bsnes_mode7_scale` | `1x` / `2x` / `3x` / `4x` / `5x` / `6x` / `7x` / `8x` |
| HD Mode 7 - Perspective Correction | `Enabled` ✅ / `Disabled` | `bsnes_mode7_perspective` | `enabled` / `disabled` |
| HD Mode 7 - Supersampling | `Disabled` ✅ / `Enabled` | `bsnes_mode7_supersample` | `disabled` / `enabled` |
| HD Mode 7 - HD->SD Mosaic | `Enabled` ✅ / `Disabled` | `bsnes_mode7_mosaic` | `enabled` / `disabled` |
| Internal Run-Ahead | `Disabled ✅ / 1 / 2 / 3 / 4` | `bsnes_run_ahead_frames` | `disabled`/ `1` / `2` / `3` / `4` |
| Coprocessors - Fast Mode | `Enabled` ✅ / `Disabled` | `bsnes_coprocessor_delayed_sync` | `enabled` / `disabled` |
| Coprocessors - Prefer HLE | `Enabled` ✅ / `Disabled` | `bsnes_coprocessor_prefer_hle` | `enabled` / `disabled` |
| Hotfixes | `Disabled` ✅ / `Enabled` | `bsnes_hotfixes` | `disabled` / `enabled` |
| Entropy (randomization) | `Low` ✅ / `High` / `None` | `bsnes_entropy` | `Low` / `High` / `None` |
| CPU Fast Math | `Disabled` ✅ / `Enabled` | `bsnes_cpu_fastmath` | `disabled` / `enabled` |
| Overclocking - CPU | `10%` / `20%` / `30%` / `40%` / `50%` / `60%` / `70%` / `80%` / `90%` / `100% (Default)` ✅ / `110%` / `120%` / `130%` / `140%` / `150%` / `160%` / `170%` / `180%` / `190%` / `200%` / `210%` / `220%` / `230%` / `240%` / `250%` / `260%` / `270%` / `280%` / `290%` / `300%` / `310%` / `320%` / `330%` / `340%` / `350%` / `360%` / `370%` / `380%` / `390%` / `400%` | `bsnes_cpu_overclock` | `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` / `11%` / `120` / `130` / `140` / `150` / `160` / `170` / `180` / `190` / `200` / `210` / `220` / `230` / `240` / `250` / `260` / `270` / `280` / `290` / `300` / `310` / `320` / `330` / `340` / `350` / `360` / `370` / `380` / `390` / `400` |
| Overclocking - CPU | `10%` / `20%` / `30%` / `40%` / `50%` / `60%` / `70%` / `80%` / `90%` / `100% (Default)` ✅ / `110%` / `120%` / `130%` / `140%` / `150%` / `160%` / `170%` / `180%` / `190%` / `200%` / `210%` / `220%` / `230%` / `240%` / `250%` / `260%` / `270%` / `280%` / `290%` / `300%` / `310%` / `320%` / `330%` / `340%` / `350%` / `360%` / `370%` / `380%` / `390%` / `400%` | `bsnes_cpu_overclock` | `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` / `11%` / `120` / `130` / `140` / `150` / `160` / `170` / `180` / `190` / `200` / `210` / `220` / `230` / `240` / `250` / `260` / `270` / `280` / `290` / `300` / `310` / `320` / `330` / `340` / `350` / `360` / `370` / `380` / `390` / `400` |
| Overclocking - SA-1 Coprocessor | `10%` / `20%` / `30%` / `40%` / `50%` / `60%` / `70%` / `80%` / `90%` / `100% (Default)` ✅ / `110%` / `120%` / `130%` / `140%` / `150%` / `160%` / `170%` / `180%` / `190%` / `200%` / `210%` / `220%` / `230%` / `240%` / `250%` / `260%` / `270%` / `280%` / `290%` / `300%` / `310%` / `320%` / `330%` / `340%` / `350%` / `360%` / `370%` / `380%` / `390%` / `400%` | `bsnes_cpu_sa1_overclock` | `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` / `11%` / `120` / `130` / `140` / `150` / `160` / `170` / `180` / `190` / `200` / `210` / `220` / `230` / `240` / `250` / `260` / `270` / `280` / `290` / `300` / `310` / `320` / `330` / `340` / `350` / `360` / `370` / `380` / `390` / `400` |
| Overclocking - SuperFX Coprocessor | `10%` / `20%` / `30%` / `40%` / `50%` / `60%` / `70%` / `80%` / `90%` / `100% (Default)` ✅ / `110%` / `120%` / `130%` / `140%` / `150%` / `160%` / `170%` / `180%` / `190%` / `200%` / `210%` / `220%` / `230%` / `240%` / `250%` / `260%` / `270%` / `280%` / `290%` / `300%` / `310%` / `320%` / `330%` / `340%` / `350%` / `360%` / `370%` / `380%` / `390%` / `400%` / `410%` / `420%` / `430%` / `440%` / `450%` / `460%` / `470%` / `480%` / `490%` / `500%` / `510%` / `520%` / `530%` / `540%` / `550%` / `560%` / `570%` / `580%` / `590%` / `600%` / `610%` / `620%` / `630%` / `640%` / `650%` / `660%` / `670%` / `680%` / `690%` / `700%` / `710%` / `720%` / `730%` / `740%` / `750%` / `760%` / `770%` / `780%` / `790%` / `800%` | `bsnes_cpu_sfx_overclock` | `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` / `11%` / `120` / `130` / `140` / `150` / `160` / `170` / `180` / `190` / `200` / `210` / `220` / `230` / `240` / `250` / `260` / `270` / `280` / `290` / `300` / `310` / `320` / `330` / `340` / `350` / `360` / `370` / `380` / `390` / `400` / `410` / `420` / `430` / `440` / `450` / `460` / `470` / `480` / `490` / `500` / `510` / `520` / `530` / `540` / `550` / `560` / `570` / `580` / `590` / `600` / `610` / `620` / `630` / `640` / `650` / `660` / `670` / `680` / `690` / `700` / `710` / `720` / `730` / `740` / `750` / `760` / `770` / `780` / `790` / `800` |
| Preferred Super Game Boy BIOS (Restart Required) | `SGB1.sfc` ✅ / `SGB2.sfc` | `bsnes_sgb_bios` | `Super Game Boy (SGB1.sfc)` / `Super Game Boy 2 (SGB2.sfc)` |
| Hide SGB Border | `Disabled` ✅ / `Enabled` | `bsnes_hide_sgb_border` | `disabled` / `enabled` |
| Touchscreen Light Gun | `Enabled` ✅ / `Disabled` | `bsnes_touchscreen_lightgun` | `enabled` / `disabled` |
| Super Scope Reverse Trigger Buttons | `Disabled` ✅ / `Enabled` | `bsnes_touchscreen_lightgun_superscope_reverse` | `disabled` / `enabled` |

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/libretro/bsnes-libretro/](https://github.com/libretro/bsnes-libretro/)
* **Libretro documentation**: [https://docs.libretro.com/library/bsnes_performance](https://docs.libretro.com/library/bsnes_performance)