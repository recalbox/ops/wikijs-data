---
title: Glossário
description: Lista de termos relacionados ao Recalbox.
published: true
date: 2021-06-26T15:20:23.655Z
tags: 
editor: markdown
dateCreated: 2021-06-25T23:19:47.473Z
---

Esta seção dá a definição dos termos técnicos relacionados ao Recalbox, se tem uma palavra que você não entende, esta página deve te ajudar.

## A

* **Arcade**: O arcade geralmente se refere a máquinas de videogame nas quais as moedas são inseridas para obter créditos (também conhecidos como salão de jogos em Portugal, ou fliperamas no Brasil). Devido a sua natureza (um grande número de máquinas diferentes, onde consoles geralmente têm um hardware definido no qual um grande número de jogos será executado), é importante entender como executar os emuladores associados, pois colocar arquivos sem saber exatamente o que você está fazendo normalmente não funcionará. Também, no contexto do Recalbox, os jogos Neogeo são emulados da mesma forma que os títulos de arcade. 
* **API:** Interface de Programação de Aplicativos.
* **Arquivo de suporte**: ver *Support Archive*.

## B

* **Big-Endian:** é a forma mais comum de armazenar arquivos [binários]https://techlib.wiki/definition/binary.html). Ele coloca primeiro o valor mais significativo (ou maior), seguido por valores menos significativos. Por exemplo, a representação do [número inteiro](https://techlib.wiki/definition/integer.html) 123 coloca primeiro o valor de centenas (1), seguido pelo valor de dezenas (2), depois o valor de unidades (3) ou [123]. 
* **BIOS**: Arquivos representando programas lidos pelo sistema original, exigidos por alguns emuladores para funcionar.
* **Buildroot:** Um conjunto de *Makefiles* e *patches* que simplifica e automatiza o processo de construção de um ambiente Linux completo e inicializável para um sistema embarcado, enquanto usa a compilação cruzada para permitir que múltiplas plataformas alvo sejam construídas em um único sistema de desenvolvimento baseado em Linux. O Buildroot pode criar automaticamente a cadeia de ferramentas de compilação cruzada necessária, criar um sistema de arquivos raiz, compilar uma imagem do kernel Linux e gerar um carregador de inicialização para o sistema incorporado alvo, ou pode executar qualquer combinação independente destas etapas. Por exemplo, um *cross-compiler* já instalado pode ser usado independentemente, enquanto o Buildroot cria apenas o sistema de arquivos raiz.

## C

* **Changelog:** Os arquivos de [changelog](https://gitlab.com/recalbox/recalbox/raw/master/CHANGELOG.md) e as [notas de lançamento](https://gitlab.com/recalbox/recalbox/raw/master/RELEASE-NOTES.md) são acessíveis a todos.
* **CHD**: Imagem do disco do jogo Arcade utilizado por alguns jogos MAME, armazena uma versão comprimida de todo o conteúdo do disco rígido de uma máquina arcade.
* **Cheats** _(ou Trapaças)_: No contexto do Recalbox, os cheats permitem reproduzir o comportamento de alguns dispositivos de trapaça dos sistemas originais, permitindo ativar ou desativar códigos pré-inscritos modificando o comportamento do jogo.
* **Checksum** _(ou Soma de verificação)_ : Uma seqüência calculada a partir do conteúdo de um arquivo, se dois arquivos do mesmo tamanho tiverem o mesmo checksum, eles provavelmente são idênticos. Usado principalmente para Netplay (jogo online).
* **Core** _(ou Núcleo)_: Um programa carregado por um emulador (geralmente RetroArch).
* **CRC** : Um tipo de soma de controle utilizada para Netplay (jogo online).
* **Cue** _(ou .cue)_ : Arquivos que descrevem como os dados brutos são distribuídos em um disco, utilizado principalmente para consoles baseados em disco, com arquivos .bin.

## D

* **Dat** _(ou .dat)_: Arquivos usados para verificar os lotes de roms para ter certeza de que pertencem a um determinado conjunto, usado principalmente para Arcade e Netplay. 
* **Depuração:** Conserto de um problema no computador devido a um *bug*, ou seja, um mau funcionamento de um programa de computador, que impede o bom funcionamento de um aplicativo, software, etc. Exemplo: Ele depurou seu Recalbox, reiniciando-o.
* ***Demo Mode*** _(Ou Modo de demonstração)_: Um recurso do Recalbox, permitindo que o EmulationStation inicie jogos aleatórios após um período definido de inatividade.
* **DragonBlaze**: O nome do Recalbox versão 6.0.

## E

* **Endianness:** Na [Ciência da computação](https://techlib.wiki/definition/computer_science.html), um termo que descreve como os [dados](https://techlib.wiki/definition/data.html) são armazenados. Especificamente, define qual extremidade de um [Tipo de Dados](https://techlib.wiki/definition/datatype.html) multibyte contém os valores mais significativos. Os dois tipos de endianness são big-endian e little-endian.
* **EmulationStation** _(ou ES)_ : O front-end (ou interface para gerenciar e acessar jogos) utilizado no Recalbox.
* **Emulador**: Um programa que imita o comportamento de um hardware definido.

## F

* **Front-end**: A interface, no caso do Recalbox é o Emulationstation.

## G

* **Gamelist** _(ou gamelist.xml)_ : Arquivo específico para cada sistema emulado, listando informações para cada jogo.
* **Gitlab**: 
* **GPI**:
* **GPIO** _(General Purpose Input/Output ou Entrada/Saída para Fins Gerais)_ : Contatos acessíveis em um Raspberry Pi, que podem ser usados para diferentes fins (principalmente para anexar botões e outros controles no caso do Recalbox).

## H

* **Hash**: Funcionalidade que calcula os checksums das ROMs para o Netplay.
* **Hotkey** _(ou Tecla de Atalho)_: Botão usado para combinações de teclas interpretadas pelo emulador como atalhos para características.

## I

* **Interface**:
* **ISO** _(ou .iso)_: Arquivos de imagem de um disco físico.
* ***Issue:*** No caso do Gitlab, um *issue* representa um relatório de *bug* ou pedido de recurso, é possível ver os *issues* ou criar um, seguindo [este link](https://gitlab.com/recalbox/recalbox/-/issues).

## K

* **Kodi**: Reprodutor de mídia incluído no Recalbox.

## L

* **Libretro**: API multiplataforma utilizada pelo Retroarch.
* **Licença**: Uma licença define o que pode ou não ser feito com um software, é possível ler a licença do Recalbox [aqui](https://gitlab.com/recalbox/recalbox/-/blob/master/LICENSE.md), também é necessário levar em conta que o Recalbox é fornecido com outros softwares, que também estão sujeitos a sua licença.
* ***Link-Cable*** _(ou Cabo-link)_: Um cabo usado para unir dois sistemas portáteis com o propósito de usar recursos multiplayer para jogos suportados, alguns emuladores podem imitar este comportamento.
* **Little Endian**: Armazena primeiro o valor menos significativo, seguido por valores cada vez mais significativos. Por exemplo, o número 123 em notação little-endian é [321]. A [cadeia de caracteres](https://pt.wikipedia.org/wiki/Cadeia_de_caracteres) "ABC" é representada como [CBA].

## M

* **MAME**: *Multiple Arcade Machine Emulator* (Emulador de Múltiplas Máquinas de Arcade) é um emulador de código aberto gratuito projetado para recriar o hardware de sistemas de jogos arcade em softwares de computadores pessoais modernos e outras plataformas.
* **MD5**: um tipo de soma de controle, usado para as BIOS.
* **MITM**: _(Man In The Middle)_: Um servidor relé, uma configuração usada para Netplay, usando um servidor remoto para sincronizar jogos em vez de uma conexão direta, em troca de uma perda de confiabilidade e velocidade.
* **Multitap**: Dispositivo que permite conectar mais controles do que há portas de controles em um console, permitindo, nos jogos suportados, ter um modo 4 jogadores em sistemas originalmente com apenas 2 portas de controles. Alguns emuladores podem imitar este comportamento.

## N

* **Netplay** _(ou Jogo online)_: Uma tecnologia que permite jogar em rede, no caso do Recalbox, ela permite jogar jogos retrô online.
* **No-intro** : Nome de um grupo que se refere a ROMs, são especializados em jogos de cartucho e têm por objetivo e visão manter apenas os *dumps* (os arquivos ROM extraídos dos cartuchos) mais próximos dos originais. Eles são **Romsets de referência** para sistemas que utilizam **cartuchos**, principalmente os **utilizados para Netplay**.
* **NOOBS:** (*New Out Of the Box Software*): O "Novo Software Proto Para Usar" tem um nome que pode soar pejorativo, mas este utilitário permite instalar um sistema operacional de forma confiável e rápida a partir de uma grande seleção. NOOBS é realmente apenas um simples gerenciador de instalação do sistema operacional para o Raspberry Pi, ele foi projetado para simplificar ao máximo esta tarefa, permitindo a instalação de um sistema operacional em apenas alguns cliques.

## O

* **OCR**: O **reconhecimento ótico de caracteres** (ROC), ou ocerização, refere-se a processos de computador para traduzir imagens de texto impresso ou datilografados, em arquivos de texto.
* **Open-Source**: A designação *open-source* ou **código fonte aberto**, aplica-se ao software cuja [licença](https://pt.wikipedia.org/wiki/Licen%C3%A7a_de_software) atende a critérios especificamente estabelecidos pela [Open Source Initiative](https://pt.wikipedia.org/wiki/Open_Source_Initiative), ou seja: oportunidade de redistribuição livre, acesso ao [código fonte](https://pt.wikipedia.org/wiki/C%C3%B3digo-fonte), e criação de trabalhos derivados. Este código fonte é disponibilizado ao público em geral e geralmente é o resultado da colaboração entre programadores.
* **OS**: Ver **Sistema Operacional**.
* **Overlays**: Imagem exibida sobre o jogo, utilizada principalmente por razões cosméticas.

## P

* **PBP** _(ou .pbp)_ : Arquivos de imagens correspondentes a UMDs físicos do PSP (Playstation Portátil), ou a jogos multi-disco do PSX (Playstation 1).
* **Pi**: Ver **Raspberry Pi**.
* **Plug and Play**: Também conhecido como **PnP**, significa "conectar e usar", ou "plugar e jogar". É uma tecnologia que permite que os sistemas operacionais reconheçam rápida e automaticamente os dispositivos compatíveis após a conexão, sem reiniciar o computador. *Plug and Play* permite a implementação com o mínimo de intervenção do usuário, sem a instalação de *software* dedicado e, portanto, minimizando erros de manipulação e configuração.

## R

* **Raspberry Pi**: 
* **Rebuild** _(ou Reconstruir)_: 
* **Recalbox**: O sistema operacional em si, baseado no *Buildroot System From Scratch*.
* **Recalbox.conf**: arquivo contendo a maioria das configurações para o Recalbox e seus emuladores, ver ["O arquivo recalbox.conf"](./.../basic-usage/getting-started/recalboxconf-file).
* **Redump**: Nome de um grupo que referência em ROMs, especializado em jogos baseados em disco.
* **Reloaded**: Nome da versão 7 do Recalbox.
* **Retroachievements** _(ou "Conquistas-retrô")_: Um serviço que permite um sistema de conquistas/troféus para jogos e emuladores suportados.
* **RetroArch**: O *Front-End* da Libretro usado por alguns emuladores no Recalbox, ver [RetroArch](./.../advanced-usage/retroarch/).
* **Rewind** _(ou Rebobinar)_: Um ajuste que permite rebobinar em alguns emuladores, mas com uma perda de desempenho.
* **ROMs**: Arquivos de imagens de jogos físicos.
* **Romset**: Um conjunto específico de ROMs, usado principalmente para Netplay e Arcade.

## S

* **Savestates**: Uma característica que salva o estado exato da máquina emulada, permitindo a qualquer momento uma gravação completa de um jogo (a criação de um *save*), mesmo que o jogo não suporte salvar originalmente. É importante observar que o *save* original do jogo e o *savestate* podem entrar em conflito.
* **Samba**: SMB (*Server Message Block*), compartilhamento de pastas e impressoras em uma rede local é uma característica dos sistemas operacionais modernos, que permite o acesso aos recursos de um computador (pastas de dados e impressoras) de outro computador na mesma rede local (rede doméstica ou corporativa).
* **Scraper**: Um programa que recupera informações de jogos na Internet e as salva em um formato que pode ser interpretado pelo EmulationStation.
* **Shaders**: Filtros que podem alterar a aparência visual de um jogo ao custo do desempenho, utilizados principalmente por razões cosméticas.
* **SHARE**: Partição do Recalbox, acessível a partir da rede.
* **Soma de verificação**: Ver _Checksum_.
* **SSH**:
* **Sobrecarga**: Arquivos para anular a configuração padrão para certos jogos ou pastas, ver [Sobrecarga de configuração](./../advanced-usage/configuration-override/).
* **Support Archive** _(ou Arquivo de suporte)_ : opção do Webmanager, permitindo gerar um link de depuração útil para os desenvolvedores em caso de problemas.
* **Sistema Operacional** : Também chamado **OS**, é um conjunto de programas que direciona o uso dos recursos de um computador através de softwares.

## T

* **Temas**: Arquivos que definem o aspecto geral do EmulationStation.

## U

* **UMD**:

## V

* **Verbose**: Para o Recalbox, este é um modo de inicialização que exibe informações de depuração para algumas arquiteturas, útil para entender porque o sistema não está inicializando corretamente.

## W

* **Webmanager**: Uma interface de rede habilitada pelo Recalbox para fácil configuração, gerenciamento de arquivos, verificação de BIOS e muito mais.

## X

* **Xin-Mo**: