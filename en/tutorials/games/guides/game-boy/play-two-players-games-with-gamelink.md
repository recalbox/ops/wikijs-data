---
title: Play two players games with GameLink
description: 
published: true
date: 2021-08-08T08:26:05.598Z
tags: two, gameboy, players, gamelink
editor: markdown
dateCreated: 2021-08-08T08:26:05.598Z
---

TGBDual allows you to play the Game Boy and Game Boy Color by simulating a GameLink cable for multiplayer. However, to use two different games, the handling is more complex.

This tutorial will assume that you already know how to run [a game on two Game Boys with TGBDual](./play-with-two-game-boy).

>Using two different games allows you to use the game save for each GameBoy.
{.is-success}

## Two games with one system

The simplest configuration is to play two different games on the same system (gb+gb or gbc+gbc), because this does not require any saving manipulation.

To do this:

* Launch a game of the desired system (gb or gbc) with TGBDual.
* Once the game is launched, you must press `Hotkey + B` to access the RetroArch menu.
* Press `Back` once and then go to `Subsystems`.
* In the menu, you will see the option `Load 2 Player Game Boy Link ★` and the line `Current Content: GameBoy #1`.

![](/tutorials/games/guides/game-boy/play-two-different-games-gamelink/2diffgames1core-1-en.png){.full-width}

* Select the available option and you will be in the directory where you launched the game. Select the game you had launched and validate.

![](/tutorials/games/guides/game-boy/play-two-different-games-gamelink/2diffgames1core-2-en.png){.full-width}

>If the game is zipped, you will have to ask to browse the archive, then open the rom contained inside.
{.is-warning}

* Once validated, you have to go back to `Subsystems`. You will see the option `Load 2 Player Game Boy Link ★` and the line `Current Content: GameBoy #2`.

![](/tutorials/games/guides/game-boy/play-two-different-games-gamelink/2diffgames1core-3-en.png){.full-width}

* Select the available option and you will be in the directory where you launched the first game. Select the second game and validate.

![](/tutorials/games/guides/game-boy/play-two-different-games-gamelink/2diffgames1core-4-en.png){.full-width}

>Here again, if the game is zipped, you will have to browse the archive, then open the rom inside.
{.is-warning}

* Once validated, you have to go back to `Subsystems`. You will see the option `Start 2 Player Game Boy Link ★` with the games to launch underneath.

![](/tutorials/games/guides/game-boy/play-two-different-games-gamelink/2diffgames1core-5-en.png){.full-width}

* Select the option and the emulator will completely restart with the 2 selected games.

![](/tutorials/games/guides/game-boy/play-two-different-games-gamelink/2diffgames1core-6.png){.full-width}

## Two games, two different systems

In case you want to run two games, but one from Game Boy Color, and the other from Game Boy, there is an extra step. You have to save both games in the same folder before starting.

So, if you want to run the Game Boy Color system from EmulationStation, you will need to move the savegame for the Game Boy game in question (in `share/saves/gb`) into the `share/saves/gbc` folder.

So, for the game `Pokemon - Version Bleue (France) (SGB Enhanced)` in the `gb` folder, you need to move the files `Pokemon - Version Bleue (France) (SGB Enhanced).rtc` and `Pokemon - Version Bleue (France) (SGB Enhanced).srm` into the `share/saves/gbc` folder.