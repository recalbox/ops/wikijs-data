---
title: DOSBox
description: Pour émuler des jeux DOS
published: true
date: 2025-01-05T00:05:03.326Z
tags: dos, msdos, dosbox
editor: markdown
dateCreated: 2021-07-29T16:25:56.587Z
---

DOSBox is an emulator capable of emulating an **IBM compatible PC** running the **DOS** operating system.  
Several types of graphic or sound cards can be emulated.

This allows you to play old PC games on Recalbox.

## ![](/emulators/license.svg) License

This core is under [**GPLv2**](https://gitlab.com/lmerckx/recalbox-dosbox/-/blob/master/COPYING) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Features

| Feature | Supported |
| :---: | :---: |
| Screenshots | ✔ |
| Core Options | ✔ |
| Controls | ✔ |
| Remapping | ✔ |

## ![](/emulators/bios.svg) BIOS

>**No bios is required.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Supported extensions

Roms must have the extension:

* .exe
* .com
* .bat
* .conf

### Location

Put the roms like this: 

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 dos
┃ ┃ ┃ ┃ ┣ 📁 **game**
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **game.exe**

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://gitlab.com/lmerckx/recalbox-dosbox/](https://gitlab.com/lmerckx/recalbox-dosbox/)
* **Official wiki**: [https://www.dosbox.com/wiki/](https://www.dosbox.com/wiki/)
* **Games list**: [https://www.dosbox.com/comp_list.php?letter=a](https://www.dosbox.com/comp_list.php?letter=a)
* **Playable games**: [https://www.dosbox.com/comp_list.php?letter=playable](https://www.dosbox.com/comp_list.php?letter=playable)