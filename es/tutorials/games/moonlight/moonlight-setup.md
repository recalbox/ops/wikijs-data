---
title: Primeros pasos con Moonlight
description: 
published: true
date: 2023-09-21T13:49:16.453Z
tags: moonlight, instalación
editor: markdown
dateCreated: 2023-09-19T09:40:56.624Z
---

## Introducción

Moonlight es una versión de código abierto de la tecnología Gamestream de NVidia.

Si tu PC cumple los requisitos, puedes transmitir la mayoría de tus juegos a tu Recalbox. Por otro lado, Recalbox lee la configuración del pad desde EmulationStation y la convierte a Moonlight.

### Funciones de Moonlight en Recalbox

* Transmite juegos a través de tu red local o Internet
* Soporte para hasta 4 jugadores
* Hasta 1080p/60fps
* Descodificación H264 acelerada por hardware en cualquier versión de Raspberry Pi
* Soporta teclado y ratón
* Hasta GFE 3.12

### Requisitos

Requisitos de Recalbox y PC para disfrutar de Moonlight:

* Un mando configurado en EmulationStation
* Una cuenta de Steam (opcional) o juegos independientes compatibles (ver [https://shield.nvidia.com/game-stream](https://shield.nvidia.com/game-stream))
* Una GPU compatible con Nvidia (ver [http://www.geforce.com/geforce-experience/system-requirements](http://www.geforce.com/geforce-experience/system-requirements))
* Se recomienda encarecidamente una conexión Ethernet, ya que el WiFi no es lo suficientemente fiable.

## Usando Moonlight

Recalbox ofrece un scrap más bonito, la posibilidad de hacer streaming desde múltiples PCs en la red y una pequeña opción para encontrar hosts GFE disponibles.

Necesitas usar 3 comandos para configurar e iniciar Moonlight.

>¡Indicad las rutas completas como se muestra!
{.is-warning}

### Descubrir los hosts

En primer lugar, hay que encontrar los hosts disponibles con este comando:

```shell
/recalbox/scripts/moonlight/Moonlight.sh find
```

### Asociar Recalbox a uno de los hosts encontrados

Hay que asociar Recalbox con el host que os interesa usando este comando :

```shell
/recalbox/scripts/moonlight/Moonlight.sh pair <host>
```

>Reemplazad `<host>` por el nombre indicado en la salida del primer comando.
{.is-info}

>Aparecerá un mensaje de Windows. Deberéis introducir el código PIN que aparece al final del comando de asociación.
{.is-info}

### Inicializar Moonlight

Basta con lanzar el siguiente comando para terminar la configuración:

```shell
/recalbox/scripts/moonlight/Moonlight.sh init <host>
```

>Reemplazad `<host>` por el nombre indicado en la salida del primer comando.
{.is-info}

## Salir del juego (terminar)

Para salir de una partida, hay que utilizar la combinación de teclas CTRL + ALT + MAYÚS + Q (o A).

## Demostración

Aquí os dejamos una demo para descubrir los hosts GFE, conectarlos y lanzarlos:


```shell
# /recalbox/scripts/moonlight/Moonlight.sh find
Listing available GFE servers :
GFE Host WIN10(192.168.2.137) NVIDIA GeForce GTX 980 running GFE 3.23.0.74
You can now run the following commands:
  /recalbox/scripts/moonlight/Moonlight.sh pair WIN10
  /recalbox/scripts/moonlight/Moonlight.sh init WIN10

# ./Moonlight.sh pair WIN10
Hostname mode
WIN10(192.168.2.137) /recalbox/share/system/configs/moonlight/moonlight-WIN10.conf | /recalbox/share/system/configs/moonlight/keydir-WIN10
When PIN is asked on your PC, enter the number ----> 3863 <----
GFE Host WIN10(192.168.2.137) NVIDIA GeForce GTX 980 running GFE 3.23.0.74

# /recalbox/scripts/moonlight/Moonlight.sh init WIN10
Hostname mode
Adding and scraping Brutal Legend ...
Adding and scraping Sacred Citadel ...
Adding and scraping Just Cause 3 ...
Adding and scraping Street Fighter V ...
Adding and scraping Just Cause 2 Multiplayer ...
Adding and scraping Tales of Zestiria ...
Adding and scraping Grand Theft Auto V ...
Adding and scraping Hell Yeah! Wrath of the Dead Rabbit ...
Adding and scraping Ultra Street Fighter IV ...
Adding and scraping Diablo III ...
Adding and scraping Pro Evolution Soccer 2017 ...
Adding and scraping Bionic Commando Rearmed ...
Adding and scraping Just Cause 2 ...
Adding and scraping Pro Evolution Soccer 2016 ...
Adding and scraping Broforce ...
Adding and scraping DmC: Devil May Cry ...
Adding and scraping Naruto Shippuden: Ultimate Ninja Storm 4 ...
Adding and scraping Steam ...
```

## ANEXO

>Sólo para usuarios avanzados: podéis transmitir desde un ordenador remoto a través de Internet.
{.is-warning}

* Necesitaréis editar `/recalbox/scripts/moonlight/Moonlight.sh` para:
  * Especificar la dirección IP del host remoto,
  * Establecer la misma dirección IP en moonlight.conf,
  * Y configurar el reenvío de puertos de acuerdo con [esta página](https://github.com/moonlight-stream/moonlight-android/wiki/Setup-Guide#streaming-over-the-internet)