---
title: Unibios
description: 
published: true
date: 2023-09-20T20:30:26.583Z
tags: neo-geo, unibios
editor: markdown
dateCreated: 2023-09-20T20:30:26.583Z
---

## Que permite Unibios

L'UNIBIOS permite:

* Modificar la región (Europe/USA/Japan) y el modo (AES/MVS).
* Acceder al 'soft dip' para cambiar la dificultad/vida/tiempo/etc. del juego.
* Utilizar la base de datos de códigos de trucos.
* Acceder al jukebox... ¡y mucho más!

## FinalBurn Neo

El UNIBIOS se encuentra dentro del romset FinalBurn Neo.

### Instalación

* Lanzad un juego y entrad en el menú de RetroArch con `Hotkey + B`, id a `Opciones`.

![](/tutorials/games/guides/neo-geo/unibios/unibios1.png){.full-width}

* En las opciones, bajad hasta el parámetro `Neo-Geo mode`.

![](/tutorials/games/guides/neo-geo/unibios/unibios2.png){.full-width}

* Sobre esta opción, elegid `Use UNIBIOS bios`.

![](/tutorials/games/guides/neo-geo/unibios/unibios3.png){.full-width}

* Cerrad el menú RetroArch (volved al juego). El juego se reiniciará con el UNIBIOS activado.

![](/tutorials/games/guides/neo-geo/unibios/unibios4.png){.full-width}

### Utilización

* Lanzad cualquier juego Neo-Geo, veréis una nueva pantalla de inicio UNIVERSE BIOS.
* Mientras se muestra esta pantalla, pulsad y mantened pulsados los botones `Y + B + A` (si utilizáis un mando original, los botones son `C + A + B`).

![Manette Neo-Geo](/tutorials/games/guides/neo-geo/unibios/neogeo-controller.jpg){.full-width}

![](/tutorials/games/guides/neo-geo/unibios/unibios5.png){.full-width}

* Para salir, utilizad el botón `X` (botón `C` del mando original).

* Para acceder a este menú durante el juego, hay que pulsar y mantener pulsados los botones `X + A + B + START`.

## Dip Switch

Para acceder al menú « dip switch » durante la presentación de la pantalla de arranque de UNIBIOS, mantened pulsados `Y + X + A` (en el mando original, los botones serían `C + D + B`).

### Ajustes del Dip Switch

Para que se vea la sangre en el juego Metal Slug, una vez dentro del menú de « dip switch » :

* Id a `SETTING UP THE SOFT DIP`.

![](/tutorials/games/guides/neo-geo/unibios/unibios6.png){.full-width}

* Id a `SLOT1 METAL SLUG`.

![](/tutorials/games/guides/neo-geo/unibios/unibios7.png){.full-width}

* Situaros sobre `BLOOD` y modicar esta opción de `OFF` a `ON`.

![](/tutorials/games/guides/neo-geo/unibios/unibios8.png){.full-width}

Para navigar por los menús, validad con `A` y volved hacia atrás con `Y` (`C`del mando original). Modificad los valores con `A` y `B`.

## Contenido de la BIOS

La Neo-Geo necesita un fichero de BIOS llamado `neogeo.zip`.

* Se puede colocar junto con vuestras roms dentro de:

```text
/recalbox/share/roms/neogeo
```

o

```text
/recalbox/share/bios
```

* Aquí tenéis el contenido de un fichero BIOS neogeo.zip certificado como funcional:

```text
000-lo.lo
japan-j3.bin
neo-epo.bin
neo-po.bin
neodebug.bin
neopen.sp1
sfix.sfix
sm1.sm1
sp-1v1_3db8c.bin
sp-45.sp1
sp-e.sp1
sp-j2.sp1
sp-j3.sp1
sp-s.sp1
sp-s2.sp1
sp-s3.sp1
sp-u2.sp1
sp1-j3.bin
sp1-u2
sp1-u3.bin
sp1-u4.bin
sp1.jipan.1024
uni-bios_1_0.rom
uni-bios_1_1.rom
uni-bios_1_2.rom
uni-bios_1_2o.rom
uni-bios_1_3.rom
uni-bios_2_0.rom
uni-bios_2_1.rom
uni-bios_2_2.rom
uni-bios_2_3.rom
uni-bios_2_3o.rom
uni-bios_3_0.rom
uni-bios_3_1.rom
uni-bios_3_2.rom
uni-bios_3_3.rom
uni-bios_4_0.rom
vs-bios.rom
```