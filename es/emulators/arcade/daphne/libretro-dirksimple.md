---
title: Libretro DirkSimple
description: 
published: true
date: 2024-06-20T19:46:20.305Z
tags: 
editor: markdown
dateCreated: 2024-06-20T19:16:40.722Z
---



## ![](/emulators/license.svg) Licencia

Este core está bajo licencia [**zlib**](https://github.com/icculus/DirkSimple/blob/main/LICENSE.txt).

## ![](/emulators/compatibility.png) Compatibilidad

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Funcionalidades

| Funcionalidad  | Soportada |
| :---: | :---: |

## ![](/emulators/bios.svg) BIOS

>**No se requiere ningún BIOS.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Extensiones Soportadas

Las roms deben tener las siguientes extensiones:

* .ogv

Las roms deben tener las siguientes extensiones:

- lair.ogv (Dragon's Lair)
- cliff.ogv (Cliff Hanger)

### Ubicación

Coloca las roms de la siguiente manera:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 🗒 **juego.ogv**

## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

>Para poder conservar tus configuraciones personalizadas durante una actualización, te recomendamos utilizar nuestra funcionalidad de [Sobrecargas de configuración](/fr/advanced-usage/configuration-override).
{.is-info}

### Acceder a las opciones

Puedes configurar diversas opciones de dos maneras diferentes.

* A través del menú RetroArch:

┣ 📁 Menu RetroArch
┃ ┣ 📁 Opciones del core
┃ ┃ ┣ 🧩 Name_option

* A través del archivo `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Opciones del core

Las opciones varían en gran medida según el juego. Dependiendo del número futuro de juegos soportados, no se podrán listar todas las opciones.

## ![](/emulators/external-links.png) Enlaces externos

* **Código fuente utilizado** : [https://github.com/icculus/DirkSimple/](https://github.com/icculus/DirkSimple/)
* **Documentación Libretro** :