---
title: Libretro Gong
description: 
published: true
date: 2024-06-23T12:08:06.076Z
tags: libretro, gong, 8.0+
editor: markdown
dateCreated: 2021-11-02T10:07:15.355Z
---

Libretro Gong est une reconstitution ouverte de Pong écrite par Dan Zeitan en 2018.

## ![](/emulators/license.svg) Licence

Ce core est sous licence [**GPLv3**](https://github.com/libretro/gong/blob/master/COPYING).

## ![](/emulators/compatibility.png) Compatibilité

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Fonctionnalités

| Fonctionnalité | Supporté |
| :---: | :---: |
| Options du core | ✔ |

## ![](/emulators/bios.svg) BIOS

>**Aucun bios n'est requis.**
{.is-success}

## ![](/emulators/roms.png) Roms

Le noyau Gong ne comporte pas d'utilisation d'extension. Il suffit de charger et de démarrer le core.

### Emplacement

Placez les roms comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 ports
┃ ┃ ┃ ┃ ┣ 📁 Gong
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **gong.c**

## ![](/emulators/advanced-configuration.png) Configuration avancée de l'émulateur

>Pour pouvoir conserver vos configurations personnalisées lors d'une mise à jour, nous vous conseillons d'utiliser notre fonctionnalité [Surcharges de configuration](./../../../advanced-usage/configuration-override).
{.is-info}

### Accéder aux options

Vous pouvez configurer diverses options de deux façons différentes.

* Via le Menu RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via le fichier `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Options du core

| Options | Valeurs des options | Variable (pour surcharge) | Valeurs des variables |
| --- | --- | --- | --- |
| Player 2 | `CPU` ✅ / `Human` | `gong_player2` | `CPU` / `Human` |
| Video Refresh Rate (Restart) | `60` ✅ / `70` / `72` / `75` / `100` / `119` / `120` / `140` / `144` | `gong_refresh` | `60` / `70` / `72` / `75` / `100` / `119` / `120` / `140` / `144` |

## ![](/emulators/external-links.png) Liens externes

* **Code source utilisé** : [https://github.com/libretro/gong/](https://github.com/libretro/gong/)
* **Code source officiel** : [https://danzaidan.itch.io/pong-in-c](https://danzaidan.itch.io/pong-in-c)