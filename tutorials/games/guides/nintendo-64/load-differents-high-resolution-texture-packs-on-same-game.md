---
title: Plusieurs packs de texture haute-résolution sur le même jeu
description: 
published: true
date: 2021-09-08T12:49:52.101Z
tags: n64, nintendo, 64, texture, pack
editor: markdown
dateCreated: 2021-06-02T23:22:11.556Z
---

## Informations

Dans le guide « [Textures haute résolution](./high-resolution-textures) », vous avez appris comment utiliser les packs de textures haute résolution.

## Général

Pour certains jeux, différents packs de textures ont été crée. Le problème étant qu'ils ont en général tous le même nom. Si vous voulez utiliser plusieurs versions, suivez ce guide.

## Déroulement

Pour chaque version de la texture haute-résolution, nous modifions le nom de la rom interne. Lorsque cela sera fait, nous modifierons les noms des textures haute-résolution.

## Ce dont nous avons besoin

Dans ce guide, nous utiliserons **N64 Rom Renamer** par Einstein II (Google est votre ami), pour modifier le nom interne de la rom.

Vous aurez aussi besoin d'un batch de renommage (exemple sous Windows : **AntRenamer**). Pour ce guide, nous utiliserons **Better Rename** (macOS).

## C'est parti

### Modifiez le nom interne de la rom

* Ouvrez **N64 Rom Renamer** avec les droits administrateur (cela ne fonctionnait pas sans pour ma part).

![](/tutorials/games/guides/nintendo-64/multiple-hires-textures/multiplehirespacks1.png){.full-width}

* Appuyez sur `F2` ou allez dans `Tools` > `Headereditor`.
* Validez l'avertissement.

![](/tutorials/games/guides/nintendo-64/multiple-hires-textures/multiplehirespacks2.png)

* En haut à gauche, cliquez sur l’icône de dossier et sélectionnez votre rom (pour notre exemple, nous utiliserons Mario Kart 64).

![](/tutorials/games/guides/nintendo-64/multiple-hires-textures/multiplehirespacks3.png){.full-width}

* Ici, vous pouvez modifier le nom interne de la rom.

### Attention

Le nom **ne peut pas être trop long.** La longueur totale des lettres et espaces doit être respectée (20 caractères). Dans notre cas, nous voyons que **MARIOKART64** possède neuf espaces en plus de son nom, ce qui nous donne la longueur totale possible pour le nom interne.

![](/tutorials/games/guides/nintendo-64/multiple-hires-textures/multiplehirespacks4.png){.full-width}

* Dans notre exemple, nous supprimerons quatre espaces qui seront remplacés par quatre lettres (SNES).

![](/tutorials/games/guides/nintendo-64/multiple-hires-textures/multiplehirespacks5.png){.full-width}

* Cliquez sur `Save changes` pour sauvegarder les modifications puis cliquez sur `Back` pour revenir en arrière.
* Le nom interne de la rom a été modifié avec succès.

Renommez ensuite le fichier rom ; par exemple : Mario Kart 64 SNES Edition.ext Le nom de fichier de la rom n'est pas important pour les textures.

### Modifiez le pack de textures haute résolution et les fichiers :

Notre exemple de structure de fichier de notre pack haute résolution ressemble à cela :

![](/tutorials/games/guides/nintendo-64/multiple-hires-textures/multiplehirespacks6.png)

Le dossier et les fichiers nommés **MARIOKART64** sont bien sûr maintenant incorrects, après que nous ayons modifié le nom interne de la rom. Nous devons corriger cela !

* Ouvrez **Better Rename**
* Category : Text
* Action : Replace text
* Replace : **MARIOKART64**
* With : **MARIOKART64SNES**

![](/tutorials/games/guides/nintendo-64/multiple-hires-textures/multiplehirespacks7.png){.full-width}

Après le renommage, vous devez vérifier que tous les noms correspondent. Si le nom de dossier et les noms de fichiers correspondent, vous pouvez copier votre pack de textures.

>**Par précaution :**
>
>Ancien nom de dossier : MARIOKART64  
>Ancien nom de fichier dans le dossier : MARIOKART64#......
>
>Nouveau nom de dossier : MARIOKART64SNES  
>Nouveau nom de fichier dans le dossier : MARIOKART64SNES#...
{.is-warning}

Suivez ces étapes pour chaque pack de textures haute-résolution voulu.