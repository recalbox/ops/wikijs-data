---
title: Libretro REminiscence
description: REminiscence Core for: Flashback - The Quest for Identity
published: true
date: 2021-08-08T23:51:45.588Z
tags: libretro, flashback, reminiscence
editor: markdown
dateCreated: 2021-06-29T21:21:36.042Z
---

Stuart Carnie has ported Gregory Montoir's Flashback emulator REminiscence to libretro! REminiscence is a recreation of the game engine of the 1992/1993 action-adventure game Flashback. It is the spiritual successor of Another World / Out Of This World and features rotoscopic graphics, polygonal cinematics and a Prince of Persia style game system.

This port is still a work in progress, but it is up and running. Currently it jumps directly into the game, skipping the main menu.

We have also added modplug support to the core for improved music playback.

The REminiscence core was created by

* Gregory Montoir
* Stuart Carnie

## ![](/emulators/license.svg) License 

This core is under **GPLv3** license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Features

| Features | Supported |
| :---: | :---: |
| Screenshots | ✔ |
| Saves | ✔ |
| Controls | ✔ |
| Remapping | ✔ |

## ![](/emulators/bios.svg) BIOS

>**No bios is required.**
{.is-success}

## ![](/emulators/roms.png) Roms

### How to install the required files

Versions of Flashback game known:

* Flashback DOS Demo (disk)
* Flashback DOS Full version (disk)
* Flashback DOS Full version (CD)

### Location

Put your roms like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 ports
┃ ┃ ┃ ┣ 📁 Flashback
┃ ┃ ┃ ┃ ┣ 📁 data
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **instru_e.pal**

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Accessing to options

You can configure various options in two different ways.

* Via RetroArch menu:

┣ 📁 RetroArch Menu
┃ ┣ 📁 Core options
┃ ┃ ┣ 🧩 Name_option

* Via the file `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core options

## 🗂 Additional content

### Ajouter le son pendant les cinématiques

The executable also supports music during the cinematics of the **Amiga** version. You have to get the files in the `music` directory of the original diskettes OR get the files on the web and add `mod.flashback-` in front of each music file of the Amiga version.

>Do not put file extensions.
{.is-danger}

List of files to put in `roms\ports\Flashback\data\`:

* mod.flashback-ascenseur
* mod.flashback-ceinturea
* mod.flashback-chute
* mod.flashback-desintegr
* mod.flashback-donneobjt
* mod.flashback-fin
* mod.flashback-fin2
* mod.flashback-game_over
* mod.flashback-holocube
* mod.flashback-introb
* mod.flashback-jungle
* mod.flashback-logo
* mod.flashback-memoire
* mod.flashback-missionca
* mod.flashback-options1
* mod.flashback-options2
* mod.flashback-reunion
* mod.flashback-taxi
* mod.flashback-teleport2
* mod.flashback-teleporta
* mod.flashback-voyage

### Adding in-game dialogs

For in-game dialogs, you must copy the `VOICE.VCE` file from the `data` directory of the SegaCD version into the `roms\ports\lashback\data` directory.

>Don't look for the French version of the voices in SegaCD. There is only the (USA) version of the SegaCD game.
{.is-warning}

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/libretro/REminiscence/](https://github.com/libretro/REminiscence)
* **Official website**: [http://cyxdown.free.fr/reminiscence/](http://cyxdown.free.fr/reminiscence/)