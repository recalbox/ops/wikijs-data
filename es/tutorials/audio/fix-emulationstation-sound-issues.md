---
title: Corregir los problemas de sonido de interfaz
description: 
published: true
date: 2024-07-26T07:14:15.095Z
tags: emulationstation, sonido, problemas
editor: markdown
dateCreated: 2023-09-17T20:37:35.264Z
---

## Introducción

En algunos casos, dependiendo del hardware utilizado, el sonido en interfaz puede parecer anormal, como entrecortado. Este breve tutorial puede ayudaros si os encontráis en esta situación.

## Utilización

Para ello, hay que conectarse vía [SSH](./../../tutorials/system/access/root-access-terminal-cli) y ejecutar las dos líneas que siguen:

```shell
mount -o remount,rw /
echo 'default-sample-rate = 48000' >> /etc/pulse/daemon.conf
```

Reiniciad y el sonido debería ser correcto.

>Deberéis realizar esta manipulación depués de cada actualización del sistema, porque no es persistente.
{.is-info}