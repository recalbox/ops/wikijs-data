---
title: Scrapeur Interne
description: 
published: true
date: 2025-02-08T12:07:02.687Z
tags: scrapeur, interne
editor: markdown
dateCreated: 2021-05-21T07:53:19.458Z
---

## Introduction

Cette fois, nous n'avons pas tourné autour du pot : nous avons entièrement refait le scrapeur interne, en partant de zéro. Il ne reste rien de l'ancien, pas même une miette.

Il en résulte un scrapeur tout neuf, bien plus propre, rapide, avec beaucoup d'options, et _**qui exploite pleinement votre compte ScreenScraper**_ (nombre de threads, quota, ...).

![](/basic-usage/features/internalscraper1.png)

## Utilisation

* Dans la liste des systèmes, appuyez sur le bouton `START` et allez dans `SCRAPEUR`.
* Ici, vous aurez plusieurs options de scrap :
  * `SCRAPER DEPUIS` : permet de choisir sa source de scrap.
  * `OPTIONS DU SCRAPEUR` : permet de choisir toutes les options de scrap :
    * `TYPE D'IMAGE` : permet de choisir le visuel à récupérer pour les jeux.
    * `TYPE DE VIDÉO` : permet de choisir si on récupère les vidéos et quel type de vidéo (les vidéos optimisée / normalisée vous prendront moins de place).
    * `TYPE DE VIGNETTE` : permet de choisir si on récupère les images.
    * `RÉGION DU JEU` : permet de choisir si on choisit la région à utiliser depuis choix région préférée ou celle détectée.
    * `RÉGION PRÉFÉRÉE` : permet de choisir votre région préférée.
    * `LANGUE PRÉFÉRÉE` : permet de choisir votre langue préférée pour les textes des scrapes entre-autres.
    * `TÉLÉCHARGER LES MANUELS` : permet de télécharger les manuels d'origine de chaque jeu.
    * `TÉLÉCHARGER LES CARTES` : permet de télécharger les cartes d'origine de chaque jeu.
    * `TÉLÉCHARGER DES CONFIGURATIONS PAD VERS CLAVIER` : permet de télécharger les configurations des manettes vers le clavier, très utile pour l'utilisation des anciens ordinateurs.
    * `NOM D'UTILISATEUR` : permet d'indiquer son identifiant ScreenScraper.
    * `MOT DE PASSE` : permet d'indiquer son mot de passe ScreenScraper.
  * `OBTENIR LES NOMS À PARTIR DE` : permet de choisir si vous souhaitez avoir le scrap depuis le nom des roms ou isos.
  * `FILTRE` : permet de choisir si vous scrapez que les médias manquants ou tous les jeux d'un système.
  * `SYSTÈMES` : permet de choisir quels système scraper.
* Une fois que vous aurez fait tous vos choix, cliquez sur le bouton `SCRAPER MAINTENANT` pour scraper.

En fonction des options choisies, de la quantité de jeu, du nombre de threads associés à votre compte ScreenScraper ainsi que du nombre de scrap autorisé par jour, le scrap peut prendre quelque minutes comme quelques heures.

## Quotas

En parlant de quotas en introduction, il est important de comprendre comment cela fonctionne. Quand vous allez scraper un jeu, vous allez utiliser une certaine quantité de votre quota, ce n'est pas 1 jeu = 1 requête affectant le quota.

Un scrap d'un jeu = 1 requête du quota pour toutes les informations textuelles **ET** 1 requête de quota **PAR IMAGE/VIDÉO** existante !

Si vous avez un jeu qui a 1 image, 1 vignette, 1p2k, 1 vidéo, 1 carte et 1 manuel et que tout est disponible, vous allez utiliser 7 requêtes de votre quota. D'un quota de 15000 par jour, pour ce jeu, vous passerez à un quota de 14993.