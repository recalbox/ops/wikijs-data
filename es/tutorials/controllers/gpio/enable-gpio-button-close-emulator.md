---
title: Activar el botón GPIO para cerrar el emulador con un solo botón
description: 
published: true
date: 2023-09-18T19:26:29.102Z
tags: gpio, emulador, botón, cerrar
editor: markdown
dateCreated: 2023-09-18T19:26:29.102Z
---

Este script Python para Raspberry Pi permite utilizar un botón para terminar el emulador pulsando un único botón GPIO. Es compatible con cualquier otra función o script previamente asignada a los botones.

El botón seleccionado por defecto es Player ONE Start; se puede cambiar durante la instalación a cualquier otro ya que no elimina ninguna función anterior.  
El emulador se cierra después de mantener pulsado el botón durante al menos 0,5 seg. También se puede cambiar el retardo de activación del botón.

## Requisitos

* Una Recalbox en el Raspberry Pi que utilice comandos GPIO (no es necesario tener un botón libre).
* El Raspberry Pi debe tener acceso a Internet: podéis activar y configurar el [wifi](./../../../tutorials/network/wifi/enable-wifi) y el nombre de host, y obtener la [dirección IP](./../../../tutorials/network/ip/discover-recalbox-ip) de tu Recalbox.
* Un ordenador desde el que conectarse vía [SSH](./../../../tutorials/system/access/root-access-terminal-cli) en Raspberry Pi.

## Instalación

* Conectaros vía [SSH](./../../../tutorials/system/access/root-access-terminal-cli).
* Copiad y pegad este comando:

```shell
wget --quiet --show-progress -O /recalbox/share/system/exit-emu-1b https://gist.githubusercontent.com/DjLeChuck/446cd415575f03c927627e378979027d/raw/9ebe3a5e178ff047b536220afd513981095fb41d/exit-emu-1b-installer && chmod 755 /recalbox/share/system/exit-emu-1b && bash /recalbox/share/system/exit-emu-1b install
```

El instalador mostrará una lista de posibles mapeos de puertos GPIO correspondientes al mapeo GPIO para Recalbox, correspondiente a la siguiente imagen :

Tras seleccionar el puerto para el botón al que queréis asignar el script, el instalador os preguntará por retardo para quitar el emulador, deberéris teclear el tiempo deseado en segundos (es importante responder a ambas preguntas, de lo contrario el instalador fallará).

Transcurridos unos segundos, si el resultado es un mensaje en pantalla que dice "Botón de inicio", entonces todo ha ido correctamente y ya tenemos el script del botón instalado y funcionando.

## Ficheros instalados

* [/recalbox/scripts/rpi-exit-emu-1b.py](https://gist.github.com/DjLeChuck/445ce3d37f41f12d5bf8cb9482db4027)
* [/etc/init.d/S98exit-emu-1b](https://gist.github.com/DjLeChuck/5f798b0d4af4071a92111bf61703aeb1)
* [/recalbox/share/system/exit-emu-1b](https://gist.github.com/DjLeChuck/446cd415575f03c927627e378979027d)

## Desinstalación

* Conectaros vía [SSH](./../../../tutorials/system/access/root-access-terminal-cli) y escribid:

```sshell
/recalbox/share/system/exit-emu-1b uninstall
```

>En la configuración por defecto de RetroArch, el script termina los emuladores por fuerza bruta utilizando el comando _**killall**_ que, dependiendo del kernel utilizado, puede provocar la pérdida de ciertos cambios de configuración del emulador realizados durante el juego. Por ejemplo, esto ocurre con Mame2003.
>
>Para evitarlo, hemos implementado un comando de red para terminar el emulador de RetroArch. RetroArch se terminará correctamente y no perderéis los cambios de configuración.
{.is-warning}

>Este cambio sólo afecta a RetroArch, aunque cubre la mayoría de emuladores del sistema, emuladores como **FBA2x**, **Scummvm** o **mupen64plus** se siguen terminando por fuerza bruta, hasta que encuentre otra forma de hacer.
{.is-info}

## Trucos y astucias

* Si en algún momento necesitáis asignar la función de terminar a otro botón, simplemente volved a lanzar el instalador con el comando `/recalbox/share/system/exit-emu-1b install` e introducid el nuevo número de puerto.
* Si cambiáis la configuración del emulador y salís con el botón asignado, se pueden perder los cambios. Verificad que se encuentran en el fichero `/recalbox/share/system/configs/retroarch/retroarchcustom.cfg`, o en el fichero de configuración personalizada de RetroArch donde se encuentra el `Network_cmd_enable` a `true`.
* Si, por cualquier motivo, el script deja de funcionar y el botón pierde su función de terminar el emulador, podéis hacer que vuelva a funcionar utilizando el comando `/etc/init.d/S98exit-emu-1b restart`. Tendréis que introducirlo dentro de una [sesión de SSH](./../../../tutorials/system/access/root-access-terminal-cli).