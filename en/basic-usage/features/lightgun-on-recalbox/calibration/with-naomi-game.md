---
title: With Naomi game
description: 
published: true
date: 2021-09-04T17:55:00.269Z
tags: lightgun, calibration, naomi, 7.2+
editor: markdown
dateCreated: 2021-08-05T18:57:06.077Z
---

## Example of calibration in a Naomi game

1) Activate Naomi service mode in RetroArch (`Hotkey` + `B`).

2) Set up a player 1 controller.

3) Return to the game (`Hotkey` + `B`) and launch the TEST MENU with the `L3` button.

4) Select the menu with `R3` and validate with `L3`.

5) Once you are in the calibration or just before, go back to RetroArch (`Hotkey` + `B`) and put a "lightgun" in player 1.

6) When the calibration is finished, you may have to do step 2 again to exit the TEST MENU.

7) When we return to the game, we go back to RetroArch (`Hotkey` + `B`) to put the lightgun in player 1.

8) Now we can test the result of the calibration

## Example of calibration in a Naomi game (_the case of The House of The Dead 2_)

For example, for this type of game (The house of dead 2 on Naomi), we enter the menu "**NAOMI TEST MODE**" but it's not easy...

**Prerequisite:** you need a controller

1) You have to enter RetroArch (`Hotkey` + `B`) - Quick menu

![](/basic-usage/features/lightgun/calibration/naomi1-en.png){.full-width}

2) Go to "Options"

![](/basic-usage/features/lightgun/calibration/naomi2-en.png){.full-width}

3) You will need to verify if the setting "Allow NAOMI Service Buttons" is enabled

![](/basic-usage/features/lightgun/calibration/naomi3-en.png){.full-width}

4) Now to to Main Menu > Settings > Input

![](/basic-usage/features/lightgun/calibration/naomi4-en.png){.full-width}

5) Go to "Port 1 Controls"

![](/basic-usage/features/lightgun/calibration/naomi5-en.png){.full-width}

6) To select the "Controller" (we need a controller and not the lightgun to enable the menu)

7) To finish (`Hotkey` + `B`) To go out of the menu and return in game.

**NAOMI TEST MODE**: you can access to it with `L3` on your controller.

![](/basic-usage/features/lightgun/calibration/naomi6.png){.full-width}

8) Select the "**GAME TEST MODE**" menu with `R3` and confirm the menu entry with `L3`.

![Menu selection](/basic-usage/features/lightgun/calibration/naomi7.png){.full-width}

![After validation, we arrive in the "GAME TEST MODE" menu](/basic-usage/features/lightgun/calibration/naomi8.png){.full-width}

9) We can now select the "**GUN SETTING**" menu with `R3` and confirm the menu entry with `L3`.

![Menu selection](/basic-usage/features/lightgun/calibration/naomi9.png){.full-width}

![After validation, we arrive in the menu "GAME TEST MODE"](/basic-usage/features/lightgun/calibration/naomi10.png){.full-width}

10) Now we can select the menu "**PLAYER1 GUN ADJUSTMENT**" with `R3` and confirm the menu entry with `L3`.

![Menu selection](/basic-usage/features/lightgun/calibration/naomi11.png){.full-width}

![](/basic-usage/features/lightgun/calibration/naomi12.png){.full-width}

11) **Please note**, you will have to go back to RetroArch (`Hotkey` + `B`) and then to the main menu / Settings / Inputs / Port 1 controls to reactivate the "Light Gun" (instead of "Controller") in order to calibrate it.

You can now calibrate by first **shooting on the blue viewfinder at the top left and then at the bottom right**.

![](/basic-usage/features/lightgun/calibration/naomi13.png){.full-width}

![After selecting the 2 corners, the calibration calculation is done...](/basic-usage/features/lightgun/calibration/naomi14.png){.full-width}

12) You can see the result and test the OFF SCREEN (which is at the bottom left in this game).

![](/basic-usage/features/lightgun/calibration/naomi15.png){.full-width}

![](/basic-usage/features/lightgun/calibration/naomi16.png){.full-width}

13) **Please note**, you will have to go back to RetroArch (`Hotkey` + `B`) and then to the main menu / Settings / Inputs / Port 1 Controls to reactivate the "Controller" (instead of "Light Gun") to be able to exit.

You can validate the configuration by pressing the **R3** button on the controller and return to the previous menu.

14) We can start again from step 3 but selecting the menu "**PLAYER2 GUN ADJUSTMENT**" if we have a setup for two players.

15) Finally, select EXIT with `L3`/`R3` and in each menu to exit and return to the game.

![Exit the "GUN SETTING" menu](/basic-usage/features/lightgun/calibration/naomi17.png){.full-width}

![Exit "GAME TEST" menu](/basic-usage/features/lightgun/calibration/naomi18.png){.full-width}

![Exit "NAOMI TEST MODE" menu](/basic-usage/features/lightgun/calibration/naomi19.png){.full-width}

![The game system restarts](/basic-usage/features/lightgun/calibration/naomi20.png){.full-width}

16) And finally, before playing, we advise to restart the game so that all the confs of RetroArch are reset to the default configuration.

![](/basic-usage/features/lightgun/calibration/naomi21.png){.full-width}

>You will have to keep your files, you will not have to do it again, it will be in your backups:  
>
>![](/basic-usage/features/lightgun/calibration/calibration2.png){.full-width}
{.is-warning}