---
title: WASM-4
description: 
published: true
date: 2023-09-27T06:43:04.348Z
tags: fantasy, 9.1+, wasm4
editor: markdown
dateCreated: 2023-09-27T06:38:12.232Z
---

![](/emulators/fantasy/wasm4.svg){.align-center}

## Technical data

* **Developer**: Bruno Garcia
* **Year of release**: 2021
* **Display**: 60x160 pixels, 4 customizable colors, updated at 60 Hz
* **RAM**: 64KB linear RAM, memory-mapped I/O, save states
* **Storage** : Cartridge Size Limit 64 KB
* **Device**: Keyboard, mouse, touchescreen, up to 4 gamepads
* **Sound**: 2 pulse wave channels, 1 triangle wave channel, 1 noise channel

## Presentation

WebAssembly, abbreviated wasm, is a World Wide Web standard for application development. It is designed to replace JavaScript with superior performance. The standard consists of bytecode, its textual representation and a sandboxed execution environment compatible with JavaScript. It can be run both inside and outside a web browser. WebAssembly is standardized within the World Wide Web Consortium.

As WebAssembly only specifies a low-level language, bytecode is generally produced by compiling a higher-level language. Among the first languages supported were Rust, with the wasm-bindgen project/module (crate), and C and C++, compiled with Emscripten (based on LLVM). Many other programming languages now have WebAssembly compilers, including C#, Go, Java, Lua, Python, Ruby, Fortran and Pascal.

Web browsers compile wasm bytecode in the machine language of the host on which they are used, before executing it.

## Emulators

[Libretro Wasm4](/emulators/fantasy/wasm4/libretro-wasm4)