---
title: Sobrecarga de configuração
description: Descrição da sobrecarga de configuração local aplicada a jogos específicos ou a pastas completas.
published: true
date: 2023-08-29T12:49:03.677Z
tags: configuração, sobrecarga
editor: markdown
dateCreated: 2021-09-18T14:39:16.705Z
---

## Visão geral das possibilidades

Você pode sobrecarregar a configuração de um jogo ou de toda uma pasta.

Isto significa que ao adicionar arquivos específico nas suas pastas de ROMs, você poderá modificar o comportamento do Recalbox, do RetroArch, ou do emulador para um determinado jogo ou para todos os jogos em uma pasta.

Em termos concretos, você será capaz de manipular:
* A configuração do Recalbox
* A configuração geral do RetroArch
* A configuração dos núcleos do RetroArch

Você também poderá sobrepor as imagens e descrições das pastas de ROMs como são exibidas no EmulationStation, veremos o porquê mais a frente.

Para um jogo ou conjunto de jogos, seremos capazes de:

* Definir um emulador ou núcleo específico
* Definir uma resolução de vídeo específica
* Modificar a configuração de vídeo do RetroArch
* Modificar as opções dos núcleos
* etc.

## Geral

As sobrecargas serão aplicadas a um arquivo base. Estes arquivos são os primeiros arquivos que são carregados pelo iniciador Python (chamado configgen) quando um jogo é iniciado:

| Arquivo de sobrecarga | Alvo | Arquivo de configuração |
| :--- | :--- | :--- |
| `.recalbox.conf` | **Recalbox** | `/recalbox/share/system/recalbox.conf` |
| `.retroarch.cfg` | **RetroArch sem opções principais** | `/recalbox/share/system/configs/retroarch/retroarchcustom.cfg.origin` |
| `.core.cfg` | **Opções principais** | `/recalbox/share/system/configs/retroarch/cores/retroarch-core-options.cfg` |

Todos os arquivos de configuração que podem ser sobrecarregados são arquivos do tipo chave/valor. Podemos, portanto, modificar o valor de uma chave na configuração base, ou definir uma chave que ainda não existe na configuração.

Podemos até mesmo sobrecarregar as sobrecargas! De fato, o sistema permite definir uma sobrecarga por nível de pastas.
  
Portanto, ele começará carregando a configuração base, depois aplicará sucessivamente todos os arquivos de sobrecarga que encontrar nas pastas, começando pela raiz. No final, ele tentará aplicar a sobrecarga do jogo, se ela existir.

Por exemplo, se executarmos a ROM `/recalbox/share/roms/snes/platform/Aladdin (France).zip`, o configgen tentará sobrecarregar a configuração do RetroArch, carregando em ordem:

1. `Base: /recalbox/share/system/configs/retroarch/retroarchcustom.cfg`
2. `Caminho: /.retroarch.cfg`
3. `Caminho: /recalbox/.retroarch.cfg`
4. `Caminho: /recalbox/share/.retroarch.cfg`
5. `Caminho: /recalbox/share/roms/.retroarch.cfg`
6. `Caminho: /recalbox/share/roms/snes/.retroarch.cfg`
7. `Caminho: /recalbox/share/roms/snes/platform/.retroarch.cfg`
8. `Caminho: /recalbox/share/roms/snes/platform/Aladdin (France).zip.retroarch.cfg`

Naturalmente, apesar de ser possível, não é realmente aconselhável sobrecarregar a configuração até que você chegue pelo menos na pasta de um sistema.

Você notará que para sobrecarregar uma pasta, os arquivos de sobrecarga devem estar dentro da pasta, e começar com um ponto (.), o que os torna invisíveis para o sistema linux. 
  
Por outro lado, a sobrecarga de um jogo, deve ser nomeada exatamente como o jogo, incluindo a extensão do arquivo, seguida pelo sufixo de sobrecarga `.retroarch.cfg`, como no exemplo acima.

>Se os arquivos de sobrecarga ficarem dentro de suas pastas de ROMs, eles não serão afetados caso houver uma falha no Recalbox, caso uma atualização der errado, ou caso haja uma falha em seu SD (considerando que, como recomendamos, você use armazenamento externo, claro).

>Tambem são transportáveis: leve seu dispositivo externo USB com você para jogar na casa de um amigo, sua configuração será aplicada sem ter que tocar em nada!
{.is-info}

## Sobrecargas do Recalbox

A sobrecarga da configuração do Recalbox tem dois interesses imediatos:

* Ser capaz de selecionar um determinado modo de vídeo para um jogo ou conjunto de jogos. Os aficionados por "Pixel Perfeito" no arcade  ficarão encantados.
* Poder escolher um núcleo ou um emulador autônomo para um jogo ou um conjunto de jogos.

Existem outras possibilidades, e sem dúvida você encontrará algumas. 😉 

>Fazer sobrecargas de chaves que não são usadas pelo configgen não terá efeito! Não espere mudar o comportamento do EmulationStation (sobrecarregando os tipos, por exemplo).
>
>Sobrecarga da pasta: `/caminho/para/suas/roms/.recalbox.conf`
>Sobrecarga da rom: `/caminho/para/suas/roms/arquivo.zip.recalbox.conf`
{.is-warning}

### Exemplo 1: múltiplas versões do MAME

Por que se contentar com uma versão do MAME quando poderíamos ter todas?

Por exemplo, você pode ter o MAME 2003 Plus e o MAME 2010, cada romset em sua própria pasta:  

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 mame
┃ ┃ ┃ ┃ ┣ 📁 MAME2003Plus
┃ ┃ ┃ ┃ ┣ 📁 MAME2010
 
Depois basta adicionar o arquivo `/recalbox/share/roms/mame/MAME2003Plus/.recalbox.conf` :

```ini
mame.emulator=libretro
mame.core=mame2003_plus
```

E o arquivo `/recalbox/share/roms/mame/MAME2010/.recalbox.conf`

```ini
mame.emulator=libretro
mame.core=mame2010
```

É isso aí! A partir de agora, todos os jogos da pasta MAME2003Plus serão executados com o núcleo mame2003_plus-libretro, e os da pasta MAME2010 com o núcleo mame2010-libretro.

### Exemplo 2: atribuição de um núcleo específico a um jogo

Meu jogo `/recalbox/share/pcengine/1943 Kai (Japão).zip` funciona melhor com o núcleo `libretro-mednafen_pce_fast` do que com o núcleo padrão `libretro-mednafen_supergrafx` (uma suposição totalmente arbitrária para o exemplo).

Até agora, era possível fazer isso via EmulationStation, modificando os metadados do jogo. As informações são então seriam armazenadas no arquivo `gamelist.xml`.
Mas, se houver um erro ao escrever o arquivo, ou um scraper mal feito, toda a configuração será perdida.

Com as sobrecargas, você só precisa adicionar o arquivo `/recalbox/share/roms/pcengine/1943 Kai (Japan).zip.recalbox.conf`.

```ini
global.emulator=libretro
global.core=mednafen_pce_fast
```

Desta vez, sem risco de perder a configuração! Naturalmente, a edição dos metadados via EmulationStation ainda funciona. Entretanto, um arquivo de sobrecarega terá prioridade sobre o que está armazenado no arquivo `gamelist.xml`.

### Exemplo 3: Mudança do modo de vídeo para um jogo

No Raspberry Pi2, o modo de vídeo padrão é o CEA 4 HDMI. Mas o jogo `Blazing Stars` no FinalBurn Neo corre um pouco lento. Mudá-lo para 240p certamente ajudaria, além de ser Pixel Perfeito (novamente, uma suposição bastante hipotética, apenas por uma questão de argumento)!

Acrescentamos ao arquivo `/recalbox/share/roms/fba_libretro/blazstar.zip.recalbox.conf`

```ini
global.videomode=CEA 8 HDMI
```
Quando inicio este jogo, minha TV muda para 240p, e posso desfrutar plenamente do `Blazing Stars`.

## Sobrecargas do RetroArch

As configurações do RetroArch dizem respeito ao próprio RetroArch (e há muitas opções de configuração!), bem como aos diferentes núcleos, cada um dos quais tem opções particulares, dependendo das máquinas que emulam.

>Já existem mecanismos específicos do Recalbox e do RetroArch para sobrecarregar ou a linha de comando que inicia o emulador (via recalbox.conf), ou diretamente as configurações do RetroArch/Núcleos (via os menus RetroArch).
>
> Mas nenhum destes sistemas permitiu aplicar sobrecarga a pastas inteiras e/ou manter estas configurações específicas no mesmo lugar que as ROMs. Isto é especialmente interessante para as pessoas que usam compartilhamentos de rede para alimentar as ROMs de vários Recalboxes!
{.is-info}

### Configuração do RetroArch

A configuração do Retroarch em si é extremamente rica e cobre muitas áreas diferentes.

Não há necessidade de entrar em detalhes, o melhor é ir diretamente para o [arquivo RetroArch](https://github.com/libretro/RetroArch/blob/master/retroarch.cfg), que está particularmente bem documentado.

As possibilidades oferecidas pelas sobrecargas locais são enormes, entre as quais podemos contar :

* Configuração do vídeo : Proporção da tela, Escala, antisserrilhamento, rotação da tela, ou mesmo seleções de shaders, etc.
* Sintonia de áudio para alguns jogos difíceis
* Overlays
* Algumas opções de entrada: seleção do mouse, sensibilidade, etc.
* Forçar configurações específicas do NetPlay
* Alterar pastas do RetroArch (a pasta dos *saves* por exemplo )
* Definir várias opções: rebobinar, avançar rapidamente, etc.
* ...

Como as sobrecargas de configuração do Recalbox, podemos criar arquivos `.retroarch.cfg` para pastas e para ROMs.

#### Exemplo: Forçar uma configuração específica do controle

Alguns jogos N64 como GoldenEye 007 ou Perfect Dark, bem como jogos Palm, devem ter a opção `Analógico para Digital` desativada.

Isto é feito adicionando o arquivo `/recalbox/share/roms/n64/.retroarch.cfg` :

```ini
input_player1_analog_dpad_mode = "0"
input_player2_analog_dpad_mode = "0"
input_player3_analog_dpad_mode = "0"
input_player4_analog_dpad_mode = "0"
```


Para um jogo, nomeie o arquivo assim: `nomedojogo.extensão.retroarch.cfg`. Para todo um sistema, `.retroarch.cfg` é suficiente.

### Configurando Núcleos

Sobrecargas das opções de núcleos oferecem enormes possibilidades, incluindo um recurso muito esperado para os fãs dos computadores: a capacidade de definir uma pasta por subsistema!

>Sobrecargas de núcleos são adicionados ao arquivo `/recalbox/share/system/configs/retroarch/cores/retroarch-core-options.cfg` quando o jogo é iniciado, o que significa que uma vez que o jogo é fechado, elas serão salvas no mesmo arquivo. Para evitar surpresas desagradáveis, recomendamos que as chaves do arquivo sejam substituídas por valores padrão (por exemplo `fbneo-frameskip = "0"`) se você quiser substituir um determinado jogo por valores específicos (por exemplo `fbneo-frameskip = "2"`), de modo que você mantenha seus valores "base" para arquivos que não tenham sobrecargas personalizadas.
{.is-info}

Isto é particularmente interessante para núcleos multi-máquinas, tais como :

* **theodore**, que cobre o Thomson MO5, MO6 e TO7 até o TO9+ 
* **atari800**, que cobre todos os Atari 8bits, desde a primeira série 800, até o XE, incluindo o XL. 
* **vice**, que agora emula o C64, PET, Vic20, CBM2, ... 
* **hatari**, que emula dos primeiros 520ST até os últimos Falcons 
* e ainda mais...

#### Exemplo 1: configurando os subsistemas Thomson

Os Thomson, computadores franceses de 8 bits dos anos 80, divididos em 2 séries: 

* Os MOs, que produziram o primeiro MO5, depois o MO6 com seu teclado mecânico e toca-fitas integrado
* Os TOs, que produziram os primeiros TO7 e TO7-70, depois o TO8 e TO8D, com unidades de disquete, e as séries TO9 e TO9 +, computadores com aparência mais profissional

Os jogos MO e TO não são totalmente compatíveis. Dentro da mesma série, há compatibilidade retroativa: Um MO6 irá (geralmente) rodar jogos MO5.  
  
Naturalmente, tentaremos emular cada jogo com a máquina mais próxima daquela para a qual foi originalmente projetada, a fim de evitar qualquer problema e maximizar as chances de ter uma emulação perfeita.

Se pegarmos os pacotes TOSEC ([http://www.tosec.org](https://www.tosecdev.org/)), os jogos da Thomson foram divididos em 4 subsistemas:

* MO5
* MO6
* TO7 (TO7-70)
* TO8, TO8D, TO9 e TO9+.

Assim, criaremos uma estrutura de árvore semelhante:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 thomson
┃ ┃ ┃ ┃ ┣ 📁 MO5
┃ ┃ ┃ ┃ ┣ 📁 MO6
┃ ┃ ┃ ┃ ┣ 📁 T07
┃ ┃ ┃ ┃ ┣ 📁 TO8,T08D,T09,T09+

Na raiz do sistema, colocaremos um arquivo de sobrecarga do núcleo, para forçar algumas opções interessantes para todos:

Vamos colocar no arquivo `/recalbox/share/roms/thomson/.core.cfg`

```ini
theodore_rom = "Auto"
theodore_autorun = "enabled"
theodore_floppy_write_protect = "enabled"
theodore_tape_write_protect = "enabled"
```

Se adicionarmos jogos à raiz, ou a outra pasta, dizemos ao emulador para tentar detectar a melhor máquina (com base no nome do arquivo).  
  
Também protegemos os arquivos roms padrão, e ativamos o mecanismo autorun, que é muito útil quando não conhecemos as máquinas originais.

Depois, em cada subpasta, acrescentaremos uma sobrecarga na chave `theodore_rom` que determina a máquina.

Arquivo `/recalbox/share/roms/thomson/MO5/.core.cfg`

```ini
theodore_rom = "MO5"
```

Arquivo `/recalbox/share/roms/thomson/MO6/.core.cfg`

```ini
theodore_rom = "MO6"
```

Arquivo `/recalbox/share/roms/thomson/TO7/.core.cfg`

```ini
theodore_rom = "TO7"
```

Arquivo `/recalbox/share/roms/thomson/TO8,TO8D,TO9,TO9+.core.cfg`

```ini
theodore_rom = "TO9+"
```

Para a última série, foi selecionada a máquina mais potente: a TO9+.

É isso aí! Agora você tem 4 subsistemas Thomson. O emulador não está mais em automático e o risco de ele escolher o sistema errado ou um sistema padrão desaparece.

É claro que o núcleo de Theodore pode às vezes "autodetectar" a máquina, mas este não é o caso de outros núcleos que precisam ter o subsistema certo ao iniciar.

## Sobrecarga da aparência das pastas

Para aperfeiçoar as possibilidades oferecidas pelas definições do subsistema, seja sobrecarregando a configuração do Recalbox para o MAME, por exemplo, ou sobrecarregando o núcleo de Theodore para tirar o máximo proveito das máquinas TO e MO da época, adicionamos a possibilidade de sobrecarregar a imagem e a descrição de uma pasta.

Isto permite, por exemplo, ter em uma pasta, a foto e a descrição da máquina cujas ROMs estão na referida pasta.

Para fazer isto, você precisa adicionar pelo menos um arquivo de imagem PNG chamado `.folder.picture.png` a pasta que você deseja substituir no EmulationStation. A resolução não importa, mas tenha em mente que o recomendado é que seja a mesma resolução que suas imagens de scrape, ou parecida.

Opcionalmente, você pode adicionar uma descrição de texto, que ficará abaixo da imagem, assim como nos scapres dos jogos. O arquivo deve ser um simples arquivo de texto, chamado `.folder.description.txt`.

### Exemplo 1: MO5 a TO9

Vamos voltar a pasta `thomson` usada acima, que dividimos em 4 subsistemas como a TOSEC fez.

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 thomson
┃ ┃ ┃ ┃ ┣ 🗒 .core.cfg
┃ ┃ ┃ ┃ ┣ 🗒 gamelist.xml
┃ ┃ ┃ ┃ ┣ 📁 media
┃ ┃ ┃ ┃ ┃ ┣ 📁 box3d
┃ ┃ ┃ ┃ ┃ ┣ 📁 images
┃ ┃ ┃ ┃ ┃ ┣ 📁 videos
┃ ┃ ┃ ┃ ┣ 📁 MO5
┃ ┃ ┃ ┃ ┃ ┣ 🗒 .core.cfg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 .folder.description.txt
┃ ┃ ┃ ┃ ┃ ┣ 🗒 .folder.picture.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 [FD]
┃ ┃ ┃ ┃ ┃ ┣ 🗒 [K7]
┃ ┃ ┃ ┃ ┃ ┣ 🗒 [M5]
┃ ┃ ┃ ┃ ┃ ┣ 🗒 [QD]
┃ ┃ ┃ ┃ ┃ ┣ 🗒 [SAP]
┃ ┃ ┃ ┃ ┃ ┣ 🗒 [WAV]
┃ ┃ ┃ ┃ ┣ 📁 MO6
┃ ┃ ┃ ┃ ┃ ┣ 🗒 .core.cfg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 .folder.description.txt
┃ ┃ ┃ ┃ ┃ ┣ 🗒 .folder.picture.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 [K7]
┃ ┃ ┃ ┃ ┣ 📁 TO7
┃ ┃ ┃ ┃ ┃ ┣ 🗒 .core.cfg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 .folder.description.txt
┃ ┃ ┃ ┃ ┃ ┣ 🗒 .folder.picture.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 [K7]
┃ ┃ ┃ ┃ ┃ ┣ 🗒 [M7]
┃ ┃ ┃ ┃ ┣ 📁 TO8,TO8D,TO9,TO9+
┃ ┃ ┃ ┃ ┃ ┣ 🗒 .core.cfg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 .folder.description.txt
┃ ┃ ┃ ┃ ┃ ┣ 🗒 .folder.picture.png
┃ ┃ ┃ ┃ ┃ ┣ 🗒 [FD]
┃ ┃ ┃ ┃ ┃ ┣ 🗒 [K7]
┃ ┃ ┃ ┃ ┃ ┣ 🗒 [QD]
┃ ┃ ┃ ┃ ┃ ┣ 🗒 [SAP]


O arquivo `.folder.picture.png` na pasta `/recalbox/share/roms/thomson` contém uma imagem da máquina:

![Foto do MO5](/advanced-usage/config-overloads/mo5.folder.picture.png){.full-width}

E o arquivo `/recalbox/share/roms/thomson/MO5/.folder.description.txt` contém :

```text
Marca:
    - Thomson (França)
CPU:
    - Motorola 6809E funcionando a 1 MHz
RAM:
    - 48Kb (expansível)
ROM:
    - 16Kb
Gráficos:
    - Modo texto: 40 colunas x 25 linhas
    - Modo gráfico: 320 x 200, 16 cores (restrição de proximidade)
Som:
    - Gerador de 1bit (6bit de extensão do módulo DAC possível)
Dispositivos de entrada:
    - Teclado integrado de 57 teclas 
    - Caneta ótica
    - Joysticks
Interfaces:
    - Fonte de alimentação externa
    - Conector DIN (caneta óptica)
    - Conector DIN (unidade de fita)
    - Peritel (RGB)
    - Porta de extensão
Software:
    - BASIC 1.0 Integrado
Dispositivos padrão:
    - Porta de cartucho (Memo5)
    - Unidade de fita externa (modelo específico MO5)
    - Unidade de disco externo
Dimensões:
    - 51 x 291 x 190 mm 
Peso:
    - 1,1 kg
Lançamento:
    - 1984
```

E este é o resultado no EmulationStation :

![EmulationStation](/advanced-usage/config-overloads/thomson-gamelist.png){.full-width}

### Exemplo 2: MAME

Da mesma forma, no primeiro exemplo que mostra como ter várias versões do MAME na pasta MAME, poderíamos imaginar ter um belo logotipo do MAME 2003 Plus e do MAME 2010, seguidos por um pequeno texto que dá o número de jogos, e a versão correspondente de cada romset MAME.

Um pequeno logotipo:

![Imagem do MAME 2003 Plus](/advanced-usage/config-overloads/mame.folder.picture.png){.full-width}

Um pequeno texto no arquivo `/recalbox/share/roms/mame/MAME2003Plus/.folder.description.txt` :

```text
MAME 2003 Plus é uma versão melhorada do MAME2003, com enormes correções de bugs e centenas de novos jogos.

Romset Especial com base no romset MAME 0.78

4859 jogos no total!
```

E este é o resultado:

![](/advanced-usage/config-overloads/mame-gamelist.png){.full-width}

## Emuladores autônomos

Atualmente, não podemos sobrecarregar as configurações dos emuladores autônomos, exceto parcialmente para o Amiberry, o emulador Amiga para sistemas ARM bem como o Hatari, que tem as suas sobrecargas em `.hatari.cfg` e funciona da mesma forma.

Não há planos para acrescentar essas sobrecargas, pois elas exigem códigos e testes especiais. Também não é necessariamente viável para todos os emuladores, mas em qualquer caso, levará algum tempo.

No entanto, dependendo dos pedidos e de sua relevância, veremos eventualmente, caso a caso.