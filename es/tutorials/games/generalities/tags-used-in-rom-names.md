---
title: Etiquetas (tags) utilizadas en los nombres de las roms
description: 
published: true
date: 2023-09-08T10:38:30.921Z
tags: rom, tags, codes, fichero, nombre, etiquetas
editor: markdown
dateCreated: 2023-09-08T10:37:46.648Z
---

>Estas etiquetas no son válidas para las roms de arcade.
{.is-info}

## Etiquetas geográficas

**(1)** Japón y Corea
**(4)** EE.UU. y Brasil NTSC
**(A)** Australia
**(As)** Asia
**(B)** no EE.UU. (Genesis / Megadrive)
**(C)** o **(Ch)** China
**(D)** Países Bajos
**(E)** Europa
**F)** Francia
**G)** Alemania
**(H)** Países Bajos
**I)** Italia
**(J)** Japón y Corea del Sur  
**(K)** Corea  
**(R)** Rusia  
**(S)** España  
**(U)** EE.UU. y Estados Unidos
**(FC)** Canadá francés
**(FN)** Finlandia
**(GR)** Grecia
**(HK)** Hong Kong
**(NL)** Países Bajos
Países Bajos **(No)** Noruega
**(PD)** **Dominio público** : Software libre
**(UK)** Reino Unido
**(SW)** Suecia y Suiza
**(UK)** Reino Unido
**(Unk)** País desconocido
**(Bra)** Brasil / Portugués
**(Kor)** Corea
**(Eng)** Inglés
**Noruego
**(Dut)** Holandés
**(Pol)** Polaco
Finlandés **(Por)** Portugués
**(Por)** Portugués
**(Fre)** Francés
**(Rus)** Ruso
**(Ger)** Alemán
**(Ser)** Serbio
**(Gre)** Griego
**(Spa)** Español
**(Ita)** Italiano
**(Swe)** Sueco

## Etiquetas generales de la versión

**\[a]** Alternative : Versión alternativa generalmente corrigen bugs
**\[b]** Bad Dump: el dump no es bueno, se trata de una versión corrupta
**\[c]** Crack: craqueado.
**\[f]** Fixed: Optimizado para funcionar mejor en el emulador
**\[h]** Hacker: versión modificada, puede ir desde trucos para hacer trampas hasta la inserción de publicidad
**(M5)** Multilanguage: 5 idiomas en este ejemplo.
**\[o]** Overdump: talla superior a la de un cartucho estandar, para el emulador no cambia nada
**\[p]** Pirate: rom no original
**\[t]** Formed: Abre generalmente el menú de trucos al lanzamiento de la rom
**\[T]** Traduction: traducción hecha pour los fans
**\[T +]** La Traducción más reciente: traducción de Rom, utilización de la rom actual
**\[T-]** La traducción antigua: La traducción ha adelantado al juego, una nueva traducción existe.
**(Unl)** Unlicensed: raro de encontrar, se trata de un juego acabado o no, pero que nunca ha salido a la luz.
**\[x]** Suma de control incorrecta
**(ZZZ)** no clasificado
**(-)** Año desconocido
**\[!]** Verified Bon Dump: Versión verificada y perfectamente jugable, lo mejor que podéis encontrar!.
**\[! p]** Volcado pendiente: Esta es la copia más parecida al juego original hasta la fecha, pero seguimos esperando una copia exacta de la ROM.
**\[###]** Suma de control
**(?? k)** Tamaño de la ROM
**(Bootleg)** Es una versión "pirata" del juego. También había versiones que a veces se modificaban, con cambios en la jugabilidad (Street Fighter II Rainbow es una de las más conocidas) o eliminación de la protección/cambio del tipo de tarjeta electrónica. En algunas partes del mundo, éstas eran más conocidas que las versiones oficiales.
**(Taikenban)** Versión de prueba, versión jugable, démo versión limitada. El nombre forma parte del origen de una revista web creada a través del foro [Limited Edition](http://www.edition-limited.net/).
**(Fukkobuban)** Edición collector, reedición revisada de una versión que corría sobre un hardware más antiguo.
**(Genteiban)** Versión Limitada
**(Shokai Genteiban)** Versión Limitada
**(Midway Classics)** Juegos reeditados después de que Midway, una empresa estadounidense que desarrolla y publica videojuegos para máquinas recreativas y consolas, comprara los derechos.

## Tags avancés

**(NG-Dump Known)** No se ha encontrado una buena copia de seguridad
**(REVXX)** Número de revisión (00 es la más antigua)
**(VX.X)** Número de versión (1.0 es la más antigua)
**(M #)** Multilingüe; nº de idiomas (seleccionables por menú)
**(?? - in-1)** Cartucho multijuego pirata
**(Vol #)** Cartucho oficial multijuego
**(GCN)** rip de Nintendo GameCube
**\[h # C]** Informationes sobre el cartucho interne pirateadas
**\[h # + # C]** Informations sobre el cartucho interno pirateadas ; # ème variante
**\[hI]** Intro del groupo de dumping
**\[hIR]** Intro del groupo de dumping suprimido
**\[M]** Juego mono-color
**\[Alpha]** Versión Alpha
**\[Beta]** Versión bêta
**(Prototype)** Versión prototype
**(Pre-version)** Versión pré-versión
**(Kiosk Demo)** Versión de démonstración disponible en los kioscos
**(Hack)** ROM pirateada
**(Menu)** Mençu multicartas, opciones no seleccionables
**(ZZZ_UNK)** ROM no classificada
**(BIOS)** Copia del sistema básico de entrada/salida de la consola
**(N64DD)** ROM Nintendo 64DD
**(## MBit)** Tamaño de la ROM en mégabits
**(## k)** Tamaño de la ROM en kilobits
**(19XX)** Año de salida (siglo 20)
**(20XX)** Año de salida (siglo 21)
**(Atmos)** Atmos ROM
**(Telestrat)** Telestrat ROM
**\[R]** Formato RSID
**(old)** Versión antigua
**\[R-XXX]** Idioma
**(SC-3000)** Cartucho SC-3000
**(SG-1000)** Cartucho Sega SG-1000
**(SF-7000)** Cartucho SF-7000
**(GG2SMS)** Mode Sega Master System
**(Cart)** Formato de cartucho
**(Adam)** Versión ADAM
**(Compilation)** la ROM es un volcado de compilación
**(Ch-Trad)** Juego tradicional chino pirata
**(Ch-Simple)** Juego estandar chino pirata
**\[f1C]** Hack, sólo se ha modificado el nombre del cartucho
**(PAL)** Europea
**(NTSC)** Americaina

## Etiquetas especiales

### Atari

**(PAL)** Versión para Europa

### Coleco

**Versión ADAM

### Game Boy Advance

**\[hIR00]** Intro suprimida ; remplazada por los valores 00
**\[hIRff]** Intro suprimida ; remplazada por los valores ff
**\[f_?]** EEPROM_V124 patch suprimido
**\[v]** Vídeo Game Boy Advance
**\[eb]** Libro electrónico ROM
**\[eba]** E-Book Advance ROM
**\[ebb]** Lector de libros Advance ROM
**\[f125]** EEPROM_V125 suprimida
**\[f126]** EEPROM_V126 suprimida
**(IQue)** ROM china Game Boy Advance para la consola IQue
**(Coleco sur émulation GBA)** ROM ColecoVision via Coleco en  GBA
**(émulation Cologne)** ROM ColecoVision via emulador de Cologne
**(émulation DrSMS)** ROM Sega Master System via un emulador DrSMS
**(émulation FooN)** ROM ZX Spectrum via emulador FooN
**(émulation PocketNES)** ROM NES via emulador PocketNES
**(émulation FC2GBA)** ROM NES via emulador FC2GBA
**(émulation Goomba)** ROM Game Boy via emulador Goomba
**(émulation GBAGI)** ROM AGI (Adventure Game Interpreter) via emulador GBAGI
**(émulation Swanadvance)** ROM WonderSwan via un emulador Swanadvance
**(émulation PCEAdvance)** ROM TurboGrafx-16 via un emulador PCEAdvance
**(émulation PocketSMS)** ROM Sega Master System via un emulador PocketSMS
**(émulation PocketSNES)** ROM SNES via emuladorPocketSNES
**(émulation SNESAdvance)** ROM SNES via emulador SNESAdvance
**(émulation Snezzyboy)** ROM SNES via emulador Snezzyboy
**(émulation VGB)** ROM Game Boy via un emulador VGB
**(Pogoshell)** Programa para la herramienta Pogoshell
**(s ## e ##)** Número de serie y número de episodio para los vídeos
**(MB)** ROM a arranque múltiple
**(MB2GBA)** Juego de arranque múltiple convertido al format GBA
**(-e)** Lectora de ROM
**(+ ## NDA)** Multicartas sin licencia con ## juegos NES
**(NDS)** Volcado del slot cartoucho 2 Nintendo DS
**(GoodBook)** E-Book utilizando el convertidor GoodBook
**(TextReader)** E-Book utilizando el convertidor TextReader
**(PictureBoy)** E-Book utilizando el convertidor PictureBoy
**(ReadBoy)** E-Book utilizando el convertidor ReadBoy

### Gameboy

**\[C]** **Color**
**\[S] Super**
**\[BF] Bug Fix** : Para un cartucho físico modificado (cartas de tipo flash como en la DS)

### Super Nintendo

**(BS)** **BS ROM** : Rom enviada por satélite a los televisores durante un programa de la televisión japonesa
**(ST)** **Sufami Turbo** : Para jugar con dos cartuchos GB al mismo tiempo en la SNES
**(NP) Nintendo Power** : Juegos de distribución privada (a suscriptores de revistas en Japón)

### Megadrive

**(1)** Japón
**(4)** USA
**(5)** sólo NTSC 
**(8)** sólo PAL 
**(B)** **non USA** : Funciona en todas las MG excepto USA
**\[c]** **Checksum** : Experimenta algunos problemas de huella
**(F)** Mundo (Megadrive/Genesis)
**(W)** Mundo (Genesis)
**\[x] Checksum incorrect** : checksum incorrecto, alto riesgo de errores
**\[R-]** **Pays** : Por ejemplo \[ES] para España
**(J-Cart)** El cartucho original tiene dos puertos controladores
**(SN)** Juego Sega-Net
**(REVXB)** Sonic the Hedgehog, versión japonesa 2
**(REVSC02)** Sonic Compilation, versión de Sonic 2
**(MP)** Versión MegaPlay
**(MD Bundle)** La ROM proviene del juego Pack-in Sega Mega Drive
**(Alt Music)** Versión musical alternativa de Sonic Spinball

### NeoGeo Pocket

**\[M]** **Mono uniquement** : Audio mono

### NES / FC

**(PC10) PlayChoice 10** : Versión Arcade basada en los juegos NES
**(VS) Versus** : Idéntico a (PC10) pero no se puede jugar en solitario
**\[hFFE]** Copiado de copiadoras del Lejano Oriente
**\[hMxx]** Hackeado para que funcione con la tarjeta xx
**(No SMB)** Hack Super Mario Bros. no especificado
**(Aladdin)** Funciona con un cartucho Aladdin que bloquea el chip de bloqueo de 10NES
**(Sachen)** Juego sin licencia Sachen
**(KC)** Clásico de Konami
**(PRG0)** Revision del programa 0
**(PRG1)** Revision del programa 1
**\[U]** Formato de archivo de imagen universal de NES
**(Mappeur ##)** Número de mapeador
**(FDS Hack)** Pirateado del Famicom Disk System a la NES
**(GBA E-reader)** Hackeado de la tarjeta e-Reader
**(E-GC)** ROM de NES extraída de una Nintendo GameCube europea
**(J-GC)** ROM de NES extraída de una Nintendo GameCube japonesa

### SNES

**(BS)** **Broadcast Satellite (Satellaview) ROM** : ROMs enviadas por satélite a los televisores, pero que nunca han tenido soporte físico.
**(NP) Nintendo Power ROM** : Juegos de distribución privada (a suscriptores de periódicos en Japón)
**(NSS)** ROM SNES de tipo Arcade
**(ST) Sufami Turbo ROM** : Cartucho especial para jugar con dos cartuchos GB al mismo tiempo en la SNES.

