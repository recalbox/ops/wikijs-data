---
title: Libretro Gambatte
description: 
published: true
date: 2024-07-24T23:18:49.225Z
tags: libretro, color, gameboy, gambatte, gbc
editor: markdown
dateCreated: 2021-07-14T06:34:16.543Z
---

**Libretro Gambatte** is a cross-platform, open source, precision-oriented Game Boy Color emulator written in C++.  
It is based on hundreds of hardware test cases, as well as previous documentation and reverse engineering efforts.

## ![](/emulators/license.svg) License

This core is under [**GPLv2**](https://github.com/libretro/gambatte-libretro/blob/master/COPYING) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Features

| Feature | Supported |
| :---: | :---: |
| Restart | ✔ |
| Saves | ✔ |
| States | ✔ |
| Rewind | ✔ |
| Core Options | ✔ |
| RetroAchievements | ✔ |
| RetroArch Cheats | ✔ |
| Controls | ✔ |
| Remapping | ✔ |
| Softpatching | ✔ |

## ![](/emulators/bios.svg) BIOS

### List of optional bios

| Filename | Description | MD5 | Provided |
| :---: | :---: | :---: | :---: |
| gbc_bios.bin | Game Boy Color BIOS | dbfce9db9deaa2567f6a84fde55f9680 | ❌ |

### Location

Put your bios like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 gbc
┃ ┃ ┃ ┃ ┣ 🗒 **gbc_bios.bin**

## ![](/emulators/roms.png) Roms

### Supported extensions

Roms must have the extension:

* .bin
* .gbc
* .zip
* .7z

This system supports compressed roms in .zip/.7z format. However, be careful, it is only an archive.

The files contained in the .zip/.7z must correspond to the extensions mentioned above.
Moreover, each .zip/.7z file must contain only one compressed rom.

### Location

Put your roms like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 gbc
┃ ┃ ┃ ┃ ┣ 🗒 **game.zip**

>The roms using **No-Intro** format are highly recommended.
{.is-success}

>For more information about roms, go to [this tutorial](./../../../tutorials/games/generalities/isos-and-roms)!
{.is-info}

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Accessing to options

You can configure various options in two different ways.

* Via RetroArch menu:

┣ 📁 RetroArch Menu
┃ ┣ 📁 Core options
┃ ┃ ┣ 🧩 Name_option

* Via the file `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core options

| Options | Options values | Variables (for configuration override) | Variables values |
| --- | --- | --- | --- |
| GB Colorization | `Disabled` ✅ / `Auto` / `GBC` / `SGB` / `Internal` / `Custom` | `gambatte_gb_colorization` | `disabled` / `auto` / `GBC` / `SGB` / `internal` / `custom` |
| Internal Palette | `GB - DMG` ✅ / `GB - Pocket` / `GB - Light` / `GBC - Blue` / `GBC - Brown` / `GBC - Dark Blue` / `GBC - Dark Brown` / `GBC - Dark Green` / `GBC - Grayscale` / `GBC - Green` / `GBC - Inverted` / `GBC - Orange` / `GBC - Pastel Mix` / `GBC - Red` / `GBC - Yellow` / `SGB - 1A` / `SGB - 1B` / `SGB - 1C` / `SGB - 1D` / `SGB - 1E` / `SGB - 1F` / `SGB - 1G` / `SGB - 1H` / `SGB - 2A` / `SGB - 2B` / `SGB - 2C` / `SGB - 2D` / `SGB - 2E` / `SGB - 2F` / `SGB - 2G` / `SGB - 2H` / `SGB - 3A` / `SGB - 3B` / `SGB - 3C` / `SGB - 3D` / `SGB - 3E` / `SGB - 3F` / `SGB - 3G` / `SGB - 3H` / `SGB - 4A` / `SGB - 4B` / `SGB - 4C` / `SGB - 4D` / `SGB - 4E` / `SGB - 4F` / `SGB - 4G` / `SGB - 4H` / `Special 1` / `Special 2` / `Special 3` / `Special 4 (TI-83 Legacy)` / `TWB64 - Pack 1` / `TWB64 - Pack 2` / `PixelShift - Pack 1` | `gambatte_gb_internal_palette` | `GB - DMG` / `GB - Pocket` / `GB - Light` / `GBC - Blue` / `GBC - Brown` / `GBC - Dark Blue` / `GBC - Dark Brown` / `GBC - Dark Green` / `GBC - Grayscale` / `GBC - Green` / `GBC - Inverted` / `GBC - Orange` / `GBC - Pastel Mix` / `GBC - Red` / `GBC - Yellow` / `SGB - 1A` / `SGB - 1B` / `SGB - 1C` / `SGB - 1D` / `SGB - 1E` / `SGB - 1F` / `SGB - 1G` / `SGB - 1H` / `SGB - 2A` / `SGB - 2B` / `SGB - 2C` / `SGB - 2D` / `SGB - 2E` / `SGB - 2F` / `SGB - 2G` / `SGB - 2H` / `SGB - 3A` / `SGB - 3B` / `SGB - 3C` / `SGB - 3D` / `SGB - 3E` / `SGB - 3F` / `SGB - 3G` / `SGB - 3H` / `SGB - 4A` / `SGB - 4B` / `SGB - 4C` / `SGB - 4D` / `SGB - 4E` / `SGB - 4F` / `SGB - 4G` / `SGB - 4H` / `Special 1` / `Special 2` / `Special 3` / `Special 4 (TI-83 Legacy)` / `TWB64 - Pack 1` / `TWB64 - Pack 2` / `PixelShift - Pack 1` |
| > TWB64 - Pack 1 Palette | `TWB64 001 - Aqours Blue` ✅ / `TWB64 002 - Anime Expo Ver.` / `TWB64 003 - SpongeBob Yellow` / `TWB64 004 - Patrick Star Pink` / `TWB64 005 - Neon Red` / `TWB64 006 - Neon Blue` / `TWB64 007 - Neon Yellow` / `TWB64 008 - Neon Green` / `TWB64 009 - Neon Pink` / `TWB64 010 - Mario Red` / `TWB64 011 - Nick Orange` / `TWB64 012 - Virtual Boy Ver.` / `TWB64 013 - Golden Wild` / `TWB64 014 - Builder Yellow` / `TWB64 015 - Classic Blurple` / `TWB64 016 - 765 Production Ver.` / `TWB64 017 - Superball Ivory` / `TWB64 018 - Crunchyroll Orange` / `TWB64 019 - Muse Pink` / `TWB64 020 - Nijigasaki Yellow` / `TWB64 021 - Gamate Ver.` / `TWB64 022 - Greenscale Ver.` / `TWB64 023 - Odyssey Gold` / `TWB64 024 - Super Saiyan God` / `TWB64 025 - Super Saiyan Blue` / `TWB64 026 - Bizarre Pink` / `TWB64 027 - Nintendo Switch Lite Ver.` / `TWB64 028 - Game.com Ver.` / `TWB64 029 - Sanrio Pink` / `TWB64 030 - BANDAI NAMCO Ver.` / `TWB64 031 - Cosmo Green` / `TWB64 032 - Wanda Pink` / `TWB64 033 - Link's Awakening DX Ver.` / `TWB64 034 - Travel Wood` / `TWB64 035 - Pokemon Ver.` / `TWB64 036 - Game Grump Orange` / `TWB64 037 - Scooby-Doo Mystery Ver.` / `TWB64 038 - Pokemon mini Ver.` / `TWB64 039 - Supervision Ver.` / `TWB64 040 - DMG Ver.` / `TWB64 041 - Pocket Ver.` / `TWB64 042 - Light Ver.` / `TWB64 043 - Miraitowa Blue` / `TWB64 044 - Someity Pink` / `TWB64 045 - Pikachu Yellow` / `TWB64 046 - Eevee Brown` / `TWB64 047 - Microvision Ver.` / `TWB64 048 - TI-83 Ver.` / `TWB64 049 - Aegis Cherry` / `TWB64 050 - Labo Fawn` / `TWB64 051 - MILLION LIVE GOLD!` / `TWB64 052 - Tokyo Midtown Ver.` / `TWB64 053 - VMU Ver.` / `TWB64 054 - Game Master Ver.` / `TWB64 055 - Android Green` / `TWB64 056 - Ticketmaster Azure` / `TWB64 057 - Google Red` / `TWB64 058 - Google Blue` / `TWB64 059 - Google Yellow` / `TWB64 060 - Google Green` / `TWB64 061 - WonderSwan Ver.` / `TWB64 062 - Neo Geo Pocket Ver.` / `TWB64 063 - Dew Green` / `TWB64 064 - Coca-Cola Red` / `TWB64 065 - GameKing Ver.` / `TWB64 066 - Do The Dew Ver.` / `TWB64 067 - Digivice Ver.` / `TWB64 068 - Bikini Bottom Ver.` / `TWB64 069 - Blossom Pink` / `TWB64 070 - Bubbles Blue` / `TWB64 071 - Buttercup Green` / `TWB64 072 - NASCAR Ver.` / `TWB64 073 - Lemon-Lime Green` / `TWB64 074 - Mega Man V Ver.` / `TWB64 075 - Tamagotchi Ver.` / `TWB64 076 - Phantom Red` / `TWB64 077 - Halloween Ver.` / `TWB64 078 - Christmas Ver.` / `TWB64 079 - Cardcaptor Pink` / `TWB64 080 - Pretty Guardian Gold` / `TWB64 081 - Camouflage Ver.` / `TWB64 082 - Legendary Super Saiyan` / `TWB64 083 - Super Saiyan Rose` / `TWB64 084 - Super Saiyan` / `TWB64 085 - Perfected Ultra Instinct` / `TWB64 086 - Saint Snow Red` / `TWB64 087 - Yellow Banana` / `TWB64 088 - Green Banana` / `TWB64 089 - Super Saiyan 3` / `TWB64 090 - Super Saiyan Blue Evolved` / `TWB64 091 - Pocket Tales Ver.` / `TWB64 092 - Investigation Yellow` / `TWB64 093 - S.E.E.S. Blue` / `TWB64 094 - Game Awards Cyan` / `TWB64 095 - Hokage Orange` / `TWB64 096 - Straw Hat Red` / `TWB64 097 - Sword Art Cyan` / `TWB64 098 - Deku Alpha Emerald` / `TWB64 099 - Blue Stripes Ver.` / `TWB64 100 - Stone Orange` | `gambatte_gb_palette_twb64_1` | `TWB64 001 - Aqours Blue` / `TWB64 002 - Anime Expo Ver.` / `TWB64 003 - SpongeBob Yellow` / `TWB64 004 - Patrick Star Pink` / `TWB64 005 - Neon Red` / `TWB64 006 - Neon Blue` / `TWB64 007 - Neon Yellow` / `TWB64 008 - Neon Green` / `TWB64 009 - Neon Pink` / `TWB64 010 - Mario Red` / `TWB64 011 - Nick Orange` / `TWB64 012 - Virtual Boy Ver.` / `TWB64 013 - Golden Wild` / `TWB64 014 - Builder Yellow` / `TWB64 015 - Classic Blurple` / `TWB64 016 - 765 Production Ver.` / `TWB64 017 - Superball Ivory` / `TWB64 018 - Crunchyroll Orange` / `TWB64 019 - Muse Pink` / `TWB64 020 - Nijigasaki Yellow` / `TWB64 021 - Gamate Ver.` / `TWB64 022 - Greenscale Ver.` / `TWB64 023 - Odyssey Gold` / `TWB64 024 - Super Saiyan God` / `TWB64 025 - Super Saiyan Blue` / `TWB64 026 - Bizarre Pink` / `TWB64 027 - Nintendo Switch Lite Ver.` / `TWB64 028 - Game.com Ver.` / `TWB64 029 - Sanrio Pink` / `TWB64 030 - BANDAI NAMCO Ver.` / `TWB64 031 - Cosmo Green` / `TWB64 032 - Wanda Pink` / `TWB64 033 - Link's Awakening DX Ver.` / `TWB64 034 - Travel Wood` / `TWB64 035 - Pokemon Ver.` / `TWB64 036 - Game Grump Orange` / `TWB64 037 - Scooby-Doo Mystery Ver.` / `TWB64 038 - Pokemon mini Ver.` / `TWB64 039 - Supervision Ver.` / `TWB64 040 - DMG Ver.` / `TWB64 041 - Pocket Ver.` / `TWB64 042 - Light Ver.` / `TWB64 043 - Miraitowa Blue` / `TWB64 044 - Someity Pink` / `TWB64 045 - Pikachu Yellow` / `TWB64 046 - Eevee Brown` / `TWB64 047 - Microvision Ver.` / `TWB64 048 - TI-83 Ver.` / `TWB64 049 - Aegis Cherry` / `TWB64 050 - Labo Fawn` / `TWB64 051 - MILLION LIVE GOLD!` / `TWB64 052 - Tokyo Midtown Ver.` / `TWB64 053 - VMU Ver.` / `TWB64 054 - Game Master Ver.` / `TWB64 055 - Android Green` / `TWB64 056 - Ticketmaster Azure` / `TWB64 057 - Google Red` / `TWB64 058 - Google Blue` / `TWB64 059 - Google Yellow` / `TWB64 060 - Google Green` / `TWB64 061 - WonderSwan Ver.` / `TWB64 062 - Neo Geo Pocket Ver.` / `TWB64 063 - Dew Green` / `TWB64 064 - Coca-Cola Red` / `TWB64 065 - GameKing Ver.` / `TWB64 066 - Do The Dew Ver.` / `TWB64 067 - Digivice Ver.` / `TWB64 068 - Bikini Bottom Ver.` / `TWB64 069 - Blossom Pink` / `TWB64 070 - Bubbles Blue` / `TWB64 071 - Buttercup Green` / `TWB64 072 - NASCAR Ver.` / `TWB64 073 - Lemon-Lime Green` / `TWB64 074 - Mega Man V Ver.` / `TWB64 075 - Tamagotchi Ver.` / `TWB64 076 - Phantom Red` / `TWB64 077 - Halloween Ver.` / `TWB64 078 - Christmas Ver.` / `TWB64 079 - Cardcaptor Pink` / `TWB64 080 - Pretty Guardian Gold` / `TWB64 081 - Camouflage Ver.` / `TWB64 082 - Legendary Super Saiyan` / `TWB64 083 - Super Saiyan Rose` / `TWB64 084 - Super Saiyan` / `TWB64 085 - Perfected Ultra Instinct` / `TWB64 086 - Saint Snow Red` / `TWB64 087 - Yellow Banana` / `TWB64 088 - Green Banana` / `TWB64 089 - Super Saiyan 3` / `TWB64 090 - Super Saiyan Blue Evolved` / `TWB64 091 - Pocket Tales Ver.` / `TWB64 092 - Investigation Yellow` / `TWB64 093 - S.E.E.S. Blue` / `TWB64 094 - Game Awards Cyan` / `TWB64 095 - Hokage Orange` / `TWB64 096 - Straw Hat Red` / `TWB64 097 - Sword Art Cyan` / `TWB64 098 - Deku Alpha Emerald` / `TWB64 099 - Blue Stripes Ver.` / `TWB64 100 - Stone Orange` |
| > TWB64 - Pack 2 Palette | `TWB64 101 - 765PRO Pink` ✅ / `TWB64 102 - CINDERELLA Blue` / `TWB64 103 - MILLION Yellow!` / `TWB64 104 - SideM Green` / `TWB64 105 - SHINY Sky Blue` / `TWB64 106 - Angry Volcano Ver.` / `TWB64 107 - Yo-kai Pink` / `TWB64 108 - Yo-kai Green` / `TWB64 109 - Yo-kai Blue` / `TWB64 110 - Yo-kai Purple` / `TWB64 111 - Aquatic Iro` / `TWB64 112 - Tea Midori` / `TWB64 113 - Sakura Pink` / `TWB64 114 - Wisteria Murasaki` / `TWB64 115 - Oni Aka` / `TWB64 116 - Golden Kiiro` / `TWB64 117 - Silver Shiro` / `TWB64 118 - Fruity Orange` / `TWB64 119 - AKB48 Pink` / `TWB64 120 - Miku Blue` / `TWB64 121 - Fairy Tail Red` / `TWB64 122 - Survey Corps Brown` / `TWB64 123 - Island Green` / `TWB64 124 - Mania Plus Green` / `TWB64 125 - Ninja Turtle Green` / `TWB64 126 - Slime Blue` / `TWB64 127 - Lime Midori` / `TWB64 128 - Ghostly Aoi` / `TWB64 129 - Retro Bogeda` / `TWB64 130 - Royal Blue` / `TWB64 131 - Neon Purple` / `TWB64 132 - Neon Orange` / `TWB64 133 - Moonlight Vision` / `TWB64 134 - Tokyo Red` / `TWB64 135 - Paris Gold` / `TWB64 136 - Beijing Blue` / `TWB64 137 - Pac-Man Yellow` / `TWB64 138 - Irish Green` / `TWB64 139 - Kakarot Orange` / `TWB64 140 - Dragon Ball Orange` / `TWB64 141 - Christmas Gold` / `TWB64 142 - Pepsi-Cola Blue` / `TWB64 143 - Bubblun Green` / `TWB64 144 - Bobblun Blue` / `TWB64 145 - Baja Blast Storm` / `TWB64 146 - Olympic Gold` / `TWB64 147 - Value Orange` / `TWB64 148 - Liella Purple!` / `TWB64 149 - Olympic Silver` / `TWB64 150 - Olympic Bronze` / `TWB64 151 - ANA Sky Blue` / `TWB64 152 - Nijigasaki Orange` / `TWB64 153 - HoloBlue` / `TWB64 154 - Wrestling Red` / `TWB64 155 - Yoshi Egg Green` / `TWB64 156 - Pokedex Red` / `TWB64 157 - Disney Dream Blue` / `TWB64 158 - Xbox Green` / `TWB64 159 - Sonic Mega Blue` / `TWB64 160 - G4 Orange` / `TWB64 161 - Scarlett Green` / `TWB64 162 - Glitchy Blue` / `TWB64 163 - Classic LCD` / `TWB64 164 - 3DS Virtual Console Ver.` / `TWB64 165 - PocketStation Ver.` / `TWB64 166 - Game and Gold` / `TWB64 167 - Smurfy Blue` / `TWB64 168 - Swampy Ogre Green` / `TWB64 169 - Sailor Spinach Green` / `TWB64 170 - Shenron Green` / `TWB64 171 - Berserk Blood` / `TWB64 172 - Super Star Pink` / `TWB64 173 - Gamebuino Classic Ver.` / `TWB64 174 - Barbie Pink` / `TWB64 175 - Star Command Green` / `TWB64 176 - Nokia 3310 Ver.` / `TWB64 177 - Clover Green` / `TWB64 178 - Crash Orange` / `TWB64 179 - Famicom Disk Yellow` / `TWB64 180 - Team Rocket Red` / `TWB64 181 - SEIKO Timer Yellow` / `TWB64 182 - PINK109` / `TWB64 183 - Doraemon Blue` / `TWB64 184 - Fury Blue` / `TWB64 185 - Rockstar Orange` / `TWB64 186 - Puyo Puyo Green` / `TWB64 187 - Susan G. Pink` / `TWB64 188 - Pizza Hut Red` / `TWB64 189 - Plumbob Green` / `TWB64 190 - Grand Ivory` / `TWB64 191 - Demon's Gold` / `TWB64 192 - SEGA Tokyo Blue` / `TWB64 193 - Champion Blue` / `TWB64 194 - DK Barrel Brown` / `TWB64 195 - Evangelion Green` / `TWB64 196 - Equestrian Purple` / `TWB64 197 - Autobot Red` / `TWB64 198 - Niconico Sea Green` / `TWB64 199 - Duracell Copper` / `TWB64 200 - TOKYO SKYTREE CLOUDY BLUE` | `gambatte_gb_palette_twb64_2` | `TWB64 101 - 765PRO Pink` / `TWB64 102 - CINDERELLA Blue` / `TWB64 103 - MILLION Yellow!` / `TWB64 104 - SideM Green` / `TWB64 105 - SHINY Sky Blue` / `TWB64 106 - Angry Volcano Ver.` / `TWB64 107 - Yo-kai Pink` / `TWB64 108 - Yo-kai Green` / `TWB64 109 - Yo-kai Blue` / `TWB64 110 - Yo-kai Purple` / `TWB64 111 - Aquatic Iro` / `TWB64 112 - Tea Midori` / `TWB64 113 - Sakura Pink` / `TWB64 114 - Wisteria Murasaki` / `TWB64 115 - Oni Aka` / `TWB64 116 - Golden Kiiro` / `TWB64 117 - Silver Shiro` / `TWB64 118 - Fruity Orange` / `TWB64 119 - AKB48 Pink` / `TWB64 120 - Miku Blue` / `TWB64 121 - Fairy Tail Red` / `TWB64 122 - Survey Corps Brown` / `TWB64 123 - Island Green` / `TWB64 124 - Mania Plus Green` / `TWB64 125 - Ninja Turtle Green` / `TWB64 126 - Slime Blue` / `TWB64 127 - Lime Midori` / `TWB64 128 - Ghostly Aoi` / `TWB64 129 - Retro Bogeda` / `TWB64 130 - Royal Blue` / `TWB64 131 - Neon Purple` / `TWB64 132 - Neon Orange` / `TWB64 133 - Moonlight Vision` / `TWB64 134 - Tokyo Red` / `TWB64 135 - Paris Gold` / `TWB64 136 - Beijing Blue` / `TWB64 137 - Pac-Man Yellow` / `TWB64 138 - Irish Green` / `TWB64 139 - Kakarot Orange` / `TWB64 140 - Dragon Ball Orange` / `TWB64 141 - Christmas Gold` / `TWB64 142 - Pepsi-Cola Blue` / `TWB64 143 - Bubblun Green` / `TWB64 144 - Bobblun Blue` / `TWB64 145 - Baja Blast Storm` / `TWB64 146 - Olympic Gold` / `TWB64 147 - Value Orange` / `TWB64 148 - Liella Purple!` / `TWB64 149 - Olympic Silver` / `TWB64 150 - Olympic Bronze` / `TWB64 151 - ANA Sky Blue` / `TWB64 152 - Nijigasaki Orange` / `TWB64 153 - HoloBlue` / `TWB64 154 - Wrestling Red` / `TWB64 155 - Yoshi Egg Green` / `TWB64 156 - Pokedex Red` / `TWB64 157 - Disney Dream Blue` / `TWB64 158 - Xbox Green` / `TWB64 159 - Sonic Mega Blue` / `TWB64 160 - G4 Orange` / `TWB64 161 - Scarlett Green` / `TWB64 162 - Glitchy Blue` / `TWB64 163 - Classic LCD` / `TWB64 164 - 3DS Virtual Console Ver.` / `TWB64 165 - PocketStation Ver.` / `TWB64 166 - Game and Gold` / `TWB64 167 - Smurfy Blue` / `TWB64 168 - Swampy Ogre Green` / `TWB64 169 - Sailor Spinach Green` / `TWB64 170 - Shenron Green` / `TWB64 171 - Berserk Blood` / `TWB64 172 - Super Star Pink` / `TWB64 173 - Gamebuino Classic Ver.` / `TWB64 174 - Barbie Pink` / `TWB64 175 - Star Command Green` / `TWB64 176 - Nokia 3310 Ver.` / `TWB64 177 - Clover Green` / `TWB64 178 - Crash Orange` / `TWB64 179 - Famicom Disk Yellow` / `TWB64 180 - Team Rocket Red` / `TWB64 181 - SEIKO Timer Yellow` / `TWB64 182 - PINK109` / `TWB64 183 - Doraemon Blue` / `TWB64 184 - Fury Blue` / `TWB64 185 - Rockstar Orange` / `TWB64 186 - Puyo Puyo Green` / `TWB64 187 - Susan G. Pink` / `TWB64 188 - Pizza Hut Red` / `TWB64 189 - Plumbob Green` / `TWB64 190 - Grand Ivory` / `TWB64 191 - Demon's Gold` / `TWB64 192 - SEGA Tokyo Blue` / `TWB64 193 - Champion Blue` / `TWB64 194 - DK Barrel Brown` / `TWB64 195 - Evangelion Green` / `TWB64 196 - Equestrian Purple` / `TWB64 197 - Autobot Red` / `TWB64 198 - Niconico Sea Green` / `TWB64 199 - Duracell Copper` / `TWB64 200 - TOKYO SKYTREE CLOUDY BLUE` |
| > PixelShift - Pack 1 Palette | `PixelShift 01 - Arctic Green` ✅ / `PixelShift 02 - Arduboy` / `PixelShift 03 - BGB 0.3 Emulator` / `PixelShift 04 - Camouflage` / `PixelShift 05 - Chocolate Bar` / `PixelShift 06 - CMYK` / `PixelShift 07 - Cotton Candy` / `PixelShift 08 - Easy Greens` / `PixelShift 09 - Gamate` / `PixelShift 10 - Game Boy Light` / `PixelShift 11 - Game Boy Pocket` / `PixelShift 12 - Game Boy Pocket Alt` / `PixelShift 13 - Game Pocket Computer` / `PixelShift 14 - Game & Watch Ball` / `PixelShift 15 - GB Backlight Blue` / `PixelShift 16 - GB Backlight Faded` / `PixelShift 17 - GB Backlight Orange` / `PixelShift 18 - GB Backlight White ` / `PixelShift 19 - GB Backlight Yellow Dark` / `PixelShift 20 - GB Bootleg` / `PixelShift 21 - GB Hunter` / `PixelShift 22 - GB Kiosk` / `PixelShift 23 - GB Kiosk 2` / `PixelShift 24 - GB New` / `PixelShift 25 - GB Nuked` / `PixelShift 26 - GB Old` / `PixelShift 27 - GBP Bivert` / `PixelShift 28 - GB Washed Yellow Backlight` / `PixelShift 29 - Ghost` / `PixelShift 30 - Glow In The Dark` / `PixelShift 31 - Gold Bar` / `PixelShift 32 - Grapefruit` / `PixelShift 33 - Gray Green Mix` / `PixelShift 34 - Missingno` / `PixelShift 35 - MS-Dos` / `PixelShift 36 - Newspaper` / `PixelShift 37 - Pip-Boy` / `PixelShift 38 - Pocket Girl` / `PixelShift 39 - Silhouette` / `PixelShift 40 - Sunburst` / `PixelShift 41 - Technicolor` / `PixelShift 42 - Tron` / `PixelShift 43 - Vaporwave` / `PixelShift 44 - Virtual Boy` / `PixelShift 45 - Wish` | `gambatte_gb_palette_pixelshift_1` | `PixelShift 01 - Arctic Green` / `PixelShift 02 - Arduboy` / `PixelShift 03 - BGB 0.3 Emulator` / `PixelShift 04 - Camouflage` / `PixelShift 05 - Chocolate Bar` / `PixelShift 06 - CMYK` / `PixelShift 07 - Cotton Candy` / `PixelShift 08 - Easy Greens` / `PixelShift 09 - Gamate` / `PixelShift 10 - Game Boy Light` / `PixelShift 11 - Game Boy Pocket` / `PixelShift 12 - Game Boy Pocket Alt` / `PixelShift 13 - Game Pocket Computer` / `PixelShift 14 - Game & Watch Ball` / `PixelShift 15 - GB Backlight Blue` / `PixelShift 16 - GB Backlight Faded` / `PixelShift 17 - GB Backlight Orange` / `PixelShift 18 - GB Backlight White ` / `PixelShift 19 - GB Backlight Yellow Dark` / `PixelShift 20 - GB Bootleg` / `PixelShift 21 - GB Hunter` / `PixelShift 22 - GB Kiosk` / `PixelShift 23 - GB Kiosk 2` / `PixelShift 24 - GB New` / `PixelShift 25 - GB Nuked` / `PixelShift 26 - GB Old` / `PixelShift 27 - GBP Bivert` / `PixelShift 28 - GB Washed Yellow Backlight` / `PixelShift 29 - Ghost` / `PixelShift 30 - Glow In The Dark` / `PixelShift 31 - Gold Bar` / `PixelShift 32 - Grapefruit` / `PixelShift 33 - Gray Green Mix` / `PixelShift 34 - Missingno` / `PixelShift 35 - MS-Dos` / `PixelShift 36 - Newspaper` / `PixelShift 37 - Pip-Boy` / `PixelShift 38 - Pocket Girl` / `PixelShift 39 - Silhouette` / `PixelShift 40 - Sunburst` / `PixelShift 41 - Technicolor` / `PixelShift 42 - Tron` / `PixelShift 43 - Vaporwave` / `PixelShift 44 - Virtual Boy` / `PixelShift 45 - Wish` |
| Color Correction | `GBC Only` ✅ / `Always` / `Disabled` | `gambatte_gbc_color_correction` | `GBC only` / `always` / `disabled` |
| Color Correction Mode | `Accurate` ✅ / `Fast` | `gambatte_gbc_color_correction_mode` | `accurate` / `fast` |
| Color Correction - Frontlight Position | `Central` ✅ / `Above Screen` / `Below Screen` | `gambatte_gbc_frontlight_position` | `central` / `above screen` / `below screen` |
| Dark Filter Level (%) | `0` ✅ / `5` / `10` / `15` / `20` / `25` / `30` / `35` / `40` / `45` / `50` | `gambatte_dark_filter_level` | `0` / `5` / `10` / `15` / `20` / `25` / `30` / `35` / `40` / `45` / `50` |
| Interframe Blending | `Disabled` ✅ / `Simple (Accurate)` / `Simple (Fast)` / `LCD Ghosting (Accurate)` / `LCD Ghosting (Fast)` | `gambatte_mix_frames` | `disabled` / `mix` / `mix_fast` / `lcd_ghosting` / `lcd_ghosting_fast` |
| Audio Resampler | `Sinc` ✅ / `Cosinus` | `gambatte_audio_resampler` | `sinc` / `cc` |
| Emulated Hardware (restart) | `Auto` ✅ / `GB` / `GBC` / `GBA` | `gambatte_gb_hwmode` | `Auto` / `GB` / `GBC` / `GBA` |
| Use Official Bootloader (restart) | `Enabled` ✅ / `Disabled` | `gambatte_gb_bootloader` | `enabled` / `disabled` |
| Allow Opposing Directions | `Disabled` ✅ / `Enabled` | `gambatte_up_down_allowed` | `disabled` / `enabled` |
| Turbo Button Delay | `4` ✅ / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `21` / `22` / `23` / `24` / `25` / `26` / `27` / `28` / `29` / `30` / `31` / `32` / `33` / `34` / `35` / `36` / `37` / `38` / `39` / `40` / `41` / `42` / `43` / `44` / `45` / `46` / `47` / `48` / `49` / `50` / `51` / `52` / `53` / `54` / `55` / `56` / `57` / `58` / `59` / `60` / `61` / `62` / `63` / `64` / `65` / `66` / `67` / `68` / `69` / `70` / `71` / `72` / `73` / `74` / `75` / `76` / `77` / `78` / `79` / `80` / `81` / `82` / `83` / `84` / `85` / `86` / `87` / `88` / `89` / `90` / `91` / `92` / `93` / `94` / `95` / `96` / `97` / `98` / `99` / `100` / `101` / `102` / `103` / `104` / `105` / `106` / `107` / `108` / `109` / `110` / `111` / `112` / `113` / `114` / `115` / `116` / `117` / `118` / `119` / `120` | `gambatte_turbo_period` | `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `21` / `22` / `23` / `24` / `25` / `26` / `27` / `28` / `29` / `30` / `31` / `32` / `33` / `34` / `35` / `36` / `37` / `38` / `39` / `40` / `41` / `42` / `43` / `44` / `45` / `46` / `47` / `48` / `49` / `50` / `51` / `52` / `53` / `54` / `55` / `56` / `57` / `58` / `59` / `60` / `61` / `62` / `63` / `64` / `65` / `66` / `67` / `68` / `69` / `70` / `71` / `72` / `73` / `74` / `75` / `76` / `77` / `78` / `79` / `80` / `81` / `82` / `83` / `84` / `85` / `86` / `87` / `88` / `89` / `90` / `91` / `92` / `93` / `94` / `95` / `96` / `97` / `98` / `99` / `100` / `101` / `102` / `103` / `104` / `105` / `106` / `107` / `108` / `109` / `110` / `111` / `112` / `113` / `114` / `115` / `116` / `117` / `118` / `119` / `120` |
| controller Rumble Strength | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` ✅ | `gambatte_rumble_level` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` |
| Game Boy Link Mode | `Not Connected` ✅ / `Network Server` / `Network Client` | `gambatte_gb_link_mode` | `Not Connected` / `Network Server` / `Network Client` |
| Network Link Port | `56400` ✅ / `56401` / `56402` / `56403` / `56404` / `56405` / `56406` / `56407` / `56408` / `56409` / `56410` / `56411` / `56412` / `56413` / `56414` / `56415` / `56416` / `56417` / `56418` / `56419` / `56420` | `gambatte_gb_link_network_port` | `56400` / `56401` / `56402` / `56403` / `56404` / `56405` / `56406` / `56407` / `56408` / `56409` / `56410` / `56411` / `56412` / `56413` / `56414` / `56415` / `56416` / `56417` / `56418` / `56419` / `56420` |
| Network Link Server Address Pt. 01: \_\_\_.\_\_\_.\_\_\_.\_\_x | `0` ✅ / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` | `gambatte_gb_link_network_server_ip_1` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` |
| Network Link Server Address Pt. 02: \_\_\_.\_\_\_.\_\_\_.\_\_x | `0` ✅ / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` | `gambatte_gb_link_network_server_ip_2` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` |
| Network Link Server Address Pt. 03: \_\_\_.\_\_\_.\_\_\_.\_\_x | `0` ✅ / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` | `gambatte_gb_link_network_server_ip_3` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` |
| Network Link Server Address Pt. 04: \_\_\_.\_\_\_.\_\_\_.\_\_x | `0` ✅ / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` | `gambatte_gb_link_network_server_ip_4` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` |
| Network Link Server Address Pt. 05: \_\_\_.\_\_\_.\_\_\_.\_\_x | `0` ✅ / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` | `gambatte_gb_link_network_server_ip_5` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` |
| Network Link Server Address Pt. 06: \_\_\_.\_\_\_.\_\_\_.\_\_x | `0` ✅ / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` | `gambatte_gb_link_network_server_ip_6` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` |
| Network Link Server Address Pt. 07: \_\_\_.\_\_\_.\_\_\_.\_\_x | `0` ✅ / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` | `gambatte_gb_link_network_server_ip_7` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` |
| Network Link Server Address Pt. 08: \_\_\_.\_\_\_.\_\_\_.\_\_x | `0` ✅ / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` | `gambatte_gb_link_network_server_ip_8` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` |
| Network Link Server Address Pt. 09: \_\_\_.\_\_\_.\_\_\_.\_\_x | `0` ✅ / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` | `gambatte_gb_link_network_server_ip_9` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` |
| Network Link Server Address Pt. 10: \_\_\_.\_\_\_.\_\_\_.\_\_x | `0` ✅ / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` | `gambatte_gb_link_network_server_ip_10` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` |
| Network Link Server Address Pt. 11: \_\_\_.\_\_\_.\_\_\_.\_\_x | `0` ✅ / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` | `gambatte_gb_link_network_server_ip_11` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` |
| Network Link Server Address Pt. 12: \_\_\_.\_\_\_.\_\_\_.\_\_x | `0` ✅ / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` | `gambatte_gb_link_network_server_ip_12` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` |

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/libretro/gambatte-libretro/](https://github.com/libretro/gambatte-libretro)
* **Libretro documentation**: [https://docs.libretro.com/library/gambatte/](https://docs.libretro.com/library/gambatte/)
* **Official source code**: [https://github.com/sinamas/gambatte](https://github.com/sinamas/gambatte)
* **Old standalone source code**: [https://sourceforge.net/projects/gambatte/files/gambatte/](https://sourceforge.net/projects/gambatte/files/gambatte/)