---
title: Libretro Swanstation
description: 
published: true
date: 2021-10-17T21:35:01.117Z
tags: libretro, ps1, playstation 1, swanstation
editor: markdown
dateCreated: 2021-08-01T23:50:23.328Z
---

 

## ![](/emulators/license.svg) License

This core is under [**GPLv3**](https://github.com/libretro/duckstation/blob/master/LICENSE) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 | 
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ❌ | ❌ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Features

| Feature | Supported |
| :---: | :---: |
|  |  |

## ![](/emulators/bios.svg) BIOS

### List of mandatory bios

| Filename | Description | MD5 | Provided |
| :---: | :---: | :---: | :---: |
| scph5500.bin | Japanese PS1 BIOS - required for japanese games | 8dd7d5296a650fac7319bce665a6a53c | ❌ |
| scph5501.bin | American PS1 BIOS - required for american games | 490f666e1afb15b7362b406ed1cea246 | ❌ |
| scph5502.bin | European PS1 BIOS - required for european games | 32736f17079d0b2b7024407c39bd3050 | ❌ |

### Location

Put the bios like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 🗒 **scph5500.bin**
┃ ┃ ┃ ┣ 🗒 **scph5501.bin**
┃ ┃ ┃ ┣ 🗒 **scph5502.bin**

### List of optional bios

| Filename | Description | MD5 | Provided |
| :---: | :---: | :---: | :---: |
| ps1_rom.bin | BIOS PS1 - Provided with latest PS3 firmware | 81bbe60ba7a3d1cea1d48c14cbcc647b | ❌ |

### Location

Put the bios like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 🗒 **ps1_rom.bin**

## ![](/emulators/isos.svg) Isos

### Supported extensions

Isos must have the extension:

* .bin/.cue
* .img/.ccd/.sub
* .chd

### Location

Put the isos like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 psx
┃ ┃ ┃ ┃ ┣ 🗒 **game.bin**
┃ ┃ ┃ ┃ ┣ 🗒 **game.cue**

>The isos using **Redump** format are highly recommended.
{.is-success}

>For more information about isos, go to [this tutorial](./../../../tutorials/games/generalities/isos-and-roms)!
{.is-info}

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Accessing to options

You can configure various options in two different ways.

* Via RetroArch menu:

┣ 📁 RetroArch Menu
┃ ┣ 📁 Core options
┃ ┃ ┣ 🧩 Name_option

* Via the file `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core options

| Options | Options values | Variables (for configuration override) | Variables values |
| --- | --- | --- | --- |
| Console Region | `Auto-Detect` ✅ / `NTSC-J (Japan)` / `NTSC-U (US, Canada)` / `PAL (Europe, Australia)` | `duckstation_Console.Region` | `Auto` / `NTSC-J` / `NTSC-U` / `PAL` |
| Fast Boot | `Enabled` / `Disabled` ✅ | `duckstation_BIOS.PatchFastBoot` | `enabled` / `disabled` |
| CD-ROM Region Check | `Enabled` / `Disabled` ✅ | `duckstation_CDROM.RegionCheck` | `enabled` / `disabled` |
| CD-ROM Read Thread | `Enabled` ✅ / `Disabled` | `duckstation_CDROM.ReadThread` | `enabled` / `disabled` |
| Preload CD-ROM Image To RAM | `Enabled` / `Disabled` ✅ | `duckstation_CDROM.LoadImageToRAM` | `enabled` / `disabled` |
| Mute CD Audio | `Enabled` / `Disabled` ✅ | `duckstation_CDROM.MuteCDAudio` | `enabled` / `disabled` |
| CD-ROM Read Speedup | `None (Double Speed)` ✅ / `2x (Quad Speed)` / `3x (6x Speed)` / `4x (8x Speed)` / `5x (10x Speed)` / `6x (12x Speed)` / `7x (14x Speed)` / `8x (16x Speed)` / `9x (18x Speed)` / `10x (20x Speed)` | `duckstation_CDROM.ReadSpeedup` | `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` |
| CPU Execution Mode | `Interpreter` / `Cached Interpreter` / `Recompiler` ✅ | `duckstation_CPU.ExecutionMode` | `Interpreter` / `CachedInterpreter` / `Recompiler` |
| CPU Overclocking | `25%` / `50%` / `100% (Default)` ✅ / `125%` / `150%` / `175%` / `200%` / `225%` / `250%` / `275%` / `300%` / `350%` / `400%` / `450%` / `500%` / `600%` / `700%` / `800%` / `900%` / `1000%` | `duckstation_CPU.Overclock` | `25` / `50` / `100` / `125` / `150` / `175` / `200` / `225` / `250` / `275` / `300` / `350` / `400` / `450` / `500` / `600` / `700` / `800` / `900` / `1000` |
| GPU Renderer | `Hardware (Auto)` / `Harware (OpenGL)` ✅ / `Hardware (Vulkan)` / `Software` | `duckstation_GPU.Renderer` | `Auto` / `OpenGL` / `Vulkan` / `Software` |
| Threaded Rendering (Software) | `Enabled` ✅ / `Disabled` | `duckstation_GPU.UseThread` | `enabled` / `disabled` |
| Internal Resolution Scale | `1x` ✅ / `2x` / `3x` / `4x` / `5x` / `6x` / `7x` / `8x` / `9x` / `10x` / `11x` / `12x` / `13x` / `14x` / `15x` / `16x` | `duckstation_GPU.ResolutionScale` | `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` |
| Multisample Antialiasing | `Disabled` ✅ / `2x MSAA` / `4x MSAA` / `8x MSAA` / `16x MSAA` / `32x MSAA` / `2x SSAA` / `4x SSAA` / `8x SSAA` / `16x SSAA` / `32x SSAA` | `duckstation_GPU.MSAA` | `1` / `2` / `4` / `8` / `16` / `32` / `2-ssaa` / `4-ssaa` / `8-ssaa` / `16-ssaa` / `32-ssaa` |
| True Color Rendering | `Enabled` / `Disabled` ✅ | `duckstation_GPU.TrueColor` | `enabled` / `disabled` |
| Scaled Dithering | `Enabled` ✅ / `Disabled` | `duckstation_GPU.ScaledDithering` | `enabled` / `disabled` |
| Disable Interlacing | `Enabled` / `Disabled` ✅ | `duckstation_GPU.DisableInterlacing` | `enabled` / `disabled` |
| Force NTSC Timings | `Enabled` / `Disabled` ✅ | `duckstation_GPU.ForceNTSCTimings` | `enabled` / `disabled` |
| Force 4:3 For 24-Bit Display | `Enabled` / `Disabled` ✅ | `duckstation_Display.Force4_3For24Bit` | `enabled` / `disabled` |
| Chroma Smoothing For 24-Bit Display | `Enabled` / `Disabled` ✅ | `duckstation_GPU.ChromaSmoothingFor24Bit` | `enabled` / `disabled` |
| Texture Filtering | `Nearest-Neighbor` ✅ / `Bilinear` / `Bilinear (No Edge Blending)` / `JINC2` / `JINC2 (No Edge Blending)` / `xBR` / `xBR (No Edge Blending)` | `duckstation_GPU.TextureFilter` | `Nearest` / `Bilinear` / `BilinearBinAlpha` / `JINC2` / `JINC2BinAlpha` / `xBR` / `xBRBinAlpha` |
| Widescreen Hack | `Enabled` / `Disabled` ✅ | `duckstation_GPU.WidescreenHack` | `enabled` / `disabled` |
| PGXP Geometry Correction | `Enabled` / `Disabled` ✅ | `duckstation_GPU.PGXPEnable` | `enabled` / `disabled` |
| PGXP Culling Correction | `Enabled` ✅ / `Disabled` | `duckstation_GPU.PGXPCulling` | `enabled` / `disabled` |
| PGXP Texture Correction | `Enabled` ✅ / `Disabled` | `duckstation_GPU.PGXPTextureCorrection` | `enabled` / `disabled` |
| PGXP Depth Buffer | `Enabled` / `Disabled` ✅ | `duckstation_GPU.PGXPDepthBuffer` | `enabled` / `disabled` |
| PGXP Vertex Cache | `Enabled` / `Disabled` ✅ | `duckstation_GPU.PGXPVertexCache` | `enabled` / `disabled` |
| PGXP CPU Mode | `Enabled` / `Disabled` ✅ | `duckstation_GPU.PGXPCPU` | `enabled` / `disabled` |
| Aspect Ratio | `Auto (Game Native)` ✅ / `4:3` / `16:9` / `16:10` / `19:9` / `21:9` / `32:9` / `8:7` / `5:4` / `3:2` / `2:1 (VRAM 1:1)` / `1:1` / `PAR 1:1` | `duckstation_Display.AspectRation` | `Auto` / `4:3` / `16:9` / `16:10` / `19:9` / `21:9` / `32:9` / `8:7` / `5:4` / `3:2` / `2:1 (VRAM 1:1)` / `1:1` / `PAR 1:1` |
| Crop Mode | `None` / `Only Overscan Area` ✅ / `All Borders` | `duckstation_Display.CropMode` | `None` / `Overscan` / `Borders` |
| Downsampling | `Disabled` ✅ / `Box (Downsample 3D/Smooth All)` / `Adaptive (Preserve 3D/Smooth 2D)` | `duckstation_GPU.DownsampleMode` | `Disabled` / `Box` / `Adaptive` |
| Load Devices From Save States | `Enabled` / `Disabled` ✅ | `duckstation_Main.LoadDevicesFromSaveStates` | `enabled` / `disabled` |
| Memory Card 1 Type | `No Memory Card` / `Let the libretro frontend handle the Card` ✅ / `Shared Between All Games` / `Separate Card Per Game (Game Code)` / `Separate Card Per Game (Game Title)` | `duckstation_MemoryCards.Card1Type` | `None` / `Libretro` / `Shared` / `PerGame` / `PerGameTitle` |
| Memory Card 2 Type | `No Memory Card` ✅ / `Let the libretro frontend handle the Card` / `Shared Between All Games` / `Separate Card Per Game (Game Code)` / `Separate Card Per Game (Game Title)` | `duckstation_MemoryCards.Card2Type` | `None` / `Libretro` / `Shared` / `PerGame` / `PerGameTitle` |
| Use Single Card For Playlist | `Enabled` ✅ / `Disabled` | `duckstation_MemoryCards.UsePlaylistTitle` | `enabled` / `disabled` |
| Multitap Mode | `Disabled` ✅ / `Enable on Port 1 Only` / `Enable on Port 2 Only` / `Enable on Ports 1 and 2` | `duckstation_ControllerPorts.MultitapMode` | `Disabled` / `Port1Only` / `Port2Only` / `BothPorts` |
| Controller 1 Type | `None` / `Digital Controller` ✅ / `Analog Controller (DualShock)` | `duckstation_Controller1.Type` | `None` / `DigitalController` / `AnalogController` |
| Controller 1 Force Analog Mode On Reset | `Enabled` / `Disabled` ✅ | `duckstation_Controller1.ForceAnalogOnReset` | `enabled` / `disabled` |
| Controller 1 Use Analog Sticks for D-Pad in Digital Mode | `Enabled` / `Disabled` ✅ | `duckstation_Controller1.AnalogDPadInDigitalMode` | `enabled` / `disabled` |
| Controller 1 Analog Axis Scale | `1.00` ✅ / `1.40` | `duckstation_Controller1.AxisScale` | `1.00f` / `1.40f` |
| Controller 2 Type | `None` ✅ / `Digital Controller` / `Analog Controller (DualShock)` | `duckstation_Controller2.Type` | `None` / `DigitalController` / `AnalogController` |
| Controller 2 Force Analog Mode On Reset | `Enabled` / `Disabled` ✅ | `duckstation_Controller2.ForceAnalogOnReset` | `enabled` / `disabled` |
| Controller 2 Use Analog Sticks for D-Pad in Digital Mode | `Enabled` / `Disabled` ✅ | `duckstation_Controller2.AnalogDPadInDigitalMode` | `enabled` / `disabled` |
| Controller 2 Analog Axis Scale | `1.00` ✅ / `1.40` | `duckstation_Controller2.AxisScale` | `1.00f` / `1.40f` |
| Controller 3 Type | `None` ✅ / `Digital Controller` / `Analog Controller (DualShock)` | `duckstation_Controller3.Type` | `None` / `DigitalController` / `AnalogController` |
| Controller 3 Force Analog Mode On Reset | `Enabled` / `Disabled` ✅ | `duckstation_Controller3.ForceAnalogOnReset` | `enabled` / `disabled` |
| Controller 3 Use Analog Sticks for D-Pad in Digital Mode | `Enabled` / `Disabled` ✅ | `duckstation_Controller3.AnalogDPadInDigitalMode` | `enabled` / `disabled` |
| Controller 3 Analog Axis Scale | `1.00` ✅ / `1.40` | `duckstation_Controller3.AxisScale` | `1.00f` / `1.40f` |
| Controller 4 Type | `None` ✅ / `Digital Controller` / `Analog Controller (DualShock)` | `duckstation_Controller4.Type` | `None` / `DigitalController` / `AnalogController` |
| Controller 4 Force Analog Mode On Reset | `Enabled` / `Disabled` ✅ | `duckstation_Controller4.ForceAnalogOnReset` | `enabled` / `disabled` |
| Controller 4 Use Analog Sticks for D-Pad in Digital Mode | `Enabled` / `Disabled` ✅ | `duckstation_Controller4.AnalogDPadInDigitalMode` | `enabled` / `disabled` |
| Controller 4 Analog Axis Scale | `1.00` ✅ / `1.40` | `duckstation_Controller4.AxisScale` | `1.00f` / `1.40f` |
| Display OSD Messages | `Enabled` ✅ / `Disabled` | `duckstation_Display.ShowOSDMessages` | `enabled` / `disabled` |
| Apply Compatibility Settings | `Enabled` ✅ / `Disabled` | `duckstation_Main.ApplyGameSettings` | `enabled` / `disabled` |
| Log Level | `None` / `Error` / `Warning` / `Performance` / `Success` / `Information` ✅ / `Developer` / `Profile` / `Debug` / `Trace` | `duckstation_Logging.LogLevel` | `None` / `Error` / `Warning` / `Performance` / `Success` / `Information` / `Developer` / `Profile` / `Debug` / `Trace` |
| CPU Recompiler ICache | `Enabled` / `Disabled` ✅ | `duckstation_CPU.RecompilerICache` | `enabled` / `disabled` |
| CPU Recompiler Fast Memory Access | `Disabled (Slowest)` / `MMAP (Hardware, Fastest, 64-Bit Only)` / `LUT (Faster)` ✅ | `duckstation_CPU.FastmemMode` | `Disabled` / `MMAP` / `LUT` |
| Display Active Start Offset | `-30` / `-29` / `-28` / `-27` / `-26` / `-25` / `-24` / `-23` / `-22` / `-21` / `-20` / `-19` / `-18` / `-17` / `-16` / `-15` / `-14` / `-13` / `-12` / `-11` / `-10` / `-9` / `-8` / `-7` / `-6` / `-5` / `-4` / `-3` / `-2` / `-1` / `0` ✅ / `+1` / `+2` / `+3` / `+4` / `+5` / `+6` / `+7` / `+8` / `+9` / `+10` / `+11` / `+12` / `+13` / `+14` / `+15` / `+16` / `+17` / `+18` / `+19` / `+20` / `+21` / `+22` / `+23` / `+24` / `+25` / `+26` / `+27` / `+28` / `+29` / `+30` | `duckstation_Display.ActiveStartOffset` | `-30` / `-29` / `-28` / `-27` / `-26` / `-25` / `-24` / `-23` / `-22` / `-21` / `-20` / `-19` / `-18` / `-17` / `-16` / `-15` / `-14` / `-13` / `-12` / `-11` / `-10` / `-9` / `-8` / `-7` / `-6` / `-5` / `-4` / `-3` / `-2` / `-1` / `0` / `+1` / `+2` / `+3` / `+4` / `+5` / `+6` / `+7` / `+8` / `+9` / `+10` / `+11` / `+12` / `+13` / `+14` / `+15` / `+16` / `+17` / `+18` / `+19` / `+20` / `+21` / `+22` / `+23` / `+24` / `+25` / `+26` / `+27` / `+28` / `+29` / `+30` |
| Display Active End Offset | `-30` / `-29` / `-28` / `-27` / `-26` / `-25` / `-24` / `-23` / `-22` / `-21` / `-20` / `-19` / `-18` / `-17` / `-16` / `-15` / `-14` / `-13` / `-12` / `-11` / `-10` / `-9` / `-8` / `-7` / `-6` / `-5` / `-4` / `-3` / `-2` / `-1` / `0` ✅ / `+1` / `+2` / `+3` / `+4` / `+5` / `+6` / `+7` / `+8` / `+9` / `+10` / `+11` / `+12` / `+13` / `+14` / `+15` / `+16` / `+17` / `+18` / `+19` / `+20` / `+21` / `+22` / `+23` / `+24` / `+25` / `+26` / `+27` / `+28` / `+29` / `+30` | `duckstation_Display.ActiveEndOffset` | `-30` / `-29` / `-28` / `-27` / `-26` / `-25` / `-24` / `-23` / `-22` / `-21` / `-20` / `-19` / `-18` / `-17` / `-16` / `-15` / `-14` / `-13` / `-12` / `-11` / `-10` / `-9` / `-8` / `-7` / `-6` / `-5` / `-4` / `-3` / `-2` / `-1` / `0` / `+1` / `+2` / `+3` / `+4` / `+5` / `+6` / `+7` / `+8` / `+9` / `+10` / `+11` / `+12` / `+13` / `+14` / `+15` / `+16` / `+17` / `+18` / `+19` / `+20` / `+21` / `+22` / `+23` / `+24` / `+25` / `+26` / `+27` / `+28` / `+29` / `+30` |
| Display Line Start Offset | `-30` / `-29` / `-28` / `-27` / `-26` / `-25` / `-24` / `-23` / `-22` / `-21` / `-20` / `-19` / `-18` / `-17` / `-16` / `-15` / `-14` / `-13` / `-12` / `-11` / `-10` / `-9` / `-8` / `-7` / `-6` / `-5` / `-4` / `-3` / `-2` / `-1` / `0` ✅ / `+1` / `+2` / `+3` / `+4` / `+5` / `+6` / `+7` / `+8` / `+9` / `+10` / `+11` / `+12` / `+13` / `+14` / `+15` / `+16` / `+17` / `+18` / `+19` / `+20` / `+21` / `+22` / `+23` / `+24` / `+25` / `+26` / `+27` / `+28` / `+29` / `+30` | `duckstation_Display.LineStartOffset` | `-30` / `-29` / `-28` / `-27` / `-26` / `-25` / `-24` / `-23` / `-22` / `-21` / `-20` / `-19` / `-18` / `-17` / `-16` / `-15` / `-14` / `-13` / `-12` / `-11` / `-10` / `-9` / `-8` / `-7` / `-6` / `-5` / `-4` / `-3` / `-2` / `-1` / `0` / `+1` / `+2` / `+3` / `+4` / `+5` / `+6` / `+7` / `+8` / `+9` / `+10` / `+11` / `+12` / `+13` / `+14` / `+15` / `+16` / `+17` / `+18` / `+19` / `+20` / `+21` / `+22` / `+23` / `+24` / `+25` / `+26` / `+27` / `+28` / `+29` / `+30` |
| Display Line End Offset | `-30` / `-29` / `-28` / `-27` / `-26` / `-25` / `-24` / `-23` / `-22` / `-21` / `-20` / `-19` / `-18` / `-17` / `-16` / `-15` / `-14` / `-13` / `-12` / `-11` / `-10` / `-9` / `-8` / `-7` / `-6` / `-5` / `-4` / `-3` / `-2` / `-1` / `0` ✅ / `+1` / `+2` / `+3` / `+4` / `+5` / `+6` / `+7` / `+8` / `+9` / `+10` / `+11` / `+12` / `+13` / `+14` / `+15` / `+16` / `+17` / `+18` / `+19` / `+20` / `+21` / `+22` / `+23` / `+24` / `+25` / `+26` / `+27` / `+28` / `+29` / `+30` | `duckstation_Display.LineEndOffset` | `-30` / `-29` / `-28` / `-27` / `-26` / `-25` / `-24` / `-23` / `-22` / `-21` / `-20` / `-19` / `-18` / `-17` / `-16` / `-15` / `-14` / `-13` / `-12` / `-11` / `-10` / `-9` / `-8` / `-7` / `-6` / `-5` / `-4` / `-3` / `-2` / `-1` / `0` / `+1` / `+2` / `+3` / `+4` / `+5` / `+6` / `+7` / `+8` / `+9` / `+10` / `+11` / `+12` / `+13` / `+14` / `+15` / `+16` / `+17` / `+18` / `+19` / `+20` / `+21` / `+22` / `+23` / `+24` / `+25` / `+26` / `+27` / `+28` / `+29` / `+30` |
| PGXP Preserve Projection Precision | `Enabled` / `Disabled` ✅ | `duckstation_GPU.PGXPPreserveProjFP` | `enabled` / `disabled` |
| PGXP Geometry Tolerance | `None` ✅ / `0.5 pixels` / `1.0 pixels` / `1.5 pixels` / `2.0 pixels` / `2.5 pixels` / `3.0 pixels` / `3.5 pixels` / `4.0 pixels` / `4.5 pixels` / `5.0 pixels` / `5.5 pixels` / `6.0 pixels` / `6.5 pixels` / `7.0 pixels` / `7.5 pixels` / `8.0 pixels` / `8.5 pixels` / `9.0 pixels` / `9.0 pixels` / `10.0 pixels` | `duckstation_GPU.PGXPTolerance` | `-1.0` / `0.5` / `1.0` / `1.5` / `2.0` / `2.5` / `3.0` / `3.5` / `4.0` / `4.5` / `5.0` / `5.5` / `6.0` / `6.5` / `7.0` / `7.5` / `8.0` / `8.5` / `9.0` / `9.0` / `10.0` |
| Internal Run-Ahead | `0 Frames (Disabled)` ✅ / `1 Frame` / `2 Frames` / `3 Frames` / `4 Frames` / `5 Frames` / `6 Frames` / `7 Frames` / `8 Frames` / `9 Frames` / `10 Frames` | `duckstation_Main.RunaheadFrameCount` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` |

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/libretro/duckstation/](https://github.com/libretro/duckstation/)