---
title: 🎞️ Vídeo
description: 
published: true
date: 2021-12-22T15:24:40.484Z
tags: tutorial, video, tutoriais, vídeo
editor: markdown
dateCreated: 2021-12-22T15:24:40.484Z
---

Se você está aqui, é porque está procurando um tutorial sobre tudo relacionado a vídeo!

Aqui estão as categorias disponíveis:

[CRT](crt)
[Configuração de exibição](display-configuration)
[HDMI](hdmi)
[LCD](lcd)
[TFT](tft)