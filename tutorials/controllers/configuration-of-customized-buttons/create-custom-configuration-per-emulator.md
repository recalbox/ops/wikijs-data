---
title: Créer une configuration personnalisée par émulateur
description: 
published: true
date: 2021-09-04T22:32:44.352Z
tags: configuration, personnalisée, émulateur
editor: markdown
dateCreated: 2021-05-21T08:34:26.990Z
---

Vous avez la possibilité de créer votre propre configuration custom pour chaque émulateur sur Recalbox.

## RetroArch

La configuration de RetroArch peut être crée à la main en appuyant votre propre configuration sur le standard [retroarch.cfg](https://github.com/libretro/RetroArch/blob/master/retroarch.cfg) ou en le modifiant dans le menu de RetroArch. Vous devrez alors créer une nouvelle configuration et paramétrer le nouveau fichier de config `recalbox.conf` afin qu'il soit chargé par votre émulateur.

### En utilisant le menu de RetroArch

Partons sur l'exemple de créer un fichier de config spécifique pour la Neo-geo.

* Lancez un jeu de l'émulateur que vous souhaitez personnaliser (ici neogeo).
* Entrez dans le menu de RetroArch (`Hotkey + B`).
* Modifiez toutes les configurations comme vous le souhaitez.
* Retournez dans la première entrée du menu de RetroArch (`Menu principal`) et allez dans `Fichiers de configuration` > `Sauvegarder une nouvelle configuration` (le nom du fichier de config devra ressembler à `fbneo_libretro.cfg`, avec le nom du core que vous être en train d'utiliser).
* En vous connectant via [SSH](./../../../tutorials/system/access/root-access-terminal-cli), modifiez le nom du fichier de configuration pour quelque chose de plus simple à se rappeler, par exemple :

```shell
mv /recalbox/share/system/configs/retroarch/fba_libretro.cfg /recalbox/share/system/configs/retroarch/inputs/neogeo_custom.cfg
```

* Ajoutez la ligne suivante dans le fichier `recalbox.conf` :

```ini
neogeo.configfile=/recalbox/share/system/configs/retroarch/inputs/neogeo_custom.cfg
```

### Inputs

#### Modification manuelle

Vous pouvez ajouter des fichiers de configuration spécifiques au système et/ou au jeu. Il y a bien sûr une priorité si un même paramètre apparaît dans chaque fichier de configuration. Ainsi, Jeu > Système > RetroArch, ce qui signifie qu'un paramètre existant dans un fichier .cfg de jeu sera celui utilisé sur un système ou le fichier de RetroArch par défaut.

Voici les fichiers que vous pouvez modifier (ou créer s'ils n'existent pas) :

*  `~/configs/retroarch/<system>.cfg` où `<système>` est le nom du système tel qu'il apparaît dans le dossier `<roms>`. Par exemple : `~/configs/retroarch/snes.cfg`.
*  `~/configs/retroarch/<system>/<romname>.cfg` où `<system>` est le nom du système tel qu'il apparaît dans le dossier `roms` , et `<romname>` est le nom exact de la rom avec l'extension du fichier. Par exemple : `~/configs/retroarch/snes/mario.zip.cfg`

## piFBA

>Cette partie est à rédiger. Si vous êtes au courant du fonctionnement de piFBA, vous pouvez ajouter votre contenu ici directement.