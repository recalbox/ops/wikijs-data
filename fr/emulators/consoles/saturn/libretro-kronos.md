---
title: Libretro Kronos
description: 
published: true
date: 2024-06-22T10:07:18.817Z
tags: libretro, saturn, kronos
editor: markdown
dateCreated: 2021-05-21T08:26:12.853Z
---

**Libretro Kronos** est un port de l'émulateur **Yabause** pour Libretro prenant en charge la Sega Saturn.

## ![](/emulators/license.svg) Licence

Ce core est sous licence [**GPLv2**](https://github.com/libretro/yabause/blob/master/yabause/COPYING).

## ![](/emulators/compatibility.png) Compatibilité

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ❌ | ❌ | ❌ | ❌ | ✅ | ❌ | ✅ | ❌ |

## ![](/emulators/features.png) Fonctionnalités

| Fonctionnalité | Supporté |
| :---: | :---: |
| Redémarrage  | ✔ |
| Captures d'écran | ✔ |
| Sauvegardes | ✔ |
| Sauvegardes instantanées | ✔ |
| Options du core | ✔ |
| RetroAchievements | ✔ |
| Cheats RetroArch | ✔ |
| Contrôles | ✔ |
| Remapping | ✔ |
| Contrôle du disque | ✔ |

## ![](/emulators/bios.svg) BIOS

### Liste des bios obligatoires

| Nom de fichier | Description | MD5 | Fourni |
| :---: | :---: | :---: | :---: |
| mpr-18811-mx.ic1 | The King of Fighters '95 - Requis pour ce jeu | 255113ba943c92a54facd25a10fd780c | ❌ |
| mpr-19367-mx.ic1 | Ultraman: Hikari no Kyojin Densetsu - Requis pour ce jeu | 1cd19988d1d72a3e7caa0b73234c96b4 | ❌ |
| saturn_bios.bin | BIOS Saturn | af5828fdff51384f99b3c4926be27762 | ❌ |
| stvbios.zip | BIOS ST-V | 2b99d9dcecde22db4371580b6bb33f32 53a094ad3a188f86de4e64624fe9b3ca 17116d2aea72454096b3f80ab714bfba 2b99d9dcecde22db4371580b6bb33f32 4cb34733b741ada074f11e3abcdc56d8 6b6d422a6c392eaec178b64b92987e99 88b9da6cda268ba1d9e37dd82d038801 8c34608cc140e1bfbf9c945a12fed0e8 b876d86e244d00b336980b4e26e94132 | ❌ |

### Emplacement

Placez les BIOS comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 saturn
┃ ┃ ┃ ┃ ┣ 🗒 **mpr-18811-mx.ic1**
┃ ┃ ┃ ┃ ┣ 🗒 **mpr-19367-mx.ic1**
┃ ┃ ┃ ┃ ┣ 🗒 **saturn_bios.bin**
┃ ┃ ┃ ┃ ┣ 🗒 **stvbios.zip**

## ![](/emulators/isos.svg) Isos

### Extensions supportées

Les isos doivent avoir les extensions suivantes :

* .bin/.cue
* .m3u
* .img/.ccd
* .chd

### Emplacement

Placez les isos comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 saturn
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.bin**
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.cue**

>Les isos au format **Redump** sont vivement conseillés.
{.is-success}

>Pour plus d'information sur les isos, rendez-vous sur [ce tutoriel](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuration avancée de l'émulateur

>Pour pouvoir conserver vos configurations personnalisées lors d'une mise à jour, nous vous conseillons d'utiliser notre fonctionnalité [Surcharges de configuration](./../../../advanced-usage/configuration-override).
{.is-info}

### Accéder aux options

Vous pouvez configurer diverses options de deux façons différentes.

* Via le menu RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via le fichier `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Options du core

| Options | Valeurs des options | Variable (pour surcharge) | Valeurs des variables |
| --- | --- | --- | --- |
| Force HLE BIOS (restart) | `Désactivé` ✅ / `Activé` | `kronos_force_hle_bios` | `disabled` / `enabled` |
| Video format | `auto` ✅ / `NTSC` / `PAL` | `kronos_videoformattype` | `auto` / `NTSC` / `PAL` |
| Frameskip | `No skipping` ✅ / `Skip rendering of 1 frames out of 2` / `Skip rendering of 2 frames out of 3` / `Skip rendering of 3 frames out of 4` / `Skip rendering of 4 frames out of 5` / `Skip rendering of 5 frames out of 6` | `kronos_skipframe` | `0` / `1` / `2` / `3` / `4` / `5` |
| SH-2 cache support (experimental) | `Désactivé` ✅ / `Activé` | `kronos_usercache` | `disabled` / `enabled` |
| Share saves with beetle | `Désactivé` ✅ / `Activé` | `kronos_use_beetle_saves` | `disabled` / `enabled` |
| Addon Cartridge (restart) | `None` / `1M_extended_ram` / `4M_extended_ram` / `16M_extended_ram` / `512K_backup_ram` ✅ / `1M_backup_ram` / `2M_backup_ram` / `4M_backup_ram` | `kronos_addon_cartridge` | `None` / `1M_extended_ram` / `4M_extended_ram` / `16M_extended_ram` / `512K_backup_ram` / `1M_backup_ram` / `2M_backup_ram` / `4M_backup_ram` |
| 6Player Adaptor on Port 1 | `Désactivé` ✅ / `Activé` | `kronos_multitap_port1` | `disabled` / `enabled` |
| 6Player Adaptor on Port 2 | `Désactivé` ✅ / `Activé` | `kronos_multitap_port2` | `disabled` / `enabled` |
| Resolution | `original` ✅ / `480p` / `720p` / `1080p` / `4k` / `8k` | `kronos_resolution_mode` | `original` / `480p` / `720p` / `1080p` / `4k` / `8k` |
| Output to original resolution | `Désactivé` ✅ / `Activé` | `kronos_force_downsampling` | `disabled` / `enabled` |
| Improved mesh | `Désactivé` ✅ / `Activé` | `kronos_meshmode` | `disabled` / `enabled` |
| Improved banding | `Désactivé` ✅ / `Activé` | `kronos_bandingmode` | `disabled` / `enabled` |
| Wireframe mode | `Désactivé` ✅ / `Activé` | `kronos_wireframe_mode` | `disabled` / `enabled` |
| ST-V Service/Test Buttons | `Désactivé` ✅ / `Activé` | `kronos_service_enabled` | `disabled` / `enabled` |
| ST-V Favorite Region | `EU` ✅ / `US` / `JP` / `TW` | `kronos_stv_favorite_region` | `EU` / `US` / `JP` / `TW` |
| Bios Language | `English` ✅ / `German` / `French` / `Spanish` / `Italian` / `Japanese` | `kronos_language_id` | `English` / `German` / `French` / `Spanish` / `Italian` / `Japanese` |

## ![](/emulators/external-links.png) Liens externes

* **Code source utilisé** : [https://github.com/FCare/Kronos/](https://github.com/FCare/Kronos/)
* **Documentation Libretro** : [https://docs.libretro.com/library/kronos/](https://docs.libretro.com/library/kronos/)