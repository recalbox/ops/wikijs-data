---
title: Libretro PCSX-ReARMed
description: 
published: true
date: 2025-02-07T20:36:38.738Z
tags: libretro, ps1, playstation 1, psx, pcsx-rearmed
editor: markdown
dateCreated: 2023-09-07T16:26:16.617Z
---

[Libretro PCSX-ReARMed](https://github.com/libretro/pcsx_rearmed) es un fork de [PCSX-ReARMed](https://github.com/notaz/pcsx_rearmed) basado en el proyecto PCSX-Reloaded.
Esta versión está orientada a una arquitectura ARM y fue creada para mejorar el rendimiento en el ordenador de bolsillo Pandora.
Este emulador fue luego portado a otros dispositivos como el Raspberry Pi.

## ![](/emulators/license.svg) Licencia

El core posee la siguiente licencia [**GPLv2**](https://github.com/libretro/pcsx_rearmed/blob/master/COPYING).

## ![](/emulators/compatibility.png) Compatibilidad

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | 
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Funcionalidades

| Funcionalidad | ¿Soportada? |
| :---: | :---: |
| Reinicio | ✔ |
| Capturas de pantalla | ✔ |
| Grabaciones | ✔ |
| Grabaciones instantáneas | ✔ |
| Rebobinado | ✔ |
| Opciones del core | ✔ |
| RetroAchievements | ✔ |
| Cheats RetroArch | ✔ |
| Controles | ✔ |
| Remapping | ✔ |
| Multi-Ratón | ✔ |
| Vibración | ✔ |
| Control de disco | ✔ |

## ![](/emulators/bios.svg) BIOS

### Lista de BIOS obligatorias

Aunque algunos juegos pueden iniciarse sin bios bajo PCSX-ReARMed, se recomienda encarecidamente añadir estos archivos al directorio de bios para mejorar el rendimiento del emulador y la gestión de copias de seguridad en tarjetas de memoria "virtuales".

| Nombre del fichero | Descripción | MD5 | ¿Preinstalada? |
| :---: | :---: | :---: | :---: |
| scph101.bin | Version 4.4 03/24/00 A | 6e3735ff4c7dc899ee98981385f6f3d0 | ❌ |
| scph7001.bin | Version 4.1 12/16/97 A | 1e68c231d0896b7eadcad1d7d8e76129 | ❌ |
| scph5501.bin | Version 3.0 11/18/96 A | 490f666e1afb15b7362b406ed1cea246 | ❌ |
| scph1001.bin | Version 2.0 05/07/95 A | dc2b9bf8da62ec93e868cfd29f0d067d 924e392ed05558ffdb115408c263dccf | ❌ |

### Emplazamiento

Disponed vuestras bios como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 🗒 **scph101.bin**
┃ ┃ ┃ ┣ 🗒 **scph7001.bin**
┃ ┃ ┃ ┣ 🗒 **scph5501.bin**
┃ ┃ ┃ ┣ 🗒 **scph1001.bin**

## ![](/emulators/isos.svg) Isos

### Extensiones de roms soportadas

Las isos deben tener las extensiones siguientes:

* .bin/.cue
* .bin/.toc
* .cbn
* .chd
* .exe
* .img/.ccd/.sub
* .iso
* .m3u
* .mdf/.mts
* .pbp

### Emplazamiento

Disponed vuestras isos como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 psx
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.bin**
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.cue**

>Se aconsejan las isos del proyecto **Redump**.
{.is-success}

>Para más información sobre las isos, consultad [este tutorial](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

>Para conservar las configuraciones personalizadas cuando se produce una actualización del sistema Recalbox, os aconsejamos utilizar las [Sobrecargas de configuración](./../../../advanced-usage/configuration-override).
{.is-info}


### Acceder a las opciones

Podéis configurar las opciones del core de dos formas diferentes.

* Via el Menú RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via el fichero `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Opciones del core

| Opción | Valor de la opción | Variable (para la sobrecarga) | Valores posibles |
| --- | --- | --- | --- |
| Region | `auto` ✅ / `NTSC` / `PAL` | `pcsx_rearmed_region` | `auto` / `NTSC` / `PAL` |
| BIOS Selection | `auto` ✅ / `hle` | `pcsx_rearmed_bios` | `auto` / `HLE` |
| Show BIOS Boot Logo | `Désactivé` ✅ / `Activé` | `pcsx_rearmed_show_bios_bootlogo` | `disabled` / `enabled` |
| Enable Second Memory Card (Shared) | `Désactivé` ✅ / `Activé` | `pcsx_rearmed_memcard2` | `disabled` / `enabled` |
| Dynamic Recompiler | `Désactivé` / `Activé` ✅ | `pcsx_rearmed_drc` | `disabled` / `enabled` |
| PSX CPU Clock Speed | `30` / `31` / `32` / `33` / `34` / `35` / `36` / `37` / `38` / `39` / `40` / `41` / `42` / `43` / `44` / `45` / `46` / `47` / `48` / `49` / `50` / `51` / `52` / `53` / `54` / `55` / `56` / `57` ✅ / `58` / `59` / `60` / `61` / `62` / `63` / `64` / `65` / `66` / `67` / `68` / `69` / `70` / `71` / `72` / `73` / `74` / `75` / `76` / `77` / `78` / `79` / `80` / `81` / `82` / `83` / `84` / `85` / `86` / `87` / `88` / `89` / `90` / `91` / `92` / `93` / `94` / `95` / `96` / `97` / `98` / `99` / `100` | `pcsx_rearmed_psxclock` | `30` / `31` / `32` / `33` / `34` / `35` / `36` / `37` / `38` / `39` / `40` / `41` / `42` / `43` / `44` / `45` / `46` / `47` / `48` / `49` / `50` / `51` / `52` / `53` / `54` / `55` / `56` / `57` / `58` / `59` / `60` / `61` / `62` / `63` / `64` / `65` / `66` / `67` / `68` / `69` / `70` / `71` / `72` / `73` / `74` / `75` / `76` / `77` / `78` / `79` / `80` / `81` / `82` / `83` / `84` / `85` / `86` / `87` / `88` / `89` / `90` / `91` / `92` / `93` / `94` / `95` / `96` / `97` / `98` / `99` / `100` |
| Dithering Pattern | `Désactivé` / `Activé` ✅ | `pcsx_rearmed_dithering` | `disabled` / `enabled` |
| Frame Duping (Speedup) | `Désactivé` / `Activé` ✅ | `pcsx_rearmed_duping_enable` | `disabled` / `enabled` |
| Frameskip | `Désactivé` ✅ / `Auto` / `Auto (Threshold)` / `Fixed Interval` | `pcsx_rearmed_frameskip_type` | `disabled` / `auto` / `auto_threshold` / `fixed_interval` |
| Frameskip Threshold (%) | `15` / `18` / `21` / `24` / `27` / `30` / `33` ✅ / `36` / `39` / `42` / `45` / `48` / `51` / `54` / `57` / `60` | `pcsx_rearmed_frameskip_threshold` | `15` / `18` / `21` / `24` / `27` / `30` / `33` / `36` / `39` / `42` / `45` / `48` / `51` / `54` / `57` / `60` |
| Frameskip Interval | `1` / `2` / `3` ✅ / `4` / `5` / `6` / `7` / `8` / `9` / `10` | `pcsx_rearmed_frameskip_interval` | `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` |
| Display Internal FPS | `Désactivé` ✅ / `Activé` | `pcsx_rearmed_display_internal_fps` | `disabled` / `enabled` |
| (GPU) Slow linked list processing | `auto` ✅ / `Désactivé` / `Activé` | `pcsx_rearmed_gpu_slow_llists` | `auto` / `disabled` / `enabled` |
| (GPU) Screen centering | `Auto` ✅ / `Game-controlled` / `Manual` | `pcsx_rearmed_screen_centering` | `auto` / `game` / `manual` |
| (GPU) Manual screen centering X | `-16` / `-14` / `-12` / `-10` / `-8` / `-6` / `-4` / `-2` / `0` ✅ / `2` / `4` / `6` / `8` / `10` / `12` / `14` / `16` | `pcsx_rearmed_screen_centering_x` | `-16` / `-14` / `-12` / `-10` / `-8` / `-6` / `-4` / `-2` / `0` / `2` / `4` / `6` / `8` / `10` / `12` / `14` / `16` |
| (GPU) Manual screen centering Y | `-16` / `-15` / `-14` / `-13` / `-12` / `-11` / `-10` / `-9` / `-8` / `-7` / `-6` / `-5` / `-4` / `-3` / `-2` / `-1` / `0` ✅ / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` | `pcsx_rearmed_screen_centering_y` | `-16` / `-15` / `-14` / `-13` / `-12` / `-11` / `-10` / `-9` / `-8` / `-7` / `-6` / `-5` / `-4` / `-3` / `-2` / `-1` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` |
| Show Interlaced Video | `Désactivé` ✅ / `Activé` | `pcsx_rearmed_neon_interlace_enable` | `disabled` / `enabled` |
| Enhanced Resolution | `Désactivé` ✅ / `Activé` | `pcsx_rearmed_neon_enhancement_enable` | `disabled` / `enabled` |
| Enhanced Resolution Speed Hack | `Désactivé` ✅ / `Activé` | `pcsx_rearmed_neon_enhancement_no_main` | `disabled` / `enabled` |
| Reverb Effects | `Désactivé` / `Activé` ✅ | `pcsx_rearmed_spu_reverb` | `disabled` / `enabled` |
| Sound Interpolation | `Simple` ✅ / `Gaussian` / `Cubic` / `Désactivé` | `pcsx_rearmed_spu_interpolation` | `simple` / `gaussian` / `cubic` / `off` |
| CD Audio | `Désactivé` / `Activé` ✅ | `pcsx_rearmed_nocdaudio` | `disabled` / `enabled` |
| XA Decoding | `Désactivé` / `Activé` ✅ | `pcsx_rearmed_noxadecoding` | `disabled` / `enabled` |
| Threaded SPU | `Désactivé` / `Activé` ✅ | `pcsx_rearmed_spu_thread` | `disabled` / `enabled` |
| Analog Axis Bounds | `Circle` ✅ / `Square` | `pcsx_rearmed_analog_axis_modifier` | `circle` / `square` |
| Rumble Effects | `Désactivé` ✅ / `Activé` | `pcsx_rearmed_vibration` | `disabled` / `enabled` |
| Multitap Mode | `Désactivé` ✅ / `Port 1` / `Port 2` / `Ports 1 and 2` | `pcsx_rearmed_multitap` | `disabled` / `port 1` / `port 2` / `ports 1 and 2` |
| NegCon Twist Deadzone | `0%` ✅ / `3%` / `5%` / `7%` / `10%` / `13%` / `15%` / `17%` / `20%` / `23%` / `25%` / `27%` / `30%` | `pcsx_rearmed_negcon_deadzone` | `0` / `3` / `5` / `7` / `10` / `13` / `15` / `17` / `20` / `23` / `25` / `27` / `30` |
| NegCon Twist Response | `Linear` ✅ / `Quadratic` / `Cubic` | `pcsx_rearmed_negcon_response` | `linear` / `quadratic` / `cubic` |
| Mouse Sensitivity | `0.05` / `0.10` / `0.15` / `0.20` / `0.25` / `0.30` / `0.35` / `0.40` / `0.45` / `0.50` / `0.55` / `0.60` / `0.65` / `0.70` / `0.75` / `0.80` / `0.85` / `0.90` / `0.95` / `1.00` ✅ / `1.05` / `1.10` / `1.15` / `1.20` / `1.25` / `1.30` / `1.35` / `1.40` / `1.45` / `1.50` / `1.55` / `1.60` / `1.65` / `1.70` / `1.75` / `1.80` / `1.85` / `1.90` / `1.95` / `2.00` | `pcsx_rearmed_input_sensitivity` | `0.05` / `0.10` / `0.15` / `0.20` / `0.25` / `0.30` / `0.35` / `0.40` / `0.45` / `0.50` / `0.55` / `0.60` / `0.65` / `0.70` / `0.75` / `0.80` / `0.85` / `0.90` / `0.95` / `1.00` / `1.05` / `1.10` / `1.15` / `1.20` / `1.25` / `1.30` / `1.35` / `1.40` / `1.45` / `1.50` / `1.55` / `1.60` / `1.65` / `1.70` / `1.75` / `1.80` / `1.85` / `1.90` / `1.95` / `2.00` |
| Player 1 Lightgun Crosshair | `Désactivé` ✅ / `blue` / `green` / `red` / `white` | `pcsx_rearmed_crosshair1` | `disabled` / `blue` / `green` / `red` / `white` |
| Player 2 Lightgun Crosshair | `Désactivé` ✅ / `blue` / `green` / `red` / `white` | `pcsx_rearmed_crosshair2` | `disabled` / `blue` / `green` / `red` / `white` |
| Konami Gun X Axis Offset | `-40` / `-39` / `-38` / `-37` / `-36` / `-35` / `-34` / `-33` / `-32` / `-31` / `-30` / `-29` / `-28` / `-27` / `-26` / `-25` / `-24` / `-23` / `-22` / `-21` / `-20` / `-19` / `-18` / `-17` / `-16` / `-15` / `-14` / `-13` / `-12` / `-11` / `-10` / `-9` / `-8` / `-7` / `-6` / `-5` / `-4` / `-3` / `-2` / `-1` / `0` ✅ / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `21` / `22` / `23` / `24` / `25` / `26` / `27` / `28` / `29` / `30` / `31` / `32` / `33` / `34` / `35` / `36` / `37` / `38` / `39` / `40` | `pcsx_rearmed_konamigunadjustx` | `-40` / `-39` / `-38` / `-37` / `-36` / `-35` / `-34` / `-33` / `-32` / `-31` / `-30` / `-29` / `-28` / `-27` / `-26` / `-25` / `-24` / `-23` / `-22` / `-21` / `-20` / `-19` / `-18` / `-17` / `-16` / `-15` / `-14` / `-13` / `-12` / `-11` / `-10` / `-9` / `-8` / `-7` / `-6` / `-5` / `-4` / `-3` / `-2` / `-1` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `21` / `22` / `23` / `24` / `25` / `26` / `27` / `28` / `29` / `30` / `31` / `32` / `33` / `34` / `35` / `36` / `37` / `38` / `39` / `40` |
| Konami Gun Y Axis Offset | `-40` / `-39` / `-38` / `-37` / `-36` / `-35` / `-34` / `-33` / `-32` / `-31` / `-30` / `-29` / `-28` / `-27` / `-26` / `-25` / `-24` / `-23` / `-22` / `-21` / `-20` / `-19` / `-18` / `-17` / `-16` / `-15` / `-14` / `-13` / `-12` / `-11` / `-10` / `-9` / `-8` / `-7` / `-6` / `-5` / `-4` / `-3` / `-2` / `-1` / `0` ✅ / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `21` / `22` / `23` / `24` / `25` / `26` / `27` / `28` / `29` / `30` / `31` / `32` / `33` / `34` / `35` / `36` / `37` / `38` / `39` / `40` | `pcsx_rearmed_konamigunadjusty` | `-40` / `-39` / `-38` / `-37` / `-36` / `-35` / `-34` / `-33` / `-32` / `-31` / `-30` / `-29` / `-28` / `-27` / `-26` / `-25` / `-24` / `-23` / `-22` / `-21` / `-20` / `-19` / `-18` / `-17` / `-16` / `-15` / `-14` / `-13` / `-12` / `-11` / `-10` / `-9` / `-8` / `-7` / `-6` / `-5` / `-4` / `-3` / `-2` / `-1` / `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `21` / `22` / `23` / `24` / `25` / `26` / `27` / `28` / `29` / `30` / `31` / `32` / `33` / `34` / `35` / `36` / `37` / `38` / `39` / `40` |
| Instruction Cache Emulation | `Désactivé` ✅ / `Activé` | `pcsx_rearmed_icache_emulation` | `disabled` / `enabled` |
| Exception and Breakpoint Emulation | `Désactivé` ✅ / `Activé` | `pcsx_rearmed_exception_emulation` | `disabled` / `enabled` |
| Disable Automatic Compatibility Hacks | `Désactivé` ✅ / `Activé` | `pcsx_rearmed_nocompathacks` | `disabled` / `enabled` |
| Disable SMC Checks | `Désactivé` ✅ / `Activé` | `pcsx_rearmed_nosmccheck` | `disabled` / `enabled` |
| Assume GTE Regs Unneeded | `Désactivé` ✅ / `Activé` | `pcsx_rearmed_gteregsunneeded` | `disabled` / `enabled` |
| Disable GTE Flags | `Désactivé` ✅ / `Activé` | `pcsx_rearmed_nogteflags` | `disabled` / `enabled` |
| Disable CPU/GTE Stalls | `Désactivé` ✅ / `Activé` | `pcsx_rearmed_nostalls` | `disabled` / `enabled` |

## ![](/emulators/external-links.png) Enlaces externos

* **Código fuente utilizado** : [https://github.com/libretro/pcsx_rearmed/](https://github.com/libretro/pcsx_rearmed/)
* **Documentación Libretro** : [https://docs.libretro.com/library/pcsx_rearmed/](https://docs.libretro.com/library/pcsx_rearmed/)