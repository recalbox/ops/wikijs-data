---
title: Retroarch AI (In-game translation)
description: Traduisez vos jeux en direct !
published: true
date: 2024-11-11T05:34:29.652Z
tags: retroarch, translation, in-game
editor: markdown
dateCreated: 2021-06-29T10:17:04.440Z
---

## What is Retroarch AI?

Welcome to the future! 

For some time, RetroArch allows the use of a translation service called "[**OCR**](./../../basic-usage/glossary)" (Optical Character Recognition) and **voice synthesis,** which will allow you to translate your foreign games almost instantly!

>This feature requires an internet connection and at least a left analog joystick!
{.is-info}

![Before](/basic-usage/features/ocr1.png)

![After](/basic-usage/features/ocr2.png)

## Configuration

As you know, everything is done to make the configuration of this kind of service as simple as possible. You have the possibility to use the default provided key or to use your own account.

### {.tabset}
#### Default account

Using the default account is very easy:

* Open your `recalbox.conf` file located in `/recalbox/share/system/`
* Find the line `;global.translate.apikey=RECALBOX`.
* Delete the `;` at the beginning of the line.
* Take the opportunity to tell the system what language you want the translation to be in by changing the `global.translate.to` parameter. You can always leave the parameter set to `auto`, in which case RetroArch will try to guess the target language.

#### Your account

The first step is to create an account on [Ztranslate.net](https://ztranslate.net/) a third party translation service called API, but other APIs can be configured!

![](/basic-usage/features/ztranslate_signup.png){.full-width}

* Once you are registered and connected to the site, go to the settings section to get your `API KEY`.

![](/basic-usage/features/ztranslate_settings.png){.full-width}

* Open your `recalbox.conf` file located in `/recalbox/share/system/`
* Find the line `;global.translate.apikey=RECALBOX`.
* Delete the `;` at the beginning of the line and paste your `API KEY` in place of `RECALBOX`.
* Take the opportunity to tell the system what language you want the translation to be in by changing the `global.translate.to` parameter. You can always leave the parameter set to `auto`, in which case RetroArch will try to guess the target language.

![](/basic-usage/features/recalbox.conf_fr.png)

## Here we go!

Have fun and forget frustration, launch a 100% Japanese game!

* To activate the translation, nothing could be easier: with your controller, press the `HOTKEY + L. STICK TO THE DOWN' buttons, the game is paused and shows you an image with the replacement text.
* Press the same key combination again to resume the game.

By default, Recalbox uses picture mode rather than text-to-speech. To use the latter, a simple [configuration override](./../../advanced-usage/configuration-override/retroarch-overrides) is sufficient.