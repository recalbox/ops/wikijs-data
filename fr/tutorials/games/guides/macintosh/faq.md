---
title: FAQ
description: 
published: true
date: 2024-07-23T23:05:44.212Z
tags: faq, apple, macintosh, 9.0+
editor: markdown
dateCreated: 2023-03-08T17:58:04.917Z
---

Voici les questions relatives à l'utilisation du système Macintosh.

## Régler l'écran du système sur deux couleurs

Certains vieux jeux Macintosh vous demanderont de passer l'écran du système sur deux couleurs (en noir et blanc).
Il peut arriver cependant que le fichier BIOS de MinivMac n'inclut pas l'utilitaire pour régler les couleurs d'écran.
Pour régler ce problème, il faudra modifier le fichier MinivMacBootv2.dsk à l'aide de l'émulateur Mini vMac.

### Vous aurez besoin :

 - D'un PC pour effectuer les manipulations
 - Des fichiers MinivMacBootv2.dsk et MacII.ROM
 - Des disquettes d'installation pour le **System 7.5**
 - De l'émulateur Mini vMac (disponible ici : https://www.gryphel.com/c/minivmac/dnld_mii.html)
 
### Étapes à suivre

1. Démarrez Mini vMac. Si Mini vMac affiche le message "Unable to locate MacII.ROM", glissez le fichier dans le répertoire où vous avez extrait Mini vMac.
![minivmac-rom-missing.png](/tutorials/games/guides/macintosh/minivmac-rom-missing.png){.full-width}
2. Si le fichier est détecté, vous entendrez un petit bip. Ouvrez le fichier "MinivMacBootv2.dsk". Vous pouvez soit glisser-déposer le fichier dans l'émulateur, soit ouvrir le fichier via le menu "File > Open Disk Image".
3. Insérez la première disquette d'installation.  Le contenu de la disquette s'ouvre.
![minivmac-install-disk1.png](/tutorials/games/guides/macintosh/minivmac-install-disk1.png){.full-width}
4. Cliquer sur le fichier "Installer". Le programme d'installation se lance. Cliquez sur "Continue"
![minivmac-install-step1.png](/tutorials/games/guides/macintosh/minivmac-install-step1.png){.full-width}
5. Pour installer l'utilitaire dont on a besoin, il faut sélectionner "Custom Install" dans le menu déroulant en haut à gauche.
![minivmac-install-step2.png](/tutorials/games/guides/macintosh/minivmac-install-step2.png){.full-width}
6. Cherchez "Control Panel", puis cliquez sur le triangle à gauche pour déployer les options. Ensuite, cochez "Monitors". Enfin, cliquez sur "Install".
![minivmac-install-step3.png](/tutorials/games/guides/macintosh/minivmac-install-step3.png){.full-width}
7. Le programme d'installation vous demande d'insérer la disquette 3. Glissez-déposez le fichier de la disquette 3 dans l'émulateur ou ouvrez le fichier.
![minivmac-install-step4.png](/tutorials/games/guides/macintosh/minivmac-install-step4.png){.full-width}
8. Le programme indique que l'installation s'est bien passée. Il demande de redémarrer le système. Cliquez sur "Restart". L'émulateur peut mettre 1 à 2 minutes pour redémarrer le système. Si l'icône d'une disquette apparaît lors du redémarrage, ouvrez à nouveau le fichier "MinivMacBootv2.dsk".
![minivmac-install-step5.png](/tutorials/games/guides/macintosh/minivmac-install-step5.png){.full-width}
9. Vous pouvez vérifier que l'utilitaire est bien installé en cliquant sur Pomme > Control Panels > Monitors. Si c'est le cas, vous pouvez éteindre la machine virtuelle (Pomme > Shut Down).
![minivmac-install-step6.png](/tutorials/games/guides/macintosh/minivmac-install-step6.png){.full-width}
10. Vous pouvez maintenant copier et remplacer le fichier MinivMacBootv2.dsk sur votre Recalbox, dans le dossier des bios. Lorsqu'un jeu vous demandera de passer en deux couleurs, il vous faudra ouvrir le panneau de contrôle "Monitors" et de cliquer sur "Black & White". Place au jeu !
![minivmac-install-step7.png](/tutorials/games/guides/macintosh/minivmac-install-step7.png){.full-width}