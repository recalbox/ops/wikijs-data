---
title: Gestión de ficheros
description: 
published: true
date: 2023-11-02T00:50:47.456Z
tags: gestion, fichiers
editor: markdown
dateCreated: 2023-04-18T08:13:34.340Z
---

**Recalbox** permite gestionar vuestros ficheros a través de la carpeta "share".

Existen dos posibilidades :

* Conexión directa
* A través de la red

## Carpeta "Share"

>La carpeta **"Share"** que permite gestionar el contenido de Recalbox se encuentra disponible **a través de la red**.
{.is-info}

![](/basic-usage/file-management/share1.png){.full-width}

Dentro de esta carpeta se encuentran varios directorios :

![](/basic-usage/file-management/share2.png){.full-width}

* **bios**

En esta carpeta se encuentran **los bios** que necesita Recalbox. Puedes consultar la lista de bios a través de EmulationStation (`INICIO` > `VERIFICACIÓN DE BIOS`).

>Algunos emuladores necesitan los bios en un subdirectorio o pueden necesitar una copia del fichero de bios dentro de la carpeta de roms. Consultad la documención de cada emulador para conocer exactamente el buen emplazamiento.
{.is-warning}

* **bootvideos**

Poned aquí los vìdeos que quéreis ver cuando Recalbox arranca.

* **cheats**

Contenido de cheats utilizables à traves del menu RetroArch.

* **extractions**

Carpeta utilizada para descomprimir las roms antes de lanzar el juego.

* **kodi**

En este directorio podeis incluir los medias que queréis añadir a Kodi, utilizando los siguientes subdirectorios :

  * music
  * movie
  * picture
  
* **music**

En esta carpeta podéis añadir vuestras canciones para reemplazar la música por defecto de EmulationStation.

>Los ficheros deben tener el format `.MP3` o `.OGG` y deben tener una tasa de muestras de 44100Hz y un caudal de 256 kb/s.
{.is-info}

* **overlays**

Esta carpeta contiene los overlays que sirven para decorar los border de la pantalla durante une juego.

* **roms**

Aquí se encuentran las roms distribuidas en un subdirectorio para cada systema/consola.

* **saves**

Carpeta donde se guardan las partidas en curso (saves).

* **screenshots**

Carpeta donde se guardan las capturas de pantalla de los juegos.

* **shaders**

Carpeta donde se encuentran los shaders del sistema.

* **system**

Carpeta relativa a la configuración del sistema Recalbox.

* **themes**

Carpeta donde se deben poner los temas para cambiar la visualización del interface de EmulationStatation.

* **userscripts**

Directorio donde se pueden poner scripts personales.

## Transferencia de ficheros

Para añadir ficheros (roms, grabaciones de partidas, bios, datos de scrap etc...) a vuestro dispositivo de almacenamiento, existen dos posibilidades :

### Conexión directa

* Enchufad vuestro dispositivo de almacenamiento a vuestro ordenador.
* Abrid el explorador de ficheros.
* Id a la pestaña que lista vuestros périféricos.
* Seleccionad el nombre de vuestro dispositivo de almacenamiento, a continuación la carpeta `recalbox` y por último la carpeta `share`.
* Copiad vuestros ficheros al directorio elegido.
* Una vez que la transferencia ha terminado, desenchufad el dispositivo y metedlo de nuevo en vuestra Recalbox, encendedla y jugad.

### A través de la red

>Cuando conectais Recalbox à la red utilizando la wifi o un cable de red, Recalbox comparte automaticamente varias carpetas con la red local del usuario.
{.is-info}

* Si Recalbox y vuestro PC se encuentran en la misma red, abrid el explorador de ficheros de vuestro PC.
* Id a la pestaña Red, à continuaciòn `recalbox` y finalemente `share`.
* Copiad vuestros ficheros al directorio seleccionado.

>Si en el explorador de ficheros no encontráis Recalbox, probad a escribir `\\RECALBOX` en la barra de direcciones del explorador de ficheros.
>
>Si tampoco funciona así, buscad vuestra [dirección IP](./../tutorials/network/ip/discover-recalbox-ip). A continuación escribid vuestra dirección IP en la barra de direcciones del explorador. (Por ejemplo : \\\192.168.1.30\).
>
>Si tampoco funciona así y utilizáis Windows 10, podeis seguir [este tutorial](./../tutorials/network/share/access-network-share).
{.is-info}

### Para hacer efectivas las modificaciones :

* Actualizad la lista de juegos :

  * Menu EmulationStation con `INICIO`.
  * `OPTIONES DEL INTERFACE`.
  * `ACTUALIZAR LA LISTA DE JUEGOS`.

* Reiniciad Recalbox :

  * Menu EmulationStation con `INICIO`.
  * Sélectionnez `REINICIAR`.

## Añadir contenido

### Añadir juegos

Sólo tenéis que copiar los ficheros de roms dentro del directorio adecuado.

>**Ejemplo para la Super Nintendo:**
>
>`/recalbox/share/roms/snes/`
{.is-success}

Podéis utilizar roms comprimidas (.zip, .7z) o roms sin compresión, depende del emulador.

>Para obtener más información sobre los formatos de roms soportados por cada emulador, se pueden consultar los ficheros `_leeme.txt` que se encuentran dentro de cada directorio de roms.
{.is-info}

### Añadir bios

Algunos emuladores necesitan bios para poder funcionar correctamente.

* Si queréis añadir BIOS a vuestro sistema, abrid la carpeta BIOS compartida por Samba o id directamente dentro de `/recalbox/share/bios/`.
* El nombre de los ficheros de BIOS y sus firmas MD5 deben corresponder con la lista que se muestra en el [Bios Manager](./../basic-usage/features/bios-manager).

Para verificar la firma MD5 de un fichero BIOS, consultad la [siguiente página](./../tutorials/utilities/rom-management/check-md5sum-of-rom-or-bios).

>Para la Neo-Geo, debéis añadir el bios `neogeo.zip` dentro de la carpeta de bios o dentro de la carpeta de roms del emulador (/recalbox/share/roms/neogeo o /recalbox/share/roms/fbneo)
>Para la Neo-Geo CD, debéis añadir el bios `neogeo.zip` y `neocdz.zip` dentro de la carpeta /recalbox/share/bios/
{.is-warning}