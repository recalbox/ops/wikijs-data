---
title: Jouer aux jeux Satellaview
description: 
published: true
date: 2024-10-30T18:33:27.429Z
tags: nintendo, satellaview
editor: markdown
dateCreated: 2024-10-30T18:26:51.328Z
---

## Introduction

La majorité du temps, vous allez vouloir jouer aux jeux Satellaview avec le premier bios valide. Sauf que ce bios, la grosse majorité du temps, sera en japonais. Voici comment jouer aux jeux Satellaview si vous n'avez pas envie de chercher un bios anglais.

## Procédure

- Lancez votre jeu. Vous allez arriver devant le premier écran (un poste de télévision qui vous parle).

![](/tutorials/games/guides/satellaview/satellaview1.png){.full-width}

- Validez et remplissez le pseudo que vous souhaitez utiliser. Celui-ci servida dans le système et dans les jeux.

![](/tutorials/games/guides/satellaview/satellaview2.png){.full-width}

- Vous pouvez cliquer sur le mot en bas à gauche `PAGE` pour changer d'alphabet. Une fois terminé, faites `DONE` en bas à droite (le fait d'appuyer sur le bouton `START` vous placera sur `DONE`). Vous devrez mettre si vous êtes plutôt garçon ou fille et validez.

![](/tutorials/games/guides/satellaview/satellaview3.png){.full-width}

- Une fois tout ceci fait, le système va tenter de se connecter au système d'origine mais n'y arrivera pas, n'existant plus. Vous vous retrouverez dans une ville qui fut animé à l'époque où le service était actif. Vous ne pouvez rien faire d'autre que de rentrer dans le batiment devant lequel vous vous trouver (`HAUT` puis `A`).

![](/tutorials/games/guides/satellaview/satellaview4.png){.full-width}

- Vous atterrirez devant un menu avec 4 choix possibles

  - `Load Stored Data` : jouer au jeu que vous avez lancé au début
  - `Delete Stored Data` :  suppriemr le contenu du jeu
  - `Receive Program` : recevoir une mise à jour (ne peut plus fonctionner)
  - `Clear Personal Data` : supprimer le profil que vous avez crée jusqu'à maintenant

![](/tutorials/games/guides/satellaview/satellaview5.png){.full-width}

- Sélectionnez `Load Stored Data`, sélectionnez le jeu et c'est parti !

![](/tutorials/games/guides/satellaview/satellaview6.png){.full-width}