---
title: Atomiswave
description: 
published: true
date: 2024-07-29T10:26:39.030Z
tags: arcade, atomiswave
editor: markdown
dateCreated: 2023-09-01T20:58:39.993Z
---

![](/emulators/arcade/atomiswave.svg){.align-center}

## Ficha técnica

* **Fabricante :** Sammy Corporation
* **Año de lanzamiento :** 2003
* **Procesador :** Hitachi SH-4 32-bit RISC CPU (@ 200 MHz 360 MIPS / 1.4 GFLOPS)
* **Procesador gráfico :** NEC-Videologic PowerVR (PVR2DC/CLX2) @ 100 MHz
* **Chip de sonido :** ARM7 Yamaha AICA 45 MHZ (con procesador interno 32-bit RISC, 64 canales ADPCM)
* **RAM :** 40 MB
* **Soporte :** ROM Board (tamaño máximo de 168 MB)

## Presentación

El **Atomiswave** es un sistema de juegos de **cartucho** para máquinas recreativas compatibles con JAMMA destinado a las salas de juegos, creado por la empresa japonesa **Sammy**. Su comercialización se lanzó a principios del año 2003. Los juegos se presentan en forma de pequeños cartuchos, muy ligeros, sólidos y fácilmente intercambiables.

El Atomiswave pretende ser un sistema de juego de bajo costo, compacto, muy fácil de usar y fácilmente modulable, con la posibilidad de conectar fácilmente una pistola, dobles joysticks, un volante o un trackball, pretendiendo ser el nuevo sistema de éxito de la arcade, el sucesor espiritual del Neo-Geo MVS.

Técnicamente, el sistema Atomiswave no es más ni menos que una Dreamcast un poco potenciada en versión arcade, todo el hardware es, por tanto, de Sega. Con este sistema de arcade, Sammy quería sacar juegos de bajo costo y lo más rentable posible para los explotadores. Además de las salas de juegos japonesas, Sammy apuntaba al mercado internacional de la arcade.

Desde su lanzamiento, el Atomiswave se beneficia del apoyo de la empresa **SNK Playmore**, que decide abandonar el desarrollo en arcade sobre **Neo-Geo MVS** en favor de este sistema para sus futuros juegos (antes de volver al Taito Type X²). Los otros desarrolladores que apoyan el sistema son **Dimps**, **IGS** y **Arc System Works**, pero la mayoría de los títulos Atomiswave son desarrollados por Sammy.

Recordemos también que Sammy y Sega, poseyendo ambos un pasado en los videojuegos y la arcade, **fusionaron en 2004**, durante la explotación de este sistema. Las dos empresas forman parte del mismo grupo, dirigido ahora por la empresa matriz **Sega Sammy Holdings**.

En 2009, tres años después del lanzamiento del último juego Atomiswave, Sega Amusement USA decide sacar dos nuevos juegos para el sistema, Sega Clay Challenge y Sega Bass Fishing Challenge, destinados a actualizar las máquinas Atomiswave aún en explotación.

El Atomiswave fue un semi-fracaso para Sammy. Al final, solo salieron una treintena de juegos en el sistema antes de que fuera **abandonado a finales de 2009**.

## Emuladores

[Libretro Flycast](libretro-flycast)
[Libretro Flycast-Next](libretro-flycast-next)
