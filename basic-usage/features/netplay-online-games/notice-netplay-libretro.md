---
title: Notice Netplay Libretro
description: 
published: true
date: 2023-01-02T14:18:47.582Z
tags: libretro, netplay
editor: markdown
dateCreated: 2021-08-05T16:52:17.294Z
---

## The Netplay Lobby

The Netplay lobby is a place where you can connect to play online with others. There are 2 ways to enjoy it:

* Create your own online game where other players join you (you are a host)
* Join an existing online game (you are a customer).

You can join an online game either as a player or as a spectator (streaming).

### Configuration

By default, netplay is not enabled. To enable and customize it, in the system list, do `Start` > `Game Options` > `Netplay Options`.

The options are as follows:

* `NETPLAY` : enables netplay
* `SURNAME` : your nickname on the netplay

>Do not use accented characters, this will prevent you from using netplay.
>{.is-info}

* `PORT` : the port to use to connect to. Default is 55435.

>If you have an IP address with only certain ports range, you need to change the port here to what you can use and create a port redirect into your internet box.
>{.is-info}

Once you have filled those informations, you are ready to try an online game.

### Create a game room (host)

* From the game list, press the `X` button while being that the desired game.
* The password menu appears.

![](/basic-usage/features/netplay/libretro-netplay1-en.png)

Here are the options:

* `SET PLAYER PASSWORD` : select ON if you want to put a player password on your game.

  * People who want to join as a player will need to have the same player password as the host.

* `SET VIEVER PASSWORD`: select ON if you want to set a spectator password on your game.

  * People who want to join as a spectator will need to have the same spectator password as the host.

* `CHOOSE PLAYER PASSWORD` : a selection of predefined passwords.

  * Of course, you can customize the passwords in this list

* `CHOOSE VIEWER-ONLY PASSWORD`: a selection of predefined passwords.

  * Of course, you can customize the passwords in this list

Once you have made your choice:

  * Select `START` to start the game.
  * Wait for players: a pop-up appears on the screen of Recalbox users who are logged in to notify them that a Netplay session is available.

![](/basic-usage/features/netplay/libretro-netplay2-en.png)

### Join a game room (client)

* Join the Netplay lobby with the button `X` in the system list. If Kodi is enabled, you will have a menu to choose the Netplay lobby.

![](/basic-usage/features/netplay/libretro-netplay3-en.png)

* Select the host you wish to connect to.

## Information in the lobby

For each game, you will have information:

### On the left

* **A sign**: ✅ (valid) or ❌ (not valid) in front of the Recalbox logo or rom name.
* **The Recalbox logo**: if the person uses Recalbox. Between the sign and the name of the rom
* **The name of the rom** : the exact name of the hosted rom file. After the sign and the Recalbox logo.

### On the right

#### Basic result

* `Username` : player name of the host
* `Country` : country code of your host
* `ROM hash` : match the rom signature
* `ROM file` - rom file match
* `Core` : emulator used
* `Core Ver.` : version of the emulator
* `Latency` : response time
* `RA Ver.` : version of RetroArch
* `Host arch.` : architecture of the host (you can have the Recalbox logo here if the host uses it)

You will see the result in front of each item in the list, ✅ (valid) or ❌ (not valid).

#### Overall result

You will see the global result at the bottom of the window on the right, indicating the chances of a successful connection:

* **Green**: you have the right rom with the right hash, the right core. All the chances are on your side for it to work!

![](/basic-usage/features/netplay/netplay-lobby-ok-en.png)

* **Blue**: no hash match (some roms don't have a hash, like arcade systems), but the right file has been found. It should work!

![](/basic-usage/features/netplay/netplay-ok-nok-en.png)

* **Red**: file not found, system forbidden, not the right core. No chance for the online game to work!

![](/basic-usage/features/netplay/netplay-nok-en.png)