---
title: Actualizaciones
description: 
published: true
date: 2024-11-17T16:19:15.803Z
tags: actualizaciones
editor: markdown
dateCreated: 2023-09-04T19:39:28.840Z
---

## Proceso de actualización

Estas son las **opciones de actualización** para Recalbox.  Esperamos que este resumen te ayude y te de las respuestas que buscas.

## Dependiendo de tu situación...

### ¿Es posible actualizar Recalbox x.x a Recalbox 7?

**No**, no es posible actualizar a la nueva versión de Recalbox 7. Se requiere una instalación limpia. Vea abajo las instrucciones.

### ¿Qué pasa si estoy usando un theme diferente al predeterminado?

* Asegúrese de que está al día con los sistemas y archivos de configuración de su theme.
* Ponte en contacto con el autor de tu theme o con el tema de tu tema para comprobar si hay disponible una nueva versión actualizada.
* Utilice un theme personalizado en 720p en Raspberry Pi. En 1080p, su Raspberry Pi sufrirá.
* No proporcionamos soporte para themes personalizados no oficiales de Recalbox. Los creadores de themes son los únicos que dan soporte a sus temas.
* Por favor, utilice el theme por defecto Recalbox para comprobar sus errores antes de publicar su problema en el foro.

### ¿Qué pasa si utilizo OVERLAYS?

Si has creado tus propios overlays, RetroArch ha añadido una nueva relación de aspecto que desplaza ciertos overlays:

* Prueba 22 o 23 en esta línea en las configuraciones de overlays:

```ini
aspect_ratio_index = 21
```

### Los sitemas arcade

Recalbox actualiza regularmente los núcleos de los emuladores arcade MAME & FinalBurn Neo. Como consecuencia, hay que actualizar a menudo vuestros romsets:

* Los ficheros "dat" globales se encuentran en `/recalbox/share/arcade`. Puedes encontrar más información en [esta página](./../../tutorials/games/generalities/isos-and-roms/sort-and-update-your-roms).

* Para crear dat parent-only, neogeo parent only etc... basta con utilizar este [datutil tutorial](./../../tutorials/utilities/dat-management/datutil).

* Para conocer las versiones actuales de los romsets arcade, podéis consultar [esta página](./../../emuladores/arcade).


### Tu Raspberry Pi en una caja

* Asegúrate de tener una buena ventilación.
* Un buen cable de alimentación.
* Si tienes algún problema, saca tu Raspberry Pi de la carcasa y prueba Recalbox 7 bare-metal.

### Nuestro consejo:

* Ten una copia de seguridad de tu parte en tu PC o disco duro externo.
* Utiliza un soporte extraíble (llave USB o disco duro externo), pronto te darás cuenta de que reinstalar es más rápido.
* Utiliza un rascador externo.
* Ten siempre 2 microsd, 16 GB son suficientes para el sistema (una para la versión estable, otra para tus pruebas, o pruebas beta) de ahí la importancia de tener tus roms en medios extraíbles.
* Instala disipadores o ventiladores si es necesario:
  * Si vas a hacer overclocking en tu Raspberry Pi.
  * Si tu Recalbox está en una caja.

## Lanza la actualización

>Se puede acceder a la **actualización** de Recalbox pulsando `START` > `UPDATES`.
{.is-info}

* Configura tu wifi o conecta un cable de red a tu Recalbox.
* Seleccione "ACTUALIZAR".
* Luego `LANZAR LA ACTUALIZACIÓN`. 

El sistema se reiniciará automáticamente una vez descargada la actualización.