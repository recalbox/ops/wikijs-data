---
title: Oricutron
description: 
published: true
date: 2024-07-17T21:18:13.228Z
tags: oric, oric-atmos, oricutron
editor: markdown
dateCreated: 2023-09-11T10:09:35.417Z
---



## ![](/emulators/license.svg) Licencia

El core posee la siguiente licencia [**GPLv2**](https://github.com/pete-gordon/oricutron/issues/159#issuecomment-749596344).

## ![](/emulators/compatibility.png) Compatibilidad

 | RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | 
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Funcionalidades



## ![](/emulators/bios.svg) BIOS

### Lista de BIOS obligatorias

Encontraréis las bios con el nombre indicado en la columna **Descripción**. Os hara falta renombrarlas con el nombre indicado en la columna **Nombre del fichero**.

| Nombre del fichero | Descripción | MD5 | ¿Preinstalada? |
| :---: | :---: | :---: | :---: |
| basic11b.rom | - | a330779c42ad7d0c4ac6ef9e92788ec6 | ❌ |
| basic10.rom | - | ebe418ec8a6c85d5ac32956c9a96c179 | ❌ |
| jasmin.rom | - | 5136f764a7dbd1352519351fbb53a9f3 | ❌ |
| microdis.rom | - | df864344d2a2091c3f952bd1c5ce1707 | ❌ |
| pravetzt.rom | - | 8712a22e7e078de3343667d9fc1f2390 | ❌ |
| teleass.rom | - | 2324c9cc227c1327a72a667c97ed2990 | ❌ |
| hyperbas.rom | - | 364bf095e0dc4222d75354d50b8cddfc | ❌ |
| telmon24.rom | - | 9a432244d9ee4a49e8ddcde64af94e05 | ❌ |

### Emplazamiento

Disponed vuestras bios como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 oricutron
┃ ┃ ┃ ┃ ┣ 🗒 **basic11b.rom**
┃ ┃ ┃ ┃ ┣ 🗒 **basic10.rom**
┃ ┃ ┃ ┃ ┣ 🗒 **jasmin.rom**
┃ ┃ ┃ ┃ ┣ 🗒 **microdis.rom**
┃ ┃ ┃ ┃ ┣ 🗒 **pravetzt.rom**
┃ ┃ ┃ ┃ ┣ 🗒 **teleass.rom**
┃ ┃ ┃ ┃ ┣ 🗒 **hyperbas.rom**
┃ ┃ ┃ ┃ ┣ 🗒 **telmon24.rom**

### Lista de BIOS opcionales

| Nombre del fichero | Descripción | MD5 | ¿Preinstalada? |
| :---: | :---: | :---: | :---: |
| bd500.rom | - | 2b6498fd29a0adbf1c529762c02c33ab | ❌ |
| 8dos2.rom | - | 5f3cd5a4307fed7a9dfe8faa4c044273 | ❌ |

### Emplazamiento

Disponed vuestras bios como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 oricutron
┃ ┃ ┃ ┃ ┣ 🗒 **bd500.rom**
┃ ┃ ┃ ┃ ┣ 🗒 **8dos2.rom**

## ![](/emulators/roms.png) Roms

### Extensiones de roms soportadas

Las roms deben tener las extensiones siguientes:

* .dsk
* .tap

### Emplazamiento

Disponed vuestras roms como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 oricatmos
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.dsk**

>Se aconsejan las roms del proyecto **TOSEC**.
{.is-success}

>Para más información sobre las roms, consultad [este tutorial](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

>Para conservar las configuraciones personalizadas cuando se produce una actualización del sistema Recalbox, os aconsejamos utilizar las [Sobrecargas de configuración](./../../../advanced-usage/configuration-override).
{.is-info}

## ![](/emulators/external-links.png) Enlaces externos

* **Código fuente utilizado** : [https://github.com/pete-gordon/oricutron/](https://github.com/pete-gordon/oricutron/)
* **Documentación** : [http://www.petergordon.org.uk/oricutron/](http://www.petergordon.org.uk/oricutron/)