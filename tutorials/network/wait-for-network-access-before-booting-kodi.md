---
title: Attendre le réseau avant le démarrage de Kodi
description: 
published: true
date: 2021-09-04T21:59:40.554Z
tags: kodi, accès, réseau
editor: markdown
dateCreated: 2021-05-21T08:08:41.955Z
---

Lorsque Kodi est en mode démarrage automatique sur Recalbox, il est possible, sur certains systèmes, que le service réseau ne soit pas encore démarré lorsque celui-ci se lance. Cela cause des problèmes pour les utilisateurs qui possèdent des partages réseaux ou utilise une base de données distante.

## Comment procéder

* Dans le fichier `recalbox.conf`, cherchez dans la section `A - System options` les lignes suivantes :

```text
;kodi.network.waitmode=required
;kodi.network.waittime=10
;kodi.network.waithost=192.168.0.50
```

* Décommentez les lignes et modifiez les valeurs selon vos besoins en supprimant le point virgule.

## Explications des options

* _**kodi.network.waitmode**_

  * **required** - Utilisez cette option afin de forcer Kodi à ne pas pouvoir se lancer avant d'avoir pu joindre l'adresse IP indiquée.
  * **wish** - Utilise un compte à rebours plutot que l'option de devoir pinger une machine avant de démarrer.


* _**kodi.network.waittime**_

  * Temps en seconde à attendre avant que le démarrage de Kodi échoue (lorsque `required` est renseigné) ou continue son démarrage (lorsque `wish` est renseigné).

* _**kodi.network.waithost**_

  * L'adresse IP du système utilisée pour tester la connexion réseau (par exemple : l'adresse IP de votre box ou de votre partage réseau utilisé par Kodi).