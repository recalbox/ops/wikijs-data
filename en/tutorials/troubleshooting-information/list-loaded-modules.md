---
title: List loaded modules
description: 
published: true
date: 2021-08-06T15:16:00.356Z
tags: modules, loaded
editor: markdown
dateCreated: 2021-08-06T14:34:24.581Z
---

You can list all the modules loaded in SSH. This can be useful if you have a problem with a particular module. One of the Recalbox developers may ask you to use this command in some cases.

## Usage

* Connect [via SSH](./../../tutorials/system/access/root-access-terminal-cli)
* Type the following command:

```shell
lsmod
```

* Once the command is executed, it should show you a list of loaded modules. You can copy/paste the necessary information where you were asked to use it.

## Example

```shell
# lsmod
Module                  Size  Used by    Tainted: G  
ipv6                  499712 70 [permanent]
i2c_dev                20480  2 
i2c_bcm2708            16384  0 
uinput                 20480  0 
rfcomm                 49152  4 
cmac                   16384  2 
algif_hash             16384  1 
aes_arm_bs             24576  2 
crypto_simd            16384  1 aes_arm_bs
cryptd                 24576  2 crypto_simd
algif_skcipher         16384  1 
af_alg                 28672  6 algif_hash,algif_skcipher
hci_uart               40960  1 
btbcm                  16384  1 hci_uart
fuse                  126976  4 
bnep                   20480  2 
8021q                  32768  0 
garp                   16384  1 8021q
stp                    16384  1 garp
llc                    16384  2 garp,stp
bluetooth             421888 31 rfcomm,hci_uart,btbcm,bnep
ecdh_generic           16384  1 bluetooth
ecc                    36864  1 ecdh_generic
brcmfmac              335872  0 
brcmutil               24576  1 brcmfmac
sha256_generic         16384  0 
vc4                   253952 13 
cec                    49152  1 vc4
drm_kms_helper        245760  3 vc4
joydev                 20480  0 
cfg80211              782336  1 brcmfmac
v3d                    77824  1 
usbhid                 49152  0 
gpu_sched              45056  1 v3d
drm                   528384  7 vc4,drm_kms_helper,v3d,gpu_sched
bcm2835_codec          40960  0 
bcm2835_isp            32768  0 
bcm2835_v4l2           49152  0 
v4l2_mem2mem           36864  1 bcm2835_codec
bcm2835_mmal_vchiq     28672  3 bcm2835_codec,bcm2835_isp,bcm2835_v4l2
videobuf2_dma_contig   20480  2 bcm2835_codec,bcm2835_isp
videobuf2_vmalloc      16384  1 bcm2835_v4l2
videobuf2_memops       16384  2 videobuf2_dma_contig,videobuf2_vmalloc
videobuf2_v4l2         32768  4 bcm2835_codec,bcm2835_isp,bcm2835_v4l2,v4l2_mem2mem
videobuf2_common       61440  5 bcm2835_codec,bcm2835_isp,bcm2835_v4l2,v4l2_mem2mem,videobuf2_v4l2
videodev              258048  6 bcm2835_codec,bcm2835_isp,bcm2835_v4l2,v4l2_mem2mem,videobuf2_v4l2,videobuf2_common
rfkill                 32768  3 bluetooth,cfg80211
snd_soc_core          229376  1 vc4
snd_bcm2835            28672  1 
snd_compress           20480  1 snd_soc_core
snd_pcm_dmaengine      16384  1 snd_soc_core
snd_pcm               114688  6 vc4,snd_soc_core,snd_bcm2835,snd_compress,snd_pcm_dmaengine
drm_panel_orientation_quirks    16384  1 drm
mc                     45056  6 bcm2835_codec,bcm2835_isp,v4l2_mem2mem,videobuf2_v4l2,videobuf2_common,videodev
raspberrypi_hwmon      16384  0 
i2c_bcm2835            16384  1 
i2c_brcmstb            16384  0 
vc_sm_cma              32768  2 bcm2835_isp,bcm2835_mmal_vchiq
rpivid_mem             16384  0 
snd_timer              36864  2 snd_pcm
snd                    77824 11 snd_soc_core,snd_bcm2835,snd_compress,snd_pcm,snd_timer
syscopyarea            16384  1 drm_kms_helper
sysfillrect            16384  1 drm_kms_helper
sysimgblt              16384  1 drm_kms_helper
fb_sys_fops            16384  1 drm_kms_helper
backlight              20480  1 drm
nvmem_rmem             16384  0 
uio_pdrv_genirq        16384  0 
uio                    20480  1 uio_pdrv_genirq
```

