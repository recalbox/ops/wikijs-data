---
title: FAQ
description: 
published: true
date: 2021-10-16T11:38:27.980Z
tags: tic-80, faq, jeux, télécharger
editor: markdown
dateCreated: 2021-08-01T22:19:40.450Z
---

## Téléchargeur de jeux

### Introduction

Les jeux Tic-80 sont des jeux libres de droit. Recalbox inclut un script qui vous permet de télécharger les jeux souhaités directement depuis le site https://tic80.com/.

### Utilisation

Via [SSH](./../../../system/access/root-access-terminal-cli), dans le dossier `/recalbox/share/roms/tic80`, tapez `bash recalbox-tic80` pour télécharger des jeux TIC-80 facilement. Vous pouvez choisir une catégorie de jeux (démos, autres...) ou les 5 catégories proposées.