---
title: Mame
description: Multiple Arcade Machine Emulator
published: true
date: 2024-07-26T07:42:30.290Z
tags: arcade, mame
editor: markdown
dateCreated: 2023-09-01T21:10:41.252Z
---

![](/emulators/arcade/mame.svg){.align-center}

## Historia

MAME, acrónimo de "Multiple Arcade Machine Emulator", es un programa de software de emulación para PC cuyo objetivo es reproducir fielmente el funcionamiento de los juegos y sistemas arcade con el fin de preservar la historia de los videojuegos.
También incluye otros emuladores y versiones, como MESS, SDLMAME y MEWUI.
La primera versión pública se remonta a febrero de 1997 con la versión 0.1; hoy nos encontramos en la v0.230 (marzo de 2021). En la actualidad, contiene más de 4.000 juegos únicos y más de 9.000 romsets, con un flujo constante de nuevos títulos.
Abarca juegos desde 1975 hasta la actualidad.

MAME contiene varios componentes:

* varios emuladores de hardware que imitan el comportamiento de los procesadores y las placas base de los juegos arcade compatibles;
* un emulador de control que simula diferentes dispositivos (joysticks) mediante teclados u otros periféricos;
* un emulador para simular y redirigir las salidas de audio y vídeo de los terminales arcade a las salidas respectivas del ordenador.

## Emuladores

[Libretro MAME](libretro-mame)
[Libretro MAME2000](libretro-mame-2000)
[Libretro MAME2003](libretro-mame-2003)
[Libretro MAME2003_Plus](libretro-mame-2003-plus)
[Libretro MAME2010](libretro-mame-2010)
[Libretro MAME2015](libretro-mame-2015)
[AdvanceMAME](advance-mame)