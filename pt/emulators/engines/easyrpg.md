---
title: EasyRPG
description: 
published: true
date: 2024-06-02T17:12:57.296Z
tags: easyrpg, motores de jogos
editor: markdown
dateCreated: 2021-10-14T23:49:36.395Z
---

![](/emulators/fantasy/easyrpg.svg){.align-center}

## Ficha técnica

* **Fabricante:** - -
* **Ano de lançamento:** - -

## Apresentação

EasyRPG é um projeto comunitário que visa criar uma ferramenta de criação de jogos de RPG (*Role-Playing Game*) livres, interplataformas e multilíngues, inspirado em um produto comercial chamado *RPG Maker*.

O projeto é composto de várias partes:

* [EasyRPG Player](https://easyrpg.org/player/) O EasyRPG Player é um programa que permite jogar jogos criados com o RPG Maker 2000 e 2003, ele tem como objetivo ser um intérprete gratuito do RPG Maker 2000/2003 multiplataforma gratuito. O objetivo principal é jogar todos os jogos criados com ele como se fosse o intérprete original do jogo (RPG_RT).
* [Substituição da RTP](https://easyrpg.org/rtp-replacement/)
* [Ferramentas](https://easyrpg.org/tools/)
* [Editor EasyRPG](https://easyrpg.org/editor/)

Seus principais objetivos:

* Livre como em liberdade: cada componente do projeto será liberado sob uma licença livre.
* Multiplataforma: eles querem que o EasyRPG alcance o maior número possível de plataformas.
* Multi-idioma: quanto mais idiomas forem suportados, melhor.

**Contributo a este projeto**

Este é um projeto gratuito e de código aberto (FLOSS). Qualquer pessoa pode ajudar, desde programadores ou artistas até testadores e tradutores.

## Emuladores

[Libretro EasyRPG](libretro-easyrpg)