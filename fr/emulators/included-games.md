---
title: Jeux inclus
description: Liste des jeux inclus
published: true
date: 2024-06-13T16:33:16.751Z
tags: jeux, inclus
editor: markdown
dateCreated: 2022-12-21T12:35:25.704Z
---

# Liste des jeux inclus dans Recalbox 9.2

| Émulateur | Jeu | Editeur | Développeur |
| :---: | :---: | :---: | :---: |
| **Amiga 600** | Flower: For the Love of the World Enchanted Road | Black Legend | Sunny Design |
| **Amstrad CPC** | Joe Contre Les Pharaons | Amstrad 100% | Jérôme Boulinguez |
| **Amstrad CPC** | Relentless | Axelay | Axelay |
| **Amstrad CPC** | The Dawn of Kernel | Polyplay | Juan J. Martinez |
| **Amstrad CPC** | Uwol 2 | The Mojon Twins | The Mojon Twins |
| **Apple 2** | Hi-Res Adventure 1 - Mystery House | Sierra | Ken & Roberta Willians |
| **Apple 2** | Lamb Chop Source | TanRuNomad | TanRuNomad |
| **Apple 2** | Retro Fever | TanRuNomad | TanRuNomad |
| **Apple 2GS** | Cogito | Brutal Deluxe Software | Antoine VIGNAU & Olivier ZARDINI |
| **Apple 2GS** | The Tinies | Brutal Deluxe Software | Antoine VIGNAU & Olivier ZARDINI |
| **Atari 2600** | Anguna 2600 | Bite The Chili | Nathan Tolbert |
| **Atari 2600** | Cave In | Spectravision | Spectravision |
| **Atari 2600** | Halo 2600 | CGE Services Corporation | Ed Fries |
| **Atari 2600** | The End 2600 | RaymanC | RaymanC |
| **Atari 5200** | Beef Drop | AtariAge | Ken Siders & Jeff Wierer |
| **Atari 7800** | B\*nq | AtariAge | Ken Siders |
| **Atari 7800** | DungeonStalker | AtariAge | Steve Engelhardt & Mike Saarna |
| **Atari 7800** | Meteor Shower | AtariAge | Robert DeCrescenzo |
| **Atari 800** | Crownland | Piotr Wisniewski | Piotr Wisniewski |
| **Atari 800** | Ridiculous Reality | MatoSimi | MatoSimi |
| **Atari 800** | Time Pilot | New Generation 2018 | Solo & Laoo |
| **Atari Lynx** | SiIas Adventure | Krzysztof Kluczek | Krzysztof Kluczek |
| **Atari Lynx** | Super Nutmeg | RetroGuru | RetroGuru |
| **Colecovision** | MazezaM Challenge | PortableDev | PortableDev |
| **Commodore 64** | Adventure Park | Skoro & Kichi | Skoro & Kichi |
| **Commodore 64** | Adventures in Time | Kichy & Luca | Kichy & Luca |
| **Commodore 64** | Digiloi | Dr. TerrorZ | Dr. TerrorZ |
| **Commodore 64** | Rescuing Orc | Polyplay | Juan J. Martinez |
| **Commodore 64** | Trance Sector Ultimate | TND & RGCD | TND & RGCD |
| **Commodore 64** | Vortex Crystals | TND | TND |
| **Commodore 64** | Wolfling | Lazycow | Lazycow |
| **Commodore 64** | X-Force | TND | TND |
| **Commodore 64** | Xplode Man | Skoro & Kichi | Skoro & Kichi |
| **Commodore 64** | Zap Fight 2 | TND | Alf Yngve, Martin Piper, Richard Bayliss |
| **Game Boy Advance** | Anguna - Warriors Of Virtue | Nathan Tolbert | Nathan Tolbert and Chris Hildenbrand |
| **Game Boy Advance** | Gemini | Mr Smayds | Mr Smayds |
| **Game Boy Advance** | Lindsi Luna Blast | PortableDev | PortableDev |
| **Game Boy Advance** | Space Twins | 9wires | 9wires |
| **Game Boy Advance** | Uranus | ZoneGN | ZoneGN |
| **Game Boy Advance** | Uranus Zero | ZoneGN | ZoneGN |
| **Game Boy Advance** | Uranus Zero Ev | ZoneGN | ZoneGN |
| **Game Boy Advance** | Uranus2 | ZoneGN | ZoneGN |
| **Game Boy Advance** | Waimanu - Grinding Blocks Adventure | Disjointed Studios | Disjointed Studios |
| **Game Boy Color** | WingWarriors | Kitmaker Entertainment | Francisco Javier Del Pino Cuesta |
| **Game Boy Color** | Inspector Waffles Early Days | Broke Studio | Goloso Games |
| **Game Boy** | Into the Blue | Jonas Fischbach | Jonas Fischbach |
| **Game Boy** | Pong Boy Recalbox Editon | Studio 40A | MBaran |
| **Game Boy** | Retroid | Jonas Fischbach | Jonas Fischbach |
| **Game Boy** | Tuff | BonsaiDen | BonsaiDen |
| **Game Gear** | WingWarriors | Kitmaker Entertainment | Francisco Javier Del Pino Cuesta |
| **GX-4000** | Baba's Palace | Rafael Castillo, John McKlain & Dragon | Rafael Castillo, John McKlain & Dragon |
| **Intellivision** | Deep Zone | Artrag | Artrag |
| **Intellivision** | Hotel Bunny | Sebastian Mihai | Sebastian Mihai |
| **Intellivision** | Princess Lydie | Marco Marrero | Marco Marrero |
| **Mame** | Gridlee | Videa, Inc. | Videa |
| **Mame** | Looping | Video Games GmbH | Video Games GmbH |
| **Mame** | Super Tank | SNK | Video Games GmbH |
| **Mame** | The Adventures of Robby Roto! | Dave Nutting Associates | Dave Nutting Associates |
| **Master System** | Astro Force | Enrique Ruiz | Enrique Ruiz |
| **Master System** | Digger Chan | Playgeneration | Playgeneration |
| **Master System** | Galactic Revenge | Mikgames | Enrique Ruiz |
| **Master System** | Papi Commando in Ceplusplus Land | Studio Vetea | Studio Vetea |
| **Master System** | Pong Master 4k | Haroldo Pinheiro | Haroldo Pinheiro |
| **Master System** | Silver Valley | Mikgames | Enrique Ruiz |
| **Master System** | WingWarriors | Kitmaker Entertainment | Francisco Javier Del Pino Cuesta |
| **Megadrive** | Astebros | Neofid Studios | Neofid Studios |
| **Megadrive** | Bomb on Basic City : Special Edition | Studio Vetea | Studio Vetea |
| **Megadrive** | Bug Hunt | The AfroMonkeys | The AfroMonkeys |
| **Megadrive** | Demons Of Asteborg (Demo) | Neofid Studios | Neofid Studios |
| **Megadrive** | Gluf | RetroSouls | RetroSouls |
| **Megadrive** | Griel's Quest for the Holy Porron | Mun | Mun |
| **Megadrive** | L'Abbaye des Morts | Locomalito | Mun |
| **Megadrive** | Misplaced | RetroSouls | Denis Grachev |
| **Megadrive** | Old Towers | RetroSouls | Denis Grachev |
| **Megadrive** | Papi Commando | Studio Vetea | Studio Vetea |
| **Megadrive** | Yazzie | RetroSouls | RetroSouls |
| **MSX1** | Night Knight | Polyplay | Juan J. Martinez |
| **MSX1** | Wingwarriors | Kitmaker Entertainment | Francisco Javier Del Pino Cuesta |
| **MSX1** | XRacing | Santi Ontañón Villar | Santi Ontañón Villar |
| **MSX1** | XSpelunker | Santi Ontañón Villar | Santi Ontañón Villar |
| **MSX1** | Yazzie | RetroSouls | RetroSouls |
| **MSX2** | Amores de Brunilda ,Los | RetroWorks | RetroWorks |
| **MSX2** | The Sword of Ianna | RetroWorks | RetroWorks |
| **NES** | 2048 | tsone | tsone |
| **NES** | Basse Def Adventures (Demo) | Omaké Games | Broke Studio |
| **NES** | Blade Buster | High Level Challenge | High Level Challenge |
| **NES** | Böbl | Morphcat Games | Morphcat Games |
| **NES** | Chase | Shiru | Shiru |
| **NES** | Cheril Perils Classic | Mojontwins | Mojontwins |
| **NES** | Driar | Stefan Adolfsson | Stefan Adolfsson |
| **NES** | Kubo 3 | SJ Games | SJ Games |
| **NES** | Lan Master | Shiru | Shiru |
| **NES** | Lawn Mower | Shiru | Shiru |
| **NES** | Lizard (Demo) | Broke Studio | Brad Smith |
| **NES** | Micro Mages | Morphcat Games | Morphcat Games |
| **NES** | Nebs 'n Debs (Demo) | Broke Studio | Dullahan Software |
| **NES** | Nova The Squirrel | NovaSquirrel | NovaSquirrel |
| **NES** | Project Blue (Demo) | Broke Studio | Toggleswitch |
| **NES** | Sir Ababol | Mojontwins | Shiru |
| **NES** | Streemerz | Faux Game | Faux Game |
| **NES** | Super Bat Puncher (Demo) | Morphcat Games | Morphcat Games |
| **NES** | Tiger Jenny | Ludosity | Ludosity |
| **NES** | Twin Dragons (Demo) | Broke Studio | Antoine Gohin |
| **NES** | Underground Adventure | dale_coop | dale_coop |
| **NES** | ZDEY The Game | Dale Coop & Raftronaut | Art'cade |
| **NES** | Zooming Secretary | Shiru | Shiru |
| **Neo-Geo** | Gladmort | PixelHeart | PixelHeart |
| **Oric Atmos** | Oricium | Defense Force | José María (Chema) Enguita |
| **Oric Atmos** | Pulsoids | Oric.org | Jonathan Bristow |
| **PC-Engine** | Reflectron | Aetherbyte | Andrew Darovich |
| **PC-Engine** | Santatlantean | Aetherbyte | Andrew Darovich |
| **Pokémon Mini** | Cortex | PlayerOne | PlayerOne |
| **Pokémon Mini** | Galactix | Lupin, Okkie, p0p | Lupin, Okkie, p0p |
| **Pokémon Mini** | Psychic Seeds | Justburn & Palkone | Justburn & Palkone |
| **Ports** | 2048 | Libretro | Gabriele Cirulli |
| **Ports** | CaveStory | Pixel Studios | Pixel Studios |
| **Ports** | Craft | Libretro | Michael Fogleman |
| **Ports** | Dinothawr | Libretro | Agnes Heyer & Hans-Kristian Arntzen |
| **Ports** | DOOM | GT Interactive | id Software |
| **Ports** | Mr. Boom | Remdy Software | Remdy Software |
| **Ports** | Pong | Libretro | Brad Parker |
| **Ports** | Quake | Activision | id Software |
| **Ports** | Quake 2 | Activision | id Software |
| **Ports** | Rick Dangerous | Microplay Software | Core Design |
| **Ports** | Sigil | Limited Run Games | John Romero |
| **Ports** | Wolfenstein 3D | Apogee | id Software |
| **ScummVM** | Beneath a Steel Sky (English) | Virgin | Revolution Software Ltd. |
| **ScummVM** | Flight of the Amazon Queen | Renegade Software | Interactive Binary Illusions |
| **ScummVM** | Lure Of The Temptress | Virgin | Revolution Software Ltd. |
| **ScummVM** | Soltys | L.K. Avalon | L.K. Avalon |
| **SG-1000** | Cheril Perils Classic | Mojontwins | Mojontwins |
| **SNES** | Uwol Quest for Money | PortableDev | PortableDev |
| **Solarus** | Yarntown | Solarus | Max Mraz |
| **Tic-80** | 8 Bit Panda | Bruno Oliveira | Bruno Oliveira |
| **Tic-80** | Cauliflower Power | librorumque | librorumque |
| **Tic-80** | Secret Agents |  |  |
| **Tic-80** | Shadow Over The Twelve Lands | Bruno Oliveira | Bruno Oliveira |
| **Tic-80** | Todor | András Parditka | András Parditka |
| **Vectrex** | Armor Attack | GCE | Cinematronics |
| **Vectrex** | Mine Storm | GCE | GCE |
| **Vectrex** | Scramble Wars | GCE | Konami |
| **Vectrex** | Thrust | Pakrat | Ville Krumlinde |
| **Vectrex** | VecZ | Madtronix | René Bauer |
| **ZX Spectrum** | Cray 5 | RetroWorks | RetroWorks |
| **ZX Spectrum** | Genesis - Dawn of a New Day | RetroWorks | RetroWorks |
| **ZX Spectrum** | Gommy, Defensor Medieval | RetroWorks | RetroWorks |
| **ZX Spectrum** | Los Amores de Brunilda | RetroWorks | RetroWorks |
| **ZX Spectrum** | The Sword of Ianna | RetroWorks | RetroWorks |