---
title: Conectar una minipantalla LCD a través de la interfaz I2C
description: 
published: true
date: 2023-09-14T14:59:45.904Z
tags: lcd, i2c, mini, pantalla
editor: markdown
dateCreated: 2023-09-14T14:59:45.904Z
---

## Requisitos

Una pantalla LCD I2c como la Hd44780 en versión A00 (soporte ascii + caracteres japoneses) o A02 (soporte ascii + caracteres europeos).

![](/tutorials/video/tft/hd44780.jpg)

## Instalación

### Conectando el I2C a un GPIO de la Raspberry Pi

Conexión del I2C a una Raspberry Pi

![](/tutorials/video/tft/i2c-gpio.png)

### Activación del I2C en Recalbox

* Modificad [recalbox-user-config.txt](./../../../tutorials/system/modification/configtxt-configuration) con nano o vim y añadid estas líneas:

```ini
#Activate I2C
dtparam=i2c1=on
dtparam=i2c_arm=on
```

* Modificad el fichero `/boot/cmdline.txt` con nano o vim y añadid estas líneas:

```ini
bcm2708.vc_i2c_override=1
```

* Reiniciad Recalbox

### Vérifier l'adresse de l'I2C

Necesitarás conocer la dirección de tu I2C** para ejecutar algunos scripts de visualización. Como regla general, la dirección es `0x27` o `0x3f`.

Para comprobarlo:

* Ejecuta el siguiente comando; ¡tarda un rato!
  * en modelos Raspberry muy antiguos: `i2cdetect -y 0`. 
  * en modelos Raspberry más recientes (pi4, pi3, pi2): `i2cdetect -y 1`.

```shell

     0  1  2  3  4  5  6  7  8  9  a  b  c  d  e  f
00:          -- -- -- -- -- -- -- -- -- -- -- -- --
10: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
20: -- -- -- -- -- -- -- 27 -- -- -- -- -- -- -- --
30: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
40: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
50: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
60: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
```

La dirección devuelta por el comando en la tabla es: `0x27`.

## Script de visualización disponible para Recalbox.

Este script está disponible en esta [discusión](https://forum.recalbox.com/topic/8689/script-clcd-display-on-recalbox).

![](/tutorials/video/tft/hd4470-result.jpg)