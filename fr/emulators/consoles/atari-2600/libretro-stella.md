---
title: Libretro Stella
description: 
published: true
date: 2024-06-23T11:23:08.972Z
tags: libretro, atari-2600, atari, 2600, stella
editor: markdown
dateCreated: 2021-05-21T08:19:28.299Z
---

**Libretro Stella** est un émulateur Atari multiplateforme.

## ![](/emulators/license.svg) Licence

Ce core est sous licence [**GPLv2**](https://github.com/stella-emu/stella/blob/master/License.txt).

## ![](/emulators/compatibility.png) Compatibilité

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Fonctionnalités

| Fonctionnalité | Supporté |
| :---: | :---: |
| Redémarrage | ✔ |
| Captures d'écran | ✔ |
| Sauvegardes instantanées | ✔ |
| Rembobinage | ✔ |
| Netplay | ✔ |
| RetroAchievements | ✔ |
| Controls | ✔ |
| Remapping | ✔ |

## ![](/emulators/bios.svg) BIOS

>**Aucun bios n'est requis.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Extensions supportées

Les roms doivent avoir les extensions suivantes :

* .a26
* .bin
* .zip
* .7z

Ce système supporte les roms compressées au format .zip/.7z. Attention toutefois, il ne s'agit que d'une archive.

Les fichiers contenus dans les .zip/.7z doivent correspondre aux extensions citées précédemment.
De plus, chaque fichier .zip/.7z ne doit contenir qu'une seule rom compressée.

### Emplacement

Placez les roms comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 atari2600
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.zip**

>Les roms au format **No-Intro** sont vivement conseillées.
{.is-success}

>Pour plus d'information sur les roms, rendez-vous sur [ce tutoriel](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuration avancée de l'émulateur

>Pour pouvoir conserver vos configurations personnalisées lors d'une mise à jour, nous vous conseillons d'utiliser notre fonctionnalité [Surcharges de configuration](./../../../advanced-usage/configuration-override).
{.is-info}

### Accéder aux options

Vous pouvez configurer diverses options de deux façons différentes.

* Via le menu RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via le fichier `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Options du core

| Options | Valeur des options | Variable (pour surcharge) | Valeurs des variables |
| --- | --- | --- | --- |
| Console display | `auto` ✅ / `ntsc` / `pal` / `secam` / `ntsc50` / `pal60` / `secam60` | `stella_console` | `auto` / `ntsc` / `pal` / `secam` / `ntsc50` / `pal60` / `secam60` |
| Palette colors | `standard` ✅ / `z26` / `user` / `custom` | `stella_palette` | `standard` / `z26` / `user` / `custom` |
| TV effects | `Désactivé` ✅ / `composite` / `s-video` / `rgb` / `badly adjusted` | `stella_filter` | `disabled` / `composite` / `s-video` / `rgb` / `badly adjusted` |
| NTSC aspect % | `par` ✅ / `50` / `75` / `76` / `77` / `78` / `79` / `80` / `81` / `82` / `83` / `84` / `85` / `86` / `87` / `88` / `89` / `90` / `91` / `92` / `93` / `94` / `95` / `96` / `97` / `98` / `99` / `100` / `101` / `102` / `103` / `104` / `105` / `106` / `107` / `108` / `109` / `110` / `111` / `112` / `113` / `114` / `115` / `116` / `117` / `118` / `119` / `120` / `121` / `122` / `123` / `124` / `125` | `stella_ntsc_aspect` | `par` / `50` / `75` / `76` / `77` / `78` / `79` / `80` / `81` / `82` / `83` / `84` / `85` / `86` / `87` / `88` / `89` / `90` / `91` / `92` / `93` / `94` / `95` / `96` / `97` / `98` / `99` / `100` / `101` / `102` / `103` / `104` / `105` / `106` / `107` / `108` / `109` / `110` / `111` / `112` / `113` / `114` / `115` / `116` / `117` / `118` / `119` / `120` / `121` / `122` / `123` / `124` / `125` |
| PAL aspect % | `par` ✅ / `50` / `75` / `76` / `77` / `78` / `79` / `80` / `81` / `82` / `83` / `84` / `85` / `86` / `87` / `88` / `89` / `90` / `91` / `92` / `93` / `94` / `95` / `96` / `97` / `98` / `99` / `100` / `101` / `102` / `103` / `104` / `105` / `106` / `107` / `108` / `109` / `110` / `111` / `112` / `113` / `114` / `115` / `116` / `117` / `118` / `119` / `120` / `121` / `122` / `123` / `124` / `125` | `stella_pal_aspect` | `par` / `50` / `75` / `76` / `77` / `78` / `79` / `80` / `81` / `82` / `83` / `84` / `85` / `86` / `87` / `88` / `89` / `90` / `91` / `92` / `93` / `94` / `95` / `96` / `97` / `98` / `99` / `100` / `101` / `102` / `103` / `104` / `105` / `106` / `107` / `108` / `109` / `110` / `111` / `112` / `113` / `114` / `115` / `116` / `117` / `118` / `119` / `120` / `121` / `122` / `123` / `124` / `125` |
| Crop horizontal overscan | `Désactivé` ✅ / `Activé` | `stella_crop_hoverscan` | `disabled` / `enabled` |
| Stereo sound | `auto` ✅ / `Désactivé` / `Activé` | `stella_stereo` | `auto` / `off` / `on` |
| Phosphor mode | `auto` ✅ / `Désactivé` / `Activé` | `stella_phosphor` | `auto` / `off` / `on` |
| Phosphor blend % | `0` / `5` / `10` / `15` / `20` / `25` / `30` / `35` / `40` / `45` / `50` / `55` / `60` ✅ / `65` / `70` / `75` / `80` / `85` / `90` / `95` / `100` | `stella_phosphor_blend` | `0` / `5` / `10` / `15` / `20` / `25` / `30` / `35` / `40` / `45` / `50` / `55` / `60` / `65` / `70` / `75` / `80` / `85` / `90` / `95` / `100` |
| Paddle joypad sensitivity | `1` / `2` / `3` ✅ / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` | `stella_paddle_joypad_sensitivity` | `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` |

## ![](/emulators/external-links.png) Liens externes

* **Code source utilisé :** [https://github.com/stella-emu/stella/](https://github.com/stella-emu/stella/)
* **Documentation Libretro :** [https://docs.libretro.com/library/stella/](https://docs.libretro.com/library/stella/)