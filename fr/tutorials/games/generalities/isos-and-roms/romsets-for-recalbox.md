---
title: Les romsets pour Recalbox
description: 
published: true
date: 2023-09-11T16:51:34.987Z
tags: romsets
editor: markdown
dateCreated: 2021-05-21T08:48:31.282Z
---

**Légende:**

  🥇    ==> Set à utiliser
  🥈    ==> Pour compléter (pour les roms hack, traduction, etc...)
  🥉    ==> En dernier recours
  🎮    ==> Set recommandé pour le Netplay
  ❌    ==> Pas de set disponible pour ce groupe

## Arcade

Pour l'arcade, le romset est différent selon le core que vous utilisez. Vous trouverez toutes les versions des romsets à utiliser sur cette [page](./../../../../emulators/arcade).

>Pour plus d'informations sur les roms Arcade, veuillez consulter [cette page](./../../../../advanced-usage/arcade-in-recalbox).
{.is-info}

## Consoles de salon

| Plateforme | No-Intro | TOSEC | Redump | Trurip |
| :---: | :---: | :---: | :---: | :---: |
| **32x** | 🥇 🎮 | 🥈 | ❌ | 🥉 |
| **Amiga CD32** | ❌ | 🥈 | 🥇 | 🥉 |
| **Amiga CDTV** | ❌ | 🥈 | 🥇 | 🥉 |
| **Amstrad GX4000** | ❌ | 🥇 | ❌ | 🥈 |
| **Atari 2600** | 🥇 🎮 | 🥈 | ❌ | ❌ |
| **Atari 5200** | 🥇 | 🥈 | ❌ | 🥉 |
| **Atari 7800** | 🥇 | 🥈 | ❌ | 🥉 |
| **Atari Jaguar** | 🥇 🎮 | 🥈 | ❌ | 🥉 |
| **ColecoVision** | 🥇 | 🥈 | ❌ | 🥉 |
| **Dreamcast** | ❌ | 🥇 | 🥈 | 🥉 |
| **Fairchild Channel F** | 🥇 | 🥈 | ❌ | 🥉 |
| **Famicom Disk System** | 🥇 🎮 | 🥈 | ❌ | 🥉 |
| **GameCube** | ❌ | 🥈 | 🥇 🎮 | 🥉 |
| **Intellivision** | 🥇 | 🥈 | ❌ | 🥉 |
| **Master System** | 🥇 🎮 | 🥈 | ❌ | 🥉 |
| **Megadrive** | 🥇 🎮 | 🥈 | ❌ | 🥉 |
| **Multivision** | ❌ | 🥇 | ❌ | ❌ |
| **NeoGeo CD** | ❌ | ❌ | 🥇 | 🥇 |
| **NES** | 🥇 🎮 | 🥈 | ❌ | ❌ |
| **Nintendo 64** | 🥇 | 🥈 | ❌ | ❌ |
| **Nintendo 64DD** | 🥇 | 🥈 | ❌ | ❌ |
| **Panasonic 3DO** | ❌ | 🥈 | 🥇 | 🥉 |
| **Pc-Engine** | 🥇 🎮 | 🥈 | ❌ | 🥉 |
| **Pc-Engine CD** | ❌ | 🥈 | 🥇 | 🥉 |
| **PCFX** | ❌ | 🥉 | 🥇 | 🥈 |
| **PlayStation 1** | ❌ | 🥉 | 🥇 | 🥈 |
| **PlayStation 2** | ❌ | 🥉 | 🥇 | 🥈 |
| **Satellaview** | 🥇 | 🥉 | ❌ | ❌ |
| **Saturn** | ❌ | 🥉 | 🥇 | 🥈 |
| **Sega Mega CD** | ❌ | 🥉 | 🥇 | 🥈 |
| **Sega SG1000** | 🥇 🎮 | 🥉 | ❌ | ❌ |
| **Super Famicom** | 🥇 | 🥈 | ❌ | 🥉 |
| **SuFami Turbo** | 🥇 | 🥈 | ❌ | ❌ |
| **Super Nintendo** | 🥇 🎮 | 🥈 | ❌ | ❌ |
| **SuperGrafx** | 🥇 🎮 | 🥈 | ❌ | 🥉 |
| **Vectrex** | 🥇 | 🥈 | ❌ | 🥉 |
| **VideoPac** | 🥇 | 🥈 | ❌ | 🥉 |
| **Virtual Boy** | 🥇 | 🥈 | ❌ | 🥉 |
| **Wii** | ❌ | 🥈 | 🥇 🎮 | ❌ |

## Consoles portables

| Plateforme | No-Intro | Tosec | Redump | Trurip |
| :---: | :---: | :---: | :---: | :---: |
| **Game and Watch** | ❌ | ❌ | ❌ | ❌ |
| **Game Boy** | 🥇 | 🥈 | ❌ | 🥉 |
| **Game Boy Color** | 🥇 | 🥈 | ❌ | 🥉 |
| **Game Boy Advance** | 🥇 | 🥈 | ❌ | 🥉 |
| **Game Gear** | 🥇 | 🥈 | ❌ | 🥉 |
| **Lynx** | 🥇 | 🥈 | ❌ | 🥉 |
| **Nintendo DS** | 🥇 | ❌ | ❌ | ❌ |
| **NeoGeo Pocket** | 🥇 | 🥈 | ❌ | 🥉 |
| **NeoGeo Pocket Color** | 🥇 | 🥈 | ❌ | 🥉 |
| **Palm OS** | 🥇 | ❌ | 🥈 | 🥉 |
| **Pokémini** | 🥇 | 🥈 | ❌ | 🥉 |
| **PSP** | 🥇 | ❌ | 🥈 | ❌ |
| **Wonderswan** | 🥇 | 🥈 | ❌ | 🥉 |
| **Wonderswan Color** | 🥇 | 🥈 | ❌ | 🥉 |

## Ordinateurs

| Plateforme | No-Intro | Tosec | Redump | Trurip |
| :---: | :---: | :---: | :---: | :---: |
| **Amiga 600** | ❌ | 🥇 | ❌ | ❌ |
| **Amiga 1200** | ❌ | 🥇 | ❌ | ❌ |
| **Amstrad CPC** | 🥈 | 🥇 | ❌ | 🥉 |
| **Apple II** | ❌ | 🥇 | ❌ | 🥈 |
| **Apple II Gs** | ❌ | 🥇 | ❌ | 🥈 |
| **Atari 800** | ❌ | 🥇 | ❌ | 🥈 |
| **Atari St** | 🥈 | 🥇 | ❌ | 🥉 |
| **Commodore 64** | 🥈 | 🥇 | ❌ | 🥉 |
| **Dragon 32/64** | ❌ | 🥇 | ❌ | ❌ |
| **Elektronika BK** | ❌ | 🥇 | ❌ | ❌ |
| **MSX 1** | 🥈 | 🥇 | ❌ | 🥉 |
| **MSX 2** | 🥈 | 🥇 | ❌ | 🥉 |
| **MSX Turbo R** | 🥈 | 🥇 | ❌ | 🥉 |
| **NEC Pc8801** | ❌ | 🥈 | 🥇 | 🥉 |
| **NEC Pc9801** | ❌ | 🥈 | 🥇 | 🥉 |
| **Oric Atmos** | ❌ | 🥇 | ❌ | ❌ |
| **Samcoupé** | ❌ | 🥇 | ❌ | 🥈 |
| **Sharp X1** | ❌ | 🥇 | ❌ | 🥈 |
| **Sharp X68000** | ❌ | 🥈 | 🥇 | 🥉 |
| **Sinclair ZXspectrum** | 🥇 | 🥈 | ❌ | 🥉 |
| **Sinclair ZX81** | ❌ | 🥇 | ❌ | 🥈 |
| **Spectravideo SV-318** | ❌ | 🥇 | ❌ | 🥈 |
| **Tandy TRS-80 Color Computer** | ❌ | 🥇 | ❌ | ❌ |
| **Texas Instruments TI-99/4A** | ❌ | 🥇 | ❌ | ❌ |
| **Thomson MO/TO** | ❌ | 🥇 | ❌ | 🥈 |