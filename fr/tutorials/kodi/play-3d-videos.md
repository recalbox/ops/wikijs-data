---
title: Lire les vidéos 3D
description: 
published: true
date: 2022-12-27T15:09:12.731Z
tags: kodi, videos, 3d
editor: markdown
dateCreated: 2022-12-27T15:09:12.731Z
---

## Introduction

Parmi pleins de fonctionnalités, Kodi permet entre-autres de pouvoir lire des vidéos 3D sur les téléviseurs compatibles 3D depuis sa version 13 avec les lunettes adaptées fourni par le fabricant de votre téléviseur.

Les vidéos 3D supportées sont les suivantes :

* Côte à côte (Side by side - SBS)
* Haut et bas (Top and bottom - TAB)

Les films 3D anaglyphes n'ont pas de configuration particulière et peuvent être lues comme n'importe quel autre film classique.

Chaque marque de téléviseur a une certaine compatibilité question rendu 3D. Vous pouvez effectuer des tests à partir du contenu ci-dessous avec les vidéos du site https://www.demolandia.net/3d-video/brands.html.

## Utilisation

### Renommage des films

Pour pouvoir utiliser Kodi pour visionner des films 3D, vous devez en premier lieu savoir si c'est un film 3D avec les images côte à côte (SBS : side by side) ou haut et bas (TAB : top and bottom). En fonction de cette particularité, vous allez devoir renommer votre film en suivant cet exemple :

```text
votrefilm.3d.sbs.mp4
```

La partie `3d.sbs` est importante, c'est elle qui va vous proposer de visionner votre film de façon correcte en 3D sans vous tromper dans les réglages.

Voici un tableau de quelques exemples de réglages existants :

| 3D | Côte à côte | Dessus / Dessous |
| :---: | :---: | :---: |
| .3D. | .sbs. | .tab. |
| -3d- | .SBS. | .TAB. |
| 3d | .HSBS. | .HTAB. |
| \_3d_ | -hsbs- | -htab- |
| .3D- | \_sbs_ | \_tab_ |
| 3D. | SBS | TAB |
| .3D | sbs | tab |
|  | -SBS | -TAB |
|  | HSBS_ | HTAB_ |

Exemples de noms de films :

* Mon film 3D.SBS.mp4
* Mon filmd 3d.sbs.mp4
* Mon film 3D-TAB.mp4

### Lecture des films

Une fois votre film renommé, vous allez le chercher dans Kodi dans la partie `Films`. Une fois trouvé, lancez-le et vous aurez un menu vous demandant le type de vidéo 3D à appliquer :

* Mode préféré (Comme le film)
* Monoscopique / 2D

En fonction du renommage de votre fichier et de choisir l'option `Monoscopique / 2D`, votre film sera visible en 3D avec la paire de lunettes adéquate correctement (pas d'image dédoublée avec les lunettes).