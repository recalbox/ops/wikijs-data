---
title: Z-Machine
description: 
published: true
date: 2024-07-26T08:25:04.791Z
tags: 9.0+, z-machine, infocom, motor de juego
editor: markdown
dateCreated: 2023-09-04T07:21:12.740Z
---

![](/emulators/fantasy/zmachine.svg){.align-center}

## Detalles técnicos

* **Desarrollador:** Joel Berez y Marc Blank
* **Año de lanzamiento:** 1979
* **Hardware:** teclado
* **Pantalla:** 640x480 píxeles

## Presentación

La Z-machine es una máquina virtual desarrollada por Joel Berez y Marc Blank en 1979, y utilizada por Infocom para sus juegos de aventuras de texto.

El juego de aventuras de texto Zork fue creado originalmente para miniordenadores PDP-10 por Dave Lebling y Marc Blank, entonces estudiantes del MIT. Tras el éxito del juego, que se difundió a través de Arpanet, ayudaron a crear Infocom, una empresa que desarrollaba juegos de aventuras de texto. La idea era adaptar Zork a los ordenadores personales, pero el juego original, codificado en MDL, pesaba 1 MB, demasiado para los microordenadores de la época. Por ello, se dividió en tres partes (Zork I, II y III) y se creó un lenguaje de programación, ZIL (Zork Implementation Language), a partir de MDL.

Para poder comercializar el juego en un gran número de plataformas y resolver los problemas de portabilidad, se decidió crear una máquina virtual, un concepto relativamente nuevo en aquella época, capaz de ejecutar Zork y cualquier otro juego codificado en ZIL. El juego, compilado para esta máquina, podía ejecutarse en cualquier tipo de microordenador con un programa capaz de interpretar correctamente sus especificaciones. Así fue como los juegos de Infocom se lanzaron en un gran número de PC norteamericanos (Atari 400/800, CP/M, IBM PC, TRS-80 Model II y III, NEC APC, DEC Rainbow, Commodore 64, TI Professional, DECmate, Tandy-2000, Kaypro II, Osborne 1, MS-DOS, TI 99/4A, Apple Macintosh, Epson QX-10, Apricot, Atari ST, Amiga; nótese la ausencia del Amstrad CPC o del Oric, más populares en Europa.

Infocom desarrolló varias versiones de la máquina Z durante la década de 1980. La versión 3 se lanzó en 1982, permitiendo jugar a juegos de hasta 128kb; los juegos desarrollados para esta versión fueron los de la serie "Standard". La versión 4 apareció en 1985, con A Mind Forever Voyaging, Trinity y otros juegos de la serie "Plus". La versión 5 apareció en 1987 con Beyond Zork, e introdujo nuevas características y opcodes para mejorar la máquina Z (juegos de 256KB o efectos de tiempo).

Tras comprar Infocom y ver que las ventas se ralentizaban, Activision quiso aumentar el ritmo de producción y la rentabilidad. Esto llevó a la introducción de nuevas características, no necesariamente muy exitosas, que iban más allá de los juegos puramente basados en texto.
En 1988 se lanzó la versión 6 del Z-machine, que añadía capacidades gráficas y gestión del ratón, lo que lo alejaba de los juegos tradicionales de Infocom; sólo se lanzaron cuatro juegos antes del cierre del estudio.

## Emuladores

[Frotz](frotz)
[Libretro-mojozork](libretro-mojozork)