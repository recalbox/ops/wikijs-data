---
title: Configure a TFT HDMI screen
description: 
published: true
date: 2021-08-06T22:38:19.184Z
tags: hdmi, tft, screen
editor: markdown
dateCreated: 2021-08-06T22:38:19.184Z
---

## Configure your 5" TFT HDMI display

You can find this display for purchase [here](https://www.banggood.com/5-Inch-800-x-480-HD-TFT-LCD-Touch-Screen-For-Raspberry-PI-2-Model-B-or-B+-or-A+-or-B-p-1023438.html). It is the equivalent of the [Adafruit device](https://learn.adafruit.com/adafruit-5-800x480-tft-hdmi-monitor-touchscreen-backpack/overview).

You can make it work on Recalbox by configuring the file [recalbox-user-config.txt](./../../system/modification/configtxt-configuration)

Remember that TFP401 driver has no video scaler! If you don't feed it exactly 800×480 pixels, the image won't stretch and will shrink to fit! So we need to configure the resolution in the `recalbox-user-config.txt` file.

* Make the [boot partition](./../../system/modification/configtxt-configuration) writable.
* Modify the `recalbox-user-config.txt` file with nano or vim and add this:

```ini
# uncomment to force a specific HDMI mode (here we are forcing 800x480!)
hdmi_group=2
hdmi_mode=1
hdmi_mode=87
hdmi_cvt=800 480 60 6 0 0 0
 
max_usb_current=1
```

The line `max_usb_current=1` increases the maximum USB output current of the Pi. This way, your Pi will provide enough power to your display. Make sure that the main power supply is powerful enough for all your devices.

If the image does not have the correct resolution when emulated, you will need to modify the `recalbox.conf` file and change the `global.videomode` variable:

```ini
global.videomode=default
```

To use the default configuration of the `config.txt` file.

See [this message](https://forum.recalbox.com/topic/4539/how-to-config-portable-5-inch-screen-pics-inside) to see this in pictures.

## Configure your 7" TFT HDMI display

You can find one for a decent price [here](https://www.waveshare.com/7inch-HDMI-LCD-C.htm) or [there](https://www.waveshare.com/wiki/7inch_HDMI_LCD_%28C%29).

You can make it work on Recalbox by configuring the file [recalbox-user-config.txt](./../../system/modification/configtxt-configuration)

Remember that TFP401 driver has no video scaler! If you don't feed it exactly 800×480 pixels, the image won't stretch and will shrink to fit! So we need to configure the resolution in the `recalbox-user-config.txt` file.

* Connect to your Pi via [SSH](./../../system/access/root-access-terminal-cli).
* Make the [boot partition](./../../system/modification/configtxt-configuration) writable.
* Modify the `recalbox-user-config.txt` file with nano or vim and add this:

```ini
config_hdmi_boost=7
hdmi_max_current=1
# uncomment to force a specific HDMI mode (here we are forcing 2014x600!)
hdmi_group=2
hdmi_mode=1
hdmi_mode=87
hdmi_cvt=1024 600 60 6 0 0 0
display_rotate=0
max_usb_current=1
hdmi_drive=1
hdmi_ignore_edid=0xa5000080
```

The `max_usb_current=1` line increases the Pi's maximum USB output current if your display is USB powered. Use `hdmi_max_current=1` if the display is powered by HDMI. This way, your Pi will provide enough power to your display. Make sure the main power supply is powerful enough for all your devices (5V-3A). For a larger screen (10" and more), make sure you use an external power supply.

If the image does not have the correct resolution when emulated, you will need to modify the `recalbox.conf` file and change the `global.videomode` variable:

```ini
global.videomode=default
```

You can set the custom mode through [this page](https://www.raspberrypi.org/documentation/configuration/config-txt/video.md).