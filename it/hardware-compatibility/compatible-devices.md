---
title: Compatibilità delle Periferiche
description: 
published: true
date: 2021-06-19T14:52:53.673Z
tags: 
editor: markdown
dateCreated: 2021-06-19T14:52:53.673Z
---

Questo è un progetto realizzato da diversi membri della comunità per condividere tutti i dispositivi che funzionano "plug and play", ed anche i dispositivi che non lo fanno.

Sentiti libero di fare qualsiasi aggiunta o modifica da apportare, per creare un ricco database di compatibilità per Recalbox!