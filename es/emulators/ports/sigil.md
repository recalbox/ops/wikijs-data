---
title: Sigil
description: Doom 2019 en version Gore
published: true
date: 2024-07-26T08:31:19.700Z
tags: ports, sigil, gore
editor: markdown
dateCreated: 2023-09-04T12:50:37.787Z
---

![](/emulators/ports/sigil.png){.align-center}

## Ficha técnica

* **Creado por**:
* **Año de lanzamiento**: 2019
* **Motor**: id Tech 1 de Doom (1993)
* **Desarrollador**: John Romero
* **Diseñador**: John Romero
* **Editor**: Romero Games
* **Música**: James Paddock, Buckethead

## Presentación

**Sigil** (estilizado como **SIGIL**) es la quinta entrega no oficial del videojuego de 1993, Doom. Publicado por Romero Games el 31 de mayo de 2019, el Megawad fue creado por uno de los autores de Doom, John Romero, independientemente del actual propietario del juego principal, Bethesda Softworks. Contiene nueve misiones, cachun teniendo una versión death match y una nueva banda sonora creada por James Paddock y Buckethead.

## Emuladores

[Libretro PrBoom](libretro-prboom)