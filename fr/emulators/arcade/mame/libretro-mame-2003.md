---
title: Libretro MAME 2003
description: 
published: true
date: 2024-06-25T16:23:49.072Z
tags: libretro, mame, mame2003
editor: markdown
dateCreated: 2021-05-21T08:13:38.603Z
---

**Libretro MAME2003** est un émulateur plus récent que imame4all. Beaucoup plus de jeux sont fonctionnels.

## ![](/emulators/license.svg) Licence

Ce core est sous licence [**MAME non-commerciale**](https://github.com/libretro/mame2003-libretro/blob/master/LICENSE.md).

## ![](/emulators/compatibility.png) Compatibilité

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :----: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Fonctionnalités

| Fonctionnalité | Supporté |
| :---: | :---: |
| Sauvegardes | ✔ |
| Sauvegardes instantanées | Selon les jeux |
| Rembobinage | ✔ |
| Netplay | ✔ |
| Native Cheats | ✔ |
| Controllers | ✔ |
| Multi-Mouse | ✔ |

## ![](/emulators/bios.svg) BIOS

Les romsets BIOS ne sont pas nécessaires lorsque vous utilisez des romsets d'arcade "Full Non-Merged". Pour les roms "Split" et "Non-Merged", placez les BIOS nécessaires dans le même répertoire que le romset du jeu.

>**Remarque** :
>Placez les bios/devices dans le répertoire : `/recalbox/share/roms/mame/`
>ou dans son sous-répertoire si vous utilisez plusieurs cores de Mame, par exemple :`/recalbox/share/roms/mame/Mame2003/`
Les jeux et leurs bios doivent être dans le même répertoire ou sous-répertoire.
{.is-warning}

## ![](/emulators/roms.png) Roms

* Basé sur le romset : MAME 0.78 (Décembre 2003)
* Fichier dat : `/recalbox/share/arcade/libretro/mame2003.dat`.

### Emplacement

Placez les roms comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 mame
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.zip**
┃ ┃ ┃ ┃ ┣ 📁 **jeu**
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **jeu.chd**

Vous pouvez aussi opter pour le sous-répertoire déjà présent `mame2003` (utile si vous voulez avoir un set mame pour un autre core). Ceci permettra de lancer le bon émulateur automatiquement.

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 mame
┃ ┃ ┃ ┃ ┣ 📁 mame2003
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **jeu.zip**
┃ ┃ ┃ ┃ ┃ ┣ 📁 **jeu**
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 **jeu.chd**

## ![](/emulators/advanced-configuration.png) Configuration avancée de l'émulateur

>Pour pouvoir conserver vos configurations personnalisées lors d'une mise à jour, nous vous conseillons d'utiliser notre fonctionnalité [Surcharges de configuration](./../../../advanced-usage/configuration-override).
{.is-info}

### Accéder aux options

Vous pouvez configurer diverses options de deux façons différentes.

* Via le Menu RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via le fichier `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Options du core

| Options | Valeur des options | Variable (pour surcharge) | Valeurs des variables |
| --- | --- | --- | --- |
| Skip Disclaimer | `Désactivé` ✅ / `Activé` | `mame2003_skip_disclaimer` | `disabled` / `enabled` |
| Skip Warnings | `Désactivé` ✅ / `Activé` | `mame2003_skip_warnings` | `disabled` / `enabled` |
| Display MAME Menu | `Désactivé` ✅ / `Activé` | `mame2003_display_setup` | `disabled` / `enabled` |
| Legacy Remapping | `Activé` ✅ / `Désactivé` | `mame2003_mame_remapping` | `enabled` / `disabled` |
| Autosave Hiscore | `default` ✅ / `recursively` / `disabled` | `mame2003_autosave_highscore` | `default` / `recursively` / `disabled` |
| NVRAM Bootstraps * | `Activé` ✅ / `Désactivé` | `mame2003_nvram_bootstraps` | `enabled` / `disabled` |
| Locate System Files Within a Subfolder | `Activé` ✅ / `Désactivé` | `mame2003_core_sys_subfolder` | `enabled` / `disabled` |
| Locate Save Files Within a Subfolder | `Activé` ✅ / `Désactivé` | `mame2003_core_save_subfolder` | `enabled` / `disabled` |
| Mouse Device | `mouse` ✅ / `pointer` / `Désactivé` | `mame2003_mouse_device` | `mouse` / `pointer` / `disabled` |
| Input Interface | `simultaneous` ✅ / `retropad` / `keyboard` | `mame2003_input_interface` | `simultaneous` / `retropad` / `keyboard` |
| 4-Way Joystick Emulation on 8-Way Joysticks * | `Désactivé` ✅ / `Activé` | `mame2003_four_way_emulation` | `disabled` / `enabled` |
| Show Lightgun Crosshairs * | `Activé` ✅ / `Désactivé` | `mame2003_crosshair_enabled` | `enabled` / `disabled` |
| Map Right Analog Stick as Buttons | `Activé` ✅ / `Désactivé` | `mame2003_rstick_to_btns` | `enabled` / `disabled` |
| Dip Switch/Cheat Input Ports * | `Désactivé` ✅ / `Activé` | `mame2003_cheat_input_ports` | `disabled` / `enabled` |
| Use Samples | `Activé` ✅ / `Désactivé` | `mame2003_use_samples` | `enabled` / `disabled` |
| DCS Speedhack | `Activé` ✅ / `Désactivé` | `mame2003_dcs_speedhack` | `enabled` / `disabled` |
| Sample Rate | `8000 KHz` / `11025 KHz` / `22050 KHz` / `30000 KHz` / `44100 KHz` / `48000 KHz` ✅ | `mame2003_sample_rate` | `8000` / `11025` / `22050` / `30000` / `44100` / `48000` |
| Bypass Timing Skew | `Désactivé` ✅ / `Activé` | `mame2003_machine_timing` | `disabled` / `enabled` |
| Brightness | `0.2` / `0.3` / `0.4` / `0.5` / `0.6` / `0.7` / `0.8` / `0.9` / `1.0` ✅ / `1.1` / `1.2` / `1.3` / `1.4` / `1.5` / `1.6` / `1.7` / `1.8` / `1.9` / `2.0` | `mame2003_brightness` | `0.2` / `0.3` / `0.4` / `0.5` / `0.6` / `0.7` / `0.8` / `0.9` / `1.0` / `1.1` / `1.2` / `1.3` / `1.4` / `1.5` / `1.6` / `1.7` / `1.8` / `1.9` / `2.0` |
| Gamma Correction | `0.5` / `0.6` / `0.7` / `0.8` / `0.9` / `1.0` ✅ / `1.1` / `1.2` / `1.3` / `1.4` / `1.5` / `1.6` / `1.7` / `1.8` / `1.9` / `2.0` | `mame2003_gamma` | `0.5` / `0.6` / `0.7` / `0.8` / `0.9` / `1.0` / `1.1` / `1.2` / `1.3` / `1.4` / `1.5` / `1.6` / `1.7` / `1.8` / `1.9` / `2.0` |
| TATE Mode | `Désactivé` ✅ / `Activé` | `mame2003_tate_mode` | `disabled` / `enabled` |
| Frameskip | `Désactivé` ✅ / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `9` / `10` / `11` / `auto` / `auto aggressive` / `auto max` | `mame2003_frameskip` | `disabled` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `9` / `10` / `11` / `auto` / `auto_aggressive` / `auto_max` |
| Display Artwork | `Activé` ✅ / `Désactivé` | `mame2003_display_artwork` | `enabled` / `disabled` |
| Resolution Multiplier | `1` ✅ / `2` | `mame2003_art_resolution` | `1` / `2` |
| Resolution * | `640x480` / `1024x768` ✅ / `1280x960` / `1440x1080` / `1600x1200` / `original` | `mame2003_vector_resolution` | `640x480` / `1024x768` / `1280x960` / `1440x1080` / `1600x1200` / `original` |
| Antialiasing * | `Activé` ✅ / `Désactivé` | `mame2003_vector_antialias` | `enabled` / `disabled` |
| Beam Width * | `1` / `1.2` ✅ / `1.4` / `1.6` / `1.8` / `2` / `2.5` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` | `mame2003_vector_beam_width` | `1` / `1.2` / `1.4` / `1.6` / `1.8` / `2` / `2.5` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` |
| Transculency * | `Activé` ✅ / `Désactivé` | `mame2003_vector_translucency` | `enabled` / `disabled` |
| Flicker * | `0` / `10` / `20` ✅ / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` | `mame2003_vector_flicker` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
| Intensity * | `0.5` / `1` / `1.5` ✅ / `2` / `2.5` / `3` | `mame2003_vector_intensity` | `0.5` / `1` / `1.5` / `2` / `2.5` / `3` |
| Specify Sega ST-V BIOS * | `default` ✅ / `japan` / `japana` / `us` / `japan_b` / `taiwan` / `europe` | `mame2003_stv_bios` | `default` / `japan` / `japana` / `us` / `japan_b` / `taiwan` / `europe` |
| CPU Clock Scale | `default` ✅ / `25%` / `30%` / `35%` / `40%` / `45%` / `50%` / `55%` / `60%` / `65%` / `70%` / `75%` / `80%` / `85%` / `90%` / `95%` / `105%` / `110%` / `115%` / `120%` / `125%` / `200%` / `250%` / `300%` | `mame2003_cpu_clock_scale` | `default` / `25` / `30` / `35` / `40` / `45` / `50` / `55` / `60` / `65` / `70` / `75` / `80` / `85` / `90` / `95` / `105` / `110` / `115` / `120` / `125` / `200` / `250` / `300` |

* Apparait si le jeu lancé est concerné par ces options.

## ![](/emulators/external-links.png) Liens externes

* **Progettosnaps** : [https://www.progettosnaps.net/](https://www.progettosnaps.net/)
* **Code source utilisé** : [https://github.com/libretro/mame2003-libretro/](https://github.com/libretro/mame2003-libretro)
* **Documentation Libretro** : [https://docs.libretro.com/library/mame_2003/](https://docs.libretro.com/library/mame_2003/)