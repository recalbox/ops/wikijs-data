---
title: Amiberry
description: 
published: true
date: 2024-07-24T21:08:39.432Z
tags: amiga-1200, amiga, 1200, amiberry
editor: markdown
dateCreated: 2021-07-29T12:34:10.531Z
---

**Amiberry** is a core for ARM optimized for Amiga.

## ![](/emulators/license.svg) License

This core is under [**GPLv3**](https://github.com/midwan/amiberry/blob/master/COPYING) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ❌ | ✅ |

## ![](/emulators/features.png) Features



## ![](/emulators/bios.svg) BIOS

### List of mandatory bios

You will find the bios with the name indicated in the column **Description** that you will have to rename with the name indicated in the column **Filename**.

| Filename | Description | MD5 | Provided |
| :---: | :---: | :---: | :---: |
| kick39106.A1200 | Kickstart v3.0 rev 39.106 (1992)(Commodore)(A1200)\[!\].rom | b7cc148386aa631136f510cd29e42fc3 | ❌ |
| kick40068.A1200 | Kickstart v3.1 r40.068 (1993-12)(Commodore)(A1200)\[!\].rom | 646773759326fbac3b2311fd8c8793ee | ❌ |
| kick40068.A4000 | Kickstart v3.1 r40.068 (1993-12)(Commodore)(A4000)\[!\].rom | 9bdedde6a4f33555b4a270c8ca53297d | ❌ |

### Location

Put the bios like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 🗒 **kick39106.A1200**
┃ ┃ ┃ ┣ 🗒 **kick40068.A1200**
┃ ┃ ┃ ┣ 🗒 **kick40068.A4000**

## ![](/emulators/roms.png) Roms

>Amiga emulation is available in two flavors: Amiga 600 and Amiga 1200.  
>Some games can be very temperamental so try both systems if a game doesn't work.
>The games can be used either in ADF disk format or in WHDLoad format, using in this case a complementary UAE file.
{.is-info}

### Supported extensions

Roms must have the extension:

* .adf
* .ipf
* .rp9
* .whd
* .zip

>Other disk image formats _could_ be supported but have not been tested by the Recalbox team.
>
>You can find more information about these formats on [this page](./../../../tutorials/games/guides/amiga/faq).
{.is-info}

### Location

Put the roms like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 amiga600
┃ ┃ ┃ ┃ ┣ 🗒 **game.bin**
┃ ┃ ┃ ┃ ┣ 🗒 **game.lst**

>The roms using **TOSEC** format are highly recommended.
{.is-success}

>For more information about roms, go to [this tutorial](./../../../tutorials/games/generalities/isos-and-roms)!
{.is-info}

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://gitlab.com/recalbox/packages/standalone/amiberry/](https://gitlab.com/recalbox/packages/standalone/amiberry)
* **Official website**: [https://blitterstudio.com/amiberry/](https://blitterstudio.com/amiberry/)