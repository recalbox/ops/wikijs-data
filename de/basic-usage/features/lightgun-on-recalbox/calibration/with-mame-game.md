---
title: Für MAME Spiele
description: 
published: true
date: 2024-06-02T20:23:44.226Z
tags: 
editor: markdown
dateCreated: 2021-07-31T09:19:11.212Z
---

## Beispiel einer Kalibrierung in MAME 2003 plus

1) Starte das Spiel und aktiviere das abgebildete Menü, indem du L3 auf deinem Controller drückst.

![](/basic-usage/features/lightgun/calibration/mame1.png){.full-width}

2) Wähle mit dem D-Pad oder dem linken Stick "Analog Controls" und bestätige die Auswahl mit der Taste `X` _(Ein SNES-Controller dient hier als Referenz)_

![](/basic-usage/features/lightgun/calibration/mame2.png){.full-width}

3) Wähle "Lightgun X Sensitivity" und "Lightgun Y Sensitivity" und stelle sie mit deinem D-Pad oder dem linken Stick auf 50 oder 100% ein (standardmäßig können es 25% sein).

![](/basic-usage/features/lightgun/calibration/mame3.png){.full-width}

4) Verlasse das Menü durch Drücken von "X" _(SNES-Controller als Referenz)_, um zum Spiel zurückzukehren. Die Einstellungen sind sofort wirksam. Viel Spaß! 

5) Beim nächsten Start des Spiels bleibt die Konfiguration erhalten (pass also gut auf deinen "saves"-Ordner auf!)

>Du musst diese Dateien aufbewahren. Auf diese Weise brauchst du die Bearbeitung nicht erneut vorzunehmen, da sie in deinen Sicherungsdateien enthalten ist: 
>
>![](/basic-usage/features/lightgun/calibration/calibration1.png)
{.is-warning}