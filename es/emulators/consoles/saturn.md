---
title: Saturn
description: Sega Saturn
published: true
date: 2024-07-26T08:05:39.382Z
tags: sega, saturn, consolas
editor: markdown
dateCreated: 2023-09-03T20:18:49.978Z
---

![](/emulators/consoles/saturn.svg){.align-center}

## Detalles técnicos

* **Fabricante**: Sega
* **Año de lanzamiento**: 1994
* **Cantidad vendida**: 9,26 millones
* **Juego más vendido**: Virtua Fighter 2
* **Procesador**: 2× Hitachi SH-2 a 28,6 MHz
* **RAM**: 16Mb
* **VRAM**: 12 Mb
* **Vídeo**: VDP1 (sprite/textura y polígonos), VDP2 (fondos)
* **Resolución**: de 320x224 a 704x224, 16,77 millones de colores
* **Chip de sonido**: Motorola 68EC000, Yamaha FH1 YMF292, 32 canales.
* **Soporte**: CD-ROM

## Presentación

La **Saturn** o **Sega Saturn** es una consola de videojuegos de quinta generación comercializada por Sega. Se trata de la cuarta consola doméstica diseñada por la empresa japonesa, tras la Mega Drive. Salió a la venta el 22 de noviembre de 1994 en Japón, el 11 de mayo de 1995 en Norteamérica y el 8 de julio de 1995 en Europa. La Saturn tenía una arquitectura que incorporaba ocho procesadores, incluidos dos microprocesadores, dos procesadores gráficos y dos procesadores de audio dedicados. Sus juegos se publican en formato CD-ROM, y su biblioteca de juegos incluye muchos juegos arcade, así como títulos originales.

El diseño de la Saturn comenzó en 1992, en torno al nuevo procesador SH-2 diseñado en colaboración entre Sega e Hitachi, en el papel de procesador central único. A lo largo de su diseño, éste fue duplicado por un segundo SH-2, al igual que la adición del segundo procesador gráfico personalizado a principios de 1994, para permitir a la consola competir técnicamente con la PlayStation de Sony. La Saturn fue inicialmente un éxito en Japón, pero no en Estados Unidos, donde salió a la venta en mayo de 1995, cuatro meses antes de lo previsto. Tras el debut de la Nintendo 64 a finales de 1996, la Saturn perdió rápidamente cuota de mercado en Estados Unidos y Europa, donde se dejó de fabricar en 1998. En Japón, la consola funcionó hasta 2000. Vendió 9,26 millones de unidades en todo el mundo, y se considera un fracaso comercial (a pesar de su éxito en Japón).

La Saturn contaba con varios juegos emblemáticos, como Nights into Dreams, Sega Rally Championship, Daytona USA y las series Panzer Dragoon, Virtua Fighter y Virtua Cop. Sin embargo, ha recibido una acogida desigual por parte de observadores y jugadores. Su compleja arquitectura dificultó la programación, el limitado apoyo de terceros desarrolladores de videojuegos y la incapacidad de los equipos de desarrollo de Sega para lanzar un juego de la serie Sonic the Hedgehog, conocido como Sonic X-treme, se consideraron los principales factores de los malos resultados de la consola. También se critica a la dirección de la compañía japonesa por las decisiones tomadas durante el desarrollo y la comercialización de la consola.

## Emuladores

[Libretro Kronos](libretro-kronos)
[Libretro Mednafen_Saturn](libretro-mednafen_saturn)
[Libretro YabaSanshiro](libretro-yabasanshiro)
[Libretro Yabause](libretro-yabause)