---
title: RetroArch
description: 
published: true
date: 2023-09-16T12:30:08.549Z
tags: retroarch
editor: markdown
dateCreated: 2021-06-29T13:02:22.012Z
---

Recalbox uses [**RetroArch**](https://github.com/libretro/RetroArch), created by Twinaphex, as an interface for emulators.

RetroArch is the program that runs most of the Recalbox emulators.

## What is it?

Retroarch is a Libretro front-end, which means it can run the contents of Libretro cores, it also unifies the way those cores work with the system.

A core is a program adapted to work with Libretro front-ends, in this case, most of them are emulators.

## Features

RetroArch, in its nature, is a program that launches other small programs, however, it also tells them how the controls are configured (so you only need to configure your controller once for all cores, instead of once per core), the added features are generally available across multiple cores, and can be accessed the same way for each of the cores. 

In short, it sets a standard that cores will follow.

The RetroArch interface allows many parameters to be changed, but in most cases the defaults should be correct. However, the core options, for example, may allow you to change the region of the system you are currently playing in, if necessary. As usual, these settings should not be changed without some thought.

It also includes the basic features offered by an emulator (such as save states, screenshots, disc-wrapping for CD-based systems, ...), as well as more advanced settings (shaders, rewind and fast-forward, netplay, ...), some of which can be accessed using **hotkey** combinations.

> It is not recommended to modify RetroArch's default options directly through its interface, as these modifications do not survive a "factory reset" of Recalbox and can generate issues. If you need to modify RetroArch's options, it is recommended to use the [configuration overrides](/advanced-usage/configuration-override).
{.is-warning}

## Requirements

Although RetroArch and Recalbox try to make things as simple as possible for the end user, some cores have requirements to work properly, such as BIOSes or the correct file format for a game; the webmanager can tell you what BIOSes you have or need for the different emulated systems, and the `readme.txt` files inside each system will tell you what formats are supported for each system.

We also urge you to understand what you are doing for certain systems (like the arcade for example), as putting in random files and expecting it to work may just not work at all depending on the situation.