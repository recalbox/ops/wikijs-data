---
title: DOS
description: 
published: true
date: 2024-07-26T12:47:29.410Z
tags: dos, msdos, ordenadores
editor: markdown
dateCreated: 2023-09-02T18:44:41.597Z
---

![](/emulators/computers/dos.svg){.align-center}

## Datos técnicos

* **Fabricante**: IBM
* **Año de lanzamiento**: 1981

## Presentación

El **DOS** _(sistema operativo de disco)_ se conoce generalmente como sistema operativo **PC-DOS**, así como la variante **MS-DOS** vendida por Microsoft para PC compatibles. Existen clones posteriores, como **DR-DOS** y **FreeDOS** de Digital Research.

Hasta principios de los 90, DOS era el tipo de sistema más utilizado en los PC compatibles. Este sistema de línea de comandos era rudimentario: sin multitarea, sin memoria virtual, gestionando sólo el modo segmentado de 16 bits del microprocesador x86.

Existen otros sistemas no relacionados que contienen la palabra DOS _(AMSDOS, AmigaDOS, Apple DOS, ProDOS, DOS en mainframe)_, pero sus nombres quedan eclipsados.

## Emuladores

[DOSBox](dosbox)
[Libretro DOSBox_Pure](libretro-dosbox-pure)