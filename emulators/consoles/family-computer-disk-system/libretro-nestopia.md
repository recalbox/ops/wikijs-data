---
title: Libretro Nestopia
description: 
published: true
date: 2024-07-24T12:43:17.092Z
tags: libretro, famicom, family computer disk system, nestopia
editor: markdown
dateCreated: 2021-07-30T18:54:14.925Z
---



## ![](/emulators/license.svg) License

This core is under [**GPLv2**](https://github.com/libretro/nestopia/blob/master/COPYING) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Features

| Feature | Supported |
| :---: | :---: |
| Restart | ✔ |
| Screenshots | ✔ |
| Saves | ✔ |
| States | ✔ |
| Rewind | ✔ |
| Netplay | ✔ |
| Core Options | ✔ |
| RetroArch Cheats | ✔ |
| Controls | ✔ |
| Remapping | ✔ |
| Softpatching | ✔ |

## ![](/emulators/bios.svg) BIOS

### List of mandatory bios

| Filename | Description | MD5 | Provided |
| :---: | :---: | :---: | :---: |
| disksys.rom | Family Computer Disk System BIOS | ca30b50f880eb660a320674ed365ef7a | ❌  |

### Location

Put the bios like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 fds
┃ ┃ ┃ ┃ ┣ 🗒 **disksys.rom**

## ![](/emulators/roms.png) Roms

### Supported extensions

Roms must have the extension:

* .fds
* .zip
* .7z

This system supports compressed roms in .zip/.7z format. However, be careful, it is only an archive.

The files contained in the .zip/.7z must correspond to the extensions mentioned above.
Moreover, each .zip/.7z file must contain only one compressed rom.

### Location

Put the roms like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 fds
┃ ┃ ┃ ┃ ┣ 🗒 **game.zip**

>The roms using **No-Intro** format are highly recommended.
{.is-success}

>For more information about roms, go to [this tutorial](./../../../tutorials/games/generalities/isos-and-roms)!
{.is-info}

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Accessing to options

You can configure various options in two different ways.

* Via RetroArch menu:

┣ 📁 RetroArch Menu
┃ ┣ 📁 Core options
┃ ┃ ┣ 🧩 Name_option

* Via the file `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core options

| Options | Options values | Variables (for configuration override) | Variables values |
| --- | --- | --- | --- |
| System Region | `Auto` ✅ / `NTSC` / `PAL` / `Famicom` / `Dendy` | `nestopia_favored_system` | `auto` / `ntsc` / `pal` / `famicom` / `dendy` |
| FDS Auto Insert | `Disabled` / `Enabled` ✅ | `nestopia_fds_auto_insert` | `disabled` / `enabled` |
| Blargg NTSC Filter | `Disabled` ✅ / `Composite Video` / `S-Video` / `RGB SCART` / `Monochrome` | `nestopia_blargg_ntsc_filter` | `disabled` / `composite` / `svideo` / `rgb` / `monochrome` |
| Palette | `CXA2025AS` ✅ / `Consumer` / `Canonical` / `Alternate` / `RGB` / `PAL` / `Composite Direct FBx` / `PVM-style D93 FBx` / `NTSC hardware FBx` / `NES Classic FBx FS` / `Raw` / `Custom` | `nestopia_palette` | `cxa2025as` / `consumer` / `composite` / `alternate` / `rgb` / `pal` / `composite-direct-fbx` / `pvm-style-d93-fbx` / `ntsc-hardware-fbx` / `nes-classic-fbx-fs` / `raw` / `custom` |
| Mask Overscan (Top Vertical) | `0` / `4` / `8` ✅ / `12` / `16` / `20` / `24` | `nestopia_overscan_v_top` | `0` / `4` / `8` / `12` / `16` / `20` / `24` |
| Mask Overscan (Bottom Vertical) | `0` / `4` / `8` ✅ / `12` / `16` / `20` / `24` | `nestopia_overscan_v_bottom` | `0` / `4` / `8` / `12` / `16` / `20` / `24` |
| Mask Overscan (Left Horizontal) | `0` ✅ / `4` / `8` / `12` / `16` / `20` / `24` | `nestopia_overscan_h_left` | `0` / `4` / `8` / `12` / `16` / `20` / `24` |
| Mask Overscan (Right Horizontal) | `0` ✅ / `4` / `8` / `12` / `16` / `20` / `24` | `nestopia_overscan_h_right` | `0` / `4` / `8` / `12` / `16` / `20` / `24` |
| Preferred Aspect Ratio | `Auto` ✅ / `NTSC` / `PAL` / `4:3` | `nestopia_aspect` | `auto` / `ntsc` / `pal` / `4:3` |
| Game Genie Sound Distortion | `Disabled` ✅ / `Enabled` | `nestopia_genie_distortion` | `disabled` / `enabled` |
| Square 1 Channel Volume % | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `nestopia_audio_vol_sq1` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
| Square 2 Channel Volume % | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `nestopia_audio_vol_sq2` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
| Triangle Channel Volume % | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `nestopia_audio_vol_tri` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
| Noise Channel Volume % | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `nestopia_audio_vol_noise` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
| DPCM Channel Volume % | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `nestopia_audio_vol_dpcm` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
| FDS Channel Volume % | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `nestopia_audio_vol_fds` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
| MMC5 Channel Volume % | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `nestopia_audio_vol_mmc5` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
| VRC6 Channel Volume % | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `nestopia_audio_vol_vrc6` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
| VRC7 Channel Volume % | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `nestopia_audio_vol_vrc7` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
| N163 Channel Volume % | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `nestopia_audio_vol_n163` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
| S5B Channel Volume % | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` ✅ | `nestopia_audio_vol_s5b` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
| Audio Output | `mono` / `stereo` ✅ | `nestopia_audio_type` | `mono` / `stereo` |
| 4 Player Adapter | `Auto` ✅ / `NTSC` / `Famicom` | `nestopia_select_adapter` | `auto` / `ntsc` / `famicom` |
| Shift Buttons Clockwise | `Disabled` ✅ / `Enabled` | `nestopia_button_shift` | `disabled` / `enabled` |
| Arkanoid device | `Mouse` ✅ / `Pointer` | `nestopia_arkanoid_device` | `mouse` / `pointer` |
| Zapper device | `Light gun` ✅ / `Mouse` / `Pointer` | `nestopia_zapper_device` | `lightgun` / `mouse` / `pointer` |
| Show Crosshair | `Disabled` / `Enabled` ✅ | `nestopia_show_crosshair` | `disabled` / `enabled` |
| Turbo Pulse Speed | `2` ✅ / `3` / `4` / `5` / `6` / `7` / `8` / `9` | `nestopia_turbo_pulse` | `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` |
| Remove Sprite Limit | `Disabled` ✅ / `Enabled` | `nestopia_nospritelimit` | `disabled` / `enabled` |
| CPU Speed (Overclock) | `1x` ✅ / `2x` | `nestopia_overclock` | `1x` / `2x` |
| RAM Power-on State | `0x00` ✅ / `0xFF` / `Random` | `nestopia_ram_power_state` | `0x00` / `0xFF` / `random` |

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/libretro/nestopia/](https://github.com/libretro/nestopia/)
* **Libretro documentation**: [https://docs.libretro.com/library/nestopia\_ue/](https://docs.libretro.com/library/nestopia_ue/)