---
title: Modificación
description: 
published: true
date: 2024-07-26T06:32:38.568Z
tags: modificación
editor: markdown
dateCreated: 2023-09-01T14:56:36.962Z
---

Esta sección os interesa si debéis efectuar modificaciones en el sistema.

Aquí tenéis la lista de tutoriales disponibles:

[Aumentar la corriente disponible en los puertos USB](increase-available-power-on-usb)
[Añadir vuestro propio script al inicio de Recalbox](add-your-own-startup-script)
[Modifcar los ficheros de boot de Recalbox](configtxt-configuration)