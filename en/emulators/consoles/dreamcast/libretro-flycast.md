---
title: Libretro Flycast
description: 
published: true
date: 2021-10-11T18:20:16.644Z
tags: libretro, dreamcast, flycast
editor: markdown
dateCreated: 2021-07-30T17:05:28.739Z
---

**Free Flycast** is a cross-platform **Sega Dreamcast** emulator capable to emulate the **Dreamcast**.

## ![](/emulators/license.svg) License

This core is under [**GPLv2**](https://github.com/libretro/flycast/blob/master/LICENSE) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ❌  | ❌  | ✅  | ✅  | ✅  | ✅  | ❌ | ✅  |

## ![](/emulators/features.png) Features

| Feature | Supported |
| :---: | :---: |
| Restart | ✔ |
| Screenshots | ✔ |
| Saves | ✔ |
| Core Options | ✔ |
| RetroArch Cheats | ✔ |
| Controls | ✔ |
| Remapping | ✔ |
| Rumble | ✔ |
| Disk Control | ✔ |

## ![](/emulators/bios.svg) BIOS

### List of mandatory bios

| Filename | Description | MD5 | Provided |
| :---: | :---: | :---: | :---: |
| dc_boot.bin | Dreamcast BIOS | e10c53c2f8b90bab96ead2d368858623 d552d8b577faa079e580659cd3517f86 d407fcf70b56acb84b8c77c93b0e5327 | ❌  |
| dc_flash.bin | Date / Hour / Language | d6e11a23f1fa01cddb5dfccf7e4cc8d7 93a9766f14159b403178ac77417c6b68 0a93f7940c455905bea6e392dfde92a4 23df18aa53c8b30784cd9a84e061d008 69c036adfca4ebea0b0c6fa4acfc8538 74e3f69c2bb92bc1fc5d9a53dcf6ffe2 2f818338f47701c606ade664a3e16a8a | ❌  |

### Location

Put the bios like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 dc
┃ ┃ ┃ ┃ ┣ 🗒 **dc_boot.bin**
┃ ┃ ┃ ┃ ┣ 🗒 **dc_flash.bin**

## ![](/emulators/isos.svg) Isos

### Supported extensions

Isos must have the extension:

* .bin/.cue
* .bin/.gdi
* .cdi
* .chd
* .elf
* .iso
* .m3u

### Location

Put the isos like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 dreamcast
┃ ┃ ┃ ┃ ┣ 🗒 **game.gdi**
┃ ┃ ┃ ┃ ┣ 🗒 **game.bin**

>The isos using **TOSEC** format are highly recommended.
{.is-success}

>For more information about isos, go to [this tutorial](./../../../tutorials/games/generalities/isos-and-roms)!
{.is-info}

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Accessing to options

You can configure various options in two different ways.

* Via RetroArch menu:

┣ 📁 RetroArch Menu
┃ ┣ 📁 Core options
┃ ┃ ┣ 🧩 Name_option

* Via the file `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core options

| Options | Options value | Variable (for configuration override) | Variables values |
| --- | --- | --- | --- |
| Boot to BIOS (Restart) | `Disabled` ✅ / `Enabled` | `flycast_boot_to_bios` | `disabled` / `enabled` |
| System Type (Restart) | `Auto` ✅ / `Dreamcast` / `NAOMI` / `Atomiswave` | `flycast_system` | `auto` / `dreamcast` / `naomi` / `atomiswave` |
| HLE BIOS | `Disabled` ✅ / `Enabled` | `flycast_hle_bios` | `disabled` / `enabled` |
| Internal resolution (restart) | `320x240` ✅ / `640x480` / `800x600` / `960x720` / `1024x768` / `1280x960` / `1440x1080` / `1600x1200` / `1920x1440` / `2560x1920` / `2880x2160` / `3200x2400` / `3840x2880` / `4480x3360` / `5120x3840` / `5760x4320 / ``6400x4800` / `7040x5280` / `7680x5760` / `8320x6240` / `8960x6720` / `9600x7200` / `10240x7680` / `10880x8160` / `11520x8640` / `12160x9120` / `12800x9600` | `flycast_internal_resolution` | `320x240` / `640x480` / `800x600` / `960x720` / `1024x768` / `1280x960` / `1440x1080` / `1600x1200` / `1920x1440` / `2560x1920` / `2880x2160` / `3200x2400` / `3840x2880` / `4480x3360` / `5120x3840` / `5760x4320` / `6400x4800` / `7040x5280` / `7680x5760` / `8320x6240` / `8960x6720` / `9600x7200` / `10240x7680` / `10880x8160` / `11520x8640` / `12160x9120` / `12800x9600` |
| Screen Orientation | `Horizontal` ✅ / `Vertical` | `flycast_screen_rotation` | `horizontal` / `vertical` |
| Alpha Sorting | `Per-Strip (fast, least accurate)` / `Per-Triangle (normal)` ✅ | `flycast_alpha_sorting` | `per-strip (fast, least accurate)` / `per-triangle (normal)` |
| GDROM Fast Loading (inaccurate) | `Disabled` ✅ / `Enabled` | `flycast_gdrom_fast_loading` | `disabled` / `enabled` |
| Mipmapping | `Enabled` ✅ / `Disabled` | `flycast_mipmapping` | `enabled` / `disabled` |
| Fog Effects | `Enabled` ✅ / `Disabled` | `flycast_fog` | `enabled` / `disabled` |
| Volume Modifier | `Enabled` ✅ / `Disabled` | `flycast_volume_modifier_enable` | `enabled` / `disabled` |
| Widescreen Hack (Restart) | `Disabled` ✅ / `Enabled` | `flycast_widescreen_hack` | `disabled` / `enabled` |
| Widescreen Cheats (Restart) | `Disabled` ✅ / `Enabled` | `flycast_widescreen_cheats` | `disabled` / `enabled` |
| Cable Type | `TV (RGB)` / `TV (Composite)` ✅ / `VGA (RGB)` | `flycast_cable_type` | `TV (RGB)` / `TV (Composite)` / `VGA (RGB)` |
| Broadcast | `Default` ✅ / `PAL-M (Brazil)` / `PAL-N (Argentina, Paraguay, Uruguay)` / `NTSC` / `PAL (World)` | `flycast_broadcast` | `Default` / `PAL_M` / `PAL_N` / `NTSC` / `PAL` |
| Region | `Default` ✅ / `Japan` / `USA` / `Europe` | `flycast_region` | `Default` / `Japan` / `USA` / `Europe` |
| Language | `Default` ✅ / `Japanese` / `English` / `German` / `French` / `Spanish` / `Italian` | `flycast_language` | `Default` / `Japanese` / `English` / `German` / `French` / `Spanish` / `Italian` |
| DIV Matching | `Disabled` / `Auto` ✅ | `flycast_div_matching` | `disabled` / `auto` |
| Force Windows CE Mode | `Disabled` ✅ / `Enabled` | `flycast_force_wince` | `disabled` / `enabled` |
| Analog Stick Deadzone | `0%` / `5%` / `10%` / `15%` ✅ / `20%` / `25%` / `30%` | `flycast_analog_stick_deadzone` | `0%` / `5%` / `10%` / `15%` / `20%` / `25%` / `30%` |
| Trigger Deadzone | `0%` ✅ / `5%` / `10%` / `15%` / `20%` / `25%` / `30%` | `flycast_trigger_deadzone` | `0%` / `5%` / `10%` / `15%` / `20%` / `25%` / `30%` |
| Digital Triggers | `Disabled` ✅ / `Enabled` | `flycast_digital_triggers` | `disabled` / `enabled` |
| Enable DSP | `Disabled`  / `Enabled` ✅ | `flycast_enable_dsp` | `disabled` / `enabled` |
| Anisotropic Filtering | `Off` / `2` / `4` ✅ / `8` / `16` | `flycast_anisotropic_filtering` | `disabled` / `2` / `4` / `8` / `16` |
| PowerVR2 Post-processing Filter | `Disabled` ✅ / `Enabled` | `flycast_pvr2_filtering` | `disabled` / `enabled` |
| Texture Upscaling (xBRZ) | `Off` ✅ / `2` / `4` / `6` | `flycast_texupscale` | `disabled` / `2` / `4` / `6` |
| Enable RTT (Render To Texture) Buffer | `Disabled` ✅ / `Enabled` | `flycast_enable_rttb` | `disabled` / `enabled` |
| Render To Texture Upscaling | `1x` ✅ / `2x` / `3x` / `4x` / `8x` | `flycast_render_to_texture_upscaling` | `1x` / `2x` / `3x` / `4x` / `8x` |
| Threaded Rendering (Restart) | `Disabled`  / `Enabled` ✅ | `flycast_threaded_rendering` | `disabled` / `enabled` |
| Synchronous Rendering | `Disabled`  / `Enabled` ✅ | `flycast_synchronous_rendering` | `disabled` / `enabled` |
| Delay Frame Swapping | `Disabled` ✅ / `Enabled` | `flycast_delay_frame_swapping` | `disabled` / `enabled` |
| Frame Skipping | `Disabled` ✅ / `1` / `2` / `3` / `4` / `5` / `6` | `flycast_frame_skipping` | `off` / `1` / `2` / `3` / `4` / `5` / `6` |
| Purupuru Pack/Vibration Pack | `Disabled`  / `Enabled` ✅ | `flycast_enable_purupuru` | `disabled` / `enabled` |
| Load Custom Textures | `Disabled` ✅ / `Enabled` | `flycast_custom_textures` | `disabled` / `enabled` |
| Dump Textures | `Disabled` ✅ / `Enabled` | `flycast_dump_textures` | `disabled` / `enabled` |
| Per-Game VMUs | `Disabled` ✅ / `VMU A1` / `All VMUs` | `flycast_per_content_vmus` | `Disabled` / `VMU A1` / `All VMUs` |
| Show VMU Display Settings | `Enabled` / `Disabled` ✅ | `flycast_show_vmu_screen_settings` | `enabled` / `disabled` |
| VMU Screen 1 Display | `Disabled` ✅ / `Enabled` | `flycast_vmu1_screen_display` | `disabled` / `enabled` |
| VMU Screen 1 Position | `Upper Left` ✅ / `Upper Right` / `Lower Left` / `Lower Right` | `flycast_vmu1_screen_position` | `Upper Left` / `Upper Right` / `Lower Left` / `Lower Right` |
| VMU Screen 1 Size | `1x` ✅ / `2x` / `3x` / `4x` / `5x` | `flycast_vmu1_screen_size_mult` | `1x` / `2x` / `3x` / `4x` / `5x` |
| VMU Screen 1 Pixel On Color | `Default ON` ✅ / `Default OFF` / `Black` / `Blue` / `Light Blue` / `Green` / `Cyan` / `Cyan Blue` / `Light Green` / `Cyan Green` / `Light Cyan` / `Red` / `Purple` / `Light Purple` / `Yellow` / `Gray` / `Light Purple (2)` / `Light Green (2)` / `Light Green (3)` / `Light Cyan (2)` / `Light Red (2)` / `Magenta` / `Light Purple (2)` / `Light Orange` / `Orange` / `Light Purple (3)` / `Light Yellow` / `Light Yellow (2)` / `White` | `flycast_vmu1_pixel_on_color` | `DEFAULT ON 00` / `DEFAULT OFF 01` / `BLACK 02` / `BLUE 03` / `LIGHT_BLUE 04` / `GREEN 05` / `CYAN 06` / `CYAN_BLUE 07` / `LIGHT_GREEN 08` / `CYAN_GREEN 09` / `LIGHT_CYAN 10` / `RED 11` / `PURPLE 12` / `LIGHT_PURPLE 13` / `YELLOW 14` / `GRAY 15` / `LIGHT_PURPLE_2 16` / `LIGHT_GREEN_2 17` / `LIGHT_GREEN_3 18` / `LIGHT_CYAN_2 19` / `LIGHT_RED_2 20` / `MAGENTA 21` / `LIGHT_PURPLE_2 22` / `LIGHT_ORANGE 23` / `ORANGE 24` / `LIGHT_PURPLE_3 25` / `LIGHT_YELLOW 26` / `LIGHT_YELLOW_2 27` / `WHITE 28` |
| VMU Screen 1 Pixel Off Color | `Default ON` / `Default OFF` ✅ / `Black` / `Blue` / `Light Blue` / `Green` / `Cyan` / `Cyan Blue` / `Light Green` / `Cyan Green` / `Light Cyan` / `Red` / `Purple` / `Light Purple` / `Yellow` / `Gray` / `Light Purple (2)` / `Light Green (2)` / `Light Green (3)` / `Light Cyan (2)` / `Light Red (2)` / `Magenta` / `Light Purple (2)` / `Light Orange` / `Orange` / `Light Purple (3)` / `Light Yellow` / `Light Yellow (2)` / `White` | `flycast_vmu1_pixel_off_color` | `DEFAULT ON 00` / `DEFAULT OFF 01` / `BLACK 02` / `BLUE 03` / `LIGHT_BLUE 04` / `GREEN 05` / `CYAN 06` / `CYAN_BLUE 07` / `LIGHT_GREEN 08` / `CYAN_GREEN 09` / `LIGHT_CYAN 10` / `RED 11` / `PURPLE 12` / `LIGHT_PURPLE 13` / `YELLOW 14` / `GRAY 15` / `LIGHT_PURPLE_2 16` / `LIGHT_GREEN_2 17` / `LIGHT_GREEN_3 18` / `LIGHT_CYAN_2 19` / `LIGHT_RED_2 20` / `MAGENTA 21` / `LIGHT_PURPLE_2 22` / `LIGHT_ORANGE 23` / `ORANGE 24` / `LIGHT_PURPLE_3 25` / `LIGHT_YELLOW 26` / `LIGHT_YELLOW_2 27` / `WHITE 28` |
| VMU Screen 1 Opacity | `10%` / `20%` / `30%` / `40%` / `50%` / `60%` / `70%` / `80%` / `90%` / `100%` ✅ | `flycast_vmu1_screen_opacity` | `10%` / `20%` / `30%` / `40%` / `50%` / `60%` / `70%` / `80%` / `90%` / `100%` |
| VMU Screen 2 Display | `Disabled` ✅ / `Enabled` | `flycast_vmu2_screen_display` | `disabled` / `enabled` |
| VMU Screen 2 Position | `Upper Left` ✅ / `Upper Right` / `Lower Left` / `Lower Right` | `flycast_vmu2_screen_position` | `Upper Left` / `Upper Right` / `Lower Left` / `Lower Right` |
| VMU Screen 2 Size | `1x` ✅ / `2x` / `3x` / `4x` / `5x` | `flycast_vmu2_screen_size_mult` | `1x` / `2x` / `3x` / `4x` / `5x` |
| VMU Screen 2 Pixel On Color | `Default ON` ✅ / `Default OFF` / `Black` / `Blue` / `Light Blue` / `Green` / `Cyan` / `Cyan Blue` / `Light Green` / `Cyan Green` / `Light Cyan` / `Red` / `Purple` / `Light Purple` / `Yellow` / `Gray` / `Light Purple (2)` / `Light Green (2)` / `Light Green (3)` / `Light Cyan (2)` / `Light Red (2)` / `Magenta` / `Light Purple (2)` / `Light Orange` / `Orange` / `Light Purple (3)` / `Light Yellow` / `Light Yellow (2)` / `White` | `flycast_vmu2_pixel_on_color` | `DEFAULT ON 00` / `DEFAULT OFF 01` / `BLACK 02` / `BLUE 03` / `LIGHT_BLUE 04` / `GREEN 05` / `CYAN 06` / `CYAN_BLUE 07` / `LIGHT_GREEN 08` / `CYAN_GREEN 09` / `LIGHT_CYAN 10` / `RED 11` / `PURPLE 12` / `LIGHT_PURPLE 13` / `YELLOW 14` / `GRAY 15` / `LIGHT_PURPLE_2 16` / `LIGHT_GREEN_2 17` / `LIGHT_GREEN_3 18` / `LIGHT_CYAN_2 19` / `LIGHT_RED_2 20` / `MAGENTA 21` / `LIGHT_PURPLE_2 22` / `LIGHT_ORANGE 23` / `ORANGE 24` / `LIGHT_PURPLE_3 25` / `LIGHT_YELLOW 26` / `LIGHT_YELLOW_2 27` / `WHITE 28` |
| VMU Screen 2 Pixel Off Color | `Default ON` / `Default OFF` ✅ / `Black` / `Blue` / `Light Blue` / `Green` / `Cyan` / `Cyan Blue` / `Light Green` / `Cyan Green` / `Light Cyan` / `Red` / `Purple` / `Light Purple` / `Yellow` / `Gray` / `Light Purple (2)` / `Light Green (2)` / `Light Green (3)` / `Light Cyan (2)` / `Light Red (2)` / `Magenta` / `Light Purple (2)` / `Light Orange` / `Orange` / `Light Purple (3)` / `Light Yellow` / `Light Yellow (2)` / `White` | `flycast_vmu2_pixel_off_color` | `DEFAULT ON 00` / `DEFAULT OFF 01` / `BLACK 02` / `BLUE 03` / `LIGHT_BLUE 04` / `GREEN 05` / `CYAN 06` / `CYAN_BLUE 07` / `LIGHT_GREEN 08` / `CYAN_GREEN 09` / `LIGHT_CYAN 10` / `RED 11` / `PURPLE 12` / `LIGHT_PURPLE 13` / `YELLOW 14` / `GRAY 15` / `LIGHT_PURPLE_2 16` / `LIGHT_GREEN_2 17` / `LIGHT_GREEN_3 18` / `LIGHT_CYAN_2 19` / `LIGHT_RED_2 20` / `MAGENTA 21` / `LIGHT_PURPLE_2 22` / `LIGHT_ORANGE 23` / `ORANGE 24` / `LIGHT_PURPLE_3 25` / `LIGHT_YELLOW 26` / `LIGHT_YELLOW_2 27` / `WHITE 28` |
| VMU Screen 2 Opacity | `10%` / `20%` / `30%` / `40%` / `50%` / `60%` / `70%` / `80%` / `90%` / `100%` ✅ | `flycast_vmu2_screen_opacity` | `10%` / `20%` / `30%` / `40%` / `50%` / `60%` / `70%` / `80%` / `90%` / `100%` |
| VMU Screen 3 Display | `Disabled` ✅ / `Enabled` | `flycast_vmu3_screen_display` | `disabled` / `enabled` |
| VMU Screen 3 Position | `Upper Left` ✅ / `Upper Right` / `Lower Left` / `Lower Right` | `flycast_vmu3_screen_position` | `Upper Left` / `Upper Right` / `Lower Left` / `Lower Right` |
| VMU Screen 3 Size | `1x` ✅ / `2x` / `3x` / `4x` / `5x` | `flycast_vmu3_screen_size_mult` | `1x` / `2x` / `3x` / `4x` / `5x` |
| VMU Screen 3 Pixel On Color | `Default ON` ✅ / `Default OFF` / `Black` / `Blue` / `Light Blue` / `Green` / `Cyan` / `Cyan Blue` / `Light Green` / `Cyan Green` / `Light Cyan` / `Red` / `Purple` / `Light Purple` / `Yellow` / `Gray` / `Light Purple (2)` / `Light Green (2)` / `Light Green (3)` / `Light Cyan (2)` / `Light Red (2)` / `Magenta` / `Light Purple (2)` / `Light Orange` / `Orange` / `Light Purple (3)` / `Light Yellow` / `Light Yellow (2)` / `White` | `flycast_vmu3_pixel_on_color` | `DEFAULT ON 00` / `DEFAULT OFF 01` / `BLACK 02` / `BLUE 03` / `LIGHT_BLUE 04` / `GREEN 05` / `CYAN 06` / `CYAN_BLUE 07` / `LIGHT_GREEN 08` / `CYAN_GREEN 09` / `LIGHT_CYAN 10` / `RED 11` / `PURPLE 12` / `LIGHT_PURPLE 13` / `YELLOW 14` / `GRAY 15` / `LIGHT_PURPLE_2 16` / `LIGHT_GREEN_2 17` / `LIGHT_GREEN_3 18` / `LIGHT_CYAN_2 19` / `LIGHT_RED_2 20` / `MAGENTA 21` / `LIGHT_PURPLE_2 22` / `LIGHT_ORANGE 23` / `ORANGE 24` / `LIGHT_PURPLE_3 25` / `LIGHT_YELLOW 26` / `LIGHT_YELLOW_2 27` / `WHITE 28` |
| VMU Screen 3 Pixel Off Color | `Default ON` / `Default OFF` ✅ / `Black` / `Blue` / `Light Blue` / `Green` / `Cyan` / `Cyan Blue` / `Light Green` / `Cyan Green` / `Light Cyan` / `Red` / `Purple` / `Light Purple` / `Yellow` / `Gray` / `Light Purple (2)` / `Light Green (2)` / `Light Green (3)` / `Light Cyan (2)` / `Light Red (2)` / `Magenta` / `Light Purple (2)` / `Light Orange` / `Orange` / `Light Purple (3)` / `Light Yellow` / `Light Yellow (2)` / `White` | `flycast_vmu3_pixel_off_color` | `DEFAULT ON 00` / `DEFAULT OFF 01` / `BLACK 02` / `BLUE 03` / `LIGHT_BLUE 04` / `GREEN 05` / `CYAN 06` / `CYAN_BLUE 07` / `LIGHT_GREEN 08` / `CYAN_GREEN 09` / `LIGHT_CYAN 10` / `RED 11` / `PURPLE 12` / `LIGHT_PURPLE 13` / `YELLOW 14` / `GRAY 15` / `LIGHT_PURPLE_2 16` / `LIGHT_GREEN_2 17` / `LIGHT_GREEN_3 18` / `LIGHT_CYAN_2 19` / `LIGHT_RED_2 20` / `MAGENTA 21` / `LIGHT_PURPLE_2 22` / `LIGHT_ORANGE 23` / `ORANGE 24` / `LIGHT_PURPLE_3 25` / `LIGHT_YELLOW 26` / `LIGHT_YELLOW_2 27` / `WHITE 28` |
| VMU Screen 3 Opacity | `10%` / `20%` / `30%` / `40%` / `50%` / `60%` / `70%` / `80%` / `90%` / `100%` ✅ | `flycast_vmu3_screen_opacity` | `10%` / `20%` / `30%` / `40%` / `50%` / `60%` / `70%` / `80%` / `90%` / `100%` |
| VMU Screen 4 Display | `Disabled` ✅ / `Enabled` | `flycast_vmu4_screen_display` | `disabled` / `enabled` |
| VMU Screen 4 Position | `Upper Left` ✅ / `Upper Right` / `Lower Left` / `Lower Right` | `flycast_vmu_screen_position` | `Upper Left` / `Upper Right` / `Lower Left` / `Lower Right` |
| VMU Screen 4 Size | `1x` ✅ / `2x` / `3x` / `4x` / `5x` | `flycast_vmu4_screen_size_mult` | `1x` / `2x` / `3x` / `4x` / `5x` |
| VMU Screen 4 Pixel On Color | `Default ON` ✅ / `Default OFF` / `Black` / `Blue` / `Light Blue` / `Green` / `Cyan` / `Cyan Blue` / `Light Green` / `Cyan Green` / `Light Cyan` / `Red` / `Purple` / `Light Purple` / `Yellow` / `Gray` / `Light Purple (2)` / `Light Green (2)` / `Light Green (3)` / `Light Cyan (2)` / `Light Red (2)` / `Magenta` / `Light Purple (2)` / `Light Orange` / `Orange` / `Light Purple (3)` / `Light Yellow` / `Light Yellow (2)` / `White` | `flycast_vmu4_pixel_on_color` | `DEFAULT ON 00` / `DEFAULT OFF 01` / `BLACK 02` / `BLUE 03` / `LIGHT_BLUE 04` / `GREEN 05` / `CYAN 06` / `CYAN_BLUE 07` / `LIGHT_GREEN 08` / `CYAN_GREEN 09` / `LIGHT_CYAN 10` / `RED 11` / `PURPLE 12` / `LIGHT_PURPLE 13` / `YELLOW 14` / `GRAY 15` / `LIGHT_PURPLE_2 16` / `LIGHT_GREEN_2 17` / `LIGHT_GREEN_3 18` / `LIGHT_CYAN_2 19` / `LIGHT_RED_2 20` / `MAGENTA 21` / `LIGHT_PURPLE_2 22` / `LIGHT_ORANGE 23` / `ORANGE 24` / `LIGHT_PURPLE_3 25` / `LIGHT_YELLOW 26` / `LIGHT_YELLOW_2 27` / `WHITE 28` |
| VMU Screen 4 Pixel Off Color | `Default ON` / `Default OFF` ✅ / `Black` / `Blue` / `Light Blue` / `Green` / `Cyan` / `Cyan Blue` / `Light Green` / `Cyan Green` / `Light Cyan` / `Red` / `Purple` / `Light Purple` / `Yellow` / `Gray` / `Light Purple (2)` / `Light Green (2)` / `Light Green (3)` / `Light Cyan (2)` / `Light Red (2)` / `Magenta` / `Light Purple (2)` / `Light Orange` / `Orange` / `Light Purple (3)` / `Light Yellow` / `Light Yellow (2)` / `White` | `flycast_vmu4_pixel_off_color` | `DEFAULT ON 00` / `DEFAULT OFF 01` / `BLACK 02` / `BLUE 03` / `LIGHT_BLUE 04` / `GREEN 05` / `CYAN 06` / `CYAN_BLUE 07` / `LIGHT_GREEN 08` / `CYAN_GREEN 09` / `LIGHT_CYAN 10` / `RED 11` / `PURPLE 12` / `LIGHT_PURPLE 13` / `YELLOW 14` / `GRAY 15` / `LIGHT_PURPLE_2 16` / `LIGHT_GREEN_2 17` / `LIGHT_GREEN_3 18` / `LIGHT_CYAN_2 19` / `LIGHT_RED_2 20` / `MAGENTA 21` / `LIGHT_PURPLE_2 22` / `LIGHT_ORANGE 23` / `ORANGE 24` / `LIGHT_PURPLE_3 25` / `LIGHT_YELLOW 26` / `LIGHT_YELLOW_2 27` / `WHITE 28` |
| VMU Screen 4 Opacity | `10%` / `20%` / `30%` / `40%` / `50%` / `60%` / `70%` / `80%` / `90%` / `100%` ✅ | `flycast_vmu4_screen_opacity` | `10%` / `20%` / `30%` / `40%` / `50%` / `60%` / `70%` / `80%` / `90%` / `100%` |
| Show Light Gun Settings | `Enabled` / `Disabled` ✅ | `flycast_show_lightgun_settings` | `enabled` / `disabled` |
| Gun Crosshair 1 Display | `Disabled` ✅ / `White` / `Red` / `Green` / `Blue` | `flycast_lightgun1_crosshair` | `disabled` / `White` / `Red` / `Green` / `Blue` |
| Gun Crosshair 2 Display | `Disabled` ✅ / `White` / `Red` / `Green` / `Blue` | `flycast_lightgun2_crosshair` | `disabled` / `White` / `Red` / `Green` / `Blue` |
| Gun Crosshair 3 Display | `Disabled` ✅ / `White` / `Red` / `Green` / `Blue` | `flycast_lightgun3_crosshair` | `disabled` / `White` / `Red` / `Green` / `Blue` |
| Gun Crosshair 4 Display | `Disabled` ✅ / `White` / `Red` / `Green` / `Blue` | `flycast_lightgun4_crosshair` | `disabled` / `White` / `Red` / `Green` / `Blue` |

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/libretro/flycast/](https://github.com/libretro/flycast/)
* **Libretro documentation**: [https://docs.libretro.com/library/flycast/](https://docs.libretro.com/library/flycast/)
* **Official source code**: [https://github.com/flyinghead/flycast](https://github.com/flyinghead/flycast)