---
title: Multivision
description: Othello Multivision
published: true
date: 2024-07-26T07:54:47.322Z
tags: othello, multivision, tsukuda, consolas
editor: markdown
dateCreated: 2023-09-03T19:34:05.257Z
---

![](/emulators/consoles/multivision.svg){.align-center}

## Datos técnicos

* **Fabricante**: Tsukuda Original
* **Año de lanzamiento**: 1983
* **Procesador**: Zilog Z80 a 3,58 MHz
* **RAM**: 1 KB
* **VRAM**: 16KB
* **Vídeo**: Texas Instruments TMS9918
* **Resolución**: 256×192 píxeles, 16 colores (de una paleta de 21 colores)
* **Chip de sonido**: Texas Instruments SN76489 PSG

## Presentación

La **Othello Multivision** es un clon bajo licencia de la SG-1000 de Sega, diseñada en 1983 por Tsukuda Original. Esta consola existe porque la intención original de Sega para la SC-3000 era permitir a otros fabricantes producir dispositivos compatibles con la esperanza de tener un estándar mundial. Por desgracia, quizá con la aparición del MSX, esta táctica fracasó y sólo se produjeron unos pocos modelos compatibles SG-1000 / SC-3000. El Othello Multivision fue uno de ellos.

Al igual que la SG-1000, existen 2 versiones de la Othello Multivision, denominadas FG-1000 (lanzada en 1983) y FG-2000 (lanzada en 1984) respectivamente. Capaz de utilizar cartuchos para el SG-1000 y el SC-3000, el Othello Multivision disponía de una serie de juegos y software con la marca Othello Multivision. El sistema estaba disponible con el juego Othello incluido.

Uno de los principales problemas de la Othello Multivision era que, en lugar de tener mandos externos, éstos estaban en la propia consola. Es compatible con algunos de los complementos de la SG-1000, pero incluye el SK-1100, que activa las funciones de teclado de la SC-3000. Los modelos FG-1000 se suministraban con una sobreimpresión que explicaba las funciones de cada botón.

El modelo FG-2000 añade soporte para un segundo joystick. Este modelo puede identificarse fácilmente por el hecho de que los botones son azules en lugar de rojos y el joystick incluido ha sido sustituido por un pad direccional.

## Emuladores

[Libretro Gearsystem](libretro-gearsystem)
