---
title: Interactions d'émulateur via un mappage GPIO
description: 
published: true
date: 2021-09-04T21:59:09.623Z
tags: gpio, émulateurs, interactions
editor: markdown
dateCreated: 2021-05-21T08:08:29.274Z
---

Les commandes réseau sont utilisées pour contrôler certaines parties de RetroArch. Pour ce faire, les commandes sont envoyées à RetroArch via le protocole UDP (User Datagram Protocol).

Habituellement, dans Recalbox, ces commandes sont transmises par la fonction **Hotkey** du contrôleur. Par exemple, pour réinitialiser le jeu, vous appuyez sur les touches `Hotkey + B` ou pour sauvegarder le jeu, vous appuyez sur les touches de `Hotkey + Y`.

>Cela ne fonctionne qu'avec RetroArch / les émulateurs qui font partie de Libretro.
{.is-info}

## Activation

Mais ces commandes peuvent également être envoyées à RetroArch via une ligne de commande ou des broches GPIO. Pour cela, vous devez activer l'option **commandes réseau** dans le menu RetroArch ou modifier la ligne suivante dans `retroarch.cfg` :

```text
network_cmd_enable = true
```

## Envoyer une commande via GPIO

#### Exemples d'application :

Vous utilisez un boîtier existant de console de jeu rétro (NES, N64, etc.) et vous voulez utiliser le bouton de réinitialisation d'origine pour réinitialiser le jeu auquel vous jouez actuellement et non pas pour réinitialiser tout le matériel (Raspberry Pi, etc.). Le script suivant montre comment envoyer la commande réseau **RESET** à RetroArch via les broches GPIO 5 et 6 pour réinitialiser le jeu en cours.

```python
import RPi.GPIO as GPIO
import time
import socket

# addressing information of target
IPADDR = "127.0.0.1"
PORTNUM = 55355

# enter the data content of the UDP packet
COMMAND = "RESET"

# initialize a socket, think of it as a cable
# SOCK_DGRAM specifies that this is UDP
try:
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
except socket.error:
    print 'Failed to create socket'
    sys.exit()

GPIO.setmode(GPIO.BCM)
GPIO.setup(3, GPIO.IN, pull_up_down=GPIO.PUD_UP)

def exitEmulator(channel):
    s.sendto(COMMAND, (IPADDR, PORTNUM))

GPIO.add_event_detect(3, GPIO.FALLING, callback=exitEmulator, bouncetime=500)
while True:
    time.sleep(10)
```

Avec ce script, il est non seulement possible de **RESET** les émulateurs, mais vous pouvez également utiliser la commande nécessaire à partir de diverses commandes. Vous pouvez **QUITTER** les émulateurs (retour à EmulationStation), **CHARGER** ou **SAUVEGARDER** un état de jeu, mettre en **PAUSE** la partie, etc. Pour cela, il suffit de remplacer la commande **RESET** de la **COMMAND = RESET** par une commande de votre choix.

>Ce script peut ne pas fonctionner avec toutes les commandes réseau de RetroArch. Pour certains, ça fonctionne, mais pour d'autres, il nécessite plus de travail pour le faire fonctionner (par exemple, FASTFORWARD_HOLD).
{.is-warning}

Vous pouvez également remapper les broches de votre Raspberry Pi.

Et n'oubliez pas : Recalbox dispose déjà d'options intégrées pour réinitialiser, démarrer et arrêter le matériel que vous utilisez [via des boutons et GPIO](./../../tutorials/others/add-on-off-button-to-your-recalbox).

## Commandes

Les commandes suivantes sont prises en charge par RetroArch :

* CHEAT_INDEX_MINUS
* CHEAT_INDEX_PLUS
* CHEAT_TOGGLE
* DISK_EJECT_TOGGLE
* DISK_NEXT
* DISK_PREV
* FAST_FORWARD
* FAST_FORWARD_HOLD
* FRAMEADVANCE
* FULLSCREEN_TOGGLE
* GRAB_MOUSE_TOGGLE
* LOAD_STATE
* MENU_TOGGLE
* MOVIE_RECORD_TOGGLE
* MUTE
* NETPLAY_FLIP
* OVERLAY_NEXT
* PAUSE_TOGGLE
* QUIT
* RESET
* REWIND
* SAVE_STATE
* SCREENSHOT
* SHADER_NEXT
* SHADER_PREV
* SLOWMOTION
* STATE_SLOT_MINUS
* STATE_SLOT_PLUS
* VOLUME_DOWN
* VOLUME_UP

## Envoyer une commande via la ligne de commande

Vous pouvez également envoyer des commandes réseau via la ligne de commande sous Linux. Voici comment procéder :

```shell
echo -n "QUIT" | nc -u -w1 127.0.0.1 55435
```

RetroArch est à l'écoute du port 55435 par défaut (le même port que pour le Netplay).