---
title: FAQ
description: 
published: true
date: 2021-09-09T22:45:14.683Z
tags: zx, spectrum, faq, trdos
editor: markdown
dateCreated: 2021-07-29T18:09:12.791Z
---

## Disquettes TRDOS

* Pour utiliser les disquettes TRDOS (_.scl ou_ .trd), il faut passer la machine a "**Scorpion 256k**" dans les options
  * Menu RetroArch
  * Options
  * Passer la machine a "**Scorpion 256k**" 
  * Utiliser le clavier pour valider "GLUK BOOT"
  * Puis le nom du jeu.