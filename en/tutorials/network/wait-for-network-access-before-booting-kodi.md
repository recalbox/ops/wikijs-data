---
title: Wait for network access before booting Kodi
description: 
published: true
date: 2021-08-06T14:50:42.268Z
tags: network, access, kodi
editor: markdown
dateCreated: 2021-08-06T08:53:50.735Z
---

When Kodi is in autostart mode on Recalbox, it is possible on some systems that the network service is not yet started when it starts. This causes problems for users who have network shares or use a remote database.

## How to proceed

* In the `recalbox.conf` file, look in the `A - System options` section for the following lines:

```text
kodi.network.waitmode=required
;kodi.network.waittime=10
;kodi.network.waithost=192.168.0.50
```

* Uncomment the lines and modify the values as needed by removing the semicolon.

## Explanations of the options

* _**kodi.network.waitmode**_

  **required** - Use this option to force Kodi to not be able to start until it can reach the specified IP address.
  **wish** - Use a countdown rather than the option to have to ping a machine before starting.


* _**kodi.network.waittime**_

  * Time in seconds to wait before Kodi fails to start (when `required` is set) or continues to start (when `wish` is set).

* _**kodi.network.waithost**_

  * The IP address of the system used to test the network connection (for example: the IP address of your box or network share used by Kodi).