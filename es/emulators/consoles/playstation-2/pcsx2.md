---
title: PCSX2
description: 
published: true
date: 2024-07-06T20:01:08.333Z
tags: playstation 2, ps2, pcsx2, 8.0+
editor: markdown
dateCreated: 2023-09-07T16:42:34.891Z
---



## ![](/emulators/license.svg) Licencia

El core posee la siguiente licencia [**GPLv2**](https://github.com/PCSX2/pcsx2/blob/master/COPYING.GPLv2).

## ![](/emulators/compatibility.png) Compatibilidad

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | 
| ❌ | ❌ | ❌ | ❌ | ❌ | ❌ | ✅ | ❌ |

## ![](/emulators/features.png) Funcionalidades



## ![](/emulators/bios.svg) BIOS

### Lista de BIOS obligatorias

| Nombre del fichero | Descripción | MD5 | ¿Preinstalada? |
| :---: | :---: | :---: | :---: |
| ps2-0230a-20080220.bin | SCPH-90001/SCPH-90010 (Version 5.0 02/20/08 A) | 21038400dc633070a78ad53090c53017 | ❌ |
| ps2-0230e-20080220.bin | SCPH-90002/SCPH-90003/SCPH-90004/SCPH-90008 (Version 5.0 02/20/08 E) | dc69f0643a3030aaa4797501b483d6c4 | ❌ |
| ps2-0230h-20080220.bin | SCPH-90005/SCPH-90006/SCPH-90007 (Version 5.0 02/20/08 J) | 30d56e79d89fbddf10938fa67fe3f34e | ❌ |
| ps2-0230j-20080220.bin | SCPH-90000 (Version 5.0 02/20/08 J) | 80ac46fa7e77b8ab4366e86948e54f83 | ❌ |

### Emplazamiento

Disponed vuestras bios como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 ps2
┃ ┃ ┃ ┃ ┣ 🗒 **ps2-0230a-20080220.bin**
┃ ┃ ┃ ┃ ┣ 🗒 **ps2-0230e-20080220.bin**
┃ ┃ ┃ ┃ ┣ 🗒 **ps2-0230h-20080220.bin**
┃ ┃ ┃ ┃ ┣ 🗒 **ps2-0230j-20080220.bin**
┃ ┃ ┃ ┃ ┣ 🗒 **ps2-0250e-20100415.bin**

## ![](/emulators/isos.svg) Isos

### Extensiones de roms soportadas

Las isos deben tener las extensiones siguientes:

* .bin
* .chd
* .cso
* .gz
* .iso
* .img
* .mdf
* .nrg

### Emplazamiento

Disponed vuestras isos como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 ps2
┃ ┃ ┃ ┃ ┣ 🗒 **juego.iso**

>Se aconsejan las isos del proyecto **Redump**.
{.is-success}

>Para más información sobre las isos, consultad [este tutorial](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

>Para conservar las configuraciones personalizadas cuando se produce una actualización del sistema Recalbox, os aconsejamos utilizar las [Sobrecargas de configuración](./../../../advanced-usage/configuration-override).
{.is-info}

## ![](/emulators/external-links.png) Enlaces externos

* **Código fuente utilizado** : [https://github.com/pcsx2/pcsx2/](https://github.com/pcsx2/pcsx2/)