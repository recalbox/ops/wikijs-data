---
title: Libretro FBNeo
description: 
published: true
date: 2021-08-09T20:18:55.037Z
tags: libretro, fbneo, neo-geo cd
editor: markdown
dateCreated: 2021-08-01T10:58:15.444Z
---



## ![](/emulators/license.svg) License

This core is under [**non commercial**](https://github.com/finalburnneo/FBNeo/blob/master/LICENSE.md) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ 🐌  | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

🐌 Low performances but playable with .iso/cue and .img/.ccd formats.

## ![](/emulators/features.png) Features

| Feature | Supported |
| :---: | :---: |
| Saves | ✔ |
| States | ✔ |
| RetroAchievements | ✔ |
| RetroArch Cheats | ✔ |
| Native Cheats | ✔ |
| Controllers | ✔ |
| Multi-Mouse | ✔ |

## ![](/emulators/bios.svg) BIOS

### List of mandatory bios

| Filename | Description | MD5 | Provided |
| :---: | :---: | :---: | :---: |
| neocdz.zip | Neo Geo CDZ System BIOS | 389dbeb0cb857c130414747462cf9d71 c85b8132799f1b2ad3754a97947809d2 b9ab07a79997fc044ca164fc72cd5680 f39572af7584cb5b3f70ae8cc848aba2 | ❌ |
| neogeo;zip | Neo Geo BIOS | 005b843b6e70b939c2cca41887cbc371 2b4ffdf24d5d4f24c4b116c50c4bafc3 42e359e394f0be1d61b94ddca0efbe6c 42e359e394f0be1d61b94ddca0efbe6c 4b9432a4571d03cc8bae9d24d8d6eb40 4cd64313e26537c04cdb561b8f84b0e4 50bbfd6bf297038b085e29265c65723a 76ee7390ccd537cddea1018025ca29a8 84d6c35aad790a77ba3cfc1b319e1061 8cbdff53661f41ddbe486a98915e1ec9 912c0a56bafe0fba39d0c668b139b250 94748719c0ef31e7375b0c357d82fc89 a3b2c9a69c28ad0bd4ea07877b744bbe ab25b9e25052a6064901a7a221037eb6 ae9e5825cde82c58e39dfd48534f7060 b18f83feef474b8a1306ef199cd810a2 b4519df27a352c2a6e42e06d31330d91 bf00272e7150b31156a8995d60bf185d c1e0a738d266926f604186aa9d63e4db ca66fd463ef7dcab492a6de8ce5f45eb d12bba59121229ff13702878b415cb7c e023d0a4b5249fdff2a4620c28963944 f4a125538dfd7a8044e025fc5188fb88 | ❌ |

### Location

Put the bios like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 🗒 **neocdz.zip**
┃ ┃ ┃ ┣ 🗒 **neogeo.zip**

## ![](/emulators/isos.svg) Isos

### Supported extensions

Isos must have the extension:

* .bin/.cue (**1 .bin file only**, see the [Conversion](./../../../tutorials/games/guides/neo-geo-cd/faq) part)
* .iso/.cue
* .img/.cue
* .img/.ccd/.sub

### Location

Put the isos like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 fbneo
┃ ┃ ┃ ┃ ┣ 🗒 **game.zip**

>For more information about isos, go to [this tutorial](./../../../tutorials/games/generalities/isos-and-roms)!
{.is-info}

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Accessing to options

You can configure various options in two different ways.

* Via RetroArch menu:

┣ 📁 RetroArch Menu
┃ ┣ 📁 Core options
┃ ┃ ┣ 🧩 Name_option

* Via the file `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core options

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/libretro/FBNeo/](https://github.com/libretro/FBNeo/)
* **Libretro documentation**: [https://docs.libretro.com/library/fbneo/](https://docs.libretro.com/library/fbneo/)
* **Differences between games**: [https://www.neogeocdworld.info/html/jeux/jeux/differences.htm](https://www.neogeocdworld.info/html/jeux/jeux/differences.htm)