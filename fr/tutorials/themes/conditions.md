---
title: 3. Les conditions
description: 
published: true
date: 2024-03-14T17:23:08.563Z
tags: themes, 9.2+
editor: markdown
dateCreated: 2024-01-24T01:25:01.958Z
---

## Introduction

Les conditions sont une possibilité nouvelle de créer votre thème. Si vous êtes développeur, vous devriez déjà connaitre.

Les conditions peuvent être utilisées avec l'attribut `if="condition"` dans :

- les balises `<include>`

```xml
<include if="crt">
  ...
</include>
```

- les balises `<view>`

```xml
<view name="system" if="arcade">
  ...
</view>
```

- les objets

```xml
<image name="deco" extra="true" if="favorite">
  ...
</image>
```

```xml
<image name="deco" extra="true" if="favorite" />
```

- les propriétés des objets

```xml
<image name="deco" extra="true">
  ...
  <path>default/image/path.png</path>
  <path if="crt">image/path/for/crt/only.png</path>
  ...
</image>
```

```xml
<image name="deco" extra="true" path="default/image/path.png">
  <path if="crt">image/path/for/crt/only.png</path>
</image>
```

## Fonctionnement

Les conditions peuvent être uniques ou complexes. Chaque pseudo-identifiant est évalué à `true` / `false` (`vrai` / `faux`) aux changements et mises à jour du thème. On peut les combiner ou les inverser avec les opérations simples de logique booléennes : `and`, `or` et `not`.

### Exemples simples

Exemple 1 :

```xml
if="qvga and not crt"
```

```xml
if="qvga & !crt"
```

Exemple 2 :

```xml
if="hd or fhd"
```

```xml
if="hd | fhd"
```

### Exemple plus complexe

```xml
if="qvga or vga and not crt"
```

```xml
if="qvga | vga & !crt"
```

L'évaluation se fait de droite à gauche, toujours entre le dernier résultat à gauche et le nouvel identifiant à droite.

L'écriture suivante :

```xml
if="qvga or vga and not crt"
```

Qui équivaut à `(qvga ou vga) et pas crt` n'est pas équivalente à l'écriture suivante :

```xml
if="not crt and qvga or vga"
```

Qui équivaut à `(pas crt et qvga) ou vga` !

Pour faciliter l'écriture *humaine*, `&` est équivalent à `et`, `|` est équivalent à `ou` et `!` est équivalent à `not`. Les caractères `&|!` peuvent être collés (par exemple `qvga&!crt`) mais pas les autres utilisations auquel un espace est requis (`qvga and not crt`).

## Les identifiants disponibles

- `crt` :

Vrai si un adaptateur CRT est branché et actif.

- `jamma` :

Vrai si un adaptateur RGB Jamma est branché et actif.

- `overscan` :

Correspond à `crt and !jamma`.

- `tate` :

Vrai si on est en mode tate.

- `qvga` :

Vrai si on est à 288 pixels de hauteur maximum.

- `vga` :

Vrai si on est à 576 pixels de hauteur maximum et pas en qvga.

- `hd` :

Vrai si on est à 920 pixels de hauteur maximum et pas en qvga ou vga.

- `fhd` :

Vrai si on est à plus de 920 pixels de hauteur.

- `virtual` :

Vrai si on est sur un système virtuel.

- `arcade` :

Vrai si on est sur un système arcade.

- `port` :

Vrai si on est sur le système ports.

- `console` :

Vrai si on est sur un système console de salon.

- `handheld` :

Vrai si on est sur un système console portable.

- `computer` :

Vrai si on est sur un système ordinateur.

- `fantasy` :

Vrai si on est sur un système fantasy.

- `engine` :

Vrai si on est sur un moteur de jeu.

- `favorite` :

Vrai si on est sur le système des favoris.

- `lastplayed` :

Vrai si on est sur le système des derniers jeux joués.

## Combinaisons

Vous pouvez identifier des groupes de systèmes facilement avec les conditions :

- Tous les systèmes arcade par constructeur :

```xml
if="arcade & virtual"
```

- Tous les types de consoles :

```xml
if="console | handheld | fantasy"
```

- Tous les systèmes bizarres qui n'ont pas d'existence physique :

```xml
if="fantasy | engine"
```