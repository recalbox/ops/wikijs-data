---
title: Controles
description: Quais controles foram testados e são compatíveis?
published: true
date: 2025-02-07T10:30:55.133Z
tags: joystick, compatibilidade, controles
editor: markdown
dateCreated: 2022-02-24T23:14:26.169Z
---

Aqui encontrarás uma lista de controladores que foram testados e aprovados pela comunidade.

>Controladores que não sejam 8bitdo ou não oficiais de fabricantes de consolas de videojogos são **muito pouco prováveis** de serem compatíveis, se é que o são. É preferível usar controladores oficiais ou 8bitdo.
{.is-warning}

## Controles oficiais de consoles

| Dispositivo | Com fio | Sem fio | Última versão testada | Comentários |
| :---: | :---: | :---: | :---: | :---: |
| **MICROSOFT** | | | | |
| Controle original do Xbox Xbox | ✅ | | | ✅ Com modificação USB |
| Controle Xbox 360 | ✅ | ☑️ | 8.0.0 | ☑️ Com dongle Microsoft Xbox 360 |
| Controle Xbox One | ✅ | ☑️ | 8.0.0 | ☑️ Com um dongle: [Adaptador sem fio Xbox](https://www.xbox.com/pt-PT/accessories/adapters/wireless-adapter-windows), 8BitDo (aguardar 5s), Janswall,... |
| Controle Xbox One Elite | ✅ | ☑️ | 7.1.1 | |
| Controle Xbox One S | ✅ | ☑️ | 9.1.0 | ☑️ Não em BT, apenas com um dongle: [adaptador sem fio Xbox](https://www.xbox.com/pt-PT/accessories/adapters/wireless-adapter-windows) |
| Controle Xbox One Series X\|S | ✅ | ☑️ | 9.1.0 | ☑️ Não em BT, apenas com um dongle: [adaptador sem fio Xbox](https://www.xbox.com/pt-PT/accessories/adapters/wireless-adapter-windows) |
| **NINTENDO** | | | | |
| Controle GameCube | ✅ | ☑️ | | ✅ Adaptador USB para PC Mayflash Mayflash GameCube ☑️ Com dongle: Adaptador 8BitDo GBros |
| Controle GameCube WaveBird | ❌ | ☑️ | 8.0.0 | ☑️ Com dongle: Adaptador 8BitDo GBros (com cabo de extensão GameCube) ou [controle GameCube para adaptador USB](https://www.raphnet-tech.com/products/gc_usb_adapter_gen3/index_en.php) |
| Controle NES Mini | ✅ | ☑️ | | ✅ Com conversor USB Mayflash ☑️ Com dongle: Adaptador 8BitDo GBros |
| Controle SNES Mini | ✅ | ☑️ | | ✅ Com conversor USB Mayflash ☑️ Com dongle: Adaptador 8BitDo GBros |
| JoyCon | | ☑️ | 8.0.0 | |
| Controle Switch Pro Controller | | ☑️ | 8.0.0 | |
| Controle Wii Classic | ✅ | ☑️ | v4.1 | ✅ Com conversor USB Mayflash ☑️ Nativamente e com dongle Adaptador 8BitDo GBros |
| Controle Wii Classic Pro | ✅ | ☑️ | | ✅ Com conversor USB Mayflash ☑️ Com dongle: Adaptador 8BitDo GBros |
| Wiimote | ✅ | ☑️ | 9.1.0 | ✅ Com Mayflash Dolphin Bar |
| Controle Wii U Pro | ❌ | ☑️ | 4.1 | Nativamente e com um dongle: 8BitDo, Janswall,...|
| Controle Super Nintendo Entertainment System para Nintendo Switch | ❌ | ☑️ | 8.0.0 | |
| Controle Nintendo 64 para Nintendo Switch | ❌ | ☑️ | 9.2 | Não suportado por drivers |
| Controle Sega MegaDrive para Nintendo Switch | ❌ | ❌ | 8.0.0 | Não suportado por drivers |
| Controle PDP Nintendo Switch Wireless Afterglow | | ☑️ | 8.0.0 | |
| **SNK** | | | | |
| Controle NeoGeo Mini | ❌ | | | Alguns problemas com um adaptador USB-C |
| **SONY** | | | | |
| Controle PlayStation 1 | ✅ | | v3.3beta 17 e v4.1.0-dev | Com adaptador USB genérico para 2 controles PS1/PS2 PAD |
| Controle PlayStation 1 Dualshock | ✅ | | v3.3beta 17 e v4.1.0-dev | Com adaptador USB genérico para 2 controles PS1/PS2 PAD |
| Controle PlayStation 2 Dualshock 2 | ✅ | | v3.3beta 17 e v4.1.0-dev | Com adaptador USB genérico para 2 controles PS1/PS2 PAD |
| PlayStation 3 DualShock 3 | ✅ | ☑️ | 7.0 | ☑️ Com um dongle: 8BitDo, Janswall,... ou qualquer dongle Bluetooth |
| PlayStation 4 DualShock 4 | ✅ | ☑️ | 7.0.1 | ☑️ Com dongle oficial Sony PS4 e outros dongles: 8BitDo, Janswall,... |
| PlayStation 5 DualSense | ✅ | | 7.0.1 | ✅ Com [este](https://forum.recalbox.com/topic/8920/a-lire-manettes-ps4). |
| Controle PlayStation Classic Mini | ✅ | | | |

## Controles

| Dispositivo | Status | Última versão testada | Comentários |
| :---: | :---: | :---: | :---: | :---: |
| Coleções de controles 8bitdo | ✅ | 8.0.0 | Consulte a página [8bitdo no Recalbox](./../../hardware-compatibility/compatible-devices/8bitdo-on-recalbox) |
| | | | |
| **Bigben Interactive** | | | |
| Pad PS3 Deluxe | ✅ | | |
| PlayStation 3 BB4259 | ✅ | | |
| PlayStation 3 BB4401 | ✅ | | |
| Big Ben BB5033 (com dongle USB 2.4GHz RF) | ✅ | v17.11.10.2 | |
| **Bowick** | | | |
| Controles Bluetooth sem fio para PS3 | ✅ | v4.0.2 | Selecione `shawan` na configuração |
| **BUFFALO** | | | |
| iBuffalo SNES Classic USB Gamepad | ✅ | | Os 2 botões especiais `Turbo` e `Clear` funcionam |
| **CSL-Computer** | | | |
| SNESw USB Gamepad | ✅ | | |
| Gamepad sem fio USB (modelo 2016) | ✅ | | |
| **EasySMX** | | | |
| Gamepad USB com fio EMS-9100 | ✅ | | Possui botão Home para configuração. Suporte Android 4.0 ou superior. |
| **Free** | | | |
| Controle Freebox | ✅ | | Modo analógico |
| **Gamesir** | | | |
| Gamesir G3s | ✅ | v4.0.2 | O controle pode ser configurado no menu, mas não funciona bem no jogo.
| **HORI** | | | |
| Fighting Commander | ✅ | v4.0.2 | |
| Pokken Tournament para Wii U | ✅ | v4.0.2 | |
| **Kreema** | | | |
| Gen game S3 | | v18.04.20 | Não funciona corretamente com uma conexão Bluetooth (Wireless Bluetooth 3.0) |
| Gen game S5 | ❌ | 17.13.02 | Pode ser emparelhado, mas age de forma estranha durante a configuração. Reconhecido como teclado se ambos os analógicos forem pressionados. Na configuração, cada botão pressionado é duplicado. Não funciona no jogo, apenas no menu principal. |
| **iNEXT** | | | |
| IN-Y-D002-024\*2 | ✅ | v4.0.2 | |
| **IPEGA** |  |  |  |
| PG-9228 | ✅ | 8.1.1 | Emparelhar com Home + B |
| **Konix** | | | |
| Konix PS3/PC | ✅ | | |
| KONIX KX-CT-PC | ✅ | v4.0.2 | |
| **KROM** | | | |
| Gamepad com fio Kay Pro Gaming (NXROMKEY) | ✅ | | Detectado como um controle do Xbox 360, mas impossível de configurá-lo. |
| **Logitech** | | | |
| Cordless Rumbleepad 2 (com dongle USB 2.4GHz RF) | ✅ | | |
| Dual Action Gamepad (USB) | ✅ | v4.0.0-beta4 | |
| Precision Gamepad (USB) | ✅ | v4.0.0-beta4 | |
| Rumblepad 2 (USB) | ✅ | | |
| **Logitech G** | | | |
| F310 Gamepad (USB) | ✅ | | |
| Gamepad sem fio F710 (com dongle RF USB de 2,4 GHz) | ✅ | v4.0.0-beta4 | Escolha o modo de entrada D, não X. |
| **MadCatz** | | | |
| MadCatz Wireless FightPad para PlayStation 3 (com dongle USB 2.4GHz RF) | ✅ | | |
| **Nacon** | | | |
| Controle com fio GC-400ES | ✅ | 6.1.1 | [Fornecedor](https://www.nacongaming.com/controllers/) (testado na versão 1, 2 e 3; reconhecido como controle genérico do Xbox) |
| Revolução com fio | ✅ | v6.1.1 | [Fornecedor](https://nacongaming.com/controllers/) (testado nas versões 1, 2 e 3; reconhecido como controle genérico do Xbox) |
| Nacon esport controllers/revolution wired et GC-400ES | ✅ | | [Fornecedor](https://www.nacongaming.com/controllers/) (testado nas versões 1, 2 e 3; reconhecido como controle genérico do Xbox) |
| **NGS** | | | |
| Maverick (PS3/PC) | ✅ | | |
| Nvidia | | | |
| Shield 2017 (USB) | ❌ | | Funciona no EmulationStation, mas os jogos podem levar algum tempo para serem reproduzidos. |
| **PDP** | | | |
| Rock Candy PlayStation 3 (com dongle Bluetooth) | ✅ | | |
| Xbox One e Windows 10 | ❌ | v18.04.20 | Detectado, mas não pode ser configurado. |
| PDP Nintendo Switch Wireless Afterglow Controller | ✅ | 8.0.2 | |
| PDP Afterglow Xbox 360 | ✅ | 9.1.0 | Com fios através de um adaptador USB, joystick translúcido com LEDs verdes ou azuis |
| **PowerA** | | | |
| MOGA PRO Power | ✅ | v17.11.02 | |
| **Razer** | | | |
| Razer Sabertooth | ✅ | v4.0.0-beta2 | |
| **Retro Games** |  |  |  |
| The Amiga 500 Mini Gamepad | ✅ | 8.1.0 |  |
| **RetroUSB** | | | |
| USB Super RetroPad (SNES) | ✅ | | |
| **Saitek** | | | |
| P380 | ✅ | | |
| P480 | ✅ | | |
| P880 | ✅ | | |
| **SlickBlue** | | | |
| SlickBlue sem fio Bluetooth Game Pad Controle para Sony PlayStation 3 | ❌ | | |
| **Snakebyte** | | | |
| idroid:con (com dongle Bluetooth) | ✅ | v3?3beta17 | |
| blu:con (com dongle Bluetooth) | ❌ | v4.0-rpi2-build15 | |
| **Speedlink** | | | |
| Speedlink Strike FX - PS3 (cabo USB) | ❌ | v4.0.0-rpi2B | |
| Speedlink Torid Wireless Gamepad | ✅ | v4.0.0-rpi3 | |
| **SteelSeries** | | | |
| SleetSeries Free Mobile Wireless | ✅ | v4.0.0-rpi3 | Dongle Bluetooth necessário |
| SteelSeries Stratus Wireless Gaming Controller | ❌ | v4.0.0-rpi3B | Pode ser emparelhado via Bluetooth, mas não será mais detectado. |
| **Subsonic** | | | |
| Controle Bluetooth Subsonic PS3 | ✅ | | (driver `gasia`) |
| **ThrustMaster** | | | |
| T-Wireless | ✅ | v17.11.10.2 | |
| **VZTEC** | | | |
| VZTEC USB Dual Shock Controller Game Pad Joystick(VZ-GA6002) | ✅ | v4.0.0-beta3 | | **Xiaomi** | | | |
| Controle Xiaomi | ✅ | | Precisa emparelhar após cada desconexão |
| **Marca não encontrada** | | | |
| PlayStation 3 Afterglow Wireless (com dongle USB 2.4GHz RF) | ✅ | | |
| PlayStation 3 GASIA Clone | ✅ | | |
| PlayStation 3 ShawnWan Clone | ✅ | | |
| Sony PlayStation Brandery compatible DualShowk Pad Controller | ✅ | v3.3beta 17 e v4.1.0-dev | Com adaptador USB genérico para 2 controles de pad PS1/PS2 |

## Adaptador d controle

| Dispositivo | Status | Última versão testada | Comentários |
| :---: | :---: | :---: | :---: | :---: |
| 4nes4snes | ✅ | 3.3.0 | |
| **Cronusmax** | | | |
| Adaptador CronusMAX (saída Xbox 360) | ✅ | v3.3beta17 | |
| **Mayflash** | | | |
| Mayflash Dolphin Bar | ✅ | 8.0.0 | |
| Adaptador de controle Mayflash GameCube | | 6.1 | Só funciona com o Dolphin. |
| Adaptador de controle Mayflash N64 | ✅ | 4.0.0-dev | |
| Adaptador de controle Mayflash NES/SNES | ✅ | 4.0.0 | Precisa de Usbhid |
| Controle Clássico de Wii Mayflash PC052 | ✅ | 4.1.0 | Para Controles Wii Classic, NES Mini e SNES Mini |
| Adaptador do controle Mayflash Sega Saturn | ✅ | 4.0.0 | Precisa de Usbhid |
| **OriGlam** | | | |
| Wii Sensor Bar USB | ❌ | 2018.x.x | |
| **Outros** | | | |
| PSP 1000 - 3000 com o driver FuSa Gamepad e Cabo MiniUSB | ✅ | 4.0.1 | O RPi não consegue carregar seu PSP |
| retrode | | 3.2.11 | Precisa de Usbhid |
| SFC/USB | ✅ | 4.0.0 | |
| Controle Sony PlayStation 3 | ✅ | 4.0.0 | |
| Trenro Dual PlayStation / Adaptador USB de Controle PlayStation 2 para PC | ✅ | v3.3beta17 e v4.1.0-dev | Com adaptador USB genérico para 2 controles PS1 / PS2 |

## Stick Arcade

| Dispositivo | Status | Última versão testada | Comentários |
| :---: | :---: | :---: | :---: | :---: |
| **8bitdo** | | | |
| Joystick Arcade FC30 (somente USB) | ✅ | | |
| Joystick Arcade NES30 (somente USB) | ✅ | | |
| **Data** | | | |
| Arcade Pro Joystick inc Rapid Fire para PS3 / Xbox 360 / PC | ✅ | | |
| **HORI** | | | |
| Fighting Stick EX2 USB (Xbox 360 / PC) | ✅ | | |
| Fighting Stick MINI 3 para PS3 | ✅ | | |
| Fighting Stick Mini 4 para PS3 / PS4 | ✅ | | |
| Real Arcade Pro. 3 Fighting Stick | ✅ | |
| Real Arcade Pro. 4 Kai (PS3/PS4/PC) | ✅ | | |
| HORI Real Arcade Pro. V (PS4) | | | |
| Tekken 6 Wireless Arcade Stick PS3 | ✅ | | |
| **LIONCAST** | | | |
| LIONCAST Arcade Fighting Stick (PS2/PS3/PC) | ✅ | | |
| **MadCatz** | | | |
| Arcade Fightstick Street Fighter V Tes+ (PS3 / PS4) | ✅ | 4.0.0-beta1 | |
| Arcade Stick Fightstick Alpha (PS3/PS4) | ✅ | 4.0.0-beta3 | |
| Arcade Fightsitck Street Fighter IV Standard Edition para Xbox 360 | ✅ | | |
| Street Fighter IV Fightstick Round 1 Tournament Edition | ✅ | | |
| Street Fighter IV Round 2 Arcade Fightstick Tournament | ✅ | | |
| Arcade Fightstick Tournament Edition 2 (para PS3 / PS4) | ✅ | 4.0.0-beta6 | |
| **Mayflash** | | | |
| Mayflash XBOX 360 / PS3 / PC Arcade Fighting Stick V2 | ✅ | | |
| Mayflash PS2 PS3 PC USB Universal Arcade Fighting Stick | ✅ | | |
| **PXN** | | | |
| PXN-0082 Joystick Arcade | ✅ | | |
| **QanBa** | | | |
| QamBa Q4 RAF Joystick Pro Fightstick Arcade 3 em 1 (Xbox 360 / PS3 / PC) | ✅ | | |
| **Sanwa** | | | |
| JLF-TP-8YT (Sanwa) Arcade Joystick | ✅ | | |
| **Time Warner** | | | |
| Mortal Kombat - Ultimate Edition (PS3) | ❌ | | |
| **Venom** | | | |
| Venom - Arcade Stick (PS3 / PS4) | ✅ | | |
| **X-Arcade** |  |  |  |
| X-Arcade Tankstick (Xinput e DirectInput) | ✅ | 8.0.2 | [Détails ici](https://support.xgaming.com/support/solutions/articles/5000554993-how-to-use-x-arcade-with-raspberry-pi) Versão com placa de circuito impresso de três modos |

## Joystick

| Dispositivo | Status | Última versão testada | Comentários |
| :---: | :---: | :---: | :---: |
| **Madcatz:** | | | |
| Cyborg F.L.Y 5 | ✅ | v18.04.20 | |

## LightGun

| Dispositivo | Status | Última versão testada | Comentários |
| :---: | :---: | :---: | :---: | :---: |
| **Nintendo** | | | |
| Wiimote | ✅ | 7.2.1 | Com Mayflash USB Converter (pode ser usado como Lightgun) |
| **Ultimarc** | | | |
| AimTrack Lightgun | ✅ | 2018.x.x | |

## Interface USB

| Dispositivo | Status | Última versão testada | Comentários |
| :---: | :---: | :---: | :---: |
| **Akishop:** | | | |
| PS360+ | ✅ | | |
| | | | |
| Codificador USB Zero Delay Controle PC/PS3 | | v3.3beta 17 e v4.0.0-dev | |
| Zero Delay USB Encoder Remix controle PC/PS3 | ✅ | v3.3beta 17 e v4.0.0-dev | |

## Botões de Arcade (Arcade Push Buttons)

| Dispositivo | Status | Última versão testada | Comentários |
| :---: | :---: | :---: | :---: |
| **Halo :** | | | |
| Botão Halo | ✅ | | |
| | | | |
| **Botão:** | | | |
| Botão SL-CV | ✅ | | |
| EG Starts Arcade Classic Kit | ✅ | 8.0.0 | ID 0079:0006 DragonRise Inc. PC TWIN SHOCK Gamepad |
