---
title: PC compatibles
description: 
published: true
date: 2025-02-16T22:45:49.726Z
tags: pc, compatibilidad
editor: markdown
dateCreated: 2023-09-19T08:59:50.003Z
---

>**Información :**
>
>Si **no encontráis vuestro PC** en las siguientes tablas, es que **todavía no ha sido probado**.
>También podéis probar vuestro hardware con la última versión de Recalbox si aún no se ha hecho, esta información puede ser actualizada (por favor, comunicarnos la información antes de modificar esta página si encontráis un nuevo PC compatible).
>
>* Especificad la **Marca** y el **Modelo**.
>* Si se trata de un PC ensamblado, especificad la CPU y la GPU.
>* No dudéis en **añadir detalles sobre el rendimiento** de vuestra configuración. 
>* Si tenéis algún problema, contactadnos en nuestro Discord.
{.is-info}

## Modelo de PC

### Dell

| Nombre | Estado | Versión de Recalbox | Notas |
| :---: | :---: | :---: | :---: |
| Dell Optiplex 390 i5-2400 3.10GHz | ✅ | 6.0 Rc3 | 100% Compatible |
| Dell Optiplex 990 i3 2100 | ✅ | 13.07.18 | 100% Compatible |
| Dell Optiplex 990 USFF i5 2400S 2.5GHZ | ✅ | 6.0.1 | 100% Compatible |
| Dell Optiplex 3020 micro i3 4150T | ✅ | 6.0 Rc3 | 100% Compatible |
| Dell Optiplex 3020 SFF I5-4590-8Go-HDD | ✅ | 9.0.2 Pulsar| Compatible pero se atranca en algunos juegos ps2-gamecube-wii |

### Intel

| Nombre | Estado | Versión de Recalbox | Notas |
| :---: | :---: | :---: | :---: |
| Intel NUC7i3BNK | ✅ | 4.1 | i3-7100U / HD Graphics 620 / BIOS 0036:OK / BIOS 0052:No temp probe |
| Intel NUC7I7BNH | ✅ | 9.1 | 100% compatible, alrededor de 60 FPS para todas las consolas, CPU Dual-Core i7-7567U, tarjeta Intel HD Graphics 650 |
| Intel NUC7i5BNH | ✅ | 6.1 | 100% compatible, alrededor de 60 FPS para todas las consolas, CPU Core i5-7260U, tarjeta gráfica Intel IRIS Plus 640 |
| Intel NUC6I7KYK | ✅ | 4.1 | 100% compatible, alrededor de 60 FPS para todas las consolas, última bios 0050, todo el hardware OK: CPU Core i7-6770HQ, tarjeta gráfica Intel IRIS PRO 580 entièrement supporté, wifi, bluetooth, sound, usbs, SSD SATA Samsung 1TB |
| Intel NUC8i7BEH | ✅ | 6.1 | 100% compatible, alrededor de 60 FPS para todas las consolas, CPU Core i7-8559U, tarjeta gráfica Intel IRIS Plus 655 |
| Intel N95 | ✅ | 9.2 |  |
| Intel N100 | ✅ | 9.2 | 100% compatible. Tarjeta gráfica Intel UHD |

### Otros

| Nombre | Estado | Versión de Recalbox | Notas |
| :---: | :---: | :---: | :---: |
| HP Slim Desktop 270-P026 i3-7100T | ✅ | 18.07.13 | 100% Compatible |
| Laptop 17.3" MSI GP72M 7RDX-871XFR | ✅ | 4.1 | Alrededor de 60 FPS para todas las consolas con la versión x86_64 con ajustes por defecto, última bios flasheada, todo el hardware ok: CPU Core i7-7700HQ, Intel HD Graphics 630 totalmente compatible, wifi, bluetooth, sonido (en altavoces + auriculares + HDMI), usbs, Samsung 1TB SATA SSD, ¡todo ok! Desde que tiene batería, ¡los juegos ya no se interrumpen! |
| Taichi  Z270 + I5 7500 | ✅ | 4.1 | Alrededor de 60 FPS en todas las consolas en la versión x86_64 |
| ZOTAC  ZBOX Nano AD10 | ✅ | 4.1 | Wii/NGC ok pero no se puede jugar bien. |
| ACEMAGICIAN AM06 Pro Mini PC Ryzen 5 5600U | ✅ | 9.0 | 100% Compatible |
| Laptop 17.3" ACER Nitro 5 AN517-41-R3J6 | ❌ | 9.2 | Arranque imposible, vidéo de introducción y pantalla negra. |
 | Laptop 17.3" ACER Nitro 5 AN517-41-R7A2 | ❌ | 9.2 |Arranque imposible, vidéo de introducción y pantalla negra. |
 | Laptop 11.6" ACER TravelMate B3 TMB311-31-C0AS | ✅ | 9.2 | Tests en curso para una compatibilidad de emulación maximal. |
 | Laptop 11.6" ACER TravelMate Spin B3 TMB311-31-C1DZ | ✅ | 9.2 | Tests en curso para una compatibilidad de emulación maximal. Pantalla táctil y rotación automática en curso de tests. |
 | ASRock B450 Pro4 + AMD Ryzen 3 2200G | ✅ | 9.2 | 100% Compatible. |
 | Laptop Samsung NC10 Blanc NP-NC10-KA01FR | ✅ | 7.2 | x86 unicamente. Memoria aumentada a 2Gb. |
 
### Apple Mac

| Nombre | Estado | Versión de Recalbox | Notas |
| :---: | :---: | :---: | :---: |
| MacBook Pro (13 pouces, début 2011) | ✅ | 4.1 | 100% compatible |
| iMac (21.5 pouces, Mid 2011) | ✅ | 4.1 | 100% compatible, algunos errores gráficos menores en el menú. |
| MacBook Pro (fin 2013) | ✅ | 4.1 | funcional |
| MacBook (13 pouces, fin 2009) | ✅ | 7.2.1 | 100% compatible |
| MacBook Pro (15 pouces, 2018) | ✅ | 7.3 | No hay sonido, el teclado no funciona. [Desactivar el chip T2 para poder arrancar Recalbox.](./../tutorials/others/disable-mac-t2-chip) |

## CPU

### Intel

| Nombre | Estado | Versión de Recalbox | Notas |
| :---: | :---: | :---: | :---: |
| Intel i3-4150T 3.00GHz | ✅ | 6.0 Rc3 | Alrededor de 60 FPS para todas las consolas en la versión x86_64 con la configuración por defecto, Dolphin funciona bien con la configuración por defecto |
| Intel Core i3-7100T | ✅ | 18.07.13 | Alrededor de 60 FPS para todas las consolas en la versión x86_64 con la configuración predeterminada.  |
| Intel Core i3-7100U | ✅ | 4.1 |  |
| Intel Core i5-2400 3.10 GHz | ✅ | 4.1 | Funciona bien en todas las consolas excepto GameCube y Wii (mejor rendimiento con una tarjeta gráfica). |
| Intel Core i5-2400S 2.5 GHZ | ✅ | 7.0.1 |  |
| Intel Core i5-3570K 3.40 GHz | ✅ | 6.0 Rc3 | Alrededor de 60 FPS para todas las consolas en la versión x64 con los parámetros por defecto |
| Intel Core i5-4570S | ✅ | 4.1 |  |
| Intel Core i5-6500 | ✅ | 4.1 |  |
| Intel Core i5-7500 | ✅ | 4.1 |  |
| Intel Core i7-2600K | ✅ | 4.1 | Alrededor de 60 FPS para todas las consolas en la versión x86_64 con la configuración predeterminada. |
| Intel Core i7-6770HQ | ✅ | 4.1 | Alrededor de 60 FPS para todas las consolas en la versión x86_64 con la configuración predeterminada. |
| Intel Core i7-7700HQ | ✅ | 4.1 | Alrededor de 60 FPS para todas las consolas en la versión x86_64 con la configuración predeterminada. |
| Intel Pentium G2130 3.20GHZ | ✅ | 4.1 |  |
| Intel G4600 | ✅ | 18.04.20 | Alrededor de 60 FPS para todas las consolas en la versión x86_64 con la configuración predeterminada. |

### AMD

| Nombre | Estado | Versión de Recalbox | Notas |
| :---: | :---: | :---: | :---: |
| AMD X2 4850B | ✅ | 4.1 |  |
| AMD Ryzen 9 5900HX | ✅ | 8.0 |  |
| AMD Ryzen 5 1600  | ✅ | 8.0 |  |
| AMD Ryzen 5 5600X | ✅ | 8.0 |  |
| AMD Ryzen 5 5600U | ✅ | 9.0 |  |
| AMD Ryzen 3 2200G | ✅ | 9.2 |  |


## GPU

>**Atencion :**  
>Ten en cuenta que la tarjeta gráfica no lo es todo, el kernel Linux contenido en Recalbox también debe ser capaz de gestionar el dispositivo.
{.is-info}

### Nouveau

Nouveau es un proyecto de la fundación X.Org y Freedesktop.org cuyo objetivo es proporcionar controladores gráficos Nvidia libres de derechos de autor mediante ingeniería inversa.

Nouveau se basa en los controladores libres (pero oscuros) nv mantenidos por Nvidia para soporte 2D. 

Para soportar OpenGL, el proyecto utiliza Mesa 3D, pero cambiado en febrero de 2018 por su sucesor Gallium3D4.5. El soporte OpenCL también se proporciona a través de Gallium3D. 

#### Proceso utilizado

El proyecto utiliza la técnica conocida como ingeniería inversa en tarjetas gráficas Nvidia estudiando cómo funcionan los controladores 3D para Linux, suministrados por el fabricante (bajo licencia propietaria), sin tocar los propios controladores. Este enfoque permite al proyecto evitar conflictos con la licencia de Nvidia.

* Según la [página de nuevas características](https://nouveau.freedesktop.org/wiki/FeatureMatrix/)
* De acuerdo con la compatibilidad de NVIDIA ([NVidia codenames](https://nouveau.freedesktop.org/wiki/CodeNames/))
* El nuevo driver en Recalbox (Versión 1.0.17)

>**Nota:**  
>Los nuevos drivers se usan por defecto cuando se detecta una tarjeta gráfica Nvidia no compatible con los drivers oficiales.
{.is-warning}

### NVIDIA

Las tarjetas gráficas Nvidia son compatibles con los **controladores oficiales** de las versiones [535.54.03](https://download.nvidia.com/XFree86/Linux-x86_64/535.54.03/README/supportedchips.html) y [470.199.02](https://download.nvidia.com/XFree86/Linux-x86_64/470.199.02/README/supportedchips.html).

### Intel

Lista de tarjetas gráficas integradas al procesador.

El [código fuente](https://cgit.freedesktop.org/xorg/driver/xf86-video-intel/tree/?id=b57abe20e81f4b8e4dd203b6a9eda7ff441bc8ce) no es muy lisible para las tarjetas soportadas, desgraciadamente.

| Nombre | Estado | Versión de Recalbox | Notas |
| :---: | :---: | :---: | :---: |
| Intel® HD Graphics 630 | ✅ | 4.1 | Alrededor de 60 FPS para todas las consolas en la versión x86_64 con la configuración predeterminada. |
| Intel® HD Graphics 3000 | ✅ | 4.1 | Alrededor de 60 FPS para todas las consolas en la versión x86_64 con la configuración predeterminada.  |
| Intel® Iris™ Plus Graphics 620 | ✅ | 4.1 | ​ |
| Intel® Iris™ Pro Graphics 580 | ✅ | 4.1 | Alrededor de 60 FPS para todas las consolas en la versión x86_64 con la configuración predeterminada.  |
| Intel® HD Graphics 4600 | ✅ | 4.1 | Alrededor de 60 FPS para todas las consolas en la versión x86_64 con la configuración predeterminada.  |
| Intel® HD Graphics 4400 | ✅ | 6.0 rc3 | Alrededor de 60 FPS para todas las consolas en la versión x86_64 con la configuración predeterminada.  |
| Intel® HD Graphics 4850 | ✅ | 6.0 DragonBlaze | Alrededor de 60 FPS para todas las consolas en la versión x86_64 con la configuración predeterminada.  |

### AMD

El [código fuente](https://cgit.freedesktop.org/xorg/driver/xf86-video-amdgpu/tree/?h=xf86-video-amdgpu-1.4.0) no es muy lisible para las tarjetas soportadas, desgraciadamente.

| Nombre | Estado | Versión de Recalbox | Notas |
| :--- | :---: | :---: | :---: |
| AMD Radeon HD 4850 512mo | ✅ | 6.0 Rc3 | Funciona en el CPU Intel i5-3570K |
| AMD Radeon HD 4890 1Go | ✅ | 4.1 | ​ |
| AMD Radeon HD 6750M | ✅ | 4.1 | Algunos errores gráficos menores en el menú |
| AMD Ryzen 5 5600U Vega 7 | ✅ | 9.0 | GPU integrada a la CPU - 100% compatible |
| AMS Ryzen 3 2200G Vega 8 | ✅ | 9.2 | GPU integrada a la CPU - 100% compatible |
| AMD Radeon HD 7970 3G | ❌ | 18.04.20 | No funciona (el menú es muy lento y no se puede jugar) |
| AMD Radeon HD 8280E | ❌ | 18.07.13 | No funciona (el menú es muy lento y no se puede jugar) |
| AMD Radeon RX 480 8gb | ❌ | 6.0 RC2 | No funciona (el menú es muy lento y no se puede jugar) |
