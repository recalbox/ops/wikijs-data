---
title: 2. 🔨 USO AVANZADO
description: 
published: true
date: 2023-09-25T19:20:48.249Z
tags: utilización, avanzada
editor: markdown
dateCreated: 2023-08-28T20:40:36.075Z
---

Esta parte de la documentación describe con detalle algunas funcionalidades útiles para los usuarios más avanzados.

Esta sección contiene las siguientes páginas:

[Sobrecargas de configuración](configuration-override)
[Visualización de sistemas](systems-display)
[Control del ventilador](fan-controller)
[El Arcade en Recalbox](arcade-in-recalbox)
[Actualizaciones manuales o sin conexión a internet](manual-update)
[Pad To Keyboard](pad-to-keyboard)
[RetroArch](retroarch)
[Scripts de eventos de EmulationStation](scripts-on-emulationstation-events)