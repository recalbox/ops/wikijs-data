---
title: Licence
description: Cet espace contient des explications sur les aspects juridiques et autres choses ennuyeuses mais importantes
published: true
date: 2023-07-06T20:10:37.831Z
tags: recalbox, licence
editor: markdown
dateCreated: 2021-05-21T07:51:31.203Z
---

Recalbox utilise plusieurs paquets sous certaines types de licence (GPL, BSD, MIT, etc.) et Recalbox utilise lui-même une licence libre. 

Voici le description de la licence utilisée pour Recalbox :

>La redistribution et l'utilisation du code RECALBOX ou de toute œuvre dérivée sont autorisées sous réserve que les conditions suivantes soient respectées/remplies :
>
>Les redistributions ne peuvent être vendues sans autorisation, ni être utilisées dans un produit ou une activité commerciale.
>Les redistributions qui sont modifiées à partir des sources originales doivent inclure le code source complet, y compris le code source de tous les composants utilisés par une distribution binaire construite à partir des sources modifiées.
>Les redistributions doivent reproduire l'avis de droit d'auteur ci-dessus, la présente liste de conditions et l'avis de non-responsabilité suivant dans la documentation et/ou les autres documents fournis avec la distribution.
>CE LOGICIEL EST FOURNI PAR LES DÉTENTEURS DES DROITS D'AUTEUR ET LES CONTRIBUTEURS " TEL QUEL " ET TOUTE GARANTIE EXPRESSE OU IMPLICITE, Y COMPRIS, MAIS SANS S'Y LIMITER, LES GARANTIES IMPLICITES DE QUALITÉ MARCHANDE ET D'ADÉQUATION À UN USAGE PARTICULIER, EST REJETÉE. EN AUCUN CAS, LE PROPRIÉTAIRE DES DROITS D'AUTEUR OU LES CONTRIBUTEURS NE PEUVENT ÊTRE TENUS RESPONSABLES DE TOUT DOMMAGE DIRECT, INDIRECT, ACCESSOIRE, SPÉCIAL, EXEMPLAIRE OU CONSÉCUTIF (Y COMPRIS, MAIS SANS S'Y LIMITER, L'APPROVISIONNEMENT EN BIENS OU SERVICES DE REMPLACEMENT ; LA PERTE D'UTILISATION, DE DONNÉES OU DE PROFITS ; OU L'INTERRUPTION D'ACTIVITÉ), QUELLE QU'EN SOIT LA CAUSE ET SELON TOUTE THÉORIE DE RESPONSABILITÉ, QU'ELLE SOIT CONTRACTUELLE, DE RESPONSABILITÉ STRICTE OU DÉLICTUELLE (Y COMPRIS LA NÉGLIGENCE OU AUTRE) DÉCOULANT DE QUELQUE FAÇON QUE CE SOIT DE L'UTILISATION DE CE LOGICIEL, MÊME S'ILS ONT ÉTÉ AVISÉS DE LA POSSIBILITÉ DE TELS DOMMAGES.

>Bien que ce ne soit pas une obligation légale encadrée par la licence, nous demandons à toute personne qui souhaiterait créer des œuvres dérivées, des versions modifiées ou des "forks" de Recalbox de ne pas utiliser de code qui n'a pas encore atteint la branche `master`.
{.is-info}