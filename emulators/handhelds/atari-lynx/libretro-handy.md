---
title: Libretro Handy
description: 
published: true
date: 2024-07-24T22:35:58.857Z
tags: libretro, atari, lynx, handy
editor: markdown
dateCreated: 2021-07-09T20:22:58.740Z
---

**Libretro Handy** is an Atari Lynx video game system emulator that can be used as the core of libretro.  
Handy was the original name of the Lynx project that was started at Epyx and then ended by Atari.

## ![](/emulators/license.svg) License

This core is under [**zlib**](https://sourceforge.net/projects/handy/) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Features

| Feature | Supported |
| :---: | :---: |
| Restart | ✔ |
| Screenshots | ✔ |
| States | ✔ |
| Rewind | ✔ |
| Netplay (State based) | ✔ (not link-cable emulation) |
| Core Options | ✔ |
| RetroAchievements | ✔ |
| Controls | ✔ |
| Remapping | ✔ |

## ![](/emulators/bios.svg) BIOS

### List of mandatory bios

| Filename | Description | MD5 | Provided |
| :---: | :---: | :---: | :---: |
| lynxboot.img | Lynx Boot Image | fcd403db69f54290b51035d82f835e7b | ❌  |

### Location

Put the bios like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 lynx
┃ ┃ ┃ ┃ ┣ 🗒 **lynxboot.img**

## ![](/emulators/roms.png) Roms

### Supported extensions

Rom must have the extension:

* .lnx
* .o
* .zip
* .7z

This system supports compressed roms in .zip/.7z format. However, be careful, it is only an archive.

The files contained in the .zip/.7z must correspond to the extensions mentioned above.
Moreover, each .zip/.7z file must contain only one compressed rom.

### Location

Put your roms like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 lynx
┃ ┃ ┃ ┃ ┣ 🗒 **game.lnx**

>The roms using **No-Intro** format are highly recommended.
{.is-success}

>For more information about roms, go to [this tutorial](./../../../tutorials/games/generalities/isos-and-roms)!
{.is-info}

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Accessing to options

You can configure various options in two different ways.

* Via RetroArch menu:

┣ 📁 RetroArch Menu
┃ ┣ 📁 Core options
┃ ┃ ┣ 🧩 Name_option

* Via the file `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core options

| Options | Options values | Variables (for configuration override) | Variables values |
| --- | --- | --- | --- |
| Video Refresh Rate | `50Hz` / `60Hz` ✅ / `75Hz` / `100Hz` / `125Hz` | `handy_refresh_rate` | `50` / `60` / `75` / `100` / `125` |
| Display Rotation | `Auto` ✅ / `Disabled` / `Clockwise` / `Bottom` / `Anticlockwise` | `handy_rot` | `Auto` / `None` / `90` / `180` / `270` |
| Color Depth (Restart Required) | `Thousands (16-bit)` ✅ / `Millions (24-bit)` | `handy_gfx_colors` | `16bit` / `24bit` |
| LCD Ghosting Filter | `Disabled` ✅ / `2 Frames` / `3 Frames` / `4 Frames` | `handy_lcd_ghosting` | `disabled` / `2frames` / `3frames` / `4frames` |
| CPU Overclock Multiplier | `1` ✅ / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `20` / `30` / `40` / `50` | `handy_overclock` | `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `20` / `30` / `40` / `50` |
| Frameskip | `Disabled` ✅ / `Auto` / `Manuel` | `handy_frameskip` | `disabled` / `auto` / `manual` |
| Frameskip Threshold (%) | `15` / `18` / `21` / `24` / `27` / `30` / `33` ✅ / `36` / `39` / `42` / `45` / `48` / `51` / `54` / `57` / `60` | `handy_frameskip_threshold` | `15` / `18` / `21` / `24` / `27` / `30` / `33` / `36` / `39` / `42` / `45` / `48` / `51` / `54` / `57` / `60` |

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/libretro/libretro-handy](https://github.com/libretro/libretro-handy)
* **Libretro documentation**: [https://docs.libretro.com/library/handy/](https://docs.libretro.com/library/handy/)
* **Official source code**: [http://handy.sourceforge.net/download.htm](http://handy.sourceforge.net/download.htm)
* **Official website**:  [http://handy.sourceforge.net/](http://handy.sourceforge.net/)