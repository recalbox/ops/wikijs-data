---
title: Libretro BK
description: 
published: true
date: 2024-07-17T21:06:38.444Z
tags: libretro, elektronika, bk, 7.2+
editor: markdown
dateCreated: 2023-09-11T09:21:10.813Z
---



## ![](/emulators/license.svg) Licencia

El core posee la siguiente licencia [**personnalisé**](https://github.com/libretro/bk-emulator/blob/master/COPYING).

## ![](/emulators/compatibility.png) Compatibilidad

 | RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | 
| ✅  | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Funcionalidades

| Funcionalidad | ¿Soportada? |
| :---: | :---: |
| Capturas de pantalla | ✔ |
| Rebobinado | ✔ |
| Opciones del core | ✔ |
| Controles | ✔ |
| Remapping | ✔ |

## ![](/emulators/bios.svg) BIOS

### Lista de BIOS obligatorias

| Nombre del fichero | Descripción | MD5 | ¿Preinstalada? |
| :---: | :---: | :---: | :---: |
| B11M_BOS.ROM | Requis | fe4627d1e3a1535874085050733263e7 | ❌ |
| B11M_EXT.ROM | Requis | dc52f365d56fa1951f5d35b1101b9e3f | ❌ |
| BAS11M_0.ROM | Requis | 946f6f23ded03c0e26187f0b3ca75993 | ❌ |
| BAS11M_1.ROM | Requis | 1e6637f32aa7d1de03510030cac40bcf | ❌ |
| DISK_327.ROM | Requis | 5015228eeeb238e65da8edcd1b6dfac7 | ❌ |
| FOCAL10.ROM | Requis | 5737f972e8638831ab71e9139abae052 | ❌ |
| MONIT10.ROM | Requis | 95f8c41c6abf7640e35a6a03cecebd01 | ❌ |

### Emplazamiento

Disponed vuestras bios como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 bk
┃ ┃ ┃ ┃ ┣ 🗒 **B11M_BOS.ROM**
┃ ┃ ┃ ┃ ┣ 🗒 **B11M_EXT.ROM**
┃ ┃ ┃ ┃ ┣ 🗒 **BAS11M_0.ROM**
┃ ┃ ┃ ┃ ┣ 🗒 **BAS11M_1.ROM**
┃ ┃ ┃ ┃ ┣ 🗒 **DISK_327.ROM**
┃ ┃ ┃ ┃ ┣ 🗒 **FOCAL10.ROM**
┃ ┃ ┃ ┃ ┣ 🗒 **MONIT10.ROM**

## ![](/emulators/roms.png) Roms

### Extensiones de roms soportadas

Las roms deben tener las extensiones siguientes:

* .bin
* .zip

Este sistema soporta las roms comprimidas en formato .zip/.7z. No obstante, se trata sólo de un archivo.

Los ficheros contenidos en los .zip/.7z deben tener las extensiones citadas previamente.
Además, cada fichero .zip/.7z sólo debe contener un fichero rom al interior.

### Emplazamiento

Disponed vuestras roms como en la siguiente imagen: 

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 bk
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.zip**

>Se aconsejan las roms del proyecto **TOSEC**.
{.is-success}

>Para más información sobre las roms, consultad [este tutorial](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

>Para conservar las configuraciones personalizadas cuando se produce una actualización del sistema Recalbox, os aconsejamos utilizar las [Sobrecargas de configuración](./../../../advanced-usage/configuration-override).
{.is-info}

### Acceder a las opciones

Podéis configurar las opciones del core de dos formas diferentes.

* Via el Menú RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via el fichero `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Opciones del core

| Opción | Valor de la opción | Variable (para la sobrecarga) | Valores posibles |
| --- | --- | --- | --- |
| Model (restart) | `BK-0010` ✅ / `BK-0010.01` / `BK-0010.01 + FDD` / `BK-0011M + FDD` / `Terak 8510/a` / `Slow BK-0011M` | `bk_model` | `BK-0010` / `BK-0010.01` / `BK-0010.01 + FDD` / `BK-0011M + FDD` / `Terak 8510/a` / `Slow BK-0011M` |
| Peripheral (UP port, restart) | `none` ✅ / `covox` / `ay_3_8910` / `mouse_high` / `mouse_low` / `joystick` | `bk_peripheral` | `none` / `covox` / `ay_3_8910` / `mouse_high` / `mouse_low` / `joystick` |
| Keyboard layout | `qwerty` ✅ / `jcuken` | `bk_layout` | `qwerty` / `jcuken` |
| Double CPU speed | `Désactivé` ✅ / `Activé` | `bk_doublespeed` | `disabled` / `enabled` |
| Use color display | `Activé` ✅ / `Désactivé` | `bk_color` | `enabled` / `disabled` |
 | Keboard type (restart) | `poll` ✅ / `callback` | `bk_keyboard_type` | `poll` / `callback` |
 | Aspect ratio | `1:1` ✅ / `4:3` | `bk_aspect_ratio` | `1:1` / `4:3` |


## ![](/emulators/external-links.png) Enlaces externos

* **Código fuente utilizado** : [https://github.com/libretro/bk-emulator/](https://github.com/libretro/bk-emulator/)
* **Documentación Libretro** : [https://docs.libretro.com/library/bk/](https://docs.libretro.com/library/bk/)