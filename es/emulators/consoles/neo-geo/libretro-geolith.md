---
title: Libretro Geolith
description: 
published: true
date: 2024-07-26T06:37:19.441Z
tags: libretro, neo-geo, 9.2+, geolith
editor: markdown
dateCreated: 2024-07-26T06:29:37.118Z
---

## ![](/emulators/license.svg) Licencia

Este núcleo está bajo la licencia [**BSD-3**](https://github.com/libretro/geolith-libretro/blob/master/LICENSE).

## ![](/emulators/compatibility.png) Compatibilidad

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Funcionalidades

| Funcionalidad | Soportado |
| :---: | :---: |

## ![](/emulators/bios.svg) BIOS

### Lista de BIOS opcionales

| Nombre del archivo | Descripción | MD5 | Proporcionado |
| :---: | :---: | :---: | :---: |
| aes.zip | BIOS Neo-Geo AES | ad9585c72130c56f04ae26aae87c289d 7a4dcd56b3924215aac7f903ff907339 | ❌ |
| neogeo.zip | BIOS Neo-Geo | 00dad01abdbf8ea9e79ad2fe11bdb182 912c0a56bafe0fba39d0c668b139b250 | ❌ |

### Ubicación

Coloca los BIOS de la siguiente manera:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 geolith
┃ ┃ ┃ ┃ ┣ 🗒 **aes.zip**
┃ ┃ ┃ ┃ ┣ 🗒 **neogeo.zip**

## ![](/emulators/roms.png) Roms

### Extensiones soportadas

La rom debe tener la extensión:

* .neo

### Ubicación

Coloca las roms de la siguiente manera:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 neogeo
┃ ┃ ┃ ┃ ┣ 🗒 **juego.neo**

>Para más información sobre las roms, consulta [este tutorial](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

>Para poder conservar vuestras configuraciones personalizadas durante una actualización, te recomendamos utilizar nuestra funcionalidad de [Sobrecargas de configuración](./../../../advanced-usage/configuration-override).
{.is-info}

### Acceder a las opciones

Podeis configurar diversas opciones de dos maneras diferentes.

* A través del menú RetroArch:

┣ 📁 Menú RetroArch
┃ ┣ 📁 Opciones del núcleo
┃ ┃ ┣ 🧩 Nombre_opción

* A través del archivo `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Opciones del núcleo

| Opciones | Valores de las opciones | Variable (para sobrecarga) | Valores de las variables |
| --- | --- | --- | --- |
| System Type (Restart) | `Neo Geo AES (Consola Hogareña)` ✅ / `Neo Geo MVS (Arcade)` / `Universe BIOS (BIOS mejorado por la comunidad)` | `geolith_system_type` | `aes` / `mvs` / `uni` |
| Region (Restart) | `EE.UU.` ✅ / `Japón` / `Asia` / `Europa` | `geolith_region` | `us` / `jp` / `as` / `eu` |
| Setting Mode (Restart, DIP Switch)	 | `Desactivado` ✅ / `Activado` | `geolith_settingmode` | `off` / `on` |
| Four Player Mode (Restart, Asia/Japan MVS Only) | `Desactivado` ✅ / `Activado` | `geolith_4player` | `off` / `on` |
| Freeplay (DIP Switch) | `Desactivado` ✅ / `Activado` | `geolith_freeplay` | `off` / `on` |
|Mask Overscan (Top) | `16` / `12` / `8` ✅ / `4` / `0` | `geolith_overscan_t` | `16` / `12` / `8` / `4` / `0` |
| Mask Overscan (Bottom) | `16` / `12` / `8` ✅ / `4` / `0` | `geolith_overscan_b` | `16` / `12` / `8` / `4` / `0` |
| Mask Overscan (Left) | `16` / `12` / `8` ✅ / `4` / `0` | `geolith_overscan_l` | `16` / `12` / `8` / `4` / `0` |
| Mask Overscan (Right) | `16` / `12` / `8` ✅ / `4` / `0` | `geolith_overscan_r` | `16` / `12` / `8` / `4` / `0` |
| Aspect Ratio | `Píxeles Perfectamente Cuadrados (1:1 PAR)` ✅ / `Relación de Aspecto NTSC Aparente (45:44 PAR)` / `Relación de Aspecto NTSC Muy Tradicional (4:3 PAR)` | `geolith_aspect` | `1:1` / `45:44` / `4:3` |
| Sprites-per-line limit (Hack) | `Precisión de Hardware (96)` ✅ / `Doble (192)` / `Triple (288)` / `MAX 381 MEGA PRO-GEAR SPEC` | `geolith_sprlimit` | `96` / `192` / `288` / `381` |
| Overclocking (Hack) | `Desactivado` ✅ / `Activado` | `geolith_oc` | `off` / `on` |

## ![](/emulators/external-links.png) Enlaces externos

* **Código fuente utilizado:** [https://github.com/libretro/geolith-libretro/](https://github.com/libretro/geolith-libretro)
* **Documentación de Libretro** :
