---
title: Blacklist wifi intégré du RPI 3
description: 
published: true
date: 2021-05-21T10:37:22.222Z
tags: 
editor: markdown
dateCreated: 2021-05-21T08:39:30.490Z
---

Déçu par le **débit du wifi intégré** du raspberry pi 3, tu souhaites le **désactiver**.

Voici comment faire :

* **Connecte toi** en [ssh via putty ou winscp](/fr/tutorials/system/access/network-access-winscp) ​
* **Monter** la partition en écriture :  `mount -o remount,rw /`  
* **Éditer** le fichier suivant : `nano /etc/modprobe.d/blacklist.conf`   
* **Ajouter** cette ligne : `blacklist brcmfmac`  
* **Enregistre** `ctrl+x` + **`Y` puis** `reboot`  
* **Branche** ton dongle wifi et **teste**.

