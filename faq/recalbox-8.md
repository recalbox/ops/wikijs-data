---
title: FAQ Recalbox 8
description: Answers to frequently asked questions for Recalbox 8
published: true
date: 2023-07-19T07:28:09.707Z
tags: 
editor: markdown
dateCreated: 2023-07-18T07:30:57.597Z
---

In this FAQ, you will find known and identified issues about version 8. We make our best to fix them :)

## Known issues

### Black screen issue


If you have a black screen issue on RPi4 or RPi3, you can fix it following those simple steps:
* Put your SD card in your PC
* In the `RECALBOX` partition, open the file `cmdline.txt`
* Add this text at the end of the line : `video=HDMI-A-1:D video=HDMI-A-2:D`
* The file should look like this:
```
dwc_otg.fiq_fix_enable=1 sdhci-bcm2708.sync_after_dma=0 dwc_otg.lpm_enable=0 console=tty3 consoleblank=0 loglevel=3 elevator=deadline vt.global_cursor_default=0 logo.nologo splash label=RECALBOX rootwait fastboot noswap video=HDMI-A-1:D video=HDMI-A-2:D
```
* Save the file
* Eject the SD card properly
* Test again in the Raspberry Pi.


### RPI3

| Issue location | Issue description | Resolved? |
| --- | --- | --- |
| General | No display for some HDMI & DSI screens | ✅ |

### RPI4

| Issue location | Issue description | Resolved? |
| --- | --- | --- |
| Kodi | Error message at startup | ✅ |
| Kodi | Gamepad not identified, mandatory to map buttons in Kodi options | ✅ |
| Kodi | Difficulties to play Netflix content | ❌ |


### PC

| Issue location | Issue description | Resolved? |
| --- | --- | --- |
| Kodi | Error message at startup | ✅ |
| Kodi | Gamepad not identified, mandatory to map buttons in Kodi options | ✅ |
| PCSX2 | Problems with DPAD, directions are rotated 1/4 clockwise (left for down, etc) | ✅ |
| Dolphin | Can't exit from GameCube games with gamepad (forced to use keyboard ALT + F4) | ❌ |

### Bricked Odroid GO Advanced/Super

First release of 8.0.1 image was corrupted. A brand new image is now available.
For those who have a bricked Odroid GO Advanced or Super, the screen stays on the Greyed Recalbox Logo, here are few steps to unbrick the device:
1. download this file http://upgrade.recalbox.com/latest/odroidgo2/boot.tar.xz,
2. use your favorite decompressor, it must support XZ and TAR fomat (Easy 7 zip seems to be able to do it). Open the boot.tar.xz file,
3. insert the SD card of the Odroid in your PC,
4. copy the 3 files from the boot directory of the archive (linux, recalbox and uInitrd) to the SD card, in the boot directory too. This will overwrite the corrupted files on the SD card,
5. eject the SD card and insert it in the Odroid and enjoy!
