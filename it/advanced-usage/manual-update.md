---
title: Aggiornamento manuale e/o offline
description: Come aggiornare Recalbox manualmente e senza connessione internet
published: true
date: 2024-06-22T06:24:54.904Z
tags: 
editor: markdown
dateCreated: 2024-06-22T06:24:54.904Z
---

## Quando utilizzare l'aggiornamento manuale?


Potrebbe essere necessario in alcuni casi avviare un aggiornamento manuale della tua Recalbox:
- Per aggiornare una Recalbox senza connessione a internet.
- Quando il team ti chiede di testare un'immagine di *sviluppo* per verificare la correzione di un bug.
- Quando hai un problema di sistema (molto raro).

> Gli esempi seguenti saranno utilizzati con l'immagine del Raspberry Pi 4, dovrai ovviamente adattare questi esempi alla tua scheda.
> {.is-info}

Per fare ciò, dovrai recuperare due file:
- Il **file immagine** di Recalbox, chiamato `recalbox-rpi4.img.xz`
- Il file **checksum** che contiene la somma di controllo dell'immagine per verificarne automaticamente l'integrità. Ha lo stesso nome dell'immagine, ma con l'estensione `.sha1`, quindi nel nostro esempio `recalbox-rpi4.img.xz.sha1`.

Scaricare l'immagine ufficiale
-------------------------------

Per aggiornare la tua Recalbox alla versione più recente disponibile in modalità offline, vai su download.recalbox.com e seleziona `Vedi tutte le immagini di Recalbox`:

![seeallimages.png](/advanced-usage/manual-update/seeallimages.png)

Scarica quindi l'immagine corrispondente alla tua scheda e il checksum `sha1` corrispondente:

![download.png](/advanced-usage/manual-update/download.png)

Copiare i file sulla scheda SD
------------------------------

Ora che hai i due file `recalbox-rpi4.img.xz` e `recalbox-rpi4.img.xz.sha1`, devi solo posizionarli nella cartella `update` della partizione denominata `RECALBOX` della scheda SD su cui è installata Recalbox:

![images-in-boot-partition.png](/advanced-usage/manual-update/images-in-boot-partition.png)

Avviare l'installazione
-----------------------

L'installazione verrà avviata automaticamente al successivo avvio della tua Recalbox. Per verificare che l'installazione sia stata eseguita correttamente, puoi visualizzare il menu con il pulsante `Start` e notare il cambio di versione.