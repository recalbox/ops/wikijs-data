---
title: 4. 🔧 COMPATIBILITÉ MATÉRIEL
description: Vérifiez la compatibilité de votre matériel !
published: true
date: 2023-07-18T11:52:43.660Z
tags: compatibilité, matériel
editor: markdown
dateCreated: 2021-05-21T07:48:46.220Z
---

Sur les pages suivantes, vous pouvez vérifier si votre matériel a été testé et est supportée, ainsi que vérifier si un système ou émulateur spécifique est compatible avec votre matériel.