---
title: Gestión de las cartas de memoria
description: 
published: true
date: 2023-09-21T11:08:10.862Z
tags: ps1, gestión, memoria, cartas
editor: markdown
dateCreated: 2023-09-21T11:08:10.862Z
---

## Activar la segunda tarjeta de memoria

### ¿Cual es el interés?

Esto os permite tener una segunda tarjeta de memoria durante el juego que es común a todos los juegos (y por lo tanto única).

### Pasos

* Iniciad cualquier juego de PSX.
* Id al menú de RetroArch con `Hotkey + B` e id a `Opciones`.

![](/tutorials/games/guides/playstation-1/memory-card-management/psxbios1.png){.full-width}

* Habilitad la opción `Habilitar segunda tarjeta de memoria`.

![](/tutorials/games/guides/playstation-1/memory-card-management/psxbios2.png){.full-width}

* Salid del menú.

>Este procedimiento debe realizarse para cada juego para el que desees tener acceso a esta tarjeta de memoria. Os recomendamos salid del juego y reiniciad después de haber aplicado esta manipulación.
{.es-aviso}

Ahora veréis una tarjeta de memoria utilizable en la ranura 2. En la carpeta `/share/saves/psx/` veréis un fichero llamado `pcsx-card2.mcd` que es el fichero de la tarjeta de memoria.

## Acceder al editor de la carta memoria

### ¿Cual es el interés?

Esto os permite gestionar vuestras grabaciones (copiar o borrar) si lo necesitáis.

### Etapas

* Lanzad un juego.
* Id dentro del menú RetroArch con `Hotkey + B` et id a `Options`.

![](/tutorials/games/guides/playstation-1/memory-card-management/psxbios1.png){.full-width}

* Activad la opción `Show Bios Bootlogo`.

![](/tutorials/games/guides/playstation-1/memory-card-management/psxbios3.png){.full-width}

* Volver al juego y salid complétamente de él.
* Relanzad el juego y durante la secuencia de arranque, ejectadlo con `Hotkey + Stick izquierdo hacia arriba`.
* Deberíais llegar a la bios de la consola y poder gestionar vuestras grabaciones.

![](/tutorials/games/guides/playstation-1/memory-card-management/psxbios4.png){.full-width}

Si habéis activado la segunda tarjeta de memoria, como se ha descrito más arriba, deberíais poder copier los datos entre las dos tarjetas.

![](/tutorials/games/guides/playstation-1/memory-card-management/psxbios5.png){.full-width}