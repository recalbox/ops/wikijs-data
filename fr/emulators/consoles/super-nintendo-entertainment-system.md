---
title: Super Nintendo Entertainment System
description: Super Nintendo Entertainment System
published: true
date: 2023-06-27T19:13:49.723Z
tags: consoles, nintendo, snes, super nintendo
editor: markdown
dateCreated: 2021-05-21T08:01:37.115Z
---

![](/emulators/consoles/snes.svg){.align-center}

## Fiche technique

* **Fabricant** : Nintendo
* **Année de sortie** : 1990
* **Quantités vendues** : 49.10 millions
* **Meilleur jeu vendu** : Super Mario World
* **Processeur** : 65C816 @ 3.58 MHz 16-bit modifié
* **RAM** : 128kB / VRAM : 64kB
* **Vidéo** : 16-bit PPU (Picture Processor Unit)
* **Résolution** : 256x224, 256 couleurs (palette de 32768 couleurs)
* **Puce sonore** : 8-bit Sony SPC700, 8 canaux
* **Taille des cartouches** : 256kB - 6MB

## Présentation

La **Super Nintendo Entertainment System** (couramment abrégée **Super NES** ou **SNES** ou encore **Super Nintendo**), ou **Super Famicom**  au Japon, est une console de jeux vidéo du constructeur japonais Nintendo commercialisée à partir de novembre 1990. En Amérique du Nord, la console est sortie avec un look résolument différent. À noter qu'en Corée du Sud, la Super Nintendo est distribuée par Hyundai Electronics sous le nom de **Super Comboy**.

En 1997 et 1998, Nintendo décide de rafraîchir le design en sortant le modèle SNS-101, parfois appelé **Super NES 2** aux États-Unis, et la **Super Famicom Jr.** au Japon.

## Émulateurs

[Libretro Mesen_s](libretro-mesen_s)
[Libretro Snes9x](libretro-snes9x)
[Libretro Snes9x_2002](libretro-snes9x-2002)
[Libretro Snes9x_2005](libretro-snes9x-2005)
[Libretro Snes9x_2010](libretro-snes9x-2010)
[Libretro bsnes](libretro-bsnes)
[Libretro bsnes HD](libretro-bsnes-hd)
[Libretro-supafaust](libretro-supafaust)
[piSNES](pisnes)