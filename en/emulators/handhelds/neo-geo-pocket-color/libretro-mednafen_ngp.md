---
title: Libretro Mednafen_NGP
description: 
published: true
date: 2021-11-04T01:07:47.003Z
tags: libretro, color, neo-geo, mednafen, pocket, ngp, ngpc
editor: markdown
dateCreated: 2021-07-27T05:31:13.960Z
---

**Libretro Mednafen_NGP** is an emulator of SNK Neo Geo Pocket and Neo Geo Pocket Color video game system based on NeoPop.

It was written by:

* neopop_uk
* Mednafen Team

## ![](/emulators/license.svg) License

This core is under [**GPLv2**](https://github.com/libretro/beetle-ngp-libretro/blob/master/COPYING) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Features

| Feature | Supported |
| :---: | :---: |
| Restart | ✔ |
| Screenshots | ✔ |
| Saves | ✔ |
| States | ✔ |
| Rewind | ✔ |
| Netplay | ✔ (not link-cable emulation) |
| Core Options | ✔ |
| RetroAchievements | ✔ |
| RetroArch Cheats | ✔ |
| Controls | ✔ |
| Remapping | ✔ |

## ![](/emulators/bios.svg) BIOS

>**No bios is required.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Supported extensions

Rom must have the extension:

* .ngpc
* .zip
* .7z

This system supports compressed roms in .zip/.7z format. However, be careful, it is only an archive.

The files contained in the .zip/.7z must correspond to the extensions mentioned above.
Moreover, each .zip/.7z file must contain only one compressed rom.

### Location

Put your roms like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 ngpc
┃ ┃ ┃ ┃ ┣ 🗒 **game.zip**

>The roms using **No-Intro** format are highly recommended.
{.is-success}

>For more information about roms, go to [this tutorial](./../../../tutorials/games/generalities/isos-and-roms)!
{.is-info}

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Accessing to options

You can configure various options in two different ways.

* Via RetroArch menu:

┣ 📁 RetroArch Menu
┃ ┣ 📁 Core options
┃ ┃ ┣ 🧩 Name_option

* Via the file `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core options

| Options | Options values | Variables (for configuration override) | Variables values |
| --- | --- | --- | --- |
| Language (\*) | `english` ✅ / `japanese` | `ngp_language` | `english` / `japanese` |
| Sound Output Sample Rate | `11025` / `22050` / `44100` ✅ / `48000` / `96000` / `192000` / `384000` | `ngp_sound_sample_rate` | `11025` / `22050` / `44100` / `48000` / `96000` / `192000` / `384000` |
| Color Depth | `Thousands (16-bit)` ✅ / `Millions (24-bit)` | `ngp_gfx_colors` | `16bit` / `24bit` |

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/libretro/beetle-ngp-libretro/](https://github.com/libretro/beetle-ngp-libretro/)
* **Libretro documentation**: [https://docs.libretro.com/library/beetle\_neopop/](https://docs.libretro.com/library/beetle_neopop/)
* **Official source code**: [https://mednafen.github.io/releases/](https://mednafen.github.io/releases/)
* **Official documentation**: [https://mednafen.github.io/documentation/ngp.html](https://mednafen.github.io/documentation/ngp.html)
* **Official website**: [https://mednafen.github.io/](https://mednafen.github.io)
* **Forum officiel** : [https://forum.fobby.net/](https://forum.fobby.net/)