---
title: TRS-80 Color Computer
description: 
published: true
date: 2021-11-16T07:45:48.733Z
tags: color, trs-80, computer, trs-80-coco, ordinateurs, tandy, 8.0+
editor: markdown
dateCreated: 2021-08-21T09:36:04.796Z
---

![](/emulators/computers/trs80coco.svg){.align-center}

## Fiche technique

* **Fabricant** : Tandy Corporation
* **Année de sortie** : 1980
* **Système d'exploitation** : Microsoft Color Basic v1.0
* **Processeur** : MC6809E @ 0.89 MHz
* **RAM** : 4KB - 16KB
* **Lecteur** : Lecteur cassettte, lecteur disquette 5.25"
* **Résolution** : Possibilité d'affichage de 32 colonnes (32 x 16 avec 4 couleurs / 256 x 192 avec 2 couleurs / 128 x 96 avec 8 couleurs / 128 x 192 avec 4 couleurs)

## Présentation

Le **TRS-80 Color Computer** de RadioShack (**Tandy Color Computer** ou encore **CoCo**) était un ordinateur personnel basé sur le microprocesseur 6809 de Motorola et faisait partie de la gamme des TRS-80.

Le CoCo a commencé sa vie sous la forme d'un système de référence chez Motorola et était destiné à servir de terminal Videotext. En fait, une version simplifiée du CoCo s'est vendue en tant que terminal Videotext avec le même boîtier et le même clavier. Le CoCo original était en versions 4 ko, 16 ko et 32 ko, bien que des bricoleurs eussent rapidement découvert une façon de transformer les systèmes à 32 ko en systèmes à 64 ko en activant le second banc de mémoire vive (qui était désactivé lors de la fabrication). Le boîtier gris et le clavier « chiclet » du CoCo I furent abandonnés en faveur du clavier blanc de taille normale des CoCo II et III.

## Émulateurs

[XRoar](xroar)