---
title: Libretro Cap32
description: 
published: true
date: 2024-07-24T21:46:20.392Z
tags: libretro, amstrad, cpc, cap32
editor: markdown
dateCreated: 2021-05-21T08:14:58.252Z
---



## ![](/emulators/license.svg) Licence

Ce core est sous licence [**GPLv2**](https://github.com/ColinPitrat/caprice32/blob/master/COPYING.txt).

## ![](/emulators/compatibility.png) Compatibilité

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Fonctionnalités

| Fonctionnalité | Supporté |
| :---: | :---: |
| Redémarrage  | ✔ |
| Captures d'écran | ✔ |
| Sauvegardes | ✔ |
| Sauvegardes instantanées | ✔ |
| Rembobinage | ✔ |
| Netplay | ✔ |
| Options du core | ✔ |
| RetroAchievements | ✔ |
| Contrôles | ✔ |
| Remapping | ✔ |
| Contrôle du disque | ✔ |

## ![](/emulators/bios.svg) BIOS

>**Aucun bios n'est requis.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Extensions supportées

Les roms doivent avoir les extensions suivantes :

* .bin
* .cdt
* .cpr
* .dsk
* .m3u
* .sna
* .tap
* .voc
* .zip
* .7z

Ce système supporte les roms compressées au format .zip/.7z. Attention toutefois, il ne s'agit que d'une archive.

Les fichiers contenus dans les .zip/.7z doivent correspondre aux extensions citées précédemment.
De plus, chaque fichier .zip/.7z ne doit contenir qu'une seule rom compressée.

>Les jeux avec l'extension .bin doivent obligatoirement être décompressés !
{.is-info}

### Emplacement

Placez les roms comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 amstradcpc
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.zip**

>Les roms au format **TOSEC** sont vivement conseillées.
{.is-success}

>Pour plus d'information sur les roms, rendez-vous sur [ce tutoriel](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuration avancée de l'émulateur

>Pour pouvoir conserver vos configurations personnalisées lors d'une mise à jour, nous vous conseillons d'utiliser notre fonctionnalité [Surcharges de configuration](./../../../advanced-usage/configuration-override).
{.is-info}

### Accéder aux options

Vous pouvez configurer diverses options de deux façons différentes.

* Via le menu RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via le fichier `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Options du core

| Options | Valeur des options | Variable (pour surcharge) | Valeurs des variables |
| --- | --- | --- | --- |
| Controls > User 1 Controller Config | `auto` ✅ / `qaop` / `incentive` / `joystick_port1` | `cap32_retrojoy0` | `auto` / `qaop` / `incentive` / `joystick_port1` |
| Controls > User 2 Controller Config | `auto` ✅ / `qaop` / `incentive` / `joystick_port1` / `joystick_port2` | `cap32_retrojoy1` | `auto` / `qaop` / `incentive` / `joystick_port1` / `joystick_port2` |
| Controls > Combo Key | `select` / `y` ✅ / `b` / `Désactivé` | `cap32_combokey` | `select` / `y` / `b` / `disabled` |
| Controls > Use internal Remap DB | `Activé` ✅ / `Désactivé` | `cap32_db_mapkeys` | `enabled` / `disabled` |
| Light Gun > Input | `Désactivé` ✅ / `phaser` / `gunstick` | `cap32_lightgun_input` | `disabled` / `phaser` / `gunstick` |
| Light Gun > Show Crosshair | `Désactivé` ✅ / `Activé` | `cap32_lightgun_show` | `disabled` / `enabled` |
| Model | `6128` ✅ / `464` / `664` / `6128+ (experimental)` | `cap32_model` | `6128` / `464` / `664` / `6128+ (experimental)` |
| Advanced > Autorun | `Activé` ✅ / `Désactivé` | `cap32_autorun` | `enabled` / `disabled` |
| Advanced > Ram size | `64` / `128` ✅ / `192` / `512` / `576` | `cap32_ram` | `64` / `128` / `192` / `512` / `576` |
| Video > Green Phosphor blueish | `5` / `10` / `15` ✅ / `20` / `30` | `cap32_advanced_green_phosphor` | `5` / `10` / `15` / `20` / `30` |
| Video > Monitor Intensity | `5` / `6` / `7` / `8` ✅ / `9` / `10` / `11` / `12` / `13` / `14` / `15` | `cap32_scr_intensity` | `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` |
| Video Advanced > Color Depth | `16bit` ✅ / `24bit` | `cap32_gfx_colors` | `16bit` / `24bit` |
| Video Advanced > Crop Screen Borders | `Désactivé` ✅ / `Activé` | `cap32_scr_crop` | `disabled` / `enabled` |
| Status Bar | `onloading` / `Activé` / `Désactivé` | `cap32_statusbar` | `onloading` / `enabled` / `disabled` |
| Keyboard Transparency | `Désactivé` ✅ / `Activé` | `cap32_` | `disabled` / `enabled` |
| Floppy Sound | `Activé` ✅ / `Désactivé` | `cap32_floppy_sound` | `enabled` / `disabled` |
| Monitor Type | `color` ✅ / `green` / `white` | `cap32_scr_tube` | `color` / `green` / `white` |
| CPC Language | `english` ✅ / `french` / `spanish` | `cap32_lang_layout` | `english` / `french` / `spanish` |

## ![](/emulators/external-links.png) Liens externes

* **Code source utilisé** : [https://github.com/libretro/libretro-cap32/](https://github.com/libretro/libretro-cap32/)
* **Documentation Libretro** : [https://docs.libretro.com/library/caprice32/](https://docs.libretro.com/library/caprice32/)