---
title: Libretro Gearsystem
description: 
published: true
date: 2024-07-29T14:00:47.786Z
tags: libretro, gearsystem, multivision
editor: markdown
dateCreated: 2023-09-06T13:53:24.924Z
---

**Libretro Gearsystem** es un emulador de código abierto, multiplataforma, Sega Mark III / Master System / Game Gear / SG-1000 / Othello Multivision escrito en C++.

* Núcleo Z80 muy preciso, incluyendo opcodes y comportamientos no documentados como los registros R y MEMPTR.
* Soporte de Multi-Mapper: cartuchos SEGA, Codemasters, SG-1000 y ROM únicamente.
* Detección automática de región: NTSC-JAP, NTSC-USA, PAL-EUR.
* Base de datos interna para la detección de ROMs.
* Emulación VDP muy precisa, incluyendo la sincronización y el soporte del modo SMS2 únicamente.
* Emulación de audio utilizando SDL Audio y la biblioteca Sms_Snd_Emu.
* Soporte para guardados de RAM alimentados por batería.
* Guardar estados.
* Soporte para trucos Game Genie y Pro Action Replay.
* Funciona en Windows, Linux, Mac OS X, Raspberry Pi, iOS y como núcleo Libretro (RetroArch).

El núcleo Libretro Gearsystem fue creado por Ignacio Sánchez.

## ![](/emulators/license.svg) Licencia

Este núcleo está bajo la licencia [**GPLv3**](https://github.com/drhelius/Gearsystem/blob/master/LICENSE).

## ![](/emulators/compatibility.png) Compatibilidad

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Funcionalidades

| Funcionalidad | Soportado |
| :---: | :---: |
| Reinicio | ✔ |
| Capturas de pantalla | ✔ |
| Guardados | ✔ |
| Guardados instantáneos | ✔ |
| Rebobinado | ✔ |
| Opciones del núcleo | ✔ |
| RetroAchievements | ✔ |
| Cheats RetroArch - Game Genie | ✔ |
| Cheats RetroArch - Pro Action Replay | ✔ |
| Controles | ✔ |
| Remapeo | ✔ |
| Softpatching | ✔ |

## ![](/emulators/bios.svg) BIOS

>**No se requiere ningún BIOS.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Extensiones soportadas

Los roms deben tener las siguientes extensiones:

* .bin
* .sg
* .zip
* .7z

Este sistema soporta roms comprimidos en formato .zip/.7z. Sin embargo, solo se trata de un archivo.

Los archivos contenidos en los .zip/.7z deben corresponder a las extensiones mencionadas anteriormente.
Además, cada archivo .zip/.7z no debe contener más de una rom comprimida.

### Ubicación

Coloque los roms de la siguiente manera:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 multivision
┃ ┃ ┃ ┃ ┣ 🗒 **juego.zip**

>Se recomienda encarecidamente el uso de roms en formato **TOSEC**.
{.is-success}

>Para más información sobre los roms, visite [este tutorial](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

>Para poder conservar sus configuraciones personalizadas durante una actualización, le recomendamos utilizar nuestra funcionalidad [Sobrecargas de configuración](./../../../advanced-usage/configuration-override).
{.is-info}

### Acceder a las opciones

Puede configurar diversas opciones de dos maneras diferentes.

* A través del menú RetroArch:

┣ 📁 Menú RetroArch
┃ ┣ 📁 Opciones del núcleo
┃ ┃ ┣ 🧩 Name_option

* A través del archivo `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Opciones del núcleo

| Opciones | Valor de las opciones | Variable (para sobrecarga) | Valores de las variables |
| --- | --- | --- | --- |
| System (restart) | `Auto` ✅ / `Master System / Mark III` / `Game Gear` / `SG-1000` / `Multivision` | `gearsystem_system` | `Auto` / `Master System / Mark III` / `Game Gear` / `SG-1000` / `Multivision` |
| Region (restart) | `Auto` ✅ / `Master System Japan` / `Master System Export` / `Game Gear Japan` / `Game Gear Export` / `Game Gear International` | `gearsystem_region` | `Auto` / `Master System Japan` / `Master System Export` / `Game Gear Japan` / `Game Gear Export` / `Game Gear International` |
| Mapper (restart) | `Auto` ✅ / `ROM` / `SEGA` / `Codemasters` / `Korean` / `MSX` / `Janggun` / `SG-1000` | `gearsystem_mapper` | `Auto` / `ROM` / `SEGA` / `Codemasters` / `Korean` / `MSX` / `Janggun` / `SG-1000` |
| Refresh Rate (restart) | `Auto` ✅ / `NTSC (60 Hz)` / `PAL (50 Hz)` | `gearsystem_timing` | `Auto` / `NTSC (60 Hz)` / `PAL (50 Hz)` |
| Master System BIOS (restart) | `Desactivado` ✅ / `Activado` | `gearsystem_bios_sms` | `Disabled` / `Enabled` |
| Game Gear BIOS (restart) | `Desactivado` ✅ / `Activado` | `gearsystem_bios_gg` | `Disabled` / `Enabled` |
| 3D Glasses | `Both Eyes / OFF` ✅ / `Left Eye` / `Right Eye` | `gearsystem_glasses` | `Both Eyes / OFF` / `Left Eye` / `Right Eye` |
| Allow Up+Down / Left+Right | `Desactivado` ✅ / `Activado` | `gearsystem_up_down_allowed` | `Disabled` / `Enabled` |

## ![](/emulators/external-links.png) Enlaces externos

* **Código fuente utilizado** : [https://github.com/drhelius/Gearsystem/](https://github.com/drhelius/Gearsystem/)
* **Documentación Libretro** : [https://docs.libretro.com/library/gearsystem/](https://docs.libretro.com/library/gearsystem/)
