---
title: Libretro DeSmuME
description: 
published: true
date: 2024-07-25T00:03:08.656Z
tags: libretro, ds, desmume
editor: markdown
dateCreated: 2021-07-27T10:47:22.706Z
---

**Libretro DeSmuME** is an emulator for the Nintendo DS game system developed in C and C++.

It is written by:

* YopYop156
* Zeromus

## ![](/emulators/license.svg) License

This core is under [**GPLv2**](https://github.com/libretro/desmume/blob/master/license.txt) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ❌ | ❌ | ❌ | ❌ | ❌ | ❌ | ✅ | ❌ |

## ![](/emulators/features.png) Features

| Feature | Supported |
| :---: | :---: |
| Restart | ✔ |
| Screenshots | ✔ |
| Saves | ✔ |
| States | ✔ |
| Rewind | ✔ |
| Netplay | ✔ (pas de Download Play, câble link ou émulation du Wi-Fi) |
| Core Options | ✔ |
| RetroAchievements | ✔ |
| RetroArch Cheats | ✔ |
| Controls | ✔ |
| Remapping | ✔ |
| Username | ✔ |

## ![](/emulators/bios.svg) BIOS

### List of mandatory bios

| Filename | Description | MD5 | Provided |
| :---: | :---: | :---: | :---: |
| bios7.bin | ARM7 BIOS | df692a80a5b1bc90728bc3dfc76cd948 | ❌ |
| bios9.bin | ARM9 BIOS | a392174eb3e572fed6447e956bde4b25 | ❌ |
| firmware.bin | NDS Firmware | 145eaef5bd3037cbc247c213bb3da1b3 e45033d9b0fa6b0de071292bba7c9d13 3ad72b2c9a736b24953f2d391da4bfcc | ❌ |

### Location

Put the bios like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 nds
┃ ┃ ┃ ┃ ┣ 🗒 **bios7.bin**
┃ ┃ ┃ ┃ ┣ 🗒 **bios9.bin**
┃ ┃ ┃ ┃ ┣ 🗒 **firmware.bin**

## ![](/emulators/roms.png) Roms

### Supported extensions

La rom doit avoir l'extension :

* .nds
* .bin
* .zip
* .7z

This system supports compressed roms in .zip/.7z format. However, be careful, it is only an archive.

The files contained in the .zip/.7z must correspond to the extensions mentioned above.
Moreover, each .zip/.7z file must contain only one compressed rom.

### Location

Put the roms like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 nds
┃ ┃ ┃ ┃ ┣ 🗒 **game.zip**

>The roms using **No-Intro** format are highly recommended.
{.is-success}

>For more information about roms, go to [this tutorial](./../../../tutorials/games/generalities/isos-and-roms)!
{.is-info}

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Accessing to options

You can configure various options in two different ways.

* Via RetroArch menu:

┣ 📁 RetroArch Menu
┃ ┣ 📁 Core options
┃ ┃ ┣ 🧩 Name_option

* Via the file `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core options

| Options | Options values | Variables (for configuration override) | Variables values |
| --- | --- | --- | --- |
| Firmware Language | `Auto` ✅ / `English` / `Japanese` / `French` / `German` / `Italian` / `Spanish` | `desmume_firmware_language` | `Auto` / `English` / `Japanese` / `French` / `German` / `Italian` / `Spanish` |
| Use External BIOS/Firmware (restart) | `Disabled` ✅ / `Enabled` | `desmume_use_external_bios` | `disabled` / `enabled` |
| Boot Into BIOS (interpreter and external bios only) | `Disabled` ✅ / `Enabled` | `desmume_boot_into_bios` | `disabled` / `enabled` |
| Load Game Into Memory (restart) | `Disabled` ✅ / `Enabled` | `desmume_load_to_memory` | `disabled` / `enabled` |
| CPU Cores | `1` ✅ / `2` / `3` / `4` | `desmume_num_cores` | `1` / `2` / `3` / `4` |
| CPU Mode (restart) |  `interpreter` ✅ / `jit` | `desmume_cpu_mode` | `interpreter` / `jit` |
| JIT Block Size | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` ✅ / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `21` / `22` / `23` / `24` / `25` / `26` / `27` / `28` / `29` / `30` / `31` / `32` / `33` / `34` / `35` / `36` / `37` / `38` / `39` / `40` / `41` / `42` / `43` / `44` / `45` / `46` / `47` / `48` / `49` / `50` / `51` / `52` / `53` / `54` / `55` / `56` / `57` / `58` / `59` / `60` / `61` / `62` / `63` / `64` / `65` / `66` / `67` / `68` / `69` / `70` / `71` / `72` / `73` / `74` / `75` / `76` / `77` / `78` / `79` / `80` / `81` / `82` / `83` / `84` / `85` / `86` / `87` / `88` / `89` / `90` / `91` / `92` / `93` / `94` / `95` / `96` / `97` / `98` / `99` / `100` | `desmume_jit_block_size` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `21` / `22` / `23` / `24` / `25` / `26` / `27` / `28` / `29` / `30` / `31` / `32` / `33` / `34` / `35` / `36` / `37` / `38` / `39` / `40` / `41` / `42` / `43` / `44` / `45` / `46` / `47` / `48` / `49` / `50` / `51` / `52` / `53` / `54` / `55` / `56` / `57` / `58` / `59` / `60` / `61` / `62` / `63` / `64` / `65` / `66` / `67` / `68` / `69` / `70` / `71` / `72` / `73` / `74` / `75` / `76` / `77` / `78` / `79` / `80` / `81` / `82` / `83` / `84` / `85` / `86` / `87` / `88` / `89` / `90` / `91` / `92` / `93` / `94` / `95` / `96` / `97` / `98` / `99` / `100` |
| Enable Advanced Bus-Level Timing | `Enabled` ✅ / `Disabled` | `desmume_advanced_timing` | `enabled` / `disabled` |
| Frameskip | `0` ✅ / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` | `desmume_frameskip` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` |
| Internal Resolution | `256x192` ✅ / `512x384` / `768x576` / `1024x768` / `1280x960` / `1536x1152` / `1792x1344` / `2048x1536` / `2304x1728` / `2560x1920` | `desmume_internal_resolution` | `256x192` / `512x384` / `768x576` / `1024x768` / `1280x960` / `1536x1152` / `1792x1344` / `2048x1536` / `2304x1728` / `2560x1920` |
| OpenGL Rasterizer (restart) | `Disabled` ✅ / `Enabled` | `desmume_opengl_mode` | `disabled` / `enabled` |
| OpenGL: Color Depth (restart) | `16-bit` ✅ / `32-bit` | `desmume_color_depth` | `16-bit` / `32-bit` |
| OpenGL: Multisampling AA | `Disabled` ✅ / `2` / `4` / `8` / `16` / `32` | `desmume_gfx_multisampling` | `disabled` / `2` / `4` / `8` / `16` / `32` |
| OpenGL: Texture Smoothing | `Disabled` ✅ / `Enabled` | `desmume_gfx_texture_smoothing` | `disabled` / `enabled` |
| OpenGL: Shadow Polygons | `Enabled` ✅ / `Disabled` | `desmume_opengl_shadow_polygon` | `enabled` / `disabled` |
| OpenGL: Special 0 Alpha | `Enabled` ✅ / `Disabled` | `desmume_opengl_special_zero_alpha` | `enabled` / `disabled` |
| OpenGL: NDS Depth Calculation | `Enabled` ✅ / `Disabled` | `desmume_opengl_nds_depth_calculation` | `enabled` / `disabled` |
| OpenGL: Depth-LEqual Polygon Facing | `Disabled` ✅ / `Enabled` | `desmume_opengl_depth_lequal_polygon_facing` | `disabled` / `enabled` |
| Soft3D: High-res Color Interpolation | `Disabled` ✅ / `Enabled` | `desmume_gfx_highres_interpolate_color` | `disabled` / `enabled` |
| Soft3D: Line Hack | `Enabled` ✅ / `Disabled` | `desmume_gfx_linehack` | `enabled` / `disabled` |
| Soft3D: Texture Hack | `Disabled` ✅ / `Enabled` | `desmume_gfx_txthack` | `disabled` / `enabled` |
| Edge Marking | `Enabled` ✅ / `Disabled` | `desmume_gfx_edgemark` | `enabled` / `disabled` |
| Texture Scaling (xBrz) | `1` ✅ / `2` / `4` | `desmume_gfx_texture_scaling` | `1` / `2` / `4` |
| Texture Deposterization | `Disabled` ✅ / `Enabled` | `desmume_gfx_texture_deposterize` | `disabled` / `enabled` |
| Screen Layout | `top/bottom` ✅ / `bottom/top` / `left/right` / `right/left` / `top only` / `bottom only` / `hybrid/top` / `hybrid/bottom` | `desmume_screens_layout` | `top/bottom` / `bottom/top` / `left/right` / `right/left` / `top only` / `bottom only` / `hybrid/top` / `hybrid/bottom` |
| Screen Gap | `0` ✅ / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `21` / `22` / `23` / `24` / `25` / `26` / `27` / `28` / `29` / `30` / `31` / `32` / `33` / `34` / `35` / `36` / `37` / `38` / `39` / `40` / `41` / `42` / `43` / `44` / `45` / `46` / `47` / `48` / `49` / `50` / `51` / `52` / `53` / `54` / `55` / `56` / `57` / `58` / `59` / `60` / `61` / `62` / `63` / `64` / `65` / `66` / `67` / `68` / `69` / `70` / `71` / `72` / `73` / `74` / `75` / `76` / `77` / `78` / `79` / `80` / `81` / `82` / `83` / `84` / `85` / `86` / `87` / `88` / `89` / `90` / `91` / `92` / `93` / `94` / `95` / `96` / `97` / `98` / `99` / `100` | `desmume_screens_gap` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `21` / `22` / `23` / `24` / `25` / `26` / `27` / `28` / `29` / `30` / `31` / `32` / `33` / `34` / `35` / `36` / `37` / `38` / `39` / `40` / `41` / `42` / `43` / `44` / `45` / `46` / `47` / `48` / `49` / `50` / `51` / `52` / `53` / `54` / `55` / `56` / `57` / `58` / `59` / `60` / `61` / `62` / `63` / `64` / `65` / `66` / `67` / `68` / `69` / `70` / `71` / `72` / `73` / `74` / `75` / `76` / `77` / `78` / `79` / `80` / `81` / `82` / `83` / `84` / `85` / `86` / `87` / `88` / `89` / `90` / `91` / `92` / `93` / `94` / `95` / `96` / `97` / `98` / `99` / `100` |
| Hybrid Layout: Ratio | `3:1` ✅ / `2:1` | `desmume_hybrid_layout_ratio` | `3:1` / `2:1` |
| Hybrid Layout: Scale So Small Screen is 1:1px | `Disabled` ✅ / `Enabled` | `desmume_hybrid_layout_scale` | `disabled` / `enabled` |
| Hybrid Layout: Show Both Screens | `Enabled` ✅ / `Disabled` | `desmume_hybrid_showboth_screens` | `enabled` / `disabled` |
| Hybrid Layout: Cursor Always on Small Screen | `Enabled` ✅ / `Disabled` | `desmume_hybrid_cursor_always_smallscreen` | `enabled` / `disabled` |
| Mouse/Pointer | `Enabled` ✅ / `Disabled` | `desmume_pointer_mouse` | `enabled` / `disabled` |
| Pointer Type | `mouse` ✅ / `touch` | `desmume_pointer_type` | `mouse` / `touch` |
| Mouse Speed | `0.01` / `0.02` / `0.03` / `0.04` / `0.05` / `0.125` / `0.25` / `0.5` / `1.0` ✅ / `1.5` / `2.0` | `desmume_mouse_speed` | `0.01` / `0.02` / `0.03` / `0.04` / `0.05` / `0.125` / `0.25` / `0.5` / `1.0` / `1.5` / `2.0` |
| Pointer Rotation | `0` ✅ / `90` / `180` / `270` | `desmume_input_rotation` | `0` / `90` / `180` / `270` |
| Pointer Mode for Left Analog | `none` ✅ / `emulated` / `absolute` / `pressed` | `desmume_pointer_device_l` | `none` / `emulated` / `absolute` / `pressed` |
| Pointer Mode for Right Analog | `none` ✅ / `emulated` / `absolute` / `pressed` | `desmume_pointer_device_r` | `none` / `emulated` / `absolute` / `pressed` |
| Emulated Pointer Deadzone Percent | `0` / `5` / `10` / `15` ✅ / `20` / `25` / `30` / `35` | `desmume_pointer_device_deadzone` | `0` / `5` / `10` / `15` / `20` / `25` / `30` / `35` |
| Emulated Pointer Acceleration Modifier Percent | `0` ✅ / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `21` / `22` / `23` / `24` / `25` / `26` / `27` / `28` / `29` / `30` / `31` / `32` / `33` / `34` / `35` / `36` / `37` / `38` / `39` / `40` / `41` / `42` / `43` / `44` / `45` / `46` / `47` / `48` / `49` / `50` / `51` / `52` / `53` / `54` / `55` / `56` / `57` / `58` / `59` / `60` / `61` / `62` / `63` / `64` / `65` / `66` / `67` / `68` / `69` / `70` / `71` / `72` / `73` / `74` / `75` / `76` / `77` / `78` / `79` / `80` / `81` / `82` / `83` / `84` / `85` / `86` / `87` / `88` / `89` / `90` / `91` / `92` / `93` / `94` / `95` / `96` / `97` / `98` / `99` / `100` | `desmume_pointer_device_acceleration_mode` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `21` / `22` / `23` / `24` / `25` / `26` / `27` / `28` / `29` / `30` / `31` / `32` / `33` / `34` / `35` / `36` / `37` / `38` / `39` / `40` / `41` / `42` / `43` / `44` / `45` / `46` / `47` / `48` / `49` / `50` / `51` / `52` / `53` / `54` / `55` / `56` / `57` / `58` / `59` / `60` / `61` / `62` / `63` / `64` / `65` / `66` / `67` / `68` / `69` / `70` / `71` / `72` / `73` / `74` / `75` / `76` / `77` / `78` / `79` / `80` / `81` / `82` / `83` / `84` / `85` / `86` / `87` / `88` / `89` / `90` / `91` / `92` / `93` / `94` / `95` / `96` / `97` / `98` / `99` / `100` |
| Emulated Stylus Pressure Modifier Percent | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `21` / `22` / `23` / `24` / `25` / `26` / `27` / `28` / `29` / `30` / `31` / `32` / `33` / `34` / `35` / `36` / `37` / `38` / `39` / `40` / `41` / `42` / `43` / `44` / `45` / `46` / `47` / `48` / `49` / `50` ✅ / `51` / `52` / `53` / `54` / `55` / `56` / `57` / `58` / `59` / `60` / `61` / `62` / `63` / `64` / `65` / `66` / `67` / `68` / `69` / `70` / `71` / `72` / `73` / `74` / `75` / `76` / `77` / `78` / `79` / `80` / `81` / `82` / `83` / `84` / `85` / `86` / `87` / `88` / `89` / `90` / `91` / `92` / `93` / `94` / `95` / `96` / `97` / `98` / `99` / `100` | `desmume_pointer_stylus_pressure` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `21` / `22` / `23` / `24` / `25` / `26` / `27` / `28` / `29` / `30` / `31` / `32` / `33` / `34` / `35` / `36` / `37` / `38` / `39` / `40` / `41` / `42` / `43` / `44` / `45` / `46` / `47` / `48` / `49` / `50` / `51` / `52` / `53` / `54` / `55` / `56` / `57` / `58` / `59` / `60` / `61` / `62` / `63` / `64` / `65` / `66` / `67` / `68` / `69` / `70` / `71` / `72` / `73` / `74` / `75` / `76` / `77` / `78` / `79` / `80` / `81` / `82` / `83` / `84` / `85` / `86` / `87` / `88` / `89` / `90` / `91` / `92` / `93` / `94` / `95` / `96` / `97` / `98` / `99` / `100` |
| Pointer Colour | `white` ✅ / `black` / `red` / `blue` / `yellow` | `desmume_pointer_colour` | `white` / `black` / `red` / `blue` / `yellow` |
| Microphone Button Noise Type | `pattern` ✅ / `random` | `desmume_mic_mode` | `pattern` / `random` |

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/libretro/desmume/](https://github.com/libretro/desmume/)
* **Libretro documentation**: [https://docs.libretro.com/library/desmume/](https://docs.libretro.com/library/desmume/)
* **Source official source code**: [https://github.com/TASVideos/desmume](https://github.com/TASVideos/desmume/)
* **Official documentation**: [https://desmume.org/documentation/](https://desmume.org/documentation/)
* **Official website**: [https://desmume.org/](https://desmume.org/)
* **Official forum** : [http://forums.desmume.org/](http://forums.desmume.org/)