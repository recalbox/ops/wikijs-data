---
title: Anbernic
description: 
published: true
date: 2025-01-22T22:13:54.337Z
tags: anbernic, rg353
editor: markdown
dateCreated: 2023-06-18T16:58:27.467Z
---

## Anbernic RG353

![anbernic-rg353m.png](/compatibility/anbernic-rg353m.png)

La console de jeu rétro basée sur le CPU RK3566 dispose d’un double système d’exploitation (Android et Linux) et prend en charge des dizaines de consoles de jeu classiques des années 1970 à 2000. Il est équipé du WiFi pour les jeux multijoueurs et d’une sortie HDMI pour jouer sur grand écran.

> Seul le modèle **M** est compatible. Les autres modèles de RG353 peuvent l'être mais n'ont pas été testés.
{.is-warning}

### Caractéristiques :

* **Processeur :** 
  * RK3566, Quad-Core, Cortex-A55 64 bits, fréquence jusqu'à 1,8 Ghz
* **GPU :**
  * Accélérateur 3D Mali™ G52
* **RAM :**
  * LPDDR4 2 Go
* **Connectique disponible:** 
  * 2 X USBC
  * 1 X mini HDMI
  * 2 X Fentes pour carte MicroSD (jusqu'à 512 Go)
  * 1 X Prise casque
* **Réseau :**
  * Wi-Fi 2.4G/5G 802.11 a/b/g/n/ac
  * Bluetooth v4.2
* **Batterie :**
  * Li-Po, 3200 mAh
* **Alimentation :**
  * Chargeur téléphone de 5W ou 10W
* **Dimensions :**
  * 14,5 x 7,05 x 1,59 cm
* **Poids :**
  * 232 grammes

## Anbernic RG351

![](/compatibility/anbernic-rg351.png)

L'Anbernic RG351 est une console de jeu portable basée sur Linux créée par Anbernic. La console utilise une carte microSD pour le stockage et est une console ROM numérique uniquement. C'est le successeur de la RG350 et elle est devenue une console portable de premier plan pour le rétrogaming.

> Seul le modèle **V** a été testé. Les autres modèles de RG351 peuvent fonctionner mais n'ont pas été testés.
{.is-warning}

* **Processeur :** 
  * RK3326, Quad-Core, Cortex-A55 64 bits, fréquence jusqu'à 1,5 Ghz
* **GPU :**
  * Courrier G31 MP2
* **RAM :**
  * DDR3L 1 Go
* **Connectique disponible:** 
  * 2 X USBC
  * 1 X Fente pour carte MicroSD (jusqu'à 512 Go)
  * 1 X Prise casque
* **Réseau :**
  * Wi-Fi 2.4G/5G 802.11 a/b/g/n/ac
  * Bluetooth v4.2
* **Batterie :**
  * Li-Po, 3200 mAh
* **Alimentation :**
  * Batterie 3500mAh, jusqu'à 8h d'utilisation
