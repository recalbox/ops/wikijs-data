---
title: Family Computer Disk System
description: 
published: true
date: 2021-10-03T18:30:27.097Z
tags: consoles, nintendo, famicom, family computer disk system
editor: markdown
dateCreated: 2021-05-21T07:59:22.156Z
---

![](/emulators/consoles/famicomdisksystem.svg){.align-center}

## Fiche technique

* **Fabricant :** Nintendo
* **Année de sortie :** 1986
* **Quantités vendues :** 4.44 million
* **Meilleur jeu vendu :** Super Mario Bros. 2
* **Processeur :** Ricoh 2A03 8 Bit processor
* **RAM :** 32 Ko
* **Taille des disquettes :** 112 Ko
* **Processeur audio / vidéo :** utilise le matériel de la Famicom
* **Ports :** utilise le matériel de la Famicom

## Présentation

La **Famicom Disk System** ou **FDS** (**Family Computer Disk System**) est un périphérique de la console de jeux vidéo Famicom. Vendu par Nintendo, il se place sous la console et utilise un format propriétaire de disquette permettant de stocker des jeux. Sorti le 21 février 1986 au Japon, la version américaine, prévue pour fin 1986, n'a jamais été publiée.

Fin 1984, Nintendo commence déjà à réfléchir à l'après-Famicom, car elle n'imagine pas que sa console puisse rester viable commercialement encore de longues années. Le principal problème lié à la première console de salon, c'est sa capacité de stockage mémoire et Nintendo décide donc d'y remédier en élaborant une extension matérielle pour sa console permettant de lire des disquettes, lesquelles disposent d'une plus grande capacité de mémoire et en outre offrant la possibilité de sauvegarder les parties.

## Émulateurs

[Libretro FBNeo](libretro-fbneo)
[Libretro FCEUmm](libretro-fceumm)
[Libretro Mesen](libretro-mesen)
[Libretro Nestopia](libretro-nestopia)