---
title: Libretro Potator
description: 
published: true
date: 2021-11-04T21:32:03.301Z
tags: libretro, supervision, potator, 7.2+
editor: markdown
dateCreated: 2021-07-27T16:09:06.218Z
---

**Libretro Potator** is a Watara supervision emulator based on the Normmatt version.

## ![](/emulators/license.svg) License

This core is under [**public domain**](https://github.com/libretro/potator/blob/master/LICENSE) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Features



## ![](/emulators/bios.svg) BIOS

>**No bios is required.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Supported extensions

Roms must have the extension:

* .sv
* .zip
* .7z

This system supports compressed roms in .zip/.7z format. However, be careful, it is only an archive.

The files contained in the .zip/.7z must correspond to the extensions mentioned above.
Moreover, each .zip/.7z file must contain only one compressed rom.

### Location

Put the roms like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 supervision
┃ ┃ ┃ ┃ ┣ 🗒 **game.zip**

>The roms using **No-Intro** format are highly recommended.
{.is-success}

>For more information about roms, go to [this tutorial](./../../../tutorials/games/generalities/isos-and-roms)!
{.is-info}

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Accessing to options

You can configure various options in two different ways.

* Via RetroArch menu:

┣ 📁 RetroArch Menu
┃ ┣ 📁 Core options
┃ ┃ ┣ 🧩 Name_option

* Via the file `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core options

| Options | Options values | Variables (for configuration override) | Variables values |
| --- | --- | --- | --- |
| Internal Palette | `Greyscale` ✅ / `Potator Amber` / `Potator Green` / `Potator Blue` / `Potator BGB` / `Potator Wataroo` / `Game Boy DMG` / `Game Boy Pocket` / `Game Boy Light` / `Blossom Pink` / `Bubbles Blue` / `Buttercup Green` / `Digivice` / `Game.com` / `GameKing` / `Game Master` / `Golden Wild` / `Greenscale` / `Hokage Orange` / `Labo Fawn` / `Legendary Super Saiyan` / `Microvision` / `Million Live Gold` / `Odyssey Gold` / `Shiny Sky Blue` / `Slime Blue` / `TI-83` / `Travel Wood` / `Virtual Boy` | `potator_palette` | `default` / `potator_amber` / `potator_green` / `potator_blue` / `potator_bgb` / `potator_wataroo` / `gb_dmg` / `gb_pocket` / `gb_light` / `blossom_pink` / `bubbles_blue` / `buttercup_green` / `digivice` / `game_com` / `gameking` / `game_master` / `golden_wild` / `greenscale` / `hokage_orange` / `labo_fawn` / `legendary_super_saiyan` / `microvision` / `million_live_gold` / `odyssey_gold` / `shiny_sky_blue` / `slime_blue` / `ti_83` / `travel_wood` / `virtual_boy` |
| LCD Ghosting | `Disabled` ✅ / `1 Frame` / `2 Frames` / `3 Frames` / `4 Frames` / `5 Frames` / `6 Frames` / `7 Frames` / `8 Frames` | `potator_lcd_ghosting` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` |

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/libretro/potator/](https://github.com/libretro/potator/)