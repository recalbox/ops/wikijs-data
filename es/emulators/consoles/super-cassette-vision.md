---
title: Super Cassette Vision
description: Epoch Super Cassette Vision
published: true
date: 2024-07-26T08:06:10.534Z
tags: 7.2+, super cassette vision, epoch, consolas
editor: markdown
dateCreated: 2023-09-03T20:23:05.819Z
---

![](/emulators/consoles/scv.svg){.align-center}

## Datos técnicos

* **Fabricante**: Epoch
* **Año de lanzamiento**: 1984
* **Procesador**: 8-bit NEC PD7801G
* **RAM**: 128KB
* **VRAM**: 4KB
* **Vídeo**: EPOCH TV-1
* **Resolución**: 309×246, 16 colores, 128 sprites en pantalla
* **Chip de sonido**: PD1771C @ 6 MHz

## Presentación

La Super Cassette Vision, sucesora de la Cassette Vision, es una videoconsola doméstica fabricada por Epoch Co. y lanzada en Japón el 17 de julio de 1984 y en Europa, más concretamente en Francia, más tarde en 1984.

La Super Cassette Vision salió a la venta en 1984, con un procesador de 8 bits y unas prestaciones mejoradas más acordes con las de sus competidoras. Fue publicado en Francia por ITMC bajo la marca Yeno. Al menos 16 juegos se trajeron de Japón para su lanzamiento en Europa.  
Una versión de la consola, la Super Lady Cassette Vision, estaba destinada al mercado femenino joven. La consola venía en un estuche rosa, junto con el juego Milky Princess.

Para que conste, la Super Cassette Vision albergó el primer juego de la licencia Dragon Ball. También tenía un "juego" Basic Nyumon que te permitía crear tus propios programas.  
Algunos juegos se podían guardar (había dos pilas en cada cartucho). El juego Pop&Chips tenía un editor de niveles.  
La consola podía mostrar 128 sprites a la vez, cada uno de los cuales podía estar compuesto por hasta 8 imágenes diferentes. Sin embargo, el número de juegos lanzados era bastante limitado (una treintena).

Epoch abandonó el mercado de consolas en 1987.

## Emuladores

[Libretro EmuSCV](libretro-emuscv)
