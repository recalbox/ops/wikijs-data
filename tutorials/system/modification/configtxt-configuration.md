---
title: Modify startup configuration files
description: 
published: true
date: 2024-08-02T09:39:06.742Z
tags: startup, configuration
editor: markdown
dateCreated: 2021-08-06T09:32:43.335Z
---

>This does not apply for PCs.
{.is-info}

## Explanations

This file is somewhat specific. It allows to define parameters of starting of all the system. It is strongly used for certain configurations.

These files are located in the system at this address:

`/boot/config.txt`
`/boot/recalbox-user-config.txt`

>It is _**STRONGLY NOT RECOMMENDED**_ to add data to the `config.txt` file (unless otherwise stated) or you will lose everything the next time you upgrade. If you need to add a configuration, edit the `recalbox-user-config.txt` file so that you will have everything back when you update.
{.is-danger}

## Usage

## {.tabset}

### config.txt

* Connect [via SSH](./../access/root-access-terminal-cli)
* Mount the [boot partition](./../access/remount-partition-with-write-access) as read-write.
* Use the following command to modify the file:

```shell
nano /boot/config.txt
```

### recalbox-user-config.txt

* Log in [via SSH](./../access/root-access-terminal-cli)
* Mount the [boot partition](./../access/remount-partition-with-write-access) as read-write.
* Use the following command to modify the file:

```shell
nano /boot/recalbox-user-config.txt
```