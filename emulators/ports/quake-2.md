---
title: Quake 2
description: 
published: true
date: 2022-07-22T10:38:14.195Z
tags: ports, quake 2, 8.1+
editor: markdown
dateCreated: 2022-02-06T19:06:19.512Z
---

![](/emulators/ports/nofile.svg){.align-center}

## Techincal data

* **Created by:** id Software
* **Year of release:** 1997
* **Genre:** First person shooter
* **Engine:** Quake Engine
* **Latest version:** Quake Champions
* **OS:** Windows 95/98/ME/NT 4.0/2000
* **CPU:** Pentium 90 MHz or higher or Athlon
* **Storage:** 25 MB free space + 45 MB for the Windows swap file
* **RAM:** 16 MB RAM minimum with Windows; 24 MB minimum for others systems
* **Resolution:** 320x200 pixels
* **Sound:** Sound Blaster and AdLib or 100% compatible sound card

## Presentation

Quake II is a first-person shooter, in which the player shoots enemies from the perspective of the main character. The gameplay is very similar to that featured in Quake, in terms of movement and controls, although the player's movement speed has been slowed down, and the player now has the ability to crouch. The game retains four of the eight weapons from Quake (the Shotgun, Super Shotgun, Grenade Launcher, and Rocket Launcher), although they have been redesigned visually and made to function in slightly different ways. The remainder of Quake's eight weapons (the Axe, Nailgun, Super Nailgun, and Thunderbolt) are not present in Quake II. The six newly introduced weapons are the Blaster, Machine Gun, Chain Gun, Hyperblaster, Railgun, and BFG10K. The Quad Damage power up from Quake is present in Quake II, and new power-ups include the Ammo Pack, Invulnerability, Bandolier, Enviro-Suit, Rebreather, and Silencer.

The single player game features a number of changes from Quake. First, the player is given mission-based objectives that correspond to the storyline, including stealing a Tank Commander's head to open a door and calling down an air-strike on a bunker. CGI cutscenes are used to illustrate the player's progress through the main objectives, although they are all essentially the same short piece of video, showing a computerized image of the player character as he moves through game's levels. Another addition is the inclusion of a non-hostile character type: the player character's captured comrades. It is not possible to interact with these characters, however, as they have all been driven insane by their Strogg captors.

The game features much larger levels than Quake, with many more wide open areas. There is also a hub system that allows the player to travel back and forth between levels, which is necessary to complete certain objectives. Some of the textures and symbols that appear in the game are very similar to some of those found in Quake. Enemies demonstrate visible wounds after they have taken damage.

## Emulators

[Libretro VitaQuake2](libretro-vitaquake2)