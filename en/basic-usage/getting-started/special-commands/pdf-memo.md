---
title: Memo PDF
description: Download the memo about special commands in PDF format
published: true
date: 2021-09-04T17:08:47.117Z
tags: special commands, pdf, memo
editor: markdown
dateCreated: 2021-06-28T22:53:00.136Z
---

## Download link

[PDF format](https://mega.nz/file/zcpE2DIb#mb4e8arxm7QVN5qz613TUPQrGi65EEkKfzegvkqq1kk)

## Preview

![](/basic-usage/getting-started/special-commands/pdf1.jpg){.full-width}

![](/basic-usage/getting-started/special-commands/pdf2.jpg){.full-width}

![](/basic-usage/getting-started/special-commands/pdf3.jpg){.full-width}

![](/basic-usage/getting-started/special-commands/pdf4.jpg){.full-width}