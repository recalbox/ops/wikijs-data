---
title: Amiberry
description: 
published: true
date: 2024-07-24T09:17:25.294Z
tags: amiga-cdtv, amiga, amiberry, cdtv, 8.0+
editor: markdown
dateCreated: 2021-09-15T10:11:52.231Z
---

**Amiberry** is a core for ARM optimized for Amiga.

## ![](/emulators/license.svg) License

This core is under [**GPLv3**](https://github.com/midwan/amiberry/blob/master/COPYING) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ❌ | ✅ | ✅ | ✅ | ✅ | ✅ 🐌 | ❌ | ✅ |

🐌 Low performances but playable

## ![](/emulators/features.png) Features



## ![](/emulators/bios.svg) BIOS

### List of mandatory bios

You will find the bios with the name indicated in the column **Description** that you will have to rename with the name indicated in the column **Filename**.

| Filename | Description | MD5 | Provided |
| :---: | :---: | :---: | :---: |
| kick34005.CDTV | CDTV Extended-ROM v1.0 (1991)(Commodore)(CDTV)\[!\] | 89da1838a24460e4b93f4f0c5d92d48d d98112f18792ee3714df16a6eb421b89 d1145ab3a0f89340f94c9e734762c198 | ❌ |

### Location

Put the bios like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 🗒  **kick34005.CDTV**

## ![](/emulators/roms.png) Roms

### Suported extensions

Isos must have the extension:

* .bin/.cue
* .iso
* .img/.ccd

### Location

Put the isos like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 amigacdtv
┃ ┃ ┃ ┃ ┣ 🗒  **jeu.cue**
┃ ┃ ┃ ┃ ┣ 🗒  **jeu.bin**

>The isos using **Redump** format are highly recommended.
{.is-success}

>For more information about isos, go to [this tutorial](./../../../tutorials/games/generalities/isos-and-roms)!
{.is-info}

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/midwan/amiberry/](https://github.com/midwan/amiberry/)
* **Documentation**: [https://blitterstudio.com/amiberry/](https://blitterstudio.com/amiberry/)