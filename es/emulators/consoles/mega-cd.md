---
title: Mega CD
description: Sega Mega CD
published: true
date: 2024-07-26T07:50:32.199Z
tags: sega, mega cd, consolas
editor: markdown
dateCreated: 2023-09-03T19:27:03.186Z
---

![](/emulators/consoles/megacd.svg){.align-center}

## Detalles técnicos

* **Fabricante :** Sega
* **Año de lanzamiento :** 1991
* **Cantidad vendida :** 30 millones
* **Juego más vendido :** Sewer Shark
* **Procesador:** Motorola 68000 a 12,5 MHz procesadores de 16 bits + Megadrive
* **RAM:** 512kB
* **VRAM:** 256kB
* **RAM de sonido:** 16kB
* **Vídeo**:ASIC personalizado
* **Resolución**:320×224 píxeles, 64 colores (de una paleta de 512 colores)
* **Chip de sonido:** Ricoh RF5C164 chips + Megadrive
* **Soporte:** Disco CD-ROM de 500MB

## Presentación

El **Mega-CD** o **Mega CD**, también conocido como **Sega CD** en Norteamérica, es un periférico para la consola de videojuegos Mega Drive de Sega. Se trata de una unidad de CD-ROM que puede leer juegos en formato CD-ROM, CD de audio y CD-G. Se lanzó a finales de 1991 en Japón, en 1992 en Estados Unidos y en 1993 en Europa.

## Emuladores

[Libretro GenesisPlusGX](libretro-genesisplusgx)
[Libretro PicoDrive](libretro-picodrive)