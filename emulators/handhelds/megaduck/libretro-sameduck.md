---
title: Libretro Sameduck
description: 
published: true
date: 2024-07-24T23:59:16.977Z
tags: 8.1+, megaduck
editor: markdown
dateCreated: 2022-07-22T10:26:32.869Z
---



## ![](/emulators/license.svg) License

This core is under [**MIT**](https://github.com/LIJI32/SameBoy/blob/master/LICENSE) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Features

| Feature | Supported |
| :---: | :---: |
| Restart  | ✔ |
| Screenshots | ✔ |
| Saves | ✔ |
| States | ✔ |
| Rewind | ✔ |
| Core Options | ✔ |
| Controls | ✔ |
| Remapping | ✔ |
| Rumble | ✔ |
| Sub-system | ✔ |

## ![](/emulators/bios.svg) BIOS

### List of optional bios

>**No bios is required.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Supported extensions

Rom must have the extension:

* .bin

### Location

Put the roms like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 megaduck
┃ ┃ ┃ ┃ ┣ 🗒 **game.bin**

>The roms using **No-Intro** format are highly recommended.
{.is-success}

>For more information about roms, go to [this tutorial](./../../../tutorials/games/generalities/isos-and-roms)!
{.is-info}

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Accessing to options

You can configure various options in two different ways.

* Via RetroArch menu:

┣ 📁 RetroArch Menu
┃ ┣ 📁 Core options
┃ ┃ ┣ 🧩 Name_option

* Via the file `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core options

Use this table for single player:

| Options | Options values | Variables (for configuration override) | Variables values |
| --- | --- | --- | --- |
| Color correction | `Désactivé` / `correct curves` / `emulate hardware` ✅ / `preserve brightness` / `reduce contrast` | `sameduck_color_correction_mode` | `off` / `correct curves` / `emulate hardware` / `preserve brightness` / `reduce contrast` |
| High-pass filter | `Désactivé` / `accurate` ✅ / `remove dc offset` | `sameduck_high_pass_filter_mode` | `off` / `accurate` / `remove dc offset` |
| Enable rumble | `all games` ✅ / `never` | `sameduck_rumble` | `all games` / `never` |

Use this table for linked players:

| Options | Options values | Variables (for configuration override) | Variables values |
| --- | --- | --- | --- |
| Link cable emulation | `Activé` ✅ / `Désactivé` | `sameduck_link` | `enabled` / `disabled` |
| Screen layout | `top-down` ✅ / `left-right` | `sameduck_screen_layout` | `top-down` / `left-right` |
| Audio output | `Mega Duck #1` ✅ / `Mega Duck #2` | `sameduck_audio_output` | `Mega Duck #1` / `Mega Duck #2` |
| High-pass filter for Mega Duck #1 | `Désactivé` / `accurate` ✅ / `more dc offset` | `sameduck_high_pass_filter_mode_1` | `off` / `accurate` / `more dc offset` |
| High-pass filter for Mega Duck #2 | `Désactivé` / `accurate` ✅ / `more dc offset` | `sameduck_high_pass_filter_mode_2` | `off` / `accurate` / `more dc offset` |
| Enable rumble for Mega Duck #1 | `all games` ✅ / `never` | `sameduck_rumble_1` | `all games` / `never` |
| Enable rumble for Mega Duck #2 | `all games` ✅ / `never` | `sameduck_rumble_2` | `all games` / `never` |

## ![](/emulators/external-links.png) External links

* **Source code used:** [https://github.com/LIJI32/SameBoy/commits/SameDuck](https://github.com/LIJI32/SameBoy/commits/SameDuck)
* **Libretro documentation**: [https://docs.libretro.com/library/sameduck/](https://docs.libretro.com/library/sameduck/)