---
title: Libretro Cap32
description: 
published: true
date: 2024-07-29T13:49:22.892Z
tags: libretro, cap32, gx-4000
editor: markdown
dateCreated: 2023-09-06T12:50:35.769Z
---

## ![](/emulators/license.svg) Licencia

Este núcleo está bajo la licencia [**GPLv2**](https://github.com/ColinPitrat/caprice32/blob/master/COPYING.txt).

## ![](/emulators/compatibility.png) Compatibilidad

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Funcionalidades

| Funcionalidad | Soportado |
| :---: | :---: |
| Reinicio | ✔ |
| Capturas de pantalla | ✔ |
| Guardados | ✔ |
| Guardados instantáneos | ✔ |
| Rebobinado | ✔ |
| Netplay | ✔ |
| Opciones del núcleo | ✔ |
| Controles | ✔ |
| Remapeo | ✔ |
| Control del disco | ✔ |

## ![](/emulators/bios.svg) BIOS

>**No se requiere ningún BIOS.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Extensiones soportadas

Los roms deben tener las siguientes extensiones:

* .cpr
* .m3u
* .zip
* .7z

Este sistema soporta roms comprimidos en formato .zip/.7z. Sin embargo, solo se trata de un archivo.

Los archivos contenidos en los .zip/.7z deben corresponder a las extensiones mencionadas anteriormente.
Además, cada archivo .zip/.7z no debe contener más de una rom comprimida.

### Ubicación

Coloque los roms de la siguiente manera:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 gx4000
┃ ┃ ┃ ┃ ┣ 🗒 **juego.zip**

>Se recomienda encarecidamente el uso de roms en formato **TOSEC**.
{.is-success}

>Para más información sobre los roms, visite [este tutorial](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

>Para poder conservar sus configuraciones personalizadas durante una actualización, le recomendamos utilizar nuestra funcionalidad [Sobrecargas de configuración](./../../../advanced-usage/configuration-overcore).
{.is-info}

### Acceder a las opciones

Puede configurar diversas opciones de dos maneras diferentes.

* A través del menú RetroArch:

┣ 📁 Menú RetroArch
┃ ┣ 📁 Opciones del núcleo
┃ ┃ ┣ 🧩 Name_option

* A través del archivo `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Opciones del núcleo

| Opciones | Valor de las opciones | Variable (para sobrecarga) | Valores de las variables |
| --- | --- | --- | --- |
| Controls > User 1 Controller Config | `auto` ✅ / `qaop` / `incentive` / `joystick_port1` | `cap32_retrojoy0` | `auto` / `qaop` / `incentive` / `joystick_port1` |
| Controls > User 2 Controller Config | `auto` ✅ / `qaop` / `incentive` / `joystick_port1` / `joystick_port2` | `cap32_retrojoy1` | `auto` / `qaop` / `incentive` / `joystick_port1` / `joystick_port2` |
| Controls > Combo Key | `select` / `y` ✅ / `b` / `Desactivado` | `cap32_combokey` | `select` / `y` / `b` / `disabled` |
| Controls > Use internal Remap DB | `Activado` ✅ / `Desactivado` | `cap32_db_mapkeys` | `enabled` / `disabled` |
| Light Gun > Input | `Desactivado` ✅ / `phaser` / `gunstick` | `cap32_lightgun_input` | `disabled` / `phaser` / `gunstick` |
| Light Gun > Show Crosshair | `Desactivado` ✅ / `Activado` | `cap32_lightgun_show` | `disabled` / `enabled` |
| Model | `6128` / `464` / `664` / `6128+ (experimental)` ✅ | `cap32_model` | `6128` / `464` / `664` / `6128+ (experimental)` |
| Advanced > Autorun | `Activado` ✅ / `Desactivado` | `cap32_autorun` | `enabled` / `disabled` |
| Advanced > Ram size | `64` / `128` ✅ / `192` / `512` / `576` | `cap32_ram` | `64` / `128` / `192` / `512` / `576` |
| Video > Green Phosphor blueish | `5` / `10` / `15` ✅ / `20` / `30` | `cap32_advanced_green_phosphor` | `5` / `10` / `15` / `20` / `30` |
| Video > Monitor Intensity | `5` / `6` / `7` / `8` ✅ / `9` / `10` / `11` / `12` / `13` / `14` / `15` | `cap32_scr_intensity` | `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` |
| Video Advanced > Color Depth | `16bit` ✅ / `24bit` | `cap32_gfx_colors` | `16bit` / `24bit` |
| Video Advanced > Crop Screen Borders | `Desactivado` ✅ / `Activado` | `cap32_scr_crop` | `disabled` / `enabled` |
| Status Bar | `onloading` / `Activado` / `Desactivado` | `cap32_statusbar` | `onloading` / `enabled` / `disabled` |
| Keyboard Transparency | `Desactivado` ✅ / `Activado` | `cap32_` | `disabled` / `enabled` |
| Floppy Sound | `Activado` ✅ / `Desactivado` | `cap32_floppy_sound` | `enabled` / `disabled` |
| Monitor Type | `color` ✅ / `green` / `white` | `cap32_scr_tube` | `color` / `green` / `white` |
| CPC Language | `english` ✅ / `french` / `spanish` | `cap32_lang_layout` | `english` / `french` / `spanish` |

## ![](/emulators/external-links.png) Enlaces externos

* **Código fuente utilizado** : [https://github.com/libretro/libretro-cap32/](https://github.com/libretro/libretro-cap32/)
* **Documentación Libretro** : [https://docs.libretro.com/library/caprice32/](https://docs.libretro.com/library/caprice32/)
