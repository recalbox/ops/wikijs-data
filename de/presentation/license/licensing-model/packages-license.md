---
title: Paketlizenzen
description: 
published: true
date: 2021-10-02T10:19:54.536Z
tags: lizenz, pakete
editor: markdown
dateCreated: 2021-10-02T10:19:54.536Z
---

Recalbox wird mit [Buildroot](https://buildroot.org/) kompiliert, das viele Pakete mit verschiedenen Lizenzen kombiniert und ein komplettes Betriebssystem ergibt.

Jedes Paket ist durch seine eigene(n) Lizenz(en) abgedeckt und geschützt und bleibt das Eigentum des/der jeweiligen Autors/Autoren.

Wir [haben daran gearbeitet](https://gitlab.com/recalbox/recalbox/issues/1501), die [rechtlichen Informationen](https://buildroot.org/downloads/manual/manual.html#_complying_with_open_source_licenses) im Recalbox-Quellcode so zu gestalten, dass sie einen umfassenden Überblick über alle von den einzelnen Paketen verwendeten Lizenzen bieten.

In der Zwischenzeit können wir euch nur dazu aufrufen, die Lizenz(en) der einzelnen Pakete manuell zu checken.

>Obwohl einige Paketlizenzen miteinander unvereinbar sein können, wird Recalbox nicht als "abgeleitetes Werk" oder als "modifizierte Version" dieser Lizenzen betrachtet, sondern als eine einfache Zusammenstellung dieser Lizenzen.
>
>Daher hat die Viralität einiger Lizenzen keinen Einfluss auf die Lizenzierung anderer Pakete oder auf die Lizenzierung von Recalbox selbst.
>
>Schaut euch die [General Public License (GPL) FAQ](https://www.gnu.org/licenses/gpl-faq.fr.html) für weitere Einzelheiten an.
{.is-info}