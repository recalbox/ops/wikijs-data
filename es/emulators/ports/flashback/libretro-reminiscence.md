---
title: Libretro REminiscence
description: 
published: true
date: 2024-07-25T22:51:47.067Z
tags: libretro, flashback, reminiscence
editor: markdown
dateCreated: 2023-09-11T14:46:05.302Z
---

¡Stuart Carnie ha portado el emulador de Flashback REminiscence de Gregory Montoir a libretro! 

REminiscence es una recreación del motor del juego de acción y aventura Flashback de 1992/1993. Es el sucesor espiritual de Another World / Out Of This World y cuenta con gráficos rotoscópicos, cinemáticas poligonales y un sistema de juego al estilo Prince of Persia.

Este port sigue en desarrollo, pero ya está en marcha. Actualmente, salta directamente al juego, saltándose el menú principal.

También hemos añadido soporte modplug al núcleo para mejorar la reproducción de música.

El núcleo REminiscence ha sido creado por:

* Grégory Montoir
* Stuart Carnie

## ![](/emulators/license.svg) Licencia 

El core posee la siguiente licencia **GPLv3**.

## ![](/emulators/compatibility.png) Compatibilidad

 | RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | 
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Funcionalidades

| Funcionalidad | ¿Soportada? |
| :---: | :---: |
| Capturas de pantalla | ✔ |
| Grabaciones | ✔ |
| Controles | ✔ |
| Remapping | ✔ |

## ![](/emulators/bios.svg) BIOS

>**No necesita bios.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Cómo instalar los ficheros necesarios

Versiones del juego Flashback reconocidas:

* Flashback DOS Demo (disquette)
* Flashback DOS Full version (disquette)
* Flashback DOS Full version (CD)

### Emplazamiento

Disponed vuestras bios como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 ports
┃ ┃ ┃ ┣ 📁 Flashback
┃ ┃ ┃ ┃ ┣ 📁 data
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **instru_e.pal**

## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

>**Este core no tiene opciones.**
{.is-success}

## 🗂 Contenidos complementarios

### Añadir sonido durante las cinemáticas

El ejecutable también soporta música durante las cinemáticas en la versión **Amiga**. Necesitas recuperar los archivos del directorio `music` en los disquetes originales O recuperar los archivos de la web y añadir `mod.flashback-` delante de cada archivo de música en la versión Amiga.

>No añada extensiones de archivo.
{.es-peligro}

Lista de archivos a colocar en `roms\ports\Flashback\data` :

* mod.flashback-ascenseur
* mod.flashback-ceinturea
* mod.flashback-chute
* mod.flashback-desintegr
* mod.flashback-donneobjt
* mod.flashback-fin
* mod.flashback-fin2
* mod.flashback-game_over
* mod.flashback-holocube
* mod.flashback-introb
* mod.flashback-jungle
* mod.flashback-logo
* mod.flashback-memoire
* mod.flashback-missionca
* mod.flashback-options1
* mod.flashback-options2
* mod.flashback-reunion
* mod.flashback-taxi
* mod.flashback-teleport2
* mod.flashback-teleporta
* mod.flashback-voyage

### Añadir diálogo en el juego

Para el diálogo en el juego, necesitas copiar el archivo `VOICE.VCE` del directorio `data` de la versión SegaCD al directorio `roms\ports\lashback\data`.

>No busques la versión francesa de las voces en SegaCD. Sólo existe la versión (USA) del juego en SegaCD.
{.is-warning}

## ![](/emulators/external-links.png) Enlaces externos

* **Código fuente utilizado** : [https://github.com/libretro/REminiscence/](https://github.com/libretro/REminiscence)
* **Página web oficial** : [http://cyxdown.free.fr/reminiscence/](http://cyxdown.free.fr/reminiscence/)