---
title: Libretro Mu
description: 
published: true
date: 2024-07-25T22:43:30.153Z
tags: libretro, palm, palm-os, mu
editor: markdown
dateCreated: 2023-09-11T14:03:59.279Z
---

**Libretro Mu** es un emulador de Palm Os desarrollado en C y C++.

La continuación del Core está dedicada a Emily (1998-2020).

## ![](/emulators/license.svg) Licencia

El core posee la siguiente licencia [**Creative Commons Attribution-NonCommercial 3.0 United States**](https://github.com/libretro/Mu#license).

## ![](/emulators/compatibility.png) Compatibilidad

 | RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | 
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | 

## ![](/emulators/features.png) Funcionalidades



## ![](/emulators/bios.svg) BIOS

### Lista de BIOS obligatorias

| Nombre del fichero | Descripción | MD5 | ¿Preinstalada? |
| :---: | :---: | :---: | :---: |
| palmos41-en-m515.rom | BIOS Palm OS 4.1 | 83cb1d1c76e568b916dc2e7c0bf669f6 | ❌ |

### Lista de BIOS opcionales

| Nombre del fichero | Descripción | MD5 | ¿Preinstalada? |
| :---: | :---: | :---: | :---: |
| bootloader-dbvz.rom | Palm Bootloader | 9da101cd2317830649a31f8fa46debec | ❌ |

### Emplazamiento

Disponed vuestras bios como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 🗒 **palmos41-en-m515.rom**
┃ ┃ ┃ ┣ 🗒 **bootloader-dbvz.rom**

## ![](/emulators/roms.png) Roms

### Extensiones de roms soportadas

Las roms deben tener las extensiones siguientes:

* .prc
* .pdb
* .pqa
* .img

### Emplazamiento

Disponed vuestras roms como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 palm
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.prc**

>Se aconsejan las roms del proyecto **No-Intro**.
{.is-success}

>Para más información sobre las roms, consultad [este tutorial](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

>Para conservar las configuraciones personalizadas cuando se produce una actualización del sistema Recalbox, os aconsejamos utilizar las [Sobrecargas de configuración](./../../../advanced-usage/configuration-override).
{.is-info}

### Acceder a las opciones

Podéis configurar las opciones del core de dos formas diferentes.

* Via el Menú RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via el fichero `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Opciones del core

| Opción | Valor de la opción | Variable (para la sobrecarga) | Valores posibles |
| --- | --- | --- | --- |
| CPU Speed | `1.0` ✅ / `1.5` / `2.0` / `2.5` / `3.0` / `0.5` | `palm_emu_cpu_speed` | `1.0` / `1.5` / `2.0` / `2.5` / `3.0` / `0.5` |
| Force Match System Clock | `Désactivé` ✅ / `Activé` | `palm_emu_feature_synced_rtc` | `disabled` / `enabled` |
| Ignore Invalid Behavior | `Désactivé` ✅ / `Activé` | `palm_emu_feature_durable` | `disabled` / `enabled` |
| Use Left Joystick As Mouse | `Désactivé` ✅ / `Activé` | `palm_emu_use_joystick_as_mouse` | `disabled` / `enabled` |
| Disable Graffiti Area | `Désactivé` ✅ / `Activé` | `palm_emu_disable_graffiti` | `disabled` / `enabled` |
| OS Version | `Palm m515/Palm OS 4.1` ✅ / `Tungsten T3/Palm OS 5.2.1` / `Tungsten T3/Palm OS 6.0` / `Palm m500/Palm OS 4.0` | `palm_emu_os_version` | `Palm m515/Palm OS 4.1` / `Tungsten T3/Palm OS 5.2.1` / `Tungsten T3/Palm OS 6.0` / `Palm m500/Palm OS 4.0` |

## ![](/emulators/external-links.png) Enlaces externos

* **Código fuente utilizado** : [https://github.com/libretro/Mu/](https://github.com/libretro/Mu/)
* **Código fuente oficial** : [https://github.com/meepingsnesroms/Mu/](https://github.com/meepingsnesroms/Mu/)
* **Página web oficial** : [https://meepingsnesroms.github.io/](https://meepingsnesroms.github.io/)