---
title: Pico-8
description: 
published: true
date: 2024-07-26T08:22:43.263Z
tags: fantasy, pico-8, 7.2+, consolas
editor: markdown
dateCreated: 2023-09-04T07:00:43.058Z
---

![](/emulators/fantasy/pico8.svg){.align-center}

## Datos técnicos

* **Affichage :** 128x128 16 couleurs
* **Taille de cartouche :** 32k
* **Son :** 4 canaux chip blerps
* **Código :** Lua
* **Sprites :** 256 8x8 sprites
* **Mapa :** 128x32 cels

## Presentación

**Pico-8** es una máquina virtual, desarrollada por **Lexaloffle Games**. Su objetivo es ofrecer una consola de fantasía, en la que es posible crear, compartir y jugar a pequeños videojuegos y otros programas informáticos.

Parece una consola normal y corriente, pero funciona con Windows / Mac / Linux.

El desarrollo se realiza a través de un entorno Lua, en el que el usuario puede crear los distintos elementos del juego (mapas, sonidos, sprites, ...).

Pico-8 ha sido utilizado por creadores veteranos, pero también por no profesionales del videojuego.

La pantalla gráfica está limitada a 16 colores, 128x128 píxeles, lo que confiere fácilmente a los juegos creados un aspecto minimalista y retro.

Al encenderla, la máquina te recibe con una línea de comandos, un conjunto de herramientas de creación de cartuchos y un navegador de cartuchos en línea llamado SPLORE.

## Emuladores

[Libretro Fake08](libretro-fake08)
[Libretro Retro8](libretro-retro8)