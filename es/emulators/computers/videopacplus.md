---
title: Videopac+
description: 
published: true
date: 2024-07-26T12:53:00.901Z
tags: philips, videopac+, ordenadores
editor: markdown
dateCreated: 2023-09-03T13:02:02.446Z
---

![](/emulators/computers/videopacplus.svg){.align-center}

## Datos técnicos

* **Fabricante**: Philips
* **Año de lanzamiento**: 1983
* **Procesador**: Intel 8048 a 5,91 MHz
* **RAM**: 6 KB + 192 Bytes
* **Audio**: 1 canal, 8 sonidos
* **ROM**: 1024 bytes
* **Resolución**: 320x238 píxeles con 16 colores

## Presentación

La Philips Videopac+ G7400 es una videoconsola de tercera generación lanzada en cantidades limitadas en 1983, y sólo en Europa; estaba previsto un lanzamiento americano del **Odyssey³ Command Center**, pero nunca llegó. La G7400 fue la sucesora de la Philips Videopac G7000, homóloga europea de la Magnavox Odyssey² estadounidense. El sistema ofrecía gráficos de fondo y primer plano perfectamente adaptados.

El G7400 podía reproducir 3 tipos diferentes de juegos: todos los juegos normales del G7000, juegos especiales del G7000 con gráficos de fondo mejorados que sólo aparecían en el G7400, y juegos compatibles con el G7400 con fondos y sprites de alta resolución.

## Emuladores

[Libretro O2EM](libretro-o2em)