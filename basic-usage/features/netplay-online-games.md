---
title: Netplay (online games)
description: 
published: true
date: 2024-10-05T17:41:17.228Z
tags: games, netplay, online
editor: markdown
dateCreated: 2021-08-04T07:28:55.172Z
---

![](/basic-usage/features/netplay/logo-recalbox_netplay.png)

## Introduction

Netplay is a feature that allows networked multiplayer with some emulators via peer-to-peer (P2P) networking to reduce network latency, to ensure the best possible experience by continuously synchronizing multiple instances running the same emulation kernel and content.
You also have the possibility to have people joining in spectator mode (additional latency).
Online gaming on our old consoles that didn't even know this capability yet!

## Recommandations

### Connection

* Connect your machine in Ethernet Cable RJ45 (a wifi connection is much less stable).
* Have a box that manages UPNP or open its port manually.
* If the Recalbox is behind the router, everything is OK (simple NAT).
* If the Recalbox is behind 2 routers then the Netplay does not work (Double NAT).

### Roms

* **For console cartridges:**
  * Use roms from **No-Intro** romsets (dumps closer to the original games)
* **For CD consoles:**
  * Use roms from **Redump** romsets (dumps closer to the original games)

### System

* If you are using a Raspberry Pi0, Pi1, Pi2, Pi3-b and Pi3-b+, an **overclock** (at 1300 MHZ) is **advisable or mandatory**.
* For Raspberry Pi4, x86_64 (64bit), x86 (32bit) PCs and Odroid, the overclock is not necessary.

## Netplay error message

### As a client: "Failed to Initialize Netplay"

"Failed to Initialize Netplay" often means that you were unable to connect to the host. Confirm that you have the correct IP address for the Host and that the Host has started hosting a NetPlay session. Tell the host to check if its host-based firewall allows RetroArch to accept connections and confirm that port forwarding is working.

### As host: "Port Mapping Failed"

"Port Mapping Failed" probably indicates a UPnP port forwarding problem.  
If you can manually configure your network gateway to forward TCP port 55435 to the local network IP address of your RetroArch device, you can do so. You can also enable the use of a relay server (adds latency).

### "Content not found, try loading content manually"

* Load the content manually, place the content in your recent history list or scan your content to a playlist.

### As a client: "A netplay peer is running different core. Cannot connect."

* Check that you are using the same core as the host
* Deactivate HD and widescreen modes, as these force cores that may be different from what you have chosen.

## FAQ

### 1 - Why can't I host ?

There are 2 possibilities:

* Insufficient internet connection

  * Try being a client.

* Your port does not open automatically

  * Activate the UPNP option of your internet box** or

  or

  * **Open your port manually (Recommended)**

  or as a last resort

  * Try using the NETPLAY MITM** function

### 2 - If I launch hosting, the game crashes and returns to the Recalbox menu

Check that your cores are in adequacy with your machine (PC, Raspberry Pi or other). Moreover, some machines only support the client function.

### 3 - Does Netplay require port opening to work ?

Yes, both the host and the client must open ports properly.

There is a backup option that can be used by those who do not want to open their ports (NETPLAY MITM function).

### 4 - What do you need for netplay to work properly?

* Same version of Recalbox (if RetroArch or Dolphin versions are not the same).
* Same version of Retroarch or Dolphin.
* Same core for the emulator with RetroArch.
* Same rom (named with the same letter and punctuation and identical rom signature).
* Be connected in RJ45 (highly recommended).

### 5 - Does the PSX / N64 / Playstation / PSP / Dreamcast / DS netplay work ?

No, the performance requirements of these consoles make netplay difficult.

### 6 - Can I play GB / GBC / GBA / PSP / DS games with several people via Netplay ?

No, RetroArch netplay is not a cable emulation.  
GB, GBC, GBA and PSP are currently not possible with our implementation.  
But, a notable exception is the same GB / GBC game (Netplay via TGB-Dual and Sameboy cores).  
So Pokémon exchanges and the like are not possible.