---
title: NesPi4Case installation
description: 
published: true
date: 2022-01-04T22:18:58.721Z
tags: installation, nespi4case
editor: markdown
dateCreated: 2021-08-06T16:16:07.198Z
---

## Introduction

On the NesPi4Case, it turns out that the SATA port on the hard drive has several problems depending on the configuration used. For simplicity, if the SATA port (integrated in the upper part of the case) is connected via USB3 cable to the bottom, but no disk (mechanical hard drive or SSD) is connected to it, then Recalbox encounters several problems:

* Installation problem during a fresh install;
* Latency problem at startup (which can be several minutes);
* Case recognition problem;
* Nespi4Case fan shutdown problem when turned off by the on / off button directly.

To solve these problems and make the case work properly while you wait for a fix from the developers, I offer this little tutorial in order to take full advantage of your case! I would like to point out that this is a temporary measure to allow the use of the case while waiting for a stable fix.

We will therefore distinguish two cases:

* Use with a hard disk integrated in the cartridge and connected to the case;
* Use without hard disk.

>Before starting, I would like to emphasize that the cables that must be connected must follow the assembly diagram recommended by RETROFLAG. No reversal of USB2 / USB3 cables.
{.is-warning}

Anyway, I assume that Pi4 is already installed in the case.


## Setting up

### Using the Nespi4Case with a hard disk:

It's the simplest case.

#### Installation
First, format your hard drive in exfat format. Flash your µSD card with your favorite software.

Then, make sure that the case button is set to ON (see the case manual) for using the safeshutdown mode.

Secondly, on the case, insert your µSD card into its slot and connect the hard drive to the SATA port. The hard drive _**MUST**_ be connected.

![](/tutorials/others/nespi4case-installation/nespi4case1.jpg)

* Start it. The installation will start quickly.
* When ES is loaded, the installation is done properly, with automatic recognition of the case, safeshutdown and buttons.
* When pressing the case's power button, the ES will safely shut down, the power will be turned off and the fan will be turned off.

You can then proceed with the installation on your hard drive, in a very classic way:

* Start it again until it's completely loaded.
* With your controller, press `START` button and go to `SYSTEM SETTINGS` > `STORAGE DEVICE` and select the hard drive disk. The installation on a hard disk also recognizes the case, the setting is automatic.

There is absolutely no other manipulation to do, no script to install alongside.

#### Usage

Use your Recalbox in the normal way. The `Power` button shuts down safely, as shown above. `Reset` performs a safe reset.


### Using Nespi4Case without hard disk (for a network connection, for example)

In this case, if the USB cable connected to the SATA is connected, the symptoms detailed at the beginning of the tutorial may occur.

#### Installation

First, flash your µSD card with your favorite software.

Then, make sure that the case button is set to ON (see the case manual) for using the safeshutdown mode. And be sure to disconnect the USB3 cable from the upper part of the case (connected to the SATA port) from the USB3 port of the Pi4. The upper part of the case must not be connected in the lower part!

Secondly, on the case, insert your µSD card into its slot.

![](/tutorials/others/nespi4case-installation/nespi4case2.jpg)

* Start it. The installation will start quickly.
* When ES is loaded, the installation is done properly, but the case is not recognized.

#### Installation verification

There is still a little manipulation to be done.

* Connect your Recalbox to the internet. Connect in SSH. Allow writing to your [boot partition](./../system/access/remount-partition-with-write-access).
* Using your favorite software ([WinSCP](./../system/access/network-access-winscp) / [Cyberduck](./../system/access/network-access-cyberduck) / [MobaXTerm](./../system/access/network-access-mobaxterm)), search for the recalbox-boot.conf file on the boot partition and open it.
* Find the line and delete it:

```ini
case=none:1
```

* Backup and shut down the system safely.
* Connect the USB3 cable from the top of the case to the USB3 port of the Pi4. Turn on your Recalbox.

Now, it's a bit random. I can't explain why, but sometimes the case is already recognized, sometimes not. To verify this, when the ES loads:

* Connect to [SSH](./../system/access/root-access-terminal-cli) and open the file /boot/recalbox-boot.conf.

![](/tutorials/others/nespi4case-installation/nespi4case3.jpeg)

If you find this line:

```ini
case=none:1
```

The case was not recognized. In that case, do again the installation verification.

![](/tutorials/others/nespi4case-installation/nespi4case4.jpeg)

If you find this line:

```ini
case=NESPi4:1
```
The case was detected. In this step, you can turn off and then disconnect the USB3 cable from the Pi4 case (since no disc will be used, this will avoid the problems described above) while keeping the case's features active since it was detected.

![](/tutorials/others/nespi4case-installation/nespi4case5.jpeg)

>If you have a hard drive available and you would like to not use it for this case, it's advised you to proceed the installation with the hard drive, without going through the installation of the hard drive afterwards. And when the case is recognized, remove the USB3 cable from the Pi4 in the same way.
{.is-info}

## Credits

[Original topic](https://forum.recalbox.com/topic/24390/recalbox-v7-2-1-tutoriel-d-installation-du-boitier-nespi4case)
[Translated topic](https://forum.recalbox.com/topic/24395/recalbox-v7-2-1-nespi4case-installation-tutorial)