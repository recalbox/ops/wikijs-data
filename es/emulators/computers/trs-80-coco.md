---
title: TRS-80 Color Computer
description: 
published: true
date: 2024-07-26T12:52:16.014Z
tags: color, trs-80, trs-80-coco, tandy, 8.0+, ordenadores
editor: markdown
dateCreated: 2023-09-03T12:52:09.754Z
---

![](/emuladores/ordenadores/trs80coco.svg){.align-center}

## Datos técnicos

* **Fabricante**: Tandy Corporation
* **Año de lanzamiento**: 1980
* **Sistema operativo**: Microsoft Color Basic v1.0
* **Procesador**: MC6809E a 0,89 MHz
* **RAM**: 4KB - 16KB
* **Disco**: Unidad de disco duro Unidad de casete, unidad de disquete de 5,25
* **Resolución** : Posibilidad de visualizar 32 columnas (32 x 16 con 4 colores / 256 x 192 con 2 colores / 128 x 96 con 8 colores / 128 x 192 con 4 colores)

## Presentación

El RadioShack **TRS-80 Color Computer** (**Tandy Color Computer** o **CoCo**) era un ordenador personal basado en el microprocesador 6809 de Motorola y formaba parte de la gama TRS-80.

El CoCo comenzó su andadura como sistema de referencia de Motorola y estaba pensado como terminal de Videotexto. De hecho, se vendió una versión simplificada del CoCo como terminal de Videotexto con la misma carcasa y teclado. El CoCo original estaba disponible en versiones de 4k, 16k y 32k, aunque los manitas pronto descubrieron una forma de convertir los sistemas de 32k en sistemas de 64k activando el segundo banco de RAM (que estaba desactivado en el momento de la fabricación). La carcasa gris y el teclado "chiclet" del CoCo I se abandonaron en favor del teclado blanco de tamaño normal de los CoCo II y III.

## Emuladores

[XRoar](xroar)

