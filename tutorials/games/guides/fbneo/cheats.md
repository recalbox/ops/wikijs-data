---
title: Cheats
description: 
published: true
date: 2025-02-08T18:58:00.639Z
tags: fbneo, finalburn, neo, cheats
editor: markdown
dateCreated: 2021-08-08T07:29:10.758Z
---

## Introduction

With FinalBurn Neo, it is possible to add cheat codes.

## Usage

* Download the [cheat codes](https://github.com/finalburnneo/FBNeo-cheats/archive/master.zip) archive.
* Unzip the archive.
* Place the unzipped directory `cheats` in the directory `/recalbox/share/bios/fbneo`.

To access the cheat codes, during the game:

* Go to the RetroArch menu by pressing `Hotkey + B`.
* Go to `Core Options`.

![](/tutorials/games/guides/fbneo/fbneocheats1-en.png){.full-width}

* At the bottom of te options screen, go to `Cheat`.

![](/tutorials/games/guides/fbneo/fbneocheats2-en.png){.full-width}

* In the options list, you will find the list of cheat codes available for your game.

![](/tutorials/games/guides/fbneo/fbneocheats3-en.png){.full-width}