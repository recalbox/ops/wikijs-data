---
title: FAQ
description: 
published: true
date: 2024-12-27T16:05:11.321Z
tags: faq
editor: markdown
dateCreated: 2023-09-19T13:17:56.589Z
---

## ¿Qué máquinas puedo utilizar con el Atari ST?

Dependiendo de los subdirectorios, la máquina a utilizar se seleccionará automáticamente según estos criterios:

```text
"/atarist/": ST,
"/atari-st/": ST,
"/st/": ST,
"/atariste/": STE,
"/atari-ste/": STE,
"/ste/": STE,
"/ataritt/": TT,
"/atari-tt/": TT,
"/tt/": TT,
"/atarimegaste/": MEGA_STE,
"/atari-megaste/": MEGA_STE,
"/atari-mega-ste/": MEGA_STE,
"/megaste/": MEGA_STE,
"/mega-ste/": MEGA_STE,
"/falcon/": FALCON,
"/atari-falcon/": FALCON,
```

## Hay varias bios para esta máquina, ¿cómo se gestiona esto?

Dependiendo de la máquina, el sistema buscará automáticamente la buena bios:

- En primer lugar, la bios particular de la máquina, dentro de la subcarpeta del sistema en cuestión:

  - `falcon.img`
  - `megaste.img`
  - `st.img`
  - `ste.img`
  - `tt.img`

- Si no se encuentra la bios de la lista anterior, se buscará la bios `tos.img` dentro de la misma subcarpeta.

- Si aún asi no se encuentra dicho fichero `tos.img`, se buscará el mismo utilizando la ruta `share/bios/tos.img`.

- Finalmente, si no se encuentra ninguna de las bios anteriores, se utilizará la bios por defecto de EmuTOS.

## ¿Beneficia el Atari ST de las sobrecargas de configuración?

Sí, todos los emuladores suministrados soportan el sistemas de sobrecargas de configuración.

Para los core Libretro, utilizad los ficheros `.retroarch.cfg`. Para el core autónomo Hatari, utilizad los ficheros `.hatari.cfg`.

## ¿Se tienen en cuenta los 2 joysticks?

No, de momento sólo se puede utilizar el primer joystick.

## ¿Cómo se configuran los botones?

Aquí está la correspondencia entre los botones:

| Botón del mando | Botón del sistema |
| :--- | :--- |
| `A` | clic derecho |
| `B` | clic izquierdo |
| `X` | abre el interfaz (para cambiar de disquete, etc...) |
| `Y` | Shift |
| `Select` | bascula entre el modo ratón/joystick |
| `R1` | cambia la sensibilidad del ratón |
| `L1` | abre el teclado virtual |
| `L2` | abre un menú que muestra el modo de control en el que te encuentras (ratón/joystick), y el ajuste de sensibilidad del ratón (un número que varía junto a MS: - un valor que puede cambiarse con R1). |

## Existen algunas herramientas para las roms ?

Si, y además os las proprocionamos por defecto. Estas herramientas se encuentran dentro del directorio `/usr/share/hatari`:

* **atari-hd-<span>image.</span>sh** : este script os ayuda a crear un disco duro con particiones, etiquetado de volúmenes, etc. Para utilizarlo: `bash atari-hd-image.sh`.
* **hmsa** : aplicación que permite convertir los formatos de disco MSA <=> ST. Para utilizarlo: `./hmsa`.
* **zip2<span>st.</span>sh** : es un convertidor del contenido zip => image disquete .st (útil para los juegos falcon que se encuentran únicamente en formato zip...). Para utilizarlo: `bash zip2st.sh`.

## ¿El emulador Hatari soporta sobrecargas?

Sí, al igual que el emulador Hatari de Libretro, el Hatari standalone soporta sus propias sobrecargas con el sufijo `.hatari.cfg`. Estas sobrecargas se pueden utilizar a través de `<game>.<ext>.hatari.cfg` para un juego específico o mediante `.hatari.cfg` para un directorio entero, de la misma manera que las sobrecargas `.retroarch.cfg` de RetroArch.

## ¿Puedo usar roms en formato `.ZIP`?

Sí, ¡incluso para el Hatari standalone!