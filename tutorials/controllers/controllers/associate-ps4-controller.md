---
title: Associate a PS4 controller
description: How to associate your PS4 controller to Recalbox
published: true
date: 2024-03-14T17:34:49.741Z
tags: controller, ps4, association, blue
editor: markdown
dateCreated: 2024-03-14T17:34:49.741Z
---

## Introduction

PS4 controllers can be a headache to recognize. This tutorial should help.

## Association procedure

>The following procedure has been tested with a PS2 version 2 controller. It may be different for a version 1.
{.is-info}

Here's how to get your PS4 controller recognized:

- Switch on the controller while your PS4 is unplugged (only the controller must be switched on).
- While the controller is on, underneath you'll see a small button accessible via a paper clip. Press it to reset the controller.

![Reset button location](/tutorials/controllers/controllers/ps4/resetbutton.jpeg){.center}

- Once the controller has been reset and switched off, press the `PS` and `SHARE` buttons simultaneously for 5 seconds.

![PS and SHARE buttons for pairing](/tutorials/controllers/controllers/ps4/ds4pairingguide.png){.center}

Now your controller should be paired and working without even needing to be reconfigured.

Procedure source: https://forum.recalbox.com/topic/8920/a-lire-manettes-ps4
Image sources: [here](https://helpdeskgeek.com/how-to/how-to-reset-a-ps4-controller/) and [here](https://randocity.com/2015/07/31/how-to-pair-your-ps4-controller-wirelessly/)

## Switching off the controller

The controller can, of course, be switched off. To do so, press and hold the PS button for 10 seconds.