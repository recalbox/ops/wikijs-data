---
title: DOSBox
description: Pour émuler des jeux DOS
published: true
date: 2021-08-09T14:43:11.290Z
tags: dos, msdos, dosbox
editor: markdown
dateCreated: 2021-07-29T16:25:56.587Z
---

DOSBox is an emulator capable of emulating an **IBM compatible PC** running the **DOS** operating system.  
Several types of graphic or sound cards can be emulated.

This allows you to play old PC games on Recalbox.

## ![](/emulators/license.svg) License

This core is under [**GPLv2**](https://gitlab.com/lmerckx/recalbox-dosbox/-/blob/master/COPYING) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Features

| Feature | Supported |
| :---: | :---: |
| Screenshots | Oui |
| Saves | - |
| Core Options | Oui |
| Controls | Oui |
| Remapping | Oui |

## ![](/emulators/bios.svg) BIOS

>**No bios is required.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Supported extensions

Roms must have the extension:

* .exe
* .com
* .bat
* .conf

### Location

Put the roms like this: 

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 dos
┃ ┃ ┃ ┃ ┣ 📁 **game**
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **game.exe**

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://gitlab.com/lmerckx/recalbox-dosbox/](https://gitlab.com/lmerckx/recalbox-dosbox/)
* **Official wiki**: [https://www.dosbox.com/wiki/](https://www.dosbox.com/wiki/)
* **Games list**: [https://www.dosbox.com/wiki/GAMES](https://www.dosbox.com/wiki/GAMES)
* **Playable games**: [https://www.dosbox.com/comp_list.php?letter=playable](https://www.dosbox.com/comp_list.php?letter=playable)