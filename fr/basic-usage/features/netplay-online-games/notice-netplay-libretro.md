---
title: Notice Netplay Libretro
description: 
published: true
date: 2023-01-02T14:18:40.164Z
tags: libretro, netplay
editor: markdown
dateCreated: 2021-05-21T08:12:36.548Z
---

## Le lobby Netplay

Le lobby Netplay est un lieu où vous pouvez vous connecter pour jouer en ligne à plusieurs. Pour en profiter, il existe 2 façons :

* Créer votre propre partie en ligne où les autres joueurs vous rejoignent (vous êtes hôte)
* Rejoindre une partie en ligne déjà existante (vous êtes client).

Vous pouvez rejoindre un jeu en ligne soit en tant que joueur, soit en tant que simple spectateur (streaming).

### Configuration

Par défaut, le netplay n'est pas activé. Pour l'activer et le personnaliser, dans la liste des systèmes, faites `Start` > `Options des jeux` > `Options netplay`.

Les options sont les suivantes:

* `NETPLAY (RÉSEAU)` : active le netplay
* `SURNOM` : votre pseudo sur le netplay

>N'utilisez pas de caractères accentués, ceci vous empêchera d'utiliser le netplay.
>{.is-info}

* `PORT` : le port à utiliser pour se connecter. Par défaut 55435.

>Si vous avez une adresse IP avec uniquement certaines plages de ports, vous devez modifier le port ici en fonction de ce que vous pouvez utiliser et faire une redirection dans votre box Internet.
>{.is-info}

Une fois ces informations remplies, vous êtes prêts pour tenter un jeu en ligne.

### Créer un salon de jeux (hôte)

* Depuis la liste des jeux, appyuez sur le bouton `X` en étant que le jeu souhaité.
* Le menu des mots de passe apparaît.

![](/basic-usage/features/netplay/libretro-netplay1.png)

Voici les options proposées :

* `MOT DE PASSE JOUEUR REQUIS` : sélectionner ON si vous voulez mettre un mot de passe joueur sur votre partie.

  * Les personnes voulant rejoindre en tant que joueur devront avoir le même mot de passe joueur que l'hôte.

* `MOT DE PASSE SPECTATEUR REQUIS` : sélectionner ON si vous voulez mettre un mot de passe spectateur sur votre partie.

  * Les personnes voulant rejoindre en tant que spectateur devront avoir le même mot de passe spectateur que l'hôte.

* `MOT DE PASSE JOUEUR` : une sélection de mot de passe prédéfinis.

  * Bien sûr, vous pouvez personnaliser les mot de passe de cette liste

* `MOT DE PASSE SPECTATEUR` : une sélection de mot de passe prédéfinis.

  * Bien sûr, vous pouvez personnaliser les mot de passe de cette liste

Une fois que vous avez fait votre choix :

  * Sélectionnez `DÉMARRER` pour lancer la partie.
  * Attendez les joueurs : une pop-up apparaît sur l'écran des utilisateurs de Recalbox qui sont connectés pour les avertir qu'une session Netplay est disponible.

![](/basic-usage/features/netplay/libretro-netplay2.png)

### Rejoindre un salon de jeux (client)

* Rejoignez le lobby avec la touche `X` Netplay depuis la liste des systèmes. Si Kodi est activé, vous aurez un menu pour choisir le lobby Netplay.

![](/basic-usage/features/netplay/libretro-netplay3.png)

* Sélectionnez l’hôte auquel vous souhaitez vous connecter.

## Informations dans le lobby

Pour chaque jeu, vous aurez des informations :

### Sur la partie de gauche

* **Un signe** : ✅ (validé) ou ❌ (non validé) devant le logo de Recalbox ou le nom de la rom.
* **Le logo Recalbox** : si la personne utilise Recalbox. Entre le signe et le nom de la rom
* **Le nom de la rom** : le nom exacte du fichier rom héberger. Après le signe et le logo Recalbox.

### Sur la partie de droite

#### Résultat de base

* `NOM` : nom de joueur de l'hôte
* `PAYS` : code du pays de votre hôte
* `Hash de la rom` : correspondance de la signature de la rom
* `Fichier rom` : correspondance de la rom
* `CORE` : émulateur utilisé
* `Ver. du Core` : version de l’émulateur
* `LATENCE` : temps de réponse
* `Ver. de RA` : version de RetroArch
* `Arch. de l'hôte` : architecture de l'hôte (vous pouvez avoir le logo Recalbox ici si l'hôte s'en sert)

Vous verrez le résultat devant chaque item de la liste, ✅ (validé) ou ❌ (non validé).

#### Résultat global

Vous verrez le résultat global dans le bas de la fenêtre à droite, vous indiquant les chances de succès de connexion :

* **Vert** : vous avez la bonne rom avec le bon hash et le bon core (émulateur). Toutes les chances sont de votre côté pour que ça marche !

![](/basic-usage/features/netplay/netplay-lobby-ok.png)

* **Bleu** : pas de correspondance du hash (certaines roms n’ont pas de hash, comme les systèmes arcade), mais le bon fichier a été trouvé. Ça devrait marcher !

![](/basic-usage/features/netplay/netplay-ok-nok.png)

* **Rouge** : fichier non trouvé, système interdit, pas le bon core. Aucune chance que le jeu en ligne marche !

![](/basic-usage/features/netplay/netplay-nok.png)