---
title: 2. 🔨 USO AVANZATO
description: 
published: true
date: 2024-11-28T15:06:32.622Z
tags: 
editor: markdown
dateCreated: 2024-11-28T15:06:32.622Z
---

Questa parte della documentazione illustra in modo più dettagliato alcune funzioni utili per un numero ridotto di persone.

Pagine in questa categoria :

[Sovrascrittura della configurazione](configuration-override)
[visualizzazione sistemi](systems-display)
[Controllore ventola](fan-controller)
[Arcade in Recalbox](arcade-in-recalbox)
[Aggiornamento manuale e/o offline (aggiornamento manuale)](manual-update)