---
title: Libretro NXEngine
description: 
published: true
date: 2021-08-08T23:49:08.755Z
tags: libretro, cavestory, nxengine
editor: markdown
dateCreated: 2021-06-29T20:43:42.653Z
---

Libretro NXEngine is an open-source Cave Story rewrite engine for Dingux and MotoMAGX. Author - Caitlin Shaw (rogueeve).

## ![](/emulators/license.svg) License

This core is under [**GPLv3**](https://github.com/gameblabla/nxengine-nspire/blob/master/LICENSE) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Features

| Feature | Supported |
| :---: | :---: |
| Restart | ✔ |
| Screenshots | ✔ |
| Saves | ✔ |
| Controls | ✔ |
| Remapping | ✔ |

## ![](/emulators/bios.svg) BIOS

>**No bios is required.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Supported extensions

Roms must have extension:

* .exe

### Location

Put roms like this: 

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 ports
┃ ┃ ┃ ┃ ┣ 📁 Cave Story
┃ ┃ ┃ ┃ ┃ ┣ 📁 CaveStory
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 **Doukutsu.exe**

This rom is included in your Recalbox.

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Accessing to options

You can configure various options in two different ways.

* Via RetroArch menu:

┣ 📁 RetroArch Menu
┃ ┣ 📁 Core options
┃ ┃ ┣ 🧩 Name_option

* Via the file `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core options

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/libretro/nxengine-libretro/](https://github.com/libretro/nxengine-libretro)
* **Official source code**: [https://github.com/EXL/NXEngine/](https://github.com/EXL/NXEngine)
* **Official website**: [nxengine.sourceforge.net](http://nxengine.sourceforge.net/)
* **Game wiki**: [https://www.cavestory.org/guides-and-faqs/list.php](https://www.cavestory.org/guides-and-faqs/list.php)