---
title: Spiele-Kompatibilitätsliste
description: 
published: true
date: 2022-06-04T13:39:26.997Z
tags: lightgun, 7.2+, spiele, kompatibilität
editor: markdown
dateCreated: 2022-06-04T13:39:26.997Z
---

## Einleitung

Im folgenden findest Du eine Liste der Spiele, die mit Lightgun gespielt werden können.

Diese Liste enthält alle Spiele, die derzeit als Lightgun-kompatibel erkannt werden. Nur Spiele mit ✅ Kompatibilität sind im virtuellen LightGun-System in EmulationStation aufgeführt. Die mit ❌ gekennzeichneten Spiele funktionieren nicht oder vielleicht schlecht und die mit ❔ gekennzeichneten wurden aus verschiedenen Gründen nicht getestet (funktionieren nur teilweise, nicht zu Beginn des Spiels, etc.).

Die Core-Kompatibilitätsliste berücksichtigt nicht, ob es sich um eine portable Version handelt oder nicht, sie gibt nur an, ob der erforderliche Emulator enthalten ist oder nicht.

Diese Liste wird gegebenenfalls für jede neue Version aktualisiert.

## Nintendo Entertainment System
### Emulator
Der erforderliche Emulator ist `Libretro FCEUmm`.

### Kompatibilität
| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

### Spiele
| Name des Spiels | Kompatibilität | Hinweise |
| :--- | :--- | :--- |
| 3 In 1 Supergun | ✅ | - |
| Asder - 20 In 1 | ✅ | - |
| Adventures of Bayou Billy | ❔ | - |
| Baby Boomer | ✅ | - |
| Barker Bill's Trick Shooting | ✅ | - |
| Blood of Jurassic | ❔ | - |
| Chiller | ✅ | - |
| Cobra Mission | ❔ | - |
| Crime Busters | ❔ | - |
| Day Dreamin' Davey | ❔ | - |
| Duck Hunt | ✅ | - |
| Freedom Force | ✅ | - |
| Gotcha | ✅ | - |
| Gumshoe | ✅ | - |
| Hit Marmot | ❔ | - |
| Hogan's Alley | ✅ | - |
| Laser Invasion | ❔ | - |
| Lethal Weapon | ❔ | - |
| Lightgun Games 2 in 1 - Cosmocop + Cyber Monster | ❔ | - |
| Lightgun Games 2 in 1 - Tough Cop + Super Tough Cop | ❔ | - |
| Lone Ranger | ❔ | - |
| Master Shooter | ❔ | - |
| Mechanized Attack | ✅ | - |
| Operation Wolf | ✅ | - |
| Russian Roulette | ❔ | - |
| Super Russian Roulette | ❔ |
| Shootingrange | ✅ | - |
| Space Shadow | ❔ | - |
| Strike Wolf | ❔ | - |
| Totheearth | ✅ | - |
| Track & Field II | ❔ | - |
| Wild Gunman | ✅ | - |

## Super Nintendo
### Emulator
Der erforderliche Emulator ist `Libretro snes9x`.

### Kompatibilität
| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

### Spiele
| Name des Spiels | Kompatibilität | Hinweise |
| :--- | :--- | :--- |
| Battle Clash | ✅ | - |
| Bazooka Blitzkrieg | ✅ | - |
| Lamborghini American Challenge | ✅ | - |
| Lethal Enforcers | ✅ | - |
| Metal Combat - Falcon's Revenge | ✅ | - |
| Nintendo Scope 6 | ✅ | - |
| Operation Thunderbolt | ✅ | - |
| Revolution X | ❌ | - |
| Super Scope 6 | ✅ | - |
| T2 - The Arcade Game | ✅ | - |
| Tin Star | ✅ | - |
| Yoshi's Safari | ✅ | - |
| X Zone | ✅ | - |

## Megadrive
### Emulator
Der erforderliche Emulator ist `Libretro GenesisPlusGX`.

### Kompatibilität
| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

### Spiele
| Name des Spiels | Kompatibilität | Hinweise |
| :--- | :--- | :--- |
| Bodycount | ✅ | - |
| Menacer - 6-game Cartridge | ✅ | - |
| T2 - The Arcade Game | ✅ | - |

## Mega-CD
### Emulator
Der erforderliche Emulator ist `Libretro GenesisPlusGX`.

### Kompatibilität
| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

### Spiele
| Name des Spiels | Kompatibilität | Hinweise |
| :--- | :--- | :--- |
| Corpse Killer | ✅ | - |
| Crime Patrol | ✅ | - |
| Ground Zero Texas | ❌ | - |
| Lethal Enforcers | ✅ | - |
| Lethal Enforcers II: Gun Fighters | ✅ | - |
| Mad Dog McCree | ✅ | - |
| Mad Dog II: The Lost Gold | ✅ | - |
| Snatcher | ❔ | - |
| Who Shot Johnny Rock ? | ✅ | - |

## Master System
### Emulator
Der erforderliche Emulator ist `Libretro GenesisPlusGX`.

### Kompatibilität
| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

### Spiele
| Name des Spiels | Kompatibilität | Hinweise |
| :--- | :--- | :--- |
| Assault City (USA, Europe, Brazil) (Light Phaser) | ✅ | - |
| Bank Panic | ❌ | - |
| Gangster Town | ✅ | - |
| Laser Ghost | ✅ | You need to click on 1 or 2 to start in Light Phaser mode from sega game pad in control 2 |
| Marksman Shooting & Trap Shooting | ✅ | - |
| Marksman Shooting & Trap Shooting & Safari Hunt | ✅ | - |
| Missile Defense 3-D | ✅ | - |
| Operation Wolf | ✅ | - |
| Rambo III | ✅ | - |
| Rescue Mission | ✅ | - |
| Shooting Gallery ~ Shooting G. | ✅ | - |
| Space Gun | ✅ | - |
| Wanted | ❔ | - |

## Dreamcast
### Emulator
Der erforderliche Emulator ist `Libretro Flycast`.

### Kompatibilität
| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ❌ | ❌ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

### Spiele
| Name des Spiels | Kompatibilität | Hinweise |
| :--- | :--- | :--- |
| Confidential Mission | ✅ | - |
| Death Crimson 2 | ✅ | - |
| Death Crimson OX | ✅ | - |
| House of the Dead 2 | ✅ | - |
| Virtua Cop 2 | ✅ | - |

## Playstation 1
### Emulator
Der erforderliche Emulator ist `Libretro Mednafen_PSX`.

### Kompatibilität
| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ❌ | ❌ | ❌ | ✅ | ❌ | ✅ | ✅ | ✅ |

### Spiele
| Name des Spiels | Kompatibilität | Hinweise |
| :--- | :--- | :--- |
| Area 51 | ✅ | - |
| Crypt Killer | ✅ | - |
| Die Hard Trilogy | ✅ | - |
| Die Hard Trilogy 2 | ✅ | - |
| Elemental Gearbolt | ❔ | - |
| Extreme Ghostbusters: The Ultimate Invasion | ✅ | In diesem Spiel musst du - und + auf der Wiimote drücken, um das Spiel zu starten/anzuhalten (entspricht den Gcon45 A- und B-Tasten). |
| Game Paradise 2 | ❔ | - |
| Ghoul Panic | ✅ | - |
| Gunfighter: The Legend of Jesse James | ✅ | - |
| Gunfighter II: Revenge of Jesse James | ✅ | - |
| Guntu: Western Front June, 1944: Tetsu no Kioku | ❔ | - |
| Judge Dredd | ✅ | - |
| Lethal Enforcers | ✅ | - |
| Maximum Force | ✅ | - |
| Mighty Hits Special | ✅ | - |
| Moorhen 3: Chicken Chase | ✅ | - |
| Moorhuhn 2: Die Jagd Geht Weiter | ❔ | - |
| Moorhuhn 3 | ✅ | - |
| Moorhuhn X | ❔ | - |
| Omega Assault | ❔ | - |
| Point Blank | ✅ | - |
| Point Blank 2 | ✅ | - |
| Point Blank 3 | ✅ | - |
| Project: Horned Owl | ❔ | - |
| Puffy no P.S. I Love You | ❔ |
| Rescue Shot | ✅ | - |
| Rescue Shot Bubibo | ✅ | - |
| Rescue Shot Bubibo & Biohazard: Gun Survivor: GunCon Taiou Shooting Taikenban | ✅ | - |
| Resident Evil: Survivor | ✅ | - |
| Revolution X | ❔ | - |
| Serofans | ❔ | - |
| Simple 1500 Series Vol. 24: The Gun Shooting | ❔ | - |
| Simple 1500 Series Vol. 63: The Gun Shooting 2 | ❔ | - |
| Time Crisis | ✅ | - |
| Time Crisis: Project Titan | ✅ | - |

## Saturn
### Emulator
Der erforderliche Emulator ist `Libretro Mednafen_Saturn`.

### Kompatibilität
| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ❌ | ❌ | ❌ | ❌ | ❌ | ❌ | ✅ | ✅ |

### Spiele
| Name des Spiels | Kompatibilität | Hinweise |
| :--- | :--- | :--- |
| Area 51 | ✅ | - |
| Chaos Control | ✅ | - |
| Corpse Killer: Graveyard Edition | ❌ | - |
| Crypt Killer | ✅ | - |
| Death Crimson | ✅ | - |
| Die Hard Trilogy | ✅ | - |
| House of the Dead | ✅ | - |
| Maximum Force | ✅ | - |
| Mechanical Violator Hakaider: Last Judgement | ✅ | - |
| Mighty Hits | ✅ | - |
| Policenauts | ❔ | - |
| Revolution X | ❌ | - |
| Scud: The Disposable Assassin | ✅ | - |
| Virtua Cop | ✅ | - |
| Virtua Cop 2 | ✅ | - |

## 3DO
### Emulator
Der erforderliche Emulator ist `Libretro Opera`.

### Kompatibilität
| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ❌ | ❌ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

### Spiele
| Name des Spiels | Kompatibilität | Hinweise |
| :--- | :--- | :--- |
| Burning Soldier | ❌ | - |
| Corpse Killer | ✅ | - |
| Creature Shock | ❌ | - |
| Crime Patrol | ✅ | - |
| CyberDillo | ❌ | - |
| Demolition Man | ❔ | - |
| Drug Wars | ✅ | - |
| Killingtime | ❌ | - |
| Mad Dog McCree | ✅ | - |
| Mad Dog II: The Lost Gold | ✅ | - |
| Shootout at Old Tucson | ❔ | - |
| Space Pirates | ✅ | - |
| The Last Bounty Hunter | ✅ | - |
| Who Shot Johnny Rock ? | ✅ | - |

## Atomiswave
### Emulator
Der erforderliche Emulator ist `Libretro Flycast`.

### Kompatibilität
| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ❌ | ❌ | ❌ | ✅ | ✅ | ✅ | ✅ | ✅ |

### Spiele
| Name des Spiels | Kompatibilität | Hinweise |
| :--- | :--- | :--- |
| Extreme Hunting | ✅ | - |
| Extreme Hunting 2 | ❌ | - |
| Ranger Mission | ✅ | - |
| Sega Clay Challenge | ✅ | - |
| Sports Shooting USA | ✅ | - |

## Naomi
### Emulator
Der erforderliche Emulator ist `Libretro Flycast`.

### Kompatibilität
| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ❌ | ❌ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

### Spiele
| Name des Spiels | Kompatibilität | Hinweise |
| :--- | :--- | :--- |
| Death Crimson OX | ✅ | - |
| House of the Dead 2 | ✅ | - |
| Gun Survivor 2: Biohazard Code:Veronica | ❌ | - |
| Mazan: Flash of the Blade | ✅ | - |
| Ninja Assault | ✅ | - |

## Naomi GD
### Emulator
Der erforderliche Emulator ist `Libretro Flycast`.

### Kompatibilität
| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ❌ | ❌ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

### Spiele
| Name des Spiels | Kompatibilität | Hinweise |
| :--- | :--- | :--- |
| Confidential Mission | ✅ | - |
| Lupin the 3rd: The Shooting | ✅ | - |
| Maze of the King | ✅ | - |

## Mame
### Emulator
Der erforderliche Emulator ist `Libretro MAME`.

### Kompatibilität
| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ❌ | ❌ | ❌ | ❌ | ❌ | ❌ | ✅ | ✅ |

### Spiele
| Name des Spiels | Kompatibilität | Hinweise |
| :--- | :--- | :--- |
| Alien3: The Gun | ❌ | - |
| Area 51 (R3000) | ✅ | - |
| Area 51 / Maximum Force Duo v2.0 | ✅ | - |
| Bang! | ✅ | - |
| Bank Panic | ❌ | - |
| Beast Busters | ✅ | - |
| Beast Busters 2nd Nightmare | ✅ | - |
| Born to Fight | ✅ | - |
| Bubble Trouble | ✅ | - |
| CarnEvil | ❌ | - |
| Catch-22 | ❌ | - |
| Cheyenne | ✅ | - |
| Chiller | ✅ | - |
| Clay Pigeon | ✅ | - |
| Claybuster | ✅ | - |
| Combat Hawk | ✅ | - |
| Combat School | ✅ | - |
| Crackshot | ✅ | - |
| Critter Crusher | ✅ | - |
| Crossbow | ✅ | - |
| Crypt Killer | ✅ | - |
| Cycle Shooting | ✅ | - |
| Deer Hunting USA | ✅ | - |
| Desert Gun | ✅ | - |
| Dragon Gun | ✅ | - |
| Egg Venture | ✅ | - |
| Freedom Force | ❔ | - |
| Ghost Hunter | ✅ | - |
| Great Guns | ✅ | - |
| Golgo 13 | ✅ | - |
| Golgo 13 Kiseki No Dandou | ✅ | - |
| Golly! Ghost! | ✅ | - |
| Gun Bullet | ❔ | - |
| Gunbuster | ✅ | - |
| Hit'n Miss | ✅ | - |
| Invasion - The Abductors | ✅ | - |
| Jurassic Park | ❌ | - |
| Laser Ghost | ❌ | - |
| Lethal Enforcers | ✅ | - |
| Lethal Enforcers II: Gun Fighters | ✅ | - |
| Lethal Justice | ✅ | - |
| Line of Fire / Bakudan Yarou | ❔ | - |
| Lord of Gun | ❔ | - |
| Mallet Madnes | ✅ | - |
| Maximum Force | ✅ | - |
| Mechanized Attack | ❔ | - |
| Mobile Suit Gundam Final Shooting | ❔ | - |
| Night Stocker | ❔ | - |
| N.Y. Captor | ✅ | - |
| Oh! Bakyuuun | ✅ | - |
| One Shot One Kill | ❌ | - |
| Operation Thunderbolt | ❌ | - |
| Operation Wolf | ✅ | - |
| Point Blank | ✅ | - |
| Point Blank 2 | ✅ | - |
| Point Blank 3 | ✅ | - |
| Police Trainer | ✅ | - |
| Rail Chase | ✅ | - |
| Rapid Fire | ✅ | - |
| Revolution X | ❌ | - |
| Sharpshooter | ✅ | - |
| Shooting Gallery | ❌ | - |
| Space Gun | ❌ | - |
| Steel Gunner | ✅ | - |
| Steel Gunner 2 | ✅ | - |
| Target Hits | ✅ | - |
| Terminator 2 - Judgment Day | ❌ | - |
| Tickee Tickats | ✅ | - |
| Time Crisis | ✅ | - |
| Trophy Hunting - Bear & Moose | ✅ | - |
| Tut's Tomb | ✅ | - |
| Under Fire | ✅ | - |
| Virtua Cop 2 | ✅ | - |
| Vs. Duck Hunt | ✅ | - |
| Vs. Freedom Force | ✅ | - |
| Vs. Hogan's Alley | ✅ | - |
| Who Dunit | ✅ | - |
| Wild Gunman | ❔ | - |
| Wild Pilot | ❌ | - |
| Wing Shooting Championship | ✅ | - |
| Zero Point | ✅ | - |
| Zero Point 2 | ✅ | - |
| Zombie Raid | ✅ | - |

## Mame 2003-Plus
### Emulator
Der erforderliche Emulator ist `Libretro MAME 2003 Plus`.

### Kompatibilität
| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

### Spiele
| Name des Spiels | Kompatibilität | Hinweise |
| :--- | :--- | :--- |
| Alien 3: The Gun | ✅ | - |
| Area 51 (R3000) | ❌ | - |
| Area 51 / Maximum Force Duo v2.0 | ❌ | - |
| Bang! | ✅ | - |
| Beast Busters | ✅ | - |
| Born To Fight | ✅ | - |
| Bubble Trouble - Golly Ghost 2 | ❌ | - |
| Cheyenne | ✅ | - |
| Chiller | ✅ | - |
| Clay Pigeon | ✅ | - |
| Crossbow | ✅ | - |
| Crypt Killer | ✅ | - |
| Deer Hunting USA | ✅ | - |
| Dragon Gun | ✅ | - |
| Egg Venture | ✅ | - |
| Ghost Hunter | ✅ | - |
| Golgo 13 | ✅ | - |
| Golgo 13 Kiseki no Dandou | ✅ | - |
| Golly! Ghost! | ✅ | - |
| Great Guns | ✅ | - |
| Gunbuster | ✅ | - |
| Hit'n Miss | ✅ | - |
| Invasion - The Abductors | ❌ | - |
| Jurassic Park | ✅ | - |
| Lethal Enforcers | ✅ | - |
| Lethal Enforcers II: Gun Fighters | ✅ | - |
| Lethal Justice | ✅ | - |
| Mallet Madnes | ❔ | - |
| Maximum Force v1.05 | ✅ | - |
| N.Y. Captor | ✅ | - |
| Oh! Bakyuuun | ❌ | - |
| One Shot One Kill | ✅ | - |
| Operation Wolf | ✅ | - |
| Operation Wolf 3 | ✅ | - |
| Point Blank | ✅ | - |
| Rail Chase | ✅ | - |
| Rapid Fire v1.1 | ✅ | - |
| Steel Gunner | ✅ | - |
| Target Hits | ✅ | - |
| Tickee Tickats | ✅ | - |
| Time Crisis | ✅ | - |
| Trophy Hunting - Bear & Moose v1.0 | ✅ | - |
| Tut's Tomb | ✅ | - |
| Under Fire | ✅ | - |
| Virtua Cop 2 | ❌ | - |
| Vs. Duck Hunt | ✅ | - |
| Vs. Freedom | ✅ | - |
| Vs. Hogan's Alley | ✅ | - |
| Who Dunit | ✅ | - |
| Wild Pilot | ✅ | - |
| Wing Shooting Championship v2.00 | ✅ | - |
| Zero Point | ✅ | - |
| Zero Point 2 | ✅ | - |
| Zombie Raid | ✅ | - |

## FinalBurn Neo
### Emulator
Der erforderliche Emulator ist `Libretro FBNeo`.

### Kompatibilität
| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

### Spiele
| Name des Spiels | Kompatibilität | Hinweise |
| :--- | :--- | :--- |
| Alien3: The Gun | ❌ | - |
| Area 51 (R-3000) | ❌ | - |
| Area 51 / Maximum Force Duo v2.0 | ❌ | - |
| Bang! | ✅ | - |
| Bank Panic | ❌ | - |
| Beast Busters | ✅ | - |
| Born to Fight | ✅ | - |
| Bubble Trouble | ❔ | - |
| Cheyenne | ✅ | - |
| Chiller | ✅ | - |
| Clay Pigeon | ✅ | - |
| Crossbow | ✅ | - |
| Crypt Killer | ✅ | - |
| Deer Hunting USA | ✅ | - |
| Dragon Gun | ✅ | - |
| Egg Venture | ✅ | - |
| Ghost Hunter | ✅ | - |
| Golgo 13 | ✅ | - |
| Golgo 13 Kiseki no Dandou | ✅ | - |
| Golly! Ghost! | ✅ | - |
| Great Guns | ✅ | - |
| Gunbuster | ✅ | - |
| Hit'n Miss | ✅ | - |
| Invasion - The Abductors | ❌ | - |
| Jurassic Park | ✅ | - |
| Lethal Enforcers II: Gun Fighters | ✅ | - |
| Lethal Justice | ✅ | - |
| Line of Fire / Bakudan Yarou | ✅ | - |
| Lord of Gun | ❌ | - |
| Mechanized Attack | ✅ | - |
| Mobile Suit Gundam Final Shooting | ❌ | - |
| Operation Thunderbolt | ✅ | - |
| Operation Wolf | ✅ | - |
| Operation Wolf 3 | ✅ | - |
| Point Blank | ✅ | - |
| Rail Chase | ✅ | - |
| Space Gun | ✅ | - |
| Steel Gunner | ✅ | - |
| Steel Gunner 2 | ✅ | - |
| Trophy Hunting - Bear & Moose v1.0 | ✅ | - |
| Turkey Hunting USA v1.0 | ✅ | - |
| Wing Shooting Championship v2.00 | ✅ | - |
| Zero Point | ✅ | - |
| Zero Point 2 | ✅ | - |
| Zombie Raid | ✅ | - |