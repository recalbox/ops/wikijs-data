---
title: Libretro ECWolf
description: 
published: true
date: 2021-08-08T23:48:24.220Z
tags: libretro, wolfenstein3d, ecwolf
editor: markdown
dateCreated: 2021-06-29T22:55:58.045Z
---

**Free ECWolf** is a source port game engine for Wolfenstein 3D. This port is courtesy of phcoder.

ECWolf is a port of the Wolfenstein 3D engine based on Wolf4SDL. It combines the original Wolfenstein 3D engine with the ZDoom user experience to create the most user and modifier friendly Wolf3D source port.

Like ZDoom, ECWolf aims to support all games that use the Wolfenstein 3D engine, including Blake Stone (coming in ECWolf 3.0), Corridor 7, Operation Body Count, Rise of the Triad and Super 3D Noah's Ark. ECWolf will also support Macintosh Wolfenstein 3D with all its user-created missions (coming in ECWolf 2.0).

* A single binary runs all supported games. (Wolfenstein 3D, Spear of Destiny, ...)
* Full support for high resolution modes with aspect ratio correction, including widescreen support.
* Modern control schemes (WASD + mouse).
* Mac Wolf / S3DNA / ROTT style automap.
* Unlimited backup locations.
* This is actually based on the Wolf3D engine instead of recreating or forcing in a more modern engine.
* Software rendered using the same 8-bit raytracing.

ECWolf can run the following content:

* Wolfenstein 3D
* Spear of Destiny
* Super 3D Noah's Ark

## ![](/emulators/license.svg) License

This core is under [**GPLv2**](https://github.com/libretro/ecwolf/blob/master/docs/license-gpl.txt) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Features



## ![](/emulators/bios.svg) BIOS

>**No bios is required.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Supported extensions

Roms must have the extension:

* .zip
* .pk3

### Location

Put your roms like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 ports
┃ ┃ ┃ ┃ ┣ 📁 Wolfenstein 3D
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **ecwolf.pk3**

This rom is included in your Recalbox.

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Accessing to options

You can configure various options in two different ways.

* Via RetroArch menu:

┣ 📁 RetroArch Menu
┃ ┣ 📁 Core options
┃ ┃ ┣ 🧩 Name_option

* Via the file `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core options



## Creating mods

* Create mods without working with the source code!
  * Mods work with all supported platforms, including Windows, macOS and Linux.
* Support for arbitrary high resolution texture, flat and sprite.
* Unlimited simultaneous mobile pushwalls.
* Unlimited things.
* Unlimited map size (although technical limitations limit GAMEMAPS to 181x181).
* Uses scripting and data formats similar to ZDoom.
  * Doom editing utilities work with ECWolf (except for levels).

Note that until ECWolf 2.0, although radical changes are not excluded, backward compatibility of mods will not necessarily be maintained. See [the wiki](http://maniacsvault.net/ecwolf/wiki/Version_compatibility) for more information.

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/libretro/ecwolf/](https://github.com/libretro/ecwolf)
* **Official website**: [http://maniacsvault.net/ecwolf/](http://maniacsvault.net/ecwolf/)
* **Official forum**: [https://forum.drdteam.org/viewforum.php?f=174](https://forum.drdteam.org/viewforum.php?f=174)
* **Official documentation**: [http://maniacsvault.net/ecwolf/wiki/Main_Page](http://maniacsvault.net/ecwolf/wiki/Main_Page)