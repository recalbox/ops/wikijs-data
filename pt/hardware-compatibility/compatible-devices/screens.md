---
title: Telas
description: 
published: true
date: 2022-02-27T17:11:23.960Z
tags: compatibilidade, telas
editor: markdown
dateCreated: 2022-02-27T17:11:23.960Z
---

### Tela TFT HDMI

| Dispositivo | Tamanho | Resolução | Status | Comentários |
| :---: | :---: | :---: | :---: | :---: |
| | 5" | 800x480 | ✅ | [banggood](https://www.banggood.com/5-Inch-800-x-480-HDMI-TFT-LCD-Touch-Screen-For-Raspberry-PI-2-Model-B-B-A-B-p-1023438.html) |
| PI-TOP CEED | 14" | 1366×768 | ✅ | Funciona, mas sem som. Precisa instalar o pacote "pt-speaker" [Fonte](https://www.neozone.org/tests/test-pi-top-ceed-lordi-ideal-pour-les-makers-en-herbe/) |

### Tela de barramento TFT SPI

| Dispositivo | Tamanho | Resolução | Status | Comentários |
| :---: | :---: | :---: | :---: | :---: |
| | | | | |

### Tela de barramento TFT DPI

| Dispositivo | Tamanho | Resolução | Status | Comentários |
| :---: | :---: | :---: | :---: | :---: |
| | | | | |

### Painel TFT com controlador de exibição

| Dispositivo | Tamanho | Resolução | Status | Comentários |
| :---: | :---: | :---: | :---: | :---: |
| Painel de laptop Assus N71Jq | 17,3" | 1600x900 | ✅ | Painel LCD Asus N71JQ reciclado conectado no controlador de monitor LCD (*Placa controladora de monitor LCD de áudio HDMI VGA DVI para LP173WD1 N173FGE B173RW01 Tela LCD LED de 15,6" 17,3" 1600x900*) usando um conector LVDS de 30 pinos |