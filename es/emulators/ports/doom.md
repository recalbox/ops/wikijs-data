---
title: Doom
description: 
published: true
date: 2024-07-26T08:29:00.575Z
tags: ports, doom
editor: markdown
dateCreated: 2023-09-04T12:29:51.826Z
---

![](/emulators/ports/doom.svg){.align-center}

## Detalles técnicos

* **Creado por :** id Software
* **Año de lanzamiento:** 1993
* **Género:** Juego de disparos en primera persona
* **Motor:** Doom Engine
* **Última versión:** The Ultimate DOOM (DOOM. WAD v1.9)
* **Sistema operativo:** MS-DOS 5.0 o superior
* **Procesador:** 386/33 mínimo
* **Almacenamiento:** 20 MB de espacio en disco
* **RAM:** 4 MB de RAM como mínimo; 8 MB recomendados
* **Resolución:** 320x200 píxeles
* **Sonido:** Sound Blaster y AdLib o tarjeta de sonido 100% compatible
* **Director Ejecutivo:** Jay Wilbur
* **Ingenieros de Software:** John Carmack, John Romero, Dave Taylor
* **Artistas:** Adrian Carmack, Kevin Cloud
* **Diseñadores:** Sandy Petersen
* **Pruebas y soporte técnico:** Shawn Green, Maurice Hale
* **PEGI** : 15

## Presentación

### Sinopsis

![](/emulators/ports/the_ultimate_doom_library_hero.jpg)

The **Ultimate Doom** te lleva mucho más allá de cualquier territorio conocido. The Ultimate Doom incluye los tres episodios originales del juego más fantástico, perturbador e intenso jamás creado: 27 niveles apocalípticos para desafiar incluso al jugador más curtido. Y eso sólo para empezar... Después, y sólo si eres lo suficientemente persistente, podrás enfrentarte al desafío de un episodio totalmente nuevo... **Thy Flesh Consumed**. Los genios satánicos de **ID Software** han rebuscado en sus diabólicas almas para traerte esta experiencia única: 9 niveles totalmente nuevos de la acción más retorcida, despiadada y tortuosa que existe."         -Detrás de la caja del juego 

### Principales versiones de Doom

Para desglosar todos los nombres que hemos oído, he aquí un rápido resumen:

* **Doom**, lanzado en 1993 por Id Software, consta de 3 episodios de 9 niveles cada uno (_Knee-Deep in the Dead_, _The Shores of Hell_ e _Inferno_).
* **Doom I** es la versión shareware de descarga gratuita, que sólo incluye el primer episodio.
* **Doom II: Hell on Earth**, lanzado en 1994, una secuela de Doom, con 30 niveles consecutivos (+2 ocultos).
* **The Ultimate Doom**, lanzado en 1995, = Doom + un 4º episodio con 9 niveles (_Thy Flesh Consumed_).
* **Master Levels for Doom II**, publicado en 1995, que añade 21 niveles para Doom II.
* **Final Doom**, lanzado en 1996, expansión para Doom II con dos episodios separados:
  * **TNT Evilution** (32 niveles)
  * **The Plutonia Experiment** (32 niveles)
* **Sigil**, lanzado en 2019, es un 5º episodio de Doom, creado por John Romero con motivo del 25º aniversario de Doom... y, por supuesto, ¡un montón de niveles extra para Doom o Doom II!

## Emuladores

[Libretro PrBoom](libretro-prboom)
