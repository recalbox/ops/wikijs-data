---
title: Analog audio output on Pi Zero
description: 
published: true
date: 2021-08-06T15:14:02.211Z
tags: analog, output, pi, zero
editor: markdown
dateCreated: 2021-08-06T14:31:28.051Z
---

## No audio output on the Pi zero? Really?

Yes! To keep the Pi Zero small and inexpensive, the headphone audio filter is not included. No jack to get music from your Recalbox. You can still get audio digitally via HDMI, so if you plug your Pi into a monitor with speakers, it will work just fine.

It's okay if you have your Recalbox hooked up to your TV in your living room, but in an arcade cabinet with a standard PC monitor, or a TV... you can't get audio from HDMI.

Well, that's not really true. If you're using an HDMI to VGA converter, you might be lucky enough to have one that has an audio jack output like [this one](https://www.amazon.fr/dp/B075L812M3/).

But if you're using a Pi zero, you probably want to put it in a small device, with a small TFT display on the SPI/I2C/DPI bus, and you won't have room for that converter.

## How audio is streamed on Raspberry Pi

As described on [this page](https://learn.adafruit.com/introducing-the-raspberry-pi-zero/audio-outputs), other Raspberry Pi's use 2 PWM outputs with a filter circuit to create the sound output. On this page, you will find the actual electronic device built into the Raspberry Pi to create the sound.

But, if you read carefully, it uses PWM on GPIO 40 and 45 which are not highlighted on the Pi Zero.

The solution is to set PWM0 on GPIO 18 (ALT5) and PWM1 on GPIO 13 (ALT0) or GPIO 19 (ALT5) and to create its own filter circuit.

The handling is described in detail on [this page](https://learn.adafruit.com/adding-basic-audio-ouput-to-raspberry-pi-zero/pi-zero-pwm-audio).

With Recalbox, the easiest way is to add this line in the file `recalbox-user-config.txt`. It will reconfigure the pins at startup without any external software or services. The PWMO will be on GPIO 18 (pin 12 on the connector), and the PWM1 on GPIO 13 (pin 33 on the connector).

```text
dtoverlay=pwm-2chan,pin=18,func=2,pin2=13,func2=4
```

As described in [this page](https://hackaday.io/project/9467-piboy-zero/log/35090-pi-zero-pwm-audio-device-tree-overlay), you can make your own overlay with the following source code:

```text
/dts-v1/;
/plugin/;

/ {
  compatible = "brcm,bcm2708";

  fragment@0 {
    target = <&gpio>;
    __overlay__ {
      pinctrl-names = "default";
      pinctrl-0 = <&pwm_audio_pins>;

    pwm_audio_pins: pwm_audio_pins {
	brcm,pins = <13 18>;   /* gpio no ('BCM' number) */
	brcm,function = <4 2>; /* 0:in, 1:out, 2: alt5, 3: alt4, 4: alt0, 5: alt1, 6: alt2, 7: alt3 */
	brcm,pull = <0 0>;     /* 2:up 1:down 0:none */
      };
    };
  };
 };
```

If you have configured buildroot and have already mounted Recalbox, you can compile the dts with :

```shell
output/build/linux-FIRMWARE/scripts/dtc/dtc -@ -I dts -O dtb -o pwm-audio-pi-zero-overlay.dtbo pwm-audio-pi-zero-overlay.dts
```

And you copy the file `pwm-audio-pi-zero-overlay.dtbo` to `/boot/overlays` of your Recalbox. The configuration is now simpler:

```ini
dtoverlay=pwm-audio-pi-zero
```

### Quick test of the sound output

You can test if it works without creating the filter circuit. You can connect a headphone speaker directly between the PWM pin and a ground as shown in the following figure.

It should work, but the sound may have some defects.

### Improving the sound

To create your crossover, you will need some electronic components:

* 2 x 270 Ohm resistors
* 2 x 150 Ohm resistors
* 2 x 10µF capacitors
* 2 x 33nF capacitors (can be replaced by 22nF or 10nF capacitors)

Here are the schematics:

![](/tutorials/audio/filter-schema.png)

and the real filter :

![](/tutorials/audio/filter-made.jpg)

You will need to amplify the output because the signals are filtered and have very low levels.

## Amplification and volume control

You can use a small 5V 2x3W amplifier.  
You can also add a stereo audio potentiometer to adjust the volume.

![](/tutorials/audio/ampli.png)