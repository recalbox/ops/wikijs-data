---
title: Macintosh
description: 
published: true
date: 2023-02-26T17:37:27.942Z
tags: apple, ordinateurs, macintosh, 9.0+
editor: markdown
dateCreated: 2023-02-24T17:34:48.150Z
---

![](/emulators/computers/macintosh.svg){.align-center}

## Fiche technique

* **Fabricant** : Apple Computer, Inc.
* **Année de sortie** : 1984
* **Système d'exploitation** : System Software 1.0
* **Processeur** : Motorola 68000 @ 7.8336 MHz
* **RAM** : 128 KB
* **ROM** : 64 KB
* **Son** : 8 TTL chips implementing a video and sound DMA controller
* **Affichage** : 9" Monochrome built-in (512×342 pixels)

## Présentation

Macintosh (/ma.kin.tɔʃ/, en anglais : /ˈmæk.ɪn.tɑʃ/) ou Mac est une série de différentes familles d'ordinateurs personnels conçus, développés et vendus par Apple. Le premier Macintosh est lancé le 24 janvier 1984 (il a été renommé Macintosh 128K dès le lancement du Macintosh 512K). Il constitue le premier succès commercial pour un ordinateur utilisant une souris et une interface graphique (au lieu d'une interface en ligne de commande).

Le Macintosh remplace l'Apple II comme principal produit d'Apple. Cependant les parts de marché d'Apple baissent, avant un renouveau des Macintosh en 1998, avec la sortie de l'ordinateur grand public tout-en-un iMac G3, qui permet à Apple d'échapper à une probable faillite et marque un succès pour la firme.

## Émulateurs

[Libretro MinivMac](libretro-minivmac)