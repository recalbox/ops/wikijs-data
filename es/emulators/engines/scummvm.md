---
title: ScummVM
description: 
published: true
date: 2024-07-26T08:24:31.308Z
tags: scummvm, ordenadores, motor de juego
editor: markdown
dateCreated: 2023-09-04T07:15:02.673Z
---

![](/emulators/computers/scummvm.svg){.align-center}

## Ficha técnica

* **Autor original** : Ludvig Strigeus
* **Año de publicación**: 2001

## Presentación

**ScummVM** es una máquina virtual que permite jugar a juegos que utilizan el sistema SCUMM (como los juegos de aventuras de LucasArts o Humongous) en plataformas distintas de las soportadas originalmente por el juego; además, el software es compatible con los siguientes juegos que no utilizan SCUMM: Simon the Sorcerer I y II, Beneath a Steel Sky, The Legend of Kyrandia, Flight of the Amazon Queen y Broken Sword 1 y 2. Como uno de los objetivos del programa es hacer accesibles estos clásicos a un amplio abanico de usuarios con distintos sistemas operativos, existen varias versiones de ScummVM compatibles con diferentes sistemas operativos: Microsoft Windows, Atari, WinCE, Pocket PC, Mac OS X, Linux, Android, Palm OS, HP webOS, Open webOS, AmigaOS, BeOS, Solaris, Dreamcast, MorphOS, Irix, PSP, PS2, GP32, Wii, DS y muchos más. ScummVM es software libre, bajo los términos de la licencia GNU GPL.

## Emuladores

[Libretro ScummVM](libretro-scummvm)
[ScummVM](scummvm)