---
title: TRS-80 Color Computer
description: 
published: true
date: 2024-07-26T07:08:16.689Z
tags: color, trs-80, computer, coco, 8.0+, juegos
editor: markdown
dateCreated: 2023-09-14T08:06:14.648Z
---

A continuación encontraréis información que os ayudará a sacar el máximo partido a vuestro TRS-80 Color Computer.

Aqui tenéis la lista de tutoriales disponibles:

[FAQ](faq)