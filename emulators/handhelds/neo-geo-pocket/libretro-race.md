---
title: Libretro RACE
description: 
published: true
date: 2024-07-25T00:00:35.511Z
tags: libretro, neo-geo, pocket, ngp, race
editor: markdown
dateCreated: 2021-07-18T17:55:00.162Z
---

**Libretro RACE** is an emulator for SNK Neo Geo Pocket and Neo Geo Pocket Color.

It was written by :

* Judge_
* Flavor
* Akop Karapetyan
* theelf
* frangarcj
* negativeExponent

It has been modified by **theelf** to work on the PSP.
It is probably derived from the Akop Karapetyan PSP version of RACE ([http://psp.akop.org/race](http://psp.akop.org/race)).

## ![](/emulators/license.svg) License

This core is under [**GPLv2**](https://github.com/libretro/RACE/blob/master/license.txt) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Features

| Feature | Supported |
| :---: | :---: |
| Restart | ✔ |
| Screenshots | ✔ |
| Saves | ✔ |
| States | ✔ |
| Rewind | ✔ |
| Netplay | (not link-cable emulation) |
| Core Options | ✔ |
| RetroAchievements | ✔ |
| Controls | ✔ |
| Remapping | ✔ |

## ![](/emulators/bios.svg) BIOS

>**No bios is required.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Supported extensions

Roms must have the extension:

* .ngp
* .zip
* .7z

This system supports compressed roms in .zip/.7z format. However, be careful, it is only an archive.

The files contained in the .zip/.7z must correspond to the extensions mentioned above.
Moreover, each .zip/.7z file must contain only one compressed rom.

### Location

Put your roms like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 ngp
┃ ┃ ┃ ┃ ┣ 🗒 **game.zip**

>The roms using **No-Intro** format are highly recommended.
{.is-success}

>For more information about roms, go to [this tutorial](./../../../tutorials/games/generalities/isos-and-roms)!
{.is-info}

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Accessing to options

You can configure various options in two different ways.

* Via RetroArch menu:

┣ 📁 RetroArch Menu
┃ ┣ 📁 Core options
┃ ┃ ┣ 🧩 Name_option

* Via the file `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core options

| Options | Options values | Variables (for configuration override) | Variables values |
| --- | --- | --- | --- |
| Language | `english` ✅ / `japanese` | `race_language` | `english` / `japanese` |
| Dark Filter Level (percent) | `0` ✅ / `5` / `10` / `15` / `20` / `25` / `30` / `35` / `40` / `45` / `50` | `race_dark_filter_level` | `0` / `5` / `10` / `15` / `20` / `25` / `30` / `35` / `40` / `45` / `50` |
| Frameskip | `Disabled` ✅ / `Auto` / `Manual` | `race_frameskip` | `disabled` / `auto` / `manual` |
| Frameskip Threshold (%) | `15` / `18` / `21` / `24` / `27` / `30` / `33` ✅ / `36` / `39` / `42` / `45` / `48` / `51` / `54` / `57` / `60` | `race_frameskip_threshold` | `15` / `18` / `21` / `24` / `27` / `30` / `33` / `36` / `39` / `42` / `45` / `48` / `51` / `54` / `57` / `60` |

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/libretro/RACE/](https://github.com/libretro/RACE/)
* **Libretro documentation**:[ https://docs.libretro.com/library/race/](https://docs.libretro.com/library/race/)
* **Official source code**: [https://github.com/TheFlav/RACE-NGPC-Emulator-PSP/](https://github.com/TheFlav/RACE-NGPC-Emulator-PSP/)
* **Official documentation**: [http://psp.akop.org/race/doc.htm](http://psp.akop.org/race/doc.htm)
* **Official website:** [http://psp.akop.org/race.htm](http://psp.akop.org/race.htm)