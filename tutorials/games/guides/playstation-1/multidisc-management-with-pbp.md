---
title: Multi-disc management with .PBP
description: 
published: true
date: 2024-07-26T07:07:06.823Z
tags: management, ps1, multidisc, pbp
editor: markdown
dateCreated: 2021-08-08T14:39:14.602Z
---

Having a single file for your multi-disc games is possible. Just create an eboot file containing your iso, bin, img images via the free software **psx2psp**.

>We remind you that you must have your own PSX disk images !
{.is-info}

## Creation of the eboot file

* Look for the **psx2psp v1.42** version, download it and run it.
* Choose the classic mode then on the left side, load one by one your ISOs.
* Choose the directory where you want to save your files.
* Click on the `Convert` button, then wait.

Place your folder or rename the eboot file created to your recalbox in the `/recalbox/share/roms/psx` folder.

## Changing CDs

To change CDs during the game, you need to activate 2-3 options in the RetroArch menu.

* Open the RetroArch menu while playing with `Hotkey + B`.
* Go to settings/general settings and set the configuration save on exit option to on. 

>From this point on, any changes you make in the options will be saved when you exit the RetroArch menu.
{.is-warning}

* Go to settings/input settings and look for options with the name, disk eject toggle, disk next, disk previous. You must then assign them to keys on your controller that do not yet have a special function assigned.
* Quit RetroArch and go back to the game.

>When you will be on the screen of a game asking you to change CD, you will just have to virtually eject the CD (by doing hotkey + the shortcut assigned to the disk eject toggle option), to change cd, and to virtually close the cd drive.  
>Your game will then start automatically on the next CD.
>
>You can [consult this thread](https://forum.recalbox.com/topic/482/roms-playstation-et-nombre-de-jeux) on the forum in case of problems.
{.is-warning}

## To change CD on PCSX-reARMed (r22)

Method for .bin files

* Enter the RetroArch menu with `Hotkey + B`.
* Choose Quick menu, then core disk option.
* Eject the virtual cd by choosing disk cycle tray status.
* Search for the directory where the cd you want to load is located (logically in `/recalbox/share/roms/psx`) with disk image append.

## Why is it not advisable to convert to .PBP format?

When you convert a game to .PBP format, the format of this file will change the alignment of the tracks on multi-track and/or multi-disk games with the conversion program (usually PSX2PSP). If you ever want to go back, there is a risk, if not more, of ending up with badly truncated or overlapping tracks by reversing the process.

When it will re-trim the tracks to `.BIN`/`.CUE`, it will usually cut 2048 bytes too far. So, you have a piece of track 2 in your track 1.

The problem happens with track 3: you have a piece of track 3 in track 2, but offset by ... 2x 2048 bytes, and so on.

The quality of the dump is still there, but you'll cut it anyhow, and the PBP headers don't allow to recreate the `.CUE` faithfully, nor the track markers (LEAD IN/LEAD OUT).

It is faster to dub your record again than to try to correct automatically or by hand.