---
title: 4. 🔧 COMPATIBILIDAD MATERIAL
description: 
published: true
date: 2023-11-16T16:01:16.526Z
tags: hardware, compatibilidad, material
editor: markdown
dateCreated: 2023-08-31T14:30:08.539Z
---

En las siguientes páginas, podéis verificar si vuestro hardware ha sido probado y es compatible con Recalbox, así como comprobar si un sistema o emulador específico es compatible con vuestro hardware.