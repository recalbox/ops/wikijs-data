---
title: md5sum
description: 
published: true
date: 2023-09-16T20:54:19.082Z
tags: md5sum
editor: markdown
dateCreated: 2023-09-16T20:54:19.082Z
---

Para verificar la firma de un fichero (o su huella digital o **checksum**) MD5.

## Online :

[HTML5 File Hash Online Calculator](https://md5file.com/calculator)

## En local :

## {.tabset}

### Windows

Descargad [winmd5free](http://www.winmd5.com/download/winmd5free.zip). Instaladlo y pulsad el botón `Browse` para seleccionar vuestro fichero. Insertad el checksum recomendado en esta página ([Voir ajouter des bios](./../../../basic-usage/gestion-des-fichiers#iii-ajouter-du-contenu)) escribiendo en la zona `Original file MD5` y pulsad `Verify`.

EXEMPLE : **gba_bios.bin**

#### **Método alternativo 1** :

Desde Powershell v4.0 (Windows 8.1 y superior), el comando `get-filehash` se puede utilizar para comprobar el checksum de un fichero de forma nativa.

1. Lanzad powershell.exe
2. Exécutad el siguiente comando:

```shell
Get-FileHash "fichier à vérifier" -Algorithm MD5
```

>Para verificar todos los ficheros de una carpeta (de forma recursiva), ejecutad el siguiente comando desde dentro de la carpeta:
>
>```shell
>Get-ChildItem -recurse | % {Get-FileHash $_.Fullname -Algorithm MD5} | select Path,Hash | ft -AutoSize
>```
{.is-info}

#### **Método alternativo 2** :

También podéis descargar este script: [HackCheck Shell Extension](http://code.kliu.org/hashcheck/) que añade una pestaña extra a las propiedades de los ficheros en Windows 10.

### macOS / Linux

Utilizad el comando `md5sum`.  
En el terminal, para verificar el checksum de los ficheros de bios, utilizad el comando `shasum` seguido del nombre del fichero.
