---
title: Préparation et Installation
description: Comment installer Recalbox
published: true
date: 2024-11-28T15:19:58.854Z
tags: installation, préparation
editor: markdown
dateCreated: 2021-05-21T07:50:18.592Z
---

## I - Les boards compatibles

Il est possible d’installer Recalbox sur différents types d'appareils:

- **Raspberry Pi 5 / CM5**
- Raspberry Pi 4 / 400 / CM4
- Raspberry Pi 3
- Raspberry Pi 2
- Raspberry Pi 1 / Zero / Zero 2
- Anbernic RG353M (console portable) 
- Odroid Go Advance (console portable)
- Odroid Go Super (console portable)
- Ordinateur 32 ou 64 bits
- Odroid XU4

Pour decouvrir Recalbox, Le **Raspberry Pi 5** est définitivement le choix de l'équipe !

## II - Installer Recalbox

### 1 - Téléchargement / Flasher l'image

Rendez-vous sur [https://www.recalbox.com/fr/download/stable/](https://www.recalbox.com/fr/download/stable/) et suivez les instructions pour télécharger et flasher l'image de Recalbox sur votre media de stockage (carte SD, carte eMMC ou Disque Dur).

>**NE DÉCOMPRESSEZ JAMAIS L'IMAGE TÉLÉCHARGÉE !** Le logiciel qui s'occupe de flasher le fait pour vous !
{.is-danger}

>Seule la **dernière version** de Recalbox est disponible.
>Les **anciennes versions** ne sont plus **téléchargeables** ni **soutenues par l'équipe de développement.**
{.is-warning}

>Vous devez utiliser un média de 16 Go pour le système (nous recommandons la **Sandisk Ultra series** pour les cartes SD).
>
>* Pour l'**installation sur Raspberry Pi** :
>   * Comme média de stockage : une **carte microSD**.
>* Pour l'**installation sur Odroid** :
>    * Comme média de stockage : une **carte microSD ou eMMC**. 
>* Pour l'**installation sur x86 et x86_64** :
>    * Comme média de stockage : une **clé USB 3.0 ou un disque dur**.

### 2 - Installation

#### Raspberry Pi / Odroid Go Advance / Odroid Go Super

* Insérez la carte microSD dans l’appareil avec lequel vous voulez utiliser Recalbox.
* Allumez/branchez votre board et l'installation de Recalbox se lance automatiquement.
* L'installation prend quelques minutes et varie selon votre matériel.

#### Boîtier Retroflag GPI

L'installation sur Gpi Case se fait tout aussi simplement : insérez la carte micro SD, branchez votre GPI Case sur secteur, et Recalbox détectera et configurera automatiquement le boitier. Pour plus d'informations, rendez vous sur la page [Boîtier RetroFlag GPi](./preparation-and-installation/retroflag-gpi-case)

#### Odroid XU4 / Odroid XU4Q

* Insérez votre carte microSD ou eMMC dans l’appareil avec lequel vous voulez utiliser Recalbox.
* Le démarrer et vous devriez voir Recalbox prendre vie.

#### x86 ou x86_64

>**Toujours tester avec une clé USB 3.0 avant une installation sur disque interne**.
{.is-warning}

L'installation peut être effectuée sur une clé USB ou disque dur.

* Branchez votre clé USB ou disque dur externe et démarrez.
* Restez appuyé sur la touche du clavier correspondant à votre PC pour choisir sur quel matériel démarrer et sélectionnez-le.
* Vous devriez voir Recalbox prendre vie.

## III - Le matériel et les accessoires nécessaires

Vérifiez si vous avez le dispositif de stockage requis et l’alimentation pour l’appareil choisi.

| Catégorie | Raspberry Pi 0/0w/1/2/3 | Raspberry Pi 4 | Odroid XU4 | Odroid Go | PC |
| :---: | :---: | :---: | :---: | :---: | :---: |
| Alimentation | µUSB 2.5A (de bonne qualité) | USB-C 3A 5V | Utiliser l'alimentation officielle Odroid | Batterie interne | Alimentation PC standard |
| Vidéo | Câble HDMIc, HDMI2VGA ou HDMI2DVI | Câble µHDMI-HDMI | Câble HDMI | Interne | HDMI/VGA (DVI et DP probablement) |
| Contrôleurs | USB ou Bluetooth | USB ou Bluetooth | USB ou Bluetooth | USB ou Bluetooth | USB ou Bluetooth |
| Stockage système | Carte µSD 16GB+ classe 10 sauf Pi1 : SD | Carte µSD 16GB+ | Carte µSD 16GB+ | Carte µSD 16GB+ | Disque dur 16GB+ |
| Stockage confiiguration, bios et roms | Disque dur externe avec sa propre alimentation | Disque dur externe avec sa propre alimentation | Disque dur externe avec sa propre alimentation | Carte µSD | Disque Dur interne ou disque Dur externe USB (Branché sur la carte mère) |

>Vous avez besoin des éléments suivants pour créer votre Recalbox :
>N'hésitez pas à visiter [notre boutique](https://www.kubii.fr/221-recalbox) chez Kubii !
{.is-info}

## Dépannage x86 / x86_64 :

* **Votre ordinateur ne boot que sur Windows :**  Vérifiez que le **Secure Boot** est désactivé.

  * Renseignez vous sur Internet où se trouve l'option Secure boot (différent selon le constructeur et le modèle de l'ordinateur).
  * Puis assignez Off ou Disable pour le Secure Boot.
  * Éteignez votre ordinateur.

* **Recalbox ne boot pas :** Configurer votre **bios** en **Legacy.**
  * Pour configurer votre bios en Legacy, au démarrage de l'ordinateur, appuyez sur la touche d'accès au bios `F*` (`F1` à `F12`, différent selon le constructeur) ou `Suppr`.
  * Renseignez-vous sur Internet où se trouve l'option Legacy (différent selon le constructeur et le modèle de l'ordinateur).
  * Puis assignez Legacy en l'activant avec "**enabled**"
* **En cas de pc dédié Multiboot**, si vous voulez que votre **Recalbox boot** avant tout autre système :
  * Accédez au **Boot Menu** des disques durs.
  * Appuyez sur `F*` pour accéder au **Boot Menu** des disques durs (`F1` à `F12`, différent selon le constructeur).
  * Sélectionnez votre **disque dur système** Recalbox.