---
title: Special commands
description: 
published: true
date: 2021-08-08T18:54:18.221Z
tags: special commands
editor: markdown
dateCreated: 2021-06-28T22:37:44.005Z
---

To make it easier for you to work with your Recalbox, we have integrated several special commands.

* [In the menu](in-menu) 
* [In the game](in-game)

You even have the possibility to have a [PDF version](pdf-memo) to be used as a memo everywhere!