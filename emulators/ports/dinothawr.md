---
title: DinoThawr
description: 
published: true
date: 2021-09-16T04:44:09.287Z
tags: ports, dinothawr
editor: markdown
dateCreated: 2021-05-21T08:04:28.851Z
---

![](/emulators/ports/dinothawr.png){.align-center}

## Fiche technique

* **Auteur :** Themaister, Agnes Heyer 
* **Design des niveaux :** Agnes Heyer 
* **Programmation :** Hans-Kristian Arntzen 
* **Musique :** Hans-Kristian Arntzen 
* **Certains designs des niveaux :** Hans-Kristian Arntzen 
* **Genre :** Puzzle

## Présentation 

**Dinothawr** est un jeu de puzzle poussant des blocs sur des surfaces glissantes.

Notre héros est un dinosaure dont les amis sont piégés dans la glace. À travers des énigmes, votre tâche est de libérer les dinosaures de leur prison de glace.

## Émulateurs

[Libretro Dinothawr](libretro-dinothawr)