---
title: 8Bitdo sur Recalbox
description: Liste complète des manettes 8bitdo déjà testées
published: true
date: 2025-02-19T23:06:12.629Z
tags: 8bitdo, manettes
editor: markdown
dateCreated: 2021-05-21T08:05:33.370Z
---

![8bitdo](http://support.8bitdo.com/images/Logo-black.svg)

![Liste des manettes 8Bitdo au 13 novembre 2019](/compatibility/8bitdocollection.png){.full-width}

Pour chaque manette, préférez le mode dit D-Input (Android). Si cela ne fonctionne pas, tentez le mode dit X-Input (Windows / Xbox).

Sur chaque manette, le fait de faire un mode de démarrage spécifique ou un bouton sur une certaine position permet de choisir ce mode, veuillez vous réferrer au mode d'emploi de votre manette ainsi qu'à la FAQ en bas de cette page.

## Manettes supportées 8Bitdo

### Manettes de style Xbox

| Image | Manette | USB | Bluetooth | Mode de démarrage | Association BT | Réinitialisation BT | Réinitialisation d'usine |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ![](/compatibility/8bitdo/wired/ultimate-wired-controller-for-xbox.gif) | Ultimate Wired Controller Xbox | ✅ | ❌ | n/a | n/a | n/a | [Voir le manuel sur le site de 8Bitdo](https://download.8bitdo.com/Manual/Controller/Xbox/Ultimate-Wired-Controller-for-Xbox.pdf) |
| ![](/compatibility/8bitdo/wired/pro2_xbox.png) | Pro 2 Wired Controller Xbox | ✅ | ❌ | n/a | n/a | n/a | [Voir le manuel sur le site de 8Bitdo](https://download.8bitdo.com/Manual/Controller/Xbox/Pro2-Wired-Controller-Xbox-Hall-8.pdf) |
| ![](/compatibility/8bitdo/bluetooth/sn30pro_xcloud.png) | SN30 Pro for Android | ✅ | ☑️ | XBOX | Maintenir appuyé le bouton Étoile pendant 3 secondes | Maintenir appuyé le bouton Étoile pendant 3 secondes | [Voir le manuel sur le site de 8Bitdo](https://download.8bitdo.com/Manual/Controller/Xbox/SN30-Pro-for-Android-8.pdf) |

### Manettes de style Ultimate

| Image | Manette | USB | Bluetooth | Mode de démarrage | Association BT | Réinitialisation BT | Réinitialisation d'usine |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ![](/compatibility/8bitdo/bluetooth/ultimate-2c-bluetooth.gif) | Ultimate 2C Bluetooth Controller | ❌ | ❌ | n/a | n/a | n/a | [Voir le manuel sur le site de 8Bitdo](https://download.8bitdo.com/Manual/Controller/Ultimate/Ultimate-2C-Bluetooth-Controller-8.pdf) |
| ![](/compatibility/8bitdo/wired/ultimate-2c-wired.gif) | Ultimate 2C Wired Controller | ✅ | ❌ | Maintenir B | n/a | n/a | [Voir le manuel sur le site de 8Bitdo](https://download.8bitdo.com/Manual/Controller/Ultimate/ultimate-2C-Wired-controller.pdf) |
| ![](/compatibility/8bitdo/24gcontrollers/ultimate-c-2.4g.gif) | Ultimate C 2.4G Wireless Controller | ✅ | ☑️ | HOME | HOME + B | HOME + B | [Voir le manuel sur le site de 8Bitdo](https://download.8bitdo.com/Manual/Controller/Ultimate/Ultimate-C-2.4G-Wireless-Controller-8.pdf) |
| ![](/compatibility/8bitdo/24gcontrollers/ultimate-2.4g.gif) | Ultimate 2.4G Wireless Controller (⚠️ en mode D uniquement) | ✅ | ☑️ | HOME | Maintenir appuyé le bouton Étoile pendant 3 secondes | Changer le commutateur de mode (un nouvel apparaige sera nécessaire à chaque changement) | [Voir le manuel sur le site de 8Bitdo](https://download.8bitdo.com/Manual/Controller/Ultimate/Ultimate-24G-Wireless-Controller-PC-8.pdf) |
| ![](/compatibility/8bitdo/bluetooth/ultimate-c-bluetooth.gif) | Ultimate C Bluetooth Controller | ❌ | ❌ | n/a | n/a | n/a | [Voir le manuel sur le site de 8Bitdo](https://download.8bitdo.com/Manual/Controller/Ultimate/Ultimate-C-Bluetooth-Controller-8.pdf) |
| ![](/compatibility/8bitdo/wired/ultimate-c-wired.gif) | Ultimate C Wired | ✅ | ❌ | Maintenir B | n/a | n/a | [Voir le manuel sur le site de 8Bitdo](https://download.8bitdo.com/Manual/Controller/Ultimate/Ultimate-C-Wired-Controller-8.pdf) |
| ![](/compatibility/8bitdo/bluetooth/ultimate-bluetooth.gif) | Ultimate Bluetooth Controller | ✅ | ☑️ | HOME | Maintenir appuyé le bouton Pair pendant 3 secondes | Maintenir appuyé le bouton Pair pendant 3 secondes | [Voir le manuel sur le site de 8Bitdo](https://download.8bitdo.com/Manual/Controller/Ultimate/Ultimate-Bluetooth-Controller.pdf) |
| ![](/compatibility/8bitdo/wired/ultimate-wired.gif) | Ultimate Wired Controller | ✅ | ❌ | n/a | n/a | n/a | [Voir le manuel sur le site de 8Bitdo](https://download.8bitdo.com/Manual/Controller/Ultimate/Ultimate-Wired-Controller-8.pdf) |

### Manettes de style N64

| Image | Manette | USB | Bluetooth | Mode de démarrage | Association BT | Réinitialisation BT | Réinitialisation d'usine |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ![](/compatibility/8bitdo/bluetooth/64-controller.gif) | 64 Controller | ✅ | ☑️ | START | Maintenir appuyé le bouton Pair pendant 3 secondes | Maintenir appuyé le bouton Pair pendant 3 secondes | [Voir le manuel sur le site de 8Bitdo](https://download.8bitdo.com/Manual/Controller/Ultimate/64-Bluetooth-Controller.pdf) |

### Manettes de style SNES

| Image | Manette | USB | Bluetooth | Mode de démarrage | Association BT | Réinitialisation BT | Réinitialisation d'usine |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ![](/compatibility/8bitdo/bluetooth/pro2.gif) | Pro 2 | ✅ | ☑️ | START | Maintenir appuyé le bouton Pair pendant 3 secondes | Maintenir appuyé le bouton Pair pendant 3 secondes | [Voir le manuel sur le site de 8Bitdo](https://download.8bitdo.com/Manual/Controller/Pro2/Pro2-8.pdf) |
| ![](/compatibility/8bitdo/wired/pro2-wired.gif) | Pro 2 Wired Controller | ✅ | ❌ | n/a | n/a | n/a | [Voir le manuel sur le site de 8Bitdo](https://download.8bitdo.com/Manual/Controller/Pro2-wired/Pro2-wired-8.pdf) |
| ![](/compatibility/8bitdo/bluetooth/sn30pro+.gif) | SN30 Pro+ | ✅ | ☑️ | START + B | Maintenir appuyé le bouton Pair pendant 2 secondes | Maintenir appuyé le bouton PAIR pendant 2 secondes | [Voir le manuel sur le site de 8Bitdo](https://download.8bitdo.com/Manual/Controller/SN30-Pro+/SN30_Pro+_Manual.pdf) |
| ![](/compatibility/8bitdo/bluetooth/sn30prov2.gif) | SN30 Pro 2 | ✅ | ☑️ | START + B | Maintenir appuyé le bouton Pair pendant 3 secondes | Maintenir appuyé le bouton Select pendant 3 secondes | [Voir le manuel sur le site de 8Bitdo](https://download.8bitdo.com/Manual/Controller/SN30pro+SF30pro/SN30pro_Manual.pdf) |
| ![](/compatibility/8bitdo/bluetooth/sf30prov1.gif) | SN30 Pro & SF30 Pro | ✅ | ☑️ | START + B | Maintenir appuyé le bouton Pair pendant 3 secondes | Maintenir appuyé le bouton Select pendant 3 secondes | [Maintenir appuyé le  bouton Select pendant 8 secondes](https://download.8bitdo.com/Manual/Controller/SN30pro+SF30pro/SN30pro+SF30pro_Manual.pdf) |
| ![](/compatibility/8bitdo/wired/sn30pro-usb.gif) | SN30 Pro USB | ❓ | n/a | n/a | n/a| n/a | [Voir le manuel sur le site de 8Bitdo](https://download.8bitdo.com/Manual/Controller/SN30pro-USB/SN30-Pro-USB.pdf) |
| ![](/compatibility/8bitdo/bluetooth/sn30v3color.gif) | SN30 GP 2 | ❓ | ❓ | B + START | Maintenir appuyé le bouton Select pendant 3 secondes | Maintenir appuyé le bouton Select pendant 3 secondes | [Voir le manuel sur le site de 8Bitdo](https://download.8bitdo.com/Manual/Controller/SN30+SF30/SN30_Manual.pdf) |
| ![](/compatibility/8bitdo/bluetooth/sn30v2.gif) | SN30 Retro Set | ❓ | ❓ | START | Maintenir appuyé le bouton Select pendant 3 secondes | Maintenir appuyé le bouton Select pendant 3 secondes | [Voir le manuel sur le site de 8Bitdo](https://download.8bitdo.com/Manual/Controller/SN30+SF30/SN30_Analogue.pdf) |
| ![](/compatibility/8bitdo/bluetooth/sn30v3.gif) | SN30 | ✅ | ☑️ | START + R | Maintenir appuyé le bouton Select pendant 3 secondes | Maintenir appuyé le bouton Select pendant 3 secondes | [Maintenir appuyé le bouton Select pendant 8 secondes](https://download.8bitdo.com/Manual/Controller/SN30+SF30/SN30_Analogue.pdf) |
| ![](/compatibility/8bitdo/bluetooth/sf30.gif) | SF30 &amp; SFC30 | ✅ | ☑️ | START + R | Maintenir appuyé le bouton Select pendant 3 secondes | Maintenir appuyé le bouton Select pendant 3 secondes | [Maintenir appuyé le bouton Select pendant 8 secondes](https://download.8bitdo.com/Manual/Controller/SN30+SF30/SN30+SF30_Manual_V4.pdf) |

### Manettes de style SNES / NES

| Image | Manette | USB | BT | Mode de démarrage | Association BT | Réinitialisation BT | Réinitialisation d'usine |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ![](/compatibility/8bitdo/bluetooth/n30pro2.gif) | N30 Pro 2 | ❓ | ❓ | START | Maintenir appuyé le bouton Pair pendant 1 seconde | Maintenir appuyé le bouton Pair pendant 3 secondes | [Voir le manuel sur le site de 8Bitdo](https://download.8bitdo.com/Manual/Controller/N30pro2/N30pro2-Manual.pdf) |
| ![](/compatibility/8bitdo/bluetooth/nes30pro.jpg) | N30 &amp; NES30 Pro | ✅ | ☑️ | POWER | Maintenir appuyé le bouton Pair pendant 1 seconde | Maintenir appuyé le bouton Pair pendant 3 secondes | [Maintenir appuyé le bouton Start pendant 8 secondes](https://download.8bitdo.com/Manual/Controller/N30pro+F30pro/N30pro+F30pro_Manual_V4.pdf) |
| ![](/compatibility/8bitdo/bluetooth/fc30pro.jpg) | F30 &amp; FC30 Pro | ✅ | ☑️ | POWER + X | Maintenir appuyé le bouton Pair pendant 1 seconde | Maintenir appuyé le bouton Pair pendant 3 secondes | [Maintenir appuyé le bouton Start pendant 8 secondes](https://download.8bitdo.com/Manual/Controller/N30pro+F30pro/N30pro+F30pro_Manual_V4.pdf) |

### Manettes de style NES

| Image | Manette | USB | Bluetooth | Mode de démarrage | Association BT | Réinitialisation BT | Réinitialisation d'usine |
| :---: |  :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ![](/compatibility/8bitdo/bluetooth/n30.png) | N30 NS | ❌ | ☑️ | START | Maintenir appuyé le bouton Select pendant 3 secondes | Maintenir appuyé le bouton Select pendant 3 secondes | [Voir le manuel sur le site de 8Bitdo](https://download.8bitdo.com/Manual/Controller/N30-NS/N30-NS.pdf) |
| ![](/compatibility/8bitdo/bluetooth/nes30.jpg) | N30 &amp; NES30 | ✅ | ☑️ | START + R | Maintenir appuyé le bouton Select pendant 3 secondes | Maintenir appuyé le bouton Select pendant 3 secondes | [Maintenir appuyé le bouton Select pendant 8 secondes](https://download.8bitdo.com/Manual/Controller/N30+F30/N30+F30_Manual_V4.pdf) |
| ![](/compatibility/8bitdo/bluetooth/fc30.jpg) | F30 &amp; FC30 | ✅ | ☑️ | START + R | Maintenir appuyé le bouton Select pendant 3 secondes | Maintenir appuyé le bouton Select pendant 3 secondes | [Maintenir appuyé le bouton Select pendant 8 secondes](https://download.8bitdo.com/Manual/Controller/N30+F30/N30+F30_Manual_V4.pdf) |

### Manettes de style MEGADRIVE

| Image | Manette | USB | Bluetooth | Mode de démarrage | Association BT | Réinitialisation BT | Réinitialisation d'usine |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ![](/compatibility/8bitdo/24gcontrollers/m30.png) | M30 | ✅ | ☑️ | START + B | Maintenir appuyé le bouton Select pendant 1 seconde | Maintenir appuyé le bouton Select pendant 3 secondes | [Voir le manuel sur le site de 8Bitdo](https://download.8bitdo.com/Manual/Controller/M30/M30_Manual.pdf) |
| ![](/compatibility/8bitdo/24gcontrollers/m30-2.4g.gif) | M30 2.4G | ❌ | ❌ | - | START | START | [Voir le manuel sur le site de 8Bitdo](https://download.8bitdo.com/Manual/Controller/M30-2.4g/M30-2.4G-for-genesis-mini-mega-drive-mini.pdf)

### Manettes de style ARCADE

| Image | Manette | USB | Bluetooth | Mode de démarrage | Association BT | Réinitialisation BT | Réinitialisation d'usine |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ![](/compatibility/8bitdo/arcade/n30-arcade-stick.png) | N30 &amp; NES30 Arcade Stick | ✅ | ☑️ | HOME | Maintenir appuyé le bouton Select pendant 3 secondes | Maintenir appuyé le bouton Select pendant 3 secondes | [Voir le manuel sur le site de 8Bitdo](https://download.8bitdo.com/Manual/Controller/Joystick/N30-Arcade-Manual.pdf) |
| ![](/compatibility/8bitdo/arcade/fc30-arcade-stick.png) | F30 & FC30 Arcade Stick | ✅ | ☑️ | HOME | Maintenir appuyé le bouton Select pendant 3 secondes | Maintenir appuyé le bouton Select pendant 3 secondes | [Voir le manuel sur le site de 8Bitdo](https://download.8bitdo.com/Manual/Controller/Joystick/F30-ArcadeJoystick-eng.pdf) |
| ![](/compatibility/8bitdo/arcade/arcade-stick.png) | Arcade Stick | ? | ? | ? | ? | ? | [Voir le manuel sur le site de 8bitdo](https://download.8bitdo.com/Manual/Controller/Joystick/Arcade-Stick-8.pdf) |

### Autres styles de manettes

| Image | Manette | USB | Bluetooth | Mode de démarrage | Association BT | Réinitialisation BT | Réinitialisation d'usine |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ![](/compatibility/8bitdo/bluetooth/micro.gif) | Micro (⚠️ en mode D uniquement) | ✅ | ☑️ | HOME | Maintenir appuyé le bouton Pair pendant 1 seconde | Maintenir appuyé le bouton Pair pendant 1 seconde | [Voir le manuel sur le site de 8Bitdo](https://download.8bitdo.com/Manual/Controller/Micro/Micro-Bluetooth-gamepad-8.pdf) |
| ![](/compatibility/8bitdo/24gcontrollers/neogeo.png) | NEOGEO Wireless Controller | ❓ | ❓ | START | Appuyer sur le bouton Pair | Maintenir appuyé le bouton Pair pendant 1 seconde | [Voir le manuel sur le site de 8Bitdo](https://download.8bitdo.com/Manual/Controller/SNK/neogeo-wireless-controller_Manual.pdf) |
| ![](/compatibility/8bitdo/bluetooth/lite.gif) | Lite | ✅ | ☑️ | HOME | Maintenir appuyé le bouton Pair pendant 2 secondes | Maintenir appuyé le bouton Pair pendant 2 secondes | [Voir le manuel sur le site de 8Bitdo](https://download.8bitdo.com/Manual/Controller/Lite/Lite-8.pdf) |
| ![](/compatibility/8bitdo/bluetooth/lite-se.png) | Lite SE | ❓ | ❓ | ❓ | ❓ | ❓ | ❓ |
| ![](/compatibility/8bitdo/bluetooth/lite-2.gif) | Lite 2 | ❓ | ❓ | ❓ | ❓ | ❓ | ❓ |
| ![](/compatibility/8bitdo/bluetooth/zero.gif) | Zero | ❌ | ☑️ | START + R1 | Maintenir appuyé le bouton Select pendant 3 secondes | Maintenir appuyé le bouton Select pendant 3 secondes | [Voir le manuel sur le site de 8Bitdo](https://download.8bitdo.com/Manual/Controller/ZERO/ZERO_Manual_V4.pdf) |
| ![](/compatibility/8bitdo/bluetooth/zero2.gif) | Zero 2 | ✅ | ☑️ | B + START | Maintenir appuyé le bouton Select pendant 3 secondes | Maintenir appuyé le bouton Select pendant 3 secondes | [Voir le manuel sur le site de 8Bitdo](https://download.8bitdo.com/Manual/Controller/Zero2/Zero2_Manual.pdf) |
| ![](/compatibility/8bitdo/24gcontrollers/pce-2_4g.gif) | PCE / TG16 / PCE Core | ❌ | ❌ | n/a | n/a | n/a | [Voir le manuel sur le site de 8Bitdo](https://download.8bitdo.com/Manual/Controller/PCE-2.4g/PCE-24g-Wireless-gamepad-2.pdf) |

## Dongles 8Bitdo supportés

| Image | Dongle | Compatibilité avec Recalbox | Manettes compatibles |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ![](/compatibility/8bitdo/dongles/wireless-usb-adapter-1.png) | USB Wireless Adapter (en mode X-Input) | ✅ | PS3, PS4 (pro), PS5, Xbox One S, Xbox One X, Wiimote, Wii U Pro, Switch Joy-Cons, Switch Pro, manettes et sticks 8bitdo. |
| ![](/compatibility/8bitdo/dongles/usb-rr-for-ps-classic.png) | USB Adapter for PS Classic (en mode X-Input) | ✅ | PS3, PS4, Wiimote, Wii U Pro, Switch Joy-Cons, Switch Pro, manettes 8bitdo (Bluetooth uniquement). |
| ![](/compatibility/8bitdo/dongles/wireless-usb-adapter-2.gif) | USB Wireless Adaptater 2 | ✅ | PS3, PS4 (pro), PS5, Xbox Series X \| S, Xbox One, Wiimote, Wii U Pro, Switch Joy-Cons, Switch Pro, manettes et sticks 8bitdo. |
| ![](/compatibility/8bitdo/dongles/gbros.png) | GBros Adapter Emeter (With 8Bitdo Receiver) | ✅ | GameCube, GameCube Wavebird (avec le câble d'extension), Wii Classic, Wii Classic Pro, NES Mini, SNES Mini. |
| ![](/compatibility/8bitdo/dongles/nesmini-rr.png) | NES Receiver | ❓ | ❓ |
| ![](/compatibility/8bitdo/dongles/snes-rr.png) | SNES Receiver | ❓ | ❓ |

## Support 8Bitdo

* [Support des manettes 8Bitdo](https://support.8bitdo.com/)
* [Outil de mise à jour](https://support.8bitdo.com/firmware-updater.html) 

Les manettes 8Bitdo ne doivent plus être associées avant de les associer de nouveau. Si vous avez un soucis d'association, vous pouvez tenter la méthode « Réinitialisation du Bluetooth », et si cela ne fonctionne pas, vous pouvez tenter la méthode « Réinitialisation d'usine ».

Vous pouvez trouver le manuel de chacune de vos manettes sur le site de [support 8Bitdo](https://support.8bitdo.com/).

## Faq

### Comment recharger votre manette ?

De préférence, utilisez le port USB de votre ordinateur ou console afin de recharger votre manette.
En cas d'utilisation d'un chargeur classique ou d'un chargeur de téléphone, veillez à ce que celui-ci ne dépasse jamais les 5V 1A.
Une intensité trop élevé peut endommager la batterie et rendre la manette inutilisable en Bluetooth.

### Comment mettre à jour le Firmware ?

Il suffit de vous rendre sur le site de [support 8Bitdo](https://support.8bitdo.com/) et de télécharger le logiciel `Upgrade tool` (Outil de mise à jour) en haut de page.

1. Installez l'application téléchargée `8BitDo Firmware Upgrader`.
2. Ouvrez l'application
3. Une fois l'application ouverte, celle-ci va vous demander de brancher votre manette à votre PC.
4. Votre manette sera automatiquement reconnue. Cliquez sur le bouton `Firmware Update` pour accéder aux mises à jour disponibles pour votre manette.
5. Sélectionnez le firmware souhaité et cliquez sur le bouton bleu `Update`.
6. Suivez la procédure indiquée pour effectuer la mise à jour. Celle-ci aura une barre de progression rouge.
7. Une fois la mise à jour réussie, cliquez sur le bouton vert `Success` et vous pouvez déconnecter votre manette.

### Comment allumer/connecter ma manette correctement ?

En fonction du périphérique (Windows, Mac OS, Android, Switch, etc.), il existe différentes combinaisons d'allumage :

* `START` + `Y` = Mode Switch
* `START` + `A` = Mode macOS
* `START` + `X` = Mode PC / Xbox / Windows (appelé x-input)
* `START` + `B` = Mode Android (appelé d-input)

Vous devez maintenir les touches simultanément jusqu'à allumage de la manette et effectuer l'association.

En cas de connection via USB, maintenir les touches simultanément jusqu'à allumage de la manette, puis relier le câble USB.

>Le mode Android est le mode à privilégier.
{.is-info}

### Comment connecter ma manette à un autre périphérique ?

La manette ne retient qu'une seule adresse de périphérique externe. Si vous souhaitez passer de votre Switch à Android par exemple, il faudra réinitialiser les données d'association de la manette. En fonction des modèles, la procédure diffère un peu. Veuillez consulter le tableau plus haut pour connaitre la procédure.

### Comment puis-je appairer mon adaptateur USB avec ma manette PS3 ?

Il existe un outil permet de gérer votre manette PS3 et les adaptateurs USB.

### {.tabset}
#### Windows

Vous pouvez télécharger cet [utilitaire](https://download.8bitdo.com/Tools/Receiver/8Bitdo_PS3_Tool_Win_V1.1.zip)

En cas de problème avec Windows, il existe 2 solutions :

**Solution 1** :
Installer SixaxisPairToolSetup-0.3.1.exe. Vous pouvez le télécharger sur [cette page](https://sixaxispairtool.en.lo4d.com/download).

**Solution 2** :
Désactiver temporairement le renforcement de la signature des pilotes de Windows. Vous trouverez des images d'aide dans le dossier `How To Temporarily Disable Driver Signature Enforcement On Win8 And Win10` contenu dans le fichier `8Bitdo_PS3_Tool_Win_V1.1.zip`.

En cas d'autres problèmes, vous pouvez installer le pilote 'libusb-win32'.

>La fonction du renforcement de la signature des pilotes de Windows peut se réactiver lors d'une mise à jour ou autre.
{.is-info}

#### macOS
Vous pouvez télécharger cet [utilitaire](https://download.8bitdo.com/Tools/Receiver/8BitDo_PS3_Tool_MacOS_V1.1.zip)

### Qu'en est-il des sticks Arcade ?

En fonction du périphérique (Windows, Mac OS, Android, Switch, etc.), il existe différentes combinaisons d'allumage :

* Windows : position XINPUT.  
* Android : position DINPUT.  
* MacOS et Switch sont non nécessaire.  
  
Procédure d'allumage :

1. Sélectionnez le mode XINPUT ou DINPUT si nécessaire.
2. Appuyez sur le bouton PAIR pour initialiser l'appareillage (La led bleu clignote).
3. Configurez l'appareillage sur votre périphérique (Windows, macOS, Android ou Switch).
4. La led doit se figer en bleu et la manette est prête à être utilisée.

Vous devez maintenir les touches simultanément jusqu'à allumage de la manette, puis effectuer l'association. En cas de connection via USB, maintenez les touches simultanément jusqu'à allumage de la manette, puis relier le câble USB.

### Quel firmware est conseillé pour Recalbox ?

Veuillez consulter le tableau en haut de page pour connaître les firmwares recommandés pour votre manette.

### Qu'en est-il de la compatibilité avec le pad zero V1 ?

Le pad zero v1 fonctionne en Bluetooth uniquement. Allumez la manette en maintenant les boutons `START` + `R1`.

En cas d'utilisation de deux pads zero V1 :

- Première manette, maintenez les boutons `START` + `R1`.
- Deuxième manette, maintenez les boutons `START` + `R1` + `B`.

### Mon pad n'est pas reconnu avec le cable USB ?

Vérifiez que vous utilisez bien le câble fourni avec la manette. Sinon, assurez-vous d'avoir un câble USB de transfert de données et non le câble USB de charge uniquement. Certains câbles USB ne laisse passer que le courant de charge et non le flux de données.