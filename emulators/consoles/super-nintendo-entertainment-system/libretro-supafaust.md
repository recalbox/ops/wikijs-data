---
title: Libretro Supafaust
description: 
published: true
date: 2024-07-24T20:32:44.716Z
tags: libretro, snes, super nintendo, 9.1+, supafaust
editor: markdown
dateCreated: 2024-07-24T20:32:44.716Z
---

**Libretro-supafaust** is an SNES emulator for ARM multi-core Cortex A7, A9, A15, A53 boards.

## ![](/emulators/license.svg) Licence

This core is under **[GPLv2](https://github.com/libretro/supafaust/blob/master/COPYING)** license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ❌ | ✅ | ✅ | ✅ | ✅ | ❌ | ✅ | ✅ |

## ![](/emulators/features.png) Features

| Feature | Supported |
| :---: | :---: |

## ![](/emulators/bios.svg) BIOS

>**No bios is required.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Supported extensions

Roms must have the extension:

* .fig
* .sfc
* .smc
* .swc
* .zip
* .7z

This system supports compressed roms in .zip/.7z format. However, be careful, it is only an archive.

The files contained in the .zip/.7z must correspond to the extensions mentioned above.
Moreover, each .zip/.7z file must contain only one compressed rom.

### Location

Put the roms like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 snes
┃ ┃ ┃ ┃ ┣ 🗒 **game.zip**

>The roms using **No-Intro** format are highly recommended.
{.is-success}

>For more information about roms, go to [this tutorial](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Accessing to options

You can configure various options in two different ways.

* Via RetroArch menu:

┣ 📁 RetroArch Menu
┃ ┣ 📁 Core options
┃ ┃ ┣ 🧩 Name_option

* Via the file `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core options

| Options | Options values | Variables (for configuration override) | Variables values |
| --- | --- | --- | --- |
| Pixel format | `rgb565` ✅ / `xrgb8888` / `0rgb1555` | `supafaust_pixel_format` | `rgb565` / `xrgb8888` / `0rgb1555` |
| Correct pixel aspect ratio | `Activé` ✅ / `Désactivé` / `force_ntsc` / `force_pal` | `supafaust_correct_aspect` | `enabled` / `disabled` / `force_ntsc` / `force_pal` |
| Horizontal blend/double filter | `phr256blend_auto512` ✅ / `phr256blend_512` / `512_blend` / `512` / `phr256blend` | `supafaust_h_filter` | `phr256blend_auto512` / `phr256blend_512` / `512_blend` / `512` / `phr256blend` |
| Deinterlacer | `bob_offset` ✅ / `weave` / `blend` | `supafaust_deinterlacer` |  `bob_offset` / `weave` / `blend` |
| First displayed scanline in NTSC mode | `0` ✅ / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` | `supafaust_slstart` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` |
| Last displayed scanline in NTSC mode | `223` ✅ / `222` / `221` / `220` / `219` / `218` / `217` / `216` / `215` / `214` / `213` / `212` / `211` / `210` / `209` / `208` / `207` | `supafaust_slend` | `223` / `222` / `221` / `220` / `219` / `218` / `217` / `216` / `215` / `214` / `213` / `212` / `211` / `210` / `209` / `208` / `207` |
| First displayed scanline in PAL mode | `0` ✅ / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `21` / `22` / `23` / `24` | `supafaust_slstartp` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `21` / `22` / `23` / `24` |
| Last displayed scanline in PAL mode | `238` ✅ / `237` / `236` / `235` / `234` / `233` / `232` / `231` / `230` / `229` / `228` / `227` / `226` / `225` / `224` / `223` / `222` / `221` / `220` / `219` / `218` / `217` / `216` / `215` / `214` | `supafaust_slendp` | `238` / `237` / `236` / `235` / `234` / `233` / `232` / `231` / `230` / `229` / `228` / `227` / `226` / `225` / `224` / `223` / `222` / `221` / `220` / `219` / `218` / `217` / `216` / `215` / `214` |
| Region/Type of SNES to emulate | `auto` ✅ / `ntsc` / `pal` / `ntsc_lie_auto` / `pal_lie_auto` / `ntsc_lie_pal` / `pal_lie_ntsc` | `supafaust_region` | `auto` / `ntsc` / `pal` / `ntsc_lie_auto` / `pal_lie_auto` / `ntsc_lie_pal` / `pal_lie_ntsc` |
| Begin frame in SNES VBlank to lower latency | `Activé` ✅ / `Désactivé` | `supafaust_frame_begin_vblank` | `enabled` / `disabled` |
| Enable 1-frame run-ahead | `Désactivé` ✅ / `video` / `video+audio` | `supafaust_run_ahead` | `disabled` / `video` / `video+audio` |
| Enable multitap | `Désactivé` ✅ / `port1` / `port2` / `port1+port2` | `supafaust_multitap` | `disabled` / `port1` / `port2` / `port1+port2` |
| CX4 clock rate % | `100` ✅ / `125` / `150` / `175` / `200` / `250` / `300` / `400` / `500` | `supafaust_cx4_clock_rate` | `100` / `125` / `150` / `175` / `200` / `250` / `300` / `400` / `500` |
| SuperFX clock rate % | `95` / `100` ✅ / `125` / `150` / `175` / `200` / `250` / `300` / `400` / `500` | `supafaust_superfx_clock_rate` | `95` / `100` / `125` / `150` / `175` / `200` / `250` / `300` / `400` / `500` |
| Emulate SuperFX instruction cache | `Désactivé` ✅ / `Activé` | `supafaust_superfx_icache` | `disabled` / `enabled` |
| Internal resampler(output rate) | `Désactivé` ✅ / `44100` / `48000` / `96000` | `supafaust_audio_rate` | `disabled` / `44100` / `48000` / `96000` |
| Emulation thread affinity mask | `0x0` / `0x1` / `0x2` ✅ / `0x3` / `0x4` / `0x5` / `0x6` / `0x7` / `0x8` / `0x9` / `0xa` / `0xb` / `0xc` / `0xd` / `0xe` / `0xf` | `supafaust_thread_affinity_emu` | `0x0` / `0x1` / `0x2` / `0x3` / `0x4` / `0x5` / `0x6` / `0x7` / `0x8` / `0x9` / `0xa` / `0xb` / `0xc` / `0xd` / `0xe` / `0xf` |
| PPU render thread affinity mask | `0x0` / `0x1` ✅ / `0x2` / `0x3` / `0x4` / `0x5` / `0x6` / `0x7` / `0x8` / `0x9` / `0xa` / `0xb` / `0xc` / `0xd` / `0xe` / `0xf` | `supafaust_thread_affinity_ppu` | `0x0` / `0x1` / `0x2` / `0x3` / `0x4` / `0x5` / `0x6` / `0x7` / `0x8` / `0x9` / `0xa` / `0xb` / `0xc` / `0xd` / `0xe` / `0xf` |
| PPU renderer | `mt` ✅ / `st` | `supafaust_renderer` | `mt` / `st` |

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/libretro/supafaust/](https://github.com/libretro/supafaust/)