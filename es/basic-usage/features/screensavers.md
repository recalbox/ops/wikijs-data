---
title: Protector de pantalla
description: 
published: true
date: 2023-08-27T18:50:03.195Z
tags: protecttor de pantalla
editor: markdown
dateCreated: 2023-08-27T18:50:03.194Z
---

## Introducción

Dentro de los menús de EmulationStation existe la posibilidad de configurar el protector de pantalla. Algunos de ellos diponen de opciones particulares.

## Uso

* En la lista de sistemas, pulsad `START` y dirigiros dentro de `OPCIONES DE LA INTERFAZ` y luego dentro de `PROTECTOR DE PANTALLA`.
* Aquí, existen varias opciones:

  * `PUESTA EN REPOSO DEPUES DE`: permite configurar el tiempo en minutos para que se lance el protector de pantalla. Entre 0 y 30 minutos sin activdad.
  * `COMPORTAMIENTO DE LA PANTALLA DE REPOSO`: permite seleccionar el protector de pantalla que se quiere aplicar. Existen varias opciones disponibles.
  * `SISTEMAS PARA UTILIZAR EN DEMOS / CLIPS DE JUEGOS`: permite seleccionar los sistemas que serán utilizados por el protector de pantalla. Por supuesto, esta opción sólo se aplica si se ha selccionado el protector de tipo demos o clips.

## {.tabset}

### Modo demo

El modo demo consiste en un fondo de pantalla que se implementa lanzado uno a uno vuestros juegos.

Este modo selecciona los juegos a partir de una lista (la lista de sistemas por defecto incluye todas las consolas de 8/16 bits salvo las consolas portátiles) y los lanza aleatoriamente. Los juegos lanzados se conservan en un histórico para evitar lanzar dos veces el mismo juego y para evitar lanzar dos juegos seguidos del mismo sistema.

![](/basic-usage/features/screensavers/demomode1.png)

#### Activación

Para activar este modo, nada más sencillo:

* Dentro de EmulationStation, abrid el menú con `START`
* Id dentro de `OPCIONES DE LA INTERFAZ` > `PROTECTOR DE PANTALLA`.
* Aquí, seleccionar `DEMOS DE JUEGOS` para la opción `COMPORTAMIENTO DE LA PANTALLA DE REPOSO`.

#### Configuración

Podéis configurar los sistemas que queréis que se utilicen para las demos de los juegos:

* Dentro de EmulationStation, abrid el menú con `START`.
* Id dentro de `OPCIONES DE LA INTERFAZ` > `PROTECTOR DE PANTALLA`
* Aquí, seleccionad los sistemas dentro de `SISTEMAS A UTILIZAR PARA DEMOS / CLIPS DE JUEGOS`.

### Modo clips de juegos

El modo de clips de juegos utiliza los vídeos de scrapping de vuestros juegos como protector de pantalla.

Este modo selecciona al hazar los juegos a partir de una lista de sistemas predefinida (por defecto la lista contiene todas las consolas de 8/16 bits excepto las consolas portátiles). Como en el modo anterior, se conserva un histórico para evitar lanzar varias veces el mismo juego o el mismo sistema.

#### Activación

Para activar este modo del protector de pantalla, basta con:

* Dentro de EmulationStation, abrid el menú con `START`.
* Id dentro de `OPCIONES DE LA INTERFAZ` > `PROTECTOR DE PANTALLA`.
* Aquí, seleccionad `CLIPS DE JUEGOS` para la opción `COMPORTAMIENTO DE LA PANTALLA EN REPOSO`.

#### Configuración

Igual que para el modo demo, podéis seleccionar los sistemas que queréis que el protector de pantalla utilice para los vídeos de los juegos:

* Dentro de EmulationStation, abrid el menú pulsando `START`.
* Id dentro de `OPCIONES DE LA INTERFAZ` > `PROTECTOR DE PANTALLA`.
* Aquí, seleccionad los sitemas que queréis dentro de `SISTEMAS A UTILIZAR PARA DEMOS / CLIPS DE VIDEOS`.