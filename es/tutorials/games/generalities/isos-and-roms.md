---
title: Las roms y las isos
description: 
published: true
date: 2023-09-09T12:30:15.663Z
tags: 
editor: markdown
dateCreated: 2023-09-09T12:30:15.663Z
---

## Qué es una rom / iso

La emulación de juegos consiste en reproducir en una máquina el comportamiento de un juego que ha sido programado para otro tipo de máquina.

En la máquina adecuada (consola o máquina recreativa), el programa no se emula; se dice que el juego funciona _**nativamente**_. 

Por lo general, se almacena en una memoria de sólo lectura (en la placa base o en cartuchos enchufables), cuyo contenido suele denominarse ROM (read only memory, memoria de sólo lectura).

Un emulador funciona del mismo modo que esta máquina, cargando esta ROM y ejecutando el programa (juego) almacenado en ella, adaptándose a los recursos disponibles (periféricos de entrada/salida, velocidad de ejecución, etc.).

Más recientemente, el contenido de los juegos almacenados en CD-ROM o DVD puede copiarse en un archivo de imagen llamado archivo ISO; siguiendo el mismo principio que la emulación de ROMs, se puede emular un juego almacenado como archivo ISO.

A menudo existen varias versiones del mismo volcado de ROM, pero la mayoría de ellas están obsoletas. Algunas versiones pueden ser volcados incorrectos, hacks, fakes, overdumps, underdumps, etc... 

Un conjunto de roms se denomina a menudo **romset**.

### Fichero dat

Un fichero dat es un fichero que contiene toda una serie de informaciones relativas a un dominio determinado. En nuestro caso, es un fichero que enumera todos los juegos conocidos por sistema.

Este fichero dat contiene información básica y un montón de juegos con sus firmas en formato MD5, SHA1 y/o MD5.

Estos ficheros pueden ser usados con una utilidad específica como [clrmamepro](./../../../tutoriales/utilidades/rom-management/clrmamepro), [Romulus Rom Manager](./../../../tutoriales/utilidades/rom-management/romulus-rom-manager), Romcenter, Romvault etc.

### Fichero header

Un fichero de cabecera son los primeros 16 bytes de una ROM que pueden ser leídos por un emulador. Indica al emulador el tamaño de la ROM, la región del juego, el mapeador que utiliza, el tipo de mirroring que tiene, la consola a la que va destinada (consola principal o sistema VS), etc. Algunos emuladores no te dejarán ejecutar el juego si no tienen esta cabecera.

En la SNES, eran las cabeceras creadas por las copiadoras de la época. Hoy en día no sirven para nada y sólo se utilizaban para "firmar" y bloquear las ROMs en la copiadora. Para los usuarios, las ROM con cabecera corresponden a las ROM que se extraían con las copiadoras más antiguas, como la Super Wild Card (extensión SWC) o la Super Magic Card (formato SMC). Estas ROMs ya no son relevantes hoy en día y ya no corresponden a una realidad, pero a veces son necesarias para aplicar hacks realizados en aquella época (finales de los 90 a mediados de los 2000) o para mantener la compatibilidad con emuladores antiguos como ZSNES o versiones antiguas de Snes9X. Las versiones sin header  no tienen esta información de las copiadoras y corresponden al contenido de los chips en las placas de circuito impreso. 

Para la NES la cosa cambia, hablamos de las cabeceras iNES, que es un formato diseñado para esquematizar los diferentes tipos de PCB y revisiones de los cartuchos existentes, en particular los coprocesadores (como los chips MMC) o los procesadores de sonido (como el Konami VRCx). Los emuladores los necesitan para describir las ROM y saber cómo lanzarlas. Pero los emuladores recientes ven las cosas de otra manera: no es la ROM la que debe tener esta información, sino una base de datos en el emulador (o junto a él), que contiene esta información adicional. En este caso, la ROM sólo contiene el código de los chips físicos, y nada más. Emuladores como Nestopia o Mesen tienen estas bases de datos y usan cálculos CRC32 para encontrar la buena entrada en sus bases de datos.
