---
title: Family Computer Disk System
description: 
published: true
date: 2024-07-26T07:47:18.914Z
tags: nintendo, famicom, family computer disk system, consolas
editor: markdown
dateCreated: 2023-09-03T15:55:39.960Z
---

![](/emulators/consoles/famicomdisksystem.svg){.align-center}

## Datos técnicos

* **Fabricante:** Nintendo
* **Año de lanzamiento :** 1986
* **Cantidad vendida :** 4,44 millones
* **Juego más vendido:** Super Mario Bros. 2
* **Procesador:** Ricoh 2A03 de 8 bits
* **RAM:** 32 KB
* **Tamaño del disquete:** 112 KB
* **Procesador de audio/vídeo:** utiliza el hardware de Famicom
* **Puertos:** utiliza hardware de Famicom

## Presentación

El **Famicom Disk System** o **FDS** (**Family Computer Disk System**) es un periférico para la videoconsola Famicom. Comercializado por Nintendo, se coloca debajo de la consola y utiliza un formato de disquete propietario para almacenar los juegos. Lanzada el 21 de febrero de 1986 en Japón, la versión estadounidense, prevista para finales de 1986, nunca llegó a publicarse.

A finales de 1984, Nintendo ya empezaba a pensar en la era post-Famicom, ya que no podía imaginar que su consola siguiera siendo comercialmente viable durante muchos años. El principal problema de la primera consola doméstica era su capacidad de almacenamiento de memoria, por lo que Nintendo decidió ponerle remedio desarrollando una extensión de hardware para su consola que pudiera leer disquetes, que tenían una mayor capacidad de memoria y además ofrecían la posibilidad de guardar partidas.

## Emuladores

[Libretro FBNeo](libretro-fbneo)
[Libretro FCEUmm](libretro-fceumm)
[Libretro Mesen](libretro-mesen)
[Libretro Nestopia](libretro-nestopia)