---
title: PCSX-ReARMed
description: 
published: true
date: 2021-08-09T20:49:53.230Z
tags: ps1, playstation 1, pcsx-rearmed
editor: markdown
dateCreated: 2021-08-01T23:56:23.047Z
---



## ![](/emulators/license.svg) License

This core is under [**GPLv2**](https://github.com/notaz/pcsx_rearmed/blob/master/COPYING) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅  🐌 | ❌ | ❌ | ❌ | ❌ | ❌ | ❌ | ❌ |

🐌  Basses performances mais jouable

## ![](/emulators/features.png) Features



## ![](/emulators/bios.svg) BIOS

### List of mandatory bios

Although some games can start without bios under _PCSX-ReARMed,_ it is strongly advised to add these files in the bios directory in order to improve the performance of the emulator and the management of saves on "virtual" memory cards.

| Filename | Description | MD5 | Provided |
| :---: | :---: | :---: | :---: |
| scph101.bin | Version 4.4 03/24/00 A | 6e3735ff4c7dc899ee98981385f6f3d0 | ❌ |
| scph7001.bin | Version 4.1 12/16/97 A | 1e68c231d0896b7eadcad1d7d8e76129 | ❌ |
| scph5501.bin | Version 3.0 11/18/96 A | 490f666e1afb15b7362b406ed1cea246 | ❌ |
| scph1001.bin | Version 2.0 05/07/95 A | dc2b9bf8da62ec93e868cfd29f0d067d 924e392ed05558ffdb115408c263dccf | ❌ |

### Location

Put the bios like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 🗒 **scph101.bin**
┃ ┃ ┃ ┣ 🗒 **scph7001.bin**
┃ ┃ ┃ ┣ 🗒 **scph5501.bin**
┃ ┃ ┃ ┣ 🗒 **scph1001.bin**

## ![](/emulators/isos.svg) Isos

### Supported extensions

Isos must have the extension:

* .bin/.cue
* .img/.ccd/.sub
* .mdf/.mts
* .pbp
* .bin/.toc
* .cbn

### Location

Put the isos like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 psx
┃ ┃ ┃ ┃ ┣ 🗒 **game.bin**
┃ ┃ ┃ ┃ ┣ 🗒 **game.cue**

>The isos using **Redump** format are highly recommended.
{.is-success}

>For more information about isos, go to [this tutorial](./../../../tutorials/games/generalities/isos-and-roms)!
{.is-info}

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/notaz/pcsx_rearmed/](https://github.com/notaz/pcsx_rearmed/)