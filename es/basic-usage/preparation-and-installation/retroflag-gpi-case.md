---
title: RetroFlag GPi Case
description: 
published: true
date: 2023-04-21T15:12:58.938Z
tags: gpi case, gpi, retroflag
editor: markdown
dateCreated: 2023-04-20T07:26:24.946Z
---

## Preparación

### Elementos necesarios

* 1 x RetroFlag GPi Case
* 1 x Raspberry Pi Zéro W
* 1 x Tarjeta SD
* 1 x Adaptador de tarjeta SD
* 1 x Disipador térmico (para el Raspberry Pi Zéro W)
* 3 X Pilas

### Les pilas recomendadas

* [Duracell Recharge Ultra Piles Rechargeables type AA 2500 mAh 5 ou 6h autonomie](https://www.amazon.fr/dp/B00E3DVQFS/) 
* [Panasonic BK-3HCDE/4BE eneloop pro 2500mAh Lot de 4 piles Ni-MH AA/Mignon/LR6](https://www.amazon.fr/gp/product/B00JWC40JY/)

Con una batería externa de 10 000maH, es la fiesta !

## Présentación del Gpi Case

![](/basic-usage/preparation-and-installation/gpicase1.png)

![](/basic-usage/preparation-and-installation/gpicase2.png)

![](/basic-usage/preparation-and-installation/gpicase3.png)

## Grabar la tarjeta y reiniciar !

>_**No hagáis la instalación usando pilas !**_
>Utilizad siempre el cable USB para la instalación y las primeras pruebas.
{.is-warning}

![GPi Case](/basic-usage/preparation-and-installation/gpicase4.png)

Grabad la tarjeta y reiniciad, no hay nada mas que hacer ! Recalbox detecta e instala todo automáticamente.

Para hacer funcionar vuestro GPi Case :

* Sòlo tenéis que grabar la última version de Recalbox para Raspberry Pi1 / Pi0 en vuestra tarjeta SD, meterla en la ranura de vuestra GPI, y encenderla !
* Depués de esperar unos segundos, deberías ver una imagen como la siguiente :

![Image d'installation](/basic-usage/preparation-and-installation/gpicase5.png)

* Esperaz unos pocos segundos más (puede tardas más dependiendo de la capacidad de la tarjeta SD) hasta que EmulationStation aparece en la pantalla.

![EmulationStation démarre...](/basic-usage/preparation-and-installation/gpicase6.png)

* Una vez aquí, observad el tema de la interfaz totalemente optimizado para la GPi Case, fluido y divertido a la vez !

## Qué cambia respecto a una Recalbox normal ?

Cuando Recalbox detecta e instala los elementos propios a la GPi Case, se optimizan algunos parametros para permitiros jugar más rapidamente.

Por supuesto, estos ajustes por defecto son configurables a través del menu `INICIO` y los podéis modificar si queréis.

### Lista de parámetros modificados en la instalación por defecto :

* Todos los modos de vídeo (videomode) se ponen a `default` para evitar los cambios de resolución HDMI inútiles.
* El nombre de la máquina se configura a **RECALBOXGPI**.
* Se desactivan Kodi y el botón X.
* Se desactiva el mando virtual.
* Se desactivan las actualizaciones automáticas.
* Se desactiva el Netplay
* Se desactiva el Bluetooth
* Se desactivan los drivers XArcade & PS3
* Se desactiva el popup musical 
* La configuración XBOX360 se reemplaza por la configuración del GPi Case (efectivamente, el mando de la GPI es tambien un mando XBOX360 !).
* El tema de la interfaz `recalbox-gpicase` optimizado para el sistema y activado por defecto se encuentran en la carpeta `/recalbox/share/themes`.

### Y además se instalan :

* dpi-pizero-gpicase.dtbo : driver de pantalla de la GPI 
* pwm-audio-pizero-gpicase.dtbo : driver de sonido de la GPI
* Un script especial para el apagado de la GPI, inicialmente desarrolado por Retroflag y que ha sido modificado por el equipo de Recalbox para que el sistema se apaga rápidamente.

## Instalación de juegos

![](/basic-usage/preparation-and-installation/gpicase7.png)

Una vez conectados a internet, los juegos se instalan normalemente utilizando la carpeta compartido SAMBA ( `\\recalboxgpi\share` camino a utilizar dentro del explorador de ficheros). Tambien podéis utilizar el WebManager ( `http://recalboxgpi` a travès de un navegador web)

Disfrutad de vuestro juegos favoritos !

## Lo que hay que saber...

### Que tipos de juegos funcionan en la GPI ?

>Podéis lanzar cualquier juego de consolas y ordenadores de 8/16 bits hasta el arcade.
>_**Sí, en serio !**_
{.is-info}

Para el arcade, existe **PiFBA**, una versión de FBA optimizada para ARM y que necesita un romset 0.2.96.37.  
**FBNeo** (antiguo FBAlpha), **MAME2000** hasta **MAME2003plus** funcionan tambien muy bien y se puede jugar a la mayoria de los juegos.
Algunos juegos **GBA/SNES** y algunos otros juegos de arcade puede tener lag o funcionar lentamente. En estos casos, podéis intentar desactivar la opcion de `REWIND` en las opciones avanzadas del emulador. No obstante, incluso con estos ajustos, es posible que algunos juegos no funcionen a toda velocidad.
 
>No se recomienda instalar juegos para ordenador porque necesitan a menudo un teclado y un ratón para arrancar.
{.is-info}

### Puedo hacer overclocking de mon Pi0 ?

Desgraciadamente, el Raspberry Pi0 no es fácil de overclockar. Además, el cartucho de la GPi no tiene agujeros para la ventilación para poder evacuar el exceso de calor (al menos en la v1 de la GPi Case utilizada para redactar esta documentación).

_**Podéis no obstante jugar con las opciones de overcloking, bajo vuestro propio riesgo !**_

>Si vuestro GPI no es estable después de haber cambiado el overcloking y que no podéis entrar en el menu para desactivarlo, podéis insertar la tarjeta SD en vuestro PC y borrar las líneas de overcloking a la mano del fichero `recalbox-oc-config.txt`. Este fichero se encuentra en la partición `RECALBOX`.
{.is-danger}

Si queréis podéis comprar un pequeño radiador en cobre como el que se muestra en la foto. Su grosor no debe superar 2mm.

![](/basic-usage/preparation-and-installation/gpicase8.jpg)

### Cuanto tiempo dura la batería ?

Les premiers tests montrent que vous aurez environ 2h de jeu sur une charge.

N'oubliez pas que votre boîtier GPi Case dispose d'une prise USB. Dans une voiture où à la maison, utilisez le câble USB.

>Si le dos de la cartouche commence à devenir très chaud, nous vous recommandons d'arrêter le GPi Case et de le laisser refroidir pendant un petit moment.
{.is-info}

### Cómo puedo escraper mis juegos ?

* **Scraper interno :** Siempre se puede utilizar el scraper interno. Se puede utilizar en momentos puntuales para rellenar metadatos que faltan o ponerlos al día rápidemande desde Recalbox. 
* **Scraper externo :** **Para vuestros fullsets os aconsejamos un scraper externo**. Será mas éficaz y rápido que desde la Raspberry Pi.

>También podeis limitar la talla de las imágenes y vidéo desde el menu de configuration del scrapper para ahorrar espacio en disco y mejorar la velocidad del **scraper interno**.
{.is-info}

## Asistencia técnica

* **Si la pantalla se queda negra o salta cuando se roza el cartucho o simplemente al dejar la consola:** 
	* Vérificar todas las connexiones.
  * Levantar el rpi dentro del cartucho, para acercarlo del circuito impreso : ajustad los 4 tornillos que soporta el Pi 0, desenroscándolos ligéramente hasta que el Pi 0 se encuentre paralelo al cartucho. Según los casos, puede ser suficiente con desatornillar los 2 tornillos dorados de arriba.
  
>Este bug aparece debido a un pequeño error de fabricación del cartucho, la conexión entre el Pi 0 y el cartucho no es perfecta.
{.is-info}

* **Résolución de problemas de estabilidad :**

  * **1. La Micro-SD**

	Vérificad la microSD.
  Algunos modelos tienen problemas con los Pi, sin hablar de las microSD chinas de malísima calidad. Probad con otra tarjeta SD, de otra marca.  

  * **2. Las pilas**

  _**No instaléis la GPi Case usando las pilas !**_

  Utilizad siempre el cable USB para la instalación de Recalbox y los primeros tests.
  Si funciona correctamente con el cable y no con las pilas, puede ser que estén descargadas o que no sean de calidad suficiente para proporcionar la energía necesaria.
	La mayoría de las pilas recargables son de 1.2V, y no de 1.5V como se necesita.

  * **3. El modelo del Pi0.**

  Alugnos modelos de Pi0 tienen problemas de estabilidad con los relojes de CPU standard y el firmeware standard del Pi.
  Abrid el fichero `recalbox-oc-config.txt` dentro de la partición `RECALBOX`, y añadid las siguientes líneas al final :
  
  ```ini
  arm_freq=1000
  gpu_freq=500
  core_freq=500
  sdram_freq=500
  sdram_schmoo=0x02000020
  over_voltage=6
  sdram_over_voltage=2
  ```

  Estas opción fuerzan la configuración de los relojes de la CPU correctamenente y aplican una pequeña mejora de rendimiento como bonus.  

  * **4. Montaje**

	Aunque el montaje del Pi es relativamente sencillo, puede ocurrir que se produzcan contactos imperfectos. En estos casos, desmotad el cartucho, limpiad los contactos del Pi y de la Gpi Case con un líquido apropiado a base de alcohol, y volved a montar todo.
  Limpiad igualemente los contactos del cartucho dentro de la Gpi Case.
  Verificad que el cartucho esté bien metido. No hay que forzar el buton de encendido y al inicio ni al apagado. Si se fuerza, significa que el cartucho está mal insertado o que hay algo que molesta.

  * **5. Para terminar...**
  
  Si a pesar de todo esto, vuestro Pi no funciona, o si es inestable, intentad con otro Pi para averigüar si el problema viene del Pi ou de la Gpi Case. Pedid que alguien os preste uno, o comprad otro sabiendo que son muy baratos.
   Si piensas que la GPi Case tiene la culpa de los problemas, podéis intentar devolverla o pedir un recambio. En caso de problemas con el vendedor, explicadle todo el procedimiento que habéis seguido para hacerla funcionar para demostrarle que váis de buena fé y que realemente hay un problema.

* Si os encontráis en una de las dos situaciones siguientes :

  * Al prime arranque, no hay nada en la pantalla depués de haber esperado unos minutos.
  * No podéis configurar el WIFI. Contactadnos en nuestro servidor Discord o en el Forum y dadnos el contenido de los siguientes ficheros, disponbiles dentro de la partición `RECALBOX` de vuestra tarjeta SD :
  
    * `config.txt`
    * `recalbox-backup.conf`
    * `hardware.log`

>Si téneis otros problemas, escribidnos en el  :   
>[Forum](https://forum.recalbox.com/) o en los [Gitlab issues](https://gitlab.com/recalbox/recalbox/issues)
{.is-info}