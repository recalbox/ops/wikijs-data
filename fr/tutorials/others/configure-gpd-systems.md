---
title: Configurer les systèmes GPD
description: Comment leur appliquer certaines configurations pour les faire fonctionner
published: true
date: 2022-12-04T15:35:32.987Z
tags: systèmes, gpd
editor: markdown
dateCreated: 2022-12-04T15:35:32.987Z
---

## Introduction

Le matériel du fabricant GPD n'est pas forcément correctement reconnu (tel que l'écran subissant une rotation non souhaitée, etc.). Néanmoins, il existe une procédure pour le rendre compatible en fonction de la référence de votre matériel.

Ce tutoriel s'applique pour les références suivantes :

- GPD Pocket
- GPD Pocket 2
- GPD Pocket 3
- GPD P2 Max
- GPD MicroPC
- GPD Win 2
- GPD Win Max
- Topjoy Falcon

>Cette procédure est à appliquer après **CHAQUE** mise à jour ainsi qu'après une installation !
{.is-warning}

## Procédure

### Pré-requis

- Avoir installé Recalbox sur une des références de la liste dessus.
- Être à l'aise avec les commandes SSH et le transfert de fichiers.

### Première installation

1. Vous devez obtenir un script spécifique afin de pouvoir installer tout les requis pour faire fonctionner correctement votre matériel. Rendez-vous sur la page https://github.com/wimpysworld/umpc-ubuntu, cliquez sur le bouton vert et cliquez sur `Download ZIP`.

   Décompressez le contenu, ouvrez le fichier `umpc-ubuntu.sh` et modifiez la 4ème ligne en fonction du modèle possédé :

| Modèle | Valeur |
| :---: | :---: |
| GPD Pocket | `gpd-pocket` |
| GPD Pocket 2 | `gpd-pocket2` |
| GPD Pocket 3 | `gpd-pocket3` |
| GPD P2 Max | `gpd-p2-max` |
| GPD MicroPC | `gpd-micropc` |
| GPD Win 2 | `gpd-win2` |
| GPD Win Max | `gpd-win-max` |
| Topjoy Falcon | `topjoy-falcon` |

`UMPC="valeur-de-votre-modele"`

   Enregistrez le fichier après avoir modifié la ligne correspondante.

2. Placez le répertoire décompressé (il doit être nommé `umpc-ubuntu-master`) dans `/recalbox/share/system/`. Vous devez avoir `/recalbox/share/system/umpc-ubuntu-master/` avec les fichiers décompressés dedans.

3. Maintenant, vous devez vous connecter [via SSH](../system/access/root-access-terminal-cli) à votre Recalbox et exécutez les commandes suivantes à la suite :

```ssh
mount -o remount,rw /
mount -o remount,rw /boot
cd umpc-ubuntu-master
bash umpc-ubuntu.sh enable
```

>Il peut y avoir une erreur concernant update-grub. Celle-ci est tout à fait normal.
{.is-info}

4. Ouvrez le fichier `/boot/EFI/BOOT/grub.cfg` et sur la ligne 9, ajoutez ceci à la fin :

```ini
fbcon=rotate:1 video=eDP-1:800x1280 drm.edid_firmware=eDP-1:edid/gpd-win-max-edid.bin
```

>Mettez un espace entre `loglevel=0` et `fbcon=rotate:1` !
{.is-warning}

>Vous pouvez faire la même chose sur la ligne 15, ça pourra servir si vous sélectionnez l'option correspondante au démarrage.
{.is-info}

Il ne vous reste plus qu'à redémarrer et de profitez de votre matériel !

### Mise à jour

Pour chaque mise à jour, vous devez exécuter les étapes 3 et 4 vu dessus.