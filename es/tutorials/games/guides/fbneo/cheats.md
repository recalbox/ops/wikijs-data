---
title: Códigos de trucos/cheats
description: 
published: true
date: 2023-09-20T06:19:55.473Z
tags: fbneo, finalburn, neo, cheats, códigos, trucos
editor: markdown
dateCreated: 2023-09-20T06:19:55.473Z
---

## Introducción

Con FinalBurn Neo, se pueden añadir cheat codes.

## Utilización

* Descargad el fichero de [cheat codes](https://github.com/finalburnneo/FBNeo-cheats/archive/master.zip).
* Descomprimid el fichero.
* Poned la carpeta `cheats` dentro de la carpeta `/recalbox/share/bios/fbneo`.

Para acceder a estos código, durante el juego:

* Abrid el menú RetroArch con `Hotkey + B`.
* Id a `Opciones`

![](/tutorials/games/guides/fbneo/fbneocheats1.png){.full-width}

* Abajo del todo, id dentro de `Cheat`

![](/tutorials/games/guides/fbneo/fbneocheats2.png){.full-width}

* En la lista de opciones, se os mostrarán los códigos disponibles para vuestro juego.

![](/tutorials/games/guides/fbneo/fbneocheats3.png){.full-width}