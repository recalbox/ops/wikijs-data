---
title: Acceso en red via Cyberduck
description: 
published: true
date: 2023-09-13T19:48:43.677Z
tags: cyberduck, red, acceso
editor: markdown
dateCreated: 2023-09-13T19:48:43.677Z
---

## Conexión a la red con Cyberduck

Estáis a punto de descubrir cómo conectaros a vuestra Recalbox en red usando SFTP con el software Cyberduck.

Podréis acceder facilmente a varios ficheros de configuración como por ejemplo el fichero `/boot/recalbox-user-config.txt`.
Si estáis acostumbrado a las herramientas shell, este tutorial no es para ti. Es para usuarios de Windows y macOS que necesitan una interfaz gráfica.

>**Cyberduck** es un software gratuito de transferencia de archivos para Windows y macOS. 
>
>Es compatible con una amplia gama de protocolos: FTP, FTP-SSL, SFTP, WebDAV, Swift, S3, Google Cloud Storage, Windows Azure Storage, Dropbox, Google Drive, Backblaze B2 Cloud Storage, Backspace Cloud Files.
{.is-info}

* Para comenzar, descargad [Cyberduck](https://cyberduck.io/).
* Vuestra Recalbox debe estar conectada a vuestra red local. Podéis encontrar la dirección IP de vuestra Recalbox siguiendo [este tutorial](./../../../tutorials/network/ip/discover-recalbox-ip).

![](/tutorials/system/access/cyberduck1.png){.full-width}

* Abrid Cyberduck y haced click en « Abrir una conexión » y rellenad las informaciones siguientes:
  * Protocolo : `SFTP (SSH File Transfert Protocol)`
  * Port : `22`
  * Servidor : meted la dirección IP de vuestra Recalbox
  * Nombre de usuario : `root`
  * Contraseña : `recalboxroot`
* Pulsad el botón `Conectar` y autorizad las firmas digitales que aparecerán.

![](/tutorials/system/access/cyberduck2.png){.full-width}

* Para guardar la configuración como favorito, haced clic en el icono de favorito de la parte superior izquierda y, a continuación, en el botón `+` de la parte inferior izquierda.

![](/tutorials/system/access/cyberduck3.png){.full-width}

* Aprovechad para renombrar la conexión antes de cerrar la ventana.

![](/tutorials/system/access/cyberduck4.png){.full-width}

* Aquí tenéis el resultado final:

![](/tutorials/system/access/cyberduck5.png){.full-width}

Ya está, habéis guardado vuestra conexión.

El fichero `recalbox-user-config.txt` se encuentra dentro de `/boot/`.
El directorio `share` de Recalbox está dentro de `/recalbox/share/`.

## Permisos de escritura en la tarjeta SD

Si sois hackers experimentados de Recalbox, es posible que necesitéis acceso de escritura a la tarjeta SD para poder modificar el fichero `recalbox-user-config.txt`. 

* En el menú Cyberduck, usad la opción `Enviar comando`.

![](/tutorials/system/access/cyberduck6.png){.full-width}

* Y escribid la siguiente línea:

`mount -o remount,rw /boot`

* Ahora puedes modificar el fichero `recalbox-user-config.txt`. Os recomendamos que hagáis una copia de seguridad del fichero original en vuestro ordneador, luego podéis reemplazar el de la tarjeta SD con el fichero modificado.

[Fuente](https://forum.recalbox.com/topic/7341/tutoriel-se-connecter-en-ssh-pour-les-nuls-avec-cyberduck-pour-config-txt-osx-et-win)