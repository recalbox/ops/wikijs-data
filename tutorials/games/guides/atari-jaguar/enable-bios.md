---
title: Enable bios
description: 
published: true
date: 2023-08-13T23:25:16.329Z
tags: bios, atari, jaguar
editor: markdown
dateCreated: 2021-08-08T06:27:56.890Z
---

## Introduction

Some games will not start with the Jaguar emulator. To work around this problem, you need to enable the bios in the core options.

## Usage

### Via game

* Launch a game that starts correctly for the Atari Jaguar.
* Go to the RetroArch menu by pressing `Hotkey` + `B`.
* Go to `Options`.

![](/tutorials/games/guides/atari-jaguar/jaguarenablebios1-en.png){.full-width}

* Turn on the `Bios` option.

![](/tutorials/games/guides/atari-jaguar/jaguarenablebios2.png){.full-width}

* Close the RetroArch menu (press "Back" once and go to "Resume") and quit the emulator completely.
* Start the game that did not start.

### Via configuration override

* Create a file named `.core.cfg` and add this content inside:

```ini
virtualjaguar_bios=enabled
```

* Put the file in `/recalbox/share/roms/jaguar`.
* Start the game that did not start.

Enjoy the game!