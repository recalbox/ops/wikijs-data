---
title: Libretro Flycast
description: 
published: true
date: 2021-11-03T12:02:54.300Z
tags: libretro, atomiswave, flycast
editor: markdown
dateCreated: 2021-07-05T21:35:19.187Z
---

**Libretro Flycast** is a multi-platform **Sega Dreamcast** emulator able to emulate the **Sammy Atomiswave**.

## ![](/emulators/license.svg) License

This core is under [**GPLv2**](https://github.com/libretro/flycast/blob/master/LICENSE) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ❌ | ❌ | ❌ | ✅ | ✅ | ✅  🐌 | ❌ | ✅ |

🐌  Low performances but playable

## ![](/emulators/features.png) Features

| Feature | Supported |
| :---: | :---: |
| Restart | ✔ |
| Screenshots | ✔ |
| Saues | ✔ |
| Core Options | ✔ |
| RetroArch Cheats | ✔ |
| Controls | ✔ |
| Remapping | ✔ |
| Rumble | ✔ |
| Disk Control | ✔ |

## ![](/emulators/bios.svg) BIOS

### List of mandatory bios

| Filename | Description | MD5 | Provided |
| :---: | :---: | :---: | :---: |
| awbios.zip | Atomiswave BIOS from MAME | 0ec5ae5b5a5c4959fa8b43fcf8687f7c | ❌ |

### Location

Put your bios like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 dc
┃ ┃ ┃ ┃ ┣ 🗒 **awbios.zip**

## ![](/emulators/roms.png) Roms

Libretro Flycast is based on the Romset of **MAME** but also on **NullDC** formats for its Atomiswave part.

### Supported extensions

Roms must have this extension:

* .zip
* .7z
* .bin/.lst
* .dat

## Mame Romset

Only Atomiswave roms **from a RomSet MAME 0.135 or higher** are compatible.  
We recommend the **RomSet Mame 0.235** which will bring a lot of additional compatibilities.  
For more information on the current RomSet version, see the [MameDev] page (https://www.mamedev.org/release.html).

>To sort your arcade roms, the **dat file** is available in the folder: `/recalbox/share/bios/dc/`
{.is-info}

### Location

Put your roms like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 atomiswave
┃ ┃ ┃ ┃ ┣ 🗒 **game.zip**

## NullDC Romset

These roms are compatible with Flycast but **less reliable** than roms **from a MAME set**.

>The **NullDC** roms are in the format : _`.bin + .lst`_.
{.is-info}

### Location

Put your roms like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 atomiswave
┃ ┃ ┃ ┃ ┣ 📁 **game**
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **game.bin**
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **game.lst**

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Accessing to options

You can configure various options in two different ways.

* Via RetroArch menu:

┣ 📁 RetroArch Menu
┃ ┣ 📁 Core options
┃ ┃ ┣ 🧩 Name_option

* Via the file `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core options

| Options | Options values | Variables (for configuration override) | Variables values |
| --- | --- | --- | --- |
| System Type (Restart) | `Auto` ✅ / `Dreamcast` / `NAOMI` / `Atomiswave` | `flycast_system` | `auto` / `dreamcast` / `naomi` / `atomiswave` |
| Internal resolution (restart) | `320x240` ✅ / `640x480` / `800x600` / `960x720` / `1024x768` / `1280x960` / `1440x1080` / `1600x1200` / `1920x1440` / `2560x1920` / `2880x2160` / `3200x2400` / `3840x2880` / `4480x3360` / `5120x3840` / `5760x4320 / ``6400x4800` / `7040x5280` / `7680x5760` / `8320x6240` / `8960x6720` / `9600x7200` / `10240x7680` / `10880x8160` / `11520x8640` / `12160x9120` / `12800x9600` | `flycast_internal_resolution` | `320x240` / `640x480` / `800x600` / `960x720` / `1024x768` / `1280x960` / `1440x1080` / `1600x1200` / `1920x1440` / `2560x1920` / `2880x2160` / `3200x2400` / `3840x2880` / `4480x3360` / `5120x3840` / `5760x4320 / ``6400x4800` / `7040x5280` / `7680x5760` / `8320x6240` / `8960x6720` / `9600x7200` / `10240x7680` / `10880x8160` / `11520x8640` / `12160x9120` / `12800x9600` |
| Screen Orientation | `Horizontal` ✅ / `Vertical` | `flycast_screen_rotation` | `horizontal` / `vertical` |
| Alpha Sorting | `Per-Strip (fast, least accurate)` / `Per-Triangle (normal)` ✅ | `flycast_alpha_sorting` | `per-strip (fast, least accurate)` / `per-triangle (normal)` |
| Mipmapping | `Enabled` ✅ / `Disabled` | `flycast_mipmapping` | `enabled` / `disabled` |
| Fog Effects | `Enabled` ✅ / `Disabled` | `flycast_fog` | `enabled` / `disabled` |
| Volume modifier | `Enabled` ✅ / `Disabled` | `flycast_volume_modifier_enable` | `enabled` / `disabled` |
| Widescreen Hack (Restart) | `Disabled` ✅ / `Enabled` | `flycast_widescreen_hack` | `disabled` / `enabled` |
| Widescreen Cheats (Restart) | `Disabled` ✅ / `Enabled` | `flycast_widescreen_cheats` | `disabled` / `enabled` |
| Region | `Default` ✅ / `Japan` / `USA` / `Europe` | `flycast_region` | `Default` / `Japan` / `USA` / `Europe` |
| DIV Matching | `Disabled` / `Auto` ✅ | `flycast_div_matching` | `disabled` / `auto` |
| Analog Stick Deadzone | `0%` / `5%` / `10%` / `15%` ✅ / `20%` / `25%` / `30%` | `flycast_analog_stick_deadzone` | `0%` / `5%` / `10%` / `15%` / `20%` / `25%` / `30%` |
| Trigger Deadzone | `0%` ✅ / `5%` / `10%` / `15%` / `20%` / `25%` / `30%` | `flycast_trigger_deadzone` | `0%` / `5%` / `10%` / `15%` / `20%` / `25%` / `30%` |
| Digital Triggers | `Disabled` ✅ / `Enabled` | `flycast_digital_triggers` | `disabled` / `enabled` |
| Enable DSP | `Disabled`  / `Enabled` ✅ | `flycast_enable_dsp` | `disabled` / `enabled` |
| Anisotropic Filtering | `Off` / `2` / `4` ✅ / `8` / `16` | `flycast_anisotropic_filtering` | `disabled` / `2` / `4` / `8` / `16` |
| PowerVR2 Post-processing Filter | `Disabled` ✅ / `Enabled` | `flycast_pvr2_filtering` | `disabled` / `enabled` |
| Texture Upscaling (xBRZ) | `Off` ✅ / `2` / `4` / `6` | `flycast_texupscale` | `disabled` / `2` / `4` / `6` |
| Enable RTT (Render To Texture) Buffer | `Disabled` ✅ / `Enabled` | `flycast_enable_rttb` | `disabled` / `enabled` |
| Render To Texture Upscaling | `1x` ✅ / `2x` / `3x` / `4x` / `8x` | `flycast_render_to_texture_upscaling` | `1x` / `2x` / `3x` / `4x` / `8x` |
| Threaded Rendering (Restart) | `Disabled`  / `Enabled` ✅ | `flycast_threaded_rendering` | `disabled` / `enabled` |
| Synchronous Rendering | `Disabled`  / `Enabled` ✅ | `flycast_synchronous_rendering` | `disabled` / `enabled` |
| Delay Frame Swapping | `Disabled` ✅ / `Enabled` | `flycast_delay_frame_swapping` | `disabled` / `enabled` |
| Frame Skipping | `Disabled` ✅ / `1` / `2` / `3` / `4` / `5` / `6` | `flycast_frame_skipping` | `off` / `1` / `2` / `3` / `4` / `5` / `6` |
| Allow NAOMI Service Buttons | `Disabled` ✅ / `Enabled` | `flycast_allow_service_buttons` | `disabled` / `enabled` |
| Enable NAOMI 15KHz Dipswitch | `Disabled` ✅ / `Enabled` | `flycast_enable_naomi_15khz_dipswitch` | `disabled` / `enabled` |
| Load Custom Textures | `Disabled` ✅ / `Enabled` | `flycast_custom_textures` | `disabled` / `enabled` |
| Dump Textures | `Disabled` ✅ / `Enabled` | `flycast_dump_textures` | `disabled` / `enabled` |
| Show Light Gun Settings | `Enabled` / `Disabled` ✅ | `flycast_show_lightgun_settings` | `enabled` / `disabled` |
| Gun Crosshair 1 Display | `Disabled` ✅ / `White` / `Red` / `Green` / `Blue` | `flycast_lightgun1_crosshair` | `disabled` / `White` / `Red` / `Green` / `Blue` |
| Gun Crosshair 2 Display | `Disabled` ✅ / `White` / `Red` / `Green` / `Blue` | `flycast_lightgun2_crosshair` | `disabled` / `White` / `Red` / `Green` / `Blue` |
| Gun Crosshair 3 Display | `Disabled` ✅ / `White` / `Red` / `Green` / `Blue` | `flycast_lightgun3_crosshair` | `disabled` / `White` / `Red` / `Green` / `Blue` |
| Gun Crosshair 4 Display | `Disabled` ✅ / `White` / `Red` / `Green` / `Blue` | `flycast_lightgun4_crosshair` | `disabled` / `White` / `Red` / `Green` / `Blue` |

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/libretro/flycast](https://github.com/libretro/flycast)
* **Libretro documentation**: [https://docs.libretro.com/library/flycast/](https://docs.libretro.com/library/flycast/)
* **Official source code**: [https://github.com/flyinghead/flycast](https://github.com/flyinghead/flycast)