---
title: Arduboy
description: 
published: true
date: 2023-09-27T06:25:02.607Z
tags: fantasy, arduboy, 9.1+
editor: markdown
dateCreated: 2023-09-27T06:20:57.862Z
---

![](/emulators/fantasy/arduboy.svg){.align-center}

## Technical data

* **Developer**: Kevin Bates
* **Release date**: 2015
* **CPU**: 8-bit ATMega32u4 @ 16 MHz
* **RAM**: 2,5KB linear RAM
* **Display**: 1.3" 128 x 64 pixels 1-bit OLED
* **Storage**: 32KB Flash
* **Sound**: 2 Ch. Piezo Speaker

## Presentation

The arduboy is a portable 8-bit video game console based on the open source Arduino platform.

It was initially a Kickstarter project by Kevin Bates that saw the light of day in early 2015 and secured over $400,000 with more than 7,000 contributors.

## Emulators

[Libretro Arduous](/emulators/fantasy/arduboy/libretro-arduous)