---
title: Naomi GD-Rom
description: 
published: true
date: 2024-07-29T12:26:46.978Z
tags: arcade, naomi, gd-rom, naomi-gd
editor: markdown
dateCreated: 2023-09-01T21:14:52.030Z
---

![](/emulators/arcade/naomigd.svg){.align-center}

## Ficha técnica

* **Procesador** : Hitachi SH-4 RISC 360 MIPS/1.4 GFLOPS
* **Vídeo** : PowerVR 2 a 100 MHz (Nec CLX2)
* **Audio** : ARM7 Yamaha XG AICA RISC 32 bits 25 MHz
* **RAM** : 32 MB1,2
* **RAM de vídeo** : 16 MB1,2
* **RAM de audio** : 8 MB1,2
* **Soporte** : Cartucho (PCB de ROMs)
* **GD-ROM** : CompactFlash8

## Presentación

El sistema Naomi se lanzó bajo el nombre de **Naomi Multi System** (también llamado **Naomi Multiboard**), acrónimo de _New Arcade Operation Machine Idea_, que literalmente significa "nueva idea de máquina de arcade". Naomi también se traduce como "belleza" en japonés.

El sistema cuenta con una arquitectura **muy similar a la de la consola Dreamcast**: un microprocesador Hitachi SH-4 a 200 MHz, un procesador gráfico PowerVR Series 2 GPU (PVR2DC) y un procesador de sonido Yamaha AICA; pero con **más memoria RAM y memoria de vídeo** (dos veces más que la Dreamcast). Los juegos se almacenan en **un cartucho** (Naomi) **o un GD-ROM** (NaomiGD).

Producido por SEGA, líder de los arcades en esa época, para competir con el Sistema 23 de NAMCO, su comercialización comenzó en 1998 con **"The House of the Dead 2"** como primer juego hasta el año 2006. Más de 160 juegos se lanzaron en este hardware (el récord mundial de número de juegos en un hardware de arcade).

Sega anuncia que su **Naomi 2** sigue siendo compatible con los **cartuchos y GD-Rom de Naomi 1**.

## Emuladores

[Libretro Flycast](libretro-flycast)
[Libretro Flycast-Next](libretro-flycast-next)