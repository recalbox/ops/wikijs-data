---
title: Configuration des shaders
description: 
published: true
date: 2023-09-07T17:04:29.255Z
tags: configuration, shaders
editor: markdown
dateCreated: 2021-05-21T08:37:50.678Z
---

## Introduction

Les shaders sont des filtres qui peuvent modifier l'image que vous obtenez lorsque vous utilisez vos émulateurs. Il est souvent utilisé pour obtenir une image plus "rétro", proche de l'image sur laquelle nous avons joué à l'époque.

Les différents types de fichiers que vous avez dans Recalbox sont :

*  **GLSL** : le shader lui-même.
*  **GLSLP** : le shader préréglé. Peut combiner plusieurs shaders.
* /recalbox/share/system/configs/shaders/**xxx.cfg** : les fichiers de configuration des pack de shaders.

Le fichier de configuration des shaders (`.CFG`) fait référence à un préréglage de shader (`.GLSLP`) pour chaque système de Recalbox. Chaque fichier `.GLSLP` fait référence à un ou plusieurs shaders (`.GLSL`).

## Shaders prédéfinis

Avec les shaders prédéfinis, vous pouvez configurer des shaders pour tous vos systèmes en une seule option.

Les shaders prédéfinis sont créés par la communauté, et sont optimisés à chaque nouvelle version de Recalbox.

Voici la liste des shaders prédéfinis :

* `AUCUN` : aucun shader actif.
* `SCANLINES` : active les lignes de balayage sur tous les émulateurs.
* `RETRO` : sélectionne les « meilleurs » shaders pour chaque système, choisis par la communauté, qui vous offriront l'expérience de jeu la plus proche de l'original.

Vous pouvez définir le pack de shaders que vous souhaitez activer dans le menu `OPTIONS DE JEU` dans EmulationStation.

## Shaders personnalisés

Il existe 2 façons de définir des shaders dans Recalbox.

### Shader global

Vous avez la possibilité de choisir d'utiliser un shader pour tous les systèmes auquel vous jouez. Pour cela, suivez ces étapes :

* Dans EmulationStation, ouvrez le menu avec `START`.
* Allez dans `OPTIONS DES JEUX` > `SHADERS`.
* Choisissez le shader souhaité et il sera appliqué au lancement de chaque jeu.

### Shader par système

>Ceci s'adresse aux utilisateurs plus expérimentés.
{.is-info}

Vous pouvez combiner la puissance des packs de shaders avec la personnalisation poussée de recalbox.conf. Le `global.shaderset` peut définir un shader de base pour chaque émulateur. Vous pouvez ensuite modifier le préréglage des shaders d'un système avec l'option `systemname.shader`. Les préréglages par défaut des shaders sont définis dans `/recalbox/share_init/shaders/`

Par exemple, si je veux :

* les shaders `RETRO` sur tous les émulateurs,
* pas de shaders pour ma Gameboy,
* un shader déjà existant préréglé pour la Megadrive,
* et un shader personnalisé préréglé pour ma Snes

```ini
# Set the retro shader set for all emulators, now i know that i have the best selection
global.shaderset=retro
# But my gameboy seem better for me with no shaders
gb.shaders=
# We use the already existing 4xbr_retro.glslp shader for megadrive
megadrive.shaders=/recalbox/share_init/shaders/4xbr_retro.glslp
# And i want to apply my own shader preset on snes
snes.shaders=/recalbox/share/shaders/custom/snes.glslp
```

Vous pouvez également changer de shaders dans le jeu en utilisant votre manette.  
Utilisez les commandes spéciales `Hotkey + R2` ou `Hotkey + L2` pour aller au shader suivant ou précédent.

>Notez que les shaders appliquent des calculs et des filtres sur toutes les images générées par l'émulateur, ce qui peut avoir un impact important sur les performances de l'émulateur.
{.is-warning}