---
title: Configurez votre petit écran TFT sur le bus SPI
description: 
published: true
date: 2022-11-11T22:09:58.834Z
tags: tft, bus, spi, écran, petit
editor: markdown
dateCreated: 2021-05-21T08:44:29.527Z
---

## Configurez votre écran TFT Adafruit 2,8" capacitif/résistif

Cet écran se trouve sur [ce site](https://www.adafruit.com/product/1601). Comme les overlays existent déjà pour cet écran dans le répertoire `/boot/overlays`, il sera pris en charge avec une configuration simple.

```ini
pitft28-capacitive.dtbo
pitft28-resistive.dtbo
```

Vérifiez si vous avez la version capacitive ou résistive de l'écran et ajustez la ligne `dt-overlay` dans le fichier [recalbox-user-config.txt](./../../../tutorials/system/modification/configtxt-configuration).

Cet écran utilise le bus SPI pour être piloté. Sa faible résolution nous permet d'obtenir une bonne fréquence d'images dans les fréquences d'émulation. Pas vraiment à 60fps, mais plus que le minimum de 30 fps grâce au programme modifié `fcbp`.

### Modification à faire pour correspondre à la résolution de l'écran

* Vous devez ouvrir le fichier [recalbox-user-config.txt](./../../../tutorials/system/modification/configtxt-configuration).
* Ajouter ceci dans ce fichier :

```ini
dtparam=spi=on
dtparam=i2c1=on
dtparam=i2c_arm=on
hdmi_cvt=320 240 60 1 0 0 0
hdmi_group=2
hdmi_mode=87
# working config for speed
# 900 Mhz -> 48000000
# 1Ghz -> 35000000
#modification of the speed   
dtoverlay=pitft28-resistive,rotate=90,speed=22000000,fps=60
```

La vitesse du bus SPI doit être ajustée si vous overclockez votre Raspberry Pi pour obtenir une bonne image. Ces réglages ont été testés sur un Raspberry Pi1 à 1,1Ghz.

Pour commencer, vous pouvez essayer avec ceci :

```ini
dtoverlay=pitft28-resistive,rotate=90,speed=32000000,fps=20
```

Dans le cas d'un écran capacitif, il suffit de remplacer par ceci :

```ini
dtoverlay=pitft28-capacitive,rotate=90,speed=32000000,fps=20
```

Lors du redémarrage, l'écran doit commencer par un écran blanc et, après quelques secondes, il doit devenir noir. C'est le signe que l'écran est maintenant pris en charge. Mais vous n'obtiendrez aucune image tant que vous ne lancerez pas `fbcp`.

### Activation de fbcp

* Ouvrez le fichier [recalbox.conf](./../../../basic-usage/getting-started/recalboxconf-file)
* Trouvez la ligne suivante tout en haut :

```ini
system.fbcp.enabled=0
```

Vous devez désactiver les controlleurs GPIO pour utiliser `fbcp`.

* Modifiez cette ligne par ceci :

```ini
system.fbcp.enabled=1
```

* Trouvez la ligne suivante et modifiez-la si nécessaire :

```ini
controllers.gpio.enabled=0
```

Si vous utilisez les controlleurs GPIO, vous pouvez à la place modifier la configuration des broches GPIO en utilisant le pin 4 ou 5 pour régler la broche utilisée, en évitant les broches utilisées par votre écran. Voir [ici](./../../../tutorials/controllers/gpio/gpio-controllers) pour plus de détails.

* Modifiez le fichier [recalbox.conf](./../../../basic-usage/getting-started/recalboxconf-file) comme ceci :

```ini
#global.videomode=DMT 87 HDMI
global.videomode=default

#global.ratio=auto
global.ratio=4/3
```

* Sauvegardez le fichier et redémarrez. L'écran devrait maintenant fonctionner parfaitement. Si vous rencontrez des problèmes, vérifiez d'abord que le programme `fbcp` fonctionne avec :

```shell
ps aux | grep fbcp
```

Si une autre ligne que celle indiquée est listée, c'est tout bon.

Cet écran de 2,8" fonctionne bien mais peut être trop petit et trop cher (35€) en ce qui concerne les clones chinois. Configurons donc maintenant un écran TFT 3,2" de Waveshare que l'on trouve sur Banggood pour un prix inférieur à 15€.

##  Configurez votre écran TFT Waveshare 3,2" capacitif/résistif

L'écran est le suivant : un module d'affichage TFT LCD de 3,2 pouces à écran tactile pour le Raspberry Pi B+, B, A+. Sa résolution est la même que celle du 2,8" : 320x240. Il s'agit en fait d'un [écran Waveshare](https://www.waveshare.com/3.2inch-rpi-lcd-b.htm).

Cet écran n'est pas pris en charge par défaut par les systèmes de fichier du noyau. Nous devrons en ajouter d'autres pour que l'écran fonctionne. Ces fichiers peuvent se trouver [ici](https://github.com/swkim01/waveshare-dtoverlays).

Obtenez les fichiers `waveshare35a-overlay.dtb` et `waveshare32b-overlay.dtb` respectivement pour l'écran WaveShare 3,2" 320x240 et l'écran WaveShare 3,5" 320x480. Pour les nouveaux noyaux de la version 4.4, nous devons renommer les fichiers dtb en fichiers dtbo pour qu'ils correspondent au nouveau nom de l'arbre de superposition. Renommez `waveshare35a-overlay.dtb` en `waveshare35a.dtbo` et `waveshare32b-overlay.dtb` en `waveshare32b.dtbo` et copiez-les dans le répertoire `/boot/overlays`.

* Vous devez ouvrir le fichier [recalbox-user-config.txt](./../../../tutorials/system/modification/configtxt-configuration) avec nano ou vim et ajoutez les lignes suivantes :

```ini
#tft screen

#Waveshare 3.2 TFT Screen
#same resolution for hdmi and tft
hdmi_cvt=320 240 60 1 0 0 0
hdmi_group=2                
hdmi_mode=1                 
hdmi_mode=87                

dtparam=spi=on              
dtoverlay=waveshare32b:rotate=270,speed=82000000
```

### Activation de fbcp

* Ouvrez le fichier [recalbox.conf](./../../../basic-usage/getting-started/recalboxconf-file)
* Trouvez la ligne suivante tout en haut :

```ini
system.fbcp.enabled=0
```

Vous devez désactiver les controlleurs GPIO pour utiliser `fbcp`.

* Modifiez cette ligne par ceci :

```ini
system.fbcp.enabled=1
```

* Trouvez la ligne suivante et modifiez-la si nécessaire :

```ini
controllers.gpio.enabled=0
```

Si vous utilisez les controlleurs GPIO, vous pouvez à la place modifier la configuration des broches GPIO en utilisant le pin 4 ou 5 pour régler la broche utilisée, en évitant les broches utilisées par votre écran. Voir [ici](./../../../tutorials/controllers/gpio/gpio-controllers) pour plus de détails.

* Modifiez le fichier [recalbox.conf](./../../../basic-usage/getting-started/recalboxconf-file) comme ceci :

```ini
#global.videomode=DMT 87 HDMI
global.videomode=default

#global.ratio=auto
global.ratio=4/3
```

* Sauvegardez le fichier et redémarrez. L'écran devrait maintenant fonctionner parfaitement. Si vous rencontrez des problèmes, vérifiez d'abord que le programme `fbcp` fonctionne avec :

```shell
ps aux | grep fbcp
```

Si une autre ligne que celle indiquée est listée, c'est tout bon.

Un écran fonctionnel en vidéo :

![Waveshare 3,2" résistif TFT](/tutorials/video/tft/waveshare-3-2.jpg)

[https://youtu.be/hcFk_vjQLVo](https://youtu.be/hcFk_vjQLVo)

## Pourquoi l'écran TFT capacitif/résistif de 3,5" de Waveshare n'est-il pas utilisable avec Recalbox ?

Cet écran ne peut pas être utilisé pour les jeux d'arcade avec Recalbox. Le bus SPI ne dispose pas d'une bande passante suffisante pour traiter cette résolution supérieure de 480x320. Si vous augmentez la vitesse du bus, l'affichage devient instable (couleurs, clignotement). Pendant mes tests, je n'ai pu obtenir que 20-25 FPS. Cet écran est utilisable avec un serveur X avec une fréquence d'images lente mais pas en mode arcade qui nécessite une fréquence d'images plus élevée.

Comme décrit sur [ce site](https://learn.adafruit.com/), il n'est pas recommandé d'utiliser cet écran pour jouer. &lt;&lt; Avec deux fois plus de pixels à pousser sur l'écran, le PiTFT 3,5" est sensiblement plus lent que ses frères plus compacts et nous le déconseillons fortement pour les jeux &gt;&gt;. Maintenant, vous savez !

Mais si vous voulez faire en sorte que cela fonctionne, vous pouvez procéder comme suit :

### Modification du fichier /boot/recalbox-user-config.txt pour correspondre à la résolution de l'écran

Obtenez les fichiers `waveshare35a-overlay.dtb` et `waveshare32b-overlay.dtb` respectivement pour l'écran WaveShare 3,2" 320x240 et l'écran WaveShare 3,5" 320x480. Pour les nouveaux noyaux de la version 4.4, nous devons renommer les fichiers dtb en fichiers dtbo pour qu'ils correspondent au nouveau nom de l'arbre de superposition. Renommez `waveshare35a-overlay.dtb` en `waveshare35a.dtbo` et `waveshare32b-overlay.dtb` en `waveshare32b.dtbo` et copiez-les dans le répertoire **/boot/overlays**

* Vous devez ouvrir le fichier [recalbox-user-config.txt](./../../../tutorials/system/modification/configtxt-configuration) avec nano ou vim et ajoutez les lignes suivantes :

```ini
#tft screen
#Waveshare 3.5 TFT Screen
#same resolution for hdmi and tft
hdmi_cvt=480 320 60 1 0 0 0
hdmi_group=2
hdmi_mode=1
hdmi_mode=87

dtparam=spi=on
dtoverlay=waveshare35a:rotate=270,speed=27000000
# speed=41000000,fps=60 for better FPS, but the colors will look a little weird.
```

### Activation de fbcp

* Ouvrez le fichier [recalbox.conf](./../../../basic-usage/getting-started/recalboxconf-file)
* Trouvez la ligne suivante tout en haut :

```ini
system.fbcp.enabled=0
```

Vous devez désactiver les controlleurs GPIO pour utiliser `fbcp`.

* Modifiez cette ligne par ceci :

```ini
system.fbcp.enabled=1
```

* Trouvez la ligne suivante et modifiez-la si nécessaire :

```ini
controllers.gpio.enabled=0
```

Si vous utilisez les controlleurs GPIO, vous pouvez à la place modifier la configuration des broches GPIO en utilisant le pin 4 ou 5 pour régler la broche utilisée, en évitant les broches utilisées par votre écran. Voir [ici](./../../../tutorials/controllers/gpio/gpio-controllers) pour plus de détails.

* Modifiez le fichier [recalbox.conf](./../../../basic-usage/getting-started/recalboxconf-file) comme ceci :

```ini
#global.videomode=DMT 87 HDMI
global.videomode=default

#global.ratio=auto
global.ratio=3/2
```

* Sauvegardez le fichier et redémarrez. L'écran devrait maintenant fonctionner parfaitement. Si vous rencontrez des problèmes, vérifiez d'abord que le programme `fbcp` fonctionne avec :

```shell
ps aux | grep fbcp
```

Si une autre ligne que celle indiquée est listée, c'est tout bon.

Un écran fonctionnel de 3,5 pouces avec des problèmes de FPS en vidéo :

![Waveshare 3,5' resistif TFT](/tutorials/video/tft/waveshare-3-5.jpg)

[https://youtu.be/HJXQAEaVhKE](https://youtu.be/HJXQAEaVhKE)

Fonctionnant avec une vitesse=41000000,fps=60 ; c'est clairement risqué, certains écrans donneront des résultats étranges... Cela fonctionnera sur certains écrans en fonction du contrôleur de la puce intégrée.

Selon mon humble avis, si vous avez le [LCD 3,5" (C) pour Raspberry Pi (480x320 ; 125Mhz)](https://www.waveshare.com/3.5inch-RPi-LCD-C.htm), il devrait fonctionner, mais avec le [LCD 3,5" (B) pour Raspberry Pi (480x320 ; IPS)](https://www.waveshare.com/3.5inch-RPi-LCD-B.htm), vous ne pourrez pas obtenir 60fps !

![Recalbox 3.5 TFT LCD Raspberry Pi 3 B](/tutorials/video/tft/waveshare-3-5-result.jpg)

[https://www.youtube.com/watch?v=cgznOcvRRqQ](https://www.youtube.com/watch?v=cgznOcvRRqQ)

## Faites fonctionner votre écran TFT 3,5" résistif Waveshare à fond !

Bientôt disponible.
Ceci est à destination des courageux : [https://github.com/juj/fbcp-ili9341](https://github.com/juj/fbcp-ili9341)