---
title: Libretro Potator
description: 
published: true
date: 2024-07-25T22:46:31.107Z
tags: libretro, supervision, potator, 7.2+
editor: markdown
dateCreated: 2023-09-11T14:16:47.555Z
---

**Libretro Potator** es un emulador de supervisión Watara basado en la versión Normmatt.

## ![](/emulators/license.svg) Licencia

El core posee la siguiente licencia [**Domaine Public**](https://github.com/libretro/potator/blob/master/LICENSE).

## ![](/emulators/compatibility.png) Compatibilidad

 | RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | 
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | 

## ![](/emulators/features.png) Funcionalidades



## ![](/emulators/bios.svg) BIOS

>**No necesita bios.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Extensiones de roms soportadas

Las roms deben tener las extensiones siguientes:

* .sv
* .zip
* .7z

Este sistema soporta las roms comprimidas en formato .zip/.7z. No obstante, se trata sólo de un archivo.

Los ficheros contenidos en los .zip/.7z deben tener las extensiones citadas previamente.
Además, cada fichero .zip/.7z sólo debe contener un fichero rom al interior.

### Emplazamiento

Disponed vuestras roms como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 supervision
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.zip**

>Se aconsejan las roms del proyecto **No-Intro**.
{.is-success}

>Para más información sobre las roms, consultad [este tutorial](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

>Para conservar las configuraciones personalizadas cuando se produce una actualización del sistema Recalbox, os aconsejamos utilizar las [Sobrecargas de configuración](./../../../advanced-usage/configuration-override).
{.is-info}

### Acceder a las opciones

Podéis configurar las opciones del core de dos formas diferentes.

* Via el Menú RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via el fichero `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Opciones del core

| Opción | Valor de la opción | Variable (para la sobrecarga) | Valores posibles |
| --- | --- | --- | --- |
| Internal Palette | `Greyscale` ✅ / `Potator Amber` / `Potator Green` / `Potator Blue` / `Potator BGB` / `Potator Wataroo` / `Game Boy DMG` / `Game Boy Pocket` / `Game Boy Light` / `Blossom Pink` / `Bubbles Blue` / `Buttercup Green` / `Digivice` / `Game.com` / `GameKing` / `Game Master` / `Golden Wild` / `Greenscale` / `Hokage Orange` / `Labo Fawn` / `Legendary Super Saiyan` / `Microvision` / `Million Live Gold` / `Odyssey Gold` / `Shiny Sky Blue` / `Slime Blue` / `TI-83` / `Travel Wood` / `Virtual Boy` | `potator_palette` | `default` / `potator_amber` / `potator_green` / `potator_blue` / `potator_bgb` / `potator_wataroo` / `gb_dmg` / `gb_pocket` / `gb_light` / `blossom_pink` / `bubbles_blue` / `buttercup_green` / `digivice` / `game_com` / `gameking` / `game_master` / `golden_wild` / `greenscale` / `hokage_orange` / `labo_fawn` / `legendary_super_saiyan` / `microvision` / `million_live_gold` / `odyssey_gold` / `shiny_sky_blue` / `slime_blue` / `ti_83` / `travel_wood` / `virtual_boy` |
| LCD Ghosting | `Disabled` ✅ / `1 Frame` / `2 Frames` / `3 Frames` / `4 Frames` / `5 Frames` / `6 Frames` / `7 Frames` / `8 Frames` | `potator_lcd_ghosting` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` |
 | Frameskip | `Désactivé` ✅ / `Auto` / `Manual` | `potator_frameskip` | `disabled` / `auto` / `manual` |
 | Frameskip Threshold (%) | `15` / `18` / `21` / `24` / `27` / `30` / `33` ✅ / `36` / `39` / `42` / `45` / `48` / `51` / `54` / `57` / `60` | `potator_frameskip_threshold` | `15` / `18` / `21` / `24` / `27` / `30` / `33` / `36` / `39` / `42` / `45` / `48` / `51` / `54` / `57` / `60` |


## ![](/emulators/external-links.png) Enlaces externos

* **Código fuente utilizado** : [https://github.com/libretro/potator/](https://github.com/libretro/potator/)