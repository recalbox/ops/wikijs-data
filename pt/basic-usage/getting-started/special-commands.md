---
title: Comandos especiais
description: 
published: true
date: 2021-07-15T23:37:21.945Z
tags: 
editor: markdown
dateCreated: 2021-07-15T23:37:21.945Z
---

Para facilitar suas tarefas em seu Recalbox, integramos vários comandos especiais.

* [No menu](in-menu) 
* [Durante o jogo](in-game)

Você ainda tem a possibilidade de ter uma versão em formato [PDF](pdf-memo) para lembrar a qualquer hora!