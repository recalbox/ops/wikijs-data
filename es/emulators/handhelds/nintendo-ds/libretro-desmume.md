---
title: Libretro DeSmuME
description: 
published: true
date: 2024-07-25T22:38:48.777Z
tags: nintendo, ds, desmume
editor: markdown
dateCreated: 2023-09-11T13:49:52.449Z
---

**Libretro DeSmuME** es un emulador para la Nintendo DS desarrollado en C y C++.

Fue escrito por :

* YopYop156
* Zeromus

## ![](/emulators/license.svg) Licencia

El core posee la siguiente licencia [**GPLv2**](https://github.com/libretro/desmume/blob/master/license.txt).

## ![](/emulators/compatibility.png) Compatibilidad

 | RPI0/RPI1 | RPI3 | RPI4/RPI400 | RPI5 | ODROID XU4 | OGA/OGS/RG351 | PC | RG353 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | 
 | ❌ | ❌ | ❌ | ❌ | ❌ | ❌ | ✅ | ❌ |

## ![](/emulators/features.png) Funcionalidades

| Funcionalidad | ¿Soportada? |
| :---: | :---: |
| Reinicio | ✔ |
| Capturas de pantalla | ✔ |
| Grabaciones | ✔ |
| Grabaciones instantáneas | ✔ |
| Rebobinado | ✔ |
| Netplay | ✔ (no hay Download Play ni cable link o emulación de la Wi-Fi) |
| Opciones del core | ✔ |
| RetroAchievements | ✔ |
| Cheats RetroArch | ✔ |
| Controles | ✔ |
| Remapping | ✔ |
| Username | ✔ |

## ![](/emulators/bios.svg) BIOS

### Lista de BIOS obligatorias

| Nombre del fichero | Descripción | MD5 | ¿Preinstalada? |
| :---: | :---: | :---: | :---: |
| bios7.bin | BIOS ARM7 | df692a80a5b1bc90728bc3dfc76cd948 | ❌ |
| bios9.bin | BIOS ARM9 | a392174eb3e572fed6447e956bde4b25 | ❌ |
| firmware.bin | Firmware NDS | 145eaef5bd3037cbc247c213bb3da1b3 e45033d9b0fa6b0de071292bba7c9d13 3ad72b2c9a736b24953f2d391da4bfcc | ❌ |

### Emplazamiento

Disponed vuestras bios como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 nds
┃ ┃ ┃ ┃ ┣ 🗒 **bios7.bin**
┃ ┃ ┃ ┃ ┣ 🗒 **bios9.bin**
┃ ┃ ┃ ┃ ┣ 🗒 **firmware.bin**

## ![](/emulators/roms.png) Roms

### Extensiones de roms soportadas

Las roms deben tener las extensiones siguientes:

* .nds
* .bin
* .zip
* .7z

Este sistema soporta las roms comprimidas en formato .zip/.7z. No obstante, se trata sólo de un archivo.

Los ficheros contenidos en los .zip/.7z deben tener las extensiones citadas previamente.
Además, cada fichero .zip/.7z sólo debe contener un fichero rom al interior.

### Emplazamiento

Disponed vuestras roms como en la siguiente imagen:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 nds
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.zip**

>Se aconsejan las roms del proyecto **No-Intro**.
{.is-success}

>Para más información sobre las roms, consultad [este tutorial](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuración avanzada del emulador

>Para conservar las configuraciones personalizadas cuando se produce una actualización del sistema Recalbox, os aconsejamos utilizar las [Sobrecargas de configuración](./../../../advanced-usage/configuration-override).
{.is-info}

### Acceder a las opciones

Podéis configurar las opciones del core de dos formas diferentes.

* Via el Menú RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via el fichero `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Opciones del core

| Opción | Valor de la opción | Variable (para la sobrecarga) | Valores posibles |
| --- | --- | --- | --- |
| Firmware Language | `Auto` ✅ / `English` / `Japanese` / `French` / `German` / `Italian` / `Spanish` | `desmume_firmware_language` | `Auto` / `English` / `Japanese` / `French` / `German` / `Italian` / `Spanish` |
| Use External BIOS/Firmware (restart) | `Désactivé` ✅ / `Activé` | `desmume_use_external_bios` | `disabled` / `enabled` |
| Boot Into BIOS (interpreter and external bios only) | `Désactivé` ✅ / `Activé` | `desmume_boot_into_bios` | `disabled` / `enabled` |
| Load Game Into Memory (restart) | `Désactivé` ✅ / `Activé` | `desmume_load_to_memory` | `disabled` / `enabled` |
| CPU Cores | `1` ✅ / `2` / `3` / `4` | `desmume_num_cores` | `1` / `2` / `3` / `4` |
 | CPU Mode (restart) |  `interpreter` ✅ / `jit` | `desmume_cpu_mode` | `interpreter` / `jit` |
| JIT Block Size | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` ✅ / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `21` / `22` / `23` / `24` / `25` / `26` / `27` / `28` / `29` / `30` / `31` / `32` / `33` / `34` / `35` / `36` / `37` / `38` / `39` / `40` / `41` / `42` / `43` / `44` / `45` / `46` / `47` / `48` / `49` / `50` / `51` / `52` / `53` / `54` / `55` / `56` / `57` / `58` / `59` / `60` / `61` / `62` / `63` / `64` / `65` / `66` / `67` / `68` / `69` / `70` / `71` / `72` / `73` / `74` / `75` / `76` / `77` / `78` / `79` / `80` / `81` / `82` / `83` / `84` / `85` / `86` / `87` / `88` / `89` / `90` / `91` / `92` / `93` / `94` / `95` / `96` / `97` / `98` / `99` / `100` | `desmume_jit_block_size` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `21` / `22` / `23` / `24` / `25` / `26` / `27` / `28` / `29` / `30` / `31` / `32` / `33` / `34` / `35` / `36` / `37` / `38` / `39` / `40` / `41` / `42` / `43` / `44` / `45` / `46` / `47` / `48` / `49` / `50` / `51` / `52` / `53` / `54` / `55` / `56` / `57` / `58` / `59` / `60` / `61` / `62` / `63` / `64` / `65` / `66` / `67` / `68` / `69` / `70` / `71` / `72` / `73` / `74` / `75` / `76` / `77` / `78` / `79` / `80` / `81` / `82` / `83` / `84` / `85` / `86` / `87` / `88` / `89` / `90` / `91` / `92` / `93` / `94` / `95` / `96` / `97` / `98` / `99` / `100` |
| Enable Advanced Bus-Level Timing | `Activé` ✅ / `Désactivé` | `desmume_advanced_timing` | `enabled` / `disabled` |
| Frameskip | `0` ✅ / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` | `desmume_frameskip` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` |
| Internal Resolution | `256x192` ✅ / `512x384` / `768x576` / `1024x768` / `1280x960` / `1536x1152` / `1792x1344` / `2048x1536` / `2304x1728` / `2560x1920` | `desmume_internal_resolution` | `256x192` / `512x384` / `768x576` / `1024x768` / `1280x960` / `1536x1152` / `1792x1344` / `2048x1536` / `2304x1728` / `2560x1920` |
| OpenGL Rasterizer (restart) | `Désactivé` ✅ / `Activé` | `desmume_opengl_mode` | `disabled` / `enabled` |
| OpenGL: Color Depth (restart) | `16-bit` ✅ / `32-bit` | `desmume_color_depth` | `16-bit` / `32-bit` |
| OpenGL: Multisampling AA | `Désactivé` ✅ / `2` / `4` / `8` / `16` / `32` | `desmume_gfx_multisampling` | `disabled` / `2` / `4` / `8` / `16` / `32` |
| OpenGL: Texture Smoothing | `Désactivé` ✅ / `Activé` | `desmume_gfx_texture_smoothing` | `disabled` / `enabled` |
| OpenGL: Shadow Polygons | `Activé` ✅ / `Désactivé` | `desmume_opengl_shadow_polygon` | `enabled` / `disabled` |
| OpenGL: Special 0 Alpha | `Activé` ✅ / `Désactivé` | `desmume_opengl_special_zero_alpha` | `enabled` / `disabled` |
| OpenGL: NDS Depth Calculation | `Activé` ✅ / `Désactivé` | `desmume_opengl_nds_depth_calculation` | `enabled` / `disabled` |
| OpenGL: Depth-LEqual Polygon Facing | `Désactivé` ✅ / `Activé` | `desmume_opengl_depth_lequal_polygon_facing` | `disabled` / `enabled` |
| Soft3D: High-res Color Interpolation | `Désactivé` ✅ / `Activé` | `desmume_gfx_highres_interpolate_color` | `disabled` / `enabled` |
| Soft3D: Line Hack | `Activé` ✅ / `Désactivé` | `desmume_gfx_linehack` | `enabled` / `disabled` |
| Soft3D: Texture Hack | `Désactivé` ✅ / `Activé` | `desmume_gfx_txthack` | `disabled` / `enabled` |
| Edge Marking | `Activé` ✅ / `Désactivé` | `desmume_gfx_edgemark` | `enabled` / `disabled` |
| Texture Scaling (xBrz) |  `1` ✅ / `2` / `4` | `desmume_gfx_texture_scaling` | `1` / `2` / `4` |
| Texture Deposterization | `Désactivé` ✅ / `Activé` | `desmume_gfx_texture_deposterize` | `disabled` / `enabled` |
| Screen Layout | `top/bottom` ✅ / `bottom/top` / `left/right` / `right/left` / `top only` / `bottom only` / `hybrid/top` / `hybrid/bottom` | `desmume_screens_layout` | `top/bottom` / `bottom/top` / `left/right` / `right/left` / `top only` / `bottom only` / `hybrid/top` / `hybrid/bottom` |
| Screen Gap | `0` ✅ / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `21` / `22` / `23` / `24` / `25` / `26` / `27` / `28` / `29` / `30` / `31` / `32` / `33` / `34` / `35` / `36` / `37` / `38` / `39` / `40` / `41` / `42` / `43` / `44` / `45` / `46` / `47` / `48` / `49` / `50` / `51` / `52` / `53` / `54` / `55` / `56` / `57` / `58` / `59` / `60` / `61` / `62` / `63` / `64` / `65` / `66` / `67` / `68` / `69` / `70` / `71` / `72` / `73` / `74` / `75` / `76` / `77` / `78` / `79` / `80` / `81` / `82` / `83` / `84` / `85` / `86` / `87` / `88` / `89` / `90` / `91` / `92` / `93` / `94` / `95` / `96` / `97` / `98` / `99` / `100` | `desmume_screens_gap` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `21` / `22` / `23` / `24` / `25` / `26` / `27` / `28` / `29` / `30` / `31` / `32` / `33` / `34` / `35` / `36` / `37` / `38` / `39` / `40` / `41` / `42` / `43` / `44` / `45` / `46` / `47` / `48` / `49` / `50` / `51` / `52` / `53` / `54` / `55` / `56` / `57` / `58` / `59` / `60` / `61` / `62` / `63` / `64` / `65` / `66` / `67` / `68` / `69` / `70` / `71` / `72` / `73` / `74` / `75` / `76` / `77` / `78` / `79` / `80` / `81` / `82` / `83` / `84` / `85` / `86` / `87` / `88` / `89` / `90` / `91` / `92` / `93` / `94` / `95` / `96` / `97` / `98` / `99` / `100` |
| Hybrid Layout: Ratio | `3:1` ✅ / `2:1` | `desmume_hybrid_layout_ratio` | `3:1` / `2:1` |
| Hybrid Layout: Scale So Small Screen is 1:1px | `Désactivé` ✅ / `Activé` | `desmume_hybrid_layout_scale` | `disabled` / `enabled` |
| Hybrid Layout: Show Both Screens | `Activé` ✅ / `Désactivé` | `desmume_hybrid_showboth_screens` | `enabled` / `disabled` |
| Hybrid Layout: Cursor Always on Small Screen | `Activé` ✅ / `Désactivé` | `desmume_hybrid_cursor_always_smallscreen` | `enabled` / `disabled` |
| Mouse/Pointer | `Activé` ✅ / `Désactivé` | `desmume_pointer_mouse` | `enabled` / `disabled` |
| Pointer Type | `mouse` ✅ / `touch` | `desmume_pointer_type` | `mouse` / `touch` |
| Mouse Speed | `0.01` / `0.02` / `0.03` / `0.04` / `0.05` / `0.125` / `0.25` / `0.5` / `1.0` ✅ / `1.5` / `2.0` | `desmume_mouse_speed` | `0.01` / `0.02` / `0.03` / `0.04` / `0.05` / `0.125` / `0.25` / `0.5` / `1.0` / `1.5` / `2.0` |
| Pointer Rotation | `0` ✅ / `90` / `180` / `270` | `desmume_input_rotation` | `0` / `90` / `180` / `270` |
| Pointer Mode for Left Analog | `none` ✅ / `emulated` / `absolute` / `pressed` | `desmume_pointer_device_l` | `none` / `emulated` / `absolute` / `pressed` |
| Pointer Mode for Right Analog | `none` ✅ / `emulated` / `absolute` / `pressed` | `desmume_pointer_device_r` | `none` / `emulated` / `absolute` / `pressed` |
| Emulated Pointer Deadzone Percent | `0` / `5` / `10` / `15` ✅ / `20` / `25` / `30` / `35` | `desmume_pointer_device_deadzone` | `0` / `5` / `10` / `15` / `20` / `25` / `30` / `35` |
| Emulated Pointer Acceleration Modifier Percent | `0` ✅ / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `21` / `22` / `23` / `24` / `25` / `26` / `27` / `28` / `29` / `30` / `31` / `32` / `33` / `34` / `35` / `36` / `37` / `38` / `39` / `40` / `41` / `42` / `43` / `44` / `45` / `46` / `47` / `48` / `49` / `50` / `51` / `52` / `53` / `54` / `55` / `56` / `57` / `58` / `59` / `60` / `61` / `62` / `63` / `64` / `65` / `66` / `67` / `68` / `69` / `70` / `71` / `72` / `73` / `74` / `75` / `76` / `77` / `78` / `79` / `80` / `81` / `82` / `83` / `84` / `85` / `86` / `87` / `88` / `89` / `90` / `91` / `92` / `93` / `94` / `95` / `96` / `97` / `98` / `99` / `100` | `desmume_pointer_device_acceleration_mode` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `21` / `22` / `23` / `24` / `25` / `26` / `27` / `28` / `29` / `30` / `31` / `32` / `33` / `34` / `35` / `36` / `37` / `38` / `39` / `40` / `41` / `42` / `43` / `44` / `45` / `46` / `47` / `48` / `49` / `50` / `51` / `52` / `53` / `54` / `55` / `56` / `57` / `58` / `59` / `60` / `61` / `62` / `63` / `64` / `65` / `66` / `67` / `68` / `69` / `70` / `71` / `72` / `73` / `74` / `75` / `76` / `77` / `78` / `79` / `80` / `81` / `82` / `83` / `84` / `85` / `86` / `87` / `88` / `89` / `90` / `91` / `92` / `93` / `94` / `95` / `96` / `97` / `98` / `99` / `100` |
| Emulated Stylus Pressure Modifier Percent | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `21` / `22` / `23` / `24` / `25` / `26` / `27` / `28` / `29` / `30` / `31` / `32` / `33` / `34` / `35` / `36` / `37` / `38` / `39` / `40` / `41` / `42` / `43` / `44` / `45` / `46` / `47` / `48` / `49` / `50` ✅ / `51` / `52` / `53` / `54` / `55` / `56` / `57` / `58` / `59` / `60` / `61` / `62` / `63` / `64` / `65` / `66` / `67` / `68` / `69` / `70` / `71` / `72` / `73` / `74` / `75` / `76` / `77` / `78` / `79` / `80` / `81` / `82` / `83` / `84` / `85` / `86` / `87` / `88` / `89` / `90` / `91` / `92` / `93` / `94` / `95` / `96` / `97` / `98` / `99` / `100` | `desmume_pointer_stylus_pressure` | `0` / `1` / `2` / `3` / `4` / `5` / `6` / `7` / `8` / `9` / `10` / `11` / `12` / `13` / `14` / `15` / `16` / `17` / `18` / `19` / `20` / `21` / `22` / `23` / `24` / `25` / `26` / `27` / `28` / `29` / `30` / `31` / `32` / `33` / `34` / `35` / `36` / `37` / `38` / `39` / `40` / `41` / `42` / `43` / `44` / `45` / `46` / `47` / `48` / `49` / `50` / `51` / `52` / `53` / `54` / `55` / `56` / `57` / `58` / `59` / `60` / `61` / `62` / `63` / `64` / `65` / `66` / `67` / `68` / `69` / `70` / `71` / `72` / `73` / `74` / `75` / `76` / `77` / `78` / `79` / `80` / `81` / `82` / `83` / `84` / `85` / `86` / `87` / `88` / `89` / `90` / `91` / `92` / `93` / `94` / `95` / `96` / `97` / `98` / `99` / `100` |
| Pointer Colour | `white` ✅ / `black` / `red` / `blue` / `yellow` | `desmume_pointer_colour` | `white` / `black` / `red` / `blue` / `yellow` |
| Microphone Button Noise Type | `pattern` ✅ / `random` | `desmume_mic_mode` | `pattern` / `random` |

## ![](/emulators/external-links.png) Enlaces externos

* **Código fuente utilizado** : [https://github.com/libretro/desmume/](https://github.com/libretro/desmume/)
* **Documentación Libretro** : [https://docs.libretro.com/library/desmume/](https://docs.libretro.com/library/desmume/)
* **Código fuente oficial** : [https://github.com/TASVideos/desmume](https://github.com/TASVideos/desmume/)
* **Documentación oficial** : [https://desmume.org/documentation/](https://desmume.org/documentation/)
* **Página web oficial** : [https://desmume.org/](https://desmume.org/)
* **Foro oficial** : [http://forums.desmume.org/](http://forums.desmume.org/)