---
title: Apple 2
description: 
published: true
date: 2024-07-26T12:45:24.351Z
tags: apple, apple-2, ordenadores
editor: markdown
dateCreated: 2023-09-02T18:34:03.665Z
---

![](/emulators/computers/apple2.svg){.align-center}

## Especificaciones técnicas

* **Fabricante**: Apple
* **Año de lanzamiento**: 1977
* **Sistema operativo**: Integer BASIC
* **Procesador**: MOS Technology 6502 a 1 MHz
* **RAM**: 4KB a 64KB
* **ROM**: 8KB (Basic Integer)
* **Chip de sonido**: Altavoz de 1 bit (integrado)
* **Resolución** :
  * Baja resolución: 40x48, 16 colores
  * Alta resolución: 280x192, 6 colores

## Presentación

El **Apple II** (a veces escrito **Apple ]\[** o **Apple //**) es uno de los primeros ordenadores personales fabricados en serie. Diseñado por Steve Wozniak y lanzado el 10 de junio de 1977 por Apple, comenzó su andadura en el mercado de la informática doméstica, pero el lanzamiento del primer programa de hoja de cálculo, VisiCalc, en 1979, propició su entrada en el mundo profesional y un enorme aumento de las ventas, lo que hizo a Apple repentinamente rica en aquella época.

Los Apple II evolucionaron a lo largo de la década de 1980 y se vendieron hasta 1993. El Apple II tenía una arquitectura predominantemente de 8 bits, y difería completamente del Apple Macintosh introducido en 1984. La parte "II" del nombre se escribía sucesivamente con multitud de símbolos gráficos y de puntuación. El II y el II+ se escribían generalmente como "]\[" y "]\[+", mientras que el IIe y el IIc se escribían como "//e" y "//c", tanto en los manuales como en las propias máquinas.

Es el sucesor del Apple I, que era una máquina casera vendida a aficionados. Esta máquina nunca se produjo en cantidad, pero allanó el camino para muchas de las características que hicieron del Apple II un éxito.

## Emuladores

[GSPlus](gsplus)
[LinApple](linapple)