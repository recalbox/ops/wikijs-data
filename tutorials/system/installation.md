---
title: Installation
description: 
published: true
date: 2024-09-18T16:07:13.610Z
tags: installation
editor: markdown
dateCreated: 2021-08-06T08:08:25.470Z
---

This part concerns you if you want to install in a bartop or to use a USB device as a storage place, for example.

Here are the available tutorials:

[Bartop / Arcade configuration](bartop-arcade-configuration)
[Use NVMe device as storage](nvme-device-as-storage)
[Use USB device as storage](usb-device-as-storage)