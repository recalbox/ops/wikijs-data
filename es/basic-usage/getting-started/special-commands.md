---
title: Comandos especiales
description: 
published: true
date: 2023-10-14T20:48:05.084Z
tags: comandos especiales
editor: markdown
dateCreated: 2023-09-03T13:14:50.615Z
---

Para simplicar ciertas acciones del sistema, hemos integrado varias órdenes especiales, aquí encontraréis sus atajos para poder lanzarlas.

* [Dentro del menú](in-menu) 
* [Dentro del juego](in-game)

También hemos creado una pequeña infografía muy útil para recordaros todas las posiblidades de un sólo vistazo en formato [PDF](pdf-memo).