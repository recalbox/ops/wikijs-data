---
title: Amstrad CPC
description: 
published: true
date: 2024-07-26T12:45:04.732Z
tags: amstrad, cpc, ordenadores
editor: markdown
dateCreated: 2023-09-02T18:32:13.898Z
---

![](/emulators/computers/amstradcpc.svg){.align-center}

## Datos técnicos

* **Fabricante**: Amstrad
* **Año de lanzamiento**: 1984
* **Sistema operativo**: AMSDOS con BASIC Locomotive
* **Procesador**: Zilog Z80A a 4 MHz
* **RAM**: 64 KB
* **Chip de sonido**: General Instrument AY-3-8910

## Presentación

El **Amstrad CPC** es un ordenador personal de 8 bits producido por Amstrad en la década de 1980. CPC son las siglas de Colour Personal Computer (ordenador personal en color), aunque también existía una versión con monitor monocromo.

El Amstrad CPC vendió alrededor de tres millones de unidades en todo el mundo, incluyendo alrededor de un millón en Francia.

## Emuladores

[Libretro Cap32](libretro-cap32)
[Libretro CrocoDS](libretro-crocods)