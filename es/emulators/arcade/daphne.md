---
title: Daphne
description: LaserDisc
published: true
date: 2024-07-26T07:36:08.041Z
tags: arcade, daphne, laserdisc
editor: markdown
dateCreated: 2023-09-01T21:03:22.278Z
---

![](/emulators/arcade/daphne.svg){.align-center}

## Detalles técnicos

* **Desarrolladores:** Matt Ownby (Daphne) y DirtBagXon (Hypseus)
* **Año de lanzamiento:** 1999
* **Página Web :** [http://www.daphne-emu.com/](http://www.daphne-emu.com/site3/index_hi.php) en C++
* **Daphne** es un programa que permite jugar a las versiones originales de muchos de los juegos arcade originales de LaserDisc.

## Presentación

El **LaserDisc** fue el primer soporte de almacenamiento óptico, inicialmente de vídeo, que se comercializó en 1978 en Norteamérica, inicialmente con el nombre de MCA DiscoVision.

Aunque ofrecía mejor calidad de sonido e imagen que los soportes contemporáneos (en particular, los casetes VHS y Betamax), el LaserDisc tuvo poco éxito, principalmente por el elevado precio de los reproductores y el hecho de que no podía grabar programas de televisión. Aunque desde el principio fue muy popular entre los propietarios de salas de cine en casa, sólo en Asia (Hong Kong, Malasia y Singapur), en la década de 1990, el LaserDisc se extendió realmente a los hogares.

No obstante, a partir de la tecnología del LaserDisc se desarrollaron varios soportes de almacenamiento óptico, en particular los CD y DVD, que han tenido un éxito considerable.

### ¿Qué es un juego LaserDisc?

Un **videojuego LaserDisc** es un juego que **utiliza vídeos pregrabados** (películas o animaciones) **reproducidos a partir de un LaserDisc**, ya sea para **todo** o para **parte de los gráficos**.
**Algunos juegos utilizan vídeos sobre los que se superponen gráficos (coches, barcos, etc.).

Pero los **juegos más populares** eran **películas o dibujos animados interactivos**, en los que **el jugador** tenía que **presionar un botón específico** o **mover el joystick** en la dirección correcta en el momento adecuado para **pasar a la siguiente escena.**

### ¿Qué es DAPHNE?

DAPHNE es un emulador, o más bien una 'emuladora' (porque ha sido diseñado como una mujer o "al femenino": _First Ever Multiple Arcade Laserdisc Emulator_) que permite jugar a **juegos arcade LaserDisc** (de la marca LaserDisc), como **Dragon's Lair** (del que el emulador toma su nombre, basado en el de la princesa del juego), **Badlands**, **Cobra Command**, **Space Ace** y muchos otros.

Fue creado por **Matt Ownby** en 1999 en un emulador de procesador Z80. El primer juego emulado fue Dragon's Lair, ¡el más famoso! Con otros colegas y amigos, consiguió emular casi una veintena de juegos. Sin embargo, dejó de publicar y actualizar en 2009 por frustración con la comunidad de emuladores, que esperaba una gestión profesional de un proyecto que era básicamente "por diversión".

Actualmente, Matt Ownby está trabajando en una reescritura de DAPHNE, con un modelo de negocio real definido para poder ofrecer un soporte de calidad comercial a sus usuarios.

## Emuladores

[Hypseus](hipseus)
[Libretro DirkSimple](libretro-dirksimple)
