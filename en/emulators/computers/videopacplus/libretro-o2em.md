---
title: Libretro O2EM
description: 
published: true
date: 2021-10-19T06:22:25.193Z
tags: libretro, videopac+, o2em
editor: markdown
dateCreated: 2021-07-29T17:58:07.889Z
---



## ![](/emulators/license.svg) License

This core is under [**artistic**](https://sourceforge.net/projects/o2em/) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Features



## ![](/emulators/bios.svg) BIOS

### List of mandatory bios

| Filename | Description | MD5 | Provided |
| :---: | :---: | :---: | :---: |
| g7400.bin | European Videopac+ - G7400 model | c500ff71236068e0dc0d0603d265ae76 | ❌ |
| jopac.bin | French Videopac+ - G7400 model | 279008e4a0db2dc5f1c048853b033828 | ❌ |

### Location

Put the bios like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 🗒 **g7400.bin**
┃ ┃ ┃ ┣ 🗒 **jopac.bin**

## ![](/emulators/roms.png) Roms

### Supported extensions

Roms must have the extension:

* .bin
* .zip
* .7z

This system supports compressed roms in .zip/.7z format. However, be careful, it is only an archive.

The files contained in the .zip/.7z must correspond to the extensions mentioned above.
Moreover, each .zip/.7z file must contain only one compressed rom.

### Location

Put the roms like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 videopacplus
┃ ┃ ┃ ┃ ┣ 🗒 **game.zip**

>The roms using **No-Intro** format are highly recommended.
{.is-success}

>For more information about roms, go to [this tutorial](./../../../tutorials/games/generalities/isos-and-roms)!
{.is-info}

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Accessing to options

You can configure various options in two different ways.

* Via RetroArch menu:

┣ 📁 RetroArch Menu
┃ ┣ 📁 Core options
┃ ┃ ┣ 🧩 Name_option

* Via the file `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core options

| Options | Options values | Variables (for configuration override) | Variables values |
| --- | --- | --- | --- |
| Emulated Hardware (Restart) | `Odyssey 2 (NTSC)` ✅ / `Videopac G7000 (European)` / `Videopac+ G7400 (European)` / `Videopac+ G7400 (French)` | `o2em_bios` | `o2rom.bin` / `c52.bin` / `g7400` / `jopac.bin` |
| Console Region (Restart) | `Auto` ✅ / `NTSC` / `PAL` | `o2em_region` | `auto` / `NTSC` / `PAL` |
| Swap Gamepads | `Disabled` ✅ / `Enabled` | `o2em_swap_gamepads` | `disabled` / `enabled` |
| Virtual KBD Transparency | `0%` ✅ / `25%` / `50%` / `75%` | `o2em_vkbd_transparency` | `0` / `25` / `50` / `75` |
| Crop Overscan | `Disabled` ✅ / `Enabled` | `o2em_crop_overscan` | `disabled` / `enabled` |
| Interframe Blending | `Disabled` ✅ / `Simple` / `Ghosting (65%)` / `Ghosting (75%)` / `Ghosting (85%)` / `Ghosting (95%)` | `o2em_mix_frames` | `disabled` / `mix` / `ghost_65` / `ghost_75` / `ghost_85` / `ghost_95` |
| Audio Volume | `0%` / `10%` / `20%` / `30%` / `40%` / `50%` ✅ / `60%` / `70%` / `80%` / `90%` / `100%` | `o2em_audio_volume` | `0` / `10` / `20` / `30` / `40` / `50` / `60` / `70` / `80` / `90` / `100` |
| Audio Filter | `Disabled` ✅ / `Enabled` | `o2em_low_pass_filter` | `disabled` / `enabled` |
| Audio Filter Level | `5%` / `10%` / `15%` / `20%` / `25%` / `30%` / `35%` / `40%` / `45%` / `50%` / `55%` / `60%` ✅ / `65%` / `70%` / `75%` / `80%` / `85%` / `90%` / `95%` | `o2em_low_pass_range` | `5` / `10` / `15` / `20` / `25` / `30` / `35` / `40` / `45` / `50` / `55` / `60` / `65` / `70` / `75` / `80` / `85` / `90` / `95` |

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/libretro/libretro-o2em/](https://github.com/libretro/libretro-o2em/)
* **Libretro documentation**: [https://docs.libretro.com/library/o2em/](https://docs.libretro.com/library/o2em/)