---
title: Jouer avec votre smartphone en utilisant la manette virtuelle
description: 
published: true
date: 2021-09-04T22:47:26.767Z
tags: smartphone, manette, virtuelle
editor: markdown
dateCreated: 2021-05-21T08:35:16.996Z
---

[Le projet de Miroof - Virtual Gamepads](https://github.com/miroof/node-virtual-gamepads) apporte une nouvelle fonctionnalité à Recalbox : jouer aux jeux avec votre smartphone comme manette !

Vous pouvez ajouter jusqu'à 4 manettes à la fois.

* Quand vous connectez votre Recalbox au réseau, vous n'avez qu'à obtenir votre [adresse IP](./../../../tutorials/network/ip/discover-recalbox-ip) puis à connecter votre smartphone sur le même réseau wifi que votre Recalbox et d'ouvrir votre navigateur web sur l'adresse IP trouvée avec le port 8080 (vous pouvez consulter [ce tutoriel](./../../../tutorials/system/access/recalbox-manager-web-interface) pour savoir comment s'y prendre)..
* Une nouvelle manette est alors ajoutée dans Recalbox.

Vous pouvez alors soit démarrer un jeu ou aller dans les réglages des manettes pour affecter cette nouvelle manette à un joueur.

![Manette virtuelle](/tutorials/controllers/controllers/virtualgamepad.png)

Pour plus d'informations sur la fonctionnalité, vous pouvez consulter la source [ici](https://github.com/jehervy/node-virtual-gamepads#features).