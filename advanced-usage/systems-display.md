---
title: Systems display
description: How it works and displayed systems edit.
published: true
date: 2023-03-05T11:23:57.493Z
tags: emulationstation, display, systems
editor: markdown
dateCreated: 2021-06-29T12:51:24.737Z
---

## How the display works

The display of systems in the Recalbox menu is handled by the `systemlist.xml` file. This file is located in the `/recalbox/share_init/system/.emulationstation/systemlist.xml` folder.

It contains the names of the different systems that are supported by your version of Recalbox. It is constructed as follows:

```xml
<?xml version="1.0"?>
<systemList>
  <defaults command="python /usr/bin/emulatorlauncher.pyc %CONTROLLERSCONFIG% -system %SYSTEM% -rom %ROM% -emulator %EMULATOR% -core %CORE% -ratio %RATIO% %NETPLAY%"/>
  <system uuid="b58b9072-0e44-4330-869d-96b72f53de1f" name="3do" fullname="Panasonic 3DO" platforms="3do">
    <descriptor path="%ROOT%/3do" theme="3do" extensions=".iso .chd .cue" icon="$F28d"/>
    <scraper screenscraper="29"/>
    <properties type="console" pad="mandatory" keyboard="no" mouse="no" lightgun="optional" releasedate="1993-10" retroachievements="1"/>
    <emulatorList>
      <emulator name="libretro">
        <core name="opera" priority="1" extensions=".chd .cue .iso" netplay="0" compatibility="high" speed="high"/>
      </emulator>
    </emulatorList>
  </system>
  <system>
      [. . .]
  </system>
  [. . .]
</systemList>
```

The display of the systems respects the order in which they are listed in this file. It also contains the configuration of these systems.

## Change the display order

>Do **NOT** change the original `systemlist.xml` file (which is in the `share_init` directory). In case of problems with changes made later, this file remains the only source for getting Recalbox to work properly.
{.is-warning}

Changing the display order should only be done from the `systemlist.xml` file in the ` /recalbox/share/system/.emulationstation/systemlist.xml` directory.

Originally, this file does not exist. You must either copy the original file or create a new file. Once the new file is created, it is possible to put the systems in the order you want. The configuration of the systems will still be taken from the original `systemlist.xml` file, but the order of the systems will be as defined in the new file.

If, in the new file, a system is missing or incorrectly filled in, priority is given to the original file. For the new file, there are only 1 key entry that is mandatory: "**UUID**", all others are optional. The file should be constructed at least in the following way:

```xml
<?xml version="1.0"?>
<systemList>
  <system uuid="62f2cbed-5bcb-46d8-bca9-daaa36613c7a"/> <!-- nes -->
  <system uuid="0a7fd1d3-9673-44ab-bfef-5bb3c8a0d79a"/> <!-- fds -->
  <system uuid="3df92492-d69a-48f8-8e14-9a62bd9805a6"/> <!-- snes -->
  <system uuid="99e312bc-7602-4038-a871-e53c17fd1d76"/> <!-- satellaview -->
  [. . .]
</systemList>
```

## Add a custom system

>You must **NOT MODIFY** the original `systemlist.xml` file (which is in the `share_init` directory). If there are any problems with the changes you make later, this file is the only source to make Recalbox work properly.
{.is-warning}

This manipulation does not add a new emulator to Recalbox but adds a new system entry to the selection menu.

It is possible to combine the modification of the system order with the addition of one or more custom systems.

As with changing the system order, adding a custom system must be done only from the `systemlist.xml` file in the following directory `/recalbox/share/system/.emulationstation/systemlist.xml`.

Originally, this file does not exist. You must either copy the original file or create a new file. Once the new file is created, it is now possible to add a new system to it.

If, in the new file, a system is incorrectly filled in, priority is given to the original file. For the new file, all the input keys are mandatory. So, to create a new system, the easiest way is to start from an existing system (and corresponding to the roms you want to include) and to modify only what is strictly necessary:

- _**"fullname"**_ : Allows to give the name of the new system.
- _**"path"**_ : Allows to indicate the directory containing the roms of the new system.
- _**"theme"**_ : Allows you to indicate which theme to use. It is necessary, beforehand, to create this new theme (logo, background, ...)

**All the other entries must not be modified.**

Here is an example of an addition for a system based on the SNES to include only translated roms:

```xml
<?xml version="1.0"?>
<systemList>
  <system uuid="21b8873a-a93e-409c-ad0c-8bb6d682bef8" name="snes" fullname="Super Nintendo Fan Trad" platform="snes">
    <descriptor path="%ROOT%/snestrad" theme="snestrad" extensions=".smc .sfc .mgd .zip .7z" icon="$0"/>
    <scraper screenscraper=""/>
    <properties type="console" pad="mandatory" keyboard="no" mouse="optional" lightgun="optional" releasedate="1990-11" retroachievements="1"/>
    <emulatorList>
      <emulator name="libretro">
        <core>snes9x2005</core>
        <core>snes9x2010</core>
        <core>snes9x2002</core>
      </emulator>
    </emulatorList>
  </system>
</systemList>
```