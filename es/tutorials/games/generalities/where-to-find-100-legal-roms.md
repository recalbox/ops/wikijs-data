---
title: Dónde encontrar roms 100% legales
description: 
published: true
date: 2023-12-21T18:27:39.657Z
tags: roms, legales, buscar
editor: markdown
dateCreated: 2023-09-08T09:51:53.729Z
---

## Introducción

El propósito de esta página es listar las distintas formas "100% legales" de obtener roms para las distintas plataformas soportadas por Recalbox.

## Atari2600

* En Steam, las roms de Atari 2600 se pueden extraer del juego **Atari Vault**. Hay 100 juegos de Atari 2600 en este pack, que cuesta 9,99 euros. Aquí está el [enlace a la página de Steam del juego](https://store.steampowered.com/app/400020/Atari_Vault/).

## Daphne

* Algunos juegos pueden descargarse directa y legalmente utilizando la herramienta **DaphneLoader** de la distribución Daphne para Windows.
* Para otros (Dragon's Lair, Dragon's Lair II, Space Ace, ...), debes demostrar que tienes una licencia válida para el juego (versión DVD por ejemplo).

## DOS

* En GOG.com (Good Old Games, una plataforma de descarga de juegos de PC sin DRM), es posible comprar juegos MS-DOS como Disney's Aladdin, Disney's Lion King, Disney's Jungle Book ...  Estos juegos se ejecutan en `DOSbox` y por lo tanto son compatibles con Recalbox.
* En el sitio 'Legacy' de 3D Realms (antes Apogee), puedes descargar algunos de sus títulos más antiguos. La mayoría están disponibles como freeware y algunos sólo como shareware. Hay algunos títulos bastante interesantes como **Alien Carnage (Halloween Harry)**. La lista de juegos descargables se puede encontrar [aquí](http://legacy.3drealms.com/downloads.html). Estos juegos funcionan con `DOSbox` y por lo tanto son compatibles con Recalbox. 
* Blizzard ofrece el juego **The Lost Vikings (USA)** como descarga gratuita desde [su sitio](https://us.battle.net/account/download/?show=classic).

## Master System

* En Steam, puedes comprar el remake **Wonder Boy: The Dragon's trap**. Una vez instalado, el directorio del juego contiene una subcarpeta `bin_pc`, luego otra subcarpeta `rom`, esta última contiene un archivo `wb3.sms` que es una rom correspondiente al juego de Master System **Wonder boy III: The Dragon's Trap** (no corresponde, sin embargo, a un juego referenciado por No-intro).

## Megadrive

* En Steam, puedes comprar el juego **SEGA Mega Drive & Genesis Classics**. Durante la instalación, se crea un directorio `uncompressed roms`. Todo lo que tienes que hacer es cambiar la extensión de las roms a .md o .bin para que sean reconocidas por Recalbox. El juego cuesta 29,99 euros y contiene 58 títulos de Megadrive + Sonic CD. La lista de juegos incluidos está disponible [en la página de Steam del juego](https://store.steampowered.com/sub/102625/?l=french). De las 67 roms incluidas, 61 corresponden a roms referenciadas por No-intro.
* También hay juegos homebrew creados por el Shiru y disponibles para descarga gratuita en [su página web](https://shiru.untergrund.net/software.shtml#genesis).

## NeoGeo

* En GOG.com (Good Old Games, una plataforma de descarga de juegos para PC sin DRM), puedes comprar **24 juegos de Neo Geo por 5,19 euros cada título**. Aquí está el [enlace a todos los títulos de SNK disponibles en GOG.com](https://www.gog.com/games?sort=popularity&search=snk&page=1). 
* En Humble Bundle, puedes conseguir una compilación de 23 juegos por **34,99 euros**. Los juegos también están libres de DRM. La lista de juegos disponibles está en la [página del pack](https://www.humblebundle.com/store/neogeo-classic-complete-collection). También es posible comprar los títulos **individualmente por 5,99 euros**. El pack contiene los mismos juegos que en GOG.com, a excepción de The King of Fighters '98, que debe adquirirse por separado con el pack "Unlimited Match".

## NES

* Hay juegos homebrew creados por el Shiru y disponibles como descargas gratuitas en [su sitio](https://shiru.untergrund.net/software.shtml#snes).
* También puedes comprar bundles de Capcom en Steam (directamente o a través de Humble Bundle).

De estos paquetes, puedes extraer las roms .nes utilizando la utilidad [romextract](https://gitlab.com/vaiski/romextract/tree/master). Entre los bundles ofrecidos por Capcom de los que se pueden extraer roms de NES están [Disney Afternoon Collection](https://www.humblebundle.com/store/the-disney-afternoon-collection) y [Megaman Legacy Collection (sólo el primer pack)](https://www.humblebundle.com/store/mega-man-legacy-collection).

## ScummVM

* Hay juegos gratuitos disponibles para descargar en el sitio web [ScummVM](https://www.scummvm.org/games/).

## SNES

* Los juegos homebrew creados por Shiru pueden descargarse gratuitamente desde [su sitio](https://shiru.untergrund.net/software.shtml#nes).
* Blizzard ofrece una descarga gratuita de la rom Rock 'N Roll Racing (USA) en [su página web](https://us.battle.net/account/download/?show=classic).
* En Steam, el editor [Piko Interactive](https://store.steampowered.com/publisher/Piko) ofrece varios títulos del catálogo de SNES. Estos juegos vienen en forma de emulador con la rom del juego. Estas roms también están disponibles en [bundle](https://store.steampowered.com/bundle/5256/RETRO_Action_SNES_Volume_1/).
* Al igual que con la NES, Capcom ha lanzado el bundle [Megaman X Legacy Collection](https://www.humblebundle.com/store/mega-man-x-legacy-collection), que puede adquirirse en Steam (directamente o a través de Humble Bundle) y contiene las roms de SNES. Estas roms pueden extraerse utilizando la utilidad [romextract](https://gitlab.com/vaiski/romextract/tree/master). Sólo se pueden extraer las roms del primer paquete.

