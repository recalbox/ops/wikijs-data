---
title: Solarus
description: Arpg Game Engine
published: true
date: 2024-07-26T08:24:44.065Z
tags: solarus, motor de juego
editor: markdown
dateCreated: 2023-09-04T07:16:19.383Z
---

![](/emulators/fantasy/solarus.svg){.align-center}

## Datos técnicos

* **Fabricante :** Solarus
* **Fecha de lanzamiento :** 19 de septiembre de 2020

## Presentación

**Solarus** es un motor de juego 2D de Acción-RPG ligero, libre y de código abierto diseñado específicamente pensando en los Action-RPG de culto de la era 2D, como **The Legend of Zelda: A Link to the Past** y **Secret of Mana** en Super Nintendo, o **Soleil** en Sega Megadrive/Genesis.

## Emuladores

[Solarus](solarus)