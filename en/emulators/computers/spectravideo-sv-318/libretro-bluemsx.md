---
title: Libretro blueMSX
description: 
published: true
date: 2021-10-07T17:31:58.591Z
tags: libretro, bluemsx, sv-318
editor: markdown
dateCreated: 2021-07-29T17:41:56.471Z
---



## ![](/emulators/license.svg) License

This core is under [**GPLv2**](https://github.com/libretro/blueMSX-libretro/blob/master/license.txt) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Features



## ![](/emulators/bios.svg) BIOS

### List of mandatory bios

| Filename | Description | MD5 | Provided |
| :---: | :---: | :---: | :---: |
| svi318.rom | - | ee6ad7ea29e791b03a28a9443e622648 | ✅ |
| svi328a.rom | - | 352f054ab09605070bdff49d73f335cc | ✅ |
| svi806.rom | - | 82f7bc9c08f43db1d79bcb565a0de12b | ✅ |
| svi328a.rom | - | 352f054ab09605070bdff49d73f335cc | ✅ |
| svi806se.rom | - | 6a5536c1eb4f0477c4f76488cd8ca3ad | ✅ |
| svi328a.rom | - | 352f054ab09605070bdff49d73f335cc | ✅ |
| svi806.rom | - | 82f7bc9c08f43db1d79bcb565a0de12b | ✅ |
| svi328.rom | - | 2cebd1db9dda475a011b7bce65c984a2 | ✅ |

### Location

Put the bios like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 Machines
┃ ┃ ┃ ┃ ┣ 📁 SVI - Spectravideo SVI-318
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **svi318.rom**
┃ ┃ ┃ ┃ ┣ 📁 SVI - Spectravideo SVI-328 80 Column
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **svi328a.rom**
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **svi806.rom**
┃ ┃ ┃ ┃ ┣ 📁 SVI - Spectravideo SVI-328 80 Swedish
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **svi328a.rom**
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **svi806se.rom**
┃ ┃ ┃ ┃ ┣ 📁 SVI - Spectravideo SVI-328 MK2
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **svi328a.rom**
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **svi806.rom**
┃ ┃ ┃ ┃ ┣ 📁 SVI - Spectravideo SVI-328
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **svi328.rom**

## ![](/emulators/roms.png) Roms

### Supported extensions

Roms must have the extension:

* .cas
* .bin
* .zip

This system supports compressed roms in .zip/.7z format. However, be careful, it is only an archive.

The files contained in the .zip/.7z must correspond to the extensions mentioned above.
Moreover, each .zip/.7z file must contain only one compressed rom.

### Location

Put the roms like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 spectravideo
┃ ┃ ┃ ┃ ┣ 🗒 **game.zip**

>The roms using **TOSEC** format are highly recommended.
{.is-success}

>For more information about roms, go to [this tutorial](./../../../tutorials/games/generalities/isos-and-roms)!
{.is-info}

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Accessing to options

You can configure various options in two different ways.

* Via RetroArch menu:

┣ 📁 RetroArch Menu
┃ ┣ 📁 Core options
┃ ┃ ┣ 🧩 Name_option

* Via the file `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core options

| Options | Options values | Variables (for configuration override) | Variables values |
| --- | --- | --- | --- |
| Machine Type (Restart) | `Auto` ✅ / `MSX` / `MSXturboR` / `MSX2` / `MSX2+` / `SEGA - SG-1000` / `SEGA - SC-3000` / `SEGA - SF-7000` / `SVI - Spectravideo SVI-318` / `SVI - Spectravideo SVI-328` / `SVI - Spectravideo SVI-328 MK2` / `ColecoVision` / `Coleco (Spectravideo SVI-603)` | `bluemsx_msxtype` | `Auto` / `MSX` / `MSXturboR` / `MSX2` / `MSX2+` / `SEGA - SG-1000` / `SEGA - SC-3000` / `SEGA - SF-7000` / `SVI - Spectravideo SVI-318` / `SVI - Spectravideo SVI-328` / `SVI - Spectravideo SVI-328 MK2` / `ColecoVision` / `Coleco (Spectravideo SVI-603)` |
| Crop Overscan | `Disabled` ✅ / `Enabled` / `MSX2` | `bluemsx_overscan` | `disabled` / `enabled` / `MSX2` |
| VDP Sync Type (Restart) | `Auto` / `50Hz` / `60Hz` ✅ | `bluemsx_vdp_synctype` | `auto` / `50 Hz` / `60 Hz` |
| No Sprite Limit | `Disabled` ✅ / `Enabled` | `bluemsx_nospritelimits` | `disabled` / `enabled` |
| Sound YM2413 Enable (Restart) | `Enabled` ✅ / `Disabled` | `bluemsx_ym2413_enable` | `enabled` / `disabled` |
| Cart Mapper Type (Restart) | `Auto` ✅ / `Normal` / `mirrored` / `basic` / `0x4000` / `0xC000` / `ascii8` / `ascii8sram` / `ascii16` / `ascii16sram` / `ascii16nf` / `konami4` / `konami4nf` / `konami5` / `konamisynth` / `korean80` / `korean90` / `korean126` / `MegaFlashRomScc` / `MegaFlashRomSccPlus` / `msxdos2` / `scc` / `sccexpanded` / `sccmirrored` / `sccplus` / `snatcher` / `sdsnatcher` / `SegaBasic` / `SG1000` / `SG1000Castle` / `SG1000RamA` / `SG1000RamB` / `SC3000` | `bluemsx_cartmapper` | `Auto` / `Normal` / `mirrored` / `basic` / `0x4000` / `0xC000` / `ascii8` / `ascii8sram` / `ascii16` / `ascii16sram` / `ascii16nf` / `konami4` / `konami4nf` / `konami5` / `konamisynth` / `korean80` / `korean90` / `korean126` / `MegaFlashRomScc` / `MegaFlashRomSccPlus` / `msxdos2` / `scc` / `sccexpanded` / `sccmirrored` / `sccplus` / `snatcher` / `sdsnatcher` / `SegaBasic` / `SG1000` / `SG1000Castle` / `SG1000RamA` / `SG1000RamB` / `SC3000` |
| Auto Rewind Cassette | `Enabled` ✅ / `Disabled` | `bluemsx_auto_rewind_cas` | `enabled` / `disabled` |

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/libretro/blueMSX-libretro/](https://github.com/libretro/blueMSX-libretro/)
* **Libretro documentation**: [https://docs.libretro.com/library/bluemsx/](https://docs.libretro.com/library/bluemsx/)