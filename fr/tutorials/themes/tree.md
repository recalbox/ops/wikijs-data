---
title: 1. Arborescence
description: 
published: true
date: 2024-06-11T20:32:21.788Z
tags: themes, 9.2+
editor: markdown
dateCreated: 2024-01-23T15:30:29.657Z
---

## Introduction

Les thèmes ont une certaine structure de dossiers et de fichiers. Tout vous est décrit ci-dessous.

Tous les fichiers qui composent un thème sont au format `XML`. C'est un langage simple à utiliser. Il utilise des balises classiques comme `<balise></balise>` ou des balises auto-fermantes `<balise />`.

## Tout un tas de dossiers...

Tout thème est composé de pleins de dossiers.

### Système

Pour chaque système disponible dans Recalbox, vous avez un dossier contenant tous les images correspondantes au système.

### Systèmes virtuels de base

Les systèmes virtuels de base correspondent aux systèmes tels que les favoris, les derniers jeux joués, les jeux multijoueurs, etc.

Voici la liste des systèmes virtuels de base et leur nom de dossier.

| Système virtuel | Nom du dossier |
| :--- | :--- |
| Jeux arcade | arcade |
| Tous les jeux | auto-allgames |
| Derniers jeux joués | auto-lastplayed |
| Lightgun | auto-lightgun |
| Multijoueurs | auto-multiplayer |
| Jeux TATE | auto-tate |
| Jeux favoris | favorites |

### Systèmes virtuels par genre

Les systèmes virtuels par genre correspondent aux genres de jeu que vous récupérez depuis les scraps de vos jeux. Les genres de chacun de vos jeux sont modifiables aussi par vous-même.

Voici la liste des systèmes virtuels par genre et leur nom de dossier.

| Système virtuel | Nom du dossier |
| :--- | :--- |
| Action (tous) | auto-action |
| Action - Battle Royale | auto-actionbattleroyale |
| Action - Beat'em up | auto-actionbeatemup |
| Action - Combat | auto-actionfighting |
| Action - Tir à la première personne | auto-actionfirstpersonshooter |
| Action - Jeu de plateformes | auto-actionplatformer |
| Action - Jeu de tir/plateformes | auto-actionplatformshooter |
| Action - Rythme & musique | auto-actionrythm |
| Action - Shoot-em up | auto-actionshootemup |
| Action - Tir avec accessoires | auto-actionshootwithgun |
| Action - Infiltration | auto-actionstealth |
| Aventure | auto-adventure |
| Aventure - Aventure graphique | auto-adventuregraphics |
| Aventure - Film interactif | auto-adventureinteractivemovie |
| Aventure - Aventure 3D temps réel | auto-adventurerealtime3d |
| Aventure - Jeux de survie | auto-adventuresurvivalhorror |
| Aventure - Aventure textuelle | auto-adventuretext |
| Aventure - Nouvelle visuelle | auto-adventurevisualnovels |
| Jeu de plateau | auto-board |
| Casino | auto-casino |
| Jeu grand public | auto-casual |
| Compilation multi-jeux | auto-compilation |
| Démo de la scène démo | auto-demoscene |
| Cartes digitales | auto-digitalcard |
| Jeu éducatif | auto-educative |
| Jeu de société multijoueurs | auto-party |
| Flipper | auto-pinball |
| Puzzle & logique | auto-puzzleandlogic |
| Jeu de rôles (tous) | auto-rpg |
| Jeu de rôles - Jeu de rôle d'action | auto-rpgaction |
| Jeu de rôles - Exploration de donjons | auto-rpgdungeoncrawler |
| Jeu de rôles - Jeux de rôles de sociétés | auto-rpgfirstpersonpartybased |
| Jeu de rôles - Jeux de rôles japonais | auto-rpgjapanese |
| Jeu de rôles - Jeux de rôles massivement multijoueurs | auto-rpgmmo |
| Jeu de rôles - Jeux de rôles tactique | auto-rpgtactical |
| Simulation | auto-simulation |
| Simulation -  | auto-simulationbuildandmanagement |
| Simulation -  | auto-simulationfishandhunt |
| Simulation -  | auto-simulationlife |
| Simulation -  | auto-simulationscifi |
| Simulation -  | auto-simulationvehicle |
| Sport (tous) | auto-sports |
| Sport - Compétition sportive | auto-sportcompetitive |
| Sport - Combat/Sport violent | auto-sportfight |
| Sport - Course | auto-sportracing |
| Sport - simulation sportives | auto-sportsimulation |
| Stratégie (toutes) | auto-strategy |
| Stratégie - Exploration, expansion, exploitation & extermination | auto-strategy4x |
| Stratégie - Artillerie | auto-strategyartillery |
| Stratégie - Batailles automatique | auto-strategyautobattler |
| Stratégie - Arène de combats multi-joueurs | auto-strategymoba |
| Stratégie - Jeux de stratégie en temps réel | auto-strategyrts |
| Stratégie - Stratégie au tour par tour | auto-strategytbs |
| Stratégie - Défense de tours | auto-strategytowerdefense |
| Stratégie - Jeu de guerre | auto-strategywargame |
| Trivia | auto-trivia |

### Systèmes virtuels arcade par constructeur

Les systèmes vistuels arcade correspondent aux constructeurs arcade référencés.

Voici la liste des systèmes virtuels arcade par constructeur et leur nom de dossier.

| Système virtuel | Nom du dossier |
| :--- | :--- |
| Acclaim | auto-arcade-acclaim |
| Atari | auto-arcade-atari |
| Atlus | auto-arcade-atlus |
| BanPresto | auto-arcade-banpresto |
| Capcom | auto-arcade-capcom |
| Capcom CPS1 | auto-arcade-capcom-cps1 |
| Capcom CPS2 | auto-arcade-capcom-cps2 |
| Capcom CPS3 | auto-arcade-capcom-cps3 |
| CAVE | auto-arcade-cave |
| Data East | auto-arcade-dataeast |
| Exidy | auto-arcade-exidy |
| International Games System (I.G.S) | auto-arcade-igs |
| IREM Software Engineering | auto-arcade-irem |
| Incredible Technologies | auto-arcade-itech |
| Jaleco | auto-arcade-jaleco |
| Kaneko | auto-arcade-kaneko |
| Konami | auto-arcade-konami |
| Midway | auto-arcade-midway |
| Mitchell | auto-arcade-mitchell |
| NAMCO | auto-arcade-namco |
| Neo-Geo | auto-arcade-neogeo |
| Nichibutsu | auto-arcade-nichibutsu |
| Nintendo | auto-arcade-nintendo |
| NMK | auto-arcade-nmk |
| Psikyo | auto-arcade-psikyo |
| Raizing | auto-arcade-raizing |
| Sammy | auto-arcade-sammy |
| SEGA | auto-arcade-sega |
| SEGA System 16 | auto-arcade-sega-system16 |
| SEGA System 32 | auto-arcade-sega-system32 |
| Seibu | auto-arcade-seibu |
| SETA | auto-arcade-seta |
| SNK | auto-arcade-snk |
| TAITO | auto-arcade-taito |
| TAITO F3 | auto-arcade-taito-f3 |
| Technōs | auto-arcade-technos |
| TECMO | auto-arcade-temco |
| TOAPLAN | auto-arcade-toaplan |
| VISCO | auto-arcade-visco |

## ... Et quelques fichiers

A la racine de n'importe quel thème, vous avez un fichier nommé `theme.xml`. Celui-ci contient des informations concernant les objets généraux tel que la configuration du menu, de la barre d'aide, etc.

Dans chaque système vu dessus, vous aurez un fichier xml (le nom du fichier dépendra de votre envie) qui contiendra les informations du système comme les textes d'informations et les couleurs des 4 barres verticales à droite de votre écran avec le thème par défaut.

## Structure des fichiers

La structure des fichiers est la suivante :

```xml
<theme>
  <include>chemin/a/inclure1</include>
  <include>chemin/a/inclure2</include>
  ...
  <include>chemin/a/inclureX</include>
  <view name="...">
    <object>
      <property1>valeur</property1> 
      <property2>valeur</property2> 
      ...
      <propertyX>valeur</propertyX> 
    </object>
    ...
  </view>
  ...
  <view name="...">
    <object>
      ...
    </object>
    ...
  </view>
</theme>
```

Le contenu exact est décrit dans les autres tutoriels concernant les thèmes.

## Fonctionnement des balises `<theme>` et `<include>`

### Balise `<theme>`

Concernant cet exemple, tout fichier xml du thème commence se trouve entre les balises `<theme></theme>`. Avec Recalbox 9.2, cette balise peut contenir 5 attributs :

#### Attribut `name`

- Obligatoire : non

L'attribut `name` indique le nom du thème. Celui-ci peut être différent du nom du dossier du thème. Ce nom sera visible dans la liste des thèmes disponibles.

Exemple :

```xml
<theme name="Mon propre thème">
  ...
</theme>
```

Sans cet attribut, le nom du dossier du thème sera utilisé.

#### Attribut `recalbox`

- Obligatoire : non

L'attribut `recalbox` indique la version minimale de Recalbox auquel le thème peut être utilisé. Ceci vous permet d'indiquer si le thème est compatible avec la version de Recalbox installée.

Exemple :

```xml
<theme recalbox="9.2">
  ...
</theme>
```

Sans cet attribut, le thème pourra être utilisée sur n'importe quelle version de Recalbox.

#### Attribut `version`

- Obligatoire : non

L'attribut `version` indique la version du thème. Si cette information existe, elle sera visible dans la liste des thèmes disponibles.

Exemple :

```xml
<theme version="3.1">
  ...
</theme>
```

Sans cet attribut, la version du thème ne sera pas affichée dans la liste des thèmes.

#### Attribut `compatibility`

- Obligatoire : non

L'attribut `compatibility` indique le type d'écran auquel le thème est compatible. Le thème peut être compatible avec un seul type d'écran ou plusieurs.

Liste des valeurs possibles :

- `hdmi` : écrans HDMI
- `crt` : écrans CRT
- `jamma` : écrans JAMMA
- `tate` : écrans verticaux TATE

Exemple :

```xml
<theme compatibility="hdmi,jamma">
  ...
</theme>
```

Sans cet attribut, le thème sera considéré comme compatible uniquement `hdmi`.

#### Attribut `resolutions`

- Obligatoire : non

L'attribut `resolutions` permet d'indiquer les tranches de résolutions avec lesquelles le thème est compatible. Il peut contenir plusieurs valeurs.

Liste des valeurs possibles :

- `qvga` : résolutions QVGA (<= 288p)
- `vga` : résolutions VGA (<= 576p)
- `hd` : résolutions HD (<= 920p)
- `fhd` : résolutions Full HD (> 920p)

Exemple :

```xml
<theme resolutions="hd,fhd">
  ...
</theme>
```

Sans cet attribut, le thème sera considéré comme compatible uniquement `hd, fhd`.

### Balise `<formatVersion>`

La balise `formatVersion` permet d'indiquer la version du format du contenu régissant le thème.

```xml
<formatVersion>5</formatVersion>
```

### Balise `<include>` 

La balise `<include></include>` sert à aller chercher/inclure un autre fichier contenant d'autres informations lié au thème actuel.

Exemple :

```xml
<include>
  /chemin/vers/fichier
</include>
```

```xml
<include path="/chemin/vers/fichier" />
```

#### Attribut `subset`

- Obligatoire : non

L'attribut `subset` permet de définir le groupe d'options auquel le contenu de la balise `<include>` appartient. Il existe une liste définie des groupes d'options possibles :

- `colorset` : couleurs
- `iconset` : icônes
- `menuset` : menus
- `systemview` : liste des systèmes
- `gamelistview` : liste des jeux
- `gameclipview` : clips de jeux

```xml
<include subset="iconset">
  /chemin/vers/fichier/des/icones.xml
</include>
```

```xml
<include subset="iconset" path="/chemin/vers/fichier/des/icones.xml" />
```

#### Attribut `name`

- Obligatoire : non

L'attribut `name` permet d'indiquer un nom au fichier à inclure.

>Ceci est à utiliser avec l'attribut `subset`.
{.is-info}

```xml
<include subset="menuset" name="Bleu clair">
  /chemin/vers/fichier/des/menus.xml
</include>
```

```xml
<include subset="menuset" name="Bleu clair" path="/chemin/vers/fichier/des/menus.xml" />
```

### Balise `<view>` 

La balise `<view></view>` ainsi que les objets enfants sont décrit dans la [page des objets](./objects).